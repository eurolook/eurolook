﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eurolook.ActiveDirectoryLink.Demo.Plugin
{
    public class DemoCleaner : CleanerBase
    {
        public DemoCleaner(
            IActiveDirectorySearcher activeDirectorySearcher,
            IEurolookAuthorRepository eurolookAuthorRepository)
            : base(activeDirectorySearcher, eurolookAuthorRepository)
        {
        }

        public override bool DeleteUserPermanently(string userName)
        {
            return false;
        }

        public override bool CleanUser(ActiveDirectoryUser activeDirectoryUser)
        {
            return false;
        }
    }
}
