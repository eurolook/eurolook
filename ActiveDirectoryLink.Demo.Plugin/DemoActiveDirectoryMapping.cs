﻿using System.DirectoryServices;
using Eurolook.ActiveDirectoryLink.Extensions;

namespace Eurolook.ActiveDirectoryLink.Demo.Plugin
{
    public class DemoActiveDirectoryMapping : IActiveDirectoryMapping
    {
        public IEnumerable<string> PropertiesToLoad()
        {
            yield return ActiveDirectoryProperty.SamAccountName;

            yield return ActiveDirectoryProperty.GivenName;

            yield return ActiveDirectoryProperty.Sn;

            yield return ActiveDirectoryProperty.Mail;

            yield return ActiveDirectoryProperty.Department;
        }

        public ActiveDirectoryUser CreateActiveDirectoryUser(DirectoryEntry entry)
        {
            var result = new ActiveDirectoryUser
            {
                Login = entry.GetFirstPropertyValue(ActiveDirectoryProperty.SamAccountName),
                LatinFirstName = entry.GetFirstPropertyValue(ActiveDirectoryProperty.GivenName),
                LatinLastName = entry.GetFirstPropertyValue(ActiveDirectoryProperty.Sn),
                Email = entry.GetFirstPropertyValue(ActiveDirectoryProperty.Mail),
                Department = entry.GetFirstPropertyValue(ActiveDirectoryProperty.Department),
            };

            result.Initials = GetInitials(result.LatinFirstName, result.LatinLastName);

            return result;
        }

        private static string GetInitials(string? firstName, string? lastName)
        {
            return $"{firstName?[..1]?.ToUpper()}{lastName?[..1]?.ToUpper()}";
        }
    }
}
