﻿using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.ActiveDirectoryLink.Demo.Plugin
{
    public class DemoActiveDirectoryLinker : ActiveDirectoryLinkerBase
    {
        [UsedImplicitly]
        public DemoActiveDirectoryLinker(
            IActiveDirectorySearcher activeDirectorySearcher,
            IEurolookAuthorRepository eurolookAuthorRepository,
            IActiveDirectoryLinkDatabase activeDirectoryLinkDatabase)
            : base(activeDirectorySearcher, eurolookAuthorRepository, activeDirectoryLinkDatabase)
        {
        }

        public override bool OnBeforeCreateAndLink(OnBeforeCreateLinkArgs args)
        {
            // nothing to be done here
            return true;
        }

        public override void PrepareLinking()
        {
            // nothing to be done here
        }

        public override void InitUserFromActiveDirectory(ActiveDirectoryUser activeDirectoryUser, Author author)
        {
            var authorMapper = new EntityMapper<ActiveDirectoryUser, Author>(activeDirectoryUser, author, author);
            authorMapper.InitFromActiveDirectory(ad => ad.LatinFirstName);
            authorMapper.InitFromActiveDirectory(ad => ad.LatinLastName);
            authorMapper.InitFromActiveDirectory(ad => ad.Gender);
            authorMapper.InitFromActiveDirectory(ad => ad.Email);
            authorMapper.InitFromActiveDirectory(ad => ad.Initials);
            authorMapper.InitFromActiveDirectory(ad => ad.Department, a => a.Service);
            authorMapper.InitFromActiveDirectory(ad => ad.WebAddress);

            // save changes to database
            EurolookAuthorRepository.UpdateAuthor(author, ModificationType.ServerModification);
        }

        public override bool UpdateUserFromActiveDirectory(ActiveDirectoryUser activeDirectoryUser, Author author)
        {
            var authorMapper = new EntityMapper<ActiveDirectoryUser, Author>(activeDirectoryUser, author, author);

            authorMapper.UpdateFromActiveDirectory(a => a.LatinFirstName);
            authorMapper.UpdateFromActiveDirectory(a => a.LatinLastName);
            authorMapper.UpdateFromActiveDirectory(a => a.Gender);
            authorMapper.UpdateFromActiveDirectory(a => a.Email);
            authorMapper.UpdateFromActiveDirectory(a => a.Initials);
            authorMapper.UpdateFromActiveDirectory(a => a.Department, a => a.Service);
            authorMapper.UpdateFromActiveDirectory(a => a.WebAddress);

            if (authorMapper.IsUpdated)
            {
                EurolookAuthorRepository.UpdateAuthor(author, ModificationType.ServerModification);
            }

            return authorMapper.IsUpdated;
        }

        public override string GetMigratedUserName(string login)
        {
            return null;
        }
    }
}
