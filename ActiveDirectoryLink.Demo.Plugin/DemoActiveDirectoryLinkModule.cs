﻿using Autofac;
using Eurolook.ActiveDirectoryLink.AutofacModules;
using JetBrains.Annotations;

namespace Eurolook.ActiveDirectoryLink.Demo.Plugin
{
    public class DemoActiveDirectoryLinkModule : Module, IActiveDirectoryLinkModule
    {
        [UsedImplicitly]
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DemoActiveDirectoryLinker>().As<IActiveDirectoryLinker>();
            builder.RegisterType<DemoActiveDirectoryMapping>().As<IActiveDirectoryMapping>();
            builder.RegisterType<DemoCleaner>().As<ICleaner>();
        }
    }
}
