using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Eurolook.OfficeNotificationBar.NativeEventArgs;
using KeyEventArgs = Eurolook.OfficeNotificationBar.NativeEventArgs.KeyEventArgs;
using MouseEventArgs = Eurolook.OfficeNotificationBar.NativeEventArgs.MouseEventArgs;

namespace Eurolook.OfficeNotificationBar
{
    /// <summary>
    /// Helper class to catch several window messages via sub-classing.
    /// </summary>
    [SuppressMessage("StyleCop", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Names refer to native types.")]
    internal sealed class SubclassedWindow : NativeWindow
    {
        public event EventHandler<ActivateAppEventArgs> ActivateApp;

        public event EventHandler<EventArgs> Activate;

        public event EventHandler<EventArgs> Close;

        public event EventHandler<ParentNotifyEventArgs> ParentNotify;

        public event EventHandler<SizeChangingEventArgs> SizeChanging;

        public event EventHandler<SizeChangedEventArgs> SizeChanged;

        public event EventHandler<WindowPosEventArgs> WindowPosChanging;

        public event EventHandler<WindowPosEventArgs> WindowPosChanged;

        public event EventHandler<VisibilityEventArgs> ShowWindow;

        public event EventHandler<MouseEventArgs> MouseMove;

        public event EventHandler<MouseWheelEventArgs> MouseWheel;

        public event EventHandler<EventArgs> MouseLeave;

        public event EventHandler<KeyEventArgs> KeyDown;

        public event EventHandler<KeyEventArgs> KeyUp;

        public event EventHandler<EventArgs> Char;

        public event EventHandler<EventArgs> KillFocus;

        public event EventHandler<EventArgs> Focus;

        public event EventHandler<EventArgs> Paint;

        public event EventHandler<WindowMessageEventArgs> RichEditShowScrollBar;

        public bool IgnoreEraseBackground { get; set; }

        /// <summary>
        /// Custom message loop.
        /// </summary>
        /// <param name="m">The window message being processed.</param>
        protected override void WndProc(ref Message m)
        {
            if (IgnoreEraseBackground && (SafeNativeMethods.WindowsMessages)m.Msg == SafeNativeMethods.WindowsMessages.WM_NCPAINT)
            {
                m.Result = new IntPtr(1);
                return;
            }

            base.WndProc(ref m);

            switch ((SafeNativeMethods.WindowsMessages)m.Msg)
            {
                case SafeNativeMethods.WindowsMessages.WM_ACTIVATEAPP:
                    {
                        OnActivateApp(m.WParam.ToInt32() != 0);
                        break;
                    }

                case SafeNativeMethods.WindowsMessages.WM_ACTIVATE:
                    {
                        OnActivate();
                        break;
                    }

                case SafeNativeMethods.WindowsMessages.WM_ERASEBKGND:
                    {
                        OnEraseBackground(m.WParam);
                        break;
                    }

                case SafeNativeMethods.WindowsMessages.WM_CLOSE:
                    {
                        OnClose();
                        break;
                    }

                case SafeNativeMethods.WindowsMessages.WM_PARENTNOTIFY:
                    {
                        OnParentNotify(new ParentNotifyEventArgs(m.WParam, m.LParam));
                        break;
                    }

                case SafeNativeMethods.WindowsMessages.WM_SHOWWINDOW:
                    {
                        OnShowWindow(m.WParam.ToInt32() != 0);
                        break;
                    }

                case SafeNativeMethods.WindowsMessages.WM_SIZE:
                    {
                        OnSizeChanged(new SizeChangedEventArgs(m.WParam, m.LParam));
                        break;
                    }

                case SafeNativeMethods.WindowsMessages.WM_SIZING:
                    {
                        OnSizeChanging(new SizeChangingEventArgs(m.WParam, m.LParam));
                        break;
                    }

                case SafeNativeMethods.WindowsMessages.WM_WINDOWPOSCHANGING:
                    {
                        var lParam = m.LParam;
                        OnWindowPosChanging(ref lParam);
                        m.LParam = lParam;
                        break;
                    }

                case SafeNativeMethods.WindowsMessages.WM_WINDOWPOSCHANGED:
                    {
                        var lParam = m.LParam;
                        OnWindowPosChanged(ref lParam);
                        m.LParam = lParam;
                        break;
                    }

                case SafeNativeMethods.WindowsMessages.WM_MOUSEMOVE:
                    {
                        var eventArgs = new MouseEventArgs(m.WParam, m.LParam);
                        OnMouseMove(eventArgs);
                        break;
                    }

                case SafeNativeMethods.WindowsMessages.WM_MOUSEWHEEL:
                    {
                        var eventArgs = new MouseWheelEventArgs(m.WParam, m.LParam);
                        OnMouseWheel(eventArgs);
                        break;
                    }

                case SafeNativeMethods.WindowsMessages.WM_MOUSELEAVE:
                    {
                        OnMouseLeave();
                        break;
                    }

                case SafeNativeMethods.WindowsMessages.WM_KEYDOWN:
                    {
                        var eventArgs = new KeyEventArgs(m.WParam, m.LParam);
                        OnKeyDown(eventArgs);
                        break;
                    }

                case SafeNativeMethods.WindowsMessages.WM_KEYUP:
                    {
                        var eventArgs = new KeyEventArgs(m.WParam, m.LParam);
                        OnKeyUp(eventArgs);
                        break;
                    }

                case SafeNativeMethods.WindowsMessages.WM_CHAR:
                    {
                        OnChar(EventArgs.Empty);
                        break;
                    }

                case SafeNativeMethods.WindowsMessages.WM_KILLFOCUS:
                    {
                        OnKillFocus();
                        break;
                    }

                case SafeNativeMethods.WindowsMessages.WM_SETFOCUS:
                    {
                        OnFocus();
                        break;
                    }

                case SafeNativeMethods.WindowsMessages.WM_PAINT:
                    {
                        OnPaint();
                        break;
                    }

                case SafeNativeMethods.WindowsMessages.EM_SHOWSCROLLBAR:
                    {
                        OnRichEditShowScrollBar(new WindowMessageEventArgs(m.WParam, m.LParam));
                        break;
                    }
            }
        }

        private void OnActivateApp(bool isActivated)
        {
            ActivateApp?.Invoke(this, new ActivateAppEventArgs(isActivated));
        }

        private void OnActivate()
        {
            Activate?.Invoke(this, EventArgs.Empty);
        }

        private void OnEraseBackground(IntPtr hDc)
        {
            if (IgnoreEraseBackground)
            {
                SafeNativeMethods.GetClientRect(Handle, out var rect);
                SafeNativeMethods.SetMapMode(hDc, SafeNativeMethods.MM_ANISOTROPIC);
                SafeNativeMethods.SetWindowExtEx(hDc, 100, 100, IntPtr.Zero);
                SafeNativeMethods.SetViewportExtEx(hDc, rect.Right, rect.Bottom, IntPtr.Zero);

                var brush = SafeNativeMethods.GetSysColorBrush(SafeNativeMethods.COLOR_WINDOW);
                SafeNativeMethods.FillRect(hDc, ref rect, brush);
            }
        }

        private void OnClose()
        {
            Close?.Invoke(this, EventArgs.Empty);
        }

        private void OnParentNotify(ParentNotifyEventArgs parentNotifyEventArgs)
        {
            ParentNotify?.Invoke(this, parentNotifyEventArgs);
        }

        private void OnWindowPosChanged(ref IntPtr lParam)
        {
            var handler = WindowPosChanged;
            if (handler != null)
            {
                var windowPos = (SafeNativeMethods.WindowPos)Marshal.PtrToStructure(lParam, typeof(SafeNativeMethods.WindowPos));
                var e = new WindowPosEventArgs(windowPos);
                handler(this, e);
                Marshal.StructureToPtr(e.WindowPos, lParam, true);
            }
        }

        private void OnWindowPosChanging(ref IntPtr lParam)
        {
            var handler = WindowPosChanging;
            if (handler != null)
            {
                var windowPos = (SafeNativeMethods.WindowPos)Marshal.PtrToStructure(lParam, typeof(SafeNativeMethods.WindowPos));
                var e = new WindowPosEventArgs(windowPos);
                handler(this, e);
                Marshal.StructureToPtr(e.WindowPos, lParam, true);
            }
        }

        private void OnSizeChanging(SizeChangingEventArgs e)
        {
            SizeChanging?.Invoke(this, e);
        }

        private void OnSizeChanged(SizeChangedEventArgs e)
        {
            SizeChanged?.Invoke(this, e);
        }

        private void OnShowWindow(bool bShown)
        {
            ShowWindow?.Invoke(this, new VisibilityEventArgs(bShown));
        }

        private void OnMouseMove(MouseEventArgs e)
        {
            MouseMove?.Invoke(this, e);
        }

        private void OnMouseWheel(MouseWheelEventArgs e)
        {
            MouseWheel?.Invoke(this, e);
        }

        private void OnMouseLeave()
        {
            MouseLeave?.Invoke(this, EventArgs.Empty);
        }

        private void OnKeyDown(KeyEventArgs e)
        {
            KeyDown?.Invoke(this, e);
        }

        private void OnKeyUp(KeyEventArgs e)
        {
            KeyUp?.Invoke(this, e);
        }

        private void OnChar(EventArgs e)
        {
            Char?.Invoke(this, e);
        }

        private void OnKillFocus()
        {
            KillFocus?.Invoke(this, EventArgs.Empty);
        }

        private void OnFocus()
        {
            Focus?.Invoke(this, EventArgs.Empty);
        }

        private void OnPaint()
        {
            Paint?.Invoke(this, EventArgs.Empty);
        }

        private void OnRichEditShowScrollBar(WindowMessageEventArgs e)
        {
            RichEditShowScrollBar?.Invoke(this, e);
        }
    }
}
