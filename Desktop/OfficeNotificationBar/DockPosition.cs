namespace Eurolook.OfficeNotificationBar
{
    public enum DockPosition
    {
        Top,
        Right,
        Bottom,
        Left,
    }
}
