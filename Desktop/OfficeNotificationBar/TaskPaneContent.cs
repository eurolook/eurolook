using System;
using System.Windows.Controls;

namespace Eurolook.OfficeNotificationBar
{
    public class TaskPaneContent : ITaskPaneContent
    {
        public TaskPaneContent(UserControl userControl)
        {
            UserControl = userControl;
            UserControl.Width = 300;
        }

        public event RequestClosingEvent RequestClosing;

        public UserControl UserControl { get; }

        public void Close()
        {
            var handler = RequestClosing;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public void OnRequestClosing()
        {
            Close();
        }
    }
}
