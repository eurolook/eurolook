using System;

namespace Eurolook.OfficeNotificationBar
{
    public class NativeWindowWrapper
    {
        private string _title;
        private string _className;

        public NativeWindowWrapper(IntPtr handle)
        {
            Handle = handle;
        }

        public IntPtr Handle { get; }

        public string Title
        {
            get
            {
                // NOTE: we don't call GetWindowText here because that might hang the application
                //   if the window is not responding (e.g. when switching Word ribbon tabs quickly)
                //   This limitation of the GetWindowText function is documented in MSDN at
                //   https://msdn.microsoft.com/en-us/library/windows/desktop/ms633520.aspx and explained by Raymond Chen
                //   in more detail: [The secret life of GetWindowText](https://blogs.msdn.microsoft.com/oldnewthing/20030821-00/?p=42833/)
                //
                //   The workaround is to use SendMessageTimeout directly
                return _title ??= SafeNativeMethods.GetWindowTextSafe(Handle);
            }
        }

        public string ClassName
        {
            get { return _className ??= SafeNativeMethods.GetClassName(Handle); }
        }
    }
}
