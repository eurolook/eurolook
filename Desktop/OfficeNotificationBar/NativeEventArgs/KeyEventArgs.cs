using System;
using System.Diagnostics.CodeAnalysis;

namespace Eurolook.OfficeNotificationBar.NativeEventArgs
{
    [SuppressMessage("StyleCop", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Names refer to native types.")]
    internal class KeyEventArgs : EventArgs
    {
        [SuppressMessage("ReSharper", "UnusedParameter.Local", Justification = "Default EventArgs signature")]
        public KeyEventArgs(IntPtr wParam, IntPtr lParam)
        {
            VirtualKeyCode = wParam.ToInt32();
        }

        public int VirtualKeyCode { get; }
    }
}
