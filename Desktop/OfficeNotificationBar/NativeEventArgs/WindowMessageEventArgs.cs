using System;

namespace Eurolook.OfficeNotificationBar.NativeEventArgs
{
    internal class WindowMessageEventArgs : EventArgs
    {
        public WindowMessageEventArgs(IntPtr wParam, IntPtr lParam)
        {
            WParam = wParam;
            LParam = lParam;
        }

        public IntPtr WParam { get; }

        public IntPtr LParam { get; }
    }
}
