using System;

namespace Eurolook.OfficeNotificationBar.NativeEventArgs
{
    public class VisibilityEventArgs : EventArgs
    {
        public VisibilityEventArgs(bool isVisible)
        {
            IsVisible = isVisible;
        }

        public bool IsVisible { get; }
    }
}
