using System;
using System.Diagnostics.CodeAnalysis;

namespace Eurolook.OfficeNotificationBar.NativeEventArgs
{
    [SuppressMessage("StyleCop", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Names refer to native types.")]
    internal class ParentNotifyEventArgs : EventArgs
    {
        public ParentNotifyEventArgs(IntPtr wParam, IntPtr lParam)
        {
            Event = (SafeNativeMethods.WindowsMessages)unchecked((short)(long)wParam);

            switch (Event)
            {
                case SafeNativeMethods.WindowsMessages.WM_CREATE:
                case SafeNativeMethods.WindowsMessages.WM_DESTROY:
                    {
                        IdChild = unchecked((short)((long)wParam >> 16));
                        HwndChild = lParam;
                    }

                    break;

                case SafeNativeMethods.WindowsMessages.WM_LBUTTONDOWN:
                case SafeNativeMethods.WindowsMessages.WM_MBUTTONDOWN:
                case SafeNativeMethods.WindowsMessages.WM_RBUTTONDOWN:
                case SafeNativeMethods.WindowsMessages.WM_XBUTTONDOWN:
                    {
                        X = unchecked((short)(long)lParam);
                        Y = unchecked((short)((long)lParam >> 16));
                    }

                    break;
            }
        }

        public SafeNativeMethods.WindowsMessages Event { get; }

        public int IdChild { get; }

        public IntPtr HwndChild { get; }

        public int X { get; }

        public int Y { get; }
    }
}
