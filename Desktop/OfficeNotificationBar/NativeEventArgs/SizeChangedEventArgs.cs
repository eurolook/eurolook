using System;
using System.Diagnostics.CodeAnalysis;

namespace Eurolook.OfficeNotificationBar.NativeEventArgs
{
    [SuppressMessage("StyleCop", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Names refer to native types.")]
    internal class SizeChangedEventArgs : EventArgs
    {
        [SuppressMessage("ReSharper", "UnusedParameter.Local", Justification = "Default EventArgs signature")]
        public SizeChangedEventArgs(IntPtr wParam, IntPtr lParam)
        {
            Width = unchecked((short)(long)lParam);
            Height = unchecked((short)((long)lParam >> 16));
        }

        public int Width { get; }

        public int Height { get; }
    }
}
