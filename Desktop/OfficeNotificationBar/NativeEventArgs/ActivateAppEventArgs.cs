using System;

namespace Eurolook.OfficeNotificationBar.NativeEventArgs
{
    public class ActivateAppEventArgs : EventArgs
    {
        public ActivateAppEventArgs(bool isActivated)
        {
            IsActivated = isActivated;
        }

        public bool IsActivated { get; set; }
    }
}
