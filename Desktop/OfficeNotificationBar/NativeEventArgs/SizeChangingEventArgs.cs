using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;

namespace Eurolook.OfficeNotificationBar.NativeEventArgs
{
    [SuppressMessage("StyleCop", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Names refer to native types.")]
    internal class SizeChangingEventArgs : EventArgs
    {
        [SuppressMessage("ReSharper", "UnusedParameter.Local", Justification = "We keep the same signature for all events.")]
        public SizeChangingEventArgs(IntPtr wParam, IntPtr lParam)
        {
            Rect = (SafeNativeMethods.RECT)Marshal.PtrToStructure(lParam, typeof(SafeNativeMethods.RECT));
        }

        public SafeNativeMethods.RECT Rect { get; }
    }
}
