using System;
using System.Diagnostics.CodeAnalysis;

namespace Eurolook.OfficeNotificationBar.NativeEventArgs
{
    [SuppressMessage("StyleCop", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Names refer to native types.")]
    internal class MouseWheelEventArgs : MouseEventArgs
    {
        public MouseWheelEventArgs(IntPtr wParam, IntPtr lParam)
            : base(wParam, lParam)
        {
            Delta = unchecked((short)((long)wParam >> 16));

            short keyState = unchecked((short)(long)wParam);
            IsCtrlDown = (keyState & (int)SafeNativeMethods.MouseMessageKeyState.MK_CONTROL) != 0;
            IsShiftDown = (keyState & (int)SafeNativeMethods.MouseMessageKeyState.MK_SHIFT) != 0;
        }

        public int Delta { get; }

        public bool IsCtrlDown { get; }

        public bool IsShiftDown { get; }
    }
}
