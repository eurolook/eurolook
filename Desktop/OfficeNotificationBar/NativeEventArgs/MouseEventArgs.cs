using System;
using System.Diagnostics.CodeAnalysis;

namespace Eurolook.OfficeNotificationBar.NativeEventArgs
{
    [SuppressMessage("StyleCop", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Names refer to native types.")]
    internal class MouseEventArgs : EventArgs
    {
        [SuppressMessage("ReSharper", "UnusedParameter.Local", Justification = "Default EventArgs signature")]
        public MouseEventArgs(IntPtr wParam, IntPtr lParam)
        {
            X = unchecked((short)(long)lParam);
            Y = unchecked((short)((long)lParam >> 16));
        }

        public int X { get; }

        public int Y { get; }
    }
}
