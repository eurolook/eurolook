using System;

namespace Eurolook.OfficeNotificationBar.NativeEventArgs
{
    internal class WindowPosEventArgs : EventArgs
    {
        public WindowPosEventArgs(SafeNativeMethods.WindowPos windowPos)
        {
            WindowPos = windowPos;
        }

        public SafeNativeMethods.WindowPos WindowPos { get; set; }
    }
}
