using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.Office.Interop.Word;

namespace Eurolook.OfficeNotificationBar
{
    [SuppressMessage("StyleCop", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Names refer to native types.")]
    internal class SubclassedWordWindow : IDisposable
    {
        public SubclassedWordWindow(Window wdWindow)
        {
            if (wdWindow == null)
            {
                throw new ArgumentNullException(nameof(wdWindow));
            }

            var hWndWordWindow = wdWindow.GetWordWindowHandle();
            var hWndWwF = WordWindowFinder.GetWordWwFHandle(hWndWordWindow);
            var hWndWwB = WordWindowFinder.GetWordWwBHandle(hWndWwF);
            var hWndWwG = WordWindowFinder.GetWordWwGHandle(hWndWwB);

            WwF = new SubclassedWindow();
            WwF.AssignHandle(hWndWwF);

            WwG = new SubclassedWindow();
            WwG.AssignHandle(hWndWwG);
        }

        ~SubclassedWordWindow()
        {
            Dispose(false);
        }

        public SubclassedWindow WwG { get; }

        public SubclassedWindow WwF { get; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        [SuppressMessage("ReSharper", "UnusedParameter.Local", Justification = "This method signature is according to the IDisposable pattern.")]
        protected virtual void Dispose(bool disposing)
        {
            WwF.ReleaseHandle();
            WwG.ReleaseHandle();
        }
    }
}
