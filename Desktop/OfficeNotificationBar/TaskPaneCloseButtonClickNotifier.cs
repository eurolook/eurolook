using System;
using System.Drawing;
using Eurolook.Common.Log;
using Eurolook.OfficeNotificationBar.NativeEventArgs;

namespace Eurolook.OfficeNotificationBar
{
    public class TaskPaneCloseButtonClickNotifier : IDisposable, ICanLog
    {
        // ReSharper disable once InconsistentNaming
        private readonly IntPtr _ancestorNUIPane;
        // ReSharper disable once InconsistentNaming
        private NativeWindowEx _subclassedNUIPane;
        private IntPtr _ancestorMsoCommandBar;
        private NativeWindowEx _subclassedMsoCommandBar;

        public event EventHandler<EventArgs> CloseButtonClicked;

        public TaskPaneCloseButtonClickNotifier(IntPtr taskPaneContentControlHandle)
        {
            var windowFinder = new AncestorWindowFinder(taskPaneContentControlHandle);

            // The task pane is embedded in the following native window tree:
            // Window "OpusApp"                 - Main application window
            // \- Window "MsoDockLeft|Right"    - If docked, the MsoCommandBar has an MsoDockLeft or MsoDockRight parent
            //    \- Window "MsoCommandBar"     - If undocked, this MsoCommandBar is a top-level window
            //       \- Window "MsoWorkPane"
            //          \- Window "NUIPane"
            //             \- Window "NetUIHWND"
            //                \- Window "NetUICtrlNotifySink"
            //                   \- Window "CMMOcxHostChildWindowMixedMode"
            //                      \- Window "CMMOcxHost"
            //                         \- Window "WindowsForms10.Window.8.app.0.c97637_r92_ad2"
            //                            \- Window "WindowsForms10.Window.8.app.0.c97637_r92_ad2"
            //                               \- HwndWrapper[...]
            //
            // The NUIPane will not change once the task pane is created. The parent of NUIPane can change dynamically,
            // e.g. if the task pane is undocked or moved. We register to the WindowPosChanged event of the NUIPane
            // window to get notified whenever the parent changes.
            _ancestorNUIPane = windowFinder.FirstOrDefault(w => w.ClassName == "NUIPane");

            _subclassedNUIPane = new NativeWindowEx((IntPtr)0x30201031, _ancestorNUIPane);
            _subclassedNUIPane.WindowPosChanged += OnWindowPosChanged;
            _subclassedNUIPane.AssignHandle();

            _ancestorMsoCommandBar = IntPtr.Zero;
            _subclassedMsoCommandBar = null;
        }

        ~TaskPaneCloseButtonClickNotifier()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void OnWindowPosChanged(object sender, WindowPosEventArgs e)
        {
            var windowFinder = new AncestorWindowFinder(_ancestorNUIPane);
            var newAncestorMsoCommandBar = windowFinder.FirstOrDefault(w => w.ClassName == "MsoCommandBar");
            if (newAncestorMsoCommandBar == _ancestorMsoCommandBar)
            {
                return;
            }

            // docking of task pane has changed and the task pane NUIPane window got new ancestors
            _ancestorMsoCommandBar = newAncestorMsoCommandBar;
            if (_subclassedMsoCommandBar != null)
            {
                _subclassedMsoCommandBar.ReleaseHandle();
                _subclassedMsoCommandBar.LeftButtonUp -= OnLeftButtonUp;
                _subclassedMsoCommandBar.WindowPosChanged -= OnCommandBarWindowPosChanged;
                _subclassedMsoCommandBar = null;
            }

            if (_ancestorMsoCommandBar != IntPtr.Zero)
            {
                _subclassedMsoCommandBar = new NativeWindowEx((IntPtr)0x30201030, _ancestorMsoCommandBar);
                _subclassedMsoCommandBar.AssignHandle();
                _subclassedMsoCommandBar.LeftButtonUp += OnLeftButtonUp;
                _subclassedMsoCommandBar.WindowPosChanged += OnCommandBarWindowPosChanged;
            }
        }

        private void OnCommandBarWindowPosChanged(object sender, WindowPosEventArgs e)
        {
            var option = e.WindowPos.Option;
            if (option.HasFlag(SafeNativeMethods.SetWindowPosOption.SWP_HIDEWINDOW))
            {
                this.LogTrace("Closing task pane");
            }
        }

        private void OnLeftButtonUp(object sender, MouseEventArgs e)
        {
            // the entire task pane area including borders
            var rectOuter = SafeNativeMethods.GetWindowRectangle(_ancestorMsoCommandBar);

            // the client area of the task pane
            var rectInner = SafeNativeMethods.GetWindowRectangle(_ancestorNUIPane);

            // we assume that left and right border will always have the same width
            var borderWidthLeftRight = rectInner.X - rectOuter.X;

            // we assume that the top border has the same height as the bottom border
            var borderWidthTopBottom = rectOuter.Y + rectOuter.Height - (rectInner.Y + rectInner.Height);

            // There is a border of 1 pixel width between the button and the client area of the task pane.
            // I had expected that this value needs adjusting in HiDPI scenarios but using 1 px seems to work just fine.
            const int insideBorderWidth = 1;

            // Note: the top bar includes the borders
            var rectTopBar = new Rectangle(
                0,
                0,
                rectInner.Width,
                rectOuter.Height - rectInner.Height - 2 * borderWidthTopBottom - insideBorderWidth);

            // the close button is a right aligned square
            var buttonSize = rectTopBar.Height;
            var rectButton = new Rectangle(
                borderWidthLeftRight + rectTopBar.Width - buttonSize,
                borderWidthTopBottom,
                buttonSize,
                buttonSize);

            if (rectButton.Contains(e.X, e.Y))
            {
                // close button clicked
                this.LogTrace(
                    $"Close button clicked at {{X={e.X},Y={e.Y}}}, button {rectButton}, "
                    + $"task pane outer {rectOuter}, task pane inner {rectInner}");

                CloseButtonClicked?.Invoke(this, EventArgs.Empty);
            }
            else
            {
                this.LogTrace(
                    $"Task pane header clicked at {{X={e.X},Y={e.Y}}}, button {rectButton}, "
                    + $"task pane outer {rectOuter}, task pane inner {rectInner}");
            }
        }

        private void ReleaseUnmanagedResources()
        {
            _subclassedNUIPane?.Dispose();
            _subclassedMsoCommandBar?.Dispose();
            _subclassedNUIPane = null;
            _subclassedMsoCommandBar = null;
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_subclassedNUIPane != null)
                {
                    _subclassedNUIPane.WindowPosChanged -= OnWindowPosChanged;
                }
            }

            ReleaseUnmanagedResources();
        }
    }
}
