using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Eurolook.OfficeNotificationBar.NativeEventArgs;
using Microsoft.Office.Interop.Word;
using Application = Microsoft.Office.Interop.Word.Application;
using Rectangle = System.Drawing.Rectangle;
using Task = System.Threading.Tasks.Task;
using Window = System.Windows.Window;

namespace Eurolook.OfficeNotificationBar
{
    [SuppressMessage("StyleCop", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Names refer to native types.")]
    [SuppressMessage("Sonar", "S110:Inheritance tree of classes should not be too deep", Justification = "Deep hierarchy ok for UI.")]
    public class LightBoxWindow : Window
    {
        private readonly Application _application;
        private readonly IntPtr _hWndOpusApp;
        private readonly IntPtr _hWndWwF;

        private readonly SubclassedWindow _subclassedOpusApp;
        private WindowExtents _windowExtents;

        public LightBoxWindow()
        {
            Closed += OnClosed;
            Loaded += OnLoaded;

            AllowsTransparency = true;
            WindowStyle = WindowStyle.None;
            Topmost = false;
            WindowStartupLocation = WindowStartupLocation.Manual;
            ShowInTaskbar = false;
            Background = new SolidColorBrush(Color.FromArgb(160, 160, 160, 160));
        }

        public LightBoxWindow(
            Application application,
            Microsoft.Office.Interop.Word.Window window,
            ILightBoxContent content,
            WindowExtents windowExtents)
            : this()
        {
            Content = content;
            WindowExtents = windowExtents;

            _application = application;
            _application.DocumentBeforeClose += OnApplicationDocumentBeforeClose;

            ////_hWndOpusApp = WaitForMainWindow();
            _hWndOpusApp = window.GetWordWindowHandle();
            _hWndWwF = WordWindowFinder.GetWordWwFHandle(_hWndOpusApp);

            _subclassedOpusApp = new SubclassedWindow();
            _subclassedOpusApp.AssignHandle(_hWndOpusApp);

            _subclassedOpusApp.Close += OnOpusAppClose;
            _subclassedOpusApp.Activate += OnOpusAppActivate;
            _subclassedOpusApp.ActivateApp += OnOpusAppActivateApp;
            _subclassedOpusApp.SizeChanging += OnOpusAppSizeChanging;
            _subclassedOpusApp.WindowPosChanging += OnOpusAppWindowPosChanging;
            _subclassedOpusApp.WindowPosChanged += OnOpusAppWindowPosChanged;

            var wndHelper = new WindowInteropHelper(this)
            {
                Owner = _hWndOpusApp,
            };

            if (content != null)
            {
                content.RequestClosing += OnContentRequestClosing;
            }
        }

        public WindowExtents WindowExtents
        {
            get { return _windowExtents; }

            private set
            {
                _windowExtents = value;
                SetSplashPosition();
            }
        }

        public async Task ShowAndFadeIn()
        {
            Opacity = 0.0;

            Show();

            // fade in
            var dblaOpacityAnimation = new DoubleAnimation
            {
                From = 0.0,
                To = 1.0,
                Duration = new Duration(TimeSpan.FromSeconds(0.25)),
            };
            Storyboard.SetTarget(dblaOpacityAnimation, this);
            Storyboard.SetTargetProperty(dblaOpacityAnimation, new PropertyPath(OpacityProperty));

            var strbStoryBoard = new Storyboard();
            strbStoryBoard.Children.Add(dblaOpacityAnimation);
            await strbStoryBoard.BeginAsync(this);
        }

        public void CloseAndFadeOut()
        {
            // fade out on closing
            var duration = new Duration(TimeSpan.FromSeconds(0.25));

            var dblaHeightAnimation = new DoubleAnimation
            {
                From = ActualHeight,
                To = 0,
                Duration = duration,
            };
            Storyboard.SetTarget(dblaHeightAnimation, this);
            Storyboard.SetTargetProperty(dblaHeightAnimation, new PropertyPath(HeightProperty));

            var dblaWidthAnimation = new DoubleAnimation
            {
                From = ActualWidth,
                To = 0,
                Duration = duration,
            };
            Storyboard.SetTarget(dblaWidthAnimation, this);
            Storyboard.SetTargetProperty(dblaWidthAnimation, new PropertyPath(WidthProperty));

            var dblaOpacityAnimation = new DoubleAnimation
            {
                From = Opacity,
                To = 0.0,
                Duration = duration,
            };
            Storyboard.SetTarget(dblaOpacityAnimation, this);
            Storyboard.SetTargetProperty(dblaOpacityAnimation, new PropertyPath(OpacityProperty));

            var strbStoryBoard = new Storyboard();
            strbStoryBoard.Completed += FadeOutCompleted;
            strbStoryBoard.Children.Add(dblaOpacityAnimation);

            strbStoryBoard.Begin(this);
        }

        ////private IntPtr WaitForMainWindow()
        ////{
        ////    // Use the window class name ("OpusApp") to retrieve a handle to Word's main window.
        ////    // Alternatively you can get the window handle via the process id:
        ////    // int hwnd = (int)Process.GetProcessById(wordPid).MainWindowHandle
        ////    var windowFinder = new WindowFinder
        ////    {
        ////        FindVisibleOnly = true
        ////    };

        ////    // wait for the main window to be created
        ////    for (int i = 0; i < 2000; i++)
        ////    {
        ////        var window = windowFinder.FirstOrDefault(w => w.ClassName == "OpusApp");
        ////        if (window != IntPtr.Zero)
        ////        {
        ////            return window;
        ////        }

        ////        Thread.Sleep(10);
        ////    }

        ////    return IntPtr.Zero;
        ////}

        private void SetSplashPosition()
        {
            var rect = GetClientWindowCoordinates();
            SetWindowPos(rect);
        }

        private void SetWindowPos(Rectangle rect)
        {
            SetWindowPos(rect.Left, rect.Top, rect.Width, rect.Height);
        }

        private void SetWindowPos(int x, int y, int width, int heigth)
        {
            var source = PresentationSource.FromVisual(this);

            // handle large fonts / dpi scaling
            if (source != null && source.CompositionTarget != null)
            {
                double scaleX = source.CompositionTarget.TransformToDevice.M11;
                double scaleY = source.CompositionTarget.TransformToDevice.M22;

                x = (int)(x / scaleX);
                y = (int)(y / scaleY);
                width = (int)(width / scaleX);
                heigth = (int)(heigth / scaleY);
            }

            Left = x;
            Top = y;
            Width = width;
            Height = heigth;
        }

        private void FadeOutCompleted(object sender, EventArgs e)
        {
            Close();
        }

        private Rectangle GetClientWindowCoordinates()
        {
            // get document window
            var rectWwF = SafeNativeMethods.GetWindowRectangle(_hWndWwF);
            var rectOpusApp = SafeNativeMethods.GetClientRectangle(_hWndOpusApp);
            rectOpusApp = SafeNativeMethods.ClientToScreen(_hWndOpusApp, rectOpusApp);

            // increase by size of task panes etc
            int left = WindowExtents.HasFlag(WindowExtents.DockLeft) ? rectOpusApp.Left : rectWwF.Left;
            int right = WindowExtents.HasFlag(WindowExtents.DockRight) ? rectOpusApp.Right : rectWwF.Right;

            int top = WindowExtents.HasFlag(WindowExtents.DockTop) ? rectOpusApp.Top : rectWwF.Top;
            int bottom = WindowExtents.HasFlag(WindowExtents.DockBottom) ? rectOpusApp.Bottom : rectWwF.Bottom;

            return new Rectangle(left, top, right - left, bottom - top);
        }

        private void OnApplicationDocumentBeforeClose(Document doc, ref bool cancel)
        {
            if (IsVisible && doc.ActiveWindow.GetWordWindowHandle() == _hWndOpusApp)
            {
                Close();
            }
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            Loaded -= OnLoaded;

            SetSplashPosition();
        }

        private void OnClosed(object sender, EventArgs eventArgs)
        {
            _subclassedOpusApp.ReleaseHandle();

            _application.DocumentBeforeClose -= OnApplicationDocumentBeforeClose;

            SafeNativeMethods.SetForegroundWindow(_hWndOpusApp);
        }

        private void OnOpusAppActivateApp(object sender, ActivateAppEventArgs e)
        {
            if (e.IsActivated)
            {
                Activate();
                Focus();
            }
        }

        private void OnOpusAppActivate(object sender, EventArgs e)
        {
            ////SetSplashPosition();
            ////this.Activate();
            ////this.Focus();
        }

        private void OnOpusAppWindowPosChanging(object sender, WindowPosEventArgs e)
        {
            SetSplashPosition();
        }

        private void OnOpusAppWindowPosChanged(object sender, WindowPosEventArgs e)
        {
            SetSplashPosition();
        }

        private void OnOpusAppSizeChanging(object sender, SizeChangingEventArgs e)
        {
            SetSplashPosition();
        }

        private void OnOpusAppClose(object sender, EventArgs e)
        {
            Close();
        }

        private void OnContentRequestClosing(object sender, EventArgs eventArgs)
        {
            CloseAndFadeOut();
        }
    }

    public static class StoryboardExtensions
    {
        public static Task BeginAsync(this Storyboard storyboard, FrameworkElement element)
        {
            var tcs = new TaskCompletionSource<bool>();
            if (storyboard == null)
            {
                tcs.SetException(new ArgumentNullException(nameof(storyboard)));
            }
            else
            {
                EventHandler onComplete = null;
                onComplete = (s, e) =>
                             {
                                 storyboard.Completed -= onComplete;
                                 tcs.SetResult(true);
                             };
                storyboard.Completed += onComplete;
                storyboard.Begin(element);
            }

            return tcs.Task;
        }
    }
}
