using System;
using System.Windows.Controls;

namespace Eurolook.OfficeNotificationBar
{
    public delegate void RequestClosingEvent(object sender, EventArgs e);

    public interface ICloseableContent
    {
        event RequestClosingEvent RequestClosing;

        UserControl UserControl { get; }

        void OnRequestClosing();
    }

    public interface INotificationBarContent : ICloseableContent
    {
    }

    public interface IFloatingToolbarContent : ICloseableContent
    {
    }

    public interface ITaskPaneContent : ICloseableContent
    {
    }

    public interface ILightBoxContent : ICloseableContent
    {
    }
}
