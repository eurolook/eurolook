using System;

namespace Eurolook.OfficeNotificationBar
{
    [Flags]
    public enum WindowExtents
    {
        None = 0x0,
        DocumentFrameOnly = 0x0,
        DockLeft = 0x1,
        DockRight = 0x2,
        DockTop = 0x4,
        DockBottom = 0x8,
    }
}
