using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Eurolook.OfficeNotificationBar
{
    [SuppressMessage("StyleCop", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Names refer to native types.")]
    public class ChildWindowFinder : WindowFinderBase
    {
        private readonly IntPtr _hWndParent;
        private readonly List<IntPtr> _windowHandles = new List<IntPtr>();
        private Func<NativeWindowWrapper, bool> _filterCondition;
        private bool _stopAfterFirstMatch;

        public ChildWindowFinder(IntPtr hWndParent)
        {
            _hWndParent = hWndParent;
        }

        public IntPtr FirstOrDefault(Func<NativeWindowWrapper, bool> filterCondition)
        {
            _stopAfterFirstMatch = true;
            _filterCondition = filterCondition;
            return GetWindows().FirstOrDefault();
        }

        public IEnumerable<IntPtr> Where(Func<NativeWindowWrapper, bool> filterCondition)
        {
            _stopAfterFirstMatch = false;
            _filterCondition = filterCondition;
            return GetWindows();
        }

        private IEnumerable<IntPtr> GetWindows()
        {
            _windowHandles.Clear();

            var ewp = new SafeNativeMethods.EnumWindowsProc(EvalChildWindow);
            var lParam = IntPtr.Zero;
            SafeNativeMethods.EnumChildWindows(_hWndParent, ewp, ref lParam);

            return _windowHandles;
        }

        private bool EvalChildWindow(IntPtr hWnd, ref IntPtr lParam)
        {
            if (!SafeNativeMethods.IsWindowVisible(hWnd) && FindVisibleOnly)
            {
                return true;
            }

            var window = new NativeWindowWrapper(hWnd);
            if (_filterCondition(window))
            {
                _windowHandles.Add(hWnd);
                return !_stopAfterFirstMatch;
            }

            return true;
        }
    }
}
