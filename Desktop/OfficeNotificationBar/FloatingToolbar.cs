﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Threading;
using Eurolook.OfficeNotificationBar.NativeEventArgs;
using Microsoft.Office.Interop.Word;
using KeyEventArgs = Eurolook.OfficeNotificationBar.NativeEventArgs.KeyEventArgs;
using MouseEventArgs = Eurolook.OfficeNotificationBar.NativeEventArgs.MouseEventArgs;
using MouseWheelEventArgs = Eurolook.OfficeNotificationBar.NativeEventArgs.MouseWheelEventArgs;
using Range = Microsoft.Office.Interop.Word.Range;
using Rectangle = System.Drawing.Rectangle;
using SizeChangedEventArgs = System.Windows.SizeChangedEventArgs;
using Timer = System.Threading.Timer;
using Window = Microsoft.Office.Interop.Word.Window;

namespace Eurolook.OfficeNotificationBar
{
    [SuppressMessage("StyleCop", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Names refer to native types.")]
    public sealed class FloatingToolbar : IDisposable
    {
        private static readonly object Lock = new object();
        private readonly IFloatingToolbarContent _content;
        private readonly DockPosition _dockPosition;

        private readonly HwndSource _hWndSource;

        private readonly IntPtr _hWndWordWindow;
        private readonly IntPtr _hWndWwB;
        private readonly IntPtr _hWndWwG;

        private readonly NativeWindowEx _subclassedWordWindow;
        private readonly NativeWindowEx _subclassedWwB;
        private readonly NativeWindowEx _subclassedWwG;

        // ReSharper disable once InconsistentNaming
        private readonly NativeWindowEx _subclassedRibbonNetUIHWND;

        private readonly Window _wordWindow;

        private Timer _updateTimer;
        private DateTime _nextPositionUpdateEarliestAt = DateTime.MinValue;

        private Rectangle _associatedObjectClientRect;
        private Rectangle _floatingToolbarClientRect;

        private bool _isMouseInDocumentWindow;

        public FloatingToolbar(Window wdWindow, IFloatingToolbarContent content, DockPosition dockPosition)
        {
            _wordWindow = wdWindow ?? throw new ArgumentNullException(nameof(wdWindow));
            _content = content ?? throw new ArgumentNullException(nameof(content));
            _dockPosition = dockPosition;

            _updateTimer = new Timer(OnUpdatePositionDelayed);

            _hWndWordWindow = wdWindow.GetWordWindowHandle();
            var hWndWwF = WordWindowFinder.GetWordWwFHandle(_hWndWordWindow);
            _hWndWwB = WordWindowFinder.GetWordWwBHandle(hWndWwF);
            _hWndWwG = WordWindowFinder.GetWordWwGHandle(_hWndWwB);

            var hWnd = new ChildWindowFinder(_hWndWordWindow).FirstOrDefault(
                w => w.Title == "Ribbon" && w.ClassName == "MsoWorkPane");
            var hWndRibbon = new ChildWindowFinder(hWnd).FirstOrDefault(w => w.ClassName == "NetUIHWND");

            _subclassedWordWindow = new NativeWindowEx((IntPtr)0x30201007, _hWndWordWindow);
            _subclassedWwB = new NativeWindowEx((IntPtr)0x30201008, _hWndWwB);
            _subclassedWwG = new NativeWindowEx((IntPtr)0x30201009, _hWndWwG);
            _subclassedRibbonNetUIHWND = new NativeWindowEx((IntPtr)0x3020100A, hWndRibbon);

            var parameters = new HwndSourceParameters
            {
                ParentWindow = _hWndWwG,
                RestoreFocusMode = RestoreFocusMode.Auto,
                AcquireHwndFocusInMenuMode = true,
                TreatAsInputRoot = true,
                PositionX = -1000,
                PositionY = -1000,
                Height = 100,
                Width = 100,
                WindowName = "Eurolook Toolbar",
                WindowStyle =
                    (int)(SafeNativeMethods.WindowStyles.WS_CHILD | SafeNativeMethods.WindowStyles.WS_VISIBLE),
            };
            _hWndSource = new HwndSource(parameters)
            {
                RootVisual = _content.UserControl,
                SizeToContent = SizeToContent.WidthAndHeight,
            };
            if (_hWndSource.CompositionTarget != null)
            {
                _hWndSource.CompositionTarget.BackgroundColor = Colors.White;
            }
        }

        ~FloatingToolbar()
        {
            Dispose(false);
        }

        public bool IsVisible
        {
            get => SafeNativeMethods.IsWindowVisible(_hWndSource.Handle);
            set
            {
                var flag = value ? SafeNativeMethods.ShowWindowStyle.Show : SafeNativeMethods.ShowWindowStyle.Hide;
                SafeNativeMethods.ShowWindow(_hWndSource.Handle, flag);
                SafeNativeMethods.InvalidateRect(_hWndWordWindow, IntPtr.Zero, true);
            }
        }

        public bool IsSubclassed
        {
            get
            {
                lock (Lock)
                {
                    return _subclassedWwG.IsSubclassed == true;
                }
            }
        }

        public bool IsAttached => AssociatedObject != null;

        public bool IsInPrintLayoutView
        {
            get
            {
                try
                {
                    return _wordWindow.View.Type == WdViewType.wdPrintView;
                }
                catch (COMException)
                {
                    return false;
                }
            }
        }

        public int DistanceFromObject { get; set; } = 5;

        private object AssociatedObject { get; set; }

        public void AttachTo(ContentControl contentControl)
        {
            if (contentControl == null)
            {
                throw new ArgumentNullException(nameof(contentControl));
            }

            AttachTo((object)contentControl);
        }

        public void AttachTo(Shape shape)
        {
            if (shape == null)
            {
                throw new ArgumentNullException(nameof(shape));
            }

            AttachTo((object)shape);
        }

        public void AttachTo(Range range)
        {
            if (range == null)
            {
                throw new ArgumentNullException(nameof(range));
            }

            AttachTo((object)range);
        }

        public void AttachTo(object obj)
        {
            lock (Lock)
            {
                IsVisible = false;
                if (AssociatedObject != null)
                {
                    UnwireEventHandlers();
                    UnSubclass();
                }

                AssociatedObject = obj;
                Subclass();
                WireEventHandlers();

                InitializePosition();
            }
        }

        public void Detach()
        {
            lock (Lock)
            {
                if (IsAttached)
                {
                    AssociatedObject = null;
                    IsVisible = false;
                    UnwireEventHandlers();
                    UnSubclass();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            lock (Lock)
            {
                AssociatedObject = null;
                IsVisible = false;
                UnwireEventHandlers();
                UnSubclass();
            }

            if (!disposing)
            {
                _updateTimer?.Change(Timeout.Infinite, Timeout.Infinite);
                _updateTimer?.Dispose();
                _updateTimer = null;
            }

            var hWndSource = _hWndSource;
            if (hWndSource?.Dispatcher != null && !hWndSource.IsDisposed)
            {
                if (hWndSource.Dispatcher.CheckAccess())
                {
                    hWndSource.Dispose();
                }
                else
                {
                    hWndSource.Dispatcher.Invoke(hWndSource.Dispose, DispatcherPriority.Normal);
                }
            }
        }

        private void OnDocumentWindowKillFocus(object sender, EventArgs e)
        {
            // FIX (EUROLOOK-612): If we lose focus we need to unsubclass, otherwise Word might no longer
            //  accept mouse events. This happens e.g. in the following case: type text inside a content control,
            //  then, with the cursor at the end of the content control change the font via the font selection box
            //  in the ribbon, then move the cursor outside the content control
            //  --> Word does no longer process mouse events.
            //
            //  The fix is to unwire all event handlers and only listen for the Focus event.
            //  In the Focus event handler events are re-wired again and the toolbar position is updated.
            //
            // FIX (ECADOCS-400): Change to the previous fix for EUROLOOK-612:
            //  If we do not completely unsubclass, Word might crash with an access violation, so we completely detach
            //  the Brick Bar now, whenever focus is lost.
            Detach();
        }

        private void OnDocumentWindowMouseLeave(object sender, EventArgs e)
        {
            _isMouseInDocumentWindow = false;
        }

        private void OnWindowSelectionChange(Selection sel)
        {
            var hWndActiveWindow = SafeNativeMethods.GetActiveWindow();
            if (hWndActiveWindow == _hWndWordWindow)
            {
                // delay triggering the event (this reduces the number of events raised in case of rapid typing)
                UpdatePositionDelayed(300);
            }
        }

        private void OnDocumentWindowPaint(object sender, EventArgs e)
        {
            UpdatePositionDelayed(20);
        }

        private void OnContentSizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdatePositionDelayed(5);
        }

        private void OnDocumentWindowClose(object sender, EventArgs e)
        {
            Detach();
        }

        private void OnDocumentWindowShowWindow(object sender, VisibilityEventArgs e)
        {
            if (!e.IsVisible)
            {
                Detach();
            }
        }

        private void OnDocumentWindowSizeChanged(object sender, EventArgs e)
        {
            UpdatePositionDelayed(20);
        }

        private void OnDocumentWindowPosChanging(object sender, WindowPosEventArgs e)
        {
            UpdatePositionDelayed(20);
        }

        private void OnDocumentWindowMouseWheel(object sender, MouseWheelEventArgs e)
        {
            UpdatePositionDelayed(20);
        }

        private void OnDocumentWindowZoomChanged(object sender, WindowMessageEventArgs windowMessageEventArgs)
        {
            UpdatePositionDelayed(20);
        }

        private void OnDocumentWindowKeyDown(object sender, KeyEventArgs e)
        {
            HideWhileTyping(e.VirtualKeyCode);
        }

        private void OnDocumentWindowKeyUp(object sender, KeyEventArgs e)
        {
            HideWhileTyping(e.VirtualKeyCode);
        }

        private void OnDocumentWindowMouseMove(object sender, MouseEventArgs e)
        {
            if (!SafeNativeMethods.IsWindow(_hWndWwG))
            {
                return;
            }

            if (!_isMouseInDocumentWindow)
            {
                _isMouseInDocumentWindow = true;
                UpdatePosition();
            }

            double opacity = ComputeOpacityForMousePosition(e);
            _content.UserControl.Opacity = opacity;
        }

        private void OnWordWindowParentNotify(object sender, ParentNotifyEventArgs e)
        {
            // detach if the user clicks somewhere outside the document window
            // without detaching Word might no longer accept mouse events (cf. OnDocumentWindowKillFocus)
            if (e.Event == SafeNativeMethods.WindowsMessages.WM_LBUTTONDOWN && !_isMouseInDocumentWindow)
            {
                var documentWindowScreenRect = SafeNativeMethods.GetWindowRectangle(_hWndWwG);
                var documentWindowRelativeRect =
                    SafeNativeMethods.ScreenToClient(_hWndWordWindow, documentWindowScreenRect);
                if (!documentWindowRelativeRect.Contains(e.X, e.Y))
                {
                    Detach();
                }
            }
        }

        private void OnRibbonMouseMove(object sender, MouseEventArgs e)
        {
            Detach();
        }

        private void OnRequestClosing(object sender, EventArgs args)
        {
            Detach();
        }

        private void OnUpdatePositionDelayed(object state)
        {
            Dispatcher.CurrentDispatcher.Invoke(UpdatePosition);
        }

        private void HideWhileTyping(int virtualKeyCode)
        {
            bool keyImpactsText = virtualKeyCode >= 0x30 || virtualKeyCode == (int)Keys.Delete
                                                         || virtualKeyCode == (int)Keys.Back
                                                         || virtualKeyCode == (int)Keys.Space;
            if (keyImpactsText)
            {
                IsVisible = false;
                UpdatePositionDelayed(500);
            }

            UpdatePositionDelayed(20);
        }

        private void InitializePosition()
        {
            _nextPositionUpdateEarliestAt = DateTime.MinValue;
            UpdatePositionDelayed(100);
        }

        private void UpdatePositionDelayed(int delayInMilliseconds)
        {
            var nextPositionUpdateRequestedAt = DateTime.UtcNow.AddMilliseconds(delayInMilliseconds);
            if (nextPositionUpdateRequestedAt < _nextPositionUpdateEarliestAt)
            {
                // position updates can only be postponed
                return;
            }

            _nextPositionUpdateEarliestAt = nextPositionUpdateRequestedAt;
            _updateTimer?.Change(delayInMilliseconds, Timeout.Infinite);
        }

        private void UpdatePosition()
        {
            try
            {
                if (!IsAttached || !IsSubclassed)
                {
                    IsVisible = false;
                    return;
                }

                if (!SafeNativeMethods.IsWindow(_hWndWwB) || !SafeNativeMethods.IsWindow(_hWndWwG))
                {
                    Detach();
                    return;
                }

                if (!IsInPrintLayoutView)
                {
                    // the toolbar can only be used in print layout view
                    // in all other views (e.g. print preview, draft, reading view) we detach it
                    Detach();
                    return;
                }

                if (SafeNativeMethods.GetForegroundWindow() != _hWndWordWindow)
                {
                    return;
                }

                if (!UpdateAssociatedObjectClientRect())
                {
                    Detach();
                    return;
                }

                var position = ComputeToolbarPosition();

                SafeNativeMethods.SetWindowPos(
                    _hWndSource.Handle,
                    IntPtr.Zero,
                    position.X,
                    position.Y,
                    position.Width,
                    position.Height,
                    SafeNativeMethods.SetWindowPosOption.SWP_NOZORDER);

                SafeNativeMethods.SetWindowPos(
                    _hWndSource.Handle,
                    (IntPtr)SafeNativeMethods.SetWindowPosZOrder.HWND_TOPMOST,
                    0,
                    0,
                    0,
                    0,
                    SafeNativeMethods.SetWindowPosOption.SWP_NOMOVE | SafeNativeMethods.SetWindowPosOption.SWP_NOSIZE);

                IsVisible = _isMouseInDocumentWindow;

                _floatingToolbarClientRect = position;
            }
            catch (Exception)
            {
                Detach();
            }
        }

        private Rectangle ComputeToolbarPosition()
        {
            var windowRectangle = SafeNativeMethods.GetWindowRectangle(_hWndSource.Handle);

            var result = new Rectangle(
                _associatedObjectClientRect.X,
                _associatedObjectClientRect.Y,
                windowRectangle.Width,
                windowRectangle.Height);

            int distance = DistanceFromObject;
            switch (_dockPosition)
            {
                case DockPosition.Top:
                    result.Y -= windowRectangle.Height + distance;
                    break;
                case DockPosition.Right:
                    result.X += _associatedObjectClientRect.Width + distance;
                    break;
                case DockPosition.Left:
                    result.X -= windowRectangle.Width + distance;
                    break;
                case DockPosition.Bottom:
                    result.Y += _associatedObjectClientRect.Height + distance;
                    break;
            }

            return result;
        }

        private bool UpdateAssociatedObjectClientRect()
        {
            var contentControl = AssociatedObject as ContentControl;
            var associatedObjectRange = contentControl != null ? contentControl.Range : AssociatedObject as Range;
            if (associatedObjectRange == null)
            {
                return false;
            }

            var rangeStart = associatedObjectRange.Duplicate;
            rangeStart.Collapse();
            var rangeStartScreenRect = GetScreenRectFromObject(rangeStart);
            if (rangeStartScreenRect == null || rangeStartScreenRect.Value.Top < -1000)
            {
                return false;
            }

            var rangeEnd = associatedObjectRange.Duplicate;
            rangeEnd.Collapse(WdCollapseDirection.wdCollapseEnd);

            var rangeEndScreenRect = GetScreenRectFromObject(rangeEnd);

            int x = rangeStartScreenRect.Value.X;
            int y = rangeStartScreenRect.Value.Y;
            int width = rangeEndScreenRect != null ? rangeEndScreenRect.Value.X - rangeStartScreenRect.Value.X : 0;
            int height = rangeEndScreenRect != null ? rangeEndScreenRect.Value.Y - rangeStartScreenRect.Value.Y : 0;

            if (contentControl != null && HasBoundingBox(contentControl))
            {
                // The content control label is shown in its own native window with class "Net UI Tool Window".
                // We retrieve all windows of this type and look whether any of them are near the range where the
                // selection is
                var childWindows = new ChildWindowFinder(_hWndWwG)
                                   .Where(
                                       w => w.ClassName == "Net UI Tool Window" && SafeNativeMethods.IsWindow(w.Handle)
                                           && SafeNativeMethods.IsWindowVisible(
                                               w.Handle)).ToList();

                if (childWindows.Any())
                {
                    var childWindowPositions = childWindows.Select(SafeNativeMethods.GetWindowRectangle).ToList();

                    foreach (var rect in childWindowPositions)
                    {
                        Debug.WriteLine(
                            $"X: {x}, Y: {y} Rect X1: {rect.X} Y1: {rect.Y} - X2: {rect.X + rect.Width} Y2: {rect.Y + rect.Height}");
                    }

                    // Possible candidates must be at the current x position or to the left thereof,
                    // the vertical distance should be close enough (we assume closer than 100 px).
                    childWindowPositions = childWindowPositions.Where(rect => rect.X <= x && Math.Abs(rect.Y - y) < 100)
                                                               .ToList();
                    if (childWindowPositions.Any())
                    {
                        int minX = childWindowPositions.Select(rect => rect.X).Min();
                        x = Math.Min(x, minX);

                        _associatedObjectClientRect = SafeNativeMethods.ScreenToClient(
                            _hWndWwG,
                            new Rectangle(x, y, width, height));
                        return true;
                    }
                }
            }

            if ((rangeEndScreenRect == null) || (rangeEndScreenRect.Value.Y != rangeStartScreenRect.Value.Y))
            {
                // extend the bounding rectangle for multiline ranges to the horizontal page boundaries
                // a range goes over multiple lines if the end of the range has a different
                // Y coordinate than the beginning
                try
                {
                    // multiline range, get bounds from current page
                    var rangePageScreenRect = GetPageScreenRectFromObject(associatedObjectRange);

                    var actualEndScreenRect = rangePageScreenRect ?? rangeEndScreenRect;
                    if (actualEndScreenRect != null)
                    {
                        // adjust rectangle for the associated object
                        x = Math.Min(x, actualEndScreenRect.Value.X);
                        width = Math.Max(width, actualEndScreenRect.Value.Width);

                        // for multi-page content controls we need to adjust the Y coordinates to the one of the start of the content control
                        // and set the height to the height of the part of the content control before the page break
                        y = Math.Max(y, rangeStartScreenRect.Value.Y);
                        height = actualEndScreenRect.Value.Height + actualEndScreenRect.Value.Y
                                 - rangeStartScreenRect.Value.Y;
                    }
                }
                catch (COMException ex)
                {
                    // HRESULT 0x800a1066 probably means that the range is locked
                    if (ex.ErrorCode != -2146824090)
                    {
                        ////throw;
                    }
                }
            }

            _associatedObjectClientRect =
                SafeNativeMethods.ScreenToClient(_hWndWwG, new Rectangle(x, y, width, height));
            return true;
        }

        private bool HasBoundingBox(ContentControl contentControl)
        {
            try
            {
                return ((dynamic)contentControl).Appearance == 0;
            }
            catch
            {
                return true;
            }
        }

        private Rectangle? GetScreenRectFromObject(Range range)
        {
            if (range == null)
            {
                return null;
            }

            int screenPixelsLeft;
            int screenPixelsTop;
            int screenPixelsWidth;
            int screenPixelsHeight;
            try
            {
                _wordWindow.GetPoint(
                    out screenPixelsLeft,
                    out screenPixelsTop,
                    out screenPixelsWidth,
                    out screenPixelsHeight,
                    range);
            }
            catch (COMException)
            {
                // fallback if GetPoint fails on a particular range: get position for beginning and end of range separately
                if (range.Start != range.End)
                {
                    var rangeBegin = range.Duplicate;
                    rangeBegin.Collapse();

                    var rangeEnd = range.Duplicate;
                    rangeEnd.Collapse(WdCollapseDirection.wdCollapseEnd);

                    _wordWindow.GetPoint(out int left, out int top, out _, out _, rangeBegin);
                    screenPixelsLeft = left;
                    screenPixelsTop = top;

                    _wordWindow.GetPoint(out left, out top, out screenPixelsWidth, out screenPixelsHeight, rangeBegin);
                    screenPixelsWidth = left - screenPixelsLeft;
                    screenPixelsHeight = top - screenPixelsTop;
                }
                else
                {
                    // unable to retrieve the position of the range
                    return null;
                }
            }

            return new Rectangle(screenPixelsLeft, screenPixelsTop, screenPixelsWidth, screenPixelsHeight);
        }

        private Rectangle? GetPageScreenRectFromObject(Range range)
        {
            try
            {
                var rangePage = range.Bookmarks[@"\Page"].Range;
                var rangePageScreenRect = GetScreenRectFromObject(rangePage);
                return rangePageScreenRect;
            }
            catch (Exception)
            {
                // ignore errors if we can't access the \Page bookmark
            }

            return null;
        }

        private double ComputeOpacityForMousePosition(MouseEventArgs e)
        {
            var rect = SafeNativeMethods.GetWindowRect(_hWndWwG);

            // bounding rect consist of the entire range (e.g. a content control) *and* the toolbar itself
            var boundingRect = Rectangle.Union(_associatedObjectClientRect, _floatingToolbarClientRect);

            double opacityX = 1.0;
            if ((e.X >= 0) && (e.X < boundingRect.X))
            {
                opacityX = e.X / (double)boundingRect.X;
            }
            else if (e.X > boundingRect.X + boundingRect.Width)
            {
                opacityX = (e.X - rect.Width) / (boundingRect.X + boundingRect.Width - rect.Width);
            }

            double opacityY = 1.0;
            if ((e.Y >= 0) && (e.Y < boundingRect.Y))
            {
                opacityY = e.Y / (double)boundingRect.Y;
            }
            else if (e.Y > boundingRect.Y + boundingRect.Height)
            {
                opacityY = (e.Y - rect.Height) / (boundingRect.Y + boundingRect.Height - rect.Height);
            }

            // decrease opacity the farther the mouse is away from the associated object / toolbar
            double opacity = Math.Max(0.4, Math.Sqrt(opacityX * opacityY));
            return opacity;
        }

        private void WireEventHandlers()
        {
            if (!SafeNativeMethods.IsWindow(_hWndWwG))
            {
                return;
            }

            _content.RequestClosing += OnRequestClosing;

            if (_wordWindow != null)
            {
                try
                {
                    _wordWindow.Application.WindowSelectionChange += OnWindowSelectionChange;
                }
                catch (COMException)
                {
                    // silently ignore if the application window no longer exists
                }
            }

            if (_content?.UserControl != null)
            {
                _content.UserControl.SizeChanged += OnContentSizeChanged;
            }

            _subclassedWwG.SizeChanged += OnDocumentWindowSizeChanged;
            _subclassedWwG.WindowPosChanging += OnDocumentWindowPosChanging;
            _subclassedWwG.MouseMove += OnDocumentWindowMouseMove;
            _subclassedWwG.MouseWheel += OnDocumentWindowMouseWheel;
            _subclassedWwG.MouseLeave += OnDocumentWindowMouseLeave;
            _subclassedWwG.KeyDown += OnDocumentWindowKeyDown;
            _subclassedWwG.KeyUp += OnDocumentWindowKeyUp;
            _subclassedWwG.Close += OnDocumentWindowClose;
            _subclassedWwG.ShowWindow += OnDocumentWindowShowWindow;
            _subclassedWwG.KillFocus += OnDocumentWindowKillFocus;
            _subclassedWwG.Paint += OnDocumentWindowPaint;

            _subclassedWwB.RichEditShowScrollBar += OnDocumentWindowZoomChanged;

            _subclassedWordWindow.ParentNotify += OnWordWindowParentNotify;

            _subclassedRibbonNetUIHWND.MouseMove += OnRibbonMouseMove;
        }

        private void Subclass()
        {
            _subclassedWwG.AssignHandle();
            _subclassedWwB.AssignHandle();
            _subclassedWordWindow.AssignHandle();
            _subclassedRibbonNetUIHWND.AssignHandle();
        }

        private void UnwireEventHandlers()
        {
            _content.RequestClosing -= OnRequestClosing;

            if ((_wordWindow != null) && SafeNativeMethods.IsWindow(_hWndWwB))
            {
                try
                {
                    _wordWindow.Application.WindowSelectionChange -= OnWindowSelectionChange;
                }
                catch (COMException)
                {
                    // silently ignore if the application window no longer exists
                }
            }

            if (_content?.UserControl != null)
            {
                _content.UserControl.SizeChanged -= OnContentSizeChanged;
            }

            _subclassedWwG.SizeChanged -= OnDocumentWindowSizeChanged;
            _subclassedWwG.WindowPosChanging -= OnDocumentWindowPosChanging;
            _subclassedWwG.MouseMove -= OnDocumentWindowMouseMove;
            _subclassedWwG.MouseWheel -= OnDocumentWindowMouseWheel;
            _subclassedWwG.MouseLeave -= OnDocumentWindowMouseLeave;
            _subclassedWwG.KeyDown -= OnDocumentWindowKeyDown;
            _subclassedWwG.KeyUp -= OnDocumentWindowKeyUp;
            _subclassedWwG.Close -= OnDocumentWindowClose;
            _subclassedWwG.ShowWindow -= OnDocumentWindowShowWindow;
            _subclassedWwG.KillFocus -= OnDocumentWindowKillFocus;
            _subclassedWwG.Paint -= OnDocumentWindowPaint;

            _subclassedWwB.RichEditShowScrollBar -= OnDocumentWindowZoomChanged;

            _subclassedWordWindow.ParentNotify -= OnWordWindowParentNotify;
            _subclassedRibbonNetUIHWND.MouseMove -= OnRibbonMouseMove;

            _updateTimer?.Change(Timeout.Infinite, Timeout.Infinite);
        }

        private void UnSubclass()
        {
            _subclassedWwG.ReleaseHandle();
            _subclassedWwB.ReleaseHandle();
            _subclassedWordWindow.ReleaseHandle();
            _subclassedRibbonNetUIHWND.ReleaseHandle();
        }
    }
}
