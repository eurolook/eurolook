using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;

namespace Eurolook.OfficeNotificationBar
{
    [SuppressMessage("StyleCop", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Names refer to native types.")]
    public class WindowFinder : WindowFinderBase
    {
        private readonly List<IntPtr> _windows = new List<IntPtr>();
        private readonly Process _process;

        private Func<NativeWindowWrapper, bool> _filterCondition;
        private bool _stopAfterFirstMatch;

        public WindowFinder()
        {
            _process = Process.GetCurrentProcess();
        }

        public WindowFinder(Process process)
        {
            _process = process;
        }

        public IntPtr FirstOrDefault(Func<NativeWindowWrapper, bool> filterCondition)
        {
            _stopAfterFirstMatch = true;
            _filterCondition = filterCondition;
            return EnumerateProcessWindowHandles().FirstOrDefault();
        }

        public IntPtr WaitForFirstOrDefault(
            Func<NativeWindowWrapper, bool> filterCondition,
            TimeSpan delayBetweenChecks,
            TimeSpan maximumProcessorTime,
            TimeSpan timeout)
        {
            var totalProcessorTimeAtBegin = _process.TotalProcessorTime;
            var startTime = DateTime.UtcNow;

            // wait for the window to be created
            while (true)
            {
                var hWnd = FirstOrDefault(filterCondition);
                if (hWnd != IntPtr.Zero)
                {
                    return hWnd;
                }

                if ((_process.TotalProcessorTime - totalProcessorTimeAtBegin > maximumProcessorTime)
                    || (DateTime.UtcNow - startTime > timeout))
                {
                    break;
                }

                Thread.Sleep(delayBetweenChecks);
            }

            return IntPtr.Zero;
        }

        public IEnumerable<IntPtr> Where(Func<NativeWindowWrapper, bool> filterCondition)
        {
            _stopAfterFirstMatch = false;
            _filterCondition = filterCondition;
            return EnumerateProcessWindowHandles();
        }

        private IEnumerable<IntPtr> EnumerateProcessWindowHandles()
        {
            _windows.Clear();

            var etd = new SafeNativeMethods.EnumThreadDelegate(EnumThreadDelegate);
            foreach (ProcessThread thread in _process.Threads)
            {
                SafeNativeMethods.EnumThreadWindows(thread.Id, etd, IntPtr.Zero);
            }

            return _windows;
        }

        private bool EnumThreadDelegate(IntPtr hWnd, IntPtr lParam)
        {
            if (!SafeNativeMethods.IsWindowVisible(hWnd) && FindVisibleOnly)
            {
                return true;
            }

            var window = new NativeWindowWrapper(hWnd);

            if (_filterCondition(window))
            {
                _windows.Add(hWnd);
                return !_stopAfterFirstMatch;
            }

            return true;
        }
    }
}
