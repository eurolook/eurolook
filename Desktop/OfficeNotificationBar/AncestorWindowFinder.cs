using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Eurolook.OfficeNotificationBar
{
    [SuppressMessage("StyleCop", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Names refer to native types.")]
    public class AncestorWindowFinder : WindowFinderBase
    {
        private readonly IntPtr _hWnd;

        public AncestorWindowFinder(IntPtr hWnd)
        {
            _hWnd = hWnd;
        }

        public IntPtr FirstOrDefault(Func<NativeWindowWrapper, bool> filterCondition)
        {
            return GetWindows().FirstOrDefault(filterCondition)?.Handle ?? IntPtr.Zero;
        }

        public IEnumerable<IntPtr> Where(Func<NativeWindowWrapper, bool> filterCondition)
        {
            return GetWindows().Where(filterCondition).Select(w => w.Handle);
        }

        private IEnumerable<NativeWindowWrapper> GetWindows()
        {
            var hWndParent = _hWnd;
            while ((hWndParent = SafeNativeMethods.GetParent(hWndParent)) != IntPtr.Zero)
            {
                var nativeWindow = new NativeWindowWrapper(hWndParent);
                yield return nativeWindow;
            }
        }
    }
}
