using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using Point = System.Drawing.Point;
using Rectangle = System.Drawing.Rectangle;
using Size = System.Drawing.Size;
using Window = Microsoft.Office.Interop.Word.Window;

namespace Eurolook.OfficeNotificationBar
{
    /// <summary>
    /// A class importing "safe" native methods as defined by
    /// <a href="http://msdn.microsoft.com/en-us/library/btadwd4w%28vs.100%29.aspx">Naming Convention for Unmanaged Code Methods</a>.
    /// </summary>
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "This class imports an external API")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "This class imports an external API")]
    [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "This class imports an external API")]
    [SuppressMessage("ReSharper", "NotAccessedField.Global", Justification = "This class imports an external API")]
    [SuppressMessage("ReSharper", "FieldCanBeMadeReadOnly.Global", Justification = "This class imports an external API")]
    [SuppressMessage("ReSharper", "IdentifierTypo", Justification = "This class imports an external API")]
    [SuppressMessage("Sonar", "S2346:Flags enumerations zero-value members should be named \"None\"", Justification = "This class imports an external API")]
    [SuppressMessage("Sonar", "S101:Types should be named in camel case", Justification = "This class imports an external API")]
    [SuppressMessage("Sonar", "S2342:Enumeration types should comply with a naming convention", Justification = "This class imports an external API")]
    internal static class SafeNativeMethods
    {
        public const int MM_ANISOTROPIC = 8;
        public const int COLOR_WINDOW = 5;
        public const int GCL_HBRBACKGROUND = -10;

        public delegate bool EnumThreadDelegate(IntPtr hWnd, IntPtr lParam);

        public delegate bool EnumWindowsProc(IntPtr hWnd, ref IntPtr lParam);

        public enum DeviceCap
        {
            /// <summary>Device driver version</summary>
            DRIVERVERSION = 0,

            /// <summary>Device classification</summary>
            TECHNOLOGY = 2,

            /// <summary>Horizontal size in millimeters</summary>
            HORZSIZE = 4,

            /// <summary>Vertical size in millimeters</summary>
            VERTSIZE = 6,

            /// <summary>Horizontal width in pixels</summary>
            HORZRES = 8,

            /// <summary>Vertical height in pixels</summary>
            VERTRES = 10,

            /// <summary>Number of bits per pixel</summary>
            BITSPIXEL = 12,

            /// <summary>Number of planes</summary>
            PLANES = 14,

            /// <summary>Number of brushes the device has</summary>
            NUMBRUSHES = 16,

            /// <summary>Number of pens the device has</summary>
            NUMPENS = 18,

            /// <summary>Number of markers the device has</summary>
            NUMMARKERS = 20,

            /// <summary>Number of fonts the device has</summary>
            NUMFONTS = 22,

            /// <summary>Number of colors the device supports</summary>
            NUMCOLORS = 24,

            /// <summary>Size required for device descriptor</summary>
            PDEVICESIZE = 26,

            /// <summary>Curve capabilities</summary>
            CURVECAPS = 28,

            /// <summary>Line capabilities</summary>
            LINECAPS = 30,

            /// <summary>Polygonal capabilities</summary>
            POLYGONALCAPS = 32,

            /// <summary>Text capabilities</summary>
            TEXTCAPS = 34,

            /// <summary>Clipping capabilities</summary>
            CLIPCAPS = 36,

            /// <summary>Bitblt capabilities</summary>
            RASTERCAPS = 38,

            /// <summary>Length of the X leg</summary>
            ASPECTX = 40,

            /// <summary>Length of the Y leg</summary>
            ASPECTY = 42,

            /// <summary>Length of the hypotenuse</summary>
            ASPECTXY = 44,

            /// <summary>Shading and Blending caps</summary>
            SHADEBLENDCAPS = 45,

            /// <summary>Logical pixels inch in X</summary>
            LOGPIXELSX = 88,

            /// <summary>Logical pixels inch in Y</summary>
            LOGPIXELSY = 90,

            /// <summary>Number of entries in physical palette</summary>
            SIZEPALETTE = 104,

            /// <summary>Number of reserved entries in palette</summary>
            NUMRESERVED = 106,

            /// <summary>Actual color resolution</summary>
            COLORRES = 108,

            /// <summary>Physical Width in device units</summary>
            PHYSICALWIDTH = 110,

            /// <summary>Physical Height in device units</summary>
            PHYSICALHEIGHT = 111,

            /// <summary>Physical Printable Area x margin</summary>
            PHYSICALOFFSETX = 112,

            /// <summary>Physical Printable Area y margin</summary>
            PHYSICALOFFSETY = 113,

            /// <summary>Scaling factor x</summary>
            SCALINGFACTORX = 114,

            /// <summary>Scaling factor y</summary>
            SCALINGFACTORY = 115,

            /// <summary>Current vertical refresh rate of the display device (for displays only) in Hz</summary>
            VREFRESH = 116,

            /// <summary>Horizontal width of entire desktop in pixels</summary>
            DESKTOPVERTRES = 117,

            /// <summary>Vertical height of entire desktop in pixels</summary>
            DESKTOPHORZRES = 118,

            /// <summary>Preferred blt alignment</summary>
            BLTALIGNMENT = 119,
        }

        public enum GWL
        {
            GWL_ID = -12,
            GWL_STYLE = -16,
            GWL_EXSTYLE = -20,
        }

        [Flags]
        public enum MouseMessageKeyState
        {
            MK_LBUTTON = 0x0001,
            MK_RBUTTON = 0x0002,
            MK_SHIFT = 0x0004,
            MK_CONTROL = 0x0008,
            MK_MBUTTON = 0x0010,
            MK_XBUTTON1 = 0x0020,
            MK_XBUTTON2 = 0x0040,
        }

        [Flags]
        public enum SendMessageTimeoutOption : uint
        {
            SMTO_NORMAL = 0x0,
            SMTO_BLOCK = 0x1,
            SMTO_ABORTIFHUNG = 0x2,
            SMTO_NOTIMEOUTIFNOTHUNG = 0x8,
            SMTO_ERRORONEXIT = 0x20,
        }

        [Flags]
        public enum SetWindowPosOption
        {
            SWP_NOSIZE = 0x0001,
            SWP_NOMOVE = 0x0002,
            SWP_NOZORDER = 0x0004,
            SWP_NOREDRAW = 0x0008,
            SWP_NOACTIVATE = 0x0010,
            SWP_FRAMECHANGED = 0x0020,      /* The frame changed: send WM_NCCALCSIZE */
            SWP_SHOWWINDOW = 0x0040,
            SWP_HIDEWINDOW = 0x0080,
            SWP_NOCOPYBITS = 0x0100,
            SWP_NOOWNERZORDER = 0x0200,     /* Don't do owner Z ordering */
            SWP_NOSENDCHANGING = 0x0400,    /* Don't send WM_WINDOWPOSCHANGING */
            SWP_NOCLIENTSIZE = 0x0800,      /* SetWindowPos undocumented flag */
            SWP_NOCLIENTMOVE = 0x1000,      /* SetWindowPos undocumented flag */
            SWP_DEFERERASE = 0x2000,
            SWP_ASYNCWINDOWPOS = 0x4000,
            SWP_STATECHANGED = 0x8000,      /* SetWindowPos undocumented flag */
        }

        [Flags]
        public enum SetWindowPosZOrder
        {
            HWND_TOP = 0,
            HWND_BOTTOM = 1,
            HWND_NOTOPMOST = -2,
            HWND_TOPMOST = -1,
        }

        public enum ShowWindowStyle : uint
        {
            /// <summary>Hides the window and activates another window.</summary>
            /// <remarks>See SW_HIDE</remarks>
            Hide = 0,

            /// <summary>
            /// Activates and displays a window. If the window is minimized or maximized, the system restores it to its original size
            /// and position. An application should specify this flag when displaying the window for the first time.
            /// </summary>
            /// <remarks>See SW_SHOWNORMAL</remarks>
            ShowNormal = 1,

            /// <summary>Activates the window and displays it as a minimized window.</summary>
            /// <remarks>See SW_SHOWMINIMIZED</remarks>
            ShowMinimized = 2,

            /// <summary>Activates the window and displays it as a maximized window.</summary>
            /// <remarks>See SW_SHOWMAXIMIZED</remarks>
            ShowMaximized = 3,

            /// <summary>Maximizes the specified window.</summary>
            /// <remarks>See SW_MAXIMIZE</remarks>
            Maximize = 3,

            /// <summary>
            /// Displays a window in its most recent size and position. This value is similar to "ShowNormal", except the
            /// window is not actived.
            /// </summary>
            /// <remarks>See SW_SHOWNOACTIVATE</remarks>
            ShowNormalNoActivate = 4,

            /// <summary> Activates the window and displays it in its current size and position. </summary>
            /// <remarks>See SW_SHOW</remarks>
            Show = 5,

            /// <summary> Minimizes the specified window and activates the next top-level window in the Z order. </summary>
            /// <remarks>See SW_MINIMIZE</remarks>
            Minimize = 6,

            /// <summary>
            /// Displays the window as a minimized window. This value is similar to "ShowMinimized", except the window is not activated.
            /// </summary>
            /// <remarks>See SW_SHOWMINNOACTIVE</remarks>
            ShowMinNoActivate = 7,

            /// <summary>
            /// Displays the window in its current size and position. This value is similar to "Show", except the window is not activated.
            /// </summary>
            /// <remarks>See SW_SHOWNA</remarks>
            ShowNoActivate = 8,

            /// <summary>
            /// Activates and displays the window. If the window is minimized or maximized, the system restores it to its
            /// original size and position. An application should specify this flag when restoring a minimized window.
            /// </summary>
            /// <remarks>See SW_RESTORE</remarks>
            Restore = 9,

            /// <summary>
            /// Sets the show state based on the SW_ value specified in the STARTUPINFO structure passed to the CreateProcess function
            /// by the program that started the application.
            /// </summary>
            /// <remarks>See SW_SHOWDEFAULT</remarks>
            ShowDefault = 10,

            /// <summary>
            /// Windows 2000/XP: Minimizes a window, even if the thread that owns the window is hung. This flag should only be used when
            /// minimizing windows from a different thread.
            /// </summary>
            /// <remarks>See SW_FORCEMINIMIZE</remarks>
            ForceMinimized = 11,
        }

        public enum WindowsMessages : uint
        {
            WM_ACTIVATE = 0x6,
            WM_ACTIVATEAPP = 0x1C,
            WM_AFXFIRST = 0x360,
            WM_AFXLAST = 0x37F,
            WM_APP = 0x8000,
            WM_ASKCBFORMATNAME = 0x30C,
            WM_CANCELJOURNAL = 0x4B,
            WM_CANCELMODE = 0x1F,
            WM_CAPTURECHANGED = 0x215,
            WM_CHANGECBCHAIN = 0x30D,
            WM_CHAR = 0x102,
            WM_CHARTOITEM = 0x2F,
            WM_CHILDACTIVATE = 0x22,
            WM_CLEAR = 0x303,
            WM_CLOSE = 0x10,
            WM_COMMAND = 0x111,
            WM_COMPACTING = 0x41,
            WM_COMPAREITEM = 0x39,
            WM_CONTEXTMENU = 0x7B,
            WM_COPY = 0x301,
            WM_COPYDATA = 0x4A,
            WM_CREATE = 0x1,
            WM_CTLCOLORBTN = 0x135,
            WM_CTLCOLORDLG = 0x136,
            WM_CTLCOLOREDIT = 0x133,
            WM_CTLCOLORLISTBOX = 0x134,
            WM_CTLCOLORMSGBOX = 0x132,
            WM_CTLCOLORSCROLLBAR = 0x137,
            WM_CTLCOLORSTATIC = 0x138,
            WM_CUT = 0x300,
            WM_DEADCHAR = 0x103,
            WM_DELETEITEM = 0x2D,
            WM_DESTROY = 0x2,
            WM_DESTROYCLIPBOARD = 0x307,
            WM_DEVICECHANGE = 0x219,
            WM_DEVMODECHANGE = 0x1B,
            WM_DISPLAYCHANGE = 0x7E,
            WM_DRAWCLIPBOARD = 0x308,
            WM_DRAWITEM = 0x2B,
            WM_DROPFILES = 0x233,
            WM_ENABLE = 0xA,
            WM_ENDSESSION = 0x16,
            WM_ENTERIDLE = 0x121,
            WM_ENTERMENULOOP = 0x211,
            WM_ENTERSIZEMOVE = 0x231,
            WM_ERASEBKGND = 0x14,
            WM_EXITMENULOOP = 0x212,
            WM_EXITSIZEMOVE = 0x232,
            WM_FONTCHANGE = 0x1D,
            WM_GETDLGCODE = 0x87,
            WM_GETFONT = 0x31,
            WM_GETHOTKEY = 0x33,
            WM_GETICON = 0x7F,
            WM_GETMINMAXINFO = 0x24,
            WM_GETOBJECT = 0x3D,
            WM_GETSYSMENU = 0x313,
            WM_GETTEXT = 0xD,
            WM_GETTEXTLENGTH = 0xE,
            WM_HANDHELDFIRST = 0x358,
            WM_HANDHELDLAST = 0x35F,
            WM_HELP = 0x53,
            WM_HOTKEY = 0x312,
            WM_HSCROLL = 0x114,
            WM_HSCROLLCLIPBOARD = 0x30E,
            WM_ICONERASEBKGND = 0x27,
            WM_IME_CHAR = 0x286,
            WM_IME_COMPOSITION = 0x10F,
            WM_IME_COMPOSITIONFULL = 0x284,
            WM_IME_CONTROL = 0x283,
            WM_IME_ENDCOMPOSITION = 0x10E,
            WM_IME_KEYDOWN = 0x290,
            WM_IME_KEYLAST = 0x10F,
            WM_IME_KEYUP = 0x291,
            WM_IME_NOTIFY = 0x282,
            WM_IME_REQUEST = 0x288,
            WM_IME_SELECT = 0x285,
            WM_IME_SETCONTEXT = 0x281,
            WM_IME_STARTCOMPOSITION = 0x10D,
            WM_INITDIALOG = 0x110,
            WM_INITMENU = 0x116,
            WM_INITMENUPOPUP = 0x117,
            WM_INPUT = 0x00FF,
            WM_INPUTLANGCHANGE = 0x51,
            WM_INPUTLANGCHANGEREQUEST = 0x50,
            WM_KEYDOWN = 0x100,
            WM_KEYFIRST = 0x100,
            WM_KEYLAST = 0x108,
            WM_KEYUP = 0x101,
            WM_KILLFOCUS = 0x8,
            WM_LBUTTONDBLCLK = 0x203,
            WM_LBUTTONDOWN = 0x201,
            WM_LBUTTONUP = 0x202,
            WM_MBUTTONDBLCLK = 0x209,
            WM_MBUTTONDOWN = 0x207,
            WM_MBUTTONUP = 0x208,
            WM_MDIACTIVATE = 0x222,
            WM_MDICASCADE = 0x227,
            WM_MDICREATE = 0x220,
            WM_MDIDESTROY = 0x221,
            WM_MDIGETACTIVE = 0x229,
            WM_MDIICONARRANGE = 0x228,
            WM_MDIMAXIMIZE = 0x225,
            WM_MDINEXT = 0x224,
            WM_MDIREFRESHMENU = 0x234,
            WM_MDIRESTORE = 0x223,
            WM_MDISETMENU = 0x230,
            WM_MDITILE = 0x226,
            WM_MEASUREITEM = 0x2C,
            WM_MENUCHAR = 0x120,
            WM_MENUCOMMAND = 0x126,
            WM_MENUDRAG = 0x123,
            WM_MENUGETOBJECT = 0x124,
            WM_MENURBUTTONUP = 0x122,
            WM_MENUSELECT = 0x11F,
            WM_MOUSEACTIVATE = 0x21,
            WM_MOUSEFIRST = 0x200,
            WM_MOUSEHOVER = 0x2A1,
            WM_MOUSELAST = 0x20A,
            WM_MOUSELEAVE = 0x2A3,
            WM_MOUSEMOVE = 0x200,
            WM_MOUSEWHEEL = 0x20A,
            WM_MOUSEHWHEEL = 0x20E,
            WM_MOVE = 0x3,
            WM_MOVING = 0x216,
            WM_NCACTIVATE = 0x86,
            WM_NCCALCSIZE = 0x83,
            WM_NCCREATE = 0x81,
            WM_NCDESTROY = 0x82,
            WM_NCHITTEST = 0x84,
            WM_NCLBUTTONDBLCLK = 0xA3,
            WM_NCLBUTTONDOWN = 0xA1,
            WM_NCLBUTTONUP = 0xA2,
            WM_NCMBUTTONDBLCLK = 0xA9,
            WM_NCMBUTTONDOWN = 0xA7,
            WM_NCMBUTTONUP = 0xA8,
            WM_NCMOUSEHOVER = 0x2A0,
            WM_NCMOUSELEAVE = 0x2A2,
            WM_NCMOUSEMOVE = 0xA0,
            WM_NCPAINT = 0x85,
            WM_NCRBUTTONDBLCLK = 0xA6,
            WM_NCRBUTTONDOWN = 0xA4,
            WM_NCRBUTTONUP = 0xA5,
            WM_NEXTDLGCTL = 0x28,
            WM_NEXTMENU = 0x213,
            WM_NOTIFY = 0x4E,
            WM_NOTIFYFORMAT = 0x55,
            WM_NULL = 0x0,
            WM_PAINT = 0xF,
            WM_PAINTCLIPBOARD = 0x309,
            WM_PAINTICON = 0x26,
            WM_PALETTECHANGED = 0x311,
            WM_PALETTEISCHANGING = 0x310,
            WM_PARENTNOTIFY = 0x210,
            WM_PASTE = 0x302,
            WM_PENWINFIRST = 0x380,
            WM_PENWINLAST = 0x38F,
            WM_POWER = 0x48,
            WM_PRINT = 0x317,
            WM_PRINTCLIENT = 0x318,
            WM_QUERYDRAGICON = 0x37,
            WM_QUERYENDSESSION = 0x11,
            WM_QUERYNEWPALETTE = 0x30F,
            WM_QUERYOPEN = 0x13,
            WM_QUERYUISTATE = 0x129,
            WM_QUEUESYNC = 0x23,
            WM_QUIT = 0x12,
            WM_RBUTTONDBLCLK = 0x206,
            WM_RBUTTONDOWN = 0x204,
            WM_RBUTTONUP = 0x205,
            WM_RENDERALLFORMATS = 0x306,
            WM_RENDERFORMAT = 0x305,
            WM_SETCURSOR = 0x20,
            WM_SETFOCUS = 0x7,
            WM_SETFONT = 0x30,
            WM_SETHOTKEY = 0x32,
            WM_SETICON = 0x80,
            WM_SETREDRAW = 0xB,
            WM_SETTEXT = 0xC,
            WM_SETTINGCHANGE = 0x1A,
            WM_SHOWWINDOW = 0x18,
            WM_SIZE = 0x5,
            WM_SIZECLIPBOARD = 0x30B,
            WM_SIZING = 0x214,
            WM_SPOOLERSTATUS = 0x2A,
            WM_STYLECHANGED = 0x7D,
            WM_STYLECHANGING = 0x7C,
            WM_SYNCPAINT = 0x88,
            WM_SYSCHAR = 0x106,
            WM_SYSCOLORCHANGE = 0x15,
            WM_SYSCOMMAND = 0x112,
            WM_SYSDEADCHAR = 0x107,
            WM_SYSKEYDOWN = 0x104,
            WM_SYSKEYUP = 0x105,
            WM_SYSTIMER = 0x118, // undocumented, see http://support.microsoft.com/?id=108938
            WM_TCARD = 0x52,
            WM_TIMECHANGE = 0x1E,
            WM_TIMER = 0x113,
            WM_UNDO = 0x304,
            WM_UNINITMENUPOPUP = 0x125,
            WM_USER = 0x400,
            WM_USERCHANGED = 0x54,
            WM_VKEYTOITEM = 0x2E,
            WM_VSCROLL = 0x115,
            WM_VSCROLLCLIPBOARD = 0x30A,
            WM_WINDOWPOSCHANGED = 0x47,
            WM_WINDOWPOSCHANGING = 0x46,
            WM_WININICHANGE = 0x1A,
            WM_XBUTTONDBLCLK = 0x20D,
            WM_XBUTTONDOWN = 0x20B,
            WM_XBUTTONUP = 0x20C,

            EM_SHOWSCROLLBAR = WM_USER + 96,
        }

        [Flags]
        public enum WindowStyles : uint
        {
            // Window Styles
            WS_OVERLAPPED = 0,
            WS_POPUP = 0x80000000,
            WS_CHILD = 0x40000000,
            WS_MINIMIZE = 0x20000000,
            WS_VISIBLE = 0x10000000,
            WS_DISABLED = 0x8000000,
            WS_CLIPSIBLINGS = 0x4000000,
            WS_CLIPCHILDREN = 0x2000000,
            WS_MAXIMIZE = 0x1000000,
            WS_CAPTION = WS_BORDER | WS_DLGFRAME,
            WS_BORDER = 0x800000,
            WS_DLGFRAME = 0x400000,
            WS_VSCROLL = 0x200000,
            WS_HSCROLL = 0x100000,
            WS_SYSMENU = 0x80000,
            WS_THICKFRAME = 0x40000,
            WS_GROUP = 0x20000,
            WS_TABSTOP = 0x10000,
            WS_MINIMIZEBOX = 0x20000,
            WS_MAXIMIZEBOX = 0x10000,
            WS_TILED = WS_OVERLAPPED,
            WS_ICONIC = WS_MINIMIZE,
            WS_SIZEBOX = WS_THICKFRAME,
            WS_TILEDWINDOW = WS_OVERLAPPEDWINDOW,
            WS_OVERLAPPEDWINDOW = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX,
            WS_POPUPWINDOW = WS_POPUP | WS_BORDER | WS_SYSMENU,
            WS_CHILDWINDOW = WS_CHILD,

            // Extended Window Styles
            WS_EX_DLGMODALFRAME = 0x0001,
            WS_EX_NOPARENTNOTIFY = 0x0004,
            WS_EX_TOPMOST = 0x0008,
            WS_EX_ACCEPTFILES = 0x0010,
            WS_EX_TRANSPARENT = 0x0020,
            WS_EX_MDICHILD = 0x0040,
            WS_EX_TOOLWINDOW = 0x0080,
            WS_EX_WINDOWEDGE = 0x0100,
            WS_EX_CLIENTEDGE = 0x0200,
            WS_EX_CONTEXTHELP = 0x0400,
            WS_EX_RIGHT = 0x1000,
            WS_EX_LEFT = 0x0000,
            WS_EX_RTLREADING = 0x2000,
            WS_EX_LTRREADING = 0x0000,
            WS_EX_LEFTSCROLLBAR = 0x4000,
            WS_EX_RIGHTSCROLLBAR = 0x0000,
            WS_EX_CONTROLPARENT = 0x10000,
            WS_EX_STATICEDGE = 0x20000,
            WS_EX_APPWINDOW = 0x40000,
            WS_EX_OVERLAPPEDWINDOW = WS_EX_WINDOWEDGE | WS_EX_CLIENTEDGE,
            WS_EX_PALETTEWINDOW = WS_EX_WINDOWEDGE | WS_EX_TOOLWINDOW | WS_EX_TOPMOST,
            WS_EX_LAYERED = 0x00080000,
            WS_EX_NOINHERITLAYOUT = 0x00100000, // Disable inheritance of mirroring by children
            WS_EX_LAYOUTRTL = 0x00400000, // Right to left mirroring
            WS_EX_COMPOSITED = 0x02000000,
            WS_EX_NOACTIVATE = 0x08000000,
        }

        public delegate int Win32SubClassProc(
            IntPtr hWnd,
            IntPtr msg,
            IntPtr wParam,
            IntPtr lParam,
            IntPtr uIdSubclass,
            IntPtr dwRefData);

        [DllImport("ComCtl32.dll", CharSet = CharSet.Auto)]
        public static extern int SetWindowSubclass(
            IntPtr hWnd,
            Win32SubClassProc newProc,
            IntPtr uIdSubclass,
            IntPtr dwRefData);

        [DllImport("ComCtl32.dll", CharSet = CharSet.Auto)]
        public static extern int RemoveWindowSubclass(IntPtr hWnd, Win32SubClassProc newProc, IntPtr uIdSubclass);

        [DllImport("ComCtl32.dll", CharSet = CharSet.Auto)]
        public static extern int DefSubclassProc(IntPtr hWnd, IntPtr msg, IntPtr wParam, IntPtr lParam);

        // Gets the IDispatch pointer of an object that supports IAccessible, which allows us to get to the native OM.
        [DllImport("Oleacc.dll")]
        public static extern int AccessibleObjectFromWindow(IntPtr hWnd, uint dwObjectID, byte[] riid, out Window ptr);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool EnumChildWindows(IntPtr hwndParent, EnumWindowsProc lpEnumFunc, ref IntPtr lParam);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool EnumWindows(EnumWindowsProc lpEnumFunc, IntPtr lParam);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool EnumThreadWindows(int dwThreadId, EnumThreadDelegate lpfn, IntPtr lParam);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int GetClassName(
            [In] IntPtr hWnd,
            [Out] [MarshalAs(UnmanagedType.LPWStr)]
            StringBuilder lpClassName,
            int nMaxCount);

        public static string GetClassName(IntPtr hWnd)
        {
            var sb = new StringBuilder(256);
            GetClassName(hWnd, sb, sb.Capacity);
            return sb.ToString();
        }

        public static IntPtr SetClassLong(IntPtr hWnd, int nIndex, IntPtr dwNewLong)
        {
            // Check for x64
            return IntPtr.Size > 4
                ? SetClassLongPtr64(hWnd, nIndex, dwNewLong)
                : new IntPtr(SetClassLongPtr32(hWnd, nIndex, unchecked((uint)dwNewLong.ToInt32())));
        }

        [DllImport("user32.dll", EntryPoint = "SetClassLong")]
        public static extern uint SetClassLongPtr32(IntPtr hWnd, int nIndex, uint dwNewLong);

        [SuppressMessage("Microsoft.Interoperability", "CA1400:PInvokeEntryPointsShouldExist", Justification = "Reviewed")]
        [DllImport("user32.dll", EntryPoint = "SetClassLongPtr")]
        public static extern IntPtr SetClassLongPtr64(IntPtr hWnd, int nIndex, IntPtr dwNewLong);

        /* Version for a message which returns a string, such as WM_GETTEXT. */
        [DllImport("user32.dll", EntryPoint = "SendMessageTimeout", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern IntPtr SendMessageTimeoutText(
            IntPtr hWnd,
            WindowsMessages uMsg,
            IntPtr wParam,
            StringBuilder lParam,
            SendMessageTimeoutOption fuOption,
            uint uTimeout,
            out int lpdwResult);

        /* Version for a message which returns an int, such as WM_GETTEXTLENGTH. */
        [DllImport("user32.dll", EntryPoint = "SendMessageTimeout", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern IntPtr SendMessageTimeout(
            IntPtr hwnd,
            WindowsMessages uMsg,
            IntPtr wParam,
            IntPtr lParam,
            SendMessageTimeoutOption fuOption,
            uint uTimeout,
            out int lpdwResult);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int GetWindowText(
            [In] IntPtr hWnd,
            [Out] [MarshalAs(UnmanagedType.LPWStr)] StringBuilder lpString,
            int nMaxCount);

        public static string GetWindowText(IntPtr hWnd)
        {
            int length = GetWindowTextLength(hWnd);
            var sb = new StringBuilder(length + 1);
            GetWindowText(hWnd, sb, sb.Capacity);
            return sb.ToString();
        }

        /// <summary>
        /// Gets the text of the specified window's title bar (if it has one).
        /// </summary>
        /// <param name="hWnd">A handle to the window containing the text.</param>
        /// <returns>The text of the title bar.</returns>
        /// <remarks>
        /// The window text is retrieved from the response to a WM_GETTEXT message via SendMessageTimeout. This is to avoid
        /// a possible application hang caused by a call to GetWindowText. GetWindowText hangs if the window is not
        /// responding, as documented in MSDN at https://msdn.microsoft.com/en-us/library/windows/desktop/ms633520.aspx and
        /// explained by Raymond Chen in more detail: [The secret life of GetWindowText]
        /// (https://blogs.msdn.microsoft.com/oldnewthing/20030821-00/?p=42833/).
        /// </remarks>
        public static string GetWindowTextSafe(IntPtr hWnd)
        {
            const SendMessageTimeoutOption timeoutFlags = SendMessageTimeoutOption.SMTO_ABORTIFHUNG
                                                          | SendMessageTimeoutOption.SMTO_BLOCK
                                                          | SendMessageTimeoutOption.SMTO_NOTIMEOUTIFNOTHUNG
                                                          | SendMessageTimeoutOption.SMTO_ERRORONEXIT;
            const uint timeoutMs = 20;

            if (SendMessageTimeout(
                    hWnd,
                    WindowsMessages.WM_GETTEXTLENGTH,
                    IntPtr.Zero,
                    IntPtr.Zero,
                    timeoutFlags,
                    timeoutMs,
                    out int length) == IntPtr.Zero)
            {
                // couldn't retrieve text length
                return null;
            }

            // increment length to include the terminating null character
            length++;

            var buf = new StringBuilder(length);
            if (SendMessageTimeoutText(
                    hWnd,
                    WindowsMessages.WM_GETTEXT,
                    new IntPtr(length),
                    buf,
                    timeoutFlags,
                    timeoutMs,
                    out length) == IntPtr.Zero)
            {
                // window did not respond to WM_TEXT message
                return null;
            }

            return buf.ToString();
        }

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int GetWindowTextLength(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        [DllImport("user32.dll")]
        public static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        [DllImport("user32.dll")]
        public static extern IntPtr GetParent(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern IntPtr GetActiveWindow();

        [DllImport("user32.dll", EntryPoint = "GetForegroundWindow")]
        public static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(
            [In] [MarshalAs(UnmanagedType.LPWStr)] string lpClassName,
            [In] [MarshalAs(UnmanagedType.LPWStr)] string lpWindowName);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindowEx(
            [In] IntPtr hWndParent,
            [In] IntPtr hWndChildAfter,
            [In] [MarshalAs(UnmanagedType.LPWStr)] string lpszClass,
            [In] [MarshalAs(UnmanagedType.LPWStr)] string lpszWindow);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);

        [SuppressMessage("Sonar", "S100:Methods and properties should be named in camel case", Justification = "This name refers to a native type.")]
        public static RECT GetWindowRECT(IntPtr hWnd)
        {
            GetWindowRect(hWnd, out var rect);
            return rect;
        }

        public static Rectangle GetWindowRectangle(IntPtr hWnd)
        {
            return GetWindowRECT(hWnd);
        }

        public static Rect GetWindowRect(IntPtr hWnd)
        {
            return GetWindowRECT(hWnd);
        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetClientRect(IntPtr hWnd, out RECT lpRect);

        public static RECT GetClientRect(IntPtr hWnd)
        {
            GetClientRect(hWnd, out var rect);
            return rect;
        }

        public static Rectangle GetClientRectangle(IntPtr hWnd)
        {
            return GetClientRect(hWnd);
        }

        [DllImport("user32.dll")]
        public static extern bool SetWindowPos(
            IntPtr hWnd,
            IntPtr hWndInsertAfter,
            int x,
            int y,
            int cx,
            int cy,
            SetWindowPosOption uOption);

        [DllImport("user32.dll", EntryPoint = "MoveWindow")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool MoveWindow(
            [In] IntPtr hWnd,
            int x,
            int y,
            int nWidth,
            int nHeight,
            [MarshalAs(UnmanagedType.Bool)] bool bRepaint);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool ShowWindow(IntPtr hWnd, ShowWindowStyle nCmdShow);

        [DllImport("user32.dll")]
        public static extern bool ScreenToClient(IntPtr hWnd, ref POINT lpPoint);

        public static Rectangle ScreenToClient(IntPtr hWnd, Rectangle rect)
        {
            var pt = new POINT(rect.Left, rect.Top);
            ScreenToClient(hWnd, ref pt);

            return new Rectangle(pt.X, pt.Y, rect.Width, rect.Height);
        }

        [DllImport("user32.dll")]
        public static extern bool ClientToScreen(IntPtr hWnd, ref POINT lpPoint);

        public static Rectangle ClientToScreen(IntPtr hWnd, Rectangle rect)
        {
            var pt = new POINT(rect.Left, rect.Top);
            ClientToScreen(hWnd, ref pt);

            return new Rectangle(pt.X, pt.Y, rect.Width, rect.Height);
        }

        [DllImport("user32.dll")]
        public static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll")]
        public static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsWindowVisible(IntPtr hWnd);

        public static bool HasWindowVisibleStyle(IntPtr hWnd)
        {
            int style = GetWindowLong(hWnd, (int)GWL.GWL_STYLE);
            int visible = style & (int)WindowStyles.WS_VISIBLE;
            return visible != 0;
        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsWindow(IntPtr hWnd);

        [DllImport("UxTheme")]
        public static extern bool IsThemeActive();

        [DllImport("user32.dll")]
        public static extern bool InvalidateRect(IntPtr hWnd, IntPtr lpRect, bool bErase);

        [DllImport("user32.dll", EntryPoint = "SetFocus")]
        public static extern IntPtr SetFocus([In] IntPtr hWnd);

        [DllImport("gdi32.dll")]
        public static extern int GetDeviceCaps(IntPtr hdc, DeviceCap nIndex);

        [DllImport("gdi32.dll")]
        public static extern bool SetViewportExtEx(IntPtr hdc, int nXExtent, int nYExtent, IntPtr lpSize);

        [DllImport("gdi32.dll")]
        public static extern bool SetWindowExtEx(IntPtr hdc, int nXExtent, int nYExtent, IntPtr lpSize);

        [DllImport("gdi32.dll")]
        public static extern int SetMapMode(IntPtr hdc, int fnMapMode);

        [DllImport("user32.dll")]
        public static extern IntPtr GetSysColorBrush(int nIndex);

        [DllImport("user32.dll")]
        public static extern int FillRect(IntPtr hDC, [In] ref RECT lprc, IntPtr hbr);

        [StructLayout(LayoutKind.Sequential)]
        public struct WindowPos
        {
            public IntPtr hwnd;
            public IntPtr hwndInsertAfter;
            public int x;
            public int y;
            public int cx;
            public int cy;
            public SetWindowPosOption Option;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            public int X;
            public int Y;

            public POINT(int x, int y)
            {
                X = x;
                Y = y;
            }

            public static implicit operator Point(POINT p)
            {
                return new Point(p.X, p.Y);
            }

            public static implicit operator POINT(Point p)
            {
                return new POINT(p.X, p.Y);
            }
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public RECT(int left, int top, int right, int bottom)
            {
                Left = left;
                Top = top;
                Right = right;
                Bottom = bottom;
            }

            public int Left { get; }

            public int Top { get; }

            public int Right { get; }

            public int Bottom { get; }

            public int Height
            {
                get { return Bottom - Top; }
            }

            public int Width
            {
                get { return Right - Left; }
            }

            public Size Size
            {
                get { return new Size(Width, Height); }
            }

            public Point Location
            {
                get { return new Point(Left, Top); }
            }

            public static implicit operator Rectangle(RECT rect)
            {
                return rect.ToRectangle();
            }

            public static implicit operator RECT(Rectangle rect)
            {
                return FromRectangle(rect);
            }

            public static implicit operator Rect(RECT rect)
            {
                return rect.ToRect();
            }

            public static RECT FromRectangle(Rectangle rectangle)
            {
                return new RECT(rectangle.Left, rectangle.Top, rectangle.Right, rectangle.Bottom);
            }

            public Rectangle ToRectangle()
            {
                return Rectangle.FromLTRB(Left, Top, Right, Bottom);
            }

            public Rect ToRect()
            {
                return new Rect(Left, Top, Width, Height);
            }
        }
    }
}
