﻿using System;
using System.Runtime.InteropServices;
using Eurolook.Common.Log;
using Eurolook.OfficeNotificationBar.NativeEventArgs;

namespace Eurolook.OfficeNotificationBar
{
    /// <summary>
    /// A class replacing the obsolete System.Windows.Forms.NativeWindow class for sub-classing.
    /// </summary>
    /// <remarks>
    /// The implementation of <see cref="System.Windows.Forms.NativeWindow"/> has a problem that may cause the
    /// application to crash. When calling <see cref="System.Windows.Forms.NativeWindow.ReleaseHandle"/>, the call
    /// replaced the window proc with User32!DefWindowProc[this], which will likely crash the app.
    /// See https://github.com/dotnet/winforms/issues/2380 and
    /// https://devblogs.microsoft.com/oldnewthing/20031111-00/?p=41883 for details.
    /// </remarks>
    internal class NativeWindowEx : IDisposable, ICanLog
    {
        private readonly IntPtr _uIdSubclass;

        private readonly object _lock = new object();

        private readonly SafeNativeMethods.Win32SubClassProc _winProcInner;
        private readonly IntPtr _handle;

        public NativeWindowEx(IntPtr uIdSubclass, IntPtr hWnd)
        {
            _uIdSubclass = uIdSubclass;
            _winProcInner = SubClassProc;
            _handle = hWnd;
        }

        ~NativeWindowEx()
        {
            Dispose(false);
        }

        public event EventHandler<ActivateAppEventArgs> ActivateApp;

        public event EventHandler<EventArgs> Activate;

        public event EventHandler<EventArgs> Close;

        public event EventHandler<ParentNotifyEventArgs> ParentNotify;

        public event EventHandler<SizeChangingEventArgs> SizeChanging;

        public event EventHandler<SizeChangedEventArgs> SizeChanged;

        public event EventHandler<WindowPosEventArgs> WindowPosChanging;

        public event EventHandler<WindowPosEventArgs> WindowPosChanged;

        public event EventHandler<VisibilityEventArgs> ShowWindow;

        public event EventHandler<MouseEventArgs> LeftButtonDown;

        public event EventHandler<MouseEventArgs> LeftButtonUp;

        public event EventHandler<MouseEventArgs> MouseMove;

        public event EventHandler<MouseWheelEventArgs> MouseWheel;

        public event EventHandler<EventArgs> MouseLeave;

        public event EventHandler<KeyEventArgs> KeyDown;

        public event EventHandler<KeyEventArgs> KeyUp;

        public event EventHandler<EventArgs> Char;

        public event EventHandler<EventArgs> KillFocus;

        public event EventHandler<EventArgs> Focus;

        public event EventHandler<EventArgs> Paint;

        public event EventHandler<WindowMessageEventArgs> RichEditShowScrollBar;

        public bool? IsSubclassed { get; private set; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void AssignHandle()
        {
            if (!IsSubclassed.GetValueOrDefault())
            {
                lock (_lock)
                {
                    if (!IsSubclassed.GetValueOrDefault() && SafeNativeMethods.IsWindow(_handle))
                    {
                        int result = SafeNativeMethods.SetWindowSubclass(
                            _handle,
                            _winProcInner,
                            _uIdSubclass,
                            IntPtr.Zero);

                        if (result != 1)
                        {
                            this.LogError("SetWindowSubClass failed");
                            return;
                        }

                        IsSubclassed = true;
                    }
                }
            }
        }

        public void ReleaseHandle()
        {
            if (IsSubclassed == true)
            {
                lock (_lock)
                {
                    if (IsSubclassed == true && SafeNativeMethods.IsWindow(_handle))
                    {
                        int result = SafeNativeMethods.RemoveWindowSubclass(_handle, _winProcInner, _uIdSubclass);
                        if (result != 1)
                        {
                            this.LogError("RemoveWindowSubclass failed");
                        }

                        IsSubclassed = false;
                    }
                }
            }
        }

        private int SubClassProc(
            IntPtr hWnd,
            IntPtr msg,
            IntPtr wParam,
            IntPtr lParam,
            IntPtr uIdSubclass,
            IntPtr dwRefData)
        {
            switch ((SafeNativeMethods.WindowsMessages)msg)
            {
                case SafeNativeMethods.WindowsMessages.WM_ACTIVATEAPP:
                {
                    OnActivateApp(wParam.ToInt32() != 0);
                    break;
                }

                case SafeNativeMethods.WindowsMessages.WM_ACTIVATE:
                {
                    OnActivate();
                    break;
                }

                case SafeNativeMethods.WindowsMessages.WM_CLOSE:
                {
                    OnClose();
                    break;
                }

                case SafeNativeMethods.WindowsMessages.WM_PARENTNOTIFY:
                {
                    OnParentNotify(new ParentNotifyEventArgs(wParam, lParam));
                    break;
                }

                case SafeNativeMethods.WindowsMessages.WM_SHOWWINDOW:
                {
                    OnShowWindow(wParam.ToInt32() != 0);
                    break;
                }

                case SafeNativeMethods.WindowsMessages.WM_SIZE:
                {
                    OnSizeChanged(new SizeChangedEventArgs(wParam, lParam));
                    break;
                }

                case SafeNativeMethods.WindowsMessages.WM_SIZING:
                {
                    OnSizeChanging(new SizeChangingEventArgs(wParam, lParam));
                    break;
                }

                case SafeNativeMethods.WindowsMessages.WM_WINDOWPOSCHANGING:
                {
                    OnWindowPosChanging(ref lParam);
                    break;
                }

                case SafeNativeMethods.WindowsMessages.WM_WINDOWPOSCHANGED:
                {
                    OnWindowPosChanged(ref lParam);
                    break;
                }

                case SafeNativeMethods.WindowsMessages.WM_LBUTTONDOWN:
                {
                    var eventArgs = new MouseEventArgs(wParam, lParam);
                    OnLeftButtonDown(eventArgs);
                    break;
                }

                case SafeNativeMethods.WindowsMessages.WM_LBUTTONUP:
                {
                    var eventArgs = new MouseEventArgs(wParam, lParam);
                    OnLeftButtonUp(eventArgs);
                    break;
                }

                case SafeNativeMethods.WindowsMessages.WM_MOUSEMOVE:
                {
                    var eventArgs = new MouseEventArgs(wParam, lParam);
                    OnMouseMove(eventArgs);
                    break;
                }

                case SafeNativeMethods.WindowsMessages.WM_MOUSEWHEEL:
                {
                    var eventArgs = new MouseWheelEventArgs(wParam, lParam);
                    OnMouseWheel(eventArgs);
                    break;
                }

                case SafeNativeMethods.WindowsMessages.WM_MOUSELEAVE:
                {
                    OnMouseLeave();
                    break;
                }

                case SafeNativeMethods.WindowsMessages.WM_KEYDOWN:
                {
                    var eventArgs = new KeyEventArgs(wParam, lParam);
                    OnKeyDown(eventArgs);
                    break;
                }

                case SafeNativeMethods.WindowsMessages.WM_KEYUP:
                {
                    var eventArgs = new KeyEventArgs(wParam, lParam);
                    OnKeyUp(eventArgs);
                    break;
                }

                case SafeNativeMethods.WindowsMessages.WM_CHAR:
                {
                    OnChar(EventArgs.Empty);
                    break;
                }

                case SafeNativeMethods.WindowsMessages.WM_KILLFOCUS:
                {
                    OnKillFocus();
                    break;
                }

                case SafeNativeMethods.WindowsMessages.WM_SETFOCUS:
                {
                    OnFocus();
                    break;
                }

                case SafeNativeMethods.WindowsMessages.WM_PAINT:
                {
                    OnPaint();
                    break;
                }

                case SafeNativeMethods.WindowsMessages.EM_SHOWSCROLLBAR:
                {
                    OnRichEditShowScrollBar(new WindowMessageEventArgs(wParam, lParam));
                    break;
                }

                case SafeNativeMethods.WindowsMessages.WM_NCDESTROY:
                {
                    ReleaseHandle();
                    return SafeNativeMethods.DefSubclassProc(hWnd, msg, wParam, lParam);
                }
            }

            return SafeNativeMethods.DefSubclassProc(hWnd, msg, wParam, lParam);
        }

        // ReSharper disable once UnusedParameter.Local
        private void Dispose(bool disposing)
        {
            ReleaseHandle();
        }

        private void OnActivateApp(bool isActivated)
        {
            ActivateApp?.Invoke(this, new ActivateAppEventArgs(isActivated));
        }

        private void OnActivate()
        {
            Activate?.Invoke(this, EventArgs.Empty);
        }

        private void OnClose()
        {
            Close?.Invoke(this, EventArgs.Empty);
        }

        private void OnParentNotify(ParentNotifyEventArgs parentNotifyEventArgs)
        {
            ParentNotify?.Invoke(this, parentNotifyEventArgs);
        }

        private void OnWindowPosChanged(ref IntPtr lParam)
        {
            var handler = WindowPosChanged;
            if (handler != null)
            {
                var windowPos = (SafeNativeMethods.WindowPos)Marshal.PtrToStructure(
                    lParam,
                    typeof(SafeNativeMethods.WindowPos));
                var e = new WindowPosEventArgs(windowPos);
                handler(this, e);
                Marshal.StructureToPtr(e.WindowPos, lParam, true);
            }
        }

        private void OnWindowPosChanging(ref IntPtr lParam)
        {
            var handler = WindowPosChanging;
            if (handler != null)
            {
                var windowPos = (SafeNativeMethods.WindowPos)Marshal.PtrToStructure(
                    lParam,
                    typeof(SafeNativeMethods.WindowPos));
                var e = new WindowPosEventArgs(windowPos);
                handler(this, e);
                Marshal.StructureToPtr(e.WindowPos, lParam, true);
            }
        }

        private void OnSizeChanging(SizeChangingEventArgs e)
        {
            SizeChanging?.Invoke(this, e);
        }

        private void OnSizeChanged(SizeChangedEventArgs e)
        {
            SizeChanged?.Invoke(this, e);
        }

        private void OnShowWindow(bool bShown)
        {
            ShowWindow?.Invoke(this, new VisibilityEventArgs(bShown));
        }

        private void OnLeftButtonDown(MouseEventArgs e)
        {
            LeftButtonDown?.Invoke(this, e);
        }

        private void OnLeftButtonUp(MouseEventArgs e)
        {
            LeftButtonUp?.Invoke(this, e);
        }

        private void OnMouseMove(MouseEventArgs e)
        {
            MouseMove?.Invoke(this, e);
        }

        private void OnMouseWheel(MouseWheelEventArgs e)
        {
            MouseWheel?.Invoke(this, e);
        }

        private void OnMouseLeave()
        {
            MouseLeave?.Invoke(this, EventArgs.Empty);
        }

        private void OnKeyDown(KeyEventArgs e)
        {
            KeyDown?.Invoke(this, e);
        }

        private void OnKeyUp(KeyEventArgs e)
        {
            KeyUp?.Invoke(this, e);
        }

        private void OnChar(EventArgs e)
        {
            Char?.Invoke(this, e);
        }

        private void OnKillFocus()
        {
            KillFocus?.Invoke(this, EventArgs.Empty);
        }

        private void OnFocus()
        {
            Focus?.Invoke(this, EventArgs.Empty);
        }

        private void OnPaint()
        {
            Paint?.Invoke(this, EventArgs.Empty);
        }

        private void OnRichEditShowScrollBar(WindowMessageEventArgs e)
        {
            RichEditShowScrollBar?.Invoke(this, e);
        }
    }
}
