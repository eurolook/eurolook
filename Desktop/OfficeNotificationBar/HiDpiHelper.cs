using System;
using System.Drawing;

namespace Eurolook.OfficeNotificationBar
{
    internal static class HiDpiHelper
    {
        public static double GetDpiScalingFactor()
        {
            int dpiX = GetDpiX();
            return dpiX / 96.0;
        }

        public static int GetDpiX()
        {
            var g = Graphics.FromHwnd(IntPtr.Zero);
            var desktop = g.GetHdc();
            int dpiX = SafeNativeMethods.GetDeviceCaps(desktop, SafeNativeMethods.DeviceCap.LOGPIXELSX);
            return dpiX;
        }
    }
}
