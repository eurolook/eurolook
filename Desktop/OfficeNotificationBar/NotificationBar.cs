using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using Eurolook.OfficeNotificationBar.NativeEventArgs;
using Window = Microsoft.Office.Interop.Word.Window;

namespace Eurolook.OfficeNotificationBar
{
    [SuppressMessage("StyleCop", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Names refer to native types.")]
    public class NotificationBar : IDisposable
    {
        private static readonly object StaticLock = new object();

        private readonly IntPtr _hWndWordWindow;
        private readonly IntPtr _hWndWwF;

        private SubclassedWindow _subclassedWwF;
        private HwndSource _hwndSource;
        private INotificationBarContent _content;

        public NotificationBar(Window wdWindow)
        {
            _hWndWordWindow = wdWindow.GetWordWindowHandle();
            _hWndWwF = WordWindowFinder.GetWordWwFHandle(_hWndWordWindow);
        }

        ~NotificationBar()
        {
            Dispose(false);
        }

        public int Height
        {
            get
            {
                double scalingFactor = HiDpiHelper.GetDpiScalingFactor();
                return Content?.UserControl != null ? (int)(Content.UserControl.ActualHeight * scalingFactor) : 0;
            }
        }

        private INotificationBarContent Content
        {
            get { return _content; }

            set
            {
                if (_content != null)
                {
                    _content.RequestClosing -= ContentOnRequestClosing;
                }

                _content = value;
                if (_content != null)
                {
                    _content.RequestClosing += ContentOnRequestClosing;
                }
            }
        }

        public void Show(INotificationBarContent content)
        {
            if (content == null)
            {
                throw new ArgumentNullException(nameof(content));
            }

            lock (StaticLock)
            {
                if (Content == content)
                {
                    return;
                }

                Hide();

                Content = content;

                var parameters = new HwndSourceParameters
                {
                    ParentWindow = _hWndWordWindow,
                    RestoreFocusMode = RestoreFocusMode.Auto,
                    AcquireHwndFocusInMenuMode = true,
                    TreatAsInputRoot = true,
                    PositionX = -1000,
                    PositionY = -1000,
                    Height = 100,
                    Width = 100,
                    WindowName = "Eurolook Notification",
                    WindowStyle = (int)(SafeNativeMethods.WindowStyles.WS_CHILD | SafeNativeMethods.WindowStyles.WS_VISIBLE),
                };

                _hwndSource = new HwndSource(parameters)
                {
                    RootVisual = Content.UserControl,
                    SizeToContent = SizeToContent.WidthAndHeight,
                };
                if (_hwndSource.CompositionTarget != null)
                {
                    _hwndSource.CompositionTarget.BackgroundColor = Colors.White;
                }

                _subclassedWwF = new SubclassedWindow();
                _subclassedWwF.AssignHandle(_hWndWwF);
                _subclassedWwF.WindowPosChanging += DocumentWindowPosChanging;

                // trigger initial resizing / positioning
                var rectWwF = SafeNativeMethods.GetWindowRECT(_hWndWwF);

                var topLeft = new SafeNativeMethods.POINT(rectWwF.Left, rectWwF.Top);
                SafeNativeMethods.ScreenToClient(_hWndWordWindow, ref topLeft);
                SafeNativeMethods.SetWindowPos(
                    _hWndWwF,
                    IntPtr.Zero,
                    topLeft.X,
                    topLeft.Y,
                    rectWwF.Width,
                    rectWwF.Height,
                    SafeNativeMethods.SetWindowPosOption.SWP_FRAMECHANGED
                    | SafeNativeMethods.SetWindowPosOption.SWP_NOACTIVATE
                    | SafeNativeMethods.SetWindowPosOption.SWP_NOZORDER);
            }
        }

        public void Hide()
        {
            lock (StaticLock)
            {
                if (_subclassedWwF != null)
                {
                    _subclassedWwF.ReleaseHandle();
                    _subclassedWwF = null;

                    if (_hWndWwF != IntPtr.Zero && SafeNativeMethods.IsWindow(_hWndWwF))
                    {
                        // restore document window
                        var rect = SafeNativeMethods.GetWindowRECT(_hWndWwF);

                        var topLeft = new SafeNativeMethods.POINT(rect.Left, rect.Top);
                        SafeNativeMethods.ScreenToClient(_hWndWordWindow, ref topLeft);

                        SafeNativeMethods.SetWindowPos(
                            _hWndWwF,
                            IntPtr.Zero,
                            topLeft.X,
                            topLeft.Y - Height,
                            rect.Width,
                            rect.Height + Height,
                            SafeNativeMethods.SetWindowPosOption.SWP_FRAMECHANGED
                            | SafeNativeMethods.SetWindowPosOption.SWP_NOACTIVATE
                            | SafeNativeMethods.SetWindowPosOption.SWP_NOZORDER);
                        SafeNativeMethods.InvalidateRect(_hWndWordWindow, IntPtr.Zero, true);
                    }
                }

                if (_hwndSource != null)
                {
                    _hwndSource.Dispose();
                    _hwndSource = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        [SuppressMessage("ReSharper", "UnusedParameter.Local", Justification = "Method signature is according to IDisposable pattern.")]
        protected virtual void Dispose(bool disposing)
        {
            Hide();
        }

        private void ContentOnRequestClosing(object sender, EventArgs args)
        {
            Hide();
        }

        private void DocumentWindowPosChanging(object sender, WindowPosEventArgs e)
        {
            if (_hwndSource == null)
            {
                return;
            }

            var windowPos = e.WindowPos;
#if DEBUG
            string className = SafeNativeMethods.GetClassName(windowPos.hwnd);
            string windowText = SafeNativeMethods.GetWindowText(windowPos.hwnd);
#endif
            if ((e.WindowPos.Option & SafeNativeMethods.SetWindowPosOption.SWP_NOSIZE) == 0)
            {
                // resizing
                UpdateWidth((int)(windowPos.cx / HiDpiHelper.GetDpiScalingFactor()));
                windowPos.cy = windowPos.cy - Height;
#if DEBUG
                Trace.WriteLine(
                    "DocumentWindowPosChanging: "
                    + $"Resizing for {windowPos.hwnd.ToInt64():X8} - {className} {windowText} - {windowPos.Option}");
#endif
            }

            if ((e.WindowPos.Option & SafeNativeMethods.SetWindowPosOption.SWP_NOMOVE) == 0)
            {
                // moving
                UpdatePosition(windowPos.x, windowPos.y);
                windowPos.y = windowPos.y + Height;
#if DEBUG
                Trace.WriteLine(
                    "DocumentWindowPosChanging: "
                    + $"Moving {windowPos.hwnd.ToInt64():X8} - {className} {windowText} - {windowPos.Option}");
#endif
            }

            e.WindowPos = windowPos;
        }

        private void UpdatePosition(int newLeft, int newTop)
        {
            SafeNativeMethods.SetWindowPos(
                _hwndSource.Handle,
                IntPtr.Zero,
                newLeft,
                newTop,
                0,
                0,
                SafeNativeMethods.SetWindowPosOption.SWP_FRAMECHANGED
                | SafeNativeMethods.SetWindowPosOption.SWP_NOSIZE
                | SafeNativeMethods.SetWindowPosOption.SWP_NOACTIVATE
                | SafeNativeMethods.SetWindowPosOption.SWP_NOZORDER);
        }

        private void UpdateWidth(int newWidth)
        {
            Content.UserControl.Width = newWidth;
            SafeNativeMethods.SetWindowPos(
                _hwndSource.Handle,
                IntPtr.Zero,
                0,
                0,
                newWidth,
                Height,
                SafeNativeMethods.SetWindowPosOption.SWP_FRAMECHANGED
                | SafeNativeMethods.SetWindowPosOption.SWP_NOMOVE
                | SafeNativeMethods.SetWindowPosOption.SWP_NOACTIVATE
                | SafeNativeMethods.SetWindowPosOption.SWP_NOZORDER);
        }
    }
}
