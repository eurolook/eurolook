using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;

namespace Eurolook.OfficeNotificationBar.Extensions
{
    public static class WindowExtensions
    {
        public static bool IsNormalView(this Window window)
        {
            if (window?.View == null)
            {
                return false;
            }

            return window.View.Type == WdViewType.wdNormalView;
        }

        public static bool IsPrintView(this Window window)
        {
            if (window?.View == null)
            {
                return false;
            }

            return window.View.Type == WdViewType.wdPrintView;
        }

        public static bool IsWebView(this Window window)
        {
            if (window?.View == null)
            {
                return false;
            }

            return window.View.Type == WdViewType.wdWebView;
        }

        public static bool CanHaveTaskPane(this Window window)
        {
            // NOTE: if no document is open, window is null --> allow task pane
            if (window == null)
            {
                return true;
            }

            return window.IsNormalView() || window.IsPrintView() || window.IsWebView();
        }

        [CanBeNull]
        public static Document GetDocumentSafe(this Window window)
        {
            try
            {
                return window.Document;
            }
            catch
            {
                return null;
            }
        }
    }
}
