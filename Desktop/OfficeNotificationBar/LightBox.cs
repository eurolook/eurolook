using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using Eurolook.OfficeNotificationBar.NativeEventArgs;
using Microsoft.Office.Interop.Word;
using Application = Microsoft.Office.Interop.Word.Application;
using Rectangle = System.Drawing.Rectangle;
using Window = Microsoft.Office.Interop.Word.Window;

namespace Eurolook.OfficeNotificationBar
{
    public class LightBox : IDisposable
    {
        private readonly Application _application;
        private readonly ILightBoxContent _content;
        private readonly WindowExtents _windowExtents;
        private readonly HwndSource _hwndSource;

        private readonly IntPtr _hWndWordWindow;
        private readonly IntPtr _hWndWwF;

        private readonly SubclassedWindow _subclassedWordWindow;

        public LightBox(
            Application application,
            Window window,
            ILightBoxContent content,
            WindowExtents windowExtents)
        {
            if (content == null)
            {
                throw new ArgumentNullException(nameof(content));
            }

            _application = application;
            _content = content;
            _windowExtents = windowExtents;

            _application.DocumentBeforeClose += OnApplicationDocumentBeforeClose;

            _hWndWordWindow = window.GetWordWindowHandle();
            _hWndWwF = WordWindowFinder.GetWordWwFHandle(_hWndWordWindow);
            var hWndWwB = WordWindowFinder.GetWordWwBHandle(_hWndWwF);
            var hWndWwG = WordWindowFinder.GetWordWwGHandle(hWndWwB);

            var parameters = new HwndSourceParameters
            {
                ParentWindow = hWndWwG,
                RestoreFocusMode = RestoreFocusMode.Auto,
                AcquireHwndFocusInMenuMode = true,
                TreatAsInputRoot = true,
                PositionX = -1000,
                PositionY = -1000,
                Height = 100,
                Width = 100,
                WindowName = "Eurolook LightBox",
                WindowStyle = (int)(SafeNativeMethods.WindowStyles.WS_CHILD | SafeNativeMethods.WindowStyles.WS_VISIBLE),
            };

            _hwndSource = new HwndSource(parameters)
            {
                RootVisual = _content.UserControl,
                SizeToContent = SizeToContent.Manual,
            };
            if (_hwndSource.CompositionTarget != null)
            {
                _hwndSource.CompositionTarget.BackgroundColor = Colors.White;
            }

            _subclassedWordWindow = new SubclassedWindow();
            _subclassedWordWindow.AssignHandle(_hWndWordWindow);

            _subclassedWordWindow.Close += OnOpusAppClose;
            _subclassedWordWindow.ActivateApp += OnOpusAppActivateApp;
            _subclassedWordWindow.SizeChanging += OnOpusAppSizeChanging;
            _subclassedWordWindow.WindowPosChanging += OnOpusAppWindowPosChanging;
            _subclassedWordWindow.WindowPosChanged += OnOpusAppWindowPosChanged;

            _content.RequestClosing += OnContentRequestClosing;
        }

        ~LightBox()
        {
            Dispose(false);
        }

        public bool IsVisible
        {
            get { return SafeNativeMethods.IsWindowVisible(_hwndSource.Handle); }

            set
            {
                var flag = value ? SafeNativeMethods.ShowWindowStyle.Show : SafeNativeMethods.ShowWindowStyle.Hide;
                SafeNativeMethods.ShowWindow(_hwndSource.Handle, flag);
                SafeNativeMethods.InvalidateRect(_hWndWordWindow, IntPtr.Zero, true);
            }
        }

        public void Show()
        {
            SafeNativeMethods.ShowWindow(_hwndSource.Handle, SafeNativeMethods.ShowWindowStyle.Show);
            SafeNativeMethods.InvalidateRect(_hWndWordWindow, IntPtr.Zero, true);
        }

        public void ShowAndFadeIn()
        {
            _content.UserControl.Opacity = 0.0;

            Show();

            // fade in
            var dblaOpacityAnimation = new DoubleAnimation
            {
                From = 0.0,
                To = 1.0,
                Duration = new Duration(TimeSpan.FromSeconds(0.25)),
            };
            Storyboard.SetTarget(dblaOpacityAnimation, _content.UserControl);
            Storyboard.SetTargetProperty(dblaOpacityAnimation, new PropertyPath(UIElement.OpacityProperty));

            var strbStoryBoard = new Storyboard();
            strbStoryBoard.Children.Add(dblaOpacityAnimation);
            strbStoryBoard.Begin(_content.UserControl);
        }

        public void Close()
        {
            Dispose();
        }

        public void CloseAndFadeOut()
        {
            // fade out on closing
            var duration = new Duration(TimeSpan.FromSeconds(0.25));

            var dblaHeightAnimation = new DoubleAnimation
            {
                From = _content.UserControl.ActualHeight,
                To = 0,
                Duration = duration,
            };
            Storyboard.SetTarget(dblaHeightAnimation, _content.UserControl);
            Storyboard.SetTargetProperty(dblaHeightAnimation, new PropertyPath(FrameworkElement.HeightProperty));

            var dblaWidthAnimation = new DoubleAnimation
            {
                From = _content.UserControl.ActualWidth,
                To = 0,
                Duration = duration,
            };
            Storyboard.SetTarget(dblaWidthAnimation, _content.UserControl);
            Storyboard.SetTargetProperty(dblaWidthAnimation, new PropertyPath(FrameworkElement.WidthProperty));

            var dblaOpacityAnimation = new DoubleAnimation
            {
                From = _content.UserControl.Opacity,
                To = 0.0,
                Duration = duration,
            };
            Storyboard.SetTarget(dblaOpacityAnimation, _content.UserControl);
            Storyboard.SetTargetProperty(dblaOpacityAnimation, new PropertyPath(UIElement.OpacityProperty));

            var strbStoryBoard = new Storyboard();
            strbStoryBoard.Completed += FadeOutCompleted;
            strbStoryBoard.Children.Add(dblaOpacityAnimation);

            strbStoryBoard.Begin(_content.UserControl);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            _subclassedWordWindow.ReleaseHandle();

            _application.DocumentBeforeClose -= OnApplicationDocumentBeforeClose;

            SafeNativeMethods.SetForegroundWindow(_hWndWordWindow);

            IsVisible = false;

            if ((_hwndSource != null) && !_hwndSource.IsDisposed)
            {
                if (_hwndSource.Dispatcher.CheckAccess())
                {
                    _hwndSource.Dispose();
                }
                else
                {
                    var hWndSource = _hwndSource;
                    _hwndSource.Dispatcher.Invoke(hWndSource.Dispose, DispatcherPriority.Normal);
                }

                _hwndSource.Dispose();
            }
        }

        private void SetSplashPosition()
        {
            var rect = GetClientWindowCoordinates();
            SetWindowPos(rect);
        }

        private void SetWindowPos(Rectangle rect)
        {
            SetWindowPos(rect.Left, rect.Top, rect.Width, rect.Height);
        }

        private void SetWindowPos(int x, int y, int width, int height)
        {
            if (_hwndSource == null)
            {
                return;
            }

            // handle large fonts / dpi scaling
            if (_hwndSource.CompositionTarget != null)
            {
                double scaleX = _hwndSource.CompositionTarget.TransformToDevice.M11;
                double scaleY = _hwndSource.CompositionTarget.TransformToDevice.M22;

                x = (int)(x / scaleX);
                y = (int)(y / scaleY);
                width = (int)(width / scaleX);
                height = (int)(height / scaleY);
            }

            SafeNativeMethods.SetWindowPos(
                _hwndSource.Handle,
                IntPtr.Zero,
                x,
                y,
                width,
                height,
                SafeNativeMethods.SetWindowPosOption.SWP_NOZORDER);

            SafeNativeMethods.SetWindowPos(
                _hwndSource.Handle,
                (IntPtr)SafeNativeMethods.SetWindowPosZOrder.HWND_TOPMOST,
                0,
                0,
                0,
                0,
                SafeNativeMethods.SetWindowPosOption.SWP_NOMOVE | SafeNativeMethods.SetWindowPosOption.SWP_NOSIZE);
        }

        private void FadeOutCompleted(object sender, EventArgs e)
        {
            _hwndSource.Dispose();
        }

        private Rectangle GetClientWindowCoordinates()
        {
            // get document window
            var rectWwF = SafeNativeMethods.GetClientRectangle(_hWndWwF);
            var rectOpusApp = SafeNativeMethods.GetClientRectangle(_hWndWordWindow);
            ////rectOpusApp = SafeNativeMethods.ClientToScreen(_hWndWordWindow, rectOpusApp);

            // increase by size of task panes etc
            int left = _windowExtents.HasFlag(WindowExtents.DockLeft) ? rectOpusApp.Left : rectWwF.Left;
            int right = _windowExtents.HasFlag(WindowExtents.DockRight) ? rectOpusApp.Right : rectWwF.Right;

            int top = _windowExtents.HasFlag(WindowExtents.DockTop) ? rectOpusApp.Top : rectWwF.Top;
            int bottom = _windowExtents.HasFlag(WindowExtents.DockBottom) ? rectOpusApp.Bottom : rectWwF.Bottom;

            return new Rectangle(left, top, right - left, bottom - top);
        }

        private void OnApplicationDocumentBeforeClose(Document doc, ref bool cancel)
        {
            if (IsVisible && doc.ActiveWindow.GetWordWindowHandle() == _hWndWordWindow)
            {
                Close();
            }
        }

        private void OnOpusAppActivateApp(object sender, ActivateAppEventArgs e)
        {
            if (e.IsActivated)
            {
                ////Activate();
                ////Focus();
            }
        }

        private void OnOpusAppWindowPosChanging(object sender, WindowPosEventArgs e)
        {
            SetSplashPosition();
        }

        private void OnOpusAppWindowPosChanged(object sender, WindowPosEventArgs e)
        {
            SetSplashPosition();
        }

        private void OnOpusAppSizeChanging(object sender, SizeChangingEventArgs e)
        {
            SetSplashPosition();
        }

        private void OnOpusAppClose(object sender, EventArgs e)
        {
            Close();
        }

        private void OnContentRequestClosing(object sender, EventArgs eventArgs)
        {
            CloseAndFadeOut();
        }
    }
}
