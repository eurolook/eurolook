using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.CSharp.RuntimeBinder;
using Microsoft.Office.Interop.Word;

namespace Eurolook.OfficeNotificationBar
{
    [SuppressMessage("StyleCop", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Names refer to native types.")]
    [SuppressMessage("StyleCop", "SA1306:FieldNamesMustBeginWithLowerCaseLetter", Justification = "Names refer to native types.")]
    public static class WordWindowFinder
    {
        public static IntPtr GetWordWindowHandle(this Window wdWindow)
        {
            if (wdWindow == null)
            {
                return Process.GetCurrentProcess().MainWindowHandle;
            }

            if (TryGetWordWindowHandle(wdWindow, out var wordWindowHandle))
            {
                return wordWindowHandle;
            }

            if (TryGetWordWindowHandleLegacy(wdWindow, out wordWindowHandle))
            {
                return wordWindowHandle;
            }

            // fallback: return main window handle of the current process
            return Process.GetCurrentProcess().MainWindowHandle;
        }

        public static IntPtr GetWordWwFHandle(IntPtr hWndAncestor)
        {
            return new ChildWindowFinder(hWndAncestor).FirstOrDefault(w => w.ClassName == "_WwF");
        }

        public static IntPtr GetWordWwBHandle(IntPtr hWndAncestor)
        {
            return new ChildWindowFinder(hWndAncestor).FirstOrDefault(w => w.ClassName == "_WwB");
        }

        public static IntPtr GetWordWwGHandle(IntPtr hWndAncestor)
        {
            return new ChildWindowFinder(hWndAncestor).FirstOrDefault(w => w.ClassName == "_WwG");
        }

        public static IntPtr GetCommandBarDockHandle(IntPtr hWndWordWindow, DockPosition dockPosition)
        {
            string windowTitle;
            switch (dockPosition)
            {
                case DockPosition.Top:
                    windowTitle = "MsoDockTop";
                    break;
                case DockPosition.Right:
                    windowTitle = "MsoDockRight";
                    break;
                case DockPosition.Bottom:
                    windowTitle = "MsoDockBottom";
                    break;
                default:
                    windowTitle = "MsoDockLeft";
                    break;
            }

            return new ChildWindowFinder(hWndWordWindow).FirstOrDefault(
                w => (w.ClassName == "MsoCommandBarDock") && (w.Title == windowTitle));
        }

        private static bool TryGetWordWindowHandle(Window wdWindow, out IntPtr wordWindowHandle)
        {
            wordWindowHandle = IntPtr.Zero;

            // use dynamic and try whether the `Window.HWnd` property (introduced with Word 2013) is available
            dynamic window = wdWindow;
            try
            {
                wordWindowHandle = new IntPtr(window.HWnd);
            }
            catch (RuntimeBinderException)
            {
                // silent catch: Property Window.HWnd does not exist in Word 2010 or older
                return false;
            }

            return true;
        }

        private static bool TryGetWordWindowHandleLegacy(Window wdWindow, out IntPtr wordWindowHandle)
        {
            wordWindowHandle = IntPtr.Zero;
            try
            {
                // search for windows with matching caption
                var windowFinder = new WindowFinder();
                var windowHandles = windowFinder.Where(
                    w =>
                        w.ClassName == "OpusApp"
                        && w.Title.Contains(wdWindow.Caption)).ToList();

                // usual case: only one window with the searched caption
                if (windowHandles.Count == 1)
                {
                    {
                        wordWindowHandle = windowHandles.First();
                        return true;
                    }
                }

                // more than one window has the same caption, we need to get the correct one
                foreach (var hWndOpusApp in windowHandles)
                {
                    // Search the accessible child window (it has class name "_WwG")
                    // as described in http://msdn.microsoft.com/en-us/library/dd317978%28VS.85%29.aspx
                    var hWndWwG = GetWordWwGHandle(hWndOpusApp);
                    if (hWndWwG == IntPtr.Zero)
                    {
                        continue;
                    }

                    // We call AccessibleObjectFromWindow, passing the constant OBJID_NATIVEOM (defined in winuser.h)
                    // and IID_IDispatch - we want an IDispatch pointer into the native object model.
                    //
                    // ReSharper disable InconsistentNaming
#pragma warning disable SA1312 // Variable names must begin with lower-case letter
                    const uint OBJID_NATIVEOM = 0xFFFFFFF0;
                    var IID_IDispatch = new Guid("{00020400-0000-0000-C000-000000000046}");
                    // ReSharper restore InconsistentNaming
#pragma warning restore SA1312 // Variable names must begin with lower-case letter

                    int hResult = SafeNativeMethods.AccessibleObjectFromWindow(
                        hWndWwG,
                        OBJID_NATIVEOM,
                        IID_IDispatch.ToByteArray(),
                        out var ptr);
                    if (hResult >= 0 && ptr == wdWindow)
                    {
                        {
                            wordWindowHandle = hWndOpusApp;
                            return true;
                        }
                    }
                }
            }
            catch (COMException)
            {
                // this method implements a try-get pattern, therefore we ignore any COMExceptions
            }

            return false;
        }
    }
}
