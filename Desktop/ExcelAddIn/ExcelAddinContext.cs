using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.AddIn;
using Microsoft.Win32;

namespace Eurolook.ExcelAddIn
{
    internal class ExcelAddinContext : AddinContextBase, IExcelAddinContext
    {
        public ExcelAddinContext(IDisabledAddInsManager disabledAddInsManager = null)
            : base(disabledAddInsManager)
        {
        }

        public override IEnumerable<TimeSpan> LoadTimes()
        {
            // TODO XXX: Make sure this works across different versions of Office
            const string key = @"Software\Microsoft\Office\16.0\Excel\AddInLoadTimes";

            string eurolookOfficeAddInProgId = GetAddInShimProgId();

            using (var addInLoadTimesKey = Registry.CurrentUser.OpenSubKey(key))
            {
                var value = (byte[])addInLoadTimesKey?.GetValue(eurolookOfficeAddInProgId);
                if (value != null)
                {
                    int length = BitConverter.ToInt32(value, 0);
                    for (int i = 0; i < length; i++)
                    {
                        int loadTimeMilliseconds = BitConverter.ToInt32(value, (i + 1) * 4);
                        yield return TimeSpan.FromMilliseconds(loadTimeMilliseconds);
                    }
                }
            }
        }

        protected override string GetAddInProgId()
        {
            var progIdAttribute = (ProgIdAttribute)typeof(ThisExcelAddIn).GetCustomAttribute(typeof(ProgIdAttribute));
            string eurolookAddInProgId = progIdAttribute.Value;
            return eurolookAddInProgId;
        }
    }
}
