﻿using System;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.Data.Constants;
using Eurolook.Data.Models;
using Eurolook.Data.Xml;
using Microsoft.Office.Interop.Excel;

namespace Eurolook.ExcelAddIn
{
    public class WorkbookViewModel : IEurolookDocument
    {
        private readonly object _lock = new object();

        private readonly IAddinContext _addinContext;
        private readonly IDocumentModelRepository _documentModelRepository;
        private readonly Workbook _workbook;
        private DocumentModel _documentModel;

        public WorkbookViewModel(
            IAddinContext addinContext,
            IDocumentModelRepository documentModelRepository,
            Workbook workbook)
        {
            _addinContext = addinContext;
            _documentModelRepository = documentModelRepository;
            _workbook = workbook;
        }

        public DocumentModel DocumentModel
        {
            get => _documentModel ??=
                _documentModelRepository.GetDocumentModel(CommonDataConstants.StandardWordDocDocumentModel);
            set => _documentModel = value;
        }

        public DocumentModelLanguage DocumentModelLanguage => null;

        public Language Language { get; set; } = new Language("EN");

        public bool IsEurolookDocument { get; set; }

        public bool IsBasedOnCustomTemplate { get => false; }

        public bool IsActive { get; set; }

        public IOfficeDocumentWrapper OfficeDocumentWrapper => new ExcelWorkbookWrapper(_workbook);

        public IEurolookProperties GetEurolookProperties()
        {
            string xml = _workbook.CustomXMLParts.GetCustomXml(EurolookPropertiesCustomXml.RootName);
            return !string.IsNullOrEmpty(xml)
                ? new EurolookPropertiesCustomXml(xml)
                : new EurolookPropertiesCustomXml
                {
                    CreationLanguage = "EN",
                    CreationDate = DateTime.Now,
                    CreationVersion = _addinContext.ClientVersion.ToString(),
                    DocumentModelId = DocumentModel.Id,
                    DocumentModelName = DocumentModel.DisplayName,
                    DocumentDate = DateTime.Now,
                    CompatibilityMode = EurolookCompatibilityMode.Eurolook4X,
                };
        }

        public void SetEurolookProperties(Action<IEurolookProperties> modifyAction)
        {
            // This should be the only place where the embedded custom XML is modified.
            lock (_lock)
            {
                // read current version from document
                var eurolookProperties = GetEurolookProperties();

                // set new values to properties
                modifyAction(eurolookProperties);

                // update document
                _workbook.CustomXMLParts.ReplaceCustomXml(
                    EurolookPropertiesCustomXml.RootName,
                    eurolookProperties.ToXDocument());
            }
        }
    }
}
