﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Threading;
using Autofac;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.AddIn;
using Eurolook.AddIn.Common.ProfileInitializer;
using Eurolook.AddIn.Common.Ribbon;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.ExcelAddIn.Autofac;
using Application = Microsoft.Office.Interop.Excel.Application;

namespace Eurolook.ExcelAddIn
{
    public sealed class ThisExcelAddIn : AddInBase, ICanLog, IOfficeAddIn, IDisposable
    {
        private IContainer _container;
        private IRibbonCallbackHandler _ribbonCallbackHandler;
        private IRibbonService _ribbonService;
        private IAddinContext _addinContext;

        public ThisExcelAddIn(Application application)
        {
            Application = application;
        }

        ~ThisExcelAddIn()
        {
            Dispose(false);
        }

        public Application Application { get; }

        public IRibbonCallbackHandler RibbonCallbackHandler =>
            _ribbonCallbackHandler ??= _container?.Resolve<IRibbonCallbackHandler>();

        public IContainer SetupContainer()
        {
            _container = AutofacRegistration.SetupContainer(this);
            this.LogDebug("Dependency injection container set up.");
            return _container;
        }

        public IRibbonService RibbonService => _ribbonService ??= _container?.Resolve<IRibbonService>();

        private IAddinContext AddinContext => _addinContext ??= _container?.Resolve<IAddinContext>();

        public override void OnStartup(object application)
        {
            try
            {
                this.LogDebug("OnStartup started.");
                AsyncHelper.EnsureSynchronizationContext();

                if (application is not Application _)
                {
                    MessageBox.Show("Add-in must be loaded into Microsoft Excel.");
                    return;
                }
            }
            catch (Exception e)
            {
                this.LogError(e);
            }
        }

        public override void OnShutdown()
        {
            Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        [SuppressMessage(
            "ReSharper",
            "UnusedParameter.Local",
            Justification = "This method signature is according to the IDisposable pattern.")]
        private void Dispose(bool disposing)
        {
            try
            {
                this.LogDebug("Shutting down Eurolook");

                AddinContext?.ClearDisabledAddins();
            }
            catch (Exception e)
            {
                this.LogError(e);
            }
        }
    }
}
