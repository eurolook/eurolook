using Eurolook.AddIn.Common;
using Microsoft.Office.Interop.Excel;

namespace Eurolook.ExcelAddIn
{
    public interface IExcelApplicationContext : IApplicationContext<Application>
    {
    }
}
