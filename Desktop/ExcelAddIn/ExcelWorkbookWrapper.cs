﻿using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.Common.Log;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;

namespace Eurolook.ExcelAddIn
{
    public class ExcelWorkbookWrapper : IOfficeDocumentWrapper, ICanLog
    {
        private readonly Workbook _workbook;

        public ExcelWorkbookWrapper(Workbook workbook)
        {
            _workbook = workbook;
        }

        public string GetCustomXml(string rootName)
        {
            return _workbook.CustomXMLParts.GetCustomXml(rootName);
        }

        public CustomXMLParts CustomXMLParts => _workbook.CustomXMLParts;

        public string Name => _workbook.Name;

        public string FullName => _workbook.FullName;

        public string FileExtension => ".xlsx";

        public bool Saved
        {
            get => _workbook.Saved && !string.IsNullOrEmpty(_workbook.Path);
            set => _workbook.Saved = value;
        }

        public void Save()
        {
            _workbook.Save();
        }

        public void SaveAs(string fileName)
        {
            _workbook.SaveAs(fileName);
        }

        public void SaveCopyAs(string fileName)
        {
            _workbook.SaveCopyAs(fileName);
        }

        public dynamic Open(string fileName) => _workbook.Application.Workbooks.Open(fileName);

        public dynamic GetDocument() => _workbook;

        public dynamic BuiltInDocumentProperties => _workbook.BuiltinDocumentProperties;

        public MetaProperties ContentTypeProperties => _workbook.ContentTypeProperties;

        public string GetHttpContentType => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    }
}
