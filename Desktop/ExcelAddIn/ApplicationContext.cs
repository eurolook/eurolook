using System;
using Eurolook.AddIn.Common.Events;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Excel;

namespace Eurolook.ExcelAddIn
{
    public sealed class ApplicationContext : IExcelApplicationContext
    {
        public ApplicationContext([NotNull] Application application)
        {
            Application = application;
            InputDetector = new InputDetector();
        }

        ~ApplicationContext()
        {
            Dispose(false);
        }

        [NotNull]
        public Application Application { get; }

        public InputDetector InputDetector { get; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                InputDetector?.Dispose();
            }
        }
    }
}
