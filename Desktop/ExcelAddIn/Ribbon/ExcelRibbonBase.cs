﻿using System;
using System.Drawing;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Properties;
using Eurolook.AddIn.Common.Ribbon;
using Eurolook.ExcelAddIn.Extensions;

namespace Eurolook.ExcelAddIn.Ribbon
{
    public abstract class ExcelRibbonBase : RibbonBase
    {
        private readonly Lazy<IExcelApplicationContext> _applicationContext;
        private readonly Lazy<IWorkbookManager> _workbookManager;

        protected ExcelRibbonBase(
            Lazy<IAddinContext> addinContext,
            Lazy<IExcelApplicationContext> applicationContext,
            Lazy<IMessageService> messageService,
            Lazy<ISettingsService> settingsService,
            Lazy<IWorkbookManager> workbookManager,
            Lazy<IEmbeddedResourceManager> embeddedResourceManager)
            : base(addinContext, messageService, settingsService, embeddedResourceManager)
        {
            _applicationContext = applicationContext;
            _workbookManager = workbookManager;
        }

        protected IWorkbookManager WorkbookManager => _workbookManager.Value;

        protected IExcelApplicationContext ApplicationContext => _applicationContext.Value;

        protected override Image GetResourceImage(string resourceId)
        {
            return GetResourceImageRecursive(GetType(), resourceId);
        }

        protected bool CanEditWorkbook()
        {
            // ReadOnly mode should disable the button,
            // however it cannot do because Word does not revalidate the ribbon when read-only mode is exited.
            return !ApplicationContext.Application.IsProtectedView();
        }
    }
}
