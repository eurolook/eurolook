using System;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.ProfileInitializer;
using Eurolook.AddIn.Common.Ribbon;

namespace Eurolook.ExcelAddIn.Ribbon
{
    public class ExcelRibbonService : RibbonServiceBase<IExcelApplicationContext, IExcelAddinContext>, IExcelRibbonService
    {
        private readonly IExcelAddinContext _addinContext;

        public ExcelRibbonService(
            IExcelApplicationContext applicationContext,
            IExcelAddinContext addinContext,
            Lazy<IProfileInitializer> profileInitializer,
            Lazy<IUserDataRepository> userDataRepository)
            : base(applicationContext, addinContext, profileInitializer, userDataRepository)
        {
            _addinContext = addinContext;
        }

        public override Task<bool> TryLoadQuickAccessToolbarAsync()
        {
            return Task.FromResult(true);
        }

        public override void UnloadQuickAccessToolbar()
        {
        }
    }
}
