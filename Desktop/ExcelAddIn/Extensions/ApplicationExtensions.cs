using Microsoft.Office.Interop.Excel;

namespace Eurolook.ExcelAddIn.Extensions
{
    public static class ApplicationExtensions
    {
        public static bool IsProtectedView(this Application app)
        {
            return app.ActiveProtectedViewWindow != null;
        }
    }
}
