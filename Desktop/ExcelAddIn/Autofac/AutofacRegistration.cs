﻿using System.IO;
using System.Reflection;
using Autofac;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Autofac;
using Eurolook.AddIn.Common.Email;
using Eurolook.AddIn.Common.PerformanceLog;
using Eurolook.AddIn.Common.SystemConfiguration;
using Eurolook.AddIn.Common.UserConfiguration;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Common;
using Eurolook.Common.Extensions;
using Eurolook.Data;
using Eurolook.ExcelAddIn.Ribbon;

namespace Eurolook.ExcelAddIn.Autofac
{
    public static class AutofacRegistration
    {
        public static IContainer SetupContainer(ThisExcelAddIn thisExcelAddIn)
        {
            var builder = new ContainerBuilder();

            var pluginLoader = new PluginLoader(
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                AppSettings.Instance.Plugins.Excel);

            builder.SetEurolookPluginAssemblies(pluginLoader.PluginAssemblies);

            // Perform registrations and build the container.
            var applicationContext = new ApplicationContext(thisExcelAddIn.Application);
            builder.RegisterInstance(applicationContext).AsImplementedInterfaces().SingleInstance();

            builder.RegisterType<ExcelAddinContext>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterInstance(() => ConfigurationInfo.Instance).AsImplementedInterfaces();

            // register services
            builder.RegisterType<ExcelRibbonService>().AsImplementedInterfaces().SingleInstance();

            var moduleAssemblies = new[] { typeof(ThisExcelAddIn).Assembly, typeof(IEurolookCommonModule).Assembly };

            builder.RegisterAssemblyModules<IEurolookCommonModule>(moduleAssemblies);

            builder.RegisterType<SettingsService>().As<ISettingsService>().SingleInstance();
            builder.RegisterType<ExcelMessageService>().As<IMessageService>().SingleInstance();
            builder.RegisterType<WorkbookManager>().AsImplementedInterfaces().SingleInstance().AutoActivate();

            builder.RegisterType<WorkbookViewModel>().AsSelf();

            builder.RegisterType<DataSyncManager>().SingleInstance();
            builder.RegisterType<DataSyncScheduler>().SingleInstance();

            builder.RegisterType<ColorManager>().SingleInstance();
            builder.RegisterType<DeviceManager>().SingleInstance();

            builder.RegisterType<PerformanceLogInfoProvider>().AsImplementedInterfaces();
            builder.RegisterType<PerformanceLogService>().AsImplementedInterfaces();

            builder.RegisterType<SystemConfigurationService>().AsImplementedInterfaces();
            builder.RegisterType<EmailService>().AsImplementedInterfaces();

            builder.RegisterGeneric(typeof(ClassNameResolver<>)).As(typeof(IClassNameResolver<>));

            pluginLoader.RegisterPluginModules(builder);

            var container = builder.Build();

            AutofacXamlResolver.Container = container;

            return container;
        }
    }
}
