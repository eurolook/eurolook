namespace Eurolook.ExcelAddIn
{
    public interface IWorkbookManager
    {
        WorkbookViewModel GetActiveWorkbookViewModel();
    }
}
