using System;
using Workbook = Microsoft.Office.Interop.Excel.Workbook;

namespace Eurolook.ExcelAddIn
{
    public class WorkbookManager : IWorkbookManager
    {
        private readonly IExcelApplicationContext _applicationContext;
        private readonly Func<Workbook, WorkbookViewModel> _workbookViewModelFunc;

        public WorkbookManager(
            IExcelApplicationContext applicationContext,
            Func<Workbook, WorkbookViewModel> workbookViewModelFunc)
        {
            _applicationContext = applicationContext;
            _workbookViewModelFunc = workbookViewModelFunc;
        }

        public WorkbookViewModel GetActiveWorkbookViewModel()
        {
            var activeWorkBook = _applicationContext.Application.ActiveWorkbook;
            return activeWorkBook != null ? _workbookViewModelFunc(activeWorkBook) : null;
        }
    }
}
