if "%1" EQU "" (set BUILDCONFIGURATION=Debug) else (set BUILDCONFIGURATION=%1)
if "%2" EQU "" (set PLATFORM=x86) else (set PLATFORM=%2)
if "%3" EQU "" (set TARGETFRAMEWORK=net48) else (set TARGETFRAMEWORK=%3)

echo Registering %1 EurolookLink for current user
set mypath=%~d0%~p0
set linkexe=%mypath%bin\%BUILDCONFIGURATION%\%PLATFORM%\%TARGETFRAMEWORK%\EurolookLink.exe
reg add "HKEY_CURRENT_USER\SOFTWARE\Classes\el10" /ve /d "Eurolook 10" /f
reg add "HKEY_CURRENT_USER\SOFTWARE\Classes\el10" /v "URL Protocol" /d "el10" /f
reg add "HKEY_CURRENT_USER\SOFTWARE\Classes\el10\DefaultIcon" /ve /d "\"%linkexe%\"" /f
reg add "HKEY_CURRENT_USER\SOFTWARE\Classes\el10\shell\open\command" /ve /d "\"%linkexe%\" \"%%1\"" /f