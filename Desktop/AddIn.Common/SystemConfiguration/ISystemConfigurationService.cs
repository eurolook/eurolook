﻿using System.Threading.Tasks;
using Eurolook.AddIn.Common.SystemConfiguration.SensitivityLabels;
using Eurolook.Data.Models;

namespace Eurolook.AddIn.Common.SystemConfiguration
{
    public interface ISystemConfigurationService
    {
        Task<bool> IsTemplateStoreEnabledForUser(User user = null);

        Task<SensitivityLabelsConfiguration> GetSensitivityLabelsConfiguration();

        Task<WhatsNewConfiguration> GetWhatsNewConfiguration();

        Task<bool> IsSensitivityLabelCheckEnabledForUser();
        Task<LabelCheckConfiguration> GetSensitivityLabelCheckForUser();

        Task<string> GetFeedbackEmailAddress();
        Task<bool> IsCreationWizardEnabled();
    }
}
