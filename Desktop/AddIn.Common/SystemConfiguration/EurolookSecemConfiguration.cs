﻿using Eurolook.Data.Database;

namespace Eurolook.AddIn.Common.SystemConfiguration
{
    public class EurolookSecemConfiguration : ISystemConfigurationSetting
    {
        public string Key => "EurolookSECEM";

        public string ServiceBaseUrl { get; set; }

        public string ServiceEndpoint { get; set; }

        public bool IsEnabledForEveryone { get; set; }

        public bool IsEnabledForWord { get; set; }

        public bool IsEnabledForExcel { get; set; }

        public bool IsEnabledForPowerPoint { get; set; }
    }
}
