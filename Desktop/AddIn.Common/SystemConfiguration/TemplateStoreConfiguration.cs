using Eurolook.Data.Database;

namespace Eurolook.AddIn.Common.SystemConfiguration
{
    public class TemplateStoreConfiguration : ISystemConfigurationSetting
    {
        public string Key => "TemplateStore";

        public bool IsEnabled { get; set; }

        public string[] EnabledForUserGroups { get; set; } = {};
    }
}
