﻿using Eurolook.Data.Database;

namespace Eurolook.AddIn.Common.SystemConfiguration
{
    public class FeedbackConfiguration : ISystemConfigurationSetting
    {
        public string Key => "Feedback";

        public string EmailAddress { get; set; }
    }
}
