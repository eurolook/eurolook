using Eurolook.Data.Database;

namespace Eurolook.AddIn.Common.SystemConfiguration
{
    public class WhatsNewConfiguration : ISystemConfigurationSetting
    {
        public string Key => "WhatsNew";

        public bool IsVisible { get; set; }

        public bool HasCustomUrl { get; set; }

        public string CustomUrl { get; set; }
    }
}
