﻿namespace Eurolook.AddIn.Common.SystemConfiguration.SensitivityLabels
{
    public class AzureConfiguration
    {
        public string TenantId { get; set; }

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        public string CertThumb { get; set; }
    }
}
