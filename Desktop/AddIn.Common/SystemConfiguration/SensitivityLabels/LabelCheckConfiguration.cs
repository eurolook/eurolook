﻿namespace Eurolook.AddIn.Common.SystemConfiguration.SensitivityLabels
{
    public class LabelCheckConfiguration
    {
        public string[] EnabledUsers { get; set; }
        public string[] EnabledDomains { get; set; }
        public string[] EnabledUserGroups { get; set; }
        public bool ShowHintAdd { get; set; }
        public LabelCheckMessage HintAdd { get; set; }
        public bool ShowHintRemove { get; set; }
        public LabelCheckMessage HintRemove { get; set; }
        public bool ShowHintSaveAs { get; set; }
        public LabelCheckMessage HintSaveAs { get; set; }
        public bool ShowHintSaveAsLabelButNoMarking { get; set; }
        public LabelCheckMessage HintSaveAsLabelButNoMarking { get; set; }
    }
}
