﻿namespace Eurolook.AddIn.Common.SystemConfiguration.SensitivityLabels
{
    public class LabelCheckMessage
    {
        public string Title { get; set; }

        public string Message { get; set; }
    }
}
