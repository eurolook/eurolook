﻿using System.Collections.Generic;
using Eurolook.Data.Database;

namespace Eurolook.AddIn.Common.SystemConfiguration.SensitivityLabels
{
    public class SensitivityLabelsConfiguration : ISystemConfigurationSetting
    {
        public string Key => "SensitivityLabelsV2";
        public AzureConfiguration Azure { get; set; }
        public LabelConfiguration DefaultLabel { get; set; }
        public LabelConfiguration SensitiveLabel { get; set; }
        public List<LabelCheckConfiguration> LabelChecks { get; set; } = new();
    }
}
