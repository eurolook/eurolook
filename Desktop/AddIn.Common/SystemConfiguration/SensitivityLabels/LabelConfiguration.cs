namespace Eurolook.AddIn.Common.SystemConfiguration.SensitivityLabels
{
    public class LabelConfiguration
    {
        public string Name { get; set; }
        public string Id { get; set; }
    }
}
