﻿using Eurolook.Data.Database;

namespace Eurolook.AddIn.Common.SystemConfiguration
{
    public class EurolookAresBridgeConfiguration : ISystemConfigurationSetting
    {
        public string Key => "EurolookAresBridge";

        public string ServiceBaseUrl { get; set; }

        public bool IsEnabledForEveryone { get; set; }

        public bool IsEnabledForAllDocuments { get; set; }
    }
}
