using Eurolook.Data.Database;

namespace Eurolook.AddIn.Common.SystemConfiguration
{
    public class CreationWizardConfiguration : ISystemConfigurationSetting
    {
        public string Key => "CreationWizard";
        public bool IsEnabled { get; set; }
    }
}
