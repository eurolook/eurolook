﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.SystemConfiguration.SensitivityLabels;
using Eurolook.Common.Extensions;
using Eurolook.Data.Database;
using Eurolook.Data.Models;

namespace Eurolook.AddIn.Common.SystemConfiguration
{
    public class SystemConfigurationService : ISystemConfigurationService
    {
        private readonly ISystemConfigurationRepository _systemConfigurationRepository;
        private readonly IUserDataRepository _userDataRepository;
        private readonly IUserGroupRepository _userGroupRepository;

        public SystemConfigurationService(
            ISystemConfigurationRepository systemConfigurationRepository,
            IUserDataRepository userDataRepository,
            IUserGroupRepository userGroupRepository)
        {
            _systemConfigurationRepository = systemConfigurationRepository;
            _userDataRepository = userDataRepository;
            _userGroupRepository = userGroupRepository;
        }

        public async Task<bool> IsTemplateStoreEnabledForUser(User user = null)
        {
            var templateStoreConfiguration = await _systemConfigurationRepository.GetConfigurationAsync<TemplateStoreConfiguration>();
            if (templateStoreConfiguration.IsEnabled)
            {
                return true;
            }

            var enabledUserGroupsNames = templateStoreConfiguration.EnabledForUserGroups;
            var enabledUserGroups = await _userDataRepository.GetUserGroups(ug => enabledUserGroupsNames.Contains(ug.Name));

            user ??= _userDataRepository.GetUser();

            return await IsEnabledForUserGroups(enabledUserGroups, user);
        }

        public async Task<SensitivityLabelsConfiguration> GetSensitivityLabelsConfiguration()
        {
            return await _systemConfigurationRepository.GetConfigurationAsync<SensitivityLabelsConfiguration>();
        }

        public async Task<WhatsNewConfiguration> GetWhatsNewConfiguration()
        {
            return await _systemConfigurationRepository.GetConfigurationAsync<WhatsNewConfiguration>();
        }

        public async Task<bool> IsSensitivityLabelCheckEnabledForUser()
        {
            return await GetSensitivityLabelCheckForUser() != null;
        }

        [SuppressMessage("SonarQube", "S2259: Null pointers should not be dereferenced", Justification = "Not null")]
        public async Task<LabelCheckConfiguration> GetSensitivityLabelCheckForUser()
        {
            var configuration = await GetSensitivityLabelsConfiguration();
            var labelChecks = configuration?.LabelChecks;
            if (labelChecks.IsNullOrEmpty())
            {
                return null;
            }

            var user = _userDataRepository.GetUser();
            if (string.IsNullOrWhiteSpace(user?.Login))
            {
                return null;
            }

            var labelCheck = labelChecks?.FirstOrDefault(c => c.EnabledUsers?.Any(x => string.Equals(user.Login, x, StringComparison.InvariantCultureIgnoreCase)) == true);
            if (labelCheck != null)
            {
                return labelCheck;
            }

            labelCheck = labelChecks?.FirstOrDefault(c => c.EnabledDomains?.Any(x => !string.IsNullOrWhiteSpace(x) && user.Login.StartsWith(x, StringComparison.InvariantCultureIgnoreCase)) == true);
            if (labelCheck != null)
            {
                return labelCheck;
            }

            foreach (var check in labelChecks)
            {
                if (check.EnabledUserGroups?.Any() == true)
                {
                    var enabledUserGroups = await _userDataRepository
                        .GetUserGroups(ug => check.EnabledUserGroups.Contains(ug.Name));
                    if (await IsEnabledForUserGroups(enabledUserGroups, user))
                    {
                        return check;
                    }
                }
            }

            return null;
        }

        public async Task<string> GetFeedbackEmailAddress()
        {
            var feedbackConfiguration = await _systemConfigurationRepository.GetConfigurationAsync<FeedbackConfiguration>();
            return feedbackConfiguration.EmailAddress;
        }

        public async Task<bool> IsCreationWizardEnabled()
        {
            var creationWizardConfiguration = await _systemConfigurationRepository.GetConfigurationAsync<CreationWizardConfiguration>();
            return creationWizardConfiguration.IsEnabled;
        }

        private async Task<bool> IsEnabledForUserGroups(List<UserGroup> enabledUserGroups, User user)
        {
            foreach (var group in enabledUserGroups)
            {
                if (await _userGroupRepository.IsMemberOfUserGroupOrMappedAdGroupAsync(user, group.Id))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
