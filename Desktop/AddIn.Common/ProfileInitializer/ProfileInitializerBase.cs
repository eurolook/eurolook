﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.Messages;
using Eurolook.AddIn.Common.StandaloneMode;
using Eurolook.AddIn.Common.UserConfiguration;
using Eurolook.Common;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.DataSync;
using GalaSoft.MvvmLight.Messaging;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.ProfileInitializer
{
    [UsedImplicitly]
    public abstract class ProfileInitializerBase : IProfileInitializer, ICanLog
    {
        private readonly IUserDataRepository _userDataRepository;
        private readonly IAuthorRepository _authorRepository;
        private readonly ISettingsService _settingsService;
        private readonly IUserIdentity _userIdentity;
        private readonly ColorManager _colorManager;
        private readonly DataSyncScheduler _dataSyncScheduler;
        private readonly string _login;
        private readonly object _initializeProfileLock;
        private readonly object _showWelcomeLock;

        private Task<bool> _initializeProfileIfNecessaryTask;
        private Task<bool> _showWelcomeWindowIfNecessaryTask;

        protected ProfileInitializerBase(
            IUserDataRepository userDataRepository,
            IAuthorRepository authorRepository,
            ISettingsService settingsService,
            IUserIdentity userIdentity,
            IClientDatabaseManager clientDatabaseManager,
            ColorManager colorManager,
            DataSyncScheduler dataSyncScheduler)
        {
            _userDataRepository = userDataRepository;
            _authorRepository = authorRepository;
            _settingsService = settingsService;
            _userIdentity = userIdentity;
            _colorManager = colorManager;
            _dataSyncScheduler = dataSyncScheduler;
            _initializeProfileLock = new object();
            _showWelcomeLock = new object();
            _login = _userIdentity.GetUserName();
            clientDatabaseManager.DropDatabaseOnVersionMismatch();
        }

        [CanBeNull]
        protected User UserProfile { get; set; }

        protected virtual bool IsNewUserProfile => UserProfile != null && !UserProfile.Settings.DeviceSettings.Any();

        protected abstract bool IsWelcomeWindowActivated { get; }

        protected bool IsAuthorIncomplete => UserProfile?.Self.IsIncomplete() ?? false;

        public Task<bool> InitializeProfileIfNecessary()
        {
            if (_initializeProfileIfNecessaryTask == null || _initializeProfileIfNecessaryTask.IsCompleted)
            {
                lock (_initializeProfileLock)
                {
                    if (_initializeProfileIfNecessaryTask == null || _initializeProfileIfNecessaryTask.IsCompleted)
                    {
                        _initializeProfileIfNecessaryTask = InitializeProfileIfNecessaryInternal();
                    }
                }
            }

            return _initializeProfileIfNecessaryTask;
        }

        private async Task<bool> InitializeProfileIfNecessaryInternal()
        {
            // When you use async/await, there it's not guaranteed that the method will actually run asynchronously.
            // In case it's critical not to block the caller, using await Task.Yield() will force the method to be asynchronous, and return control at that point.
            await Task.Yield();

            if (IsDatabaseInitialized())
            {
                return true;
            }

            _colorManager.ApplyDefaultColorScheme();

            if (_settingsService.IsStandaloneMode)
            {
                return InitializeFromFile();
            }

            return await InitializeFromServer();
        }

        public Task<bool> ShowWelcomeWindowIfNecessary()
        {
            return Task.Run(
                async () =>
                {
                    if (_showWelcomeWindowIfNecessaryTask == null
                        || _showWelcomeWindowIfNecessaryTask.IsCompleted)
                    {
                        lock (_showWelcomeLock)
                        {
                            if (_showWelcomeWindowIfNecessaryTask == null
                                || _showWelcomeWindowIfNecessaryTask.IsCompleted)
                            {
                                _showWelcomeWindowIfNecessaryTask = ShowWelcomeWindowIfNecessaryInternal();
                            }
                        }
                    }

                    return await _showWelcomeWindowIfNecessaryTask;
                });
        }

        public async Task<bool> ShowWelcomeWindowIfNecessaryInternal()
        {
            if (UserProfile == null)
            {
                await CheckUserProfile();
            }

            bool result = true;
            if (IsWelcomeWindowActivated)
            {
                Execute.OnUiThread(
                    () =>
                    {
                        result = ShowWelcomeWindow() == true;
                    });
            }

            return result;
        }

        public async Task CheckUserProfile()
        {
            UserProfile = await _userDataRepository.GetUserForStartupAsync(_login);
        }

        public bool IsDatabaseInitialized()
        {
            return _userDataRepository.UserExists(_userIdentity.GetUserName());
        }

        public abstract bool? ShowWelcomeWindow();

        private async Task<bool> InitializeFromServer()
        {
            // time to let Word open a document or do other startup stuff before starting the progress bar animation
            await Task.Delay(1000);
            Execute.OnUiThread(
                () =>
                {
                    Messenger.Default.Send(new ProfileCreationStartedMessage());
                });

            // let the progress bar refresh
            await Task.Delay(300);

            string error = await RunDataSync();
            if (error != null)
            {
                this.LogError($"Failed to initialize profile: {error}");
                Execute.OnUiThread(
                    () =>
                    {
                        Messenger.Default.Send(new ProfileCreationFailedMessage());
                    });
                return false;
            }

            await CheckUserProfile();
            Execute.OnUiThread(
                () =>
                {
                    Messenger.Default.Send(new ProfileCreationCompletedMessage());
                    _colorManager.ApplyColorScheme();
                });
            return true;
        }

        protected virtual bool InitializeFromFile()
        {
            try
            {
                // Don't show the progress bar when initializing from file.
                // It's so fast, it's flickering.
                ////Messenger.Default.Send(new ProfileCreationStartedMessage());

                var programDir = Directories.GetProgramDirectory();
                var appDataDir = Directories.GetApplicationDataDirectory(_settingsService.UseRoamingDataDirectory);
                File.Copy(
                    Path.Combine(programDir.FullName, "EurolookStandalone.db"),
                    Path.Combine(appDataDir.FullName, "Eurolook.db"),
                    true);

                var user = _userDataRepository.GetUser();

                var authorDataBackup = AuthorDataBackupXml.Load(_settingsService);
                if (authorDataBackup != null)
                {
                    _authorRepository.RestoreAuthorDataFromBackup(user, authorDataBackup);
                }

                Execute.OnUiThread(
                    () =>
                    {
                        _colorManager.ApplyColorScheme();
                        Messenger.Default.Send(new ProfileCreationCompletedMessage());
                    });
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                Execute.OnUiThread(
                    () =>
                    {
                        Messenger.Default.Send(new ProfileCreationFailedMessage());
                    });
                return false;
            }

            return true;
        }

        private async Task<string> RunDataSync()
        {
            string error = null;
            try
            {
                this.LogInfo("Starting DataSync...");
                await _dataSyncScheduler.RunDataSyncAsync(false);

                if (_dataSyncScheduler.LastRun != null
                    && _dataSyncScheduler.LastRun.Result == DataSyncResult.RemoteWipe)
                {
                    this.LogInfo("There was a remote wipe requested. Starting again.");
                    await _dataSyncScheduler.RunDataSyncAsync(false);
                }

                if (_dataSyncScheduler.LastRun == null || _dataSyncScheduler.LastRun.Result != DataSyncResult.Success)
                {
                    error = _dataSyncScheduler.LastRun == null
                        ? "Last data sync run does not have any result or data sync did not run."
                        : _dataSyncScheduler.LastRun.StandardOutput;
                }
            }
            catch (Exception ex)
            {
                var dataSyncResult = _dataSyncScheduler.LastRun?.Result ?? DataSyncResult.UnexpectedError;
                switch (dataSyncResult)
                {
                    case DataSyncResult.NoConnection:
                        error = "No connection to the Eurolook server";
                        break;
                    default:
                        error = ex.Message;
                        break;
                }
            }

            return error;
        }
    }
}
