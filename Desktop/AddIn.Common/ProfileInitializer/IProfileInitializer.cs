using System.Threading.Tasks;

namespace Eurolook.AddIn.Common.ProfileInitializer
{
    public interface IProfileInitializer
    {
        /// <summary>
        /// Check if the user profile exists and may open a window to create it.
        /// </summary>
        /// <returns>
        /// Returns true if the profile existed or was successfully created during the call of this method.
        /// </returns>
        Task<bool> InitializeProfileIfNecessary();

        bool IsDatabaseInitialized();

        Task<bool> ShowWelcomeWindowIfNecessary();

        Task CheckUserProfile();

        bool? ShowWelcomeWindow();
    }
}
