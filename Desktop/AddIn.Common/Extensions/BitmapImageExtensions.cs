using System;
using System.IO;
using System.Windows.Media.Imaging;

namespace Eurolook.AddIn.Common.Extensions
{
    public static class BitmapImageExtensions
    {
        public static BitmapImage InitAndFreeze(this BitmapImage image, byte[] array)
        {
            image.BeginInit();
            image.StreamSource = new MemoryStream(array);
            image.EndInit();
            image.Freeze();
            return image;
        }

        public static BitmapImage InitAndFreeze(this BitmapImage image, Uri uri)
        {
            image.BeginInit();
            image.CacheOption = BitmapCacheOption.OnLoad;
            image.UriSource = uri;
            image.EndInit();
            image.Freeze();
            return image;
        }
    }
}
