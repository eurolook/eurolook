﻿using System;
using System.Linq;
using JetBrains.Annotations;
using Microsoft.Office.Core;

namespace Eurolook.AddIn.Common.Extensions
{
    public static class CustomXmlNodeExtensions
    {
        public static bool IsIdenticalWith(this CustomXMLNode node, CustomXMLNode other)
        {
            if (other != null)
            {
                var nodePart = node.OwnerPart.Id;
                var otherPart = other.OwnerPart.Id;

                var nodeXpath = node.XPath;
                var otherXpath = other.XPath;

                var result = node.OwnerPart.Id.Equals(other.OwnerPart.Id, StringComparison.InvariantCulture) && node.XPath.Equals(other.XPath, StringComparison.InvariantCulture);
                return result;
            }

            return false;
        }

        [CanBeNull]
        public static CustomXMLNode GetAttribute(this CustomXMLNode node, string name)
        {
            if (node != null)
            {
                foreach (var attr in node.Attributes.OfType<CustomXMLNode>())
                {
                    if (attr.BaseName.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return attr;
                    }
                }

                for (int i = 0; i < node.Attributes.Count; i++)
                {
                    var attr = node.Attributes[i];

                    if (attr.BaseName.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return attr;
                    }
                }
            }

            return null;
        }
    }
}
