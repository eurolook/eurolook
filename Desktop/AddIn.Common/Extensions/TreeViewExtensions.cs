using System;
using System.Windows.Controls;
using System.Windows.Media;
using Eurolook.Common.Log;

namespace Eurolook.AddIn.Common.Extensions
{
    public static class TreeViewExtensions
    {
        public static void TryScrollToTop(this TreeView treeView)
        {
            try
            {
                if (VisualTreeHelper.GetChildrenCount(treeView) == 0)
                {
                    return;
                }

                if (VisualTreeHelper.GetChild(treeView, 0) is Border border
                    && VisualTreeHelper.GetChild(border, 0) is ScrollViewer scrollViewer)
                {
                    scrollViewer.ScrollToHome();
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(typeof(TreeViewExtensions)).Error(ex.Message, ex);
            }
        }

        public static TreeViewItem ContainerFromItemRecursive(this ItemContainerGenerator root, object item)
        {
            if (root.ContainerFromItem(item) is TreeViewItem treeViewItem)
            {
                return treeViewItem;
            }

            foreach (var subItem in root.Items)
            {
                treeViewItem = root.ContainerFromItem(subItem) as TreeViewItem;
                var search = treeViewItem?.ItemContainerGenerator.ContainerFromItemRecursive(item);
                if (search != null)
                {
                    return search;
                }
            }

            return null;
        }
    }
}
