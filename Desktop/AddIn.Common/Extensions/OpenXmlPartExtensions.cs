using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using DocumentFormat.OpenXml.Packaging;

namespace Eurolook.AddIn.Common.Extensions
{
    public static class OpenXmlPartExtensions
    {
        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times", Justification = "Reviewed")]
        public static XDocument GetXDocument(this OpenXmlPart part)
        {
            var xdoc = part.Annotation<XDocument>();
            if (xdoc != null)
            {
                return xdoc;
            }

            using (var stream = part.GetStream())
            {
                using (var streamReader = new StreamReader(stream))
                {
                    using (var xr = XmlReader.Create(streamReader))
                    {
                        xdoc = XDocument.Load(xr);
                    }
                }
            }

            part.AddAnnotation(xdoc);
            return xdoc;
        }
    }
}
