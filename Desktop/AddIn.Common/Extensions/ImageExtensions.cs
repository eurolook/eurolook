using System;
using System.Drawing;
using System.Windows.Media.Imaging;

namespace Eurolook.AddIn.Common.Extensions
{
    public static class ImageExtensions
    {
        public static void ApplyRotationFromExifMetadata(this Image image)
        {
            const int exifPropertyIdOrientation = 274;
            if (Array.IndexOf(image.PropertyIdList, exifPropertyIdOrientation) <= -1)
            {
                return;
            }

            var orientation = (int)image.GetPropertyItem(274).Value[0];
            switch (orientation)
            {
                case 1:
                    // No rotation required.
                    break;
                case 2:
                    image.RotateFlip(RotateFlipType.RotateNoneFlipX);
                    break;
                case 3:
                    image.RotateFlip(RotateFlipType.Rotate180FlipNone);
                    break;
                case 4:
                    image.RotateFlip(RotateFlipType.Rotate180FlipX);
                    break;
                case 5:
                    image.RotateFlip(RotateFlipType.Rotate90FlipX);
                    break;
                case 6:
                    image.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    break;
                case 7:
                    image.RotateFlip(RotateFlipType.Rotate270FlipX);
                    break;
                case 8:
                    image.RotateFlip(RotateFlipType.Rotate270FlipNone);
                    break;
            }

            // This EXIF data is now invalid and should be removed.
            image.RemovePropertyItem(274);
        }

        public static BitmapSource ToBitmapSource(this Image image)
        {
            var bitmap = new Bitmap(image);
            var bitmapSource = bitmap.ToBitmapSource();
            return bitmapSource;
        }
    }
}
