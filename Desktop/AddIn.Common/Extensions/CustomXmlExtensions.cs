﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using JetBrains.Annotations;
using Microsoft.Office.Core;

namespace Eurolook.AddIn.Common.Extensions
{
    public static class CustomXmlExtensions
    {
        [CanBeNull]
        public static string GetCustomXml(
            this CustomXMLParts parts,
            string rootName,
            string rootNamespaceUri = null)
        {
            return parts.GetCustomXmlParts(rootName, rootNamespaceUri)
                        .Select(part => part.XML)
                        .FirstOrDefault();
        }

        public static IEnumerable<CustomXMLPart> GetCustomXmlParts(
            this CustomXMLParts parts,
            string rootName,
            string rootNamespaceUri = null)
        {
            var result = parts.Cast<CustomXMLPart>();
            return result.Where(p => p.DocumentElement.BaseName == rootName)
                         .Where(p => rootNamespaceUri == null || p.DocumentElement.NamespaceURI == rootNamespaceUri);
        }

        public static XDocument GetCustomXmlAsXDocument(this CustomXMLParts parts, string rootName)
        {
            string xml = parts.GetCustomXml(rootName);
            return xml == null ? null : XDocument.Parse(xml);
        }

        public static CustomXMLPart GetCustomXmlPart(this CustomXMLParts parts, string partId)
        {
            return parts.OfType<CustomXMLPart>().FirstOrDefault(p => p.Id == partId);
        }

        public static void ReplaceCustomXml(this CustomXMLParts parts, string rootName, XDocument newContent)
        {
            ReplaceCustomXml(parts, rootName, null, newContent);
        }

        public static void ReplaceCustomXml(this CustomXMLParts parts, string rootName, string rootNamespaceUri, XDocument newContent)
        {
            if (parts == null)
            {
                return;
            }

            var part = parts.Cast<CustomXMLPart>()
                            .FirstOrDefault(
                                p => p.DocumentElement.BaseName == rootName && (rootNamespaceUri == null
                                    || p.DocumentElement.NamespaceURI == rootNamespaceUri));
            if (part != null)
            {
                // replace existing part
                part.DocumentElement.ParentNode.ReplaceChildSubtree(newContent.ToString(), part.DocumentElement);
            }
            else
            {
                // create new part
                parts.Add(newContent.ToString());
            }
        }

        [CanBeNull]
        public static CustomXMLNode GetCustomXmlNode(this CustomXMLParts parts, string id, string xpath)
        {
            var part = parts.SelectByID(id);
            if (part != null)
            {
                return part.SelectSingleNode(xpath);
            }

            return null;
        }
    }
}
