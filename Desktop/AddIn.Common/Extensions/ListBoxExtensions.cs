﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using Eurolook.Common.Log;

namespace Eurolook.AddIn.Common.Extensions
{
    public static class ListBoxExtensions
    {
        private const int Period = 500; // ms
        private const int Max = 3000; // ms
        private static readonly Dictionary<ListBox, Timer> _scrollTimers = new Dictionary<ListBox, Timer>();
        private static readonly Dictionary<ListBox, int> _scrollTimerDurations = new Dictionary<ListBox, int>();
        private static bool _isRunning;

        public static void ScrollToSelectionWhenLoaded(this ListBox listBox)
        {
            StartScrollTimer(listBox);
        }

        private static void StartScrollTimer(ListBox listBox)
        {
            try
            {
                if (!_scrollTimers.ContainsKey(listBox))
                {
                    _scrollTimerDurations.Add(listBox, 0);
                    _scrollTimers.Add(listBox, new Timer(
                        (state) => TryScrollToSelectedItem(listBox),
                        listBox,
                        Period,
                        Period));
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger().Error($"{nameof(StartScrollTimer)} failed.", ex);
                StopScrollTimer(listBox);
            }
        }

        private static void TryScrollToSelectedItem(ListBox listBox)
        {
            try
            {
                if (_isRunning)
                {
                    return;
                }

                _isRunning = true;
                var duration = _scrollTimerDurations.ContainsKey(listBox) ? _scrollTimerDurations[listBox] : 0;
                if (duration > Max)
                {
                    StopScrollTimer(listBox);
                    return;
                }

                listBox.Dispatcher.Invoke(DispatcherPriority.Normal, () =>
                {
                    if (VisualTreeHelper.GetChild(listBox, 0) is Decorator border)
                    {
                        if (border.Child is ScrollViewer scrollViewer)
                        {
                            bool hasItems = listBox.HasItems;
                            bool hasScrollbar = scrollViewer.ExtentHeight > scrollViewer.ViewportHeight;
                            if (hasItems && hasScrollbar && listBox.SelectedItem != null)
                            {
                                var listBoxItem = listBox.ItemContainerGenerator.ContainerFromItem(listBox.SelectedItem) as ListBoxItem;
                                if (listBoxItem != null)
                                {
                                    var elementHeight = listBoxItem.ActualHeight;
                                    var transform = listBoxItem.TransformToAncestor(listBox) as MatrixTransform;
                                    var elementPosY = transform.Value.OffsetY + elementHeight;
                                    var isElementFullyVisible = elementPosY < scrollViewer.ViewportHeight;
                                    if (!isElementFullyVisible)
                                    {
                                        var offset = elementPosY - scrollViewer.ViewportHeight;
                                        scrollViewer.ScrollToVerticalOffset(offset);
                                        StopScrollTimer(listBox);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                });

                _scrollTimerDurations[listBox] = duration + Period;
            }
            catch (Exception ex)
            {
                LogManager.GetLogger().Error($"{nameof(TryScrollToSelectedItem)} failed.", ex);
                StopScrollTimer(listBox);
            }
            finally
            {
                _isRunning = false;
            }
        }

        private static void StopScrollTimer(ListBox listBox)
        {
            try
            {
                if (_scrollTimers.ContainsKey(listBox))
                {
                    _scrollTimers[listBox].Change(Timeout.Infinite, Timeout.Infinite);
                    _scrollTimers.Remove(listBox);
                }

                if (_scrollTimerDurations.ContainsKey(listBox))
                {
                    _scrollTimerDurations.Remove(listBox);
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger().Error($"{nameof(StopScrollTimer)} failed.", ex);
            }
        }
    }
}
