using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Eurolook.AddIn.Common.Extensions
{
    public static class ObservableCollectionExtensions
    {
        public static void Sort<T, TKey>(this ObservableCollection<T> observable, Func<T, TKey> keySelector, IComparer<TKey> comparer = null)
        {
            comparer ??= Comparer<TKey>.Default;
            var sorted = observable.OrderBy(keySelector, comparer).ToList();

            int ptr = 0;
            while (ptr < sorted.Count)
            {
                if (!observable[ptr].Equals(sorted[ptr]))
                {
                    var item = observable[ptr];
                    observable.RemoveAt(ptr);
                    observable.Insert(sorted.IndexOf(item), item);
                }
                else
                {
                    ptr++;
                }
            }
        }
    }
}
