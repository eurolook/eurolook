using System.Windows;
using System.Windows.Media;
using Color = System.Drawing.Color;

namespace Eurolook.AddIn.Common.Extensions
{
    public static class ColorExtensions
    {
        public static Brush ToBrush(this Color color)
        {
            var dispatcher = Application.Current.Dispatcher;
            if (dispatcher.CheckAccess())
            {
                var result = new SolidColorBrush(color.ToWpfColor());
                result.Freeze();
                return result;
            }

            return Application.Current.Dispatcher.Invoke(
                () =>
                {
                    var result = new SolidColorBrush(color.ToWpfColor());
                    result.Freeze();
                    return result;
                });
        }

        public static System.Windows.Media.Color ToWpfColor(this Color color)
        {
            return System.Windows.Media.Color.FromArgb(color.A, color.R, color.G, color.B);
        }
    }
}
