using System;
using System.Windows;
using System.Windows.Interop;

namespace Eurolook.AddIn.Common.Extensions
{
    public static class WindowExtensions
    {
        public static void DisableMinimizeButton(this Window window)
        {
            if (window == null)
            {
                throw new ArgumentNullException(nameof(window));
            }

            var hWnd = new WindowInteropHelper(window).Handle;
            if (hWnd == IntPtr.Zero)
            {
                throw new InvalidOperationException("The window has not yet been completely initialized");
            }

            SafeNativeMethods.SetWindowLong(
                hWnd,
                SafeNativeMethods.GWL_STYLE,
                SafeNativeMethods.GetWindowLong(hWnd, SafeNativeMethods.GWL_STYLE) & ~SafeNativeMethods.WS_MINIMIZEBOX);
        }

        public static void DisableMaximizeButton(this Window window)
        {
            if (window == null)
            {
                throw new ArgumentNullException(nameof(window));
            }

            var hWnd = new WindowInteropHelper(window).Handle;
            if (hWnd == IntPtr.Zero)
            {
                throw new InvalidOperationException("The window has not yet been completely initialized");
            }

            SafeNativeMethods.SetWindowLong(
                hWnd,
                SafeNativeMethods.GWL_STYLE,
                SafeNativeMethods.GetWindowLong(hWnd, SafeNativeMethods.GWL_STYLE) & ~SafeNativeMethods.WS_MAXIMIZEBOX);
        }
    }
}
