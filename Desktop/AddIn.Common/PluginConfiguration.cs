﻿using System.Collections.Generic;

namespace Eurolook.AddIn.Common
{
    public class PluginConfiguration
    {
        public List<string> Excel { get; set; }
        public List<string> Word { get; set; }
        public List<string> PowerPoint { get; set; }
    }
}
