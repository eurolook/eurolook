﻿using System;
using Eurolook.Data.Models;
using Eurolook.Data.Xml;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common
{
    public interface IEurolookDocument
    {
        DocumentModel DocumentModel { get; }

        Language Language { get; set; }

        [CanBeNull]
        DocumentModelLanguage DocumentModelLanguage { get; }

        bool IsEurolookDocument { get; }

        bool IsBasedOnCustomTemplate { get; }

        bool IsActive { get; }

        IEurolookProperties GetEurolookProperties();

        void SetEurolookProperties(Action<IEurolookProperties> modifyAction);

        IOfficeDocumentWrapper OfficeDocumentWrapper { get; }
    }
}
