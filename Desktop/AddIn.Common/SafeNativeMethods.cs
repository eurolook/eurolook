﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using Point = System.Drawing.Point;
using Size = System.Drawing.Size;

namespace Eurolook.AddIn.Common
{
    /// <summary>
    /// A class importing "safe" native methods as defined by
    /// http://msdn.microsoft.com/en-us/library/btadwd4w%28vs.100%29.aspx
    /// </summary>
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "This class imports an external API")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "This class imports an external API")]
    [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "This class imports an external API")]
    [SuppressMessage("ReSharper", "NotAccessedField.Global", Justification = "This class imports an external API")]
    [SuppressMessage("ReSharper", "FieldCanBeMadeReadOnly.Global", Justification = "This class imports an external API")]
    [SuppressMessage(
        "Sonar",
        "S2346:Flags enumerations zero-value members should be named \"None\"",
        Justification = "This class imports an external API")]
    [SuppressMessage("Sonar", "S101:Types should be named in camel case", Justification = "This class imports an external API")]
    internal static class SafeNativeMethods
    {
        [DllImport("user32.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
        public static extern short GetKeyState(int keyCode);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsWindowVisible(IntPtr hWnd);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern void SwitchToThisWindow(IntPtr hWnd, bool turnOn);

        // Is Window minimized
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsIconic(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern int ShowWindow(IntPtr hWnd, uint nCmdShow);

        [DllImport("user32.dll")]
        public static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, int dwExtraInfo);

        [DllImport("user32.dll", EntryPoint = "SetFocus")]
        public static extern IntPtr SetFocus([In] IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        [DllImport("user32.dll", EntryPoint = "GetForegroundWindow")]
        public static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int GetClassName(
            [In] IntPtr hWnd,
            [Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder lpClassName,
            int nMaxCount);

        public static string GetClassName(IntPtr hWnd)
        {
            var sb = new StringBuilder(256);
            GetClassName(hWnd, sb, sb.Capacity);
            return sb.ToString();
        }

        [DllImport("user32.dll")]
        public static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll")]
        public static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        public const int GWL_STYLE = -16;
        public const int WS_MINIMIZEBOX = 0x00020000;
        public const int WS_MAXIMIZEBOX = 0x00010000;

        public static RECT GetWindowRECT(IntPtr hWnd)
        {
            GetWindowRect(hWnd, out var rect);
            return rect;
        }

        public static Rectangle GetWindowRectangle(IntPtr hWnd)
        {
            return GetWindowRECT(hWnd);
        }

        public static Rect GetWindowRect(IntPtr hWnd)
        {
            return GetWindowRECT(hWnd);
        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);

        [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int y, int cx, int cy, int wFlags);

        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DeleteObject(IntPtr hObject);

        [DllImport("gdi32.dll")]
        public static extern int GetDeviceCaps(IntPtr hdc, int nIndex);

        [Serializable]
        [StructLayout(LayoutKind.Sequential)]
        public struct CLSID
        {
            private readonly int a;
            private readonly short b;
            private readonly short c;
            private readonly byte d;
            private readonly byte e;
            private readonly byte f;
            private readonly byte g;
            private readonly byte h;
            private readonly byte i;
            private readonly byte j;
            private readonly byte k;

            public static explicit operator Guid(CLSID clsid) => new Guid(
                clsid.a,
                clsid.b,
                clsid.c,
                clsid.d,
                clsid.e,
                clsid.f,
                clsid.g,
                clsid.h,
                clsid.i,
                clsid.j,
                clsid.k);
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential)]
        public struct SIZEL
        {
            private readonly int cx;
            private readonly int cy;
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential)]
        public struct POINTL
        {
            private readonly int x;
            private readonly int y;
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public RECT(int left, int top, int right, int bottom)
            {
                Left = left;
                Top = top;
                Right = right;
                Bottom = bottom;
            }

            public int Left { get; }

            public int Top { get; }

            public int Right { get; }

            public int Bottom { get; }

            public int Height
            {
                get { return Bottom - Top; }
            }

            public int Width
            {
                get { return Right - Left; }
            }

            public Size Size
            {
                get { return new Size(Width, Height); }
            }

            public Point Location
            {
                get { return new Point(Left, Top); }
            }

            public static RECT FromRectangle(Rectangle rectangle)
            {
                return new RECT(rectangle.Left, rectangle.Top, rectangle.Right, rectangle.Bottom);
            }

            public static implicit operator Rectangle(RECT rect)
            {
                return rect.ToRectangle();
            }

            public static implicit operator RECT(Rectangle rect)
            {
                return FromRectangle(rect);
            }

            public static implicit operator Rect(RECT rect)
            {
                return rect.ToRect();
            }

            public Rectangle ToRectangle()
            {
                return Rectangle.FromLTRB(Left, Top, Right, Bottom);
            }

            public Rect ToRect()
            {
                return new Rect(Left, Top, Width, Height);
            }
        }
    }
}
