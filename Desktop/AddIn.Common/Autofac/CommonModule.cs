﻿using Autofac;
using Eurolook.AddIn.Common.Properties;
using Eurolook.AddIn.Common.UserConfiguration;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.Autofac
{
    [UsedImplicitly]
    public class CommonModule : Module, IEurolookCommonModule
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(new UserConfigurationService(UserConfigurationService.ConfigurationPath))
                   .AsImplementedInterfaces()
                   .SingleInstance();

            builder.RegisterType<EmbeddedResourceManager>().AsImplementedInterfaces();
        }
    }
}
