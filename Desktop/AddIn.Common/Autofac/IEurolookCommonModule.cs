using Autofac.Core;

namespace Eurolook.AddIn.Common.Autofac
{
    public interface IEurolookCommonModule : IModule
    {
    }
}
