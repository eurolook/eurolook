﻿using Autofac;
using Eurolook.AddIn.Common.Authors;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.Autofac
{
    [UsedImplicitly]
    public class ViewModelsModule : Module, IEurolookCommonModule
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AuthorCompletionViewModel>();
            builder.RegisterType<AuthorManagerViewModel2>();
            builder.RegisterType<AuthorSearchViewModel>();
            builder.RegisterType<AuthorPickerViewModel>();
            builder.RegisterType<AuthorRolesSelectionViewModel>();
            builder.RegisterType<AuthorSearchLightBoxViewModel>();
            builder.RegisterType<AuthorPickerLightBoxViewModel>();
            builder.RegisterType<AuthorsAndJobsViewModel>();
        }
    }
}
