﻿using Autofac;
using Eurolook.AddIn.Common.ViewModels;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.Autofac
{
    [UsedImplicitly]
    public class CommonEcModule : Module, IEurolookCommonModule
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<StartupViewModel>().SingleInstance();
        }
    }
}
