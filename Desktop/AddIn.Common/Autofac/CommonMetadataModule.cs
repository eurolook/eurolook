﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Eurolook.AddIn.Common.BrickCommands.EditDocumentMetadata;
using Eurolook.AddIn.Common.DocumentMetadata;
using Eurolook.AddIn.Common.DocumentMetadata.BuiltInProperties;
using Eurolook.AddIn.Common.DocumentMetadata.CalculationCommands;
using Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels;
using Eurolook.AddIn.Common.DocumentMetadata.SharePoint;
using Eurolook.Data.Models.Metadata;
using Eurolook.Data.TermStore;
using Eurolook.Data.Xml.Metadata;

namespace Eurolook.AddIn.Common.Autofac
{
    public class CommonMetadataModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MetadataDialog>().AsImplementedInterfaces();
            builder.RegisterType<EditDocumentMetadataViewModel>().AsSelf();
            builder.RegisterType<TermStoreCache>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<TermStoreXmlParser>().AsImplementedInterfaces();
            builder.RegisterType<TermFilter>().AsImplementedInterfaces();
            builder.RegisterType<MetadataPropertyFactory>().AsImplementedInterfaces();
            builder.RegisterType<MetadataReaderWriter>().AsImplementedInterfaces();
            builder.RegisterType<MetadataXmlReaderWriter>().AsImplementedInterfaces();
            builder.RegisterType<MetadataFormatProvider>().AsImplementedInterfaces();
            builder.RegisterType<ContentTypeProperties>().AsImplementedInterfaces();
            builder.RegisterType<BuiltInProperties>().AsImplementedInterfaces();
            builder.RegisterType<MetaPropertyReaderWriterFactory>().AsImplementedInterfaces();
            builder.RegisterType<EurolookMetadataPropertyCustomReaderWriterFactory>().AsImplementedInterfaces();
            builder.RegisterType<TimeParser>().AsImplementedInterfaces();
            builder.RegisterType<DateTimeParser>().AsImplementedInterfaces();
            builder.RegisterType<MetadataDefaultsWriter>().AsImplementedInterfaces();
            builder.RegisterType<EmptyContentTypePropertyRepairHandler>().AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(typeof(CommonMetadataModule).Assembly)
                   .Where(t => t.GetInterfaces().Contains(typeof(IMetadataViewModel)) && !t.IsAbstract)
                   .AsImplementedInterfaces()
                   .InstancePerDependency();

            builder.Register(c => new MetadataViewModelFactory(c.Resolve<IEnumerable<Func<IMetadataViewModel>>>()))
                   .AsImplementedInterfaces()
                   .SingleInstance();

            builder.RegisterAssemblyTypes(typeof(CommonMetadataModule).Assembly)
                   .Where(t => t.GetInterfaces().Contains(typeof(IMetaPropertyReaderWriter)) && !t.IsAbstract)
                   .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(typeof(CommonMetadataModule).Assembly)
                   .Where(t => t.GetInterfaces().Contains(typeof(ICalculationCommand)) && !t.IsAbstract)
                   .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(typeof(EurolookMetadataPropertyCustomReaderWriterFactory).Assembly)
                   .Where(t => t.GetInterfaces().Contains(typeof(IMetadataPropertyXmlReaderWriter)) && !t.IsAbstract)
                   .AsImplementedInterfaces()
                   .InstancePerDependency();

            builder.RegisterType<TermStoreClientDatabase>().AsImplementedInterfaces().SingleInstance();
        }
    }
}
