using System;
using System.Collections.Generic;
using Eurolook.Data;

namespace Eurolook.AddIn.Common
{
    public interface IAddinContext : IConfigurationInfo
    {
        /// <summary>
        /// Gets the full path of the folder where Eurolook is installed.
        /// </summary>
        string InstallRoot { get; }

        /// <summary>
        /// Gets the current full Office version (including revision and build number).
        /// </summary>
        Version OfficeVersion { get; }

        bool IsPerUserInstall { get; }

        bool IsPerMachineInstall { get; }

        IEnumerable<TimeSpan> LoadTimes();

        void ClearDisabledAddins();
    }
}
