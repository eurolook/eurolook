﻿using Eurolook.AddIn.Common.UserConfiguration;
using Eurolook.Common;

namespace Eurolook.AddIn.Common
{
    public class BasicUserIdentity : IUserIdentity
    {
        public bool IsImpersonated => false;

        public string GetUserName()
        {
            return Windows.Login;
        }
    }
}
