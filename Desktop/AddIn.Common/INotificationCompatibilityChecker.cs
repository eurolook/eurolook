using Eurolook.Data.Models;

namespace Eurolook.AddIn.Common
{
    public interface INotificationCompatibilityChecker
    {
        bool IsCompatibleWithCurrentVersion(Notification notification);
    }
}
