using Eurolook.AddIn.Common.ViewModels;

namespace Eurolook.AddIn.Common.Messages
{
    public class ExpansionStateChangedMessage
    {
        public BrickCategoryViewModel Source { get; set; }
    }
}
