using Eurolook.DataSync;

namespace Eurolook.AddIn.Common.Messages
{
    public class DataSyncCompletedMessage
    {
        public DataSyncCompletedMessage(DataSyncStatus dataSyncStatus)
        {
            DataSyncStatus = dataSyncStatus;
        }

        public DataSyncStatus DataSyncStatus { get; }
    }
}
