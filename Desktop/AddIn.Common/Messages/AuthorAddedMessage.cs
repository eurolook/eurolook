using System;

namespace Eurolook.AddIn.Common.Messages
{
    public class AuthorAddedMessage
    {
        public Guid AuthorId { get; }

        public AuthorAddedMessage(Guid authorId)
        {
            AuthorId = authorId;
        }
    }
}
