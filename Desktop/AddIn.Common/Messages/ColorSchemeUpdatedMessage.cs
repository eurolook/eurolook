using Eurolook.Data.Models;

namespace Eurolook.AddIn.Common.Messages
{
    public class ColorSchemeUpdatedMessage
    {
        public ColorSchemeUpdatedMessage(ColorScheme newColorScheme)
        {
            NewColorScheme = newColorScheme;
        }

        public ColorScheme NewColorScheme { get; set; }
    }
}
