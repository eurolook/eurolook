using Eurolook.AddIn.Common.DocumentMetadata.TermPicker;

namespace Eurolook.AddIn.Common.Messages
{
    public class TermSelectionChangedMessage
    {
        public TermSelectionChangedMessage(TermViewModel termViewModel)
        {
            TermViewModel = termViewModel;
        }

        public TermViewModel TermViewModel { get; }
    }
}
