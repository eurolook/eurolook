﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.Office.Core;

namespace Eurolook.AddIn.Common
{
    public interface IOfficeDocumentWrapper
    {
        string GetCustomXml(string rootName);

        [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "Use name from Office API")]
        CustomXMLParts CustomXMLParts { get; }

        string Name { get; }

        string FullName { get; }

        string FileExtension { get; }

        bool Saved { get; set; }

        void Save();

        void SaveAs(string fileName);

        void SaveCopyAs(string fileName);

        dynamic Open(string fileName);

        dynamic GetDocument();

        dynamic BuiltInDocumentProperties { get; }

        MetaProperties ContentTypeProperties { get; }

        string GetHttpContentType { get; }
    }
}
