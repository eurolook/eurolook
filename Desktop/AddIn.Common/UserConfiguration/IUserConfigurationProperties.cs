namespace Eurolook.AddIn.Common.UserConfiguration
{
    public interface IUserConfigurationProperties
    {
        string ProgramVersion { get; set; }

        string ImpersonationUser { get; set; }

        bool IsTaskPaneVisible { get; set; }

        int TaskPaneWidth { get; set; }

        string DataSyncUrl { get; set; }

        string ColorScheme { get; set; }

        string TaskPanePosition { get; set; }

        int TaskPaneHeight { get; set; }

        string LastUsedRuleFiles { get; set; }
    }
}
