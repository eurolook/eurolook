namespace Eurolook.AddIn.Common.UserConfiguration
{
    public interface IUserIdentity
    {
        bool IsImpersonated { get; }

        string GetUserName();
    }
}
