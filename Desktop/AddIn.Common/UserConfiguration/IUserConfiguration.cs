﻿namespace Eurolook.AddIn.Common.UserConfiguration
{
    public interface IUserConfiguration : IUserConfigurationProperties, IDataSyncEnvironmentConfiguration
    {
        void Save();
    }
}
