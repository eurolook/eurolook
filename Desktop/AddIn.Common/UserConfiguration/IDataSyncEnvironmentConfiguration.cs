﻿namespace Eurolook.AddIn.Common.UserConfiguration
{
    public interface IDataSyncEnvironmentConfiguration
    {
        string GetDataSyncUrl();

        string GetQuickStartGuideUrl();

        string GetOnlineHelpUrl();

        string GetAdminUIUrl();

        string GetTemplateStoreUrl();

        void SetDataSyncUrl(string newUrl);
    }
}
