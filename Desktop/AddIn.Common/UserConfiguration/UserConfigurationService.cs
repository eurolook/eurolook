﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Eurolook.Common;
using Eurolook.Common.Log;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Eurolook.AddIn.Common.UserConfiguration
{
    public class UserConfigurationService : IUserConfiguration, ICanLog
    {
        private static readonly JsonSerializerSettings SerializerSettings = new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            Formatting = Formatting.Indented,
        };

        private readonly string _configPath;
        private readonly UserConfigurationProperties _properties;

        public UserConfigurationService(string configPath)
        {
            _configPath = configPath;
            _properties = LoadSettings();
        }

        public static string ConfigurationPath
        {
            get
            {
                var appDataFolder = Directories.GetApplicationDataDirectory(AppSettings.Instance.UseRoamingDataDirectory);
                return Path.Combine(appDataFolder.FullName, "Eurolook.config");
            }
        }

        public string ProgramVersion
        {
            get => _properties.ProgramVersion;
            set => _properties.ProgramVersion = value;
        }

        public string ImpersonationUser
        {
            get => _properties.ImpersonationUser;
            set => _properties.ImpersonationUser = value;
        }

        public bool IsTaskPaneVisible
        {
            get => _properties.IsTaskPaneVisible;
            set => _properties.IsTaskPaneVisible = value;
        }

        public int TaskPaneWidth
        {
            get => _properties.TaskPaneWidth;
            set => _properties.TaskPaneWidth = value;
        }

        public string DataSyncUrl
        {
            get => _properties.DataSyncUrl;
            set => _properties.DataSyncUrl = value;
        }

        public string ColorScheme
        {
            get => _properties.ColorScheme;
            set => _properties.ColorScheme = value;
        }

        public string TaskPanePosition
        {
            get => _properties.TaskPanePosition;
            set => _properties.TaskPanePosition = value;
        }

        public int TaskPaneHeight
        {
            get => _properties.TaskPaneHeight;
            set => _properties.TaskPaneHeight = value;
        }

        public string LastUsedRuleFiles
        {
            get => _properties.LastUsedRuleFiles;
            set => _properties.LastUsedRuleFiles = value;
        }

        public string GetDataSyncUrl()
        {
            return GetDataSyncEnvironment()?.DataSyncUrl;
        }

        public string GetQuickStartGuideUrl()
        {
            return GetDataSyncEnvironment()?.QuickStartGuideUrl;
        }

        public string GetOnlineHelpUrl()
        {
            return GetDataSyncEnvironment()?.OnlineHelpUrl;
        }

        public string GetAdminUIUrl()
        {
            return GetDataSyncEnvironment()?.AdminUIUrl;
        }

        public string GetTemplateStoreUrl()
        {
            return GetDataSyncEnvironment()?.TemplateStoreUrl;
        }

        public void SetDataSyncUrl(string dataSyncUrl)
        {
            DataSyncUrl = dataSyncUrl;
            Save();
        }

        public void Save()
        {
            Save(_properties);
        }

        private static UserConfigurationProperties UpgradeToNewFormat(string xmlText)
        {
            var xml = XDocument.Parse(xmlText);
            var properties = new UserConfigurationProperties();

            var oldSettings = xml.Root?
                                 .Elements("version")
                                 .OrderByDescending(e => e.Attribute("val")?.Value)
                                 .FirstOrDefault()
                                 ?.Descendants("group").FirstOrDefault(
                                     e => e.Attribute("name")?.Value == "Eurolook.WordAddIn.Properties.Settings");

            if (oldSettings != null)
            {
                properties.ProgramVersion = oldSettings.Parent?.Attribute("val")?.Value ?? string.Empty;

                properties.IsTaskPaneVisible = bool.TryParse(
                    oldSettings.Element("IsTaskPaneVisible")?.Value,
                    out bool bVal)
                    ? bVal
                    : properties.IsTaskPaneVisible;

                properties.TaskPaneHeight = int.TryParse(oldSettings.Element("TaskPaneHeight")?.Value, out int iVal)
                    ? iVal
                    : properties.TaskPaneHeight;
                properties.TaskPaneWidth = int.TryParse(oldSettings.Element("TaskPaneWidth")?.Value, out iVal)
                    ? iVal
                    : properties.TaskPaneWidth;

                properties.TaskPanePosition =
                    oldSettings.Element("TaskPanePosition")?.Value ?? properties.TaskPanePosition;
                properties.ColorScheme =
                    oldSettings.Element("ColorScheme")?.Value ?? properties.ColorScheme;
                properties.DataSyncUrl =
                    oldSettings.Element("DataSyncUrl")?.Value ?? properties.DataSyncUrl;
                properties.LastUsedRuleFiles =
                    oldSettings.Element("LastUsedRuleFiles")?.Value ?? properties.LastUsedRuleFiles;
                properties.ImpersonationUser =
                    oldSettings.Element("ImpersonationUser")?.Value ?? properties.ImpersonationUser;
            }

            return properties;
        }

        private UserConfigurationProperties LoadSettings()
        {
            UserConfigurationProperties properties;

            try
            {
                if (!File.Exists(_configPath))
                {
                    if (!Directory.Exists(Path.GetDirectoryName(_configPath)))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(_configPath) ?? string.Empty);
                    }

                    properties = new UserConfigurationProperties();
                    Save(properties);
                }
                else
                {
                    var fileContent = File.ReadAllText(_configPath);
                    if (fileContent.StartsWith("<?xml"))
                    {
                        properties = UpgradeToNewFormat(fileContent);
                        Save(properties);
                    }
                    else
                    {
                        properties = JsonConvert.DeserializeObject(
                            fileContent,
                            typeof(UserConfigurationProperties),
                            SerializerSettings) as UserConfigurationProperties;
                    }
                }
            }
            catch (Exception e)
            {
                this.LogWarn("Could not load user configuration.", e);

                properties = new UserConfigurationProperties();
                Save(properties);
            }

            return properties ?? new UserConfigurationProperties();
        }

        private void Save(UserConfigurationProperties properties)
        {
            var json = JsonConvert.SerializeObject(properties, typeof(IUserConfiguration), SerializerSettings);
            File.WriteAllText(_configPath, json);
        }

        /// <summary>
        /// Gets the configured <see cref="DataSyncEnvironment" />.
        /// </summary>
        /// <returns>The configured <see cref="DataSyncEnvironment" />.</returns>
        /// <remarks>
        /// The configured DataSyncEnvironment is retrieved as follows:
        /// - First the value configured in the Eurolook.config is checked.
        /// If this value exists in the list of available then this value is taken.
        /// - Second, the DefaultDataSyncEnvironment defined in the global config file in ProgramFiles is considered.
        /// If this value exists in the list of available then this value is taken.
        /// - Third, the first entry in the list of available environments is taken.
        /// - Last, as a fallback, the first environment in the list of available environments is taken.
        /// </remarks>
        private DataSyncEnvironment GetDataSyncEnvironment()
        {
            var environments = AppSettings.Instance.DataSyncEnvironments;

            return environments?.Find(dse => dse.DataSyncUrl == _properties.DataSyncUrl)
                   ?? environments?.Find(e => e.Name == AppSettings.Instance.DefaultDataSyncEnvironment)
                   ?? environments?.FirstOrDefault()
                   ?? new DataSyncEnvironment
                   {
                       Name = "LOCAL",
                       DataSyncUrl = "http://localhost/api/v2",
                   };
        }
    }
}
