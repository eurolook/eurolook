namespace Eurolook.AddIn.Common.UserConfiguration
{
    public class UserConfigurationProperties : IUserConfigurationProperties
    {
        public UserConfigurationProperties()
        {
            ProgramVersion = string.Empty;
            ImpersonationUser = string.Empty;
            IsTaskPaneVisible = true;
            TaskPaneWidth = 320;
            DataSyncUrl = string.Empty;
            ColorScheme = string.Empty;
            TaskPanePosition = "Left";
            TaskPaneHeight = 0;
            LastUsedRuleFiles = string.Empty;
        }

        public string ProgramVersion { get; set; }

        public string ImpersonationUser { get; set; }

        public bool IsTaskPaneVisible { get; set; }

        public int TaskPaneWidth { get; set; }

        public string DataSyncUrl { get; set; }

        public string ColorScheme { get; set; }

        public string TaskPanePosition { get; set; }

        public int TaskPaneHeight { get; set; }

        public string LastUsedRuleFiles { get; set; }
    }
}
