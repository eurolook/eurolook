﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace Eurolook.AddIn.Common.Database
{
    public class AddinDatabase : EurolookClientDatabase, IEurolookDataRepository
    {
        private readonly ILanguageRepository _languageRepository;

        public AddinDatabase(ILanguageRepository languageRepository)
        {
            _languageRepository = languageRepository;
            SetLocalDataDirectory(AppSettings.Instance.UseRoamingDataDirectory);
        }

        public string GetFilePath()
        {
            using (var context = GetContext())
            {
                string dataDir = AppDomain.CurrentDomain.GetData("DataDirectory") + "\\";
                string dataSource = context.Database.GetDbConnection().DataSource;
                return dataSource.Replace("|DataDirectory|", dataDir);
            }
        }

        #region Create Document Dialog

        [NotNull]
        public List<DocumentStructure> GetDocumentStructures(DocumentModel model)
        {
            if (model == null)
            {
                return new List<DocumentStructure>();
            }

            using (var context = GetContext())
            {
                var result = context.DocumentStructure.AsNoTracking()
                                    .Include(ds => ds.Brick).ThenInclude(x => x.Group)
                                    .Include(ds => ds.Brick).ThenInclude(x => x.Category)
                                    .Include(ds => ds.Brick).ThenInclude(x => x.Texts).ThenInclude(t => t.Text)
                                    .Where(ds => ds.DocumentModelId == model.Id).ToList();

                foreach (var ds in result)
                {
                    ds.DocumentModel = model;
                }

                return result;
            }
        }

        [CanBeNull]
        public Resource GetResource(string alias)
        {
            using var context = GetContext();
            return context.Resources.AsNoTracking().FirstOrDefault(r => r.Alias == alias);
        }

        [CanBeNull]
        public Task<Resource> GetResourceAsync(string alias)
        {
            using var context = GetContext();
            return context.Resources.AsNoTracking().FirstOrDefaultAsync(r => r.Alias == alias);
        }

        [CanBeNull]
        public LocalisedResource GetLocalisedResource(string alias, [CanBeNull] Language lang)
        {
            using (var context = GetContext())
            {
                if (lang != null)
                {
                    var result = context.LocalisedResources.AsNoTracking()
                                        .FirstOrDefault(r => r.LanguageId == lang.Id && r.Resource.Alias == alias);
                    if (result != null)
                    {
                        return result;
                    }
                }

                var en = _languageRepository.GetEnglishLanguage(context);
                return context.LocalisedResources.AsNoTracking()
                              .FirstOrDefault(r => r.LanguageId == en.Id && r.Resource.Alias == alias);
            }
        }

        public List<LocalisedResource> GetLocalisedResources(DocumentModel model, Language lang)
        {
            var result = new List<LocalisedResource>();
            var context = GetContext();

            // get the structures of the document model
            foreach (var structure in context.DocumentStructure.AsNoTracking()
                                             .Where(x => x.DocumentModelId == model.Id))
            {
                // get the brick resources
                var structure1 = structure;
                foreach (var brickResource in context.BrickResources.AsNoTracking()
                                                     .Where(x => x.BrickId == structure1.BrickId))
                {
                    // get the localisation
                    var resource = brickResource;
                    var localisation =
                        context.LocalisedResources.AsNoTracking().Where(
                                   x => x.ResourceId == resource.ResourceId && x.LanguageId == lang.Id)
                               .Include(x => x.Resource)
                               .FirstOrDefault();
                    if (localisation != null)
                    {
                        result.Add(localisation);
                    }
                }
            }

            return result;
        }

        #endregion

        #region Insert Person Name Dialog

        public List<PersonName> GetPersonNames()
        {
            using var context = GetContext();
            return context.PersonNames.ToList();
        }

        public List<CharacterMapping> GetCharacterMappings()
        {
            using var context = GetContext();
            return context.CharacterMappings.ToList();
        }

        #endregion

        #region Apply style via style shortcut (F6)

        public List<StyleShortcut> GetStyleShortcuts()
        {
            using var context = GetContext();
            return context.StyleShortcuts.ToList();
        }

        #endregion

        #region Color Schemes

        public ColorScheme[] GetAllColorSchemes()
        {
            using var context = GetContext();
            return context.ColorSchemes.ToArray();
        }

        #endregion
    }
}
