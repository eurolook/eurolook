﻿using System;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.PerformanceLog;
using Eurolook.Data.ActionLogs;
using Eurolook.Data.Models;

namespace Eurolook.AddIn.Common.Database
{
    public interface IStatisticsRepository
    {
        Task LogDocumentCreationAsync(DocumentModel documentModel, DocumentCreationLog info);

        Task LogDocumentFromTemplateStoreCreationAsync(DocumentModel documentModel, TemplateStoreDocumentCreationLog info);

        Task LogBrickInsertionAsync(DocumentModel documentModel, BrickInsertionLog info);

        Task LogToolCommandExecution(DocumentModel documentModel, ToolCommandExecutionLog info);

        Task IncreaseSessionCountAsync();

        Task WritePerformanceLogAsync(PerformanceLogEvent logEvent, TimeSpan time, IPerformanceLogInfoProvider info);

        Task LogDocumentConversionAsync(DocumentModel documentModel, DocumentConversionLog info);
    }
}
