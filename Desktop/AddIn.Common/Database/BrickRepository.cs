﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Eurolook.AddIn.Common.Database
{
    public class BrickRepository : EurolookClientDatabase, IBrickRepository
    {
        private readonly IUserDataRepository _userDataRepository;

        public BrickRepository(IUserDataRepository userDataRepository)
        {
            _userDataRepository = userDataRepository;
        }

        public List<UserBrick> GetUserBricks(DocumentModel model)
        {
            var user = _userDataRepository.GetUser();
            using (var context = GetContext())
            {
                return context.UserBricks
                              .Where(x => x.UserId == user.Id)
                              .Include(x => x.Brick)
                              .ToList();
            }
        }

        public void DeleteUserBrick(BuildingBlockBrick brick)
        {
            var user = _userDataRepository.GetUser();
            using var context = GetContext();

            var dbBrick = context.Bricks.IgnoreQueryFilters().FirstOrDefault(x => x.Id == brick.Id);
            dbBrick?.SetDeletedFlag(ModificationType.ClientModification);

            var dbUserBrick = context.UserBricks.IgnoreQueryFilters().FirstOrDefault(x => x.BrickId == brick.Id && x.UserId == user.Id);
            dbUserBrick?.SetDeletedFlag(ModificationType.ClientModification);

            context.SaveChanges();
        }

        public async Task<T> GetBrickSettingsAsync<T>(Guid userSettingsId, Guid brickId)
            where T : class
        {
            await using (var context = GetContext())
            {
                return await GetBrickSettingsAsync<T>(context, userSettingsId, brickId);
            }
        }

        public async Task<T> GetBrickSettingsAsync<T>(EurolookClientContext context, Guid userSettingsId, Guid brickId)
            where T : class
        {
            string settingsJson = await context.BrickSettings
                                               .Where(x => x.UserSettingsId == userSettingsId && x.BrickId == brickId)
                                               .Select(x => x.SettingsJson)
                                               .FirstOrDefaultAsync();
            return settingsJson != null ? JsonConvert.DeserializeObject<T>(settingsJson) : null;
        }

        public async Task SaveBrickSettings<T>(Guid userSettingsId, Guid brickId, T settingsObject)
        {
            await using (var context = GetContext())
            {
                await SaveBrickSettings(context, userSettingsId, brickId, settingsObject);
            }
        }

        public async Task SaveBrickSettings<T>(
            EurolookClientContext context,
            Guid userSettingsId,
            Guid brickId,
            T settingsObject)
        {
            string json = JsonConvert.SerializeObject(settingsObject);
            var brickSettings =
                context.BrickSettings.FirstOrDefault(x => x.UserSettingsId == userSettingsId && x.BrickId == brickId);
            if (brickSettings != null)
            {
                brickSettings.SettingsJson = json;
                brickSettings.SetModification(ModificationType.ClientModification);
            }
            else
            {
                brickSettings = new BrickSettings();
                brickSettings.Init();
                brickSettings.SetModification(ModificationType.ClientModification);
                brickSettings.UserSettingsId = userSettingsId;
                brickSettings.BrickId = brickId;
                brickSettings.SettingsJson = json;
                context.BrickSettings.Add(brickSettings);
            }

            await context.SaveChangesAsync();
        }
    }
}
