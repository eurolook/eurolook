using System.Threading.Tasks;

namespace Eurolook.AddIn.Common.Database
{
    public interface IClientDatabaseManager
    {
        bool EnsureCreated();

        bool EnsureDeleted();

        Task DropDatabaseAsync();

        void DropDatabaseOnVersionMismatch();
    }
}
