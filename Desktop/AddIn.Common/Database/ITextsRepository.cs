using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.Database
{
    public interface ITextsRepository
    {
        void LoadOrgaEntityTexts(OrgaEntity orgaEntity, Language language, Language fallback = null);

        [CanBeNull]
        Translation GetTranslation(string alias, [CanBeNull] Language lang);

        Task<Translation> GetTranslationAsync(string alias, [CanBeNull] Language lang);

        Task<Translation> GetTranslationAsync(Guid textId, Language lang, bool fallbackToEnglish = true);

        [NotNull]
        Task<List<Translation>> GetTranslationsAsync(string alias);

        Task<Text[]> GetBrickTexts(Guid brickId);

        IEnumerable<Translation> GetTranslations(DocumentModel model, Language lang);
    }
}
