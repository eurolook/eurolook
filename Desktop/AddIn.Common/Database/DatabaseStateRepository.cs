using System;
using System.Linq;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.Database
{
    [UsedImplicitly]
    public class DatabaseStateRepository : AddinDatabase, IDatabaseStateRepository
    {
        public DatabaseStateRepository(ILanguageRepository languageRepository)
            : base(languageRepository)
        {
        }

        public DatabaseState GetDatabaseState()
        {
            using (var context = GetContext())
            {
                return context.DatabaseState.FirstOrDefault();
            }
        }

        public string GetProductCustomizationId()
        {
            using (var context = GetContext())
            {
                return context.DatabaseState.FirstOrDefault()?.ProductCustomizationId;
            }
        }

        public Version GetClientVersion()
        {
            using (var context = GetContext())
            {
                return context.DatabaseState.FirstOrDefault()?.ClientVersion;
            }
        }

        public DateTime? GetLastUpdate()
        {
            using (var context = GetContext())
            {
                return context.DatabaseState.FirstOrDefault()?.LastUpdateUtc?.ToLocalTime();
            }
        }

        public DateTime? GetLastDataSyncRunDate()
        {
            using (var context = GetContext())
            {
                return context.DatabaseState.FirstOrDefault()?.LastRunDateUtc?.ToLocalTime();
            }
        }

        public DateTime? GetLastSuccessfulDataSyncDate()
        {
            using (var context = GetContext())
            {
                return context.DatabaseState.FirstOrDefault()?.LastSuccessfulRunDateUtc?.ToLocalTime();
            }
        }
    }
}
