﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.Database
{
    public interface IEurolookDataRepository
    {
        List<DocumentStructure> GetDocumentStructures(DocumentModel model);

        DocumentStructure GetDocumentStructure(Guid id);

        List<LocalisedResource> GetLocalisedResources(DocumentModel model, Language lang);

        LocalisedResource GetLocalisedResource(string alias, [CanBeNull] Language lang);

        Resource GetResource(string alias);

        Task<Resource> GetResourceAsync(string alias);

        List<PersonName> GetPersonNames();

        List<CharacterMapping> GetCharacterMappings();

        List<StyleShortcut> GetStyleShortcuts();

        ColorScheme[] GetAllColorSchemes();

        OrgaEntity GetOrgaEntity(Guid id);
    }
}
