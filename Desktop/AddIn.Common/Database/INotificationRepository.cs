using System;
using System.Threading.Tasks;
using Eurolook.Data.Models;

namespace Eurolook.AddIn.Common.Database
{
    public interface INotificationRepository
    {
        Notification[] GetActiveNotifications(INotificationCompatibilityChecker compatibilityChecker);

        Task<int> GetHiddenNotificationsCountAsync();

        void RestoreActiveNotifications();

        void TryClearNotification(Guid referenceId);
    }
}
