using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.Database
{
    public interface IUserDataRepository
    {
        User GetUser();

        bool UserExists(string login);

        User GetUserForLogging();

        Task<User> GetUserForDocumentCreationAsync();

        Task<User> GetUserForStartupAsync(string login);

        Task<User> GetUserIncludingAuthorsAndJobAssignmentsAsync();

        bool AddAuthorToUser(User user, Author author);

        void DeleteAuthorFromUser(Guid userId, Author author);

        [CanBeNull]
        UserSettings GetUserSettings();

        Task<UserSettings> GetUserSettingsAsync();

        UserSettings UpdateUserSettings(UserSettings settings);

        bool IsSuperior(User user, Author author);

        DeviceSettings GetOrCreateCurrentDeviceSettings();

        void UpdateDeviceSettings(DeviceSettings deviceSettings);

        Task<User> GetUserForManagerOverviewAsync(string login);

        Task<List<UserGroup>> GetUserGroups(Expression<Func<UserGroup, bool>> criteria);

        // ReSharper disable once InconsistentNaming
        Task UpdateCustomerExperienceProgramAsync(bool isEnabled);

        Task UpdateShowWelcomeWindowShownAsync();
    }
}
