﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.Authors;
using Eurolook.AddIn.Common.StandaloneMode;
using Eurolook.Data;
using Eurolook.Data.Models;
using Eurolook.Data.Models.AuthorRoles;

namespace Eurolook.AddIn.Common.Database
{
    public interface IAuthorRepository
    {
        Task LoadOrgaTreeAsync(ICollection<OrgaEntityViewModel> orgaTree, Language language = null);
        Task LoadOrgaTreeTranslationsAsync(OrgaEntityViewModel dg, Language language, bool goDeep = true);

        Task<PredefinedFunction[]> GetPredefinedFunctionsAsync();
        Address[] GetAddresses();

        Author GetAuthorForDocumentCreation(Guid authorId, Language language);

        Author GetAuthorForDocumentCreation(Guid authorId, Language language, Guid fallbackId);

        JobAssignment GetJobAssignmentForDocumentCreation(Guid id, Language language);

        // Author editor
        Task<Author> GetAuthorForEditorAsync(Guid authorId);
        void UpdateAuthorFromEditor(Author author);

        // Job editor
        Task<Author> GetMainJobAsync(Guid authorId);
        Task<JobAssignment[]> GetJobsAsync(Guid authorId);
        void UpdateMainJob(IJobAssignment job);
        void UpdateJobAssignment(IJobAssignment job);
        void CreateJobAssignment(IJobAssignment job);
        void DeleteJobAssignment(IJobAssignment job);

        // Workplace editor
        Workplace[] GetWorkplaces(Guid authorId);
        void UpdateWorkplace(Workplace workplace);

        Author UpdateAuthor(Author author);

        Author GetAuthor(Guid id);

        Task<List<DocumentModelAuthorRole>> GetAuthorRolesAsync(Guid documentModelId);

        Task LoadJobAssignmentsAsync(Author author);

        void LoadTranslationsForAuthor(Author author, Language language, EurolookClientContext dbContext = null);

        void RestoreAuthorDataFromBackup(User user, AuthorDataBackupXml authorDataBackup);
    }
}
