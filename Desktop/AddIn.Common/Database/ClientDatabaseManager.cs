using System.IO;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.Messages;
using Eurolook.Common;
using Eurolook.Common.Log;
using Eurolook.Data;
using GalaSoft.MvvmLight.Messaging;

namespace Eurolook.AddIn.Common.Database
{
    public class ClientDatabaseManager : IClientDatabaseManager, ICanLog
    {
        private readonly IDatabaseStateRepository _database;
        private readonly IMessageService _messageService;
        private readonly IConfigurationInfo _configurationInfo;
        private readonly ColorManager _colorManager;

        public ClientDatabaseManager(
            IDatabaseStateRepository database,
            IMessageService messageService,
            IConfigurationInfo configurationInfo,
            ColorManager colorManager)
        {
            _database = database;
            _messageService = messageService;
            _colorManager = colorManager;
            _configurationInfo = configurationInfo;
        }

        public bool EnsureCreated()
        {
            if (_database.EnsureCreated())
            {
                this.LogInfo("Client database created");
                return true;
            }

            return false;
        }

        public bool EnsureDeleted()
        {
            if (_database.EnsureDeleted())
            {
                this.LogInfo("Client database deleted");
                return true;
            }

            return false;
        }

        public async Task DropDatabaseAsync()
        {
            // Wait for running DataSync.exe processes
            await DataSync.DataSync.WaitForExit();

            // Wait for file access on the DB
            await Tools.WaitForFileAccessAsync(
                _database.GetFilePath(),
                FileAccess.ReadWrite,
                fileStream =>
                {
                    fileStream.Close();

                    // close all open windows to prevent any conflicts when another dialog tries to access the deleted database.
                    _messageService.CloseAllWindows();

                    // delete the database and recreate it
                    EnsureDeleted();
                    EnsureCreated();
                    Messenger.Default.Send(new LocalDataDeletedMessage());

                    _colorManager.ApplyDefaultColorScheme();

                    return Task.CompletedTask;
                });
        }

        public void DropDatabaseOnVersionMismatch()
        {
            if (EnsureCreated())
            {
                // database was just created, doesn't need to be dropped.
                return;
            }

            // drop database when from a different environment or when running a newer version of the addin
            var databaseState = _database.GetDatabaseState();
            if (_configurationInfo.ProductCustomizationId == databaseState.ProductCustomizationId
                && _configurationInfo.ClientVersion == databaseState.ClientVersion)
            {
                return;
            }

            this.LogWarn(
                "Dropping local database due to version mismatch. "
                + $"Found database version {databaseState.ClientVersion} in customization "
                + $"'{databaseState.ProductCustomizationId}' with "
                + $"client version {_configurationInfo.ClientVersion} in customization "
                + $"'{_configurationInfo.ProductCustomizationId}'");

            // Wait for file access on the DB
            Tools.WaitForFileAccess(
                _database.GetFilePath(),
                FileAccess.ReadWrite,
                fileStream =>
                {
                    fileStream.Close();
                    EnsureDeleted();
                    EnsureCreated();
                    Messenger.Default.Send(new LocalDataDeletedMessage());
                });
        }
    }
}
