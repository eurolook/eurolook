﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.UserConfiguration;
using Eurolook.Data;
using Eurolook.Data.Extensions;
using Eurolook.Data.Models;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace Eurolook.AddIn.Common.Database
{
    public class UserDataRepository : EurolookClientDatabase, IUserDataRepository
    {
        private readonly IAuthorRepository _authorRepository;
        private readonly IUserIdentity _userIdentity;

        public UserDataRepository(IAuthorRepository authorRepository, IUserIdentity userIdentity)
        {
            _authorRepository = authorRepository;
            _userIdentity = userIdentity;
        }

        public User GetUser()
        {
            return GetUser(_userIdentity.GetUserName());
        }

        private User GetUser(string login)
        {
            using (var context = GetContext())
            {
                return GetUser(context, login);
            }
        }

        private User GetUser(EurolookContext context, string login)
        {
            return context.Users.AsNoTracking().FirstOrDefault(u => u.Login == login);
        }

        public User GetUserForLogging()
        {
            using (var context = GetContext())
            {
                string userName = _userIdentity.GetUserName();
                return context.Users
                              .Include(u => u.Self)
                              .Include(u => u.Settings)
                              .FirstOrDefault(u => u.Login == userName);
            }
        }

        public async Task<User> GetUserForDocumentCreationAsync()
        {
            await using var context = GetContext();

            // in this method data is only read, never written - so we make the context non-tracking
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            string login = _userIdentity.GetUserName();

            var user = await context.Users
                                    .Where(u => u.Login == login)
                                    .Include(u => u.Self)
                                    .FirstAsync();

            user.Authors = new HashSet<UserAuthor>(await context.UserAuthors
                                                                .Where(u => u.UserId == user.Id)
                                                                .Include(u => u.Author)
                                                                .ToListAsync());

            user.Superiors = new HashSet<UserSuperior>(await context.UserSuperiors
                                                                    .Where(s => s.UserId == user.Id)
                                                                    .Include(s => s.Author)
                                                                    .ToListAsync());

            user.Settings = await context.UserSettings
                                         .Where(s => s.Id == user.SettingsId)
                                         .Include(s => s.DocumentModelSettings)
                                         .FirstAsync();

            user.Authors.RemoveWhere(x => x.Author == null);
            user.Superiors.RemoveWhere(x => x.Author == null);

            await LoadAuthorDataAsync(context, user.Self);
            foreach (var favorite in user.Authors)
            {
                await LoadAuthorDataAsync(context, favorite.Author);
            }

            foreach (var superior in user.Superiors)
            {
                await LoadAuthorDataAsync(context, superior.Author);
            }

            return user;
        }

        private async Task LoadAuthorDataAsync(EurolookClientContext context, Author author)
        {
            author.OrgaEntity = await context.OrgaEntities.FirstOrDefaultAsync(x => x.Id == author.OrgaEntityId);
            author.PredefinedFunction = await context.PredefinedFunctions.FirstOrDefaultAsync(x => x.Id == author.PredefinedFunctionId);
            author.Functions = context.JobFunctions.Where(x => x.AuthorId == author.Id).ToHashSet();
        }

        [CanBeNull]
        public async Task<User> GetUserForStartupAsync(string login)
        {
            using (var context = GetContext())
            {
                return await context.Users
                                    .Include(u => u.Self)
                                    .Include(u => u.Settings).ThenInclude(x => x.DeviceSettings)
                                    .FirstOrDefaultAsync(u => u.Login == login);
            }
        }

        public async Task<User> GetUserIncludingAuthorsAndJobAssignmentsAsync()
        {
            var user = await GetUserForDocumentCreationAsync();
            foreach (var author in user.Authors.Select(a => a.Author).Prepend(user.Self))
            {
                await _authorRepository.LoadJobAssignmentsAsync(author);
            }

            foreach (var author in user.Superiors.Select(a => a.Author).Prepend(user.Self))
            {
                await _authorRepository.LoadJobAssignmentsAsync(author);
            }

            return user;
        }

        public bool AddAuthorToUser(User user, Author author)
        {
            if (user.SelfId == author.Id)
            {
                return false;
            }

            bool added = true;
            using (var context = GetContext())
            {
                // check if there is already a existing Author, recycle or add it
                var dbAuthor = context.Authors.IgnoreQueryFilters().FirstOrDefault(a => a.Id == author.Id);
                if (dbAuthor == null)
                {
                    foreach (var workplace in author.Workplaces)
                    {
                        workplace.ClientModification = true;
                        context.Workplaces.AddOrUpdate(ShallowClone.Clone(workplace));
                    }

                    foreach (var jobFunction in author.Functions)
                    {
                        jobFunction.ClientModification = true;
                        context.JobFunctions.AddOrUpdate(ShallowClone.Clone(jobFunction));
                    }

                    foreach (var jobAssignment in author.JobAssignments)
                    {
                        jobAssignment.ClientModification = true;
                        context.JobAssignments.AddOrUpdate(ShallowClone.Clone(jobAssignment));
                        foreach (var jobFunction in jobAssignment.Functions)
                        {
                            jobFunction.ClientModification = true;
                            context.JobFunctions.AddOrUpdate(ShallowClone.Clone(jobFunction));
                        }
                    }

                    context.Authors.Add(ShallowClone.Clone(author));
                }
                else if (dbAuthor.Deleted)
                {
                    dbAuthor.SetDeletedFlag(ModificationType.ClientModification, false);
                }

                // check if there is already a existing UserAuthor relation, recycle or add it
                var dbUserAuthor = context.UserAuthors.IgnoreQueryFilters().FirstOrDefault(ua => ua.UserId == user.Id && ua.AuthorId == author.Id);
                if (dbUserAuthor == null)
                {
                    var ua = new UserAuthor { UserId = user.Id, AuthorId = author.Id };
                    ua.Init();
                    ua.SetModification(ModificationType.ClientModification);
                    context.UserAuthors.AddOrUpdate(ua);
                }
                else if (dbUserAuthor.Deleted)
                {
                    dbUserAuthor.SetDeletedFlag(ModificationType.ClientModification, false);
                }
                else
                {
                    added = false;
                }

                context.SaveChanges();
            }

            return added;
        }

        public void DeleteAuthorFromUser(Guid userId, Author author)
        {
            if (author == null)
            {
                return;
            }

            using (var context = GetContext())
            {
                foreach (var dbUserAuthor in context.UserAuthors.Where(ua => ua.UserId == userId && ua.AuthorId == author.Id))
                {
                    dbUserAuthor.SetDeletedFlag(ModificationType.ClientModification);
                }

                context.SaveChanges();
            }
        }

        public UserSettings GetUserSettings()
        {
            string login = _userIdentity.GetUserName();
            using (var context = GetContext())
            {
                var userSettingsId = context.Users.Where(u => u.Login == login).Select(u => u.SettingsId).FirstOrDefault();
                return context.UserSettings
                              .Include(us => us.ColorScheme)
                              .Include(us => us.DocumentModelSettings)
                              .FirstOrDefault(us => us.Id == userSettingsId);
            }
        }

        public async Task<UserSettings> GetUserSettingsAsync()
        {
            string login = _userIdentity.GetUserName();
            using (var context = GetContext())
            {
                var userSettingsId = await context.Users.Where(u => u.Login == login).Select(u => u.SettingsId).FirstOrDefaultAsync();
                return await context.UserSettings.FirstOrDefaultAsync(us => us.Id == userSettingsId);
            }
        }

        public UserSettings UpdateUserSettings(UserSettings settings)
        {
            return UpdateUserSettings(settings, ModificationType.ClientModification);
        }

        public bool IsSuperior(User user, Author author)
        {
            using var context = GetContext();
            return context.UserSuperiors.Any(x => x.UserId == user.Id && x.AuthorId == author.Id);
        }

        private static readonly object DeviceSettingsLock = new object();

        /// <summary>
        /// Gets or creates DeviceSettings for the current user and the current device.
        /// Return null of the user does not exist.
        /// </summary>
        /// <returns>Returns the <see cref="DeviceSettings"/> of the current device.</returns>
        public DeviceSettings GetOrCreateCurrentDeviceSettings()
        {
            // The following code is not thread-safe, so lock it.
            // The method is called multiple times in parallel during addin start.
            lock (DeviceSettingsLock)
            {
                var user = GetUser();
                if (user == null)
                {
                    return null;
                }

                using (var context = GetContext())
                {
                    string deviceName = Environment.MachineName;
                    var deviceSettings = context.DeviceSettings.FirstOrDefault(ds => ds.UserSettingsId == user.SettingsId && ds.DeviceName == deviceName);

                    if (deviceSettings == null)
                    {
                        deviceSettings = new DeviceSettings();
                        deviceSettings.Init();
                        deviceSettings.ClientModification = true;
                        deviceSettings.UserSettingsId = user.SettingsId;
                        deviceSettings.DeviceName = deviceName;

                        context.DeviceSettings.Add(deviceSettings);
                        context.SaveChanges();
                    }

                    return deviceSettings;
                }
            }
        }

        public void UpdateDeviceSettings(DeviceSettings deviceSettings)
        {
            using (var context = GetContext())
            {
                Update(context.DeviceSettings, deviceSettings, ModificationType.ClientModification);
                context.SaveChanges();
            }
        }

        public async Task<User> GetUserForManagerOverviewAsync(string login)
        {
            using (var context = GetContext())
            {
                var result = await context.Users
                                          .Include(user => user.Self).ThenInclude(self => self.OrgaEntity)
                                          .Include(user => user.Self).ThenInclude(self => self.PredefinedFunction)
                                          .Include(user => user.Self).ThenInclude(self => self.Functions)
                                          .Include(user => user.Self).ThenInclude(self => self.JobAssignments).ThenInclude(jobAssignment => jobAssignment.PredefinedFunction)
                                          .Include(user => user.Self).ThenInclude(self => self.JobAssignments).ThenInclude(jobAssignment => jobAssignment.Functions)
                                          .Include(user => user.Authors).ThenInclude(userAuthor => userAuthor.Author).ThenInclude(author => author.OrgaEntity)
                                          .Include(user => user.Authors).ThenInclude(userAuthor => userAuthor.Author).ThenInclude(author => author.PredefinedFunction)
                                          .Include(user => user.Authors).ThenInclude(userAuthor => userAuthor.Author).ThenInclude(author => author.Functions)
                                          .Include(user => user.Authors).ThenInclude(userAuthor => userAuthor.Author).ThenInclude(author => author.JobAssignments).ThenInclude(jobAssignment => jobAssignment.PredefinedFunction)
                                          .Include(user => user.Authors).ThenInclude(userAuthor => userAuthor.Author).ThenInclude(author => author.JobAssignments).ThenInclude(jobAssignment => jobAssignment.Functions)
                                          .Include(user => user.Superiors).ThenInclude(superior => superior.Author).ThenInclude(author => author.OrgaEntity)
                                          .Include(user => user.Superiors).ThenInclude(superior => superior.Author).ThenInclude(author => author.PredefinedFunction)
                                          .Include(user => user.Superiors).ThenInclude(superior => superior.Author).ThenInclude(author => author.Functions)
                                          .Include(user => user.Superiors).ThenInclude(superior => superior.Author).ThenInclude(author => author.JobAssignments).ThenInclude(jobAssignment => jobAssignment.PredefinedFunction)
                                          .Include(user => user.Superiors).ThenInclude(superior => superior.Author).ThenInclude(author => author.JobAssignments).ThenInclude(jobAssignment => jobAssignment.Functions)
                                          .FirstOrDefaultAsync(x => x.Login == login);

                result.Authors.RemoveWhere(x => x.AuthorId == null);
                result.Superiors.RemoveWhere(x => x.AuthorId == null);
                return result;
            }
        }

        public Task<List<UserGroup>> GetUserGroups(Expression<Func<UserGroup, bool>> criteria)
        {
            using (var context = GetContext())
            {
                return context.UserGroups.Where(criteria).ToListAsync();
            }
        }

        // ReSharper disable once InconsistentNaming
        public async Task UpdateCustomerExperienceProgramAsync(bool isEnabled)
        {
            await using var context = GetContext();
            string userName = _userIdentity.GetUserName();
            var userSettings = await context.Users
                                            .Where(u => u.Login == userName)
                                            .Select(u => u.Settings)
                                            .FirstOrDefaultAsync();

            if (userSettings != null)
            {
                userSettings.IsCepEnabled = isEnabled;
                userSettings.SetModification(ModificationType.ClientModification);
                await context.SaveChangesAsync();
            }
        }

        public async Task UpdateShowWelcomeWindowShownAsync()
        {
            await using var context = GetContext();
            string userName = _userIdentity.GetUserName();
            var userSettings = await context.Users
                                            .Where(u => u.Login == userName)
                                            .Select(u => u.Settings)
                                            .FirstOrDefaultAsync();

            if (userSettings != null)
            {
                userSettings.WelcomeWindowShown = DateTime.UtcNow;
                userSettings.SetModification(ModificationType.ClientModification);
                await context.SaveChangesAsync();
            }
        }
    }
}
