﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.PerformanceLog;
using Eurolook.AddIn.Common.UserConfiguration;
using Eurolook.Common.Log;
using Eurolook.Data;
using Eurolook.Data.ActionLogs;
using Eurolook.Data.Models;
using Eurolook.DataSync;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Eurolook.AddIn.Common.Database
{
    public class StatisticsRepository : EurolookClientDatabase, IStatisticsRepository
    {
        private readonly IUserDataRepository _userDataRepository;
        private readonly IUserIdentity _userIdentity;

        public StatisticsRepository(IUserDataRepository userDataRepository, IUserIdentity userIdentity)
        {
            _userDataRepository = userDataRepository;
            _userIdentity = userIdentity;
        }

        public Task LogDocumentCreationAsync(DocumentModel documentModel, DocumentCreationLog info)
        {
            return LogActionAsync(DocumentCreationLog.ActionId, documentModel, info);
        }

        public Task LogDocumentFromTemplateStoreCreationAsync(DocumentModel documentModel, TemplateStoreDocumentCreationLog info)
        {
            return LogActionAsync(TemplateStoreDocumentCreationLog.ActionId, documentModel, info);
        }

        public Task LogDocumentConversionAsync(DocumentModel documentModel, DocumentConversionLog info)
        {
            return LogActionAsync(DocumentConversionLog.ActionId, documentModel, info);
        }

        public Task LogBrickInsertionAsync(DocumentModel documentModel, BrickInsertionLog info)
        {
            return LogActionAsync(BrickInsertionLog.ActionId, documentModel, info);
        }

        public Task LogToolCommandExecution(DocumentModel documentModel, ToolCommandExecutionLog info)
        {
            return LogActionAsync(ToolCommandExecutionLog.ActionId, documentModel, info);
        }

        public Task LogApplyStyleAsync(string commandId, DocumentModel documentModel, ApplyStyleLog info)
        {
            return LogActionAsync(commandId, documentModel, info);
        }

        public async Task IncreaseSessionCountAsync()
        {
            try
            {
                var todayUtc = DateTime.UtcNow.Date;
                using (var context = GetContext())
                {
                    // load the user
                    string userName = _userIdentity.GetUserName();
                    var user = await context.Users
                                            .Where(u => u.Login == userName)
                                            .Include(u => u.Self)
                                            .Include(u => u.Settings.DeviceSettings)
                                            .FirstOrDefaultAsync();

                    // no valid user or no CEP enabled: exit
                    if (user == null || user.Self.OrgaEntityId == null || !user.Settings.IsCepEnabled)
                    {
                        return;
                    }

                    // get or create a track
                    var todaysTrack = await context.ActivityTracks.FirstOrDefaultAsync(
                                          u => u.DateUtc == todayUtc && u.OrgaEntityId == user.Self.OrgaEntityId)
                                      ?? new ActivityTrack
                                      {
                                          DateUtc = todayUtc,
                                          OrgaEntityId = user.Self.OrgaEntityId.Value,
                                      };

                    // increase the session count and save it
                    todaysTrack.SessionCount += 1;
                    if (context.ActivityTracks.Any(x => x.Id == todaysTrack.Id))
                    {
                        context.ActivityTracks.Update(todaysTrack);
                    }
                    else
                    {
                        context.ActivityTracks.Add(todaysTrack);
                    }

                    // set an active month for this device
                    string deviceName = DeviceInfoRetriever.GetDeviceName();
                    var deviceSettings = user.Settings.DeviceSettings.FirstOrDefault(x => x.DeviceName == deviceName);
                    if (deviceSettings != null)
                    {
                        var thisMonth = new DateTime(todayUtc.Year, todayUtc.Month, 1);
                        var track = deviceSettings.DeserializeActivityTrack();
                        if (!track.Contains(thisMonth))
                        {
                            track.Add(thisMonth);
                            deviceSettings.SerializeActivityTrack(track);
                            deviceSettings.ClientModification = true;
                        }
                    }

                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        public async Task WritePerformanceLogAsync(
            PerformanceLogEvent logEvent,
            TimeSpan time,
            IPerformanceLogInfoProvider info)
        {
            var eventName = logEvent.ToString();
            var date = DateTime.UtcNow;
            var clientVersion = info.ClientVersion.ToString();
            var deviceId = info.DeviceSettings?.Id;
            var deviceName = info.DeviceSettings?.DeviceName;
            using (var context = GetContext())
            {
                await context.PerformanceLogs.AddAsync(
                    new Data.Models.PerformanceLog
                    {
                        Event = eventName,
                        Date = date,
                        Version = clientVersion,
                        DeviceSettingsId = deviceId,
                        DeviceName = deviceName,
                        Total = time,
                    });
                await context.SaveChangesAsync();
            }
        }

        private async Task LogActionAsync(string action, DocumentModel documentModel, object info)
        {
            var user = _userDataRepository.GetUserForLogging();
            if (user == null || user.Self == null || user.Settings == null)
            {
                return;
            }

            if (!user.Settings.IsCepEnabled)
            {
                return;
            }

            using (var context = GetContext())
            {
                context.ActionLogs.Add(
                    new ActionLog
                    {
                        Action = action,
                        OriginatorId = user.Self.OrgaEntityId,
                        DocumentModelId = documentModel.Id,
                        Info = JsonConvert.SerializeObject(info),
                    });
                await context.SaveChangesAsync();
            }
        }
    }
}
