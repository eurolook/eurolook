﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Eurolook.AddIn.Common.Database
{
    public class NotificationRepository : EurolookClientDatabase, INotificationRepository
    {
        public Notification[] GetActiveNotifications(INotificationCompatibilityChecker compatibilityChecker)
        {
            using var context = GetContext();
            var now = DateTime.UtcNow;
            var notifications = context.Notifications
                                       .Where(x => !x.IsHidden && now > x.FromDate && now < x.ToDate)
                                       .OrderByDescending(x => x.FromDate)
                                       .ToHashSet();
            notifications.RemoveWhere(x => !compatibilityChecker.IsCompatibleWithCurrentVersion(x));
            return notifications.ToArray();
        }

        public Task<int> GetHiddenNotificationsCountAsync()
        {
            using var context = GetContext();
            var now = DateTime.UtcNow;
            return context.Notifications.CountAsync(x => x.IsHidden && now > x.FromDate && now < x.ToDate);
        }

        public void RestoreActiveNotifications()
        {
            using var context = GetContext();
            var now = DateTime.UtcNow;
            foreach (var notification in context.Notifications.Where(x => x.IsHidden && now > x.FromDate && now < x.ToDate))
            {
                notification.IsHidden = false;
            }

            context.SaveChanges();
        }

        public void TryClearNotification(Guid referenceId)
        {
            using var context = GetContext();
            var notification = context.Notifications.FirstOrDefault(x => x.Id == referenceId);
            if (notification != null)
            {
                notification.IsHidden = true;
                context.SaveChanges();
            }
        }
    }
}
