﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using NotNullAttribute = JetBrains.Annotations.NotNullAttribute;

namespace Eurolook.AddIn.Common.Database
{
    public class TextsRepository : EurolookClientDatabase, ITextsRepository
    {
        private readonly ILanguageRepository _languageRepository;

        public TextsRepository(ILanguageRepository languageRepository)
        {
            _languageRepository = languageRepository;
        }

        public void LoadOrgaEntityTexts(OrgaEntity orgaEntity, Language language, Language fallback = null)
        {
            var fallbackLanguage = fallback ?? _languageRepository.GetEnglishLanguage();
            while (orgaEntity != null)
            {
                LoadTexts(orgaEntity, language, fallbackLanguage, false);
                orgaEntity.PrimaryAddress = GetAddress(orgaEntity.PrimaryAddressId);
                LoadTexts(orgaEntity.PrimaryAddress, language, fallbackLanguage, false);
                orgaEntity.SecondaryAddress = GetAddress(orgaEntity.SecondaryAddressId);
                LoadTexts(orgaEntity.SecondaryAddress, language, fallbackLanguage, false);
                orgaEntity = orgaEntity.SuperEntity;
            }
        }

        [CanBeNull]
        public Translation GetTranslation(string alias, [CanBeNull] Language lang)
        {
            using var context = GetContext();
            if (lang != null)
            {
                var result = context.Translations.AsNoTracking().FirstOrDefault(
                    t => t.Language.Name == lang.Name && t.Text.Alias == alias);
                if (result != null)
                {
                    return result;
                }
            }

            var en = _languageRepository.GetEnglishLanguage(context);
            return context.Translations.AsNoTracking().FirstOrDefault(
                t => t.Language.Name == en.Name && t.Text.Alias == alias);
        }

        [CanBeNull]
        public async Task<Translation> GetTranslationAsync(string alias, [CanBeNull] Language lang)
        {
            using var context = GetContext();
            if (lang != null)
            {
                var result = await context.Translations.AsNoTracking().FirstOrDefaultAsync(
                    t => t.Language.Name == lang.Name && t.Text.Alias == alias);
                if (result != null)
                {
                    return result;
                }
            }

            var en = _languageRepository.GetEnglishLanguage(context);
            return await context.Translations.AsNoTracking().FirstOrDefaultAsync(
                t => t.Language.Name == en.Name && t.Text.Alias == alias);
        }

        public async Task<Translation> GetTranslationAsync(Guid textId, Language lang, bool fallbackToEnglish = true)
        {
            using var context = GetContext();
            var result = await context.Translations.AsNoTracking().FirstOrDefaultAsync(t => t.TextId == textId && t.LanguageId == lang.Id);
            if (result == null && fallbackToEnglish)
            {
                var en = _languageRepository.GetEnglishLanguage(context);
                result = await context.Translations.AsNoTracking().FirstOrDefaultAsync(t => t.TextId == textId && t.LanguageId == en.Id);
            }

            return result;
        }

        [NotNull]
        public async Task<List<Translation>> GetTranslationsAsync(string alias)
        {
            using var context = GetContext();
            return await context.Translations.AsNoTracking().Where(t => t.Text.Alias == alias).ToListAsync();
        }

        public Task<Text[]> GetBrickTexts(Guid brickId)
        {
            using var context = GetContext();
            return context.BrickTexts
                          .Where(x => x.BrickId == brickId)
                          .Include(x => x.Text)
                          .Select(x => x.Text)
                          .ToArrayAsync();
        }

        [SuppressMessage(
        "StyleCop.CSharp.ReadabilityRules",
        "SA1102:Query clause must follow previous clause",
        Justification = "The current formatting gives better readability")]
        public IEnumerable<Translation> GetTranslations(DocumentModel model, Language lang)
        {
            using (var context = GetContext())
            {
                var en = _languageRepository.GetEnglishLanguage(context);

                var brickIds = context.DocumentStructure.AsNoTracking()
                                      .Where(x => x.DocumentModelId == model.Id)
                                      .Select(x => x.BrickId).Distinct().ToArray();

                var texts = context.BrickTexts.AsNoTracking()
                                   .Where(x => brickIds.Contains(x.BrickId))
                                   .Select(x => x.Text).Distinct().ToArray();

                var textIds = texts.Select(x => x.Id).Distinct().ToArray();

                var langTranslations = context.Translations.AsNoTracking()
                                              .Where(x => textIds.Contains(x.TextId) && x.LanguageId == lang.Id)
                                              .ToArray();

                var enTranslations = context.Translations.AsNoTracking()
                                            .Where(x => textIds.Contains(x.TextId) && x.LanguageId == en.Id)
                                            .ToArray();

                var result = new List<Translation>();
                foreach (var textId in textIds)
                {
                    var translation = langTranslations.FirstOrDefault(x => x.TextId == textId)
                                      ?? enTranslations.FirstOrDefault(x => x.TextId == textId);
                    if (translation != null)
                    {
                        translation.Text = texts.FirstOrDefault(x => x.Id == textId);
                        result.Add(translation);
                    }
                }

                return result;
            }
        }
    }
}
