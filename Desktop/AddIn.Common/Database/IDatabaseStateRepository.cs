using System;
using Eurolook.Data.Models;

namespace Eurolook.AddIn.Common.Database
{
    public interface IDatabaseStateRepository
    {
        string GetFilePath();

        DatabaseState GetDatabaseState();

        string GetProductCustomizationId();

        Version GetClientVersion();

        DateTime? GetLastUpdate();

        DateTime? GetLastDataSyncRunDate();

        DateTime? GetLastSuccessfulDataSyncDate();

        bool EnsureDeleted();

        bool EnsureCreated();
    }
}
