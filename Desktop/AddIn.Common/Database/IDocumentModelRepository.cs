using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data.Compatibility;
using Eurolook.Data.Models;

namespace Eurolook.AddIn.Common.Database
{
    public interface IDocumentModelRepository
    {
        DocumentModel GetDocumentModel(Guid id);

        DocumentModel GetDocumentModel(string el4DocType);

        Task<List<DocumentModel>> GetVisibleDocumentModelsAsync(
            IDocumentModelVersionCompatibilityTester documentModelVersionCompatibilityTester,
            IBrickVersionCompatibilityTester brickVersionCompatibilityTester,
            bool loadHidden = false);

        void UpdateDocumentModelSettings(DocumentModelSettings docModelSettings);
    }
}
