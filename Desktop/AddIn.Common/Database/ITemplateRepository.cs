﻿using System;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.StandaloneMode;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;

namespace Eurolook.AddIn.Common.Database
{
    public interface ITemplateRepository
    {
        Task SaveTemplateDownload(Template template);

        Task<Template> GetTemplateAsync(Guid templateId);

        TemplateFile GetTemplateFile(Guid templateId, Language language);

        void MakeUserTemplateAvailableOffline(Guid userTemplateId);

        void RestoreTemplatesFromBackup(User user, TemplateDataBackupXml templateDataBackup);

        bool IsTemplateAvailableOffline(Guid? templateId);
    }
}
