﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.Authors;
using Eurolook.AddIn.Common.StandaloneMode;
using Eurolook.AddIn.Common.UserConfiguration;
using Eurolook.Data;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.Data.Models.AuthorRoles;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace Eurolook.AddIn.Common.Database
{
    public class AuthorRepository : EurolookClientDatabase, IAuthorRepository
    {
        private readonly ITextsRepository _textsRepository;
        private readonly ILanguageRepository _languageRepository;
        private readonly IUserIdentity _userIdentity;

        public AuthorRepository(ITextsRepository textsRepository, ILanguageRepository languageRepository, IUserIdentity userIdentity)
        {
            _textsRepository = textsRepository;
            _languageRepository = languageRepository;
            _userIdentity = userIdentity;
        }

        /// <summary>
        /// Loads the orga tree recursively into the given ObservableCollection.
        /// </summary>
        /// <param name="orgaTree">The target view model property.</param>
        /// <param name="language">An optional language to load the header translations. When null, no translations are loaded.</param>
        public async Task LoadOrgaTreeAsync(ICollection<OrgaEntityViewModel> orgaTree, Language language = null)
        {
            if (orgaTree == null)
            {
                return;
            }

            // load data from db
            // for SQL CE it is actually faster to load all translations at once and filter in memory (see GetViewModel)
            // than loading them one by one.
            OrgaEntity[] entities;
            Translation[] translations = null;
            using (var context = GetContext())
            {
                entities = await context.OrgaEntities.Include(o => o.HeaderText).ToArrayAsync();
                if (language != null)
                {
                    translations = await context.Translations.Where(t => t.LanguageId == language.Id).ToArrayAsync();
                }
            }

            // build the tree
            foreach (var level1Entity in entities.Where(x => x.LogicalLevel == 1).OrderBy(o => o.Name))
            {
                var level1EntityVm = GetViewModel(level1Entity, orgaTree, translations);
                foreach (var level2Entity in entities.Where(x => x.SuperEntityId == level1Entity.Id)
                                                     .OrderBy(o => o.OrderIndex))
                {
                    var level2EntityVm = GetViewModel(level2Entity, level1EntityVm.SubEntities, translations);
                    level2EntityVm.ParentEntityVm = level1EntityVm;
                    foreach (var level3Entity in entities.Where(x => x.SuperEntityId == level2Entity.Id)
                                                         .OrderBy(o => o.OrderIndex))
                    {
                        var level3EntityVm = GetViewModel(level3Entity, level2EntityVm.SubEntities, translations);
                        level3EntityVm.ParentEntityVm = level2EntityVm;
                    }
                }
            }
        }

        public async Task LoadOrgaTreeTranslationsAsync(
            OrgaEntityViewModel entity,
            Language language,
            bool goDeep = true)
        {
            using (var context = GetContext())
            {
                await LoadOrgaTreeTranslationsAsync(context, entity, language, goDeep);
            }
        }

        [CanBeNull]
        public Author GetAuthorForDocumentCreation(Guid authorId, Language language)
        {
            using (var context = GetContext())
            {
                var author = context.Authors.AsNoTracking().Where(a => a.Id == authorId)
                                    .Include(a => a.PredefinedFunction)
                                    .Include(a => a.Functions).ThenInclude(f => f.Language)
                                    .Include(a => a.Workplaces).ThenInclude(wp => wp.Address)
                                    .Include(a => a.OrgaEntity).ThenInclude(x => x.SuperEntity).ThenInclude(x => x.SuperEntity).ThenInclude(x => x.SuperEntity)
                                    .Include(a => a.OrgaEntity).ThenInclude(x => x.PrimaryAddress)
                                    .Include(a => a.OrgaEntity).ThenInclude(x => x.SecondaryAddress)
                                    .FirstOrDefault();
                if (author == null)
                {
                    return null;
                }

                LoadTranslationsForAuthor(author, language, context);

                return author;
            }
        }

        [CanBeNull]
        public Author GetAuthorForDocumentCreation(Guid authorId, Language language, Guid fallbackId)
        {
            return this.GetAuthorForDocumentCreation(authorId, language) ?? this.GetAuthorForDocumentCreation(fallbackId, language);
        }

        public JobAssignment GetJobAssignmentForDocumentCreation(Guid id, Language language)
        {
            using (var context = GetContext())
            {
                var jobAssignment = context.JobAssignments.
                                            Where(j => j.Id == id).
                                            Include(a => a.PredefinedFunction).
                                            Include(a => a.Functions).ThenInclude(f => f.Language).
                                            Include(a => a.OrgaEntity).ThenInclude(x => x.SuperEntity).ThenInclude(x => x.SuperEntity).
                                            Include(a => a.OrgaEntity).ThenInclude(x => x.PrimaryAddress).
                                            Include(a => a.OrgaEntity).ThenInclude(x => x.SecondaryAddress).
                                            FirstOrDefault();
                if (jobAssignment != null)
                {
                    var fallbackLanguage = _languageRepository.GetEnglishLanguage(context);
                    _textsRepository.LoadOrgaEntityTexts(jobAssignment.OrgaEntity, language, fallbackLanguage);
                    LoadTexts(jobAssignment.PredefinedFunction, language, fallbackLanguage);
                }

                return jobAssignment;
            }
        }

        public async Task<Author> GetAuthorForEditorAsync(Guid authorId)
        {
            using var context = GetContext();
            return await context.Authors
                                .Include(x => x.PredefinedFunction)
                                .Include(x => x.Functions)
                                .Include(x => x.JobAssignments).ThenInclude(x => x.Functions)
                                .Include(x => x.JobAssignments).ThenInclude(x => x.PredefinedFunction)
                                .Include(x => x.Workplaces).ThenInclude(x => x.Address)
                                .FirstOrDefaultAsync(x => x.Id == authorId);
        }

        public void UpdateAuthorFromEditor(Author author)
        {
            using (var context = GetContext())
            {
                var dbAuthor = context.Authors.FirstOrDefault(x => x.Id == author.Id);
                if (dbAuthor != null)
                {
                    dbAuthor.LatinFirstName = author.LatinFirstName;
                    dbAuthor.LatinLastName = author.LatinLastName;
                    dbAuthor.GreekFirstName = author.GreekFirstName;
                    dbAuthor.GreekLastName = author.GreekLastName;
                    dbAuthor.BulgarianFirstName = author.BulgarianFirstName;
                    dbAuthor.BulgarianLastName = author.BulgarianLastName;
                    dbAuthor.Initials = author.Initials;
                    dbAuthor.Email = author.Email;
                    dbAuthor.Gender = author.Gender;
                    dbAuthor.SetModification(ModificationType.ClientModification);
                    context.SaveChanges();
                }
            }
        }

        public Address[] GetAddresses()
        {
            using var context = GetContext();
            return context.Addresses.ToArray();
        }

        public Workplace[] GetWorkplaces(Guid authorId)
        {
            using (var context = GetContext())
            {
                return context.Workplaces
                              .Include(x => x.Address)
                              .Where(x => x.AuthorId == authorId)
                              .ToArray();
            }
        }

        public void UpdateWorkplace(Workplace workplace)
        {
            using (var context = GetContext())
            {
                var dbWorkplace = context.Workplaces.FirstOrDefault(x => x.Id == workplace.Id);
                if (dbWorkplace != null)
                {
                    dbWorkplace.Office = workplace.Office;
                    dbWorkplace.PhoneExtension = workplace.PhoneExtension;
                    dbWorkplace.FaxExtension = workplace.FaxExtension;
                    dbWorkplace.AddressId = workplace.AddressId;
                    dbWorkplace.SetModification(ModificationType.ClientModification);
                    context.SaveChanges();
                }
            }
        }

        public async Task<Author> GetMainJobAsync(Guid authorId)
        {
            using (var context = GetContext())
            {
                return await context.Authors
                                    .Include(x => x.OrgaEntity)
                                    .Include(x => x.PredefinedFunction)
                                    .Include(x => x.Functions)
                                    .Where(x => x.Id == authorId)
                                    .FirstOrDefaultAsync();
            }
        }

        public async Task<JobAssignment[]> GetJobsAsync(Guid authorId)
        {
            using (var context = GetContext())
            {
                return await context.JobAssignments
                                    .Include(x => x.OrgaEntity)
                                    .Include(x => x.PredefinedFunction)
                                    .Include(x => x.Functions)
                                    .Where(x => x.AuthorId == authorId)
                                    .ToArrayAsync();
            }
        }

        public async Task<PredefinedFunction[]> GetPredefinedFunctionsAsync()
        {
            using var context = GetContext();
            return await context.PredefinedFunctions.ToArrayAsync();
        }

        public void CreateJobAssignment(IJobAssignment jobAssignment)
        {
            using (var context = GetContext())
            {
                if (jobAssignment is JobAssignment job)
                {
                    job.SetModification(ModificationType.ClientModification);
                    context.JobAssignments.Add(ShallowClone.Clone(job));
                    SaveJobFunctions(job, context);
                    context.SaveChanges();
                }
            }
        }

        public void DeleteJobAssignment(IJobAssignment jobAssignment)
        {
            using (var context = GetContext())
            {
                var dbJob = context.JobAssignments
                                   .IgnoreQueryFilters()
                                   .Include(x => x.Functions)
                                   .FirstOrDefault(x => x.Id == jobAssignment.Id);

                if (dbJob != null)
                {
                    foreach (var dbFunction in dbJob.Functions)
                    {
                        dbFunction.SetDeletedFlag(ModificationType.ClientModification);
                    }

                    dbJob.SetDeletedFlag(ModificationType.ClientModification);
                    context.SaveChanges();
                }
            }
        }

        public void UpdateMainJob(IJobAssignment author)
        {
            using (var context = GetContext())
            {
                var dbAuthor = context.Authors.FirstOrDefault(x => x.Id == author.Id);
                if (dbAuthor != null)
                {
                    dbAuthor.OrgaEntityId = author.OrgaEntityId;
                    dbAuthor.Service = author.Service;
                    dbAuthor.WebAddress = author.WebAddress;
                    dbAuthor.FunctionalMailbox = author.FunctionalMailbox;
                    dbAuthor.PredefinedFunctionId = author.PredefinedFunctionId;
                    SaveJobFunctions(author, context);
                    dbAuthor.SetModification(ModificationType.ClientModification);
                    context.SaveChanges();
                }
            }
        }

        public void UpdateJobAssignment(IJobAssignment job)
        {
            using (var context = GetContext())
            {
                var dbJobAssignment = context.JobAssignments.FirstOrDefault(x => x.Id == job.Id);
                if (dbJobAssignment != null)
                {
                    dbJobAssignment.OrgaEntityId = job.OrgaEntityId;
                    dbJobAssignment.Service = job.Service;
                    dbJobAssignment.WebAddress = job.WebAddress;
                    dbJobAssignment.FunctionalMailbox = job.FunctionalMailbox;
                    dbJobAssignment.PredefinedFunctionId = job.PredefinedFunctionId;
                    dbJobAssignment.SetModification(ModificationType.ClientModification);
                    SaveJobFunctions(job, context);
                    context.SaveChanges();
                }
            }
        }

        public Author GetAuthor(Guid id)
        {
            using (var context = GetContext())
            {
                return context.Authors.FirstOrDefault(a => a.Id == id);
            }
        }

        public Author UpdateAuthor(Author author)
        {
            return UpdateAuthor(author, ModificationType.ClientModification);
        }

        public async Task<List<DocumentModelAuthorRole>> GetAuthorRolesAsync(Guid documentModelId)
        {
            using (var context = GetContext())
            {
                return await context.DocumentModelAuthorRoles
                                    .AsNoTracking()
                                    .Where(x => x.DocumentModelId == documentModelId)
                                    .Include(x => x.AuthorRole)
                                    .ToListAsync();
            }
        }

        public async Task LoadJobAssignmentsAsync(Author author)
        {
            using (var context = GetContext())
            {
                author.JobAssignments = new HashSet<JobAssignment>();
                foreach (var jobAssignment in await context.JobAssignments.AsNoTracking()
                                                           .Include(j => j.Functions)
                                                           .Include(j => j.OrgaEntity)
                                                           .Include(j => j.PredefinedFunction)
                                                           .Where(j => j.AuthorId == author.Id)
                                                           .ToArrayAsync())
                {
                    author.JobAssignments.Add(jobAssignment);
                }
            }
        }

        public void LoadTranslationsForAuthor(Author author, Language language, EurolookClientContext dbContext = null)
        {
            using (var context = dbContext ?? GetContext())
            {
                var fallbackLanguage = _languageRepository.GetEnglishLanguage(context);
                LoadPredefinedFunctionForAuthor(author.PredefinedFunction, language, fallbackLanguage, context);
                _textsRepository.LoadOrgaEntityTexts(author.OrgaEntity, language, fallbackLanguage);
                foreach (var workplace in author.Workplaces)
                {
                    LoadTexts(workplace.Address, language, fallbackLanguage);
                }
            }
        }

        public void RestoreAuthorDataFromBackup(User user, AuthorDataBackupXml authorDataBackup)
        {
            if (user == null)
            {
                return;
            }

            using (var context = GetContext())
            {
                var author = context.Authors.FirstOrDefault(x => x.Id == user.SelfId);
                if (author != null)
                {
                    var orgaEntity = context.OrgaEntities.FirstOrDefault(x => x.Name == authorDataBackup.OrgaEntity);
                    author.Initials = authorDataBackup.Initials;
                    author.LatinFirstName = authorDataBackup.LatinFirstName;
                    author.LatinLastName = authorDataBackup.LatinLastName;
                    author.OrgaEntityId = orgaEntity?.Id;
                    context.SaveChanges();
                }
            }
        }

        private async Task LoadOrgaTreeTranslationsAsync(
            EurolookContext context,
            OrgaEntityViewModel entity,
            Language language,
            bool goDeep = true)
        {
            if (entity == null)
            {
                return;
            }

            var translation = await context.Translations.FirstOrDefaultAsync(
                x => x.TextId == entity.OrgaEntity.HeaderTextId && x.LanguageId == language.Id);
            entity.HeaderText = translation?.Value;
            if (!goDeep)
            {
                return;
            }

            foreach (var subEntity in entity.SubEntities)
            {
                await LoadOrgaTreeTranslationsAsync(context, subEntity, language);
            }
        }

        private static OrgaEntityViewModel GetViewModel(
            OrgaEntity entity,
            ICollection<OrgaEntityViewModel> orgaTree,
            IEnumerable<Translation> translations)
        {
            entity.Header = translations?.FirstOrDefault(t => t.TextId == entity.HeaderTextId);
            var entityVm = new OrgaEntityViewModel(entity);
            orgaTree.Add(entityVm);
            return entityVm;
        }

        private void SaveJobFunctions(IJobAssignment job, EurolookClientContext context)
        {
            foreach (var jobFunction in job.Functions)
            {
                var dbFunction = context.JobFunctions.FirstOrDefault(x => x.Id == jobFunction.Id);
                if (dbFunction != null)
                {
                    dbFunction.Function = jobFunction.Function;
                    dbFunction.SetModification(ModificationType.ClientModification);
                }
                else
                {
                    jobFunction.SetModification(ModificationType.ClientModification);
                    context.JobFunctions.Add(jobFunction);
                }
            }
        }
    }
}
