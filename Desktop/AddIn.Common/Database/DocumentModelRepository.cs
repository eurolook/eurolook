﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.Compatibility;
using Eurolook.Data.Extensions;
using Eurolook.Data.Models;
using Eurolook.Data.Models.AuthorRoles;
using Eurolook.Data.Models.Metadata;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace Eurolook.AddIn.Common.Database
{
    public class DocumentModelRepository : EurolookClientDatabase, IDocumentModelRepository
    {
        public DocumentModel GetDocumentModel(Guid id)
        {
            using var context = GetContext();
            var documentModel = context.DocumentModels.AsNoTracking().FirstOrDefault(x => x.Id == id);
            LoadDocumentModelGraphAsync(context, documentModel, null).Wait();
            return documentModel;
        }

        [CanBeNull]
        public DocumentModel GetDocumentModel(string el4DocType)
        {
            using var context = GetContext();
            var documentModel = context.DocumentModels.AsNoTracking().FirstOrDefault(x => x.EL4DocType == el4DocType);
            LoadDocumentModelGraphAsync(context, documentModel, null).Wait();
            return documentModel;
        }

        public void UpdateDocumentModelSettings(DocumentModelSettings docModelSettings)
        {
            using var context = GetContext();
            context.DocumentModelSettings.AddOrUpdate(docModelSettings);
            context.SaveChanges();
        }

        public async Task<List<DocumentModel>> GetVisibleDocumentModelsAsync(
            IDocumentModelVersionCompatibilityTester documentModelVersionCompatibilityTester,
            IBrickVersionCompatibilityTester brickVersionCompatibilityTester,
            bool loadHidden = false)
        {
            await using var context = GetContext();
            // in this method data is only read, never written - so we make the context non-tracking
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            var query = context.DocumentModels.AsNoTracking().Where(x => !x.Deleted && x.IsHidden != true);
            if (loadHidden)
            {
                query = context.DocumentModels.AsNoTracking().Where(x => !x.Deleted);
            }

            var result = await query.Include(x => x.DocumentCategory).ToListAsync();
            result.RemoveAll(
                dm => !documentModelVersionCompatibilityTester.IsDocumentModelCompatibleWithAddinVersion(dm));

            await LoadDocumentModelGraphManyAsync(context, result, brickVersionCompatibilityTester);

            // don't show document models with no 'active' language or author role
            result.RemoveAll(x => !x.DocumentLanguages.Any());
            result.RemoveAll(x => !x.AuthorRoles.Any());

            return result;
        }

        private static async Task LoadDocumentModelGraphManyAsync(
            EurolookClientContext context,
            List<DocumentModel> documentModels,
            IBrickVersionCompatibilityTester brickVersionCompatibilityTester)
        {
            var dms = documentModels.Select(dm => dm.Id).ToList();

            var requiredBrickReferences = new HashSet<DocumentStructure>(
                await context.DocumentStructure.Where(x => dms.Contains(x.DocumentModelId))
                             .Include(x => x.Brick).ThenInclude(x => x.Group)
                             .ToListAsync());

            var requiredDocumentLanguages = new HashSet<DocumentModelLanguage>(
                await context.DocumentModelLanguages.Where(x => dms.Contains(x.DocumentModelId))
                             .Include(x => x.DocumentLanguage)
                             .ToListAsync());

            var requiredMetadataDefinitions = new HashSet<DocumentModelMetadataDefinition>(
                await context.DocumentModelMetadataDefinitions.Where(x => dms.Contains(x.DocumentModelId))
                             .Include(x => x.MetadataDefinition).ThenInclude(x => x.MetadataCategory)
                             .ToListAsync());

            var requiredAuthorRoles = new HashSet<DocumentModelAuthorRole>(
                await context.DocumentModelAuthorRoles.Where(x => dms.Contains(x.DocumentModelId))
                             .Include(x => x.AuthorRole)
                             .ToListAsync());

            foreach (var dm in documentModels)
            {
                var brickReferences = new HashSet<DocumentStructure>(requiredBrickReferences);
                brickReferences.ExceptWith(requiredBrickReferences.Where(x => x.DocumentModelId != dm.Id));

                // remove incompatible bricks
                if (brickVersionCompatibilityTester != null)
                {
                    brickReferences.RemoveWhere(
                        x => !brickVersionCompatibilityTester.IsBrickCompatibleWithAddinVersion(x.Brick));
                }

                var documentLanguages = new HashSet<DocumentModelLanguage>(requiredDocumentLanguages);
                documentLanguages.ExceptWith(requiredDocumentLanguages.Where(x => x.DocumentModelId != dm.Id));

                var metadataDefinitions = new HashSet<DocumentModelMetadataDefinition>(requiredMetadataDefinitions);
                metadataDefinitions.ExceptWith(requiredMetadataDefinitions.Where(x => x.DocumentModelId != dm.Id));

                var authorRoles = new HashSet<DocumentModelAuthorRole>(requiredAuthorRoles);
                authorRoles.ExceptWith(requiredAuthorRoles.Where(x => x.DocumentModelId != dm.Id));

                dm.BrickReferences = brickReferences;
                dm.DocumentLanguages = documentLanguages;
                dm.DocumentModelMetadataDefinitions = metadataDefinitions;
                dm.AuthorRoles = authorRoles;
            }
        }

        private static async Task LoadDocumentModelGraphAsync(
            EurolookClientContext context,
            DocumentModel dm,
            IBrickVersionCompatibilityTester brickVersionCompatibilityTester)
        {
            if (dm == null)
            {
                return;
            }

            dm.BrickReferences = new HashSet<DocumentStructure>(
                await context.DocumentStructure
                             .Where(x => x.DocumentModelId == dm.Id)
                             .Include(x => x.Brick).ThenInclude(x => x.Group)
                             .ToListAsync());

            // remove incompatible bricks
            if (brickVersionCompatibilityTester != null)
            {
                dm.BrickReferences.RemoveWhere(
                    x => !brickVersionCompatibilityTester.IsBrickCompatibleWithAddinVersion(x.Brick));
            }

            dm.DocumentLanguages = new HashSet<DocumentModelLanguage>(
                await context.DocumentModelLanguages.AsNoTracking()
                             .Where(x => x.DocumentModelId == dm.Id)
                             .Include(x => x.DocumentLanguage)
                             .ToListAsync());

            dm.DocumentModelMetadataDefinitions = new HashSet<DocumentModelMetadataDefinition>(
                await context.DocumentModelMetadataDefinitions.AsNoTracking()
                             .Where(x => x.DocumentModelId == dm.Id)
                             .Include(x => x.MetadataDefinition).ThenInclude(x => x.MetadataCategory)
                             .ToListAsync());

            dm.AuthorRoles = new HashSet<DocumentModelAuthorRole>(
                await context.DocumentModelAuthorRoles.AsNoTracking()
                             .Where(x => x.DocumentModelId == dm.Id)
                             .Include(x => x.AuthorRole)
                             .ToListAsync());
        }
    }
}
