﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.StandaloneMode;
using Eurolook.Data;
using Eurolook.Data.Extensions;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;
using Microsoft.EntityFrameworkCore;

namespace Eurolook.AddIn.Common.Database
{
    public class TemplateRepository : EurolookClientDatabase, ITemplateRepository
    {
        public async Task SaveTemplateDownload(Template template)
        {
            using (var context = GetContext())
            {
                // TODO: the owners are a problem because they might reference a UserId that doesn't exist in the local database
                // var owners = template.Owners != null ? template.Owners.ToArray() : new TemplateOwner[0];
                var publications = template.Publications != null
                    ? template.Publications.ToArray()
                    : Array.Empty<TemplatePublication>();

                var files = template.TemplateFiles != null
                    ? template.TemplateFiles.ToArray()
                    : Array.Empty<TemplateFile>();

                template.BaseDocumentModel = null;
                template.TemplateFiles = null;
                template.Publications = null;
                template.Owners = null;
                context.Templates.AddOrUpdate(template);

                foreach (var publication in publications)
                {
                    publication.Template = null;
                    publication.OrgaEntity = null;
                    context.TemplatePublications.AddOrUpdate(publication);
                }

                foreach (var templateFile in files)
                {
                    templateFile.Template = null;
                    templateFile.Language = null;
                    context.TemplateFiles.AddOrUpdate(templateFile);
                }

                await context.SaveChangesAsync();
            }
        }

        public async Task<Template> GetTemplateAsync(Guid templateId)
        {
            using var context = GetContext();
            return await context.Templates
                                .Include(t => t.BaseDocumentModel).ThenInclude(dm => dm.AuthorRoles).ThenInclude(ar => ar.AuthorRole)
                                .Include(t => t.TemplateFiles).ThenInclude(f => f.Language)
                                .Include(t => t.Publications).ThenInclude(p => p.OrgaEntity)
                                .FirstOrDefaultAsync(t => t.Id == templateId);
        }

        public TemplateFile GetTemplateFile(Guid templateId, Language language)
        {
            using var context = GetContext();
            return context.TemplateFiles.FirstOrDefault(x => x.TemplateId == templateId && x.LanguageId == language.Id);
        }

        public void MakeUserTemplateAvailableOffline(Guid userTemplateId)
        {
            using (var context = GetContext())
            {
                var userTemplate = context.UserTemplates.FirstOrDefault(x => x.Id == userTemplateId);
                if (userTemplate != null)
                {
                    userTemplate.IsOfflineAvailable = true;
                    userTemplate.SetModification(ModificationType.ClientModification);
                    context.SaveChanges();
                }
            }
        }

        public void RestoreTemplatesFromBackup(User user, TemplateDataBackupXml templateDataBackup)
        {
            if (user == null)
            {
                return;
            }

            using var context = GetContext();
            if (context.Templates.Any())
            {
                return;
            }

            context.Templates.AddRange(templateDataBackup.Templates);
            context.TemplateFiles.AddRange(templateDataBackup.TemplateFiles);

            foreach (var template in templateDataBackup.Templates)
            {
                var userTemplate = new UserTemplate
                {
                    UserId = user.Id,
                    TemplateId = template.Id,
                    IsOfflineAvailable = true,
                };
                userTemplate.Init();

                context.UserTemplates.Add(userTemplate);
            }

            context.SaveChanges();
        }

        public bool IsTemplateAvailableOffline(Guid? templateId)
        {
            using var context = GetContext();
            var userTemplate = context.UserTemplates.FirstOrDefault(x => x.TemplateId == templateId);
            return userTemplate is { IsOfflineAvailable: true };
        }
    }
}
