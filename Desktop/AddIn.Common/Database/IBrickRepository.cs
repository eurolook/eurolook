using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.Models;

namespace Eurolook.AddIn.Common.Database
{
    public interface IBrickRepository
    {
        Brick GetBrick(Guid id);

        Task<T> GetBrickSettingsAsync<T>(Guid userSettingsId, Guid brickId)
            where T : class;

        Task<T> GetBrickSettingsAsync<T>(EurolookClientContext context, Guid userSettingsId, Guid brickId)
            where T : class;

        BrickGroup GetBrickGroup(Guid id);

        Task<List<Brick>> GetBricksOfGroup(Guid groupId);

        BrickGroup GetBrickGroupForEdit(Guid id);

        int GetCommandBricksOfGroupCount(Guid groupId);

        List<CommandBrick> GetCommandBricksOfGroup(Guid groupId);

        List<UserBrick> GetUserBricks(DocumentModel model);

        void DeleteUserBrick(BuildingBlockBrick brick);

        Task SaveBrickSettings<T>(Guid userSettingsId, Guid brickId, T settingsObject);

        Task SaveBrickSettings<T>(
            EurolookClientContext context,
            Guid userSettingsId,
            Guid brickId,
            T settingsObject);
    }
}
