using System;

namespace Eurolook.AddIn.Common.Database
{
    public class PropertyUpdate
    {
        public string Name { get; set; }

        public string CurrentValue { get; set; }

        public Guid? CurrentValueId { get; set; }

        public string OldAdValue { get; set; }

        public Guid? OldAdValueId { get; set; }

        public string NewAdValue { get; set; }

        public Guid? NewAdValueId { get; set; }
    }
}
