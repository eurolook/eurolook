﻿using System;
using System.Drawing;
using System.IO;
using System.Reflection;
using Eurolook.Common.Log;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.Properties
{
    public class EmbeddedResourceManager : IEmbeddedResourceManager, ICanLog
    {
        [CanBeNull]
        public Bitmap GetEmbeddedResourceImage(Assembly assembly, string folder, string resourceId)
        {
            try
            {
                var resourceName = $"{assembly.GetName().Name}.{folder}.{resourceId}.png";
                using var stream = assembly.GetManifestResourceStream(resourceName);
                if (stream != null)
                {
                    return new Bitmap(stream);
                }
            }
            catch (Exception e)
            {
                this.LogError(e);
            }

            return null;
        }

        [CanBeNull]
        public string GetEmbeddedResourceText(Assembly assembly, string folder, string resourceName)
        {
            try
            {
                var fullResourceName = $"{assembly.GetName().Name}.{folder}.{resourceName}";
                using var stream = assembly.GetManifestResourceStream(fullResourceName);

                if (stream != null)
                {
                    using (var resourceReader = new StreamReader(stream))
                    {
                        return resourceReader.ReadToEnd();
                    }
                }
            }
            catch (Exception e)
            {
                this.LogError(e);
            }

            return null;
        }
    }
}
