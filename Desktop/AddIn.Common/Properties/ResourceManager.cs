﻿using System.Globalization;
using System.IO;

namespace Eurolook.AddIn.Common.Properties;

public class ResourceManager : IResourceManager
{
    public string GetString(string name)
    {
        return Resources.ResourceManager.GetString(name);
    }

    public string GetString(string name, CultureInfo culture)
    {
        return Resources.ResourceManager.GetString(name, culture);
    }

    public object GetObject(string name)
    {
        return Resources.ResourceManager.GetObject(name);
    }

    public object GetObject(string name, CultureInfo culture)
    {
        return Resources.ResourceManager.GetObject(name, culture);
    }

    public UnmanagedMemoryStream GetStream(string name)
    {
        return Resources.ResourceManager.GetStream(name);
    }

    public UnmanagedMemoryStream GetStream(string name, CultureInfo culture)
    {
        return Resources.ResourceManager.GetStream(name, culture);
    }
}
