﻿using System.Globalization;
using System.IO;

namespace Eurolook.AddIn.Common.Properties;

public interface IResourceManager
{
    string GetString(string name);

    string GetString(string name, CultureInfo culture);

    object GetObject(string name);

    object GetObject(string name, CultureInfo culture);

    UnmanagedMemoryStream GetStream(string name);

    UnmanagedMemoryStream GetStream(string name, CultureInfo culture);
}
