﻿using System.Drawing;
using System.Reflection;

namespace Eurolook.AddIn.Common.Properties
{
    public interface IEmbeddedResourceManager
    {
        Bitmap GetEmbeddedResourceImage(Assembly assembly, string folder, string resourceId);

        string GetEmbeddedResourceText(Assembly assembly, string folder, string resourceName);
    }
}
