﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.Messages;
using Eurolook.AddIn.Common.PerformanceLog;
using Eurolook.AddIn.Common.UserConfiguration;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.DataSync;
using GalaSoft.MvvmLight.Messaging;

namespace Eurolook.AddIn.Common
{
    public sealed class DataSyncScheduler : IDisposable, ICanLog
    {
        private readonly IPerformanceLogService _performanceLogService;
        private readonly object _dataSyncLock = new object();
        private readonly object _timerLock = new object();

        private volatile Process _dataSyncProcess;
        private volatile Timer _syncTimer;

        public DataSyncScheduler(
            ISettingsService settingsService,
            IPerformanceLogService performanceLogService,
            IUserIdentity userIdentity)
        {
            _performanceLogService = performanceLogService;
            Server = settingsService.DataSyncUrl;
            User = userIdentity.GetUserName();
            Messenger.Default.Register<LocalDataDeletedMessage>(this, OnLocalDataDeleted);
        }

        ~DataSyncScheduler()
        {
            Dispose(false);
        }

        public DataSyncStatus LastRun { get; private set; }

        public string User { get; }

        public string Server { get; set; }

        public bool IsRunning { get; private set; }

        [SuppressMessage("SonarQube", "S2583:Conditions should not unconditionally evaluate to true or to false", Justification = "Check is required by locking")]
        public void StartTimer(long delay, long interval, bool miniMode)
        {
            if (_syncTimer == null)
            {
                lock (_timerLock)
                {
                    // use double-checked locking
                    if (_syncTimer == null)
                    {
                        this.LogInfoFormat("Starting sync timer with interval={0}", interval);
                        _syncTimer = new Timer(state => RunDataSync(miniMode), this, delay, interval);
                    }
                }
            }
        }

        [SuppressMessage("SonarQube", "S2583:Conditions should not unconditionally evaluate to true or to false", Justification = "Check is required by locking")]
        public void StopTimer()
        {
            if (_syncTimer != null)
            {
                lock (_timerLock)
                {
                    // use double-checked locking
                    if (_syncTimer != null)
                    {
                        _syncTimer.Dispose();
                        _syncTimer = null;
                    }
                }
            }
        }

        public Task RunDataSyncAsync(bool miniMode)
        {
            return Task.Run(() => RunDataSync(miniMode));
        }

        private void RunDataSync(bool miniMode)
        {
            try
            {
                if (_dataSyncProcess != null && !_dataSyncProcess.HasExited)
                {
                    WaitForExit();
                }

                var stopwatch = new Stopwatch();
                stopwatch.Start();

                OnSyncStarted();
                CreateProcess(miniMode);
                WaitForExit();

                stopwatch.Stop();
                _performanceLogService
                    .LogDataSyncFinishedAsync(stopwatch.ElapsedMilliseconds)
                    .FireAndForgetSafeAsync(this.LogError);
            }
            catch (ThreadAbortException)
            {
                const string warn = "DataSync was aborted while it was running. Probably Word has been closed.";
                this.LogWarn(warn);
                LastRun = new DataSyncStatus
                {
                    StandardOutput = warn,
                    Result = DataSyncResult.Aborted,
                };
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                LastRun = new DataSyncStatus
                {
                    StandardOutput = ex.Message,
                    Result = DataSyncResult.UnexpectedError,
                };
            }
            finally
            {
                OnSyncCompleted(LastRun);
            }
        }

        private void CreateProcess(bool miniMode)
        {
            lock (_dataSyncLock)
            {
                string args = $"--server {Server}";
                if (!string.IsNullOrWhiteSpace(User))
                {
                    args += $" --user {User}";
                }

                if (miniMode)
                {
                    args += " --mini";
                }

                string fileName = Path.ChangeExtension(typeof(DataSync.DataSync).Assembly.Location, "exe");
                string workingDirectory = Path.GetDirectoryName(fileName) ?? Environment.SystemDirectory;

                _dataSyncProcess = Process.Start(
                    new ProcessStartInfo
                    {
                        FileName = fileName,
                        Arguments = args,
                        CreateNoWindow = true,
                        UseShellExecute = false, // cannot read from output if true
                        RedirectStandardOutput = true,
                        WorkingDirectory = workingDirectory,
                    });
            }
        }

        [SuppressMessage("SonarQube", "S2583:Conditions should not unconditionally evaluate to true or to false", Justification = "Check is required by locking")]
        private void WaitForExit()
        {
            if (_dataSyncProcess != null)
            {
                lock (_dataSyncLock)
                {
                    if (_dataSyncProcess != null)
                    {
                        string stdOut = _dataSyncProcess.StandardOutput.ReadToEnd();
                        _dataSyncProcess.WaitForExit();

                        LastRun = new DataSyncStatus
                        {
                            StandardOutput = stdOut,
                            Result = (DataSyncResult)_dataSyncProcess.ExitCode,
                        };

                        _dataSyncProcess.Dispose();
                        _dataSyncProcess = null;
                    }
                }
            }
        }

        private void OnSyncStarted()
        {
            IsRunning = true;
            Messenger.Default.Send(new DataSyncStartedMessage());
        }

        private void OnSyncCompleted(DataSyncStatus dataSyncStatus)
        {
            if (dataSyncStatus.Result == DataSyncResult.RemoteWipe)
            {
                StopTimer();
            }

            IsRunning = false;
            Messenger.Default.Send(new DataSyncCompletedMessage(dataSyncStatus));
        }

        private void OnLocalDataDeleted(LocalDataDeletedMessage message)
        {
            LastRun = null;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        [SuppressMessage("ReSharper", "UnusedParameter.Local", Justification = "Signature is according to the IDisposable pattern.")]
        private void Dispose(bool disposing)
        {
            StopTimer();
        }
    }
}
