﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Interop;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.AddIn.Common.Views;
using Eurolook.AddIn.Common.Wpf.FolderBrowser;
using Eurolook.Common.Log;
using Eurolook.OfficeNotificationBar;
using GalaSoft.MvvmLight;

namespace Eurolook.AddIn.Common
{
    public abstract class MessageService : IMessageService, ICanLog
    {
#pragma warning disable SA1310, IDE1006 // Field names should not contain underscore // Naming Styles

        private const int SW_RESTORE = 9;
        private const int KEYEVENTF_EXTENDEDKEY = 0x0001;
        private const int KEYEVENTF_KEYUP = 0x0002;
        private const int LEFT_ALT_KEY = 0xA4;

#pragma warning restore SA1310, IDE1006  // Field names should not contain underscore // Naming Styles

        private static readonly Dictionary<Type, Window> SingletonWindows = new Dictionary<Type, Window>();

        private bool _isPopupClosing;

        ~MessageService()
        {
            Dispose(false);
        }

        public void ActivateApplicationWindow()
        {
            var appWindowHandle = GeApplicationWindowHandle();
            SafeNativeMethods.SwitchToThisWindow(appWindowHandle, true);
            Thread.Sleep(200);

            if (SafeNativeMethods.IsIconic(appWindowHandle))
            {
                // Restore minimized window;
                SafeNativeMethods.ShowWindow(appWindowHandle, SW_RESTORE);
            }

            //// Seems to be necessary to actually bring the window to the foreground (otherwise it might be only flashing in the taskbar)
            //// Note: while debugging, the behaviour of SetForegroundWindow() is different which can be misleading
            //// (cf. https://learn.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-setforegroundwindow)
            // simulate left ALT key released
            SafeNativeMethods.keybd_event(LEFT_ALT_KEY, 0x45, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);

            SafeNativeMethods.SetForegroundWindow(appWindowHandle);
        }

        public void ShowDebugError(object obj, string caption = "Eurolook - Error")
        {
            if (AppSettings.Instance.ShowDebugErrors)
            {
                var messageBox = new MessageWindow(
                    new MessageViewModel
                    {
                        Title = caption,
                        Message = obj.ToString(),
                        IsShowCancelButton = false,
                    });
                messageBox.Show();
            }
        }

        public abstract void ShowSplashScreen();

        public abstract void ShowLightBox(ILightBoxContent lightBoxContent);

        public Window ShowSingleton(Window window, bool setOwner = true, bool showCentered = false, bool adaptWindowSizeForScreen = false)
        {
            if (window == null)
            {
                return null;
            }

            if (adaptWindowSizeForScreen)
            {
                SetDialogSize(window);
            }

            if (showCentered)
            {
                SetWindowCentered(window);
            }

            var type = window.GetType();
            if (SingletonWindows.ContainsKey(type))
            {
                var existingWindow = SingletonWindows[type];
                if (setOwner)
                {
                    SetActiveWordWindowAsOwner(existingWindow);
                }

                existingWindow.WindowState = WindowState.Normal;
                existingWindow.Activate();
                return existingWindow;
            }

            SingletonWindows[type] = window;
            if (setOwner)
            {
                SetActiveWordWindowAsOwner(window);
            }

            window.Closing += SingletonWindowClosing;
            window.Closed += SingletonWindowClosed;
            window.Show();
            return window;
        }

        public void ShowWindowActivated(Window window, bool showCentered = true)
        {
            SetActiveWordWindowAsOwner(window);
            if (showCentered)
            {
                SetWindowCentered(window);
            }

            ActivateOwner(window);
            window.Show();
        }

        public bool? ShowDialog(Window window, bool showCentered = true, bool adaptWindowSizeForScreen = false)
        {
            SetActiveWordWindowAsOwner(window);
            if (adaptWindowSizeForScreen)
            {
                SetDialogSize(window);
            }

            if (showCentered)
            {
                SetWindowCentered(window);
            }

            return ShowDialogInForeground(window);
        }

        public string ShowBrowseForFolderDialog(string title, string initialDirectory = null)
        {
            using (var folderBrowser = new BrowseForFolderDialog
            {
                Title = title,
            })
            {
                if (!string.IsNullOrEmpty(initialDirectory) && Directory.Exists(initialDirectory))
                {
                    folderBrowser.SelectedPath = initialDirectory;
                }

                if (folderBrowser.ShowDialog() == DialogResult.OK)
                {
                    return folderBrowser.SelectedPath;
                }
            }

            return null;
        }

        public bool? ShowMessageWindow(MessageViewModel messageViewModel, ViewModelBase ownerViewModel, bool? isTopMost = null)
        {
            var window = new MessageWindow(messageViewModel);
            if (isTopMost.HasValue)
            {
                window.Topmost = isTopMost.Value;
            }

            var owner = SingletonWindows.Values
                                        .FirstOrDefault(
                                            w => w.DataContext == ownerViewModel || w
                                                .FindVisualChildren<FrameworkElement>()
                                                .Any(fe => fe.DataContext == ownerViewModel));

            if (owner != null)
            {
                window.Owner = owner;
            }
            else
            {
                SetActiveWordWindowAsOwner(window);
            }

            SetWindowCentered(window);
            ShowDialogInForeground(window);
            return window.Result;
        }

        public bool? ShowSimpleMessage(string message, string title, ViewModelBase ownerViewModel = null)
        {
            return ShowMessageWindow(
                new MessageViewModel
                {
                    IsShowCancelButton = false,
                    Message = message,
                    Title = title,
                    WindowTitle = $"Eurolook - {title}",
                },
                ownerViewModel);
        }

        public void MoveTopMostDialog(int x, int y)
        {
            try
            {
                const short SwpNosize = 1;
                const short SwpNozorder = 0X4;
                const short SwpNoactivate = 0x0010;

                var windowHandle = SafeNativeMethods.GetForegroundWindow();
                bool result = SafeNativeMethods.SetWindowPos(
                    windowHandle,
                    0,
                    x,
                    y,
                    0,
                    0,
                    SwpNozorder | SwpNosize | SwpNoactivate);
                if (!result)
                {
                    this.LogDebug("Unable to set window position");
                }
            }
            catch (COMException ex)
            {
                this.LogDebug("Unable to set window position", ex);
            }
        }

        public void CloseAllWindows()
        {
            var windowsToClose = SingletonWindows.Values.ToList();
            foreach (var window in windowsToClose)
            {
                window.Close();
            }
        }

        public void ShowPopup(Window window, bool showCentered = true)
        {
            _isPopupClosing = false;
            window.Deactivated += OnPopupDeactivated;
            window.Closing += OnPopupClosing;
            if (showCentered)
            {
                SetWindowCentered(window);
            }

            window.Show();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        [SuppressMessage(
            "ReSharper",
            "UnusedParameter.Local",
            Justification = "This method signature is according to the IDisposable pattern.")]
        protected virtual void Dispose(bool disposing)
        {
            CloseAllWindows();
        }

        private static void SetWindowCentered(Window window)
        {
            // prefer to set the window centered to the owner
            var helper = new WindowInteropHelper(window);
            window.WindowStartupLocation = window.Owner != null
                ? WindowStartupLocation.CenterOwner
                : helper.Owner != IntPtr.Zero
                    ? WindowStartupLocation.Manual
                    : WindowStartupLocation.CenterScreen;

            // if we have a non-WPF owner, we need to center manually, see
            // https://blogs.msdn.microsoft.com/wpfsdk/2007/04/03/centering-wpf-windows-with-wpf-and-non-wpf-owner-windows/
            if (window.WindowStartupLocation == WindowStartupLocation.Manual && helper.Owner != IntPtr.Zero)
            {
                var nonWpfOwnerRect = SafeNativeMethods.GetWindowRect(helper.Owner);

                double scalingFactor = GetScalingFactor();
                nonWpfOwnerRect.Scale(scalingFactor, scalingFactor);

                double left = nonWpfOwnerRect.Left + (nonWpfOwnerRect.Width - GetEffectiveWindowWidth(window)) / 2;
                double top = nonWpfOwnerRect.Top + ((nonWpfOwnerRect.Height - GetEffectiveWindowHeight(window)) / 2);

                var workingArea = GetWorkingArea(window);

                LogManager.GetLogger().Debug($"Working area: {workingArea.Width} {workingArea.Height}");

                // keep dialog within visible area of screen, if the dialog is too large prefer
                // moving the dialog outside the screen to the top and left so that the OK / Cancel buttons stay visible.
                double minDistanceFromScreenEdge = 20.0 * scalingFactor;
                if (left < 0)
                {
                    left = minDistanceFromScreenEdge;
                }

                if (top < 0)
                {
                    top = minDistanceFromScreenEdge;
                }

                workingArea.Scale(scalingFactor, scalingFactor);
                if (left + window.Width > workingArea.Width)
                {
                    left = workingArea.Width - window.Width;
                }

                if (top + window.Height > workingArea.Height)
                {
                    top = workingArea.Height - window.Height;
                }

                window.Left = left;
                window.Top = top;
            }
        }

        private void SetDialogSize(Window window)
        {
            var workingArea = GetWorkingArea(window);
            double scalingFactor = GetScalingFactor();
            workingArea.Scale(scalingFactor, scalingFactor);

            if (window.Width > workingArea.Width)
            {
                window.Width = workingArea.Width;
                if (!double.IsNaN(window.MinWidth))
                {
                    window.MinWidth = window.Width;
                }
            }

            if (window.Height > workingArea.Height)
            {
                window.Height = workingArea.Height;
                if (!double.IsNaN(window.MinHeight))
                {
                    window.MinHeight = window.Height;
                }
            }
        }

        private static Rect GetWorkingArea(Window window)
        {
            var helper = new WindowInteropHelper(window);
            var screen = GetScreenFromWindow(helper.Owner);
            var workingArea = screen.WorkingArea;
            return new Rect(workingArea.X, workingArea.Y, workingArea.Width, workingArea.Height);
        }

        private static double GetScalingFactor()
        {
            return 1.0 / HiDpiHelper.GetDpiScalingFactor();
        }

        private static double GetEffectiveWindowHeight(Window window)
        {
            double effectiveWindowHeight = !double.IsNaN(window.Height)
                ? window.Height
                : !double.IsNaN(window.MinHeight)
                    ? window.MinHeight
                    : double.NaN;
            return effectiveWindowHeight;
        }

        private static double GetEffectiveWindowWidth(Window window)
        {
            double effectiveWindowWidth = !double.IsNaN(window.Width)
                ? window.Width
                : !double.IsNaN(window.MinWidth)
                    ? window.MinWidth
                    : double.NaN;
            return effectiveWindowWidth;
        }

        private static void ActivateOwner(Window window)
        {
            var helper = new WindowInteropHelper(window);
            if (SafeNativeMethods.IsWindow(helper.Owner))
            {
                SafeNativeMethods.SetForegroundWindow(helper.Owner);
            }
        }

        private static Screen GetScreenFromWindow(IntPtr hWnd)
        {
            var screen = Screen.FromHandle(hWnd);
            return screen;
        }

        private void OnPopupDeactivated(object sender, EventArgs e)
        {
            if (sender is Window window)
            {
                if (!_isPopupClosing)
                {
                    window.Deactivated -= OnPopupDeactivated;
                    window.Close();
                }
            }
        }

        private void OnPopupClosing(object sender, CancelEventArgs e)
        {
            _isPopupClosing = true;
        }

        protected bool? ShowDialogInForeground(Window window)
        {
            bool isDialogOpen = true;
            var helper = new WindowInteropHelper(window);
            int currentProcessId = Process.GetCurrentProcess().Id;

            // run a background task to check that the modal dialog stays on top
            // whenever any Word window is active
            // ReSharper disable once UnusedVariable
#pragma warning disable S1481 // Unused local variables should be removed
            var activateModalDialogTask = Task.Run(
                () =>
                {
                    Thread.Sleep(1000);

                    // ReSharper disable once AccessToModifiedClosure
                    // NOTE: we explicitly want to know whether the value has been modified outside the closure
                    while (isDialogOpen)
                    {
                        Thread.Sleep(100);
                        Execute.OnUiThread(
                            () =>
                            {
                                try
                                {
                                    if (helper.Owner != IntPtr.Zero && !SafeNativeMethods.IsWindowVisible(helper.Owner))
                                    {
                                        // native owner window has been closed -> close dialog
                                        window.Close();
                                        return;
                                    }

                                    var hWndForeground = SafeNativeMethods.GetForegroundWindow();
                                    if (hWndForeground == helper.Handle)
                                    {
                                        return;
                                    }

                                    string className = SafeNativeMethods.GetClassName(hWndForeground);

                                    // modal dialog is not the foreground window
                                    SafeNativeMethods.GetWindowThreadProcessId(
                                        hWndForeground,
                                        out uint foregroundProcessId);
                                    if (currentProcessId == foregroundProcessId && className == "OpusApp")
                                    {
                                        // Word is the foreground/active process, activate the modal dialog
                                        SafeNativeMethods.SetForegroundWindow(helper.Handle);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    this.LogError("Could not show dialog.", ex);
                                }
                            });
                    }
                });
#pragma warning restore S1481 // Unused local variables should be removed

            var result = window.ShowDialog();

            // cancel background task
            isDialogOpen = false;
            return result;
        }

        private void SingletonWindowClosing(object sender, EventArgs e)
        {
            if (sender is Window window)
            {
                // The active window might have changed while the singleton dialog was open.
                // To avoid that the original owner becomes active when the singleton window is closed,
                // we change the owner to the currently active window (see EUROLOOK-809)
                SetActiveWordWindowAsOwner(window);
                window.Closing -= SingletonWindowClosing;
            }
        }

        protected abstract void SetActiveWordWindowAsOwner(Window window);

        protected abstract IntPtr GeApplicationWindowHandle();

        private void SingletonWindowClosed(object sender, EventArgs e)
        {
            if (sender is Window window)
            {
                SingletonWindows.Remove(window.GetType());
                ActivateOwner(window);
                window.Closed -= SingletonWindowClosed;
            }
        }
    }
}
