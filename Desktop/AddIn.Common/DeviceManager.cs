using System;
using System.Linq;
using Eurolook.AddIn.Common.Database;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.DataSync;
using Newtonsoft.Json;

namespace Eurolook.AddIn.Common
{
    public class DeviceManager
    {
        private readonly IAddinContext _addinContext;
        private readonly IUserDataRepository _userDataRepository;

        public DeviceManager(IAddinContext addinContext, IUserDataRepository userDataRepository)
        {
            _addinContext = addinContext;
            _userDataRepository = userDataRepository;
        }

        public void UpdateDeviceProperties(bool updateQuickStartGuideShown = false)
        {
            var deviceSettings = _userDataRepository.GetOrCreateCurrentDeviceSettings();
            if (deviceSettings == null)
            {
                return;
            }

            var deviceInfoRetriever = new DeviceInfoRetriever();
            var deviceInfo = deviceInfoRetriever.GetDeviceInfo();
            string addinVersion = _addinContext.ClientVersion.ToString();
            string officeVersion = _addinContext.OfficeVersion.ToString();

            if (updateQuickStartGuideShown && deviceSettings.QuickStartGuideShown != addinVersion)
            {
                deviceSettings.QuickStartGuideShown = addinVersion;
            }

            deviceSettings.OperatingSystem = deviceInfo.OsName;
            deviceSettings.IsTouchScreen = deviceInfo.IsTouchScreen;
            deviceSettings.HasBattery = deviceInfo.HasBattery;
            deviceSettings.ScreenWidth = deviceInfo.ScreenWidth;
            deviceSettings.ScreenHeight = deviceInfo.ScreenHeight;
            deviceSettings.ScreenDpi = deviceInfo.ScreenDpi;
            deviceSettings.PhysicallyInstalledSystemMemoryKb = deviceInfo.PhysicallyInstalledSystemMemoryKb;
            deviceSettings.ProcessorName = deviceInfo.ProcessorName;
            deviceSettings.DotNetRuntimeVersion = deviceInfo.DotNetRuntimeVersion;
            deviceSettings.DotNetFullFrameworkReleaseVersion = deviceInfo.DotNetFullFrameworkReleaseVersion;
            deviceSettings.AddinVersion = addinVersion;
            deviceSettings.OfficeVersion = officeVersion;
            deviceSettings.InstallRoot = _addinContext.InstallRoot;
            deviceSettings.IsPerUserInstall = _addinContext.IsPerUserInstall;
            deviceSettings.IsPerMachineInstall = _addinContext.IsPerMachineInstall;

            var loadTimes = _addinContext.LoadTimes().ToList();
            deviceSettings.AddInLoadTimesMilliseconds = JsonConvert.SerializeObject(loadTimes.Select(t => t.TotalMilliseconds), Formatting.Indented);
            if (loadTimes.Any())
            {
                float maxAddInLoadTimeBucketSeconds = (float)Math.Ceiling(loadTimes.Max().TotalSeconds * 2.0) / 2.0f;
                deviceSettings.MaxAddInLoadTimeBucketSeconds = maxAddInLoadTimeBucketSeconds;
            }

            deviceSettings.Owner = deviceInfo.DeviceOwner;

            if (IsLastLoginChanged(deviceSettings))
            {
                deviceSettings.LastLogin = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            }

            if (!deviceSettings.IsInitialised)
            {
                deviceSettings.IsInitialised = true;
            }

            _userDataRepository.UpdateDeviceSettings(deviceSettings);
            LogDeviceSettings(deviceSettings);
        }

        private static bool IsLastLoginChanged(DeviceSettings deviceSettings)
        {
            return deviceSettings.LastLogin == null || DateTime.Now.Year > deviceSettings.LastLogin.Value.Year
                || DateTime.Now.Month > deviceSettings.LastLogin.Value.Month;
        }

        public static void LogDeviceSettings(DeviceSettings deviceSettings)
        {
            var filteredDeviceSettings = deviceSettings;
            filteredDeviceSettings.ActivityTrack = null;
            filteredDeviceSettings.TaskpaneExpansionState = null;
            var formattedSettings = JsonConvert.SerializeObject(filteredDeviceSettings, Formatting.None);
            LogManager.GetLogger().Info($"Device settings: {formattedSettings}");
        }
    }
}
