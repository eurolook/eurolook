﻿using System.Collections.Generic;
using Eurolook.DataSync;
using Microsoft.Extensions.Configuration;

namespace Eurolook.AddIn.Common;

public class AppSettings
{
    public static AppSettings Instance { get; } = GetSettings();

    public string ProductCustomizationId { get; set; }

    public bool UseRoamingDataDirectory { get; set; }

    public bool StandaloneMode { get; set; }

    public bool ShowDebugErrors { get; set; }

    public string DefaultDataSyncEnvironment { get; set; }

    public List<DataSyncEnvironment> DataSyncEnvironments { get; set; }

    public PluginConfiguration Plugins { get; set; }

    private static AppSettings GetSettings()
    {
        var config = AppSettingsRetriever.GetConfiguration();

        return config.GetRequiredSection(nameof(AppSettings)).Get<AppSettings>();
    }
}
