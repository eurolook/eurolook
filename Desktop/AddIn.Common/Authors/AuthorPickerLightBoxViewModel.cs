﻿using System;
using System.Collections.Generic;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;

namespace Eurolook.AddIn.Common.Authors
{
    public class AuthorPickerLightBoxViewModel : ViewModelBase
    {
        private readonly Func<AuthorPickerViewModel> _authorPickerViewModelFunc;
        private readonly Action<AuthorAndAllJobs> _pickSelectedAuthor;
        private readonly Action<bool> _showSearch;
        private readonly Action _cancelPicker;

        private string _authorRoleName = string.Empty;

        private bool _isPickerVisible;
        private RelayCommand _pickSelectionCommand;
        private AuthorPickerViewModel _authorPickerViewModel;

        public AuthorPickerLightBoxViewModel(
            ISettingsService settingsService,
            Func<AuthorPickerViewModel> authorPickerViewModelFunc,
            Action<AuthorAndAllJobs> pickSelectedAuthor,
            Action cancelPicker,
            Action<bool> showSearch)
        {
            CanSearch = !settingsService.IsStandaloneMode;

            _authorPickerViewModelFunc = authorPickerViewModelFunc;
            _pickSelectedAuthor = pickSelectedAuthor;
            _cancelPicker = cancelPicker;
            _showSearch = showSearch;
        }

        public AuthorPickerViewModel AuthorPickerViewModel
        {
            get => _authorPickerViewModel;
            private set
            {
                _authorPickerViewModel = value;
                RaisePropertyChanged(() => AuthorPickerViewModel);
            }
        }

        public bool IsPickerVisible
        {
            get => _isPickerVisible;
            private set => Set(() => IsPickerVisible, ref _isPickerVisible, value);
        }

        public string AuthorRoleName
        {
            get => _authorRoleName;
            private set => Set(() => AuthorRoleName, ref _authorRoleName, value);
        }

        public RelayCommand PickSelectionCommand
        {
            get => _pickSelectionCommand;
            set
            {
                _pickSelectionCommand = value;
                RaisePropertyChanged(() => PickSelectionCommand);
            }
        }

        public RelayCommand ShowSearchCommand => new(ShowSearch);

        public bool CanSearch { get; }

        public RelayCommand CancelPickingCommand => new(CancelPicking);

        public void Show(IEnumerable<AuthorAndAllJobs> authors, string authorRoleName)
        {
            AuthorRoleName = authorRoleName;
            AuthorPickerViewModel = _authorPickerViewModelFunc();
            AuthorPickerViewModel.RefreshData(authors);

            PickSelectionCommand = new RelayCommand(PickSelection, () => AuthorPickerViewModel.IsSelectionComplete);
            AuthorPickerViewModel.PickAuthorCommand = PickSelectionCommand;
            IsPickerVisible = true;
        }

        private void ShowSearch()
        {
            IsPickerVisible = false;
            _showSearch(true);
        }

        private void CancelPicking()
        {
            _cancelPicker();
            IsPickerVisible = false;
        }

        private void PickSelection()
        {
            _pickSelectedAuthor(AuthorPickerViewModel.SelectedAuthor);
            IsPickerVisible = false;
        }
    }
}
