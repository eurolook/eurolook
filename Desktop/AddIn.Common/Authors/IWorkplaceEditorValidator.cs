namespace Eurolook.AddIn.Common.Authors
{
    public interface IWorkplaceEditorValidator
    {
        bool IsValidAddress(WorkplaceEditorViewModel2 workplaceEditor);
    }
}
