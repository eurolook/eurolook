﻿using System.Collections.Generic;
using System.Linq;
using Eurolook.Common.Extensions;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.Authors
{
    public interface ITextPairDisplayItem
    {
        string PrimaryText { get; }

        string SecondaryText { get; }

        bool IsDeemphasized { get; }
    }

    public class AuthorAndJob : ITextPairDisplayItem
    {
        public AuthorAndJob([NotNull] AuthorViewModel author, JobAssignmentViewModel jobAssignment)
        {
            Author = author;
            JobAssignment = jobAssignment;
        }

        [NotNull]
        public AuthorViewModel Author { get; }

        public JobAssignmentViewModel JobAssignment { get; }

        public string PrimaryText => Author.ToString();

        public string SecondaryText => JobAssignment.ToString();

        public bool IsDeemphasized => Author.IsOriginatingFromDocumentCustomXml;

        public static IEnumerable<AuthorAndJob> CreateAll(User user)
        {
            var authorViewModels = new AuthorViewModel(user.Self, true).CreateList();
            authorViewModels.AddRange(user.Superiors.Select(ua => new AuthorViewModel(ua.Author)));
            authorViewModels.AddRange(
                user.Authors
                    .Where(ua => authorViewModels.All(vm => vm.Author?.Id != ua.AuthorId))
                    .Select(ua => new AuthorViewModel(ua.Author)));
            return authorViewModels.OrderBy(x => x.Title).SelectMany(CreateAll);
        }

        public static IEnumerable<AuthorAndJob> CreateAll(AuthorViewModel authorViewModel)
        {
            {
                yield return new AuthorAndJob(authorViewModel, new JobAssignmentViewModel(authorViewModel.Author));

                foreach (var authorJobAssignment in authorViewModel.Author.JobAssignments.Where(j => !j.Deleted))
                {
                    yield return new AuthorAndJob(authorViewModel, new JobAssignmentViewModel(authorJobAssignment));
                }
            }
        }

        public static AuthorAndJob CreateSingle(AuthorViewModel authorViewModel)
        {
            return new AuthorAndJob(authorViewModel, new JobAssignmentViewModel(authorViewModel.Author));
        }
    }
}
