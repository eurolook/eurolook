﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.AddIn.Common.Messages;
using Eurolook.AddIn.Common.UserConfiguration;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace Eurolook.AddIn.Common.Authors
{
    public class AuthorManagerViewModel2 : ViewModelBase, ICanLog
    {
        private readonly ISettingsService _settingsService;
        private readonly IUserIdentity _userIdentity;
        private readonly IAuthorEditorValidator _authorEditorValidator;
        private readonly IJobEditorValidator _jobEditorValidator;
        private readonly IWorkplaceEditorValidator _workplaceEditorValidator;
        private readonly IFavoriteService _favoriteService;
        private readonly IMessageService _messageService;
        private readonly IUserDataRepository _userDataRepository;
        private readonly ColorManager _colorManager;
        private readonly IAuthorRepository _authorRepository;
        private readonly ILanguageRepository _languageRepository;

        private AuthorInfoTileViewModel _myAuthorTile;
        private bool _isAuthorEditorVisible;
        private AuthorEditorViewModel2 _authorEditorViewModel;
        private Author _self;
        private bool _isAuthorSearchVisible;
        private AuthorSearchViewModel _authorSearchViewModel;
        private bool _isWorkplaceEditorVisible;
        private bool _isJobEditorVisible;
        private IEnumerable<Address> _addresses;
        private ObservableCollectionEx<PredefinedFunctionViewModel> _predefinedFunctions;
        private IEnumerable<Language> _languages;
        private WorkplaceEditorViewModel2 _selectedWorkplace;
        private JobEditorViewModel2 _selectedJob;
        private Task _loadJobDataTask;
        private ObservableCollectionEx<OrgaEntityViewModel> _orgaEntities;
        private Language _en;

        public AuthorManagerViewModel2(
            ISettingsService settingsService,
            IFavoriteService favoriteService,
            IMessageService messageService,
            IUserDataRepository userDataRepository,
            IAuthorRepository authorRepository,
            ILanguageRepository languageRepository,
            IAuthorEditorValidator authorEditorValidator,
            IJobEditorValidator jobEditorValidator,
            IWorkplaceEditorValidator workplaceEditorValidator,
            IUserIdentity userIdentity,
            ColorManager colorManager)
        {
            _authorRepository = authorRepository;
            _languageRepository = languageRepository;
            _settingsService = settingsService;
            _authorEditorValidator = authorEditorValidator;
            _jobEditorValidator = jobEditorValidator;
            _workplaceEditorValidator = workplaceEditorValidator;
            _favoriteService = favoriteService;
            _messageService = messageService;
            _userDataRepository = userDataRepository;
            _userIdentity = userIdentity;
            _colorManager = colorManager;

            GetHelpCommand = new RelayCommand(() => _settingsService.OpenHelpLink("ManagingAuthors"));
            CloseCommand = new RelayCommand(Close);
            AddFavoriteCommand = new RelayCommand(AddFavorite);

            GetAuthorEditorHelpCommand = new RelayCommand(() => _settingsService.OpenHelpLink("AuthorEditor"));
            SaveAuthorEditorCommand = new RelayCommand(SaveAuthorEditor);
            CancelAuthorEditorCommand = new RelayCommand(CancelAuthorEditor);

            GetSearchHelpCommand = new RelayCommand(() => _settingsService.OpenHelpLink("AuthorSearch"));
            ShowSearchCommand = new RelayCommand(ShowAuthorSearch);
            CancelSearchCommand = new RelayCommand(CancelAuthorSearch);

            GetJobEditorHelpCommand = new RelayCommand(() => _settingsService.OpenHelpLink("ManagingJobs"));
            SaveJobEditorCommand = new RelayCommand(SaveJobEditor);
            CancelJobEditorCommand = new RelayCommand(CancelJobEditor);
            AddJobCommand = new RelayCommand(AddJob);

            GetWorkplaceEditorHelpCommand = new RelayCommand(() => _settingsService.OpenHelpLink("ManagingWorkplaces"));
            SaveWorkplaceEditorCommand = new RelayCommand(SaveWorkplaceEditor);
            CancelWorkplaceEditorCommand = new RelayCommand(CancelWorkplaceEditor);

            MyFavoritesTiles = new ObservableCollectionEx<AuthorInfoTileViewModel>();
            MySuperiorsTiles = new ObservableCollectionEx<AuthorInfoTileViewModel>();
            Workplaces = new ObservableCollectionEx<WorkplaceEditorViewModel2>();
            Jobs = new ObservableCollectionEx<JobEditorViewModel2>();
            OrgaEntities = new ObservableCollectionEx<OrgaEntityViewModel>();
            PredefinedFunctions = new ObservableCollectionEx<PredefinedFunctionViewModel>();

            AuthorSearchViewModel = new AuthorSearchViewModel(settingsService) { SingleResultPerAuthor = true };

            AuthorEditorViewModel = new AuthorEditorViewModel2(authorEditorValidator, settingsService)
            {
                ShowJobEditorCommand = new GalaSoft.MvvmLight.CommandWpf.RelayCommand(ShowJobEditor),
                ShowWorkplaceEditorCommand = new GalaSoft.MvvmLight.CommandWpf.RelayCommand(ShowWorkplaceEditor)
            };

            Messenger.Default.Register<ColorSchemeUpdatedMessage>(this, OnColorSchemeUpdated);
        }

        public Action CloseAction { get; set; }

        public RelayCommand GetHelpCommand { get; set; }

        public RelayCommand CloseCommand { get; set; }

        public RelayCommand ShowSearchCommand { get; set; }

        public RelayCommand AddFavoriteCommand { get; set; }

        public RelayCommand GetAuthorEditorHelpCommand { get; set; }

        public RelayCommand SaveAuthorEditorCommand { get; set; }

        public RelayCommand CancelAuthorEditorCommand { get; set; }

        public RelayCommand CancelSearchCommand { get; set; }

        public RelayCommand GetSearchHelpCommand { get; set; }

        public RelayCommand GetWorkplaceEditorHelpCommand { get; set; }

        public RelayCommand SaveWorkplaceEditorCommand { get; set; }

        public RelayCommand CancelWorkplaceEditorCommand { get; set; }

        public RelayCommand GetJobEditorHelpCommand { get; set; }

        public RelayCommand SaveJobEditorCommand { get; set; }

        public RelayCommand CancelJobEditorCommand { get; set; }

        public RelayCommand AddJobCommand { get; set; }

        public ObservableCollectionEx<AuthorInfoTileViewModel> MyFavoritesTiles { get; set; }

        public ObservableCollectionEx<AuthorInfoTileViewModel> MySuperiorsTiles { get; set; }

        public ObservableCollectionEx<WorkplaceEditorViewModel2> Workplaces { get; set; }

        public ObservableCollectionEx<JobEditorViewModel2> Jobs { get; set; }

        public ObservableCollectionEx<OrgaEntityViewModel> OrgaEntities
        {
            get => _orgaEntities;
            set => Set(() => OrgaEntities, ref _orgaEntities, value);
        }

        public ObservableCollectionEx<PredefinedFunctionViewModel> PredefinedFunctions
        {
            get => _predefinedFunctions;
            set => Set(() => PredefinedFunctions, ref _predefinedFunctions, value);
        }

        public AuthorInfoTileViewModel MyAuthorTile
        {
            get => _myAuthorTile;
            set => Set(() => MyAuthorTile, ref _myAuthorTile, value);
        }

        public bool IsFavouritesListVisible => !_settingsService.IsStandaloneMode;

        public bool IsFavouritesEmptyStageVisible =>
            IsFavouritesListVisible && ((MyFavoritesTiles == null) || (MyFavoritesTiles.Count == 0));

        public bool IsSuperiorListVisible => MySuperiorsTiles is { Count: > 0 };

        public bool IsBaseFormEnabled => !IsAuthorEditorVisible && !IsAuthorSearchVisible && !IsJobEditorVisible
                                         && !IsWorkplaceEditorVisible;

        public bool IsAuthorEditorEnabled => !IsWorkplaceEditorVisible && !IsJobEditorVisible;

        public bool IsAuthorEditorVisible
        {
            get => _isAuthorEditorVisible;
            set
            {
                Set(() => IsAuthorEditorVisible, ref _isAuthorEditorVisible, value);
                RaisePropertyChanged(() => IsBaseFormEnabled);
            }
        }

        public bool IsAuthorSearchVisible
        {
            get => _isAuthorSearchVisible;
            set
            {
                AuthorSearchViewModel.SearchText = "";
                AuthorSearchViewModel.IsSearchBoxFocused = false;
                Set(() => IsAuthorSearchVisible, ref _isAuthorSearchVisible, value);
                RaisePropertyChanged(() => IsBaseFormEnabled);
                Task.Delay(300);
                AuthorSearchViewModel.IsSearchBoxFocused = true;
            }
        }

        public bool IsWorkplaceEditorVisible
        {
            get => _isWorkplaceEditorVisible;
            set
            {
                Set(() => IsWorkplaceEditorVisible, ref _isWorkplaceEditorVisible, value);
                RaisePropertyChanged(() => IsAuthorEditorEnabled);
                RaisePropertyChanged(() => IsBaseFormEnabled);
            }
        }

        public bool IsJobEditorVisible
        {
            get => _isJobEditorVisible;
            set
            {
                Set(() => IsJobEditorVisible, ref _isJobEditorVisible, value);
                RaisePropertyChanged(() => IsAuthorEditorEnabled);
                RaisePropertyChanged(() => IsBaseFormEnabled);
            }
        }

        public AuthorEditorViewModel2 AuthorEditorViewModel
        {
            get => _authorEditorViewModel;
            set => Set(() => AuthorEditorViewModel, ref _authorEditorViewModel, value);
        }

        public AuthorSearchViewModel AuthorSearchViewModel
        {
            get => _authorSearchViewModel;
            set => Set(() => AuthorSearchViewModel, ref _authorSearchViewModel, value);
        }

        public WorkplaceEditorViewModel2 SelectedWorkplace
        {
            get => _selectedWorkplace;
            set
            {
                Set(() => SelectedWorkplace, ref _selectedWorkplace, value);
                if (SelectedWorkplace == null)
                {
                    return;
                }

                SelectedWorkplace.IsSelected = true;
                foreach (var workplace in Workplaces.Where(x => x != _selectedWorkplace))
                {
                    workplace.IsSelected = false;
                }
            }
        }

        public bool IsAddButtonVisible => !_settingsService.IsStandaloneMode;

        public JobEditorViewModel2 SelectedJob
        {
            get => _selectedJob;
            set
            {
                Set(() => SelectedJob, ref _selectedJob, value);
                if (SelectedJob == null)
                {
                    return;
                }

                SelectedJob.IsSelected = true;
                foreach (var job in Jobs.Where(x => x != _selectedJob))
                {
                    job.IsSelected = false;
                }
            }
        }

        public async Task LoadAsync()
        {
            var user = await _userDataRepository.GetUserForManagerOverviewAsync(_userIdentity.GetUserName());
            _self = user.Self;
            AuthorEditorViewModel.Self = _self;

            _languages = await _languageRepository.GetAllLanguagesAsync();
            _en = _languages.FirstOrDefault(x => x.Name == "EN");
            AuthorEditorViewModel.English = _en;

            MyAuthorTile = CreateAuthorInfoTile(_self, true, false);

            MyFavoritesTiles.Clear();
            MyFavoritesTiles.AddRange(
                user.Authors.Select(x => CreateAuthorInfoTile(x.Author, false, true)).OrderBy(x => x.Title));
            RaisePropertyChanged(() => IsFavouritesEmptyStageVisible);

            MySuperiorsTiles.Clear();
            MySuperiorsTiles.AddRange(
                user.Superiors.OrderBy(x => x.OrderIndex).Select(x => CreateAuthorInfoTile(x.Author, false, false)));
            RaisePropertyChanged(() => IsSuperiorListVisible);

            _loadJobDataTask = Task.Run(
                async () =>
                {
                    await Task.Delay(1000);
                    await LoadJobEditorDataSync();
                });
        }

        private void RemoveFavorite(AuthorInfoTileViewModel favoriteTile)
        {
            if (favoriteTile?.Model == null)
            {
                return;
            }

            _favoriteService.RemoveFavorite(favoriteTile.Model);
            MyFavoritesTiles.Remove(favoriteTile);
            RaisePropertyChanged(() => IsFavouritesEmptyStageVisible);
        }

        private void ShowAuthorEditor(AuthorInfoTileViewModel authorTile)
        {
            if (authorTile?.Model == null)
            {
                return;
            }

            ShowAuthorEditorAsync(authorTile).FireAndForgetSafeAsync(this.LogError);
        }

        private async Task ShowAuthorEditorAsync(AuthorInfoTileViewModel authorTile)
        {
            var author = await _authorRepository.GetAuthorForEditorAsync(authorTile.AuthorId);
            AuthorEditorViewModel.Init(author);
            IsAuthorEditorVisible = true;
        }

        private void SaveAuthorEditor()
        {
            AuthorEditorViewModel.Validate();
            if (AuthorEditorViewModel.IsValid && AuthorEditorViewModel.RunPlausibilityCheck())
            {
                AuthorEditorViewModel.Apply();
                _authorRepository.UpdateAuthorFromEditor(AuthorEditorViewModel.Model);
                UpdateAuthorInfoTiles(AuthorEditorViewModel.Model);
                IsAuthorEditorVisible = false;
            }
        }

        private void CancelAuthorEditor()
        {
            AuthorEditorViewModel.Cancel();
            UpdateAuthorInfoTiles(AuthorEditorViewModel.Model);
            IsAuthorEditorVisible = false;
        }

        private void ShowAuthorSearch()
        {
            IsAuthorSearchVisible = true;
        }

        private void CancelAuthorSearch()
        {
            IsAuthorSearchVisible = false;
        }

        private void AddFavorite()
        {
            var authorAndJob = AuthorSearchViewModel.SelectedAuthorAndJob;
            if (authorAndJob?.Author.Author == null)
            {
                return;
            }

            var existingFavorite =
                MyFavoritesTiles.FirstOrDefault(x => x.Model?.Id.Equals(authorAndJob.Author.Author.Id) == true);
            if (existingFavorite == null)
            {
                _favoriteService.AddFavoriteIfNotExisting(authorAndJob.Author.Author);
                var tiles = MyFavoritesTiles.ToList();
                tiles.Add(CreateAuthorInfoTile(authorAndJob.Author.Author, false, true));
                MyFavoritesTiles.ReplaceRange(tiles.OrderBy(x => x.Title));
            }

            AuthorSearchViewModel.SearchResults.Clear();
            IsAuthorSearchVisible = false;
            RaisePropertyChanged(() => IsFavouritesEmptyStageVisible);
        }

        private void ShowJobEditor()
        {
            if (AuthorEditorViewModel?.Model == null)
            {
                return;
            }

            ShowJobEditorAsync().FireAndForgetSafeAsync(this.LogError);
        }

        private async Task LoadJobEditorDataSync()
        {
            var predefinedFunctions = await _authorRepository.GetPredefinedFunctionsAsync();
            PredefinedFunctions.Add(
                new PredefinedFunctionViewModel
                {
                    PredefinedFunctionId = null,
                    DisplayName = "Enter a custom value..."
                });
            PredefinedFunctions.AddRange(
                predefinedFunctions
                    .Select(x => new PredefinedFunctionViewModel(x)).OrderBy(x => x.DisplayName));
            await _authorRepository.LoadOrgaTreeAsync(OrgaEntities);
            foreach (var dg in OrgaEntities)
            {
                await _authorRepository.LoadOrgaTreeTranslationsAsync(dg, _en, false);
            }
        }

        private async Task ShowJobEditorAsync()
        {
            await _loadJobDataTask;

            Jobs.Clear();
            var mainJob = await _authorRepository.GetMainJobAsync(AuthorEditorViewModel.Model.Id);
            Jobs.Add(
                new JobEditorViewModel2(
                    mainJob,
                    OrgaEntities,
                    PredefinedFunctions,
                    _languages,
                    _jobEditorValidator,
                    _authorRepository)
                {
                    DisplayName = "Main Job",
                    IsLocked = AuthorEditorViewModel.IsLocked,
                    DeleteJobCommand = new RelayCommand(DeleteJob)
                });
            var i = 2;
            foreach (var job in await _authorRepository.GetJobsAsync(AuthorEditorViewModel.Model.Id))
            {
                Jobs.Add(
                    new JobEditorViewModel2(
                        job,
                        OrgaEntities,
                        PredefinedFunctions,
                        _languages,
                        _jobEditorValidator,
                        _authorRepository)
                    {
                        DisplayName = $"Job {i}",
                        IsLocked = AuthorEditorViewModel.IsLocked,
                        DeleteJobCommand = new RelayCommand(DeleteJob)
                    });
                i++;
            }

            SelectedJob = Jobs.FirstOrDefault(x => x.IsMainJob) ?? Jobs.FirstOrDefault();
            IsJobEditorVisible = true;
        }

        private void AddJob()
        {
            if (CheckForUnsavedJobs(
                    "You have an new and unsaved job. Save or cancel before you create a new job."))
            {
                return;
            }

            var newJob = new JobAssignment();
            newJob.Init();
            newJob.AuthorId = AuthorEditorViewModel.Model.Id;
            var jobEditor = new JobEditorViewModel2(
                newJob,
                OrgaEntities,
                PredefinedFunctions,
                _languages,
                _jobEditorValidator,
                _authorRepository)
            {
                DisplayName = $"Job {Jobs.Count + 1}",
                IsNew = true
            };
            Jobs.Add(jobEditor);
            SelectedJob = jobEditor;
            AuthorEditorViewModel.Model.JobAssignments.Add(newJob);
            AuthorEditorViewModel.UpdateJobPreview(Jobs.Count);
        }

        private bool CheckForUnsavedJobs(string message)
        {
            var count = Jobs.Count(x => x.IsNew);
            if (count > 0)
            {
                _messageService.ShowMessageWindow(
                    new MessageViewModel
                    {
                        Title = "Unsaved Jobs",
                        IsShowNoButton = false,
                        IsShowCancelButton = false,
                        Message = message
                    },
                    this);
                return true;
            }

            return false;
        }

        private void DeleteJob()
        {
            if (_messageService.ShowMessageWindow(
                    new MessageViewModel
                    {
                        Title = "Delete Job",
                        Message = "Do you really want to permanently delete this job?",
                        YesButtonText = "Yes",
                        NoButtonText = "No",
                        IsShowNoButton = true,
                        IsShowCancelButton = false
                    },
                    this) == true)
            {
                _authorRepository.DeleteJobAssignment(SelectedJob.Model);
                AuthorEditorViewModel.Model.JobAssignments.RemoveWhere(job => job.Id == SelectedJob.Model.Id);
                Jobs.Remove(SelectedJob);
                SelectedJob = Jobs.FirstOrDefault();
                AuthorEditorViewModel.UpdateJobPreview(Jobs.Count);
            }
        }

        private void SaveJobEditor()
        {
            foreach (var job in Jobs)
            {
                job.Validate();
                if (job.IsValid)
                {
                    job.Apply();
                    if (job.IsNew)
                    {
                        _authorRepository.CreateJobAssignment(job.Model);
                        job.IsNew = false;
                    }
                    else
                    {
                        if (job.IsMainJob)
                        {
                            _authorRepository.UpdateMainJob(job.Model);
                        }
                        else
                        {
                            _authorRepository.UpdateJobAssignment(job.Model);
                        }
                    }

                    if (job.IsMainJob && job.Model is Author author)
                    {
                        UpdateMainJobPreviews(author);
                    }
                    else if (job.Model is JobAssignment jobAssignment)
                    {
                        UpdateAdditionalJobModel(jobAssignment);
                    }
                }
            }

            if (Jobs.All(x => x.IsValid))
            {
                IsJobEditorVisible = false;
                SelectedJob = null;
            }
            else
            {
                SelectedJob = Jobs.First(x => !x.IsValid);
            }
        }

        private void UpdateMainJobPreviews(Author author)
        {
            AuthorEditorViewModel.Model.Service = author.Service;
            AuthorEditorViewModel.Model.PredefinedFunction = author.PredefinedFunction;
            AuthorEditorViewModel.Model.PredefinedFunctionId = author.PredefinedFunctionId;
            AuthorEditorViewModel.Model.Functions = author.Functions;
            AuthorEditorViewModel.UpdateJobPreview(author, Jobs.Count);
        }

        private void UpdateAdditionalJobModel(JobAssignment jobAssignment)
        {
            var model = AuthorEditorViewModel.Model.JobAssignments.FirstOrDefault(j => j.Id == jobAssignment.Id);
            if (model != null)
            {
                model.Service = jobAssignment.Service;
                model.PredefinedFunction = jobAssignment.PredefinedFunction;
                model.PredefinedFunctionId = jobAssignment.PredefinedFunctionId;
                model.Functions = jobAssignment.Functions;
            }
        }

        private void CancelJobEditor()
        {
            foreach (var job in Jobs.Where(x => x.IsNew).ToList())
            {
                AuthorEditorViewModel.Model.JobAssignments.RemoveWhere(j => j.Id == job.Model.Id);
                Jobs.Remove(job);
            }

            foreach (var job in Jobs)
            {
                job.Cancel();
            }

            AuthorEditorViewModel.UpdateJobPreview(Jobs.Count);
            IsJobEditorVisible = false;
            SelectedJob = null;
        }

        private void ShowWorkplaceEditor()
        {
            if (AuthorEditorViewModel.Model?.MainWorkplaceId == null)
            {
                return;
            }

            if (_addresses == null)
            {
                _addresses = _authorRepository.GetAddresses();
            }

            var author = AuthorEditorViewModel.Model;
            Workplaces.Clear();
            var workplaces = _authorRepository.GetWorkplaces(author.Id);
            var mainWorkplace = workplaces.FirstOrDefault(x => x.Id == author.MainWorkplaceId);
            if (mainWorkplace != null)
            {
                Workplaces.Add(
                    new WorkplaceEditorViewModel2(mainWorkplace, _addresses, _workplaceEditorValidator)
                    {
                        DisplayName = "Main Workplace",
                        IsMainWorkplace = true,
                        IsLocked = AuthorEditorViewModel.IsLocked
                    });
            }

            var i = 2;
            foreach (var otherWorkplace in workplaces.Where(x => x.Id != author.MainWorkplaceId))
            {
                Workplaces.Add(
                    new WorkplaceEditorViewModel2(otherWorkplace, _addresses, _workplaceEditorValidator)
                    {
                        DisplayName = $"Workplace {i}",
                        IsLocked = AuthorEditorViewModel.IsLocked
                    });
                i++;
            }

            SelectedWorkplace = Workplaces.FirstOrDefault();
            if (SelectedWorkplace != null)
            {
                SelectedWorkplace.IsSelected = true;
            }

            IsWorkplaceEditorVisible = true;
        }

        private void SaveWorkplaceEditor()
        {
            var selectedIds = Workplaces.Select(wp => wp.SelectedAddress?.Address.Id);

            if (selectedIds.Count() != selectedIds.Distinct().Count())
            {
                _messageService.ShowSimpleMessage("Two workplaces with the same address are not allowed.", "Error");
                return;
            }

            foreach (var workplace in Workplaces)
            {
                workplace.Validate();
                if (workplace.IsValid)
                {
                    workplace.Apply();
                    _authorRepository.UpdateWorkplace(workplace.Model);
                    if (workplace.IsMainWorkplace)
                    {
                        AuthorEditorViewModel.UpdateWorkplacePreview(workplace.Model, Workplaces.Count);
                    }
                }
            }

            if (Workplaces.All(x => x.IsValid))
            {
                IsWorkplaceEditorVisible = false;
            }
            else
            {
                SelectedWorkplace = Workplaces.First(x => !x.IsValid);
            }
        }

        private void CancelWorkplaceEditor()
        {
            Workplaces.RemoveRange(Workplaces.Where(x => x.IsNew));
            foreach (var workplace in Workplaces)
            {
                workplace.Cancel();
            }

            IsWorkplaceEditorVisible = false;
        }

        private async Task UpdateMyAuthorTileAsync()
        {
            var user = await _userDataRepository.GetUserForManagerOverviewAsync(_userIdentity.GetUserName());
            _self = user.Self;
            AuthorEditorViewModel.Self = _self;
            UpdateAuthorInfoTiles(_self);
        }

        private void Close()
        {
            CloseAction?.Invoke();
        }

        private void UpdateAuthorInfoTiles(Author author)
        {
            var tiles = new List<AuthorInfoTileViewModel>();
            if (MyAuthorTile.AuthorId == author.Id)
            {
                tiles.Add(MyAuthorTile);
            }

            tiles.AddRange(MySuperiorsTiles.Where(x => x.AuthorId == author.Id));
            tiles.AddRange(MyFavoritesTiles.Where(x => x.AuthorId == author.Id));
            foreach (var tile in tiles)
            {
                tile.Title = AuthorViewModel.GetDisplayName(author);
                tile.Description = GetPrimaryJobDescription(author);
                tile.SecondLineDescription = GetAdditionalJobsDescription(author);
            }
        }

        private AuthorInfoTileViewModel CreateAuthorInfoTile(Author author, bool isSelf, bool isFavorite)
        {
            var canEdit = isSelf || (_authorEditorValidator?.CanEdit(MyAuthorTile?.Model, author) == true);
            var tile = new AuthorInfoTileViewModel(author)
            {
                HeroIconPath =
                    canEdit
                        ? "/Graphics/heroicons-outline/white/user.svg"
                        : "/Graphics/heroicons-outline/white/lock-closed.svg",
                Title = AuthorViewModel.GetDisplayName(author),
                Description = GetPrimaryJobDescription(author),
                SecondLineDescription = GetAdditionalJobsDescription(author),
                ColorBrush = _colorManager.CurrentColorScheme.PrimaryColor.ToBrush(),
                TileCommand =
                    new GalaSoft.MvvmLight.CommandWpf.RelayCommand<AuthorInfoTileViewModel>(ShowAuthorEditor)
            };

            if (canEdit)
            {
                tile.SecondaryCommands.Add(
                    new AuthorInfoTileCommand
                    {
                        Title = "Edit",
                        IconPath = "/Graphics/heroicons-outline/black/pencil.svg",
                        Tooltip = "Edit this author.",
                        Command = new GalaSoft.MvvmLight.CommandWpf.RelayCommand<AuthorInfoTileViewModel>(
                            ShowAuthorEditor)
                    });
            }
            else
            {
                tile.SecondaryCommands.Add(
                    new AuthorInfoTileCommand
                    {
                        Title = "View",
                        IconPath = "/Graphics/heroicons-outline/black/eye.svg",
                        Tooltip = "View this author.",
                        Command = new GalaSoft.MvvmLight.CommandWpf.RelayCommand<AuthorInfoTileViewModel>(
                            ShowAuthorEditor)
                    });
            }

            if (isFavorite)
            {
                tile.SecondaryCommands.Add(
                    new AuthorInfoTileCommand
                    {
                        Title = "Remove",
                        IconPath = "/Graphics/heroicons-outline/black/trash.svg",
                        Tooltip = "Remove this author from your favourites.",
                        Command = new GalaSoft.MvvmLight.CommandWpf.RelayCommand<AuthorInfoTileViewModel>(
                            RemoveFavorite)
                    });
            }

            return tile;
        }

        private string GetPrimaryJobDescription(Author author)
        {
            return GetDisplayDescription(author);
        }

        private string GetAdditionalJobsDescription(Author author)
        {
            return string.Join(
                "\n",
                author?.JobAssignments.Select(GetDisplayDescription).ToArray() ?? Array.Empty<string>());
        }

        private string GetDisplayDescription(IJobAssignment job)
        {
            var orgaAcronym = job?.Service;
            var function = job?.PredefinedFunction?.Name;
            if (string.IsNullOrWhiteSpace(function))
            {
                var jobFunction = job?.Functions.FirstOrDefault(x => x.LanguageId == _en?.Id && !x.Function.IsNullOrEmpty())
                                  ?? job?.Functions.FirstOrDefault(x => !x.Function.IsNullOrEmpty());
                function = jobFunction?.Function;
            }

            return string.IsNullOrWhiteSpace(function) ? orgaAcronym : $"{orgaAcronym} - {function}";
        }

        private void UpdateColorScheme(ColorScheme colorScheme)
        {
            if (colorScheme == null)
            {
                return;
            }

            // EUROLOOK-3369 and EUROLOOK-3975: setting Color Scheme crashes in rare circumstances
            // MyAuthorTile may be null if Load Task has not yet finished
            if (MyAuthorTile != null)
            {
                MyAuthorTile.ColorBrush = colorScheme.PrimaryColor.ToBrush();
            }

            foreach (var tile in MySuperiorsTiles)
            {
                tile.ColorBrush = colorScheme.PrimaryColor.ToBrush();
            }

            foreach (var tile in MyFavoritesTiles)
            {
                tile.ColorBrush = colorScheme.PrimaryColor.ToBrush();
            }
        }

        private void OnColorSchemeUpdated(ColorSchemeUpdatedMessage message)
        {
            Execute.OnUiThread(() => UpdateColorScheme(message.NewColorScheme));
        }
    }
}
