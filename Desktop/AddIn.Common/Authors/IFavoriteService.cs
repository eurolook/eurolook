using System.Collections.Generic;
using Eurolook.Data.Models;

namespace Eurolook.AddIn.Common.Authors
{
    public interface IFavoriteService
    {
        void AddFavoriteIfNotExisting(Author author);

        void AddFavoritesIfNotExisting(IEnumerable<Author> author);

        void RemoveFavorite(Author author);
    }
}
