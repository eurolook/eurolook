﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;

namespace Eurolook.AddIn.Common.Authors
{
    public class AuthorInfoTileCommand : ViewModelBase
    {
        private string _title;
        private RelayCommand<AuthorInfoTileViewModel> _command;
        private string _iconPath;
        private string _tooltip;

        public string IconPath
        {
            get { return _iconPath; }
            set { Set(() => IconPath, ref _iconPath, value); }
        }

        public string Title
        {
            get { return _title; }
            set { Set(() => Title, ref _title, value); }
        }

        public string Tooltip
        {
            get { return _tooltip; }
            set { Set(() => Tooltip, ref _tooltip, value); }
        }

        public RelayCommand<AuthorInfoTileViewModel> Command
        {
            get { return _command; }
            set { Set(() => Command, ref _command, value); }
        }
    }
}
