﻿using System.Collections.Generic;
using System.Linq;
using Eurolook.AddIn.Common.Wpf;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.Authors
{
    public class AuthorsAndJobsViewModel
    {
        private readonly ISettingsService _settingsService;

        public AuthorsAndJobsViewModel(ISettingsService settingsService)
        {
            _settingsService = settingsService;
            AuthorsAndJobs = new ObservableCollectionEx<AuthorAndJob>();
            AuthorsAndAllJobs = new ObservableCollectionEx<AuthorAndAllJobs>();
        }

        public ObservableCollectionEx<AuthorAndAllJobs> AuthorsAndAllJobs { get; }

        public ObservableCollectionEx<AuthorAndJob> AuthorsAndJobs { get; }

        public void InitAuthorsAndAllJobs(IEnumerable<AuthorAndAllJobs> authorsAndAllJobs)
        {
            AuthorsAndAllJobs.Clear();
            AuthorsAndAllJobs.AddRange(
                authorsAndAllJobs.OrderBy(aj => aj.Author.Title)
                                 .ThenBy(aj => aj.MainJobAssignment.DisplayName));

            InitAuthorsAndJobs(AuthorsAndAllJobs.SelectMany(aj => aj.All));
        }

        public void InitAuthorsAndJobs(IEnumerable<AuthorAndJob> authorsAndJobs)
        {
            AuthorsAndJobs.Clear();
            AuthorsAndJobs.AddRange(
                authorsAndJobs.OrderBy(aj => aj.Author.Title)
                              .ThenBy(aj => aj.JobAssignment.IsMainJob ? 0 : 1)
                              .ThenBy(aj => aj.JobAssignment.DisplayName));

            if (!_settingsService.IsStandaloneMode)
            {
                var searchEntry = AuthorViewModel.GetSearchEntry();
                AuthorsAndJobs.Add(new AuthorAndJob(searchEntry, new JobAssignmentViewModel { DisplayName = "" }));
            }
        }

        public AuthorAndJob AddIfNotPresent([NotNull] AuthorAndJob authorAndJob)
        {
            var existingAuthorAndJob = AuthorsAndJobs.FirstOrDefault(
                aj => (aj.Author.Author?.Id.Equals(authorAndJob.Author.Author?.Id) == true)
                      && !aj.Author.IsOriginatingFromDocumentCustomXml
                      && (authorAndJob.JobAssignment?.JobAssignmentId == aj.JobAssignment?.JobAssignmentId));

            if (existingAuthorAndJob != null)
            {
                return existingAuthorAndJob;
            }

            AuthorsAndJobs.Insert(AuthorsAndJobs.Count - 1, authorAndJob);
            return authorAndJob;
        }

        public AuthorAndAllJobs AddIfNotPresent([NotNull] AuthorAndAllJobs authorAndJob)
        {
            if (authorAndJob.Type == AuthorAndAllJobs.AuthorType.FromDocument)
            {
                return authorAndJob;
            }

            var existingAuthorAndJob = AuthorsAndAllJobs.FirstOrDefault(
                aj => (aj.Author.Author?.Id.Equals(authorAndJob.Author.Author?.Id) == true)
                      && !aj.Author.IsOriginatingFromDocumentCustomXml);

            if (existingAuthorAndJob != null)
            {
                return existingAuthorAndJob;
            }

            AuthorsAndAllJobs.Insert(AuthorsAndAllJobs.Count - 1, authorAndJob);
            foreach (var aj in authorAndJob.All)
            {
                AddIfNotPresent(aj);
            }

            return authorAndJob;
        }
    }
}
