﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Data.Constants;
using Eurolook.Data.Models.AuthorRoles;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.Authors
{
    public class AuthorRolesSelectionViewModel : ViewModelBase
    {
        private AuthorRoleViewModelChangedMessage _authorRoleViewModelChangedMessage;
        private AuthorRoleSelectionViewModel _selectionViewModel;
        private bool _isPickerActive;

        public AuthorRolesSelectionViewModel(
            Func<Action<AuthorAndAllJobs>, Action, AuthorSearchLightBoxViewModel> authorSearchLightBoxViewModelFunc,
            Func<Action<AuthorAndAllJobs>, Action, Action<bool>, AuthorPickerLightBoxViewModel>
                authorPickerLightBoxViewModelFunc,
            AuthorsAndJobsViewModel authorsAndJobsViewModel,
            bool showAuthorRolesLabels = true)
        {
            ShowAuthorRolesLabels = showAuthorRolesLabels;

            AuthorSearchLightBoxViewModel = authorSearchLightBoxViewModelFunc(PickResult, CancelSearch);
            AuthorPickerLightBoxViewModel = authorPickerLightBoxViewModelFunc(
                PickResult,
                CancelPicker,
                singleResultPerAuthor => AuthorSearchLightBoxViewModel.Show(singleResultPerAuthor));
            AuthorsAndJobsViewModel = authorsAndJobsViewModel;
            AuthorRoleViewModels = new ObservableCollectionEx<AuthorRoleSelectionViewModel>();

            ShowAuthorPickerCommand = new RelayCommand<AuthorRoleSelectionViewModel>(ShowAuthorPicker);

            Messenger.Default?.Register<AuthorRoleViewModelChangedMessage>(this, OnAuthorRoleViewModelChangedMessage);
        }

        public AuthorRolesSelectionViewModel(
            AuthorsAndJobsViewModel authorsAndJobsViewModel,
            bool showAuthorRolesLabels)
        {
            ShowAuthorRolesLabels = showAuthorRolesLabels;
            AuthorsAndJobsViewModel = authorsAndJobsViewModel;

            AuthorRoleViewModels = new ObservableCollectionEx<AuthorRoleSelectionViewModel>();
            Messenger.Default?.Register<AuthorRoleViewModelChangedMessage>(this, OnAuthorRoleViewModelChangedMessage);
        }

        public AuthorSearchLightBoxViewModel AuthorSearchLightBoxViewModel { get; }

        public AuthorPickerLightBoxViewModel AuthorPickerLightBoxViewModel { get; }

        public AuthorsAndJobsViewModel AuthorsAndJobsViewModel { get; }

        public ObservableCollectionEx<AuthorRoleSelectionViewModel> AuthorRoleViewModels { get; }

        public RelayCommand<AuthorRoleSelectionViewModel> ShowAuthorPickerCommand { get; }

        public bool ShowAuthorRolesLabels { get; }

        public void InitAuthorRoles(
            IEnumerable<AuthorRoleViewModel> authorRoleViewModels,
            List<AuthorRoleSetting> authorRoleSettings)
        {
            AuthorRoleViewModels.Clear();
            AuthorRoleViewModels.AddRange(
                authorRoleViewModels.OrderBy(ar => ar.AuthorRole.UiPositionIndex)
                                    .Select(AuthorRoleSelectionViewModel.FromAuthorRoleViewModel));
            InitAuthorAndJobOfAuthorRoles(authorRoleSettings);
        }

        public void InitAuthorsAndJobs(IEnumerable<AuthorAndAllJobs> authorsAndJobs)
        {
            AuthorsAndJobsViewModel.InitAuthorsAndAllJobs(authorsAndJobs);
        }

        public void HideSigners()
        {
            foreach (var authorRoleViewModel in AuthorRoleViewModels)
            {
                if (authorRoleViewModel.AuthorRole.Id == CommonDataConstants.SignatoryAuthorRoleId)
                {
                    authorRoleViewModel.IsVisible = false;
                }
            }
        }

        private void InitAuthorAndJobOfAuthorRoles(List<AuthorRoleSetting> authorRoleSettings)
        {
            foreach (var authorRoleViewModel in AuthorRoleViewModels)
            {
                InitAuthorAndJobOfAuthorRole(authorRoleViewModel, authorRoleSettings);
            }
        }

        private void InitAuthorAndJobOfAuthorRole(
            AuthorRoleSelectionViewModel authorRoleViewModel,
            List<AuthorRoleSetting> authorRoleSettings)
        {
            var authorRoleSetting =
                authorRoleSettings?.FirstOrDefault(ar => ar.AuthorRoleId == authorRoleViewModel.AuthorRole.Id);

            var authorsAndJobs = AuthorsAndJobsViewModel.AuthorsAndJobs.Where(
                aj => (aj.Author.Author?.Id.Equals(authorRoleSetting?.AuthorId) == true) || aj.Author.IsSelf).ToList();

            var authorAndJob =
                authorsAndJobs.FirstOrDefault(
                    aj => aj.JobAssignment.JobAssignmentId?.Equals(authorRoleSetting?.JobAssignmentId) == true)
                ?? authorsAndJobs.FirstOrDefault(
                    aj => aj.Author.Author?.Id.Equals(authorRoleSetting?.AuthorId) == true)
                ?? authorsAndJobs.FirstOrDefault(aj => aj.JobAssignment.IsMainJob)
                ?? authorsAndJobs.FirstOrDefault(aj => aj.Author.IsSelf);

            authorRoleViewModel.AuthorAndAllJobs =
                AuthorsAndJobsViewModel.AuthorsAndAllJobs.FirstOrDefault(
                    aj => aj.Author.Author?.Id.Equals(authorAndJob?.Author.Author?.Id) == true);
            authorRoleViewModel.SelectedJob = authorAndJob;
            authorRoleViewModel.JobAssignmentFromSettings = authorAndJob;
        }

        private void OnAuthorRoleViewModelChangedMessage(AuthorRoleViewModelChangedMessage message)
        {
            if (message.AuthorRoleViewModel.Author?.IsDummyForSearch == true)
            {
                _authorRoleViewModelChangedMessage = message;
                _selectionViewModel = message.AuthorRoleViewModel as AuthorRoleSelectionViewModel;
                AuthorSearchLightBoxViewModel.Show();
            }
        }

        private void ShowAuthorPicker(AuthorRoleSelectionViewModel viewModel)
        {
            _selectionViewModel = viewModel;
            _isPickerActive = true;
            AuthorPickerLightBoxViewModel.Show(AuthorsAndJobsViewModel.AuthorsAndAllJobs, viewModel.NiceName);
        }

        private void PickResult([NotNull] AuthorAndAllJobs authorAndJob)
        {
            if (_selectionViewModel != null)
            {
                _selectionViewModel.AuthorAndAllJobs = AuthorsAndJobsViewModel.AddIfNotPresent(authorAndJob);
                if (_selectionViewModel.SelectedJob == null)
                {
                    if (_selectionViewModel.JobAssignmentFromSettings != null)
                    {
                        _selectionViewModel.SelectedJob =
                            _selectionViewModel.AvailableJobs.FirstOrDefault(
                                _selectionViewModel.JobAssignmentFromSettings.Equals);
                    }

                    if (authorAndJob.All.Count() == 1)
                    {
                        // Result comes from a search with separate entries per job
                        _selectionViewModel.SelectedJob = AuthorsAndJobsViewModel.AuthorsAndJobs.FirstOrDefault(
                            aj => (aj.Author.Author?.Id.Equals(authorAndJob.Author.Author?.Id) == true)
                                  && !aj.Author.IsOriginatingFromDocumentCustomXml
                                  && (authorAndJob.MainJobAssignment?.JobAssignmentId == aj.JobAssignment?.JobAssignmentId));
                    }
                }

                _isPickerActive = false;
            }
        }

        private void CancelSearch()
        {
            if (_authorRoleViewModelChangedMessage?.AuthorRoleViewModel != null)
            {
                _authorRoleViewModelChangedMessage.AuthorRoleViewModel.AuthorAndJob =
                    _authorRoleViewModelChangedMessage?.OldAuthorAndJob;
            }

            if (_isPickerActive)
            {
                ShowAuthorPicker(_selectionViewModel);
            }
        }

        private void CancelPicker()
        {
            _isPickerActive = false;
        }
    }

    public class AuthorRolesSelectionDesignViewModel : AuthorRolesSelectionViewModel
    {
        public AuthorRolesSelectionDesignViewModel()
            : base((a1, a2) => null, (a1, a2, a3) => null, new AuthorsAndJobsViewModel(null))
        {
            AuthorRoleViewModels.Add(
                new AuthorRoleSelectionViewModel(
                    new AuthorRole
                    {
                        Id = Guid.NewGuid(),
                        Name = "Signer",
                        Description =
                            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.\r\nNunc viverra imperdiet enim. Fusce est. Vivamus a tellus.\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci.\r\nAenean nec lorem. In porttitor. Donec laoreet nonummy augue.\r\nSuspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc. Mauris eget neque at sem venenatis eleifend. Ut nonummy.\r\n"
                    }));
            AuthorRoleViewModels.Add(new AuthorRoleSelectionViewModel(new AuthorRole { Name = "Writer" }));
            AuthorRoleViewModels.Add(
                new AuthorRoleSelectionViewModel(
                    new AuthorRole { DisplayName = "Long role or author name which does not fit" }));

            AuthorsAndJobsViewModel.AuthorsAndJobs.Add(
                new AuthorAndJob(
                    new AuthorViewModel("Signer first name last name", false),
                    new JobAssignmentViewModel { DisplayName = "Deputy Head of Cabinet of the President" }));
        }
    }
}
