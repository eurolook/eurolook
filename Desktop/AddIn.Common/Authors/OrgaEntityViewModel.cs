using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Eurolook.Data.Models;
using GalaSoft.MvvmLight;

namespace Eurolook.AddIn.Common.Authors
{
    public class OrgaEntityViewModel : ViewModelBase
    {
        private bool _isExpanded;
        private bool _isSelected;
        private string _headerText;

        public OrgaEntityViewModel()
        {
            SubEntities = new ObservableCollection<OrgaEntityViewModel>();
        }

        public OrgaEntityViewModel(OrgaEntity orgaEntity)
            : this()
        {
            if (orgaEntity == null)
            {
                return;
            }

            OrgaEntity = orgaEntity;
            Id = orgaEntity.Id;
            Name = orgaEntity.Name;
            GenderText = string.IsNullOrWhiteSpace(orgaEntity.Gender) ? "" : $"({orgaEntity.Gender})";

            if (OrgaEntity?.Header == null)
            {
                return;
            }

            HeaderText = orgaEntity.Header.Value.Replace("<>", " ");
        }

        public Guid Id { get; set; }

        public OrgaEntity OrgaEntity { get; set; }

        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string HeaderText
        {
            get => _headerText;
            set
            {
                Set(() => HeaderText, ref _headerText, value);
                SetDisplayName();
            }
        }

        private void SetDisplayName()
        {
            DisplayName = HeaderText != "-" ? $"{OrgaEntity.Name}: {HeaderText}" : HeaderText;
        }

        public OrgaEntityViewModel ParentEntityVm { get; set; }

        public ObservableCollection<OrgaEntityViewModel> SubEntities { get; set; }

        public bool IsSelected
        {
            get => _isSelected;
            set { Set(() => IsSelected, ref _isSelected, value); }
        }

        public bool IsExpanded
        {
            get => _isExpanded;
            set { Set(() => IsExpanded, ref _isExpanded, value); }
        }

        public string GenderText { get; set; }

        public static OrgaEntityViewModel SelectAndExpandItem(ObservableCollection<OrgaEntityViewModel> orgaTree, Guid? id)
        {
            if (id == null)
            {
                return null;
            }

            var orgaEntityVm = GetOrgaEntityViewModel(orgaTree, id);
            if (orgaEntityVm != null)
            {
                DeselectTree(orgaTree);
                orgaEntityVm.ExpandAndScrollIntoView();
                orgaEntityVm.IsSelected = true;
            }

            return orgaEntityVm;
        }

        public static void DeselectTree(ObservableCollection<OrgaEntityViewModel> orgaTree)
        {
            DeselectItemsRecursive(orgaTree);
        }

        public override string ToString()
        {
            return DisplayName ?? OrgaEntity.Name;
        }

        public void ExpandAndScrollIntoView()
        {
            ExpandRecursive(this);
        }

        private static void DeselectItemsRecursive(ObservableCollection<OrgaEntityViewModel> viewModels)
        {
            foreach (var vm in viewModels)
            {
                if (vm == null)
                {
                    continue;
                }

                vm.IsSelected = false;
                if (vm.SubEntities.Any())
                {
                    DeselectItemsRecursive(vm.SubEntities);
                }
            }
        }

        public static OrgaEntityViewModel GetOrgaEntityViewModel(IEnumerable<OrgaEntityViewModel> viewModels, Guid? id)
        {
            foreach (var vm in viewModels)
            {
                if (vm?.OrgaEntity?.Id == id)
                {
                    return vm;
                }

                if (vm != null && vm.SubEntities.Any())
                {
                    var result = GetOrgaEntityViewModel(vm.SubEntities, id);
                    if (result != null)
                    {
                        return result;
                    }
                }
            }

            return null;
        }

        private static void ExpandRecursive(OrgaEntityViewModel orgaEntityVm)
        {
            orgaEntityVm.IsExpanded = true;
            if (orgaEntityVm.ParentEntityVm != null)
            {
                ExpandRecursive(orgaEntityVm.ParentEntityVm);
            }
        }
    }
}
