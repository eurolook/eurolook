﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Eurolook.AddIn.Common.Authors
{
    public class AuthorRoleViewModelValidator : IAuthorRoleViewModelValidator
    {
        public void Validate(List<AuthorRoleViewModel> authorRoleViewModels)
        {
            if (authorRoleViewModels.Any(ar => ar.Author?.Author == null))
            {
                throw new EurolookValidationException("Select a person and a job assignment for each author role.");
            }

            if (authorRoleViewModels.Any(
                ar => ar.JobAssignment?.JobAssignment == null && ar.JobAssignment?.IsMainJob != true))
            {
                throw new EurolookValidationException("Select a job assignment for each author role.");
            }

            ValidateIncompleteAuthors(authorRoleViewModels);
            ValidateSameJobAssignmentForSameAuthor(authorRoleViewModels);
            ValidateAuthorOriginatingFromDocumentNotUsedInDifferentRole(authorRoleViewModels);
        }

        private void ValidateIncompleteAuthors(List<AuthorRoleViewModel> authorRoleViewModels)
        {
            var incompleteAuthors = authorRoleViewModels.Where(ar => ar.IsIncomplete)
                                                        .Select(ar => ar.Author.Author)
                                                        .ToList();

            if (incompleteAuthors.Any())
            {
                string incompleteString = string.Join(
                    " and ",
                    incompleteAuthors.Select(a => a.FullNameWithLastNameLast).Distinct());

                throw new EurolookValidationException(
                    "Some required information is missing for the selected author(s)."
                    + $" Ask {incompleteString} to complete the required information.");
            }
        }

        private void ValidateSameJobAssignmentForSameAuthor(List<AuthorRoleViewModel> authorRoleViewModels)
        {
            var authorJobDictionary = new Dictionary<Guid, Guid?>();
            foreach (var authorRoleViewModel in authorRoleViewModels)
            {
                var authorId = authorRoleViewModel.Author.Author.Id;
                var jobAssignmentId = authorRoleViewModel.JobAssignment?.JobAssignmentId;

                if (authorJobDictionary.ContainsKey(authorId))
                {
                    if (authorJobDictionary[authorId] != jobAssignmentId)
                    {
                        throw new EurolookValidationException(
                            $"The author {authorRoleViewModel.Author.Author.FullNameWithLastNameLast} is used with different job assignments which is not supported."
                            + " Select the same job assignment for different roles.");
                    }
                }
                else
                {
                    authorJobDictionary[authorId] = jobAssignmentId;
                }
            }
        }

        private void ValidateAuthorOriginatingFromDocumentNotUsedInDifferentRole(List<AuthorRoleViewModel> authorRoleViewModels)
        {
            foreach (var authorRoleViewModel in authorRoleViewModels)
            {
                if (authorRoleViewModel.Author.IsOriginatingFromDocumentCustomXml && authorRoleViewModel.IsModified)
                {
                    throw new EurolookValidationException(
                        $"The author {authorRoleViewModel.Author.Author.FullNameWithLastNameLast} is not in your favorites list." +
                        $" A selection is not possible for author role {authorRoleViewModel.AuthorRole.DisplayName}");
                }
            }
        }
    }
}
