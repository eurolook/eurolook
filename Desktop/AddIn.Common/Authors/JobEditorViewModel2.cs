﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using GalaSoft.MvvmLight.Command;

namespace Eurolook.AddIn.Common.Authors
{
    public class JobEditorViewModel2 : CancelableViewModel<IJobAssignment>, IDataErrorInfo, ICanLog
    {
        private readonly Dictionary<string, string> _errors = new();
        private string _displayName;
        private bool _isMainJob;
        private bool _isSelected;
        private string _webAddress;
        private string _functionalMailbox;
        private string _acronym;
        private bool _isOrgaEntityInitialized;
        private OrgaEntityViewModel _selectedOrgaEntity;
        private JobFunctionViewModel _selectedFunction;
        private ObservableCollectionEx<PredefinedFunctionViewModel> _predefinedFunctions;
        private PredefinedFunctionViewModel _selectedPredefinedFunction;
        private IEnumerable<Language> _languages;
        private string _error;
        private IJobEditorValidator _jobEditorValidator;
        private bool _isLocked;
        private IAuthorRepository _authorRepository;
        private ObservableCollectionEx<OrgaEntityViewModel> _orgaEntities;
        private bool _isNew;
        private Language _en;
        private OrgaEntityViewModel _originalOrgaEntity;

        public JobEditorViewModel2(
            ObservableCollectionEx<OrgaEntityViewModel> orgaEntities,
            ObservableCollectionEx<PredefinedFunctionViewModel> predefinedFunctions,
            IEnumerable<Language> languages,
            IJobEditorValidator jobEditorValidator,
            IAuthorRepository authorRepository)
            : base(null)
        {
            CommonConstructor(orgaEntities, predefinedFunctions, languages, jobEditorValidator, authorRepository);
        }

        public JobEditorViewModel2(
            Author author,
            ObservableCollectionEx<OrgaEntityViewModel> orgaEntities,
            ObservableCollectionEx<PredefinedFunctionViewModel> predefinedFunctions,
            IEnumerable<Language> languages,
            IJobEditorValidator jobEditorValidator,
            IAuthorRepository authorRepository)
            : base(author)
        {
            IsMainJob = true;
            CommonConstructor(orgaEntities, predefinedFunctions, languages, jobEditorValidator, authorRepository);
        }

        public JobEditorViewModel2(
            JobAssignment jobAssignment,
            ObservableCollectionEx<OrgaEntityViewModel> orgaEntities,
            ObservableCollectionEx<PredefinedFunctionViewModel> predefinedFunctions,
            IEnumerable<Language> languages,
            IJobEditorValidator jobEditorValidator,
            IAuthorRepository authorRepository)
            : base(jobAssignment)
        {
            IsMainJob = false;
            CommonConstructor(orgaEntities, predefinedFunctions, languages, jobEditorValidator, authorRepository);
        }

        public RelayCommand DeleteJobCommand { get; set; }

        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                var wasSelected = _isSelected;
                Set(() => IsSelected, ref _isSelected, value);
                if (_isSelected && !wasSelected)
                {
                    SelectOrgaEntityInTree(true);
                    _isOrgaEntityInitialized = true;
                    Validate();
                }
            }
        }

        public bool IsMainJob
        {
            get => _isMainJob;
            set
            {
                Set(() => IsMainJob, ref _isMainJob, value);
                RaisePropertyChanged(() => IsDeletable);
            }
        }

        public string DisplayName
        {
            get => _displayName;
            set => Set(() => DisplayName, ref _displayName, value);
        }

        public string WebAddress
        {
            get => _webAddress;
            set
            {
                Set(() => WebAddress, ref _webAddress, value);
                RaisePropertyChanged(() => WebAddressPlaceholder);
            }
        }

        public string WebAddressPlaceholder
        {
            get
            {
                if (string.IsNullOrEmpty(WebAddress) && (SelectedOrgaEntity != null))
                {
                    return SelectedOrgaEntity.OrgaEntity.InheritedWebAddress;
                }

                return null;
            }
        }

        public string FunctionalMailbox
        {
            get => _functionalMailbox;
            set => Set(() => FunctionalMailbox, ref _functionalMailbox, value);
        }

        public string Acronym
        {
            get => _acronym;
            set => Set(() => Acronym, ref _acronym, value);
        }

        public ObservableCollectionEx<OrgaEntityViewModel> OrgaEntities
        {
            get => _orgaEntities;
            set => Set(() => OrgaEntities, ref _orgaEntities, value);
        }

        public OrgaEntityViewModel SelectedOrgaEntity
        {
            get => _selectedOrgaEntity;
            set => Set(() => SelectedOrgaEntity, ref _selectedOrgaEntity, value);
        }

        public ObservableCollectionEx<JobFunctionViewModel> Functions { get; private set; }

        public JobFunctionViewModel SelectedFunction
        {
            get => _selectedFunction;
            set => Set(() => SelectedFunction, ref _selectedFunction, value);
        }

        public ObservableCollectionEx<PredefinedFunctionViewModel> PredefinedFunctions
        {
            get => _predefinedFunctions;
            set => Set(() => PredefinedFunctions, ref _predefinedFunctions, value);
        }

        public PredefinedFunctionViewModel SelectedPredefinedFunction
        {
            get => _selectedPredefinedFunction;
            set
            {
                Set(() => SelectedPredefinedFunction, ref _selectedPredefinedFunction, value);
                RaisePropertyChanged(() => IsPredefinedFunctionSelected);
            }
        }

        public bool IsNew
        {
            get => _isNew;
            set
            {
                Set(() => IsNew, ref _isNew, value);
                RaisePropertyChanged(() => IsDeletable);
            }
        }

        public bool IsPredefinedFunctionSelected => _selectedPredefinedFunction?.PredefinedFunctionId != null;

        public bool IsLocked
        {
            get => _isLocked;
            set
            {
                Set(() => IsLocked, ref _isLocked, value);
                RaisePropertyChanged(() => SaveTooltip);
                RaisePropertyChanged(() => CancelTooltip);
                RaisePropertyChanged(() => IsDeletable);
            }
        }

        public bool IsDeletable => !IsLocked && !IsMainJob && !IsNew;

        public string SaveTooltip => IsLocked ? "" : "Save the job(s).";

        public string CancelTooltip => IsLocked ? "Go back." : "Don't save the job(s), go back.";

        public string Error
        {
            get => _error;
            set
            {
                Set(() => Error, ref _error, value);
                RaisePropertyChanged(() => IsValid);
            }
        }

        public bool IsValid => Error == null;

        public string this[string columnName]
        {
            get
            {
                if ((columnName == nameof(SelectedOrgaEntity)) && _isOrgaEntityInitialized)
                {
                    if (_jobEditorValidator.IsValidOrgaEntity(this))
                    {
                        _errors.Remove(columnName);
                    }
                    else
                    {
                        _errors[columnName] = "Select an organisational entity.";
                    }
                }
                else if (columnName == nameof(Acronym))
                {
                    if (_jobEditorValidator.IsValidAcronym(this))
                    {
                        _errors.Remove(columnName);
                    }
                    else
                    {
                        _errors[columnName] = "Enter an Orga Entity acronym.";
                    }
                }

                Error = _errors.Count > 0 ? "All fields with a red border are mandatory." : null;
                return _errors.TryGetValue(columnName, out var error) ? error : null;
            }
        }

        public override void Apply()
        {
            var orgaEntity = SelectedOrgaEntity?.OrgaEntity;
            if (orgaEntity != null)
            {
                Model.OrgaEntity = orgaEntity;
                Model.OrgaEntityId = orgaEntity.Id;
            }

            Model.Service = Acronym;
            Model.WebAddress = WebAddress;
            Model.FunctionalMailbox = FunctionalMailbox;
            Model.PredefinedFunction = SelectedPredefinedFunction.PredefinedFunction;
            Model.PredefinedFunctionId = SelectedPredefinedFunction.PredefinedFunction?.Id;
            var modifiedFunctions = Functions.Where(x => x.IsModified).ToArray();
            if (modifiedFunctions.Any())
            {
                ApplyModifiedFunctions(modifiedFunctions);
            }
        }

        public override void Cancel()
        {
            Acronym = Model.Service;
            WebAddress = Model.WebAddress;
            FunctionalMailbox = Model.FunctionalMailbox;
            SelectedOrgaEntity = _originalOrgaEntity;
            SelectedPredefinedFunction = PredefinedFunctions.FirstOrDefault(
                x => x.PredefinedFunctionId == Model.PredefinedFunctionId);
            InitFunctions(Model);
            Validate();
        }

        public void Validate()
        {
            _ = this[nameof(SelectedOrgaEntity)];
        }

        public async Task LoadTextsOfDirectorateGeneral(OrgaEntityViewModel orgaEntity)
        {
            var headerText = orgaEntity?.HeaderText;
            var subEntityHeaderText = orgaEntity?.SubEntities.FirstOrDefault()?.HeaderText;
            if (!string.IsNullOrEmpty(headerText) && !string.IsNullOrEmpty(subEntityHeaderText))
            {
                return;
            }

            var dg = FindDirectorateGeneral(orgaEntity);
            await _authorRepository.LoadOrgaTreeTranslationsAsync(dg, _en);
        }

        protected override void InitViewModel(IJobAssignment job)
        {
            WebAddress = job.WebAddress;
            FunctionalMailbox = job.FunctionalMailbox;
            Acronym = job.Service;
        }

        private void CommonConstructor(
            ObservableCollectionEx<OrgaEntityViewModel> orgaEntities,
            ObservableCollectionEx<PredefinedFunctionViewModel> predefinedFunctions,
            IEnumerable<Language> languages,
            IJobEditorValidator jobEditorValidator,
            IAuthorRepository authorRepository)
        {
            _languages = languages;
            _en = _languages.FirstOrDefault(x => x.Name == "EN");
            _jobEditorValidator = jobEditorValidator;
            _authorRepository = authorRepository;
            OrgaEntities = orgaEntities;
            PredefinedFunctions = predefinedFunctions;
            Functions = new ObservableCollectionEx<JobFunctionViewModel>();
            if (Model != null)
            {
                InitFunctions(Model);
                InitViewModel(Model);
                _originalOrgaEntity = OrgaEntityViewModel.GetOrgaEntityViewModel(OrgaEntities, Model.OrgaEntityId);
                SelectedOrgaEntity = _originalOrgaEntity;
                SelectedPredefinedFunction =
                    PredefinedFunctions?.FirstOrDefault(x => x.PredefinedFunctionId == Model.PredefinedFunctionId);
            }
        }

        private void InitFunctions(IJobAssignment job)
        {
            Functions.Clear();
            foreach (var language in _languages.OrderBy(l => l.DisplayName))
            {
                var authorFunction = job.Functions.FirstOrDefault(af => af.LanguageId == language.Id);
                Functions.Add(
                    authorFunction != null
                        ? new JobFunctionViewModel(language, authorFunction)
                        : new JobFunctionViewModel(language, Model));
            }

            SelectedFunction = Functions.FirstOrDefault(x => x.Language?.Name == "EN");
        }

        private void ApplyModifiedFunctions(JobFunctionViewModel[] modifiedFunctions)
        {
            foreach (var functionVm in modifiedFunctions)
            {
                if (functionVm.JobFunction != null)
                {
                    //// update the existing function
                    var jobFunction = Model.Functions.FirstOrDefault(x => x.Id == functionVm.JobFunction.Id);
                    if (jobFunction != null)
                    {
                        jobFunction.Function = functionVm.Function;
                    }
                }
                else if (!string.IsNullOrEmpty(functionVm.Function))
                {
                    //// create a new one
                    var jobFunction = new JobFunction();
                    jobFunction.Init();
                    jobFunction.ClientModification = true;
                    jobFunction.AuthorId = functionVm.AuthorId;
                    jobFunction.JobAssignmentId = functionVm.JobAssignmentId;
                    jobFunction.LanguageId = functionVm.Language.Id;
                    jobFunction.Function = functionVm.Function;
                    Model.Functions.Add(jobFunction);
                }
            }
        }

        private void SelectOrgaEntityInTree(bool loadTexts)
        {
            if (SelectedOrgaEntity != null)
            {
                OrgaEntityViewModel.SelectAndExpandItem(OrgaEntities, SelectedOrgaEntity.Id);
                if ((SelectedOrgaEntity.HeaderText == null) && loadTexts)
                {
                    LoadTextsOfDirectorateGeneral(SelectedOrgaEntity).FireAndForgetSafeAsync(this.LogError);
                }
            }
            else
            {
                OrgaEntityViewModel.DeselectTree(OrgaEntities);
            }
        }

        private OrgaEntityViewModel FindDirectorateGeneral(OrgaEntityViewModel orgaEntity)
        {
            if (orgaEntity.OrgaEntity.LogicalLevel == 1)
            {
                return orgaEntity;
            }

            return FindDirectorateGeneral(orgaEntity.ParentEntityVm);
        }
    }
}
