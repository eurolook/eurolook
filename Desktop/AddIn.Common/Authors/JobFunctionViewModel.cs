using System;
using Eurolook.Data.Models;
using GalaSoft.MvvmLight;

namespace Eurolook.AddIn.Common.Authors
{
    public class JobFunctionViewModel : ViewModelBase
    {
        private string _function;

        public JobFunctionViewModel(Language language)
        {
            Language = language;
            IsModified = false;
        }

        public JobFunctionViewModel(Language language, IJobAssignment jobAssignment)
            : this(language)
        {
            if (jobAssignment is Author)
            {
                AuthorId = (jobAssignment as Author).Id;
                JobAssignmentId = null;
            }
            else if (jobAssignment is JobAssignment)
            {
                AuthorId = null;
                JobAssignmentId = (jobAssignment as JobAssignment).Id;
            }
        }

        public JobFunctionViewModel(Language language, JobFunction jobFunction)
            : this(language)
        {
            AuthorId = jobFunction.AuthorId;
            JobAssignmentId = jobFunction.JobAssignmentId;
            JobFunction = jobFunction;
            _function = jobFunction.Function;
        }

        public bool IsModified { get; private set; }

        public string Function
        {
            get { return _function; }
            set
            {
                _function = value;
                IsModified = true;
            }
        }

        public Guid? AuthorId { get; }

        public Guid? JobAssignmentId { get; set; }

        public Language Language { get; }

        public JobFunction JobFunction { get; }

        public override string ToString()
        {
            return Language.DisplayName;
        }
    }
}
