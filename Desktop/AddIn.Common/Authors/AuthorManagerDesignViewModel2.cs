﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using Eurolook.AddIn.Common.Wpf;

namespace Eurolook.AddIn.Common.Authors
{
    public class AuthorManagerDesignViewModel2 : AuthorManagerViewModel2
    {
        private static readonly AuthorInfoTileViewModel MyTile = new AuthorInfoTileViewModel(null)
        {
            ColorBrush = Application.Current.FindResource("PrimaryColorBrush") as SolidColorBrush,
            HeroIconPath = "/Graphics/heroicons-outline/white/user.svg",
            Title = "TEST, Entwickler",
            Description = "Abteilung 3.14, Chef von Pi",
            SecondLineDescription = "Entwickler.Test@authormanager.test",
            SecondaryCommands = new ObservableCollection<AuthorInfoTileCommand>(
                new[]
                {
                    new AuthorInfoTileCommand
                    {
                        Title = "Edit",
                        IconPath = "/Graphics/heroicons-outline/black/pencil.svg",
                    },
                }),
        };

        private static readonly AuthorInfoTileViewModel SuperiorTile = new AuthorInfoTileViewModel(null)
        {
            ColorBrush = Application.Current.FindResource("PrimaryColorBrush") as SolidColorBrush,
            HeroIconPath = "/Graphics/heroicons-outline/white/user.svg",
            Title = "QA, Developer",
            Description = "Sector 3.14 - Head of Chef of Pi",
            SecondLineDescription = "Developer.Qa@authormanager.test",
            SecondaryCommands = new ObservableCollection<AuthorInfoTileCommand>(
                new[]
                {
                    new AuthorInfoTileCommand
                    {
                        Title = "Edit",
                        IconPath = "/Graphics/heroicons-outline/black/pencil.svg",
                    },
                }),
        };

        private static readonly AuthorInfoTileViewModel SuperiorLocked = new AuthorInfoTileViewModel(null)
        {
            ColorBrush = Application.Current.FindResource("PrimaryColorBrush") as SolidColorBrush,
            HeroIconPath = "/Graphics/heroicons-outline/white/lock-closed.svg",
            Title = "DEV, Tester",
            Description = "Sector 3.14 - Director of Pi",
            SecondLineDescription = "Tester.Dev@authormanager.test",
            SecondaryCommands = new ObservableCollection<AuthorInfoTileCommand>(
                new[]
                {
                    new AuthorInfoTileCommand
                    {
                        Title = "View",
                        IconPath = "/Graphics/heroicons-outline/black/eye.svg",
                    },
                }),
        };

        private static readonly AuthorInfoTileViewModel FavoriteTile = new AuthorInfoTileViewModel(null)
        {
            ColorBrush = Application.Current.FindResource("PrimaryColorBrush") as SolidColorBrush,
            HeroIconPath = "/Graphics/heroicons-outline/white/lock-closed.svg",
            Title = "DEV, Tester",
            Description = "Sector 3.14 - Director of Pi",
            SecondLineDescription = "Tester.Dev@authormanager.test",
            SecondaryCommands = new ObservableCollection<AuthorInfoTileCommand>(
                new[]
                {
                    new AuthorInfoTileCommand
                    {
                        Title = "View",
                        IconPath = "/Graphics/heroicons-outline/black/eye.svg",
                    },
                    new AuthorInfoTileCommand
                    {
                        Title = "Remove",
                        IconPath = "/Graphics/heroicons-outline/black/trash.svg",
                    },
                }),
        };

        public AuthorManagerDesignViewModel2()
            : base(null, null, null, null, null, null, null, null, null, null, null)
        {
            MyAuthorTile = MyTile;
            MySuperiorsTiles.Add(SuperiorTile);
            MySuperiorsTiles.Add(SuperiorLocked);
            MySuperiorsTiles.Add(SuperiorLocked);
            MyFavoritesTiles.Add(FavoriteTile);

            Workplaces = new ObservableCollectionEx<WorkplaceEditorViewModel2>(
                new[]
                {
                    new WorkplaceEditorViewModel2(null, null, null) { DisplayName = "Main Workplace" },
                    new WorkplaceEditorViewModel2(null, null, null)
                    {
                        DisplayName = "Workplace 2",
                        IsSelected = true,
                    },
                    new WorkplaceEditorViewModel2(null, null, null) { DisplayName = "Workplace 3" },
                });
            SelectedWorkplace = Workplaces.FirstOrDefault();

            Jobs = new ObservableCollectionEx<JobEditorViewModel2>(
                new[]
                {
                    new JobEditorViewModel2(null, null, null, null, null)
                    {
                        DisplayName = "Main Job",
                        IsMainJob = true,
                    },
                    new JobEditorViewModel2(null, null, null, null, null) { DisplayName = "Job 2" },
                });
            SelectedJob = Jobs.FirstOrDefault();
        }
    }
}
