﻿using Eurolook.Data.Models;

namespace Eurolook.AddIn.Common.Authors
{
    public class DefaultAuthorEditorValidator : IAuthorEditorValidator
    {
        public virtual string LockedForEditingMessage => "Please contact this person if this data needs to be updated.";

        public virtual bool CanEdit(Author self, Author other)
        {
            // only allow editing own data
            return self.Id == other.Id;
        }

        public virtual bool IsValidFirstName(AuthorEditorViewModel2 viewModel)
        {
            return !string.IsNullOrWhiteSpace(viewModel.LatinFirstName);
        }

        public virtual bool IsValidLastName(AuthorEditorViewModel2 viewModel)
        {
            return !string.IsNullOrWhiteSpace(viewModel.LatinLastName);
        }

        public virtual bool IsValidInitials(AuthorEditorViewModel2 viewModel)
        {
            return !string.IsNullOrWhiteSpace(viewModel.Initials);
        }

        public virtual bool RunPlausibilityCheck(AuthorEditorViewModel2 viewModel)
        {
            return true;
        }
    }
}
