﻿using System;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;

namespace Eurolook.AddIn.Common.Authors
{
    public class AuthorSearchLightBoxViewModel : ViewModelBase
    {
        private readonly Func<AuthorSearchViewModel> _authorSearchViewModelFunc;
        private readonly Action<AuthorAndAllJobs> _pickSearchResult;
        private readonly Action _cancelSearch;
        private bool _isSearchVisible;
        private RelayCommand _pickSearchResultCommand;
        private AuthorSearchViewModel _authorSearchViewModel;

        public AuthorSearchLightBoxViewModel(
            Func<AuthorSearchViewModel> authorSearchViewModelFunc,
            Action<AuthorAndAllJobs> pickSearchResult,
            Action cancelSearch)
        {
            _authorSearchViewModelFunc = authorSearchViewModelFunc;
            _pickSearchResult = pickSearchResult;
            _cancelSearch = cancelSearch;
        }

        public AuthorSearchViewModel AuthorSearchViewModel
        {
            get => _authorSearchViewModel;
            private set
            {
                _authorSearchViewModel = value;
                RaisePropertyChanged(() => AuthorSearchViewModel);
            }
        }

        public bool IsSearchVisible
        {
            get => _isSearchVisible;
            private set => Set(() => IsSearchVisible, ref _isSearchVisible, value);
        }

        public RelayCommand PickSearchResultCommand
        {
            get => _pickSearchResultCommand;
            set
            {
                _pickSearchResultCommand = value;
                RaisePropertyChanged(() => PickSearchResultCommand);
            }
        }

        public RelayCommand CancelSearchCommand => new(CancelSearch);

        public void Show(bool singleResultPerAuthor = false)
        {
            AuthorSearchViewModel = _authorSearchViewModelFunc();
            AuthorSearchViewModel.SingleResultPerAuthor = singleResultPerAuthor;
            PickSearchResultCommand = new RelayCommand(
                PickSearchResult,
                () => AuthorSearchViewModel.IsSearchResultComplete);
            AuthorSearchViewModel.AddAuthorCommand = PickSearchResultCommand;
            IsSearchVisible = true;
            Execute.OnUiThread(
                () =>
                {
                    // It is necessary to first set the focus property to false in order to update the binding consistently.
                    // Also a delay seems to be necessary.
                    AuthorSearchViewModel.IsSearchBoxFocused = false;
                    AuthorSearchViewModel.IsSearchBoxFocused = true;
                },
                50);
        }

        public void CancelSearch()
        {
            IsSearchVisible = false;
            _cancelSearch();
        }

        private void PickSearchResult()
        {
            _pickSearchResult(AuthorSearchViewModel.SelectedAuthorAndJob);
            IsSearchVisible = false;
        }
    }
}
