using System;

namespace Eurolook.AddIn.Common.Authors
{
    public class AuthorRoleSetting
    {
        public Guid AuthorRoleId { get; set; }

        public Guid? AuthorId { get; set; }

        public Guid? JobAssignmentId { get; set; }
    }
}
