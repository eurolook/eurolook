﻿namespace Eurolook.AddIn.Common.Authors
{
    public class AuthorRoleViewModelChangedMessage
    {
        public AuthorRoleViewModelChangedMessage(AuthorRoleViewModel authorRoleViewModel, AuthorAndJob oldAuthorAndJob)
        {
            AuthorRoleViewModel = authorRoleViewModel;
            OldAuthorAndJob = oldAuthorAndJob;
        }

        public AuthorRoleViewModel AuthorRoleViewModel { get; }

        public AuthorAndJob OldAuthorAndJob { get; }

        public bool HasAuthorChanged =>
            AuthorRoleViewModel.AuthorAndJob.Author.Author?.Id.Equals(OldAuthorAndJob.Author.Author?.Id) ?? true;
    }
}
