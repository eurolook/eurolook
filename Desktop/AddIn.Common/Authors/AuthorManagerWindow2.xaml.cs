using System.Windows;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;

namespace Eurolook.AddIn.Common.Authors
{
    public partial class AuthorManagerWindow2 : Window, ICanLog
    {
        public AuthorManagerViewModel2 ViewModel { get; set; }

        public AuthorManagerWindow2(AuthorManagerViewModel2 viewModel)
        {
            ViewModel = viewModel;
            ViewModel.CloseAction = Close;
            InitializeComponent();
            Loaded += OnLoaded;
            DataContext = ViewModel;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            ViewModel.LoadAsync().FireAndForgetSafeAsync(this.LogError);
        }
    }
}
