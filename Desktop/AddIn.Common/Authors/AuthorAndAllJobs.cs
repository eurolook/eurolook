﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Common.Extensions;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.Authors
{
    public class AuthorAndAllJobs
    {
        private readonly AuthorAndJob[] _allAuthorAndJobs;

        public AuthorAndAllJobs(
            [NotNull] AuthorViewModel author,
            JobAssignmentViewModel mainJobAssignment,
            IEnumerable<JobAssignmentViewModel> additionalJobs = null,
            AuthorType authorType = AuthorType.Self,
            int orderIndex = 0)
        {
            Author = author;
            MainJobAssignment = mainJobAssignment;
            AdditionalJobs = additionalJobs ?? Array.Empty<JobAssignmentViewModel>();
            Type = Author.IsOriginatingFromDocumentCustomXml ? AuthorType.FromDocument : authorType;

            _allAuthorAndJobs = ToAuthorAndJobs().ToArray();
            OrderIndex = orderIndex;
        }

        public enum AuthorType
        {
            Self = 0,
            Superior = 1,
            Favorite = 2,
            FromDocument = 99
        }

        [NotNull]
        public AuthorViewModel Author { get; }

        public JobAssignmentViewModel MainJobAssignment { get; }

        public IEnumerable<JobAssignmentViewModel> AdditionalJobs { get; }

        public string AuthorName => Author.Title;

        public string AuthorMail => Author.Author?.Email ?? string.Empty;

        public string MainJobDescription => MainJobAssignment.ToString();

        public string AdditionalJobsDescription => string.Join("\n", AdditionalJobs.Select(job => job.ToString()));

        public bool IsDeemphasized => Author.IsOriginatingFromDocumentCustomXml;

        public bool IsSelf => Author.IsSelf;

        public AuthorType Type { get; }

        public string Category =>
            Type switch
            {
                AuthorType.Self => "Me",
                AuthorType.Superior => "My Superiors",
                AuthorType.Favorite => "My Favorites",
                AuthorType.FromDocument => "From Document",
                _ => "Unknown"
            };

        public int OrderIndex { get; }

        public IEnumerable<AuthorAndJob> All => _allAuthorAndJobs;

        public static IEnumerable<AuthorAndAllJobs> CreateAll(User user)
        {
            var authorViewModels = (author: new AuthorViewModel(user.Self, true), authorType: AuthorType.Self,
                orderIndex: 0).CreateList();
            authorViewModels.AddRange(
                user.Superiors.Select(
                    sup => (author: new AuthorViewModel(sup.Author), authorType: AuthorType.Superior,
                        orderIndex: sup.OrderIndex)));
            authorViewModels.AddRange(
                user.Authors
                    .Where(ua => authorViewModels.All(vm => vm.author.Author?.Id != ua.AuthorId))
                    .Select(
                        ua => (author: new AuthorViewModel(ua.Author), authorType: AuthorType.Favorite,
                            orderIndex: 0)));
            return authorViewModels.OrderBy(x => x.author.Title)
                                   .Select(x => Create(x.author, x.authorType, x.orderIndex));
        }

        public static AuthorAndAllJobs Create(
            AuthorViewModel authorViewModel,
            AuthorType authorType = AuthorType.Self,
            int orderIndex = 0)
        {
            return new AuthorAndAllJobs(
                authorViewModel,
                new JobAssignmentViewModel(authorViewModel.Author),
                authorViewModel.Author?.JobAssignments.Where(j => !j.Deleted)
                               .Select(j => new JobAssignmentViewModel(j)),
                authorType,
                orderIndex);
        }

        public static IEnumerable<AuthorAndAllJobs> CreateInstancePerJob(
            AuthorViewModel authorViewModel,
            AuthorType authorType = AuthorType.Self)
        {
            yield return new AuthorAndAllJobs(
                authorViewModel,
                new JobAssignmentViewModel(authorViewModel.Author),
                authorType: authorType);

            if (authorViewModel.Author != null)
            {
                foreach (var jobAssignment in authorViewModel.Author.JobAssignments.Where(j => !j.Deleted)
                                                             .Select(j => new JobAssignmentViewModel(j)))
                {
                    yield return new AuthorAndAllJobs(
                        authorViewModel,
                        jobAssignment,
                        authorType: authorType);
                }
            }
        }

        public AuthorAndJob ToAuthorAndJob()
        {
            return new AuthorAndJob(Author, MainJobAssignment);
        }

        public IEnumerable<AuthorAndJob> ToAuthorAndJobs()
        {
            yield return ToAuthorAndJob();
            foreach (var job in AdditionalJobs)
            {
                yield return new AuthorAndJob(Author, job);
            }
        }
    }
}
