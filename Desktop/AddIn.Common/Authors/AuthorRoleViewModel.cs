﻿using System;
using System.ComponentModel;
using Eurolook.Common.Log;
using Eurolook.Data.Models.AuthorRoles;
using Eurolook.DocumentProcessing.DocumentCreation;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;

namespace Eurolook.AddIn.Common.Authors
{
    public class AuthorRoleViewModel : ViewModelBase, ICanLog, IDataErrorInfo
    {
        private Guid? _initialAuthorId;
        private Guid? _initialJobAssignmentId;
        private string _niceName;
        private string _description;
        private bool _isVisible = true;
        private AuthorAndJob _authorAndJob;

        public AuthorRoleViewModel(AuthorRole authorRole, string customXmlStoreId = null)
        {
            AuthorRole = authorRole;
            CustomXmlStoreId = customXmlStoreId;
            NiceName = authorRole.DisplayName ?? authorRole.Name;
            Description = authorRole.Description;
        }

        public AuthorRoleViewModel(DocumentModelAuthorRole documentModelAuthorRole, string customXmlStoreId = null)
        : this(documentModelAuthorRole.AuthorRole, customXmlStoreId)
        {
            if (!string.IsNullOrEmpty(documentModelAuthorRole.Description))
            {
                Description = documentModelAuthorRole.Description;
            }
        }

        public string Description
        {
            get => _description;
            set
            {
                Set(() => Description, ref _description, value);
            }
        }

        public string NiceName
        {
            get => _niceName;
            set
            {
                Set(() => NiceName, ref _niceName, value);
            }
        }

        public AuthorRole AuthorRole { get; }

        public string CustomXmlStoreId { get; }

        public AuthorAndJob AuthorAndJob
        {
            get => _authorAndJob;
            set
            {
                if (_authorAndJob == null && _initialAuthorId == null && _initialJobAssignmentId == null)
                {
                    _initialAuthorId = value?.Author?.Author?.Id;
                    _initialJobAssignmentId = value?.JobAssignment?.JobAssignmentId;
                }

                var message = new AuthorRoleViewModelChangedMessage(this, _authorAndJob);
                _authorAndJob = value;
                Messenger.Default.Send(message);
                RaisePropertyChanged(() => AuthorAndJob);
                RaisePropertyChanged(() => IsModified);
            }
        }

        public AuthorViewModel Author
        {
            get => AuthorAndJob?.Author;
        }

        public JobAssignmentViewModel JobAssignment
        {
            get => AuthorAndJob?.JobAssignment;
        }

        public bool IsIncomplete
        {
            get
            {
                if (Author?.IsFixedSignatoryDummy == true)
                {
                    return false;
                }

                if (Author?.IsOriginatingFromDocumentCustomXml != true && Author?.Author?.IsIncomplete() != false)
                {
                    return true;
                }

                if (JobAssignment?.JobAssignment?.IsIncomplete() != false && JobAssignment?.IsMainJob == false)
                {
                    return true;
                }

                return false;
            }
        }

        public bool IsValid
        {
            get
            {
                if (Author?.IsOriginatingFromDocumentCustomXml == true && IsModified)
                {
                    return false;
                }

                if (IsIncomplete)
                {
                    return false;
                }

                return true;
            }
        }

        public bool IsVisible
        {
            get => _isVisible;
            set => Set(() => IsVisible, ref _isVisible, value);
        }

        public AuthorRoleMapping ToAuthorRoleMapping()
        {
            return new AuthorRoleMapping(Author.Author, JobAssignment.JobAssignment, AuthorRole);
        }

        public bool IsModified => _initialAuthorId != AuthorAndJob?.Author?.Author?.Id
                                  || _initialJobAssignmentId != AuthorAndJob?.JobAssignment?.JobAssignmentId;

        public string this[string columnName] => IsValid ? "" : "Invalid Author or Job Assignment";

        public string Error { get; }
    }
}
