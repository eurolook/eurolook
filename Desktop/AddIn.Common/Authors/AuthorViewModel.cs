﻿using Eurolook.AddIn.Common.ViewModels;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.Authors
{
    public class AuthorViewModel : NavigationListItem
    {
        private bool _isSelected;

        public AuthorViewModel(Author author, bool isSelf = false)
        {
            IsVisible = true;
            IsSelf = isSelf;
            Init(author);
        }

        public AuthorViewModel(Author author, string originatingCustomXmlPartId)
        {
            IsVisible = true;
            OriginatingCustomXmlPartId = originatingCustomXmlPartId;
            IsSelf = false;
            Init(author);
        }

        public AuthorViewModel(string displayName, bool isDummyForSearch, bool isFixedSignatoryDummy = false)
        {
            IsVisible = true;
            IsDummyForSearch = isDummyForSearch;
            IsFixedSignatoryDummy = isFixedSignatoryDummy;
            Title = displayName;
        }

        public string OriginatingCustomXmlPartId { get; }

        public bool IsOriginatingFromDocumentCustomXml => OriginatingCustomXmlPartId != null;

        public bool IsDummyForSearch { get; }

        public bool IsFixedSignatoryDummy { get; }

        [CanBeNull]
        public Author Author { get; private set; }

        public bool IsSelf { get; set; }

        public bool IsSelected
        {
            get => _isSelected;
            set { Set(() => IsSelected, ref _isSelected, value); }
        }

        public bool IsIncomplete => Author != null && Author.IsIncomplete();

        public void Init(Author author)
        {
            Author = author;
            Title = GetDisplayName(author);
        }

        public static string GetDisplayName(Author author)
        {
            if (!string.IsNullOrEmpty(author?.LatinLastName) && !string.IsNullOrEmpty(author?.LatinFirstName))
            {
                return $"{author.LatinLastName}, {author.LatinFirstName}";
            }

            if (!string.IsNullOrEmpty(author?.LatinLastName))
            {
                return author.LatinLastName;
            }

            if (!string.IsNullOrEmpty(author?.LatinFirstName))
            {
                return author.LatinFirstName;
            }

            if (!string.IsNullOrEmpty(author?.Email))
            {
                return author.Email;
            }

            return "(Incomplete Author)";
        }

        public override string ToString()
        {
            return Title + (IsOriginatingFromDocumentCustomXml ? " (from document)" : "");
        }

        public static AuthorViewModel GetSearchEntry()
        {
            return new AuthorViewModel("Search in central repository ...", true);
        }
    }
}
