﻿using System;
using System.Linq;
using Eurolook.Data.Constants;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.Authors
{
    public class JobAssignmentViewModel
    {
        private readonly IJobAssignment _jobAssignmentModel;

        public JobAssignmentViewModel()
        {
        }

        public JobAssignmentViewModel(IJobAssignment assignment)
        {
            _jobAssignmentModel = assignment;
            var orgaName = GetOrgaName();
            var function = GetFunctionName(CommonDataConstants.EnglishLanguageId);
            DisplayName = string.IsNullOrWhiteSpace(function) ? orgaName : $"{orgaName} - {function}";
        }

        [CanBeNull]
        public JobAssignment JobAssignment => _jobAssignmentModel as JobAssignment;

        [CanBeNull]
        public Guid? JobAssignmentId => JobAssignment?.Id ?? _jobAssignmentModel?.Id;

        public bool IsMainJob => _jobAssignmentModel is Author;

        public string DisplayName { get; set; }

        public override string ToString()
        {
            return DisplayName;
        }

        private string GetOrgaName()
        {
            var jobService = _jobAssignmentModel.Service;
            return !string.IsNullOrWhiteSpace(jobService)
                ? jobService
                : !string.IsNullOrWhiteSpace(_jobAssignmentModel.OrgaEntity?.Name)
                    ? _jobAssignmentModel.OrgaEntity.Name
                    : _jobAssignmentModel.OrgaEntityId.ToString();
        }

        private string GetFunctionName(Guid languageId)
        {
            if ((_jobAssignmentModel.PredefinedFunctionId != null) && (_jobAssignmentModel.PredefinedFunction != null))
            {
                return _jobAssignmentModel.PredefinedFunction.Name;
            }

            var selfDefinedFunction = _jobAssignmentModel.Functions.FirstOrDefault(f => f.LanguageId == languageId);
            if (string.IsNullOrWhiteSpace(selfDefinedFunction?.Function))
            {
                selfDefinedFunction = _jobAssignmentModel.Functions.FirstOrDefault(f => f.Function != null);
            }

            return !string.IsNullOrWhiteSpace(selfDefinedFunction?.Function) ? selfDefinedFunction.Function : null;
        }
    }
}
