using System.Linq;
using Eurolook.Data.Models;
using Eurolook.Data.Xml;

namespace Eurolook.AddIn.Common.Authors
{
    public class AuthorUpdateInfo
    {
        public AuthorUpdateInfo(AuthorCustomXmlStoreInfo authorCustomXmlStoreInfo, string description, string longDescription)
        {
            AuthorCustomXml = authorCustomXmlStoreInfo.AuthorCustomXml;
            StoreId = authorCustomXmlStoreInfo.StoreId;
            var fullName = AuthorCustomXml.Root?.Descendants("FullName").FirstOrDefault();
            if (fullName != null)
            {
                DisplayName = fullName.Value;
            }

            Description = description;
            LongDescription = longDescription;
            IsOk = false;
        }

        public AuthorUpdateInfo(AuthorCustomXmlStoreInfo authorCustomXmlStoreInfo, Author author, JobAssignment jobAssignment)
        {
            Author = author;
            JobAssignment = jobAssignment;
            AuthorCustomXml = authorCustomXmlStoreInfo.AuthorCustomXml;
            StoreId = authorCustomXmlStoreInfo.StoreId;
            var roleOrBrickTitle = AuthorCustomXml.AuthorRoleName ?? AuthorCustomXml.ContentControlTitle;
            roleOrBrickTitle = roleOrBrickTitle == null ? "" : $" ({roleOrBrickTitle})";
            DisplayName = $"{author.LatinFullName}{roleOrBrickTitle}";
            IsOk = true;
        }

        public AuthorCustomXml AuthorCustomXml { get; set; }

        public string StoreId { get; }

        public Author Author { get; set; }

        public IJobAssignment JobAssignment { get; set; }

        public string DisplayName { get; set; }

        public string Description { get; set; }

        public string LongDescription { get; set; }

        public bool IsOk { get; set; }
    }
}
