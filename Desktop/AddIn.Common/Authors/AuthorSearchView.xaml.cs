using System.Diagnostics;
using System.Windows.Navigation;

namespace Eurolook.AddIn.Common.Authors
{
    public partial class AuthorSearchView
    {
        public AuthorSearchView()
        {
            InitializeComponent();
        }

        private void OnRequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(e.Uri.AbsoluteUri);
            e.Handled = true;
        }
    }
}
