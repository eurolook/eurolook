﻿using System;
using System.Collections.ObjectModel;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.Data.Models;
using GalaSoft.MvvmLight.CommandWpf;

namespace Eurolook.AddIn.Common.Authors
{
    public class AuthorInfoTileViewModel : InfoTileViewModel
    {
        private static RelayCommand<AuthorInfoTileViewModel> _tileCommand;

        public AuthorInfoTileViewModel(Author model)
        {
            if (!IsInDesignMode)
            {
                AuthorId = model.Id;
                Model = model;
            }

            SecondaryCommands = new ObservableCollection<AuthorInfoTileCommand>();
        }

        public Guid AuthorId { get; }

        public Author Model { get; set; }

        public new RelayCommand<AuthorInfoTileViewModel> TileCommand
        {
            get { return _tileCommand; }
            set { Set(() => TileCommand, ref _tileCommand, value); }
        }

        public ObservableCollection<AuthorInfoTileCommand> SecondaryCommands { get; set; }
    }
}
