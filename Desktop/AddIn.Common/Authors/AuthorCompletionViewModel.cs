﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.StandaloneMode;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using GalaSoft.MvvmLight;

namespace Eurolook.AddIn.Common.Authors
{
    public class AuthorCompletionViewModel : ViewModelBase, IDataErrorInfo
    {
        private const string ErrorIncomplete = "Enter the missing fields.";
        private static Author _author;

        private readonly IMessageService _messageService;
        private readonly ISettingsService _settingsService;
        private readonly ILanguageRepository _languageRepository;
        private readonly IAuthorRepository _authorRepository;
        private readonly Dictionary<string, string> _errors;

        private string _latinFirstNamePlaceholder;
        private string _latinFirstName;
        private string _latinLastNamePlaceholder;
        private string _latinLastName;
        private string _initials;
        private string _error;
        private bool _isValid;
        private OrgaEntityViewModel _selectedOrgaEntityVm;
        private ObservableCollection<OrgaEntityViewModel> _orgaTree;

        public AuthorCompletionViewModel(
            IMessageService messageService,
            ISettingsService settingsService,
            ILanguageRepository languageRepository,
            IAuthorRepository authorRepository)
        {
            _messageService = messageService;
            _settingsService = settingsService;
            _languageRepository = languageRepository;
            _authorRepository = authorRepository;
            _isValid = true;
            _errors = new Dictionary<string, string>();
            OrgaTree = new ObservableCollection<OrgaEntityViewModel>();
        }

        public string LatinFirstNamePlaceholder
        {
            get => _latinFirstNamePlaceholder;
            set { Set(() => LatinFirstNamePlaceholder, ref _latinFirstNamePlaceholder, value); }
        }

        public string LatinFirstName
        {
            get => _latinFirstName;
            set { Set(() => LatinFirstName, ref _latinFirstName, value); }
        }

        public string LatinLastNamePlaceholder
        {
            get => _latinLastNamePlaceholder;
            set { Set(() => LatinLastNamePlaceholder, ref _latinLastNamePlaceholder, value); }
        }

        public string LatinLastName
        {
            get => _latinLastName;
            set { Set(() => LatinLastName, ref _latinLastName, value); }
        }

        public string Initials
        {
            get => _initials;
            set { Set(() => Initials, ref _initials, value); }
        }

        public bool IsValid
        {
            get => _isValid;
            set { Set(() => IsValid, ref _isValid, value); }
        }

        public ObservableCollection<OrgaEntityViewModel> OrgaTree
        {
            get => _orgaTree;
            set { Set(() => OrgaTree, ref _orgaTree, value); }
        }

        public OrgaEntityViewModel SelectedOrgaEntityVm
        {
            get => _selectedOrgaEntityVm;
            set { Set(() => SelectedOrgaEntityVm, ref _selectedOrgaEntityVm, value); }
        }

        public string Error
        {
            get => _error;
            set
            {
                Set(() => Error, ref _error, value);
                IsValid = value == null;
            }
        }

        public string this[string columnName] => ValidateProperty(columnName);

        public bool Confirm()
        {
            if (LatinLastName.Equals(_author.AdLatinLastName, StringComparison.InvariantCulture))
            {
                var model = new MessageViewModel
                {
                    YesButtonText = "Yes",
                    CancelButtonText = "No",
                    Title = "Spellcheck your names",
                    Message = $"Do you want to continue with your name spelled like this?\n\n{LatinFirstName} {LatinLastName}",
                };
                if (_messageService.ShowMessageWindow(model, this) != true)
                {
                    return false;
                }
            }

            _author.LatinFirstName = LatinFirstName;
            _author.LatinLastName = LatinLastName;
            _author.Initials = Initials;
            _author.OrgaEntity = SelectedOrgaEntityVm?.OrgaEntity;
            _author.OrgaEntityId = SelectedOrgaEntityVm?.OrgaEntity.Id;

            _authorRepository.UpdateAuthor(_author);

            if (_settingsService.IsStandaloneMode)
            {
                var authorBackupXml = new AuthorDataBackupXml(_author, _author.OrgaEntity);
                authorBackupXml.Save(_settingsService);
            }

            return true;
        }

        public async Task InitAsync(Author author)
        {
            _author = author;

            LatinFirstName = author.LatinFirstName;
            LatinLastName = author.LatinLastName;
            LatinFirstNamePlaceholder = author.AdLatinFirstName;
            LatinLastNamePlaceholder = author.AdLatinLastName;
            Initials = author.Initials;

            var en = _languageRepository.GetEnglishLanguage();
            OrgaTree.Clear();
            await _authorRepository.LoadOrgaTreeAsync(OrgaTree, en);

            SelectedOrgaEntityVm = OrgaEntityViewModel.SelectAndExpandItem(OrgaTree, _author.OrgaEntityId);

            ValidateForm();
        }

        public void ValidateForm()
        {
            ValidateProperty(nameof(LatinFirstName));
            ValidateProperty(nameof(LatinLastName));
            ValidateProperty(nameof(Initials));
            ValidateProperty(nameof(SelectedOrgaEntityVm));
        }

        private string ValidateProperty(string propertyName)
        {
            string result = string.Empty;
            if (_author == null)
            {
                // init state: don't validate
                return result;
            }

            if (propertyName == nameof(LatinFirstName))
            {
                if (string.IsNullOrWhiteSpace(LatinFirstName))
                {
                    _errors[propertyName] = ErrorIncomplete;
                }
                else
                {
                    _errors.Remove(propertyName);
                }
            }

            if (propertyName == nameof(LatinLastName))
            {
                if (string.IsNullOrWhiteSpace(LatinLastName))
                {
                    _errors[propertyName] = ErrorIncomplete;
                }
                else
                {
                    _errors.Remove(propertyName);
                }
            }

            if (propertyName == nameof(Initials))
            {
                if (string.IsNullOrWhiteSpace(Initials))
                {
                    _errors[propertyName] = ErrorIncomplete;
                }
                else
                {
                    _errors.Remove(propertyName);
                }
            }

            if (propertyName == nameof(SelectedOrgaEntityVm))
            {
                if (SelectedOrgaEntityVm == null)
                {
                    _errors[propertyName] = ErrorIncomplete;
                }
                else
                {
                    _errors.Remove(propertyName);
                }
            }

            Error = _errors.Any() ? ErrorIncomplete : null;
            return _errors.ContainsKey(propertyName) ? ErrorIncomplete : null;
        }
    }
}
