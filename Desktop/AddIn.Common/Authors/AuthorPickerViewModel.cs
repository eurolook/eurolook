﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Data;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using static Eurolook.AddIn.Common.Authors.AuthorAndAllJobs;

namespace Eurolook.AddIn.Common.Authors
{
    public class AuthorPickerViewModel : ViewModelBase, ICanLog
    {
        private readonly CollectionViewSource _authorsSource = new();

        private AuthorAndAllJobs _selectedAuthor;
        private bool _isSelectionComplete;
        private RelayCommand _pickAuthorCommand;

        public AuthorAndAllJobs SelectedAuthor
        {
            get => _selectedAuthor;
            set
            {
                Set(() => SelectedAuthor, ref _selectedAuthor, value);
                IsSelectionComplete = _selectedAuthor is { Author.IsIncomplete: false } or
                    { Type: AuthorType.FromDocument };
            }
        }

        public bool IsSelectionComplete
        {
            get => _isSelectionComplete;
            set => Set(() => IsSelectionComplete, ref _isSelectionComplete, value);
        }

        public ICollectionView Authors => _authorsSource.View;

        public RelayCommand PickAuthorCommand
        {
            get => _pickAuthorCommand;
            set
            {
                _pickAuthorCommand = value;
                RaisePropertyChanged(() => PickAuthorCommand);
            }
        }

        public void RefreshData(IEnumerable<AuthorAndAllJobs> authors)
        {
            _authorsSource.Source = authors;

            Authors.SortDescriptions.Clear();
            Authors.SortDescriptions.Add(new SortDescription { PropertyName = nameof(AuthorAndAllJobs.Type) });
            Authors.SortDescriptions.Add(
                new SortDescription { PropertyName = nameof(AuthorAndAllJobs.OrderIndex) });
            Authors.SortDescriptions.Add(
                new SortDescription { PropertyName = nameof(AuthorAndAllJobs.AuthorName) });

            Authors.GroupDescriptions.Clear();
            Authors.GroupDescriptions.Add(
                new PropertyGroupDescription
                {
                    PropertyName = nameof(AuthorAndAllJobs.Category),
                    SortDescriptions = { new SortDescription { PropertyName = nameof(AuthorAndAllJobs.Type) } }
                });

            Authors.Refresh();
            RaisePropertyChanged(() => Authors);

            SelectedAuthor = null;
        }
    }

    public class AuthorPickerDesignViewModel : AuthorPickerViewModel
    {
        public AuthorPickerDesignViewModel()
        {
            var authors = new[]
            {
                new AuthorAndAllJobs(
                    new AuthorViewModel(
                        new Author
                        {
                            LatinFirstName = "Entwickler",
                            LatinLastName = "Test",
                            Service = "Abteilung 3.14"
                        }) { IsSelf = true },
                    new JobAssignmentViewModel(new JobAssignment { Service = "Abteilung 3.14" }),
                    new[]
                    {
                        new JobAssignmentViewModel(new JobAssignment { Service = "Abteilung 3.14" }),
                        new JobAssignmentViewModel(new JobAssignment { Service = "Abteilung 3.14" })
                    }),
                new AuthorAndAllJobs(
                    new AuthorViewModel(
                        new Author
                        {
                            LatinFirstName = "Entwickler",
                            LatinLastName = "Test",
                            Service = "Abteilung 3.14"
                        }) { IsSelf = false },
                    new JobAssignmentViewModel(new JobAssignment { Service = "Abteilung 3.14" }),
                    authorType: AuthorType.Superior,
                    orderIndex: 0),
                new AuthorAndAllJobs(
                    new AuthorViewModel(
                        new Author
                        {
                            LatinFirstName = "Entwickler",
                            LatinLastName = "Test",
                            Service = "Abteilung 3.14"
                        }) { IsSelf = false },
                    new JobAssignmentViewModel(new JobAssignment { Service = "Abteilung 3.14" }),
                    authorType: AuthorType.Superior,
                    orderIndex: 1),
                new AuthorAndAllJobs(
                    new AuthorViewModel(
                        new Author
                        {
                            LatinFirstName = "Entwickler",
                            LatinLastName = "Test",
                            Service = "Abteilung 3.14"
                        }) { IsSelf = false },
                    new JobAssignmentViewModel(new JobAssignment { Service = "Abteilung 3.14" }),
                    authorType: AuthorType.Favorite)
            };

            RefreshData(authors);
        }
    }
}
