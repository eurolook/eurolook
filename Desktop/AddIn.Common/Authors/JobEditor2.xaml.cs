using System.Windows;
using System.Windows.Controls;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;

namespace Eurolook.AddIn.Common.Authors
{
    public partial class JobEditor2 : ICanLog
    {
        public JobEditor2()
        {
            InitializeComponent();
        }

        private void OnOrgaTreeViewItemExpanded(object sender, RoutedEventArgs e)
        {
            if (DataContext is JobEditorViewModel2 viewModel && viewModel.IsSelected)
            {
                // load the texts of the DG if a node is expanded
                var treeViewItem = e.OriginalSource as TreeViewItem;
                if (treeViewItem?.DataContext is OrgaEntityViewModel orgaEntity && orgaEntity.IsExpanded)
                {
                    viewModel.LoadTextsOfDirectorateGeneral(orgaEntity)
                             .FireAndForgetSafeAsync(this.LogError);
                }
            }
        }

        private void OnOrgaTreeSelectionChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (DataContext is JobEditorViewModel2 jobEditor && e.NewValue != null)
            {
                if (jobEditor.IsSelected && e.NewValue is OrgaEntityViewModel newValue && newValue.IsSelected)
                {
                    // select the TreeViewItem anyway
                    if (sender is TreeView treeView)
                    {
                        var treeViewItem = treeView.ItemContainerGenerator.ContainerFromItemRecursive(newValue);
                        treeViewItem?.BringIntoView(new Rect(0, 0, 100, 100));
                    }

                    // in case a user selected a node in the UI, set the JobEditorViewModel
                    if (jobEditor.SelectedOrgaEntity != newValue)
                    {
                        jobEditor.SelectedOrgaEntity = newValue;
                        jobEditor.Validate();
                    }
                }
            }
        }
    }
}
