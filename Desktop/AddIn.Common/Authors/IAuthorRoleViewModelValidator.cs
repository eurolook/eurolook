using System.Collections.Generic;

namespace Eurolook.AddIn.Common.Authors
{
    public interface IAuthorRoleViewModelValidator
    {
        void Validate(List<AuthorRoleViewModel> authorRoleViewModels);
    }
}
