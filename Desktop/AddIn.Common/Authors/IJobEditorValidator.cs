﻿namespace Eurolook.AddIn.Common.Authors
{
    public interface IJobEditorValidator
    {
        bool IsValidOrgaEntity(JobEditorViewModel2 jobEditorViewModel);

        bool IsValidAcronym(JobEditorViewModel2 jobEditorViewModel);
    }
}
