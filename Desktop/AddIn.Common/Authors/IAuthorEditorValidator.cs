﻿using Eurolook.Data.Models;

namespace Eurolook.AddIn.Common.Authors
{
    public interface IAuthorEditorValidator
    {
        string LockedForEditingMessage { get; }

        bool CanEdit(Author self, Author other);

        bool IsValidFirstName(AuthorEditorViewModel2 viewModel);

        bool IsValidLastName(AuthorEditorViewModel2 viewModel);

        bool IsValidInitials(AuthorEditorViewModel2 viewModel);

        bool RunPlausibilityCheck(AuthorEditorViewModel2 viewModel);
    }
}
