﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Media;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.Data.Models;
using GalaSoft.MvvmLight.CommandWpf;

namespace Eurolook.AddIn.Common.Authors
{
    public class AuthorEditorViewModel2 : CancelableViewModel<Author>, IDataErrorInfo
    {
        private readonly Dictionary<string, string> _errors = new Dictionary<string, string>();
        private readonly IAuthorEditorValidator _authorEditorValidator;
        private readonly ISettingsService _settingsService;

        private bool _isLocked;
        private string _latinFirstName;
        private string _latinLastName;
        private string _greekFirstName;
        private string _greekLastName;
        private string _cyrillicFirstName;
        private string _cyrillicLastName;
        private string _initials;
        private FormOfAddressViewModel _selectedFormOfAddress;
        private string _email;
        private bool _isEmailReadOnly;
        private string _titleName;
        private string _mainJobLine1;
        private string _mainJobLine2;
        private string _mainJobLine3;
        private string _mainWorkplaceLine1;
        private string _mainWorkplaceLine2;
        private string _mainWorkplaceLine3;
        private RelayCommand _showJobEditorCommand;
        private RelayCommand _showWorkplaceEditorCommand;
        private string _error;
        private bool _isSelf;
        private string _notes;
        private SolidColorBrush _notesColor;

        public AuthorEditorViewModel2(
            IAuthorEditorValidator authorEditorValidator,
            ISettingsService settingsService)
        {
                _authorEditorValidator = authorEditorValidator;
                _settingsService = settingsService;
                FormOfAddressItems = new ObservableCollection<FormOfAddressViewModel>(
                new[]
                {
                    new FormOfAddressViewModel("", null),
                    new FormOfAddressViewModel("Mr", "m"),
                    new FormOfAddressViewModel("Ms", "f"),
                });
        }

        public Author Self { get; set; }

        public Language English { get; set; }

        public string TitleName
        {
            get => _titleName;
            set => Set(() => TitleName, ref _titleName, value);
        }

        public bool IsLocked
        {
            get => _isLocked;
            set
            {
                Set(() => IsLocked, ref _isLocked, value);
                RaisePropertyChanged(() => SaveTooltip);
                RaisePropertyChanged(() => CancelTooltip);
                RaisePropertyChanged(() => CancelButtonName);
                if (_isLocked)
                {
                    IsEmailReadOnly = true;
                }
            }
        }

        public string Notes
        {
            get => _notes;
            set => Set(() => Notes, ref _notes, value);
        }

        public SolidColorBrush NotesColor
        {
            get => _notesColor;
            set => Set(() => NotesColor, ref _notesColor, value);
        }

        public string LatinFirstName
        {
            get => _latinFirstName;
            set => Set(() => LatinFirstName, ref _latinFirstName, value);
        }

        public string LatinLastName
        {
            get => _latinLastName;
            set => Set(() => LatinLastName, ref _latinLastName, value);
        }

        public string GreekFirstName
        {
            get => _greekFirstName;
            set => Set(() => GreekFirstName, ref _greekFirstName, value);
        }

        public string GreekLastName
        {
            get => _greekLastName;
            set => Set(() => GreekLastName, ref _greekLastName, value);
        }

        public string CyrillicFirstName
        {
            get => _cyrillicFirstName;
            set => Set(() => CyrillicFirstName, ref _cyrillicFirstName, value);
        }

        public string CyrillicLastName
        {
            get => _cyrillicLastName;
            set => Set(() => CyrillicLastName, ref _cyrillicLastName, value);
        }

        public string Initials
        {
            get => _initials;
            set => Set(() => Initials, ref _initials, value);
        }

        public ObservableCollection<FormOfAddressViewModel> FormOfAddressItems { get; set; }

        public FormOfAddressViewModel SelectedFormOfAddress
        {
            get => _selectedFormOfAddress;
            set => Set(() => SelectedFormOfAddress, ref _selectedFormOfAddress, value);
        }

        public string Email
        {
            get => _email;
            set => Set(() => Email, ref _email, value);
        }

        public bool IsEmailReadOnly
        {
            get => _isEmailReadOnly;
            set => Set(() => IsEmailReadOnly, ref _isEmailReadOnly, value);
        }

        public string MainJobLine1
        {
            get => _mainJobLine1;
            set => Set(() => MainJobLine1, ref _mainJobLine1, value);
        }

        public string MainJobLine2
        {
            get => _mainJobLine2;
            set => Set(() => MainJobLine2, ref _mainJobLine2, value);
        }

        public string MainJobLine3
        {
            get => _mainJobLine3;
            set => Set(() => MainJobLine3, ref _mainJobLine3, value);
        }

        public string MainWorkplaceLine1
        {
            get => _mainWorkplaceLine1;
            set => Set(() => MainWorkplaceLine1, ref _mainWorkplaceLine1, value);
        }

        public string MainWorkplaceLine2
        {
            get => _mainWorkplaceLine2;
            set => Set(() => MainWorkplaceLine2, ref _mainWorkplaceLine2, value);
        }

        public string MainWorkplaceLine3
        {
            get => _mainWorkplaceLine3;
            set => Set(() => MainWorkplaceLine3, ref _mainWorkplaceLine3, value);
        }

        public RelayCommand ShowJobEditorCommand
        {
            get => _showJobEditorCommand;
            set => Set(() => ShowJobEditorCommand, ref _showJobEditorCommand, value);
        }

        public RelayCommand ShowWorkplaceEditorCommand
        {
            get => _showWorkplaceEditorCommand;
            set => Set(() => ShowWorkplaceEditorCommand, ref _showWorkplaceEditorCommand, value);
        }

        public string Error
        {
            get => _error;
            set
            {
                Set(() => Error, ref _error, value);
                RaisePropertyChanged(() => IsValid);
            }
        }

        public bool IsValid => Error == null;

        public string SaveTooltip
        {
            get => IsLocked ? "" : "Save the author.";
        }

        public string CancelTooltip
        {
            get => IsLocked ? "Go back." : "Don't save the author, go back.";
        }

        public string CancelButtonName
        {
            get => IsLocked ? "Close" : "Cancel";
        }

        protected override void InitViewModel(Author author)
        {
            if (author == null)
            {
                return;
            }

            TitleName = GetTitleName(author);
            LatinFirstName = author.LatinFirstName;
            LatinLastName = author.LatinLastName;
            GreekFirstName = author.GreekFirstName;
            GreekLastName = author.GreekLastName;
            CyrillicFirstName = author.BulgarianFirstName;
            CyrillicLastName = author.BulgarianLastName;
            Initials = author.Initials;
            Email = author.Email;
            SelectedFormOfAddress = FormOfAddressItems.FirstOrDefault(x => x.Value == author.Gender);
            UpdateJobPreview(
                author,
                author.JobAssignments.Count + 1);
            UpdateWorkplacePreview(
                author.Workplaces.FirstOrDefault(x => x.Id == author.MainWorkplaceId),
                author.Workplaces.Count);
            Notes = "Use accented characters and lowercase where appropriate.";
            NotesColor = new SolidColorBrush(Colors.LightGoldenrodYellow);

            _isSelf = Self.Id == author.Id;
            bool canEdit = _isSelf || _authorEditorValidator?.CanEdit(Self, author) == true;

            if (!canEdit && _authorEditorValidator != null)
            {
                Notes = _authorEditorValidator.LockedForEditingMessage;
                NotesColor = new SolidColorBrush(Color.FromRgb(255, 215, 215));
            }

            IsLocked = !canEdit;

            IsEmailReadOnly = !_settingsService.IsStandaloneMode;
            Validate();
        }

        public bool RunPlausibilityCheck()
        {
            return _authorEditorValidator.RunPlausibilityCheck(this);
        }

        public override void Apply()
        {
            TitleName = GetTitleName(Model);
            Model.LatinFirstName = LatinFirstName;
            Model.LatinLastName = LatinLastName;
            Model.GreekFirstName = GreekFirstName;
            Model.GreekLastName = GreekLastName;
            Model.BulgarianFirstName = CyrillicFirstName;
            Model.BulgarianLastName = CyrillicLastName;
            Model.Initials = Initials;
            Model.Email = Email;
            Model.Gender = SelectedFormOfAddress.Value;
        }

        public override void Cancel()
        {
            if (Model == null)
            {
                return;
            }

            TitleName = GetTitleName(Model);
            LatinFirstName = Model.LatinFirstName;
            LatinLastName = Model.LatinLastName;
            GreekFirstName = Model.GreekFirstName;
            GreekLastName = Model.GreekLastName;
            CyrillicFirstName = Model.BulgarianFirstName;
            CyrillicLastName = Model.BulgarianLastName;
            Initials = Model.Initials;
            Email = Model.Email;
            SelectedFormOfAddress = FormOfAddressItems.FirstOrDefault(x => x.Value == Model.Gender);
        }

        public void Validate()
        {
            _ = this[nameof(LatinFirstName)];
            _ = this[nameof(LatinLastName)];
            _ = this[nameof(Initials)];
        }

        public string this[string columnName]
        {
            get
            {
                if (Model == null)
                {
                    // init state: no author has been selected, don't validate
                    return null;
                }

                if (columnName == nameof(LatinFirstName))
                {
                    if (_authorEditorValidator.IsValidFirstName(this))
                    {
                        _errors.Remove(columnName);
                    }
                    else
                    {
                        _errors[columnName] = "Enter a Latin first name";
                    }
                }

                if (columnName == nameof(LatinLastName))
                {
                    if (_authorEditorValidator.IsValidLastName(this))
                    {
                        _errors.Remove(columnName);
                    }
                    else
                    {
                        _errors[columnName] = "Enter a Latin last name";
                    }
                }

                if (columnName == nameof(Initials))
                {
                    if (_authorEditorValidator.IsValidInitials(this))
                    {
                        _errors.Remove(columnName);
                    }
                    else
                    {
                        _errors[columnName] = "Enter your initials";
                    }
                }

                if (_errors.Count > 0 && _isSelf)
                {
                    Error = "All fields with a red border are mandatory.";
                }
                else if (_errors.Count > 0)
                {
                    Error = "Some required information is missing.";
                }
                else
                {
                    Error = null;
                }

                return _errors.ContainsKey(columnName) ? _errors[columnName] : null;
            }
        }

        public void UpdateJobPreview(IJobAssignment job, int jobCount)
        {
            MainJobLine1 = job.Service;
            MainJobLine2 = job.PredefinedFunction?.Name;
            if (string.IsNullOrWhiteSpace(MainJobLine2))
            {
                var jobFunction = job.Functions.FirstOrDefault(x => x.LanguageId == English?.Id)
                                  ?? job.Functions.FirstOrDefault();
                MainJobLine2 = jobFunction != null ? jobFunction.Function : "-";
            }

            UpdateJobPreview(jobCount);
        }

        internal void UpdateJobPreview(int jobCount)
        {
            int more = jobCount - 1;
            MainJobLine3 = more > 0 ? $"{more} more..." : "";
        }

        public void UpdateWorkplacePreview(Workplace workplace, int workplaceCount)
        {
            if (workplace == null)
            {
                return;
            }

            MainWorkplaceLine1 = workplace.Address.Name;
            MainWorkplaceLine2 = $"Office {workplace.Office}, {workplace.Address.PhoneNumberPrefix}{workplace.PhoneExtension}";
            int more = workplaceCount - 1;
            if (more > 0)
            {
                MainWorkplaceLine3 = $"{more} more...";
            }
        }

        private string GetTitleName(Author author)
        {
            if (!string.IsNullOrWhiteSpace(author.AdLatinFirstName)
                && !string.IsNullOrWhiteSpace(author.AdLatinLastName))
            {
                return $"{author.AdLatinFirstName} {author.AdLatinLastName}";
            }

            return $"{author.LatinFirstName} {author.LatinLastName}";
        }
    }
}
