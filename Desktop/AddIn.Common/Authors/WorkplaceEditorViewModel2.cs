﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Data.Constants;
using Eurolook.Data.Models;

namespace Eurolook.AddIn.Common.Authors
{
    public class WorkplaceEditorViewModel2 : CancelableViewModel<Workplace>, IDataErrorInfo
    {
        private readonly IWorkplaceEditorValidator _workplaceEditorValidator;
        private readonly Dictionary<string, string> _errors = new();

        private AddressViewModel _selectedAddress;
        private string _phoneFaxPrefix;
        private string _phoneExtension;
        private string _faxExtension;
        private string _office;
        private string _displayName;
        private bool _isSelected;
        private string _error;
        private bool _isLocked;
        private bool _isNew;

        public WorkplaceEditorViewModel2(
            Workplace workplace,
            IEnumerable<Address> addresses,
            IWorkplaceEditorValidator workplaceEditorValidator)
            : base(workplace)
        {
            _workplaceEditorValidator = workplaceEditorValidator;

            Addresses = new ObservableCollectionEx<AddressViewModel>();
            if (addresses != null)
            {
                Addresses.AddRange(addresses.OrderBy(x => x.Name).Select(x => new AddressViewModel(x)));
            }

            SelectedAddress = Addresses.FirstOrDefault(x => x.Address.Id == workplace.AddressId);
        }

        public ObservableCollectionEx<AddressViewModel> Addresses { get; set; }

        public bool IsSelected
        {
            get => _isSelected;
            set => Set(() => IsSelected, ref _isSelected, value);
        }

        public AddressViewModel SelectedAddress
        {
            get => _selectedAddress;
            set
            {
                Set(() => SelectedAddress, ref _selectedAddress, value);
                PhoneFaxPrefix = SelectedAddress?.Address?.PhoneNumberPrefix;
                RaisePropertyChanged(() => AreTextboxesEnabled);
            }
        }

        public string DisplayName
        {
            get => _displayName;
            set => Set(() => DisplayName, ref _displayName, value);
        }

        public bool AreTextboxesEnabled
        {
            get => SelectedAddress?.Address.Id != CommonDataConstants.NoAddressId;
        }

        public string Office
        {
            get => _office;
            set
            {
                Set(() => Office, ref _office, value);
            }
        }

        public string PhoneFaxPrefix
        {
            get => _phoneFaxPrefix;
            set => Set(() => PhoneFaxPrefix, ref _phoneFaxPrefix, value);
        }

        public string PhoneExtension
        {
            get => _phoneExtension;
            set
            {
                Set(() => PhoneExtension, ref _phoneExtension, value);
            }
        }

        public string FaxExtension
        {
            get => _faxExtension;
            set => Set(() => FaxExtension, ref _faxExtension, value);
        }

        public bool IsLocked
        {
            get => _isLocked;
            set
            {
                Set(() => IsLocked, ref _isLocked, value);
                RaisePropertyChanged(() => SaveTooltip);
                RaisePropertyChanged(() => CancelTooltip);
            }
        }

        public string SaveTooltip
        {
            get => IsLocked ? "" : "Save the workplace(s).";
        }

        public string CancelTooltip
        {
            get => IsLocked ? "Go back." : "Don't save the workplace(s), go back.";
        }

        public bool IsNew
        {
            get => _isNew;
            set
            {
                Set(() => IsNew, ref _isNew, value);
                RaisePropertyChanged(() => IsDeletable);
            }
        }

        public bool IsDeletable => !IsLocked && !IsMainWorkplace && !IsNew;

        protected override void InitViewModel(Workplace workplace)
        {
            PhoneExtension = workplace.PhoneExtension;
            FaxExtension = workplace.FaxExtension;
            Office = workplace.Office;
        }

        public override void Cancel()
        {
            SelectedAddress = Addresses.FirstOrDefault(x => x.Address.Id == Model.AddressId);
            PhoneFaxPrefix = SelectedAddress?.Address?.PhoneNumberPrefix;
            PhoneExtension = Model.PhoneExtension;
            FaxExtension = Model.FaxExtension;
            Office = Model.Office;
        }

        public override void Apply()
        {
            if (SelectedAddress?.Address != null)
            {
                Model.AddressId = SelectedAddress.Address.Id;
                Model.Address = SelectedAddress.Address;
            }

            if (Model.AddressId == CommonDataConstants.NoAddressId)
            {
                Model.PhoneExtension = string.Empty;
                Model.FaxExtension = string.Empty;
                Model.Office = string.Empty;
            }
            else
            {
                Model.PhoneExtension = PhoneExtension;
                Model.FaxExtension = FaxExtension;
                Model.Office = Office;
            }
        }

        public void Validate()
        {
            _ = this[nameof(SelectedAddress)];
        }

        public string this[string columnName]
        {
            get
            {
                if (columnName == nameof(SelectedAddress))
                {
                    if (_workplaceEditorValidator.IsValidAddress(this))
                    {
                        _errors.Remove(columnName);
                    }
                    else
                    {
                        _errors[columnName] = "Select an address.";
                    }
                }

                Error = _errors.Count > 0 ? "All fields with a red border are mandatory." : null;
                return _errors.ContainsKey(columnName) ? _errors[columnName] : null;
            }
        }

        public string Error
        {
            get => _error;
            set
            {
                Set(() => Error, ref _error, value);
                RaisePropertyChanged(() => IsValid);
            }
        }

        public bool IsValid => Error == null;

        public bool IsMainWorkplace { get; internal set; }
    }
}
