using Eurolook.Data.Constants;

namespace Eurolook.AddIn.Common.Authors
{
    public class WorkplaceEditorValidator : IWorkplaceEditorValidator
    {
        public bool IsValidAddress(WorkplaceEditorViewModel2 workplaceEditor)
        {
            var selectedAddressId = workplaceEditor.SelectedAddress?.Address.Id;
            if (selectedAddressId == null || (workplaceEditor.IsMainWorkplace && selectedAddressId == CommonDataConstants.NoAddressId))
            {
                return false;
            }

            return true;
        }
    }
}
