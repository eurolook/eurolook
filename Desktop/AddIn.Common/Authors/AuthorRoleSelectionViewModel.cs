﻿using System.Collections.Generic;
using System.Linq;
using Eurolook.Common.Extensions;
using Eurolook.Data.Models.AuthorRoles;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.Authors
{
    public class AuthorRoleSelectionViewModel : AuthorRoleViewModel
    {
        private AuthorAndAllJobs _authorAndAllJobs;

        public AuthorRoleSelectionViewModel(AuthorRole authorRole, string customXmlStoreId = null)
            : base(authorRole, customXmlStoreId)
        {
        }

        public AuthorRoleSelectionViewModel(
            DocumentModelAuthorRole documentModelAuthorRole,
            string customXmlStoreId = null)
            : base(documentModelAuthorRole, customXmlStoreId)
        {
        }

        public AuthorAndAllJobs AuthorAndAllJobs
        {
            get => _authorAndAllJobs;
            set
            {
                _authorAndAllJobs = value;
                RaisePropertyChanged(() => AuthorAndAllJobs);
                RaisePropertyChanged(() => AvailableJobs);
                RaisePropertyChanged(() => HasMultipleJobs);

                if ((value == null) || !value.AdditionalJobs.IsNullOrEmpty())
                {
                    SelectedJob = null;
                }
                else
                {
                    SelectedJob = value.All.First();
                }
            }
        }

        public IEnumerable<AuthorAndJob> AvailableJobs => _authorAndAllJobs?.All ?? new List<AuthorAndJob>();

        public bool HasMultipleJobs => AvailableJobs.Count() > 1;

        [CanBeNull]
        public AuthorAndJob JobAssignmentFromSettings { get; set; }

        public AuthorAndJob SelectedJob
        {
            get => AuthorAndJob;
            set
            {
                AuthorAndJob = value;
                RaisePropertyChanged(() => SelectedJob);
            }
        }

        public static AuthorRoleSelectionViewModel FromAuthorRoleViewModel(AuthorRoleViewModel vm)
        {
            return new AuthorRoleSelectionViewModel(vm.AuthorRole, vm.CustomXmlStoreId);
        }
    }
}
