namespace Eurolook.AddIn.Common.Authors
{
    public class FormOfAddressViewModel
    {
        public FormOfAddressViewModel(string displayName, string value)
        {
            DisplayName = displayName;
            Value = value;
        }

        public string DisplayName { get; set; }

        public string Value { get; set; }

        public override string ToString()
        {
            return DisplayName;
        }
    }
}
