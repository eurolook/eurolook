using System.Threading.Tasks;
using Eurolook.Data.Models;
using Eurolook.Data.Xml;
using Eurolook.DocumentProcessing.DocumentCreation;

namespace Eurolook.AddIn.Common.Authors
{
    public interface IAuthorDataLoader
    {
        void LoadCompleteAuthorData(AuthorRoleMapping authorRolesMapping, Language language);

        Task<AuthorUpdateInfo> GetAuthorInfoFromDbOrServerAsync(AuthorCustomXmlStoreInfo authorCustomXmlStoreInfo, Language language);
    }
}
