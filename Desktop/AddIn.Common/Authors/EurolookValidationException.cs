using System;
using System.Runtime.Serialization;

namespace Eurolook.AddIn.Common.Authors
{
    // TODO: Rename to sth more specific, e.g. AuthorConstrainedViolationException?
    [Serializable]
    public class EurolookValidationException : Exception
    {
        public EurolookValidationException()
        {
        }

        public EurolookValidationException(string message)
            : base(message)
        {
        }

        public EurolookValidationException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected EurolookValidationException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}
