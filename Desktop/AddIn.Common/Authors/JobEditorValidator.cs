﻿namespace Eurolook.AddIn.Common.Authors
{
    public class JobEditorValidator : IJobEditorValidator
    {
        public bool IsValidOrgaEntity(JobEditorViewModel2 jobEditorViewModel)
        {
            return jobEditorViewModel.SelectedOrgaEntity != null;
        }

        public bool IsValidAcronym(JobEditorViewModel2 jobEditorViewModel)
        {
            return !string.IsNullOrWhiteSpace(jobEditorViewModel.Acronym);
        }
    }
}
