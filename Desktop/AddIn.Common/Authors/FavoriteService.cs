using System.Collections.Generic;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.Messages;
using Eurolook.Data.Models;
using GalaSoft.MvvmLight.Messaging;

namespace Eurolook.AddIn.Common.Authors
{
    public class FavoriteService : IFavoriteService
    {
        private readonly IUserDataRepository _userDataRepository;
        private User _user;

        public FavoriteService(IUserDataRepository userDataRepository)
        {
            _userDataRepository = userDataRepository;
            Messenger.Default.Register<LocalDataDeletedMessage>(this, OnLocalDataDeleted);
        }

        private void OnLocalDataDeleted(LocalDataDeletedMessage obj)
        {
            // clear the cached information
            _user = null;
        }

        private User User => _user ??= _userDataRepository.GetUser();

        public void AddFavoriteIfNotExisting(Author author)
        {
            if (_userDataRepository.IsSuperior(User, author))
            {
                return;
            }

            bool added = _userDataRepository.AddAuthorToUser(User, author);
            if (added)
            {
                Messenger.Default.Send(new AuthorAddedMessage(author.Id));
            }
        }

        public void AddFavoritesIfNotExisting(IEnumerable<Author> authors)
        {
            foreach (var author in authors)
            {
                AddFavoriteIfNotExisting(author);
            }
        }

        public void RemoveFavorite(Author author)
        {
            _userDataRepository.DeleteAuthorFromUser(User.Id, author);
        }
    }
}
