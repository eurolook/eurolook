﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.DataSync.HttpClient;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using static Eurolook.AddIn.Common.Authors.AuthorAndAllJobs;

namespace Eurolook.AddIn.Common.Authors
{
    public class AuthorSearchViewModel : ViewModelBase, IDisposable, ICanLog
    {
        // NOTE: There are people with very short last names, such as "Ms LE"
        //   The search should be able to retrieve everyone just by their last name
        private const int MinLength = 1;
        private const int PageSize = 30;

        private readonly ISettingsService _settingsService;
        private readonly Timer _searchTimer;
        private readonly SearchClient _searchClient;

        private string _searchText;
        private string _message;
        private bool _isSearchBoxFocused;
        private bool _hasSingleResultPerAuthor;
        private RelayCommand _loadMoreResultsCommand;
        private int _currentPage;
        private bool _hasMoreResults = true;
        private bool _isSearchResultComplete;
        private bool _isSearching;
        private CancellationTokenSource _tokenSource;
        private bool _isSearchNotFound;
        private string _messageLink;
        private AuthorAndAllJobs _selectedAuthorAndJob;
        private RelayCommand _addAuthorCommand;

        public AuthorSearchViewModel(ISettingsService settingsService)
        {
            _settingsService = settingsService;
            _searchTimer = new Timer(SearchTimerElapsed);
            _searchClient = new SearchClient(settingsService?.DataSyncUrl);
            StartSearchCommand = new RelayCommand(StartSearch);
            SearchResults = new ObservableCollectionEx<AuthorAndAllJobs>();
        }

        ~AuthorSearchViewModel()
        {
            Dispose(false);
        }

        public string SearchText
        {
            get => _searchText ?? "";
            set => Set(() => SearchText, ref _searchText, value);
        }

        public ObservableCollectionEx<AuthorAndAllJobs> SearchResults { get; }

        public AuthorAndAllJobs SelectedAuthorAndJob
        {
            get => _selectedAuthorAndJob;
            set
            {
                Set(() => SelectedAuthorAndJob, ref _selectedAuthorAndJob, value);
                IsSearchResultComplete = _selectedAuthorAndJob is { Author.IsIncomplete: false };
                RaisePropertyChanged(() => IsIncompleteAuthorMessageVisible);
                RaisePropertyChanged(() => IncompleteAuthorMessage);
            }
        }

        public bool HasSearchResults => SearchResults.Any();

        public bool IsSearching
        {
            get => _isSearching;
            set => Set(() => IsSearching, ref _isSearching, value);
        }

        public bool IsMessageVisible => !(HasSearchResults || string.IsNullOrEmpty(SearchText));

        public string Message
        {
            get => _message;
            set
            {
                Set(() => Message, ref _message, value);
                RaisePropertyChanged(() => IsMessageVisible);
            }
        }

        public string MessageLink
        {
            get => _messageLink;
            set
            {
                Set(() => MessageLink, ref _messageLink, value);
                RaisePropertyChanged(() => IsMessageLinkVisible);
            }
        }

        public bool IsMessageLinkVisible => IsMessageVisible && !string.IsNullOrWhiteSpace(MessageLink);

        public bool IsIncompleteAuthorMessageVisible =>
            HasSearchResults && !string.IsNullOrEmpty(IncompleteAuthorMessage);

        public string IncompleteAuthorMessage
        {
            get
            {
                var messageSelectedAuthorIncomplete = SelectedAuthorAndJob?.Author.IsIncomplete == true
                    ? "Some required information is missing for the selected author. Ask the person to complete her/his personal details in Eurolook."
                    : null;
                return messageSelectedAuthorIncomplete;
            }
        }

        public bool IsSearchBoxFocused
        {
            get => _isSearchBoxFocused;
            set => Set(() => IsSearchBoxFocused, ref _isSearchBoxFocused, value);
        }

        public bool IsSearchNotFound
        {
            get => _isSearchNotFound;
            set => Set(() => IsSearchNotFound, ref _isSearchNotFound, value);
        }

        public RelayCommand AddAuthorCommand
        {
            get => _addAuthorCommand;
            set
            {
                _addAuthorCommand = value;
                RaisePropertyChanged(() => AddAuthorCommand);
            }
        }

        public RelayCommand StartSearchCommand { get; set; }

        public RelayCommand LoadMoreResultsCommand => _loadMoreResultsCommand ??= new RelayCommand(LoadMoreResults);

        public bool IsSearchResultComplete
        {
            get => _isSearchResultComplete;
            set => Set(ref _isSearchResultComplete, value);
        }

        public bool SingleResultPerAuthor
        {
            get => _hasSingleResultPerAuthor;
            set => Set(() => SingleResultPerAuthor, ref _hasSingleResultPerAuthor, value);
        }

        public void Search(string text)
        {
            SearchText = text ?? "";
            StartSearch();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _tokenSource?.Dispose();
                _searchTimer?.Dispose();
            }
        }

        protected void AddSearchResultsOnUiThread(Author[] searchResults)
        {
            Execute.OnUiThread(() => { AddSearchResults(searchResults); });
        }

        protected void AddSearchResults(Author[] searchResults)
        {
            var authorViewModels = searchResults.Select(a => new AuthorViewModel(a));

            if (SingleResultPerAuthor)
            {
                SearchResults.AddRange(authorViewModels.Select(x => Create(x, AuthorType.Favorite)));
            }
            else
            {
                SearchResults.AddRange(authorViewModels.SelectMany(x => CreateInstancePerJob(x, AuthorType.Favorite)));
            }

            RaisePropertyChanged(() => HasSearchResults);
        }

        private async Task<Author[]> FetchSearchResultsAsync(CancellationToken cancellationToken, int page = 0)
        {
            var trimmedSearchText = SearchText.Trim();
            if (trimmedSearchText.Length < MinLength)
            {
                // no search: term too short
                Message = trimmedSearchText.Length > 0
                    ? $"Type at least {MinLength} characters."
                    : "";
                ClearSearchResults();
                return new Author[] { };
            }

            // start searching
            Message = "Searching ...";
            IsSearching = true;
            IsSearchNotFound = false;
            var login = await Login();
            if (!login.IsSuccess)
            {
                Message = "Cannot login to the Eurolook server.";
                return new Author[] { };
            }

            var searchResults = await _searchClient.SearchAuthors(cancellationToken, trimmedSearchText, page, PageSize);
            _hasMoreResults = searchResults is { Length: PageSize };

            return searchResults;
        }

        [SuppressMessage(
            "SonarQube",
            "S3168:async methods should not return void",
            Justification = "Method is a RelayCommand Action")]
        private async void LoadMoreResults()
        {
            if (!_hasMoreResults)
            {
                return;
            }

            try
            {
                IsSearching = true;
                _currentPage++;

                var searchResults = await FetchSearchResultsAsync(CancellationToken.None, _currentPage);

                if (searchResults != null)
                {
                    AddSearchResultsOnUiThread(searchResults);
                }
            }
            finally
            {
                IsSearching = false;
            }
        }

        private void StartSearch()
        {
            _searchTimer.Change(800, Timeout.Infinite);
        }

        [SuppressMessage(
            "SonarQube",
            "S3168:async methods should not return void",
            Justification = "Method is a TimerCallback Action")]
        private async void SearchTimerElapsed(object state)
        {
            try
            {
                _tokenSource?.Cancel();
                _tokenSource = new CancellationTokenSource();
                MessageLink = "";

                var searchResults = await FetchSearchResultsAsync(_tokenSource.Token);

                // search returned
                ClearSearchResults();
                if (searchResults == null)
                {
                    Message = "No connection to the Eurolook server.";
                }
                else
                {
                    AddSearchResultsOnUiThread(searchResults);
                    SelectedAuthorAndJob = SearchResults.FirstOrDefault();
                    IsSearchNotFound = true;
                    Message = !SearchResults.Any()
                        ? "Author not found in the Eurolook address book.\n\nAsk the person you are looking for to enter their author details in Eurolook."
                        : null;
                    MessageLink = null;
                }
            }
            catch (OperationCanceledException)
            {
                // we don't log this
            }
            catch (HttpRequestException)
            {
                Message = "No connection to the Eurolook server.";
                ClearSearchResults();
            }
            finally
            {
                IsSearching = false;
            }
        }

        private Task<HttpClientBase.LoginResult> Login()
        {
            return _searchClient.Login();
        }

        private void ClearSearchResults()
        {
            _currentPage = 0;
            Execute.OnUiThread(
                () =>
                {
                    SearchResults.Clear();
                    RaisePropertyChanged(() => HasSearchResults);
                });
        }
    }

    public class AuthorSearchDesignViewModel : AuthorSearchViewModel
    {
        public AuthorSearchDesignViewModel()
            : base(null)
        {
            var authors = new[]
            {
                new Author
                {
                    LatinFirstName = "Entwickler",
                    LatinLastName = "Test",
                    Service = "Abteilung 3.14"
                },
                new Author
                {
                    LatinFirstName = "Developer",
                    LatinLastName = "QA",
                    Service = "Sector 3.14"
                },
                new Author
                {
                    LatinFirstName = "Tester",
                    LatinLastName = "Dev",
                    Service = "Sector 3.14"
                }
            };

            AddSearchResults(authors);
        }
    }
}
