using System;
using Eurolook.AddIn.Common.Database;
using Eurolook.Common.Log;

namespace Eurolook.AddIn.Common
{
    public class DataSyncManager : ICanLog
    {
        private const int DefaultDelayMs = 10 * 1000; // 10 seconds
        private const int DefaultIntervalMs = 60 * 60 * 1000; // 60 minutes

        private readonly DataSyncScheduler _dataSyncScheduler;
        private readonly IDatabaseStateRepository _databaseStateRepository;
        private readonly IUserDataRepository _userDataRepository;

        public DataSyncManager(
            DataSyncScheduler dataSyncScheduler,
            IDatabaseStateRepository databaseStateRepository,
            IUserDataRepository userDataRepository)
        {
            _dataSyncScheduler = dataSyncScheduler;
            _databaseStateRepository = databaseStateRepository;
            _userDataRepository = userDataRepository;
        }

        public void StartDataSyncScheduler(bool miniMode)
        {
            int delay = DefaultDelayMs;
            int interval = DefaultIntervalMs;
            if (!miniMode)
            {
                var settings = _userDataRepository.GetUserSettings();
                if (settings != null)
                {
                    interval = settings.DataSyncIntervalMinutes * 60 * 1000;
                    delay = settings.DataSyncDelayMinutes * 60 * 1000;
                    var lastUpdate = _databaseStateRepository.GetLastUpdate();
                    if (lastUpdate != null)
                    {
                        var nextSyncAt = lastUpdate.Value + TimeSpan.FromMilliseconds(interval);
                        int nextSyncDelay = (int)(nextSyncAt - DateTime.Now).TotalMilliseconds;

                        // randomize delay in the range of +/- 30 seconds
                        int randomDelay = (int)(new Random().NextDouble() * 60 * 1000 - 30 * 1000);

                        // reduce the delay on Word restart and randomize to distribute server load
                        nextSyncDelay = nextSyncDelay / 2 + randomDelay;
                        delay = Math.Max(delay, nextSyncDelay);
                    }
                }
            }

            _dataSyncScheduler.StartTimer(delay, interval, miniMode);
            string miniModeStr = miniMode ? " in mini-mode" : "";
            this.LogDebug(
                $"DataSync scheduler started{miniModeStr}, using DataSync server {_dataSyncScheduler.Server}.");
        }

        public void StopDataSyncScheduler()
        {
            _dataSyncScheduler.StopTimer();
            this.LogDebug("DataSync scheduler stopped.");
        }

        public void ShutdownDataSyncScheduler()
        {
            StopDataSyncScheduler();
        }
    }
}
