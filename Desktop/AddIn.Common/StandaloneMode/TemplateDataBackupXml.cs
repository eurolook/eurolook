using System.Collections.Generic;
using Eurolook.Data.Models.TemplateStore;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.StandaloneMode
{
    public class TemplateDataBackupXml : DataBackupXmlBase
    {
        private const string FileName = "TemplateDataBackup.xml";

        [UsedImplicitly]
        public TemplateDataBackupXml()
        {
        }

        public TemplateDataBackupXml(IEnumerable<Template> templates, IEnumerable<TemplateFile> templateFiles)
        {
            Templates.AddRange(templates);
            TemplateFiles.AddRange(templateFiles);
        }

        public List<Template> Templates { get; set; } = new();

        public List<TemplateFile> TemplateFiles { get; set; } = new();

        public static TemplateDataBackupXml Load(ISettingsService settingsService)
        {
            return Load<TemplateDataBackupXml>(settingsService, FileName);
        }

        public void Save(ISettingsService settingsService)
        {
            Save<TemplateDataBackupXml>(settingsService, FileName);
        }
    }
}
