using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.StandaloneMode
{
    public class AuthorDataBackupXml : DataBackupXmlBase
    {
        private const string FileName = "AuthorDataBackup.xml";

        [UsedImplicitly]
        public AuthorDataBackupXml()
        {
        }

        public AuthorDataBackupXml(IAuthor author, OrgaEntity orgaEntity)
        {
            LatinFirstName = author.LatinFirstName;
            LatinLastName = author.LatinLastName;
            Initials = author.Initials;
            OrgaEntity = orgaEntity.Name;
        }

        public string LatinFirstName { get; set; }

        public string LatinLastName { get; set; }

        public string Initials { get; set; }

        public string OrgaEntity { get; set; }

        public static AuthorDataBackupXml Load(ISettingsService settingsService)
        {
            return Load<AuthorDataBackupXml>(settingsService, FileName);
        }

        public void Save(ISettingsService settingsService)
        {
            Save<AuthorDataBackupXml>(settingsService, FileName);
        }
    }
}
