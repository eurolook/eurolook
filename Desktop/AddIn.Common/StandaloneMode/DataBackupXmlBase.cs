using System.IO;
using System.Xml.Serialization;
using Eurolook.Common;

namespace Eurolook.AddIn.Common.StandaloneMode
{
    public class DataBackupXmlBase
    {
        protected DataBackupXmlBase()
        {
        }

        protected void Save<T>(ISettingsService settingsService, string fileName)
            where T : DataBackupXmlBase
        {
            var dir = Directories.GetApplicationDataDirectory(settingsService.UseRoamingDataDirectory);
            string filePath = Path.Combine(dir.FullName, fileName);
            using var writer = new StreamWriter(filePath);
            var serializer = new XmlSerializer(typeof(T));
            serializer.Serialize(writer, this);
        }

        protected static T Load<T>(ISettingsService settingsService, string fileName)
            where T : DataBackupXmlBase
        {
            var dir = Directories.GetApplicationDataDirectory(settingsService.UseRoamingDataDirectory);
            string filePath = Path.Combine(dir.FullName, fileName);
            if (!File.Exists(filePath))
            {
                return null;
            }

            using var fileStream = new FileStream(filePath, FileMode.Open);
            var serializer = new XmlSerializer(typeof(T));
            return serializer.Deserialize(fileStream) as T;
        }
    }
}
