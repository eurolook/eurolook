﻿using JetBrains.Annotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Eurolook.AddIn.Common
{
    [UsedImplicitly]
    public class DataSyncEnvironment
    {
        private static readonly JsonSerializerSettings JsonSettings = new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
        };

        [UsedImplicitly]
        public string Name { get; set; }

        [UsedImplicitly]
        public string DataSyncUrl { get; set; }

        public string QuickStartGuideUrl { get; set; }

        public string OnlineHelpUrl { get; set; }

        public string AdminUIUrl { get; set; }

        [UsedImplicitly]
        public string TemplateStoreUrl { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
