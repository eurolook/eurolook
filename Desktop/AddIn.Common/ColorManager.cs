﻿using System;
using System.Drawing;
using System.Windows;
using System.Windows.Media;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.AddIn.Common.Messages;
using Eurolook.Data.Models;
using GalaSoft.MvvmLight.Messaging;
using Color = System.Drawing.Color;
using Point = System.Windows.Point;

namespace Eurolook.AddIn.Common
{
    public class ColorManager
    {
        private readonly Lazy<ISettingsService> _settingsService;
        private readonly IUserDataRepository _userDataRepository;

        public ColorManager(
            Lazy<ISettingsService> settingsService,
            IUserDataRepository userDataRepository)
        {
            _settingsService = settingsService;
            _userDataRepository = userDataRepository;
            DefaultColorScheme = ReadDefaultColorScheme();
            CurrentColorScheme = DefaultColorScheme;
        }

        public ColorScheme DefaultColorScheme { get; }

        public ColorScheme CurrentColorScheme { get; private set; }

        public void ApplyColorSchemeFromLocalSettings()
        {
            if (_settingsService.Value.ColorScheme != null)
            {
                ApplyColorScheme(_settingsService.Value.ColorScheme);
            }
            else
            {
                ApplyDefaultColorScheme();
            }
        }

        // Applies the Color scheme that is saved in the user settings.
        // Applies the default scheme if nothing is saved in the settings.
        public void ApplyColorScheme()
        {
            var settings = _userDataRepository.GetUserSettings();
            if (settings?.ColorScheme != null)
            {
                ApplyColorScheme(settings.ColorScheme);
            }
            else
            {
                ApplyDefaultColorScheme();
            }
        }

        public void ApplyColorScheme(ColorScheme colorScheme)
        {
            Execute.OnUiThread(
                () =>
                {
                    CurrentColorScheme = colorScheme ?? DefaultColorScheme;
                    _settingsService.Value.ColorScheme = CurrentColorScheme;

                    var primaryColor = CurrentColorScheme.PrimaryColor.ToWpfColor();
                    var secondaryColor = CurrentColorScheme.SecondaryColor.ToWpfColor();
                    var tileColor1 = CurrentColorScheme.TileColor1.ToWpfColor();
                    var tileColor2 = CurrentColorScheme.TileColor2.ToWpfColor();
                    var tileColor3 = CurrentColorScheme.TileColor3.ToWpfColor();

                    // colors
                    var app = Application.Current;
                    app.Resources["PrimaryColor"] = primaryColor;
                    app.Resources["SecondaryColor"] = secondaryColor;
                    app.Resources["TileColor1"] = tileColor1;
                    app.Resources["TileColor2"] = tileColor2;
                    app.Resources["TileColor3"] = tileColor3;

                    var primaryGradientMid = primaryColor;
                    primaryGradientMid.A = 219;
                    app.Resources["PrimaryGradientMid"] = primaryGradientMid;
                    var primaryGradientEnd = primaryColor;
                    primaryGradientEnd.A = 163;
                    app.Resources["PrimaryGradientEnd"] = primaryGradientEnd;

                    // dependency brushes
                    app.Resources["PrimaryColorBrush"] = new SolidColorBrush(primaryColor);
                    app.Resources["SecondaryColorBrush"] = new SolidColorBrush(secondaryColor);
                    app.Resources["TileColor1Brush"] = new SolidColorBrush(tileColor1);
                    app.Resources["TileColor2Brush"] = new SolidColorBrush(tileColor2);
                    app.Resources["TileColor3Brush"] = new SolidColorBrush(tileColor3);
                    app.Resources["HoverBackgroundBrush"] = new SolidColorBrush(primaryColor) { Opacity = 0.1 };
                    app.Resources["PressedBackgroundBrush"] = new SolidColorBrush(primaryColor) { Opacity = 0.3 };
                    app.Resources["InverseHoverBackgroundBrush"] = new SolidColorBrush(primaryColor) { Opacity = 0.9 };
                    app.Resources["InversePressedBackgroundBrush"] =
                        new SolidColorBrush(primaryColor) { Opacity = 0.7 };
                    app.Resources["BackgroundGradientBrush"] = new LinearGradientBrush
                    {
                        StartPoint = new Point(0, 0),
                        EndPoint = new Point(1, 1),
                        GradientStops = new GradientStopCollection
                        {
                            new GradientStop
                            {
                                Color = primaryColor,
                                Offset = 0.0,
                            },
                            new GradientStop
                            {
                                Color = primaryGradientMid,
                                Offset = 0.4,
                            },
                            new GradientStop
                            {
                                Color = primaryGradientEnd,
                                Offset = 1.0,
                            },
                        },
                    };
                });

            // broadcast new color theme
            Messenger.Default.Send(new ColorSchemeUpdatedMessage(CurrentColorScheme));
        }

        public void ApplyDefaultColorScheme()
        {
            ApplyColorScheme(DefaultColorScheme);
        }

        public Color GetShade(Color color, double[] luminanceValues, Luminance luminance)
        {
            double lumVal = 0.0;
            if (luminance == Luminance.Bright && luminanceValues.Length > 0)
            {
                lumVal = luminanceValues[0];
            }

            if (luminance == Luminance.Dark && luminanceValues.Length > 1)
            {
                lumVal = luminanceValues[1];
            }

            if (luminance == Luminance.Highlighted)
            {
                var highlighted = Application.Current.Resources["Highlighted"].ToString();
                return ColorTranslator.FromHtml(highlighted);
            }

            return Color.FromArgb(
                color.A,
                ColorLuminance(color.R, lumVal),
                ColorLuminance(color.G, lumVal),
                ColorLuminance(color.B, lumVal));
        }

        public byte ColorLuminance(byte c, double lum)
        {
            return (byte)Math.Round(Math.Min(Math.Max(0, c + (c * lum)), 255));
        }

        public double[] GetColorLuminancesFromScheme(ColorScheme colorScheme, int i)
        {
            switch (i)
            {
                case 1:
                    return colorScheme.TileColor1Luminances;
                case 2:
                    return colorScheme.TileColor2Luminances;
                case 3:
                    return colorScheme.TileColor3Luminances;
                default:
                    return new[] { 0.0, 0.0 };
            }
        }

        public Color GetColorFromScheme(ColorScheme colorScheme, int i)
        {
            switch (i)
            {
                case 1:
                    return colorScheme.TileColor1;
                case 2:
                    return colorScheme.TileColor2;
                case 3:
                    return colorScheme.TileColor3;
                default:
                    return colorScheme.PrimaryColor;
            }
        }

        private static ColorScheme ReadDefaultColorScheme()
        {
            if (Application.LoadComponent(
                new Uri("/Eurolook.AddIn.Common;component/Views/DefaultColorScheme.xaml", UriKind.Relative)) is ResourceDictionary resources)
            {
                return new ColorScheme
                {
                    PrimaryColorValue = resources["PrimaryColor"].ToString(),
                    SecondaryColorValue = resources["SecondaryColor"].ToString(),
                    TileColor1Value = resources["TileColor1"].ToString(),
                    TileColor2Value = resources["TileColor2"].ToString(),
                    TileColor3Value = resources["TileColor3"].ToString(),
                    TileColor1LuminancesValue = "0.3, -0.3",
                    TileColor2LuminancesValue = "0.2, -0.25",
                    TileColor3LuminancesValue = "0.25, -0.2",
                };
            }

            return null;
        }
    }
}
