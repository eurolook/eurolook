using System;
using System.Windows;
using Eurolook.AddIn.Common.ViewModels;

namespace Eurolook.AddIn.Common.Views
{
    public partial class MessageWindow
    {
        public bool? Result { get; private set; }

        public MessageWindow(MessageViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;

            EventHandler handler = null;
            handler = (s, e) =>
                               {
                                   if (MessageScrollViewer.ComputedVerticalScrollBarVisibility == Visibility.Visible)
                                   {
                                       SizeToContent = SizeToContent.Manual;
                                       MessageScrollViewer.MaxHeight = double.PositiveInfinity;
                                       ResizeMode = ResizeMode.CanResizeWithGrip;
                                   }

                                   SourceInitialized -= handler;
                               };
            SourceInitialized += handler;
        }

        private void OnOkClick(object sender, RoutedEventArgs e)
        {
            try
            {
                Result = true;
                DialogResult = Result; // will close the dialog automatically
            }
            catch (InvalidOperationException)
            {
                // when not opened as dialog
                Close();
            }
        }

        private void OnNoClick(object sender, RoutedEventArgs e)
        {
            try
            {
                Result = false;
                DialogResult = Result; // will close the dialog automatically
            }
            catch (InvalidOperationException)
            {
                // when not opened as dialog
                Close();
            }
        }

        private void OnCancelClick(object sender, RoutedEventArgs e)
        {
            try
            {
                Result = null;
                DialogResult = Result; // null will set DialogResult to false and close the dialog automatically
            }
            catch (InvalidOperationException)
            {
                // when not opened as dialog
                Close();
            }
        }
    }
}
