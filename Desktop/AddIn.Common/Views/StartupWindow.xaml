<Window x:Class="Eurolook.AddIn.Common.Views.StartupWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:designTime="clr-namespace:Eurolook.AddIn.Common.ViewModels.DesignTime"
        xmlns:viewModels="clr-namespace:Eurolook.AddIn.Common.ViewModels"
        xmlns:wpf="clr-namespace:Eurolook.AddIn.Common.Wpf"
        x:Name="MyWindow"
        Title="Eurolook - Welcome"
        Height="620"
        d:DesignWidth="1200"
        ResizeMode="NoResize"
        WindowStartupLocation="CenterOwner"
        mc:Ignorable="d">

    <Window.Resources>
        <ResourceDictionary>
            <designTime:StartupDesignViewModel x:Key="DesignTimeVm" />
            <Storyboard x:Key="SlideOver" Completed="SlideOverCompleted">
                <ThicknessAnimation Storyboard.TargetProperty="Margin"
                                    To="-600,0,0,0"
                                    Duration="0:0:0.33" />
            </Storyboard>
            <Storyboard x:Key="SlideBack" Completed="SlideBackCompleted">
                <ThicknessAnimation Storyboard.TargetProperty="Margin"
                                    To="0,0,0,0"
                                    Duration="0:0:0.33" />
            </Storyboard>
            <Style x:Key="TaskTileButtonStyle" BasedOn="{StaticResource Tile}" TargetType="{x:Type Button}">
                <Setter Property="HorizontalAlignment" Value="Stretch" />
                <Setter Property="Height" Value="64" />
                <Setter Property="Background" Value="{StaticResource BackgroundColor}" />
                <Setter Property="Margin" Value="0,0,0,4" />
                <Setter Property="ToolTipService.Placement" Value="Right" />
                <Setter Property="ToolTipService.IsEnabled" Value="True" />
                <Setter Property="ToolTipService.InitialShowDelay" Value="1800" />
                <Setter Property="Template">
                    <Setter.Value>
                        <ControlTemplate TargetType="Button">
                            <Border x:Name="PART_Border" Padding="0" Background="{TemplateBinding Background}"
                                    BorderBrush="Transparent" BorderThickness="2" ClipToBounds="True">
                                <Grid Margin="0">
                                    <Grid.ColumnDefinitions>
                                        <ColumnDefinition Width="60" />
                                        <ColumnDefinition Width="*" />
                                        <ColumnDefinition Width="60" />
                                    </Grid.ColumnDefinitions>
                                    <Grid.RowDefinitions>
                                        <RowDefinition Height="Auto" />
                                        <RowDefinition Height="*" />
                                    </Grid.RowDefinitions>

                                    <Border Background="{DynamicResource PrimaryColorBrush}" Grid.Column="0" Grid.Row="0" Grid.RowSpan="2">
                                        <wpf:HeroIcon x:Name="HeroIcon" 
                                                  Size="40" IconPath="{Binding HeroIconPath}"
                                                  Visibility="Collapsed" />
                                    </Border>
                                    
                                    <TextBlock x:Name="Title" Grid.Row="0" Grid.Column="1" Margin="10,0,10,2"
                                               FontFamily="Segoe UI" FontSize="16" FontWeight="ExtraLight"
                                               Foreground="{DynamicResource PrimaryColorBrush}"
                                               Text="{Binding Title}"
                                               TextTrimming="CharacterEllipsis" TextWrapping="NoWrap" />
                                    
                                    <TextBlock x:Name="Description" Grid.Row="1" Grid.Column="1" Margin="10,0"
                                               FontFamily="Segoe UI" FontSize="12" Foreground="#666666"
                                               Text="{Binding SingleLineDescription}" TextTrimming="CharacterEllipsis" TextWrapping="Wrap" />

                                    <wpf:HeroIcon Grid.Row="0" Grid.RowSpan="2" Grid.Column="2"
                                                  Size="32" IconPath="/Graphics/heroicons-outline/green/check-circle.svg"
                                                  Visibility="{Binding IsDone, Converter={StaticResource BooleanToVisibilityConverter}}" />

                                    <wpf:HeroIcon Grid.Row="0" Grid.RowSpan="2" Grid.Column="2"
                                                  Size="32" IconPath="/Graphics/heroicons-outline/black/circle.svg"
                                                  Visibility="{Binding IsDone, Converter={StaticResource BooleanToInvisibilityConverter}}" />
                                   
                                </Grid>
                            </Border>
                            <ControlTemplate.Triggers>
                                <DataTrigger Binding="{Binding HasIcon}" Value="True">
                                    <Setter TargetName="HeroIcon" Property="Visibility" Value="Collapsed" />
                                </DataTrigger>
                                <DataTrigger Binding="{Binding HasHeroIconPath}" Value="True">
                                    <Setter TargetName="HeroIcon" Property="Visibility" Value="Visible" />
                                </DataTrigger>
                                <Trigger Property="IsMouseOver" Value="True">
                                    <Setter TargetName="PART_Border" Property="BorderBrush" Value="{StaticResource TileBorderOver}" />
                                </Trigger>
                                <Trigger Property="IsEnabled" Value="False">
                                    <Setter TargetName="PART_Border" Property="Background" Value="Transparent" />
                                    <Setter TargetName="Title" Property="Foreground" Value="{StaticResource ControlForegroundDisabled}" />
                                    <Setter TargetName="Description" Property="Foreground" Value="{StaticResource ControlForegroundDisabled}" />
                                </Trigger>
                            </ControlTemplate.Triggers>
                        </ControlTemplate>
                    </Setter.Value>
                </Setter>
            </Style>
            <DataTemplate DataType="{x:Type viewModels:TaskTileViewModel}">
                <Grid>
                    <Button AutomationProperties.AutomationId="{Binding AutomationId}"
                            Command="{Binding TileCommand}"
                            Style="{StaticResource TaskTileButtonStyle}">
                        <Button.ToolTip>
                            <Control Template="{StaticResource TileToolTipContent}" />
                        </Button.ToolTip>
                    </Button>
                </Grid>
            </DataTemplate>
        </ResourceDictionary>
    </Window.Resources>

    <Grid d:DataContext="{Binding Source={StaticResource DesignTimeVm}}">
        <StackPanel Name="SlidingPanel"
                    Width="1200"
                    Orientation="Horizontal">
            <StackPanel.Style>
                <Style TargetType="StackPanel">
                    <Style.Triggers>
                        <DataTrigger Binding="{Binding SlidePosition}" Value="1">
                            <DataTrigger.EnterActions>
                                <RemoveStoryboard BeginStoryboardName="SlideOverBegin" />
                                <RemoveStoryboard BeginStoryboardName="SlideBackBegin" />
                                <BeginStoryboard Name="SlideOverBegin" Storyboard="{StaticResource SlideOver}" />
                            </DataTrigger.EnterActions>
                        </DataTrigger>
                        <DataTrigger Binding="{Binding SlidePosition}" Value="2">
                            <DataTrigger.EnterActions>
                                <RemoveStoryboard BeginStoryboardName="SlideOverBegin" />
                                <RemoveStoryboard BeginStoryboardName="SlideBackBegin" />
                                <BeginStoryboard Name="SlideBackBegin" Storyboard="{StaticResource SlideBack}" />
                            </DataTrigger.EnterActions>
                        </DataTrigger>
                    </Style.Triggers>
                </Style>
            </StackPanel.Style>

            <Grid Name="WelcomePanel" Width="600">
                <Grid.RowDefinitions>
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="*" />
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="Auto" />
                </Grid.RowDefinitions>

                <Border Grid.Row="0"
                        Grid.RowSpan="4"
                        Background="{DynamicResource BackgroundGradientBrush}" />

                <Image Grid.Row="0"
                       Grid.RowSpan="5"
                       Width="140"
                       Margin="0 -20 0 0"
                       HorizontalAlignment="Left"
                       VerticalAlignment="Top"
                       Opacity="0.25"
                       Source="../Graphics/logo_about.png" />

                <TextBlock Grid.Row="0"
                           Margin="40 20 40 0"
                           Style="{DynamicResource HeroHeader}">
                    Welcome to Eurolook
                </TextBlock>

                <TextBlock Grid.Row="1"
                           Margin="40 12 40 32"
                           Style="{DynamicResource HeroText}">
                    <Run Text="{Binding Converter={StaticResource ResourceConverter}, ConverterParameter=WelcomeToEurolook, Mode=OneWay, FallbackValue='Welcome to Eurolook, the Commission tool for drafting documents.'}" />
                    <LineBreak />
                    Before you can start we would like to ask you to complete the following steps.</TextBlock>

                <ItemsControl Grid.Row="2"
                              Margin="40 0 40 12"
                              ItemsSource="{Binding Tasks}" />

                <CheckBox Grid.Row="3"
                          Margin="40 0 40 20"
                          VerticalAlignment="Top"
                          VerticalContentAlignment="Top"
                          IsChecked="{Binding JoinCustomerExperienceProgram}"
                          Visibility="{Binding ShowCustomerExperienceProgramOption, Converter={StaticResource BooleanToVisibilityConverter}}">
                    <StackPanel>
                        <TextBlock Margin="0 0 0 8" Style="{DynamicResource HeroText}">
                            Yes, I want to help Eurolook to be improved
                        </TextBlock>
                        <TextBlock Margin="0 0 0 8" Style="{DynamicResource HeroText}">
                            By enabling this feature, you allow the Eurolook team to gather anonymous usage and performance statistics.
                        </TextBlock>
                        <TextBlock Margin="0 0 0 8" Style="{DynamicResource HeroText}">
                            This information is purely of a technical nature and only reflects global trends; it is not shared with any other parties.
                            It will not and cannot be used to track an individual’s activities.
                        </TextBlock>
                        <TextBlock Style="{DynamicResource HeroText}">
                            Thank you!
                        </TextBlock>
                    </StackPanel>
                </CheckBox>

                <Border Grid.Row="4"
                        Height="54"
                        Background="{DynamicResource PrimaryColorBrush}">
                    <StackPanel HorizontalAlignment="Right" Orientation="Horizontal">
                        <Button Width="80"
                                Margin="40 0"
                                AutomationProperties.AutomationId="Startup.InitializeEurolook"
                                Click="FinishClick"
                                IsEnabled="{Binding IsValid}"
                                Style="{DynamicResource InverseButton}">
                            <TextBlock Foreground="White" Style="{DynamicResource ButtonText}">Finish</TextBlock>
                        </Button>
                    </StackPanel>
                </Border>

            </Grid>

            <Grid Name="AuthorDetailsPanel" Width="600">
                <Grid.Resources>
                    <Style BasedOn="{StaticResource ControlLabel}" TargetType="Label">
                        <Setter Property="Foreground" Value="White" />
                        <Setter Property="Margin" Value="0 16 0 0" />
                    </Style>
                </Grid.Resources>
                <Grid.RowDefinitions>
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="*" />
                    <RowDefinition Height="Auto" />
                </Grid.RowDefinitions>

                <Border Grid.Row="0"
                        Grid.RowSpan="2"
                        Background="{DynamicResource BackgroundGradientBrush}" />

                <TextBlock Grid.Row="0"
                           Margin="40 20 40 0"
                           Style="{DynamicResource HeroHeader}">
                    Complete your personal details
                </TextBlock>

                <Grid Grid.Row="1"
                      Margin="40 0"
                      DataContext="{Binding AuthorCompletionVm}">
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="*" />
                        <ColumnDefinition Width="20" />
                        <ColumnDefinition Width="*" />
                        <ColumnDefinition Width="20" />
                        <ColumnDefinition Width="100" />
                    </Grid.ColumnDefinitions>
                    <Grid.RowDefinitions>
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="Auto" />
                    </Grid.RowDefinitions>

                    <TextBlock Grid.Row="0"
                               Grid.Column="0"
                               Grid.ColumnSpan="5"
                               Margin="0 12 0 12"
                               Foreground="#FCD428"
                               Style="{DynamicResource HeroText}">
                        Use accented characters and lowercase where appropriate.
                    </TextBlock>

                    <Label Grid.Row="1" Grid.Column="0">Latin first name:</Label>
                    <wpf:PlaceholderTextBox x:Name="LatinFirstNameText"
                                            Grid.Row="2"
                                            Grid.Column="0"
                                            Placeholder="{Binding LatinFirstNamePlaceholder}"
                                            Style="{StaticResource SimpleTextBox}"
                                            Text="{Binding LatinFirstName, UpdateSourceTrigger=PropertyChanged, ValidatesOnDataErrors=true, NotifyOnValidationError=true}"
                                            ToolTip="The first name in Latin script." />

                    <Label Grid.Row="1" Grid.Column="2">Latin last name:</Label>
                    <wpf:PlaceholderTextBox x:Name="LatinLastNameText"
                                            Grid.Row="2"
                                            Grid.Column="2"
                                            Placeholder="{Binding LatinLastNamePlaceholder}"
                                            Style="{StaticResource SimpleTextBox}"
                                            Text="{Binding LatinLastName, UpdateSourceTrigger=PropertyChanged, ValidatesOnDataErrors=true, NotifyOnValidationError=true}"
                                            ToolTip="The last name in Latin script." />

                    <Label Grid.Row="1" Grid.Column="4">Initials:</Label>
                    <TextBox Name="InitialsText"
                             Grid.Row="2"
                             Grid.Column="4"
                             Style="{StaticResource SimpleTextBox}"
                             Text="{Binding Initials, UpdateSourceTrigger=PropertyChanged, ValidatesOnDataErrors=true, NotifyOnValidationError=true}" />

                    <Label Grid.Row="3"
                           Grid.Column="0"
                           Grid.ColumnSpan="5">
                        Organisational entity:
                    </Label>
                    <TreeView Grid.Row="4"
                              Grid.Column="0"
                              Grid.ColumnSpan="5"
                              Height="260"
                              BorderThickness="1"
                              IsVisibleChanged="OrgaTreeVisiblityChanged"
                              ItemsSource="{Binding OrgaTree}"
                              SelectedItemChanged="OnOrgaTreeSelectionChanged"
                              Style="{StaticResource OrgaTreeView}"
                              TreeViewItem.Expanded="OnOrgaTreeViewItemExpanded" />
                </Grid>

                <Border Grid.Row="2"
                        Height="54"
                        Background="{DynamicResource PrimaryColorBrush}">
                    <Grid>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="*" />
                            <ColumnDefinition Width="Auto" />
                        </Grid.ColumnDefinitions>

                        <TextBlock Grid.Column="0"
                                   Margin="60 0"
                                   HorizontalAlignment="Left"
                                   VerticalAlignment="Center"
                                   Foreground="White"
                                   Style="{DynamicResource PureContentText}"
                                   Text="{Binding AuthorCompletionVm.Error}" />

                        <Button Grid.Column="1"
                                Width="80"
                                Margin="20 12 40 12"
                                Command="{Binding ConfirmAuthorCommand}"
                                IsEnabled="{Binding AuthorCompletionVm.IsValid}"
                                Style="{DynamicResource InverseButton}">
                            <TextBlock Foreground="White" Style="{DynamicResource ButtonText}">Confirm</TextBlock>
                        </Button>
                    </Grid>

                </Border>
            </Grid>


        </StackPanel>

    </Grid>
</Window>