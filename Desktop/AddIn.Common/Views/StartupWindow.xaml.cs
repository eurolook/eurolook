using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Eurolook.AddIn.Common.Authors;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;

namespace Eurolook.AddIn.Common.Views
{
    public partial class StartupWindow : ICanLog
    {
        public StartupWindow(StartupViewModel viewModel)
        {
            ViewModel = viewModel;
            DataContext = ViewModel;
            ViewModel.SlidePosition = 0;
            if (!ViewModel.IsInDesignMode)
            {
                Width = 616; // 600 + some borders
            }

            InitializeComponent();
        }

        public StartupViewModel ViewModel { get; }

        private void FinishClick(object sender, RoutedEventArgs e)
        {
            FinishAsync().FireAndForgetSafeAsync(this.LogError);
        }

        private async Task FinishAsync()
        {
            await ViewModel.Finish();
            DialogResult = true;
        }

        private void SlideOverCompleted(object sender, EventArgs e)
        {
            ViewModel.SlideOverCompleted();
        }

        private void SlideBackCompleted(object sender, EventArgs e)
        {
            ViewModel.SlideBackCompleted();
        }

        private void OnOrgaTreeViewItemExpanded(object sender, RoutedEventArgs e)
        {
            var treeViewItem = e.OriginalSource as TreeViewItem;
            treeViewItem?.BringIntoView();
        }

        private void OnOrgaTreeSelectionChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            ViewModel.AuthorCompletionVm.SelectedOrgaEntityVm = e.NewValue as OrgaEntityViewModel;
            ViewModel.AuthorCompletionVm.ValidateForm();
        }

        private void OrgaTreeVisiblityChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!(bool)e.NewValue)
            {
                return;
            }

            if (ViewModel?.AuthorCompletionVm?.SelectedOrgaEntityVm == null)
            {
                var treeView = sender as TreeView;
                treeView?.TryScrollToTop();
            }
        }
    }
}
