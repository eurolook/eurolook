namespace Eurolook.AddIn.Common.ViewModels
{
    public class TaskTileViewModel : InfoTileViewModel
    {
        private bool _isDone;

        public bool IsDone
        {
            get => _isDone;
            set { Set(() => IsDone, ref _isDone, value); }
        }
    }
}
