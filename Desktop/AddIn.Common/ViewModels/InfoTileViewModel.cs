﻿using System.Windows.Media.Imaging;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Data.Models;

namespace Eurolook.AddIn.Common.ViewModels
{
    public class InfoTileViewModel : TileViewModel
    {
        private string _secondLineDescription;

        public InfoTileViewModel()
        {
        }

        public InfoTileViewModel(Brick brick)
            : base(brick.HotKey)
        {
            BrickId = brick.Id;
            Title = brick.DisplayName;
            Description = brick.Description;
            AutomationId = brick.Id.ToString("B");
            ColorLuminance = brick.ColorLuminance;
            if (HeroIcon.IsSvgFile(brick.VectorIcon))
            {
                HeroIconSource = HeroIcon.CreateImageFromBytes(brick.VectorIcon);
            }
            else if (brick.Icon != null && brick.Icon.Length > 0)
            {
                Icon = new BitmapImage().InitAndFreeze(brick.Icon);
            }
        }

        public override int UIPositionIndex => 0;

        public string SecondLineDescription
        {
            get => _secondLineDescription;
            set => Set(() => SecondLineDescription, ref _secondLineDescription, value);
        }
    }
}
