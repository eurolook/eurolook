using Eurolook.Data.Models;
using GalaSoft.MvvmLight;

namespace Eurolook.AddIn.Common.ViewModels
{
    public class AddressViewModel : ViewModelBase
    {
        private bool _isEnabled;

        public AddressViewModel(Address address)
        {
            Address = address;
            IsEnabled = true;
        }

        public Address Address { get; }

        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { Set(() => IsEnabled, ref _isEnabled, value); }
        }

        public override string ToString()
        {
            return Address.Name;
        }
    }
}
