using System.Threading.Tasks;
using Eurolook.Common.Log;
using GalaSoft.MvvmLight;

namespace Eurolook.AddIn.Common.ViewModels
{
    public class SimpleProgressReporterViewModel : ViewModelBase, IProgressReporter<int>, ICanLog
    {
        private int _progress;
        private bool _isBusy;
        private string _busyMessage;

        public int Progress
        {
            get => _progress;
            set => Set(() => Progress, ref _progress, value);
        }

        public bool IsBusy
        {
            get => _isBusy;
            set { Set(() => IsBusy, ref _isBusy, value); }
        }

        public string BusyMessage
        {
            get => _busyMessage ?? "Ready.";
            set => Set(() => BusyMessage, ref _busyMessage, value);
        }

        public async Task ReportProgressAsync(int value)
        {
            Progress = value;
            await Task.Delay(50);
        }

        public async Task ReportProgressAsync(int value, string message)
        {
            IsBusy = !string.IsNullOrEmpty(message);
            BusyMessage = message;
            Progress = value;
            this.LogInfo(message);
            await Task.Delay(50);
        }
    }
}
