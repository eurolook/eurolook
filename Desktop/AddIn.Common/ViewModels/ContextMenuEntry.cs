﻿using System.Windows.Input;
using GalaSoft.MvvmLight;

namespace Eurolook.AddIn.Common.ViewModels
{
    public class ContextMenuEntry : ViewModelBase
    {
        private ICommand _command;
        private string _title;
        private string _iconPath;

        public string IconPath
        {
            get { return _iconPath; }
            set { Set(() => IconPath, ref _iconPath, value); }
        }

        public string Title
        {
            get { return _title; }
            set { Set(() => Title, ref _title, value); }
        }

        public ICommand Command
        {
            get { return _command; }
            set { Set(() => Command, ref _command, value); }
        }
    }
}
