﻿using GalaSoft.MvvmLight;

namespace Eurolook.AddIn.Common.ViewModels
{
    public class NavigationListItem : ViewModelBase
    {
        private bool _isVisible;
        private string _iconPath;

        public string Title { get; set; }

        public string IconPath
        {
            get => _iconPath;
            set { Set(() => IconPath, ref _iconPath, value); }
        }

        public bool IsVisible
        {
            get => _isVisible;
            set { Set(() => IsVisible, ref _isVisible, value); }
        }

        public bool IsIconVisible => IconPath == null;

        public override string ToString()
        {
            return Title;
        }
    }
}
