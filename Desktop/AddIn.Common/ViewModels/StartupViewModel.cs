﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.Authors;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.ProfileInitializer;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;

namespace Eurolook.AddIn.Common.ViewModels
{
    public class StartupViewModel : ViewModelBase, ICanLog
    {
        private readonly ISettingsService _settingsService;
        private readonly IUserDataRepository _userDataRepository;
        private readonly Lazy<IProfileInitializer> _profileInitializer;
        private readonly TaskTileViewModel _completeAuthorTile;
        private readonly TaskTileViewModel _quickStartGuideTile;
        private readonly DeviceManager _deviceManager;

        private bool _showCustomerExperienceProgramOption;
        private bool _joinCustomerExperienceProgram;
        private int _slidePosition;

        #region Properties

        public int SlidePosition
        {
            get => _slidePosition;
            set { Set(() => SlidePosition, ref _slidePosition, value); }
        }

        public bool ShowCustomerExperienceProgramOption
        {
            get => _showCustomerExperienceProgramOption;
            set { Set(() => ShowCustomerExperienceProgramOption, ref _showCustomerExperienceProgramOption, value); }
        }

        public bool IsValid => Tasks.All(x => x.IsDone);

        public RelayCommand ConfirmAuthorCommand { get; set; }

        public bool JoinCustomerExperienceProgram
        {
            get => _joinCustomerExperienceProgram;
            set { Set(() => JoinCustomerExperienceProgram, ref _joinCustomerExperienceProgram, value); }
        }

        public AuthorCompletionViewModel AuthorCompletionVm { get; set; }

        public ObservableCollection<TaskTileViewModel> Tasks { get; set; }

        public User UserProfile { get; set; }

        public bool ShowQuickStartGuideTile
        {
            get => Tasks.Contains(_quickStartGuideTile);
            set
            {
                Tasks.Remove(_quickStartGuideTile);
                if (value)
                {
                    Tasks.Add(_quickStartGuideTile);
                }
            }
        }

        #endregion

        public StartupViewModel(
            ISettingsService settingsService,
            IUserDataRepository userDataRepository,
            Lazy<IProfileInitializer> profileInitializer,
            AuthorCompletionViewModel authorCompletionViewModel,
            DeviceManager deviceManager)
        {
            _settingsService = settingsService;
            _userDataRepository = userDataRepository;
            _profileInitializer = profileInitializer;
            _deviceManager = deviceManager;
            _joinCustomerExperienceProgram = true;
            AuthorCompletionVm = authorCompletionViewModel;

            _quickStartGuideTile = new TaskTileViewModel
            {
                HeroIconPath = "/Graphics/heroicons-outline/white/film.svg",
                Title = "Watch the Quick Start Guide",
                Description = "The Quick Start Guide provides an easy-to-follow demo of how to create a new document.",
                TileCommand = new RelayCommand(WatchQuickStartGuide),
                AutomationId = "WatchQuickStartGuide",
            };
            _completeAuthorTile = new TaskTileViewModel
            {
                HeroIconPath = "/Graphics/heroicons-outline/white/users.svg",
                Title = "Complete your author details",
                Description = "In order to create documents you need to complete some missing author details.",
                TileCommand = new RelayCommand(CompleteAuthor),
                AutomationId = "CompleteAuthor",
            };
            Tasks = new ObservableCollection<TaskTileViewModel>
            {
                _completeAuthorTile,
            };
            ConfirmAuthorCommand = new RelayCommand(ConfirmAuthor);
            SlidePosition = 0;
        }

        private async void WatchQuickStartGuide()
        {
            Process.Start(_settingsService.QuickStartGuideUrl);
            await Task.Delay(1000);
            _quickStartGuideTile.IsDone = true;
            RaisePropertyChanged(() => IsValid);
        }

        private void CompleteAuthor()
        {
            SlideOver();
        }

        public void ConfirmAuthor()
        {
            if (AuthorCompletionVm.Confirm())
            {
                _completeAuthorTile.IsDone = true;
                RaisePropertyChanged(() => IsValid);
                SlideBack();
            }
        }

        internal async Task Finish()
        {
            _deviceManager.UpdateDeviceProperties(ShowQuickStartGuideTile && _quickStartGuideTile.IsDone);
            if (ShowCustomerExperienceProgramOption)
            {
                await _userDataRepository.UpdateCustomerExperienceProgramAsync(JoinCustomerExperienceProgram);
            }

            await _userDataRepository.UpdateShowWelcomeWindowShownAsync();
            await _profileInitializer.Value.CheckUserProfile();
        }

        private void SlideOver()
        {
            SlidePosition = 1;
        }

        private void SlideBack()
        {
            SlidePosition = 2;
        }

        [SuppressMessage("SonarQube", "S3168:async methods should not return void", Justification = "Method is an event handler")]
        public async void SlideOverCompleted()
        {
            await Task.Delay(300);
            await AuthorCompletionVm.InitAsync(UserProfile.Self);
        }

        public void SlideBackCompleted()
        {
        }
    }
}
