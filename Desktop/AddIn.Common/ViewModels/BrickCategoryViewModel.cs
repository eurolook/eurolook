using System.Windows.Media;
using Eurolook.AddIn.Common.Messages;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Data.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;

namespace Eurolook.AddIn.Common.ViewModels
{
    public class BrickCategoryViewModel : ViewModelBase
    {
        private bool _isExpanded;
        private Brush _colorBrush;
        private bool _isDisabled;

        public BrickCategoryViewModel(BrickCategory category, bool isExpanded)
        {
            _isExpanded = isExpanded;
            Category = category;
        }

        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                Set(() => IsExpanded, ref _isExpanded, value);
                Messenger.Default.Send(new ExpansionStateChangedMessage { Source = this });
            }
        }

        public BrickCategory Category { get; }

        public string AutomationId
        {
            get { return Category.Id.ToString("B"); }
        }

        public string Name
        {
            get { return Category.Name; }
        }

        public bool IsEmpty
        {
            get { return Bricks.Count == 0; }
        }

        public bool IsDisabled
        {
            get { return _isDisabled; }
            set { Set(() => IsDisabled, ref _isDisabled, value); }
        }

        public Brush ColorBrush
        {
            get { return _colorBrush; }
            set { Set(() => ColorBrush, ref _colorBrush, value); }
        }

        public ObservableCollectionEx<TileViewModel> Bricks { get; } = new ObservableCollectionEx<TileViewModel>();
    }
}
