using System;
using Eurolook.Data.Models;
using GalaSoft.MvvmLight;

namespace Eurolook.AddIn.Common.ViewModels
{
    public class PredefinedFunctionViewModel : ViewModelBase
    {
        public PredefinedFunctionViewModel(PredefinedFunction model)
        {
            PredefinedFunction = model;
            PredefinedFunctionId = model.Id;
            DisplayName = model.Function?.Value ?? model.Name;
        }

        public PredefinedFunctionViewModel()
        {
        }

        public PredefinedFunction PredefinedFunction { get; }

        public Guid? PredefinedFunctionId { get; set; }

        public string DisplayName { get; set; }

        public override string ToString()
        {
            return DisplayName;
        }
    }
}
