﻿using System;
using System.Windows;
using System.Windows.Media;
using Eurolook.AddIn.Common.ClipboardTools;
using Eurolook.AddIn.Common.Properties;
using Eurolook.Common.Extensions;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Eurolook.AddIn.Common.ViewModels
{
    public class MessageViewModel : ViewModelBase
    {
        private string _windowTitle;

        public MessageViewModel()
        {
            YesButtonText = "OK";
            CancelButtonText = "Cancel";
            IsShowCancelButton = true;
            IsShowNoButton = false;
            GetHelpCommand = new RelayCommand(OnHelpLink);
            TitleBarColor = Application.Current.TryFindResource("PrimaryColorBrush") as SolidColorBrush;
        }

        public RelayCommand GetHelpCommand { get; set; }

        public string WindowTitle
        {
            get
            {
                return _windowTitle ?? (Title.IsNullOrEmpty() ? "Eurolook" : $"Eurolook - {Title}");
            }

            set { _windowTitle = value; }
        }

        public string Title { get; set; }

        public string Message { get; set; }

        public string YesButtonText { get; set; }

        public string NoButtonText { get; set; }

        public string CancelButtonText { get; set; }

        public Action HelpLinkAction { get; set; }

        public string HelpLinkText { get; set; }

        public bool IsShowCancelButton { get; set; }

        public bool IsShowNoButton { get; set; }

        public bool IsShowHelpButton { get; set; }

        public bool IsShowHelpLink { get; set; }

        public bool FormatStringLinks { get; set; }

        public SolidColorBrush TitleBarColor { get; set; }

        public RelayCommand CopyMessageCommand
        {
            get { return new RelayCommand(CopyMessage); }
        }

        private void OnHelpLink()
        {
            HelpLinkAction?.Invoke();
        }

        private void CopyMessage()
        {
            string plainTextMessage =
                $@"_{WindowTitle}_

*{Title}*

```
{{code}}
{Message}
{{code}}
```
";

            // loading Word/Outlook-compatible HTML template for message formatting
            var assembly = typeof(MessageViewModel).Assembly;
            string template = new EmbeddedResourceManager().GetEmbeddedResourceText(assembly, "Graphics", "ClipboardTemplate.html");
            if (template != null)
            {
                string htmlMessage = template.Replace("{WindowTitle}", WindowTitle)
                                             .Replace("{Title}", Title)
                                             .Replace("{Message}", Message);

                ClipboardHtmlHelper.CopyToClipboard(htmlMessage, plainTextMessage);
            }
            else
            {
                Clipboard.SetText(plainTextMessage);
            }
        }
    }
}
