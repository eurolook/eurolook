using System.Collections.ObjectModel;
using Eurolook.AddIn.Common.DocumentMetadata.TermPicker;

namespace Eurolook.AddIn.Common.ViewModels.DesignTime
{
    public class TermPickerDesignViewModel : TermPickerViewModel
    {
        private readonly TermViewModel _displayTerm =
            new TermViewModel
            {
                DisplayName = "Term 1",
            };

        public TermPickerDesignViewModel()
            : base(null, null, null, 0)
        {
            IsVisible = true;
            Terms = new ObservableCollection<TermViewModel>
            {
                _displayTerm,
                new TermViewModel
                {
                    DisplayName = "Term 2",
                    Terms = new ObservableCollection<TermViewModel>
                    {
                        new TermViewModel
                        {
                            DisplayName = "Term 2.1",
                        },
                        new TermViewModel
                        {
                            DisplayName = "Term 2.2",
                        },
                    },
                },
                new TermViewModel
                {
                    DisplayName = "Term 3",
                },
            };
            SelectedItems.Add(_displayTerm);
        }
    }
}
