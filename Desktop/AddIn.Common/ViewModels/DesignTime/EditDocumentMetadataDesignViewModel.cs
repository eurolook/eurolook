﻿using System;
using System.Collections.Generic;
using Eurolook.AddIn.Common.BrickCommands.EditDocumentMetadata;
using Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels;
using Eurolook.AddIn.Common.DocumentMetadata.TermPicker;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.AddIn.Common.ViewModels.DesignTime
{
    public class EditDocumentMetadataDesignViewModel : EditDocumentMetadataViewModel
    {
        public EditDocumentMetadataDesignViewModel()
            : base(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null)
        {
            var cat1 = new MetadataCategory { Id = Guid.NewGuid(), Name = "General", DisplayName = "General" };
            Metadata = new List<IMetadataViewModel>
            {
                new DateMetadataViewModel(null)
                {
                    MetadataDefinition = new MetadataDefinition
                    {
                        DisplayName = "Meeting Date",
                        MetadataType = MetadataType.Date,
                        MetadataCategory = cat1,
                    },
                    Value = DateTime.UtcNow.Date,
                },
                new SingleBoundedTermMetadataViewModel
                {
                    MetadataDefinition = new MetadataDefinition
                    {
                        DisplayName = "Confidentiality Level",
                        MetadataType = MetadataType.SingleBoundedTerm,
                        MetadataCategory = cat1,
                    },
                    SelectedTerm = new SelectedTermViewModel { Label = "Internal" },
                },
                new TextMetadataViewModel
                {
                    MetadataDefinition = new MetadataDefinition
                    {
                        DisplayName = "Originating Department",
                        MetadataType = MetadataType.Text,
                        MetadataCategory = cat1,
                        IsMandatory = true,
                    },
                    Value = "",
                },
                new TimeMetadataViewModel(null, null)
                {
                    MetadataDefinition = new MetadataDefinition
                    {
                        DisplayName = "Meeting Time",
                        MetadataType = MetadataType.Time,
                        MetadataCategory = cat1,
                    },
                    Value = DateTime.UtcNow,
                },
                new DateTimeMetadataViewModel(null)
                {
                    MetadataDefinition = new MetadataDefinition
                    {
                        DisplayName = "Meeting Time",
                        MetadataType = MetadataType.Time,
                        MetadataCategory = cat1,
                    },
                    DateValue = DateTime.UtcNow,
                    TimeValue = DateTime.UtcNow,
                },
            };

            TermPickerViewModel = new TermPickerViewModel(null, null, null, 1) { IsVisible = false };
        }
    }
}
