﻿using System.Collections.ObjectModel;

namespace Eurolook.AddIn.Common.ViewModels.DesignTime
{
    public class StartupDesignViewModel : StartupViewModel
    {
        public StartupDesignViewModel()
            : base(null, null, null, null, null)
        {
            ShowCustomerExperienceProgramOption = true;
            Tasks = new ObservableCollection<TaskTileViewModel>
            {
                new TaskTileViewModel
                {
                    HeroIconPath = "/Graphics/heroicons-outline/white/users.svg",
                    Title = "Complete your author details",
                    Description = "In order to create documents you need to complete some missing author details.",
                    AutomationId = "CompleteAuthor",
                    IsDone = true,
                },
                new TaskTileViewModel
                {
                    HeroIconPath = "/Graphics/heroicons-outline/white/film.svg",
                    Title = "Watch the Quick Start Guide",
                    Description = "Lorem ipsum...",
                    AutomationId = "WatchQuickStartGuide",
                },
            };
        }
    }
}
