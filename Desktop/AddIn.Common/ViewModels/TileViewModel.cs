﻿using System;
using System.Text.RegularExpressions;
using System.Timers;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Data.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.ViewModels
{
    public abstract class TileViewModel : ViewModelBase, IContextMenu, IDisposable
    {
        private Timer _hotKeyTimer;

        private bool _showTileText;
        private bool _showCheckmark;
        private bool _showHotKeyText;
        private bool _showDisclosureIndicator;
        private string _description;
        private string _title;
        private string _acronym;
        private string _automationId;
        private ICommand _tileCommand;
        private Brush _colorBrush;
        private bool _hasContextMenu;
        private string _contextMenuTitle;
        private Luminance _colorLuminance;

        protected TileViewModel()
        {
            ContextMenuEntries = new ObservableCollectionEx<ContextMenuEntry>();

            _showHotKeyText = false;
            _showTileText = true;
            _showDisclosureIndicator = false;
        }

        protected TileViewModel(string hotKey)
            : this()
        {
            HotKey = hotKey;

            _hotKeyTimer = new Timer(800);
            _hotKeyTimer.Elapsed += OnHotKeyTimerElapsed;

            StartHotKeyTimerCmd = new RelayCommand(StartMouseTimer);
            StopHotKeyTimerCmd = new RelayCommand(StopMouseTimer);
        }

        ~TileViewModel()
        {
            Dispose(false);
        }

        [UsedImplicitly]
        public bool ShowDisclosureIndicator
        {
            get { return _showDisclosureIndicator; }
            set { Set(() => ShowDisclosureIndicator, ref _showDisclosureIndicator, value); }
        }

        public bool ShowCheckmark
        {
            get { return _showCheckmark; }
            set { Set(() => ShowCheckmark, ref _showCheckmark, value); }
        }

        [UsedImplicitly]
        [CanBeNull]
        public NotifyTask<int> BadgeCount { get; set; }

        [UsedImplicitly]
        public bool IsBadgeCountVisible
        {
            get { return BadgeCount?.IsSuccessfullyCompleted == true && BadgeCount.Result > 0; }
        }

        [UsedImplicitly]
        public string Description
        {
            get { return _description?.Trim(); }
            set
            {
                Set(() => Description, ref _description, value);
                RaisePropertyChanged(() => SingleLineDescription);
                RaisePropertyChanged(() => IsDescriptionVisible);
            }
        }

        [UsedImplicitly]
        public bool IsHighlighted => ColorLuminance == Luminance.Highlighted;

        [UsedImplicitly]
        public Luminance ColorLuminance
        {
            get { return _colorLuminance; }
            set
            {
                Set(() => ColorLuminance, ref _colorLuminance, value);
                RaisePropertyChanged(() => IsHighlighted);
            }
        }

        [UsedImplicitly]
        public bool IsDescriptionVisible
        {
            get { return !string.IsNullOrWhiteSpace(Description); }
        }

        [UsedImplicitly]
        public string SingleLineDescription
        {
            get { return Description?.Replace("\r", "").Replace("\n", ""); }
        }

        [UsedImplicitly]
        public bool ShowHotKeyText
        {
            get { return _showHotKeyText; }
            set { Set(() => ShowHotKeyText, ref _showHotKeyText, value); }
        }

        public abstract int UIPositionIndex { get; }

        [UsedImplicitly]
        public bool ShowTileText
        {
            get { return _showTileText; }
            set { Set(() => ShowTileText, ref _showTileText, value); }
        }

        [UsedImplicitly]
        public string HotKey { get; }

        public string Title
        {
            get { return _title; }
            set { Set(() => Title, ref _title, value); }
        }

        /// <summary>
        /// Gets a single-line title where multiple spaces or line-breaks are replaced by a single blank.
        /// </summary>
        [UsedImplicitly]
        public string ToolTipTitle
        {
            get
            {
                string title = Title != null ? $"{Regex.Replace(Title, @"\s+", " ")}" : "";
                string hotKeySuffix = string.IsNullOrEmpty(HotKey) ? "" : $"({HotKey.Trim()})";
                return $"{title} {hotKeySuffix}";
            }
        }

        [UsedImplicitly]
        public string Acronym
        {
            get { return _acronym; }
            set { Set(() => Acronym, ref _acronym, value); }
        }

        public string AutomationId
        {
            get { return _automationId; }
            set { Set(() => AutomationId, ref _automationId, value); }
        }

        public BitmapImage Icon { get; set; }

        public DrawingImage HeroIconSource { get; set; }

        public string HeroIconPath { get; set; }

        [UsedImplicitly]
        public bool HasIcon => Icon != null;

        [UsedImplicitly]
        public bool HasHeroIconSource => HeroIconSource != null;

        [UsedImplicitly]
        public bool HasHeroIconPath => !string.IsNullOrWhiteSpace(HeroIconPath);

        public Brush ColorBrush
        {
            get { return _colorBrush; }
            set { Set(() => ColorBrush, ref _colorBrush, value); }
        }

        [UsedImplicitly]
        public RelayCommand StartHotKeyTimerCmd { get; set; }

        [UsedImplicitly]
        public RelayCommand StopHotKeyTimerCmd { get; set; }

        public ICommand TileCommand
        {
            get { return _tileCommand; }
            set { Set(() => TileCommand, ref _tileCommand, value); }
        }

        public Guid? BrickId { get; set; }

        public string ContextMenuTitle
        {
            get { return _contextMenuTitle; }
            set { Set(() => ContextMenuTitle, ref _contextMenuTitle, value); }
        }

        public bool HasContextMenu
        {
            get { return _hasContextMenu; }
            set { Set(() => HasContextMenu, ref _hasContextMenu, value); }
        }

        public ObservableCollectionEx<ContextMenuEntry> ContextMenuEntries { get; set; }

        public virtual void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            _hotKeyTimer?.Dispose();
            _hotKeyTimer = null;
        }

        private void StartMouseTimer()
        {
            if (!string.IsNullOrEmpty(HotKey))
            {
                _hotKeyTimer?.Start();
            }
        }

        private void OnHotKeyTimerElapsed(object sender, ElapsedEventArgs e)
        {
            if (string.IsNullOrEmpty(HotKey))
            {
                return;
            }

            ShowTileText = false;
            ShowHotKeyText = true;
        }

        private void StopMouseTimer()
        {
            _hotKeyTimer?.Stop();
            ShowHotKeyText = false;
            ShowTileText = true;
        }
    }
}
