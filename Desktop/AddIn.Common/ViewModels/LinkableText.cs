﻿using GalaSoft.MvvmLight;

namespace Eurolook.AddIn.Common.ViewModels
{
    public class LinkableText : ViewModelBase
    {
        private bool _isLinked;

        public LinkableText(string text, string link)
        {
            Text = text;
            Hyperlink = link;
            IsLinked = !string.IsNullOrEmpty(Hyperlink);
        }

        public string Text { get; set; }

        public string Hyperlink { get; set; }

        public bool IsLinked
        {
            get => _isLinked;
            set => Set(() => IsLinked, ref _isLinked, value);
        }

        public override string ToString()
        {
            return Text;
        }
    }
}
