using GalaSoft.MvvmLight.CommandWpf;

namespace Eurolook.AddIn.Common.ViewModels
{
    public abstract class DialogViewModelBase : SimpleProgressReporterViewModel
    {
        private bool? _dialogResult;

        protected DialogViewModelBase()
        {
            CommitCommand = new RelayCommand(Commit, CanCommit);
            CancelCommand = new RelayCommand(Cancel, CanCancel);
        }

        public bool? DialogResult
        {
            get => _dialogResult;
            set { Set(() => DialogResult, ref _dialogResult, value); }
        }

        public RelayCommand CancelCommand { get; }

        public RelayCommand CommitCommand { get; }

        public virtual string Title => $"Eurolook - {DisplayName}";

        public virtual string DisplayName { get; set; }

        public virtual bool CanCancel()
        {
            return !IsBusy;
        }

        public virtual void Cancel()
        {
            DialogResult = false;
        }

        public virtual bool CanCommit()
        {
            return !IsBusy;
        }

        public virtual void Commit()
        {
            DialogResult = true;
        }
    }
}
