using Eurolook.AddIn.Common.Wpf;

namespace Eurolook.AddIn.Common.ViewModels
{
    public interface IContextMenu
    {
        string ContextMenuTitle { get; }
        bool HasContextMenu { get; }
        ObservableCollectionEx<ContextMenuEntry> ContextMenuEntries { get; }
    }
}
