using System;
using GalaSoft.MvvmLight;

namespace Eurolook.AddIn.Common.ViewModels
{
    public abstract class CancelableViewModel<T> : ViewModelBase
        where T : ICloneable
    {
        protected CancelableViewModel()
        {
        }

        protected CancelableViewModel(T model)
        {
            Init(model);
        }

        public void Init(T model)
        {
            if (model == null)
            {
                return;
            }

            Model = model;
            InitViewModel(Model);
        }

        public T Model { get; protected set; }

        protected abstract void InitViewModel(T model);

        public abstract void Apply();

        public abstract void Cancel();
    }
}
