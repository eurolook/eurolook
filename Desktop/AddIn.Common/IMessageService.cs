﻿using System;
using System.Windows;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.OfficeNotificationBar;
using GalaSoft.MvvmLight;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common
{
    public interface IMessageService : IDisposable
    {
        void ActivateApplicationWindow();

        void ShowDebugError(object obj, string caption = "Eurolook - Error");

        void ShowSplashScreen();

        void ShowLightBox(ILightBoxContent lightBoxContent);

        Window ShowSingleton(Window window, bool setOwner = true, bool showCentered = false, bool adaptWindowSizeForScreen = false);

        void ShowWindowActivated(Window window, bool setOwner = true);

        bool? ShowDialog(Window window, bool showCentered = true, bool adaptWindowSizeForScreen = false);

        bool? ShowMessageWindow(MessageViewModel messageViewModel, ViewModelBase ownerViewModel, bool? isTopMost = null);

        bool? ShowSimpleMessage(string message, string title, ViewModelBase ownerViewModel = null);

        void MoveTopMostDialog(int x, int y);

        void CloseAllWindows();

        void ShowPopup(Window window,  bool showCentered = true);

        [CanBeNull]
        string ShowBrowseForFolderDialog(string title, [CanBeNull] string initialDirectory = null);
    }
}
