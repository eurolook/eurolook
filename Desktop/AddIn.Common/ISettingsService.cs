﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.Data.Models;

namespace Eurolook.AddIn.Common
{
    public interface ISettingsService
    {
        string DataSyncUrl { get; }

        string QuickStartGuideUrl { get; }

        string TemplateStoreUrl { get; }

        bool IsTaskpaneVisible { get; set; }

        int TaskPaneWidth { get; set; }

        int TaskPaneHeight { get; set; }

        string TaskPanePosition { get; set; }

        string LastUsedRuleFiles { get; set; }

        ColorScheme ColorScheme { get; set; }

        Task<Dictionary<Guid, bool>> LoadExpansionStateAsync();

        Task SaveExpansionStateAsync(IEnumerable<BrickCategoryViewModel> documentBrickCategories);

        string OnlineHelpUrl { get; }

        string GetOnlineHelpLink(string id = null);

        void OpenHelpLink(string id = null);

        void EnableTestMode();

        void DisableTestMode();

        bool IsInTestMode { get; }

        bool IsStandaloneMode { get; }

        bool UseRoamingDataDirectory { get; }
    }
}
