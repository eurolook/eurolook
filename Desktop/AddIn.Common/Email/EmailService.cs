﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.SystemConfiguration;
using Eurolook.AddIn.Common.UserConfiguration;
using Eurolook.Common;
using Eurolook.Common.Extensions;
using Microsoft.Office.Interop.Outlook;
using Exception = System.Exception;

namespace Eurolook.AddIn.Common.Email
{
    public class EmailService : IEmailService
    {
        private readonly ISettingsService _settingsService;
        private readonly IUserIdentity _userIdentity;
        private readonly ISystemConfigurationService _systemConfigurationService;
        private readonly IMessageService _messageService;

        public EmailService(
            ISystemConfigurationService systemConfigurationService,
            ISettingsService settingsService,
            IUserIdentity userIdentity,
            IMessageService messageService)
        {
            _systemConfigurationService = systemConfigurationService;
            _settingsService = settingsService;
            _userIdentity = userIdentity;
            _messageService = messageService;
        }

        public async Task SendFeedback()
        {
            var recipientAddress = await _systemConfigurationService.GetFeedbackEmailAddress();
            var mailItem = CreateOutlookMailItem("My Eurolook Feedback", recipientAddress);
            OpenOutlookMailItem(mailItem);
        }

        public async Task SendLogFiles()
        {
            var appDataDir = Directories.GetApplicationDataDirectory(_settingsService.UseRoamingDataDirectory);
            using var tempFile = new TemporaryFile(".zip");
            var logsDir = Path.Combine(appDataDir.FullName, "Logs");

            await CreateZipFromDirectory(logsDir, tempFile.FullName);

            var fileName = $"{_userIdentity.GetUserName()}_Logs.zip".GetSafeFilename();
            var recipientAddress = await _systemConfigurationService.GetFeedbackEmailAddress();
            var mailItem = CreateOutlookMailItem("Eurolook Log Files", recipientAddress, new AttachmentInfo(tempFile.FullName, fileName));
            OpenOutlookMailItem(mailItem);
        }

        public void OpenOutlookMailItem(MailItem mailItem)
        {
            mailItem.Display();
            mailItem.GetInspector.Activate(); // Open editor in the foreground.
        }

        public MailItem CreateOutlookMailItem(string subject, string recipientAddress = null, AttachmentInfo attachment = null)
        {
            var outlookProcess = Process.GetProcessesByName("outlook")
                                        .FirstOrDefault(p => SafeNativeMethods.IsWindowVisible(p.MainWindowHandle));
            if (outlookProcess == null)
            {
                try
                {
                    outlookProcess = Process.Start("outlook.exe");
                    outlookProcess?.WaitForInputIdle(5000);
                }
                catch (Exception)
                {
                    _messageService.ShowSimpleMessage("Could not start Outlook", "Error");
                }
            }

            var outlookApplication = new Application();
            var mailItem = (MailItem)outlookApplication.CreateItem(OlItemType.olMailItem);
            mailItem.Subject = subject;
            AddAttachment(mailItem, attachment);

            if (!string.IsNullOrWhiteSpace(recipientAddress))
            {
                var recipient = mailItem.Recipients.Add(recipientAddress);
                recipient.Resolve();
            }

            return mailItem;
        }

        public void AddAttachment(MailItem mailItem, AttachmentInfo attachment)
        {
            if (attachment != null && File.Exists(attachment.FullName))
            {
                mailItem.Attachments.Add(attachment.FullName, OlAttachmentType.olByValue, Type.Missing, attachment.DisplayName);
            }
        }

        public void EncryptMailItem(MailItem mailItem)
        {
            mailItem.PropertyAccessor.SetProperty(@"http://schemas.microsoft.com/mapi/proptag/0x6E010003", 1);
        }

        private async Task CreateZipFromDirectory(string sourceDir, string zipFilePath)
        {
            // log files are kept open by nlog, hence we have to stream the content of the logs folder
            using var zipToOpen = new FileStream(zipFilePath, FileMode.Create);
            using var zipArchive = new ZipArchive(zipToOpen, ZipArchiveMode.Create);

            foreach (var file in Directory.GetFiles(sourceDir))
            {
                await AddFileToZipArchive(file, zipArchive);
            }
        }

        private async Task AddFileToZipArchive(string file, ZipArchive zipArchive)
        {
            var entryName = Path.GetFileName(file);
            var newZipEntry = zipArchive.CreateEntry(entryName);
            using var fileStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            using var newZipEntryStream = newZipEntry.Open();
            await fileStream.CopyToAsync(newZipEntryStream, 10240);
        }
    }
}
