﻿using System.Threading.Tasks;
using Microsoft.Office.Interop.Outlook;

namespace Eurolook.AddIn.Common.Email
{
    public interface IEmailService
    {
        MailItem CreateOutlookMailItem(
            string subject,
            string recipientAddress = null,
            AttachmentInfo attachment = null);

        void OpenOutlookMailItem(MailItem mailItem);

        void AddAttachment(MailItem mailItem, AttachmentInfo attachment);

        void EncryptMailItem(MailItem mailItem);

        Task SendFeedback();

        Task SendLogFiles();
    }
}
