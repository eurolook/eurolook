﻿namespace Eurolook.AddIn.Common.Email
{
    public class AttachmentInfo
    {
        public AttachmentInfo(string fullName, string displayName)
        {
            FullName = fullName;
            DisplayName = displayName;
        }

        public string FullName { get; }

        public string DisplayName { get; }
    }
}
