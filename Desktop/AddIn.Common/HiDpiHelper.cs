using System;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;

namespace Eurolook.AddIn.Common
{
    public static class HiDpiHelper
    {
        [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "This type is defined by an external API.")]
        [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "This type is defined by an external API.")]
        private enum DeviceCap
        {
            LOGPIXELSX = 88,
            LOGPIXELSY = 90,
        }

        public static double GetDpiScalingFactor()
        {
            int dpiX = GetDpiX();
            return dpiX / 96.0;
        }

        public static int GetDpiX()
        {
            var g = Graphics.FromHwnd(IntPtr.Zero);
            var desktop = g.GetHdc();
            int dpiX = SafeNativeMethods.GetDeviceCaps(desktop, (int)DeviceCap.LOGPIXELSX);
            return dpiX;
        }

        public static int GetRealPixels(int effectivePixels)
        {
            return (int)(GetDpiScalingFactor() * effectivePixels);
        }

        public static int GetEffectivePixels(int realPixels)
        {
            return (int)(realPixels / GetDpiScalingFactor());
        }
    }
}
