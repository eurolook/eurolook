﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.UserConfiguration;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Newtonsoft.Json;

namespace Eurolook.AddIn.Common
{
    public class SettingsService : ISettingsService, ICanLog
    {
        private readonly IUserDataRepository _userDataRepository;
        private readonly IUserConfiguration _userConfiguration;
        private readonly IUserIdentity _userIdentity;

        public SettingsService(IUserDataRepository userDataRepository, IUserConfiguration userConfiguration, IUserIdentity userIdentity)
        {
            _userDataRepository = userDataRepository;
            _userConfiguration = userConfiguration;
            _userIdentity = userIdentity;
        }

        public string DataSyncUrl => _userConfiguration.GetDataSyncUrl();

        public string TemplateStoreUrl => _userConfiguration.GetTemplateStoreUrl();

        public string QuickStartGuideUrl => _userConfiguration.GetQuickStartGuideUrl();

        public bool IsStandaloneMode => AppSettings.Instance.StandaloneMode;

        public bool IsInTestMode { get; private set; } = false;

        public ColorScheme ColorScheme
        {
            get
            {
                string value = _userConfiguration.ColorScheme;
                return !string.IsNullOrWhiteSpace(value)
                    ? JsonConvert.DeserializeObject<ColorScheme>(value)
                    : null;
            }
            set
            {
                _userConfiguration.ColorScheme = value != null
                    ? JsonConvert.SerializeObject(value)
                    : null;

                _userConfiguration.Save();
            }
        }

        public string OnlineHelpUrl => _userConfiguration.GetOnlineHelpUrl();

        public bool UseRoamingDataDirectory => AppSettings.Instance.UseRoamingDataDirectory;

        public string ImpersonationUser { get => _userConfiguration.ImpersonationUser; }

        public bool IsTaskpaneVisible
        {
            get
            {
                // NOTE: The original idea was to load the taskpane settings from DB and use user.config as fallback if
                //   there is no profile in the DB yet or the DB does not yet exist.
                //   However, this code may trigger a synchronous DB request during Word startup, which takes about 2 seconds.
                //   Therefore, we only read the task pane settings from the config file.
                return _userConfiguration.IsTaskPaneVisible;
            }
            set
            {
                _userConfiguration.IsTaskPaneVisible = value;
                _userConfiguration.Save();
            }
        }

        // NOTE: The original idea was to load the TaskPane settings from DB and use user.config as fallback if
        //   there is no profile in the DB yet or the DB does not yet exist.
        //   However, this code may trigger a synchronous DB request during Word startup, which takes about 2 seconds.
        //   Therefore, we only read the task pane settings from the config file.
        public int TaskPaneWidth
        {
            get => _userConfiguration.TaskPaneWidth;
            set
            {
                _userConfiguration.TaskPaneWidth = value;
                _userConfiguration.Save();

                var deviceSettings = GetOrCreateCurrentDeviceSettings();
                if (deviceSettings != null && deviceSettings.TaskpaneWidth != value)
                {
                    deviceSettings.TaskpaneWidth = value;
                    UpdateDeviceSettings(deviceSettings);
                }
            }
        }

        public int TaskPaneHeight
        {
            get => _userConfiguration.TaskPaneHeight;
            set
            {
                _userConfiguration.TaskPaneHeight = value;
                _userConfiguration.Save();
                // TODO: Add to DeviceSettings
            }
        }

        public string TaskPanePosition
        {
            get => _userConfiguration.TaskPanePosition;
            set
            {
                _userConfiguration.TaskPanePosition = value;
                _userConfiguration.Save();
                // TODO: Add to DeviceSettings
            }
        }

        public string LastUsedRuleFiles
        {
            get => _userConfiguration.LastUsedRuleFiles;
            set
            {
                _userConfiguration.LastUsedRuleFiles = value;
                _userConfiguration.Save();
            }
        }

        public Task<Dictionary<Guid, bool>> LoadExpansionStateAsync()
        {
            return Task.Factory.StartNew(
                () =>
                {
                    var settings = GetOrCreateCurrentDeviceSettings();
                    return settings != null ? settings.DeserializeExpansionState() : new Dictionary<Guid, bool>();
                });
        }

        public Task SaveExpansionStateAsync(IEnumerable<BrickCategoryViewModel> documentBrickCategories)
        {
            return Task.Factory.StartNew(() => SaveExpansionState(documentBrickCategories));
        }

        public void OpenHelpLink(string id = null)
        {
            try
            {
                Process.Start(GetOnlineHelpLink(id));
            }
            catch (Exception ex)
            {
                this.LogError($"Failed executing {nameof(OpenHelpLink)}.\n{ex}");
            }
        }

        public void EnableTestMode()
        {
            IsInTestMode = true;
        }

        public void DisableTestMode()
        {
            IsInTestMode = false;
        }

        public string GetOnlineHelpLink(string id = null)
        {
            if (IsStandaloneMode)
            {
                return $"{Path.GetDirectoryName(GetType().Assembly.Location)}\\OfflineHelp.pdf";
            }

            string clientVersion = Assembly.GetAssembly(typeof(SettingsService)).GetName().Version.ToString();
            var userNameHash = GetMd5Hash(_userIdentity.GetUserName());
            string parameters = $"?clientVersion={clientVersion}&originator={userNameHash}";
            return id != null
                ? $"{OnlineHelpUrl}#cshid={id}{parameters}"
                : $"{OnlineHelpUrl}{parameters}";
        }

        private string GetMd5Hash(string input)
        {
            using (var md5Hash = MD5.Create())
            {
                var data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
                var stringBuilder = new StringBuilder();
                foreach (byte b in data)
                {
                    stringBuilder.Append(b.ToString("x2"));
                }

                return stringBuilder.ToString();
            }
        }

        private void SaveExpansionState(IEnumerable<BrickCategoryViewModel> documentBrickCategories)
        {
            var deviceSettings = GetOrCreateCurrentDeviceSettings();
            if (deviceSettings == null)
            {
                return;
            }

            // check the expansion state
            var expansionState = deviceSettings.DeserializeExpansionState();
            bool expansionStateChanged = false;
            foreach (var categoryViewModel in documentBrickCategories)
            {
                bool isExpanded = true;
                if (expansionState.Keys.Contains(categoryViewModel.Category.Id))
                {
                    isExpanded = expansionState[categoryViewModel.Category.Id];
                }

                if (isExpanded != categoryViewModel.IsExpanded)
                {
                    expansionStateChanged = true;
                    expansionState[categoryViewModel.Category.Id] = categoryViewModel.IsExpanded;
                }
            }

            // write changes
            if (expansionStateChanged)
            {
                deviceSettings.SerializeExpansionState(expansionState);
                UpdateDeviceSettings(deviceSettings);
            }
        }

        private void UpdateDeviceSettings(DeviceSettings deviceSettings)
        {
            _userDataRepository.UpdateDeviceSettings(deviceSettings);
        }

        private DeviceSettings GetOrCreateCurrentDeviceSettings()
        {
            var deviceSettings = _userDataRepository.GetOrCreateCurrentDeviceSettings();
            return deviceSettings;
        }
    }
}
