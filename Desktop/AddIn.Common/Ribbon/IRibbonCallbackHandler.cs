﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.Office.Core;
using stdole;

namespace Eurolook.AddIn.Common.Ribbon
{
    public interface IRibbonCallbackHandler : IRibbonCustomUICallbackHandler, IRibbonGenericCallbackHandler, IRibbonButtonCallbackHandler,
                                              IRibbonToggleButtonCallbackHandler, IRibbonItemsControlCallbackHandler,
                                              IRibbonGalleryCallbackHandler, IRibbonDynamicMenuCallbackHandler,
                                              IRibbonMenuSeparatorCallbackHandler
    {
        string GetCustomUI(string ribbonId);
    }

    public interface IRibbonCustomUICallbackHandler
    {
        object LoadImage(string imageId);
    }

    [SuppressMessage("ReSharper", "UnusedParameter.Global", Justification = "The method signature is defined by the Office API.")]
    public interface IRibbonGenericCallbackHandler
    {
        string GetDescription(IRibbonControl control);

        bool GetEnabled(IRibbonControl control);

        object GetImage(IRibbonControl control);

        string GetImageMso(IRibbonControl control);

        string GetLabel(IRibbonControl control);

        string GetKeytip(IRibbonControl control);

        RibbonControlSize GetSize(IRibbonControl control);

        string GetScreentip(IRibbonControl control);

        string GetSupertip(IRibbonControl control);

        bool GetVisible(IRibbonControl control);
    }

    [SuppressMessage("ReSharper", "UnusedParameter.Global", Justification = "The method signature is defined by the Office API.")]
    public interface IRibbonButtonCallbackHandler
    {
        bool GetShowImage(IRibbonControl control);

        bool GetShowLabel(IRibbonControl control);

        void OnAction(IRibbonControl control);

        void OnCanceableAction(IRibbonControl control, ref bool cancelDefault);
    }

    [SuppressMessage("ReSharper", "UnusedParameter.Global", Justification = "The method signature is defined by the Office API.")]
    public interface IRibbonToggleButtonCallbackHandler
    {
        bool GetPressed(IRibbonControl control);

        void OnToggleAction(IRibbonControl control, bool pressed);

        void OnCanceableToggleAction(IRibbonControl control, bool pressed, ref bool cancelDefault);
    }

    [SuppressMessage("ReSharper", "UnusedParameter.Global", Justification = "The method signature is defined by the Office API.")]
    [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "The method signature is defined by the Office API.")]
    public interface IRibbonItemsControlCallbackHandler
    {
        int GetItemCount(IRibbonControl control);

        string GetItemID(IRibbonControl control, int index);

        IPictureDisp GetItemImage(IRibbonControl control, int index);

        string GetItemLabel(IRibbonControl control, int index);

        string GetItemScreenTip(IRibbonControl control, int index);

        string GetItemSuperTip(IRibbonControl control, int index);

        string GetText(IRibbonControl control);

        void OnChange(IRibbonControl control, string text);

        string GetSelectedItemID(IRibbonControl control);

        int GetSelectedItemIndex(IRibbonControl control);

        void OnItemAction(IRibbonControl control, string selectedId, int selectedIndex);
    }

    [SuppressMessage("ReSharper", "UnusedParameter.Global", Justification = "The method signature is defined by the Office API.")]
    public interface IRibbonGalleryCallbackHandler
    {
        int GetItemHeight(IRibbonControl control);

        int GetItemWidth(IRibbonControl control);
    }

    [SuppressMessage("ReSharper", "UnusedParameter.Global", Justification = "The method signature is defined by the Office API.")]
    public interface IRibbonDynamicMenuCallbackHandler
    {
        string GetContent(IRibbonControl control);
    }

    [SuppressMessage("ReSharper", "UnusedParameter.Global", Justification = "The method signature is defined by the Office API.")]
    public interface IRibbonMenuSeparatorCallbackHandler
    {
        string GetTitle(IRibbonControl control);
    }
}
