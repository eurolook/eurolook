using System.Threading.Tasks;
using Microsoft.Office.Core;

namespace Eurolook.AddIn.Common.Ribbon
{
    public interface IRibbonService
    {
        void OnLoad(IRibbonUI ribbonUI);

        void InvalidateRibbon();

        Task<bool> TryLoadQuickAccessToolbarAsync();

        void UnloadQuickAccessToolbar();
    }
}
