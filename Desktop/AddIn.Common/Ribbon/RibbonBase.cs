﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Eurolook.AddIn.Common.Properties;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Microsoft.Office.Core;
using stdole;

namespace Eurolook.AddIn.Common.Ribbon
{
    public abstract class RibbonBase : ICanLog, IRibbonCallbackHandler
    {
        private readonly Lazy<IAddinContext> _addinContext;
        private readonly Lazy<IMessageService> _messageService;
        private readonly Lazy<ISettingsService> _settingsService;
        private readonly Lazy<IEmbeddedResourceManager> _embeddedResourceManager;

        protected RibbonBase(
            Lazy<IAddinContext> addinContext,
            Lazy<IMessageService> messageService,
            Lazy<ISettingsService> settingsService,
            Lazy<IEmbeddedResourceManager> embeddedResourceManager)
        {
            _addinContext = addinContext;
            _messageService = messageService;
            _settingsService = settingsService;
            _embeddedResourceManager = embeddedResourceManager;
        }

        protected IMessageService MessageService => _messageService.Value;

        protected ISettingsService SettingsService => _settingsService.Value;

        protected IAddinContext AddinContext => _addinContext.Value;

        protected IEmbeddedResourceManager EmbeddedResourceManager => _embeddedResourceManager.Value;

        protected Dictionary<string, RibbonControlHandler> ControlMap { get; set; }

        public abstract string GetCustomUI(string ribbonId);

        public virtual object LoadImage(string imageId)
        {
            return GetResourceImage(imageId);
        }

        public virtual string GetDescription(IRibbonControl control)
        {
            return GetControlHandler(control)?.GetDescription?.Invoke(control);
        }

        public virtual bool GetEnabled(IRibbonControl control)
        {
            return GetControlHandler(control)?.GetEnabled?.Invoke(control) ?? true;
        }

        /// <summary>
        /// Get an image from the resources to be used for the ribbon control.
        /// </summary>
        /// <param name="control">An <see cref="IRibbonControl" /> instance.</param>
        /// <returns>An <see cref="stdole.IPictureDisp" /> object.</returns>
        public virtual object GetImage(IRibbonControl control)
        {
            return GetControlHandler(control)?.GetImage?.Invoke(control)
                   ?? GetResourceImage(control.Id)
                   ?? GetResourceImage(control.Tag);
        }

        public virtual string GetImageMso(IRibbonControl control)
        {
            throw new NotImplementedException();
        }

        public virtual string GetLabel(IRibbonControl control)
        {
            return GetControlHandler(control)?.GetLabel?.Invoke(control);
        }

        public virtual string GetKeytip(IRibbonControl control)
        {
            throw new NotImplementedException();
        }

        public virtual RibbonControlSize GetSize(IRibbonControl control)
        {
            throw new NotImplementedException();
        }

        public virtual string GetScreentip(IRibbonControl control)
        {
            return GetControlHandler(control)?.GetScreentip?.Invoke(control);
        }

        public virtual string GetSupertip(IRibbonControl control)
        {
            throw new NotImplementedException();
        }

        public virtual bool GetVisible(IRibbonControl control)
        {
            return GetControlHandler(control)?.GetVisible?.Invoke(control) ?? true;
        }

        public virtual bool GetShowImage(IRibbonControl control)
        {
            throw new NotImplementedException();
        }

        public virtual bool GetShowLabel(IRibbonControl control)
        {
            return GetControlHandler(control)?.GetShowLabel?.Invoke(control) ?? true;
        }

        public virtual void OnCanceableAction(IRibbonControl control, ref bool cancelDefault)
        {
            GetControlHandler(control)?.OnAction?.Invoke(control);
        }

        public virtual void OnAction(IRibbonControl control)
        {
            GetControlHandler(control)?.OnAction?.Invoke(control);
        }

        public virtual bool GetPressed(IRibbonControl control)
        {
            return GetControlHandler(control)?.GetPressed?.Invoke(control) ?? false;
        }

        public virtual void OnToggleAction(IRibbonControl control, bool pressed)
        {
            GetControlHandler(control)?.OnToggleAction?.Invoke(control, pressed);
        }

        public virtual void OnCanceableToggleAction(IRibbonControl control, bool pressed, ref bool cancelDefault)
        {
            throw new NotImplementedException();
        }

        public virtual int GetItemCount(IRibbonControl control)
        {
            throw new NotImplementedException();
        }

        public virtual string GetItemID(IRibbonControl control, int index)
        {
            throw new NotImplementedException();
        }

        public virtual IPictureDisp GetItemImage(IRibbonControl control, int index)
        {
            throw new NotImplementedException();
        }

        public virtual string GetItemLabel(IRibbonControl control, int index)
        {
            throw new NotImplementedException();
        }

        public virtual string GetItemScreenTip(IRibbonControl control, int index)
        {
            throw new NotImplementedException();
        }

        public virtual string GetItemSuperTip(IRibbonControl control, int index)
        {
            throw new NotImplementedException();
        }

        public virtual string GetText(IRibbonControl control)
        {
            throw new NotImplementedException();
        }

        public virtual void OnChange(IRibbonControl control, string text)
        {
            throw new NotImplementedException();
        }

        public virtual string GetSelectedItemID(IRibbonControl control)
        {
            throw new NotImplementedException();
        }

        public virtual int GetSelectedItemIndex(IRibbonControl control)
        {
            throw new NotImplementedException();
        }

        public virtual void OnItemAction(IRibbonControl control, string selectedId, int selectedIndex)
        {
            throw new NotImplementedException();
        }

        public virtual int GetItemHeight(IRibbonControl control)
        {
            throw new NotImplementedException();
        }

        public virtual int GetItemWidth(IRibbonControl control)
        {
            throw new NotImplementedException();
        }

        public virtual string GetContent(IRibbonControl control)
        {
            return GetControlHandler(control)?.GetContent?.Invoke(control);
        }

        public virtual string GetTitle(IRibbonControl control)
        {
            throw new NotImplementedException();
        }

        public void ShowHelpPage(string id)
        {
            SettingsService.OpenHelpLink(id);
        }

        protected string GetResourceTextInAssembliesOfTypeHierarchy(string folder, string resourceName, Type startType)
        {
            var result = EmbeddedResourceManager.GetEmbeddedResourceText(startType.Assembly, folder, resourceName);
            return result != null || startType.Name == nameof(RibbonBase)
                ? result
                : GetResourceTextInAssembliesOfTypeHierarchy(folder, resourceName, startType.BaseType);
        }

        protected Image GetResourceImageRecursive(Type type, string resourceId)
        {
            try
            {
                var image = EmbeddedResourceManager.GetEmbeddedResourceImage(type.Assembly, "Graphics", resourceId);
                if (image == null)
                {
                    return type.Name == nameof(RibbonBase) ? null : GetResourceImageRecursive(type.BaseType, resourceId);
                }

                return image;
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }

            return null;
        }

        protected virtual Image GetResourceImage(string resourceId)
        {
            return GetResourceImageRecursive(GetType(), resourceId);
        }

        protected RibbonControlHandler GetControlHandler(IRibbonControl control)
        {
            if (ControlMap.ContainsKey(control.Id))
            {
                return ControlMap[control.Id];
            }

            if (ControlMap.ContainsKey(control.Tag))
            {
                return ControlMap[control.Tag];
            }

            return null;
        }

        protected class RibbonControlHandler
        {
            public Func<IRibbonControl, string> GetDescription { get; set; }

            public Func<IRibbonControl, bool> GetEnabled { get; set; }

            public Func<IRibbonControl, bool> GetVisible { get; set; }

            public Func<IRibbonControl, string> GetLabel { get; set; }

            public Func<IRibbonControl, bool> GetShowLabel { get; set; }

            public Action<IRibbonControl> OnAction { get; set; }

            public Func<IRibbonControl, bool> GetPressed { get; set; }

            public Action<IRibbonControl, bool> OnToggleAction { get; set; }

            public Func<IRibbonControl, string> GetContent { get; set; }

            public Func<IRibbonControl, Image> GetImage { get; set; }

            public Func<IRibbonControl, string> GetScreentip { get; set; }

            public CommandBrick CommandBrick { get; set; }
        }
    }
}
