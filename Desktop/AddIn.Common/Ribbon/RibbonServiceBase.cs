using System;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.ProfileInitializer;
using Eurolook.Common.Log;
using Microsoft.Office.Core;

namespace Eurolook.AddIn.Common.Ribbon
{
    public abstract class RibbonServiceBase<TApplicationContext, TAddinContext> : IRibbonService, ICanLog
        where TApplicationContext : IApplicationContext
        where TAddinContext : IAddinContext
    {
        private IRibbonUI _ribbonUI;

        protected RibbonServiceBase(
            TApplicationContext applicationContext,
            TAddinContext addinContext,
            Lazy<IProfileInitializer> profileInitializer,
            Lazy<IUserDataRepository> userDataRepository)
        {
            ApplicationContext = applicationContext;
            AddinContext = addinContext;
            ProfileInitializer = profileInitializer;
            UserDataRepository = userDataRepository;
        }

        protected TApplicationContext ApplicationContext { get; }

        protected TAddinContext AddinContext { get; }

        protected Lazy<IProfileInitializer> ProfileInitializer { get; }

        protected Lazy<IUserDataRepository> UserDataRepository { get; }

        public void OnLoad(IRibbonUI ribbonUI)
        {
            _ribbonUI = ribbonUI;
        }

        public void InvalidateRibbon()
        {
            _ribbonUI?.Invalidate();
        }

        public abstract Task<bool> TryLoadQuickAccessToolbarAsync();

        public abstract void UnloadQuickAccessToolbar();
    }
}
