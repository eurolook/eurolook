using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using Eurolook.Common;

namespace Eurolook.AddIn.Common.ClipboardTools
{
    public class ClipboardBackup
    {
        /// <summary>
        /// Empty the Clipboard and restore to the data contained in a collection of DataClip objects
        /// </summary>
        /// <param name="clipData">The collection of DataClip containing data stored from clipboard</param>
        /// <param name="hWnd">The window handle to associate the clipboard operations with</param>
        public static bool RestoreClipboard(ReadOnlyCollection<DataClip> clipData, IntPtr hWnd)
        {
            // Retry several times as mitigation for clipboard locking issues in TS sessions.
            // See also the reference source of the System.Windows.Clipboard implementation
            Retry.OnException<Win32Exception>(
                () =>
                {
                    try
                    {
                        Clipboard.Clear();

                        if (!NativeMethods.OpenClipboard(hWnd))
                        {
                            throw new Win32Exception();
                        }

                        // Clear the clipboard
                        ////EmptyClipboard();

                        foreach (var dataClip in clipData)
                        {
                            // Get the pointer for inserting the buffer data into the clipboard
                            var alloc = NativeMethods.GlobalAlloc(
                                (uint)(
                                    NativeMethods.GlobalMemoryFlags.GMEM_MOVEABLE
                                    | NativeMethods.GlobalMemoryFlags.GMEM_DDESHARE),
                                dataClip.Size);
                            var gLock = NativeMethods.GlobalLock(alloc);

                            // Copy the buffer of the ClipData into the clipboard
                            if ((int)dataClip.Size > 0)
                            {
                                Marshal.Copy(dataClip.Buffer, 0, gLock, dataClip.Buffer.GetLength(0));
                            }

                            // Release pointers
                            NativeMethods.GlobalUnlock(alloc);
                            var handle = NativeMethods.SetClipboardData(dataClip.Format, alloc);
                            if (handle == IntPtr.Zero)
                            {
                                throw new Win32Exception();
                            }
                        }
                    }
                    finally
                    {
                        NativeMethods.CloseClipboard();
                    }
                },
                20,
                50);

            return true;
        }

        /// <summary>
        /// Convert all data present in the clipboard to a DataClip collection
        /// </summary>
        /// <param name="hWnd">The window handle to associate the clipboard operations with</param>
        public static ReadOnlyCollection<DataClip> BackupClipboard(IntPtr hWnd)
        {
            var clipData = new List<DataClip>();

            // Retry several times as mitigation for clipboard locking issues in TS sessions.
            // See also the reference source of the System.Windows.Clipboard implementation
            Retry.OnException<Win32Exception>(
                () =>
                {
                    try
                    {
                        if (!NativeMethods.OpenClipboard(hWnd))
                        {
                            throw new Win32Exception();
                        }

                        // Loop for each clipboard data type
                        uint format = 0;
                        while ((format = NativeMethods.EnumClipboardFormats(format)) != 0)
                        {
                            // skip expensive, delay-rendered formats that we currently do not need
                            if (
                                format == (uint)NativeMethods.ClipFormat.CF_BITMAP
                                || format == (uint)NativeMethods.ClipFormat.CF_ENHMETAFILE
                                || format == (uint)NativeMethods.ClipFormat.CF_DIB
                                || format == (uint)NativeMethods.ClipFormat.CF_METAFILEPICT
                                || format == (uint)NativeMethods.ClipFormat.CF_WAVE)
                            {
                                continue;
                            }

                            // Check if clipboard data type is recognized, and get its name
                            string formatName = "0";
                            if (format > 14)
                            {
                                var res = new StringBuilder();
                                if (NativeMethods.GetClipboardFormatName(format, res, 255) > 0)
                                {
                                    formatName = res.ToString();
                                }
                            }

                            // Get the pointer for the current Clipboard Data
                            var handle = NativeMethods.GetClipboardData(format);
                            if (handle == IntPtr.Zero)
                            {
                                continue;
                            }

                            var pointer = IntPtr.Zero;
                            try
                            {
                                int size = (int)NativeMethods.GlobalSize(handle);
                                if (size > 0)
                                {
                                    pointer = NativeMethods.GlobalLock(handle);
                                    var buffer = new byte[size];

                                    int l = Convert.ToInt32(size.ToString());
                                    Marshal.Copy(pointer, buffer, 0, l);

                                    // Create a ClipData object that represents current clipboard data
                                    clipData.Add(new DataClip(format, formatName, buffer));
                                }
                                else
                                {
                                    clipData.Add(new DataClip(format, formatName, new byte[0]));
                                }
                            }
                            finally
                            {
                                if (pointer != IntPtr.Zero)
                                {
                                    NativeMethods.GlobalUnlock(handle);
                                }
                            }
                        }
                    }
                    finally
                    {
                        NativeMethods.CloseClipboard();
                    }
                },
                20,
                50);

            return new ReadOnlyCollection<DataClip>(clipData);
        }

        /// <summary>
        /// Holds clipboard data of a single data format.
        /// </summary>
        [Serializable]
        public class DataClip
        {
            public DataClip()
            {
            }

            public DataClip(uint format, string formatName, byte[] buffer)
            {
                Format = format;
                FormatName = formatName;
                Buffer = buffer;
            }

            public uint Format { get; set; }

            public string FormatName { get; set; }

            public byte[] Buffer { get; set; }

            public UIntPtr Size
            {
                get { return Buffer != null ? new UIntPtr(Convert.ToUInt32(Buffer.GetLength(0))) : new UIntPtr(0); }
            }
        }
    }
}
