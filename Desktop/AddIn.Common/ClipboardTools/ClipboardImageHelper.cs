using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Eurolook.Common;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;

namespace Eurolook.AddIn.Common.ClipboardTools
{
    /// <summary>
    /// A class with static helper methods addressing WPF clipboard limitations.
    /// </summary>
    /// <remarks>
    /// See https://www.thomaslevesque.com/2009/02/05/wpf-paste-an-image-from-the-clipboard/
    /// </remarks>
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "This class imports an external API")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "This class imports an external API")]
    [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "This class imports an external API")]
    [SuppressMessage("ReSharper", "NotAccessedField.Global", Justification = "This class imports an external API")]
    [SuppressMessage("ReSharper", "FieldCanBeMadeReadOnly.Global", Justification = "This class imports an external API")]
    [SuppressMessage("Sonar", "S2346:Flags enumerations zero-value members should be named \"None\"", Justification = "This class imports an external API")]
    [SuppressMessage("Sonar", "S101:Types should be named in camel case", Justification = "This class imports an external API")]
    [SuppressMessage("StyleCop", "SA1307:Accessible fields must begin with upper-case letter", Justification = "This class imports an external API")]
    [SuppressMessage("StyleCop", "SA1305:Field names must not use Hungarian notation", Justification = "This class imports an external API")]
    public static class ClipboardImageHelper
    {
        private static readonly string[] BitmapFormats =
        {
            DataFormats.Dib,
            DataFormats.Bitmap,
        };

        public static bool ClipboardContainsImage()
        {
            // Accessing the data in the clipboard might result in a COMException (See ticket EUROLOOK-2049)
            // => Try another format!
            foreach (string bitmapFormat in BitmapFormats)
            {
                try
                {
                    var data = Clipboard.GetData(bitmapFormat);
                    return data is MemoryStream || data is InteropBitmap;
                }
                catch (COMException)
                {
                }
            }

            return false;
        }

        public static ImageSource ImageFromClipboardDib()
        {
            foreach (string bitmapFormat in BitmapFormats)
            {
                try
                {
                    var data = Clipboard.GetData(bitmapFormat);
                    if (data is MemoryStream ms)
                    {
                        var msBitmap = new MemoryStream();
                        AddBitmapFileHeader(ms, msBitmap);
                        msBitmap.Seek(0, SeekOrigin.Begin);
                        return BitmapFrame.Create(msBitmap);
                    }

                    if (data is InteropBitmap bitmap)
                    {
                        return bitmap;
                    }
                }
                catch (Exception)
                {
                    // ignored
                }
            }

            LogManager.GetLogger().Error("Failed to load bitmap from clipboard");
            return null;
        }

        public static TemporaryFile GetTemporaryImageFileFromClipboard()
        {
            return GetTemporaryImageFileFromClipboardDib();
        }

        public static TemporaryFile GetTemporaryImageFileFromClipboardDib()
        {
            var ms = Clipboard.GetData("DeviceIndependentBitmap") as MemoryStream;
            if (ms == null)
            {
                return null;
            }

            var result = new TemporaryFile(".bmp");
            using (var fs = new FileStream(result.FullName, FileMode.OpenOrCreate))
            {
                AddBitmapFileHeader(ms, fs);
                fs.Flush();
            }

            return result;
        }

        private static void AddBitmapFileHeader(Stream input, Stream output)
        {
            var dibBuffer = input.ToByteArray();
            var infoHeader = BinaryStructConverter.FromByteArray<BITMAPINFOHEADER>(dibBuffer);

            int fileHeaderSize = Marshal.SizeOf(typeof(BITMAPFILEHEADER));
            int infoHeaderSize = infoHeader.biSize;
            int fileSize = fileHeaderSize + infoHeader.biSize + infoHeader.biSizeImage;

            var fileHeader = new BITMAPFILEHEADER
            {
                bfType = BITMAPFILEHEADER.BM,
                bfSize = fileSize,
                bfReserved1 = 0,
                bfReserved2 = 0,
                bfOffBits = fileHeaderSize + infoHeaderSize + infoHeader.biClrUsed * 4,
            };

            var fileHeaderBytes = BinaryStructConverter.ToByteArray(fileHeader);

            output.Write(fileHeaderBytes, 0, fileHeaderSize);
            output.Write(dibBuffer, 0, dibBuffer.Length);
        }

        [StructLayout(LayoutKind.Sequential, Pack = 2)]
        private struct BITMAPFILEHEADER
        {
            public static readonly short BM = 0x4d42; // BM

            public short bfType;
            public int bfSize;
            public short bfReserved1;
            public short bfReserved2;
            public int bfOffBits;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct BITMAPINFOHEADER
        {
            // ReSharper disable MemberCanBePrivate.Local
            // ReSharper disable FieldCanBeMadeReadOnly.Local
#pragma warning disable S1144 // Unused private types or members should be removed
#pragma warning disable S3459 // Unassigned members should be removed
            public int biSize;
            public int biWidth;
            public int biHeight;
            public short biPlanes;
            public short biBitCount;
            public int biCompression;
            public int biSizeImage;
            public int biXPelsPerMeter;
            public int biYPelsPerMeter;
            public int biClrUsed;
            public int biClrImportant;
#pragma warning restore S3459 // Unassigned members should be removed
#pragma warning restore S1144 // Unused private types or members should be removed
            // ReSharper restore FieldCanBeMadeReadOnly.Local
            // ReSharper restore MemberCanBePrivate.Local
        }

        public static class BinaryStructConverter
        {
            public static T FromByteArray<T>(byte[] bytes)
                where T : struct
            {
                var ptr = IntPtr.Zero;
                try
                {
                    int size = Marshal.SizeOf(typeof(T));
                    ptr = Marshal.AllocHGlobal(size);
                    Marshal.Copy(bytes, 0, ptr, size);
                    var obj = Marshal.PtrToStructure(ptr, typeof(T));
                    return (T)obj;
                }
                finally
                {
                    if (ptr != IntPtr.Zero)
                    {
                        Marshal.FreeHGlobal(ptr);
                    }
                }
            }

            public static byte[] ToByteArray<T>(T obj)
                where T : struct
            {
                var ptr = IntPtr.Zero;
                try
                {
                    int size = Marshal.SizeOf(typeof(T));
                    ptr = Marshal.AllocHGlobal(size);
                    Marshal.StructureToPtr(obj, ptr, true);
                    var bytes = new byte[size];
                    Marshal.Copy(ptr, bytes, 0, size);
                    return bytes;
                }
                finally
                {
                    if (ptr != IntPtr.Zero)
                    {
                        Marshal.FreeHGlobal(ptr);
                    }
                }
            }
        }
    }
}
