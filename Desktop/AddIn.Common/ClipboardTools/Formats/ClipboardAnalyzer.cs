using System;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.ClipboardTools.Formats
{
    public class ClipboardAnalyzer : IClipboardAnalyzer
    {
        [CanBeNull]
        public ArtGvmlClipFormat ArtGvmlClipFormat { get; private set; }

        [CanBeNull]
        public ObjectDescriptorFormat ObjectDescriptorFormat { get; private set; }

        public bool HasExcelSource
        {
            get
            {
                var fullUserTypeName = ObjectDescriptorFormat?.FullUserTypeName;
                return fullUserTypeName != null
                       && fullUserTypeName.IndexOf("Excel", StringComparison.OrdinalIgnoreCase) >= 0;
            }
        }

        public bool HasPowerPointSource
        {
            get
            {
                var fullUserTypeName = ObjectDescriptorFormat?.FullUserTypeName;
                return fullUserTypeName != null
                       && fullUserTypeName.IndexOf("PowerPoint", StringComparison.OrdinalIgnoreCase) >= 0;
            }
        }

        public bool IsExcelRange
        {
            get
            {
                return string.Equals(
                    "Microsoft Excel Worksheet",
                    ObjectDescriptorFormat?.FullUserTypeName,
                    StringComparison.OrdinalIgnoreCase);
            }
        }

        public bool IsExcelChart
        {
            get
            {
                return string.Equals(
                    "Microsoft Excel Chart",
                    ObjectDescriptorFormat?.FullUserTypeName,
                    StringComparison.OrdinalIgnoreCase);
            }
        }

        public bool IsOfficeShape
        {
            get
            {
                var format = ArtGvmlClipFormat?.Format ?? ArtGvmlClipFormat.DrawingFormat.Unknown;
                return format == ArtGvmlClipFormat.DrawingFormat.Shape
                       || format == ArtGvmlClipFormat.DrawingFormat.GroupShape
                       || format == ArtGvmlClipFormat.DrawingFormat.TextShape;
            }
        }

        public void Analyze()
        {
            ArtGvmlClipFormat = ArtGvmlClipFormat.CreateFromClipboard();
            ObjectDescriptorFormat = ObjectDescriptorFormat.CreateFromClipboard();
        }
    }
}
