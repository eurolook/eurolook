using System;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using System.Windows;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Eurolook.Common.Log;

namespace Eurolook.AddIn.Common.ClipboardTools.Formats
{
    public class ArtGvmlClipFormat : ICanLog
    {
        private const string RelationshipTypeDrawing =
            "http://schemas.openxmlformats.org/officeDocument/2006/relationships/drawing";

        private static readonly XNamespace RelationShipsNs =
            "http://schemas.openxmlformats.org/officeDocument/2006/relationships";

        private static readonly XNamespace DrawingMlNs = "http://schemas.openxmlformats.org/drawingml/2006/main";

        private static readonly XNamespace LockedCanvasNs =
            "http://schemas.openxmlformats.org/drawingml/2006/lockedCanvas";

        private static readonly XNamespace ChartNs = "http://schemas.openxmlformats.org/drawingml/2006/chart";

        private ArtGvmlClipFormat()
        {
        }

        public static ArtGvmlClipFormat CreateFromClipboard()
        {
            try
            {
                if (!Clipboard.ContainsData(OfficeDataFormats.ArtGvmlClipFormat))
                {
                    return null;
                }

                var stream = Clipboard.GetData(OfficeDataFormats.ArtGvmlClipFormat) as Stream;
                if (stream == null)
                {
                    return null;
                }

                return CreateFromStream(stream);
            }
            catch (Exception ex)
            {
                LogManager.GetLogger().Warn("Could not get data from clipboard.", ex);
            }

            return null;
        }

        public enum DrawingFormat
        {
            Unknown,
            Chart,
            Picture,
            GroupShape,
            Shape,
            TextShape,
        }

        public DrawingFormat Format { get; private set; } = DrawingFormat.Unknown;

        public string ContentType { get; private set; }

        public static ArtGvmlClipFormat CreateFromStream(Stream data)
        {
            try
            {
                using (var package = Package.Open(data, FileMode.Open, FileAccess.Read))
                {
                    // get drawing part
                    var relationship = package.GetRelationshipsByType(RelationshipTypeDrawing).FirstOrDefault();
                    if (relationship == null)
                    {
                        return null;
                    }

                    var uriDocumentTarget = PackUriHelper.ResolvePartUri(
                        relationship.SourceUri,
                        relationship.TargetUri);

                    var drawingPart = package.GetPart(uriDocumentTarget);
                    using (var drawingPartStream = drawingPart.GetStream(FileMode.Open, FileAccess.Read))
                    {
                        var xDrawingPart = XDocument.Load(drawingPartStream);

                        var namespaceManager = new XmlNamespaceManager(new NameTable());
                        namespaceManager.AddNamespace("a", DrawingMlNs.NamespaceName);
                        namespaceManager.AddNamespace("lc", LockedCanvasNs.NamespaceName);
                        namespaceManager.AddNamespace("c", ChartNs.NamespaceName);
                        namespaceManager.AddNamespace("r", RelationShipsNs.NamespaceName);

                        // group shape
                        var groupShapeElement = xDrawingPart.XPathSelectElement("//a:grpSp", namespaceManager);
                        if (groupShapeElement != null)
                        {
                            return new ArtGvmlClipFormat
                            {
                                ContentType = drawingPart.ContentType,
                                Format = DrawingFormat.GroupShape,
                            };
                        }

                        // charts
                        var chartElement = xDrawingPart.XPathSelectElement(
                            "/a:graphic/a:graphicData/lc:lockedCanvas/a:graphicFrame/a:graphic/a:graphicData/c:chart",
                            namespaceManager);
                        if (chartElement != null)
                        {
                            string relId = (string)chartElement.Attribute(RelationShipsNs + "id");
                            return new ArtGvmlClipFormat
                            {
                                ContentType = GetRelatedPartContentType(drawingPart, relId),
                                Format = DrawingFormat.Chart,
                            };
                        }

                        // shape
                        var shapeElement = xDrawingPart.XPathSelectElement("//a:sp", namespaceManager);
                        if (shapeElement != null)
                        {
                            return new ArtGvmlClipFormat
                            {
                                ContentType = drawingPart.ContentType,
                                Format = DrawingFormat.Shape,
                            };
                        }

                        // text shape
                        var textShapeElement = xDrawingPart.XPathSelectElement("//a:txSp", namespaceManager);
                        if (textShapeElement != null)
                        {
                            return new ArtGvmlClipFormat
                            {
                                ContentType = drawingPart.ContentType,
                                Format = DrawingFormat.TextShape,
                            };
                        }

                        // pictures
                        var blipElement = xDrawingPart.XPathSelectElement("//a:blip", namespaceManager);
                        if (blipElement != null)
                        {
                            string relId = (string)blipElement.Attribute(RelationShipsNs + "embed");
                            return new ArtGvmlClipFormat
                            {
                                ContentType = GetRelatedPartContentType(drawingPart, relId),
                                Format = DrawingFormat.Picture,
                            };
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger().Warn("Could not get data from clipboard.", ex);
            }

            return null;
        }

        private static string GetRelatedPartContentType(PackagePart basePart, string relId)
        {
            var relationship = basePart.GetRelationship(relId);

            var partUri = PackUriHelper.ResolvePartUri(relationship.SourceUri, relationship.TargetUri);
            var part = basePart.Package.GetPart(partUri);

            return part.ContentType;
        }
    }
}
