namespace Eurolook.AddIn.Common.ClipboardTools.Formats
{
    public static class OfficeDataFormats
    {
        public const string ObjectDescriptor = "Object Descriptor";
        public const string ArtGvmlClipFormat = "Art::GVML ClipFormat";
    }
}
