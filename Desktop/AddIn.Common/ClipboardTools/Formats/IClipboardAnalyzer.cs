namespace Eurolook.AddIn.Common.ClipboardTools.Formats
{
    public interface IClipboardAnalyzer
    {
        ArtGvmlClipFormat ArtGvmlClipFormat { get; }

        ObjectDescriptorFormat ObjectDescriptorFormat { get; }

        bool HasExcelSource { get; }

        bool HasPowerPointSource { get; }

        bool IsExcelRange { get; }

        bool IsExcelChart { get; }

        bool IsOfficeShape { get; }

        void Analyze();
    }
}
