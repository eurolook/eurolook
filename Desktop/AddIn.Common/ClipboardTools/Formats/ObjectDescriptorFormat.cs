using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using Eurolook.Common.Log;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.ClipboardTools.Formats
{
    public class ObjectDescriptorFormat : ICanLog
    {
        private ObjectDescriptorFormat()
        {
        }

        [CanBeNull]
        public static ObjectDescriptorFormat CreateFromClipboard()
        {
            try
            {
                if (!Clipboard.ContainsData(OfficeDataFormats.ObjectDescriptor))
                {
                    return null;
                }

                var data = Clipboard.GetData(OfficeDataFormats.ObjectDescriptor) as MemoryStream;
                if (data == null)
                {
                    return null;
                }

                var buffer = data.ToArray();
                var result = FromBytes(buffer);

                return new ObjectDescriptorFormat
                {
                    Guid = (Guid)result.clsid,
                    FullUserTypeName = Encoding.Unicode.GetString(
                        buffer,
                        result.dwFullUserTypeName,
                        result.dwSrcOfCopy - result.dwFullUserTypeName).Split('\0').First(),
                    SrcOfCopy = Encoding.Unicode.GetString(
                        buffer,
                        result.dwSrcOfCopy,
                        result.cbSize - result.dwSrcOfCopy).Split('\0').First(),
                };
            }
            catch (Exception ex)
            {
                LogManager.GetLogger().Warn("Could not get data from clipboard.", ex);
                return null;
            }
        }

        /// <summary>
        /// Gets the full user type name of the object being transferred. This string is used by the destination of a
        /// data transfer to create labels in the Paste Special dialog box. The destination application must be able
        /// to handle the cases when this string is omitted.
        /// </summary>
        public string FullUserTypeName { get; private set; }

        /// <summary>
        /// Gets the display name of the temporary moniker that identifies the data source. The value for dwSrcOfCopy is
        /// displayed in the Source line of the Paste Special dialog box.
        /// </summary>
        public string SrcOfCopy { get; private set; }

        /// <summary>
        /// Gets the CLSID of the object being transferred. The clsid is used to obtain the icon for the
        /// Display As Icon option in the Paste Special dialog box and is applicable only if the Embed Source
        /// or Embedded Object formats are offered.
        /// </summary>
        public Guid Guid { get; private set; }

        private static OBJECTDESCRIPTOR FromBytes(byte[] arr)
        {
            var str = default(OBJECTDESCRIPTOR);

            var size = Marshal.SizeOf(str);
            var ptr = Marshal.AllocHGlobal(size);

            Marshal.Copy(arr, 0, ptr, size);

            str = (OBJECTDESCRIPTOR)Marshal.PtrToStructure(ptr, str.GetType());
            Marshal.FreeHGlobal(ptr);

            return str;
        }

        [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "This type imports an external API")]
        [SuppressMessage("ReSharper", "NotAccessedField.Local", Justification = "This type imports an external API")]
        [SuppressMessage(
            "ReSharper",
            "FieldCanBeMadeReadOnly.Local",
            Justification = "This type imports an external API")]
        [SuppressMessage("ReSharper", "MemberCanBePrivate.Local", Justification = "This type imports an external API")]
        [SuppressMessage(
            "Sonar",
            "S101:Types should be named in camel case",
            Justification = "This type imports an external API")]
        [SuppressMessage(
            "StyleCop.CSharp.NamingRules",
            "SA1307:Accessible fields should begin with upper-case letter",
            Justification = "This type imports an external API")]
        [SuppressMessage(
            "StyleCop.CSharp.NamingRules",
            "SA1305:Field names should not use Hungarian notation",
            Justification = "This type imports an external API")]
        [StructLayout(LayoutKind.Sequential)]
        private struct OBJECTDESCRIPTOR
        {
            public int cbSize;
            public SafeNativeMethods.CLSID clsid;
            public uint dwDrawAspect;
            public SafeNativeMethods.SIZEL sizel;
            public SafeNativeMethods.POINTL pointl;
            public uint dwStatus;
            public int dwFullUserTypeName;
            public int dwSrcOfCopy;
        }
    }
}
