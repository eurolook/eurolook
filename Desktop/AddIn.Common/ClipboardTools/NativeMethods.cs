using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using System.Text;

#pragma warning disable S101 // Types should be named in camel case
#pragma warning disable S2342 // Enumeration types should comply with a naming convention
#pragma warning disable S2344 // Enumeration type names should not have "Flags" or "Enum" suffixes
#pragma warning disable S2346 // Flags enumerations zero-value members should be named "None"

namespace Eurolook.AddIn.Common.ClipboardTools
{
    /// <summary>
    /// A class importing native methods as defined by
    /// http://msdn.microsoft.com/en-us/library/btadwd4w%28vs.100%29.aspx.
    /// </summary>
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "This class imports an external API")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "This class imports an external API")]
    [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "This class imports an external API")]
    [SuppressMessage("ReSharper", "NotAccessedField.Global", Justification = "This class imports an external API")]
    [SuppressMessage("ReSharper", "FieldCanBeMadeReadOnly.Global", Justification = "This class imports an external API")]
    internal static class NativeMethods
    {
        public enum ClipFormat
        {
            CF_TEXT = 1,
            CF_BITMAP = 2,
            CF_METAFILEPICT = 3,
            CF_SYLK = 4,
            CF_DIF = 5,
            CF_TIFF = 6,
            CF_OEMTEXT = 7,
            CF_DIB = 8,
            CF_PALETTE = 9,
            CF_PENDATA = 10,
            CF_RIFF = 11,
            CF_WAVE = 12,
            CF_UNICODETEXT = 13,
            CF_ENHMETAFILE = 14,
            CF_HDROP = 15,
            CF_LOCALE = 16,
            CF_MAX = 17,
            CF_OWNERDISPLAY = 0x80,
            CF_DSPTEXT = 0x81,
            CF_DSPBITMAP = 0x82,
            CF_DSPMETAFILEPICT = 0x83,
            CF_DSPENHMETAFILE = 0x8E,
        }

        [Flags]
        public enum GlobalMemoryFlags : uint
        {
            GMEM_FIXED = 0x0000,
            GMEM_MOVEABLE = 0x0002,
            GMEM_ZEROINIT = 0x0040,
            GMEM_MODIFY = 0x0080,
            GMEM_VALID_FLAGS = 0x7F72,
            GMEM_INVALID_HANDLE = 0x8000,
            GHND = GMEM_MOVEABLE | GMEM_ZEROINIT,
            GPTR = GMEM_FIXED | GMEM_ZEROINIT,

            /* The following values are obsolete, but are provided for compatibility with 16-bit Windows. They are ignored. */
            GMEM_DDESHARE = 0x2000,
            GMEM_DISCARDABLE = 0x0100,
            GMEM_LOWER = GMEM_NOT_BANKED,
            GMEM_NOCOMPACT = 0x0010,
            GMEM_NODISCARD = 0x0020,
            GMEM_NOT_BANKED = 0x1000,
            GMEM_NOTIFY = 0x4000,
            GMEM_SHARE = 0x2000,
        }

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool OpenClipboard(IntPtr hWndNewOwner);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool EmptyClipboard();

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr GetClipboardData(uint uFormat);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr SetClipboardData(uint uFormat, IntPtr hMem);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool CloseClipboard();

        [DllImport("user32.dll", SetLastError = true)]
        public static extern uint EnumClipboardFormats(uint format);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern int GetClipboardFormatName(uint format, [Out] StringBuilder lpszFormatName, int cchMaxCount);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern uint RegisterClipboardFormat(string lpszFormat);

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern bool IsClipboardFormatAvailable(uint format);

        [DllImport("kernel32.dll")]
        public static extern IntPtr GlobalAlloc(uint uFlags, UIntPtr dwBytes);

        [DllImport("kernel32.dll")]
        public static extern IntPtr GlobalLock(IntPtr hMem);

        [DllImport("kernel32.dll")]
        public static extern IntPtr GlobalUnlock(IntPtr hMem);

        [DllImport("kernel32.dll")]
        public static extern IntPtr GlobalFree(IntPtr hMem);

        [DllImport("kernel32.dll")]
        public static extern UIntPtr GlobalSize(IntPtr hMem);
    }
}
