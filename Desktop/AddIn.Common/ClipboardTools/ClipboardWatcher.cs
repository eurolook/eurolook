using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Eurolook.AddIn.Common.ClipboardTools
{
    /// <summary>
    /// Provides notifications when the contents of the clipboard is updated.
    /// </summary>
    public static class ClipboardWatcher
    {
        // See http://msdn.microsoft.com/en-us/library/ms649021%28v=vs.85%29.aspx
#pragma warning disable SA1310 // Field names must not contain underscore
        // ReSharper disable once InconsistentNaming
        private const int WM_CLIPBOARDUPDATE = 0x031D;
#pragma warning restore SA1310 // Field names must not contain underscore

        private static readonly IntPtr HwndMessage = new IntPtr(-3);

        // ReSharper disable once UnusedMember.Local
#pragma warning disable S1144 // Unused private types or members should be removed
        private static readonly NotificationForm Form = new NotificationForm();
#pragma warning restore S1144 // Unused private types or members should be removed

        /// <summary>
        /// Occurs when the contents of the clipboard is updated.
        /// </summary>
        public static event EventHandler ClipboardUpdate;

        /// <summary>
        /// Raises the <see cref="ClipboardUpdate" /> event.
        /// </summary>
        /// <param name="e">Event arguments for the event.</param>
        private static void OnClipboardUpdate(EventArgs e)
        {
            ClipboardUpdate?.Invoke(null, e);
        }

        // See http://msdn.microsoft.com/en-us/library/ms632599%28VS.85%29.aspx#message_only
        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool AddClipboardFormatListener(IntPtr hwnd);

        // See http://msdn.microsoft.com/en-us/library/ms633541%28v=vs.85%29.aspx
        // See http://msdn.microsoft.com/en-us/library/ms649033%28VS.85%29.aspx
        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        /// <summary>
        /// Hidden form to receive the WM_CLIPBOARDUPDATE message.
        /// </summary>
        private class NotificationForm : Form
        {
            public NotificationForm()
            {
                SetParent(Handle, HwndMessage);
                AddClipboardFormatListener(Handle);
            }

            protected override void WndProc(ref Message m)
            {
                if (m.Msg == WM_CLIPBOARDUPDATE)
                {
                    OnClipboardUpdate(null);
                }

                base.WndProc(ref m);
            }
        }
    }
}
