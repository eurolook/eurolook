using System;
using Eurolook.AddIn.Common.Events;

namespace Eurolook.AddIn.Common
{
    public interface IApplicationContext
    {
    }

    public interface IApplicationContext<out TApplication> : IDisposable, IApplicationContext
    {
        TApplication Application { get; }

        InputDetector InputDetector { get; }
    }
}
