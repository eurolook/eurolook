using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.Wpf
{
    public class CalloutControl : ContentControl
    {
        public static readonly DependencyProperty CalloutPlacementProperty = DependencyProperty.Register(
            "CalloutPlacement",
            typeof(CalloutPlacementMode),
            typeof(CalloutControl),
            new PropertyMetadata(CalloutPlacementMode.Left));

        public static readonly DependencyProperty CalloutMarginProperty = DependencyProperty.Register(
            "CalloutMargin",
            typeof(Thickness),
            typeof(CalloutControl),
            new PropertyMetadata(new Thickness(10)));

        public static readonly DependencyProperty CalloutOffsetProperty = DependencyProperty.Register(
            "CalloutOffset",
            typeof(double),
            typeof(CalloutControl),
            new PropertyMetadata(60.0));

        public static readonly DependencyProperty DropShadowBlurRadiusProperty = DependencyProperty.Register(
            "DropShadowBlurRadius",
            typeof(double),
            typeof(CalloutControl),
            new PropertyMetadata(5.0));

        public static readonly DependencyProperty DropShadowColorProperty = DependencyProperty.Register(
            "DropShadowColor",
            typeof(Color),
            typeof(CalloutControl),
            new PropertyMetadata(Colors.DimGray));

        public static readonly DependencyProperty DropShadowDirectionProperty = DependencyProperty.Register(
            "DropShadowDirection",
            typeof(double),
            typeof(CalloutControl),
            new PropertyMetadata(315.0));

        public static readonly DependencyProperty RelativeCalloutOffsetProperty = DependencyProperty.Register(
            "RelativeCalloutOffset",
            typeof(double),
            typeof(CalloutControl),
            new PropertyMetadata(default(double)));

        public static readonly DependencyProperty UseRelativeCalloutOffsetProperty = DependencyProperty.Register(
            "UseRelativeCalloutOffset",
            typeof(bool),
            typeof(CalloutControl),
            new PropertyMetadata(false));

        public static readonly DependencyProperty StrokeThicknessProperty = DependencyProperty.Register(
            "StrokeThickness",
            typeof(double),
            typeof(CalloutControl),
            new PropertyMetadata(1.0));

        public static readonly DependencyProperty StrokeBrushProperty = DependencyProperty.Register(
            "StrokeBrush",
            typeof(Brush),
            typeof(CalloutControl),
            new PropertyMetadata(default(Brush)));

        public enum CalloutPlacementMode
        {
            None,
            Left,
            Top,
            Right,
            Bottom,
        }

        [UsedImplicitly]
        public double DropShadowDirection
        {
            get { return (double)GetValue(DropShadowDirectionProperty); }
            set { SetValue(DropShadowDirectionProperty, value); }
        }

        [UsedImplicitly]
        public Color DropShadowColor
        {
            get { return (Color)GetValue(DropShadowColorProperty); }
            set { SetValue(DropShadowColorProperty, value); }
        }

        [UsedImplicitly]
        public double DropShadowBlurRadius
        {
            get { return (double)GetValue(DropShadowBlurRadiusProperty); }
            set { SetValue(DropShadowBlurRadiusProperty, value); }
        }

        [UsedImplicitly]
        public bool UseRelativeCalloutOffset
        {
            get { return (bool)GetValue(UseRelativeCalloutOffsetProperty); }
            set { SetValue(UseRelativeCalloutOffsetProperty, value); }
        }

        [UsedImplicitly]
        public double RelativeCalloutOffset
        {
            get { return (double)GetValue(RelativeCalloutOffsetProperty); }
            set { SetValue(RelativeCalloutOffsetProperty, value); }
        }

        [UsedImplicitly]
        public CalloutPlacementMode CalloutPlacement
        {
            get { return (CalloutPlacementMode)GetValue(CalloutPlacementProperty); }
            set { SetValue(CalloutPlacementProperty, value); }
        }

        [UsedImplicitly]
        public Thickness CalloutMargin
        {
            get { return (Thickness)GetValue(CalloutMarginProperty); }
            set { SetValue(CalloutMarginProperty, value); }
        }

        [UsedImplicitly]
        public double CalloutOffset
        {
            get { return (double)GetValue(CalloutOffsetProperty); }
            set { SetValue(CalloutOffsetProperty, value); }
        }

        [UsedImplicitly]
        public Brush StrokeBrush
        {
            get { return (Brush)GetValue(StrokeBrushProperty); }
            set { SetValue(StrokeBrushProperty, value); }
        }

        [UsedImplicitly]
        public double StrokeThickness
        {
            get { return (double)GetValue(StrokeThicknessProperty); }
            set { SetValue(StrokeThicknessProperty, value); }
        }

        [UsedImplicitly]
        public double ActualCalloutWidth
        {
            get { return ActualWidth - DropShadowBlurRadius - StrokeThickness; }
        }

        [UsedImplicitly]
        public double ActualCalloutHeight
        {
            get { return ActualHeight - DropShadowBlurRadius - StrokeThickness; }
        }

        private Rect ActualCalloutRect
        {
            get
            {
                var actualCalloutRect = new Rect(
                    new Point(CalloutMargin.Left + StrokeThickness, CalloutMargin.Top + StrokeThickness),
                    new Point(ActualCalloutWidth - CalloutMargin.Right, ActualCalloutHeight - CalloutMargin.Bottom));
                return actualCalloutRect;
            }
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (e.Property == CalloutOffsetProperty
                || e.Property == UseRelativeCalloutOffsetProperty
                || e.Property == RelativeCalloutOffsetProperty
                || e.Property == CalloutMarginProperty
                || e.Property == CalloutPlacementProperty
                || e.Property == StrokeBrushProperty
                || e.Property == StrokeThicknessProperty
                || e.Property == DropShadowBlurRadiusProperty
                || e.Property == DropShadowDirectionProperty
                || e.Property == DropShadowColorProperty)
            {
                // force recalculation of callout polygon
                InvalidateVisual();
            }

            base.OnPropertyChanged(e);
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            AddCalloutPolygon();

            AdjustContentPartMargin();
        }

        private void AddCalloutPolygon()
        {
            var canvas = (Canvas)Template.FindName("Callout_PART", this);
            if (canvas == null)
            {
                throw new InvalidOperationException("Required Template part 'Callout_PART' not found.");
            }

            canvas.Children.Clear();

            // add one polygon with a drop shadow
            var dropShadowPolygon = GenerateCalloutPolygon();
            dropShadowPolygon.StrokeThickness = 0;
            dropShadowPolygon.Effect = new DropShadowEffect
            {
                Color = DropShadowColor,
                Direction = DropShadowDirection,
                BlurRadius = DropShadowBlurRadius,
            };
            canvas.Children.Add(dropShadowPolygon);

            // add an identical polygon on top to avoid blurriness
            var polygon = GenerateCalloutPolygon();
            canvas.Children.Add(polygon);
        }

        private void AdjustContentPartMargin()
        {
            var contentPart = (Grid)Template.FindName("Content_PART", this);
            if (contentPart == null)
            {
                throw new InvalidOperationException("Required Template part 'Content_PART' not found.");
            }

            contentPart.Margin = new Thickness(
                CalloutMargin.Left + 1.5 * StrokeThickness,
                CalloutMargin.Top + 1.5 * StrokeThickness,
                CalloutMargin.Right + DropShadowBlurRadius + 1.5 * StrokeThickness,
                CalloutMargin.Bottom + DropShadowBlurRadius + 1.5 * StrokeThickness);
        }

        private Polygon GenerateCalloutPolygon()
        {
            var polygonPoints = ComputeCalloutPolygonPoints();

            var polygon = new Polygon
            {
                Fill = Background,
                Stroke = StrokeBrush,
                SnapsToDevicePixels = true,
                StrokeThickness = StrokeThickness,
                Points = polygonPoints,
            };

            RenderOptions.SetEdgeMode(polygon, EdgeMode.Aliased);
            return polygon;
        }

        private PointCollection ComputeCalloutPolygonPoints()
        {
            var points = new PointCollection();

            double calloutSize = ComputeCalloutSize();
            double calloutOffset = ComputeCalloutOffset(calloutSize);

            var actualCalloutRect = ActualCalloutRect;

            // top-left
            points.Add(actualCalloutRect.TopLeft);

            // top callout
            if (CalloutPlacement == CalloutPlacementMode.Top)
            {
                points.Add(new Point(calloutOffset - calloutSize, actualCalloutRect.Top));
                points.Add(new Point(calloutOffset, actualCalloutRect.Top - calloutSize));
                points.Add(new Point(calloutOffset + calloutSize, actualCalloutRect.Top));
            }

            // top-right
            points.Add(actualCalloutRect.TopRight);

            // right callout
            if (CalloutPlacement == CalloutPlacementMode.Right)
            {
                points.Add(new Point(actualCalloutRect.Right, calloutOffset - calloutSize));
                points.Add(new Point(actualCalloutRect.Right + calloutSize, calloutOffset));
                points.Add(new Point(actualCalloutRect.Right, calloutOffset + calloutSize));
            }

            // bottom-right
            points.Add(actualCalloutRect.BottomRight);

            // bottom callout
            if (CalloutPlacement == CalloutPlacementMode.Bottom)
            {
                points.Add(new Point(calloutOffset + calloutSize, actualCalloutRect.Bottom));
                points.Add(new Point(calloutOffset, actualCalloutRect.Bottom + calloutSize));
                points.Add(new Point(calloutOffset - calloutSize, actualCalloutRect.Bottom));
            }

            // bottom left
            points.Add(actualCalloutRect.BottomLeft);

            // left callout
            if (CalloutPlacement == CalloutPlacementMode.Left)
            {
                points.Add(new Point(actualCalloutRect.Left, calloutOffset + calloutSize));
                points.Add(new Point(actualCalloutRect.Left - calloutSize, calloutOffset));
                points.Add(new Point(actualCalloutRect.Left, calloutOffset - calloutSize));
            }

            return points;
        }

        private double ComputeCalloutSize()
        {
            double innerHeight = ActualCalloutHeight - CalloutMargin.Top - CalloutMargin.Bottom - StrokeThickness;
            double innerWidth = ActualCalloutWidth - CalloutMargin.Left - CalloutMargin.Right - StrokeThickness;

            double calloutSize = 10.0;
            switch (CalloutPlacement)
            {
                case CalloutPlacementMode.Left:
                    calloutSize = Math.Min(CalloutMargin.Left, innerHeight / 2.0);
                    break;
                case CalloutPlacementMode.Right:
                    calloutSize = Math.Min(CalloutMargin.Right, innerHeight / 2.0);
                    break;
                case CalloutPlacementMode.Top:
                    calloutSize = Math.Min(CalloutMargin.Top, innerWidth / 2.0);
                    break;
                case CalloutPlacementMode.Bottom:
                    calloutSize = Math.Min(CalloutMargin.Bottom, innerWidth / 2.0);
                    break;
            }

            return calloutSize;
        }

        private double ComputeCalloutOffset(double calloutSize)
        {
            double actualCalloutOffset = CalloutOffset;
            if (UseRelativeCalloutOffset)
            {
                switch (CalloutPlacement)
                {
                    case CalloutPlacementMode.Left:
                    case CalloutPlacementMode.Right:
                        double innerHeight = ActualCalloutRect.Bottom - ActualCalloutRect.Top;
                        actualCalloutOffset = (RelativeCalloutOffset * innerHeight) + ActualCalloutRect.Top;
                        break;
                    case CalloutPlacementMode.Top:
                    case CalloutPlacementMode.Bottom:
                        double innerWidth = ActualCalloutRect.Width;
                        actualCalloutOffset = RelativeCalloutOffset * innerWidth + ActualCalloutRect.Left;
                        break;
                }
            }

            switch (CalloutPlacement)
            {
                case CalloutPlacementMode.Left:
                case CalloutPlacementMode.Right:
                    actualCalloutOffset = Math.Max(actualCalloutOffset, calloutSize + ActualCalloutRect.Top);
                    actualCalloutOffset = Math.Min(ActualCalloutRect.Bottom - calloutSize, actualCalloutOffset);
                    break;
                case CalloutPlacementMode.Top:
                case CalloutPlacementMode.Bottom:
                    actualCalloutOffset = Math.Max(actualCalloutOffset, calloutSize + ActualCalloutRect.Left);
                    actualCalloutOffset = Math.Min(ActualCalloutRect.Right - calloutSize, actualCalloutOffset);
                    break;
            }

            return actualCalloutOffset;
        }
    }
}
