﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace Eurolook.AddIn.Common.Wpf
{
    public class MultiBindingLogicalAndConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                return (bool)values[0] && (bool)values[1];
            }
            catch (Exception)
            {
                return DependencyProperty.UnsetValue;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return Enumerable.Range(1, targetTypes.Length).Select(t => DependencyProperty.UnsetValue).ToArray();
        }
    }
}
