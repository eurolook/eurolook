using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace Eurolook.AddIn.Common.Wpf
{
    public class ListBoxItemDoubleClickCommandBehavior : Behavior<ListBox>
    {
        public static readonly DependencyProperty CommandProperty = DependencyProperty.Register(
            "Command",
            typeof(ICommand),
            typeof(ListBoxItemDoubleClickCommandBehavior),
            new PropertyMetadata(default(ICommand)));

        public static readonly DependencyProperty CommandParameterProperty = DependencyProperty.Register(
            "CommandParameter",
            typeof(object),
            typeof(ListBoxItemDoubleClickCommandBehavior),
            new PropertyMetadata(default(object)));

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        protected override void OnAttached()
        {
            AssociatedObject.MouseDoubleClick += AssociatedObjectOnMouseDoubleClick;
            base.OnAttached();
        }

        protected override void OnDetaching()
        {
            AssociatedObject.MouseDoubleClick -= AssociatedObjectOnMouseDoubleClick;
            base.OnDetaching();
        }

        private void AssociatedObjectOnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var item = ItemsControl.ContainerFromElement(AssociatedObject, (DependencyObject)e.OriginalSource) as ListBoxItem;
            if (item == null)
            {
                // double-click wasn't on a ListBoxItem
                return;
            }

            if (Command?.CanExecute(CommandParameter) == true)
            {
                Command?.Execute(CommandParameter);
            }
        }
    }
}
