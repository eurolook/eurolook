using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace Eurolook.AddIn.Common.Wpf
{
    [ValueConversion(typeof(List<string>), typeof(string))]
    public class StringListToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType != typeof(string))
            {
                throw new InvalidOperationException();
            }

            return string.Join(Environment.NewLine, (List<string>)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
