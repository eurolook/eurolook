using System;
using System.Globalization;
using System.Windows.Data;

namespace Eurolook.AddIn.Common.Wpf
{
    public class GreaterThanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                double dblValue = double.Parse(value.ToString());
                double dblParam = double.Parse(parameter.ToString());
                return dblValue > dblParam;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new Exception("LessThanConverter cannot ConvertBack.");
        }
    }
}
