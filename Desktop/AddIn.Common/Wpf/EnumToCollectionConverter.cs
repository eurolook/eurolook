using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace Eurolook.AddIn.Common.Wpf
{
    public class EnumToCollectionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Enum e)
            {
                return Enum.GetValues(e.GetType()).Cast<Enum>().ToList();
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
