using System.Windows;
using System.Windows.Controls;

namespace Eurolook.AddIn.Common.Wpf
{
    public class EditableComboBox
    {
        public static readonly DependencyProperty MaxLengthProperty = DependencyProperty.RegisterAttached(
            "MaxLength",
            typeof(int),
            typeof(EditableComboBox),
            new UIPropertyMetadata(OnMaxLengthChanged));

        public static readonly DependencyProperty TextWrappingProperty = DependencyProperty.RegisterAttached(
            "TextWrapping",
            typeof(TextWrapping),
            typeof(EditableComboBox),
            new UIPropertyMetadata(OnTextWrappingChanged));

        public static int GetMaxLength(DependencyObject obj)
        {
            return (int)obj.GetValue(MaxLengthProperty);
        }

        public static void SetMaxLength(DependencyObject obj, int value)
        {
            obj.SetValue(MaxLengthProperty, value);
        }

        public static TextWrapping GetTextWrapping(DependencyObject obj)
        {
            return (TextWrapping)obj.GetValue(TextWrappingProperty);
        }

        public static void SetTextWrapping(DependencyObject obj, TextWrapping value)
        {
            obj.SetValue(TextWrappingProperty, value);
        }

        private static void OnMaxLengthChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            if (obj is not ComboBox comboBox)
            {
                return;
            }

            comboBox.Loaded +=
                (_, _) =>
                {
                    var textBox = (TextBox)comboBox.Template.FindName("PART_EditableTextBox", comboBox);
                    if (textBox == null)
                    {
                        return;
                    }

                    textBox.SetValue(TextBox.MaxLengthProperty, args.NewValue);
                };
        }

        private static void OnTextWrappingChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            if (obj is not ComboBox comboBox)
            {
                return;
            }

            comboBox.Loaded +=
                (_, _) =>
                {
                    var textBox = (TextBox)comboBox.Template.FindName("PART_EditableTextBox", comboBox);
                    if (textBox == null)
                    {
                        return;
                    }

                    textBox.SetValue(TextBox.TextWrappingProperty, args.NewValue);
                };
        }
    }
}
