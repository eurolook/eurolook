using System;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.ProfileInitializer;

namespace Eurolook.AddIn.Common.Wpf
{
    public class InitializationRelayCommand : RelayCommandBase
    {
        private readonly IProfileInitializer _profileInitializer;
        private readonly Action _onExecute;
        private readonly Func<Task> _onExecuteAsync;

        public InitializationRelayCommand(Action execute, Func<bool> canExecute, IProfileInitializer profileInitializer, bool keepTargetAlive = false)
        {
            _profileInitializer = profileInitializer;
            _onExecute = execute;
            SetCanExecuteAction(canExecute, keepTargetAlive);
            SetExecuteAction(InternalExecute, keepTargetAlive);
        }

        public InitializationRelayCommand(Func<Task> execute, Func<bool> canExecute, IProfileInitializer profileInitializer, bool keepTargetAlive = false)
        {
            _profileInitializer = profileInitializer;
            _onExecuteAsync = execute;
            SetCanExecuteAction(canExecute, keepTargetAlive);
            SetExecuteAction(InternalExecute, keepTargetAlive);
        }

        private async void InternalExecute()
        {
            if (!await _profileInitializer.InitializeProfileIfNecessary())
            {
                return;
            }

            if (await _profileInitializer.ShowWelcomeWindowIfNecessary())
            {
                if (_onExecuteAsync != null)
                {
                    await _onExecuteAsync.Invoke();
                }
                else
                {
                    _onExecute?.Invoke();
                }
            }
        }
    }
}
