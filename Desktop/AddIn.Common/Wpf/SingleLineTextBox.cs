using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

namespace Eurolook.AddIn.Common.Wpf
{
    /// <summary>
    /// A single-line textbox that replaces newline characters with blanks when pasting text.
    /// </summary>
    public class SingleLineTextBox : TextBox
    {
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            DataObject.AddPastingHandler(this, OnTextBoxPaste);
        }

        private void OnTextBoxPaste(object sender, DataObjectPastingEventArgs e)
        {
            string clipboardText = e.DataObject.GetData(typeof(string)) as string;
            if (clipboardText == null)
            {
                return;
            }

            var filterPattern = new Regex(@"\s*\r?\n\s*");
            string result = filterPattern.Replace(clipboardText, " ");

            int start = SelectionStart;
            int length = SelectionLength;
            int caret = CaretIndex;

            string text = Text.Substring(0, start);
            text += Text.Substring(start + length);

            string newText = text.Substring(0, CaretIndex) + result;
            newText += text.Substring(caret);
            Text = newText;
            CaretIndex = caret + result.Length;

            e.CancelCommand();
        }
    }
}
