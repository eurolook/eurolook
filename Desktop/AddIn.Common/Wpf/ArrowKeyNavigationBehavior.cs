using System;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Interactivity;
using Eurolook.AddIn.Common.Extensions;

namespace Eurolook.AddIn.Common.Wpf
{
    /// <summary>
    /// This class enables arrow key navigation in grouped list boxes.
    /// </summary>
    /// <remarks>
    /// In grouped list boxes, the navigation using the up/down keys only works within the same group.
    /// This behavior moves the  current item also between groups and expands (if necessary) the new group.
    /// </remarks>
    public class ArrowKeyNavigationBehavior : Behavior<ListBox>
    {
        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.PreviewKeyDown += OnListBoxKeyDown;
            AssociatedObject.SelectionChanged += OnSelectionChanged;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            if (AssociatedObject == null)
            {
                return;
            }

            AssociatedObject.KeyDown -= OnListBoxKeyDown;
            AssociatedObject.SelectionChanged -= OnSelectionChanged;
        }

        private void OnListBoxKeyDown(object sender, KeyEventArgs e)
        {
            if (AssociatedObject == null || !ReferenceEquals(sender, AssociatedObject)
                                         || AssociatedObject.Items.Count <= 0)
            {
                return;
            }

            switch (e.Key)
            {
                case Key.Down:
                    if (!AssociatedObject.Items.MoveCurrentToNext())
                    {
                        AssociatedObject.Items.MoveCurrentToLast();
                    }

                    break;

                case Key.Up:
                    if (!AssociatedObject.Items.MoveCurrentToPrevious())
                    {
                        AssociatedObject.Items.MoveCurrentToFirst();
                    }

                    break;

                default:
                    return;
            }

            AssociatedObject.SelectedItem = AssociatedObject.Items.CurrentItem;

            if (AssociatedObject.ItemContainerGenerator
                                .ContainerFromItem(AssociatedObject.Items.CurrentItem) is ListBoxItem item)
            {
                item.Focus();
            }

            e.Handled = true;
        }

        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (AssociatedObject == null || !ReferenceEquals(sender, AssociatedObject)
                                         || AssociatedObject.Items.Count <= 0)
            {
                return;
            }

            AssociatedObject.Dispatcher.BeginInvoke(
                (Action)(() =>
                         {
                             ////AssociatedObject.UpdateLayout();
                             if (AssociatedObject.SelectedItem == null)
                             {
                                 return;
                             }

                             AssociatedObject.Items.MoveCurrentTo(AssociatedObject.SelectedItem);

                             if (AssociatedObject.Items.CurrentPosition == 0)
                             {
                                 var group = GetCollectionViewGroup(AssociatedObject.SelectedItem);
                                 if (group != null)
                                 {
                                     AssociatedObject.ScrollIntoView(group);
                                 }
                             }
                             else
                             {
                                 AssociatedObject.ScrollIntoView(AssociatedObject.SelectedItem);
                             }

                             ExpandGroupOfItem(AssociatedObject.Items.CurrentItem);
                         }));
        }

        private void ExpandGroupOfItem(object item)
        {
            var groups = AssociatedObject.Items.Groups?.OfType<CollectionViewGroup>();
            if (groups == null)
            {
                return;
            }

            foreach (var collectionViewGroup in groups)
            {
                var groupItem = (GroupItem)AssociatedObject.ItemContainerGenerator
                                                           .ContainerFromItem(collectionViewGroup);
                var expander = groupItem.FindVisualChildren<Expander>().FirstOrDefault();
                if (expander != null)
                {
                    expander.IsExpanded = collectionViewGroup.Items.Contains(item);
                }
            }
        }

        private CollectionViewGroup GetCollectionViewGroup(object item)
        {
            return AssociatedObject.Items.Groups?.OfType<CollectionViewGroup>()
                                   .FirstOrDefault(g => g.Items.Contains(item));
        }
    }
}
