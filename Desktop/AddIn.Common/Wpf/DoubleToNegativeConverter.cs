using System;
using System.Globalization;
using System.Windows.Data;

namespace Eurolook.AddIn.Common.Wpf
{
    public class DoubleToNegativeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double)
            {
                return -1.0 * (double)value;
            }

            throw new ArgumentException();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double)
            {
                return -1.0 * (double)value;
            }

            throw new ArgumentException();
        }
    }
}
