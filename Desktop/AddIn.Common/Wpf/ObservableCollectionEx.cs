using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;

namespace Eurolook.AddIn.Common.Wpf
{
    /// <summary>
    /// Represents a dynamic data collection that provides notifications when items get added, removed, or when the
    /// whole list is refreshed.
    /// </summary>
    /// <typeparam name="T">The type of elements in the collection.</typeparam>
    public sealed class ObservableCollectionEx<T> : ObservableCollection<T>, IObservableCollectionEx
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ObservableCollectionEx{T}" /> class.
        /// </summary>
        public ObservableCollectionEx()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ObservableCollectionEx{T}" /> class that
        /// contains elements copied from the specified collection.
        /// </summary>
        /// <param name="collection">collection: The collection from which the elements are copied.</param>
        /// <exception cref="System.ArgumentNullException">The collection parameter cannot be null.</exception>
        public ObservableCollectionEx(IEnumerable<T> collection)
            : base(collection)
        {
        }

        public event NotifyCollectionChangedExEventHandler BeforeCollectionChanged;

        public event NotifyCollectionChangedExEventHandler AfterCollectionChanged;

        /// <summary>
        /// Adds the elements of the specified collection to the end of the ObservableCollection&lt;T&gt;.
        /// </summary>
        public void AddRange(IEnumerable collection)
        {
            if (collection == null)
            {
                return;
            }

            var newItems = collection.Cast<T>();
            AddRange(newItems);
        }

        /// <summary>
        /// Adds the elements of the specified collection to the end of the ObservableCollection&lt;T&gt;.
        /// </summary>
        public void AddRange(IEnumerable<T> collection)
        {
            if (collection == null)
            {
                return;
            }

            var oldItems = Items.ToList();
            var newItems = collection.ToList();
            OnBeforeCollectionChanged(new NotifyCollectionChangedExEventArgs(NotifyCollectionChangedAction.Add, newItems, oldItems));
            foreach (var i in newItems)
            {
                Items.Add(i);
            }

            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            OnAfterCollectionChanged(new NotifyCollectionChangedExEventArgs(NotifyCollectionChangedAction.Add, newItems, oldItems));
        }

        /// <summary>
        /// Removes the first occurence of each item in the specified collection from ObservableCollection&lt;T&gt;.
        /// </summary>
        public void RemoveRange(IEnumerable collection)
        {
            if (collection == null)
            {
                return;
            }

            var removeItems = collection.Cast<T>();
            RemoveRange(removeItems);
        }

        /// <summary>
        /// Removes the first occurence of each item in the specified collection from ObservableCollection&lt;T&gt;.
        /// </summary>
        public void RemoveRange(IEnumerable<T> collection)
        {
            if (collection == null)
            {
                return;
            }

            var oldItems = Items.ToList();
            var removeItems = collection.ToList();
            OnBeforeCollectionChanged(new NotifyCollectionChangedExEventArgs(NotifyCollectionChangedAction.Remove, removeItems, oldItems));
            foreach (var i in removeItems)
            {
                Items.Remove(i);
            }

            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            OnAfterCollectionChanged(new NotifyCollectionChangedExEventArgs(NotifyCollectionChangedAction.Remove, removeItems, oldItems));
        }

        /// <summary>
        /// Clears the current collection and replaces it with the specified item.
        /// </summary>
        public void Replace(object item)
        {
            ReplaceRange(new[] { (T)item });
        }

        /// <summary>
        /// Clears the current collection and replaces it with the specified item.
        /// </summary>
        public void Replace(T item)
        {
            ReplaceRange(new[] { item });
        }

        /// <summary>
        /// Clears the current collection and replaces it with the specified collection.
        /// </summary>
        public void ReplaceRange(IEnumerable collection)
        {
            var newItems = collection?.Cast<T>() ?? new List<T>();
            ReplaceRange(newItems);
        }

        /// <summary>
        /// Clears the current collection and replaces it with the specified collection.
        /// </summary>
        public void ReplaceRange(IEnumerable<T> collection)
        {
            var oldItems = Items.ToList();
            var newItems = collection?.ToList() ?? new List<T>();

            OnBeforeCollectionChanged(new NotifyCollectionChangedExEventArgs(NotifyCollectionChangedAction.Replace, newItems, oldItems));

            Items.Clear();
            foreach (var i in newItems)
            {
                Items.Add(i);
            }

            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            OnAfterCollectionChanged(new NotifyCollectionChangedExEventArgs(NotifyCollectionChangedAction.Replace, newItems, oldItems));
        }

        /// <summary>
        /// Removes some items and adds new ones.
        /// </summary>
        public void AddAndRemoveRange(IEnumerable<T> itemsToRemove, IEnumerable<T> itemsToAdd)
        {
            var oldItems = itemsToRemove.ToList();
            var newItems = itemsToAdd?.ToList() ?? new List<T>();
            OnBeforeCollectionChanged(new NotifyCollectionChangedExEventArgs(NotifyCollectionChangedAction.Replace, newItems, oldItems));

            foreach (var i in oldItems)
            {
                Items.Remove(i);
            }

            foreach (var i in newItems)
            {
                Items.Add(i);
            }

            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            OnAfterCollectionChanged(new NotifyCollectionChangedExEventArgs(NotifyCollectionChangedAction.Replace, newItems, oldItems));
        }

        /// <summary>
        /// Removes some items and inserts new ones.
        /// </summary>
        public void InsertAt(T item, int index)
        {
            var oldItems = Items.ToList();
            var newItems = new List<T> { item };
            OnBeforeCollectionChanged(new NotifyCollectionChangedExEventArgs(NotifyCollectionChangedAction.Add, newItems, oldItems));

            Items.Insert(index, item);

            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, newItems, index));
            OnAfterCollectionChanged(new NotifyCollectionChangedExEventArgs(NotifyCollectionChangedAction.Add, newItems, oldItems));
        }

        /// <summary>
        /// Removes some items and inserts new ones.
        /// </summary>
        public void InsertAndRemoveRange(IEnumerable<T> itemsToRemove, IEnumerable<T> itemsToInsert, int index)
        {
            var oldItems = itemsToRemove.ToList();
            var newItems = itemsToInsert?.ToList() ?? new List<T>();

            OnBeforeCollectionChanged(new NotifyCollectionChangedExEventArgs(NotifyCollectionChangedAction.Replace, newItems, oldItems));

            foreach (var i in oldItems)
            {
                Items.Remove(i);
            }

            foreach (var i in ((IEnumerable<T>)newItems).Reverse())
            {
                Items.Insert(index, i);
            }

            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            OnAfterCollectionChanged(new NotifyCollectionChangedExEventArgs(NotifyCollectionChangedAction.Replace, newItems, oldItems));
        }

        /// <summary>
        /// Removes some items and insert a new one.
        /// </summary>
        public void InsertAndRemoveRange(IEnumerable<T> itemsToRemove, T itemToInsert, int index)
        {
            InsertAndRemoveRange(itemsToRemove, new[] { itemToInsert }, index);
        }

        /// <summary>
        /// Replaces the item at the given index (without clearing the entire list).
        /// </summary>
        public void ReplaceItem(int index, T item)
        {
            SetItem(index, item);
        }

        public void Refresh()
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        private void OnBeforeCollectionChanged(NotifyCollectionChangedExEventArgs e)
        {
            if (BeforeCollectionChanged != null)
            {
                using (BlockReentrancy())
                {
                    BeforeCollectionChanged?.Invoke(this, e);
                }
            }
        }

        private void OnAfterCollectionChanged(NotifyCollectionChangedExEventArgs e)
        {
            if (AfterCollectionChanged != null)
            {
                using (BlockReentrancy())
                {
                    AfterCollectionChanged?.Invoke(this, e);
                }
            }
        }
    }

    public interface IObservableCollectionEx
    {
        void AddRange(IEnumerable collection);

        void RemoveRange(IEnumerable collection);

        void Replace(object item);

        void ReplaceRange(IEnumerable collection);

        void Refresh();
    }

    public class NotifyCollectionChangedExEventArgs : EventArgs
    {
        public NotifyCollectionChangedExEventArgs(NotifyCollectionChangedAction action, IList changedItems, IList oldItems)
        {
            Action = action;
            ChangedItems = changedItems;
            OldItems = oldItems;
        }

        public NotifyCollectionChangedAction Action { get; }

        /// <summary>
        /// The list of items that are added to the collection, removed from collection or newly inserted on a replace
        /// </summary>
        public IList OldItems { get; }

        /// <summary>
        /// The list of items that are in the collection before the change is applied
        /// </summary>
        public IList ChangedItems { get; }
    }

    public delegate void NotifyCollectionChangedExEventHandler(object sender, NotifyCollectionChangedExEventArgs e);
}
