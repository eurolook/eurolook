using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace Eurolook.AddIn.Common.Wpf
{
    public class EnumToDescriptionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Enum e)
            {
                return GetDisplayName(e);
            }

            return null;
        }

        public static string GetDisplayName(Enum e)
        {
            var type = e.GetType();
            var value = Enum.GetName(type, e) ?? "";
            var member = type.GetMember(value).FirstOrDefault();

            var attribute = member?.GetCustomAttributes(typeof(DisplayAttribute), false)?.FirstOrDefault() as DisplayAttribute;
            var result = "";

            if (attribute == null)
            {
                return result;
            }

            result = attribute.Name;

            if (attribute.ResourceType != null)
            {
                result = attribute.GetName();
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
