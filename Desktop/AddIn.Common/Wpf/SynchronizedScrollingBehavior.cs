﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace Eurolook.AddIn.Common.Wpf
{
    public class SynchronizedScrollingBehavior : Behavior<ScrollViewer>
    {
        public static readonly DependencyProperty SourceProperty = DependencyProperty.Register(
            nameof(Source),
            typeof(ScrollViewer),
            typeof(SynchronizedScrollingBehavior),
            new PropertyMetadata(default(ScrollViewer), OnSourceObjectChanged));

        public static readonly DependencyProperty DirectionProperty = DependencyProperty.Register(
            nameof(Direction),
            typeof(ScrollingDirection),
            typeof(SynchronizedScrollingBehavior),
            new PropertyMetadata(default(ScrollingDirection)));

        private ScrollViewer _scrollViewer;

        public enum ScrollingDirection
        {
            Horizontal,
            Vertical,
            Both
        }

        public ScrollViewer Source
        {
            get => (ScrollViewer)GetValue(SourceProperty);
            set => SetValue(SourceProperty, value);
        }

        public ScrollingDirection Direction
        {
            get => (ScrollingDirection)GetValue(DirectionProperty);
            set => SetValue(DirectionProperty, value);
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            if (_scrollViewer != null)
            {
                _scrollViewer.ScrollChanged += OnScrollChanged;
            }
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            if (_scrollViewer != null)
            {
                _scrollViewer.ScrollChanged -= OnScrollChanged;
            }
        }

        private static void OnSourceObjectChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is SynchronizedScrollingBehavior behavior)
            {
                if ((e.OldValue != null) && (behavior._scrollViewer == e.OldValue))
                {
                    behavior._scrollViewer.ScrollChanged -= behavior.OnScrollChanged;
                }

                behavior._scrollViewer = null;

                if (e.NewValue is ScrollViewer newScrollViewer)
                {
                    behavior._scrollViewer = newScrollViewer;
                    if (behavior.AssociatedObject != null)
                    {
                        behavior._scrollViewer.ScrollChanged += behavior.OnScrollChanged;
                    }
                }
            }
        }

        private void OnScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            switch (Direction)
            {
                case ScrollingDirection.Horizontal:
                    AssociatedObject.ScrollToHorizontalOffset(e.HorizontalOffset);
                    break;
                case ScrollingDirection.Vertical:
                    AssociatedObject.ScrollToVerticalOffset(e.VerticalOffset);
                    break;
                case ScrollingDirection.Both:
                    AssociatedObject.ScrollToHorizontalOffset(e.HorizontalOffset);
                    AssociatedObject.ScrollToVerticalOffset(e.VerticalOffset);
                    break;
            }
        }
    }
}
