﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using SharpVectors.Converters;
using SharpVectors.Renderers.Wpf;

namespace Eurolook.AddIn.Common.Wpf
{
    public class HeroIcon : Border
    {
        private const double DefaultSize = 24;

        public string DllName { get; }

        private static WpfDrawingSettings _settings = new WpfDrawingSettings
        {
            IncludeRuntime = true,
            TextAsGeometry = false,
            OptimizePath = true,
        };

        public DrawingImage Icon
        {
            get { return (DrawingImage)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }

        public static readonly DependencyProperty IconProperty = DependencyProperty.Register(
            nameof(Icon),
            typeof(DrawingImage),
            typeof(HeroIcon),
            new PropertyMetadata(null, OnIconChanged));

        public string IconPath
        {
            get { return (string)GetValue(IconPathProperty); }
            set { SetValue(IconPathProperty, value); }
        }

        public static readonly DependencyProperty IconPathProperty = DependencyProperty.Register(
            nameof(IconPath),
            typeof(string),
            typeof(HeroIcon),
            new PropertyMetadata(null, OnIconPathChanged));

        public double Size
        {
            get { return (double)GetValue(SizeProperty); }
            set { SetValue(SizeProperty, value); }
        }

        public static readonly DependencyProperty SizeProperty = DependencyProperty.Register(
            nameof(Size),
            typeof(double),
            typeof(HeroIcon),
            new PropertyMetadata(DefaultSize, OnSizeChanged));

        public HeroIcon()
        {
            DllName = Assembly.GetAssembly(typeof(HeroIcon)).GetName().Name;
        }

        private static void OnSizeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is HeroIcon icon)
            {
                DrawingImage svgDrawing = null;
                if (icon.IconPath != null)
                {
                    svgDrawing = icon.CreateImageFromIconPath();
                    if (svgDrawing != null)
                    {
                        icon.SetContent(svgDrawing);
                    }
                }
                else if (icon.Icon != null)
                {
                    icon.SetContent(icon.Icon);
                }
            }
        }

        private static void OnIconPathChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is HeroIcon icon && e.NewValue != null)
            {
                var svgDrawing = icon.CreateImageFromIconPath();
                if (svgDrawing != null)
                {
                    icon.SetContent(svgDrawing);
                }
            }
        }

        private static void OnIconChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is HeroIcon icon && e.NewValue != null)
            {
                var svgDrawing = e.NewValue as DrawingImage;
                if (svgDrawing != null)
                {
                    icon.SetContent(svgDrawing);
                }
            }
        }

        public static bool IsSvgFile(byte[] bytes)
        {
            if (bytes == null || bytes.Length < 4)
            {
                return false;
            }

            // 3C 73 76 67 = "<svg"
            return BitConverter.ToString(bytes.Take(4).ToArray()) == "3C-73-76-67";
        }

        public static DrawingImage CreateImageFromBytes(byte[] bytes)
        {
            DrawingGroup drawingGroup = null;
            using (var stream = new MemoryStream(bytes))
            {
                using (var reader = new FileSvgReader(_settings))
                {
                    drawingGroup = reader.Read(stream);
                }
            }

            var svgDrawing = new DrawingImage(drawingGroup);
            svgDrawing.Freeze();
            return svgDrawing;
        }

        private DrawingImage CreateImageFromIconPath()
        {
            var resourceUri = new Uri($"pack://application:,,,/{DllName};component{IconPath}");
            DrawingGroup drawingGroup = null;
            var svgStreamInfo = Application.GetResourceStream(resourceUri);
            var svgStream = (svgStreamInfo != null) ? svgStreamInfo.Stream : null;
            if (svgStream != null)
            {
                using (svgStream)
                {
                    using (FileSvgReader reader = new FileSvgReader(_settings))
                    {
                        drawingGroup = reader.Read(svgStream);
                    }
                }

                if (drawingGroup != null)
                {
                    var svgDrawing = new DrawingImage(drawingGroup);
                    svgDrawing.Freeze();
                    return svgDrawing;
                }
            }

            return null;
        }

        private void SetContent(DrawingImage svgDrawing)
        {
            if (double.IsNaN(Size) || Size == 0)
            {
                Size = DefaultSize;
            }

            Height = Size;
            Width = Size;
            Background = new SolidColorBrush(Colors.Transparent);

            var scalingFactor = Size / DefaultSize;

            var image = new Image();
            image.Height = svgDrawing.Height * scalingFactor;
            image.Width = svgDrawing.Width * scalingFactor;
            image.Source = svgDrawing;

            Child = image;
        }
    }
}
