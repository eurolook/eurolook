using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace Eurolook.AddIn.Common.Wpf
{
    /// <summary>
    /// Watches a task and raises property-changed notifications when the task completes.
    /// </summary>
    /// <typeparam name="TResult">The type of the task result.</typeparam>
    /// <remarks>Based on the Mvvm.Async lib by Stephen Cleary, see https://github.com/StephenCleary/Mvvm.Async. </remarks>
    public sealed class NotifyTask<TResult> : ObservableObject
    {
        private readonly TResult _defaultResult;

        /// <summary>
        /// Initializes a new instance of the <see cref="NotifyTask{TResult}" /> class.
        /// </summary>
        /// <param name="defaultResult">The value to return from <see cref="Result" /> while the task is not yet complete.</param>
        public NotifyTask(TResult defaultResult = default)
        {
            _defaultResult = defaultResult;
        }

        public Task TaskCompleted { get; private set; }

        public string ErrorMessage => InnerException?.Message;

        public AggregateException Exception => Task.Exception;

        public Exception InnerException => Exception?.InnerException;

        public bool IsCanceled => Task.IsCanceled;

        public bool IsCompleted => Task.IsCompleted;

        public bool IsFaulted => Task.IsFaulted;

        public bool IsNotCompleted => !Task.IsCompleted;

        public bool IsSuccessfullyCompleted => Task.Status == TaskStatus.RanToCompletion;

        public List<Action> OwnerNotifications { get; } = new List<Action>();

        public TResult Result => Task.Status == TaskStatus.RanToCompletion ? Task.Result : _defaultResult;

        public TaskStatus Status => Task.Status;

        public Task<TResult> Task { get; private set; }

        public NotifyTask<TResult> NotifyOnCompletion<TExpression>(
            ObservableObject owner,
            Expression<Func<TExpression>> propertyExpression)
        {
            OwnerNotifications.Add(() => owner.RaisePropertyChanged(propertyExpression));
            return this;
        }

        public NotifyTask<TResult> NotifyOnCompletion(ObservableObject owner)
        {
            // ReSharper disable once ExplicitCallerInfoArgument
            OwnerNotifications.Add(() => owner.RaisePropertyChanged(""));
            return this;
        }

        public NotifyTask<TResult> Start(Func<Task<TResult>> taskFunc)
        {
            if (Task == null)
            {
                TaskCompleted = WatchTaskAsync(taskFunc);
            }

            return this;
        }

        private void OnTaskCompletion()
        {
            RaisePropertyChanged(() => Status);
            RaisePropertyChanged(() => IsCompleted);
            RaisePropertyChanged(() => IsNotCompleted);

            if (Task == null)
            {
                RaisePropertyChanged();
            }
            else if (Task.IsCanceled)
            {
                RaisePropertyChanged(() => IsCanceled);
            }
            else if (Task.IsFaulted)
            {
                RaisePropertyChanged(() => IsFaulted);
                RaisePropertyChanged(() => Exception);
                RaisePropertyChanged(() => InnerException);
                RaisePropertyChanged(() => ErrorMessage);
            }
            else
            {
                RaisePropertyChanged(() => IsSuccessfullyCompleted);
                RaisePropertyChanged(() => Result);
            }

            foreach (var notification in OwnerNotifications)
            {
                notification();
            }
        }

        private async Task WatchTaskAsync(Func<Task<TResult>> taskFunc)
        {
            try
            {
                Task = taskFunc();
                await Task;
            }
            catch (Exception ex)
            {
                // exceptions are kept in Task.Exception
                if (Task == null)
                {
                    Task = System.Threading.Tasks.Task.FromException<TResult>(ex);
                }
            }

            OnTaskCompletion();
        }
    }
}
