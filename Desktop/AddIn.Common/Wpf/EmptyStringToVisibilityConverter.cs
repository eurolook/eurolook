using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Eurolook.AddIn.Common.Wpf
{
    /// <summary>
    /// Converts empty, null or whitespace strings to Visibility properties.
    /// </summary>
    public class EmptyStringToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool flag = value != null && !string.IsNullOrWhiteSpace(value.ToString());

            // a parameter value of True inverts conversion
            if (parameter != null && System.Convert.ToBoolean(parameter))
            {
                flag = !flag;
            }

            return flag ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
