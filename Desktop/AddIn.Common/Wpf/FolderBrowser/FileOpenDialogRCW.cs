using System.Runtime.InteropServices;

namespace Eurolook.AddIn.Common.Wpf.FolderBrowser
{
    [ComImport]
    [ClassInterface(ClassInterfaceType.None)]
    [TypeLibType(TypeLibTypeFlags.FCanCreate)]
    [Guid("dc1c5a9c-e88a-4dde-a5a1-60f82a20aef7")]
    internal class FileOpenDialogRCW
    {
    }
}
