using System.Runtime.InteropServices;

namespace Eurolook.AddIn.Common.Wpf.FolderBrowser
{
    [ComImport]
    [Guid(IIDGuid.IFileOpenDialog)]
    [CoClass(typeof(FileOpenDialogRCW))]
    internal interface NativeFileOpenDialog : IFileOpenDialog
    {
    }
}
