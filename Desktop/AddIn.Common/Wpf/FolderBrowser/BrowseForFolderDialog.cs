using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Eurolook.AddIn.Common.Wpf.FolderBrowser
{
    public class BrowseForFolderDialog : CommonDialog
    {
        public BrowseForFolderDialog()
        {
            Title = "";
            SelectedPath = null;
        }

        public string Title { get; set; }

        public string SelectedPath { get; set; }

        public override void Reset()
        {
            Title = "";
            SelectedPath = null;
        }

        protected override bool RunDialog(IntPtr hwndOwner)
        {
            IFileDialog dialog = null;
            try
            {
                dialog = new NativeFileOpenDialog();
                SetDialogProperties(dialog);
                int result = dialog.Show(hwndOwner);
                if (result < 0)
                {
                    if ((uint)result == (uint)NativeMethods.HRESULT.ERROR_CANCELLED)
                    {
                        return false;
                    }
                    else
                    {
                        throw Marshal.GetExceptionForHR(result);
                    }
                }

                GetResult(dialog);
                return true;
            }
            finally
            {
                if (dialog != null)
                {
                    Marshal.FinalReleaseComObject(dialog);
                }
            }
        }

        private void SetDialogProperties(IFileDialog dialog)
        {
            dialog.SetTitle(Title);

            dialog.SetOptions(
                NativeMethods.FOS.PICKFOLDERS | NativeMethods.FOS.FORCEFILESYSTEM
                                              | NativeMethods.FOS.FILEMUSTEXIST);

            if (!string.IsNullOrEmpty(SelectedPath))
            {
                string parent = Path.GetDirectoryName(SelectedPath);
                if (parent == null || !Directory.Exists(parent))
                {
                    dialog.SetFileName(SelectedPath);
                }
                else
                {
                    string folder = Path.GetFileName(SelectedPath);
                    dialog.SetFolder(CreateItemFromParsingName(parent));
                    dialog.SetFileName(folder);
                }
            }
        }

        private static IShellItem CreateItemFromParsingName(string path)
        {
            var guid = new Guid(IIDGuid.IShellItem); // IID_IShellItem
            int hr = NativeMethods.SHCreateItemFromParsingName(path, IntPtr.Zero, ref guid, out var item);
            if (hr != 0)
            {
                throw new Win32Exception(hr);
            }

            return (IShellItem)item;
        }

        private void GetResult(IFileDialog dialog)
        {
            dialog.GetResult(out var item);
            item.GetDisplayName(NativeMethods.SIGDN.FILESYSPATH, out string selectedPath);
            SelectedPath = selectedPath;
        }
    }
}
