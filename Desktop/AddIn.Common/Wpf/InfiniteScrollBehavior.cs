﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using Eurolook.AddIn.Common.Extensions;

namespace Eurolook.AddIn.Common.Wpf
{
    public class InfiniteScrollBehavior : Behavior<ListBox>
    {
        public static readonly DependencyProperty LoadCommandProperty = DependencyProperty.Register(
            "LoadCommand",
            typeof(ICommand),
            typeof(InfiniteScrollBehavior),
            new PropertyMetadata(default(ICommand)));

        public static readonly DependencyProperty ScrollViewerNameProperty = DependencyProperty.Register(
            "ScrollViewerName",
            typeof(string),
            typeof(InfiniteScrollBehavior),
            new PropertyMetadata(default(string)));

        private ScrollViewer _scrollViewer;

        public ICommand LoadCommand
        {
            get => (ICommand)GetValue(LoadCommandProperty);
            set => SetValue(LoadCommandProperty, value);
        }

        public string ScrollViewerName
        {
            get => (string)GetValue(ScrollViewerNameProperty);
            set => SetValue(ScrollViewerNameProperty, value);
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.LayoutUpdated += AssociatedObjectOnLayoutUpdated;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.LayoutUpdated -= AssociatedObjectOnLayoutUpdated;
            if (_scrollViewer != null)
            {
                _scrollViewer.ScrollChanged -= OnScrollChanged;
            }
        }

        private void AssociatedObjectOnLayoutUpdated(object sender, EventArgs eventArgs)
        {
            var scrollViewer = string.IsNullOrEmpty(ScrollViewerName)
                ? AssociatedObject.FindVisualChildren<ScrollViewer>().FirstOrDefault()
                : AssociatedObject.FindVisualChildren<ScrollViewer>()
                                  .FirstOrDefault(v => v.Name.Equals(ScrollViewerName));

            if (!ReferenceEquals(_scrollViewer, scrollViewer))
            {
                if (_scrollViewer != null)
                {
                    _scrollViewer.ScrollChanged -= OnScrollChanged;
                }

                _scrollViewer = scrollViewer;

                if (_scrollViewer != null)
                {
                    _scrollViewer.ScrollChanged += OnScrollChanged;
                }
            }
        }

        private void OnScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var scrollViewer = (ScrollViewer)sender;
            if (Math.Abs(scrollViewer.VerticalOffset - scrollViewer.ScrollableHeight) < 0.001)
            {
                if ((LoadCommand != null) && LoadCommand.CanExecute(null))
                {
                    LoadCommand.Execute(null);
                }
            }
        }
    }
}
