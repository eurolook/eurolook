﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace Eurolook.AddIn.Common.Wpf
{
    public class RestoreFocusBehavior : Behavior<Button>
    {
        private bool _hadFocus = false;
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.Click += OnButtonClick;
            AssociatedObject.IsEnabledChanged += OnIsEnabledChanged;
        }

        private void OnIsEnabledChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue is true)
            {
                if (_hadFocus)
                {
                    AssociatedObject.Focus();
                }
                _hadFocus = false;
            }
        }

        private void OnButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            _hadFocus = AssociatedObject.IsKeyboardFocusWithin;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.Click -= OnButtonClick;
            AssociatedObject.IsEnabledChanged -= OnIsEnabledChanged;
        }
    }
}
