using System;
using System.Threading;
using System.Windows.Input;
using GalaSoft.MvvmLight.Helpers;

namespace Eurolook.AddIn.Common.Wpf
{
    public abstract class RelayCommandBase : ICommand
    {
        private WeakAction _execute;
        private WeakFunc<bool> _canExecute;
        private EventHandler _reQuerySuggestedLocal;

        protected void SetExecuteAction(Action action, bool keepTargetAlive = false)
        {
            _execute = action != null ? new WeakAction(action, keepTargetAlive) : throw new ArgumentNullException();
        }

        protected void SetCanExecuteAction(Func<bool> action, bool keepTargetAlive = false)
        {
            _canExecute = new WeakFunc<bool>(action, keepTargetAlive);
        }

        /// <summary>
        /// Defines the method that determines whether the command can execute in its current state.
        /// </summary>
        /// <param name="parameter">This parameter will always be ignored.</param>
        /// <returns>true if this command can be executed; otherwise, false.</returns>
        public bool CanExecute(object parameter)
        {
            if (this._canExecute == null)
            {
                return true;
            }

            return (this._canExecute.IsStatic || this._canExecute.IsAlive) && this._canExecute.Execute();
        }

        /// <summary>
        /// Defines the method to be called when the command is invoked.
        /// </summary>
        /// <param name="parameter">This parameter will always be ignored.</param>
        public virtual void Execute(object parameter)
        {
            if (!this.CanExecute(parameter) || this._execute == null ||
                (!this._execute.IsStatic && !this._execute.IsAlive))
            {
                return;
            }

            this._execute.Execute();
        }

        /// <summary>
        /// Occurs when changes occur that affect whether the command should execute.
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add
            {
                if (this._canExecute == null)
                {
                    return;
                }

                var eventHandler = this._reQuerySuggestedLocal;
                EventHandler comparator;
                do
                {
                    comparator = eventHandler;
                    eventHandler = Interlocked.CompareExchange(ref this._reQuerySuggestedLocal, comparator + value, comparator);
                }
                while (eventHandler != comparator);
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                if (this._canExecute == null)
                {
                    return;
                }

                var eventHandler = this._reQuerySuggestedLocal;
                EventHandler comparator;
                do
                {
                    comparator = eventHandler;
                    // ReSharper disable once DelegateSubtraction
                    eventHandler = Interlocked.CompareExchange(ref this._reQuerySuggestedLocal, comparator - value, comparator);
                }
                while (eventHandler != comparator);
                CommandManager.RequerySuggested -= value;
            }
        }

        /// <summary>
        /// Raises the <see cref="E:GalaSoft.MvvmLight.CommandWpf.RelayCommand.CanExecuteChanged" /> event.
        /// </summary>
        public void RaiseCanExecuteChanged() => CommandManager.InvalidateRequerySuggested();
    }
}
