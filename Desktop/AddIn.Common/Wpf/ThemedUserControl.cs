using System.Windows.Controls;
using Eurolook.AddIn.Common.Messages;
using Eurolook.Common.Log;
using GalaSoft.MvvmLight.Messaging;

namespace Eurolook.AddIn.Common.Wpf
{
    public class ThemedUserControl : UserControl, ICanLog
    {
        // ReSharper disable once MemberCanBeProtected.Global
        // Public default constructor is required when type is to be used within a ResourceDictionary
        public ThemedUserControl()
        {
            Messenger.Default.Register<ColorSchemeUpdatedMessage>(this, OnColorSchemeUpdated);
        }

        private void OnColorSchemeUpdated(ColorSchemeUpdatedMessage message)
        {
            Execute.OnUiThread(UpdateDefaultStyle);
        }
    }
}
