using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Eurolook.AddIn.Common.Wpf
{
    public class HexColorToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string valueAsString = value?.ToString();
            if (valueAsString?.StartsWith("#") == true)
            {
                var color = ColorConverter.ConvertFromString(valueAsString);
                if (color != null)
                {
                    return new SolidColorBrush((Color)color);
                }
            }

            return Colors.Red;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
