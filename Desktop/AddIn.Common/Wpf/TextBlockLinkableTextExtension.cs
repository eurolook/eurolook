﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Navigation;
using Eurolook.AddIn.Common.ViewModels;

namespace Eurolook.AddIn.Common.Wpf
{
    public static class TextBlockLinkableTextExtension
    {
        private static readonly string TextGroupName = "text";
        private static readonly string LinkGroupName = "link";

        /*
         * E.g.: [European Comission|https://ec.europa.eu]
         */
        private static readonly Regex EscapeLinksFromText = new Regex(@"(\[.*\])");
        private static readonly Regex SplitLinksToContent = new Regex(@"^\[(?<text>.*)\|(?<link>.*)\]$");

        public static readonly DependencyProperty FormattedTextProperty = DependencyProperty.RegisterAttached(
            "Message",
            typeof(string),
            typeof(TextBlockLinkableTextExtension),
            new FrameworkPropertyMetadata(string.Empty, FrameworkPropertyMetadataOptions.AffectsMeasure, MessagePropertyChanged));

        public static void SetMessage(DependencyObject textBlock, string value)
        {
            textBlock.SetValue(FormattedTextProperty, value);
        }

        public static string GetMessage(DependencyObject textBlock)
        {
            return (string)textBlock.GetValue(FormattedTextProperty);
        }

        private static void MessagePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var textBlock = d as TextBlock;
            if (textBlock == null)
            {
                return;
            }

            textBlock.Inlines.Clear();

            var text = (string)e.NewValue;
            if (text == null)
            {
                return;
            }

            var runs = ParseTextRunsFromMessage(text);
            foreach (var run in runs)
            {
                if (run.IsLinked)
                {
                    bool isValidUri = Uri.TryCreate(run.Hyperlink, UriKind.Absolute, out Uri link);
                    var hyperLink = new Hyperlink()
                    {
                        IsEnabled = isValidUri,
                        NavigateUri = link,
                        ToolTip = run.Hyperlink,
                    };
                    hyperLink.Inlines.Add(run.Text);
                    hyperLink.RequestNavigate += OnHyperLinkRequestNavigate;
                    textBlock.Inlines.Add(hyperLink);
                }
                else
                {
                    textBlock.Inlines.Add(new Run()
                    {
                        Text = run.Text,
                    });
                }
            }
        }

        private static void OnHyperLinkRequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(e.Uri.AbsoluteUri);
            e.Handled = true;
        }

        private static IEnumerable<LinkableText> ParseTextRunsFromMessage(string message)
        {
            var results = new List<LinkableText>();

            var runs = EscapeLinksFromText.Split(message);
            foreach (var run in runs)
            {
                if (SplitLinksToContent.IsMatch(run))
                {
                    var split = SplitLinksToContent.Match(run);
                    var name = split.Groups[TextGroupName].Value;
                    var link = split.Groups[LinkGroupName].Value;
                    results.Add(new LinkableText(name, link));
                }
                else
                {
                    results.Add(new LinkableText(run, null));
                }
            }

            return results;
        }
    }
}
