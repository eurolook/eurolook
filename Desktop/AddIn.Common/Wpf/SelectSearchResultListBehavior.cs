using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace Eurolook.AddIn.Common.Wpf
{
    public class SelectSearchResultListBehavior : Behavior<TextBox>
    {
        public static readonly DependencyProperty SearchResultListBoxProperty = DependencyProperty.Register(
            "SearchResultListBox",
            typeof(ListBox),
            typeof(SelectSearchResultListBehavior),
            new PropertyMetadata(default(ListBox)));

        public ListBox SearchResultListBox
        {
            get { return (ListBox)GetValue(SearchResultListBoxProperty); }
            set { SetValue(SearchResultListBoxProperty, value); }
        }

        protected override void OnAttached()
        {
            AssociatedObject.PreviewKeyDown += AssociatedObjectOnPreviewKeyDown;
            base.OnAttached();
        }

        protected override void OnDetaching()
        {
            AssociatedObject.PreviewKeyDown -= AssociatedObjectOnPreviewKeyDown;
            base.OnDetaching();
        }

        private void AssociatedObjectOnPreviewKeyDown(object sender, KeyEventArgs keyEventArgs)
        {
            if (SearchResultListBox == null)
            {
                return;
            }

            if (keyEventArgs.Key == Key.Down)
            {
                int selectedIndex = SearchResultListBox.SelectedIndex + 1;
                SelectIndex(selectedIndex);
            }
            else if (keyEventArgs.Key == Key.Up)
            {
                int selectedIndex = SearchResultListBox.SelectedIndex - 1;
                SelectIndex(selectedIndex);
            }
        }

        private void SelectIndex(int selectedIndex)
        {
            if (selectedIndex >= 0 && selectedIndex < SearchResultListBox.Items.Count)
            {
                SearchResultListBox.SelectedIndex = selectedIndex;
                SearchResultListBox.ScrollIntoView(SearchResultListBox.Items[selectedIndex]);
            }
        }
    }
}
