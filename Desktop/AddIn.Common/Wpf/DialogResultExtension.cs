using System.Windows;
using System.Windows.Interop;

namespace Eurolook.AddIn.Common.Wpf
{
    public static class DialogResultExtension
    {
        public static readonly DependencyProperty DialogResultProperty = DependencyProperty.RegisterAttached(
            "DialogResult",
            typeof(bool?),
            typeof(DialogResultExtension),
            new PropertyMetadata(DialogResultChanged));

        public static void SetDialogResult(Window target, bool? value)
        {
            target.SetValue(DialogResultProperty, value);
        }

        private static void DialogResultChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is Window window)
            {
                Execute.OnUiThread(
                    () =>
                    {
                        // ComponentDispatcher.IsThreadModal can determine, whether the calling thread
                        // is displaying a modal window
                        if (ComponentDispatcher.IsThreadModal)
                        {
                            // DialogResult can only be set on modal dialogs
                            window.DialogResult = e.NewValue as bool?;
                        }
                        else
                        {
                            // simply close non-modal dialogs
                            window.Close();
                        }
                    });
            }
        }
    }
}
