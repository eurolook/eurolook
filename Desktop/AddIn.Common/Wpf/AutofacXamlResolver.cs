﻿using System;
using System.Reflection;
using System.Windows;
using System.Windows.Markup;
using Autofac;

namespace Eurolook.AddIn.Common.Wpf;

/// <summary>
/// A class to inject dependencies into static XAML resources.
/// </summary>
/// <remarks>
/// The implementation is based on this suggestion (https://stackoverflow.com/a/41611854/40347)
/// but adjusted to work with Autofac.
/// </remarks>
public class AutofacXamlResolver : MarkupExtension
{
    public static IContainer Container { get; set; }

    public override object ProvideValue(IServiceProvider serviceProvider)
    {
        var provideValueTarget = (IProvideValueTarget)serviceProvider.GetService(typeof(IProvideValueTarget));

        var targetProperty = provideValueTarget.TargetProperty as PropertyInfo;
        if (targetProperty == null)
        {
            throw new InvalidOperationException("Target property could not be resolved.");
        }

        return Container != null ? Container.Resolve(targetProperty.PropertyType) : DependencyProperty.UnsetValue;
    }
}
