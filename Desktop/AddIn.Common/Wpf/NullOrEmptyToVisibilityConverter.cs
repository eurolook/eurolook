using System;
using System.Collections;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Eurolook.AddIn.Common.Wpf
{
    /// <summary>
    /// Converts objects that are null or collections that are empty to Visibility.Collapsed.
    /// </summary>
    public class NullOrEmptyToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool flag;

            switch (value)
            {
                case string s:
                {
                    flag = !string.IsNullOrEmpty(s);
                    break;
                }

                case ICollection c:
                {
                    flag = c.Count > 0;
                    break;
                }

                case IEnumerable e:
                {
                    var enumerator = e.GetEnumerator();
                    flag = enumerator.MoveNext();

                    if (enumerator is IDisposable disp)
                    {
                        disp.Dispose();
                    }

                    break;
                }

                default:
                {
                    flag = !(value is null);
                    break;
                }
            }

            // a parameter value of True inverts conversion
            if (parameter != null && System.Convert.ToBoolean(parameter))
            {
                flag = !flag;
            }

            return flag ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
