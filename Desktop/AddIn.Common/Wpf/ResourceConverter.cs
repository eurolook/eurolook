﻿using System;
using System.Globalization;
using System.Windows.Data;
using Eurolook.AddIn.Common.Properties;

namespace Eurolook.AddIn.Common.Wpf
{
    public class ResourceConverter : IValueConverter
    {
        public IResourceManager ResourceManager { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string resourceKey = parameter as string;
            return resourceKey == null
                ? null
                : ResourceManager?.GetString(resourceKey);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
