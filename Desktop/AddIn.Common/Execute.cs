using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Eurolook.AddIn.Common
{
    /// <summary>
    /// A static helper class for easily dispatching code to the UI thread.
    /// </summary>
    /// <remarks>
    /// The class must be initialized once by calling <see cref="Execute.InitializeWithDispatcher()" />.
    /// </remarks>
    /// <code>
    /// Execute.OnUIThread(() =>
    ///     {
    ///         // code to be executed on UI thread
    ///     });
    /// </code>
    public static class Execute
    {
        private static Action<Action> _executor = action => action();

        /// <summary>
        /// Initializes the framework using the current dispatcher.
        /// </summary>
        public static void InitializeWithDispatcher()
        {
            var dispatcher = Dispatcher.CurrentDispatcher;

            SetUiThreadMarshaller(
                action =>
                {
                    try
                    {
                        if (dispatcher.HasShutdownStarted)
                        {
                            return;
                        }

                        if (dispatcher.CheckAccess())
                        {
                            action();
                        }
                        else
                        {
                            dispatcher.Invoke(action);
                        }
                    }
                    catch (TaskCanceledException)
                    {
                        // this exception is triggered when shutting down the add-in and can be ignored
                    }
                });
        }

        /// <summary>
        /// Resets the executor to use a non-dispatcher-based action executor.
        /// </summary>
        public static void ResetWithoutDispatcher()
        {
            SetUiThreadMarshaller(action => action());
        }

        /// <summary>
        /// Sets a custom UI thread marshaller.
        /// </summary>
        /// <param name="marshaller">The marshaller.</param>
        public static void SetUiThreadMarshaller(Action<Action> marshaller)
        {
            _executor = marshaller;
        }

        /// <summary>
        /// Executes the action on the UI thread.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        public static void OnUiThread(this Action action)
        {
            _executor(action);
        }

        /// <summary>
        /// Executes the action on the UI thread.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        /// <param name="delay">A delay in ms.</param>
        public static void OnUiThread(this Action action, int delay)
        {
            var context = new DelayContext { Action = action };
            var timer = new Timer(OnUiThreadDelayed, context, delay, 0);
            context.Timer = timer;
        }

        private static void OnUiThreadDelayed(object state)
        {
            var context = state as DelayContext;
            if (context != null)
            {
                _executor(context.Action);
                context.Timer.Dispose();
            }
        }
    }

    internal class DelayContext
    {
        public Timer Timer { get; set; }

        public Action Action { get; set; }
    }
}
