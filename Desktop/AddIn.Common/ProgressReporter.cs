using System.Threading.Tasks;

namespace Eurolook.AddIn.Common
{
    public class ProgressReporter<T> : IProgressReporter<T>
    {
        public static readonly IProgressReporter<T> Empty = new ProgressReporter<T>();

        public Task ReportProgressAsync(T value)
        {
            return Task.CompletedTask;
        }

        public Task ReportProgressAsync(T value, string message)
        {
            return Task.CompletedTask;
        }
    }
}
