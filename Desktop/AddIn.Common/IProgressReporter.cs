using System.Threading.Tasks;

namespace Eurolook.AddIn.Common
{
    public interface IProgressReporter<in T>
    {
        Task ReportProgressAsync(T value);

        Task ReportProgressAsync(T value, string message);
    }
}
