﻿using System.Threading.Tasks;

namespace Eurolook.AddIn.Common
{
    public interface IDocumentProcessingHandler
    {
        /// <summary>
        /// Gets a value influencing in what order several handlers will be executed. Lower values are executed first.
        /// </summary>
        float ExecutionOrder { get; }

        Task HandleAsync(IEurolookDocument eurolookDocument);
    }

    public interface IChangeAuthorHandler : IDocumentProcessingHandler
    {
    }

    /// <summary>
    /// An interface for document processing handlers which gets called before a document is saved.
    /// </summary>
    public interface IDocumentBeforeSaveHandler
    {
        /// <summary>
        /// The actual handler method that is invoked.
        /// </summary>
        /// <param name="eurolookDocument">The document that is about to be saved.</param>
        /// <param name="saveasui">Indicates whether Save As dialog is displayed.</param>
        /// <returns>False will cancel saving.</returns>
        Task<bool> OnBeforeSaveAsync(IEurolookDocument eurolookDocument, bool saveasui);
    }

    public interface IDocumentOnOpenHandler : IDocumentProcessingHandler
    {
    }

    public interface IDocumentCreationHandler : IDocumentProcessingHandler
    {
    }
}
