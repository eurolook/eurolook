using System;
using System.Drawing;

namespace Eurolook.AddIn.Common.Events
{
    public class CaretPositionChangedEventArgs : EventArgs
    {
        public CaretPositionChangedEventArgs(Point oldCaretPosition, Point newCaretPosition)
        {
            OldCaretPosition = oldCaretPosition;
            NewCaretPosition = newCaretPosition;
        }

        public Point OldCaretPosition { get; set; }

        public Point NewCaretPosition { get; set; }
    }
}
