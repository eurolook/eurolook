using System;
using System.Windows.Input;

namespace Eurolook.AddIn.Common.Events
{
    public class KeyEventArgs : EventArgs
    {
        private readonly ulong _wParam;
        private readonly long _lParam;

        public KeyEventArgs(UIntPtr wParam, IntPtr lParam)
        {
            _wParam = wParam.ToUInt64();
            _lParam = lParam.ToInt64();
        }

        [Flags]
        private enum Keys
        {
            /// <summary>The left mouse button.</summary>
            LButton = 0x00000001,

            /// <summary>The right mouse button.</summary>
            RButton = 0x00000002,

            /// <summary>The SHIFT key.</summary>
            ShiftKey = 0x00000010,

            /// <summary>The CTRL key.</summary>
            ControlKey = ShiftKey | LButton, // 0x00000011

            /// <summary>The ALT key.</summary>
            Menu = ShiftKey | RButton, // 0x00000012

            /// <summary>The SHIFT modifier key.</summary>
            Shift = 0x00010000,

            /// <summary>The CTRL modifier key.</summary>
            Control = 0x00020000,

            /// <summary>The ALT modifier key.</summary>
            Alt = 0x00040000,
        }

        public bool IsUp => (0x80000000 & _lParam) != 0;

        public bool IsDown => !IsUp;

        public bool IsExtendedKey => (0x01000000 & _lParam) != 0;

        public int RepeatCount => (int)(0xFFFF & _lParam);

        // ReSharper disable once ConvertToAutoProperty
        public int VirtualKeyCode => (int)_wParam;

        public Key Key => KeyInterop.KeyFromVirtualKey(VirtualKeyCode);

        public bool IsModifier => VirtualKeyCode == 0x12 || VirtualKeyCode == 0x11 || VirtualKeyCode == 0x10;

        public int ModifierKeys
        {
            get
            {
                Keys modifiers = 0;
                if (SafeNativeMethods.GetKeyState((int)Keys.ShiftKey) < 0)
                {
                    modifiers |= Keys.Shift;
                }

                if (SafeNativeMethods.GetKeyState((int)Keys.ControlKey) < 0)
                {
                    modifiers |= Keys.Control;
                }

                if (SafeNativeMethods.GetKeyState((int)Keys.Menu) < 0)
                {
                    modifiers |= Keys.Alt;
                }

                return (int)modifiers;
            }
        }

        public int ScanCode => (int)(0xFF0000 & _lParam) >> 16;

        public bool AltPressed => (0x20000000 & _lParam) != 0;

        public bool PreviousKeyStateWasDown => (0x40000000 & _lParam) != 0;

        public bool Handled { get; set; }

        public override string ToString()
        {
            return $"{Key}, "
                   + $"Key{(IsUp ? "Up" : "Down")}, "
                   + $"Repeat Count: {RepeatCount}, "
                   + $"Scan Code: {ScanCode}, "
                   + $"Alt pressed {AltPressed}, "
                   + $"Was down {PreviousKeyStateWasDown}, "
                   + $"Is extended {IsExtendedKey}";
        }
    }
}
