using System;

namespace Eurolook.AddIn.Common.Events
{
    public class MouseWheelEventArgs : MouseEventArgs
    {
        public MouseWheelEventArgs(UIntPtr wParam, IntPtr lParam)
            : base(wParam, lParam)
        {
        }

        public int Delta
        {
            get { return unchecked((short)((long)MouseData >> 16)); }
        }
    }
}
