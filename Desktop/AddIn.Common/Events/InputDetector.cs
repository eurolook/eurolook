﻿using System;
using System.Threading;

namespace Eurolook.AddIn.Common.Events
{
    /// <summary>
    /// A class that raises an event for any kind of keyboard or mouse input made in the current process.
    /// </summary>
    /// <remarks>
    /// Note that there should ever be only a single instance of this class.
    /// Make sure this class is properly disposed to release the keyboard and mouse hooks.
    /// </remarks>
    public sealed class InputDetector : IDisposable
    {
#pragma warning disable S1450 // Private fields only used as local variables in methods should become local variables
        // NOTE: We need a backing field to prevent garbage collection
        // ReSharper disable PrivateFieldCanBeConvertedToLocalVariable
        private readonly SafeNativeMethods.HookProc _mouseProc;
        private readonly SafeNativeMethods.HookProc _keyboardProc;
        // ReSharper restore PrivateFieldCanBeConvertedToLocalVariable
#pragma warning restore S1450 // Private fields only used as local variables in methods should become local variables

        private readonly IntPtr _hookIdMouse;
        private readonly IntPtr _hookIdKeyboard;

        private readonly Timer _timer;

        public InputDetector()
        {
            // make sure that there is a single callback that does not get garbage collected (i.e. backed by a field of this class)
            _mouseProc = MouseHookCallback;
            _keyboardProc = KeyboardHookCallback;

            uint threadId = (uint)SafeNativeMethods.GetCurrentThreadId();
            _hookIdMouse = SafeNativeMethods.SetWindowsHookEx(
                (int)SafeNativeMethods.HookType.WhMouse,
                _mouseProc,
                IntPtr.Zero,
                threadId);

            _hookIdKeyboard = SafeNativeMethods.SetWindowsHookEx(
                (int)SafeNativeMethods.HookType.WhKeyboard,
                _keyboardProc,
                IntPtr.Zero,
                threadId);

            _timer = new Timer(OnInputDetectedDelayed);

            EventDelay = 500;
        }

        ~InputDetector()
        {
            Dispose(false);
        }

        public delegate void InputDetectedEventHandler(object sender, EventArgs e);

        public delegate void MouseEventHandler(object sender, MouseEventArgs e);

        public delegate void MouseWheelEventHandler(object sender, MouseWheelEventArgs e);

        public delegate void KeyEventHandler(object sender, KeyEventArgs e);

        public event InputDetectedEventHandler InputDetectedEvent;

        public event MouseEventHandler PreviewMouseLeftButtonDown;

        public event MouseEventHandler MouseLeftButtonDown;

        public event MouseEventHandler PreviewMouseLeftButtonDoubleClick;

        public event MouseEventHandler MouseLeftButtonDoubleClick;

        public event MouseEventHandler PreviewMouseLeftButtonUp;

        public event MouseEventHandler MouseLeftButtonUp;

        public event MouseEventHandler PreviewMouseRightButtonDown;

        public event MouseEventHandler MouseRightButtonDown;

        public event MouseEventHandler PreviewMouseRightButtonUp;

        public event MouseEventHandler MouseRightButtonUp;

        public event MouseWheelEventHandler PreviewMouseWheel;

        public event MouseWheelEventHandler MouseWheel;

        public event KeyEventHandler PreviewKeyDown;

        public event KeyEventHandler KeyDown;

        public event KeyEventHandler PreviewKeyUp;

        public event KeyEventHandler KeyUp;

        public int EventDelay { get; set; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void RaisePreviewMouseLeftButtonDown(MouseEventArgs e)
        {
            PreviewMouseLeftButtonDown?.Invoke(this, e);
        }

        private void RaiseMouseLeftButtonDown(MouseEventArgs e)
        {
            MouseLeftButtonDown?.Invoke(this, e);
        }

        private void RaisePreviewMouseLeftButtonDoubleClick(MouseEventArgs e)
        {
            PreviewMouseLeftButtonDoubleClick?.Invoke(this, e);
        }

        private void RaiseMouseLeftButtonDoubleClick(MouseEventArgs e)
        {
            MouseLeftButtonDoubleClick?.Invoke(this, e);
        }

        private void RaisePreviewMouseLeftButtonUp(MouseEventArgs e)
        {
            PreviewMouseLeftButtonUp?.Invoke(this, e);
        }

        private void RaiseMouseLeftButtonUp(MouseEventArgs e)
        {
            MouseLeftButtonUp?.Invoke(this, e);
        }

        private void RaisePreviewMouseRightButtonDown(MouseEventArgs e)
        {
            PreviewMouseRightButtonDown?.Invoke(this, e);
        }

        private void RaiseMouseRightButtonDown(MouseEventArgs e)
        {
            MouseRightButtonDown?.Invoke(this, e);
        }

        private void RaisePreviewMouseRightButtonUp(MouseEventArgs e)
        {
            PreviewMouseRightButtonUp?.Invoke(this, e);
        }

        private void RaiseMouseRightButtonUp(MouseEventArgs e)
        {
            MouseRightButtonUp?.Invoke(this, e);
        }

        private void RaisePreviewMouseWheel(MouseWheelEventArgs e)
        {
            PreviewMouseWheel?.Invoke(this, e);
        }

        private void RaiseMouseWheel(MouseWheelEventArgs e)
        {
            MouseWheel?.Invoke(this, e);
        }

        private void RaisePreviewKeyDown(KeyEventArgs e)
        {
            PreviewKeyDown?.Invoke(this, e);
        }

        private void RaiseKeyDown(KeyEventArgs e)
        {
            KeyDown?.Invoke(this, e);
        }

        private void RaisePreviewKeyUp(KeyEventArgs e)
        {
            PreviewKeyUp?.Invoke(this, e);
        }

        private void RaiseKeyUp(KeyEventArgs e)
        {
            KeyUp?.Invoke(this, e);
        }

        private void RaiseInputDetectedEvent()
        {
            InputDetectedEvent?.Invoke(this, new EventArgs());
        }

        private void OnInputDetectedDelayed(object state)
        {
            RaiseInputDetectedEvent();
        }

        private void OnInputDetected()
        {
            // delay triggering the event (this reduces the number of events raised in case of rapid typing)
            _timer.Change(EventDelay, Timeout.Infinite);
        }

        private IntPtr MouseHookCallback(int nCode, UIntPtr wParam, IntPtr lParam)
        {
            ////Trace.TraceInformation("Mouse hooked: {0} - {1}, {2}", nCode, (SafeNativeMethods.WindowMessage)wParam, lParam);
            if (nCode < 0)
            {
                return SafeNativeMethods.CallNextHookEx(_hookIdMouse, nCode, wParam, lParam);
            }

            switch ((SafeNativeMethods.WindowMessage)wParam)
            {
                ////case SafeNativeMethods.WindowMessage.LButtonDown:
                case SafeNativeMethods.WindowMessage.LButtonUp:
                ////case SafeNativeMethods.WindowMessage.MouseMove:
                ////case SafeNativeMethods.WindowMessage.MouseWheel:
                ////case SafeNativeMethods.WindowMessage.RButtonDown:
                case SafeNativeMethods.WindowMessage.RButtonUp:
                    OnInputDetected();
                    break;
            }

            // only handle mouse input into the document window
            var hWnd = SafeNativeMethods.GetFocus();
            string className = SafeNativeMethods.GetClassName(hWnd);
            if (className != "_WwG")
            {
                return SafeNativeMethods.CallNextHookEx(_hookIdMouse, nCode, wParam, lParam);
            }

            Action previewEventHandlerCall = () => { };
            Action eventHandlerCall = () => { };
            MouseEventArgs eventArgs = null;

            switch ((SafeNativeMethods.WindowMessage)wParam)
            {
                case SafeNativeMethods.WindowMessage.LButtonDown:
                    {
                        eventArgs = new MouseEventArgs(wParam, lParam);
                        previewEventHandlerCall = () => RaisePreviewMouseLeftButtonDown(eventArgs);
                        eventHandlerCall = () => RaiseMouseLeftButtonDown(eventArgs);
                        break;
                    }

                case SafeNativeMethods.WindowMessage.LButtonUp:
                    {
                        eventArgs = new MouseEventArgs(wParam, lParam);
                        previewEventHandlerCall = () => RaisePreviewMouseLeftButtonUp(eventArgs);
                        eventHandlerCall = () => RaiseMouseLeftButtonUp(eventArgs);
                        break;
                    }

                case SafeNativeMethods.WindowMessage.LButtonDblClk:
                    {
                        eventArgs = new MouseEventArgs(wParam, lParam);
                        previewEventHandlerCall = () => RaisePreviewMouseLeftButtonDoubleClick(eventArgs);
                        eventHandlerCall = () => RaiseMouseLeftButtonDoubleClick(eventArgs);
                        break;
                    }

                ////case SafeNativeMethods.WindowMessage.MouseMove:
                case SafeNativeMethods.WindowMessage.RButtonDown:
                    {
                        eventArgs = new MouseEventArgs(wParam, lParam);
                        previewEventHandlerCall = () => RaisePreviewMouseRightButtonDown(eventArgs);
                        eventHandlerCall = () => RaiseMouseRightButtonDown(eventArgs);
                        break;
                    }

                case SafeNativeMethods.WindowMessage.RButtonUp:
                    {
                        eventArgs = new MouseEventArgs(wParam, lParam);
                        previewEventHandlerCall = () => RaisePreviewMouseRightButtonUp(eventArgs);
                        eventHandlerCall = () => RaiseMouseRightButtonUp(eventArgs);
                        break;
                    }

                case SafeNativeMethods.WindowMessage.MouseWheel:
                    {
                        var mouseWheelEventArgs = new MouseWheelEventArgs(wParam, lParam);

                        previewEventHandlerCall = () => RaisePreviewMouseWheel(mouseWheelEventArgs);
                        eventHandlerCall = () => RaiseMouseWheel(mouseWheelEventArgs);

                        eventArgs = mouseWheelEventArgs;
                        break;
                    }
            }

            if (eventArgs != null && nCode == 0)
            {
                previewEventHandlerCall();
                if (eventArgs.Handled)
                {
                    // do not pass the mouse input to Word when it's already been handled
                    return new IntPtr(1);
                }

                var result = SafeNativeMethods.CallNextHookEx(_hookIdMouse, nCode, wParam, lParam);

                eventHandlerCall();

                return result;
            }

            return SafeNativeMethods.CallNextHookEx(_hookIdMouse, nCode, wParam, lParam);
        }

        private IntPtr KeyboardHookCallback(int nCode, UIntPtr wParam, IntPtr lParam)
        {
            if (nCode != 0)
            {
                return SafeNativeMethods.CallNextHookEx(_hookIdKeyboard, nCode, wParam, lParam);
            }

            var keyEventArgs = new KeyEventArgs(wParam, lParam);

            // ignore single modifier key press (Alt, Ctrl, Shift)
            if (!keyEventArgs.IsModifier)
            {
                OnInputDetected();
            }

            // only handle keyboard input into the document window
            var hWnd = SafeNativeMethods.GetFocus();
            string className = SafeNativeMethods.GetClassName(hWnd);
            if (className != "_WwG")
            {
                return SafeNativeMethods.CallNextHookEx(_hookIdKeyboard, nCode, wParam, lParam);
            }

            ////Trace.TraceInformation("Key hooked: {0} - {1}, {2}, {3}", nCode, wParam, lParam, keyEventArgs);

            if (keyEventArgs.IsUp)
            {
                RaisePreviewKeyUp(keyEventArgs);
            }
            else
            {
                RaisePreviewKeyDown(keyEventArgs);
            }

            if (keyEventArgs.Handled)
            {
                // do not pass the keyboard input to Word when it's already been handled
                return new IntPtr(1);
            }

            var result = SafeNativeMethods.CallNextHookEx(_hookIdKeyboard, nCode, wParam, lParam);

            if (keyEventArgs.IsUp)
            {
                RaiseKeyUp(keyEventArgs);
            }
            else
            {
                RaiseKeyDown(keyEventArgs);
            }

            return result;
        }

        private void Dispose(bool disposing)
        {
            SafeNativeMethods.UnhookWindowsHookEx(_hookIdMouse);
            SafeNativeMethods.UnhookWindowsHookEx(_hookIdKeyboard);

            if (disposing)
            {
                _timer?.Dispose();
            }
        }
    }
}
