using System;
using System.Runtime.InteropServices;

namespace Eurolook.AddIn.Common.Events
{
    public class MouseEventArgs : EventArgs
    {
        private readonly SafeNativeMethods.ModifierKey _mouseKeys;
        private readonly SafeNativeMethods.MouseHookStructEx _mouseHookStruct;

        public MouseEventArgs(UIntPtr wParam, IntPtr lParam)
        {
            _mouseKeys = (SafeNativeMethods.ModifierKey)wParam;
            _mouseHookStruct =
                (SafeNativeMethods.MouseHookStructEx)Marshal.PtrToStructure(lParam, typeof(SafeNativeMethods.MouseHookStructEx));
        }

        public int X
        {
            get { return _mouseHookStruct.pt.X; }
        }

        public int Y
        {
            get { return _mouseHookStruct.pt.Y; }
        }

        public bool IsCtrlDown
        {
            get { return _mouseKeys.HasFlag(SafeNativeMethods.ModifierKey.MK_CONTROL); }
        }

        public int MouseData
        {
            get { return _mouseHookStruct.MouseData; }
        }

        public bool Handled { get; set; }
    }
}
