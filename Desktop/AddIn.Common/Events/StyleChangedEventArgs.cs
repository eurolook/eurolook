using System;

namespace Eurolook.AddIn.Common.Events
{
    public class StyleChangedEventArgs : EventArgs
    {
        public StyleChangedEventArgs(string oldStyleName, string newStyleName)
        {
            OldStyleName = oldStyleName;
            NewStyleName = newStyleName;
        }

        public string OldStyleName { get; set; }

        public string NewStyleName { get; set; }
    }
}
