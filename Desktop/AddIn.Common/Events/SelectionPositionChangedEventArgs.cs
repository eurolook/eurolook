using System;

namespace Eurolook.AddIn.Common.Events
{
    public class SelectionPositionChangedEventArgs : EventArgs
    {
        public SelectionPositionChangedEventArgs(
            double oldHorizontalPositionRelativeToPage,
            double oldHorizontalPositionRelativeToTextBoundary,
            double oldVerticalPositionRelativeToPage,
            double oldVerticalPositionRelativeToTextBoundary,
            double newHorizontalPositionRelativeToPage,
            double newHorizontalPositionRelativeToTextBoundary,
            double newVerticalPositionRelativeToPage,
            double newVerticalPositionRelativeToTextBoundary)
        {
            OldHorizontalPositionRelativeToPage = oldHorizontalPositionRelativeToPage;
            OldHorizontalPositionRelativeToTextBoundary = oldHorizontalPositionRelativeToTextBoundary;
            OldVerticalPositionRelativeToPage = oldVerticalPositionRelativeToPage;
            OldVerticalPositionRelativeToTextBoundary = oldVerticalPositionRelativeToTextBoundary;
            NewHorizontalPositionRelativeToPage = newHorizontalPositionRelativeToPage;
            NewHorizontalPositionRelativeToTextBoundary = newHorizontalPositionRelativeToTextBoundary;
            NewVerticalPositionRelativeToPage = newVerticalPositionRelativeToPage;
            NewVerticalPositionRelativeToTextBoundary = newVerticalPositionRelativeToTextBoundary;
        }

        public double OldHorizontalPositionRelativeToPage { get; set; }

        public double OldHorizontalPositionRelativeToTextBoundary { get; set; }

        public double OldVerticalPositionRelativeToPage { get; set; }

        public double OldVerticalPositionRelativeToTextBoundary { get; set; }

        public double NewHorizontalPositionRelativeToPage { get; set; }

        public double NewHorizontalPositionRelativeToTextBoundary { get; set; }

        public double NewVerticalPositionRelativeToPage { get; set; }

        public double NewVerticalPositionRelativeToTextBoundary { get; set; }
    }
}
