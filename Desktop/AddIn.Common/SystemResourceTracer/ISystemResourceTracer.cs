namespace Eurolook.AddIn.Common.SystemResourceTracer
{
    public interface ISystemResourceTracer
    {
        void TraceMemoryUsage();
    }
}
