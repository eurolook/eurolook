namespace Eurolook.AddIn.Common.SystemResourceTracer
{
    public class DefaultSystemResourceTracer : ISystemResourceTracer
    {
        public void TraceMemoryUsage()
        {
            // don't trace anything by default
        }
    }
}
