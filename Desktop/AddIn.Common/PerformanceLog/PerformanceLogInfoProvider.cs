using System;
using Eurolook.AddIn.Common.Database;
using Eurolook.Data.Models;

namespace Eurolook.AddIn.Common.PerformanceLog
{
    public class PerformanceLogInfoProvider : IPerformanceLogInfoProvider
    {
        private readonly IUserDataRepository _userDataRepository;
        private bool _isInitialized;

        public PerformanceLogInfoProvider(IAddinContext addinContext, IUserDataRepository userDataRepository)
        {
            _userDataRepository = userDataRepository;
            ClientVersion = addinContext.ClientVersion;
        }

        public void RetrieveInfos()
        {
            if (_isInitialized)
            {
                return;
            }

            DeviceSettings = _userDataRepository.GetOrCreateCurrentDeviceSettings();
            var settings = _userDataRepository.GetUserSettings();
            IsCepEnabled = settings?.IsCepEnabled ?? false;
            _isInitialized = true;
        }

        public DeviceSettings DeviceSettings { get; set; }
        public Version ClientVersion { get; set; }
        public bool IsCepEnabled { get; set; }

        public void ClearCache()
        {
            _isInitialized = false;
        }
    }
}
