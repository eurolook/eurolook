using System;
using Eurolook.Data.Models;

namespace Eurolook.AddIn.Common.PerformanceLog
{
    public interface IPerformanceLogInfoProvider
    {
        DeviceSettings DeviceSettings { get; }

        Version ClientVersion { get; }

        bool IsCepEnabled { get; }

        void RetrieveInfos();

        void ClearCache();
    }
}
