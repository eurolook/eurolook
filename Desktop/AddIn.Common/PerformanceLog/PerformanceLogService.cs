using System;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.Messages;
using Eurolook.Data.Models;
using GalaSoft.MvvmLight.Messaging;

namespace Eurolook.AddIn.Common.PerformanceLog
{
    public class PerformanceLogService : IPerformanceLogService
    {
        private readonly IPerformanceLogInfoProvider _performanceLogInfoProvider;
        private readonly IStatisticsRepository _statisticsRepository;

        public PerformanceLogService(
            IPerformanceLogInfoProvider performanceLogInfoProvider,
            IStatisticsRepository statisticsRepository)
        {
            _performanceLogInfoProvider = performanceLogInfoProvider;
            _statisticsRepository = statisticsRepository;
            Messenger.Default.Register<LocalDataDeletedMessage>(this, OnLocalDataDeleted);
        }

        private void OnLocalDataDeleted(LocalDataDeletedMessage obj)
        {
            _performanceLogInfoProvider.ClearCache();
        }

        public async Task LogAddinStartedAsync(IAddinContext addinContext)
        {
            _performanceLogInfoProvider.RetrieveInfos();
            if (!_performanceLogInfoProvider.IsCepEnabled)
            {
                return;
            }

            var times = addinContext.LoadTimes();
            if (times == null)
            {
                return;
            }

            var timesArray = times as TimeSpan[] ?? times.ToArray();
            if (!timesArray.Any())
            {
                return;
            }

            await _statisticsRepository.WritePerformanceLogAsync(
                PerformanceLogEvent.AddInLoaded,
                timesArray.First(),
                _performanceLogInfoProvider);
        }

        public async Task LogCreationDialogLoadedAsync(long milliseconds)
        {
            var time = new TimeSpan(TimeSpan.TicksPerMillisecond * milliseconds);
            await LogEventAsync(PerformanceLogEvent.CreationDialogLoaded, time);
        }

        public async Task LogDocumentCreatedAsync(long milliseconds)
        {
            var time = new TimeSpan(TimeSpan.TicksPerMillisecond * milliseconds);
            await LogEventAsync(PerformanceLogEvent.DocumentCreated, time);
        }

        public async Task LogDataSyncFinishedAsync(long milliseconds)
        {
            var time = new TimeSpan(TimeSpan.TicksPerMillisecond * milliseconds);
            await LogEventAsync(PerformanceLogEvent.DataSyncFinished, time);
        }

        private async Task LogEventAsync(PerformanceLogEvent logEvent, TimeSpan time)
        {
            _performanceLogInfoProvider.RetrieveInfos();
            if (!_performanceLogInfoProvider.IsCepEnabled)
            {
                return;
            }

            await _statisticsRepository.WritePerformanceLogAsync(
                logEvent,
                time,
                _performanceLogInfoProvider);
        }
    }
}
