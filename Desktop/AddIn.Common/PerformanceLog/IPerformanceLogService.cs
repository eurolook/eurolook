﻿using System.Threading.Tasks;

namespace Eurolook.AddIn.Common.PerformanceLog
{
    public interface IPerformanceLogService
    {
        Task LogAddinStartedAsync(IAddinContext addinContext);

        Task LogCreationDialogLoadedAsync(long milliseconds);

        Task LogDocumentCreatedAsync(long milliseconds);

        Task LogDataSyncFinishedAsync(long milliseconds);
    }
}
