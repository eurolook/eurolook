using System.Threading.Tasks;

namespace Eurolook.AddIn.Common.DocumentMetadata
{
    public interface IMetadataDialog
    {
        Task<bool> ShowAsync(string windowTitle, IEurolookDocument eurolookDocument, bool validateMetadata = false);
    }
}
