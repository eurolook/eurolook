using Eurolook.Data.Models.Metadata;

namespace Eurolook.AddIn.Common.DocumentMetadata.BuiltInProperties
{
    public interface IBuiltInProperties
    {
        void Init(IEurolookDocument document);

        bool IsPropertyDefined(string propertyName);

        MetadataProperty GetPropertyContent(string propertyName);

        void WritePropertyContent(MetadataProperty property);
    }
}
