using System;
using System.Collections.Generic;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.AddIn.Common.DocumentMetadata.BuiltInProperties
{
    public class BuiltInProperties : IBuiltInProperties
    {
        private static readonly Lazy<HashSet<string>> BuiltInPropertyNames = new Lazy<HashSet<string>>();
        private dynamic _builtInDocumentProperties;

        public void Init(IEurolookDocument document)
        {
            // accessing the BuiltInDocumentProperties property may modify the Saved state of the document.
            bool isSaved = document.OfficeDocumentWrapper.Saved;
            try
            {
                _builtInDocumentProperties = document.OfficeDocumentWrapper.BuiltInDocumentProperties;

                if (!BuiltInPropertyNames.IsValueCreated)
                {
                    var propertyNames = BuiltInPropertyNames.Value;
                    foreach (var builtInDocumentProperty in _builtInDocumentProperties)
                    {
                        propertyNames.Add(builtInDocumentProperty.Name);
                    }
                }
            }
            finally
            {
                document.OfficeDocumentWrapper.Saved = isSaved;
            }
        }

        public bool IsPropertyDefined(string propertyName)
        {
            return BuiltInPropertyNames.Value.Contains(propertyName);
        }

        public MetadataProperty GetPropertyContent(string propertyName)
        {
            // NOTE: The variable `value` needs to be explicitly typed as string
            //  otherwise we may end up with a DynamicRuntimeBinder exception in the case that
            //  the variable contains null.
            string value = _builtInDocumentProperties[propertyName].Value;
            return new MetadataProperty(propertyName, value);
        }

        public void WritePropertyContent(MetadataProperty property)
        {
            _builtInDocumentProperties[property.Name].Value = property.GetSimpleValue();
        }
    }
}
