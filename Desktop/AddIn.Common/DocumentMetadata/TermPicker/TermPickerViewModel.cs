﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using Eurolook.AddIn.Common.BrickCommands.EditDocumentMetadata;
using Eurolook.AddIn.Common.Messages;
using Eurolook.Data.Models.SharePointTermStore;
using Eurolook.Data.TermStore;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace Eurolook.AddIn.Common.DocumentMetadata.TermPicker
{
    public enum TermSelectionMode
    {
        Single,
        Multiple,
    }

    public class TermPickerViewModel : ViewModelBase, IDisposable
    {
        private readonly TermStore _termStore;
        private readonly ITermFilter _termFilter;
        private readonly ITermOverrides _termOverrides;
        private readonly int _languageCode;
        private readonly Timer _searchTimer;

        private bool _isVisible;
        private TermSelectionMode _selectionMode;
        private Action<TermPickerViewModel> _confirmAction;
        private string _searchText;
        private bool _isSearchBoxFocused;
        private List<TermViewModel> _allTermVms;
        private bool _isSearchResultListVisible;
        private string _titleText;

        public TermPickerViewModel(TermStore termStore, ITermFilter termFilter, ITermOverrides termOverrides, int languageCode)
            : this()
        {
            _termStore = termStore;
            _termFilter = termFilter;
            _termOverrides = termOverrides;
            _languageCode = languageCode;
        }

        private TermPickerViewModel()
        {
            OkCommand = new RelayCommand(Confirm);
            CancelCommand = new RelayCommand(Cancel);
            StartSearchCommand = new RelayCommand(StartSearch);
            Terms = new ObservableCollection<TermViewModel>();
            SearchResults = new ObservableCollection<TermViewModel>();
            SelectedItems = new ObservableCollection<TermViewModel>();
            TitleText = "Term Picker";
            _selectionMode = TermSelectionMode.Multiple;
            _searchTimer = new Timer(SearchTimerElapsed);
            IsSearchResultListVisible = false;
            Messenger.Default.Register<TermSelectionChangedMessage>(this, TermSelectionChanged);
        }

        ~TermPickerViewModel()
        {
            Dispose(false);
        }

        public ObservableCollection<TermViewModel> Terms { get; set; }

        public ObservableCollection<TermViewModel> SearchResults { get; set; }

        public ObservableCollection<TermViewModel> SelectedItems { get; }

        public string TitleText
        {
            get => _titleText;
            set => Set(() => TitleText, ref _titleText, value);
        }

        public RelayCommand OkCommand { get; set; }

        public RelayCommand CancelCommand { get; set; }

        public RelayCommand StartSearchCommand { get; set; }

        public bool IsVisible
        {
            get => _isVisible;
            set => Set(() => IsVisible, ref _isVisible, value);
        }

        public string SearchText
        {
            get => _searchText;
            set => Set(() => SearchText, ref _searchText, value);
        }

        public bool IsSearchBoxFocused
        {
            get => _isSearchBoxFocused;
            set => Set(() => IsSearchBoxFocused, ref _isSearchBoxFocused, value);
        }

        public bool IsSearchResultListVisible
        {
            get => _isSearchResultListVisible;
            set
            {
                Set(() => IsSearchResultListVisible, ref _isSearchResultListVisible, value);
                RaisePropertyChanged(() => IsTreeVisible);
            }
        }

        public bool IsTreeVisible => !IsSearchResultListVisible;

        public void Init(
            Guid sharePointTermSetId,
            string displayName,
            TermSelectionMode selectionMode,
            List<Guid> selectedTermIds,
            Action<TermPickerViewModel> confirmAction)
        {
            _confirmAction = confirmAction;
            _selectionMode = selectionMode;

            var allTerms = _termStore.GetTermsFlat(sharePointTermSetId, _termFilter)
                                     .Select(t => _termOverrides.GetTermOverride(t)).Where(t => !t.IsExcluded).ToList();
            _allTermVms = allTerms.Select(x => new TermViewModel(_termStore, x, selectedTermIds, _languageCode))
                                  .OrderBy(t => t.CustomSortIndex)
                                  .ThenBy(t => t.DisplayName)
                                  .ToList();

            TitleText = displayName;
            SearchText = string.Empty;

            Terms.Clear();
            InitTerms(null, Terms);

            SelectedItems.Clear();
            InitSelectedItems(Terms);
        }

        public void Show()
        {
            IsVisible = true;
            FocusSearchBox();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _searchTimer?.Dispose();
            }
        }

        private void StartSearch()
        {
            _searchTimer.Change(200, Timeout.Infinite);
        }

        private void SearchTimerElapsed(object state)
        {
            Execute.OnUiThread(() => { SearchInAllTerms(SearchText); });
        }

        private void SearchInAllTerms(string searchText)
        {
            if (string.IsNullOrWhiteSpace(searchText))
            {
                IsSearchResultListVisible = false;
                return;
            }

            SearchResults.Clear();
            foreach (var termVm in _allTermVms.Where(t => t.IsMatch(searchText)))
            {
                SearchResults.Add(termVm);
            }

            IsSearchResultListVisible = true;
        }

        private void TermSelectionChanged(TermSelectionChangedMessage message)
        {
            // add or remove it from the SelectedItems
            if (message.TermViewModel.IsSelected)
            {
                if (_selectionMode == TermSelectionMode.Single)
                {
                    SelectedItems.Clear();
                }

                SelectedItems.Add(message.TermViewModel);
            }
            else if (SelectedItems.Contains(message.TermViewModel))
            {
                SelectedItems.Remove(message.TermViewModel);
            }

            // handle tree
            if (_selectionMode == TermSelectionMode.Single)
            {
                DeselectOtherTermsSilently(message.TermViewModel);
            }
        }

        private void FocusSearchBox()
        {
            // NOTE: The focusing mechanism uses a XAML extension and we actively need to
            // change the bound value to trigger that the element gets focused.
            IsSearchBoxFocused = false;
            IsSearchBoxFocused = true;
        }

        private void InitTerms(Guid? parentId, ICollection<TermViewModel> target)
        {
            foreach (var termVm in _allTermVms.Where(t => t.ParentTermId == parentId))
            {
                target.Add(termVm);
                InitTerms(termVm.SharePointId, termVm.Terms);
            }
        }

        private void DeselectOtherTermsSilently(TermViewModel selectedTermVm)
        {
            foreach (var termVm in Terms)
            {
                DeselectOtherChildTermsSilently(termVm, selectedTermVm);
            }
        }

        private void DeselectOtherChildTermsSilently(TermViewModel termVm, TermViewModel selectedTermVm)
        {
            if (termVm != selectedTermVm)
            {
                termVm.DeselectSilently();
            }

            foreach (var childTerm in termVm.Terms)
            {
                DeselectOtherChildTermsSilently(childTerm, selectedTermVm);
            }
        }

        private void InitSelectedItems(IEnumerable<TermViewModel> termVms)
        {
            foreach (var termVm in termVms)
            {
                if (termVm.IsSelected)
                {
                    SelectedItems.Add(termVm);
                }

                InitSelectedItems(termVm.Terms);
            }
        }

        private void Confirm()
        {
            IsVisible = false;
            _confirmAction(this);
        }

        private void Cancel()
        {
            SelectedItems.Clear();
            IsVisible = false;
        }
    }
}
