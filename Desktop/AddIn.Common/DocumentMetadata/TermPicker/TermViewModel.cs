using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Eurolook.AddIn.Common.Messages;
using Eurolook.Common.Extensions;
using Eurolook.Data.Models.SharePointTermStore;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace Eurolook.AddIn.Common.DocumentMetadata.TermPicker
{
    public class TermViewModel : ViewModelBase
    {
        private bool _isSelected;
        private bool _suppressNextMessage;

        public TermViewModel()
        {
            Terms = new ObservableCollection<TermViewModel>();
            DeselectCommand = new RelayCommand(Deselect);
        }

        public TermViewModel(TermStore termStore, Term term, ICollection<Guid> selectedTermIds, int languageCode)
            : this()
        {
            Term = term;
            ParentTermId = term.ParentTermId;
            SharePointId = term.SharePointId;
            CustomSortIndex = term.CustomSortIndex;
            DisplayName = termStore.GetDefaultTermLabel(SharePointId, languageCode)?.Value
                          ?? termStore.GetDefaultTermLabel(SharePointId)?.Value;

            Aliases = Term.Labels.Count(l => l.LanguageId == languageCode) > 1
                ? Term.Labels.Where(l => !l.IsDefaultForLanguage && l.LanguageId == languageCode).Select(l => l.Value)
                : Enumerable.Empty<string>();

            IsSelected = selectedTermIds.Contains(SharePointId);
            IsSelectable = term.IsAvailableForTagging;
        }

        public Term Term { get; }

        public string DisplayName { get; set; }

        public IEnumerable<string> Aliases { get; }

        public bool HasAliases => Aliases.Any();

        public Guid SharePointId { get; }

        public int CustomSortIndex { get; }

        public ObservableCollection<TermViewModel> Terms { get; set; }

        public RelayCommand DeselectCommand { get; set; }

        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                Set(() => IsSelected, ref _isSelected, value);
                if (_suppressNextMessage)
                {
                    _suppressNextMessage = false;
                }
                else
                {
                    Messenger.Default.Send(new TermSelectionChangedMessage(this));
                }
            }
        }

        public bool IsSelectable { get; }

        public Guid? ParentTermId { get; }

        public bool IsMatch(string searchTerm)
        {
            return Term.Labels.Any(l => l.Value.ContainsInvariantIgnoreCase(searchTerm));
        }

        /// <summary>
        /// Deselects the term without sending a TermSelectionChangedMessage.
        /// </summary>
        public void DeselectSilently()
        {
            _suppressNextMessage = true;
            IsSelected = false;
        }

        private void Deselect()
        {
            IsSelected = false;
        }
    }
}
