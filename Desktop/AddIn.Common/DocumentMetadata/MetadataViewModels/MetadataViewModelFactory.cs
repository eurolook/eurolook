using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels
{
    public class MetadataViewModelFactory : IMetadataViewModelFactory
    {
        private readonly IEnumerable<Func<IMetadataViewModel>> _candidates;

        public MetadataViewModelFactory(IEnumerable<Func<IMetadataViewModel>> candidates)
        {
            _candidates = candidates;
        }

        public IMetadataViewModel CreateViewModel(MetadataDefinition metadataDefinition)
        {
            var result = _candidates
                         .Select(c => c())
                         .FirstOrDefault(
                             c => c.GetType().Name.StartsWith(metadataDefinition.MetadataType + "MetadataViewModel"));

            if (result == null)
            {
                // Return null (and don't throw) if the metadata data type is not supported by
                // the current version of the add-in. If the data type is not supported, the metadata
                // field shall simply not appear in the metadata dialog.
                return null;
            }

            result.MetadataDefinition = metadataDefinition;

            return result;
        }
    }
}
