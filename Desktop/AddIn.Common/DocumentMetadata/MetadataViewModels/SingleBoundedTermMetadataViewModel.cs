using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.AddIn.Common.DocumentMetadata.TermPicker;
using Eurolook.Data.Models.Metadata;
using GalaSoft.MvvmLight.Command;

namespace Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels
{
    public sealed class SingleBoundedTermMetadataViewModel : MetadataViewModelBase
    {
        private readonly Action<TermPickerViewModel> _termPickerConfirmAction;
        private SelectedTermViewModel _selectedTerm;

        public SingleBoundedTermMetadataViewModel()
        {
            OpenTermPickerCommand = new RelayCommand<TermPickerViewModel>(OpenTermPicker);
            _termPickerConfirmAction = termPickerViewModel =>
                                       {
                                           var selectedTerm = termPickerViewModel.SelectedItems.FirstOrDefault();
                                           SelectedTerm = selectedTerm == null
                                               ? null
                                               : new SelectedTermViewModel
                                               {
                                                   Label = selectedTerm.DisplayName,
                                                   SharePointId = selectedTerm.SharePointId,
                                               };
                                       };
        }

        public SelectedTermViewModel SelectedTerm
        {
            get => _selectedTerm;
            set
            {
                SetAndRefresh(() => SelectedTerm, ref _selectedTerm, value);
                RaisePropertyChanged(() => SelectedTermSerializedValue);
            }
        }

        public string SelectedTermSerializedValue => SelectedTerm?.Label;

        public RelayCommand<TermPickerViewModel> OpenTermPickerCommand { get; set; }

        public override void SetValue(MetadataProperty property)
        {
            var termBinding = property.GetTermBinding();
            SelectedTerm = termBinding == null
                ? null
                : new SelectedTermViewModel { Label = termBinding.TermName, SharePointId = termBinding.TermId };
        }

        public override MetadataProperty GetMetadataProperty()
        {
            return new MetadataProperty(
                MetadataDefinition.Name,
                SelectedTerm == null
                    ? null
                    : new TermBinding(SelectedTerm.Label, SelectedTerm.SharePointId));
        }

        private void OpenTermPicker(TermPickerViewModel termPickerViewModel)
        {
            if (!MetadataDefinition.TermSetId.HasValue)
            {
                return;
            }

            var selectedTerms = new List<Guid>();
            if (SelectedTerm != null)
            {
                selectedTerms.Add(SelectedTerm.SharePointId);
            }

            termPickerViewModel.Init(
                MetadataDefinition.TermSetId.Value,
                MetadataDefinition.DisplayName,
                TermSelectionMode.Single,
                selectedTerms,
                _termPickerConfirmAction);

            termPickerViewModel.Show();
        }
    }
}
