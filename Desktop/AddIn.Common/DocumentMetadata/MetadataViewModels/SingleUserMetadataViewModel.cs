using Eurolook.Common.Log;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels
{
    public sealed class SingleUserMetadataViewModel : MetadataViewModelBase, ICanLog
    {
        private UserInfo _selectedUserInfo;

        public UserInfo SelectedUserInfo
        {
            get => _selectedUserInfo;
            set
            {
                Set(() => SelectedUserInfo, ref _selectedUserInfo, value);
                RaisePropertyChanged(() => SelectedUserInfo);
                RaisePropertyChanged(() => SelectedUserInfoSerializedValue);
            }
        }

        public string SelectedUserInfoSerializedValue => SelectedUserInfo?.DisplayName;

        public override void SetValue(MetadataProperty property)
        {
            var userInfo = property.GetUserInfo();
            SelectedUserInfo = userInfo;
        }

        public override MetadataProperty GetMetadataProperty()
        {
            return new MetadataProperty(MetadataDefinition.Name, SelectedUserInfo);
        }
    }
}
