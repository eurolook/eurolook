using System;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels
{
    public class DateMetadataViewModel : MetadataViewModelBase
    {
        private readonly IDateTimeParser _dateTimeParser;
        private DateTime? _value;

        public DateMetadataViewModel(IDateTimeParser dateTimeParser)
        {
            _dateTimeParser = dateTimeParser;
        }

        public DateTime? Value
        {
            get => _value;
            set
            {
                SetAndRefresh(() => Value, ref _value, value);
            }
        }

        public override void SetValue(MetadataProperty property)
        {
            string simpleValue = property.GetSimpleValue();
            var dateValue = _dateTimeParser.ParseDateTimeString(simpleValue);
            if (dateValue != null)
            {
                Value = dateValue.Value;
            }
        }

        public override MetadataProperty GetMetadataProperty()
        {
            return new MetadataProperty(MetadataDefinition.Name, Value?.ToString("s"));
        }
    }
}
