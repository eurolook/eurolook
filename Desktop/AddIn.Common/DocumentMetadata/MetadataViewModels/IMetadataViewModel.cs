﻿using System;
using Eurolook.AddIn.Common.DocumentMetadata.CalculationCommands;
using Eurolook.Data.Models;
using Eurolook.Data.Models.Metadata;
using GalaSoft.MvvmLight.CommandWpf;

namespace Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels
{
    public interface IMetadataViewModel
    {
        MetadataDefinition MetadataDefinition { get; set; }

        Language DocumentLanguage { get; set; }

        string DisplayName { get; }

        string MetadataCategoryDisplayName { get; }

        bool IsReadOnly { get; set; }

        bool IsHidden { get; set; }

        bool IsMandatory { get; set; }

        int? MaxLength { get; set; }

        int? MinLength { get; set; }

        ICalculationCommand CalculationCommand { get; set; }

        void SetValue(MetadataProperty property);

        MetadataProperty GetMetadataProperty();

        IMetadataReaderWriter MetadataReaderWriter { get; set; }

        bool HasCalculationCommand { get; }

        RelayCommand UpdateFromCalculatedValueCommand { get; set; }

        void SetValidationFunction(Func<MetadataProperty, string> validationFunction);

        void SetRefreshCalculationFunction(Action refreshCalculationFunction);

        void SetCalculatedValue(MetadataProperty property);

        void SetCalculatedValueToolTip(string toolTip);
    }
}
