using System;
using System.Linq;
using System.Windows.Threading;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels
{
    public class NumericMetadataViewModel : MetadataViewModelBase
    {
        private string _value;

        public string Value
        {
            get => _value;
            set
            {
                string oldValue = _value;
                _value = value;
                RaisePropertyChanged(() => Value);

                string numericValue = value == null ? null : new string(value.Where(char.IsDigit).ToArray());
                if (_value != numericValue)
                {
                    Dispatcher.CurrentDispatcher.BeginInvoke(
                        new Action(() => Value = null),
                        DispatcherPriority.ApplicationIdle);
                }

                if (oldValue != Value)
                {
                    RefreshCalculationFunction?.Invoke();
                }
            }
        }

        public override void SetValue(MetadataProperty property)
        {
            Value = property.GetSimpleValue();
        }

        public override MetadataProperty GetMetadataProperty()
        {
            return new MetadataProperty(MetadataDefinition.Name, Value);
        }
    }
}
