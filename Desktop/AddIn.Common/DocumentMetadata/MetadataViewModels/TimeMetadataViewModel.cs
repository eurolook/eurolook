using System;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels
{
    public class TimeMetadataViewModel : MetadataViewModelBase
    {
        private readonly ITimeParser _timeParser;
        private readonly IMetadataFormatProvider _metadataFormatProvider;
        private DateTime? _value;

        public TimeMetadataViewModel(ITimeParser timeParser, IMetadataFormatProvider metadataFormatProvider)
        {
            _timeParser = timeParser;
            _metadataFormatProvider = metadataFormatProvider;
        }

        public DateTime? Value
        {
            get => _value;
            set
            {
                SetAndRefresh(() => Value, ref _value, value);
            }
        }

        public override void SetValue(MetadataProperty property)
        {
            string simpleValue = property.GetSimpleValue();
            Value = _timeParser.ParseTimeString(simpleValue);
        }

        public override MetadataProperty GetMetadataProperty()
        {
            var dateTimeFormat = _metadataFormatProvider.GetDateTimeFormat(DocumentLanguage);

            return new MetadataProperty(
                MetadataDefinition.Name,
                Value?.ToString(dateTimeFormat.ShortTimePattern, dateTimeFormat));
        }
    }
}
