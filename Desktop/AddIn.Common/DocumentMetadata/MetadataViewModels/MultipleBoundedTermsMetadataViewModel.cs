using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.AddIn.Common.DocumentMetadata.TermPicker;
using Eurolook.Data.Models.Metadata;
using GalaSoft.MvvmLight.Command;

namespace Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels
{
    public sealed class MultipleBoundedTermsMetadataViewModel : MetadataViewModelBase
    {
        private readonly Action<TermPickerViewModel> _termPickerConfirmAction;

        private List<SelectedTermViewModel> _selectedTerms;

        public MultipleBoundedTermsMetadataViewModel()
        {
            SelectedTerms = new List<SelectedTermViewModel>();
            OpenTermPickerCommand = new RelayCommand<TermPickerViewModel>(OpenTermPicker);
            _termPickerConfirmAction = termPickerViewModel =>
                                       {
                                           SelectedTerms = termPickerViewModel.SelectedItems
                                                                              .OrderBy(t => t.CustomSortIndex)
                                                                              .ThenBy(t => t.DisplayName)
                                                                              .Select(
                                                                                  t => new SelectedTermViewModel
                                                                                  {
                                                                                      SharePointId = t.SharePointId,
                                                                                      Label = t.DisplayName,
                                                                                  })
                                                                              .ToList();
                                       };
        }

        public List<SelectedTermViewModel> SelectedTerms
        {
            get => _selectedTerms;
            set
            {
                SetAndRefresh(() => SelectedTerms, ref _selectedTerms, value);
                RaisePropertyChanged(() => SelectedTermsSerializedValue);
            }
        }

        public RelayCommand<TermPickerViewModel> OpenTermPickerCommand { get; set; }

        public string SelectedTermsSerializedValue
        {
            get { return string.Join("; ", SelectedTerms.Select(s => s.Label)); }
        }

        public override void SetValue(MetadataProperty property)
        {
            SelectedTerms = property.GetTermBindings()
                                    .Select(
                                        tb => new SelectedTermViewModel
                                        {
                                            Label = tb.TermName,
                                            SharePointId = tb.TermId,
                                        })
                                    .ToList();
        }

        public override MetadataProperty GetMetadataProperty()
        {
            return new MetadataProperty(
                MetadataDefinition.Name,
                SelectedTerms.Select(st => new TermBinding(st.Label, st.SharePointId)));
        }

        private void OpenTermPicker(TermPickerViewModel termPickerViewModel)
        {
            if (!MetadataDefinition.TermSetId.HasValue)
            {
                return;
            }

            var selectedTerms = SelectedTerms.Select(t => t.SharePointId).ToList();

            termPickerViewModel.Init(
                MetadataDefinition.TermSetId.Value,
                MetadataDefinition.DisplayName,
                TermSelectionMode.Multiple,
                selectedTerms,
                _termPickerConfirmAction);

            termPickerViewModel.Show();
        }
    }
}
