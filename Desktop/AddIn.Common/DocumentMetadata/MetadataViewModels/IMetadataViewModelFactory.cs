using Eurolook.Data.Models.Metadata;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels
{
    public interface IMetadataViewModelFactory
    {
        [CanBeNull]
        IMetadataViewModel CreateViewModel(MetadataDefinition metadataDefinition);
    }
}
