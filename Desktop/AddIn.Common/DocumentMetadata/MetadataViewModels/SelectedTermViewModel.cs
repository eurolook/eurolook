using System;

namespace Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels
{
    public class SelectedTermViewModel
    {
        public Guid SharePointId { get; set; }

        public string Label { get; set; }
    }
}
