using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Common.Log;
using Eurolook.Data.Models.Metadata;
using GalaSoft.MvvmLight.Command;

namespace Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels
{
    public sealed class MultipleUsersMetadataViewModel : MetadataViewModelBase, ICanLog
    {
        public IEnumerable<UserInfo> SelectedUserInfos { get; set; }

        public string SelectedUserInfosSerializedValue => string.Join(
            "; ",
            SelectedUserInfos?.Select(ui => ui.DisplayName) ?? new[] { "" });

        public RelayCommand SelectUserCommand => new RelayCommand(SelectDocumentAuthor);

        public override void SetValue(MetadataProperty property)
        {
            var userInfos = property.GetUserInfos();
            SelectedUserInfos = userInfos;

            // TODO: currently only readonly
            IsReadOnly = true;
        }

        public override MetadataProperty GetMetadataProperty()
        {
            // TODO
            return new MetadataProperty(MetadataDefinition.Name, SelectedUserInfos);
        }

        private void SelectDocumentAuthor()
        {
            try
            {
                // TODO
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }
    }
}
