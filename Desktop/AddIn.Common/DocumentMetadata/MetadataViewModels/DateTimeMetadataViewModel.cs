using System;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels
{
    public class DateTimeMetadataViewModel : MetadataViewModelBase
    {
        private readonly IDateTimeParser _dateTimeParser;
        private DateTime? _dateValue;
        private DateTime? _timeValue;

        public DateTimeMetadataViewModel(IDateTimeParser dateTimeParser)
        {
            _dateTimeParser = dateTimeParser;
        }

        public DateTime? DateValue
        {
            get => _dateValue;
            set
            {
                SetAndRefresh(() => TimeValue, ref _dateValue, value);
            }
        }

        public DateTime? TimeValue
        {
            get => _timeValue;
            set
            {
                SetAndRefresh(() => TimeValue, ref _timeValue, value);
            }
        }

        public override void SetValue(MetadataProperty property)
        {
            string simpleValue = property.GetSimpleValue();
            var dateTimeValue = _dateTimeParser.ParseDateTimeString(simpleValue);
            if (dateTimeValue != null)
            {
                DateValue = dateTimeValue.Value.Date;
                TimeValue = dateTimeValue.Value;
            }
        }

        public override MetadataProperty GetMetadataProperty()
        {
            var time = TimeValue?.TimeOfDay ?? TimeSpan.Zero;
            string value = (DateValue + time)?.ToString("s");

            return new MetadataProperty(MetadataDefinition.Name, value);
        }
    }
}
