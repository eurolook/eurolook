using Eurolook.Data.Models.Metadata;

namespace Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels
{
    public class BoolMetadataViewModel : MetadataViewModelBase
    {
        private bool _value;

        public bool Value
        {
            get => _value;
            set
            {
                SetAndRefresh(() => Value, ref _value, value);
            }
        }

        public override void SetValue(MetadataProperty property)
        {
            Value = bool.Parse(property.GetSimpleValue());
        }

        public override MetadataProperty GetMetadataProperty()
        {
            return new MetadataProperty(MetadataDefinition.Name, Value.ToString());
        }
    }
}
