﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using Eurolook.AddIn.Common.DocumentMetadata.CalculationCommands;
using Eurolook.Data.Models;
using Eurolook.Data.Models.Metadata;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;

namespace Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels
{
    public abstract class MetadataViewModelBase : ViewModelBase, IMetadataViewModel, IDataErrorInfo
    {
        private Func<MetadataProperty, string> _validationFunction;

        public MetadataViewModelBase()
        {
            UpdateFromCalculatedValueCommand = new RelayCommand(
                UpdateFromCalculatedValue,
                CanUpdateFromCalculatedValue);
        }

        public MetadataDefinition MetadataDefinition { get; set; }

        public Language DocumentLanguage { get; set; }

        public string DisplayName => MetadataDefinition.DisplayName ?? MetadataDefinition.Name;

        public string MetadataCategoryDisplayName =>
            MetadataDefinition.MetadataCategory?.DisplayName ?? MetadataDefinition.MetadataCategory?.Name;

        public bool IsReadOnly { get; set; }

        public string EditableByUserGroups { get; set; }

        public bool IsHidden { get; set; }

        public bool IsMandatory { get; set; }

        public int? MaxLength { get; set; }

        public int? MinLength { get; set; }

        public MetadataProperty CalculatedValue { get; set; }

        public bool HasCalculationCommand => CalculationCommand != null;

        public ICalculationCommand CalculationCommand { get; set; }

        public bool ShowCalculationCommand => HasCalculationCommand && !IsReadOnly;

        public bool IsUpdateFromCalculatedValueNeeded
            => HasCalculationCommand
               && CalculatedValue != null
               && !string.IsNullOrEmpty(CalculatedValue.ToString())
               && !new MetadataPropertyEqualityComparer().Equals(CalculatedValue, GetMetadataProperty());

        public string UpdateFromCalculatedValueTooltip { get; set; }

        public string Error => string.Empty;

        public RelayCommand UpdateFromCalculatedValueCommand { get; set; }

        public IMetadataReaderWriter MetadataReaderWriter { get; set; }

        protected Action RefreshCalculationFunction { get; set; }

        public string this[string columnName] => _validationFunction?.Invoke(GetMetadataProperty());

        public abstract void SetValue(MetadataProperty property);

        public void SetCalculatedValue(MetadataProperty property)
        {
            CalculatedValue = property;
            RaisePropertyChanged(string.Empty);
            UpdateFromCalculatedValueCommand.RaiseCanExecuteChanged();
        }

        public void SetCalculatedValueToolTip(string toolTip)
        {
            UpdateFromCalculatedValueTooltip = toolTip;
            RaisePropertyChanged(() => UpdateFromCalculatedValueTooltip);
        }

        public abstract MetadataProperty GetMetadataProperty();

        public void SetValidationFunction(Func<MetadataProperty, string> validationFunction)
        {
            _validationFunction = validationFunction;
        }

        public void SetRefreshCalculationFunction(Action refreshCalculationFunction)
        {
            RefreshCalculationFunction = refreshCalculationFunction;
        }

        protected void SetAndRefresh<T>(Expression<Func<T>> propertyExpression, ref T field, T newValue)
        {
            var oldValue = field;
            Set(propertyExpression, ref field, newValue);

            if (oldValue == null && newValue == null)
            {
                return;
            }

            if (oldValue == null || !oldValue.Equals(newValue))
            {
                RefreshCalculationFunction?.Invoke();
            }
        }

        private bool CanUpdateFromCalculatedValue()
        {
            return HasCalculationCommand
                   && CalculatedValue != null
                   && !string.IsNullOrEmpty(CalculatedValue.ToString());
            ////&& !new MetadataPropertyEqualityComparer().Equals(CalculatedValue, GetMetadataProperty());
        }

        private void UpdateFromCalculatedValue()
        {
            if (!CanUpdateFromCalculatedValue())
            {
                return;
            }

            SetValue(CalculatedValue);
            RaisePropertyChanged(string.Empty);
            UpdateFromCalculatedValueCommand.RaiseCanExecuteChanged();
        }
    }

    public class MetadataPropertyEqualityComparer : IEqualityComparer<MetadataProperty>
    {
        public bool Equals(MetadataProperty x, MetadataProperty y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            if (ReferenceEquals(x, null))
            {
                return false;
            }

            if (ReferenceEquals(y, null))
            {
                return false;
            }

            if (x.GetType() != y.GetType())
            {
                return false;
            }

            return x.MetadataSerializationType == y.MetadataSerializationType
                   && x.Name == y.Name
                   && string.Equals(x.ToString(), y.ToString(), StringComparison.Ordinal);
        }

        public int GetHashCode(MetadataProperty obj)
        {
            return HashCode.Combine((int)obj.MetadataSerializationType, obj.Name, obj.ToString());
        }
    }
}
