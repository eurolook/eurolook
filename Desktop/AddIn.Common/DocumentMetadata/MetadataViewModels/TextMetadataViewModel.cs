using Eurolook.Data.Models.Metadata;

namespace Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels
{
    public class TextMetadataViewModel : MetadataViewModelBase
    {
        private string _value;

        public string Value
        {
            get => _value;
            set
            {
                SetAndRefresh(() => Value, ref _value, value);
            }
        }

        public override void SetValue(MetadataProperty property)
        {
            Value = property.GetSimpleValue();
        }

        public override MetadataProperty GetMetadataProperty()
        {
            return new MetadataProperty(MetadataDefinition.Name, Value);
        }
    }
}
