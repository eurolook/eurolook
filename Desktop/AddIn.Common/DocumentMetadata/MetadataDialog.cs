using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.BrickCommands.EditDocumentMetadata;

namespace Eurolook.AddIn.Common.DocumentMetadata
{
    public class MetadataDialog : IMetadataDialog
    {
        private readonly IMessageService _messageService;
        private readonly Func<EditDocumentMetadataViewModel> _editDocumentMetadataViewModelFactory;
        private readonly IEnumerable<IMetadataUpdatedHandler> _metadataUpdatedHandlers;

        public MetadataDialog(
            IMessageService messageService,
            Func<EditDocumentMetadataViewModel> editDocumentMetadataViewModelFactory,
            IEnumerable<IMetadataUpdatedHandler> metadataUpdatedHandlers)
        {
            _messageService = messageService;
            _editDocumentMetadataViewModelFactory = editDocumentMetadataViewModelFactory;
            _metadataUpdatedHandlers = metadataUpdatedHandlers;
        }

        public async Task<bool> ShowAsync(string windowTitle, IEurolookDocument eurolookDocument, bool validateMetadata = false)
        {
            using (var editDocumentMetadataViewModel = _editDocumentMetadataViewModelFactory())
            {
                var win = new EditDocumentMetadataWindow(editDocumentMetadataViewModel);
                await editDocumentMetadataViewModel.InitAsync(windowTitle, eurolookDocument, validateMetadata);

                if (_messageService.ShowDialog(win) == true)
                {
                    editDocumentMetadataViewModel.WriteMetadataToDocument();

                    foreach (var metadataUpdatedHandler in _metadataUpdatedHandlers.OrderBy(h => h.ExecutionOrder))
                    {
                        await metadataUpdatedHandler.HandleAsync(eurolookDocument);
                    }

                    return true;
                }

                return false;
            }
        }
    }
}
