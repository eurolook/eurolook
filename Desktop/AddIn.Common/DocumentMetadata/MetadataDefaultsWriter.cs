using System.Linq;
using System.Threading.Tasks;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.AddIn.Common.DocumentMetadata
{
    public class MetadataDefaultsWriter : IDocumentCreationHandler
    {
        private readonly IMetadataPropertyFactory _metadataPropertyFactory;
        private readonly IMetadataReaderWriter _metadataReaderWriter;

        public MetadataDefaultsWriter(IMetadataPropertyFactory metadataPropertyFactory, IMetadataReaderWriter metadataReaderWriter)
        {
            _metadataPropertyFactory = metadataPropertyFactory;
            _metadataReaderWriter = metadataReaderWriter;
        }

        public float ExecutionOrder => 10;

        public async Task HandleAsync(IEurolookDocument eurolookDocument)
        {
            int languageCode = eurolookDocument.Language.CultureInfo.TextInfo.LCID;
            var documentMetadataDefinitions = eurolookDocument.DocumentModel.DocumentModelMetadataDefinitions;

            await _metadataPropertyFactory.InitAsync(languageCode);
            var metadataProperties = documentMetadataDefinitions
                .ToDictionary(
                    dmmd => dmmd.MetadataDefinition,
                    dmmd => _metadataPropertyFactory.Create(dmmd.MetadataDefinition, dmmd.DefaultValue));

            _metadataReaderWriter.Init(eurolookDocument);
            _metadataReaderWriter.WriteMetadata(metadataProperties);
        }
    }
}
