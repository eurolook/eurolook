using System;
using System.Globalization;
using Eurolook.Data.Database;

namespace Eurolook.AddIn.Common.DocumentMetadata
{
    public class DateTimeParser : IDateTimeParser
    {
        private readonly ILanguageRepository _languageRepository;

        public DateTimeParser(ILanguageRepository languageRepository)
        {
            _languageRepository = languageRepository;
        }

        public DateTime? ParseDateTimeString(string value)
        {
            if (DateTime.TryParse(value, out var dateTimeValue))
            {
                return dateTimeValue;
            }

            var languages = _languageRepository.GetAllLanguages();

            foreach (var language in languages)
            {
                var culture = new CultureInfo(language.Locale);
                if (DateTime.TryParse(value, culture, DateTimeStyles.AllowWhiteSpaces, out var dateTimeValueLocalized))
                {
                    return dateTimeValueLocalized;
                }
            }

            return null;
        }
    }
}
