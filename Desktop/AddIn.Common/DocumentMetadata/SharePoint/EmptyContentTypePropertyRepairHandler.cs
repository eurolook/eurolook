﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Eurolook.Common.Log;
using JetBrains.Annotations;
using Microsoft.Office.Core;

namespace Eurolook.AddIn.Common.DocumentMetadata.SharePoint
{
    /// <inheritdoc cref="IDocumentCreationHandler" />
    /// <inheritdoc cref="IDocumentBeforeSaveHandler" />
    /// <summary>
    ///     <para>
    ///     Fixes an issue with content type metadata that are bound to the term store and are not set to any value.
    ///     </para>
    ///     <para>
    ///     Word only creates an empty &lt;Terms&gt; element in the custom XML, but SharePoint requires the complete node structure
    ///     including the children
    ///     <code>
    ///         &lt;TermInfo&gt;&lt;TermName&gt;&lt;TermId&gt;
    ///     </code>
    ///     </para>
    /// </summary>
    [UsedImplicitly]
    public class EmptyContentTypePropertyRepairHandler : IDocumentCreationHandler, IDocumentBeforeSaveHandler, ICanLog
    {
        public float ExecutionOrder => 100.0f;

        public Task HandleAsync(IEurolookDocument eurolookDocument)
        {
            try
            {
                var document = eurolookDocument.OfficeDocumentWrapper;

                const string rootName = "properties";
#pragma warning disable S1075 // URIs should not be hardcoded
                const string rootNamespaceUri = "http://schemas.microsoft.com/office/2006/metadata/properties";
#pragma warning restore S1075 // URIs should not be hardcoded

                var customXmlParts = (IEnumerable<CustomXMLPart>)document.CustomXMLParts.Cast<CustomXMLPart>().ToList();

                var part = customXmlParts.FirstOrDefault(
                    p => (p.DocumentElement.BaseName == rootName)
                         && (p.DocumentElement.NamespaceURI == rootNamespaceUri));

                if (part != null)
                {
                    XNamespace nsPartnerControls = "http://schemas.microsoft.com/office/infopath/2007/PartnerControls";

                    var nodes = part.SelectNodes("//*[local-name() = 'Terms' and not(*)]");

                    var emptyTermsNode = new XElement(
                        nsPartnerControls + "Terms",
                        new XElement(
                            nsPartnerControls + "TermInfo",
                            new XAttribute("xmlns", nsPartnerControls),
                            new XElement(nsPartnerControls + "TermName", new XAttribute("xmlns", nsPartnerControls), ""),
                            new XElement(nsPartnerControls + "TermId", new XAttribute("xmlns", nsPartnerControls), "")));

                    string completeNode = emptyTermsNode.ToString();

                    foreach (CustomXMLNode node in nodes)
                    {
                        node.ParentNode.ReplaceChildSubtree(completeNode, node);
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }

            return Task.FromResult(0);
        }

        public async Task<bool> OnBeforeSaveAsync(IEurolookDocument eurolookDocument, bool saveasui)
        {
            await HandleAsync(eurolookDocument);
            return true;
        }
    }
}
