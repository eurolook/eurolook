using Eurolook.Data.Models.Metadata;

namespace Eurolook.AddIn.Common.DocumentMetadata.SharePoint
{
    public interface IMetaPropertyReaderWriterFactory
    {
        IMetaPropertyReaderWriter GetMetaPropertyReaderWriter(MetadataType metadataType);
    }
}
