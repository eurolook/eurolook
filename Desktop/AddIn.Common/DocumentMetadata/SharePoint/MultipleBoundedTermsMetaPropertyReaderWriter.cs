using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.Models.Metadata;
using Microsoft.Office.Core;

namespace Eurolook.AddIn.Common.DocumentMetadata.SharePoint
{
    public class MultipleBoundedTermsMetaPropertyReaderWriter : IMetaPropertyReaderWriter
    {
        public MetadataProperty ReadProperty(MetaProperty wordProperty)
        {
            return new MetadataProperty(wordProperty.Id, TryReadTermBindings(wordProperty));
        }

        public void WriteProperty(MetaProperty wordProperty, MetadataProperty content)
        {
            wordProperty.Value = content?.GetTermBindings()
                                        ?.SelectMany(tb => new[] { tb.TermName, tb.TermId.ToString() })
                                        .ToArray();
        }

        public bool CanHandle(MetadataType metadataType)
        {
            return metadataType == MetadataType.MultipleBoundedTerms;
        }

        private IEnumerable<TermBinding> TryReadTermBindings(MetaProperty wordProperty)
        {
            try
            {
                return ReadTermBindings(wordProperty);
            }
            catch (Exception)
            {
                return new List<TermBinding>();
            }
        }

        private IEnumerable<TermBinding> ReadTermBindings(MetaProperty wordProperty)
        {
            var namePropertyPairs = wordProperty.Value as string[];
            if (namePropertyPairs == null)
            {
                return new List<TermBinding>();
            }

            var termNames = namePropertyPairs.Where((s, i) => i % 2 == 0);
            var termIds = namePropertyPairs.Where((s, i) => i % 2 == 1);
            return termNames.Zip(termIds, (name, id) => new { TermName = name, TermId = id })
                            .Where(p => !string.IsNullOrEmpty(p.TermName) && !string.IsNullOrEmpty(p.TermId))
                            .Select(p => new TermBinding(p.TermName, Guid.Parse(p.TermId)));
        }
    }
}
