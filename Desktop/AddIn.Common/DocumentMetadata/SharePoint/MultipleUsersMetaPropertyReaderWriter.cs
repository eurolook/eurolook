using System.Linq;
using Eurolook.Data.Models.Metadata;
using Microsoft.Office.Core;

namespace Eurolook.AddIn.Common.DocumentMetadata.SharePoint
{
    public class MultipleUsersMetaPropertyReaderWriter : UserMetaPropertyReaderWriter, IMetaPropertyReaderWriter
    {
        public MetadataProperty ReadProperty(MetaProperty wordProperty)
        {
            return new MetadataProperty(wordProperty.Id, TryReadUserInfos(wordProperty));
        }

        public void WriteProperty(MetaProperty wordProperty, MetadataProperty content)
        {
            wordProperty.Value = content?.GetUserInfos()?
                                        .Where(ui => ui.AccountId != null)
                                        .SelectMany(tb => new[] { tb.AccountId.ToString(), tb.DisplayName })
                                        .ToArray();
        }

        public bool CanHandle(MetadataType metadataType)
        {
            return metadataType == MetadataType.MultipleUsers;
        }
    }
}
