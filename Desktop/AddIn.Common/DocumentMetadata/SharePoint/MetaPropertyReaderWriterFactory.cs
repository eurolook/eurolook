using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.AddIn.Common.DocumentMetadata.SharePoint
{
    public class MetaPropertyReaderWriterFactory : IMetaPropertyReaderWriterFactory
    {
        private readonly IEnumerable<IMetaPropertyReaderWriter> _candidates;

        public MetaPropertyReaderWriterFactory(IEnumerable<IMetaPropertyReaderWriter> candidates)
        {
            _candidates = candidates;
        }

        public IMetaPropertyReaderWriter GetMetaPropertyReaderWriter(MetadataType metadataType)
        {
            return _candidates.Single(c => c.CanHandle(metadataType));
        }
    }
}
