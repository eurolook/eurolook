using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using DocumentFormat.OpenXml.Packaging;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.Data.Constants;
using Eurolook.Data.Models.Metadata;
using Microsoft.Office.Core;

namespace Eurolook.AddIn.Common.DocumentMetadata.SharePoint
{
    public class ContentTypeProperties : IContentTypeProperties
    {
        private readonly IMetaPropertyReaderWriterFactory _metaPropertyReaderWriterFactory;
        private IEurolookDocument _document;
        private List<MetaProperty> _properties;
        private XDocument _contentTypeSchemaFromTemplate;
        private XDocument _contentTypePropertiesFromTemplate;

        public ContentTypeProperties(
            IMetaPropertyReaderWriterFactory metaPropertyReaderWriterFactory)
        {
            _metaPropertyReaderWriterFactory = metaPropertyReaderWriterFactory;
        }

        public MetadataProperty GetPropertyContent(MetadataDefinition metadataDefinition)
        {
            var wordProperty = GetWordPropertyFromDocument(metadataDefinition.Name);
            var readerWriter = _metaPropertyReaderWriterFactory.GetMetaPropertyReaderWriter(metadataDefinition.MetadataType);
            return readerWriter.ReadProperty(wordProperty);
        }

        public bool IsPropertyDefined(string propertyId)
        {
            return GetWordPropertyFromDocument(propertyId) != null;
        }

        public void Init(IEurolookDocument document)
        {
            _document = document;
            try
            {
                var contentTypeProperties = _document?.OfficeDocumentWrapper?.ContentTypeProperties;
                _properties = contentTypeProperties != null
                    ? contentTypeProperties.OfType<MetaProperty>().ToList()
                    : new List<MetaProperty>();
            }
            catch (Exception)
            {
                // Exception is thrown if there are no ContentTypeProperties in the document
                _properties = new List<MetaProperty>();
            }
        }

        public void WritePropertyContent(MetadataDefinition metadataDefinition, MetadataProperty content)
        {
            var wordProperty = GetWordPropertyFromDocument(content.Name);
            var readerWriter = _metaPropertyReaderWriterFactory.GetMetaPropertyReaderWriter(metadataDefinition.MetadataType);
            readerWriter.WriteProperty(wordProperty, content);
        }

        public XDocument GetContentTypeSchemaXmlFromTemplate()
        {
            if (_contentTypeSchemaFromTemplate == null)
            {
                LoadContentTypeXmlData();
            }

            return _contentTypeSchemaFromTemplate;
        }

        public XDocument GetContentTypePropertiesFromTemplate()
        {
            if (_contentTypePropertiesFromTemplate == null)
            {
                LoadContentTypeXmlData();
            }

            return _contentTypePropertiesFromTemplate;
        }

        /// <summary>
        /// Check whether the content type schema has been restored externally by uploading it to a
        /// document library that attaches the 'correct' content type schema.
        /// </summary>
        /// <remarks>
        /// We assume that the schema has been restored externally by SharePoint in the following case:
        /// There is at least one mandatory property that is not empty in the EurolookCustomXML
        /// but that is empty in the ContentTypeProperties.
        /// </remarks>
        public bool IsContentTypeSchemaRestored()
        {
            var mandatoryProperties = _document.DocumentModel
                                               .DocumentModelMetadataDefinitions
                                               .Where(dmmd => dmmd.IsMandatory)
                                               .Select(dmmd => dmmd.MetadataDefinition);

            var documentMetadata = _document.GetEurolookProperties()?.DocumentMetadata;
            foreach (var mandatoryPropertyDefinition in mandatoryProperties)
            {
                MetadataProperty propertyInEurolookCustomXml = null;
                documentMetadata?.TryGetValue(mandatoryPropertyDefinition.Name, out propertyInEurolookCustomXml);
                if (string.IsNullOrWhiteSpace(propertyInEurolookCustomXml?.ToString()))
                {
                    continue;
                }

                if (IsPropertyDefined(mandatoryPropertyDefinition.Name))
                {
                    var propertyInContentType = GetPropertyContent(mandatoryPropertyDefinition);
                    if (string.IsNullOrWhiteSpace(propertyInContentType?.ToString()))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private void LoadContentTypeXmlData()
        {
            var template = _document.DocumentModel.Template;

            if (template == null)
            {
                return;
            }

            using (var stream = new MemoryStream())
            {
                stream.Write(template, 0, template.Length);
                using (var docx = WordprocessingDocument.Open(stream, true))
                {
                    if (docx.MainDocumentPart == null)
                    {
                        return;
                    }

                    foreach (var customXmlPart in docx.MainDocumentPart.CustomXmlParts)
                    {
                        var xdoc = customXmlPart.GetXDocument();
                        var xname = xdoc.Root?.Name;
                        if (xname == Namespaces.ContentTypeSchemaNs + "contentTypeSchema")
                        {
                            _contentTypeSchemaFromTemplate = xdoc;
                        }
                        else if (xname == Namespaces.ContentTypePropertiesNs + "properties")
                        {
                            _contentTypePropertiesFromTemplate = xdoc;
                        }
                    }
                }
            }
        }

        private MetaProperty GetWordPropertyFromDocument(string propertyId)
        {
            return _properties.SingleOrDefault(mp => mp.Id == propertyId);
        }
    }
}
