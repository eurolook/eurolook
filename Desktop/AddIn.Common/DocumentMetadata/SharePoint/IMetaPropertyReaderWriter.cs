using Eurolook.Data.Models.Metadata;
using Microsoft.Office.Core;

namespace Eurolook.AddIn.Common.DocumentMetadata.SharePoint
{
    public interface IMetaPropertyReaderWriter
    {
        MetadataProperty ReadProperty(MetaProperty wordProperty);

        void WriteProperty(MetaProperty wordProperty, MetadataProperty content);

        bool CanHandle(MetadataType metadataType);
    }
}
