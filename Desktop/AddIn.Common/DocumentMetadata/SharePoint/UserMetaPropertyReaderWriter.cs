using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.Models.Metadata;
using Microsoft.Office.Core;

namespace Eurolook.AddIn.Common.DocumentMetadata.SharePoint
{
    public abstract class UserMetaPropertyReaderWriter
    {
        protected IEnumerable<UserInfo> TryReadUserInfos(MetaProperty wordProperty)
        {
            try
            {
                return ReadUserInfos(wordProperty);
            }
            catch (Exception)
            {
                return Enumerable.Empty<UserInfo>();
            }
        }

        protected IEnumerable<UserInfo> ReadUserInfos(MetaProperty wordProperty)
        {
            var namePropertyPairs = wordProperty.Value as string[];
            if (namePropertyPairs == null)
            {
                return Enumerable.Empty<UserInfo>();
            }

            var accountIds = namePropertyPairs.Where((s, i) => i % 2 == 0);
            var displayNames = namePropertyPairs.Where((s, i) => i % 2 == 1);
            return accountIds.Zip(
                                 displayNames,
                                 (accountId, displayName) => new
                                 {
                                     AccountId = int.TryParse(accountId, out int tempVal) ? tempVal : (int?)null,
                                     DisplayName = displayName,
                                 })
                             .Where(p => (p.AccountId != null) && !string.IsNullOrEmpty(p.DisplayName))
                             .Select(p => new UserInfo(p.AccountId, p.DisplayName));
        }
    }
}
