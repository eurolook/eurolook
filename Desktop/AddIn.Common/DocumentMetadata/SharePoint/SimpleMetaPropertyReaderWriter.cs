using Eurolook.Data.Models.Metadata;
using Microsoft.Office.Core;

namespace Eurolook.AddIn.Common.DocumentMetadata.SharePoint
{
    public class SimpleMetaPropertyReaderWriter : IMetaPropertyReaderWriter
    {
        public MetadataProperty ReadProperty(MetaProperty wordProperty)
        {
            string value = null;
            if (wordProperty.Value != null)
            {
                // ensure that date/time values are converted to a culture-independent
                // format that supports round-trips
                value = wordProperty.Type == MsoMetaPropertyType.msoMetaPropertyTypeDateTime
                    ? (string)((dynamic)wordProperty.Value).ToString("s")
                    : (string)wordProperty.Value.ToString();
            }

            return new MetadataProperty(wordProperty.Id, value);
        }

        public void WriteProperty(MetaProperty wordProperty, MetadataProperty content)
        {
            wordProperty.Value = content.GetSimpleValue();
        }

        public bool CanHandle(MetadataType metadataType)
        {
            return (metadataType != MetadataType.SingleBoundedTerm)
                   && (metadataType != MetadataType.MultipleBoundedTerms)
                   && (metadataType != MetadataType.SingleUser)
                   && (metadataType != MetadataType.MultipleUsers);
        }
    }
}
