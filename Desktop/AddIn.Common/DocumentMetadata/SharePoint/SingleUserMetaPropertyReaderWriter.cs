using System.Linq;
using Eurolook.Data.Models.Metadata;
using Microsoft.Office.Core;

namespace Eurolook.AddIn.Common.DocumentMetadata.SharePoint
{
    public class SingleUserMetaPropertyReaderWriter : UserMetaPropertyReaderWriter, IMetaPropertyReaderWriter
    {
        public MetadataProperty ReadProperty(MetaProperty wordProperty)
        {
            return new MetadataProperty(wordProperty.Id, TryReadUserInfos(wordProperty).FirstOrDefault());
        }

        public void WriteProperty(MetaProperty wordProperty, MetadataProperty content)
        {
            var userInfo = content?.GetUserInfo();

            wordProperty.Value = userInfo?.AccountId == null
                ? null
                : new[] { userInfo.AccountId.ToString(), userInfo.DisplayName };
        }

        public bool CanHandle(MetadataType metadataType)
        {
            return metadataType == MetadataType.SingleUser;
        }
    }
}
