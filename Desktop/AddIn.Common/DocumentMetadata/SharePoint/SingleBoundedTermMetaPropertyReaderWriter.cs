using System;
using Eurolook.Data.Models.Metadata;
using Microsoft.Office.Core;

namespace Eurolook.AddIn.Common.DocumentMetadata.SharePoint
{
    public class SingleBoundedTermMetaPropertyReaderWriter : IMetaPropertyReaderWriter
    {
        public MetadataProperty ReadProperty(MetaProperty wordProperty)
        {
            return new MetadataProperty(wordProperty.Id, TryReadTermBinding(wordProperty));
        }

        public void WriteProperty(MetaProperty wordProperty, MetadataProperty content)
        {
            var termBinding = content?.GetTermBinding();

            wordProperty.Value = termBinding == null
                ? null
                : new[] { termBinding.TermName, termBinding.TermId.ToString() };
        }

        public bool CanHandle(MetadataType metadataType)
        {
            return metadataType == MetadataType.SingleBoundedTerm;
        }

        private TermBinding TryReadTermBinding(MetaProperty wordProperty)
        {
            try
            {
                return ReadTermBinding(wordProperty);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private TermBinding ReadTermBinding(MetaProperty wordProperty)
        {
            if (wordProperty.Value == null)
            {
                return null;
            }

            var termName = ((dynamic)wordProperty.Value)[0];

            if (!Guid.TryParse(((dynamic)wordProperty.Value)[1], out Guid termId))
            {
                return null;
            }

            return new TermBinding(termName, termId);
        }
    }
}
