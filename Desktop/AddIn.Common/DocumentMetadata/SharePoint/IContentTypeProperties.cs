using System.Xml.Linq;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.AddIn.Common.DocumentMetadata.SharePoint
{
    public interface IContentTypeProperties
    {
        void Init(IEurolookDocument document);

        bool IsPropertyDefined(string propertyId);

        MetadataProperty GetPropertyContent(MetadataDefinition metadataDefinition);

        void WritePropertyContent(MetadataDefinition metadataDefinition, MetadataProperty content);

        XDocument GetContentTypeSchemaXmlFromTemplate();

        XDocument GetContentTypePropertiesFromTemplate();

        bool IsContentTypeSchemaRestored();
    }
}
