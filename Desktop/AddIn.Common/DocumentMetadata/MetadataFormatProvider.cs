using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.Database;
using Eurolook.Data.Database;
using Eurolook.Data.Models;

namespace Eurolook.AddIn.Common.DocumentMetadata
{
    public class MetadataFormatProvider : IMetadataFormatProvider
    {
        private static readonly string MetadataShortTimeFormatAlias = "TimeFormatShort";
        private static readonly string MetadataShortTimeFormatAMDesignatorAlias = "TimeFormatAMDesignator";
        private static readonly string MetadataShortTimeFormatPMDesignatorAlias = "TimeFormatPMDesignator";

        private readonly Dictionary<Guid, DateTimeFormatInfo> _dateTimeFormatCache = new Dictionary<Guid, DateTimeFormatInfo>();
        private readonly ITextsRepository _textsRepository;
        private readonly ILanguageRepository _languageRepository;

        public MetadataFormatProvider(ITextsRepository textsRepository, ILanguageRepository languageRepository)
        {
            _textsRepository = textsRepository;
            _languageRepository = languageRepository;
        }

        public DateTimeFormatInfo GetDateTimeFormat(Language language)
        {
            return _dateTimeFormatCache.ContainsKey(language.Id)
                ? _dateTimeFormatCache[language.Id]
                : language.CultureInfo.DateTimeFormat;
        }

        public async Task InitAsync()
        {
            if (_dateTimeFormatCache.Any())
            {
                return;
            }

            var languages = await _languageRepository.GetAllLanguagesAsync();
            var shortTimeFormats =
                (await _textsRepository.GetTranslationsAsync(MetadataShortTimeFormatAlias)).ToDictionary(
                    t => t.LanguageId,
                    t => t.Value);
            var amDesignators =
                (await _textsRepository.GetTranslationsAsync(MetadataShortTimeFormatAMDesignatorAlias)).ToDictionary(
                    t => t.LanguageId,
                    t => t.Value);
            var pmDesignators =
                (await _textsRepository.GetTranslationsAsync(MetadataShortTimeFormatPMDesignatorAlias)).ToDictionary(
                    t => t.LanguageId,
                    t => t.Value);

            foreach (var language in languages)
            {
                CreateDateTimeFormatInfoAsync(language, shortTimeFormats, amDesignators, pmDesignators);
            }
        }

        private void CreateDateTimeFormatInfoAsync(
            Language language,
            IReadOnlyDictionary<Guid, string> shortTimeFormats,
            IReadOnlyDictionary<Guid, string> amDesignators,
            IReadOnlyDictionary<Guid, string> pmDesignators)
        {
            var dateTimeFormatInfo = (DateTimeFormatInfo)language.CultureInfo.DateTimeFormat.Clone();

            if (shortTimeFormats.ContainsKey(language.Id))
            {
                dateTimeFormatInfo.ShortTimePattern = shortTimeFormats[language.Id];
            }

            if (amDesignators.ContainsKey(language.Id))
            {
                dateTimeFormatInfo.AMDesignator = amDesignators[language.Id];
            }

            if (pmDesignators.ContainsKey(language.Id))
            {
                dateTimeFormatInfo.PMDesignator = pmDesignators[language.Id];
            }

            _dateTimeFormatCache[language.Id] = dateTimeFormatInfo;
        }
    }
}
