using System;

namespace Eurolook.AddIn.Common.DocumentMetadata
{
    public interface IDateTimeParser
    {
        DateTime? ParseDateTimeString(string value);
    }
}
