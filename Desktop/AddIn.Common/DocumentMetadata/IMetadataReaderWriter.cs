using System.Collections.Generic;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.AddIn.Common.DocumentMetadata
{
    public interface IMetadataReaderWriter
    {
        void Init(IEurolookDocument eurolookDocument);

        MetadataProperty ReadMetadataProperty(MetadataDefinition metadataDefinition);

        void WriteMetadata(Dictionary<MetadataDefinition, MetadataProperty> metadataDictionary);

        void ReplaceMetadataProperty(MetadataDefinition metadataDefinition, MetadataProperty metadataProperty);

        MetadataDefinition GetMetadataDefinition(string metadataName);
    }
}
