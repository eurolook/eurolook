using Eurolook.Data.Models.Metadata;

namespace Eurolook.AddIn.Common.DocumentMetadata
{
    public interface IMetadataPropertyValidator
    {
        bool IsPropertyValidated(string propertyName, IEurolookDocument eurolookDocument);

        string Validate(MetadataProperty metadataProperty, IEurolookDocument eurolookDocument);
    }
}
