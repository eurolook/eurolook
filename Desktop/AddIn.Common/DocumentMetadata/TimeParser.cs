using System;

namespace Eurolook.AddIn.Common.DocumentMetadata
{
    public class TimeParser : ITimeParser
    {
        public DateTime? ParseTimeString(string value)
        {
            value = value?.Replace("h", ":");
            value = value?.Replace(" ", "");
            value = value?.Replace(".", "");

            if (DateTime.TryParse(value, out var dateTimeValue))
            {
                return dateTimeValue;
            }

            return null;
        }
    }
}
