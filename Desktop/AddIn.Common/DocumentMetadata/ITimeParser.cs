using System;

namespace Eurolook.AddIn.Common.DocumentMetadata
{
    public interface ITimeParser
    {
        DateTime? ParseTimeString(string value);
    }
}
