using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels;

namespace Eurolook.AddIn.Common.DocumentMetadata.Immc
{
    public interface IMetadataToImmcMapper
    {
        Task<ImmcWork> GetImmcWorkAsync(IEurolookDocument eurolookDocument, List<IMetadataViewModel> metadata);
    }
}
