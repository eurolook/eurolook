using System.Xml.Serialization;

namespace Eurolook.AddIn.Common.DocumentMetadata.Immc
{
    public class Agent
    {
        [XmlAttribute("role")]
        public string Role { get; set; }

        [XmlText]
        public string Value { get; set; }
    }
}
