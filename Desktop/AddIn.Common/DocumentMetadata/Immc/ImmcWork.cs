using System.Xml.Serialization;

namespace Eurolook.AddIn.Common.DocumentMetadata.Immc
{
    [XmlRoot("work", Namespace = "http://publications.europa.eu/resource/core-metadata", IsNullable = false)]
    public class ImmcWork
    {
        public ImmcWork()
        {
        }

        public ImmcWork(string type, string identifier, /*string author, */string isoDate, string version, string language, string title)
        {
            Type = type;
            Identifier = identifier;
            Agent = new Agent
            {
                Role = "RESP_INST",
                Value = "ECA",
            };
            ////AgentPerson = new Agent
            ////{
            ////    Role = "AUT",
            ////    Value = author
            ////};
            Date = isoDate;
            Version = version;
            Expression = new Expression
            {
                Language = language,
                Title = title,
            };
        }

        [XmlElement("type_work")]
        public string Type { get; set; }

        [XmlElement("identifier_work")]
        public string Identifier { get; set; }

        [XmlElement("agent_work")]
        public Agent Agent { get; set; }

        ////[XmlElement("agent_person_work")]
        ////public Agent AgentPerson { get; set;}

        [XmlElement("date_work")]
        public string Date { get; set; }

        [XmlElement("version_work")]
        public string Version { get; set; }

        [XmlElement("expression")]
        public Expression Expression { get; set; }
    }
}
