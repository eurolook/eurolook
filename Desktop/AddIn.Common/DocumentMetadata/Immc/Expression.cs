using System.Xml.Serialization;

namespace Eurolook.AddIn.Common.DocumentMetadata.Immc
{
    public class Expression
    {
        [XmlElement("language")]
        public string Language { get; set; }

        [XmlElement("title")]
        public string Title { get; set; }
    }
}
