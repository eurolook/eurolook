using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels;
using Eurolook.Data.TermStore;

namespace Eurolook.AddIn.Common.DocumentMetadata.CalculationCommands
{
    public interface ICalculationCommand
    {
        void CalculateValue(List<IMetadataViewModel> metadataViewModels);

        Task InitAsync(
            IMetadataViewModel targetMetadataViewModel,
            IEurolookDocument eurolookDocument,
            ITermStoreCache termStoreCache);
    }

    public interface ICalculationCommand<in TDocument> : ICalculationCommand
        where TDocument : IEurolookDocument
    {
        Task InitAsync(
            IMetadataViewModel targetMetadataViewModel,
            TDocument eurolookDocument,
            ITermStoreCache termStoreCache);
    }
}
