using System.Collections.Generic;
using Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels;

namespace Eurolook.AddIn.Common.DocumentMetadata.CalculationCommands
{
    public class DefaultCalculationCommandRunner : ICalculationCommandRunner
    {
        public void RefreshAllCalculatedValues(IEurolookDocument eurolookDocument, List<IMetadataViewModel> metadata)
        {
        }
    }
}
