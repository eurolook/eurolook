using System.Collections.Generic;
using Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels;

namespace Eurolook.AddIn.Common.DocumentMetadata.CalculationCommands
{
    public interface ICalculationCommandRunner
    {
        void RefreshAllCalculatedValues(IEurolookDocument eurolookDocument, List<IMetadataViewModel> metadata);
    }
}
