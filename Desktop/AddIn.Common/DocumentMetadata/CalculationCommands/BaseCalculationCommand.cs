using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels;
using Eurolook.Common.Extensions;
using Eurolook.Data.Models.Metadata;
using Eurolook.Data.Models.SharePointTermStore;
using Eurolook.Data.TermStore;

namespace Eurolook.AddIn.Common.DocumentMetadata.CalculationCommands
{
    public abstract class BaseCalculationCommand<TEurolookDocument> : ICalculationCommand<TEurolookDocument>
        where TEurolookDocument : IEurolookDocument
    {
        protected IMetadataViewModel TargetMetadataViewModel { get; private set; }

        protected TEurolookDocument EurolookDocument { get; private set; }

        protected TermStore TermStore { get; private set; }

        public virtual async Task InitAsync(
            IMetadataViewModel targetMetadataViewModel,
            TEurolookDocument eurolookDocument,
            ITermStoreCache termStoreCache)
        {
            TargetMetadataViewModel = targetMetadataViewModel;
            EurolookDocument = eurolookDocument;
            TermStore = await termStoreCache.GetTermStoreAsync();
        }

        async Task ICalculationCommand.InitAsync(
            IMetadataViewModel targetMetadataViewModel,
            IEurolookDocument eurolookDocument,
            ITermStoreCache termStoreCache)
        {
            await InitAsync(targetMetadataViewModel, (TEurolookDocument)eurolookDocument, termStoreCache);
        }

        public abstract void CalculateValue(List<IMetadataViewModel> metadataViewModels);

        protected void CleanAndSetValue(string value)
        {
            string cleanValue = value?.ReplaceAllConsecutiveControlCharacters(" ")?.Trim();
            cleanValue = Regex.Replace(cleanValue ?? "", @"(Type your .*? here\.?)", "<$1>");
            cleanValue = Regex.Replace(cleanValue, @"(xx+)", "<$1>");

            var currentValue = TargetMetadataViewModel.GetMetadataProperty();
            var calculatedValue = new MetadataProperty(currentValue.Name, cleanValue);
            TargetMetadataViewModel.SetCalculatedValue(calculatedValue);
            TargetMetadataViewModel.SetCalculatedValueToolTip("Fill-in this value retrieved from the document: " + cleanValue);

            // automatically fill or update if read-only or init
            if (TargetMetadataViewModel.IsReadOnly || string.IsNullOrEmpty(currentValue.GetSimpleValue()))
            {
                TargetMetadataViewModel.SetValue(calculatedValue);
            }
        }
    }
}
