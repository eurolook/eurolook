using System.Collections.Generic;
using System.Linq;
using Eurolook.AddIn.Common.DocumentMetadata.BuiltInProperties;
using Eurolook.AddIn.Common.DocumentMetadata.SharePoint;
using Eurolook.Data.Models.Metadata;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.DocumentMetadata
{
    public class MetadataReaderWriter : IMetadataReaderWriter
    {
        private bool _isContentTypeSchemaRestored;

        public MetadataReaderWriter(
            IContentTypeProperties contentTypeProperties,
            IBuiltInProperties builtInProperties)
        {
            ContentTypeProperties = contentTypeProperties;
            BuiltInProperties = builtInProperties;
        }

        protected IContentTypeProperties ContentTypeProperties { get; }

        protected IBuiltInProperties BuiltInProperties { get; }

        protected IEurolookDocument EurolookDocument { get; private set; }

        public void Init(IEurolookDocument eurolookDocument)
        {
            EurolookDocument = eurolookDocument;
            ContentTypeProperties.Init(eurolookDocument);
            BuiltInProperties.Init(eurolookDocument);
            _isContentTypeSchemaRestored = ContentTypeProperties.IsContentTypeSchemaRestored();
        }

        public virtual MetadataProperty ReadMetadataProperty(MetadataDefinition metadataDefinition)
        {
            string propertyName = metadataDefinition.Name;
            if (!_isContentTypeSchemaRestored && ContentTypeProperties.IsPropertyDefined(propertyName))
            {
                return ContentTypeProperties.GetPropertyContent(metadataDefinition);
            }

            if (BuiltInProperties.IsPropertyDefined(propertyName))
            {
                return BuiltInProperties.GetPropertyContent(propertyName);
            }

            MetadataProperty property = null;
            EurolookDocument.GetEurolookProperties()?.DocumentMetadata?.TryGetValue(propertyName, out property);
            return property;
        }

        public virtual void WriteMetadata(Dictionary<MetadataDefinition, MetadataProperty> metadataDictionary)
        {
            var customEurolookMetadata = new Dictionary<string, MetadataProperty>();
            foreach (var metadataDefinition in metadataDictionary.Keys)
            {
                var metadataProperty = metadataDictionary[metadataDefinition];
                string propertyName = metadataProperty.Name;
                if (ContentTypeProperties.IsPropertyDefined(propertyName))
                {
                    ContentTypeProperties.WritePropertyContent(metadataDefinition, metadataProperty);
                    customEurolookMetadata.Add(propertyName, metadataProperty);
                }
                else if (BuiltInProperties.IsPropertyDefined(propertyName))
                {
                    BuiltInProperties.WritePropertyContent(metadataProperty);
                }
                else
                {
                    customEurolookMetadata.Add(propertyName, metadataProperty);
                }
            }

            EurolookDocument.SetEurolookProperties(prop => prop.DocumentMetadata = customEurolookMetadata);
        }

        public virtual void ReplaceMetadataProperty(
            MetadataDefinition metadataDefinition,
            MetadataProperty metadataProperty)
        {
            string propertyName = metadataProperty.Name;
            if (ContentTypeProperties.IsPropertyDefined(propertyName))
            {
                ContentTypeProperties.WritePropertyContent(metadataDefinition, metadataProperty);
            }
            else if (BuiltInProperties.IsPropertyDefined(propertyName))
            {
                BuiltInProperties.WritePropertyContent(metadataProperty);
            }
            else
            {
                var metadata = EurolookDocument.GetEurolookProperties().DocumentMetadata;
                metadata[propertyName] = metadataProperty;
                EurolookDocument.SetEurolookProperties(prop => prop.DocumentMetadata = metadata);
            }
        }

        [CanBeNull]
        public MetadataDefinition GetMetadataDefinition(string metadataName)
        {
            var metadataDefinitions = EurolookDocument.DocumentModel.DocumentModelMetadataDefinitions;
            var documentModelMetadataDefinition =
                metadataDefinitions?.FirstOrDefault(md => md.MetadataDefinition.Name == metadataName);
            return documentModelMetadataDefinition?.MetadataDefinition;
        }
    }
}
