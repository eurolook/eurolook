using System.Globalization;
using System.Threading.Tasks;
using Eurolook.Data.Models;

namespace Eurolook.AddIn.Common.DocumentMetadata
{
    public interface IMetadataFormatProvider
    {
        DateTimeFormatInfo GetDateTimeFormat(Language language);

        Task InitAsync();
    }
}
