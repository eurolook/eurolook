﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Eurolook.AddIn.Common.AddIn;
using Eurolook.Common.Log;
using Eurolook.Data;
using Microsoft.Win32;

namespace Eurolook.AddIn.Common
{
    public abstract class AddinContextBase : IAddinContext, ICanLog
    {
        private readonly IDisabledAddInsManager _disabledAddInsManager;

        protected AddinContextBase(IDisabledAddInsManager disabledAddInsManager)
        {
            _disabledAddInsManager = disabledAddInsManager;
        }

        public string ProductCustomizationId => ConfigurationInfo.Instance.ProductCustomizationId;

        public Version ClientVersion => ConfigurationInfo.Instance.ClientVersion;

        public string InstallRoot
        {
            get { return Path.GetDirectoryName(typeof(AddinContextBase).Assembly.Location) ?? ""; }
        }

        public Version OfficeVersion
        {
            get
            {
                // NOTE: do not use Application.Version because it does not include major/minor revision
                //  i.e. it is only of the form aa.0 and we want aa.bb.cc.dd
                var process = Process.GetCurrentProcess();
                string fileVersion = process.MainModule?.FileVersionInfo.FileVersion;
                return fileVersion != null ? new Version(fileVersion) : new Version(0, 0, 0, 0);
            }
        }

        public bool IsPerUserInstall
        {
            get { return ProgIdKeyExists(Registry.CurrentUser); }
        }

        public bool IsPerMachineInstall
        {
            get { return ProgIdKeyExists(Registry.LocalMachine); }
        }

        /// <summary>
        /// Returns true if the current application has focus, false otherwise.
        /// </summary>
        public static bool IsApplicationInForeground()
        {
            var foregroundWindow = SafeNativeMethods.GetForegroundWindow();
            if (foregroundWindow == IntPtr.Zero)
            {
                return false; // No window is currently activated
            }

            int currentProcessId = Process.GetCurrentProcess().Id;
            SafeNativeMethods.GetWindowThreadProcessId(foregroundWindow, out uint foregroundWindowProcessId);

            return foregroundWindowProcessId == currentProcessId;
        }

        public abstract IEnumerable<TimeSpan> LoadTimes();

        public void ClearDisabledAddins()
        {
            if (_disabledAddInsManager == null)
            {
                this.LogWarn("Disabled add-ins are not cleaned.");
                return;
            }

            this.LogInfo("Checking for disabled add-ins");
            var disabledItems = _disabledAddInsManager.GetDisabledItems().ToList();
            foreach (var disabledItem in disabledItems)
            {
                this.LogDebug($"Disabled: {disabledItem}");
            }

            var enabledItems = _disabledAddInsManager.Enable(
                d => d.Path?.IndexOf("eurolook", StringComparison.InvariantCultureIgnoreCase) >= 0);
            foreach (var enabledItem in enabledItems)
            {
                this.LogInfo($"Re-enabled: {enabledItem}");
            }
        }

        protected abstract string GetAddInProgId();

        protected string GetAddInShimProgId() => ComIdentifiers.AddInShim.ProgId;

        private bool ProgIdKeyExists(RegistryKey hive)
        {
            string key = @"Software\Classes\" + GetAddInShimProgId();

            using (var progIdKey = hive.OpenSubKey(key))
            {
                return progIdKey != null;
            }
        }
    }
}
