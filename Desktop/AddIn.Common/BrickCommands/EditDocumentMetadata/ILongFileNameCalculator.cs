﻿using System.Threading.Tasks;

namespace Eurolook.AddIn.Common.BrickCommands.EditDocumentMetadata
{
    public interface ILongFileNameCalculator
    {
        Task<string> GetLongFileNameAsync(IEurolookDocument eurolookDocument);

        Task<string> GetLongFileNameInClipboardFormatAsync(IEurolookDocument eurolookDocument);
    }
}
