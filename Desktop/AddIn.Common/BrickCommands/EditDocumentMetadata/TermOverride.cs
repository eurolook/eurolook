﻿using System;
using System.Collections.Generic;

namespace Eurolook.AddIn.Common.BrickCommands.EditDocumentMetadata
{
    public class TermOverride
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string DefaultAcronym { get; set; }

        public bool? IsExcluded { get; set; }

        public bool? IsExpanded { get; set; }

        public Dictionary<string, string> DefaultLabelPerLanguage { get; set; }
    }
}
