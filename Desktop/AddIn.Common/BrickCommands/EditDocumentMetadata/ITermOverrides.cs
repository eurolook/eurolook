﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data.Models.SharePointTermStore;

namespace Eurolook.AddIn.Common.BrickCommands.EditDocumentMetadata
{
    /// <summary>
    /// An interface providing modified terms for UI and LFN.
    /// </summary>
    /// <remarks>
    /// The terms provided by the term store should be used as is when stored as metadata in the document. However,
    /// in certain places, it is necessary to use slightly different value, e.g. the acronyms used in the LFN or labels
    /// shown in the UI. It might also be necessary to use different properties, e.g. to hide certain terms from the UI.
    /// The term overrides are configured with the LFN brick and can be more easily updated than the Term Store.
    /// </remarks>
    public interface ITermOverrides
    {
        Task InitAsync(IEurolookDocument eurolookDocument);

        Dictionary<Guid, TermOverride> TermOverridesByTermId { get; }

        Term GetTermOverride(Term term);
    }
}
