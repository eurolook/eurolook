using System;
using System.Windows;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.Common.Log;

namespace Eurolook.AddIn.Common.BrickCommands.EditDocumentMetadata
{
    public partial class EditDocumentMetadataWindow : ICanLog
    {
        public EditDocumentMetadataWindow()
        {
            InitializeComponent();
            SourceInitialized += OnSourceInitialized;
        }

        public EditDocumentMetadataWindow(EditDocumentMetadataViewModel viewModel)
            : this()
        {
            ViewModel = viewModel;
            DataContext = ViewModel;
        }

        public EditDocumentMetadataViewModel ViewModel { get; set; }

        private void OnOkClick(object sender, RoutedEventArgs e)
        {
            try
            {
                DialogResult = true; // will close the dialog automatically
            }
            catch (InvalidOperationException)
            {
                // when not opened as dialog
                Close();
            }
        }

        private void OnSourceInitialized(object sender, EventArgs e)
        {
            try
            {
                this.DisableMinimizeButton();
                this.DisableMaximizeButton();
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }
    }
}
