﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Xml.Linq;
using System.Xml.Serialization;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.DocumentMetadata;
using Eurolook.AddIn.Common.DocumentMetadata.CalculationCommands;
using Eurolook.AddIn.Common.DocumentMetadata.Immc;
using Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels;
using Eurolook.AddIn.Common.DocumentMetadata.SharePoint;
using Eurolook.AddIn.Common.DocumentMetadata.TermPicker;
using Eurolook.Common;
using Eurolook.Common.AsyncCommands;
using Eurolook.Common.Log;
using Eurolook.Data.Constants;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.Data.Models.Metadata;
using Eurolook.Data.TermStore;
using Eurolook.Data.Xml;
using Eurolook.Data.Xml.Metadata;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;
using Newtonsoft.Json;

namespace Eurolook.AddIn.Common.BrickCommands.EditDocumentMetadata
{
    public class EditDocumentMetadataViewModel : ViewModelBase, IDisposable, ICanLog
    {
        private const string MetadataClipboardFormat = "Eurolook:Clipboard:Metadata";

        private readonly IUserDataRepository _userDataRepository;
        private readonly IUserGroupRepository _userGroupRepository;
        private readonly IMetadataViewModelFactory _metadataViewModelFactory;
        private readonly ITermStoreCache _termStoreCache;
        private readonly ITermOverrides _termOverrides;
        private readonly IMetadataReaderWriter _metadataReaderWriter;
        private readonly IMetadataXmlReaderWriter _metadataXmlReaderWriter;
        private readonly ILongFileNameCalculator _longFileNameCalculator;
        private readonly IMetadataToImmcMapper _metadataToImmcMapper;
        private readonly IClassNameResolver<ICalculationCommand> _calculationCommandResolver;
        private readonly IMessageService _messageService;
        private readonly IMetadataFormatProvider _metadataFormatProvider;
        private readonly ITermFilter _termFilter;
        private readonly IContentTypeProperties _contentTypeProperties;
        private readonly IMetadataPropertyFactory _metadataPropertyFactory;
        private readonly Dictionary<string, string> _errors;
        private readonly ICalculationCommandRunner _calculationCommandRunner;
        private readonly IMetadataPropertyValidator _metadataPropertyValidator;
        private List<IMetadataViewModel> _calculatedMetadata;

        private IEurolookDocument _eurolookDocument;
        private bool _validateMetadata;

        public EditDocumentMetadataViewModel(
            IUserDataRepository userDataRepository,
            IUserGroupRepository userGroupRepository,
            IMetadataViewModelFactory metadataViewModelFactory,
            ITermStoreCache termStoreCache,
            ITermOverrides termOverrides,
            ITermFilter termFilter,
            IContentTypeProperties contentTypeProperties,
            IMetadataPropertyFactory metadataPropertyFactory,
            IMetadataReaderWriter metadataReaderWriter,
            IMetadataXmlReaderWriter metadataXmlReaderWriter,
            IMetadataFormatProvider metadataFormatProvider,
            ILongFileNameCalculator longFileNameCalculator,
            IMetadataToImmcMapper metadataToImmcMapper,
            IMetadataPropertyValidator metadataPropertyValidator,
            ICalculationCommandRunner calculationCommandRunner,
            IClassNameResolver<ICalculationCommand> calculationCommandResolver,
            IMessageService messageService)
        {
            _userDataRepository = userDataRepository;
            _userGroupRepository = userGroupRepository;
            _metadataViewModelFactory = metadataViewModelFactory;
            _termStoreCache = termStoreCache;
            _termOverrides = termOverrides;
            _termFilter = termFilter;
            _contentTypeProperties = contentTypeProperties;
            _metadataPropertyFactory = metadataPropertyFactory;
            _metadataReaderWriter = metadataReaderWriter;
            _metadataXmlReaderWriter = metadataXmlReaderWriter;
            _metadataFormatProvider = metadataFormatProvider;
            _longFileNameCalculator = longFileNameCalculator;
            _metadataToImmcMapper = metadataToImmcMapper;
            _metadataPropertyValidator = metadataPropertyValidator;
            _calculationCommandRunner = calculationCommandRunner;
            _calculationCommandResolver = calculationCommandResolver;
            _messageService = messageService;
            _errors = new Dictionary<string, string>();
        }

        ~EditDocumentMetadataViewModel()
        {
            Dispose(false);
        }

        public TermPickerViewModel TermPickerViewModel { get; set; }

        public List<IMetadataViewModel> Metadata { get; set; }

        public string WindowTitle { get; private set; }

        public bool IsValid => _errors.All(kvp => kvp.Value == null);

        public AsyncCommand CopyToClipboardCommand => new AsyncCommand(
            CopyToClipboardAsync,
            errorHandler: this.LogError);

        public ICommand PasteFromClipboardCommand => new RelayCommand(PasteFromClipboard);

        public AsyncCommand ExportMetadataToImmcXmlCommand => new AsyncCommand(
            ExportMetadataToImmcXmlAsync,
            errorHandler: this.LogError);

        public async Task InitAsync(
            string windowTitle,
            IEurolookDocument eurolookDocument,
            bool validateMetadata = false)
        {
            WindowTitle = windowTitle;

            _eurolookDocument = eurolookDocument;
            _validateMetadata = validateMetadata;
            _metadataReaderWriter.Init(eurolookDocument);
            _contentTypeProperties.Init(_eurolookDocument);
            await _termOverrides.InitAsync(_eurolookDocument);

            var termStore = await _termStoreCache.GetTermStoreAsync();

            await _metadataFormatProvider.InitAsync();
            var language = eurolookDocument.Language;

            await _metadataPropertyFactory.InitAsync(language.CultureInfo.LCID);

            var viewModels = await Task.WhenAll(
                eurolookDocument.DocumentModel.DocumentModelMetadataDefinitions.Select(
                    async dmmd => await CreateViewModel(dmmd, language)));

            Metadata = viewModels.Where(vm => vm != null)
                                 .OrderBy(vm => vm.MetadataDefinition.Position)
                                 .ThenBy(vm => vm.DisplayName)
                                 .ToList();

            _calculatedMetadata = Metadata.Where(m => m.CalculationCommand != null).ToList();
            _calculationCommandRunner.RefreshAllCalculatedValues(_eurolookDocument, Metadata);

            int languageCode = language.CultureInfo.TextInfo.LCID;
            TermPickerViewModel = new TermPickerViewModel(termStore, _termFilter, _termOverrides, languageCode);
        }

        public bool Validate()
        {
            foreach (var metadataViewModel in Metadata)
            {
                var property = metadataViewModel.GetMetadataProperty();
                var result = ValidationFunction(property);
            }

            return IsValid;
        }

        public void WriteMetadataToDocument()
        {
            AssertCustomXmlPartsExist();

            var metadataPropertiesDictionary = Metadata.ToDictionary(
                vm => vm.MetadataDefinition,
                vm => vm.GetMetadataProperty());
            _metadataReaderWriter.WriteMetadata(metadataPropertiesDictionary);
        }

        private void AssertCustomXmlPartsExist()
        {
            var document = _eurolookDocument.OfficeDocumentWrapper;
            var eurolookPropertiesMissing = document.GetCustomXml(EurolookPropertiesCustomXml.RootName) == null;
            var contentTypeSchemaMissing =
                document.CustomXMLParts.SelectByNamespace(Namespaces.ContentTypeSchemaNs.NamespaceName)?.Count == 0;
            var contentTypePropertiesMissing =
                document.CustomXMLParts.SelectByNamespace(Namespaces.ContentTypePropertiesNs.NamespaceName)?.Count == 0;

            if (!eurolookPropertiesMissing && !contentTypeSchemaMissing && !contentTypePropertiesMissing)
            {
                return;
            }

            if (eurolookPropertiesMissing)
            {
                var propertiesCustomXml = _eurolookDocument.GetEurolookProperties();
                if (propertiesCustomXml != null)
                {
                    document.CustomXMLParts.Add(propertiesCustomXml.ToXDocument().ToString());
                }
            }

            if (contentTypeSchemaMissing)
            {
                var contentTypeSchemaXml = _contentTypeProperties.GetContentTypeSchemaXmlFromTemplate();
                if (contentTypeSchemaXml != null)
                {
                    document.CustomXMLParts.Add(contentTypeSchemaXml.ToString());
                }
            }

            if (contentTypePropertiesMissing)
            {
                var contentTypePropertiesXml = _contentTypeProperties.GetContentTypePropertiesFromTemplate();
                if (contentTypePropertiesXml != null)
                {
                    document.CustomXMLParts.Add(contentTypePropertiesXml.ToString());
                }
            }

            _metadataReaderWriter.Init(_eurolookDocument);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private async Task<IMetadataViewModel> CreateViewModel(DocumentModelMetadataDefinition dmmd, Language language)
        {
            var metadataDefinition = dmmd.MetadataDefinition;

            var viewModel = _metadataViewModelFactory.CreateViewModel(metadataDefinition);
            if (viewModel == null)
            {
                return null;
            }

            viewModel.IsReadOnly = IsReadOnlyForCurrentUser(dmmd);
            viewModel.IsHidden = dmmd.IsHidden;
            viewModel.IsMandatory = dmmd.IsMandatory;
            viewModel.MaxLength = dmmd.MaxLength;
            viewModel.MinLength = dmmd.MinLength;

            viewModel.DocumentLanguage = language;
            viewModel.MetadataReaderWriter = _metadataReaderWriter;

            var property = _metadataReaderWriter.ReadMetadataProperty(metadataDefinition)
                ?? _metadataPropertyFactory.Create(dmmd.MetadataDefinition, dmmd.DefaultValue);
            if (property != null)
            {
                viewModel.SetValue(property);
            }

            if (_validateMetadata && _metadataPropertyValidator.IsPropertyValidated(metadataDefinition.Name, _eurolookDocument))
            {
                viewModel.SetValidationFunction(ValidationFunction);
            }

            viewModel.CalculationCommand = _calculationCommandResolver.Resolve(dmmd.CalculationCommandClassName);
            if (viewModel.CalculationCommand == null)
            {
                viewModel.SetRefreshCalculationFunction(
                    () => _calculationCommandRunner.RefreshAllCalculatedValues(_eurolookDocument, Metadata));
            }
            else
            {
                await viewModel.CalculationCommand.InitAsync(viewModel, _eurolookDocument, _termStoreCache);
            }

            return viewModel;
        }

        private bool IsReadOnlyForCurrentUser(DocumentModelMetadataDefinition dmmd)
        {
            if (!dmmd.IsReadOnly)
            {
                return false;
            }

            var allowedGroupsAsString = dmmd.IsReadOnlyOverride == true ? dmmd.EditableByUserGroups : dmmd.MetadataDefinition.EditableByUserGroups;
            if (string.IsNullOrWhiteSpace(allowedGroupsAsString))
            {
                return true;
            }

            var user = _userDataRepository.GetUser();
            var allowedGroups = allowedGroupsAsString.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            foreach (var group in allowedGroups)
            {
                bool validGuid = Guid.TryParse(group, out var id);
                if (!validGuid)
                {
                    this.LogError($"User group id '{group}' is not a valid Guid and could not be parsed.");
                    continue;
                }

                if (_userGroupRepository.IsMemberOfUserGroupOrMappedAdGroup(user, id))
                {
                    return false;
                }
            }

            return true;
        }

        private string ValidationFunction(MetadataProperty metadataProperty)
        {
            _errors[metadataProperty.Name] = _metadataPropertyValidator.Validate(metadataProperty, _eurolookDocument);
            RaisePropertyChanged(() => IsValid);

            return _errors[metadataProperty.Name];
        }

        private async Task CopyToClipboardAsync()
        {
            WriteMetadataToDocument();
            string lfn = await _longFileNameCalculator.GetLongFileNameInClipboardFormatAsync(_eurolookDocument);

            var metadataRoot = new XElement("Metadata");
            var metadataPropertiesDictionary = Metadata.Select(m => m.GetMetadataProperty()).ToDictionary(p => p.Name);
            _metadataXmlReaderWriter.WriteMetadata(metadataRoot, metadataPropertiesDictionary);

            var dataObject = new DataObject();
            dataObject.SetText(lfn);
            dataObject.SetData(MetadataClipboardFormat, JsonConvert.SerializeObject(metadataRoot, Formatting.None));

            Clipboard.SetDataObject(dataObject);
        }

        private void PasteFromClipboard()
        {
            try
            {
                if (Clipboard.ContainsData(MetadataClipboardFormat))
                {
                    HandleMetadataPaste();
                    return;
                }

                bool isOldLfnInClipboard = Clipboard.GetText().Count(c => c == '|') == 14;
                if (isOldLfnInClipboard)
                {
                    _messageService.ShowSimpleMessage(
                        "Old LFN clipboard format is not supported.",
                        "Paste unsuccessful",
                        this);
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private void HandleMetadataPaste()
        {
            string clipBoardData = Clipboard.GetData(MetadataClipboardFormat) as string;
            var metadataRoot = JsonConvert.DeserializeObject<XElement>(clipBoardData);
            var metadataPropertiesDictionary = _metadataXmlReaderWriter.ReadMetadata(metadataRoot);
            foreach (var metadataViewModel in Metadata.Where(vm => !vm.IsReadOnly && !vm.IsHidden))
            {
                if (metadataPropertiesDictionary.TryGetValue(
                    metadataViewModel.MetadataDefinition.Name,
                    out var metadataProperty))
                {
                    if (metadataViewModel.GetMetadataProperty().ToString() != metadataProperty.ToString())
                    {
                        metadataViewModel.SetValue(metadataProperty);
                    }
                }
            }
        }

        private async Task ExportMetadataToImmcXmlAsync()
        {
            var dialog = new SaveFileDialog
            {
                Filter = "XML File (*.xml)|*.xml",
                AddExtension = true,
                DefaultExt = "xml",
                Title = "Select File for Export",
                FileName = "immc-export.xml",
            };

            if (dialog.ShowDialog() != true)
            {
                return;
            }

            WriteMetadataToDocument();
            var immcWork = await _metadataToImmcMapper.GetImmcWorkAsync(_eurolookDocument, Metadata.ToList());

            var serializer = new XmlSerializer(immcWork.GetType());
            using (var writer = new StreamWriter(dialog.FileName))
            {
                serializer.Serialize(writer, immcWork);
            }
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                TermPickerViewModel?.Dispose();
            }
        }
    }
}
