namespace Eurolook.AddIn.Common.BrickCommands.Configuration
{
    public interface IBrickConfigurationReader
    {
        T Read<T>(string json);
    }
}
