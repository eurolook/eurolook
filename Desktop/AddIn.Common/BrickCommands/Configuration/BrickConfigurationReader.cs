using System.Linq;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;

namespace Eurolook.AddIn.Common.BrickCommands.Configuration
{
    public class BrickConfigurationReader : IBrickConfigurationReader
    {
        [CanBeNull]
        public T Read<T>(string json)
        {
            if (string.IsNullOrEmpty(json))
            {
                return default;
            }

            var jObject = JObject.Parse(json);
            var allConfigurations = jObject["Configurations"];
            if (allConfigurations != null)
            {
                // JSON contains "Configurations" root
                return ReadTypedConfiguration<T>(allConfigurations);
            }

            // JSON is simply the configuration object
            return ReadPlainJsonObject<T>(jObject);
        }

        private static T ReadPlainJsonObject<T>(JObject jObject)
        {
            return jObject.ToObject<T>();
        }

        private static T ReadTypedConfiguration<T>(JToken allConfigurations)
        {
            var configuration =
                allConfigurations.FirstOrDefault(c => c["SerializationType"]?.Value<string>() == typeof(T).FullName)
                ?? allConfigurations.FirstOrDefault(c => c["SerializationType"]?.Value<string>() == typeof(T).Name);
            if (configuration == null)
            {
                return default;
            }

            var versionedBrickConfiguration = configuration.ToObject<TypedBrickConfiguration<T>>();
            return versionedBrickConfiguration.Configuration;
        }
    }
}
