namespace Eurolook.AddIn.Common.BrickCommands.Configuration
{
    public class TypedBrickConfiguration<T>
    {
        public string SerializationType { get; set; }

        public T Configuration { get; set; }
    }
}
