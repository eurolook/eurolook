﻿using System.Threading.Tasks;
using Eurolook.AddIn.Common.ProfileInitializer;
using Microsoft.Office.Core;

namespace Eurolook.AddIn.Common.AddIn
{
    public interface IOfficeAddInBase
    {
        void OnStartup(object application);

        Task OnStartupLongRunningAsync(IProfileInitializer profileInitializer);

        void OnShutdown();

        void OnTaskPaneFactoryAvailable(ICTPFactory ctpFactoryInst);

        object RequestComAddInAutomationService();
    }
}
