using System.Runtime.InteropServices;

namespace Eurolook.AddIn.Common.AddIn
{
    /// <summary>
    /// This interface is implemented by the managed aggregator - the single
    /// method is a wrapper around Marshal.CreateAggregatedObject, which can be
    /// called from unmanaged code (that is, called from the shim).
    /// </summary>
    [ComImport]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [Guid("142A261B-1550-4849-B109-715AA4629A14")]
    internal interface IManagedAggregator
    {
        void CreateAggregatedInstance(string assemblyName, string typeName, IComAggregator outerObject);
    }
}
