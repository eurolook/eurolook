﻿namespace Eurolook.AddIn.Common.AddIn;

public static class ComIdentifiers
{
    public static class AddInShim
    {
        public const string ProgId = "Eurolook.OfficeAddIn.10";

        public static string ClsId = "2AE023F6-3AC9-3258-AC27-8DD3EBBAA828";
    }

    public static class ManagedAddIn
    {
        public const string ProgId = "Eurolook.OfficeAddIn.10";

        public const string ClsId = "6D6712AD-D134-4141-BE61-FF4332D2ACB4";
    }

    public static class TaskPaneContentControl
    {
        public const string ProgId = "Eurolook.OfficeAddIn.TaskPaneContentControl.10";

        public const string ClsId = "6E1C0429-44EE-4780-96D3-4DFD3A218D25";
    }

    public static class ComAddInAutomationService
    {
        public const string ProgId = "Eurolook.OfficeAddIn.ComAddInAutomationService.10";

        public const string ClsId = "4C938F51-5DC8-40F1-B87C-78615BB58196";
    }
}
