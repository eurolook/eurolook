using System;
using System.Collections.Generic;

namespace Eurolook.AddIn.Common.AddIn
{
    public interface IDisabledAddInsManager
    {
        IEnumerable<DisabledAddInsManager.DisabledItem> GetDisabledItems();

        void Disable(DisabledAddInsManager.DisabledItem disabledItem);

        /// <summary>
        /// Re-enables all add-ins matching the given criteria.
        /// </summary>
        /// <param name="predicate">A predicate specifying which items to re-enable.</param>
        /// <returns>Returns an enumerable containing the items that were (re-)enabled.</returns>
        IEnumerable<DisabledAddInsManager.DisabledItem> Enable(Func<DisabledAddInsManager.DisabledItem, bool> predicate);
    }
}
