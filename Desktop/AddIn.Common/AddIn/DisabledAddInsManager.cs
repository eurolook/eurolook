using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Eurolook.Common.Log;
using JetBrains.Annotations;
using Microsoft.Win32;

namespace Eurolook.AddIn.Common.AddIn
{
    /// <summary>
    /// A class to manage and re-enable add-ins disabled by Word.
    /// </summary>
    public class DisabledAddInsManager : IDisabledAddInsManager, ICanLog
    {
        public IEnumerable<DisabledItem> GetDisabledItems()
        {
            using (var regKeyOffice = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Office"))
            {
                if (regKeyOffice == null)
                {
                    yield break;
                }

                foreach (string subKeyName in regKeyOffice.GetSubKeyNames())
                {
                    if (!Regex.IsMatch(subKeyName, @"\d+\.\d+"))
                    {
                        continue;
                    }

                    string key = $@"Software\Microsoft\Office\{subKeyName}\Word\Resiliency\DisabledItems";
                    using (var regKey = Registry.CurrentUser.OpenSubKey(key, true))
                    {
                        if (regKey == null)
                        {
                            continue;
                        }

                        foreach (string valueName in regKey.GetValueNames())
                        {
                            var data = (byte[])regKey.GetValue(valueName);
                            var disabledItem = Parse(data);
                            if (disabledItem == null)
                            {
                                continue;
                            }

                            disabledItem.OfficeVersion = subKeyName;
                            disabledItem.Key = valueName;

                            yield return disabledItem;
                        }
                    }
                }
            }
        }

        public void Disable(DisabledItem disabledItem)
        {
            if (string.IsNullOrWhiteSpace(disabledItem.OfficeVersion))
            {
                throw new Exception("No office version set");
            }

            using (var regRoot = Registry.CurrentUser.OpenSubKey(
                $@"Software\Microsoft\Office\{disabledItem.OfficeVersion}",
                true))
            {
                if (regRoot == null)
                {
                    throw new Exception("Registry key not found.");
                }

                using (var regKey = regRoot.CreateSubKey(@"Word\Resiliency\DisabledItems", true))
                {
                    var rnd = new Random();
                    string itemKey = rnd.Next(0xA000000, int.MaxValue).ToString("X");
                    while (regKey.GetValueNames().Any(
                        v => string.Equals(v, itemKey, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        itemKey = rnd.Next(0xA000000, int.MaxValue).ToString("X");
                    }

                    regKey.SetValue(itemKey, ToArray(disabledItem));
                }
            }
        }

        /// <summary>
        /// Re-enables all add-ins matching the giving criteria.
        /// </summary>
        /// <param name="predicate">A predicate specifying which items to re-enable.</param>
        /// <returns>Returns an enumerable containing the items that were (re-)enabled.</returns>
        public IEnumerable<DisabledItem> Enable(Func<DisabledItem, bool> predicate)
        {
            var disabledItems = GetDisabledItems().Where(predicate);

            var result = new List<DisabledItem>();

            foreach (var disabledItem in disabledItems)
            {
                string key = $@"Software\Microsoft\Office\{disabledItem.OfficeVersion}\Word\Resiliency\DisabledItems";
                using (var regKey = Registry.CurrentUser.OpenSubKey(key, true))
                {
                    if (regKey == null)
                    {
                        continue;
                    }

                    regKey.DeleteValue(disabledItem.Key);
                    result.Add(disabledItem);
                }
            }

            return result;
        }

        private static byte[] ToArray(DisabledItem disabledItem)
        {
            using (var stream = new MemoryStream())
            {
                using (var writer = new BinaryWriter(stream, Encoding.Unicode))
                {
                    writer.Write((int)disabledItem.Type);

                    var pathBytes = disabledItem.Type == DisabledItem.DisabledAddInType.Previewer
                        ? disabledItem.ClsId.ToByteArray()
                        : Encoding.Unicode.GetBytes(disabledItem.Path + '\0');

                    var descriptionBytes = disabledItem.Type == DisabledItem.DisabledAddInType.Document
                        ? Encoding.Unicode.GetBytes(disabledItem.Description + "\0\0")
                        : Encoding.Unicode.GetBytes(disabledItem.Description + "\0");

                    writer.Write(pathBytes.Length);
                    writer.Write(descriptionBytes.Length);
                    writer.Write(pathBytes);
                    writer.Write(descriptionBytes);
                }

                return stream.ToArray();
            }
        }

        [CanBeNull]
        private DisabledItem Parse(byte[] value)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            try
            {
                using (var stream = new MemoryStream(value))
                {
                    using (var reader = new BinaryReader(stream, Encoding.Unicode))
                    {
                        // first 4 bytes contain an int describing the type
                        var type = (DisabledItem.DisabledAddInType)reader.ReadInt32();
                        int pathLength = reader.ReadInt32();
                        int descriptionLength = reader.ReadInt32();

                        var pathBytes = reader.ReadBytes(pathLength);
                        var nameBytes = reader.ReadBytes(descriptionLength);

                        var result = new DisabledItem
                        {
                            Type = type,
                            Description = string.Concat(Encoding.Unicode.GetString(nameBytes).TakeWhile(x => x != 0)),
                        };

                        switch (type)
                        {
                            case DisabledItem.DisabledAddInType.AddIn:
                            case DisabledItem.DisabledAddInType.Document:
                                result.Path = string.Concat(
                                    Encoding.Unicode.GetString(pathBytes).TakeWhile(x => x != 0));
                                break;
                            case DisabledItem.DisabledAddInType.TaskPane:
                            case DisabledItem.DisabledAddInType.Unknown6:
                            case DisabledItem.DisabledAddInType.Unknown8:
                                // unclear what to parse here
                                break;
                            case DisabledItem.DisabledAddInType.Previewer:
                                result.ClsId = new Guid(pathBytes);
                                break;
                            default:
                                throw new ArgumentOutOfRangeException($"Info: Unknown disabled add-in type {type} for add-in {result.Description}");
                        }

                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                return null;
            }
        }

        public class DisabledItem
        {
            public enum DisabledAddInType
            {
                AddIn = 1,
                Document = 2,
                TaskPane = 3,
                Previewer = 4,
                Unknown5 = 5,
                Unknown6 = 6, // could be spell checker?
                Unknown7 = 7,
                Unknown8 = 8, // could be Web add-in?
            }

            public DisabledAddInType Type { get; set; }

            public string Path { get; set; }

            public Guid ClsId { get; set; }

            public string Description { get; set; }

            public string OfficeVersion { get; set; } = "16.0";

            public string Key { get; set; }

            public override string ToString()
            {
                string separator = string.IsNullOrEmpty(Description) ? "" : " - ";

                return Type == DisabledAddInType.Previewer
                    ? $"{Type}: {ClsId:B}{separator}{Description}"
                    : $"{Type}: {Path}{separator}{Description}";
            }
        }
    }
}
