﻿using Autofac;
using Eurolook.AddIn.Common.Ribbon;
using JetBrains.Annotations;
using Microsoft.Office.Core;

namespace Eurolook.AddIn.Common.AddIn
{
    public interface IOfficeAddIn : IOfficeAddInBase, ICustomTaskPaneConsumer
    {
        IRibbonService RibbonService { get; }

        [CanBeNull]
        IRibbonCallbackHandler RibbonCallbackHandler { get; }

        IContainer SetupContainer();
    }
}
