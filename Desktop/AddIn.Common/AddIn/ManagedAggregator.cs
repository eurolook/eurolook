using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using JetBrains.Annotations;

namespace Eurolook.AddIn.Common.AddIn
{
    // The unmanaged shim will instantiate this object in order to call
    // through to Marshal.CreateAggregatedObject.
    [ClassInterface(ClassInterfaceType.None)]
    [ProgId("ManagedHelpers.ManagedAggregator")]
    [UsedImplicitly]
    internal class ManagedAggregator : IManagedAggregator
    {
        public void CreateAggregatedInstance(string assemblyName, string typeName, IComAggregator outerObject)
        {
            var pOuter = IntPtr.Zero;
            var pInner = IntPtr.Zero;

            try
            {
                Trace.WriteLine(
                    $"Eurolook.WordAddIn.AddIn.ManagedAggregator.CreateAggregatedInstance - AssemblyName {assemblyName}, Type {typeName}");

                // We use Marshal.CreateAggregatedObject to create a CCW where
                // the inner object (the target managed add-in) is aggregated
                // with the supplied outer object (the shim).
                pOuter = Marshal.GetIUnknownForObject(outerObject);
                object innerObject = AppDomain.CurrentDomain.CreateInstanceAndUnwrap(assemblyName, typeName);
                pInner = Marshal.CreateAggregatedObject(pOuter, innerObject);

                // Make sure the shim has a pointer to the add-in.
                outerObject.SetInnerPointer(pInner);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
            finally
            {
                if (pOuter != IntPtr.Zero)
                {
                    Marshal.Release(pOuter);
                }

                if (pInner != IntPtr.Zero)
                {
                    Marshal.Release(pInner);
                }

                Marshal.ReleaseComObject(outerObject);
            }
        }
    }
}
