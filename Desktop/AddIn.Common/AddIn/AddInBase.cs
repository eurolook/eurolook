﻿using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using Eurolook.AddIn.Common.ProfileInitializer;
using Eurolook.AddIn.Common.UserConfiguration;
using Extensibility;
using Office = Microsoft.Office.Core;

#pragma warning disable S927

namespace Eurolook.AddIn.Common.AddIn
{
    [ComVisible(true)]
#if NET6_0_OR_GREATER
    public abstract class AddInBase :
#else
    public abstract class AddInBase : StandardOleMarshalObject,
#endif

                                      IOfficeAddInBase,
                                      IDTExtensibility2,
                                      Office.ICustomTaskPaneConsumer
    {
        protected AddInBase()
        {
            Application.EnableVisualStyles();
        }

        public void CTPFactoryAvailable(Office.ICTPFactory ctpFactoryInst)
        {
            OnTaskPaneFactoryAvailable(ctpFactoryInst);
        }

        public void OnConnection(object application, ext_ConnectMode connectMode, object addInInst, ref Array custom)
        {
            OnStartup(application);

            var comAddin = addInInst as Office.COMAddIn;
            var automationService = RequestComAddInAutomationService();
            if (comAddin != null)
            {
                comAddin.Object = automationService;
            }
        }

        public void OnDisconnection(ext_DisconnectMode removeMode, ref Array custom)
        {
            OnShutdown();
        }

        public void OnAddInsUpdate(ref Array custom)
        {
            // do nothing
        }

        public void OnStartupComplete(ref Array custom)
        {
            // do nothing
        }

        public void OnBeginShutdown(ref Array custom)
        {
            // do nothing
        }

        public abstract void OnStartup(object application);

        public virtual Task OnStartupLongRunningAsync(IProfileInitializer profileInitializer)
        {
            return Task.CompletedTask;
        }

        public abstract void OnShutdown();

        public virtual void OnTaskPaneFactoryAvailable(Office.ICTPFactory ctpFactoryInst)
        {
            // do nothing
        }

        public virtual object RequestComAddInAutomationService()
        {
            // do nothing
            return null;
        }

        protected UserConfigurationService CreateTemporaryUserConfigurationService()
        {
            return new UserConfigurationService(UserConfigurationService.ConfigurationPath);
        }

        protected IUserIdentity CreateTemporaryUserIdentity()
        {
            return new BasicUserIdentity();
        }
    }
}
