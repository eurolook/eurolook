using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;

namespace Eurolook.AddIn.Common.AddIn
{
    /// <summary>
    ///     Provides COM servers and applications with the ability to selectively handle incoming and outgoing COM messages
    ///     while waiting for responses from synchronous calls. Filtering messages helps to ensure that calls are handled in a
    ///     manner that improves performance and avoids deadlocks. COM messages can be synchronous, asynchronous, or
    ///     input-synchronized; the majority of interface calls are synchronous.
    /// </summary>
    /// <see cref="http://msdn.microsoft.com/en-us/library/windows/desktop/ms693740%28v=vs.85%29.aspx" />
    [ComImport]
    [Guid("00000016-0000-0000-C000-000000000046")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Names refer to native types.")]
    internal interface IOleMessageFilter
    {
        /// <summary>
        ///     Provides a single entry point for incoming calls.
        ///     This method is called prior to each method invocation originating outside the current process
        ///     and provides the ability to filter or reject incoming calls (or callbacks) to an object or a process.
        /// </summary>
        /// <param name="dwCallType">
        ///     The type of incoming call that has been received. Possible values are from the enumeration
        ///     CALLTYPE.
        /// </param>
        /// <param name="hTaskCaller">The thread id of the caller.</param>
        /// <param name="dwTickCount">
        ///     The elapsed tick count since the outgoing call was made, if dwCallType is not CALLTYPE_TOPLEVEL.
        ///     If dwCallType is CALLTYPE_TOPLEVEL, dwTickCount should be ignored.
        /// </param>
        /// <param name="lpInterfaceInfo">
        ///     A pointer to an INTERFACEINFO structure that identifies the object, interface, and method being called.
        ///     In the case of DDE calls, lpInterfaceInfo can be NULL because the DDE layer does not return interface information.
        /// </param>
        /// <returns>
        ///     This method can return the following values.
        ///     <list type="table">
        ///         <listheader>
        ///             <term>Return value</term>
        ///             <term>Description</term>
        ///         </listheader>
        ///         <item>
        ///             <term>SERVERCALL_ISHANDLED</term>
        ///             <term>The application might be able to process the call.</term>
        ///         </item>
        ///         <item>
        ///             <term>SERVERCALL_REJECTED</term>
        ///             <term>
        ///                 The application cannot handle the call due to an unforeseen problem, such as network unavailability,
        ///                 or if it is in the process of terminating.
        ///             </term>
        ///         </item>
        ///         <item>
        ///             <term>SERVERCALL_RETRYLATER</term>
        ///             <term>
        ///                 The application cannot handle the call at this time. An application might return this value when it
        ///                 is in a user-controlled modal state.
        ///             </term>
        ///         </item>
        ///     </list>
        /// </returns>
        [PreserveSig]
        int HandleInComingCall(int dwCallType, IntPtr hTaskCaller, int dwTickCount, IntPtr lpInterfaceInfo);

        /// <summary>
        ///     Provides applications with an opportunity to display a dialog box offering retry, cancel, or task-switching
        ///     options.
        /// </summary>
        /// <param name="hTaskCallee">The thread id of the called application.</param>
        /// <param name="dwTickCount">The number of elapsed ticks since the call was made.</param>
        /// <param name="dwRejectType">
        ///     Specifies either SERVERCALL_REJECTED or SERVERCALL_RETRYLATER, as returned by the object
        ///     application.
        /// </param>
        /// <returns>
        ///     This method can return the following values.
        ///     <list type="table">
        ///         <listheader>
        ///             <term>Return value</term>
        ///             <term>Description</term>
        ///         </listheader>
        ///         <item>
        ///             <term>-1</term>
        ///             <term>The call should be canceled. COM then returns RPC_E_CALL_REJECTED from the original method call.</term>
        ///         </item>
        ///         <item>
        ///             <term>0 ≤ value &lt; 100</term>
        ///             <term>The call is to be retried immediately.</term>
        ///         </item>
        ///         <item>
        ///             <term>100 ≤ value</term>
        ///             <term>COM will wait for this many milliseconds and then retry the call.</term>
        ///         </item>
        ///     </list>
        /// </returns>
        [PreserveSig]
        int RetryRejectedCall(IntPtr hTaskCallee, int dwTickCount, int dwRejectType);

        /// <summary>
        ///     Indicates that a message has arrived while COM is waiting to respond to a remote call.
        /// </summary>
        /// <param name="hTaskCallee">The thread id of the called application.</param>
        /// <param name="dwTickCount">The number of ticks since the call was made. It is calculated from the GetTickCount function.</param>
        /// <param name="dwPendingType">
        ///     The type of call made during which a message or event was received.
        ///     Possible values are from the enumeration PENDINGTYPE, where PENDINGTYPE_TOPLEVEL means the outgoing call
        ///     was not nested within a call from another application and PENDINTGYPE_NESTED means the outgoing call
        ///     was nested within a call from another application.
        /// </param>
        /// <returns>
        ///     This method can return the following values.
        ///     <list type="table">
        ///         <listheader>
        ///             <term>Return value</term>
        ///             <term>Description</term>
        ///         </listheader>
        ///         <item>
        ///             <term>PENDINGMSG_CANCELCALL</term>
        ///             <term>
        ///                 Cancel the outgoing call. This should be returned only under extreme conditions. Canceling a call
        ///                 that has not replied or been rejected can create orphan transactions and lose resources. COM fails the
        ///                 original call and returns RPC_E_CALL_CANCELLED.
        ///             </term>
        ///         </item>
        ///         <item>
        ///             <term>PENDINGMSG_WAITNOPROCESS</term>
        ///             <term>Unused.</term>
        ///         </item>
        ///         <item>
        ///             <term>PENDINGMSG_WAITDEFPROCESS</term>
        ///             <term>
        ///                 Keyboard and mouse messages are no longer dispatched. However there are some cases where mouse and
        ///                 keyboard messages could cause the system to deadlock, and in these cases, mouse and keyboard messages
        ///                 are discarded. WM_PAINT messages are dispatched. Task-switching and activation messages are handled as
        ///                 before
        ///             </term>
        ///         </item>
        ///     </list>
        /// </returns>
        [PreserveSig]
        int MessagePending(IntPtr hTaskCallee, int dwTickCount, int dwPendingType);
    }
}
