using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// ReSharper disable once CheckNamespace
namespace Extensibility
{
    /// <summary>
    /// Informs the add-in about how it was loaded by the integrated development environment (IDE).
    /// </summary>
    [Guid("289E9AF1-4973-11D1-AE81-00A0C90F26F4")]
    [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "This type is defined by an external API.")]
    [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "This type is defined by an external API.")]
    [SuppressMessage("Sonar", "S2342:Enumeration types should comply with a naming convention", Justification = "This type is defined by an external API.")]
    public enum ext_ConnectMode
    {
        /// <summary>
        /// The add-in was loaded after Visual Studio started.
        /// </summary>
        ext_cm_AfterStartup = 0,

        /// <summary>
        /// The add-in was loaded when Visual Studio started.
        /// </summary>
        ext_cm_Startup = 1,

        /// <summary>
        /// The add-in was loaded by an external client.
        /// </summary>
        ext_cm_External = 2,

        /// <summary>
        /// The add-in was loaded from the command line.
        /// </summary>
        ext_cm_CommandLine = 3,

        /// <summary>
        /// The add-in was loaded with a solution.
        /// </summary>
        ext_cm_Solution = 4,

        /// <summary>
        /// The add-in was loaded for user interface setup.
        /// </summary>
        ext_cm_UISetup = 5,
    }

    /// <summary>
    /// Informs the add-in about how it was unloaded by the integrated development environment (IDE).
    /// </summary>
    [Guid("289E9AF2-4973-11D1-AE81-00A0C90F26F4")]
    [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "This type is defined by an external API.")]
    [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "This type is defined by an external API.")]
    [SuppressMessage("Sonar", "S2342:Enumeration types should comply with a naming convention", Justification = "This type is defined by an external API.")]
    public enum ext_DisconnectMode
    {
        /// <summary>
        /// The add-in was unloaded when Visual Studio was shut down.
        /// </summary>
        ext_dm_HostShutdown = 0,

        /// <summary>
        /// The add-in was unloaded while Visual Studio was running.
        /// </summary>
        ext_dm_UserClosed = 1,

        /// <summary>
        /// The add-in was unloaded after the user interface was set up.
        /// </summary>
        ext_dm_UISetupComplete = 2,

        /// <summary>
        /// The add-in was unloaded when the solution was closed.
        /// </summary>
        ext_dm_SolutionClosed = 3,
    }

    /// <summary>
    /// Hosts the event notifications that occur to add-ins, such as when they are loaded, unloaded, updated, and so forth.
    /// </summary>
    [ComImport]
    [Guid("B65AD801-ABAF-11D0-BB8B-00A0C90F2744")]
    [TypeLibType(0x1040)]
    public interface IDTExtensibility2
    {
        /// <summary>
        /// Occurs whenever an add-in is loaded.
        /// </summary>
        /// <param name="application">A reference to an instance of the environment application, which is the root object of the automation model.</param>
        /// <param name="connectMode">An ext_ConnectMode enumeration value that indicates the way the add-in was loaded.</param>
        /// <param name="addInInst">An AddIn reference to the add-in's own instance. This is stored for later use, such as determining the parent collection for the add-in.</param>
        /// <param name="custom">An empty array that you can use to pass host-specific data for use in the add-in.</param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [DispId(1)]
        void OnConnection(
            [In, MarshalAs(UnmanagedType.IDispatch)] object application,
            [In] ext_ConnectMode connectMode,
            [In, MarshalAs(UnmanagedType.IDispatch)] object addInInst,
            [In, MarshalAs(UnmanagedType.SafeArray, SafeArraySubType = VarEnum.VT_VARIANT)] ref Array custom);

        /// <summary>
        /// Occurs whenever an add-in is unloaded.
        /// </summary>
        /// <param name="removeMode">An ext_DisconnectMode enumeration value that informs an add-in why it was unloaded.</param>
        /// <param name="custom">An empty array that you can use to pass host-specific data for use after the add-in unloads.</param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [DispId(2)]
        void OnDisconnection(
            [In] ext_DisconnectMode removeMode,
            [In, MarshalAs(UnmanagedType.SafeArray, SafeArraySubType = VarEnum.VT_VARIANT)] ref Array custom);

        /// <summary>
        /// Occurs whenever an add-in is loaded or unloaded from the environment.
        /// </summary>
        /// <param name="custom">An empty array that you can use to pass host-specific data for use in the add-in.</param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [DispId(3)]
        void OnAddInsUpdate([In, MarshalAs(UnmanagedType.SafeArray, SafeArraySubType = VarEnum.VT_VARIANT)] ref Array custom);

        /// <summary>
        /// Occurs whenever an add-in, which is set to load when the environment starts, loads.
        /// </summary>
        /// <param name="custom">An empty array that you can use to pass host-specific data for use when the add-in loads.</param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [DispId(4)]
        void OnStartupComplete([In, MarshalAs(UnmanagedType.SafeArray, SafeArraySubType = VarEnum.VT_VARIANT)] ref Array custom);

        /// <summary>
        /// Occurs whenever the environment shuts down while an add-in is running.
        /// </summary>
        /// <param name="custom">An empty array that you can use to pass host-specific data for use in the add-in.</param>
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [DispId(5)]
        void OnBeginShutdown([In, MarshalAs(UnmanagedType.SafeArray, SafeArraySubType = VarEnum.VT_VARIANT)] ref Array custom);
    }
}
