using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;

namespace Eurolook.AddIn.Common.AddIn
{
    /// <summary>
    /// A class to fix 'Application is Busy' and 'Call was Rejected By Callee' errors.
    /// </summary>
    /// <seealso cref="http://msdn.microsoft.com/en-us/library/ms228772.aspx" />
    [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Names refer to native types.")]
    public class MessageFilter : IOleMessageFilter
    {
        /// <summary>
        /// Indicates the status of server call.
        /// </summary>
        [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "This type is defined by an external API.")]
        [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "This type is defined by an external API.")]
        private enum SERVERCALL
        {
            /// <summary>
            /// The object may be able to process the call.
            /// </summary>
            SERVERCALL_ISHANDLED = 0,

            /// <summary>
            /// The object cannot handle the call due to an unforeseen problem, such as network unavailability.
            /// </summary>
            SERVERCALL_REJECTED = 1,

            /// <summary>
            /// The object cannot handle the call at this time. For example, an application might return this
            /// value when it is in a user-controlled modal state.
            /// </summary>
            SERVERCALL_RETRYLATER = 2,
        }

        /// <summary>
        /// Indicates the level of nesting in the IMessageFilter::MessagePending method.
        /// </summary>
        [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "This type is defined by an external API.")]
        [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "This type is defined by an external API.")]
        private enum PENDINGTYPE
        {
            /// <summary>
            /// Top-level call.
            /// </summary>
            PENDINGTYPE_TOPLEVEL = 1,

            /// <summary>
            /// Nested call.
            /// </summary>
            PENDINGTYPE_NESTED = 2,
        }

        /// <summary>
        /// Specifies the return values for the IMessageFilter::MessagePending method.
        /// </summary>
        [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "This type is defined by an external API.")]
        [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "This type is defined by an external API.")]
        private enum PENDINGMSG
        {
            /// <summary>
            /// Cancel the outgoing call.
            /// </summary>
            PENDINGMSG_CANCELCALL = 0,

            /// <summary>
            /// Wait for the return and don't dispatch the message.
            /// </summary>
            PENDINGMSG_WAITNOPROCESS = 1,

            /// <summary>
            /// Wait and dispatch the message.
            /// </summary>
            PENDINGMSG_WAITDEFPROCESS = 2,
        }

        public static void Register()
        {
            IOleMessageFilter newFilter = new MessageFilter();

            // Start the filter.
            CoRegisterMessageFilter(newFilter, out _);
        }

        public static void Unregister()
        {
            // Done with the filter, close it.
            CoRegisterMessageFilter(null, out _);
        }

        int IOleMessageFilter.HandleInComingCall(
            int dwCallType,
            IntPtr hTaskCaller,
            int dwTickCount,
            IntPtr lpInterfaceInfo)
        {
            // Process the call.
            return (int)SERVERCALL.SERVERCALL_ISHANDLED;
        }

        int IOleMessageFilter.RetryRejectedCall(IntPtr hTaskCallee, int dwTickCount, int dwRejectType)
        {
            if (dwRejectType == (int)SERVERCALL.SERVERCALL_RETRYLATER)
            {
                // Retry the thread call immediately if return >=0 & <100.
                return 99;
            }

            // Too busy; cancel call.
            return -1;
        }

        int IOleMessageFilter.MessagePending(IntPtr hTaskCallee, int dwTickCount, int dwPendingType)
        {
            // Wait and dispatch the message.
            return (int)PENDINGMSG.PENDINGMSG_WAITDEFPROCESS;
        }

        [DllImport("Ole32.dll")]
        private static extern int CoRegisterMessageFilter(IOleMessageFilter newFilter, out IOleMessageFilter oldFilter);
    }
}
