using System.Threading;
using System.Windows.Threading;

namespace Eurolook.AddIn.Common.AddIn
{
    public static class AsyncHelper
    {
        public static void EnsureSynchronizationContext()
        {
            if (SynchronizationContext.Current == null)
            {
                SynchronizationContext.SetSynchronizationContext(new DispatcherSynchronizationContext());
            }
        }
    }
}
