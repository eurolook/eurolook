using System;
using System.Runtime.InteropServices;

namespace Eurolook.AddIn.Common.AddIn
{
    /// <summary>
    /// This interface will be implemented by the outer object in the
    /// aggregation - that is, by the shim.
    /// </summary>
    [ComImport]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [Guid("7B70C487-B741-4973-B915-B812A91BDF63")]
    internal interface IComAggregator
    {
        void SetInnerPointer(IntPtr pUnkInner);
    }
}
