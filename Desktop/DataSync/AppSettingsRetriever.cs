﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Eurolook.Common;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Microsoft.Extensions.Configuration;

namespace Eurolook.DataSync
{
    public class AppSettingsRetriever
    {
        protected AppSettingsRetriever()
        {
        }

        public static IConfiguration GetConfiguration()
        {
            var appSettingsId = GetAppSettingsId();

            var basePath = Path.GetDirectoryName(typeof(AppSettingsRetriever).Assembly.Location);

            IConfiguration config = new ConfigurationBuilder()
                                    .SetBasePath(basePath)
                                    .AddDefaultConfigurationProviders(appSettingsId)
                                    .Build();
            return config;
        }

        protected static string GetAppSettingsId()
        {
            try
            {
                // ENV variable has the highest priority. This supports the scenario that the environment name
                // is configured via an env variable, e.g. from a Visual Studio launch profile.
                var appSettingsId = Environment.GetEnvironmentVariable("EUROLOOK_APPSETTINGS");
                if (!string.IsNullOrEmpty(appSettingsId))
                {
                    // Remember the environment. This supports the scenario that the environment name is configured via an
                    // ENV variable, e.g. from a Visual Studio launch profile, and then Office is launched later on directly
                    // without an ENV variable being set.
                    SaveAppSettingsIdToDotEnvFile(appSettingsId);

                    LogManager.GetLogger().Info($"Loading appsettings for environment '{appSettingsId}' set as ENV variable.");
                    return appSettingsId;
                }

                // If there is no ENV var we check whether there is only a single customization.
                // This would be the typical case then the add-in is deployed to PROD.
                appSettingsId = InterpolateAppSettingsIdFromFilename();
                if (!string.IsNullOrEmpty(appSettingsId))
                {
                    LogManager.GetLogger().Info(
                        $"Loading appsettings for environment '{appSettingsId}' interpolated from appsettings.*.json file names.");
                    return appSettingsId;
                }

                // If there is no ENV var and there are appsettings.json files for several environments
                // we use the value from .env file in the local appdata folder. This is a typical scenario when debugging
                appSettingsId = ReadAppSettingsIdFromDotEnvFile();
                if (!string.IsNullOrEmpty(appSettingsId))
                {
                    LogManager.GetLogger().Info($"Loading appsettings for environment '{appSettingsId}' set in .env file.");
                    return appSettingsId;
                }

                // This is just a fallback value
                return "Debug";
            }
            catch (Exception e)
            {
                LogManager.GetLogger().Error(e.ToString());
                return "Debug";
            }
        }

        private static string ReadAppSettingsIdFromDotEnvFile()
        {
            try
            {
                var appDataFolder = Directories.GetApplicationDataDirectory(false);
                var fileName = Path.Combine(appDataFolder.FullName, ".env");
                if (!File.Exists(fileName))
                {
                    return null;
                }

                var lines = File.ReadAllLines(fileName).ToList();
                var line = lines.Find(line => line.StartsWith("EUROLOOK_APPSETTINGS=", StringComparison.Ordinal));
                return line.Substring("EUROLOOK_APPSETTINGS=".Length);
            }
            catch (Exception e)
            {
                LogManager.GetLogger().Error(e.ToString());
                return null;
            }
        }

        private static void SaveAppSettingsIdToDotEnvFile(string appSettingsId)
        {
            try
            {
                var appDataFolder = Directories.GetApplicationDataDirectory(false);
                var dotEnvFile = new DotEnvFile(appDataFolder.FullName);
                dotEnvFile.AddOrUpdateValue("EUROLOOK_APPSETTINGS", appSettingsId);
            }
            catch (Exception e)
            {
                LogManager.GetLogger().Error(e.ToString());
            }
        }

        private static string InterpolateAppSettingsIdFromFilename()
        {
            // extract ids from appsettings.*.json files found and return the id if unique
            var location = Path.GetDirectoryName(typeof(AppSettingsRetriever).Assembly.Location);
            if (location == null)
            {
                return null;
            }

            var appSettingFiles = Directory.EnumerateFiles(location, "appsettings.*.json").ToList();

            var ids = appSettingFiles
                      .Select(f =>
                              {
                                  var match = Regex.Match(f, "appsettings\\.(?<id>[^.]+)(?:\\.[^.]+)?\\.json");
                                  return match.Success ? match.Groups["id"].Value : null;
                              })
                      .Where(id => id != null)
                      .Distinct()
                      .ToList();

            return ids.Count == 1 ? ids[0] : null;
        }

        private class DotEnvFile
        {
            private static readonly object Lock = new();

            private readonly string _fullPath;

            public DotEnvFile(string folder)
            {
                _fullPath = Path.Combine(folder, ".env");
            }

            public string GetValue(string key)
            {
                if (!File.Exists(_fullPath))
                {
                    return null;
                }

                lock (Lock)
                {
                    var lines = File.ReadAllLines(_fullPath).ToList();
                    var line = lines.Find(line => line.StartsWith($"{key}=", StringComparison.Ordinal));
                    return line.Substring($"{key}=".Length);
                }
            }

            public void AddOrUpdateValue(string key, string value)
            {
                lock (Lock)
                {
                    var lines = File.Exists(_fullPath) ? File.ReadAllLines(_fullPath).ToList() : new List<string>(1);
                    var entry = lines.FindIndex(line => line.StartsWith($"{key}=", StringComparison.Ordinal));
                    if (entry >= 0)
                    {
                        lines[entry] = $"{key}={value}";
                    }
                    else
                    {
                        lines.Add($"{key}={value}");
                    }

                    File.WriteAllLines(_fullPath, lines);
                }
            }
        }
    }
}
