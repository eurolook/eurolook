﻿using System;
using System.Diagnostics;
using System.Threading;
using Eurolook.Common.Log;

namespace Eurolook.DataSync
{
    /// <summary>
    /// This class checks to make sure that only one instance of
    /// this application is running at a time.
    /// </summary>
    public static class SingleInstance
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SingleInstance));

        /// <summary>
        /// Application mutex.
        /// </summary>
        private static Mutex _singleInstanceMutex;

        /// <summary>
        /// Checks if the instance of the application attempting to start is the first instance.
        /// </summary>
        /// <returns>True if this is the first instance of the application.</returns>
        public static bool InitializeAsFirstInstance(string uniqueName)
        {
            // Build unique application Id and the IPC channel name
            // (add the process owner to allow multiple instances when using 'runas')
            int sessionId = Process.GetCurrentProcess().SessionId;
            string applicationIdentifier =
                $"{uniqueName}_{Environment.UserDomainName}_{Environment.UserName}_{sessionId}";

            // Create mutex based on unique application Id to check if this is the first instance of the application.
            _singleInstanceMutex = new Mutex(true, applicationIdentifier, out bool firstInstance);

            Logger.Info(
                firstInstance
                    ? "Starting first application instance with identifier '{0}'"
                    : "Signalling first application instance with identifier '{0}'",
                applicationIdentifier);

            return firstInstance;
        }

        /// <summary>
        /// Cleans up single-instance code, clearing shared resources, mutexes, etc.
        /// </summary>
        public static void Cleanup()
        {
            if (_singleInstanceMutex != null)
            {
                _singleInstanceMutex.Close();
                _singleInstanceMutex = null;
            }
        }
    }
}
