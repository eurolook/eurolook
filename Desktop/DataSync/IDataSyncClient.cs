using System;
using System.Threading.Tasks;
using Eurolook.Data.ExceptionLog;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;

namespace Eurolook.DataSync
{
    internal interface IDataSyncClient
    {
        Task<UpdatePlan> GetUpdates(UpdateRequest request);

        // User Profile Sync
        Task<User> GetUserProfile(string login);

        Task<User> CreateUser(User user);

        Task<Template> GetTemplate(Guid id);

        // Upload methods (sync from the client to the server)
        Task<Author> UpdateAuthor(Guid id, Author author, string originator);

        Task<UserSettings> UpdateUserSettings(Guid id, UserSettings settings);

        Task<DocumentModelSettings> AddOrUpdateDocumentModelSettings(Guid id, DocumentModelSettings settings);

        Task<DeviceSettings> AddOrUpdateDeviceSettings(Guid id, DeviceSettings settings);

        Task<BrickSettings> AddOrUpdateBrickSettings(Guid id, BrickSettings settings);

        Task<JobFunction> AddOrUpdateJobFunction(Guid id, JobFunction jobFunction);

        Task<UserAuthor> AddOrUpdateUserAuthor(Guid id, UserAuthor userAuthor);

        Task<Workplace> AddOrUpdateWorkplace(Guid id, Workplace workplace);

        Task<JobAssignment> AddOrUpdateJobAssignment(Guid id, JobAssignment jobAssignment);

        Task<UserBrick> AddOrUpdateUserBrick(Guid id, UserBrick userBrick);

        Task<BuildingBlockBrick> AddOrUpdateBuildingBlockBrick(Guid id, BuildingBlockBrick buildingBlockBrick);

        Task<bool> UploadActionLogs(ActionLog[] actionLogs);

        Task<bool> UploadExceptionLogs(ExceptionBucket[] exceptionBuckets, ExceptionLogEntry[] exceptionLogs);

        Task<bool> UploadActivityTracks(ActivityTrack[] activityTracks);

        // Mini-Mode
        Task<bool> UploadDeviceInformation(DeviceInfo deviceInfo, string userName);

        // Remote Wipe
        Task<bool> ConfirmRemoteWipe(string login, string deviceName);

        Task<bool> UploadPerformanceLogs(PerformanceLog[] performanceLogs);

        Task<UserTemplate> AddOrUpdateUserTemplate(Guid id, UserTemplate userTemplate);
    }
}
