﻿using System;
using System.Drawing;
using System.Linq;
using System.Management;
using Eurolook.Common;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Microsoft.Win32;

namespace Eurolook.DataSync
{
    public class DeviceInfoRetriever : ICanLog
    {
        public DeviceInfo GetDeviceInfo()
        {
            var dotNetVersionInfo = new DotNetVersionInfo();

            return new DeviceInfo
            {
                DeviceName = GetDeviceName(),
                DeviceOwner = GetDeviceOwner(),
                OsName = GetOsName(),
                IsTouchScreen = CheckHasTouchScreen(),
                HasBattery = CheckHasBattery(),
                ScreenDpi = GetPrimaryScreenDpi(),
                ScreenWidth = PrimaryScreenWidth(),
                ScreenHeight = PrimaryScreenHeight(),
                PhysicallyInstalledSystemMemoryKb = GetPhysicallyInstalledSystemMemory(),
                ProcessorName = GetProcessorName(),
                DotNetRuntimeVersion = dotNetVersionInfo.RuntimeVersion,
                DotNetFullFrameworkReleaseVersion = dotNetVersionInfo.FullFrameworkReleaseVersion,
                IsPerMachineInstall = IsInstalledIn(Registry.LocalMachine),
                IsPerUserInstall = IsInstalledIn(Registry.CurrentUser),
            };
        }

        public static string GetDeviceName()
        {
            return Environment.MachineName;
        }

        private string GetDeviceOwner()
        {
            string deviceOwner = "";
            if (!string.IsNullOrEmpty(DataSyncSettings.Instance.DeviceOwnerEnvironmentVariable))
            {
                deviceOwner = Environment.GetEnvironmentVariable(
                    DataSyncSettings.Instance.DeviceOwnerEnvironmentVariable,
                    EnvironmentVariableTarget.User);
                this.LogDebug(
                    $"Found device owner '{deviceOwner}' in environment variable {DataSyncSettings.Instance.DeviceOwnerEnvironmentVariable}");
            }

            if (string.IsNullOrWhiteSpace(deviceOwner))
            {
                deviceOwner = DataSyncSettings.Instance.DefaultDeviceOwner;
                this.LogDebug($"Using default device owner '{deviceOwner}' from app settings.");
            }

            return deviceOwner;
        }

        private string GetOsName()
        {
            try
            {
                if (Environment.OSVersion.Version.Major >= 10)
                {
                    var versionInfo = new WindowsVersionInfo();
                    return versionInfo.ToString().TruncateLongString(50);
                }

                using (var searcher = new ManagementObjectSearcher("SELECT Caption FROM Win32_OperatingSystem"))
                {
                    string osName = (from ManagementBaseObject os in searcher.Get() select os["Caption"].ToString()).FirstOrDefault();
                    return osName;
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                return "Unknown OS";
            }
        }

        private bool CheckHasBattery()
        {
            try
            {
                using (var searcher = new ManagementObjectSearcher("SELECT * FROM Win32_Battery"))
                {
                    bool hasBattery = searcher.Get().Count > 0;
                    return hasBattery;
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                return false;
            }
        }

        private static int PrimaryScreenHeight()
        {
            return NativeMethods.GetSystemMetrics(NativeMethods.SystemMetric.SM_CXSCREEN);
        }

        private static int PrimaryScreenWidth()
        {
            return NativeMethods.GetSystemMetrics(NativeMethods.SystemMetric.SM_CYSCREEN);
        }

        private static bool CheckHasTouchScreen()
        {
            return NativeMethods.GetSystemMetrics(NativeMethods.SystemMetric.SM_MAXIMUMTOUCHES) > 0;
        }

        private static double GetPrimaryScreenDpi()
        {
            using (var graphics = Graphics.FromHwnd(IntPtr.Zero))
            {
                float dpiX = graphics.DpiX;
                float dpiY = graphics.DpiY;
                return (dpiX + dpiY) / 2;
            }
        }

        private static long GetPhysicallyInstalledSystemMemory()
        {
            return NativeMethods.GetPhysicallyInstalledSystemMemory(out long memKb) ? memKb : 0;
        }

        private static string GetProcessorName()
        {
            using (var key = Registry.LocalMachine.OpenSubKey(@"HARDWARE\DESCRIPTION\System\CentralProcessor\0\"))
            {
                var processorName = key?.GetValue("ProcessorNameString") as string;
                if (!string.IsNullOrEmpty(processorName))
                {
                    return processorName;
                }
            }

            return Environment.GetEnvironmentVariable("PROCESSOR_IDENTIFIER");
        }

        private static bool IsInstalledIn(RegistryKey hive)
        {
            try
            {
                var key = hive.OpenSubKey(@"Software\Microsoft\Office\Word\Addins\Eurolook.WordAddIn.10");
                var loadBehavior = key?.GetValue("LoadBehavior");
                return loadBehavior != null && loadBehavior.ToString() == "3";
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
