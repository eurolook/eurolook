namespace Eurolook.DataSync
{
    public class DataSyncStatus
    {
        public DataSyncResult Result { get; set; }

        public string StandardOutput { get; set; }
    }
}
