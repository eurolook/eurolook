﻿using System;
using System.Diagnostics;
using System.Security.Principal;
using System.Threading.Tasks;
using CommandLine;
using Eurolook.Common;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data;
using Eurolook.Data.ExceptionLog;
using Eurolook.Data.Models;
using Eurolook.DataSync.Database;

namespace Eurolook.DataSync
{
    public static class Program
    {
        private const string UniqueAppId = "{2EBD200B-E062-4A0E-A91B-BB5804841ED8}";
        private static readonly Version _clientVersion;

        static Program()
        {
            _clientVersion = typeof(Program).Assembly.GetName().Version;
            LogManager.LogFileName = "Eurolook.DataSync";
            LogManager.AssemblyVersion = _clientVersion;
        }

        [STAThread]
        private static int Main(string[] args)
        {
            ConfigurationInfo.Initialize(DataSyncSettings.Instance.ProductCustomizationId);

            RegisterExceptionLogger();

            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
            TaskScheduler.UnobservedTaskException += TaskSchedulerOnUnobservedTaskException;

            // parse options
            var parser = new Parser(with => { with.HelpWriter = Console.Error; });
            return parser.ParseArguments<Options>(args)
                         .MapResult(
                             RunAndReturnExitCode,
                             _ => (int)DataSyncResult.InvalidParameters);
        }

        private static int RunAndReturnExitCode(Options options)
        {
            if (SingleInstance.InitializeAsFirstInstance(UniqueAppId))
            {
                try
                {
                    LogStartupMessage(options);

                    var sw = Stopwatch.StartNew();
                    var sync = new DataSync();
                    sync.RunAsync(options).Wait();

                    LogManager.GetLogger().Info("DataSync finished in {0}", sw.Elapsed);

                    return (int)sync.Result;
                }
                catch (Exception ex)
                {
                    LogManager.GetLogger(typeof(Program)).Fatal(ex.ToString());
                }
                finally
                {
                    // Allow single instance code to perform cleanup operations
                    SingleInstance.Cleanup();
                }
            }

            return 0;
        }

        private static void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var logger = LogManager.GetLogger(typeof(Program));

            logger.Fatal(e.ToString());
            logger.Fatal(e.ExceptionObject.ToString());
            if (e.IsTerminating)
            {
                logger.Fatal("DataSync is terminating.");
            }
        }

        private static void TaskSchedulerOnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            var logger = LogManager.GetLogger(typeof(Program));

            logger.Fatal(e.ToString());
            logger.Fatal(e.Exception.ToString());

            e.SetObserved();
        }

        private static void RegisterExceptionLogger()
        {
            try
            {
                var user = TryGetUserForLogging();
                var exceptionLogInfoProvider = new ExceptionLogInfoProvider
                {
                    User = user,
                    ClientVersion = _clientVersion,
                };
                NLogExceptionLogger.Register(exceptionLogInfoProvider);
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(typeof(Program)).Error(ex.ToString());
            }
        }

        private static User TryGetUserForLogging()
        {
            try
            {
                return new DataSyncDatabase().GetUserForLogging(Windows.Login);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static void LogStartupMessage(Options options)
        {
            var logger = LogManager.GetLogger();
            var assembly = typeof(Program).Assembly;
            string userName = options.User ?? WindowsIdentity.GetCurrent().Name;
            logger.LogStartupMessage(assembly, userName);
        }
    }
}
