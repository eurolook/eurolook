﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.ExceptionLog;
using Eurolook.Data.Extensions;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;
using Microsoft.EntityFrameworkCore;

namespace Eurolook.DataSync.Database
{
    public class DataSyncDatabase : EurolookClientDatabase
    {
        public DataSyncDatabase()
        {
            SetLocalDataDirectory(DataSyncSettings.Instance.UseRoamingDataDirectory);
        }

        protected override EurolookClientContext GetContext()
        {
            return new EurolookClientContext(ignoreDeletedFlags: false);
        }

        /// <summary>
        /// Gets the full profile of the user (including deleted entities).
        /// </summary>
        /// <param name="login">The login of the user.</param>
        public User GetUserProfileForSync(string login)
        {
            using (var context = GetContext())
            {
                // perform any check before doing the join statement for performance reasons
                if (context.Users.Any(u => u.Login == login))
                {
                    var user = context.Users.AsNoTracking().FirstOrDefault(x => x.Login == login);
                    if (user != null)
                    {
                        user.Settings = context.UserSettings.AsNoTracking()
                                               .Where(x => x.Id == user.SettingsId)
                                               .Include(x => x.DocumentModelSettings)
                                               .Include(x => x.DeviceSettings)
                                               .Include(x => x.BrickSettings)
                                               .FirstOrDefault();
                        user.Self = context.Authors.AsNoTracking()
                                           .Where(x => x.Id == user.SelfId)
                                           .Include(x => x.OrgaEntity).ThenInclude(x => x.SuperEntity)
                                           .ThenInclude(x => x.SuperEntity)
                                           .Include(x => x.OrgaEntity).ThenInclude(x => x.PrimaryAddress)
                                           .Include(x => x.OrgaEntity).ThenInclude(x => x.SecondaryAddress)
                                           .Include(x => x.Functions).ThenInclude(x => x.Language)
                                           .Include(x => x.Workplaces)
                                           .Include(x => x.JobAssignments).ThenInclude(x => x.Functions)
                                           .FirstOrDefault();
                        user.Superiors = context.UserSuperiors.AsNoTracking()
                                              .Where(x => x.UserId == user.Id)
                                              .Include(x => x.Author).ThenInclude(x => x.Functions)
                                              .Include(x => x.Author).ThenInclude(x => x.Workplaces)
                                              .Include(x => x.Author).ThenInclude(x => x.JobAssignments)
                                              .ThenInclude(x => x.Functions)
                                              .ToHashSet();
                        user.Authors = context.UserAuthors.AsNoTracking()
                                              .Where(x => x.UserId == user.Id)
                                              .Include(x => x.Author).ThenInclude(x => x.Functions)
                                              .Include(x => x.Author).ThenInclude(x => x.Workplaces)
                                              .Include(x => x.Author).ThenInclude(x => x.JobAssignments)
                                              .ThenInclude(x => x.Functions)
                                              .ToHashSet();
                        user.Templates = context.UserTemplates.AsNoTracking()
                                              .Where(x => x.UserId == user.Id)
                                              .Include(x => x.Template).ThenInclude(x => x.Owners)
                                              .Include(x => x.Template).ThenInclude(x => x.Publications)
                                              .Include(x => x.Template).ThenInclude(x => x.TemplateFiles)
                                              .ToHashSet();
                        user.Bricks = context.UserBricks.AsNoTracking()
                                             .Where(x => x.UserId == user.Id)
                                             .Include(x => x.Brick)
                                             .ToHashSet();
                        return user;
                    }
                }

                return null;
            }
        }

        public ActionLog[] GetActionLogs()
        {
            using (var context = GetContext())
            {
                return context.ActionLogs.ToArray();
            }
        }

        internal void DeleteActionLogs(ActionLog[] actionLogs)
        {
            using (var context = GetContext())
            {
                foreach (var actionLog in actionLogs)
                {
                    context.ActionLogs.Attach(actionLog);
                }

                context.ActionLogs.RemoveRange(actionLogs);
                context.SaveChanges();
            }
        }

        public ExceptionBucket[] GetExceptionBuckets()
        {
            using (var context = GetContext())
            {
                return context.ExceptionBuckets.ToArray();
            }
        }

        public ExceptionLogEntry[] GetExceptionLogEntries()
        {
            using (var context = GetContext())
            {
                return context.ExceptionLogEntries.ToArray();
            }
        }

        internal void DeleteExceptionLogs()
        {
            using (var context = GetContext())
            {
                context.ExceptionLogEntries.RemoveRange(context.ExceptionLogEntries);
                context.ExceptionBuckets.RemoveRange(context.ExceptionBuckets);
                context.SaveChanges();
            }
        }

        public DateTime GetLastUpdateUtc()
        {
            using (var context = GetContext())
            {
                return context.DatabaseState.FirstOrDefault()?.LastUpdateUtc ?? DateTime.MinValue;
            }
        }

        private async Task SetLastUpdateUtc(EurolookClientContext context, DateTime date)
        {
            var state = await context.DatabaseState.FirstOrDefaultAsync();
            if (state == null)
            {
                return;
            }

            state.LastUpdateUtc = date;

            // we are in a context where AutoDetectChangesEnabled might be set to false
            context.Entry(state).State = EntityState.Modified;
        }

        public void SetLastDataSyncState(DataSyncResult result)
        {
            using (var context = GetContext())
            {
                var currentState = context.DatabaseState.FirstOrDefault();
                if (currentState == null)
                {
                    return;
                }

                var dateTime = DateTime.UtcNow;
                if (result == DataSyncResult.Success)
                {
                    currentState.LastSuccessfulRunDateUtc = dateTime;
                }

                currentState.LastRunDateUtc = dateTime;
                context.SaveChanges();
            }
        }

        public async Task InsertOrUpdateEurolookDataAsync(UpdatePlan updates, bool isInitialization)
        {
            using (var context = GetContext())
            {
                context.ChangeTracker.AutoDetectChangesEnabled = false;
                if (isInitialization)
                {
                    await BulkInsertUpdatePlan(context, updates);
                }
                else
                {
                    AddOrUpdateUpdatePlan(context, updates);
                }

                await SetLastUpdateUtc(context, updates.ServerTimestampUTC);
                await context.SaveChangesAsync();
            }
        }

        internal PerformanceLog[] GetPerformanceLogs()
        {
            using (var context = GetContext())
            {
                return context.PerformanceLogs.ToArray();
            }
        }

        public void DeletePerformanceLogs(PerformanceLog[] performanceLogs)
        {
            using (var context = GetContext())
            {
                foreach (var performanceLog in performanceLogs)
                {
                    context.PerformanceLogs.Attach(performanceLog);
                }

                context.PerformanceLogs.RemoveRange(performanceLogs);
                context.SaveChanges();
            }
        }

        public async Task UpdateDiscriminators(UpdatePlan updates)
        {
            using (var context = GetContext())
            {
                await UpdateDiscriminator(context, updates.CommandBricks.Cast<Brick>().ToArray());
                await UpdateDiscriminator(context, updates.DynamicBricks.Cast<Brick>().ToArray());
                await UpdateDiscriminator(context, updates.ContentBricks.Cast<Brick>().ToArray());
            }
        }

        private async Task BulkInsertUpdatePlan(EurolookContext context, UpdatePlan updates)
        {
            await context.AddRangeAsync(updates.Languages);
            await context.AddRangeAsync(updates.Resources);
            await context.AddRangeAsync(updates.LocalisedResources);
            await context.AddRangeAsync(updates.Texts);
            await context.AddRangeAsync(updates.Translations);
            await context.AddRangeAsync(updates.Addresses);
            await context.AddRangeAsync(updates.OrgaEntities);
            await context.AddRangeAsync(updates.BrickCategories);
            await context.AddRangeAsync(updates.BrickGroups);
            await context.AddRangeAsync(updates.UserGroups);
            await context.AddRangeAsync(updates.CommandBricks);
            await context.AddRangeAsync(updates.DynamicBricks);
            await context.AddRangeAsync(updates.ContentBricks);
            await context.AddRangeAsync(updates.DocumentCategories);
            await context.AddRangeAsync(updates.DocumentModels);
            await context.AddRangeAsync(updates.DocumentStructures);
            await context.AddRangeAsync(updates.DocumentModelLanguages);
            await context.AddRangeAsync(updates.BrickTexts);
            await context.AddRangeAsync(updates.BrickResources);
            await context.AddRangeAsync(updates.CharacterMappings);
            await context.AddRangeAsync(updates.PersonNames);
            await context.AddRangeAsync(updates.StyleShortcuts);
            await context.AddRangeAsync(updates.ColorSchemes);
            await context.AddRangeAsync(updates.TermStoreImports);
            await context.AddRangeAsync(updates.MetadataCategories);
            await context.AddRangeAsync(updates.MetadataDefinitions);
            await context.AddRangeAsync(updates.DocumentModelMetadataDefinitions);
            await context.AddRangeAsync(updates.AuthorRoles);
            await context.AddRangeAsync(updates.DocumentModelAuthorRoles);
            await context.AddRangeAsync(updates.PredefinedFunctions);
            await context.AddRangeAsync(updates.Notifications);
            await context.AddRangeAsync(updates.SystemConfigurations);
        }

        internal Task Cleanup(string login)
        {
            return Task.Run(() =>
            {
                CleanupUserData(login);
                CleanupEurolookData();
            });
        }

        private void CleanupUserData(string login)
        {
            using (var context = GetContext())
            {
                var user = context.Users.FirstOrDefault(u => u.Login == login);
                if (user == null)
                {
                    return;
                }

                // remove deleted DocumentModelSettings
                var deletedDocumentModels = context.DocumentModelSettings.Where(dms => dms.Deleted).ToArray();
                context.DocumentModelSettings.RemoveRange(deletedDocumentModels);

                var userAuthors = context.Users.Select(u => u.SelfId).ToHashSet();

                // removed unreferenced author records
                foreach (var author in context.Authors)
                {
                    // don't remove self authors
                    if (userAuthors.Contains(author.Id))
                    {
                        continue;
                    }

                    // remove deleted references to this author
                    var deletedUserAuthors = context.UserAuthors.Where(ua => ua.AuthorId == author.Id && ua.Deleted).ToArray();
                    context.UserAuthors.RemoveRange(deletedUserAuthors);

                    // no more references? delete the author
                    if (!context.UserAuthors.Any(ua => ua.AuthorId == author.Id))
                    {
                        RemoveAuthor(context, author.Id, author);
                    }
                }

                context.SaveChanges();
            }
        }

        private static void RemoveAuthor(EurolookClientContext context, Guid authorId, Author author)
        {
            context.JobFunctions.RemoveRange(context.JobFunctions.Where(x => x.AuthorId == authorId));
            foreach (var jobAssignment in context.JobAssignments.Where(x => x.AuthorId == authorId))
            {
                var job = jobAssignment;
                context.JobFunctions.RemoveRange(context.JobFunctions.Where(x => x.JobAssignmentId == job.Id));
                context.JobAssignments.Remove(job);
            }

            context.Workplaces.RemoveRange(context.Workplaces.Where(wp => wp.AuthorId == authorId));
            context.Authors.Remove(author);
        }

        private void CleanupEurolookData()
        {
            using (var context = GetContext())
            {
                context.ColorSchemes.RemoveRange(context.ColorSchemes.Where(x => x.Deleted));
                context.StyleShortcuts.RemoveRange(context.StyleShortcuts.Where(x => x.Deleted));
                context.PersonNames.RemoveRange(context.PersonNames.Where(x => x.Deleted));
                context.CharacterMappings.RemoveRange(context.CharacterMappings.Where(x => x.Deleted));
                context.BrickResources.RemoveRange(context.BrickResources.Where(x => x.Deleted));
                context.BrickTexts.RemoveRange(context.BrickTexts.Where(x => x.Deleted));
                context.DocumentModelLanguages.RemoveRange(context.DocumentModelLanguages.Where(x => x.Deleted));
                context.DocumentStructure.RemoveRange(context.DocumentStructure.Where(x => x.Deleted));
                context.DocumentModels.RemoveRange(context.DocumentModels.Where(x => x.Deleted));
                context.Bricks.RemoveRange(context.Bricks.Where(x => x.Deleted));
                context.BrickGroups.RemoveRange(context.BrickGroups.Where(x => x.Deleted));
                context.BrickCategories.RemoveRange(context.BrickCategories.Where(x => x.Deleted));
                context.UserGroups.RemoveRange(context.UserGroups.Where(x => x.Deleted));

                RemoveOrgaEntities(context);

                context.Addresses.RemoveRange(context.Addresses.Where(x => x.Deleted));
                context.Translations.RemoveRange(context.Translations.Where(x => x.Deleted));
                context.Texts.RemoveRange(context.Texts.Where(x => x.Deleted));
                context.LocalisedResources.RemoveRange(context.LocalisedResources.Where(x => x.Deleted));
                context.Resources.RemoveRange(context.Resources.Where(x => x.Deleted));
                context.Languages.RemoveRange(context.Languages.Where(x => x.Deleted));
                context.TermStoreImports.RemoveRange(context.TermStoreImports.Where(x => x.Deleted));
                context.DocumentModelMetadataDefinitions.RemoveRange(context.DocumentModelMetadataDefinitions.Where(x => x.Deleted || x.MetadataDefinition.Deleted));
                context.MetadataDefinitions.RemoveRange(context.MetadataDefinitions.Where(x => x.Deleted));
                context.MetadataCategories.RemoveRange(context.MetadataCategories.Where(x => x.Deleted));
                context.DocumentModelAuthorRoles.RemoveRange(context.DocumentModelAuthorRoles.Where(x => x.Deleted || x.AuthorRole.Deleted));
                context.AuthorRoles.RemoveRange(context.AuthorRoles.Where(x => x.Deleted));
                context.SystemConfigurations.RemoveRange(context.SystemConfigurations.Where(x => x.Deleted));
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Removes self-referencing orga entities in correct order with cascading delete (i.e. all if an orga entity is deleted,
        /// all sub-entities are also removed.
        /// </summary>
        /// <param name="context">A <see cref="EurolookContext"/> database context.</param>
        private void RemoveOrgaEntities(EurolookContext context)
        {
            var orgaEntitiesToBeDeleted = context.OrgaEntities.Include(x => x.SubEntities).Where(x => x.Deleted).ToList();

            foreach (var orgaEntity in orgaEntitiesToBeDeleted)
            {
                var subEntities = GetSubEntities(orgaEntity, context);
                context.OrgaEntities.RemoveRange(subEntities);

                context.OrgaEntities.Remove(orgaEntity);
            }
        }

        /// <summary>
        /// Recursively enumerates all sub-entities of a given organizational entity.
        /// </summary>
        /// <param name="orgaEntity">The <see cref="OrgaEntity"/> whose sub-entities to enumerate.</param>
        /// <param name="context">An instance of a <see cref="EurolookContext"/> database context.</param>
        /// <returns>An <see cref="IEnumerable{OrgaEntity}"/> enumerating all descendent organizational entities.</returns>
        private IEnumerable<OrgaEntity> GetSubEntities(OrgaEntity orgaEntity, EurolookContext context)
        {
            context.OrgaEntities.Where(x => x.SuperEntityId == orgaEntity.Id).Load();

            if (orgaEntity.SubEntities != null)
            {
                var subEntities = orgaEntity.SubEntities.ToList();
                foreach (var subEntity in subEntities)
                {
                    foreach (var oe in GetSubEntities(subEntity, context))
                    {
                        yield return oe;
                    }

                    yield return subEntity;
                }
            }
        }

        internal ActivityTrack[] GetActivityTracksForUpload()
        {
            using (var context = GetContext())
            {
                var today = DateTime.UtcNow.Date;
                return context.ActivityTracks.Where(u => u.DateUtc < today).ToArray();
            }
        }

        internal void DeleteActivityTracks(ActivityTrack[] activityTracks)
        {
            using (var context = GetContext())
            {
                foreach (var track in activityTracks)
                {
                    context.ActivityTracks.Attach(track);
                }

                context.ActivityTracks.RemoveRange(activityTracks);
                context.SaveChanges();
            }
        }

        public void AddOrUpdateUserAuthorOnClient(UserAuthor userAuthor)
        {
            using (var context = GetContext())
            {
                userAuthor.ClientModification = false; // because it comes from the server
                context.UserAuthors.AddOrUpdate(ShallowClone.Clone(userAuthor));
                context.SaveChanges();
            }
        }

        public void AddOrUpdateUserSuperiorOnClient(UserSuperior userSuperior)
        {
            using (var context = GetContext())
            {
                context.UserSuperiors.AddOrUpdate(ShallowClone.Clone(userSuperior));
                context.SaveChanges();
            }
        }

        public void AddOrUpdateJobFunctionOnClient(JobFunction jobFunction)
        {
            using (var context = GetContext())
            {
                jobFunction.ClientModification = false; // because it comes from the server
                context.JobFunctions.AddOrUpdate(ShallowClone.Clone(jobFunction));
                context.SaveChanges();
            }
        }

        public void AddOrUpdateJobAssignmentOnClient(JobAssignment jobAssignment)
        {
            using (var context = GetContext())
            {
                jobAssignment.ClientModification = false; // because it comes from the server
                context.JobAssignments.AddOrUpdate(ShallowClone.Clone(jobAssignment));
                context.SaveChanges();
            }
        }

        public void AddOrUpdateWorkplaceOnClient(Workplace workplace)
        {
            using (var context = GetContext())
            {
                workplace.ClientModification = false; // because it comes from the server
                context.Workplaces.AddOrUpdate(ShallowClone.Clone(workplace));
                context.SaveChanges();
            }
        }

        public void AddOrUpdateAuthorOnClient(Author author)
        {
            using (var context = GetContext())
            {
                author.ClientModification = false; // because it comes from the server
                context.Authors.AddOrUpdate(ShallowClone.Clone(author));
                context.SaveChanges();
            }
        }

        public void AddOrUpdateDeviceSettingsOnClient(DeviceSettings deviceSettings)
        {
            using (var context = GetContext())
            {
                deviceSettings.ClientModification = false; // because it comes from the server
                context.DeviceSettings.AddOrUpdate(ShallowClone.Clone(deviceSettings));
                context.SaveChanges();
            }
        }

        public void AddOrUpdateDocumentModelSettingsOnClient(DocumentModelSettings documentModelSettings)
        {
            using (var context = GetContext())
            {
                documentModelSettings.ClientModification = false; // because it comes from the server
                context.DocumentModelSettings.AddOrUpdate(ShallowClone.Clone(documentModelSettings));
                context.SaveChanges();
            }
        }

        public void AddOrUpdateBrickSettingsOnClient(BrickSettings brickSettings)
        {
            using (var context = GetContext())
            {
                brickSettings.ClientModification = false; // because it comes from the server
                context.BrickSettings.AddOrUpdate(ShallowClone.Clone(brickSettings));
                context.SaveChanges();
            }
        }

        public void UpdateUserSettingsOnClient(UserSettings userSettings)
        {
            using (var context = GetContext())
            {
                userSettings.ClientModification = false; // because it comes from the server
                context.UserSettings.AddOrUpdate(ShallowClone.Clone(userSettings));
                context.SaveChanges();
            }
        }

        public void AddOrUpdateUserOnClient(User user)
        {
            using (var context = GetContext())
            {
                user.SetModification(ModificationType.ServerModification);
                context.Users.AddOrUpdate(ShallowClone.Clone(user));
                context.SaveChanges();
            }
        }

        internal void AddOrUpdateBuildingBlockBrickOnClient(BuildingBlockBrick buildingBlockBrick)
        {
            using (var context = GetContext())
            {
                buildingBlockBrick.ClientModification = false; // because it comes from the server
                context.Bricks.AddOrUpdate(ShallowClone.Clone(buildingBlockBrick));
                context.SaveChanges();
            }
        }

        internal void AddOrUpdateUserBrickOnClient(UserBrick userBrick)
        {
            using (var context = GetContext())
            {
                userBrick.ClientModification = false; // because it comes from the server
                context.UserBricks.AddOrUpdate(ShallowClone.Clone(userBrick));
                context.SaveChanges();
            }
        }

        public bool IsInitialized()
        {
            using (var context = GetContext())
            {
                return context.DatabaseState.AsNoTracking().Any(ds => ds.LastUpdateUtc != null);
            }
        }

        public User GetUserForLogging(string login)
        {
            using (var context = GetContext())
            {
                return context.Users.FirstOrDefault(u => u.Login == login);
            }
        }

        public void AddOrUpdateTemplateOwnerOnClient(TemplateOwner templateOwner)
        {
            using (var context = GetContext())
            {
                context.TemplateOwners.AddOrUpdate(ShallowClone.Clone(templateOwner));
                context.SaveChanges();
            }
        }

        public void AddOrUpdateTemplatePublicationOnClient(TemplatePublication templatePublication)
        {
            using (var context = GetContext())
            {
                context.TemplatePublications.AddOrUpdate(ShallowClone.Clone(templatePublication));
                context.SaveChanges();
            }
        }

        public void AddOrUpdateTemplateFileOnClient(TemplateFile templateFile)
        {
            using (var context = GetContext())
            {
                context.TemplateFiles.AddOrUpdate(ShallowClone.Clone(templateFile));
                context.SaveChanges();
            }
        }

        public void AddOrUpdateTemplateOnClient(Template template)
        {
            using (var context = GetContext())
            {
                context.Templates.AddOrUpdate(ShallowClone.Clone(template));
                context.SaveChanges();
            }
        }

        public void AddOrUpdateUserTemplateOnClient(UserTemplate userTemplate)
        {
            using (var context = GetContext())
            {
                userTemplate.ClientModification = false; // because it comes from the server
                context.UserTemplates.AddOrUpdate(ShallowClone.Clone(userTemplate));
                context.SaveChanges();
            }
        }
    }
}
