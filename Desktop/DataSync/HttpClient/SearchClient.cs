using System.Threading;
using System.Threading.Tasks;
using Eurolook.Data.Models;

namespace Eurolook.DataSync.HttpClient
{
    public class SearchClient : HttpClientBase
    {
        public SearchClient(string appUrl)
            : base(appUrl)
        {
        }

        public async Task<Author[]> SearchAuthors(
            CancellationToken token,
            string query,
            int page = 0,
            int pageSize = 20)
        {
            return await HttpGet<Author[]>(GetBaseUrl(), $"authors?q={query}&page={page}&pageSize={pageSize}", token);
        }

        public override string GetBaseUrl()
        {
            return $"{AppUrl}/search";
        }
    }
}
