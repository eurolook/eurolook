using System.Threading;
using System.Threading.Tasks;

namespace Eurolook.DataSync.HttpClient
{
    public class StatusClient : HttpClientBase
    {
        public StatusClient(string appUrl)
            : base(appUrl)
        {
        }

        public async Task<bool> IsAlive()
        {
            return await HttpGet<bool>(GetBaseUrl(), "isalive", CancellationToken.None);
        }

        public override string GetBaseUrl()
        {
            return $"{AppUrl}/status";
        }
    }
}
