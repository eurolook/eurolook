﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Eurolook.Common.Log;

namespace Eurolook.DataSync.HttpClient
{
    public abstract class HttpClientBase : ICanLog
    {
        private readonly uint _timeoutSeconds;
        private JwtToken _jwtToken;
        private DateTime _loginExpirationDate = DateTime.MinValue;

        protected HttpClientBase(string appUrl, uint timeoutSeconds = 240)
        {
            AppUrl = appUrl;
            _timeoutSeconds = timeoutSeconds;
        }

        protected string AppUrl { get; }

        public abstract string GetBaseUrl();

        public async Task<T> HttpGet<T>(string resourceBaseUrl, string request, CancellationToken cancelationToken)
        {
            var result = default(T);
            var client = GetHttpClient(resourceBaseUrl, request, _jwtToken, _timeoutSeconds);
            var response = await client.GetAsync(request, cancelationToken);
            if (response.IsSuccessStatusCode)
            {
                result = await response.Content.ReadAsAsync<T>(cancelationToken);
            }
            else
            {
                this.LogTraceFormat(
                    "HTTP GET {0} returned {1}.",
                    response.RequestMessage.RequestUri,
                    response.StatusCode);
            }

            return result;
        }

        /// <summary>
        /// Sends a HTTP GET request to the resource url ([BaseUrl]/[ResourceName]/[ResourceId]).
        /// </summary>
        public async Task<T> GetResource<T>(string resourceId)
        {
            string url = GetResourceUrl<T>();
            return await HttpGet<T>(url, resourceId, CancellationToken.None);
        }

        public async Task<T> HttpPost<T>(string resourceBaseUrl, string request, T resource)
        {
            var client = GetHttpClient(resourceBaseUrl, request, _jwtToken, _timeoutSeconds);
            var response = await client.PostAsJsonAsync(request, resource);
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<T>();
            }

            this.LogTraceFormat("HTTP POST {0} returned {1}.", response.RequestMessage.RequestUri, response.StatusCode);
            throw new HttpRequestException(response.ToString());
        }

        public async Task<T2> HttpPost<T1, T2>(string resourceBaseUrl, string request, T1 resource)
        {
            var client = GetHttpClient(resourceBaseUrl, request, _jwtToken, _timeoutSeconds);
            var response = await client.PostAsJsonAsync(request, resource);
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<T2>();
            }

            this.LogTraceFormat("HTTP POST {0} returned {1}.", response.RequestMessage.RequestUri, response.StatusCode);
            throw new HttpRequestException(response.ToString());
        }

        public async Task<T> HttpFormPost<T>(
            string resourceBaseUrl,
            string request,
            FormUrlEncodedContent content,
            Dictionary<string, string> headers = null)
        {
            var client = GetHttpClient(resourceBaseUrl, request, null, _timeoutSeconds);
            if (headers != null)
            {
                foreach (string key in headers.Keys)
                {
                    client.DefaultRequestHeaders.Add(key, headers[key]);
                }
            }

            var response = await client.PostAsync(request, content);
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<T>();
            }

            this.LogTraceFormat("HTTP POST {0} returned {1}.", response.RequestMessage.RequestUri, response.StatusCode);
            throw new HttpRequestException(response.ToString());
        }

        public async Task<T> Post<T>(T resource)
        {
            string url = GetResourceUrl<T>();
            return await HttpPost(url, null, resource);
        }

        public async Task<T> Post<T>(string request, T resource)
        {
            string url = GetResourceUrl<T>();
            return await HttpPost(url, request, resource);
        }

        public async Task<T> HttpPut<T>(string resourceBaseUrl, string request, T resource)
        {
            T result;
            var client = GetHttpClient(resourceBaseUrl, request, _jwtToken, _timeoutSeconds);
            var response = await client.PutAsJsonAsync(request, resource);
            if (response.IsSuccessStatusCode)
            {
                result = await response.Content.ReadAsAsync<T>();
            }
            else
            {
                this.LogTraceFormat(
                    "HTTP PUT {0} returned {1}.",
                    response.RequestMessage.RequestUri,
                    response.StatusCode);
                throw new HttpRequestException(response.ToString());
            }

            return result;
        }

        /// <summary>
        /// Sends a HTTP PUT request to the resource url ([BaseUrl]/[ResourceName]/[ResourceId]).
        /// </summary>
        public async Task<T> Put<T>(string request, T resource)
        {
            string url = GetResourceUrl<T>();
            return await HttpPut(url, request, resource);
        }

        public async Task<bool> HttpDelete(string resourceBaseUrl, string request)
        {
            var client = GetHttpClient(resourceBaseUrl, request, _jwtToken, _timeoutSeconds);
            var response = await client.DeleteAsync(request);
            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            this.LogTraceFormat(
                "HTTP DELETE {0} returned {1}.",
                response.RequestMessage.RequestUri,
                response.StatusCode);
            throw new HttpRequestException(response.ToString());
        }

        /// <summary>
        /// Sends a HTTP DELETE request to the resource url ([BaseUrl]/[ResourceName]/[ResourceId]).
        /// </summary>
        /// <returns>
        /// A <see cref="Task" /> representing the asynchronous operation. The result of the task is
        /// <code>true</code> if the operation was successful.
        /// </returns>
        public async Task<bool> Delete<T>(string request)
        {
            string url = GetResourceUrl<T>();
            return await HttpDelete(url, request);
        }

        public async Task<LoginResult> Login()
        {
            return await Login(
                DataSyncSettings.Instance.DataSyncServiceUser,
                SecretsProviderRetriever.SecretsProvider.RetrieveDataSyncServicePassword(DataSyncSettings.Instance.DataSyncServicePasswordEncrypted));
        }

        public async Task<LoginResult> Login(string user, string password)
        {
            try
            {
                // don't login when there is still a valid token
                if (_jwtToken != null && (_loginExpirationDate > DateTime.Now))
                {
                    return new LoginResult
                    {
                        IsSuccess = true,
                    };
                }

                var headers = new Dictionary<string, string>
                {
                    { "Audience", "Any" },
                };
                var content = new FormUrlEncodedContent(
                    new Dictionary<string, string>
                    {
                        { "username", user },
                        { "password", password },
                        { "grant_type", "password" },
                    });

                string baseUrl = Regex.Replace(AppUrl, "/api/v\\d", "/oauth2/token");
                _jwtToken = await HttpFormPost<JwtToken>(baseUrl, "", content, headers);

                // set _loginExpirationDate to 1 minute before the token really expires
                _loginExpirationDate = DateTime.Now.AddSeconds(Convert.ToDouble(_jwtToken.ExpiresIn) - 60);
                return new LoginResult
                {
                    IsSuccess = true,
                };
            }
            catch (Exception ex)
            {
                _jwtToken = null;
                return new LoginResult
                {
                    IsSuccess = false,
                    Error = ex,
                };
            }
        }

        private static Uri GetBaseUri(string baseUrl, string resource)
        {
            if (!string.IsNullOrWhiteSpace(resource) && !baseUrl.EndsWith("/", StringComparison.Ordinal))
            {
                return new Uri(baseUrl + "/");
            }

            return new Uri(baseUrl);
        }

        private static System.Net.Http.HttpClient GetHttpClient(string baseUrl, string resource, JwtToken token, uint timeoutSeconds)
        {
            var client = new System.Net.Http.HttpClient(new HttpClientHandler { UseProxy = false });
            ////var client = new HttpClient(new HttpClientHandler
            ////{
            ////    UseProxy = true,
            ////    Proxy = new WebProxy("http://localhost:8888", false, new string[] { }),
            ////});
            client.DefaultRequestHeaders.ExpectContinue = false;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.BaseAddress = GetBaseUri(baseUrl, resource);
            client.Timeout = TimeSpan.FromSeconds(timeoutSeconds);

            if (token != null)
            {
                client.DefaultRequestHeaders.Add("Authorization", token.TokenType + " " + token.AccessToken);
            }

            return client;
        }

        private string GetResourceUrl<T>()
        {
            string baseUrl = GetBaseUrl();
            string format = "{0}/{1}";
            if (baseUrl.EndsWith("/", StringComparison.Ordinal))
            {
                format = "{0}{1}";
            }

            return string.Format(format, baseUrl, typeof(T).Name);
        }

        public class LoginResult
        {
            public bool IsSuccess { get; set; }

            public Exception Error { get; set; }
        }
    }
}
