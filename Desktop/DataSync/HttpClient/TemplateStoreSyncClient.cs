﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Eurolook.Data.Models.TemplateStore;
using Eurolook.Data.TemplateStore;

namespace Eurolook.DataSync.HttpClient;

public class TemplateStoreSyncClient : HttpClientBase
{
    public TemplateStoreSyncClient(string appUrl, uint timeoutSeconds)
        : base(appUrl, timeoutSeconds)
    {
    }

    public async Task<UserTemplate[]> GetUserTemplates(string login, uint modifiedSinceHours, CancellationToken token)
    {
        return await HttpGet<UserTemplate[]>(GetBaseUrl(), $"usertemplates?login={login}&modifiedSinceHours={modifiedSinceHours}", token);
    }

    public async Task<TemplateOwnerInfo[]> GetOwners(Guid templateId, CancellationToken token)
    {
        return await HttpGet<TemplateOwnerInfo[]>(GetBaseUrl(), $"templates/{templateId}/owners", token);
    }

    public async Task<string> SaveAsUserTemplate(string login, Guid templateId)
    {
        return await HttpPut(GetBaseUrl(), $"usertemplates/{templateId}?login={login}", "");
    }

    public async Task<bool> DeleteFromUserTemplates(string login, Guid templateId)
    {
        return await HttpDelete(GetBaseUrl(), $"usertemplates/{templateId}?login={login}");
    }

    public async Task<Template> DownloadTemplate(string login, Guid templateId, CancellationToken token)
    {
        return await HttpGet<Template>(GetBaseUrl(), $"templates/{templateId}/download?login={login}", token);
    }

    public override string GetBaseUrl()
    {
        return $"{AppUrl}/TemplateStoreSync";
    }
}
