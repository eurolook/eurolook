using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.ExceptionLog;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;

namespace Eurolook.DataSync.HttpClient
{
    public class DataSyncHttpClient : HttpClientBase, IDataSyncClient
    {
        private readonly string _clientVersion;

        public DataSyncHttpClient(string appUrl)
            : base(appUrl)
        {
            _clientVersion = Assembly.GetAssembly(typeof(DataSyncHttpClient)).GetName().Version.ToString();
        }

        public override string GetBaseUrl()
        {
            return $"{AppUrl}/datasync";
        }

        public async Task<UpdatePlan> GetUpdates(UpdateRequest request)
        {
            return await HttpPost<UpdateRequest, UpdatePlan>(GetBaseUrl(), $"updates?clientVersion={_clientVersion}", request);
        }

        public async Task<User> GetUserProfile(string login)
        {
            return await HttpGet<User>(GetBaseUrl(), $"userprofile?login={login}&clientVersion={_clientVersion}", CancellationToken.None);
        }

        public async Task<User> CreateUser(User user)
        {
            return await Post($"?clientVersion={_clientVersion}", user);
        }

        public async Task<Template> GetTemplate(Guid id)
        {
            return await HttpGet<Template>(GetBaseUrl(), $"template?id={id}&clientVersion={_clientVersion}", CancellationToken.None);
        }

        public async Task<Author> UpdateAuthor(Guid id, Author author, string originator)
        {
            return await Put($"{id}?clientVersion={_clientVersion}&originator={originator}", author);
        }

        public async Task<UserSettings> UpdateUserSettings(Guid id, UserSettings settings)
        {
            return await Put($"{id}?clientVersion={_clientVersion}", settings);
        }

        public async Task<DocumentModelSettings> AddOrUpdateDocumentModelSettings(Guid id, DocumentModelSettings settings)
        {
            return await Put($"{id}?clientVersion={_clientVersion}", settings);
        }

        public async Task<DeviceSettings> AddOrUpdateDeviceSettings(Guid id, DeviceSettings settings)
        {
            return await Put($"{id}?clientVersion={_clientVersion}", settings);
        }

        public async Task<BrickSettings> AddOrUpdateBrickSettings(Guid id, BrickSettings settings)
        {
            return await Put($"{id}?clientVersion={_clientVersion}", settings);
        }

        public async Task<JobFunction> AddOrUpdateJobFunction(Guid id, JobFunction jobFunction)
        {
            return await Put($"{id}?clientVersion={_clientVersion}", jobFunction);
        }

        public async Task<UserAuthor> AddOrUpdateUserAuthor(Guid id, UserAuthor userAuthor)
        {
            return await Put($"{id}?clientVersion={_clientVersion}", userAuthor);
        }

        public async Task<Workplace> AddOrUpdateWorkplace(Guid id, Workplace workplace)
        {
            return await Put($"{id}?clientVersion={_clientVersion}", workplace);
        }

        public async Task<JobAssignment> AddOrUpdateJobAssignment(Guid id, JobAssignment jobAssignment)
        {
            return await Put($"{id}?clientVersion={_clientVersion}", jobAssignment);
        }

        public async Task<UserBrick> AddOrUpdateUserBrick(Guid id, UserBrick userBrick)
        {
            var userBrickClone = ShallowClone.Clone(userBrick);
            return await Put($"{id}?clientVersion={_clientVersion}", userBrickClone);
        }

        public async Task<BuildingBlockBrick> AddOrUpdateBuildingBlockBrick(Guid id, BuildingBlockBrick buildingBlockBrick)
        {
            return await Put($"{id}?clientVersion={_clientVersion}", buildingBlockBrick);
        }

        public async Task<UserTemplate> AddOrUpdateUserTemplate(Guid id, UserTemplate userTemplate)
        {
            var userTemplateClone = ShallowClone.Clone(userTemplate);
            return await Put($"{id}?clientVersion={_clientVersion}", userTemplateClone);
        }

        public async Task<bool> UploadPerformanceLogs(PerformanceLog[] performanceLogs)
        {
            return await HttpPost<PerformanceLog[], bool>(GetBaseUrl(), $"performancelogs?clientVersion={_clientVersion}", performanceLogs);
        }

        public async Task<bool> UploadActionLogs(ActionLog[] actionLogs)
        {
            return await HttpPost<ActionLog[], bool>(GetBaseUrl(), $"actionlogs?clientVersion={_clientVersion}", actionLogs);
        }

        public async Task<bool> UploadExceptionLogs(ExceptionBucket[] exceptionBuckets, ExceptionLogEntry[] exceptionLogs)
        {
            bool result = await HttpPost<ExceptionBucket[], bool>(
                GetBaseUrl(),
                $"exceptionbuckets?clientVersion={_clientVersion}",
                exceptionBuckets);

            result &= await HttpPost<ExceptionLogEntry[], bool>(
                GetBaseUrl(),
                $"exceptionlogentries?clientVersion={_clientVersion}",
                exceptionLogs);

            return result;
        }

        public async Task<bool> UploadActivityTracks(ActivityTrack[] activityTracks)
        {
            return await HttpPost<ActivityTrack[], bool>(GetBaseUrl(), $"activitytracks?clientVersion={_clientVersion}", activityTracks);
        }

        public async Task<bool> ConfirmRemoteWipe(string login, string deviceName)
        {
            return await HttpPost<string, bool>(
                GetBaseUrl(),
                $"confirmremotewipe?deviceName={deviceName}&clientVersion={_clientVersion}",
                login);
        }

        public async Task<bool> UploadDeviceInformation(DeviceInfo deviceInfo, string userName)
        {
            return await HttpPost<DeviceInfo, bool>(
                GetBaseUrl(),
                $"deviceinfo?userName={userName}&clientVersion={_clientVersion}",
                deviceInfo);
        }
    }
}
