namespace Eurolook.DataSync.HttpClient
{
    public class UserFeedbackClient : HttpClientBase
    {
        public UserFeedbackClient(string appUrl)
            : base(appUrl)
        {
        }

        public override string GetBaseUrl()
        {
            return AppUrl;
        }
    }
}
