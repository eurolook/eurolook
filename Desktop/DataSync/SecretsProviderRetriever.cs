﻿using System;
using System.IO;
using Eurolook.Common;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Common.SecretsProvider;
#if NET6_0_OR_GREATER
using System.Runtime.Loader;
#else
using System.Reflection;
#endif

namespace Eurolook.DataSync
{
    public class SecretsProviderRetriever : ICanLog
    {
        protected SecretsProviderRetriever()
        {
        }

        public static ISecretsProvider SecretsProvider { get; } = GetProvider();

        private static ISecretsProvider GetProvider()
        {
            try
            {
                if (!DataSyncSettings.Instance.DataSyncServiceSecretsProviderAssembly.IsNullOrEmpty())
                {
                    var directory = AppContext.BaseDirectory;
                    var dllFile = Path.Combine(
                        directory,
                        DataSyncSettings.Instance.DataSyncServiceSecretsProviderAssembly);
                    if (File.Exists(dllFile))
                    {
#if NET6_0_OR_GREATER
                        var assembly = AssemblyLoadContext.Default.LoadFromAssemblyPath(dllFile);
#else
                        var assembly = Assembly.LoadFile(dllFile);
#endif
                        Type type = assembly.GetType(DataSyncSettings.Instance.DataSyncServiceSecretsProviderClass);
                        if (type != null && Activator.CreateInstance(type) is ISecretsProvider result)
                        {
                            LogManager.GetLogger(typeof(SecretsProviderRetriever)).Debug(
                                $"Successfully loaded SecretsProvider from {DataSyncSettings.Instance.DataSyncServiceSecretsProviderAssembly}");
                            return result;
                        }
                    }

                    throw new EurolookConfigurationException(
                        $"Failed to retrieve secrets provider from {DataSyncSettings.Instance.DataSyncServiceSecretsProviderAssembly}");
                }
            }
            catch (Exception e)
            {
                LogManager.GetLogger(typeof(SecretsProviderRetriever)).Error(
                    $"Failed to retrieve secrets provider from {DataSyncSettings.Instance.DataSyncServiceSecretsProviderAssembly}",
                    e);
            }

            return new DefaultSecretsProvider();
        }
    }
}
