namespace Eurolook.DataSync
{
    public enum DataSyncResult
    {
        Success = 0,
        Error = 1,
        FinishedWithErrors = 2,
        NoConnection = 3,
        RemoteWipe = 4,
        LoginFailed = 5,
        Aborted = 98,
        UnexpectedError = 99,
        InvalidParameters = 100,
    }
}
