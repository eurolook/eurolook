﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Eurolook.Common;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;
using Eurolook.DataSync.Database;
using Eurolook.DataSync.HttpClient;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace Eurolook.DataSync
{
    public class DataSync : ICanLog
    {
        private IDataSyncClient _dataSyncClient;
        private HttpClientBase.LoginResult _login;

        public DataSyncResult Result { get; set; }

        public static Task WaitForExit()
        {
            return Task.Factory.StartNew(
                () =>
                {
                    while (true)
                    {
                        if (Process.GetProcessesByName("DataSync").Any())
                        {
                            Thread.Sleep(1000);
                        }
                        else
                        {
                            break;
                        }
                    }
                },
                CancellationToken.None,
                TaskCreationOptions.LongRunning,
                TaskScheduler.Default);
        }

        public async Task RunAsync(Options options)
        {
            Result = DataSyncResult.Success;
            try
            {
                var sw = new Stopwatch();
                if (new DataSyncDatabase().EnsureCreated())
                {
                    this.LogInfo($"Created local database in {sw.ElapsedMilliseconds}ms.");
                }

                this.LogInfo("DataSync started.");
                string userName = string.IsNullOrWhiteSpace(options.User) ? Windows.Login : options.User;
                string deviceName = Environment.MachineName;

                var httpSyncClient = new DataSyncHttpClient(options.Server);
                this.LogInfoFormat("Using server {0}", httpSyncClient.GetBaseUrl());

                _login =
                    await
                        httpSyncClient.Login(
                            DataSyncSettings.Instance.DataSyncServiceUser,
                            SecretsProviderRetriever.SecretsProvider.RetrieveDataSyncServicePassword(DataSyncSettings.Instance.DataSyncServicePasswordEncrypted));
                if (!_login.IsSuccess)
                {
                    if (_login.Error is HttpRequestException)
                    {
                        this.LogWarn("DataSync is offline.", _login.Error);
                        Result = DataSyncResult.NoConnection;
                    }
                    else
                    {
                        this.LogError("DataSync failed to login.", _login.Error);
                        Result = DataSyncResult.LoginFailed;
                    }

                    return;
                }

                _dataSyncClient = httpSyncClient;
                this.LogInfo("Logged in successfully");

                if (options.IsMiniMode)
                {
                    this.LogInfo("DataSync is in MINI MODE");
                    await ExecuteMiniMode(userName);
                }
                else
                {
                    await ExecuteFullMode(userName, deviceName);
                }

                if (Result == DataSyncResult.Success)
                {
                    this.LogInfo("DataSync finished SUCCESSFULLY");
                }
                else if (Result == DataSyncResult.RemoteWipe)
                {
                    this.LogInfo("DataSync finished with a REMOTE WIPE");
                }
                else
                {
                    this.LogInfo("DataSync finished WITH ERRORS.");
                }
            }
            catch (Exception ex)
            {
                this.LogError("DataSync failed.", ex);
#if DEBUG
                Console.WriteLine(ex);
#endif
                Result = DataSyncResult.Error;
            }
            finally
            {
                try
                {
                    if (_login?.IsSuccess == true && Result != DataSyncResult.RemoteWipe)
                    {
                        await UploadExceptionLogsAsync();
                        new DataSyncDatabase().SetLastDataSyncState(Result);
                    }
                }
                catch (Exception ex)
                {
                    this.LogError(ex);
                }
            }
        }

        private async Task ExecuteMiniMode(string userName)
        {
            this.LogDebug("Uploading device information...");
            var deviceInfoRetriever = new DeviceInfoRetriever();
            await _dataSyncClient.UploadDeviceInformation(deviceInfoRetriever.GetDeviceInfo(), userName);
        }

        private async Task ExecuteFullMode(string userName, string deviceName)
        {
            var db = new DataSyncDatabase();
            bool isInitialization = !db.IsInitialized();
            if (isInitialization)
            {
                this.LogInfo("DataSync is in INITIALIZATION MODE");
            }

            var updates = await RequestUpdates(userName, deviceName);
            if (updates.IsRemoteWipeRequested)
            {
                DropDatabaseForRemoteWipe(db);
                await ConfirmRemoteWipeAsync(userName, deviceName);
                this.LogInfo("DataSync SUCCESSFULLY dropped the database.");
            }
            else
            {
                if (isInitialization)
                {
                    this.LogInfo("Dropping database for initialization mode...");
                    db.EnsureDeleted();
                    db.EnsureCreated();
                }

                await InsertOrUpdateEurolookDataAsync(updates, isInitialization);
                await SynchronizeProfileAsync(userName);

                if (!isInitialization)
                {
                    await UploadActionLogs();
                    await UploadActivityTracks();
                    await UploadPerformanceLogs();
                }
            }
        }

        private async Task ConfirmRemoteWipeAsync(string login, string deviceName)
        {
            if (await _dataSyncClient.ConfirmRemoteWipe(login, deviceName))
            {
                this.LogInfo("Confirmed remote wipe.");
            }
            else
            {
                this.LogInfo("FAILED to confirm remote wipe.");
            }
        }

        private void DropDatabaseForRemoteWipe(DataSyncDatabase db)
        {
            this.LogInfoFormat("Received remote wipe request, deleting database...");
            db.EnsureDeleted();
            Result = DataSyncResult.RemoteWipe;
        }

        private async Task UploadPerformanceLogs()
        {
            this.LogDebug("Uploading performance logs...");
            var sw = Stopwatch.StartNew();

            var db = new DataSyncDatabase();
            var performanceLogs = db.GetPerformanceLogs();
            if (performanceLogs != null && performanceLogs.Any())
            {
                if (await _dataSyncClient.UploadPerformanceLogs(performanceLogs))
                {
                    this.LogInfoFormat("Uploaded {0} performance log entries.", performanceLogs.Length);
                    db.DeletePerformanceLogs(performanceLogs);
                    this.LogInfo("Cleared performance log.");
                }
                else
                {
                    this.LogError("FAILED to upload performance log data. Will try again next time.");
                    Result = DataSyncResult.FinishedWithErrors;
                }

                this.LogInfo($"Uploaded performance logs in {sw.ElapsedMilliseconds}ms");
            }
            else
            {
                this.LogInfo("No performance log data was found.");
            }
        }

        private async Task UploadActionLogs()
        {
            this.LogDebug("Uploading action logs...");
            var sw = Stopwatch.StartNew();

            var db = new DataSyncDatabase();
            var actionLogs = db.GetActionLogs();
            if (actionLogs != null && actionLogs.Any())
            {
                if (await _dataSyncClient.UploadActionLogs(actionLogs))
                {
                    this.LogInfoFormat("Uploaded {0} action log entries.", actionLogs.Length);
                    db.DeleteActionLogs(actionLogs);
                    this.LogInfo("Cleared action log.");
                }
                else
                {
                    this.LogError("FAILED to upload action log data. Will try again next time.");
                    Result = DataSyncResult.FinishedWithErrors;
                }

                this.LogInfo($"Uploaded action logs in {sw.ElapsedMilliseconds}ms");
            }
            else
            {
                this.LogInfo("No action log data was found.");
            }
        }

        private async Task UploadActivityTracks()
        {
            this.LogDebug("Uploading activity tracks...");
            var sw = Stopwatch.StartNew();

            var db = new DataSyncDatabase();
            var activityTracks = db.GetActivityTracksForUpload();
            if (activityTracks != null && activityTracks.Any())
            {
                if (await _dataSyncClient.UploadActivityTracks(activityTracks))
                {
                    this.LogInfoFormat("Uploaded {0} activity tracks.", activityTracks.Length);
                    db.DeleteActivityTracks(activityTracks);
                    this.LogInfo("Cleared activity tracks.");
                }
                else
                {
                    this.LogError("FAILED to upload activity tracks. Will try again next time.");
                    Result = DataSyncResult.FinishedWithErrors;
                }

                this.LogInfo($"Uploaded activity tracks in {sw.ElapsedMilliseconds}ms");
            }
            else
            {
                this.LogInfo("No usage tracks were found.");
            }
        }

        private async Task UploadExceptionLogsAsync()
        {
            this.LogDebug("Uploading exception logs...");
            var sw = Stopwatch.StartNew();

            var db = new DataSyncDatabase();
            var exceptionBuckets = db.GetExceptionBuckets();
            if (exceptionBuckets == null || !exceptionBuckets.Any())
            {
                this.LogInfo("No exception log data was found.");
                return;
            }

            var exceptionLogEntries = db.GetExceptionLogEntries();
            if (exceptionLogEntries == null || !exceptionLogEntries.Any())
            {
                this.LogInfo("No exception log data was found.");
                return;
            }

            bool isUploadSuccessful = await _dataSyncClient.UploadExceptionLogs(exceptionBuckets, exceptionLogEntries);
            if (isUploadSuccessful)
            {
                this.LogInfoFormat("Uploaded {0} log entries in {1} buckets.", exceptionLogEntries.Length, exceptionBuckets.Length);
                db.DeleteExceptionLogs();
                this.LogInfo("Cleared exception log.");
            }
            else
            {
                this.LogError("FAILED to upload exception log data. Will try again next time.");
                Result = DataSyncResult.FinishedWithErrors;
            }

            this.LogInfo($"Uploaded exception logs in {sw.ElapsedMilliseconds}ms");
        }

        private async Task SynchronizeProfileAsync(string login)
        {
            var localUserProfile = LoadLocalUserProfile(login);
            if (localUserProfile == null)
            {
                // no local profile present, download or create it, no need to sync
                this.LogInfo($"Could not find a local profile for user '{login}'.");
                if (!string.IsNullOrWhiteSpace(login))
                {
                    var serverUserProfile = await DownloadUserProfile(login);
                    if (serverUserProfile == null)
                    {
                        this.LogInfo($"Could not find a server profile for user '{login}'");
                        serverUserProfile = await CreateUserProfile(login);
                    }

                    if (serverUserProfile != null)
                    {
                        SaveUserProfile(serverUserProfile);
                    }
                    else
                    {
                        throw new Exception("Failed to download or create user profile.");
                    }
                }
            }
            else
            {
                // local profile present, just sync
                var serverUserProfile = await DownloadUserProfile(localUserProfile.Login);
                if (serverUserProfile != null)
                {
                    await SynchronizeUserDataAsync(login, localUserProfile, serverUserProfile);
                }
                else
                {
                    throw new Exception("Failed to download user profile.");
                }
            }
        }

        private User LoadLocalUserProfile(string login)
        {
            var sw = Stopwatch.StartNew();
            this.LogDebug("Loading local user profile from database...");
            var db = new DataSyncDatabase();
            var localUserProfile = db.GetUserProfileForSync(login);
            this.LogInfo($"Loaded local user profile in {sw.ElapsedMilliseconds}ms");
            return localUserProfile;
        }

        private void SaveUserProfile(User serverUserProfile)
        {
            var sw = Stopwatch.StartNew();
            var db = new DataSyncDatabase();
            db.AddUserProfile(serverUserProfile);
            this.LogInfo($"Saved user profile in local database in {sw.ElapsedMilliseconds}ms");
            sw.Stop();
        }

        private async Task<User> DownloadUserProfile(string login)
        {
            var sw = Stopwatch.StartNew();
            this.LogDebug("Downloading user profile from server...");
            var user = await _dataSyncClient.GetUserProfile(login);
            this.LogInfo($"Downloaded user profile in {sw.ElapsedMilliseconds}ms");
            sw.Stop();
            return user;
        }

        private async Task<User> CreateUserProfile(string login)
        {
            var sw = Stopwatch.StartNew();
            this.LogDebug("Creating user profile on server...");
            var user = await _dataSyncClient.CreateUser(new User { Login = login });
            this.LogInfo($"Created user profile in {sw.ElapsedMilliseconds}ms");
            sw.Stop();
            return user;
        }

        [NotNull]
        private async Task<UpdatePlan> RequestUpdates(string login, string deviceName)
        {
            var db = new DataSyncDatabase();
            var updateRequest = new UpdateRequest
            {
                MinimumServerModificationTimeUTC = db.GetLastUpdateUtc(),
                Login = login,
                DeviceName = deviceName,
            };

            this.LogInfo($"Requesting updates from server... (MinimumServerModificationTimeUTC={updateRequest.MinimumServerModificationTimeUTC:s})");

            var sw = Stopwatch.StartNew();
            var updates = await _dataSyncClient.GetUpdates(updateRequest);
            if (updates == null)
            {
                throw new Exception(
                    "Failed to request updates, likely due to a broken HTTP connection or server error");
            }

            this.LogInfo($"Updates retrieved in {sw.ElapsedMilliseconds}ms");
            sw.Stop();
            return updates;
        }

        private async Task InsertOrUpdateEurolookDataAsync(UpdatePlan updates, bool isInitialization)
        {
            try
            {
                this.LogDebug("Updating data on client...");
                var sw = Stopwatch.StartNew();
                var db = new DataSyncDatabase();
                await db.InsertOrUpdateEurolookDataAsync(updates, isInitialization);
                string action = "Inserted";
                if (!isInitialization)
                {
                    action = "Updated";
                    await db.UpdateDiscriminators(updates);
                }

                this.LogInfo($"{action} basic Eurolook data in {sw.ElapsedMilliseconds}ms");
            }
            catch (ValidationException ex)
            {
                this.LogError("FAILED to insert or update basic Eurolook data.\nValidation Error:\n", ex);
                this.LogErrorFormat(
                        "Property: {0} Error: {1}",
                        ex.ValidationAttribute,
                        ex.Message);
                Result = DataSyncResult.FinishedWithErrors;
            }
            catch (DbUpdateException ex)
            {
                string message = FindExceptionMessage(ex);
                string entities = string.Join(", ", ex.Entries.Select(e => e.Entity.GetType() + $" ({(e.Entity as Identifiable)?.Id})"));
                this.LogError(
                    $"FAILED to insert or update basic Eurolook data. \n{message}\nThere is probably inconsistent data:\n{entities}");
                Result = DataSyncResult.FinishedWithErrors;
            }
        }

        private async Task SynchronizeUserDataAsync(string login, User localUserProfile, User serverUserProfile)
        {
            var sw = new Stopwatch();
            try
            {
                this.LogDebug($"Synchronizing user profile data for '{localUserProfile.Login}'...");

                var db = new DataSyncDatabase();

                // user
                db.AddOrUpdateUserOnClient(serverUserProfile);

                // user settings
                await
                    SynchronizeAsync(
                        localUserProfile.Settings,
                        serverUserProfile.Settings,
                        db.UpdateUserSettingsOnClient,
                        _dataSyncClient.UpdateUserSettings);

                // local user templates
                foreach (var localUserTemplate in localUserProfile.Templates)
                {
                    var serverUserTemplate = serverUserProfile.Templates?.FirstOrDefault(x => x.Id == localUserTemplate.Id);
                    await SynchronizeUserTemplate(localUserTemplate, serverUserTemplate);
                }

                // server user templates
                if (serverUserProfile.Templates != null)
                {
                    foreach (var serverUserTemplate in serverUserProfile.Templates)
                    {
                        if (localUserProfile.Templates.Any(x => x.Id == serverUserTemplate.Id))
                        {
                            continue;
                        }

                        var localUserTemplate =
                            localUserProfile.Templates?.FirstOrDefault(x => x.Id == serverUserTemplate.Id);
                        await SynchronizeUserTemplate(localUserTemplate, serverUserTemplate);
                    }
                }

                // document/template settings
                foreach (var localDocSettings in localUserProfile.Settings.DocumentModelSettings)
                {
                    var serverDocSettings =
                        serverUserProfile.Settings.DocumentModelSettings.FirstOrDefault(dms => dms.Id == localDocSettings.Id);

                    if (serverDocSettings?.Template != null)
                    {
                        db.AddOrUpdateTemplateOnClient(serverDocSettings.Template);
                    }

                    await
                        SynchronizeAsync(
                            localDocSettings,
                            serverDocSettings,
                            db.AddOrUpdateDocumentModelSettingsOnClient,
                            _dataSyncClient.AddOrUpdateDocumentModelSettings);
                }

                // device settings
                foreach (var localDeviceSettings in localUserProfile.Settings.DeviceSettings)
                {
                    var serverDeviceSettings =
                        serverUserProfile.Settings.DeviceSettings.FirstOrDefault(dms => dms.Id == localDeviceSettings.Id);
                    await
                        SynchronizeAsync(
                            localDeviceSettings,
                            serverDeviceSettings,
                            db.AddOrUpdateDeviceSettingsOnClient,
                            _dataSyncClient.AddOrUpdateDeviceSettings);
                }

                // brick settings
                foreach (var localBrickSettings in localUserProfile.Settings.BrickSettings)
                {
                    var serverBrickSettings =
                        serverUserProfile.Settings.BrickSettings.FirstOrDefault(x => x.Id == localBrickSettings.Id);
                    await
                        SynchronizeAsync(
                            localBrickSettings,
                            serverBrickSettings,
                            db.AddOrUpdateBrickSettingsOnClient,
                            _dataSyncClient.AddOrUpdateBrickSettings);
                }

                // user bricks
                foreach (var localUserBrick in localUserProfile.Bricks)
                {
                    var serverUserBrick = serverUserProfile.Bricks?.FirstOrDefault(x => x.Id == localUserBrick.Id);
                    await SynchronizeAsync(
                        localUserBrick.Brick,
                        serverUserBrick?.Brick,
                        db.AddOrUpdateBuildingBlockBrickOnClient,
                        _dataSyncClient.AddOrUpdateBuildingBlockBrick);
                    await SynchronizeAsync(
                        localUserBrick,
                        serverUserBrick,
                        db.AddOrUpdateUserBrickOnClient,
                        _dataSyncClient.AddOrUpdateUserBrick);
                }

                // your own author data
                await SyncAuthorDataClientWins(login, localUserProfile.Self, serverUserProfile.Self, db);

                // your local favorites
                foreach (var localUserAuthor in localUserProfile.Authors)
                {
                    var serverUserAuthor = serverUserProfile.Authors.FirstOrDefault(ua => ua.Id == localUserAuthor.Id);
                    if (localUserAuthor?.Author != null && !localUserAuthor.Deleted &&
                        serverUserAuthor != null && !serverUserAuthor.Deleted)
                    {
                        await SyncAuthorDataClientWins(login, localUserAuthor.Author, serverUserAuthor.Author, db);
                    }

                    await SynchronizeAsync(localUserAuthor, serverUserAuthor, db.AddOrUpdateUserAuthorOnClient, _dataSyncClient.AddOrUpdateUserAuthor);
                }

                // new favorites from the server
                foreach (var serverUserAuthor in serverUserProfile.Authors)
                {
                    if (localUserProfile.Authors.Any(ua => ua.Id == serverUserAuthor.Id))
                    {
                        continue;
                    }

                    await SyncAuthorDataClientWins(login, null, serverUserAuthor?.Author, db);
                    await SynchronizeAsync(null, serverUserAuthor, db.AddOrUpdateUserAuthorOnClient, _dataSyncClient.AddOrUpdateUserAuthor);
                }

                // your local superiors
                foreach (var localUserSuperior in localUserProfile.Superiors)
                {
                    var serverUserSuperior = serverUserProfile.Superiors.FirstOrDefault(x => x.Id == localUserSuperior.Id);
                    if (localUserSuperior?.Author != null && !localUserSuperior.Deleted &&
                        serverUserSuperior != null && !serverUserSuperior.Deleted)
                    {
                        await SyncAuthorDataClientWins(login, localUserSuperior.Author, serverUserSuperior.Author, db);
                    }

                    db.AddOrUpdateUserSuperiorOnClient(serverUserSuperior);
                }

                // new superiors from the server
                foreach (var serverUserSuperior in serverUserProfile.Superiors)
                {
                    if (localUserProfile.Superiors.Any(ua => ua.Id == serverUserSuperior.Id))
                    {
                        continue;
                    }

                    await SyncAuthorDataClientWins(login, null, serverUserSuperior?.Author, db);
                    db.AddOrUpdateUserSuperiorOnClient(serverUserSuperior);
                }
            }
            finally
            {
                this.LogInfo($"Synchronized user profile data in {sw.ElapsedMilliseconds}ms");
                sw.Stop();
            }
        }

        private async Task SynchronizeUserTemplate(UserTemplate localUserTemplate, UserTemplate serverUserTemplate)
        {
            // for the template itself: server wins (Template is Updatable)
            if (serverUserTemplate?.Template != null)
            {
                // the template was already referenced by the user profile,
                // therefore it's included in the user profile: use it.
                AddOrUpdateTemplateData(serverUserTemplate.Template);
            }
            else if (localUserTemplate?.ClientModification == true)
            {
                // the template referenced was just added by the client,
                // it's not included in the user profile: download it.
                await DownloadAndUpdateTemplate(localUserTemplate.TemplateId);
            }

            // for the relation: synchronize
            var db = new DataSyncDatabase();
            await SynchronizeAsync(
                localUserTemplate,
                serverUserTemplate,
                db.AddOrUpdateUserTemplateOnClient,
                _dataSyncClient.AddOrUpdateUserTemplate);
        }

        private async Task DownloadAndUpdateTemplate(Guid templateId)
        {
            try
            {
                var template = await _dataSyncClient.GetTemplate(templateId);
                if (template != null)
                {
                    AddOrUpdateTemplateData(template);
                }
            }
            catch (Exception ex)
            {
                this.LogWarn("FAILED to download template data.", ex);
            }
        }

        private static void AddOrUpdateTemplateData(Template serverTemplate)
        {
            var db = new DataSyncDatabase();
            db.AddOrUpdateTemplateOnClient(serverTemplate);

            foreach (var templatePublication in serverTemplate.Publications)
            {
                db.AddOrUpdateTemplatePublicationOnClient(templatePublication);
            }

            foreach (var templateFile in serverTemplate.TemplateFiles)
            {
                db.AddOrUpdateTemplateFileOnClient(templateFile);
            }
        }

        private async Task SyncAuthorDataClientWins(string login, Author localAuthor, Author serverAuthor, DataSyncDatabase db)
        {
            await SynchronizeAsync(
                localAuthor,
                serverAuthor,
                db.AddOrUpdateAuthorOnClient,
                login,
                _dataSyncClient.UpdateAuthor);

            var localFunctions = localAuthor?.Functions ?? new HashSet<JobFunction>();
            foreach (var localFunction in localFunctions)
            {
                // sync your local JobFunctions with server
                var serverFunction = serverAuthor.Functions.FirstOrDefault(jf => jf.Id == localFunction.Id);
                await
                    SynchronizeAsync(
                        localFunction,
                        serverFunction,
                        db.AddOrUpdateJobFunctionOnClient,
                        _dataSyncClient.AddOrUpdateJobFunction);
            }

            foreach (var serverJobFunction in serverAuthor.Functions.Where(jf => !jf.Deleted))
            {
                // get new JobFunctions from server
                if (localAuthor == null || localAuthor.Functions.Any(jf => jf.Id == serverJobFunction.Id))
                {
                    continue;
                }

                await
                    SynchronizeAsync(
                        null,
                        serverJobFunction,
                        db.AddOrUpdateJobFunctionOnClient,
                        _dataSyncClient.AddOrUpdateJobFunction);
            }

            var localWorkplaces = localAuthor?.Workplaces ?? new HashSet<Workplace>();
            foreach (var localWorkplace in localWorkplaces)
            {
                // sync your local Workplaces with server
                var serverWorkplace = serverAuthor.Workplaces.FirstOrDefault(wp => wp.Id == localWorkplace.Id);
                await
                    SynchronizeAsync(
                        localWorkplace,
                        serverWorkplace,
                        db.AddOrUpdateWorkplaceOnClient,
                        _dataSyncClient.AddOrUpdateWorkplace);
            }

            foreach (var serverWorkplace in serverAuthor.Workplaces.Where(wp => !wp.Deleted))
            {
                // get new Workplaces from server
                if (localAuthor == null || localAuthor.Workplaces.Any(wp => wp.Id == serverWorkplace.Id))
                {
                    continue;
                }

                await SynchronizeAsync(
                    null,
                    serverWorkplace,
                    db.AddOrUpdateWorkplaceOnClient,
                    _dataSyncClient.AddOrUpdateWorkplace);
            }

            var localJobAssignments = localAuthor?.JobAssignments ?? new HashSet<JobAssignment>();
            foreach (var localJobAssignment in localJobAssignments)
            {
                // sync your local JobAssignments with server
                var serverJobAssignment =
                    serverAuthor.JobAssignments.FirstOrDefault(f => f.Id == localJobAssignment.Id);
                await
                    SynchronizeAsync(
                        localJobAssignment,
                        serverJobAssignment,
                        db.AddOrUpdateJobAssignmentOnClient,
                        _dataSyncClient.AddOrUpdateJobAssignment);
                foreach (var localJobFunction in localJobAssignment.Functions)
                {
                    JobFunction serverJobFunction = null;
                    if (serverJobAssignment != null)
                    {
                        serverJobFunction = serverJobAssignment.Functions.FirstOrDefault(ja => ja.Id == localJobFunction.Id);
                    }

                    await
                        SynchronizeAsync(
                            localJobFunction,
                            serverJobFunction,
                            db.AddOrUpdateJobFunctionOnClient,
                            _dataSyncClient.AddOrUpdateJobFunction);
                }
            }

            foreach (var serverJobAssignment in serverAuthor.JobAssignments.Where(ja => !ja.Deleted))
            {
                // get new JobAssignments from server
                if (localAuthor == null || localAuthor.JobAssignments.Any(ja => ja.Id == serverJobAssignment.Id))
                {
                    continue;
                }

                await
                    SynchronizeAsync(
                        null,
                        serverJobAssignment,
                        db.AddOrUpdateJobAssignmentOnClient,
                        _dataSyncClient.AddOrUpdateJobAssignment);
                foreach (var serverJobFunction in serverJobAssignment.Functions.Where(jf => !jf.Deleted))
                {
                    await
                        SynchronizeAsync(
                            null,
                            serverJobFunction,
                            db.AddOrUpdateJobFunctionOnClient,
                            _dataSyncClient.AddOrUpdateJobFunction);
                }
            }
        }

        private async Task SyncAuthorDataServerWins(string login, Author serverAuthor, Author localAuthor, DataSyncDatabase db)
        {
            if (serverAuthor != null)
            {
                await
                    SynchronizeAsync(
                        localAuthor,
                        serverAuthor,
                        db.AddOrUpdateAuthorOnClient,
                        login,
                        _dataSyncClient.UpdateAuthor);

                if (localAuthor == null)
                {
                    return;
                }

                foreach (var serverFunction in serverAuthor.Functions)
                {
                    var localFunction = localAuthor.Functions.FirstOrDefault(f => f.Id == serverFunction.Id);
                    await
                        SynchronizeAsync(
                            localFunction,
                            serverFunction,
                            db.AddOrUpdateJobFunctionOnClient,
                            _dataSyncClient.AddOrUpdateJobFunction);
                }

                foreach (var serverWorkplace in serverAuthor.Workplaces)
                {
                    var localWorkplace = localAuthor.Workplaces.FirstOrDefault(wp => wp.Id == serverWorkplace.Id);
                    await
                        SynchronizeAsync(
                            localWorkplace,
                            serverWorkplace,
                            db.AddOrUpdateWorkplaceOnClient,
                            _dataSyncClient.AddOrUpdateWorkplace);
                }

                foreach (var serverJobAssignment in serverAuthor.JobAssignments)
                {
                    var localJobAssignment =
                        localAuthor.JobAssignments.FirstOrDefault(j => j.Id == serverJobAssignment.Id);
                    await
                        SynchronizeAsync(
                            localJobAssignment,
                            serverJobAssignment,
                            db.AddOrUpdateJobAssignmentOnClient,
                            _dataSyncClient.AddOrUpdateJobAssignment);
                    foreach (var serverJobFunction in serverJobAssignment.Functions)
                    {
                        JobFunction localJobFunction = null;
                        if (localJobAssignment != null)
                        {
                            localJobFunction = localJobAssignment.Functions.FirstOrDefault(f => f.Id == serverJobFunction.Id);
                        }

                        await
                            SynchronizeAsync(
                                localJobFunction,
                                serverJobFunction,
                                db.AddOrUpdateJobFunctionOnClient,
                                _dataSyncClient.AddOrUpdateJobFunction);
                    }
                }
            }
        }

        private async Task SynchronizeAsync<T>(
            T localEntity,
            T serverEntity,
            Action<T> updateOnClient,
            Func<Guid, T, Task<T>> updateOnServer)
            where T : Synchronizable
        {
            await SynchronizeInternal(localEntity, serverEntity, updateOnClient, updateOnServer);
        }

        private async Task SynchronizeAsync<T>(
            T localEntity,
            T serverEntity,
            Action<T> updateOnClient,
            string originator,
            Func<Guid, T, string, Task<T>> updateOnServerWithOriginator)
            where T : Synchronizable
        {
            await SynchronizeInternal(localEntity, serverEntity, updateOnClient, null, updateOnServerWithOriginator, originator);
        }

        private async Task SynchronizeInternal<T>(
            T localEntity,
            T serverEntity,
            Action<T> updateOnClient,
            Func<Guid, T, Task<T>> updateOnServer = null,
            Func<Guid, T, string, Task<T>> updateOnServerWithOriginator = null,
            string originator = null)
            where T : Synchronizable
        {
            if (serverEntity != null && localEntity == null)
            {
                updateOnClient(serverEntity);
                this.LogInfo($"PULLED {typeof(T)} from server to client (ADD).");
            }

            if (serverEntity != null && localEntity != null &&
                serverEntity.ServerModificationTimeUtc > localEntity.ServerModificationTimeUtc)
            {
                updateOnClient(serverEntity);
                this.LogInfo($"PULLED {typeof(T)} from server to client (UPDATE).");
            }
            else if (localEntity != null && localEntity.ClientModification)
            {
                T updatedEntity = null;
                if (updateOnServer != null)
                {
                    updatedEntity = await updateOnServer(localEntity.Id, localEntity);
                }
                else if (updateOnServerWithOriginator != null)
                {
                    updatedEntity = await updateOnServerWithOriginator(localEntity.Id, localEntity, originator);
                }

                if (updatedEntity != null)
                {
                    updateOnClient(updatedEntity);
                    this.LogInfo($"PUSHED {typeof(T)} from client to server (UPDATE).");
                }
                else
                {
                    this.LogError($"FAILED to push {typeof(T)} from client to server (UPDATE).");
                    Result = DataSyncResult.FinishedWithErrors;
                }
            }
        }

        private string FindExceptionMessage(Exception ex)
        {
            if (ex.Message.Contains("See the inner") && ex.InnerException != null)
            {
                return FindExceptionMessage(ex.InnerException);
            }

            return ex.Message;
        }
    }
}
