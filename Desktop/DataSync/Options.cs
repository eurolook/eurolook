using CommandLine;
using JetBrains.Annotations;

namespace Eurolook.DataSync
{
    [UsedImplicitly]
    public class Options
    {
        [Option('s', "server", HelpText = "The address of the API endpoint, e.g. https://example.com:port/api/v2.")]
        public string Server { get; set; }

        [Option('u', "user", HelpText = "Run under another user account.")]
        public string User { get; set; }

        [Option('m', "mini", HelpText = "Run in mini mode.")]
        public bool IsMiniMode { get; set; }
    }
}
