﻿using Microsoft.Extensions.Configuration;

namespace Eurolook.DataSync;

public class DataSyncSettings
{
    public static DataSyncSettings Instance { get; } = GetSettings();

    public bool UseRoamingDataDirectory { get; set; }

    public string ProductCustomizationId { get; set; } = string.Empty;

    public string DataSyncServiceUser { get; set; } = string.Empty;

    public string DataSyncServicePasswordEncrypted { get; set; } = string.Empty;

    public string DataSyncServiceSecretsProviderAssembly { get; set; } = string.Empty;

    public string DataSyncServiceSecretsProviderClass { get; set; } = string.Empty;

    public string DefaultDeviceOwner { get; set; } = string.Empty;

    public string DeviceOwnerEnvironmentVariable { get; set; } = string.Empty;

    private static DataSyncSettings GetSettings()
    {
        var config = AppSettingsRetriever.GetConfiguration();

        var settings = config.GetSection(nameof(DataSyncSettings)).Get<DataSyncSettings>()
            ?? new DataSyncSettings();
        settings.UseRoamingDataDirectory = config.GetSection("AppSettings").GetValue<bool>("UseRoamingDataDirectory");
        settings.ProductCustomizationId = config.GetSection("AppSettings").GetValue<string>("ProductCustomizationId");
        return settings;
    }
}
