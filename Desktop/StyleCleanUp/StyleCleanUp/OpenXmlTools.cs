using System;
using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.CustomProperties;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using JetBrains.Annotations;

namespace Eurolook.StyleCleanUp
{
    public static class OpenXmlTools
    {
        private static readonly List<KeyValuePair<string, string>> DefaultNamespaces =
            new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>(
                    "wpc",
                    "http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"),
                new KeyValuePair<string, string>("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006"),
                new KeyValuePair<string, string>("o", "urn:schemas-microsoft-com:office:office"),
                new KeyValuePair<string, string>(
                    "r",
                    "http://schemas.openxmlformats.org/officeDocument/2006/relationships"),
                new KeyValuePair<string, string>("m", "http://schemas.openxmlformats.org/officeDocument/2006/math"),
                new KeyValuePair<string, string>("v", "urn:schemas-microsoft-com:vml"),
                new KeyValuePair<string, string>(
                    "wp14",
                    "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"),
                new KeyValuePair<string, string>(
                    "wp",
                    "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"),
                new KeyValuePair<string, string>("w10", "urn:schemas-microsoft-com:office:word"),
                new KeyValuePair<string, string>("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main"),
                new KeyValuePair<string, string>("w14", "http://schemas.microsoft.com/office/word/2010/wordml"),
                new KeyValuePair<string, string>("w15", "http://schemas.microsoft.com/office/word/2012/wordml"),
                new KeyValuePair<string, string>(
                    "wpg",
                    "http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"),
                new KeyValuePair<string, string>(
                    "wpi",
                    "http://schemas.microsoft.com/office/word/2010/wordprocessingInk"),
                new KeyValuePair<string, string>("wne", "http://schemas.microsoft.com/office/word/2006/wordml"),
                new KeyValuePair<string, string>(
                    "wps",
                    "http://schemas.microsoft.com/office/word/2010/wordprocessingShape"),
            };

        private static readonly string[] DefaultIgnorables = { "w14", "w15", "wp14" };

        public static MainDocumentPart GetOrCreateMainDocumentPart(this WordprocessingDocument docx)
        {
            var mainDocumentPart = docx.MainDocumentPart;
            if (mainDocumentPart == null)
            {
                mainDocumentPart = docx.AddMainDocumentPart();
                mainDocumentPart.Document = new Document();
                mainDocumentPart.Document.AddDefaultNamespaceDeclarations();
            }

            return mainDocumentPart;
        }

        public static StyleDefinitionsPart GetOrCreateMainStyleDefinitionsPart(this WordprocessingDocument docx)
        {
            var styleDefinitionsPart = docx.GetOrCreateMainDocumentPart().StyleDefinitionsPart;
            if (styleDefinitionsPart == null)
            {
                styleDefinitionsPart = docx.MainDocumentPart.AddNewPart<StyleDefinitionsPart>();
                styleDefinitionsPart.Styles = new Styles();
                styleDefinitionsPart.Styles.Save();
            }

            return styleDefinitionsPart;
        }

        public static void AddDefaultNamespaceDeclarations(this OpenXmlPartRootElement element)
        {
            var namespacesToAdd = DefaultNamespaces.Except(element.NamespaceDeclarations).ToList();
            foreach (var ns in namespacesToAdd)
            {
                element.AddNamespaceDeclaration(ns.Key, ns.Value);
            }

            var ignorables = element.MCAttributes != null
                             && element.MCAttributes.Ignorable != null
                             && element.MCAttributes.Ignorable.Value != null
                ? element.MCAttributes.Ignorable.Value.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                : new string[] { };

            string ignorable = string.Join(" ", DefaultIgnorables.Union(ignorables));
            element.MCAttributes = new MarkupCompatibilityAttributes { Ignorable = ignorable };
        }

        public class Parts
        {
            /// <summary>
            /// Gets the <see cref="Body"/> of the given document or null if the body does not exist.
            /// </summary>
            /// <param name="document">An Open XML <see cref="WordprocessingDocument"/>.</param>
            /// <returns>The <see cref="Body"/> of the document.</returns>
            [CanBeNull]
            public static Body GetDocumentBody(WordprocessingDocument document)
            {
                return document?.MainDocumentPart?.Document?.Body;
            }

            /// <summary>
            /// returns the "custom properties" of the given Open XML document and null if the access fails.
            /// </summary>
            /// <param name="document">An Open XML document.</param>
            /// <returns>The custom properties of the document.</returns>
            public static Properties CustomProperties(
                WordprocessingDocument document)
            {
                return document?.CustomFilePropertiesPart?.Properties;
            }

            /// <summary>
            /// returns the "numbering" of the given Open XML document and null if the access fails.
            /// </summary>
            /// <param name="document">Open XML document</param>
            /// <returns>document numbering</returns>
            public static Numbering GetNumbering(WordprocessingDocument document)
            {
                return document?.MainDocumentPart?.NumberingDefinitionsPart?.Numbering;
            }

            /// <summary>
            /// returns the "settings" of the given Open XML document and null if the access fails.
            /// </summary>
            /// <param name="document">An Open XML document.</param>
            /// <returns>The document settings.</returns>
            public static Settings GetSettings(WordprocessingDocument document)
            {
                return document?.MainDocumentPart?.DocumentSettingsPart?.Settings;
            }

            /// <summary>
            /// returns the "styles" of the given Open XML document and null if the access fails.
            /// </summary>
            /// <param name="document">An Open XML document.</param>
            /// <returns>The document styles.</returns>
            public static Styles GetStyles(WordprocessingDocument document)
            {
                return document?.MainDocumentPart?.StyleDefinitionsPart?.Styles;
            }
        }

        public class StyleHelper
        {
            public static Style GetStyle(WordprocessingDocument document, string strStyleId)
            {
                var styles = Parts.GetStyles(document);
                return styles?.Elements<Style>().FirstOrDefault(e => e.StyleId == strStyleId);
            }
        }

        public class NumberingHelper
        {
            public static AbstractNum GetAbstractNumById(WordprocessingDocument document, int id)
            {
                if (document == null)
                {
                    throw new ArgumentNullException(nameof(document));
                }

                var numbering = document.MainDocumentPart?.NumberingDefinitionsPart?.Numbering;
                return numbering?.Descendants<AbstractNum>().FirstOrDefault(an => an.AbstractNumberId.Value == id);
            }

            public static string GetAbstractNumNameById(WordprocessingDocument document, int id)
            {
                var absNum = GetAbstractNumById(document, id);
                return absNum?.AbstractNumDefinitionName?.Val?.Value ?? string.Empty;
            }
        }
    }
}
