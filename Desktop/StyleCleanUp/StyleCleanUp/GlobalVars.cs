using DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.StyleCleanUp
{
    public class GlobalVars
    {
        public Numbering DocumentNumbering { get; set; }

        public Styles DocumentStyles { get; set; }

        public Numbering TemplateNumberingPart { get; set; }

        public Styles TemplateStylesPart { get; set; }
    }
}
