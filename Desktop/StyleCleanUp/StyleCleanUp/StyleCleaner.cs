﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Eurolook.Common;
using Eurolook.Common.Log;

namespace Eurolook.StyleCleanUp
{
    public class StyleCleaner : ICanLog
    {
        private readonly GlobalVars _globals = new GlobalVars();

        // dictionaries
        private readonly Dictionary<int, NumberingInstance> _dictNumIdValToNum = new Dictionary<int, NumberingInstance>();

        // set for all template styles (-1 if there is no numbering)
        private readonly Dictionary<string, int> _dictStyleIdToAbsNumId = new Dictionary<string, int>();

        // set for all template styles (-1 if there is no numPr, 0 if there is no ilvl)
        private readonly Dictionary<string, int> _dictStyleIdToILvl = new Dictionary<string, int>();

        private readonly Dictionary<int, int> _dictStyleOldAbsNumToNewAbsNum = new Dictionary<int, int>();

        // set for all (old and new) num elements
        private readonly Dictionary<int, string> _dictAbsNumIdToNsid = new Dictionary<int, string>();

        // set for the new num elements only
        private readonly Dictionary<string, int> _dictNsidToAbsNumId = new Dictionary<string, int>();

        // set for the new numbered styles only
        private readonly Dictionary<string, string> _dictStyleIdToOldNsid = new Dictionary<string, string>();

        // which nsid value is referred to by the document paragraphs so far? (maps to Nothing)
        private readonly Dictionary<string, object> _dictUsedNsid = new Dictionary<string, object>();

        // set for template styles only
        private readonly Dictionary<string, Style> _dictStyleIdToStyle = new Dictionary<string, Style>();

        // @3 set for the linked paragraph styles of the document template
        private readonly Dictionary<string, string> _dictParaStyleIdToCharStyleId = new Dictionary<string, string>();

        // misc
        private int _nextNumIdValue = 1; // smallest unused ID
        private int _nextAbsNumIdValue; // smallest unused ID
        private int _offsetNumIdValue;
        private int _offsetAbsNumIdValue;
        private bool _restoreDeletedNumbering;

        /// <summary>
        /// Defines the possible return values of the conversion.
        /// </summary>
        public enum ReturnCode
        {
            Ok = 0,
            FileError = 1,
            MergeFailed = 2,
            ConversionFailed = 3,
            RemoveFailed = 4,
            UnknownError = 5,
        }

        /// <summary>
        /// runs the style clean up with the specified arguments.
        /// </summary>
        /// <param name="templateFullPath">The full path to the Word template (.dotm or .dotx).</param>
        /// <param name="inputDocxFullPath">The full path to the input Word document (.docx or .docm).</param>
        /// <param name="outputDocxFullPath">
        /// The full path to the cleaned output document (.docx or .docm). Same extension as the
        /// input file.
        /// </param>
        /// <param name="restoreDeletedNumbering">A boolean flag indicating whether manually deleted numbering is to be restored.</param>
        /// <param name="blnTopLevelParagraphsOnly">which paragraphs should be checked.</param>
        /// <returns>the return code.</returns>
        public ReturnCode MakeClean(
            string templateFullPath,
            string inputDocxFullPath,
            string outputDocxFullPath,
            bool restoreDeletedNumbering,
            bool blnTopLevelParagraphsOnly = false)
        {
            var watch = Stopwatch.StartNew();
            var enReturnCode = ReturnCode.Ok;
            _restoreDeletedNumbering = restoreDeletedNumbering;

            try
            {
                this.LogInfo("Template file '" + templateFullPath + "'");
                this.LogInfo("Input file '" + inputDocxFullPath + "'");
                this.LogInfo("Output file '" + outputDocxFullPath + "'");

                // check file name conflicts
                if (string.Equals(inputDocxFullPath, outputDocxFullPath, StringComparison.OrdinalIgnoreCase))
                {
                    throw new StyleCleanUpException("Input file and output file are the same.");
                }

                using (var temporaryTemplateFile = new TemporaryFile(Path.GetExtension(templateFullPath)))
                {
                    File.Copy(templateFullPath, temporaryTemplateFile.FullName);
                    File.Copy(inputDocxFullPath, outputDocxFullPath, false);

                    using (var template = WordprocessingDocument.Open(temporaryTemplateFile.FullName, false))
                    {
                        using (var outputDoc = WordprocessingDocument.Open(outputDocxFullPath, true))
                        {
                            // initialize the Open XML access
                            if (!InitializeOpenXmlAccess(template, outputDoc))
                            {
                                this.LogInfo("the Open XML access failed");
                                return ReturnCode.ConversionFailed;
                            }

                            // merge template into document, build up data structures
                            if (!TemplateToDoc())
                            {
                                this.LogInfo("merging template into document failed");
                                return ReturnCode.MergeFailed;
                            }

                            // repair the document paragraphs
                            if (!ProcessDoc_1RepairParagraphs(outputDoc, blnTopLevelParagraphsOnly))
                            {
                                this.LogInfo("paragraph reparation failed");
                                return ReturnCode.ConversionFailed;
                            }

                            // remove unreferenced list templates
                            if (!ProcessDoc_2RemoveUnusedListTemplates())
                            {
                                this.LogInfo("removing the unused list templates failed");
                                return ReturnCode.RemoveFailed;
                            }

                            // renumber the remaining list templates consecutively
                            // NOTE: This function invalidates the previously stored relations to the absNum values in the dictionaries!
                            if (!ProcessDoc_3RenumberListTemplates())
                            {
                                this.LogInfo("renumbering the list templates failed");
                                return ReturnCode.ConversionFailed;
                            }

                            // remove the setting LinkStyles to avoid overriding document styles from the
                            // attached templated when opening the document
                            if (!ProcessDoc_4RemoveLinkStylesOption(outputDoc))
                            {
                                this.LogInfo("removing link styles option failed");
                                return ReturnCode.ConversionFailed;
                            }

                            // customer dependent post processing
                            if (!PostProcessing.Main(outputDoc))
                            {
                                this.LogInfo("postprocessing failed");
                                return ReturnCode.ConversionFailed;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                return ReturnCode.UnknownError;
            }
            finally
            {
                watch.Stop();
                this.LogInfo("ends after " + watch.Elapsed.TotalSeconds + " s. exit code is '" + enReturnCode + "'");
            }

            return enReturnCode;
        }

        /// <summary>
        /// Checks if two style definitions are equal.
        /// </summary>
        /// <remarks>Replaces the former function based on String comparisons.</remarks>
        private bool AreStylesEqualXmlBasedComparison(Style styOne, Style styTwo)
        {
            bool areEqual = false;
            var xmlDocOne = new XmlDocument();
            var xmlDocTwo = new XmlDocument();

            try
            {
                if (styOne == null || styTwo == null)
                {
                    // compare is not possible
                    this.LogInfo("StyleCleaner.AreStylesEqual: At least one of the two styles is Nothing.");
                    return false;
                }

                if (styOne.Type.Value == StyleValues.Character)
                {
                    // As for character styles, the "w:shd" element sometimes differs between document and template.
                    // workaround (so far): do not report character styles
                    return true;
                }

                string strOne = styOne.OuterXml;
                string strTwo = styTwo.OuterXml;

                xmlDocOne.LoadXml(strOne);
                xmlDocTwo.LoadXml(strTwo);

                // Remove elements that do not contribute to the visual appearance before the comparison.
                foreach (string tagName in new[]
                {
                    "lang", "qFormat", "rsid", "semiHidden", "uiPriority", "unhideWhenUsed",
                })
                {
                    DeleteElementsInList(xmlDocOne.GetElementsByTagName(tagName, OpenXmlNs.W));
                    DeleteElementsInList(xmlDocTwo.GetElementsByTagName(tagName, OpenXmlNs.W));
                }

                // Remove the numId attributes before the comparison.
                RemoveAttributesFromElementsInList(xmlDocOne.GetElementsByTagName("numId", OpenXmlNs.W));
                RemoveAttributesFromElementsInList(xmlDocTwo.GetElementsByTagName("numId", OpenXmlNs.W));

                areEqual = AreXmlDocumentsEqual(xmlDocOne, xmlDocTwo);

                if (!areEqual)
                {
                    this.LogInfo("definitions are different:");
                    this.LogTrace("Style 1 (normalized): " + xmlDocOne.OuterXml + "'");
                    this.LogTrace("Style 2 (normalized): " + xmlDocTwo.OuterXml + "'");
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }

            return areEqual;
        }

        private void DeleteElementsInList(XmlNodeList xmlList)
        {
            for (int i = xmlList.Count - 1; i >= 0; i += -1)
            {
                var elem = xmlList.Item(i);
                elem?.ParentNode?.RemoveChild(elem);
            }
        }

        private void RemoveAttributesFromElementsInList(XmlNodeList xmlList)
        {
            for (int i = xmlList.Count - 1; i >= 0; i += -1)
            {
                xmlList.Item(i)?.Attributes?.RemoveAll();
            }
        }

        private bool AreXmlDocumentsEqual(XmlDocument xmlDocOne, XmlDocument xmlDocTwo)
        {
            var xmlNodeOne = xmlDocOne.FirstChild;
            var xmlNodeTwo = xmlDocTwo.FirstChild;

            return AreXmlNodesEqual(xmlNodeOne, xmlNodeTwo);
        }

        private bool AreXmlNodesEqual(XmlNode xmlNodeOne, XmlNode xmlNodeTwo)
        {
            if (!string.Equals(xmlNodeOne.Name, xmlNodeTwo.Name, StringComparison.InvariantCultureIgnoreCase))
            {
                return false;
            }

            if (xmlNodeOne.Attributes?.Count != xmlNodeTwo.Attributes?.Count)
            {
                return false;
            }

            if (xmlNodeOne.ChildNodes.Count != xmlNodeTwo.ChildNodes.Count)
            {
                return false;
            }

            if (xmlNodeOne.Attributes != null && xmlNodeTwo.Attributes != null)
            {
                foreach (XmlAttribute attrOne in xmlNodeOne.Attributes)
                {
                    var attrTwo = xmlNodeTwo.Attributes[attrOne.Name];

                    if (attrTwo == null || !string.Equals(
                        attrOne.Value,
                        attrTwo.Value,
                        StringComparison.InvariantCultureIgnoreCase))
                    {
                        return false;
                    }
                }
            }

            for (int i = 0; i <= xmlNodeOne.ChildNodes.Count - 1; i++)
            {
                if (!AreXmlNodesEqual(xmlNodeOne.ChildNodes[i], xmlNodeTwo.ChildNodes[i]))
                {
                    return false;
                }
            }

            return true;
        }

        private bool AreNumberingsEqual(int firstAbsNumId, int secondAbsNumId)
        {
            const string strAbsNumId = "w:abstractNumId=\""; // excluded: do not compare IDs
            const string
                strTmpl =
                    "w:tmpl w:val=\""; // excluded: rather belongs to the user interface than to the style definition

            // jaco, 18.08.2017:
            // Durch die "Mischumgebung" 2010/2016 in DocuWrite gibt es Probleme beim Vergleich Template-><-Dokument
            // Beim Speichern unter 2016 ergänzt Word das restartNumberingAfterBreak Attribut, welches unter 2010 noch nicht bekannt ist.
            // Wenn unter 2016 in Vergleich zu einem 2010er Template gemacht wird, führt dies zu vermeintlich unterschiedlichen Numberings.
            // Um dies zu umgehen, wird das Attribut vorm Vergleich entfernt.
            // ! Da das Attribut nur dann gelöscht wird, wenn es den Defaultwert 0 beträgt, sollte sich dieses Vorgehen (auch bei zukünfitger Nutzung des Attributs)
            // ! nicht auf anderweitig auf das Ergebnis des Vergleichs auswirken.
            const string attrNameRestartNum = "restartNumberingAfterBreak";
            const string attrValueRestartNum = "0";

            string firstXml = "";
            string secondXml = "";
            bool areEqual;

            try
            {
                var firstAbsNum = _globals.DocumentNumbering.Descendants<AbstractNum>()
                                          .First(e => e.AbstractNumberId.Value == firstAbsNumId);
                firstXml = firstAbsNum.OuterXml;
                TrimAccordingToPattern(ref firstXml, strTmpl, "\"");
                TrimAccordingToPattern(ref firstXml, strAbsNumId, "\"");
                TrimNamespacedAttribute(ref firstXml, OpenXmlNs.W15, attrNameRestartNum, attrValueRestartNum); // s.o.

                var secondAbsNum = _globals.DocumentNumbering.Descendants<AbstractNum>()
                                           .First(e => e.AbstractNumberId.Value == secondAbsNumId);
                secondXml = secondAbsNum.OuterXml;
                TrimAccordingToPattern(ref secondXml, strTmpl, "\"");
                TrimAccordingToPattern(ref secondXml, strAbsNumId, "\"");
                TrimNamespacedAttribute(ref secondXml, OpenXmlNs.W15, attrNameRestartNum, attrValueRestartNum); // s.o.

                areEqual = string.Compare(firstXml, secondXml, StringComparison.InvariantCulture) == 0;
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                areEqual = false;
            }

            if (!areEqual)
            {
                string msg = "'" + firstXml + "'" + Environment.NewLine + "'" + secondXml + "'";
                this.LogWarn("definitions are different, " + msg);
            }

            return areEqual;
        }

        private void TrimAccordingToPattern(
            ref string s,
            string startPattern,
            string endPattern,
            bool keepPattern = true)
        {
            if (!string.IsNullOrEmpty(startPattern.Trim()) && !string.IsNullOrEmpty(s.Trim()))
            {
                int indexEnd;

                int indexStart = s.IndexOf(startPattern, StringComparison.InvariantCultureIgnoreCase);
                if (indexStart >= 0)
                {
                    indexEnd = s.IndexOf(
                        endPattern,
                        indexStart + startPattern.Length,
                        StringComparison.InvariantCultureIgnoreCase);
                }
                else
                {
                    // no numbering
                    indexEnd = -1;
                }

                if (indexEnd >= 0)
                {
                    // remove the numID, but further indicate that the style has a numbering
                    s = keepPattern
                        ? s.Substring(0, indexStart) + startPattern + s.Substring(indexEnd)
                        : s.Substring(0, indexStart) + s.Substring(indexEnd);
                }
            }
        }

        // Removes the specified attribute from the xml fragment.
        // In case filterValue is specified, the attribute is removed only if its value equals filterValue.
        // If no other occurrence of the namespace is found, the namespace declaration attribute is removed too.
        private void TrimNamespacedAttribute(
            ref string xml,
            string namespaceUri,
            string attribute,
            string filterValue = null)
        {
            const string namespaceDeclarationPrefix = "xmlns:";
            var xmlDoc = new XmlDocument();

            xmlDoc.LoadXml(xml);
            var xmlDocElem = xmlDoc.DocumentElement;

            // Does the attribute exist and does it match the specified value? -> remove it
            string value = xmlDocElem?.GetAttribute(attribute, namespaceUri);
            if (!string.IsNullOrEmpty(value))
            {
                if (filterValue == null || string.Equals(value, filterValue))
                {
                    xmlDocElem.RemoveAttribute(attribute, namespaceUri);
                }
            }

            xml = xmlDoc.OuterXml;

            string namespacePrefix = xmlDocElem.GetPrefixOfNamespace(namespaceUri);

            // If the xml string contains only one occurrence of the prefix, this occurrence has to be the namespace declaration
            // In this case it is no longer needed and the declaration can be removed
            if (xml.Split(new[] { namespacePrefix }, StringSplitOptions.None).Count() == 2)
            {
                xmlDocElem.RemoveAttribute(string.Concat(namespaceDeclarationPrefix, namespacePrefix));
            }

            xml = xmlDoc.OuterXml;
        }

        // gets the value of the NumId descendant of a style if numbering is applied. Returns -1 in any other case.
        private void GetNumPropertiesOfStyle(Style styleVar, ref int intNumId, ref int intILvl)
        {
            try
            {
                intNumId = -1; // default
                intILvl = -1; // default

                var elemNumPr = styleVar.Descendants<NumberingProperties>().FirstOrDefault();
                if (elemNumPr == null)
                {
                    return;
                }

                if (elemNumPr.NumberingId != null)
                {
                    intNumId = elemNumPr.NumberingId.Val;
                }

                intILvl = elemNumPr.NumberingLevelReference != null ? (int)elemNumPr.NumberingLevelReference.Val : 0;
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        // sets the value of the NumId descendant of a style if numbering is applied. Returns "true" if the assignment was successful
        private bool SetNumIdValueOfStyle(Style styleVar, int value)
        {
            try
            {
                var numId = styleVar.Descendants<NumberingId>().First();
                numId.Val = value;
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        // adds a restart to a numbering instance; returns true if successful
        private bool AddRestartToNumElem(ref NumberingInstance numElem, string styleIdString)
        {
            bool blnSuccess = true;
            int[] intRestartValues = { 1, 1, 1, 1, 1, 1, 1, 1, 1 }; // default restart values (levels 0-8)

            try
            {
                // assertion
                if (numElem == null)
                {
                    this.LogInfo("numElem is Nothing");
                    return false;
                }

                // determine restart values
                // check if the style belongs to the template
                if (_dictStyleIdToAbsNumId.ContainsKey(styleIdString))
                {
                    // check if the style has a numbering
                    int sAbsNumIdValue = _dictStyleIdToAbsNumId[styleIdString];
                    if (sAbsNumIdValue >= 0)
                    {
                        // select the appropriate abstractNum definition
                        foreach (var objAbsNum in _globals.DocumentNumbering.Elements<AbstractNum>())
                        {
                            if (objAbsNum.AbstractNumberId.Value == sAbsNumIdValue)
                            {
                                // extract the restart values
                                foreach (var lvl in objAbsNum.Elements<Level>())
                                {
                                    int intLevel = lvl.LevelIndex;

                                    var startNumVal = lvl.GetFirstChild<StartNumberingValue>();
                                    if (startNumVal == null)
                                    {
                                        continue;
                                    }

                                    int intStart = startNumVal.Val;

                                    if (intLevel >= 0 && intLevel <= 8)
                                    {
                                        intRestartValues[intLevel] = intStart;
                                    }
                                }

                                break;
                            }
                        }
                    }
                }

                // preserve existing overrides
                var lvlOverTmp = numElem.GetFirstChild<LevelOverride>();
                if (lvlOverTmp == null)
                {
                    // create and add 9 children "lvlOverride"
                    for (int intLevel = 0; intLevel <= 8; intLevel++)
                    {
                        // start value element
                        var startOverElem = new StartOverrideNumberingValue { Val = intRestartValues[intLevel] };

                        // lvlOverride element
                        var lvlOverElem = new LevelOverride { LevelIndex = intLevel };
                        lvlOverElem.AppendChild(startOverElem);

                        // add new element
                        numElem.AppendChild(lvlOverElem);
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                blnSuccess = false;
            }

            return blnSuccess;
        }

        // determines the nsid value for a style with numbering. Returns nothing if there is no nsid value.
        private string GetStyleNsid(string strStyle)
        {
            // technically, we are looking for: style -> num -> abstractNum . nsid
            // logically, we use the mappings: styleId -> absNumId -> nsid

            // which absNumId is associated with the style?
            if (_dictStyleIdToAbsNumId.ContainsKey(strStyle))
            {
                int absNumId = _dictStyleIdToAbsNumId[strStyle];

                // which Nsid is associated with the absNumId?
                if (_dictAbsNumIdToNsid.ContainsKey(absNumId))
                {
                    return _dictAbsNumIdToNsid[absNumId];
                }
            }

            return null;
        }

        // initializes the Open XML access
        private bool InitializeOpenXmlAccess(WordprocessingDocument template, WordprocessingDocument document)
        {
            bool blnSuccess = true;
            try
            {
                // --- document STYLE PART
                _globals.DocumentStyles = OpenXmlTools.Parts.GetStyles(document);
                if (_globals.DocumentStyles == null)
                {
                    this.LogInfo("cannot access the 'styles' of the Word document.");
                    blnSuccess = false;
                }

                // --- document NUMBERING PART
                _globals.DocumentNumbering = OpenXmlTools.Parts.GetNumbering(document);
                if (_globals.DocumentNumbering == null)
                {
                    this.LogInfo("cannot access the 'numbering' of the Word document.");
                    blnSuccess = false;
                }

                // --- document MAIN PART
                if (OpenXmlTools.Parts.GetDocumentBody(document) == null)
                {
                    this.LogInfo("cannot access the 'body' of the Word document.");
                    blnSuccess = false;
                }

                // --- template STYLE PART
                _globals.TemplateStylesPart = OpenXmlTools.Parts.GetStyles(template);
                if (_globals.TemplateStylesPart == null)
                {
                    this.LogInfo("cannot access the 'styles' of the template.");
                    blnSuccess = false;
                }

                // --- template NUMBERING PART
                _globals.TemplateNumberingPart = OpenXmlTools.Parts.GetNumbering(template);
                if (_globals.TemplateNumberingPart == null)
                {
                    this.LogInfo("cannot access the 'numbering' of the template.");
                    blnSuccess = false;
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                blnSuccess = false;
            }

            return blnSuccess;
        }

        // Determine the largest used numID and abstractNumId in the current document (used for insertions).
        // Find the location to append new num elements and abstractNum elements in the current document.
        // returns lastAbsNumElem, offsetNumIdValue
        private bool TemplateToDoc_1ReadDocumentNumbering(
            ref OpenXmlElement lastNumElem,
            ref OpenXmlElement lastAbsNumElem)
        {
            try
            {
                _offsetAbsNumIdValue = 0; // list template offset
                lastNumElem = default;

                lastAbsNumElem = default;
                _offsetNumIdValue = 0;

                for (int intI = 0; intI <= _globals.DocumentNumbering.ChildElements.Count - 1; intI++)
                {
                    var elem = _globals.DocumentNumbering.ChildElements[intI];

                    switch (elem.LocalName)
                    {
                        case "abstractNum":
                            {
                                // cast to abstractNum
                                var absNumElem = (AbstractNum)elem;

                                // mapping: absNumId -> NSID
                                _dictAbsNumIdToNsid.Add(absNumElem.AbstractNumberId.Value, absNumElem.Nsid.Val.Value);

                                // remember largest abstractNumId
                                if (absNumElem.AbstractNumberId.Value >= _offsetAbsNumIdValue)
                                {
                                    _offsetAbsNumIdValue =
                                        absNumElem.AbstractNumberId.Value
                                        + 1; // offset is the largest ID + 1 (because it is 0-based)
                                }

                                // remember last occurrence
                                lastAbsNumElem = elem;
                                break;
                            }

                        case "num":
                            {
                                // cast to num
                                var numElem = (NumberingInstance)elem;

                                // remember largest numId
                                int numIdValue = numElem.NumberID.Value;
                                if (numIdValue > _offsetNumIdValue)
                                {
                                    _offsetNumIdValue = numIdValue; // offset is the largest ID (because it is 1-based)
                                }

                                // remember last occurrence
                                lastNumElem = elem;

                                // collect reference in dictionary
                                _dictNumIdValToNum.Add(numIdValue, numElem);
                                break;
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                return false;
            }

            return true;
        }

        // Append copies of all num elements and abstractNum elements of the template to the document.
        // During this procedure, we must assign unique IDs to the copies.
        // returns lastAbsNumElem, offsetNumIdValue
        private bool TemplateToDoc_2CopyNumbering(ref OpenXmlElement lastNumElem, ref OpenXmlElement lastAbsNumElem)
        {
            try
            {
                _nextNumIdValue = _offsetNumIdValue + 1; // because it is 1-based
                _nextAbsNumIdValue = _offsetAbsNumIdValue; // because it is 0-based

                for (int i = 0; i <= _globals.TemplateNumberingPart.ChildElements.Count - 1; i++)
                {
                    var elem = _globals.TemplateNumberingPart.ChildElements[i];

                    if (elem is AbstractNum absNumElem)
                    {
                        // copy element and assign unique ID
                        var newAbsNumElem = (AbstractNum)absNumElem.Clone();
                        newAbsNumElem.AbstractNumberId.Value =
                            newAbsNumElem.AbstractNumberId.Value + _offsetAbsNumIdValue;

                        // insert copy
                        lastAbsNumElem = lastAbsNumElem == null
                            ? _globals.DocumentNumbering.InsertAt(newAbsNumElem, 0)
                            : _globals.DocumentNumbering.InsertAfter(newAbsNumElem, lastAbsNumElem);

                        // mapping: absNumId -> NSID
                        _dictAbsNumIdToNsid.Add(newAbsNumElem.AbstractNumberId.Value, newAbsNumElem.Nsid.Val.Value);

                        // @1: mapping: NSID -> absNumId
                        _dictNsidToAbsNumId.Add(newAbsNumElem.Nsid.Val.Value, newAbsNumElem.AbstractNumberId.Value);

                        // we prepare coming insertions
                        if (newAbsNumElem.AbstractNumberId.Value >= _nextAbsNumIdValue)
                        {
                            _nextAbsNumIdValue = newAbsNumElem.AbstractNumberId.Value + 1;
                        }
                    }
                    else if (elem is NumberingInstance numElemOld)
                    {
                        // copy element, assign unique ID
                        var numElemNew = (NumberingInstance)numElemOld.Clone();
                        int intNumIdValue = numElemNew.NumberID.Value + _offsetNumIdValue;
                        numElemNew.NumberID.Value = intNumIdValue;

                        // adjust the abstractNumId reference
                        numElemNew.AbstractNumId.Val = numElemNew.AbstractNumId.Val.Value + _offsetAbsNumIdValue;

                        // insert copy
                        lastNumElem = lastNumElem == null
                            ? _globals.DocumentNumbering.InsertAt(numElemNew, 0)
                            : _globals.DocumentNumbering.InsertAfter(numElemNew, lastNumElem);

                        // collect reference in dictionary
                        _dictNumIdValToNum.Add(intNumIdValue, numElemNew);

                        // we prepare coming insertions
                        if (intNumIdValue >= _nextNumIdValue)
                        {
                            _nextNumIdValue = intNumIdValue + 1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                return false;
            }

            return true;
        }

        // Append copies of the template styles to the document.
        // returns offsetNumIdValue, intNumberOfOldStyles
        private bool TemplateToDoc_3CopyStyles(ref int intNumberOfOldStyles)
        {
            bool blnSuccess = true;

            int intNumIdValue = 0;
            int intILvl = 0;

            try
            {
                // number of elements before the insertion of styles from the template document
                intNumberOfOldStyles = _globals.DocumentStyles.ChildElements.Count;

                foreach (var style in _globals.TemplateStylesPart.ChildElements.OfType<Style>())
                {
                    // make a copy (clone) of the  style
                    var newStyleObj = (Style)style.Clone();

                    // flo/marb 04.07.2018:
                    // ToDo: Öffnet man ein EN-Template in einem DE-Word legt der StyleCleanup Duplikate der Built-In
                    // Styles an (styleId Normal vs. Standard) (oder der Chr bzw. Zhn Ableger).
                    // Der Grund könnte in der nächsten Zeile liegen. Evtl. muss man für BuiltIn-Styles einen Schritt
                    // zurück gehen und über die StyleID den Name aus der latentStyle Liste besorgen.
                    // Das wäre dann sowohl für DE und EN "Normal"
                    // Danach würde der Style-Cleanup den Style finden und nicht doppelt anlegen
                    string strStyleId = newStyleObj.StyleId;

                    _dictStyleIdToStyle.Add(strStyleId, newStyleObj);

                    // adjust the numId if numbering is applied to the style
                    int intAbsNumIdValue = -1;
                    GetNumPropertiesOfStyle(newStyleObj, ref intNumIdValue, ref intILvl);

                    if (intNumIdValue >= 0)
                    {
                        // adjust numId
                        intNumIdValue += _offsetNumIdValue;
                        SetNumIdValueOfStyle(newStyleObj, intNumIdValue);

                        // get abstractNumId
                        try
                        {
                            intAbsNumIdValue = _dictNumIdValToNum[intNumIdValue].AbstractNumId.Val;
                        }
                        catch (Exception ex)
                        {
                            this.LogError(ex);
                        }
                    }

                    // Add the new reference to the dictionary "styleId to numID".
                    // Be aware that a "negative ID" means "no reference".
                    _dictStyleIdToAbsNumId.Add(strStyleId, intAbsNumIdValue);

                    // Add entry to dictionary "styleId to ilvl"
                    _dictStyleIdToILvl.Add(strStyleId, intILvl);

                    // add new style
                    _globals.DocumentStyles.AppendChild(newStyleObj);

                    // <@3 is newStyleObj a linked "paragraph" style
                    if (newStyleObj.Type?.Value == StyleValues.Paragraph)
                    {
                        var linkedStyleVal = newStyleObj.LinkedStyle?.Val;
                        if (!string.IsNullOrEmpty(linkedStyleVal))
                        {
                            // store style pair in the dictionary
                            _dictParaStyleIdToCharStyleId.Add(strStyleId, linkedStyleVal);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                blnSuccess = false;
            }

            return blnSuccess;
        }

        // Remove styles that are redefined in the template.
        // the reversed order is necessary because of the deletions
        private bool TemplateToDoc_4RemoveStyleDuplicates(ref int intNumberOfOldStyles)
        {
            bool blnSuccess = true;

            try
            {
                for (int intI = intNumberOfOldStyles - 1; intI >= 0; intI -= 1)
                {
                    var elem = _globals.DocumentStyles.ChildElements[intI];

                    if (!string.Equals(elem.LocalName, "style", StringComparison.Ordinal))
                    {
                        continue;
                    }

                    var styleElem = (Style)elem;
                    string strStyleId = styleElem.StyleId;

                    // remove style definition if it is redefined in the document template
                    // in case we delete the former style definition, we store the mapping "oldAbsNum -> newAbsNum"
                    if (_dictStyleIdToAbsNumId.ContainsKey(strStyleId))
                    {
                        // the style is redefined

                        // check if the style is modified
                        // the result does not influence the following procedure, but is used for the return value
                        bool isStyleModified = !AreStylesEqualXmlBasedComparison(styleElem, _dictStyleIdToStyle[strStyleId]);

                        // does the NEW style definition have a numbering?
                        int intNewAbsNum = _dictStyleIdToAbsNumId[strStyleId];
                        if (intNewAbsNum >= 0)
                        {
                            // does the OLD style definition have a numbering?
                            int intNum = 0;
                            int intILvl = 0;
                            GetNumPropertiesOfStyle(styleElem, ref intNum, ref intILvl);
                            if (intNum >= 0)
                            {
                                // is there already an entry for this mapping?
                                int intOldAbsNum = 0;
                                try
                                {
                                    intOldAbsNum = _dictNumIdValToNum[intNum].AbstractNumId.Val;
                                    if (!_dictStyleOldAbsNumToNewAbsNum.ContainsKey(intOldAbsNum))
                                    {
                                        // add an entry to the abstractNumId mapping
                                        _dictStyleOldAbsNumToNewAbsNum.Add(intOldAbsNum, intNewAbsNum);
                                    }

                                    // <@2 create mapping
                                    if (!_dictStyleIdToOldNsid.ContainsKey(strStyleId))
                                    {
                                        string strOldNsid = _dictAbsNumIdToNsid[intOldAbsNum];
                                        _dictStyleIdToOldNsid.Add(strStyleId, strOldNsid);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    this.LogError(ex);
                                }

                                // check if the style's numbering is modified
                                isStyleModified =
                                    isStyleModified || !AreNumberingsEqual(intOldAbsNum, intNewAbsNum);
                            }
                        }

                        // remove the old style definition
                        styleElem.Remove();
                    }
                    else if (styleElem.Type?.Value == StyleValues.Character)
                    {
                        // we keep this style
                        // <@3 remove link if this style is a "character" style that wrongly links to a "paragraph" style
                        var objLinkedStyle = styleElem.LinkedStyle;
                        if (objLinkedStyle != null && !string.IsNullOrEmpty(objLinkedStyle.Val))
                        {
                            // this style is a linked "character" style, so check if the pair is valid
                            if (_dictStyleIdToAbsNumId.ContainsKey(objLinkedStyle.Val))
                            {
                                // this style links to a style from the document template
                                if (!_dictParaStyleIdToCharStyleId.ContainsKey(objLinkedStyle.Val)
                                    || _dictParaStyleIdToCharStyleId[objLinkedStyle.Val] != strStyleId)
                                {
                                    styleElem.RemoveAllChildren<LinkedStyle>();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                blnSuccess = false;
            }

            return blnSuccess;
        }

        // copy selected default values of the styles part
        private bool TemplateToDoc_5CopyStyleDefaults()
        {
            // simple approach: clone the entire DocDefaults from the template
            try
            {
                // delete the doc element
                var elemDocDefaults = _globals.DocumentStyles.DocDefaults;
                elemDocDefaults?.Remove();

                // clone the template element
                var templateDocDefaults = _globals.TemplateStylesPart.DocDefaults;
                if (templateDocDefaults != null)
                {
                    elemDocDefaults = (DocDefaults)templateDocDefaults.Clone();
                    _globals.DocumentStyles.InsertAt(elemDocDefaults, 0);
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                return false;
            }

            return true;
        }

        // merges the template with the document (main function)
        // returns true if successful
        private bool TemplateToDoc()
        {
            bool blnSuccess = true;

            OpenXmlElement lastNumElem = default; // last element reference
            OpenXmlElement lastAbsNumElem = null /* TODO Change to default(_) if this is not a reference type */;
            int intNumberOfOldStyles = 0;

            try
            {
                // -- Determines the largest used numID and abstractNumId in the current document (used for insertions).
                // Find the location to append new num elements and abstractNum elements in the current document.
                blnSuccess = TemplateToDoc_1ReadDocumentNumbering(ref lastNumElem, ref lastAbsNumElem);

                // -- Appends copies of all num elements and abstractNum elements of the template to the document.
                // During this procedure, we must assign unique IDs to the copies.
                if (blnSuccess)
                {
                    blnSuccess = TemplateToDoc_2CopyNumbering(ref lastNumElem, ref lastAbsNumElem);
                }

                // -- Appends copies of the template styles to the document.
                if (blnSuccess)
                {
                    blnSuccess = TemplateToDoc_3CopyStyles(ref intNumberOfOldStyles);
                }

                // -- Removes styles that are redefined in the template.
                if (blnSuccess)
                {
                    blnSuccess = TemplateToDoc_4RemoveStyleDuplicates(ref intNumberOfOldStyles);
                }

                // -- Copies selected default values of the styles part.
                if (blnSuccess)
                {
                    blnSuccess = TemplateToDoc_5CopyStyleDefaults();
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                blnSuccess = false;
            }

            return blnSuccess;
        }

        // conversion of all paragraphs being descendants of the document body
        private bool ProcessDoc_1RepairParagraphs(WordprocessingDocument outputDocument, bool blnTopLevelParagraphsOnly)
        {
            bool blnSuccess = true;

            // document properties
            try
            {
                var bodyObj = OpenXmlTools.Parts.GetDocumentBody(outputDocument);
                if (bodyObj == null)
                {
                    return true;
                }

                // -------------------------------------------
                // Determine the paragraphs under consideration
                // -------------------------------------------
                var allParagraphs = blnTopLevelParagraphsOnly
                    ? bodyObj.Elements<Paragraph>()
                    : bodyObj.Descendants<Paragraph>();

                foreach (var parObj in allParagraphs)
                {
                    // -------------------------------------------
                    // Does the paragraph fulfill the requirements?
                    // -------------------------------------------

                    // CONTINUE FOR if there is no "paragraph property" (if there is no w:pPr child of w:p)
                    // CONTINUE FOR if there is no pStyle element
                    var pPrObj = parObj.GetFirstChild<ParagraphProperties>();
                    var pStyleObj = pPrObj?.GetFirstChild<ParagraphStyleId>();
                    if (pStyleObj == null)
                    {
                        continue;
                    }

                    string pStyleId = pStyleObj.Val;

                    // CONTINUE FOR if the style is not defined in the document template
                    if (!_dictStyleIdToAbsNumId.ContainsKey(pStyleId))
                    {
                        continue;
                    }

                    // CONTINUE FOR if there is no numbering attached to the style
                    int sAbsNumIdValue = _dictStyleIdToAbsNumId[pStyleId];
                    if (sAbsNumIdValue < 0)
                    {
                        continue;
                    }

                    // -----------------------------------------------------------------------------------
                    // => A numbered style from the document template is applied to the current paragraph.
                    // -----------------------------------------------------------------------------------

                    // is there a w:numPr element in the paragraph?
                    var numPrObj = pPrObj.GetFirstChild<NumberingProperties>();
                    if (numPrObj == null)
                    {
                        // CASE A: There is no numbering applied to the paragraph.
                        string nsid = null;
                        try
                        {
                            nsid = _dictStyleIdToOldNsid[pStyleId];
                        }
                        catch (Exception ex)
                        {
                            this.LogError(ex);
                        }

                        if (nsid == null)
                        {
                            // assertion violation
                            this.LogInfo("nsid unknown");
                            continue;
                        }

                        if (!_dictUsedNsid.ContainsKey(nsid))
                        {
                            // this is the first time the nsid is used, so we apply a restart

                            // mark nsid as used
                            _dictUsedNsid.Add(nsid, null);

                            // create a new num element with a unique ID
                            var restartNumObj = new NumberingInstance { NumberID = _nextNumIdValue };
                            _nextNumIdValue += 1;

                            // append the abstractNumId element
                            var restartAbsNumId = new AbstractNumId { Val = sAbsNumIdValue };
                            restartNumObj.AppendChild(restartAbsNumId);

                            // add restart information to new num element
                            AddRestartToNumElem(ref restartNumObj, pStyleId);

                            // append the num element to numberings.xml
                            _globals.DocumentNumbering.AppendChild(restartNumObj);

                            // attach new num element to this paragraph; we do not add any ilvl element
                            pPrObj.NumberingProperties = new NumberingProperties
                            {
                                NumberingId = new NumberingId { Val = restartNumObj.NumberID.Value },
                            };
                        }
                    }
                    else
                    {
                        // CASE B: There is a numbering applied to the paragraph.
                        // compare the paragraph ilvl to the style ilvl (if they exist)
                        var elemParaILvl = numPrObj.GetFirstChild<NumberingLevelReference>();
                        if (elemParaILvl != null)
                        {
                            // Perform the iLvl correction on the current paragraph
                            int intParaILvl = elemParaILvl.Val;
                            if (intParaILvl >= 0)
                            {
                                int intStyleILvl = _dictStyleIdToILvl[pStyleId];
                                if (intParaILvl != intStyleILvl)
                                {
                                    this.LogInfo("The ilvl of the paragraph was reset to the style ilvl.");
                                    elemParaILvl.Val = intStyleILvl; // correction
                                }
                            }
                        }

                        // ------------------------
                        // Check further assertions
                        // ------------------------

                        // search the abstractNumId that belongs to the numPr if it exists
                        var numIdObj = numPrObj.GetFirstChild<NumberingId>();
                        if (numIdObj == null)
                        {
                            // assertion violation
                            this.LogInfo("the numPr element does not contain a numId element");
                            continue;
                        }

                        int nNumIdValue = numIdObj.Val.Value;

                        // Enorm: Stelle die Nummerierung des Style wieder her, die vom Benutzer händisch entfernt wurde.
                        if (_restoreDeletedNumbering)
                        {
                            if (nNumIdValue == 0)
                            {
                                pPrObj.RemoveChild(numPrObj); // erzwingt default Werte für "numId" und "iLvl"
                                continue;
                            }
                        }

                        if (!_dictNumIdValToNum.ContainsKey(nNumIdValue))
                        {
                            // assertion violation
                            if (nNumIdValue <= 0 || nNumIdValue >= _nextNumIdValue)
                            {
                                // the Word document is definitely incorrect. we cannot deal with this case in the style clean up.
                                continue;
                            }

                            this.LogInfo(
                                "Word document incorrect? numId range seems to be 1 to " + _nextNumIdValue
                                                                                         + ", but accessing numId="
                                                                                         + nNumIdValue
                                                                                         + " did not work");
                            continue;
                        }

                        var numObj = _dictNumIdValToNum[nNumIdValue];
                        if (numObj == null)
                        {
                            // assertion violation
                            this.LogInfo("dictNumIdValToNumObj maps to nothing");
                            continue;
                        }

                        int nAbsNumIdValue = numObj.AbstractNumId.Val.Value;

                        // CONTINUE FOR if the style and the num element are linked to the same (new) list template (i.e. it is already repaired)
                        if (sAbsNumIdValue == nAbsNumIdValue)
                        {
                            continue;
                        }

                        if (nAbsNumIdValue >= _offsetAbsNumIdValue)
                        {
                            // assertion violation
                            this.LogInfo("several styles were combined with the same numbering");
                            continue;
                        }

                        // determine the nsid of the OLD list template referred to by the numId
                        if (!_dictAbsNumIdToNsid.ContainsKey(nAbsNumIdValue))
                        {
                            // assertion violation
                            this.LogInfo("dictAbsNumIdToNsid does not contain the key");
                        }
                        else
                        {
                            string nsid = _dictAbsNumIdToNsid[nAbsNumIdValue];

                            // ----------------------------
                            // all assertions are fulfilled
                            // ----------------------------

                            // update the num element: refer to the abstractNumId of the style from now on.
                            numObj.AbstractNumId.Val = sAbsNumIdValue;

                            // apply restart if it is the first time we encounter this list template
                            if (!_dictUsedNsid.ContainsKey(nsid))
                            {
                                // mark nsid as used
                                _dictUsedNsid.Add(nsid, null);

                                AddRestartToNumElem(ref numObj, pStyleId);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                blnSuccess = false;
            }

            return blnSuccess;
        }

        // remove the unused list templates from numberings.xml
        // @1: in addition, remove a list template if its nsid value conflicts with a new list template
        private bool ProcessDoc_2RemoveUnusedListTemplates()
        {
            bool blnSuccess = true;

            try
            {
                // Delete the unused list templates.
                var referredAbsNumIdArray = new bool[_nextAbsNumIdValue + 1];

                // The reversed order is necessary since we make use of the fact that the children of numbering elements are sorted.
                // First, we collect the abstractNum references, then we delete the unused elements.
                for (int intI = _globals.DocumentNumbering.ChildElements.Count - 1; intI >= 0; intI -= 1)
                {
                    var elem = _globals.DocumentNumbering.ChildElements[intI];

                    if (elem is AbstractNum absNumObj)
                    {
                        if (!referredAbsNumIdArray[absNumObj.AbstractNumberId.Value])
                        {
                            absNumObj.Remove();
                        }
                    }
                    else if (elem is NumberingInstance objNum)
                    {
                        int abstractNumId = objNum.AbstractNumId.Val;

                        string strNsid = _dictAbsNumIdToNsid.ContainsKey(abstractNumId) ? _dictAbsNumIdToNsid[abstractNumId] : "";

                        // does the num element refer to an abstract num element with a template nsid number?
                        if (_dictNsidToAbsNumId.ContainsKey(strNsid))
                        {
                            // yes, so take care

                            // is +this the new definition itself?
                            if (abstractNumId == _dictNsidToAbsNumId[strNsid])
                            {
                                // mark as used
                                referredAbsNumIdArray[abstractNumId] = true;
                            }
                            else
                            {
                                // first, set the abstractNumId to the new definition and
                                objNum.AbstractNumId.Val = _dictNsidToAbsNumId[strNsid];
                            }
                        }
                        else
                        {
                            // no, so mark as used
                            referredAbsNumIdArray[abstractNumId] = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                blnSuccess = false;
            }

            return blnSuccess;
        }

        // renumber the remaining list templates consecutively
        // NOTE: This function invalidates the previously stored relations to the absNum values in the dictionaries!
        private bool ProcessDoc_3RenumberListTemplates()
        {
            bool blnSuccess = true;

            // map(oldAbsNum) returns newAbsNum if oldAbsNum is valid
            var mapOldAbsNumToNewAbsNum = new int[_nextAbsNumIdValue + 1];

            try
            {
                int intCurrentAbsNum = 0;

                // The reversed order is necessary since we make use of the fact that the children of numbering elements are sorted.
                // First, we collect the abstractNum references, then we delete the unused elements.
                for (int intI = 0; intI <= _globals.DocumentNumbering.ChildElements.Count - 1; intI++)
                {
                    var elem = _globals.DocumentNumbering.ChildElements[intI];

                    if (elem is AbstractNum absNumObj)
                    {
                        // map the former absNum to the newAbsNum and apply the new absNum
                        mapOldAbsNumToNewAbsNum[absNumObj.AbstractNumberId.Value] = intCurrentAbsNum;
                        absNumObj.AbstractNumberId = intCurrentAbsNum;
                        intCurrentAbsNum += 1;
                    }
                    else if (elem is NumberingInstance objNum)
                    {
                        // adjust the abstractNumId
                        objNum.AbstractNumId.Val = mapOldAbsNumToNewAbsNum[objNum.AbstractNumId.Val];
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                blnSuccess = false;
            }

            return blnSuccess;
        }

        private bool ProcessDoc_4RemoveLinkStylesOption(WordprocessingDocument outputDocument)
        {
            bool blnSuccess = true;

            try
            {
                var settings = OpenXmlTools.Parts.GetSettings(outputDocument);
                settings.RemoveAllChildren<LinkStyles>();
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                blnSuccess = false;
            }

            return blnSuccess;
        }

        public static class OpenXmlNs
        {
            public const string W15 = "http://schemas.microsoft.com/office/word/2012/wordml";
            public const string W = "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
        }
    }
}
