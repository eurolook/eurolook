using System;
using System.Runtime.Serialization;

namespace Eurolook.StyleCleanUp
{
    [Serializable]
    public class StyleCleanUpException : Exception
    {
        public StyleCleanUpException()
        {
        }

        public StyleCleanUpException(string message)
            : base(message)
        {
        }

        public StyleCleanUpException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected StyleCleanUpException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}
