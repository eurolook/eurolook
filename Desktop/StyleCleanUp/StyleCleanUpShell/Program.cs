﻿using System;
using CommandLine;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.StyleCleanUp;

namespace Eurolook.StyleCleanUpShell
{
    public static class Program
    {
        static Program()
        {
            LogManager.LogFileName = "Eurolook.WordAddIn";
            LogManager.AssemblyVersion = typeof(Program).Assembly.GetName().Version;
        }

        private static int Main(string[] args)
        {
            LogStartupMessage();

            // parse options
            var parser = new Parser(with => { with.HelpWriter = Console.Error; });
            return parser
                   .ParseArguments<Options>(args)
                   .MapResult(
                       RunAndReturnExitCode,
                       _ => -1);
        }

        private static int RunAndReturnExitCode(Options options)
        {
            try
            {
                var styleCleaner = new StyleCleaner();
                styleCleaner.MakeClean(
                    options.TemplateFullPath,
                    options.InputFullName,
                    options.OutputFullName,
                    options.RestoreDeletedNumbering);
            }
            catch (Exception ex)
            {
                LogManager.GetLogger().Error("Error executing style clean up.", ex);
                return -1;
            }

            return 0;
        }

        private static void LogStartupMessage()
        {
            var logger = LogManager.GetLogger();
            var assembly = typeof(Program).Assembly;
            string userName = $"{Environment.UserDomainName}\\{Environment.UserName}";
            logger.LogStartupMessage(assembly, userName);
        }
    }
}
