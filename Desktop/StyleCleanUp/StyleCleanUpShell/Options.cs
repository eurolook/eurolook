using System.IO;
using CommandLine;
using JetBrains.Annotations;

namespace Eurolook.StyleCleanUpShell
{
    [UsedImplicitly]
    public class Options
    {
        [UsedImplicitly]
        [Option('t', "template", Required = true, HelpText = "The template containing the original style definitions.")]
        public string Template { get; set; }

        [UsedImplicitly]
        [Option('i', "input", Required = true, HelpText = "The document to be cleaned up.")]
        public string Input { get; set; }

        [UsedImplicitly]
        [Option('o', "output", Required = true, HelpText = "The cleaned output document to be created.")]
        public string Output { get; set; }

        [UsedImplicitly]
        [Option('r', "restoreDeletedNumbering", HelpText = "Restore manually deleted numbering.")]
        public bool RestoreDeletedNumbering { get; set; }

        public string TemplateFullPath => Path.GetFullPath(Template);

        public string InputFullName => Path.GetFullPath(Input);

        public string OutputFullName => Path.GetFullPath(Output);
    }
}
