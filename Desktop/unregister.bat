@echo off
setlocal
cd /d "%~dp0"

goto check_Permissions

:check_permissions
REM Quick test for Windows generation: UAC aware or not ; all OS before NT4 ignored for simplicity
SET NewOSWith_UAC=YES
VER | FINDSTR /IL "5." > NUL
IF %ERRORLEVEL% == 0 SET NewOSWith_UAC=NO
VER | FINDSTR /IL "4." > NUL
IF %ERRORLEVEL% == 0 SET NewOSWith_UAC=NO
VER | FINDSTR /IL "Verion 10." > NUL
IF %ERRORLEVEL% == 0 SET NewOSWith_UAC=YES

CALL NET SESSION >nul 2>&1
IF NOT %ERRORLEVEL% == 0 (

	if /i "%NewOSWith_UAC%"=="YES" (
		echo Restarting script as administrator
        rem Start batch again with UAC
        echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
        echo UAC.ShellExecute "%~s0", "", "", "runas", 1 >> "%temp%\getadmin.vbs"

		cscript //nologo "%temp%\getadmin.vbs"
        del "%temp%\getadmin.vbs"
		exit /B
	)
	goto :eof
)

:unregister_addin

REM Cleanup old Eurolook registrations
reg DELETE "HKCR\Eurolook.WordAddIn.2013.TaskPaneContentControl" /f
reg DELETE "HKCR\Eurolook.WordAddIn.2013" /f
reg DELETE "HKCR\Eurolook.WordAddIn.ComAddInAutomationService" /f
reg DELETE "HKCR\Eurolook.WordAddIn.UpdateNotifier" /f
reg DELETE "HKEY_CLASSES_ROOT\Wow6432Node\CLSID\{2ae023f6-3ac9-3258-ac27-8dd3ebbaa828}" /f
reg DELETE "HKEY_CLASSES_ROOT\Wow6432Node\CLSID\{8E384175-5CA8-4081-B11D-1CC10DECD5BF}" /f
reg DELETE "HKEY_CLASSES_ROOT\Wow6432Node\CLSID\{4C938F51-5DC8-40F1-B87C-78615BB58196}" /f
reg DELETE "HKEY_CLASSES_ROOT\Wow6432Node\CLSID\{6E1C0429-44EE-4780-96D3-4DFD3A218D25}" /f
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Classes\Eurolook.ExcelAddIn.10" /f
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Classes\Eurolook.PowerPointAddIn.10" /f
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Classes\Eurolook.WordAddIn.10" /f
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Classes\Wow6432Node\CLSID\{3CBFEF40-1BCD-4893-AFE6-8F0671CC61A6}" /f
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Classes\Wow6432Node\CLSID\{8D26C024-DAEE-4B32-ABEF-20B7769979AE}" /f
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Classes\Wow6432Node\CLSID\{9580E02E-D75E-4885-A0B5-1032700564A6}" /f
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\14.0\Common\CustomUIValidationCache" /v Eurolook.WordAddIn.2013.Microsoft.Word.Document /f
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\15.0\Common\CustomUIValidationCache" /v Eurolook.WordAddIn.2013.Microsoft.Word.Document /f
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\Common\CustomUIValidationCache" /v Eurolook.WordAddIn.2013.Microsoft.Word.Document /f
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\Common\CustomUIValidationCache" /v Eurolook.WordAddIn.10.Microsoft.Word.Document /f
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\Common\CustomUIValidationCache" /v Eurolook.OfficeAddIn.10.Microsoft.Word.Document /f
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\15.0\Word\AddInLoadTimes" /v Eurolook.WordAddIn.2013 /f
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\Word\AddInLoadTimes" /v Eurolook.WordAddIn.2013 /f
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\Word\AddInLoadTimes" /v Eurolook.WordAddIn.10 /f
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\Word\AddInLoadTimes" /v Eurolook.OfficeAddIn.10 /f
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\16.0\Word\AddinEventTimes\Connect" /v Eurolook.OfficeAddIn.10 /f
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\16.0\Word\AddinEventTimes\Shutdown" /v Eurolook.OfficeAddIn.10 /f
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\Word\Addins\Eurolook.WordAddIn.2013" /f
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\Word\Addins\Eurolook.WordAddIn.10" /f
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Excel\Addins\Eurolook.OfficeAddIn.10" /f
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Excel\AddinsData\Eurolook.WordAddIn.10" /f
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Excel\AddinsData\Eurolook.OfficeAddIn.10" /f
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\PowerPoint\Addins\Eurolook.OfficeAddIn.10" /f
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\PowerPoint\AddinsData\Eurolook.WordAddIn.10" /f
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\PowerPoint\AddinsData\Eurolook.OfficeAddIn.10" /f
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Word\Addins\Eurolook.OfficeAddIn.10" /f
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Word\AddinsData\Eurolook.WordAddIn.10" /f
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Word\AddinsData\Eurolook.OfficeAddIn.10" /f

REM Entries created from within O365
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Classes\Eurolook.WordAddIn.10" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Classes\Eurolook.OfficeAddIn.10" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Classes\CLSID\{2ae023f6-3ac9-3258-ac27-8dd3ebbaa828}" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Classes\CLSID\{8E384175-5CA8-4081-B11D-1CC10DECD5BF}" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Classes\CLSID\{4C938F51-5DC8-40F1-B87C-78615BB58196}" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Classes\CLSID\{6E1C0429-44EE-4780-96D3-4DFD3A218D25}" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Classes\Wow6432Node\CLSID\{2ae023f6-3ac9-3258-ac27-8dd3ebbaa828}" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Classes\Wow6432Node\CLSID\{8E384175-5CA8-4081-B11D-1CC10DECD5BF}" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Classes\Wow6432Node\CLSID\{4C938F51-5DC8-40F1-B87C-78615BB58196}" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Classes\Wow6432Node\CLSID\{6E1C0429-44EE-4780-96D3-4DFD3A218D25}" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Microsoft\Office\Excel\AddIns\Eurolook.OfficeAddIn.10" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Microsoft\Office\PowerPoint\AddIns\Eurolook.OfficeAddIn.10" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Microsoft\Office\Word\AddIns\Eurolook.OfficeAddIn.10" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Microsoft\Office\Word\AddIns\Eurolook.WordAddIn.10" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Wow6432Node\Microsoft\Office\Excel\AddIns\Eurolook.OfficeAddIn.10" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Wow6432Node\Microsoft\Office\PowerPoint\AddIns\Eurolook.OfficeAddIn.10" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Wow6432Node\Microsoft\Office\Word\AddIns\Eurolook.OfficeAddIn.10" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Wow6432Node\Microsoft\Office\Word\AddIns\Eurolook.WordAddIn.10" /f


reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Classes\Eurolook.OfficeAddIn.10" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Classes\Eurolook.WordAddIn.TaskPaneContentControl.10" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Classes\Eurolook.OfficeAddIn.TaskPaneContentControl.10" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Classes\WOW6432Node\CLSID\{2AE023F6-3AC9-3258-AC27-8DD3EBBAA828}" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Classes\WOW6432Node\CLSID\{6E1C0429-44EE-4780-96D3-4DFD3A218D25}" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\Office\Word\Addins\Eurolook.OfficeAddIn.10" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Classes\CLSID\{2AE023F6-3AC9-3258-AC27-8DD3EBBAA828" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Classes\CLSID\{6E1C0429-44EE-4780-96D3-4DFD3A218D25}" /f

REM EurolookLink
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Classes\el10" /f
reg DELETE "HKEY_CLASSES_ROOT\el10" /f

REM Event Log Registration
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\WINEVT\Channels\Dialogika-Eurolook-WordAddIn/Operational" /f
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\WINEVT\Publishers\{8c96ec8d-5dfc-4b71-a244-c075e8b23a83}" /f

"%WINDIR%\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe" /unregister OfficeAddIn\bin\x86\Debug\net48\Eurolook.OfficeAddIn.dll
IF NOT %ERRORLEVEL% == 0 (goto error)

"%WINDIR%\SysWOW64\regsvr32.exe" /u /s "OfficeAddIn\bin\x86\Debug\net48\Eurolook.AddInShim.dll"
IF NOT %ERRORLEVEL% == 0 (goto error)

call unregisterHKCU.bat

goto :eof

:error
pause
