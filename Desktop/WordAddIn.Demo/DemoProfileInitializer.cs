﻿using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.ProfileInitializer;
using Eurolook.AddIn.Common.UserConfiguration;

namespace Eurolook.WordAddIn.Demo
{
    public class DemoProfileInitializer
        : ProfileInitializerBase
    {
        public DemoProfileInitializer(
            IUserDataRepository userDataRepository,
            IAuthorRepository authorRepository,
            ISettingsService settingsService,
            IUserIdentity userIdentity,
            IClientDatabaseManager clientDatabaseManager,
            ColorManager colorManager,
            DataSyncScheduler dataSyncScheduler)
            : base(
                  userDataRepository,
                  authorRepository,
                  settingsService,
                  userIdentity,
                  clientDatabaseManager,
                  colorManager,
                  dataSyncScheduler)
        {
        }

        protected override bool IsWelcomeWindowActivated => false;

        public override bool? ShowWelcomeWindow()
        {
            return false;
        }
    }
}
