﻿using System;
using Eurolook.Common.Log;
using Eurolook.WordAddIn.Compatibility;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.Demo.Compatibility
{
    public class DemoCompatibilityModeManager : CompatibilityModeManager, ICompatibilityModeManager, ICanLog
    {
        public DemoCompatibilityModeManager(
            Func<Document, ICompatibilityMode> compatibilityModeCreateFunc)
            : base(compatibilityModeCreateFunc)
        {
        }
    }
}
