﻿using Autofac;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Authors;
using Eurolook.WordAddIn.Compatibility;
using Eurolook.WordAddIn.Demo.Compatibility;
using Eurolook.WordAddIn.Demo.Ribbon;
using Eurolook.WordAddIn.Ribbon;

namespace Eurolook.WordAddIn.Demo
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<BasicUserIdentity>()
                .AsImplementedInterfaces()
                .SingleInstance();

            builder.RegisterType<DemoProfileInitializer>()
                .SingleInstance()
                .AsImplementedInterfaces();

            builder.RegisterType<DemoHomeTabViewModel>()
                .AsSelf()
                .AsImplementedInterfaces()
                .ExternallyOwned();

            builder.RegisterType<TaskPaneButtonHandler>()
                .AsImplementedInterfaces()
                .SingleInstance();

            builder.RegisterType<DemoRibbon>()
                   .AsImplementedInterfaces()
                   .SingleInstance();

            builder.RegisterType<DemoCompatibilityModeManager>()
                .AsImplementedInterfaces()
                .SingleInstance();

            builder.RegisterType<NonEurolookDocumentCompatibilityMode>()
                   .AsImplementedInterfaces();

            builder.RegisterType<Eurolook.AddIn.Common.Properties.ResourceManager>().AsSelf().AsImplementedInterfaces();
            builder.RegisterType<WordAddIn.Properties.ResourceManager>().AsSelf().AsImplementedInterfaces();
            builder.RegisterType<Properties.ResourceManager>().AsSelf().AsImplementedInterfaces();

            builder.RegisterType<DefaultAuthorEditorValidator>()
                .AsImplementedInterfaces();

            builder.RegisterType<JobEditorValidator>()
                .AsImplementedInterfaces();

            builder.RegisterType<WorkplaceEditorValidator>()
                .AsImplementedInterfaces();
        }
    }
}
