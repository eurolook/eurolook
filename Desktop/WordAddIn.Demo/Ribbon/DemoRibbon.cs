﻿using System;
using System.Collections.Generic;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Properties;
using Eurolook.WordAddIn.Ribbon;

namespace Eurolook.WordAddIn.Demo.Ribbon
{
    public class DemoRibbon : WordRibbonBase
    {
        public DemoRibbon(
                Lazy<IAddinContext> addinContext,
                Lazy<IWordApplicationContext> applicationContext,
                Lazy<IMessageService> messageService,
                Lazy<ISettingsService> settingsService,
                Lazy<IDocumentManager> documentManager,
                Lazy<ITaskPaneButtonHandler> taskPaneButtonHandler,
                Lazy<IEmbeddedResourceManager> embeddedResourceManager)
        : base(addinContext, applicationContext, messageService, settingsService, documentManager, embeddedResourceManager)
        {
            ControlMap = new Dictionary<string, RibbonControlHandler>
            {
                {
                    "EurolookTaskPaneButton", new RibbonControlHandler
                    {
                        OnToggleAction = (_, pressed) => taskPaneButtonHandler.Value.OnEurolookTaskPaneButton(pressed),
                        GetEnabled = _ => taskPaneButtonHandler.Value.IsTaskPaneButtonEnabled(ApplicationContext),
                        GetPressed = _ => taskPaneButtonHandler.Value.IsTaskPaneButtonPressed(ApplicationContext),
                    }
                }
            };
        }

        public override string GetCustomUI(string ribbonId)
        {
            return GetResourceTextInAssembliesOfTypeHierarchy("Ribbon", "DemoRibbon.xml", GetType());
        }
    }
}
