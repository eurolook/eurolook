﻿using Eurolook.Common;
using Eurolook.Common.SecretsProvider;
using Eurolook.DataSync;

namespace Eurolook.WordAddIn.Demo
{
    public class DemoSecretsProvider : ISecretsProvider
    {
        public string RetrieveDataSyncServicePassword(string input)
        {
            return Aes.Decrypt256(
                DataSyncSettings.Instance.DataSyncServicePasswordEncrypted,
                Aes.Sha256(typeof(Aes).FullName));
        }
    }
}
