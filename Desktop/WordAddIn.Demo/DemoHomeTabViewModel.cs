﻿using System;
using System.Collections.Generic;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Authors;
using Eurolook.AddIn.Common.Email;
using Eurolook.AddIn.Common.ProfileInitializer;
using Eurolook.AddIn.Common.SystemConfiguration;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.WordAddIn.DocumentCreation;
using Eurolook.WordAddIn.Options;
using Eurolook.WordAddIn.ViewModels;
using Eurolook.WordAddIn.Wizard;

namespace Eurolook.WordAddIn.Demo
{
    public class DemoHomeTabViewModel : HomeTabViewModel
    {
        public DemoHomeTabViewModel(
            IWordApplicationContext applicationContext,
            IMessageService messageService,
            ISettingsService settingsService,
            Lazy<IProfileInitializer> profileInitializer,
            InitializationStatusViewModel initializationStatusVm,
            IEnumerable<IBeforeNewDocumentCreationTask> beforeNewDocumentCreationTasks,
            IEnumerable<IBeforeNewDocumentFromTemplateStoreCreationTask> beforeNewDocumentFromTemplateStoreCreationTasks,
            IEnumerable<IAfterNewDocumentCreationTask> afterNewDocumentCreationTasks,
            IEnumerable<IAfterNewDocumentFromTemplateStoreCreationTask> afterNewDocumentFromTemplateStoreCreationTasks,
            Func<CreateDocumentViewModel> createDocumentViewModelFunc,
            Func<CreateDocumentWizardViewModel> createDocumentWizardViewModelFunc,
            IDocumentCreationService documentCreationService,
            ISystemConfigurationService systemConfigurationService,
            Lazy<IEmailService> emailService,
            Func<AuthorManagerViewModel2> authorManagerViewModel2Func,
            Func<OptionsViewModel> optionsViewModelCreateFunc,
            ColorManager colorManager)
            : base(applicationContext, messageService, settingsService, profileInitializer, initializationStatusVm, beforeNewDocumentCreationTasks, beforeNewDocumentFromTemplateStoreCreationTasks, afterNewDocumentCreationTasks, afterNewDocumentFromTemplateStoreCreationTasks, createDocumentViewModelFunc, createDocumentWizardViewModelFunc, documentCreationService, systemConfigurationService, emailService, authorManagerViewModel2Func, optionsViewModelCreateFunc, colorManager)
        {
            InsertCreateDocumentTile().FireAndForgetSafeAsync(this.LogError);
            UpdateColorScheme();
        }
    }
}
