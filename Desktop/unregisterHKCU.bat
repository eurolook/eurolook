@echo off
setlocal
cd /d "%~dp0"

:unregister_addin
echo Unregistering Eurolook for current user

set REGASMEXE=RegAsm.x86.exe
set ASMNAME=Eurolook.WordAddIn.dll
set TEMP_REG_FILE=%TMP%\%ASMNAME%.reg

echo Unregistering any x64 versions
for /D %%P in ("bin\x64\*") do (
    set REGASMEXE=RegAsm.x64.exe

    if exist "%%P\%ASMNAME%" (
        echo Unregistering %%P\%ASMNAME%

        @REM Copy app.config so that all binding redirects defined in the config are considered, see https://stackoverflow.com/a/20135641/40347
        copy "%%P\%ASMNAME%.config" "%REGASMEXE%.config"

        %REGASMEXE% /nologo /regfile:"%TEMP_REG_FILE%" "%%P\%ASMNAME%"
        powershell -ExecutionPolicy Bypass -Command "(gc '%TEMP_REG_FILE%') -replace 'HKEY_CLASSES_ROOT', '-HKEY_CURRENT_USER\Software\Classes' | Out-File '%TEMP_REG_FILE%'"
        reg.exe import "%TEMP_REG_FILE%"
        del "%TEMP_REG_FILE%"
        del "%REGASMEXE%.config"
        IF NOT %ERRORLEVEL% == 0 (goto error)
    )

    if exist "%%P\Eurolook.AddInShim.dll" (
        echo Unregistering "%%P\Eurolook.AddInShim.dll"
        ..\BuildTools\RegSvrEx\Tool\x64\RegSvrEx.exe /u /c "%%P\Eurolook.AddInShim.dll"
        IF NOT %ERRORLEVEL% == 0 (goto error)
    )
)

echo Unregistering any x86 versions
for /D %%P in ("bin\x86\*") do (
    set REGASMEXE=RegAsm.x86.exe

    if exist "%%P\%ASMNAME%" (
        echo Unregistering %%P\%ASMNAME%

        @REM Copy app.config so that all binding redirects defined in the config are considered, see https://stackoverflow.com/a/20135641/40347
        copy "%%P\%ASMNAME%.config" "%REGASMEXE%.config"

        %REGASMEXE% /nologo /regfile:"%TEMP_REG_FILE%" "%%P\%ASMNAME%"
        powershell -ExecutionPolicy Bypass -Command "(gc '%TEMP_REG_FILE%') -replace 'HKEY_CLASSES_ROOT', '-HKEY_CURRENT_USER\Software\Classes' | Out-File '%TEMP_REG_FILE%'"
        reg.exe import "%TEMP_REG_FILE%"
        del "%TEMP_REG_FILE%"
        del "%REGASMEXE%.config"
        IF NOT %ERRORLEVEL% == 0 (goto error)
    )

    if exist "%%P\Eurolook.AddInShim.dll" (
        echo Unregistering "%%P\Eurolook.AddInShim.dll"
        ..\BuildTools\RegSvrEx\Tool\x86\RegSvrEx.exe /u /c "%%P\Eurolook.AddInShim.dll"
        IF NOT %ERRORLEVEL% == 0 (goto error)
    )
)

IF %ERRORLEVEL% NEQ 0 (
    echo "Error unregistering"
    EXIT /B %ERRORLEVEL%
)

@REM ExcelAddIn
reg DELETE "HKEY_CURRENT_USER\Software\Classes\CLSID\{3CBFEF40-1BCD-4893-AFE6-8F0671CC61A6}" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Classes\WOW6432Node\CLSID\{3CBFEF40-1BCD-4893-AFE6-8F0671CC61A6}" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Classes\Eurolook.ExcelAddIn.10" /f 2> nul

@REM PowerPointAddIn
reg DELETE "HKEY_CURRENT_USER\Software\Classes\CLSID\{9580E02E-D75E-4885-A0B5-1032700564A6}" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Classes\WOW6432Node\CLSID\{9580E02E-D75E-4885-A0B5-1032700564A6}" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Classes\Eurolook.PowerPointAddIn.10" /f 2> nul

@REM WordAddIn
reg DELETE "HKEY_CURRENT_USER\Software\Classes\CLSID\{8D26C024-DAEE-4B32-ABEF-20B7769979AE}" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Classes\WOW6432Node\CLSID\{8D26C024-DAEE-4B32-ABEF-20B7769979AE}" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Classes\Eurolook.WordAddIn.10" /f 2> nul

@REM OfficeAddIn
reg DELETE "HKEY_CURRENT_USER\Software\Classes\CLSID\{6D6712AD-D134-4141-BE61-FF4332D2ACB4}" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Classes\WOW6432Node\CLSID\{6D6712AD-D134-4141-BE61-FF4332D2ACB4}" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Classes\Eurolook.OfficeAddIn.10" /f 2> nul

@REM AddInShim
reg DELETE "HKEY_CURRENT_USER\Software\Classes\CLSID\{2AE023F6-3AC9-3258-AC27-8DD3EBBAA828}" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Classes\WOW6432Node\CLSID\{2AE023F6-3AC9-3258-AC27-8DD3EBBAA828}" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Classes\Eurolook.OfficeAddIn.10" /f 2> nul

@REM Eurolook.WordAddIn.UpdateNotifier (obsolote)
reg DELETE "HKEY_CURRENT_USER\Software\Classes\CLSID\{8E384175-5CA8-4081-B11D-1CC10DECD5BF}" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Classes\WOW6432Node\CLSID\{8D26C024-DAEE-4B32-ABEF-20B7769979AE}" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Classes\Eurolook.WordAddIn.UpdateNotifier" /f 2> nul

@REM Eurolook.WordAddIn.ComAddInAutomationService.10
reg DELETE "HKEY_CURRENT_USER\Software\Classes\CLSID\{4C938F51-5DC8-40F1-B87C-78615BB58196}" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Classes\WOW6432Node\CLSID\{4C938F51-5DC8-40F1-B87C-78615BB58196}" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Classes\Eurolook.WordAddIn.ComAddInAutomationService.10" /f 2> nul

@REM Eurolook.WordAddIn.TaskPaneContentControl.10
reg DELETE "HKEY_CURRENT_USER\Software\Classes\CLSID\{6E1C0429-44EE-4780-96D3-4DFD3A218D25}" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Classes\WOW6432Node\CLSID\{6E1C0429-44EE-4780-96D3-4DFD3A218D25}" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Classes\Eurolook.WordAddIn.TaskPaneContentControl.10" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Classes\Eurolook.OfficeAddIn.TaskPaneContentControl.10" /f 2> nul

reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\Common\CustomUIValidationCache" /v Eurolook.ExcelAddIn.2013.Microsoft.Excel.Workbook /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\Common\CustomUIValidationCache" /v Eurolook.OfficeAddIn.10.Microsoft.Excel.Workbook /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\Common\CustomUIValidationCache" /v Eurolook.OfficeAddIn.10.Microsoft.PowerPoint.Presentation /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\14.0\Common\CustomUIValidationCache" /v Eurolook.WordAddIn.2013.Microsoft.Word.Document /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\15.0\Common\CustomUIValidationCache" /v Eurolook.WordAddIn.2013.Microsoft.Word.Document /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\Common\CustomUIValidationCache" /v Eurolook.WordAddIn.2013.Microsoft.Word.Document /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\Common\CustomUIValidationCache" /v Eurolook.OfficeAddIn.10.Microsoft.Word.Document /f 2> nul

reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\Excel\AddInLoadTimes" /v Eurolook.WordAddIn.10 /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\Excel\AddInLoadTimes" /v Eurolook.ExcelAddIn.10 /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\Excel\AddInLoadTimes" /v Eurolook.OfficeAddIn.10 /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\15.0\PowerPoint\AddInLoadTimes" /v Eurolook.WordAddIn.2013 /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\PowerPoint\AddInLoadTimes" /v Eurolook.WordAddIn.2013 /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\PowerPoint\AddInLoadTimes" /v Eurolook.WordAddIn.10 /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\PowerPoint\AddInLoadTimes" /v Eurolook.OfficeAddIn.10 /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\15.0\Word\AddInLoadTimes" /v Eurolook.WordAddIn.2013 /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\Word\AddInLoadTimes" /v Eurolook.WordAddIn.2013 /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\Word\AddInLoadTimes" /v Eurolook.WordAddIn.10 /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\Word\AddInLoadTimes" /v Eurolook.OfficeAddIn.10 /f 2> nul

@REM Add-in registration
@REM Excel
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Excel\Addins\Eurolook.ExcelAddIn.10" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Excel\Addins\Eurolook.WordAddIn.10" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Excel\Addins\Eurolook.OfficeAddIn.10" /f 2> nul

@REM PowerPoint
reg DELETE "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\PowerPoint\Addins\Eurolook.PowerPointAddIn.10" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\PowerPoint\Addins\Eurolook.WordAddIn.2013" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\PowerPoint\Addins\Eurolook.WordAddIn.10" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\PowerPoint\Addins\Eurolook.OfficeAddIn.10" /f 2> nul

@REM Word
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\Word\Addins\Eurolook.WordAddIn.2013" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\Word\Addins\Eurolook.WordAddIn.10" /f 2> nul
reg DELETE "HKEY_CURRENT_USER\Software\Microsoft\Office\Word\Addins\Eurolook.OfficeAddIn.10" /f 2> nul

@REM Add-in registration modified via O365
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Classes\Wow6432Node\CLSID\{3CBFEF40-1BCD-4893-AFE6-8F0671CC61A6}" /f 2> nul
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Classes\CLSID\{3CBFEF40-1BCD-4893-AFE6-8F0671CC61A6}" /f 2> nul
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Microsoft\Office\Excel\AddIns\Eurolook.OfficeAddIn.10" /f 2> nul
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Wow6432Node\Microsoft\Office\Excel\AddIns\Eurolook.OfficeAddIn.10" /f 2> nul

reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Classes\Wow6432Node\CLSID\{9580E02E-D75E-4885-A0B5-1032700564A6}" /f 2> nul
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Classes\CLSID\{9580E02E-D75E-4885-A0B5-1032700564A6}" /f 2> nul
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Microsoft\Office\PowerPoint\AddIns\Eurolook.OfficeAddIn.10" /f 2> nul
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Wow6432Node\Microsoft\Office\PowerPoint\AddIns\Eurolook.OfficeAddIn.10" /f 2> nul

reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Classes\Wow6432Node\CLSID\{8D26C024-DAEE-4B32-ABEF-20B7769979AE}" /f 2> nul
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Classes\CLSID\{8D26C024-DAEE-4B32-ABEF-20B7769979AE}" /f 2> nul
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Microsoft\Office\Word\AddIns\Eurolook.OfficeAddIn.10" /f 2> nul
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Wow6432Node\Microsoft\Office\Word\AddIns\Eurolook.OfficeAddIn.10" /f 2> nul

reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Classes\Wow6432Node\CLSID\{2AE023F6-3AC9-3258-AC27-8DD3EBBAA828}" /f 2> nul
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Classes\CLSID\{2AE023F6-3AC9-3258-AC27-8DD3EBBAA828}" /f 2> nul
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Classes\Eurolook.OfficeAddIn.10" /f 2> nul

reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Classes\Wow6432Node\CLSID\{6D6712AD-D134-4141-BE61-FF4332D2ACB4}" /f 2> nul
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Classes\CLSID\{6D6712AD-D134-4141-BE61-FF4332D2ACB4}" /f 2> nul

reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Classes\Wow6432Node\CLSID\{6D6712AD-D134-4141-BE61-FF4332D2ACB4}" /f 2> nul
reg DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\ClickToRun\REGISTRY\MACHINE\Software\Classes\CLSID\{6D6712AD-D134-4141-BE61-FF4332D2ACB4}" /f 2> nul

echo Successfully unregistered Eurolook for current user
EXIT /B 0

goto :eof

:error
pause
