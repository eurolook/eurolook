@echo off
setlocal enableDelayedExpansion
cd /d "%~dp0"

:cleanup
call unregisterHKCU.bat

:register_addin

set TARGETPATH=%~1%

if "%2" EQU "" (set PLATFORM=x86) else (set PLATFORM=%2)

if "%TARGETPATH%" EQU "" (
    set TARGETPATH=%~dp0%OfficeAddIn\bin\x86\Debug\net6.0-windows\Eurolook.OfficeAddIn.dll
    echo "No target path specified, using default target path !TARGETPATH!"
)

set COMHOSTSUFFIX=.comhost.dll

call set COMHOSTPATH=%%TARGETPATH:.dll=%COMHOSTSUFFIX%%%

IF NOT %ERRORLEVEL% == 0 (goto error)

@echo on

reg ADD "HKEY_CURRENT_USER\SOFTWARE\Classes\Eurolook.OfficeAddIn.10" /f /ve /d "Eurolook.OfficeAddIn.ThisAddIn"
reg ADD "HKEY_CURRENT_USER\SOFTWARE\Classes\Eurolook.OfficeAddIn.10\CLSID" /f /ve /d "{6D6712AD-D134-4141-BE61-FF4332D2ACB4}"
reg ADD "HKEY_CURRENT_USER\SOFTWARE\Classes\Eurolook.OfficeAddIn.TaskPaneContentControl.10" /f /ve /d "Eurolook.OfficeAddIn.TaskPaneContentControl"
reg ADD "HKEY_CURRENT_USER\SOFTWARE\Classes\Eurolook.OfficeAddIn.TaskPaneContentControl.10\CLSID" /f /ve /d "{6E1C0429-44EE-4780-96D3-4DFD3A218D25}"

if %PLATFORM% EQU x86 (
    reg add "HKEY_CURRENT_USER\SOFTWARE\Classes\WOW6432Node\CLSID\{6D6712AD-D134-4141-BE61-FF4332D2ACB4}" /f /ve /d "CoreCLR COMHost Server"
    reg add "HKEY_CURRENT_USER\SOFTWARE\Classes\WOW6432Node\CLSID\{6D6712AD-D134-4141-BE61-FF4332D2ACB4}\InProcServer32" /f /ve /d "%COMHOSTPATH%"
    reg add "HKEY_CURRENT_USER\SOFTWARE\Classes\WOW6432Node\CLSID\{6D6712AD-D134-4141-BE61-FF4332D2ACB4}\InProcServer32" /f /v ThreadingModel /t REG_SZ /d "Both"
    reg add "HKEY_CURRENT_USER\SOFTWARE\Classes\WOW6432Node\CLSID\{6D6712AD-D134-4141-BE61-FF4332D2ACB4}\ProgID" /f /ve /d "Eurolook.OfficeAddIn.10"

    reg add "HKEY_CURRENT_USER\SOFTWARE\Classes\WOW6432Node\CLSID\{6E1C0429-44EE-4780-96D3-4DFD3A218D25}" /f /ve /d "CoreCLR COMHost Server"
    reg add "HKEY_CURRENT_USER\SOFTWARE\Classes\WOW6432Node\CLSID\{6E1C0429-44EE-4780-96D3-4DFD3A218D25}\InProcServer32" /f /ve /d "%COMHOSTPATH%"
    reg add "HKEY_CURRENT_USER\SOFTWARE\Classes\WOW6432Node\CLSID\{6E1C0429-44EE-4780-96D3-4DFD3A218D25}\InProcServer32" /f /v ThreadingModel /t REG_SZ /d "Both"
    reg add "HKEY_CURRENT_USER\SOFTWARE\Classes\WOW6432Node\CLSID\{6E1C0429-44EE-4780-96D3-4DFD3A218D25}\ProgID" /f /ve /d "Eurolook.OfficeAddIn.TaskPaneContentControl.10"
) else (
    reg add "HKEY_CURRENT_USER\SOFTWARE\Classes\CLSID\{6D6712AD-D134-4141-BE61-FF4332D2ACB4}" /f /ve /d "CoreCLR COMHost Server"
    reg add "HKEY_CURRENT_USER\SOFTWARE\Classes\CLSID\{6D6712AD-D134-4141-BE61-FF4332D2ACB4}\InProcServer32" /f /ve /d "%COMHOSTPATH%"
    reg add "HKEY_CURRENT_USER\SOFTWARE\Classes\CLSID\{6D6712AD-D134-4141-BE61-FF4332D2ACB4}\InProcServer32" /f /v ThreadingModel /t REG_SZ /d "Both"
    reg add "HKEY_CURRENT_USER\SOFTWARE\Classes\CLSID\{6D6712AD-D134-4141-BE61-FF4332D2ACB4}\ProgID" /f /ve /d "Eurolook.OfficeAddIn.10"

    reg add "HKEY_CURRENT_USER\SOFTWARE\Classes\CLSID\{6E1C0429-44EE-4780-96D3-4DFD3A218D25}" /f /ve /d "CoreCLR COMHost Server"
    reg add "HKEY_CURRENT_USER\SOFTWARE\Classes\CLSID\{6E1C0429-44EE-4780-96D3-4DFD3A218D25}\InProcServer32" /f /ve /d "%COMHOSTPATH%"
    reg add "HKEY_CURRENT_USER\SOFTWARE\Classes\CLSID\{6E1C0429-44EE-4780-96D3-4DFD3A218D25}\InProcServer32" /f /v ThreadingModel /t REG_SZ /d "Both"
    reg add "HKEY_CURRENT_USER\SOFTWARE\Classes\CLSID\{6E1C0429-44EE-4780-96D3-4DFD3A218D25}\ProgID" /f /ve /d "Eurolook.OfficeAddIn.TaskPaneContentControl.10"
)

@REM Excel
reg ADD "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Excel\Addins\Eurolook.OfficeAddIn.10" /f /v Description /t REG_SZ /d "Eurolook Add-in"
reg ADD "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Excel\Addins\Eurolook.OfficeAddIn.10" /f /v FriendlyName /t REG_SZ /d "Eurolook 10 (Visual Studio, netcore)"
reg ADD "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Excel\Addins\Eurolook.OfficeAddIn.10" /f /v LoadBehavior /t REG_DWORD /d 3

@REM PowerPoint
reg ADD "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\PowerPoint\Addins\Eurolook.OfficeAddIn.10" /f /v Description /t REG_SZ /d "Eurolook Add-in"
reg ADD "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\PowerPoint\Addins\Eurolook.OfficeAddIn.10" /f /v FriendlyName /t REG_SZ /d "Eurolook 10 (Visual Studio, netcore)"
reg ADD "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\PowerPoint\Addins\Eurolook.OfficeAddIn.10" /f /v LoadBehavior /t REG_DWORD /d 3

@REM Word
reg ADD "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Word\Addins\Eurolook.OfficeAddIn.10" /f /v Description /t REG_SZ /d "Eurolook Add-in"
reg ADD "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Word\Addins\Eurolook.OfficeAddIn.10" /f /v FriendlyName /t REG_SZ /d "Eurolook 10 (Visual Studio, netcore)"
reg ADD "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Word\Addins\Eurolook.OfficeAddIn.10" /f /v LoadBehavior /t REG_DWORD /d 3

IF NOT %ERRORLEVEL% == 0 (goto error)

echo Successfully registered Eurolook for current user
goto :eof

:error
exit /b -1
pause

