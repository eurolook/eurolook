﻿using System.Runtime.InteropServices;
using Eurolook.AddIn.Common.AddIn;
using Eurolook.WordAddIn.TaskPane;

namespace Eurolook.OfficeAddIn
{
    /// <summary>
    /// A Windows Forms User control to host a WPF task pane.
    /// </summary>
    [ComVisible(true)]
    [ProgId(ComIdentifiers.TaskPaneContentControl.ProgId)]
    [Guid(ComIdentifiers.TaskPaneContentControl.ClsId)]
    [ComDefaultInterface(typeof(ITaskPaneContentControl))]
    public partial class TaskPaneContentControl : UserControl, ITaskPaneContentControl
    {
        public TaskPaneContentControl()
        {
            InitializeComponent();
        }

        public System.Windows.Controls.UserControl? UserControl
        {
            get { return wpfHost.Child as System.Windows.Controls.UserControl; }
            set { wpfHost.Child = value; }
        }
    }
}
