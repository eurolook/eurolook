﻿using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.InteropServices;

using System.Windows;
using System.Windows.Interop;
using System.Windows.Threading;
using Autofac;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.AddIn;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.PerformanceLog;
using Eurolook.AddIn.Common.ProfileInitializer;
using Eurolook.AddIn.Common.Ribbon;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data;
using Eurolook.Data.ExceptionLog;
using Eurolook.ExcelAddIn;
using Eurolook.PowerPointAddIn;
using Eurolook.WordAddIn;
using Eurolook.WordAddIn.EurolookLink;
using Extensibility;
using JetBrains.Annotations;
using Microsoft.Office.Core;
using stdole;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;

#if NET6_0_OR_GREATER
using System.Runtime.Loader;
#endif

#pragma warning disable S1075 // do not use hardcoded URLs

namespace Eurolook.OfficeAddIn;

[ComVisible(true)]
[ProgId(ComIdentifiers.ManagedAddIn.ProgId)]
[Guid(ComIdentifiers.ManagedAddIn.ClsId)]
public sealed class ThisAddIn : AddInBase, ICanLog, IRibbonCallbackHandler, IRibbonExtensibility, IDisposable, IDTExtensibility2
{
    private Task _initTask;

    private ComAddInAutomationService _comAddInAutomationService;
    private IContainer? _container;

    private IAddinContext? _addinContext;

    static ThisAddIn()
    {
        LogManager.LogFileName = GetLogFileName();
        LogManager.AssemblyVersion = typeof(ThisAddIn).Assembly.GetName().Version;
    }

    public ThisAddIn()
    {
        try
        {
            var userConfig = CreateTemporaryUserConfigurationService();
            var userIdentity = CreateTemporaryUserIdentity();

            WireUnhandledExceptionEvents();

            LogStartupMessage(userIdentity.GetUserName());

            ConfigurationInfo.Initialize(AppSettings.Instance.ProductCustomizationId);

            var programVersion = typeof(ThisAddIn).Assembly.GetName().Version;
            if (!Version.TryParse(userConfig.ProgramVersion, out var lastVersion) || lastVersion < programVersion)
            {
                userConfig.ProgramVersion = programVersion.ToString();
                userConfig.ColorScheme = "";
                OnFirstRun();
                userConfig.Save();
            }

#if NET6_0_OR_GREATER
            var resolver = new AssemblyDependencyResolver(typeof(ThisAddIn).Assembly.Location);

            AssemblyLoadContext.Default.Resolving += (context, name) =>
                                                     {
                                                         string assemblyPath = resolver.ResolveAssemblyToPath(name);
                                                         return assemblyPath != null
                                                             ? context.LoadFromAssemblyPath(assemblyPath)
                                                             : null;
                                                     };
#endif
        }
        catch (Exception e)
        {
            this.LogError(e);
        }

        _initTask = Task.CompletedTask;
    }

    ~ThisAddIn()
    {
        Dispose(false);
    }

    private IAddinContext? AddinContext => _addinContext ??= _container?.Resolve<IAddinContext>();

    private DataSyncManager? DataSyncManager { get; set; }

    private IOfficeAddIn? AddIn { get; set; }

    #region Addin Events

    public override void OnStartup(object application)
    {
        try
        {
            this.LogDebug("OnStartup started.");
            AsyncHelper.EnsureSynchronizationContext();

            AddIn = application switch
            {
                Microsoft.Office.Interop.Word.Application wordApplication => new ThisWordAddIn(wordApplication),
                Microsoft.Office.Interop.Excel.Application excelApplication => new ThisExcelAddIn(excelApplication),
                Microsoft.Office.Interop.PowerPoint.Application powerPointApplication => new ThisPowerPointAddIn(
                    powerPointApplication),
                _ => null,
            };

            if (AddIn == null)
            {
                MessageBox.Show("This application does not support Eurolook.");
                return;
            }

            // Setup WPF application.
            if (Application.Current == null)
            {
                var app = new App
                {
                    ShutdownMode = ShutdownMode.OnExplicitShutdown,
                };

                app.DispatcherUnhandledException += ApplicationOnDispatcherUnhandledException;
                app.MainWindow = CreateMainWindow();

                app.Resources.MergedDictionaries.Add(
                    new ResourceDictionary
                    {
                        Source = new Uri(
                            "pack://application:,,,/Eurolook.AddIn.Common;component/Views/DefaultColorScheme.xaml",
                            UriKind.Absolute),
                    });
                app.Resources.MergedDictionaries.Add(
                    new ResourceDictionary
                    {
                        Source = new Uri(
                            "pack://application:,,,/Eurolook.AddIn.Common;component/Views/Resources.xaml",
                            UriKind.Absolute),
                    });

                app.Resources.MergedDictionaries.Add(
                    new ResourceDictionary
                    {
                        Source = new Uri(
                            "pack://application:,,,/Eurolook.AddIn.Common;component/Views/ResourcesCalendarControl.xaml",
                            UriKind.Absolute),
                    });

                this.LogDebug("WPF application created.");
            }

            _container = AddIn.SetupContainer();

            var colorManager = _container.Resolve<ColorManager>();
            colorManager.ApplyColorSchemeFromLocalSettings();
            Execute.InitializeWithDispatcher();

            AddIn.OnStartup(application);

            // Note: Profile initialization is started on main thread.
            // That way the init task will continue on the main thread, no matter where it is awaited (because
            // `ConfigureAwait(true)? , which is the default tries to continue on the captured context).
            var profileInitializer = _container.Resolve<IProfileInitializer>();
            profileInitializer.InitializeProfileIfNecessary().FireAndForgetSafeAsync(this.LogError);

            // Do database access and DataSync setup in a task to reduce Word startup time.
            // The first db access and takes a while.
            _initTask = Task.Factory.StartNew(
                () => OnStartupLongRunningAsync(profileInitializer),
                TaskCreationOptions.LongRunning);

            this.LogDebug("OnStartup done.");
        }
        catch (Exception e)
        {
            this.LogError(e);
        }
    }

    public override async Task OnStartupLongRunningAsync(IProfileInitializer profileInitializer)
    {
        try
        {
            this.LogDebug("Asynchronous initialization task started.");

            if (_container == null)
            {
                this.LogFatal("DI Container is not initialized.");
                return;
            }

            bool isInitialized = await profileInitializer.InitializeProfileIfNecessary();

            DataSyncManager = _container.Resolve<DataSyncManager>();
            if (isInitialized && !_container.Resolve<ISettingsService>().IsStandaloneMode)
            {
                RegisterExceptionLogger();

                var deviceManager = _container.Resolve<DeviceManager>();
                deviceManager.UpdateDeviceProperties();

                var performanceLogManager = _container.Resolve<IPerformanceLogService>();
                await performanceLogManager.LogAddinStartedAsync(AddinContext);

                DataSyncManager?.StartDataSyncScheduler(false);
            }

            // start EurolookLink server
            var linkServer = _container.ResolveOptional<ILinkServer>();
            linkServer?.StartServer();

            await (AddIn?.OnStartupLongRunningAsync(profileInitializer) ?? Task.CompletedTask);

            await base.OnStartupLongRunningAsync(profileInitializer);

            this.LogDebug("Asynchronous initialization task done.");
        }
        catch (Exception ex)
        {
            this.LogError("Error on startup", ex);
        }
    }

    public override void OnShutdown()
    {
        Dispose();
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    [SuppressMessage(
        "ReSharper",
        "UnusedParameter.Local",
        Justification = "This method signature is according to the IDisposable pattern.")]
    private void Dispose(bool disposing)
    {
        this.LogDebug("Shutting down Eurolook");

        AddIn?.OnShutdown();

        DataSyncManager?.ShutdownDataSyncScheduler();

        // dispose AutoFac container and all IDisposable components resolved from the container
        _container?.Dispose();

        Application.Current.Shutdown();

        // FIX: This call fixes an issue that occurred on Windows 8
        // http://social.msdn.microsoft.com/Forums/office/en-US/bb990ddb-ecde-4161-8915-e66e913e3a3b/invalidoperationexception-localdatastoreslot-storage-has-been-freed?forum=exceldev
        Dispatcher.CurrentDispatcher.InvokeShutdown();

        this.LogDebug("Shutting down Eurolook completed");
    }

    private void OnFirstRun()
    {
        RemovingLogFilesFromOldLocation();
    }

    private void RemovingLogFilesFromOldLocation()
    {
        try
        {
            string oldLogPath = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                "Dialogika",
                "Eurolook");
            foreach (string oldLogFile in Directory.EnumerateFiles(oldLogPath, "Eurolook.WordAddin.?.zip")
                                                   .Concat(
                                                       Directory.EnumerateFiles(oldLogPath, "Eurolook.WordAddin.log"))
                                                   .Concat(
                                                       Directory.EnumerateFiles(oldLogPath, "Eurolook.DataSync.?.zip"))
                                                   .Concat(
                                                       Directory.EnumerateFiles(oldLogPath, "Eurolook.DataSync.log")))
            {
                this.LogDebug($"Removing {oldLogFile}");
                try
                {
                    File.Delete(oldLogFile);
                }
                catch (Exception ex)
                {
                    this.LogError(ex);
                }
            }
        }
        catch (Exception ex)
        {
            this.LogError(ex);
        }
    }

    public override void OnTaskPaneFactoryAvailable(ICTPFactory ctpFactoryInst)
    {
        AsyncHelper.EnsureSynchronizationContext();

        // IMPORTANT: This method is in a call chain initiated from
        //   the ICustomTaskPaneConsumer.CTPFactoryAvailable(Office.ICTPFactory ctpFactoryInst) callback.
        //   The callback happens while Word is displaying its splash screen with a "Processing..." message.
        //   No long-running task or other heavy operations such a database access should be done here.
        AddIn?.OnTaskPaneFactoryAvailable(ctpFactoryInst);
    }

    public override object? RequestComAddInAutomationService()
    {
        return AddIn?.RequestComAddInAutomationService();
    }

    #endregion

    private static Window CreateMainWindow()
    {
        var window = new Window
        {
            Width = 0,
            Height = 0,
            Top = -10,
            Left = -10,
            ShowInTaskbar = false,
            Title = "Eurolook",
        };

        var interopHelper = new WindowInteropHelper(window);
        interopHelper.EnsureHandle();

        return window;
    }

    private static string GetLogFileName()
    {
        var process = Process.GetCurrentProcess();
        return process.ProcessName switch
        {
            "winword.exe" => "Eurolook.WordAddIn",
            "excel.exe" => "Eurolook.ExcelAddIn",
            "powerpnt.exe" => "Eurolook.PowerPointAddIn",
            _ => "Eurolook.OfficeAddIn",
        };
    }

    private static void LogStartupMessage(string userName)
    {
        var logger = LogManager.GetLogger();
        var assembly = typeof(ThisAddIn).Assembly;
        logger.LogStartupMessage(assembly, userName);
    }

    private void RegisterExceptionLogger()
    {
        if (_container == null || AddinContext == null)
        {
            return;
        }

        var userDataRepository = _container.Resolve<IUserDataRepository>();
        var exceptionLogInfoProvider = new ExceptionLogInfoProvider
        {
            User = userDataRepository.GetUserForLogging(),
            ClientVersion = AddinContext.ClientVersion,
            OfficeVersion = AddinContext.OfficeVersion,
        };
        NLogExceptionLogger.Register(exceptionLogInfoProvider);
    }

    #region Unhandled Exception Handling

    private void WireUnhandledExceptionEvents()
    {
        AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
#if ENABLE_FIRST_CHANCE_EXCEPTION_LOGGING
            AppDomain.CurrentDomain.FirstChanceException += CurrentDomainOnFirstChanceException;
#endif
        TaskScheduler.UnobservedTaskException += TaskSchedulerOnUnobservedTaskException;
        Dispatcher.CurrentDispatcher.UnhandledException += CurrentDispatcherOnUnhandledException;
    }

    private void ApplicationOnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
    {
        this.LogError("Unhandled exception", e.Exception);

        Execute.OnUiThread(
            () =>
            {
                var messageService = _container?.Resolve<IMessageService>();
                messageService?.ShowDebugError(e.Exception, "Unhandled Exception");
            });

        e.Handled = true;
    }

    private void CurrentDomainOnUnhandledException(
        object sender,
        UnhandledExceptionEventArgs unhandledExceptionEventArgs)
    {
        var exception = unhandledExceptionEventArgs.ExceptionObject as Exception;
        if (exception != null)
        {
            this.LogFatal($"CurrentDomainOnUnhandledException: {exception.Message}", exception);
        }
        else
        {
            this.LogFatalFormat("CurrentDomainOnUnhandledException: {0}", unhandledExceptionEventArgs.ExceptionObject);
        }
    }

    private void CurrentDispatcherOnUnhandledException(
        object sender,
        DispatcherUnhandledExceptionEventArgs dispatcherUnhandledExceptionEventArgs)
    {
        this.LogFatal(
            $"CurrentDispatcherOnUnhandledException: {dispatcherUnhandledExceptionEventArgs.Exception.Message}",
            dispatcherUnhandledExceptionEventArgs.Exception);
    }

#if ENABLE_FIRST_CHANCE_EXCEPTION_LOGGING
        private static readonly object StaticLock = new object();
        private bool _tryToLogFirstChanceException = true;
    
        private void CurrentDomainOnFirstChanceException(object sender, System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs firstChanceExceptionEventArgs)
        {
            try
            {
                Trace.WriteLine("Eurolook::CurrentDomainOnFirstChanceException::" + firstChanceExceptionEventArgs.Exception);

                if (_tryToLogFirstChanceException)
                {
                    lock (StaticLock)
                    {
                        _tryToLogFirstChanceException = false;
                        this.LogWarn("CurrentDomainOnFirstChanceException {0}", firstChanceExceptionEventArgs.Exception);
                        var stackTrace = new StackTrace();
                        this.LogDebug(stackTrace);
                        _tryToLogFirstChanceException = true;
                    }
                }
            }
            catch
            {
                // ignore any further errors
            }
        }
#endif

    private void TaskSchedulerOnUnobservedTaskException(
        object sender,
        UnobservedTaskExceptionEventArgs unobservedTaskExceptionEventArgs)
    {
        this.LogError("TaskSchedulerOnUnobservedTaskException", unobservedTaskExceptionEventArgs.Exception);
        this.LogDebugFormat("current stack:{0}{1}", Environment.NewLine, new StackTrace());
    }

    #endregion

    #region IRibbonExtensibility and Ribbon Callbacks

    // NOTE: all Ribbon callbacks must be in this class. They don't get called by Office if moved to the base class.
    public string GetCustomUI(string ribbonId)
    {
        try
        {
            AsyncHelper.EnsureSynchronizationContext();
            return AddIn?.RibbonCallbackHandler?.GetCustomUI(ribbonId);
        }
        catch (Exception ex)
        {
            this.LogError(ex);
        }

        return null;
    }

    public object? LoadImage(string imageId)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.LoadImage(imageId);
    }

    [UsedImplicitly]
    public void OnLoad(IRibbonUI ribbonUI)
    {
        AsyncHelper.EnsureSynchronizationContext();
        AddIn?.RibbonService.OnLoad(ribbonUI);
    }

    public string? GetDescription(IRibbonControl control)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetDescription(control);
    }

    public bool GetEnabled(IRibbonControl control)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetEnabled(control) ?? default;
    }

    /// <summary>
    /// Get an image to be shown on the Ribbon control.
    /// </summary>
    /// <param name="control">An <see cref="IRibbonControl" /> instance.</param>
    /// <returns>Returns an <see cref="stdole.IPictureDisp" /> object.</returns>
    [UsedImplicitly]
    public object? GetImage(IRibbonControl control)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetImage(control);
    }

    public string? GetImageMso(IRibbonControl control)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetImageMso(control);
    }

    public string? GetLabel(IRibbonControl control)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetLabel(control);
    }

    public string? GetKeytip(IRibbonControl control)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetKeytip(control);
    }

    public RibbonControlSize GetSize(IRibbonControl control)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetSize(control) ?? default;
    }

    public string? GetScreentip(IRibbonControl control)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetScreentip(control);
    }

    public string? GetSupertip(IRibbonControl control)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetSupertip(control);
    }

    public bool GetVisible(IRibbonControl control)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetVisible(control) ?? default;
    }

    public bool GetShowImage(IRibbonControl control)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetShowImage(control) ?? default;
    }

    public bool GetShowLabel(IRibbonControl control)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetShowLabel(control) ?? default;
    }

    public void OnCanceableAction(IRibbonControl control, ref bool cancelDefault)
    {
        AsyncHelper.EnsureSynchronizationContext();
        AddIn?.RibbonCallbackHandler?.OnCanceableAction(control, ref cancelDefault);
    }

    public async void OnAction(IRibbonControl control)
    {
        AsyncHelper.EnsureSynchronizationContext();
        await _initTask;
        AddIn?.RibbonCallbackHandler?.OnAction(control);
    }

    public bool GetPressed(IRibbonControl control)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetPressed(control) ?? default;
    }

    public async void OnToggleAction(IRibbonControl control, bool pressed)
    {
        AsyncHelper.EnsureSynchronizationContext();
        await _initTask;
        AddIn?.RibbonCallbackHandler?.OnToggleAction(control, pressed);
    }

    public void OnCanceableToggleAction(IRibbonControl control, bool pressed, ref bool cancelDefault)
    {
        AsyncHelper.EnsureSynchronizationContext();
        AddIn?.RibbonCallbackHandler?.OnCanceableToggleAction(control, pressed, ref cancelDefault);
    }

    public int GetItemCount(IRibbonControl control)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetItemCount(control) ?? default;
    }

    public string? GetItemID(IRibbonControl control, int index)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetItemID(control, index);
    }

    public IPictureDisp? GetItemImage(IRibbonControl control, int index)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetItemImage(control, index);
    }

    public string? GetItemLabel(IRibbonControl control, int index)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetItemLabel(control, index);
    }

    public string? GetItemScreenTip(IRibbonControl control, int index)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetItemScreenTip(control, index);
    }

    public string? GetItemSuperTip(IRibbonControl control, int index)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetItemSuperTip(control, index);
    }

    public string? GetText(IRibbonControl control)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetText(control);
    }

    public void OnChange(IRibbonControl control, string text)
    {
        AsyncHelper.EnsureSynchronizationContext();
        AddIn?.RibbonCallbackHandler?.OnChange(control, text);
    }

    public string? GetSelectedItemID(IRibbonControl control)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetSelectedItemID(control);
    }

    public int GetSelectedItemIndex(IRibbonControl control)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetSelectedItemIndex(control) ?? default;
    }

    public async void OnItemAction(IRibbonControl control, string selectedId, int selectedIndex)
    {
        AsyncHelper.EnsureSynchronizationContext();
        await _initTask;
        AddIn?.RibbonCallbackHandler?.OnItemAction(control, selectedId, selectedIndex);
    }

    public int GetItemHeight(IRibbonControl control)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetItemHeight(control) ?? default;
    }

    public int GetItemWidth(IRibbonControl control)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetItemWidth(control) ?? default;
    }

    public string? GetContent(IRibbonControl control)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetContent(control);
    }

    public string? GetTitle(IRibbonControl control)
    {
        AsyncHelper.EnsureSynchronizationContext();
        return AddIn?.RibbonCallbackHandler?.GetTitle(control);
    }

    #endregion
}
