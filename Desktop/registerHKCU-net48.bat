@echo on
setlocal
cd /d "%~dp0"

:cleanup
call unregisterHKCU.bat

:register_addin

if "%1" EQU "" (set BUILDCONFIGURATION=Debug) else (set BUILDCONFIGURATION=%1)
if "%2" EQU "" (set PLATFORM=x86) else (set PLATFORM=%2)

set REGASMEXE=RegAsm.x86.exe
set ASMNAME=Eurolook.OfficeAddIn.dll
set ASMPATH=OfficeAddIn\bin\%PLATFORM%\%BUILDCONFIGURATION%\net48\%ASMNAME%
set TEMP_REG_FILE=%TMP%\%ASMNAME%.reg
set REGEXE=%SystemRoot%\SysWOW64\reg.exe

if (%PLATFORM%==x64) (
    echo Registering configuration %1 of Eurolook for 64-bit Word for current user
    set REGASMEXE=RegAsm.x64.exe
    set REGEXE=%SystemRoot%\System32\reg.exe
) else (
    echo Registering configuration %1 of Eurolook for 32-bit Word for current user
)

@REM Copy app.config so that all binding redirects defined in the config are considered, see https://stackoverflow.com/a/20135641/40347
copy "%ASMPATH%.config" "%REGASMEXE%.config"

@REM Create a temporary registry file with the COM registration entries
%REGASMEXE% /regfile:"%TEMP_REG_FILE%" "%ASMPATH%"

@REM Replace all machine keys by user keys
powershell -ExecutionPolicy Bypass -Command "(gc '%TEMP_REG_FILE%') -replace 'HKEY_CLASSES_ROOT', 'HKEY_CURRENT_USER\Software\Classes' | Out-File '%TEMP_REG_FILE%'"

@REM Import the registry file
%REGEXE% import "%TEMP_REG_FILE%"

@REM Delete temporary files
del "%TEMP_REG_FILE%"
del "%REGASMEXE%.config"

IF NOT %ERRORLEVEL% == 0 (goto error)

echo ..\..\BuildTools\RegSvrEx\Tool\%PLATFORM%\RegSvrEx.exe /c "OfficeAddIn\bin\%PLATFORM%\%BUILDCONFIGURATION%\net48\Eurolook.AddInShim.dll"
..\..\BuildTools\RegSvrEx\Tool\%PLATFORM%\RegSvrEx.exe /c "OfficeAddIn\bin\%PLATFORM%\%BUILDCONFIGURATION%\net48\Eurolook.AddInShim.dll"

@REM Excel
reg ADD "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Excel\Addins\Eurolook.OfficeAddIn.10" /f /v Description /t REG_SZ /d "Eurolook Add-in"
reg ADD "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Excel\Addins\Eurolook.OfficeAddIn.10" /f /v FriendlyName /t REG_SZ /d "Eurolook 10 (Visual Studio, net48)"
reg ADD "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Excel\Addins\Eurolook.OfficeAddIn.10" /f /v LoadBehavior /t REG_DWORD /d 3

@REM PowerPoint
reg ADD "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\PowerPoint\Addins\Eurolook.OfficeAddIn.10" /f /v Description /t REG_SZ /d "Eurolook Add-in"
reg ADD "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\PowerPoint\Addins\Eurolook.OfficeAddIn.10" /f /v FriendlyName /t REG_SZ /d "Eurolook 10 (Visual Studio, net48)"
reg ADD "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\PowerPoint\Addins\Eurolook.OfficeAddIn.10" /f /v LoadBehavior /t REG_DWORD /d 3

@REM Word
reg ADD "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Word\Addins\Eurolook.OfficeAddIn.10" /f /v Description /t REG_SZ /d "Eurolook Add-in"
reg ADD "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Word\Addins\Eurolook.OfficeAddIn.10" /f /v FriendlyName /t REG_SZ /d "Eurolook 10 (Visual Studio, net48)"
reg ADD "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Word\Addins\Eurolook.OfficeAddIn.10" /f /v LoadBehavior /t REG_DWORD /d 3

IF NOT %ERRORLEVEL% == 0 (goto error)

echo Successfully registered Eurolook for current user
goto :eof

:error
pause

