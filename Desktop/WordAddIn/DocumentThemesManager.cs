using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using Eurolook.Common.Log;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn
{
    public interface IDocumentThemesManager
    {
        void InstallOfficeThemes();
    }

    public sealed class DocumentThemesManager : IDisposable, ICanLog, IDocumentThemesManager
    {
        private readonly IWordAddinContext _addinContext;
        private readonly IWordApplicationContext _applicationContext;

        public DocumentThemesManager(IWordAddinContext addinContext, IWordApplicationContext applicationContext)
        {
            _addinContext = addinContext;
            _applicationContext = applicationContext;
        }

        ~DocumentThemesManager()
        {
            Dispose(false);
        }

        public void InstallOfficeThemes()
        {
            try
            {
                var sourcePath = Path.Combine(_addinContext.GlobalTemplateFolder, "Document Themes");
                if (!Directory.Exists(sourcePath))
                {
                    return;
                }

                var templatesPath =
                    _applicationContext.Application.Options.DefaultFilePath[WdDefaultFilePath.wdUserTemplatesPath];

                var documentThemesPath = Path.Combine(templatesPath, "Document Themes");
                CopyDirectory(sourcePath, documentThemesPath, true);
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private static void CopyDirectory(string sourceDirName, string destDirName, bool copySubDirs)
        {
            var dir = new DirectoryInfo(sourceDirName);
            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            var files = dir.GetFiles();
            foreach (var sourceFile in files)
            {
                var targetFile = new FileInfo(Path.Combine(destDirName, sourceFile.Name));

                if (targetFile.Exists)
                {
                    if (sourceFile.LastWriteTime > targetFile.LastWriteTime)
                    {
                        // overwrite if newer
                        sourceFile.CopyTo(targetFile.FullName, true);
                    }
                }
                else
                {
                    sourceFile.CopyTo(targetFile.FullName, false);
                }
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                var dirs = dir.GetDirectories();
                foreach (var subDir in dirs)
                {
                    string targetDirFullPath = Path.Combine(destDirName, subDir.Name);
                    CopyDirectory(subDir.FullName, targetDirFullPath, true);
                }
            }
        }

        [SuppressMessage(
            "ReSharper",
            "UnusedParameter.Local",
            Justification = "This method signature is according to the IDisposable pattern.")]
        private void Dispose(bool disposing)
        {
            try
            {
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }
    }
}
