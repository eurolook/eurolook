﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.AddIn;
using Eurolook.AddIn.Common.Ribbon;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.OfficeNotificationBar;
using Eurolook.WordAddIn.ViewModels;
using Eurolook.WordAddIn.Views;
using Microsoft.Office.Core;
using NotNullAttribute = JetBrains.Annotations.NotNullAttribute;
using Timer = System.Threading.Timer;
using Window = Microsoft.Office.Interop.Word.Window;

namespace Eurolook.WordAddIn.TaskPane
{
    public class TaskPaneHost : IDisposable, ICanLog
    {
        private readonly Dictionary<string, MsoCTPDockPosition> _taskPanePositions =
            new Dictionary<string, MsoCTPDockPosition>
            {
                { "Left", MsoCTPDockPosition.msoCTPDockPositionLeft },
                { "Right", MsoCTPDockPosition.msoCTPDockPositionRight },
                { "Floating", MsoCTPDockPosition.msoCTPDockPositionFloating },
            };

        private readonly IRibbonService _ribbonService;
        private readonly ISettingsService _settingsService;
        private readonly CustomTaskPane _customTaskPane;
        private readonly Timer _resizeTimer;
        private readonly TaskPaneCloseButtonClickNotifier _taskPaneCloseButtonClickNotifier;

        private int _desiredWidth = 320;
        private int _desiredHeight;

        public TaskPaneHost(
            CustomTaskPane customTaskPane,
            TaskPaneView taskPaneView,
            TaskPaneViewModel taskPaneViewModel,
            IRibbonService ribbonService,
            ISettingsService settingsService)
        {
            _customTaskPane = customTaskPane;
            TaskPaneView = taskPaneView;
            TaskPaneViewModel = taskPaneViewModel;
            _ribbonService = ribbonService;
            _settingsService = settingsService;

            ((ITaskPaneContentControl)_customTaskPane.ContentControl).UserControl = taskPaneView;

            var handle = ((UserControl)_customTaskPane.ContentControl).Handle;
            _taskPaneCloseButtonClickNotifier = new TaskPaneCloseButtonClickNotifier(handle);
            _taskPaneCloseButtonClickNotifier.CloseButtonClicked += OnTaskPaneCloseButtonClicked;

            _customTaskPane.VisibleStateChange += TaskPaneVisibleStateChanged;
            _customTaskPane.DockPositionStateChange += OnPositionChanged;
            TaskPaneView.SizeChanged += OnSizeChanged;

            _resizeTimer = new Timer(OnSizeChangedDelayed);
        }

        ~TaskPaneHost()
        {
            Dispose(false);
        }

        public int Width
        {
            get { return _customTaskPane.Width; }
            set
            {
                // remember the width in a separate field because Word might reset the
                // task pane width to 0
                _desiredWidth = value;
                _customTaskPane.Width = value;
            }
        }

        public int Height
        {
            get { return _customTaskPane.Height; }
            set
            {
                // remember the width in a separate field because Word might reset the
                // task pane width to 0
                _desiredHeight = value;
                _customTaskPane.Height = value;
            }
        }

        public bool IsVisible
        {
            get => _customTaskPane.Visible;
            set { _customTaskPane.Visible = value; }
        }

        public Window Window => (Window)_customTaskPane.Window;

        public bool IsAlive
        {
            get
            {
                try
                {
                    // check if the Window object is still valid
                    int tryAccessIndex = Window?.Index ?? 0;

                    // check if the task pane is still valid
                    var tryAccess = _customTaskPane.ContentControl;

                    return tryAccessIndex >= 0 && tryAccess != null;
                }
                catch (COMException)
                {
                    return false;
                }
            }
        }

        public TaskPaneView TaskPaneView { get; }

        [NotNull]
        public TaskPaneViewModel TaskPaneViewModel { get; }

        public void ApplyPositionAndSize()
        {
            _customTaskPane.DockPositionRestrict = MsoCTPDockPositionRestrict.msoCTPDockPositionRestrictNone;
            _customTaskPane.DockPosition = GetDockPosition(_settingsService.TaskPanePosition);
            _customTaskPane.DockPositionRestrict = MsoCTPDockPositionRestrict.msoCTPDockPositionRestrictNoHorizontal;

            if (_customTaskPane.DockPosition == MsoCTPDockPosition.msoCTPDockPositionLeft
                || _customTaskPane.DockPosition == MsoCTPDockPosition.msoCTPDockPositionRight ||
                _customTaskPane.DockPosition == MsoCTPDockPosition.msoCTPDockPositionFloating)
            {
                Width = HiDpiHelper.GetRealPixels(_settingsService.TaskPaneWidth);
            }

            if (_customTaskPane.DockPosition == MsoCTPDockPosition.msoCTPDockPositionFloating)
            {
                Height = HiDpiHelper.GetRealPixels(_settingsService.TaskPaneHeight);
            }
        }

        public void AssertIsDockedCorrectly()
        {
            // For unknown reason sometimes the task pane ends up docked to the right. This method tries to
            // assert that the task pane will be moved to the correct position in such a case.
            // One way to reproduce the issue in Word 2013 is the following:
            // - hide the task pane
            // - open a document in reading view
            // - switch to editing the document
            // - show the task pane
            if (_customTaskPane.DockPosition == GetDockPosition(_settingsService.TaskPanePosition))
            {
                return;
            }

            _customTaskPane.DockPositionRestrict = MsoCTPDockPositionRestrict.msoCTPDockPositionRestrictNone;
            _customTaskPane.DockPosition = GetDockPosition(_settingsService.TaskPanePosition);
            _customTaskPane.DockPositionRestrict = MsoCTPDockPositionRestrict.msoCTPDockPositionRestrictNoHorizontal;

            // In case that the task pane has jumped to another docking position, also the size is no longer correct
            if (_customTaskPane.DockPosition == MsoCTPDockPosition.msoCTPDockPositionLeft
                || _customTaskPane.DockPosition == MsoCTPDockPosition.msoCTPDockPositionRight ||
                _customTaskPane.DockPosition == MsoCTPDockPosition.msoCTPDockPositionFloating)
            {
                _customTaskPane.Width = _desiredWidth;
            }

            if (_customTaskPane.DockPosition == MsoCTPDockPosition.msoCTPDockPositionFloating)
            {
                _customTaskPane.Height = _desiredHeight;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        [SuppressMessage("ReSharper", "UnusedParameter.Local", Justification = "Signature is according to the IDisposable pattern.")]
        protected virtual void Dispose(bool disposing)
        {
            try
            {
                _taskPaneCloseButtonClickNotifier.CloseButtonClicked -= OnTaskPaneCloseButtonClicked;
                _taskPaneCloseButtonClickNotifier.Dispose();

                _customTaskPane.VisibleStateChange -= TaskPaneVisibleStateChanged;
                _customTaskPane.Delete();

                TaskPaneView.SizeChanged -= OnSizeChanged;
                _resizeTimer.Dispose();

                TaskPaneViewModel.Dispose();
            }
            catch (Exception ex)
            {
                this.LogError("Unable to delete task pane", ex);
            }
        }

        private void TaskPaneVisibleStateChanged(CustomTaskPane customTaskPane)
        {
            AsyncHelper.EnsureSynchronizationContext();
            _ribbonService.InvalidateRibbon();
        }

        private void OnPositionChanged(CustomTaskPane customTaskPane)
        {
            try
            {
                AsyncHelper.EnsureSynchronizationContext();
                _settingsService.TaskPanePosition = GetTaskPanePosition(_customTaskPane.DockPosition);
            }
            catch (Exception ex)
            {
                this.LogWarn("Could not update TaskPane position.", ex);
            }
        }

        private void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            AsyncHelper.EnsureSynchronizationContext();

            // When the TaskPane is drawn for the first time the PreviousSize is 0
            if (e.PreviousSize.Width.AlmostEquals(0.0))
            {
                return;
            }

            // we are only interested in width changes
            if (!e.WidthChanged)
            {
                return;
            }

            // This event occurs with every pixel that is changed.
            // Use a timer to ensure that the DB is only updated once.
            // The timer is reset every time the event occurs.
            // The callback is only executed if no event occured for x milliseconds.
            _resizeTimer.Change(500, 0);
        }

        private void OnSizeChangedDelayed(object state)
        {
            try
            {
                _resizeTimer.Change(Timeout.Infinite, Timeout.Infinite);

                // save the new width
                int width = HiDpiHelper.GetEffectivePixels(_customTaskPane.Width);
                _settingsService.TaskPaneWidth = width;
                _desiredWidth = Math.Max(width, 140);

                // save the new height
                int height = HiDpiHelper.GetEffectivePixels(_customTaskPane.Height);
                _settingsService.TaskPaneHeight = height;
                _desiredHeight = Math.Max(height, 100);
            }
            catch (Exception ex)
            {
                this.LogWarn("Could not update TaskPane size.", ex);
            }
        }

        private void OnTaskPaneCloseButtonClicked(object sender, EventArgs e)
        {
            AsyncHelper.EnsureSynchronizationContext();
            _settingsService.IsTaskpaneVisible = false;
        }

        private MsoCTPDockPosition GetDockPosition(string position)
        {
            if (_taskPanePositions != null && _taskPanePositions.ContainsKey(position))
            {
                return _taskPanePositions[position];
            }

            return MsoCTPDockPosition.msoCTPDockPositionLeft;
        }

        private string GetTaskPanePosition(MsoCTPDockPosition position)
        {
            if (_taskPanePositions != null && _taskPanePositions.ContainsValue(position))
            {
                return _taskPanePositions.Where(e => e.Value == position).Select(e => e.Key).First();
            }

            return "Left";
        }
    }
}
