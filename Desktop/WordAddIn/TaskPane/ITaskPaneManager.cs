using System.Collections.Generic;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.TaskPane
{
    public interface ITaskPaneManager
    {
        IEnumerable<TaskPaneHost> TaskPanes { get; }

        void RemoveTaskPane(Document wordDocument);

        TaskPaneHost GetActiveTaskPane();

        [CanBeNull]
        TaskPaneHost GetOrCreateTaskPane(Window window);
    }
}
