﻿namespace Eurolook.WordAddIn.TaskPane
{
    public interface ITaskPaneContentControl
    {
        System.Windows.Controls.UserControl UserControl { get; set; }
    }
}
