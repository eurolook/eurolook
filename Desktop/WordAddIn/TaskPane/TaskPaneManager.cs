﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.InteropServices;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.AddIn;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.OfficeNotificationBar.Extensions;
using Eurolook.WordAddIn.AddIn;
using Eurolook.WordAddIn.Messages;
using Eurolook.WordAddIn.ViewModels;
using Eurolook.WordAddIn.Views;
using GalaSoft.MvvmLight.Messaging;
using JetBrains.Annotations;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using NotNullAttribute = JetBrains.Annotations.NotNullAttribute;

namespace Eurolook.WordAddIn.TaskPane
{
    [UsedImplicitly]
    public sealed class TaskPaneManager : ITaskPaneManager, IDisposable, ICanLog
    {
        private static readonly object StaticLock = new object();
        private static readonly object StaticLockGetOrCreate = new object();
        private static readonly WindowComparer WindowComparer = new WindowComparer();
        private static bool _isInTaskPaneGetOrCreate;

        private readonly IWordApplicationContext _applicationContext;
        private readonly ITaskPaneFactory _taskPaneFactory;
        private readonly Func<TaskPaneViewModel> _taskPaneViewModelCreateFunc;
        private readonly Func<CustomTaskPane, TaskPaneView, TaskPaneViewModel, TaskPaneHost> _taskPaneHostCreateFunc;

        private readonly List<TaskPaneHost> _taskPane = new List<TaskPaneHost>();
        private readonly ISettingsService _settingsService;

        public TaskPaneManager(
            IWordApplicationContext applicationContext,
            ITaskPaneFactory taskPaneFactory,
            ISettingsService settingsService,
            Func<TaskPaneViewModel> taskPaneViewModelCreateFunc,
            Func<CustomTaskPane, TaskPaneView, TaskPaneViewModel, TaskPaneHost> taskPaneHostCreateFunc)
        {
            _applicationContext = applicationContext;
            _taskPaneFactory = taskPaneFactory;
            _settingsService = settingsService;
            _taskPaneViewModelCreateFunc = taskPaneViewModelCreateFunc;
            _taskPaneHostCreateFunc = taskPaneHostCreateFunc;

            _applicationContext.Application.WindowActivate += OnApplicationWindowActivate;

            Messenger.Default.Register<ShowCommandBrickPanelMessage>(this, OnShowCommandBrickPanel);
        }

        ~TaskPaneManager()
        {
            Dispose(false);
        }

        [NotNull]
        public IEnumerable<TaskPaneHost> TaskPanes
        {
            get { return _taskPane; }
        }

        public void RemoveTaskPane(Document wordDocument)
        {
            if (wordDocument == null || wordDocument.Windows.Count == 0)
            {
                return;
            }

            RemoveTaskPane(wordDocument.Windows[1]);
        }

        [CanBeNull]
        public TaskPaneHost GetActiveTaskPane()
        {
            var activeWindow = _applicationContext.Application.GetActiveWindow();
            return GetTaskPane(activeWindow);
        }

        public TaskPaneHost GetOrCreateTaskPane(Window window)
        {
            lock (StaticLockGetOrCreate)
            {
                try
                {
                    _isInTaskPaneGetOrCreate = true;
                    if (window == null || _applicationContext.Application.IsObjectValid[window])
                    {
                        return GetTaskPane(window) ?? CreateCustomTaskPane(window);
                    }

                    return null;
                }
                finally
                {
                    _isInTaskPaneGetOrCreate = false;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void RemoveTaskPane(Window window)
        {
            if (window == null)
            {
                return;
            }

            lock (StaticLock)
            {
                var taskPanesToRemove = _taskPane.Where(
                    ctp =>
                    {
                        try
                        {
                            return ctp.Window == window || ctp.Window == null;
                        }
                        catch (COMException)
                        {
                            return true;
                        }
                    }).ToList();

                foreach (var customTaskPane in taskPanesToRemove)
                {
                    _taskPane.Remove(customTaskPane);
                    customTaskPane.Dispose();
                }
            }
        }

        [CanBeNull]
        private TaskPaneHost GetTaskPane(Window window)
        {
            lock (StaticLock)
            {
                RemoveOrphanedTaskPanes();
                return _taskPane.FirstOrDefault(
                    ctp =>
                    {
                        try
                        {
                            return WindowComparer.Equals(ctp.Window, window);
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    });
            }
        }

        [CanBeNull]
        private TaskPaneHost CreateCustomTaskPane(Window window)
        {
            try
            {
                this.LogDebug("Creating custom task pane started.");

                lock (StaticLock)
                {
                    if (!_taskPaneFactory.IsReady)
                    {
                        this.LogDebug("Cannot create custom task pane. Factory is not ready.");
                        return null;
                    }

                    // NOTE: The original idea was to load the taskpane settings from DB and use user.config as fallback if
                    //   there is no profile in the DB yet or the DB does not yet exist.
                    //   However, this code may trigger a synchronous DB request during Word startup, which takes about 2 seconds.
                    //   Therefore, we only read the task pane settings from the config file.
                    bool visible = _settingsService.IsTaskpaneVisible;
                    if (_applicationContext.Application.IsProtectedView() || _applicationContext.Application.IsReadingView())
                    {
                        // NOTE: Prevent the task pane from being shown in protected view and reading view
                        //  We set the visibility to false to prevent any flickering / jumping of the document window
                        visible = false;
                    }

                    var taskPaneViewModel = _taskPaneViewModelCreateFunc();
                    var taskPaneView = CreateTaskPaneView(window, taskPaneViewModel);
                    var customTaskPane = _taskPaneFactory.CreateCustomTaskPane(window);
                    var taskPaneHost = _taskPaneHostCreateFunc(customTaskPane, taskPaneView, taskPaneViewModel);
                    taskPaneHost.ApplyPositionAndSize();
                    taskPaneHost.IsVisible = visible;

                    _taskPane.Add(taskPaneHost);

                    this.LogDebug("Creating custom task pane done.");
                    return taskPaneHost;
                }
            }
            catch (COMException ex) when (ex.HasObjectBeenDeletedException())
            {
                this.LogDebug("Cannot create custom task pane. Object has been deleted.", ex);
            }
            catch (COMException ex)
            {
                this.LogError("Cannot create custom task pane", ex);
            }

            return null;
        }

        private TaskPaneView CreateTaskPaneView(Window window, TaskPaneViewModel taskPaneViewModel)
        {
            // NOTE: We need to call the non-throwing extension method GetDocumentSafe in the LINQ query
            //   to avoid exception caused by invalid Document objects
            //   A COMException (0x800A13E9): 'Word has encountered a problem' might be thrown otherwise,
            //   likely related to the Windows Explorer Preview mode.
            var taskPaneHost = _taskPane.FirstOrDefault(
                tp => tp.Window != null && window != null && tp.Window.GetDocumentSafe() == window.GetDocumentSafe());

            if (taskPaneHost != null)
            {
                // there are two windows for this DocumentViewModel, reuse the existing DocumentViewModel
                taskPaneViewModel.DocumentViewModel = taskPaneHost.TaskPaneViewModel.DocumentViewModel;
            }

            return new TaskPaneView(_applicationContext, taskPaneViewModel);
        }

        private void RemoveOrphanedTaskPanes()
        {
            var orphanedTaskPanes = _taskPane.Where(tp => !tp.IsAlive).ToList();

            foreach (var orphanedTaskPane in orphanedTaskPanes)
            {
                _taskPane.Remove(orphanedTaskPane);
                orphanedTaskPane.Dispose();
            }
        }

        private void OnShowCommandBrickPanel(ShowCommandBrickPanelMessage message)
        {
            if (message?.CommandBrickPanel?.EurolookDocument == null)
            {
                return;
            }

            try
            {
                var document = message.CommandBrickPanel.EurolookDocument.Document;
                var taskPaneHost = GetOrCreateTaskPane(document.ActiveWindow);
                if (taskPaneHost != null)
                {
                    taskPaneHost.IsVisible = true;
                }
            }
            catch (COMException ex)
            {
                this.LogWarn(ex);
            }
        }

        private void OnApplicationWindowActivate(Document document, Window window)
        {
            AsyncHelper.EnsureSynchronizationContext();

            // prevent re-entrance when the task pane is created from the OnTaskPaneFactoryAvailable method.
            if (_isInTaskPaneGetOrCreate)
            {
                return;
            }

            var taskPaneHost = GetOrCreateTaskPane(window);
            if (taskPaneHost == null)
            {
                return;
            }

            if (!window.CanHaveTaskPane())
            {
                taskPaneHost.IsVisible = false;
            }

            taskPaneHost.AssertIsDockedCorrectly();
        }

        [SuppressMessage("ReSharper", "UnusedParameter.Local", Justification = "Signature is according to the IDisposable pattern.")]
        private void Dispose(bool disposing)
        {
            ////_applicationContext.Application.WindowActivate -= OnApplicationWindowActivate;
        }
    }

    public class WindowComparer : IEqualityComparer<Window>
    {
        public bool Equals(Window x, Window y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
            {
                return false;
            }

            return x.Index == y.Index;
        }

        public int GetHashCode(Window obj)
        {
            return obj.GetHashCode();
        }
    }
}
