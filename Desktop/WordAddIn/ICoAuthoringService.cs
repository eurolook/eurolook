using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn
{
    public interface ICoAuthoringService
    {
        bool IsCoAuthoring(Document document, string commandName);
        CoAuthoringInfo GetCoAuthoringInfo(Document document);
    }

    public enum CoAuthoringInfo
    {
        NotShared,
        NotInUse,
        PendingUpdates,
        MultipleAuthors,
        //Locked
    }
}
