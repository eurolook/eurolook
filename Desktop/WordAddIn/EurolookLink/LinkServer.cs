﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using Eurolook.AddIn.Common;
using Eurolook.Common.Log;
using Eurolook.EurolookLink;

namespace Eurolook.WordAddIn.EurolookLink
{
    public class LinkServer : ILinkServer, ICanLog
    {
        private readonly IEnumerable<ILinkHandler> _handlers;
        private readonly string _pipeName;

        public LinkServer(IEnumerable<ILinkHandler> handlers)
        {
            _handlers = handlers;
            _pipeName = EurolookLinkClient.GetPipeName();
        }

        public void StartServer()
        {
            try
            {
                StartServerInternal();
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private void StartServerInternal()
        {
            var pipeServer = new NamedPipeServerStream(
                _pipeName,
                PipeDirection.In,
                NamedPipeServerStream.MaxAllowedServerInstances,
                PipeTransmissionMode.Byte,
                PipeOptions.Asynchronous);
            pipeServer.BeginWaitForConnection(WaitForConnectionCallBack, pipeServer);
        }

        private void WaitForConnectionCallBack(IAsyncResult iar)
        {
            try
            {
                var pipeServer = (NamedPipeServerStream)iar.AsyncState;
                pipeServer.EndWaitForConnection(iar);
                if (pipeServer.CanRead)
                {
                    string data;
                    using (var reader = new StreamReader(pipeServer))
                    {
                        data = reader.ReadToEnd();
                        this.LogDebug($"Received data from pipe {_pipeName}: {data}");
                    }

                    ExecuteHandlers(data);
                }

                pipeServer.Close();
                StartServerInternal();
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private void ExecuteHandlers(string data)
        {
            var args = data.Split('/');
            if (args.Length > 0)
            {
                var commandName = args[0];
                var commandArgs = new string[args.Length - 1];
                Array.Copy(args, 1, commandArgs, 0, commandArgs.Length);
                var handlers = _handlers
                               .Where(
                                   handler => string.Equals(
                                       commandName,
                                       handler.HandlerName,
                                       StringComparison.InvariantCultureIgnoreCase))
                               .ToArray();
                this.LogDebug($"Found {handlers.Length} pipe handlers.");
                foreach (var handler in handlers)
                {
                    Execute.OnUiThread(async () => await handler.Execute(commandArgs));
                    this.LogDebug($"Executed handler {handler.HandlerName}.");
                }
            }
        }
    }
}
