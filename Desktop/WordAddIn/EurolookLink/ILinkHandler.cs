using System.Threading.Tasks;

namespace Eurolook.WordAddIn.EurolookLink
{
    public interface ILinkHandler
    {
        string HandlerName { get; }
        Task Execute(string[] args);
    }
}
