namespace Eurolook.WordAddIn.EurolookLink
{
    public interface ILinkServer
    {
        void StartServer();
    }
}
