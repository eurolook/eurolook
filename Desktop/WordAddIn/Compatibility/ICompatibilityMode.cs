﻿using Eurolook.Data.Models;
using Eurolook.Data.Xml;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.Compatibility
{
    public interface ICompatibilityMode
    {
        [NotNull]
        DocumentModel DocumentModel { get; }

        EurolookPropertiesCustomXml GetEurolookProperties();
    }
}
