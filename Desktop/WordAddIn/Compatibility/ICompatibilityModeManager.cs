﻿using Eurolook.AddIn.Common;

namespace Eurolook.WordAddIn.Compatibility
{
    public interface ICompatibilityModeManager
    {
        ICompatibilityMode CreateCompatibilityMode(IEurolookDocument document);

        bool IsLegacyEurolookDocument(IEurolookDocument document);
    }
}
