﻿using System;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.Common.Log;
using Eurolook.Data.Constants;
using Eurolook.Data.Models;
using Eurolook.Data.Xml;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.Compatibility
{
    public class NonEurolookDocumentCompatibilityMode : ICompatibilityMode, ICanLog
    {
        private readonly IAddinContext _addinContext;

        public NonEurolookDocumentCompatibilityMode(
            IDocumentModelRepository documentModelRepository,
            IAddinContext addinContext)
        {
            _addinContext = addinContext;

            DocumentModel = documentModelRepository.GetDocumentModel(CommonDataConstants.StandardWordDocDocumentModel);
        }

        public DocumentModel DocumentModel { get; }

        [NotNull]
        public EurolookPropertiesCustomXml GetEurolookProperties()
        {
            return new EurolookPropertiesCustomXml
            {
                CreationLanguage = "EN",
                CreationDate = DateTime.Now,
                CreationVersion = _addinContext.ClientVersion.ToString(),
                DocumentModelId = DocumentModel.Id,
                DocumentModelName = DocumentModel.DisplayName,
                DocumentDate = DateTime.Now,
                CompatibilityMode = EurolookCompatibilityMode.Eurolook4X,
            };
        }
    }
}
