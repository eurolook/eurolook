﻿using System;
using Eurolook.AddIn.Common;
using Eurolook.Data.Constants;
using Eurolook.Data.Xml;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.Extensions;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.Compatibility
{
    public abstract class CompatibilityModeManager : ICompatibilityModeManager
    {
        private readonly Func<Document, ICompatibilityMode> _compatibilityModeCreateFunc;

        public CompatibilityModeManager(
            Func<Document, ICompatibilityMode> compatibilityModeCreateFunc)
        {
            _compatibilityModeCreateFunc = compatibilityModeCreateFunc;
        }

        [CanBeNull]
        public virtual ICompatibilityMode CreateCompatibilityMode(IEurolookDocument document)
        {
            return _compatibilityModeCreateFunc(document.GetDocument());
        }

        public virtual bool IsLegacyEurolookDocument(IEurolookDocument document)
        {
            if (document.GetDocument().IsTemplate())
            {
                return false;
            }

            string propertiesCustomXml = document.OfficeDocumentWrapper.GetCustomXml(EurolookPropertiesCustomXml.RootName);
            if (propertiesCustomXml == null)
            {
                return true;
            }

            var eurolookPropertiesCustomXml = new EurolookPropertiesCustomXml(propertiesCustomXml);

            return eurolookPropertiesCustomXml.DocumentModelId == CommonDataConstants.StandardWordDocDocumentModel;
        }
    }
}
