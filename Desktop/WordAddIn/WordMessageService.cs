﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;
using Eurolook.AddIn.Common;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.OfficeNotificationBar;
using Eurolook.WordAddIn.Views;
using Range = Microsoft.Office.Interop.Word.Range;
using Window = Microsoft.Office.Interop.Word.Window;

namespace Eurolook.WordAddIn
{
    public class WordMessageService : MessageService, IWordMessageService
    {
        private readonly IWordApplicationContext _applicationContext;

        private readonly ConcurrentDictionary<Window, NotificationBar> _notificationBars =
            new ConcurrentDictionary<Window, NotificationBar>();

        public WordMessageService(IWordApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public void ShowNotificationBar(INotificationBarContent notificationBarContent, Window window)
        {
            if (!_notificationBars.TryGetValue(window, out var notificationBar))
            {
                notificationBar = new NotificationBar(window);
                notificationBar.Show(notificationBarContent);
                _notificationBars[window] = notificationBar;
            }
            else
            {
                notificationBar.Show(notificationBarContent);
            }
        }

        public void CloseNotificationBar(Window window)
        {
            if (_notificationBars.TryGetValue(window, out var notificationBar))
            {
                notificationBar.Dispose();
            }
        }

        public override void ShowSplashScreen()
        {
            Execute.OnUiThread(
                () =>
                {
                    try
                    {
                        var overlayWindow = new SplashOverlayWindow(_applicationContext.Application);
                        overlayWindow.ShowAndFadeIn().Wait();
                    }
                    catch (Exception ex)
                    {
                        this.LogError("Could not show splash screen.", ex);
                    }
                });
        }

        public override void ShowLightBox(ILightBoxContent lightBoxContent)
        {
            Execute.OnUiThread(
                () =>
                {
                    try
                    {
                        var overlayWindow = new LightBoxWindow(
                            _applicationContext.Application,
                            _applicationContext.Application.GetActiveWindow(),
                            lightBoxContent,
                            WindowExtents.DockLeft);
                        overlayWindow.Show();
                    }
                    catch (Exception ex)
                    {
                        this.LogError("Could not show splash screen.", ex);
                    }
                });
        }

        public bool? ShowDialogNextToRange(System.Windows.Window window, Range range, int distanceFromRange)
        {
            // set owner
            var helper = new WindowInteropHelper(window)
            {
                Owner = _applicationContext.Application.GetActiveWindow().GetWordWindowHandle(),
            };

            // set initial window position to the left of selection
            try
            {
                // Although it might lead to unwanted scrolling the call to ScrollIntoView is required for the subsequent
                // call to range.GetScreenRectFromObject. Otherwise this call might throw a COMException 0x800A1066 Command failed.
                range.Document.ActiveWindow.ScrollIntoView(range);

                double scalingFactor = 1.0 / HiDpiHelper.GetDpiScalingFactor();

                var pos = range.GetScreenRectFromObject();
                var posRect = new Rect(pos.X, pos.Y, pos.Width, pos.Height);
                posRect.Scale(scalingFactor, scalingFactor);

                var wordWindowRect = SafeNativeMethods.GetWindowRect(helper.Owner);
                wordWindowRect.Scale(scalingFactor, scalingFactor);

                double scaledDistanceFromRange = distanceFromRange * scalingFactor;

                double minDistanceFromScreenEdge = 20.0 * scalingFactor;

                double desiredLeft = posRect.Left - window.Width - scaledDistanceFromRange;
                window.Left = Math.Max(minDistanceFromScreenEdge, desiredLeft);

                double desiredTop = posRect.Top - (window.Height / 1.618);
                window.Top = Math.Max(minDistanceFromScreenEdge, Math.Min(desiredTop, wordWindowRect.Bottom - window.Height - minDistanceFromScreenEdge));
            }
            catch (COMException ex)
            {
                this.LogDebug("Unable to set initial window position", ex);
            }

            return ShowDialogInForeground(window);
        }

        [SuppressMessage("ReSharper", "UnusedParameter.Local", Justification = "This method signature is according to the IDisposable pattern.")]
        protected override void Dispose(bool disposing)
        {
            foreach (var notificationBar in _notificationBars.Values)
            {
                notificationBar.Dispose();
            }

            base.Dispose(disposing);
        }

        protected override void SetActiveWordWindowAsOwner(System.Windows.Window window)
        {
            var helper = new WindowInteropHelper(window)
            {
                Owner = _applicationContext.Application.GetActiveWindow().GetWordWindowHandle(),
            };
        }

        protected override IntPtr GeApplicationWindowHandle()
        {
            return _applicationContext.Application.GetActiveWindow().GetWordWindowHandle();
        }
    }
}
