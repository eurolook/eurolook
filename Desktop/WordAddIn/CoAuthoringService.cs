﻿using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.ViewModels;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn
{
    public class CoAuthoringService : ICoAuthoringService
    {
        private readonly IMessageService _messageService;

        public CoAuthoringService(IMessageService messageService)
        {
            _messageService = messageService;
        }

        public CoAuthoringInfo GetCoAuthoringInfo(Document document)
        {
            if (!document.CoAuthoring.CanShare)
            {
                return CoAuthoringInfo.NotShared;
            }

            // It seems that Office 2016 always locks a document for editing,
            // even if it is used by one user only.
            // therefore Locked is useless...
            //if (document.CoAuthoring.Locks.Count > 0)
            //{
            //    return CoAuthoringInfo.Locked;
            //}

            if (document.CoAuthoring.PendingUpdates)
            {
                return CoAuthoringInfo.PendingUpdates;
            }

            if (document.CoAuthoring.Authors.Count > 1)
            {
                return CoAuthoringInfo.MultipleAuthors;
            }

            return CoAuthoringInfo.NotInUse;
        }

        public bool IsCoAuthoring(Document document, string commandName)
        {
            var coAuthoringInfo = GetCoAuthoringInfo(document);
            if (coAuthoringInfo != CoAuthoringInfo.NotShared && coAuthoringInfo != CoAuthoringInfo.NotInUse)
            {
                ShowCoAuthoringError(commandName, coAuthoringInfo);
                return true;
            }

            return false;
        }

        public void ShowCoAuthoringError(string commandName, CoAuthoringInfo info)
        {
            string message = $"{commandName} cannot be executed while someone else is editing the document in SharePoint:";
            if (info == CoAuthoringInfo.MultipleAuthors)
            {
                message += "\n\nOther authors are currently active in the document.";
            }

            if (info == CoAuthoringInfo.PendingUpdates)
            {
                message += "\n\nThe document is awaiting updates from SharePoint.\nIf this problem persist, it could help to save the document.";
            }

            _messageService.ShowMessageWindow(
                new MessageViewModel
                {
                    Title = "SharePoint Co-Authoring",
                    Message = message,
                    IsShowCancelButton = false,
                    IsShowNoButton = false,
                },
                null);
        }
    }
}
