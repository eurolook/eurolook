﻿using Autofac;
using Eurolook.AddIn.Common.Autofac;
using Eurolook.WordAddIn.BrickCommands.EditDocumentMetadata.CalculationCommands;
using Eurolook.WordAddIn.DocumentMetadata;
using Eurolook.WordAddIn.DocumentMetadata.MetadataUpdatedHandlers;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.Autofac
{
    [UsedImplicitly]
    public class MetadataModule : Module, IEurolookCommonModule
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<CommonMetadataModule>();

            builder.RegisterType<DocumentContentExtractor>()
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();

            builder.RegisterType<CalculationCommandRunner>()
                   .AsImplementedInterfaces();

            builder.RegisterType<WordFieldUpdater>()
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();

            builder.RegisterType<WordFilenameFieldUpdater>()
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();

            builder.RegisterType<WordTitleFieldUpdater>()
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();
        }
    }
}
