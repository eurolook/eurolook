﻿using Autofac;
using Eurolook.AddIn.Common.Autofac;
using Eurolook.WordAddIn.Authors;
using Eurolook.WordAddIn.BrickCommands.ApplyStyleViaShortcut;
using Eurolook.WordAddIn.BrickCommands.ChangeLanguage;
using Eurolook.WordAddIn.BrickCommands.ConvertToEurolook;
using Eurolook.WordAddIn.BrickCommands.InsertName;
using Eurolook.WordAddIn.BrickCommands.InsertPicture;
using Eurolook.WordAddIn.BrickCommands.Placeholders.Date;
using Eurolook.WordAddIn.BrickCommands.Placeholders.Linked;
using Eurolook.WordAddIn.BrickCommands.RepairStyles;
using Eurolook.WordAddIn.BrickCommands.ViewOpenXmlPackageParts;
using Eurolook.WordAddIn.Options;
using Eurolook.WordAddIn.ViewModels;
using Eurolook.WordAddIn.ViewModels.Languages;
using Eurolook.WordAddIn.Wizard;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.Autofac
{
    [UsedImplicitly]
    public class ViewModelsModule : Module, IEurolookCommonModule
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TaskPaneViewModel>().ExternallyOwned();
            builder.RegisterType<DataSyncStatusViewModel>().SingleInstance();
            builder.RegisterType<InitializationStatusViewModel>().SingleInstance();
            builder.RegisterType<BricksTabViewModel>().ExternallyOwned();
            builder.RegisterType<ToolsTabViewModel>().ExternallyOwned();
            builder.RegisterType<DocumentViewModel>();
            builder.RegisterType<CreateDocumentViewModel>();
            builder.RegisterType<CreateDocumentWizardViewModel>().SingleInstance();
            builder.RegisterType<ChangeLanguageAuthorDialogViewModel>();
            builder.RegisterType<ChangeAuthorViewModel>();
            builder.RegisterType<UpdateDocumentAuthorsViewModel>();
            builder.RegisterType<AddContactViewModel>();

            // options
            builder.RegisterType<GeneralOptionsViewModel>().AsImplementedInterfaces();
            builder.RegisterType<PersonalisationOptionsViewModel>().AsImplementedInterfaces();
            builder.RegisterType<DocumentTypesOptionsViewModel>().AsImplementedInterfaces();
            builder.RegisterType<AdvancedOptionsViewModel>().AsImplementedInterfaces();
            builder.RegisterType<NotificationOptionsViewModel>().AsImplementedInterfaces();
            builder.RegisterType<OptionsViewModel>();

            // command bricks
            builder.RegisterType<RepairStyleViewModel>();
            builder.RegisterType<ConvertToEurolookSaveOptionViewModel>();
            builder.RegisterType<InsertPictureViewModel>();
            builder.RegisterType<InsertNameViewModel>();
            builder.RegisterType<InsertLinkedPlaceholderViewModel>();
            builder.RegisterType<InsertDatePlaceholderViewModel>();

            builder.RegisterType<AboutWindowViewModel>().SingleInstance();
            builder.RegisterType<ApplyStyleViewModel>();
            builder.RegisterType<LanguageLoader>().AsImplementedInterfaces();
            builder.RegisterType<OpenXmlPackageViewerViewModel>().AsSelf();

            builder.RegisterType<TemplateChooserViewModel>().AsSelf();
            builder.RegisterType<BricksChooserViewModel>().AsSelf();
        }
    }
}
