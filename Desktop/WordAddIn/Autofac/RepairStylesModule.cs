using Autofac;
using Eurolook.AddIn.Common.Autofac;
using Eurolook.WordAddIn.BrickCommands.RepairStyles;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.Autofac
{
    [UsedImplicitly]
    public class RepairStylesModule : Module, IEurolookCommonModule
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<StyleRepairer>().AsImplementedInterfaces();
        }
    }
}
