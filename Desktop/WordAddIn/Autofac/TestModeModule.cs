﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Eurolook.AddIn.Common.Autofac;
using Eurolook.WordAddIn.TaskPaneMessages;

namespace Eurolook.WordAddIn.Autofac
{
    public class TestModeModule : Module, IEurolookCommonModule
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TestModeMessageProvider>()
                   .AsImplementedInterfaces();
        }
    }
}
