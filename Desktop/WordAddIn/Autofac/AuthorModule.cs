using System.Linq;
using Autofac;
using Eurolook.AddIn.Common.Authors;
using Eurolook.AddIn.Common.Autofac;
using Eurolook.DocumentProcessing;
using Eurolook.WordAddIn.Authors;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.Autofac
{
    [UsedImplicitly]
    public class AuthorModule : Module, IEurolookCommonModule
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AuthorManager>().AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(typeof(AuthorModule).Assembly)
                   .Where(t => t.GetInterfaces().Contains(typeof(IAuthorChangeExecutor)) && !t.IsAbstract)
                   .AsImplementedInterfaces();

            builder.RegisterType<AuthorRoleViewModelValidator>().AsImplementedInterfaces();
            builder.RegisterType<AuthorDataLoader>().AsImplementedInterfaces();
            builder.RegisterType<FavoriteService>().AsImplementedInterfaces();
        }
    }
}
