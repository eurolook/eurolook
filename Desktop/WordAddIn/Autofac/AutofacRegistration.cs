﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Windows;
using Autofac;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.AddIn;
using Eurolook.AddIn.Common.Autofac;
using Eurolook.AddIn.Common.ClipboardTools.Formats;
using Eurolook.AddIn.Common.Email;
using Eurolook.AddIn.Common.PerformanceLog;
using Eurolook.AddIn.Common.SystemConfiguration;
using Eurolook.AddIn.Common.SystemResourceTracer;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Common;
using Eurolook.Common.Extensions;
using Eurolook.Data;
using Eurolook.Data.Compatibility;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.OpenXml;
using Eurolook.WordAddIn.AddIn;
using Eurolook.WordAddIn.BrickCommands;
using Eurolook.WordAddIn.BrickCommands.SimplifiedPromoteDemote;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.Events;
using Eurolook.WordAddIn.HotKeys;
using Eurolook.WordAddIn.Ribbon;
using Eurolook.WordAddIn.TaskPane;
using Eurolook.WordAddIn.TaskPaneMessages;

namespace Eurolook.WordAddIn.Autofac
{
    public static class AutofacRegistration
    {
        public static IContainer SetupContainer(ThisWordAddIn thisWordAddIn)
        {
            var builder = new ContainerBuilder();

            var pluginLoader = new PluginLoader(
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                AppSettings.Instance.Plugins.Word);

            LoadWpfResources(pluginLoader.PluginAssemblies);

            var moduleAssemblies = builder.GetEurolookPluginAssemblies()
                                          .Concat(
                                              new[]
                                              {
                                                  typeof(ThisWordAddIn).Assembly,
                                                  typeof(IEurolookCommonModule).Assembly,
                                              }).ToArray();

            builder.SetEurolookPluginAssemblies(pluginLoader.PluginAssemblies);

            // Perform registrations and build the container.
            var applicationContext = new ApplicationContext(thisWordAddIn.Application);

            builder.RegisterInstance(applicationContext).AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<WordAddinContext>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterInstance(() => ConfigurationInfo.Instance).AsImplementedInterfaces();
            builder.RegisterType<SessionContext>().As<ISessionContext>().SingleInstance();

            // register services
            builder.RegisterType<WordRibbonService>().AsImplementedInterfaces().SingleInstance();

            builder.RegisterAssemblyModules<IEurolookCommonModule>(moduleAssemblies);

            builder.RegisterInstance(thisWordAddIn).As<ITaskPaneFactory>().SingleInstance().ExternallyOwned();
            builder.RegisterType<DisabledAddInsManager>().AsImplementedInterfaces();

            builder.RegisterType<SystemConfigurationService>().AsImplementedInterfaces();

            builder.RegisterGeneric(typeof(ClassNameResolver<>)).As(typeof(IClassNameResolver<>));

            builder.RegisterType<DocumentLocationRetriever>().AsImplementedInterfaces();
            builder.RegisterType<DocumentModelHelper>().AsImplementedInterfaces();

            builder.RegisterType<StylesExtractor>().AsImplementedInterfaces();

            builder.RegisterType<WindowSelectionChangeEventHelper>().As<IWindowSelectionChangeEventHelper>()
                   .SingleInstance();

            builder.RegisterType<SettingsService>().As<ISettingsService>().SingleInstance();
            builder.RegisterType<TaskPaneManager>().As<ITaskPaneManager>().SingleInstance();
            builder.RegisterType<WordMessageService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<CoAuthoringService>().As<ICoAuthoringService>().SingleInstance();
            builder.RegisterType<DocumentManager>().As<IDocumentManager>().SingleInstance().AutoActivate();
            builder.RegisterType<DocumentThemesManager>().As<IDocumentThemesManager>().SingleInstance();

            builder.RegisterType<TaskPaneHost>();
            builder.RegisterType<BrickVisibilityChecker>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<NotificationCompatibilityChecker>().AsImplementedInterfaces().SingleInstance();

            builder.RegisterType<DataSyncManager>().SingleInstance();
            builder.RegisterType<DataSyncScheduler>().SingleInstance();

            builder.RegisterType<HotKeyManager>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<DefaultHotKeys>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ColorManager>().SingleInstance();
            builder.RegisterType<DeviceManager>().SingleInstance();

            builder.RegisterType<ClipboardAnalyzer>().AsImplementedInterfaces();

            builder.RegisterType<BrickExecutionContext>().AsImplementedInterfaces();
            builder.RegisterType<BrickVersionCompatibilityTester>().AsImplementedInterfaces();

            builder.RegisterType<DocumentModelVersionCompatibilityTester>().AsImplementedInterfaces();

            builder.RegisterType<PerformanceLogInfoProvider>().AsImplementedInterfaces();
            builder.RegisterType<PerformanceLogService>().AsImplementedInterfaces();

            builder.RegisterType<EmailService>().AsImplementedInterfaces();
            builder.RegisterType<SimplifiedPromoteDemoteService>().AsImplementedInterfaces();
            builder.RegisterType<UpdateTocService>().AsImplementedInterfaces();

            // register default implementations
            builder.RegisterType<DefaultTaskPaneMessageProvider>()
                   .As<ITaskPaneMessageProvider>()
                   .IfNotRegistered(typeof(ITaskPaneMessageProvider))
                   .InstancePerLifetimeScope();

            builder.RegisterType<DefaultSystemResourceTracer>()
                   .As<ISystemResourceTracer>()
                   .IfNotRegistered(typeof(ISystemResourceTracer))
                   .SingleInstance();

            // register command bricks
            var commandBrickAssemblies = builder.GetEurolookPluginAssemblies()
                                                .Concat(
                                                    new[]
                                                    {
                                                        typeof(ThisWordAddIn).Assembly,
                                                        typeof(IBrickCommand).Assembly,
                                                    }).ToArray();
            builder.RegisterAssemblyTypes(commandBrickAssemblies)
                   .Where(t => typeof(IBrickCommand).IsAssignableFrom(t))
                   .Keyed<IBrickCommand>(t => t.Name)
                   .InstancePerDependency()
                   .AsSelf()
                   .AsImplementedInterfaces();

            builder.RegisterType<CommandBrickResolver>().AsImplementedInterfaces().SingleInstance();

            pluginLoader.RegisterPluginModules(builder);

            var container = builder.Build();

            AutofacXamlResolver.Container = container;

            return container;
        }

        private static void LoadWpfResources(IEnumerable<Assembly> pluginAssemblies)
        {
            foreach (var pluginDll in pluginAssemblies)
            {
                if (ResourcesXamlExists(pluginDll))
                {
                    Application.Current.Resources.MergedDictionaries.Add(
                        new ResourceDictionary
                        {
                            Source = new Uri($"pack://application:,,,/{pluginDll.GetName().Name};component/Resources.xaml", UriKind.RelativeOrAbsolute),
                        });
                }
            }
        }

        private static bool ResourcesXamlExists(Assembly assembly)
        {
            var resources = GetResourceNames(assembly);
            return resources.Contains("resources.baml");
        }

        private static IEnumerable<string> GetResourceNames(Assembly assembly)
        {
            string resName = assembly.GetName().Name + ".g.resources";
            using (var stream = assembly.GetManifestResourceStream(resName))
            {
                if (stream == null)
                {
                    return Enumerable.Empty<string>();
                }

                using (var reader = new ResourceReader(stream))
                {
                    return reader.Cast<DictionaryEntry>().Select(entry => (string)entry.Key).ToList();
                }
            }
        }
    }
}
