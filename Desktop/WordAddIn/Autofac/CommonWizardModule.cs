using Autofac;
using Eurolook.AddIn.Common.Autofac;
using Eurolook.WordAddIn.Wizard;

namespace Eurolook.WordAddIn.Autofac
{
    public class CommonWizardModule : Module, IEurolookCommonModule
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Create Document pages
            builder.RegisterType<BasicsPageViewModel>().As<ICreateDocumentWizardPage>();
        }
    }
}
