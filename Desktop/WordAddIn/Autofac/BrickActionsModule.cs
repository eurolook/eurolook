﻿using System.Linq;
using Autofac;
using Eurolook.AddIn.Common.Autofac;
using Eurolook.Common.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.ContentBrickActions;
using Eurolook.WordAddIn.ContentBrickActions.BrickToolbar;
using Eurolook.WordAddIn.ContentBrickActions.BrickToolbar.Buttons;

namespace Eurolook.WordAddIn.Autofac
{
    public class BrickActionsModule : Module, IEurolookCommonModule
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assemblies = builder.GetEurolookPluginAssemblies()
                                    .Append(typeof(BrickActionsModule).Assembly)
                                    .ToArray();
            builder.RegisterAssemblyTypes(assemblies)
                   .Where(
                       t => (t.GetInterfaces().Contains(typeof(IContentBrickActions)) ||
                             t.GetInterfaces().Contains(typeof(IBrickToolbar)))
                            && !t.IsAbstract)
                   .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(assemblies)
                   .Where(t => t.GetInterfaces().Contains(typeof(IBrickToolbarButton)) && !t.IsAbstract)
                   .AsImplementedInterfaces();

            builder.RegisterType<ExecuteActionAfterBrickCreationHandler>()
                   .AsImplementedInterfaces();

            builder.RegisterType<ContentBrickActionsResolver>()
                   .AsImplementedInterfaces();
        }
    }
}
