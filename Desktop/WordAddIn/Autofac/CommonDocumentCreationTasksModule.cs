using Autofac;
using Eurolook.AddIn.Common.Autofac;
using Eurolook.WordAddIn.DocumentCreation;

namespace Eurolook.WordAddIn.Autofac
{
    public class CommonDocumentCreationTasksModule : Module, IEurolookCommonModule
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CloseEmptyWordDocumentNewDocumentCreationTask>().AsImplementedInterfaces();
            builder.RegisterType<UpdateSettingsAfterDocumentCreationTask>().AsImplementedInterfaces();
            builder.RegisterType<AfterNewDocumentCreationTask>().AsImplementedInterfaces();
            builder.RegisterType<AddFavoritesBeforeDocumentCreationTask>().AsImplementedInterfaces();
        }
    }
}
