﻿using Autofac;
using Eurolook.AddIn.Common.Autofac;
using Eurolook.WordAddIn.Startup;
using Eurolook.WordAddIn.TaskPaneMessages;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.Autofac
{
    [UsedImplicitly]
    public class RegionalSettingsCheckModule : Module,
                                               IEurolookCommonModule
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<RegionalSettingsCheck>()
                   .SingleInstance()
                   .AsSelf();
            builder.RegisterType<RegionalSettingsMessageProvider>()
                   .AsImplementedInterfaces();
        }
    }
}
