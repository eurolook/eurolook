﻿using Autofac;
using Eurolook.AddIn.Common.Autofac;
using Eurolook.WordAddIn.TaskPaneMessages;

namespace Eurolook.WordAddIn.Autofac
{
    public class TaskPaneMessagesModule : Module, IEurolookCommonModule
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<NotificationMessageProvider>()
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();
        }
    }
}
