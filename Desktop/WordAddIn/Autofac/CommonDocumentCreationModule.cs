using Autofac;
using Eurolook.AddIn.Common.Autofac;
using Eurolook.DocumentProcessing.DocumentCreation;
using Eurolook.DocumentProcessing.OpenXml;
using Eurolook.WordAddIn.DocumentCreation;

namespace Eurolook.WordAddIn.Autofac
{
    public class CommonDocumentCreationModule : Module, IEurolookCommonModule
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<OpenXmlDocumentFactory>().As<IDocumentFactory>();
            builder.RegisterType<DocumentCreator>().AsImplementedInterfaces();
            builder.RegisterType<DocumentCreationService>().AsImplementedInterfaces();
        }
    }
}
