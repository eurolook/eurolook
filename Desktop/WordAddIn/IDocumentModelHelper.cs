using System;
using System.Collections.Generic;
using Eurolook.Data.Models;

namespace Eurolook.WordAddIn
{
    public interface IDocumentModelHelper
    {
        Brick GetBrick(Guid brickId);

        List<Brick> GetBricksOfGroup(Guid groupId);
    }
}
