namespace Eurolook.WordAddIn.Ribbon
{
    public interface ITrackChangesHandler
    {
        void TrackChangesCutParagraph(IWordApplicationContext context);

        void TrackChangesPasteParagraph(IWordApplicationContext context);
    }
}
