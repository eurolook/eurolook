﻿using System.Collections.Generic;
using Eurolook.Data.Models;

namespace Eurolook.WordAddIn.Ribbon
{
    public interface IRibbonLauncher
    {
        void ExecuteCommandBrick(CommandBrick brick);
        Dictionary<string, List<CommandBrick>> GetLauncherCommands();
    }
}
