﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.ProfileInitializer;
using Eurolook.AddIn.Common.Ribbon;
using Eurolook.Common;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.HotKeys;

namespace Eurolook.WordAddIn.Ribbon
{
    public class WordRibbonService : RibbonServiceBase<IWordApplicationContext, IWordAddinContext>, IWordRibbonService
    {
        private readonly IWordAddinContext _addinContext;
        private readonly IHotKeyManager _hotKeyManager;
        private Task<bool> _loadQuickAccessToolbarTask;
        private Task<bool> _loadGlobalAddInTask;

        public WordRibbonService(
            IWordApplicationContext applicationContext,
            IWordAddinContext addinContext,
            IHotKeyManager hotKeyManager,
            Lazy<IProfileInitializer> profileInitializer,
            Lazy<IUserDataRepository> userDataRepository)
            : base(applicationContext, addinContext, profileInitializer, userDataRepository)
        {
            _addinContext = addinContext;
            _hotKeyManager = hotKeyManager;
        }

        public override Task<bool> TryLoadQuickAccessToolbarAsync()
        {
            if (_loadQuickAccessToolbarTask == null || _loadQuickAccessToolbarTask.IsCompleted)
            {
                _loadQuickAccessToolbarTask = TryLoadQuickAccessToolbarAsyncInternal();
            }

            return _loadQuickAccessToolbarTask;
        }

        public override void UnloadQuickAccessToolbar()
        {
            string templatePath = _addinContext.QuickAccessToolbarTemplatePath;
            UnloadAddIn(templatePath);
        }

        public Task<bool> TryLoadGlobalAddInAsync()
        {
            this.LogDebug($"Task Status: {_loadGlobalAddInTask?.Status}");
            if (_loadGlobalAddInTask == null || _loadGlobalAddInTask.IsCompleted)
            {
                _loadGlobalAddInTask = TryLoadGlobalAddInAsyncInternal();
            }

            return _loadGlobalAddInTask;
        }

        private Task<bool> TryLoadQuickAccessToolbarAsyncInternal()
        {
            return Task.Run(
                async () =>
                {
                    string templatePath = _addinContext.QuickAccessToolbarTemplatePath;
                    if (!File.Exists(templatePath))
                    {
                        return false;
                    }

                    if (!await ProfileInitializer.Value.InitializeProfileIfNecessary())
                    {
                        return false;
                    }

                    if (GetAddIns(templatePath).Any())
                    {
                        // already loaded
                        return true;
                    }

                    var userSettings = await UserDataRepository.Value.GetUserSettingsAsync();
                    if (userSettings?.IsStyleBoxDisabled == true)
                    {
                        return false;
                    }

                    return TryLoadAddIn(templatePath);
                });
        }

        private Task<bool> TryLoadGlobalAddInAsyncInternal()
        {
            return Task.Run(
                async () =>
                {
                    string templatePath = _addinContext.GlobalAddInPath;
                    if (!File.Exists(templatePath))
                    {
                        return false;
                    }

                    if (!await ProfileInitializer.Value.InitializeProfileIfNecessary())
                    {
                        return false;
                    }

                    if (GetAddIns(templatePath).Any())
                    {
                        // already loaded
                        return true;
                    }

                    var result = TryLoadAddIn(templatePath);

                    _hotKeyManager.ActivateGlobalHotKeys(_addinContext, ApplicationContext.Application);
                    return result;
                });
        }

        private bool TryLoadAddIn(string templatePath)
        {
            this.LogInfo($"Trying to load add-in {templatePath}");

            try
            {
                using (new SingleGlobalInstance("EurolookAddInLoad", 8000))
                {
                    // NOTE: loading add-ins fails if the current document is shown in reading view,
                    //   leading to the following exception:
                    //
                    //   System.Runtime.InteropServices.COMException (0x800A11FD): This method or property is
                    //       not available because this command is not available for reading.
                    //
                    //   Using the DocumentAutomationHelper temporarily switches off the Reading View
                    //   and creates a dummy document that is not in protected view
                    using (new DocumentAutomationHelper(
                        ApplicationContext.Application,
                        null,
                        DocumentAutomationOption.CreateDummyDocumentInProtectedOrReadingView
                        | DocumentAutomationOption.PreventScreenUpdating))
                    {
                        this.LogDebug($"Loading add-in {templatePath}.");
                        ApplicationContext.Application.AddIns.Add(templatePath, true);
                    }

                    InvalidateRibbon();

                    this.LogInfo($"Successfully loaded add-in {templatePath}");
                    return true;
                }
            }
            catch (COMException ex) when (ex.IsLockedOrReadonlyException())
            {
                this.LogDebug(ex);
                return false;
            }
            catch (COMException ex)
            {
                this.LogError(ex);
            }

            return false;
        }

        private void UnloadAddIn(string templatePath)
        {
            foreach (var addIn in GetAddIns(templatePath))
            {
                addIn.Delete();
            }
        }

        private IEnumerable<Microsoft.Office.Interop.Word.AddIn> GetAddIns(string templatePath)
        {
            var result = ApplicationContext.Application.AddIns.OfType<Microsoft.Office.Interop.Word.AddIn>()
                                           .Where(
                                               a =>
                                               {
                                                   try
                                                   {
                                                       return templatePath.EndsWith(
                                                           a.Name,
                                                           StringComparison.InvariantCultureIgnoreCase);
                                                   }
                                                   catch (COMException e)
                                                   {
                                                       this.LogDebug(e);
                                                   }

                                                   return false;
                                               });

            return result;
        }
    }
}
