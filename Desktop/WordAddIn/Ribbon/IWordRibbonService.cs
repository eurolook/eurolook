using System.Threading.Tasks;
using Eurolook.AddIn.Common.Ribbon;

namespace Eurolook.WordAddIn.Ribbon
{
    public interface IWordRibbonService : IRibbonService
    {
        Task<bool> TryLoadGlobalAddInAsync();
    }
}
