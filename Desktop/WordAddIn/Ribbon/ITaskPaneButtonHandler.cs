namespace Eurolook.WordAddIn.Ribbon
{
    public interface ITaskPaneButtonHandler
    {
        bool IsTaskPaneButtonEnabled(IWordApplicationContext context);

        bool IsTaskPaneButtonPressed(IWordApplicationContext context);

        void OnEurolookTaskPaneButton(bool pressed);
    }
}
