using System;
using Eurolook.AddIn.Common;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.TaskPane;

namespace Eurolook.WordAddIn.Ribbon
{
    public class TaskPaneButtonHandler : ICanLog, ITaskPaneButtonHandler
    {
        private readonly ITaskPaneManager _taskPaneManager;
        private readonly ISettingsService _settingsService;

        public TaskPaneButtonHandler(ITaskPaneManager taskPaneManager, ISettingsService settingsService)
        {
            _taskPaneManager = taskPaneManager;
            _settingsService = settingsService;
        }

        public void OnEurolookTaskPaneButton(bool pressed)
        {
            try
            {
                // toggle task pane visibility
                var customTaskPane = _taskPaneManager.GetActiveTaskPane();
                if (customTaskPane != null)
                {
                    customTaskPane.AssertIsDockedCorrectly();
                    customTaskPane.IsVisible = pressed;
                    _settingsService.IsTaskpaneVisible = pressed;
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        public bool IsTaskPaneButtonEnabled(IWordApplicationContext context)
        {
            try
            {
                // ReadOnly mode should disable the button,
                // however it cannot do because Word does not revalidate the ribbon when read-only mode is exited.
                return !context.Application.IsProtectedView(); //// && !Application.IsCompareView();
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }

            return false;
        }

        public bool IsTaskPaneButtonPressed(IWordApplicationContext context)
        {
            try
            {
                if (!context.Application.IsProtectedView())
                {
                    var activeTaskPane = _taskPaneManager.GetActiveTaskPane();
                    return activeTaskPane != null && activeTaskPane.IsVisible;
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }

            return false;
        }
    }
}
