﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using Eurolook.AddIn.Common;
using Eurolook.Common;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.DocumentProcessing.OpenXml;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.Ribbon
{
    public class TrackChangesHandler : ITrackChangesHandler, ICanLog
    {
        public static readonly string MovePrefixContentControlTag = "TrackChangesMovePrefix";
        private readonly IDocumentManager _documentManager;
        private readonly IMessageService _messageService;

        private List<ClippedParagraph> _clippedParagraphs;
        private TemporaryFile _temporaryBuildingBlockTemplate;

        public TrackChangesHandler(IDocumentManager documentManager, IMessageService messageService)
        {
            _documentManager = documentManager;
            _messageService = messageService;
        }

        public void TrackChangesCutParagraph(IWordApplicationContext context)
        {
            try
            {
                var document = _documentManager.GetActiveDocumentViewModel().Document;
                var selection = context.Application.GetSelection();
                if (selection == null)
                {
                    return;
                }

                var range = selection.Range.Duplicate;

                _temporaryBuildingBlockTemplate?.Dispose();

                _temporaryBuildingBlockTemplate = new TemporaryFile(".dotx");

                using (var buildingBlockTemplate = BuildingBlockTemplate.Create(range.Application, _temporaryBuildingBlockTemplate.FullName))
                {
                    using (new DocumentAutomationHelper(
                        range,
                        "Cut Paragraph with Track Changes",
                        DocumentAutomationOption.Default | DocumentAutomationOption.DisableTrackRevisions))
                    {
                        range.Expand(WdUnits.wdParagraph);

                        // 1. Copy the contents of the paragraph (excluding the paragraph mark) with Track Changes switched off
                        // 2. Then deleted the entire paragraph (including the paragraph mark) with Track Changes.
                        range.Document.TrackRevisions = false;

                        // TODO: do not copy paragraph by paragraph, but copy bricks such as figures as an entire element
                        _clippedParagraphs = range.Paragraphs.OfType<Paragraph>()
                                                  .Select(p => new ClippedParagraph(p, buildingBlockTemplate.Template)).ToList();

                        document.TrackRevisions = true;
                        range.Delete();
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        public void TrackChangesPasteParagraph(IWordApplicationContext context)
        {
            try
            {
                if (_clippedParagraphs == null)
                {
                    _messageService.ShowSimpleMessage("Use the Cut Paragraph command first.", "Track Changes");
                    return;
                }

                var selection = context.Application.GetSelection();
                if (selection == null)
                {
                    return;
                }

                var range = selection.Range.Duplicate;

                using (var buildingBlockTemplate = BuildingBlockTemplate.Load(range.Application, _temporaryBuildingBlockTemplate.FullName))
                {
                    using (var documentAutomationHelper =
                        new DocumentAutomationHelper(
                            range,
                            "Paste Paragraph with Track Changes",
                            DocumentAutomationOption.Default | DocumentAutomationOption.DisableTrackRevisions))
                    {
                        range.Expand(WdUnits.wdParagraph);
                        range.Collapse(WdCollapseDirection.wdCollapseStart);

                        for (int index = _clippedParagraphs.Count - 1; index >= 0; index--)
                        {
                            var clippedParagraph = _clippedParagraphs[index];

                            var placeholderRange = clippedParagraph.Paste(range, buildingBlockTemplate.Template);
                            if (placeholderRange != null)
                            {
                                documentAutomationHelper.SelectWhenDone(placeholderRange);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        public sealed class BuildingBlockTemplate : IDisposable
        {
            private readonly Microsoft.Office.Interop.Word.AddIn _addIn;
            private readonly Document _document;

            private BuildingBlockTemplate(Application application, string fileName)
            {
                _document = application.Documents.Open(fileName, Visible: false, AddToRecentFiles: false);
                _addIn = application.AddIns.Add(fileName);
                Template = application.Templates
                                      .OfType<Template>()
                                      .FirstOrDefault(t => t.Name == Path.GetFileName(fileName));
            }

            public Template Template { get; }

            public static BuildingBlockTemplate Create(Application application, string fileName)
            {
                CreateBuildingBlockTemplate(fileName);
                return new BuildingBlockTemplate(application, fileName);
            }

            public static BuildingBlockTemplate Load(Application application, string fileName)
            {
                return new BuildingBlockTemplate(application, fileName);
            }

            public void Dispose()
            {
                _document.Save();
                _document.Close();
                _addIn.Delete();
            }

            private static void CreateBuildingBlockTemplate(string fileName)
            {
                using (var docx = WordprocessingDocument.Create(fileName, WordprocessingDocumentType.Template))
                {
                    docx.GetOrCreateGlossaryDocumentPart();
                    docx.GetOrCreateMainDocumentPart();
                    docx.Save();
                }
            }
        }

        public class ClippedParagraph
        {
            private readonly Style _paragraphStyle;
            private readonly string _buildingBlockName = Guid.NewGuid().ToString("N");

            public ClippedParagraph(Paragraph paragraph, Template template)
            {
                var range = paragraph.Range;

                range.MoveEnd(WdUnits.wdCharacter, -1);
                range.Copy();

                // ReSharper disable once UseIndexedProperty
                template.BuildingBlockEntries.Add(_buildingBlockName, WdBuildingBlockTypes.wdTypeCustom1, "Eurolook", range);

                range.MoveEnd(WdUnits.wdCharacter, 1);

                // ReSharper disable once UseIndexedProperty
                _paragraphStyle = (Style)range.Paragraphs.Last.get_Style();
            }

            public Range Paste(Range range, Template template)
            {
                range.Document.TrackRevisions = true;
                var para = range.Paragraphs.Add();

                // ReSharper disable once UseIndexedProperty
                para.set_Style(_paragraphStyle);
                range.Collapse(WdCollapseDirection.wdCollapseStart);

                range.Document.TrackRevisions = false;
                int start = range.Start;

                var buildingBlock = template.BuildingBlockEntries.Item(_buildingBlockName);
                buildingBlock.Insert(range, true);
                buildingBlock.Delete();
                template.Saved = true;

                range.Start = start;

                range.Document.TrackRevisions = true;
                var placeholderRange = InsertMovePrefix(para);

                range.Collapse(WdCollapseDirection.wdCollapseStart);

                return placeholderRange;
            }

            private static Range InsertMovePrefix(Paragraph paragraph)
            {
                bool hasNumbering = !string.IsNullOrWhiteSpace(paragraph.Range.ListFormat.ListString);
                if (!hasNumbering)
                {
                    return null;
                }

                var prefixRange = paragraph.Range.Duplicate;
                prefixRange.Collapse(WdCollapseDirection.wdCollapseStart);

                var cc = prefixRange.ContentControls.Add(WdContentControlType.wdContentControlRichText, prefixRange);
                cc.Tag = MovePrefixContentControlTag;
                cc.Range.Text = "(ex-paragraph ) ";

                var placeHolderRange = cc.Range.Duplicate;
                placeHolderRange.Start += 14;
                placeHolderRange.End = placeHolderRange.Start;

                var placeholderCc = placeHolderRange.ContentControls.Add(WdContentControlType.wdContentControlRichText, placeHolderRange);
                placeholderCc.Temporary = true;
                placeholderCc.SetPlaceholderText(Text: "<Number>");
                return placeholderCc.Range;
            }
        }
    }
}
