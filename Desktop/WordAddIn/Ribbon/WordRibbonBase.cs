﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Windows.Forms;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Properties;
using Eurolook.AddIn.Common.Ribbon;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.ViewModels;
using stdole;

namespace Eurolook.WordAddIn.Ribbon
{
    public abstract class WordRibbonBase : RibbonBase, IRibbonCallbackHandler, IRibbonLauncher
    {
        private readonly Lazy<IWordApplicationContext> _applicationContext;
        private readonly Lazy<IDocumentManager> _documentManager;

        protected WordRibbonBase(
            Lazy<IAddinContext> addinContext,
            Lazy<IWordApplicationContext> applicationContext,
            Lazy<IMessageService> messageService,
            Lazy<ISettingsService> settingsService,
            Lazy<IDocumentManager> documentManager,
            Lazy<IEmbeddedResourceManager> embeddedResourceManager)
            : base(addinContext, messageService, settingsService, embeddedResourceManager)
        {
            _applicationContext = applicationContext;
            _documentManager = documentManager;
        }

        protected IDocumentManager DocumentManager => _documentManager.Value;

        protected IWordApplicationContext ApplicationContext => _applicationContext.Value;

        public abstract override string GetCustomUI(string ribbonId);

        protected override Image GetResourceImage(string resourceId)
        {
            return GetResourceImageRecursive(GetType(), resourceId);
        }

        protected bool CanEditDocument()
        {
            // ReadOnly mode should disable the button,
            // however it cannot do because Word does not revalidate the ribbon when read-only mode is exited.
            return !ApplicationContext.Application.IsProtectedView()
                   && ApplicationContext.Application.Documents.Count > 0;
        }

        protected bool IsEurolookDocument()
        {
            var document = DocumentManager.GetActiveDocumentViewModel();
            return IsEurolookDocument(document);
        }

        protected bool IsEurolookDocument(DocumentViewModel document)
        {
            return document != null && document.IsEurolookDocument && document.IsDocumentModelKnown;
        }

        [SuppressMessage(
            "ReSharper",
            "ClassNeverInstantiated.Local",
            Justification = "We want this as a non-static class.")]
        private sealed class ConvertImageHelper : AxHost
        {
            // ReSharper disable once AssignNullToNotNullAttribute
            private ConvertImageHelper()
                : base(null)
            {
            }

            public static IPictureDisp Convert(Image image)
            {
                return (IPictureDisp)GetIPictureDispFromPicture(image);
            }
        }

        public virtual Dictionary<string, List<CommandBrick>> GetLauncherCommands()
        {
            return new Dictionary<string, List<CommandBrick>>();
        }

        public void ExecuteCommandBrick(CommandBrick brick)
        {
            try
            {
                var documentViewModel = DocumentManager.GetActiveDocumentViewModel();
                documentViewModel.ExecuteToolCommand(brick).FireAndForgetSafeAsync(this.LogError);
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }
    }
}
