using System.Collections.Generic;
using Eurolook.WordAddIn.ContentBrickActions.BrickToolbar.Buttons;

namespace Eurolook.WordAddIn.ContentBrickActions.Actions
{
    public class SignatureContentBrickActions : DefaultContentBrickActions
    {
        public SignatureContentBrickActions(
            IEnumerable<IDefaultBrickToolbarButton> defaultButtons,
            IEnumerable<IESignatureBrickToolbarButton> eSignatureButtons)
            : base(defaultButtons)
        {
            AddAndSortButtons(eSignatureButtons);
        }
    }
}
