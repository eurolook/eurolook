﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.WordAddIn.BrickCommands;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.ContentBrickActions.BrickToolbar;
using Eurolook.WordAddIn.ContentBrickActions.BrickToolbar.Buttons;
using Eurolook.WordAddIn.ViewModels;

namespace Eurolook.WordAddIn.ContentBrickActions.Actions
{
    public class DefaultContentBrickActions : IContentBrickActions, IBrickToolbar
    {
        public DefaultContentBrickActions(IEnumerable<IDefaultBrickToolbarButton> buttons)
        {
            BrickToolbarButtons = new List<IBrickToolbarButton>();
            AddAndSortButtons(buttons);
        }

        public List<IBrickToolbarButton> BrickToolbarButtons { get; }

        public void InitBrickToolbar(
            IBrickToolbarViewModel contentControlToolbarViewModel,
            DocumentViewModel documentViewModel)
        {
            foreach (var button in BrickToolbarButtons)
            {
                button.Init(contentControlToolbarViewModel, documentViewModel);
            }
        }

        public virtual Task AfterDocumentCreationAsync(IEurolookWordDocument eurolookDocument)
        {
            return Task.CompletedTask;
        }

        public virtual Task AfterBrickInsertionAsync(IBrickExecutionContext context)
        {
            return Task.CompletedTask;
        }

        public virtual async Task DeleteBrickAsync(IBrickExecutionContext context)
        {
            using (var command = new DeleteBrickCommand(context.EurolookDocument.BrickEngine))
            {
                await command.ExecuteAsync(context, null);
            }
        }

        protected void AddAndSortButtons(IEnumerable<IBrickToolbarButton> additionalButtons)
        {
            BrickToolbarButtons.AddRange(additionalButtons);
            BrickToolbarButtons.Sort((a, b) => a.Sequence.CompareTo(b.Sequence));
        }
    }
}
