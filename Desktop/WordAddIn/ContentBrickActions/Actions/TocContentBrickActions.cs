﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.ContentBrickActions.BrickToolbar.Buttons;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.ContentBrickActions.Actions
{
    public class TocContentBrickActions : DefaultContentBrickActions
    {
        private readonly IUpdateTocService _updateTocService;
        public TocContentBrickActions(
            IEnumerable<IDefaultBrickToolbarButton> defaultButtons,
            IEnumerable<ITocBrickToolbarButton> fieldButtons,
            IUpdateTocService updateTocService)
        : base(defaultButtons)
        {
            AddAndSortButtons(fieldButtons);
            _updateTocService = updateTocService;
        }

        public override Task AfterDocumentCreationAsync(IEurolookWordDocument eurolookDocument)
        {
            return UpdateTocTask(eurolookDocument);
        }

        public override Task AfterBrickInsertionAsync(IBrickExecutionContext context)
        {
            return UpdateTocTask(context.EurolookDocument);
        }

        private Task UpdateTocTask(IEurolookWordDocument eurolookDocument)
        {
            foreach (TableOfContents toc in eurolookDocument.Document.TablesOfContents)
            {
                _updateTocService.UpdateToc(toc);
            }

            return Task.CompletedTask;
        }
    }
}
