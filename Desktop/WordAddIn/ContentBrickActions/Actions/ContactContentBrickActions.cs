﻿using System.Collections.Generic;
using Eurolook.WordAddIn.ContentBrickActions.BrickToolbar.Buttons;

namespace Eurolook.WordAddIn.ContentBrickActions.Actions
{
    public class ContactContentBrickActions : DefaultContentBrickActions
    {
        public ContactContentBrickActions(
            IEnumerable<IDefaultBrickToolbarButton> defaultButtons,
            IEnumerable<IContactBrickToolbarButton> contactButtons)
            : base(defaultButtons)
        {
            AddAndSortButtons(contactButtons);
        }
    }
}
