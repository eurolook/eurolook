using System;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.Extensions;

namespace Eurolook.WordAddIn.ContentBrickActions
{
    public class ExecuteActionAfterBrickCreationHandler : IDocumentCreationHandler
    {
        private readonly IBrickRepository _brickRepository;
        private readonly Lazy<IContentBrickActionsResolver> _contentBrickActionsResolver;

        public ExecuteActionAfterBrickCreationHandler(
            IBrickRepository brickRepository,
            Lazy<IContentBrickActionsResolver> contentBrickActionsResolver)
        {
            _brickRepository = brickRepository;
            _contentBrickActionsResolver = contentBrickActionsResolver;
        }

        public float ExecutionOrder => 1000;

        public async Task HandleAsync(IEurolookDocument eurolookDocument)
        {
            // Down-cast to Word-specific interface to get a handle to the Document.
            var eurolookWordDocument = eurolookDocument.AsEurolookWordDocument();
            if (eurolookWordDocument == null)
            {
                return;
            }

            var contentBricks = eurolookWordDocument.GetDocument()
                                                    .LoadAllBricks(_brickRepository)
                                                    .Select(bc => bc.Brick as ContentBrick)
                                                    .Where(b => b != null);

            foreach (var contentBrick in contentBricks)
            {
                await _contentBrickActionsResolver.Value.Resolve(contentBrick).AfterDocumentCreationAsync(eurolookWordDocument);
            }
        }
    }
}
