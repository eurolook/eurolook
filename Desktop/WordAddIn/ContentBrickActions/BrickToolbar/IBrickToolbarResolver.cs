using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.ContentBrickActions.BrickToolbar
{
    public interface IBrickToolbarResolver
    {
        IBrickToolbar Resolve(ContentControl contentControl);
    }
}
