using System.Collections.Generic;
using Eurolook.WordAddIn.ContentBrickActions.BrickToolbar.Buttons;
using Eurolook.WordAddIn.ViewModels;

namespace Eurolook.WordAddIn.ContentBrickActions.BrickToolbar
{
    public interface IBrickToolbar
    {
        List<IBrickToolbarButton> BrickToolbarButtons { get; }

        void InitBrickToolbar(IBrickToolbarViewModel brickToolbarViewModel, DocumentViewModel documentViewModel);
    }
}
