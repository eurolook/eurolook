using System.Collections.Generic;
using Eurolook.WordAddIn.ContentBrickActions.Actions;
using Eurolook.WordAddIn.ContentBrickActions.BrickToolbar.Buttons;

namespace Eurolook.WordAddIn.ContentBrickActions.BrickToolbar
{
    public class BrickToolbarDesignViewModel : BrickToolbarViewModel
    {
        public BrickToolbarDesignViewModel()
            : base(null, null)
        {
            BrickToolbar = new DefaultContentBrickActions(new List<IDefaultBrickToolbarButton>
            {
                new ShowAlternativeOptionsButton(null),
                new DeleteBrickButton(),
                new InsertParagraphBeforeContentControlButton(),
                new InsertParagraphAfterContentControlButton(),
            });
        }
    }
}
