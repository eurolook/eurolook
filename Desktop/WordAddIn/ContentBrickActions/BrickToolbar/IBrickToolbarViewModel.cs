using Eurolook.WordAddIn.BrickEngine;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.ContentBrickActions.BrickToolbar
{
    public interface IBrickToolbarViewModel
    {
        IBrickExecutionContext BrickExecutionContext { get; }

        ContentControl SelectedContentControl { get; }

        void Detach();
    }
}
