using System;
using System.Diagnostics.CodeAnalysis;
using Eurolook.AddIn.Common;
using Eurolook.Common.Log;
using Eurolook.OfficeNotificationBar;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.ViewModels;
using Eurolook.WordAddIn.Views;
using GalaSoft.MvvmLight;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.ContentBrickActions.BrickToolbar
{
    public class BrickToolbarViewModel : ViewModelBase, IBrickToolbarViewModel, ICanLog, IDisposable
    {
        private readonly Func<IBrickExecutionContext> _brickExecutionContextFunc;
        private readonly IBrickToolbarResolver _brickToolBarResolver;
        private IBrickToolbar _brickToolbar;

        private Document Document { get; }

        private FloatingToolbar FloatingToolbar { get; set; }

        public BrickToolbarViewModel(
            Func<IBrickExecutionContext> brickExecutionContextFunc,
            IBrickToolbarResolver brickToolBarResolver)
        {
            _brickExecutionContextFunc = brickExecutionContextFunc;
            _brickToolBarResolver = brickToolBarResolver;
            Document = _brickExecutionContextFunc?.Invoke().Document;
        }

        ~BrickToolbarViewModel()
        {
            Dispose(false);
        }

        public ContentControl SelectedContentControl { get; set; }

        public IBrickExecutionContext BrickExecutionContext => _brickExecutionContextFunc();

        public bool IsAttached
        {
            get { return FloatingToolbar?.IsAttached ?? false; }
        }

        public IBrickToolbar BrickToolbar
        {
            get => _brickToolbar;
            set
            {
                _brickToolbar = value;
                RaisePropertyChanged(() => BrickToolbar);
            }
        }

        public void AttachTo(ContentControl contentControl, DocumentViewModel documentViewModel)
        {
            SelectedContentControl = contentControl;
            if (FloatingToolbar == null)
            {
                // lazy creation of the actual toolbar
                Execute.OnUiThread(
                    () =>
                    {
                        var content = new BrickToolbarView(this);
                        FloatingToolbar = new FloatingToolbar(Document.ActiveWindow, content, DockPosition.Left);
                    });
            }

            BrickToolbar = _brickToolBarResolver.Resolve(contentControl);
            BrickToolbar.InitBrickToolbar(this, documentViewModel);

            FloatingToolbar.AttachTo(contentControl);
        }

        public void Detach()
        {
            FloatingToolbar?.Detach();
            SelectedContentControl = null;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        [SuppressMessage("ReSharper", "UnusedParameter.Local", Justification = "This method signature is according to the IDisposable pattern.")]
        private void Dispose(bool disposing)
        {
            FloatingToolbar?.Dispose();
            FloatingToolbar = null;
        }
    }
}
