﻿using System.Windows.Input;

namespace Eurolook.WordAddIn.ContentBrickActions.BrickToolbar.Buttons
{
    public class DeleteBrickButton : BrickToolbarButtonBase, IDefaultBrickToolbarButton
    {
        public override string HeroIconPath => "/Graphics/heroicons-outline/white/trash.svg";

        public override string BackgroundColorHex => "#bf3232";

        public override string Tooltip => "Delete the brick.";

        public override float Sequence => 90;

        public override ICommand Command => DocumentViewModel.DeleteBrickCommand;
    }
}
