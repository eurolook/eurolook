﻿using System;
using System.Linq;
using System.Windows.Input;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.Messages;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace Eurolook.WordAddIn.ContentBrickActions.BrickToolbar.Buttons
{
    public class ShowAlternativeOptionsButton : BrickToolbarButtonBase, IDefaultBrickToolbarButton
    {
        private readonly IBrickVisibilityChecker _brickVisibilityChecker;

        public ShowAlternativeOptionsButton(IBrickVisibilityChecker brickVisibilityChecker)
        {
            _brickVisibilityChecker = brickVisibilityChecker;
        }

        public override bool IsVisible
        {
            get
            {
                try
                {
                    var groupId = SelectedContentControl?.GetBrickContainer()?.GroupId;
                    if (groupId == null)
                    {
                        return false;
                    }

                    // group has more than one visible brick
                    return BrickExecutionContext.EurolookDocument.DocumentStructures.Count(
                               ds => ds.Brick?.GroupId == groupId
                                     && _brickVisibilityChecker.IsBrickVisible(ds.Brick)) > 1;
                }
                catch (Exception ex)
                {
                    this.LogError(ex);
                }

                return false;
            }
        }

        public override string HeroIconPath => "/Graphics/heroicons-outline/white/bars-3.svg";
        public override string Tooltip { get; } = "Show alternative options.";

        public override float Sequence => 30;

        public override ICommand Command => new RelayCommand(ShowAlternativeOptions);

        private void ShowAlternativeOptions()
        {
            try
            {
                var groupId = SelectedContentControl?.GetBrickContainer()?.GroupId;
                if (groupId != null)
                {
                    Messenger.Default.Send(new ShowBrickGroupPanelMessage
                    {
                        EurolookDocument = BrickExecutionContext.EurolookDocument,
                        GroupId = groupId,
                    });
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }
    }
}
