﻿using System.Windows.Input;

namespace Eurolook.WordAddIn.ContentBrickActions.BrickToolbar.Buttons
{
    public class ChangeBrickAuthorButton : BrickToolbarButtonBase, IDefaultBrickToolbarButton
    {
        public override string HeroIconPath => "/Graphics/heroicons-outline/white/user.svg";

        public override string Tooltip => "Change the author information in the selected brick.";

        public override float Sequence => 60;

        public override ICommand Command => DocumentViewModel.ChangeBrickAuthorCommand;

        public override bool IsVisible => DocumentViewModel.HasSelectedBrickAuthorMapping;
    }
}
