﻿using System.Windows.Input;
using Eurolook.Common.Log;
using Eurolook.WordAddIn.ViewModels;

namespace Eurolook.WordAddIn.ContentBrickActions.BrickToolbar.Buttons
{
    public interface IBrickToolbarButton : ICanLog
    {
        bool IsVisible { get; }

        string HeroIconPath { get; }

        bool UseStaticBackgroundColor { get; }

        string BackgroundColorHex { get; }

        string Tooltip { get; }

        float Sequence { get; }

        ICommand Command { get; }

        void Init(IBrickToolbarViewModel contentControlToolbarViewModel, DocumentViewModel documentViewModel);
    }

    public interface IDefaultBrickToolbarButton : IBrickToolbarButton
    {
    }

    public interface IESignatureBrickToolbarButton : IBrickToolbarButton
    {
    }

    public interface IContactBrickToolbarButton : IBrickToolbarButton
    {
    }

    public interface ITocBrickToolbarButton : IBrickToolbarButton
    {
    }

    public interface IMarkingBrickToolbarButton : IBrickToolbarButton
    {
    }
}
