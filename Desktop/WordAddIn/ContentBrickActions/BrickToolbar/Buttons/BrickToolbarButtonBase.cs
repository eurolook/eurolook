﻿using System.Windows.Input;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.ViewModels;
using GalaSoft.MvvmLight;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.ContentBrickActions.BrickToolbar.Buttons
{
    public abstract class BrickToolbarButtonBase : ViewModelBase, IBrickToolbarButton
    {
        public virtual bool IsVisible { get; } = true;

        public abstract string HeroIconPath { get; }

        public bool UseStaticBackgroundColor => BackgroundColorHex != null;

        public virtual string BackgroundColorHex { get; } = null;

        public abstract string Tooltip { get; }

        public abstract ICommand Command { get; }

        public abstract float Sequence { get; }

        protected IBrickToolbarViewModel ContentControlToolbarViewModel { get; private set; }

        protected DocumentViewModel DocumentViewModel { get; private set; }

        protected IBrickExecutionContext BrickExecutionContext => ContentControlToolbarViewModel?.BrickExecutionContext;

        protected ContentControl SelectedContentControl => ContentControlToolbarViewModel?.SelectedContentControl;

        protected IEurolookWordDocument EurolookDocument => BrickExecutionContext?.EurolookDocument;

        protected IBrickEngine BrickEngine => EurolookDocument?.BrickEngine;

        protected DocumentModel DocumentModel => EurolookDocument?.DocumentModel;

        protected Document Document => BrickExecutionContext?.Document;

        public virtual void Init(
            IBrickToolbarViewModel contentControlToolbarViewModel,
            DocumentViewModel documentViewModel)
        {
            ContentControlToolbarViewModel = contentControlToolbarViewModel;
            DocumentViewModel = documentViewModel;
            RaisePropertyChanged(() => IsVisible);
        }
    }
}
