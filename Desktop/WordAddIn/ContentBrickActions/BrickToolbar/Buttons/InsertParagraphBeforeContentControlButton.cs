﻿using System.Windows.Input;

namespace Eurolook.WordAddIn.ContentBrickActions.BrickToolbar.Buttons
{
    public class InsertParagraphBeforeContentControlButton
        : BrickToolbarButtonBase, IDefaultBrickToolbarButton
    {
        public override string HeroIconPath => "/Graphics/heroicons-outline/white/para-before.svg";

        public override string Tooltip => "Insert a text paragraph before the selected brick.";

        public override ICommand Command => DocumentViewModel.InsertParagraphBeforeContentControlCommand;

        public override float Sequence => 70;
    }
}
