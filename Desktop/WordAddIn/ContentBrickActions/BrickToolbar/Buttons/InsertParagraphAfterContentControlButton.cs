﻿using System.Windows.Input;

namespace Eurolook.WordAddIn.ContentBrickActions.BrickToolbar.Buttons
{
    public class InsertParagraphAfterContentControlButton
        : BrickToolbarButtonBase, IDefaultBrickToolbarButton
    {
        public override string HeroIconPath => "/Graphics/heroicons-outline/white/para-after.svg";

        public override string Tooltip => "Insert a text paragraph after the selected brick.";

        public override ICommand Command => DocumentViewModel.InsertParagraphAfterContentControlCommand;

        public override float Sequence => 80;
    }
}
