﻿using System;
using System.Linq;
using System.Windows.Input;
using DocumentFormat.OpenXml.Wordprocessing;
using Eurolook.Common.Log;
using GalaSoft.MvvmLight.Command;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.ContentBrickActions.BrickToolbar.Buttons
{
    public class UpdateTocButton : BrickToolbarButtonBase, ITocBrickToolbarButton
    {
        private readonly IUpdateTocService _updateTocService;
        public UpdateTocButton(IUpdateTocService updateTocService)
        {
            _updateTocService = updateTocService;
        }

        public override string HeroIconPath => "/Graphics/heroicons-outline/white/arrow-path.svg";

        public override string BackgroundColorHex => "#2c8f6c";

        public override string Tooltip => "Update Table of Contents.";

        public override float Sequence => 20;

        public override ICommand Command => new RelayCommand(UpdateTocFields);

        private void UpdateTocFields()
        {
            try
            {
                var range = SelectedContentControl.Range;

                using (new DocumentAutomationHelper(range, "Update", DocumentAutomationOption.EnableUndo))
                {
                    foreach (var field in range.Fields.OfType<Field>().Where(f => f.Type == WdFieldType.wdFieldTOC))
                    {
                        UpdateTocField(range, field);
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private void UpdateTocField(Range range, Field field)
        {
            var toc = range.Document.TablesOfContents
                           .OfType<TableOfContents>()
                           .FirstOrDefault(
                               t =>
                                   t.Range.StoryType == field.Result.StoryType &&
                                   t.Range.Start <= field.Result.Start &&
                                   t.Range.End >= field.Result.End);

            _updateTocService.UpdateToc(toc);
        }
    }
}
