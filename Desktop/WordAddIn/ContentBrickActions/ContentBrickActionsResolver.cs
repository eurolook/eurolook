using Eurolook.AddIn.Common.Database;
using Eurolook.Common;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.ContentBrickActions.Actions;
using Eurolook.WordAddIn.ContentBrickActions.BrickToolbar;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.ContentBrickActions
{
    public class ContentBrickActionsResolver : IContentBrickActionsResolver, IBrickToolbarResolver
    {
        private readonly IClassNameResolver<IContentBrickActions> _contentBrickActionsResolver;
        private readonly IClassNameResolver<IBrickToolbar> _brickToolBarResolver;
        private readonly IBrickRepository _brickRepository;

        public ContentBrickActionsResolver(
            IClassNameResolver<IContentBrickActions> contentBrickActionsResolver,
            IClassNameResolver<IBrickToolbar> brickToolBarResolver,
            IBrickRepository brickRepository)
        {
            _contentBrickActionsResolver = contentBrickActionsResolver;
            _brickToolBarResolver = brickToolBarResolver;
            _brickRepository = brickRepository;
        }

        public IContentBrickActions Resolve(ContentBrick contentBrick)
        {
            var className = contentBrick.ContentBrickActionsClassName ?? nameof(DefaultContentBrickActions);
            return _contentBrickActionsResolver.Resolve(className);
        }

        public IContentBrickActions Resolve(ContentControl contentControl)
        {
            var className = GetContentBrickActionsClassName(contentControl);
            return _contentBrickActionsResolver.Resolve(className);
        }

        IBrickToolbar IBrickToolbarResolver.Resolve(ContentControl contentControl)
        {
            var className = GetContentBrickActionsClassName(contentControl);
            return _brickToolBarResolver.Resolve(className);
        }

        private string GetContentBrickActionsClassName(ContentControl contentControl)
        {
            var brickContainer = contentControl?.GetBrickContainer();
            var contentBrick = brickContainer != null ? _brickRepository.GetBrick(brickContainer.Id) as ContentBrick : null;
            return contentBrick?.ContentBrickActionsClassName ?? nameof(DefaultContentBrickActions);
        }
    }
}
