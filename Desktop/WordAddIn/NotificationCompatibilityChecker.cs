using System;
using Eurolook.AddIn.Common;
using Eurolook.Data;
using Eurolook.Data.Models;

namespace Eurolook.WordAddIn
{
    public class NotificationCompatibilityChecker : INotificationCompatibilityChecker
    {
        private readonly IConfigurationInfo _configurationInfo;

        public NotificationCompatibilityChecker(IConfigurationInfo configurationInfo)
        {
            _configurationInfo = configurationInfo;
        }

        public bool IsCompatibleWithCurrentVersion(Notification notification)
        {
            if (!string.IsNullOrWhiteSpace(notification.MinVersion) && Version.TryParse(notification.MinVersion, out var minVersion)
                                                                    && minVersion > _configurationInfo.ClientVersion)
            {
                // Add-in is older than the allowed MinVersion
                return false;
            }

            if (!string.IsNullOrWhiteSpace(notification.MaxVersion) && Version.TryParse(notification.MaxVersion, out var maxVersion)
                                                                    && _configurationInfo.ClientVersion > maxVersion)
            {
                // Add-in is newer than the allowed MaxVersion
                return false;
            }

            return true;
        }
    }
}
