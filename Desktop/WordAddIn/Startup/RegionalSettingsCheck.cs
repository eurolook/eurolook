using System.Globalization;

namespace Eurolook.WordAddIn.Startup
{
    public class RegionalSettingsCheck
    {
        private bool _isExecuted;

        public bool IsExpectedListSeparator { get; set; }

        public string DisplayName { get; set; }

        public string ExpectedListSeparator => ";";

        public void Execute()
        {
            if (_isExecuted)
            {
                return;
            }

            var cultureInfo = CultureInfo.CurrentCulture;
            DisplayName = cultureInfo.EnglishName;
            IsExpectedListSeparator = cultureInfo.TextInfo.ListSeparator == ExpectedListSeparator;
            _isExecuted = true;
        }
    }
}
