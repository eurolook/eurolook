﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.PerformanceLog;
using Eurolook.Common.AsyncCommands;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.WordAddIn.DocumentCreation;
using GalaSoft.MvvmLight.Command;

namespace Eurolook.WordAddIn.Wizard
{
    public class CreateDocumentWizardViewModel : WizardViewModel, ICanLog
    {
        private readonly IEnumerable<IBeforeNewDocumentCreationTask> _beforeNewDocumentCreationTasks;
        private readonly IEnumerable<IBeforeNewDocumentFromTemplateStoreCreationTask> _beforeNewDocumentFromTemplateStoreCreationTasks;
        private readonly IEnumerable<IAfterNewDocumentCreationTask> _afterNewDocumentCreationTasks;
        private readonly IEnumerable<IAfterNewDocumentFromTemplateStoreCreationTask> _afterNewDocumentFromTemplateStoreCreationTasks;
        private readonly IDocumentCreationService _documentCreationService;
        private readonly ISettingsService _settingsService;
        private readonly IPerformanceLogService _performanceLogService;

        private Stopwatch _stopwatch = new();
        private string _helpRef;

        public string HelpReference
        {
            get => _helpRef;
            set => Set(() => HelpReference, ref _helpRef, value);
        }

        public CreateDocumentWizardViewModel(
            IEnumerable<ICreateDocumentWizardPage> pages,
            IEnumerable<IBeforeNewDocumentCreationTask> beforeNewDocumentCreationTasks,
            IEnumerable<IBeforeNewDocumentFromTemplateStoreCreationTask> beforeNewDocumentFromTemplateStoreCreationTasks,
            IEnumerable<IAfterNewDocumentCreationTask> afterNewDocumentCreationTasks,
            IEnumerable<IAfterNewDocumentFromTemplateStoreCreationTask> afterNewDocumentFromTemplateStoreCreationTasks,
            IDocumentCreationService documentCreationService,
            ISettingsService settingsService,
            IPerformanceLogService performanceLogService)
            : base(pages.OrderBy(x => x.DisplayOrder), "Create Document")
        {
            _beforeNewDocumentCreationTasks = beforeNewDocumentCreationTasks;
            _beforeNewDocumentFromTemplateStoreCreationTasks = beforeNewDocumentFromTemplateStoreCreationTasks;
            _afterNewDocumentCreationTasks = afterNewDocumentCreationTasks;
            _afterNewDocumentFromTemplateStoreCreationTasks = afterNewDocumentFromTemplateStoreCreationTasks;
            _documentCreationService = documentCreationService;
            _settingsService = settingsService;
            _performanceLogService = performanceLogService;
            HelpReference = "CreateDocument";
            ShowSteps = false;
            ConfirmButtonText = "Create";
            ConfirmCommand = new AsyncCommand<WizardViewModel>(CreateDocument, errorHandler: this.LogError);
            CancelCommand = new RelayCommand<WizardViewModel>(CancelWizard);
            GetHelpCommand = new RelayCommand(() => _settingsService.OpenHelpLink(HelpReference));
        }

        public WizardWindow GetWindow()
        {
            var width = 1180;
            var height = 820;
            var window = new WizardWindow { Title = "Eurolook - Create Document" };
            window.ResizeMode = ResizeMode.CanResizeWithGrip;
            window.MinHeight = height;
            window.Height = height;
            window.Width = width;
            window.MinWidth = width;
            return window;
        }

        public override void OnBeforeShow()
        {
            _stopwatch.Restart();

            foreach (var page in Pages)
            {
                page.OnBeforeWizardShow();
            }

            PageIndex = 0;
        }

        public override void OnAfterShow()
        {
            _stopwatch.Stop();
            _performanceLogService.LogCreationDialogLoadedAsync(_stopwatch.ElapsedMilliseconds).FireAndForgetSafeAsync(this.LogError);

            this.LogTrace($"Task ShowWizard for {WizardTitle} finished in {_stopwatch.ElapsedMilliseconds} ms.");
        }

        public override void OnBeforeClose()
        {
            foreach (var page in Pages)
            {
                page.OnBeforeWizardClose();
            }
        }

        private async Task CreateDocument(WizardViewModel wizardViewModel)
        {
            var basicsPageViewModel = Pages.First(x => x is BasicsPageViewModel) as BasicsPageViewModel;
            var documentCreationInfo = basicsPageViewModel.GetDocumentCreationInfo();

            var mergedAfterDocumentCreationTasks = new List<IAfterNewDocumentCreationTask>(_afterNewDocumentCreationTasks);
            mergedAfterDocumentCreationTasks.AddRange(Pages.OfType<IAfterNewDocumentCreationTask>());

            var mergedBeforeDocumentCreationTasks = new List<IBeforeNewDocumentCreationTask>(_beforeNewDocumentCreationTasks);
            mergedBeforeDocumentCreationTasks.AddRange(Pages.OfType<IBeforeNewDocumentCreationTask>());

            await _documentCreationService.CreateDocumentAsync(
                documentCreationInfo,
                documentCreationInfo.IsTemplateFromTemplateStore ? _beforeNewDocumentFromTemplateStoreCreationTasks : mergedBeforeDocumentCreationTasks,
                documentCreationInfo.IsTemplateFromTemplateStore ? _afterNewDocumentFromTemplateStoreCreationTasks : mergedAfterDocumentCreationTasks,
                ProgressReporter<int>.Empty);

            wizardViewModel.CloseAction();
        }

        private void CancelWizard(WizardViewModel wizardViewModel)
        {
            wizardViewModel.CloseAction();
        }
    }
}
