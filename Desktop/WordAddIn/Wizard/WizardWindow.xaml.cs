﻿using System.ComponentModel;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.Common.Log;

namespace Eurolook.WordAddIn.Wizard
{
    public partial class WizardWindow : ICanLog
    {
        public WizardWindow()
        {
            InitializeComponent();
        }

        public async Task ShowWizard(WizardViewModel viewModel, IMessageService messageService)
        {
            DataContext = viewModel;
            viewModel.OnBeforeShow();
            var actualWindow = messageService.ShowSingleton(this, true, false, true);
            viewModel.CloseAction = actualWindow.Close;
            await viewModel.CurrentPage.OnLoad();
            viewModel.CurrentPage.IsLoaded = true;
            await viewModel.CurrentPage.OnShow();
            viewModel.OnAfterShow();
        }

        private void OnWindowClosing(object sender, CancelEventArgs e)
        {
            if (DataContext is WizardViewModel viewModel)
            {
                viewModel.OnBeforeClose();
            }
        }
    }
}
