﻿using System;

namespace Eurolook.WordAddIn.Wizard
{
    public class OpenCustomTemplateFromLink
    {
        public Guid TemplateId { get; set; }

        public bool AddToUserProfileUponDocumentCreation { get; set; }
    }
}
