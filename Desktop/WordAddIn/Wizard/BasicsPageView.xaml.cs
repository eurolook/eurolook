﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

namespace Eurolook.WordAddIn.Wizard
{
    public partial class BasicsPageView : UserControl
    {
        public BasicsPageView()
        {
            InitializeComponent();
        }

        private void TemplateInfoClicked(object sender, RoutedEventArgs e)
        {
            if (sender is Hyperlink hyperlink)
            {
                Process.Start(hyperlink.NavigateUri.ToString());
            }
        }
    }
}
