using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Eurolook.Common.Log;
using Eurolook.Data;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.ViewModels;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Eurolook.WordAddIn.Wizard
{
    public class BricksChooserViewModel : ViewModelBase, ICanLog
    {
        private DocumentModel _documentModel;
        private Dictionary<Guid, bool> _userBrickChoicesDictionary;
        private bool _isVisible;

        public ObservableCollection<UserBrickChoiceViewModel> Bricks { get; }

        public ICommand RestoreCommand { get; private set; }

        public bool IsVisible
        {
            get => _isVisible;
            set { Set(() => IsVisible, ref _isVisible, value); }
        }

        public BricksChooserViewModel()
        {
            Bricks = new ObservableCollection<UserBrickChoiceViewModel>();
            RestoreCommand = new RelayCommand(RestoreDefaults);
        }

        public void Load(DocumentModel documentModel, string userBrickChoices)
        {
            _documentModel = documentModel;
            Bricks.Clear();
            if (documentModel == null)
            {
                return;
            }

            _userBrickChoicesDictionary = UserBrickChoicesFormatter.ToDictionary(
                documentModel.BrickReferences,
                userBrickChoices);

            Bricks.Clear();
            AddUserBrickChoices(StoryType.Header);
            AddUserBrickChoices(StoryType.Begin);
            AddUserBrickChoices(StoryType.Body);
            AddUserBrickChoices(StoryType.End);
            AddUserBrickChoices(StoryType.Footer);
        }

        private void RestoreDefaults()
        {
            Load(_documentModel, null);
        }

        private void AddUserBrickChoices(StoryType story)
        {
            // hidden bricks shall be displayed here to make the creation dialog more transparent
            var selectableDocumentStructures = _documentModel.BrickReferences
                .Where(x => !(x.Brick is CommandBrick) && EnumHelper.IsStory(x.Position, story) && x.AutoBrickCreationType != AutoBrickCreationType.SimpleBodyContent)
                .OrderBy(x => x.VerticalPositionIndex);

            foreach (var documentStructure in selectableDocumentStructures)
            {
                if (documentStructure.Brick.Group != null)
                {
                    // group already exists?
                    var groupVm = Bricks.FirstOrDefault(x => x.GroupId == documentStructure.Brick.GroupId);
                    if (groupVm == null)
                    {
                        // no? create it!
                        groupVm = new UserBrickChoiceViewModel(documentStructure.Brick.Group);
                        Bricks.Add(groupVm);
                    }

                    var brickVm = new UserBrickChoiceViewModel(documentStructure, _userBrickChoicesDictionary, groupVm);
                    groupVm.Items.Add(brickVm);
                    if (brickVm.IsSelected)
                    {
                        groupVm.IsExpanded = true;
                    }
                }
                else
                {
                    Bricks.Add(new UserBrickChoiceViewModel(documentStructure, _userBrickChoicesDictionary, null));
                }
            }
        }
    }

    public class BricksChooserDesignViewModel : BricksChooserViewModel
    {
        public BricksChooserDesignViewModel()
        {
            Bricks.Add(new UserBrickChoiceViewModel()
            {
                DisplayName = "Brick A",
                BrickId = Guid.NewGuid(),
                Tooltip = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa.",
            });
            Bricks.Add(new UserBrickChoiceViewModel()
            {
                DisplayName = "Brick Group B",
                GroupId = Guid.NewGuid(),
                Items = new ObservableCollection<UserBrickChoiceViewModel>()
                {
                    new UserBrickChoiceViewModel()
                    {
                        DisplayName = "Brick B.1",
                        BrickId = Guid.NewGuid(),
                        Tooltip = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa.",
                    },
                    new UserBrickChoiceViewModel()
                    {
                        DisplayName = "Brick B.2",
                        BrickId = Guid.NewGuid(),
                        Tooltip = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa.",
                    },
                },
            });
            Bricks.Add(new UserBrickChoiceViewModel()
            {
                DisplayName = "Brick C",
                BrickId = Guid.NewGuid(),
                Tooltip = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa.",
            });
            Bricks.Add(new UserBrickChoiceViewModel()
            {
                DisplayName = "Brick D",
                BrickId = Guid.NewGuid(),
                Tooltip = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa.",
            });
            Bricks.Add(new UserBrickChoiceViewModel()
            {
                DisplayName = "Brick Group E",
                GroupId = Guid.NewGuid(),
                Items = new ObservableCollection<UserBrickChoiceViewModel>()
                {
                    new UserBrickChoiceViewModel()
                    {
                        DisplayName = "Brick E.1",
                        BrickId = Guid.NewGuid(),
                        Tooltip = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa.",
                    },
                },
            });
        }
    }
}
