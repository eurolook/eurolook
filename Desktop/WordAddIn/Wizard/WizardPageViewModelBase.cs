﻿using System.Threading.Tasks;
using Eurolook.Common.Log;
using GalaSoft.MvvmLight;

namespace Eurolook.WordAddIn.Wizard
{
    public abstract class WizardPageViewModelBase : ViewModelBase, IWizardPageViewModel, ICanLog
    {
        private bool _isLoaded;

        public abstract string PageTitle { get; }

        public virtual string PageDescription => null;

        public virtual bool IsVisible => true;

        public bool IsLoaded
        {
            get => _isLoaded;
            set
            {
                _isLoaded = value;
                RaisePropertyChanged(() => IsLoaded);
            }
        }

        public virtual bool IsValid => true;

        public bool IsCurrentPage => WizardViewModel.CurrentPage == this;

        public WizardViewModel WizardViewModel { get; set; }

        public virtual Task OnLoad()
        {
            return Task.CompletedTask;
        }

        public virtual Task OnShow()
        {
            RaisePropertyChanged(() => IsCurrentPage);
            return Task.CompletedTask;
        }

        public virtual Task OnHide()
        {
            RaisePropertyChanged(() => IsCurrentPage);
            return Task.CompletedTask;
        }

        public virtual Task OnBeforeWizardShow()
        {
            return Task.CompletedTask;
        }

        public virtual Task OnBeforeWizardClose()
        {
            return Task.CompletedTask;
        }
    }
}
