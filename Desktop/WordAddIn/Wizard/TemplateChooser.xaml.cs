﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Eurolook.AddIn.Common.Extensions;

namespace Eurolook.WordAddIn.Wizard
{
    public partial class TemplateChooser : UserControl
    {
        public TemplateChooser()
        {
            InitializeComponent();
        }

        private void OnSearchBoxPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key is Key.Down or Key.Up)
            {
                var listBoxItem = (ListBoxItem)TemplatesListBox.ItemContainerGenerator.ContainerFromItem(TemplatesListBox.SelectedItem);
                listBoxItem?.Focus();
            }
        }

        private void OnTemplatesListBoxLoaded(object sender, EventArgs e)
        {
            TemplatesListBox.ScrollToSelectionWhenLoaded();
        }

        private void UpdateScrollButtonVisibility(object sender, RoutedEventArgs e)
        {
            var canScroll = PreviewScroller.ExtentHeight > PreviewScroller.ActualHeight;
            var isTop = PreviewScroller.VerticalOffset == 0;
            var isBottom = PreviewScroller.VerticalOffset + PreviewScroller.ViewportHeight == PreviewScroller.ExtentHeight;

            ScrollUpButton.Visibility = canScroll && !isTop && (PreviewScroller.IsMouseOver || ScrollUpButton.IsMouseOver)
                ? Visibility.Visible
                : Visibility.Hidden;

            ScrollDownButton.Visibility = canScroll && !isBottom && (PreviewScroller.IsMouseOver || ScrollDownButton.IsMouseOver)
                ? Visibility.Visible
                : Visibility.Hidden;
        }

        private void ScrollPreviewUp(object sender, RoutedEventArgs e)
        {
            PreviewScroller.ScrollToTop();
        }

        private void ScrollPreviewDown(object sender, RoutedEventArgs e)
        {
            PreviewScroller.ScrollToEnd();
        }
    }
}
