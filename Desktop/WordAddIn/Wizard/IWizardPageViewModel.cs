﻿using System.Threading.Tasks;

namespace Eurolook.WordAddIn.Wizard
{
    public interface IWizardPageViewModel
    {
        string PageTitle { get; }

        string PageDescription { get; }

        bool IsVisible { get; }

        bool IsCurrentPage { get; }

        bool IsLoaded { get; set; }

        bool IsValid { get; }

        WizardViewModel WizardViewModel { get; set; }

        Task OnLoad();

        Task OnShow();

        Task OnHide();

        Task OnBeforeWizardShow();

        Task OnBeforeWizardClose();
    }
}
