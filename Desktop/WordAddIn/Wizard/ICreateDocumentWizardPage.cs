﻿namespace Eurolook.WordAddIn.Wizard
{
    public interface ICreateDocumentWizardPage : IWizardPageViewModel
    {
        public int DisplayOrder { get; }
    }
}
