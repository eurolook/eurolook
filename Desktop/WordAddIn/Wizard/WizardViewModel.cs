﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Eurolook.WordAddIn.Wizard
{
    public class WizardViewModel : ViewModelBase, ICanLog
    {
        private int _pageIndex;
        private string _wizardTitle;
        private bool _showBottomBar;

        public WizardViewModel(IEnumerable<IWizardPageViewModel> pages, string wizardTitle)
        {
            if (pages.Any() != true)
            {
                throw new Exception("Wizard has to be initialized with pages.");
            }

            WizardTitle = wizardTitle;
            Pages = pages.ToArray();
            foreach (var page in Pages)
            {
                page.WizardViewModel = this;
            }

            VisiblePages = new ObservableCollectionEx<IWizardPageViewModel>(Pages.Where(x => x.IsVisible));
            ShowBottomBar = true;
            ShowSteps = true;
            NextPageCommand = new RelayCommand(() => GoToNextPage().FireAndForgetSafeAsync(this.LogError), () => CanGoToNextPage);
            PreviousPageCommand = new RelayCommand(() => GoToPreviousPage().FireAndForgetSafeAsync(this.LogError), () => CanGoToPreviousPage);
            ConfirmButtonText = "Finish";
        }

        public string WizardTitle
        {
            get => _wizardTitle;
            set => Set(() => WizardTitle, ref _wizardTitle, value);
        }

        public IWizardPageViewModel[] Pages { get; private set; }

        public ObservableCollectionEx<IWizardPageViewModel> VisiblePages { get; set; }

        public IWizardPageViewModel CurrentPage => VisiblePages.ElementAt(PageIndex);

        public bool IsFirstPage => PageIndex < 1;

        public bool IsLastPage => PageIndex == VisiblePages.Count() - 1;

        public bool CanGoToNextPage => !IsLastPage && CurrentPage.IsValid;

        public bool CanGoToPreviousPage => !IsFirstPage;

        public string PageNumbering => $"Step {PageIndex + 1} of {VisiblePages.Count()}";

        public bool ShowSteps { get; set; }

        public bool ShowBottomBar
        {
            get => _showBottomBar;
            set => Set(() => ShowBottomBar, ref _showBottomBar, value);
        }

        public RelayCommand NextPageCommand { get; }

        public RelayCommand PreviousPageCommand { get; }

        public RelayCommand<WizardViewModel> CancelCommand { get; set; }

        public ICommand ConfirmCommand { get; set; }

        public string ConfirmButtonText { get; set; }

        public RelayCommand GetHelpCommand { get; set; }

        public Action CloseAction { get; set; }

        public virtual void OnBeforeShow()
        {
        }

        public virtual void OnAfterShow()
        {
        }

        public virtual void OnBeforeClose()
        {
        }

        public async Task GoToNextPage()
        {
            if (!CanGoToNextPage)
            {
                return;
            }

            var previousPage = CurrentPage;

            PageIndex++;
            if (!CurrentPage.IsLoaded)
            {
                await CurrentPage.OnLoad();
                CurrentPage.IsLoaded = true;
            }

            await previousPage.OnHide();
            await CurrentPage.OnShow();
        }

        public async Task GoToPreviousPage()
        {
            if (!CanGoToPreviousPage)
            {
                return;
            }

            var previousPage = CurrentPage;
            PageIndex--;

            await previousPage.OnHide();
            await CurrentPage.OnShow();
        }

        public void RefreshState()
        {
            VisiblePages.Clear();
            VisiblePages.AddRange(Pages.Where(p => p.IsVisible));
            RaisePropertyChanged(() => PageIndex);
            RaisePropertyChanged(() => CurrentPage);
            RaisePropertyChanged(() => IsFirstPage);
            RaisePropertyChanged(() => IsLastPage);
            RaisePropertyChanged(() => CanGoToNextPage);
            RaisePropertyChanged(() => CanGoToPreviousPage);
            RaisePropertyChanged(() => PageNumbering);
            NextPageCommand.RaiseCanExecuteChanged();
            PreviousPageCommand.RaiseCanExecuteChanged();
        }

        public int PageIndex
        {
            get => _pageIndex;
            set
            {
                if (value < 0 || value >= VisiblePages.Count())
                {
                    return;
                }

                _pageIndex = value;
                RefreshState();
            }
        }
    }

    public class WizardDesignPage : WizardPageViewModelBase
    {
        public WizardDesignPage(string pageTitle)
        {
            PageTitle = pageTitle;
        }

        public override string PageTitle { get; }
    }

    public class WizardDesignViewModel : WizardViewModel
    {
        public WizardDesignViewModel()
            : base(
                new List<IWizardPageViewModel>
                {
                    new WizardDesignPage("Wizard Page 1")
                },
                "Creation Wizard")
        {
        }
    }
}
