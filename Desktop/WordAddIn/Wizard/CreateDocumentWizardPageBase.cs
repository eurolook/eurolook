﻿using Eurolook.Data.Models;

namespace Eurolook.WordAddIn.Wizard
{
    public class CreateDocumentWizardPageBase : WizardPageViewModelBase, ICreateDocumentWizardPage
    {
        public int DisplayOrder { get; }

        public override string PageTitle { get; }

        public CreateDocumentWizardPageBase(string pageTitle, int displayOrder)
        {
            PageTitle = pageTitle;
            DisplayOrder = displayOrder;
        }

        public bool IsDocumentModelSupported(DocumentModel documentModel)
        {
            if (string.IsNullOrEmpty(documentModel?.CreateDocumentWizardPages))
            {
                return false;
            }

            var pageName = GetType().FullName;
            foreach (var supportedPage in documentModel.CreateDocumentWizardPages.Split(',', ';', '\n'))
            {
                if (pageName.EndsWith(supportedPage))
                {
                    return true;
                }
            }

            return false;
        }

        public virtual void OnSelectedTemplateChanged(TemplateViewModel templateViewModel)
        {
        }
    }
}
