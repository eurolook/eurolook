﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using DocumentFormat.OpenXml.Office2010.Excel;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.AddIn.Common.SystemConfiguration;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data.Compatibility;
using Eurolook.Data.Constants;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;
using Eurolook.WordAddIn.Database;
using Eurolook.WordAddIn.TemplateStore;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Eurolook.WordAddIn.Wizard
{
    public class TemplateChooserViewModel : ViewModelBase, ICanLog
    {
        private readonly IDocumentModelRepository _documentModelRepository;
        private readonly ITemplateRepository _templateRepository;
        private readonly ISharedTemplateRepository _sharedTemplateRepository;
        private readonly IRecentTemplateRepository _recentTemplateRepository;
        private readonly ILanguageRepository _languageRepository;
        private readonly IEurolookDataRepository _eurolookDataRepository;
        private readonly IDocumentModelVersionCompatibilityTester _documentModelVersionCompatibilityTester;
        private readonly IBrickVersionCompatibilityTester _brickVersionCompatibilityTester;
        private readonly ISettingsService _settingsService;
        private readonly ISystemConfigurationService _systemConfigurationService;
        private readonly ITemplateStoreManager _templateStoreManager;
        private readonly IMessageService _messageService;
        private readonly IWordUserDataRepository _wordUserDataRepository;
        private int _selectedTemplateViewIndex;
        private TemplateViewModel _selectedTemplate;
        private string _searchText;
        private bool _isSearchBoxFocused;

        public User User { get; set; }
        public OpenCustomTemplateFromLink OpenCustomTemplateFromLink { get; set; }
        public ObservableCollectionEx<TemplateViewModel> Templates { get; set; }
        public CollectionViewSource TemplateListSource { get; set; }
        public ICollectionView TemplateListView => TemplateListSource.View;
        protected TemplateViewKind EurolookTemplates => new() { DisplayName = "Eurolook Templates" };
        protected TemplateViewKind CustomTemplates => new() { DisplayName = "Custom Templates" };
        protected TemplateViewKind FavouriteTemplates => new() { DisplayName = "Favourites" };
        protected TemplateViewKind RecentlyUsedTemplates => new() { DisplayName = "Recently Used" };
        public ObservableCollectionEx<TemplateViewKind> TemplateViewKinds { get; set; }

        public TemplateViewKind SelectedTemplateViewKind => TemplateViewKinds[SelectedTemplateViewIndex];
        public int SelectedTemplateViewIndex
        {
            get => _selectedTemplateViewIndex;
            set
            {
                SelectedTemplateViewKind.SelectedTemplate = _selectedTemplate;
                Set(() => SelectedTemplateViewIndex, ref _selectedTemplateViewIndex, value);
                RefreshTemplateListView();
                SelectedTemplate = SelectedTemplateViewKind.SelectedTemplate;
            }
        }

        public TemplateViewModel SelectedTemplate
        {
            get => _selectedTemplate;
            set => Set(() => SelectedTemplate, ref _selectedTemplate, value);
        }

        public bool ShowCustomTemplates => SelectedTemplateViewKind.Equals(CustomTemplates) && !TemplateListView.IsEmpty && SearchText.IsNullOrEmpty();
        public bool ShowRecentTemplates => SelectedTemplateViewKind.Equals(RecentlyUsedTemplates) && !TemplateListView.IsEmpty && SearchText.IsNullOrEmpty();
        public bool ShowTemplateStoreLink => SelectedTemplateViewKind.Equals(CustomTemplates) && !_settingsService.IsStandaloneMode;
        public bool ShowEmptyStageForCustomTemplates => SelectedTemplateViewKind.Equals(CustomTemplates) && TemplateListView.IsEmpty && SearchText.IsNullOrEmpty();
        public bool ShowEmptyStageForRecentTemplates => SelectedTemplateViewKind.Equals(RecentlyUsedTemplates) && TemplateListView.IsEmpty && SearchText.IsNullOrEmpty();
        public bool ShowEmptyStageForSearch => TemplateListView.IsEmpty && !SearchText.IsNullOrEmpty();

        public string SearchText
        {
            get => _searchText;
            set
            {
                Set(() => SearchText, ref _searchText, value);
                RefreshTemplateListView();
            }
        }

        public bool IsSearchBoxFocused
        {
            get => _isSearchBoxFocused;
            set { Set(() => IsSearchBoxFocused, ref _isSearchBoxFocused, value); }
        }

        public RelayCommand BrowseTemplateStoreCommand { get; private set; }

        public TemplateChooserViewModel(
            IDocumentModelRepository documentModelRepository,
            ITemplateRepository templateRepository,
            ISharedTemplateRepository sharedTemplateRepository,
            IRecentTemplateRepository recentTemplateRepository,
            ILanguageRepository languageRepository,
            IEurolookDataRepository eurolookDataRepository,
            IDocumentModelVersionCompatibilityTester documentModelVersionCompatibilityTester,
            IBrickVersionCompatibilityTester brickVersionCompatibilityTester,
            ISettingsService settingsService,
            ISystemConfigurationService systemConfigurationService,
            IMessageService messageService,
            IWordUserDataRepository wordUserDataRepository,
            ITemplateStoreManager templateStoreManager = null)
        {
            _documentModelRepository = documentModelRepository;
            _templateRepository = templateRepository;
            _sharedTemplateRepository = sharedTemplateRepository;
            _recentTemplateRepository = recentTemplateRepository;
            _languageRepository = languageRepository;
            _eurolookDataRepository = eurolookDataRepository;
            _documentModelVersionCompatibilityTester = documentModelVersionCompatibilityTester;
            _brickVersionCompatibilityTester = brickVersionCompatibilityTester;
            _settingsService = settingsService;
            _systemConfigurationService = systemConfigurationService;
            _templateStoreManager = templateStoreManager;
            _messageService = messageService;
            _wordUserDataRepository = wordUserDataRepository;
            BrowseTemplateStoreCommand = new RelayCommand(OpenTemplateStore);
            TemplateViewKinds = new ObservableCollectionEx<TemplateViewKind>(new[]
            {
                EurolookTemplates,
            });

            Templates = new ObservableCollectionEx<TemplateViewModel>();
            TemplateListSource = new CollectionViewSource();
            TemplateListSource.Filter += OnTemplateListSourceFilter;
            TemplateListSource.Source = Templates;
            TemplateListView.CollectionChanged += OnTemplateListViewChanged;
        }

        private void OpenTemplateStore()
        {
            _templateStoreManager?.OpenTemplateStore();
        }

        public async Task LoadAsync()
        {
            bool isTemplateStoreEnabled = !_settingsService.IsStandaloneMode && await _systemConfigurationService.IsTemplateStoreEnabledForUser();
            if (_settingsService.IsStandaloneMode || isTemplateStoreEnabled)
            {
                if (!TemplateViewKinds.Contains(CustomTemplates))
                {
                    TemplateViewKinds.InsertAt(CustomTemplates, 1);
                }

                if (!TemplateViewKinds.Contains(RecentlyUsedTemplates))
                {
                    TemplateViewKinds.InsertAt(RecentlyUsedTemplates, 2);
                }
            }
            else if (!TemplateViewKinds.Contains(RecentlyUsedTemplates))
            {
                TemplateViewKinds.InsertAt(RecentlyUsedTemplates, 1);
            }

            Templates.Clear();

            // load document models
            var documentModels = await _documentModelRepository
                .GetVisibleDocumentModelsAsync(_documentModelVersionCompatibilityTester, _brickVersionCompatibilityTester);

            Templates.AddRange(documentModels.Select(x => new TemplateViewModel(this, x, EurolookTemplates)).ToArray());

            // load templates
            if (_settingsService.IsStandaloneMode || isTemplateStoreEnabled)
            {
                var templateStoreTemplates = await _sharedTemplateRepository.GetUserTemplatesAsync(User.Id);
                Templates.AddRange(templateStoreTemplates.Select(x => new TemplateViewModel(this, x, CustomTemplates, _settingsService.IsStandaloneMode)).ToArray());
            }

            if (OpenCustomTemplateFromLink != null && Templates.All(x => x.Template?.Id != OpenCustomTemplateFromLink.TemplateId))
            {
                var templateOpenedFromLink = await _templateRepository.GetTemplateAsync(OpenCustomTemplateFromLink.TemplateId);
                var templateViewModel = new TemplateViewModel(this, templateOpenedFromLink, CustomTemplates)
                {
                    OpenCustomTemplateFromLink = OpenCustomTemplateFromLink,
                };
                Templates.Add(templateViewModel);
            }

            if (isTemplateStoreEnabled)
            {
                var _ = Task.Run(UpdateUserTemplatesFromServer);
            }

            await UpdateRecentlyUsedTemplates();

            var templateToSelect = GetSelectedTemplate(OpenCustomTemplateFromLink?.TemplateId);
            ActivateTab(templateToSelect);
            SelectedTemplate = templateToSelect;
            IsSearchBoxFocused = true;
            OpenCustomTemplateFromLink = null;
        }

        private async Task UpdateUserTemplatesFromServer()
        {
            bool isTemplateStoreEnabled = await _systemConfigurationService.IsTemplateStoreEnabledForUser(User);
            if (!isTemplateStoreEnabled)
            {
                return;
            }

            var sw = Stopwatch.StartNew();
            var userTemplates = await _templateStoreManager.GetUserTemplatesFromServer(5, 24);

            Execute.OnUiThread(
                () =>
                {
                    foreach (var userTemplate in userTemplates)
                    {
                        if (Templates.All(t => t.Id != userTemplate.TemplateId))
                        {
                            // BaseDocumentModel object is not transferred from the server
                            userTemplate.Template.BaseDocumentModel = _documentModelRepository.GetDocumentModel(userTemplate.Template.BaseDocumentModelId.Value);

                            foreach (var templateFile in userTemplate.Template.TemplateFiles)
                            {
                                // language object is not transferred from the server
                                templateFile.Language = _languageRepository.GetLanguage(templateFile.LanguageId);
                            }

                            foreach (var publication in userTemplate.Template.Publications)
                            {
                                // orga entity object is not transferred from the server
                                publication.OrgaEntity = _eurolookDataRepository.GetOrgaEntity(publication.OrgaEntityId);
                            }

                            Templates.Add(new TemplateViewModel(this, userTemplate, CustomTemplates, _settingsService.IsStandaloneMode));
                        }
                    }
                });

            sw.Stop();
            this.LogTrace($"Retrieved and processed {userTemplates.Length} user templates in {sw.ElapsedMilliseconds}ms.");
        }

        [SuppressMessage("SonarQube", "S2259: Null pointers should not be dereferenced", Justification = "Not null")]
        private async Task UpdateRecentlyUsedTemplates()
        {
            bool isTemplateStoreEnabled = await _systemConfigurationService.IsTemplateStoreEnabledForUser(User);

            var recentTemplates = await _recentTemplateRepository.GetRecentTemplatesAsync(User.Id);
            var recentTemplatesViewModels = new List<TemplateViewModel>();

            foreach (var template in recentTemplates)
            {
                if (template.IsDocumentModel)
                {
                    var documentModel = Templates.FirstOrDefault(temp => temp.DocumentModel.Id == template.TemplateId).DocumentModel;
                    recentTemplatesViewModels.Add(new TemplateViewModel(this, documentModel, RecentlyUsedTemplates));
                }
                else
                {
                    var templateStoreTemplate = Templates.FirstOrDefault(temp => temp.Template?.Id == template.TemplateId);
                    if (templateStoreTemplate?.Template != null)
                    {
                        var recentTemplateViewModel = new TemplateViewModel(this, templateStoreTemplate.Template, RecentlyUsedTemplates, _settingsService.IsStandaloneMode)
                        {
                            ContextMenuEntries = null,
                            IsOfflineAvailable = templateStoreTemplate.IsOfflineAvailable,
                        };
                        recentTemplatesViewModels.Add(recentTemplateViewModel);
                    }
                }
            }

            Templates.AddRange(recentTemplatesViewModels);
        }

        private void ActivateTab(TemplateViewModel selectedTemplate)
        {
            SelectedTemplateViewIndex = TemplateViewKinds.IndexOf(selectedTemplate.TemplateViewKind);
        }

        private TemplateViewModel GetSelectedTemplate(Guid? preselectedTemplateId = null)
        {
            return Templates.FirstOrDefault(item => item.IsCustomTemplate && item.Id == preselectedTemplateId)
                ?? Templates.FirstOrDefault(item => User.Settings.LastTemplateId.HasValue && item.Id == User.Settings.LastTemplateId && User.Settings.LastSelectedTab == TemplateViewKinds.IndexOf(item.TemplateViewKind))
                ?? Templates.FirstOrDefault(item => item.Id == User.Settings.LastDocumentModelId && User.Settings.LastSelectedTab == TemplateViewKinds.IndexOf(item.TemplateViewKind))
                ?? Templates.FirstOrDefault(item => item.Id == CommonDataConstants.NoteId)
                ?? Templates.FirstOrDefault();
        }

        private void RefreshTemplateListView()
        {
            TemplateListView.SortDescriptions.Clear();

            // Sort Recently Used by usage
            if (!SelectedTemplateViewKind.Equals(RecentlyUsedTemplates))
            {
                TemplateListView.SortDescriptions.Add(new SortDescription() { PropertyName = nameof(TemplateViewModel.CategoryName) });
                TemplateListView.SortDescriptions.Add(new SortDescription() { PropertyName = nameof(TemplateViewModel.DisplayName) });
            }

            TemplateListView.GroupDescriptions.Clear();

            var templateGroupCount = Templates
                .Where(x => x.TemplateViewKind.Equals(SelectedTemplateViewKind))
                .GroupBy(x => x.CategoryName)
                .Count();

            // Group by category only in views Eurolook Templates and Favourites
            if (templateGroupCount > 1 &&
                (SelectedTemplateViewKind.Equals(EurolookTemplates) || SelectedTemplateViewKind.Equals(FavouriteTemplates)))
            {
                TemplateListView.GroupDescriptions.Add(new PropertyGroupDescription() { PropertyName = nameof(TemplateViewModel.CategoryName) });
            }

            TemplateListView.Refresh();
            RaisePropertyChanged(() => ShowTemplateStoreLink);
        }

        private void OnTemplateListViewChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            RefreshEmptyStages();
        }

        private void OnTemplateListSourceFilter(object sender, FilterEventArgs e)
        {
            if (e.Item is not TemplateViewModel templateViewModel)
            {
                return;
            }

            bool isMatchingSearch = true;
            if (!string.IsNullOrWhiteSpace(SearchText))
            {
                var searchTerms = SearchText.Trim().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                isMatchingSearch = searchTerms.All(
                    term =>
                        templateViewModel.DisplayName?.Contains(term, StringComparison.CurrentCultureIgnoreCase) == true
                        || templateViewModel.SearchTags?.Contains(term, StringComparison.CurrentCultureIgnoreCase)
                        == true);
            }

            bool isMatchingViewKind = SelectedTemplateViewKind.Equals(templateViewModel.TemplateViewKind);
            e.Accepted = isMatchingViewKind && isMatchingSearch;
        }

        public void DeleteCustomTemplate(TemplateViewModel templateViewModel)
        {
            var delete = _messageService.ShowMessageWindow(
                new MessageViewModel
                {
                    Title = "Remove this custom template from your profile",
                    Message = "Remove this template from your list of templates. You can re-add the template any time later if needed.",
                    IsShowCancelButton = false,
                    IsShowNoButton = true,
                    YesButtonText = "Yes",
                    NoButtonText = "No",
                },
                this);

            if (delete == true)
            {
                var templateId = templateViewModel.Template.Id;
                _ = Task.Run(() => _templateStoreManager?.DeleteUserTemplateOnServer(3, templateId).FireAndForgetSafeAsync(this.LogError));
                _sharedTemplateRepository.DeleteUserTemplate(templateId, User.Id);
                Templates.Remove(templateViewModel);
                TemplateListView.Refresh();
                DeleteCustomTemplateFromRecentlyUsedTemplates(templateViewModel.Template);
            }
        }

        private void DeleteCustomTemplateFromRecentlyUsedTemplates(Template template)
        {
            var recentTemplateViewModels = Templates.Where(t => t.Template != null && t.TemplateViewKind.Equals(RecentlyUsedTemplates) && t.Template.Id == template.Id).ToArray();
            foreach (var recentTemplateViewModel in recentTemplateViewModels)
            {
                _recentTemplateRepository.DeleteCustomTemplate(User.Id, recentTemplateViewModel.Template.Id);
                RemoveSelectedTemplateForTemplateView(recentTemplateViewModel, RecentlyUsedTemplates);
            }
        }

        private void RemoveSelectedTemplateForTemplateView(TemplateViewModel templateViewModel, TemplateViewKind templateViewKind)
        {
            Templates.Remove(templateViewModel);
            var index = TemplateViewKinds.IndexOf(templateViewKind);
            var selectedRecentTemplate = TemplateViewKinds[index].SelectedTemplate;
            if (selectedRecentTemplate == templateViewModel)
            {
                TemplateViewKinds[index].SelectedTemplate = null;
            }

            TemplateListView.Refresh();
        }

        private void RefreshEmptyStages()
        {
            RaisePropertyChanged(() => ShowCustomTemplates);
            RaisePropertyChanged(() => ShowRecentTemplates);
            RaisePropertyChanged(() => ShowTemplateStoreLink);
            RaisePropertyChanged(() => ShowEmptyStageForCustomTemplates);
            RaisePropertyChanged(() => ShowEmptyStageForRecentTemplates);
            RaisePropertyChanged(() => ShowEmptyStageForSearch);
        }
    }

    public class TemplateViewKind : ViewModelBase
    {
        public string DisplayName { get; set; }
        public TemplateViewModel SelectedTemplate { get; set; }
        public override string ToString()
        {
            return DisplayName;
        }

        public override bool Equals(object obj)
        {
            if (obj is TemplateViewKind other)
            {
                return other.DisplayName == this.DisplayName;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return DisplayName.GetHashCode();
        }
    }

    public class TemplateViewModel : ViewModelBase
    {
        public TemplateViewKind TemplateViewKind { get; set; }
        public string CategoryName { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public string SearchTags { get; set; }
        public BitmapImage PreviewImage1 { get; set; }
        public BitmapImage PreviewImage2 { get; set; }
        public bool IsCustomTemplate { get; set; }
        public string DisplayInfo { get; set; }
        public string PublishedTo { get; set; }
        public Guid Id { get; set; }
        public DocumentModel DocumentModel { get; set; }
        public Template Template { get; set; }
        public bool HasContextMenu { get; set; }
        public ContextMenuEntry[] ContextMenuEntries { get; set; }
        public TemplateChooserViewModel TemplateChooserViewModel { get; set; }
        public bool IsOfflineAvailable { get; set; }
        public OpenCustomTemplateFromLink OpenCustomTemplateFromLink { get; set; }
        public bool HasPreview2 { get; set; }

        public TemplateViewModel()
        {
        }

        public TemplateViewModel(
            TemplateChooserViewModel templateChooserViewModel,
            DocumentModel documentModel,
            TemplateViewKind templateViewKind)
        {
            TemplateChooserViewModel = templateChooserViewModel;
            Id = documentModel.Id;
            DocumentModel = documentModel;
            CategoryName = documentModel.DocumentCategory?.Name;
            DisplayName = documentModel.DisplayName;
            Description = documentModel.Description;
            SearchTags = documentModel.Keywords;
            DisplayInfo = "Eurolook Template";
            IsOfflineAvailable = true;
            if (documentModel.PreviewImage != null)
            {
                PreviewImage1 = new BitmapImage().InitAndFreeze(documentModel.PreviewImage);
            }

            TemplateViewKind = templateViewKind;
            HasContextMenu = false;
            ContextMenuEntries = null;
        }

        public TemplateViewModel(
            TemplateChooserViewModel templateChooserViewModel,
            Template template,
            TemplateViewKind templateViewKind,
            bool isTemplateManagerTemplate = false)
        {
            Template = template;
            TemplateChooserViewModel = templateChooserViewModel;
            Id = template.Id;
            DocumentModel = template.BaseDocumentModel;
            Template = template;
            CategoryName = "";
            DisplayName = template.Name;
            Description = template.Description;
            SearchTags = template.Tags;
            IsCustomTemplate = true;
            DisplayInfo = "<Owner>"; // template.Owners?.First()?.User?.Login;
            IsOfflineAvailable = true;

            if (isTemplateManagerTemplate)
            {
                PublishedTo = $"by {template.Tags}";
            }
            else if (template.Publications != null)
            {
                var entityNames = string.Join(", ", template.Publications
                    .Where(x => !x.Deleted)
                    .OrderBy(p => p.OrgaEntity.LogicalLevel != 0)
                    .ThenBy(p => p.OrgaEntity.Name)
                    .Select(x => x.OrgaEntity.Name)
                    .ToArray());

                if (!string.IsNullOrEmpty(entityNames))
                {
                    PublishedTo = $"Published to {entityNames}";
                }
                else
                {
                    PublishedTo = "Not published";
                }
            }

            if (!template.PreviewPage1.IsNullOrEmpty())
            {
                PreviewImage1 = new BitmapImage().InitAndFreeze(template.PreviewPage1);
            }

            if (!template.PreviewPage2.IsNullOrEmpty())
            {
                PreviewImage2 = new BitmapImage().InitAndFreeze(template.PreviewPage2);
                HasPreview2 = true;
            }

            if (template.PreviewPage1.IsNullOrEmpty() && template.PreviewPage2.IsNullOrEmpty())
            {
                PreviewImage1 = new BitmapImage(
                    new Uri(
                        "pack://application:,,,/Eurolook.WordAddIn;component/Graphics/nopreview.png",
                        UriKind.Absolute));
            }

            TemplateViewKind = templateViewKind;
            ContextMenuEntries = isTemplateManagerTemplate
                ? Array.Empty<ContextMenuEntry>()
                : new[]
                {
                    new ContextMenuEntry
                    {
                        IconPath = "/Graphics/heroicons-outline/black/trash.svg",
                        Title = "Remove Template",
                        Command = new RelayCommand(() => TemplateChooserViewModel.DeleteCustomTemplate(this)),
                    },
                };

            HasContextMenu = ContextMenuEntries.Length > 0;
        }

        public TemplateViewModel(
            TemplateChooserViewModel templateChooserViewModel,
            UserTemplate userTemplate,
            TemplateViewKind templateViewKind,
            bool isTemplateManagerTemplate)
            : this(templateChooserViewModel, userTemplate.Template, templateViewKind, isTemplateManagerTemplate)
        {
            IsOfflineAvailable = userTemplate.IsOfflineAvailable;
        }
    }

    public class SelectedTemplateChangedMessage
    {
        public TemplateViewModel Template { get; set; }
    }

    public class DesignTimeTemplateChooserViewModel : TemplateChooserViewModel, ICanLog
    {
        public DesignTimeTemplateChooserViewModel()
            : base(null, null, null, null, null, null, null, null, null, null, null, null)
        {
            Templates.AddRange(new[]
            {
                new TemplateViewModel() { TemplateViewKind = EurolookTemplates, CategoryName = "Category A", DisplayName = "Contract", Description = "Item Description. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua." },
                new TemplateViewModel() { TemplateViewKind = EurolookTemplates, CategoryName = "Category A", DisplayName = "Fax" },
                new TemplateViewModel() { TemplateViewKind = EurolookTemplates, CategoryName = "Category A", DisplayName = "Letter" },
                new TemplateViewModel() { TemplateViewKind = EurolookTemplates, CategoryName = "Category A", DisplayName = "Note" },
                new TemplateViewModel() { TemplateViewKind = EurolookTemplates, CategoryName = "Category A", DisplayName = "Notification" },
                new TemplateViewModel() { TemplateViewKind = EurolookTemplates, CategoryName = "Category B", DisplayName = "My Custom Template", PublishedTo = "Published to Sector 3.14", IsCustomTemplate = true },
            });
            SelectedTemplateViewIndex = 0;
            SelectedTemplate = Templates.First();
        }
    }
}
