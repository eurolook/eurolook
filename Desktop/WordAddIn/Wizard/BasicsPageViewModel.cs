﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Authors;
using Eurolook.AddIn.Common.Database;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data.Constants;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing.DocumentCreation;
using Eurolook.WordAddIn.TemplateStore;
using Eurolook.WordAddIn.ViewModels.Languages;
using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;

namespace Eurolook.WordAddIn.Wizard
{
    public class BasicsPageViewModel : WizardPageViewModelBase, ICanLog, ICreateDocumentWizardPage
    {
        private const string TemplateInfoText = "Eurolook Template";
        private const string TemplateManagerInfoText = "Template Manager Template";
        private const string SignerFixedDefaultName = "Predefined";
        private const string SignerFixedDefaultDescription = "Signatory data is taken from the template";

        private readonly ISettingsService _settingsService;
        private readonly IUserDataRepository _userDataRepository;
        private readonly ILanguageLoader _languageLoader;
        private readonly IAuthorRoleViewModelValidator _authorRoleViewModelValidator;
        private readonly ITemplateStoreManager _templateStoreManager;

        private TemplateChooserViewModel _templateChooserViewModel;
        private LanguageSelectionViewModel _languageSelectionViewModel;
        private AuthorRolesSelectionViewModel _authorRolesSelectionViewModel;
        private bool _isSignerFixed;
        private AuthorRolesSelectionViewModel _fixedSignerViewModel;
        private string _warning;
        private bool _isValid;
        private BricksChooserViewModel _bricksChooserViewModel;
        private string _templateInfo;
        private string _templateLink;
        private bool _isBrickChooserButtonVisible;
        private bool _isLightboxEnabled;

        public BasicsPageViewModel(
            ISettingsService settingsService,
            IUserDataRepository userDataRepository,
            ILanguageLoader languageLoader,
            Func<BricksChooserViewModel> bricksChooserViewModel,
            AuthorRolesSelectionViewModel authorRolesSelectionViewModel,
            IAuthorRoleViewModelValidator authorRoleViewModelValidator,
            Func<TemplateChooserViewModel> templateChooserViewModel,
            ITemplateStoreManager templateStoreManager = null)
        {
            _settingsService = settingsService;
            _userDataRepository = userDataRepository;
            _languageLoader = languageLoader;
            _bricksChooserViewModel = bricksChooserViewModel();
            _authorRolesSelectionViewModel = authorRolesSelectionViewModel;
            _authorRoleViewModelValidator = authorRoleViewModelValidator;
            _templateStoreManager = templateStoreManager;
            _templateChooserViewModel = templateChooserViewModel();
            _languageSelectionViewModel = new LanguageSelectionViewModel(
                LoadFavoriteLanguages,
                LoadAllLanguages,
                Validate);
            _authorRolesSelectionViewModel.AuthorSearchLightBoxViewModel.PropertyChanged +=
                OnAuthorSearchPropertyChanged;
            _authorRolesSelectionViewModel.AuthorPickerLightBoxViewModel.PropertyChanged +=
                OnAuthorPickerPropertyChanged;
            Messenger.Default.Register<AuthorRoleViewModelChangedMessage>(this, OnAuthorRoleViewModelChangedMessage);
        }

        public override string PageTitle => "Basics";

        public int DisplayOrder => 0;

        public User User { get; set; }

        public TemplateChooserViewModel TemplateChooserViewModel
        {
            get => _templateChooserViewModel;
            set => Set(() => TemplateChooserViewModel, ref _templateChooserViewModel, value);
        }

        public LanguageSelectionViewModel LanguageSelectionViewModel
        {
            get => _languageSelectionViewModel;
            set => Set(() => LanguageSelectionViewModel, ref _languageSelectionViewModel, value);
        }

        public AuthorRolesSelectionViewModel AuthorRolesSelectionViewModel
        {
            get => _authorRolesSelectionViewModel;
            set => Set(() => AuthorRolesSelectionViewModel, ref _authorRolesSelectionViewModel, value);
        }

        public BricksChooserViewModel BricksChooserViewModel
        {
            get => _bricksChooserViewModel;
            set => Set(() => BricksChooserViewModel, ref _bricksChooserViewModel, value);
        }

        public bool IsSignerFixed
        {
            get => _isSignerFixed;
            set => Set(() => IsSignerFixed, ref _isSignerFixed, value);
        }

        public AuthorRolesSelectionViewModel FixedSignerViewModel
        {
            get => _fixedSignerViewModel;
            set => Set(() => FixedSignerViewModel, ref _fixedSignerViewModel, value);
        }

        public bool IsBrickChooserButtonVisible
        {
            get => _isBrickChooserButtonVisible;
            set => Set(() => IsBrickChooserButtonVisible, ref _isBrickChooserButtonVisible, value);
        }

        public string Warning
        {
            get => _warning;
            set => Set(() => Warning, ref _warning, value);
        }

        public string TemplateInfo
        {
            get => _templateInfo;
            set => Set(() => TemplateInfo, ref _templateInfo, value);
        }

        public string TemplateLink
        {
            get => _templateLink;
            set
            {
                Set(() => TemplateLink, ref _templateLink, value);
                RaisePropertyChanged(() => IsTemplateLinkEnabled);
            }
        }

        public bool IsLightboxEnabled
        {
            get => _isLightboxEnabled;
            set => Set(() => IsLightboxEnabled, ref _isLightboxEnabled, value);
        }

        public bool IsTemplateLinkEnabled => !string.IsNullOrWhiteSpace(TemplateLink);

        public override bool IsValid => _isValid;

        public DocumentModelSettings DocumentModelSettings =>
            GetDocumentModelSettings(TemplateChooserViewModel.SelectedTemplate);

        public override async Task OnLoad()
        {
            IsLoaded = false;
            User = await _userDataRepository.GetUserIncludingAuthorsAndJobAssignmentsAsync();
            AuthorRolesSelectionViewModel.InitAuthorsAndJobs(AuthorAndAllJobs.CreateAll(User));
            LanguageSelectionViewModel.User = User;
            LanguageSelectionViewModel.IsFavoriteListEnabled = User.Settings.IsFavoriteLanguageListEnabled;
            TemplateChooserViewModel.User = User;
            TemplateChooserViewModel.PropertyChanged += OnTemplateChooserViewModelPropertyChanged;
            await TemplateChooserViewModel.LoadAsync();
            IsLoaded = true;
        }

        public override Task OnBeforeWizardShow()
        {
            IsLoaded = false;
            BricksChooserViewModel.IsVisible = false;
            TemplateChooserViewModel.SearchText = null;
            AuthorRolesSelectionViewModel?.AuthorSearchLightBoxViewModel?.CancelSearch();
            return Task.CompletedTask;
        }

        public override Task OnBeforeWizardClose()
        {
            if (_authorRolesSelectionViewModel.AuthorSearchLightBoxViewModel.IsSearchVisible)
            {
                _authorRolesSelectionViewModel.AuthorSearchLightBoxViewModel.CancelSearchCommand.Execute(null);
            }

            if (_authorRolesSelectionViewModel.AuthorPickerLightBoxViewModel.IsPickerVisible)
            {
                _authorRolesSelectionViewModel.AuthorPickerLightBoxViewModel.CancelPickingCommand.Execute(null);
            }

            return Task.CompletedTask;
        }

        public override Task OnShow()
        {
            WizardViewModel.WizardTitle = "Create Document";
            return base.OnShow();
        }

        public DocumentCreationInfo GetDocumentCreationInfo()
        {
            var authorRoleViewModels = AuthorRolesSelectionViewModel.AuthorRoleViewModels;
            if (authorRoleViewModels.Any(ar => ar.Author?.Author == null))
            {
                return null;
            }

            if (IsSignerFixed)
            {
                authorRoleViewModels.RemoveRange(
                    authorRoleViewModels.Where(vm => vm.AuthorRole.Id == CommonDataConstants.SignatoryAuthorRoleId));
            }

            var selectedTemplate = TemplateChooserViewModel.SelectedTemplate;
            var result = selectedTemplate.IsCustomTemplate
                ? new DocumentCreationInfo(selectedTemplate.Template)
                : new DocumentCreationInfo(selectedTemplate.DocumentModel);
            result.IsTemplateAvailableOffline = selectedTemplate.IsOfflineAvailable;
            result.AddToUserProfileUponDocumentCreation =
                selectedTemplate.OpenCustomTemplateFromLink?.AddToUserProfileUponDocumentCreation ?? false;
            result.SelectedTemplateViewKind =
                TemplateChooserViewModel.TemplateViewKinds.IndexOf(selectedTemplate.TemplateViewKind);
            result.User = User;
            result.Language = LanguageSelectionViewModel.SelectedLanguage.Model;
            result.AuthorRolesMappingList = authorRoleViewModels.Select(x => x.ToAuthorRoleMapping()).ToList();
            result.IsSignerFixed = IsSignerFixed;
            result.UserBrickChoices = _settingsService.IsInTestMode
                ? new Dictionary<Guid, bool>()
                : UserBrickChoicesFormatter.ToDictionary(BricksChooserViewModel.Bricks);
            return result;
        }

        private void OnAuthorSearchPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(_authorRolesSelectionViewModel.AuthorSearchLightBoxViewModel.IsSearchVisible))
            {
                // hide bottom bar when search appears
                WizardViewModel.ShowBottomBar =
                    !_authorRolesSelectionViewModel.AuthorSearchLightBoxViewModel.IsSearchVisible;
                IsLightboxEnabled = _authorRolesSelectionViewModel.AuthorSearchLightBoxViewModel.IsSearchVisible;
                if (!IsLightboxEnabled)
                {
                    // validate when search disappears
                    Validate();
                }
            }
        }

        private void OnAuthorPickerPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(_authorRolesSelectionViewModel.AuthorPickerLightBoxViewModel.IsPickerVisible))
            {
                // hide bottom bar when search appears
                WizardViewModel.ShowBottomBar =
                    !_authorRolesSelectionViewModel.AuthorPickerLightBoxViewModel.IsPickerVisible;
                IsLightboxEnabled = _authorRolesSelectionViewModel.AuthorPickerLightBoxViewModel.IsPickerVisible;
                if (!IsLightboxEnabled)
                {
                    // validate when search disappears
                    Validate();
                }
            }
        }

        private void OnTemplateChooserViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(TemplateChooserViewModel.SelectedTemplate))
            {
                var selectedTemplate = TemplateChooserViewModel.SelectedTemplate;
                if (selectedTemplate == null)
                {
                    Validate();
                    return;
                }

                BricksChooserViewModel.IsVisible = false;
                IsBrickChooserButtonVisible = !selectedTemplate.IsCustomTemplate
                                              && (selectedTemplate.DocumentModel?.HideBrickChooser != true);

                // load languages for the selected template
                LanguageSelectionViewModel.LoadLanguages(false);
                LanguageSelectionViewModel.InitDocumentLanguage();

                // load author roles for the selected template
                var authorRoleViewModels =
                    selectedTemplate.DocumentModel.AuthorRoles.Select(ar => new AuthorRoleViewModel(ar));
                var authorRoleSettings = LoadAuthorRoleSettings();
                AuthorRolesSelectionViewModel.InitAuthorRoles(authorRoleViewModels, authorRoleSettings);

                // fixed signers
                IsSignerFixed =
                    selectedTemplate.IsCustomTemplate && selectedTemplate.Template.IsSignerFixed &&
                    authorRoleViewModels.Any(vm => vm.AuthorRole.Id == CommonDataConstants.SignatoryAuthorRoleId);
                if (IsSignerFixed)
                {
                    AuthorRolesSelectionViewModel.HideSigners();
                    AddPredefinedSignatoryModel(
                        authorRoleViewModels.Where(vm => vm.AuthorRole.Id == CommonDataConstants.SignatoryAuthorRoleId)
                                            .FirstOrDefault(),
                        authorRoleSettings);
                }

                // Bricks
                if (!selectedTemplate.IsCustomTemplate)
                {
                    BricksChooserViewModel.Load(
                        selectedTemplate.DocumentModel,
                        DocumentModelSettings?.UserBrickChoices);
                    TemplateInfo = TemplateInfoText;
                    TemplateLink = null;
                }
                else if (_settingsService.IsStandaloneMode)
                {
                    TemplateInfo = TemplateManagerInfoText;
                    TemplateLink = null;
                }
                else
                {
                    LoadOwnerInfoFromStoreAsync(selectedTemplate).FireAndForgetSafeAsync(this.LogError);
                }

                Validate();

                if (WizardViewModel is CreateDocumentWizardViewModel createDocumentWizardViewModel)
                {
                    foreach (CreateDocumentWizardPageBase pageViewModel in createDocumentWizardViewModel.Pages.Where(
                                 x => x is CreateDocumentWizardPageBase))
                    {
                        pageViewModel.OnSelectedTemplateChanged(selectedTemplate);
                    }
                }
            }
        }

        [SuppressMessage("SonarQube", "S2259: Null pointers should not be dereferenced", Justification = "Null check afterwards")]
        private async Task LoadOwnerInfoFromStoreAsync(TemplateViewModel selectedTemplate)
        {
            TemplateInfo = null;
            var owners = await _templateStoreManager?.GetOwnersOfTemplate(selectedTemplate.Id, 3);
            var mainOwner = owners?.FirstOrDefault(x => x.IsMain) ?? owners?.FirstOrDefault();
            TemplateInfo = mainOwner?.DisplayName != null ? $"By {mainOwner.DisplayName}" : "Template Store Template";
            TemplateLink = mainOwner?.Email != null ? $"mailto:{mainOwner.Email}" : "";
        }

        private void OnAuthorRoleViewModelChangedMessage(AuthorRoleViewModelChangedMessage obj)
        {
            Validate();
        }

        private IEnumerable<Language> LoadAllLanguages()
        {
            if (TemplateChooserViewModel?.SelectedTemplate == null)
            {
                return Enumerable.Empty<Language>();
            }

            return TemplateChooserViewModel.SelectedTemplate.IsCustomTemplate
                ? _languageLoader.GetAllLanguages(TemplateChooserViewModel.SelectedTemplate.Template)
                : _languageLoader.GetAllLanguages(TemplateChooserViewModel.SelectedTemplate.DocumentModel);
        }

        private IEnumerable<Language> LoadFavoriteLanguages()
        {
            if (TemplateChooserViewModel?.SelectedTemplate == null)
            {
                return Enumerable.Empty<Language>();
            }

            return TemplateChooserViewModel.SelectedTemplate.IsCustomTemplate
                ? _languageLoader.GetFavoriteLanguages(
                    TemplateChooserViewModel.SelectedTemplate.Template,
                    User?.Settings)
                : _languageLoader.GetFavoriteLanguages(
                    TemplateChooserViewModel.SelectedTemplate.DocumentModel,
                    User?.Settings);
        }

        private List<AuthorRoleSetting> LoadAuthorRoleSettings()
        {
            try
            {
                var lastUsedAuthorRoles = DocumentModelSettings?.LastUsedAuthorRoles;
                if (lastUsedAuthorRoles != null)
                {
                    return JsonConvert.DeserializeObject<List<AuthorRoleSetting>>(lastUsedAuthorRoles);
                }

                return LoadLastAuthorFromPreviousSettings();
            }
            catch (Exception ex)
            {
                this.LogWarn(ex);
            }

            return new List<AuthorRoleSetting>();
        }

        private List<AuthorRoleSetting> LoadLastAuthorFromPreviousSettings()
        {
            var lastAuthorId = User.Settings.LastAuthorId ?? User.SelfId;
            var lastJobAssignmentId = User.Settings.LastJobAssignmentId;
            return TemplateChooserViewModel.SelectedTemplate.DocumentModel?.AuthorRoles
                                           .Select(
                                               dmar => new AuthorRoleSetting
                                               {
                                                   AuthorRoleId = dmar.AuthorRoleId,
                                                   AuthorId = lastAuthorId,
                                                   JobAssignmentId = lastJobAssignmentId
                                               })
                                           .ToList();
        }

        private void AddPredefinedSignatoryModel(
            AuthorRoleViewModel authorRoleViewModel,
            List<AuthorRoleSetting> authorRoleSettings)
        {
            var authorsAndJobsVm = new AuthorsAndJobsViewModel(_settingsService);
            var authorRolesSelectionVm = new AuthorRolesSelectionViewModel(authorsAndJobsVm, true);

            var authorVm = new AuthorViewModel(SignerFixedDefaultName, false, true);
            var jobAssignmentVm = new JobAssignmentViewModel { DisplayName = SignerFixedDefaultDescription };

            var authorAndJob = new AuthorAndAllJobs(authorVm, jobAssignmentVm);

            authorRolesSelectionVm.InitAuthorsAndJobs(new List<AuthorAndAllJobs> { authorAndJob });
            authorRolesSelectionVm.InitAuthorRoles(
                new List<AuthorRoleViewModel> { authorRoleViewModel },
                authorRoleSettings);

            authorRolesSelectionVm.AuthorRoleViewModels.ForEach(vm => vm.AuthorAndAllJobs = authorAndJob);

            FixedSignerViewModel = authorRolesSelectionVm;
        }

        private DocumentModelSettings GetDocumentModelSettings(TemplateViewModel templateViewModel)
        {
            return templateViewModel.IsCustomTemplate
                ? User?.Settings?.GetSettingsForTemplate(templateViewModel.Id)
                : User?.Settings?.GetSettingsForDocumentModel(templateViewModel.Id);
        }

        private void Validate()
        {
            try
            {
                Warning = "";
                _authorRoleViewModelValidator.Validate(
                    AuthorRolesSelectionViewModel.AuthorRoleViewModels.Cast<AuthorRoleViewModel>().ToList());
                _isValid = (TemplateChooserViewModel?.SelectedTemplate != null)
                           && (LanguageSelectionViewModel.SelectedLanguage != null)
                           && !_authorRolesSelectionViewModel.AuthorSearchLightBoxViewModel.IsSearchVisible;
            }
            catch (EurolookValidationException ex)
            {
                if (!_authorRolesSelectionViewModel.AuthorSearchLightBoxViewModel.IsSearchVisible)
                {
                    Warning = ex.Message;
                }

                _isValid = false;
            }

            WizardViewModel.RefreshState();
        }
    }

    public class DesignTimeBasicsPageViewModel : BasicsPageViewModel
    {
        public DesignTimeBasicsPageViewModel()
            : base(null, null, null, null, null, null, null)
        {
            TemplateChooserViewModel = new DesignTimeTemplateChooserViewModel();
            TemplateInfo = "Eurolook Template";
        }
    }
}
