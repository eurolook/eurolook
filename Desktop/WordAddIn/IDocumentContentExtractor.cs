﻿using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.DocumentMetadata.CalculationCommands
{
    public interface IDocumentContentExtractor
    {
        ContentControl GetContentControlByNamePrefix(Document document, string contentControlNameStart);

        string ExtractTextFromContentControl(
            Document document,
            string titleContentControlNameStart,
            string excludeStyleName = null);

        string ExtractTextFromContentControlWithoutPlaceholderText(
            Document document,
            string titleContentControlNameStart,
            string excludeStyleName = null);

        string ExtractTextFromFirstMatchingParagraph(Document document, string paragraphStyleName);

        string ExtractTextFromFirstMatchingParagraph(ContentControl contentControl, string paragraphStyleName);

        string ExtractTextFromFirstMatchingParagraph(Range range, string paragraphStyleName);
    }
}
