﻿using System.Threading.Tasks;

namespace Eurolook.WordAddIn
{
    public interface ISessionContext
    {
        bool IsSessionActive { get; }
        Task SetSessionActive();
    }
}
