﻿using Eurolook.AddIn.Common.ProfileInitializer;
using GalaSoft.MvvmLight.Command;

namespace Eurolook.WordAddIn.ViewModels
{
    public class InitNotificationViewModel : NotificationViewModel
    {
        private readonly IProfileInitializer _profileInitializer;

        public InitNotificationViewModel(IProfileInitializer profileInitializer)
        {
            NotificationText =
                "You just opened a Eurolook document. Eurolook was recently installed or updated. "
                + "To use all features of Eurolook, it needs to be initialised first. It will download updates and set up your profile.";
            DefaultButtonText = "Initialise";
            DefaultCommand = new RelayCommand(InitializeEurolook);
            _profileInitializer = profileInitializer;
        }

        public async void InitializeEurolook()
        {
            // The db could be initialized after the notification bar was shown (e.g. via taskpane).
            // So check again.
            if (await _profileInitializer.InitializeProfileIfNecessary())
            {
                NotificationBar?.Close();
            }
        }
    }
}
