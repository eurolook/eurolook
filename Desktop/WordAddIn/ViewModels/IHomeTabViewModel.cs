﻿using System.Collections.ObjectModel;
using Eurolook.AddIn.Common.ViewModels;

namespace Eurolook.WordAddIn.ViewModels
{
    public interface IHomeTabViewModel
    {
        ObservableCollection<InfoTileViewModel> Tiles { get; }
    }
}
