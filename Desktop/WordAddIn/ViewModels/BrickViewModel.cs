﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Media.Imaging;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using GalaSoft.MvvmLight.CommandWpf;

namespace Eurolook.WordAddIn.ViewModels
{
    public class BrickViewModel : TileViewModel, ICanLog
    {
        private readonly bool _isSingleBrickInGroup;
        private RelayCommand _insertBrickCommand;

        public BrickViewModel(Brick brick, IEurolookWordDocument eurolookDocument, bool isSingleBrickInGroup)
            : base(brick.HotKey)
        {
            BrickId = brick.Id;
            EurolookDocument = eurolookDocument;
            _isSingleBrickInGroup = isSingleBrickInGroup;
            Brick = brick;
            Title = _isSingleBrickInGroup ? Brick.Group.DisplayName : brick.DisplayName;
            Description = brick.Description;
            Acronym = _isSingleBrickInGroup ? Brick.Group.Acronym : brick.Acronym;
            ColorLuminance = brick.ColorLuminance;
            AutomationId = brick.Id.ToString("B");
            StyleName = "TileButton";
            if (Description != null)
            {
                DescriptionNoBreak = Description.Replace("\n", " ");
            }

            if (brick is CommandBrick commandBrick)
            {
                ShowDisclosureIndicator = commandBrick.HasCustomUi;
            }

            if (HeroIcon.IsSvgFile(brick.VectorIcon))
            {
                HeroIconSource = HeroIcon.CreateImageFromBytes(brick.VectorIcon);
            }
            else if (brick.Icon != null && brick.Icon.Length > 0)
            {
                Icon = new BitmapImage().InitAndFreeze(brick.Icon);
            }
        }

        public IEurolookWordDocument EurolookDocument { get; }

        public string StyleName { get; set; }

        public Style Style => TryFindStyle(StyleName);

        public Brick Brick { get; }

        public override int UIPositionIndex =>
            _isSingleBrickInGroup ? Brick.Group.UiPositionIndex : Brick.UiPositionIndex;

        public RelayCommand InsertBrickCommand => _insertBrickCommand ??= new RelayCommand(InsertBrick);

        public string DescriptionNoBreak { get; }

        public override string ToString()
        {
            return Brick.DisplayName;
        }

        [SuppressMessage("SonarQube", "S3168:async methods should not return void", Justification = "Method is a RelayCommand Action")]
        private async void InsertBrick()
        {
            try
            {
                await EurolookDocument.InsertBrick(Brick, true);
            }
            catch (Exception ex)
            {
                this.LogError(
                    Brick != null ? $"InsertBrick failed: {Brick.Id} - {Brick.DisplayName}." : "InsertBrick failed.",
                    ex);
            }
        }

        private Style TryFindStyle(string styleName)
        {
            var resource = Application.Current.TryFindResource(styleName);
            return resource as Style;
        }
    }
}
