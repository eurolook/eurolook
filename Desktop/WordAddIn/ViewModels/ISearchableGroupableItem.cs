using System;

namespace Eurolook.WordAddIn.ViewModels
{
    public interface ISearchableGroupableItem
    {
        Guid Id { get;  }

        string Name { get; }

        string DisplayName { get; }

        string Description { get; }

        string Keywords { get; }

        string GroupBy { get; }

        string SortBy { get; }

        string ThenBy { get; }
    }
}
