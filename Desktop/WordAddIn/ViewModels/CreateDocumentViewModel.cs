﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Authors;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.AddIn.Common.SystemConfiguration;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data;
using Eurolook.Data.Compatibility;
using Eurolook.Data.Constants;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.Data.Models.AuthorRoles;
using Eurolook.Data.Models.TemplateStore;
using Eurolook.DocumentProcessing.DocumentCreation;
using Eurolook.WordAddIn.Messages;
using Eurolook.WordAddIn.Options;
using Eurolook.WordAddIn.ViewModels.Languages;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using JetBrains.Annotations;
using Newtonsoft.Json;

namespace Eurolook.WordAddIn.ViewModels
{
    public class CreateDocumentViewModel : ViewModelBase, ICanLog, ISearchableGroupableCollection
    {
        private const string FilterTooltipOn = "Some document types are currently hidden, click to show them.";
        private const string FilterTooltipOff = "Hidden document types are currently visible, click to hide them.";

        private readonly ITemplateRepository _templateRepository;
        private readonly ISharedTemplateRepository _sharedTemplateRepository;
        private readonly IUserDataRepository _userDataRepository;
        private readonly IDocumentModelRepository _documentModelRepository;
        private readonly ILanguageLoader _languageLoader;
        private readonly IAuthorRoleViewModelValidator _authorRoleViewModelValidator;
        private readonly IDocumentModelVersionCompatibilityTester _documentModelVersionCompatibilityTester;
        private readonly IBrickVersionCompatibilityTester _brickVersionCompatibilityTester;
        private readonly ISystemConfigurationService _systemConfigurationService;
        private readonly IMessageService _messageService;
        private readonly Func<OptionsViewModel> _optionsViewModelFunc;
        private readonly ISettingsService _settingsService;

        private bool _isValid;
        private bool _useTileBasedPicker;
        private DocumentType _selectedDocumentType;
        private string _warning;
        private bool _isLoaded;
        private bool _isRequestButtonVisible;
        private bool _isCreating;
        private string _title;
        private string _helpRef;

        private Template _templateOpenedFromLink;
        private int _selectedTabIndex;
        private bool _isDocumentTypeFilterApplied;
        private UserTemplate[] _userTemplates;
        private string _documentTypeFilterIcon;
        private DocumentType[] _allDocumentTypes;
        private DocumentType[] _hiddenDocumentTypes;
        private string _documentTypeFilterTooltip;
        private bool _signerFixed;
        private bool _isDocumentFilterVisible;

        [UsedImplicitly]
        public CreateDocumentViewModel(
            ITemplateRepository templateRepository,
            ISharedTemplateRepository sharedTemplateRepository,
            IUserDataRepository userDataRepository,
            IDocumentModelRepository documentModelRepository,
            ISettingsService settingsService,
            ILanguageLoader languageLoader,
            IAuthorRoleViewModelValidator authorRoleViewModelValidator,
            IDocumentModelVersionCompatibilityTester documentModelVersionCompatibilityTester,
            IBrickVersionCompatibilityTester brickVersionCompatibilityTester,
            ISystemConfigurationService systemConfigurationService,
            IMessageService messageService,
            AuthorRolesSelectionViewModel authorRolesSelectionViewModel,
            Func<OptionsViewModel> optionsViewModelFunc)
        {
            _templateRepository = templateRepository;
            _sharedTemplateRepository = sharedTemplateRepository;
            _userDataRepository = userDataRepository;
            _documentModelRepository = documentModelRepository;
            _languageLoader = languageLoader;
            _authorRoleViewModelValidator = authorRoleViewModelValidator;
            _documentModelVersionCompatibilityTester = documentModelVersionCompatibilityTester;
            _brickVersionCompatibilityTester = brickVersionCompatibilityTester;
            _systemConfigurationService = systemConfigurationService;
            _messageService = messageService;
            _optionsViewModelFunc = optionsViewModelFunc;
            _settingsService = settingsService;

            IsLoaded = false;

            RestoreDefaultsCommand = new RelayCommand(RestoreDefaults);
            GetHelpCommand = new RelayCommand(() => _settingsService.OpenHelpLink(HelpReference));

            AuthorRolesSelectionViewModel = authorRolesSelectionViewModel;
            DocumentTypes = new ObservableCollectionEx<DocumentType>();
            UserBrickChoices = new ObservableCollection<UserBrickChoiceViewModel>();
            LanguageSelectionVm = new LanguageSelectionViewModel(LoadFavoriteLanguages, LoadAllLanguages, Validate);
            Messenger.Default.Register<AuthorRoleViewModelChangedMessage>(this, OnAuthorRoleViewModelChangedMessage);
        }

        public User User { get; set; }

        public string Title
        {
            get => _title;
            set => Set(() => Title, ref _title, value);
        }

        public string HelpReference
        {
            get => _helpRef;
            set => Set(() => HelpReference, ref _helpRef, value);
        }

        public bool IsDocumentTypeFilterApplied
        {
            get => _isDocumentTypeFilterApplied;
            set
            {
                Set(() => IsDocumentTypeFilterApplied, ref _isDocumentTypeFilterApplied, value);
                InitDocumentTypes(!value);
            }
        }

        public string DocumentTypeFilterIcon
        {
            get => _documentTypeFilterIcon;
            set => Set(() => DocumentTypeFilterIcon, ref _documentTypeFilterIcon, value);
        }

        public string DocumentTypeFilterTooltip
        {
            get => _documentTypeFilterTooltip;
            set => Set(() => DocumentTypeFilterTooltip, ref _documentTypeFilterTooltip, value);
        }

        public AuthorRolesSelectionViewModel AuthorRolesSelectionViewModel { get; set; }

        public bool UseTileBasedAuthorPicker
        {
            get => _useTileBasedPicker;
            set => Set(() => UseTileBasedAuthorPicker, ref _useTileBasedPicker, value);
        }

        public bool SignerFixed
        {
            get => _signerFixed;
            set => Set(() => SignerFixed, ref _signerFixed, value);
        }

        public ObservableCollectionEx<DocumentType> DocumentTypes { get; }

        public ObservableCollection<UserBrickChoiceViewModel> UserBrickChoices { get; }

        public RelayCommand RestoreDefaultsCommand { get; }

        public RelayCommand GetHelpCommand { get; }

        public RelayCommand ShowOptionsTab => new(
            () => { SelectedTabIndex = 1; },
            () => SelectedDocumentType?.IsTemplateFromTemplateStore != true);

        public RelayCommand ShowDocumentTypeOptionsCommand => new(ShowDocumentTypeOptions, () => true);

        public string ChangeDefaultsText =>
            SelectedDocumentType != null
                ? $"Change default content of {SelectedDocumentType.DisplayName}..."
                : "Change default content...";

        public string ChangeDefaultsToolTip => SelectedDocumentType != null
            ? $"Click to change the default content for {SelectedDocumentType.DisplayName}..."
            : "Click to change the default content of a document type.";

        public bool IsValid
        {
            get => _isValid;
            set
            {
                Set(() => IsValid, ref _isValid, value);
                RaisePropertyChanged(() => IsOkButtonEnabled);
            }
        }

        public LanguageSelectionViewModel LanguageSelectionVm { get; }

        public bool IsRequestButtonVisible
        {
            get => _isRequestButtonVisible;
            set => Set(() => IsRequestButtonVisible, ref _isRequestButtonVisible, value);
        }

        public bool IsOptionsButtonVisible => false;

        public int SelectedTabIndex
        {
            get => _selectedTabIndex;
            set
            {
                _selectedTabIndex = value;
                RaisePropertyChanged(() => SelectedTabIndex);
            }
        }

        public bool IsLoaded
        {
            get => _isLoaded;
            set
            {
                Set(() => IsLoaded, ref _isLoaded, value);
                RaisePropertyChanged(() => IsOkButtonEnabled);
            }
        }

        public bool IsCreating
        {
            get => _isCreating;
            set
            {
                Set(() => IsCreating, ref _isCreating, value);
                RaisePropertyChanged(() => IsOkButtonEnabled);
            }
        }

        public bool IsOkButtonEnabled => IsValid && IsLoaded && !IsCreating;

        public bool IsDocumentFilterVisible
        {
            get => _isDocumentFilterVisible;
            set => Set(() => IsDocumentFilterVisible, ref _isDocumentFilterVisible, value);
        }

        public DocumentType SelectedDocumentType
        {
            get => _selectedDocumentType;
            set
            {
                if (value == null)
                {
                    return;
                }

                Set(() => SelectedDocumentType, ref _selectedDocumentType, value);
                RaisePropertyChanged(() => SelectedItem);
                RaisePropertyChanged(() => PreviewImage);
                RaisePropertyChanged(() => ChangeDefaultsText);
                RaisePropertyChanged(() => ChangeDefaultsToolTip);

                if (SelectedDocumentType.IsTemplateFromTemplateStore)
                {
                    SelectedTabIndex = 0;
                    RaisePropertyChanged(() => ShowOptionsTab);
                }

                LanguageSelectionVm.LoadLanguages(false);
                LanguageSelectionVm.InitDocumentLanguage();

                LoadUserBrickChoices();

                var authorRoleViewModels =
                    SelectedDocumentType.DocumentModel.AuthorRoles.Select(ar => new AuthorRoleViewModel(ar));
                var authorRoleSettings = LoadAuthorRoleSettings();

                AuthorRolesSelectionViewModel.InitAuthorRoles(authorRoleViewModels, authorRoleSettings);
                SignerFixed = IsTemplateFromTemplateStoreAndSignerFixed();
                if (SignerFixed)
                {
                    AuthorRolesSelectionViewModel.HideSigners();
                }

                Validate();
            }
        }

        public BitmapImage PreviewImage
        {
            get
            {
                try
                {
                    if (SelectedDocumentType == null)
                    {
                        return null;
                    }

                    var preview = SelectedDocumentType.Template?.PreviewPage1
                                  ?? (SelectedDocumentType.DocumentModel.PreviewImage?.Length > 0
                                      ? SelectedDocumentType.DocumentModel.PreviewImage
                                      : null);

                    return preview == null ? null : new BitmapImage().InitAndFreeze(preview);
                }
                catch (Exception ex)
                {
                    this.LogError("Failed to display the preview image.", ex);
                }

                return null;
            }
        }

        public string Warning
        {
            get => _warning;
            set => Set(() => Warning, ref _warning, value);
        }

        public DocumentModelSettings DocumentModelSettings => GetDocumentModelSettings(SelectedDocumentType);

        public Func<Task> CreateDocumentFunc { get; set; }

        public Guid? TemplateIdOpenedFromLink { get; set; }

        public bool LoadTemplateStoreTemplates { get; set; } = true;

        public IEnumerable<ISearchableGroupableItem> Items => DocumentTypes;

        public ISearchableGroupableItem SelectedItem
        {
            get => SelectedDocumentType;
            set => SelectedDocumentType = value as DocumentType;
        }

        public async Task LoadAsync()
        {
            IsLoaded = false;

            // CAUTION: starting a Task and using await inside seems to be silly.
            // However the window is opened directly and the content is loaded afterwards.
            // When not using Task.Factory.StartNew we did not have this behavior.
            // The window was opened only after the async methods returned.
            await Task.Factory.StartNew(
                async () =>
                {
                    User = await _userDataRepository.GetUserIncludingAuthorsAndJobAssignmentsAsync();
                    LanguageSelectionVm.User = User;
                    LanguageSelectionVm.IsFavoriteListEnabled = User.Settings.IsFavoriteLanguageListEnabled;

                    var loadTemplateStoreTemplates =
                        LoadTemplateStoreTemplates &&
                        (await _systemConfigurationService.IsTemplateStoreEnabledForUser(User)
                         || _settingsService.IsStandaloneMode);

                    _userTemplates = loadTemplateStoreTemplates
                        ? await _sharedTemplateRepository.GetUserTemplatesAsync(User.Id)
                        : new UserTemplate[] { };

                    if (TemplateIdOpenedFromLink != null)
                    {
                        _templateOpenedFromLink =
                            await _templateRepository.GetTemplateAsync(TemplateIdOpenedFromLink.Value);
                    }

                    var documentModels = await _documentModelRepository
                        .GetVisibleDocumentModelsAsync(
                            _documentModelVersionCompatibilityTester,
                            _brickVersionCompatibilityTester);
                    _allDocumentTypes = GetDocumentTypes(documentModels);
                    _hiddenDocumentTypes = GetHiddenDocumentTypes(_allDocumentTypes);

                    UseTileBasedAuthorPicker = await _systemConfigurationService.IsCreationWizardEnabled();
                });

            Execute.OnUiThread(
                () =>
                {
                    IsDocumentTypeFilterApplied = _hiddenDocumentTypes.Length > 0; // calls InitDocumentTypes
                    IsDocumentFilterVisible = _hiddenDocumentTypes.Length > 0;
                    AuthorRolesSelectionViewModel.InitAuthorsAndJobs(AuthorAndAllJobs.CreateAll(User));

                    // preselect values
                    PreselectDocumentType(TemplateIdOpenedFromLink);

                    _templateOpenedFromLink = null;
                    TemplateIdOpenedFromLink = null;

                    IsLoaded = true;
                });
        }

        public void InitDocumentTypes(bool showHidden)
        {
            var documentTypes = _allDocumentTypes.ToList();
            if (!showHidden)
            {
                foreach (var dt in _hiddenDocumentTypes)
                {
                    documentTypes.Remove(dt);
                }
            }

            var isFilterOn = _hiddenDocumentTypes.Any() && !showHidden;
            DocumentTypeFilterIcon = isFilterOn
                ? "/Graphics/heroicons-outline/black/eye-slash.svg"
                : "/Graphics/heroicons-outline/black/eye.svg";
            DocumentTypeFilterTooltip = isFilterOn ? FilterTooltipOn : FilterTooltipOff;
            DocumentTypes.Clear();
            DocumentTypes.AddRange(documentTypes);
        }

        public DocumentCreationInfo GetDocumentCreationInfo()
        {
            var authorRoleViewModels = AuthorRolesSelectionViewModel.AuthorRoleViewModels;

            if (authorRoleViewModels.Any(ar => ar.Author?.Author == null))
            {
                return null;
            }

            if (SignerFixed)
            {
                authorRoleViewModels.RemoveRange(
                    authorRoleViewModels.Where(vm => vm.AuthorRole.Id == CommonDataConstants.SignatoryAuthorRoleId));
            }

            return new DocumentCreationInfo(SelectedDocumentType)
            {
                User = User,
                Language = LanguageSelectionVm.SelectedLanguage.Model,
                AuthorRolesMappingList = authorRoleViewModels.Select(x => x.ToAuthorRoleMapping()).ToList(),
                IsSignerFixed = SignerFixed,
                UserBrickChoices = UserBrickChoicesFormatter.ToDictionary(UserBrickChoices)
            };
        }

        public bool IsTemplateFromTemplateStoreAndSignerFixed()
        {
            return SelectedDocumentType.IsTemplateFromTemplateStore && SelectedDocumentType.Template.IsSignerFixed;
        }

        private void ShowDocumentTypeOptions()
        {
            var optionsViewModel = _optionsViewModelFunc();
            optionsViewModel.SelectedSubOptions = optionsViewModel.DocumentTypesOptions;
            var win = new OptionsWindow(optionsViewModel);
            if (_messageService.ShowDialog(win) == true)
            {
                LoadAsync().FireAndForgetSafeAsync(this.LogError);
            }
        }

        private DocumentModelSettings GetDocumentModelSettings(DocumentType documentType)
        {
            var settings = User?.Settings;

            return documentType.IsTemplateFromTemplateStore
                ? settings?.GetSettingsForTemplate(documentType.Template.Id)
                : settings?.GetSettingsForDocumentModel(documentType.DocumentModel.Id);
        }

        private DocumentType[] GetDocumentTypes(List<DocumentModel> documentModels)
        {
            foreach (var documentModel in documentModels.Where(dm => dm.DocumentCategory == null))
            {
                documentModel.DocumentCategory = new DocumentCategory { Name = "Others" };
            }

            var documentTypes = documentModels.Select(dm => new DocumentType(dm)).ToList();
            if (_templateOpenedFromLink != null)
            {
                documentTypes.Add(new DocumentType(_templateOpenedFromLink, true));
            }

            var isStandalone = _settingsService.IsStandaloneMode;
            documentTypes.AddRange(
                _userTemplates.Where(ut => ut.TemplateId != _templateOpenedFromLink?.Id)
                              .Select(ut => new DocumentType(ut.Template, ut.IsOfflineAvailable, isStandalone)));

            return documentTypes.ToArray();
        }

        private DocumentType[] GetHiddenDocumentTypes(DocumentType[] documentTypes)
        {
            return documentTypes.Where(dt => GetDocumentModelSettings(dt)?.IsHidden == true).ToArray();
        }

        private void OnAuthorRoleViewModelChangedMessage(AuthorRoleViewModelChangedMessage message)
        {
            Validate();
        }

        private void LoadUserBrickChoices()
        {
            if (SelectedDocumentType == null)
            {
                return;
            }

            if (SelectedDocumentType.IsTemplateFromTemplateStore)
            {
                UserBrickChoices.Clear();
                return;
            }

            var userBrickChoicesDict = UserBrickChoicesFormatter.ToDictionary(
                SelectedDocumentType.DocumentModel.BrickReferences,
                DocumentModelSettings?.UserBrickChoices);

            UserBrickChoices.Clear();
            AddUserBrickChoices(StoryType.Header, userBrickChoicesDict);
            AddUserBrickChoices(StoryType.Begin, userBrickChoicesDict);
            AddUserBrickChoices(StoryType.Body, userBrickChoicesDict);
            AddUserBrickChoices(StoryType.End, userBrickChoicesDict);
            AddUserBrickChoices(StoryType.Footer, userBrickChoicesDict);
        }

        private List<AuthorRoleSetting> LoadAuthorRoleSettings()
        {
            try
            {
                var lastUsedAuthorRoles = DocumentModelSettings?.LastUsedAuthorRoles;
                if (lastUsedAuthorRoles != null)
                {
                    return JsonConvert.DeserializeObject<List<AuthorRoleSetting>>(lastUsedAuthorRoles);
                }

                return LoadLastAuthorFromPreviousSettings();
            }
            catch (Exception ex)
            {
                this.LogWarn(ex);
            }

            return new List<AuthorRoleSetting>();
        }

        private List<AuthorRoleSetting> LoadLastAuthorFromPreviousSettings()
        {
            var lastAuthorId = User.Settings.LastAuthorId ?? User.SelfId;
            var lastJobAssignmentId = User.Settings.LastJobAssignmentId;
            return SelectedDocumentType.DocumentModel?.AuthorRoles
                                       .Select(
                                           dmar => new AuthorRoleSetting
                                           {
                                               AuthorRoleId = dmar.AuthorRoleId,
                                               AuthorId = lastAuthorId,
                                               JobAssignmentId = lastJobAssignmentId
                                           })
                                       .ToList();
        }

        private void AddUserBrickChoices(StoryType story, Dictionary<Guid, bool> userBricks)
        {
            // hidden bricks shall be displayed here to make the creation dialog more transparent
            var selectableDocumentStructures = SelectedDocumentType.DocumentModel.BrickReferences
                                                                   .Where(
                                                                       x =>
                                                                           !(x.Brick is CommandBrick)
                                                                           && EnumHelper.IsStory(x.Position, story)
                                                                           && (x.AutoBrickCreationType
                                                                               != AutoBrickCreationType
                                                                                   .SimpleBodyContent))
                                                                   .OrderBy(x => x.VerticalPositionIndex);

            foreach (var documentStructure in selectableDocumentStructures)
            {
                if (documentStructure.Brick.Group != null)
                {
                    // group already exists?
                    var groupVm = UserBrickChoices.FirstOrDefault(x => x.GroupId == documentStructure.Brick.GroupId);
                    if (groupVm == null)
                    {
                        // no? create it!
                        groupVm = new UserBrickChoiceViewModel(documentStructure.Brick.Group);
                        UserBrickChoices.Add(groupVm);
                    }

                    var brickVm = new UserBrickChoiceViewModel(documentStructure, userBricks, groupVm);
                    groupVm.Items.Add(brickVm);
                    if (brickVm.IsSelected)
                    {
                        groupVm.IsExpanded = true;
                    }
                }
                else
                {
                    UserBrickChoices.Add(new UserBrickChoiceViewModel(documentStructure, userBricks, null));
                }
            }
        }

        private void RestoreDefaults()
        {
            if (SelectedDocumentType == null)
            {
                return;
            }

            if (DocumentModelSettings == null)
            {
                return;
            }

            DocumentModelSettings.UserBrickChoices = null;
            LoadUserBrickChoices();

            _documentModelRepository.UpdateDocumentModelSettings(DocumentModelSettings);
        }

        private IEnumerable<Language> LoadAllLanguages()
        {
            return SelectedDocumentType.IsTemplateFromTemplateStore
                ? _languageLoader.GetAllLanguages(SelectedDocumentType.Template)
                : _languageLoader.GetAllLanguages(SelectedDocumentType.DocumentModel);
        }

        private IEnumerable<Language> LoadFavoriteLanguages()
        {
            return SelectedDocumentType.IsTemplateFromTemplateStore
                ? _languageLoader.GetFavoriteLanguages(SelectedDocumentType.Template, User?.Settings)
                : _languageLoader.GetFavoriteLanguages(SelectedDocumentType.DocumentModel, User?.Settings);
        }

        private void Validate()
        {
            try
            {
                Warning = "";
                _authorRoleViewModelValidator.Validate(
                    AuthorRolesSelectionViewModel.AuthorRoleViewModels.Cast<AuthorRoleViewModel>().ToList());
                IsValid = (SelectedDocumentType != null) && (LanguageSelectionVm.SelectedLanguage != null);
            }
            catch (EurolookValidationException ex)
            {
                Warning = ex.Message;
                IsValid = false;
            }

            Messenger.Default.Send(new CreateDocumentViewModelValidatedMessage());
        }

        private void PreselectDocumentType(Guid? preselectedTemplateId = null)
        {
            SelectedDocumentType = DocumentTypes.FirstOrDefault(
                                       item => item.IsTemplateFromTemplateStore
                                               && (item.Template.Id == preselectedTemplateId))
                                   ?? DocumentTypes.FirstOrDefault(
                                       item => User.Settings.LastTemplateId.HasValue
                                               && (item.Template?.Id == User.Settings.LastTemplateId))
                                   ?? DocumentTypes.FirstOrDefault(
                                       item => item.DocumentModel.Id == User.Settings.LastDocumentModelId)
                                   ?? DocumentTypes.FirstOrDefault(
                                       item => item.DocumentModel.Id == CommonDataConstants.NoteId)
                                   ?? DocumentTypes.FirstOrDefault();
        }
    }

    internal class CreateDocumentDesignViewModel : CreateDocumentViewModel
    {
        public CreateDocumentDesignViewModel()
            : base(null, null, null, null, null, null, new AuthorRoleViewModelValidator(), null, null, null, null, null, null)
        {
            var authorsAndJobsViewModel = new AuthorsAndJobsViewModel(null);
            AuthorRolesSelectionViewModel =
                new AuthorRolesSelectionViewModel((a1, a2) => null, (a1, a2, a3) => null, authorsAndJobsViewModel);

            var authorViewModel = new AuthorViewModel(
                new Author
                {
                    LatinFirstName = "Hubert Blaine",
                    LatinLastName = "Wolfeschlegelsteinhausenbergerdorff Sr."
                });
            var jobAssignmentViewModel = new JobAssignmentViewModel
            {
                DisplayName = "Stellvetretender Generaldirektor"
            };

            authorsAndJobsViewModel.AuthorsAndJobs.Add(new AuthorAndJob(authorViewModel, jobAssignmentViewModel));

            AuthorRolesSelectionViewModel.AuthorRoleViewModels.Add(
                new AuthorRoleSelectionViewModel(new AuthorRole { Name = "Writer" })
                {
                    AuthorAndJob = authorsAndJobsViewModel.AuthorsAndJobs.First()
                });
            AuthorRolesSelectionViewModel.AuthorRoleViewModels.Add(
                new AuthorRoleSelectionViewModel(new AuthorRole { Name = "Signer" }));
            AuthorRolesSelectionViewModel.AuthorRoleViewModels.Add(
                new AuthorRoleSelectionViewModel(new AuthorRole { Name = "Secretary" }));
            AuthorRolesSelectionViewModel.AuthorRoleViewModels.Add(
                new AuthorRoleSelectionViewModel(new AuthorRole { Name = "Signer2" }));
            Warning = "Select a person for each author role.";
        }
    }
}
