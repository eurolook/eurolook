﻿using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.AddIn.Common.Views;
using Eurolook.Data.Constants;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.BrickCommands.CustomBricks;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.Database;
using Eurolook.WordAddIn.Messages;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace Eurolook.WordAddIn.ViewModels
{
    public class CustomBrickViewModel : BrickViewModel
    {
        private readonly ISettingsService _settingsService;
        private readonly IMessageService _messageService;
        private readonly IWordBrickRepository _brickRepository;
        private readonly IWordApplicationContext _applicationContext;

        public CustomBrickViewModel(
            Brick brick,
            ISettingsService settingsService,
            IMessageService messageService,
            IWordBrickRepository brickRepository,
            IEurolookWordDocument eurolookDocument,
            IWordApplicationContext applicationContext,
            bool isSingleBrickInGroup)
            : base(brick, eurolookDocument, isSingleBrickInGroup)
        {
            _settingsService = settingsService;
            _messageService = messageService;
            _brickRepository = brickRepository;
            _applicationContext = applicationContext;
            if (Brick.Id == CommonDataConstants.CreateCustomBrickId)
            {
                // patch the Style of the "Create Custom Brick" button
                StyleName = "CreateCustomBrickButton";
            }
            else
            {
                string description = !string.IsNullOrEmpty(Description) ? $"{Description}\n\n" : "";
                Description = $"{description}Right-click to Edit or Delete this brick.";
                HasContextMenu = true;
                ContextMenuTitle = Title;
                ContextMenuEntries.AddRange(new[]
                {
                    new ContextMenuEntry
                    {
                        IconPath = "/Graphics/heroicons-outline/black/trash.svg",
                        Title = "Delete Custom Brick",
                        Command = new RelayCommand(DeleteCustomBrick),
                    },
                    new ContextMenuEntry
                    {
                        IconPath = "/Graphics/heroicons-outline/black/pencil.svg",
                        Title = "Edit Custom Brick",
                        Command = new RelayCommand(EditCustomBrick),
                    },
                });
            }
        }

        private void EditCustomBrick()
        {
            if (Brick is BuildingBlockBrick buildingBlockBrick)
            {
                var viewModel =
                    new CreateCustomBrickViewModel(
                        _applicationContext,
                        _settingsService,
                        _messageService,
                        EurolookDocument,
                        true)
                    {
                        Acronym = buildingBlockBrick.Acronym,
                        Name = buildingBlockBrick.Name,
                        Description = buildingBlockBrick.Description,
                        PreviewBytes = buildingBlockBrick?.PreviewImageBytes,
                    };
                viewModel.Tile.ColorBrush = ColorBrush;
                viewModel.UpdateAction = () => Update(EurolookDocument, viewModel);
                _messageService.ShowSingleton(new CreateCustomBrickWindow(viewModel), false);
            }
        }

        private void Update(IEurolookWordDocument eurolookDocument, CreateCustomBrickViewModel viewModel)
        {
            var buildingBlockBytes = BuildingBlockBrickInstance.CreateBuildingBlock(
                eurolookDocument.Document.Application,
                eurolookDocument.DocumentModel.Template,
                viewModel.Content);

            _brickRepository.UpdateUserBrick(Brick.Id, viewModel, buildingBlockBytes);
            eurolookDocument.BrickEngine.RemoveCachedBrickInstance(Brick.Id);
            Messenger.Default.Send(new CustomBricksModifiedMessage());
        }

        private void DeleteCustomBrick()
        {
            if (Brick is BuildingBlockBrick buildingBlockBrick)
            {
                var messageWindow = new MessageWindow(
                    new MessageViewModel
                    {
                        Title = "Delete Custom Brick",
                        Message = $"Do you really want to delete '{Title}'?",
                        YesButtonText = "Yes",
                        NoButtonText = "No",
                        IsShowCancelButton = false,
                        IsShowNoButton = true,
                    });
                if (messageWindow.ShowDialog() == true)
                {
                    _brickRepository.DeleteUserBrick(buildingBlockBrick);
                    Messenger.Default.Send(new CustomBricksModifiedMessage());
                }
            }
        }
    }
}
