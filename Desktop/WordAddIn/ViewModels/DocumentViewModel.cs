﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Windows.Input;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.AddIn;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.DocumentMetadata;
using Eurolook.AddIn.Common.ProfileInitializer;
using Eurolook.AddIn.Common.SystemResourceTracer;
using Eurolook.Common.Log;
using Eurolook.Data.ActionLogs;
using Eurolook.Data.Constants;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.Data.TermStore;
using Eurolook.Data.Xml;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.DocumentProcessing.OpenXml;
using Eurolook.WordAddIn.BrickCommands;
using Eurolook.WordAddIn.BrickCommands.InsertCrossReference;
using Eurolook.WordAddIn.BrickCommands.InsertTable;
using Eurolook.WordAddIn.BrickCommands.Placeholders.Date;
using Eurolook.WordAddIn.BrickCommands.Placeholders.Linked;
using Eurolook.WordAddIn.BrickCommands.Placeholders.Text;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.Compatibility;
using Eurolook.WordAddIn.ContentBrickActions.BrickToolbar;
using Eurolook.WordAddIn.Events;
using Eurolook.WordAddIn.Extensions;
using Eurolook.WordAddIn.Messages;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Language = Eurolook.Data.Models.Language;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.ViewModels
{
    [UsedImplicitly]
    public class DocumentViewModel : BaseViewModel, IDisposable, IEurolookWordDocument
    {
        private readonly object _lock = new object();

        private readonly IAddinContext _addinContext;
        private readonly IEurolookDataRepository _eurolookDataRepository;
        private readonly IDocumentModelRepository _documentModelRepository;
        private readonly ILanguageRepository _languageRepository;
        private readonly IBrickRepository _brickRepository;
        private readonly IStatisticsRepository _statisticsRepository;
        private readonly ISessionContext _sessionContext;
        private readonly IWindowSelectionChangeEventHelper _windowSelectionChangeHelper;
        private readonly IStylesExtractor _stylesExtractor;
        private readonly IEnumerable<IDocumentBeforeSaveHandler> _documentBeforeSaveHandlers;
        private readonly Func<ChangeBrickAuthorCommand> _changeBrickAuthorCommandFunc;
        private readonly Func<IEurolookWordDocument, IBrickExecutionContext> _brickExecutionContextFunc;
        private readonly Func<Language, ILocalisedResourceResolver> _localisedResourceResolverFunc;
        private readonly Func<ICommandBrickResolver> _commandBrickResolverCreateFunc;
        private readonly ITermStoreCache _termStoreCache;
        private readonly IAuthorManager _authorManager;
        private readonly ISystemResourceTracer _systemResourceTracer;
        private readonly IBrickToolbarResolver _brickToolbarResolver;
        private readonly Func<IEurolookWordDocument, InsertLinkedPlaceholderViewModel>
            _insertLinkedPlaceholderViewModelFunc;
        private readonly Func<IEurolookWordDocument, InsertDatePlaceholderViewModel>
            _insertDatePlaceholderViewModelFunc;
        private readonly IContentBrickActionsResolver _contentBrickActionsResolver;

        private DocumentModel _documentModel;
        private DocumentModelLanguage _documentModelLanguage;
        private List<DocumentStructure> _documentStructures;
        private Language _language;

        private bool _isValid;
        private bool _isActive;
        private ContentControlNavigator _contentControlNavigator;
        private ContentControl _selectedBrick;

        private InsertCrossReferenceViewModel _insertCrossReferenceViewModel;
        private InsertTableViewModel _insertTableViewModel;
        private InsertTextPlaceholderViewModel _insertTextPlaceholderViewModel;
        private InsertDatePlaceholderViewModel _insertDatePlaceholderViewModel;
        private InsertLinkedPlaceholderViewModel _insertLinkedPlaceholderViewModel;
        private List<UserBrick> _userBricks;

        public DocumentViewModel(
            Document document,
            IAddinContext addinContext,
            IWordApplicationContext applicationContext,
            ISessionContext sessionContext,
            IProfileInitializer profileInitializer,
            IMessageService messageService,
            ICompatibilityModeManager compatibilityModeManager,
            IEurolookDataRepository eurolookDataRepository,
            IDocumentModelRepository documentModelRepository,
            ILanguageRepository languageRepository,
            IBrickRepository brickRepository,
            IStatisticsRepository statisticsRepository,
            ISettingsService settingsService,
            IWindowSelectionChangeEventHelper windowSelectionChangeEventHelper,
            IStylesExtractor stylesExtractor,
            IEnumerable<IDocumentBeforeSaveHandler> documentBeforeSaveHandlers,
            Func<ChangeBrickAuthorCommand> changeBrickAuthorCommandFunc,
            Func<IEurolookWordDocument, IBrickExecutionContext> brickExecutionContextFunc,
            Func<Language, ILocalisedResourceResolver> localisedResourceResolverFunc,
            Func<ICommandBrickResolver> commandBrickResolverCreateFunc,
            ITermStoreCache termStoreCache,
            IAuthorManager authorManager,
            ISystemResourceTracer systemResourceTracer,
            IBrickToolbarResolver brickToolBarResolver,
            Func<IEurolookWordDocument, InsertLinkedPlaceholderViewModel> insertLinkedPlaceholderViewModelFunc,
            Func<IEurolookWordDocument, InsertDatePlaceholderViewModel> insertDatePlaceholderViewModelFunc,
            IContentBrickActionsResolver contentBrickActionsResolver)
            : base(applicationContext, messageService, settingsService)
        {
            _addinContext = addinContext;
            _sessionContext = sessionContext;
            _eurolookDataRepository = eurolookDataRepository;
            _documentModelRepository = documentModelRepository;
            _languageRepository = languageRepository;
            _brickRepository = brickRepository;
            _statisticsRepository = statisticsRepository;
            Document = document;
            _isValid = true;
            IsLegacyEurolookDocument = compatibilityModeManager.IsLegacyEurolookDocument(this);
            EurolookProperties = GetEurolookProperties();
            _windowSelectionChangeHelper = windowSelectionChangeEventHelper;
            _stylesExtractor = stylesExtractor;
            _documentBeforeSaveHandlers = documentBeforeSaveHandlers;
            _changeBrickAuthorCommandFunc = changeBrickAuthorCommandFunc;
            _brickExecutionContextFunc = brickExecutionContextFunc;
            _localisedResourceResolverFunc = localisedResourceResolverFunc;
            _commandBrickResolverCreateFunc = commandBrickResolverCreateFunc;
            _termStoreCache = termStoreCache;
            _authorManager = authorManager;
            _systemResourceTracer = systemResourceTracer;
            _brickToolbarResolver = brickToolBarResolver;
            _insertLinkedPlaceholderViewModelFunc = insertLinkedPlaceholderViewModelFunc;
            _insertDatePlaceholderViewModelFunc = insertDatePlaceholderViewModelFunc;
            _contentBrickActionsResolver = contentBrickActionsResolver;

            if (document.IsTemplate())
            {
                return;
            }

            if (!profileInitializer.IsDatabaseInitialized())
            {
                return;
            }

            if (IsLegacyEurolookDocument && !settingsService.IsStandaloneMode)
            {
                var compatibilityMode = compatibilityModeManager.CreateCompatibilityMode(this);
                if (compatibilityMode == null)
                {
                    return;
                }

                if (EurolookProperties == null)
                {
                    EurolookProperties = compatibilityMode.GetEurolookProperties();
                }

                _documentModel = compatibilityMode.DocumentModel;
            }

            DefaultTab = DocumentModel?.Id == CommonDataConstants.EL4DocumentModelId
                ? TaskPaneTabKind.Tools
                : TaskPaneTabKind.Bricks;

            if (!IsDocumentModelKnown)
            {
                // If the document originates from another environment and is not available in the local database
                // do not initialize the Eurolook Bricks tab etc.
                return;
            }

            Messenger.Default.Register<HotKeyMessage>(this, OnHotKey);

            InitContentControlToolbar();
            CreateBrickEngine();

            // Performance optimization: initialize style map ahead-of-time so we don't get hit
            // when we need to work with style names for the first time
            Task.Run(() => LanguageIndependentStyle.InitializeStyleNameMaps(document));

            _systemResourceTracer.TraceMemoryUsage();
        }

        ~DocumentViewModel()
        {
            Dispose(false);
        }

        public InsertCrossReferenceViewModel InsertCrossReferenceViewModel =>
            _insertCrossReferenceViewModel ??= new InsertCrossReferenceViewModel(this, ApplicationContext);

        public InsertTableViewModel InsertTableViewModel => _insertTableViewModel ??= new InsertTableViewModel(this, SettingsService);

        public InsertTextPlaceholderViewModel InsertTextPlaceholderViewModel => _insertTextPlaceholderViewModel ??= new InsertTextPlaceholderViewModel(SettingsService);

        public InsertDatePlaceholderViewModel InsertDatePlaceholderViewModel =>
            _insertDatePlaceholderViewModel ??= _insertDatePlaceholderViewModelFunc(this);

        public InsertLinkedPlaceholderViewModel InsertLinkedPlaceholderViewModel => _insertLinkedPlaceholderViewModel ??= _insertLinkedPlaceholderViewModelFunc(this);

        private BrickToolbarViewModel BrickToolbar { get; set; }

        public TaskPaneTabKind DefaultTab { get; }

        public Document Document { get; }

        public IOfficeDocumentWrapper OfficeDocumentWrapper => new WordDocumentWrapper(Document);

        public bool IsValid
        {
            get
            {
                // once this document has become invalid, it will always remain invalid
                return _isValid = _isValid && Document != null && ApplicationContext.IsObjectValid(Document);
            }
        }

        public bool IsLockedForEditing => Document?.IsLockedForEditing() == true;

        /// <summary>
        /// Gets or sets a value indicating whether the underlying document is in the foreground, i.e. whether it is the active document.
        /// </summary>
        public bool IsActive
        {
            get => _isActive;
            set { Set(() => IsActive, ref _isActive, value); }
        }

        public IReadOnlyEurolookProperties EurolookProperties { get; private set; }

        public bool IsNewlyCreated => EurolookProperties != null && !IsLegacyEurolookDocument && EurolookProperties.EditDate == null;

        public bool IsEurolookDocument
        {
            get
            {
                // Check whether the current document has been created with the current customization, i.e. documents created at the EC
                // shall only be detected as Eurolook documents when running the EC version of Eurolook.
                // For backward compatibility, an empty ProductCustomizationId is accepted as matching the current customization.
                string documentCustomizationId = EurolookProperties?.ProductCustomizationId;
                string applicationCustomizationId = _addinContext.ProductCustomizationId;
                bool doesCustomizationIdMatch = string.IsNullOrWhiteSpace(documentCustomizationId)
                                                || string.IsNullOrWhiteSpace(applicationCustomizationId)
                                                || Regex.IsMatch(documentCustomizationId, $"\\b{applicationCustomizationId}\\b");

                return doesCustomizationIdMatch
                       && EurolookProperties?.DocumentModelId != CommonDataConstants.StandardWordDocDocumentModel
                       && !IsLegacyEurolookDocument
                       && EurolookProperties?.DocumentModelId != null;
            }
        }

        public bool IsBasedOnCustomTemplate
        {
            get
            {
                return IsEurolookDocument
                       && EurolookProperties?.CustomTemplateId != null;
            }
        }

        public bool IsLegacyEurolookDocument { get; }

        public bool IsDocumentModelKnown => DocumentModel != null;

        public bool IsStandardEurolookDocument => DocumentModel?.Id == CommonDataConstants.StandardWordDocDocumentModel;

        public bool IsClassicDocument => DocumentModel?.IsClassicDocument == true;

        [CanBeNull]
        public DocumentModel DocumentModel
        {
            get
            {
                if (_documentModel == null && EurolookProperties?.DocumentModelId != null)
                {
                    _documentModel =
                        _documentModelRepository.GetDocumentModel(
                            EurolookProperties.DocumentModelId.GetValueOrDefault());
                }

                if (_documentModel == null)
                {
                    _documentModel = _documentModelRepository.GetDocumentModel(
                        CommonDataConstants.StandardWordDocDocumentModel);
                }

                return _documentModel;
            }
        }

        [CanBeNull]
        public DocumentModelLanguage DocumentModelLanguage
        {
            get
            {
                if (_documentModelLanguage == null && DocumentModel != null)
                {
                    _documentModelLanguage = _languageRepository.GetDocumentModelLanguage(_documentModel.Id, _language.Id);
                }

                return _documentModelLanguage;
            }
        }

        // TODO: Could be simplified as DocumentStructures seem already loaded into DocumentModel.BrickReferences
        public List<DocumentStructure> DocumentStructures =>
            _documentStructures ??= _eurolookDataRepository.GetDocumentStructures(DocumentModel);

        public bool CanHaveUserBricks => _documentStructures.Any(x => x.BrickId == CommonDataConstants.CreateCustomBrickId);

        public List<UserBrick> UserBricks
        {
            get
            {
                if (_userBricks == null && CanHaveUserBricks)
                {
                    _userBricks = _brickRepository.GetUserBricks(DocumentModel);
                }
                else if (_userBricks == null)
                {
                    _userBricks = new List<UserBrick>();
                }

                return _userBricks;
            }
            set
            {
                _userBricks = value;
            }
        }

        public Language Language
        {
            get => _language ?? (_language = _languageRepository.GetLanguage(EurolookProperties?.CreationLanguage)
                                             ?? _languageRepository.GetEnglishLanguage());
            set
            {
                _language = value;
                if (_language != null)
                {
                    SetEurolookProperties(prop => prop.CreationLanguage = _language.Name);
                }
            }
        }

        public IBrickEngine BrickEngine { get; private set; }

        private ContentControl SelectedBrick
        {
            get
            {
                lock (_lock)
                {
                    if (_selectedBrick != null && !ApplicationContext.IsObjectValid(_selectedBrick))
                    {
                        SetSelectedBrick(null);
                    }

                    return _selectedBrick;
                }
            }
            set
            {
                lock (_lock)
                {
                    if (_selectedBrick.IsSame(value) && BrickToolbar?.IsAttached == true)
                    {
                        return;
                    }

                    // NOTE: This check has been disabled as it may lead to Word crashes, see ECADOCS-258
                    // ignore bricks that are part of a deleted revision
                    ////if (value.IsInDeletedRevision())
                    ////{
                    ////    value = null;
                    ////}

                    SetSelectedBrick(value);
                }
            }
        }

        private void SetSelectedBrick(ContentControl value)
        {
            Set(() => SelectedBrick, ref _selectedBrick, value);

            if (BrickToolbar != null)
            {
                if (_selectedBrick != null)
                {
                    BrickToolbar.AttachTo(_selectedBrick, this);
                }
                else
                {
                    BrickToolbar.Detach();
                }
            }

            RaisePropertyChanged(() => IsBrickSelected);
            RaisePropertyChanged(() => HasSelectedBrickAuthorMapping);
        }

        private void CreateBrickEngine()
        {
            if (DocumentModel == null)
            {
                return;
            }

            var resourceResolver = _localisedResourceResolverFunc(Language);
            var resolver = _commandBrickResolverCreateFunc();

            BrickEngine = new BrickEngine.BrickEngine(
                DocumentStructures,
                resourceResolver,
                _termStoreCache,
                _stylesExtractor,
                DocumentModel,
                DocumentModelLanguage,
                Language,
                resolver,
                _authorManager,
                GetBrickExecutionContext,
                _contentBrickActionsResolver,
                _systemResourceTracer,
                _statisticsRepository,
                MessageService,
                SettingsService);
        }

        #region Commands

        public bool HasNormalSelection => ApplicationContext.Application.GetSelection()?.Type == WdSelectionType.wdSelectionNormal;

        public RelayCommand PreviousBrickCommand => new RelayCommand(PreviousBrick);

        public RelayCommand NextBrickCommand => new RelayCommand(NextBrick);

        public bool CanNavigate => _contentControlNavigator?.CanNavigate() == true;

        private void PreviousBrick()
        {
            if (!CanNavigate)
            {
                return;
            }

            try
            {
                _contentControlNavigator.SelectPreviousContentControl();
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private void NextBrick()
        {
            if (!CanNavigate)
            {
                return;
            }

            try
            {
                _contentControlNavigator.SelectNextContentControl();
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        public RelayCommand DeleteBrickCommand => new RelayCommand(DeleteBrick);

        [SuppressMessage("SonarQube", "S3168:async methods should not return void", Justification = "Method is a RelayCommand Action")]
        private async void DeleteBrick()
        {
            try
            {
                var contentBrickActions = _contentBrickActionsResolver.Resolve(SelectedBrick);
                await contentBrickActions.DeleteBrickAsync(GetBrickExecutionContext());

                CommandManager.InvalidateRequerySuggested();
                BrickToolbar?.Detach();
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        public bool HasSelectedBrickAuthorMapping => IsBrickSelected && _authorManager.GetAuthorXmlMappings(SelectedBrick).Any();

        public RelayCommand ChangeBrickAuthorCommand => new RelayCommand(ChangeBrickAuthor);

        [SuppressMessage("SonarQube", "S3168:async methods should not return void", Justification = "Method is a RelayCommand Action")]
        private async void ChangeBrickAuthor()
        {
            try
            {
                if (!HasSelectedBrickAuthorMapping)
                {
                    return;
                }

                using (var command = _changeBrickAuthorCommandFunc())
                {
                    await command.ExecuteAsync(GetBrickExecutionContext(), SelectedBrick);
                }

                await _statisticsRepository.LogToolCommandExecution(
                    DocumentModel,
                    new ToolCommandExecutionLog
                    {
                        BrickId = CommonDataConstants.ChangeBrickAuthorActionId,
                        Method = "Click",
                    });
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        public RelayCommand InsertParagraphBeforeContentControlCommand => new RelayCommand(InsertParagraphBeforeContentControl);

        public RelayCommand InsertParagraphAfterContentControlCommand => new RelayCommand(InsertParagraphAfterContentControl);

        public bool IsBrickSelected => SelectedBrick != null;

        private void InsertParagraphBeforeContentControl()
        {
            try
            {
                using (var documentAutomationHelper = new DocumentAutomationHelper(
                    GetBrickExecutionContext(),
                    "Insert Paragraph Before Brick"))
                {
                    var range = SelectedBrick.GetControlRange();
                    var newParagraphRange = range.InsertParagraphBeforeAndHandleLockedContentControls();

                    var style = LanguageIndependentStyle.FromNeutralStyleName("Normal", Document);
                    StyleConceptNew.ApplyStyleToRange(style, newParagraphRange);

                    newParagraphRange.Collapse();
                    documentAutomationHelper.SelectWhenDone(newParagraphRange);
                }
            }
            catch (COMException comEx) when (comEx.IsLockedOrReadonlyException())
            {
                // Ignoring
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private void InsertParagraphAfterContentControl()
        {
            try
            {
                using (var documentAutomationHelper = new DocumentAutomationHelper(
                    GetBrickExecutionContext(),
                    "Insert Paragraph After Brick"))
                {
                    var range = SelectedBrick.GetControlRange();
                    var newParagraphRange = range.InsertParagraphAfterAndHandleLockedContentControls();

                    var style = LanguageIndependentStyle.FromNeutralStyleName("Normal", Document);
                    StyleConceptNew.ApplyStyleToRange(style, newParagraphRange);

                    newParagraphRange.Collapse();
                    documentAutomationHelper.SelectWhenDone(newParagraphRange);
                }
            }
            catch (COMException comEx) when (comEx.IsLockedOrReadonlyException())
            {
                // Ignoring
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        public async Task ExecuteToolCommand(Brick brick)
        {
            this.LogInfoFormat("ExecuteToolCommand for '{0}'", brick.Name);

            await BrickEngine.ApplyBrickAsync(brick);
            await _statisticsRepository.LogToolCommandExecution(
                DocumentModel,
                new ToolCommandExecutionLog
                {
                    BrickId = brick.Id,
                    Method = "Click",
                });
            await _sessionContext.SetSessionActive();
        }

        public async Task InsertBrick(Brick brick, bool activateApplication)
        {
            this.LogInfoFormat("InsertBrick for '{0}'", brick.Name);

            _systemResourceTracer.TraceMemoryUsage();

            await BrickEngine.ApplyBrickWithCheckForDeletedRevisionsAsync(brick);

            var selection = ApplicationContext.Application.GetSelection();
            SelectedBrick = selection?.Range.FindBrickContentControl();

            if (activateApplication)
            {
                ApplicationContext.Application.ActiveWindow.SetFocus();
            }

            if (brick.Group != null)
            {
                // NOTE: Always close group after brick has been inserted, see EUROLOOK-589.
                //   If we do not want to close multiple-choice groups we can add the following condition
                //   to the if expression:  && !brick.Group.IsMultipleChoice
                Messenger.Default.Send(new LeaveBrickGroupPanelMessage { DocumentViewModel = this });
            }

            await _statisticsRepository.LogBrickInsertionAsync(
                DocumentModel,
                new BrickInsertionLog
                {
                    BrickId = brick.Id,
                    Method = "Click",
                });
            await _sessionContext.SetSessionActive();
        }

        #endregion

        private void InitContentControlToolbar()
        {
            if (DocumentModel == null)
            {
                return;
            }

            try
            {
                _contentControlNavigator = new ContentControlNavigator(Document);
                BrickToolbar = new BrickToolbarViewModel(GetBrickExecutionContext, _brickToolbarResolver);
                WireEventHandlers();
            }
            catch (COMException ex)
            {
                this.LogWarn("Could not initialize ContentControlToolbar.", ex);
            }
        }

        [SuppressMessage("SonarQube", "S3168:async methods should not return void", Justification = "Method is a Message handler")]
        private async void OnHotKey(HotKeyMessage message)
        {
            if (message.DocumentViewModel != this || (!message.IsGlobalHotKey && !IsEurolookDocument))
            {
                return;
            }

            if (DocumentStructures.Select(ds => ds.Brick)
                                  .OfType<CommandBrick>()
                                  .All(b => b.CommandClass.Split('.').Last() != message.CommandBrickType.Name))
            {
                // do not apply shortcut commands for commands that are not part of this document type
                return;
            }

            await BrickEngine.ApplyBrickAndLogInsertionAsync(message.CommandBrickType, message.Parameter, "HotKey");
        }

        #region Event Handlers

        private void DocumentBeforeSave(Document doc, ref bool saveasui, ref bool cancel)
        {
            try
            {
                AsyncHelper.EnsureSynchronizationContext();

                bool isInAutosave = doc.GetIsInAutosave();
                if (isInAutosave)
                {
                    this.LogDebug($"Autosaving '{doc.FullName}'.");
                    return;
                }

                // NOTE: DocumentBeforeSave is an application event in Word (and not an event on the current document,
                // therefore we must check that the event belongs to the current document.
                if (doc != Document)
                {
                    return;
                }

                // FIX: If the following conditions are met, Word will reset the binding of a content to empty text:
                //   - the user created a new document and filled a bound content control that previously was empty,
                //     e.g. the document title in a technical report
                //   - the selection is still within that content control (i.e. the binding has not been updated yet)
                //   - the user saves the document (after saving the control shows the placeholder again)
                doc.UpdateCustomXmlBindings();

                foreach (var documentBeforeSaveHandler in _documentBeforeSaveHandlers)
                {
                    var task = documentBeforeSaveHandler.OnBeforeSaveAsync(this, saveasui);
                    var canContinue = task.GetAwaiter().GetResult();
                    if (canContinue == false)
                    {
                        cancel = true;
                    }
                }

                this.LogDebug($"About to save {doc.FullName}.");
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private void OnWindowSelectionChangeDelayed(object sender, WindowSelectionChangeEventHelper.WindowSelectionChangeEventArgs windowSelectionChangeEventArgs)
        {
            try
            {
                AsyncHelper.EnsureSynchronizationContext();

                var selection = windowSelectionChangeEventArgs.Selection;
                if (DocumentAutomationHelper.IsActive || selection == null || selection.Document != Document)
                {
                    return;
                }

                SelectedBrick = selection.Range.FindBrickContentControl();
                RaisePropertyChanged(() => CanNavigate);
            }
            catch (COMException ex)
            {
                this.LogWarn(ex);
            }
        }

        private void ContentControlEntered(ContentControl contentControl)
        {
            try
            {
                AsyncHelper.EnsureSynchronizationContext();

                if (DocumentAutomationHelper.IsActive)
                {
                    return;
                }

                SelectedBrick = contentControl.GetBrickContentControl();

                // EUROLOOK-819: Special handling has been disabled
                ////if (contentControl.LockContents)
                ////{
                ////    contentControl.GetControlRange().Select();
                ////}
            }
            catch (COMException ex)
            {
                this.LogWarn(ex);
            }
        }

        private void ContentControlExited(ContentControl contentControl, ref bool cancel)
        {
            try
            {
                AsyncHelper.EnsureSynchronizationContext();

                if (DocumentAutomationHelper.IsActive)
                {
                    return;
                }

                var selection = ApplicationContext.Application.GetSelection();
                SelectedBrick = selection?.Range.FindBrickContentControl();
            }
            catch (COMException ex)
            {
                this.LogWarn(ex);
            }
        }

        private void ContentControlBeforeStoreUpdate(ContentControl contentControl, ref string content)
        {
            try
            {
                AsyncHelper.EnsureSynchronizationContext();

                // ECADOCS-379 Metadata: Date binding to custom EurolookProperty does not work in FR
                //    Due to a bug in Word the date value of date content controls is serialized as a formatted string
                //    instead of an xsd:date, even if the date storage format is set to use and xsd:date or xsd:datetime format.
                //    As a workaround, we intercept the binding of date controls and make sure that the date
                //    value is serialized in xsd:date format (yyyy-MM-ddThh:mm:ss).
                if (contentControl.Type == WdContentControlType.wdContentControlDate
                    && (contentControl.DateStorageFormat == WdContentControlDateStorageFormat.wdContentControlDateStorageDate
                        || contentControl.DateStorageFormat == WdContentControlDateStorageFormat.wdContentControlDateStorageDateTime)
                    && !DateTime.TryParseExact(content, "s", CultureInfo.InvariantCulture, DateTimeStyles.None, out _))
                {
                    var parser = new DateTimeParser(_languageRepository);
                    var parsedContent = parser.ParseDateTimeString(content);
                    if (parsedContent != null)
                    {
                        content = parsedContent.Value.ToString("s");
                    }
                }
            }
            catch (COMException ex)
            {
                this.LogError(ex);
            }
        }

        private void WireEventHandlers()
        {
            if (Document == null)
            {
                return;
            }

            _windowSelectionChangeHelper.WindowSelectionChangeDelayed += OnWindowSelectionChangeDelayed;
            Document.ContentControlOnEnter += ContentControlEntered;
            Document.ContentControlOnExit += ContentControlExited;
            Document.ContentControlBeforeStoreUpdate += ContentControlBeforeStoreUpdate;
            ApplicationContext.Application.DocumentBeforeSave += DocumentBeforeSave;
            ////((DocumentEvents_Event)Document).Close += DocumentClose;
        }

        private void DocumentClose()
        {
            ////AsyncHelper.EnsureSynchronizationContext();

            ////if (Document == null)
            ////    return;

            ////foreach (Window window in Document.Windows)
            ////{
            ////    MessageService.CloseNotificationBar(window);
            ////}

            ////UnwireEventHandlers();
        }

        private void UnwireEventHandlers()
        {
            if (Document == null)
            {
                return;
            }

            _windowSelectionChangeHelper.WindowSelectionChangeDelayed -= OnWindowSelectionChangeDelayed;
            try
            {
                Document.ContentControlOnEnter -= ContentControlEntered;
                Document.ContentControlOnExit -= ContentControlExited;
                Document.ContentControlBeforeStoreUpdate -= ContentControlBeforeStoreUpdate;
                ((DocumentEvents_Event)Document).Close -= DocumentClose;
                ApplicationContext.Application.DocumentBeforeSave -= DocumentBeforeSave;
            }
            catch (COMException ex)
            {
                this.LogWarn(ex);
            }
        }

        #endregion

        private IBrickExecutionContext GetBrickExecutionContext()
        {
            return _brickExecutionContextFunc(this);
        }

        [CanBeNull]
        public IEurolookProperties GetEurolookProperties()
        {
            // check whether we can access doc.CustomXMLParts
            string xml = Document.GetCustomXml(EurolookPropertiesCustomXml.RootName);
            return string.IsNullOrEmpty(xml) ? (IEurolookProperties)EurolookProperties : new EurolookPropertiesCustomXml(xml);
        }

        public void SetEurolookProperties(Action<IEurolookProperties> modifyAction)
        {
            // This should be the only place where the embedded custom XML is modified.
            lock (_lock)
            {
                // read current version from document
                var eurolookProperties = GetEurolookProperties();
                if (eurolookProperties == null)
                {
                    return;
                }

                // set new values to properties
                modifyAction(eurolookProperties);

                // update document
                Document.ReplaceCustomXml(EurolookPropertiesCustomXml.RootName, eurolookProperties.ToXDocument());
                EurolookProperties = eurolookProperties;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                UnwireEventHandlers();
            }

            BrickToolbar?.Dispose();
        }
    }
}
