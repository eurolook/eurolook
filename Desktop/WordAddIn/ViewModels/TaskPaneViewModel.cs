﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Messages;
using Eurolook.AddIn.Common.ProfileInitializer;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.Messages;
using Eurolook.WordAddIn.TaskPaneMessages;
using Eurolook.WordAddIn.Views;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using JetBrains.Annotations;
using NotNullAttribute = JetBrains.Annotations.NotNullAttribute;

namespace Eurolook.WordAddIn.ViewModels
{
    public enum TaskPaneTabKind
    {
        Home,
        Bricks,
        Tools,
    }

    [UsedImplicitly]
    public class TaskPaneViewModel : BaseViewModel, IDisposable
    {
        private readonly IEnumerable<ITaskPaneMessageProvider> _taskPaneMessageProviders;
        private readonly Func<AboutWindowViewModel> _aboutWindowViewModelCreateFunc;
        private readonly Lazy<IProfileInitializer> _profileInitializer;
        private readonly ScheduledActionWithDelay _scheduledActionWithDelay;

        private DocumentViewModel _documentViewModel;
        private TaskPaneTabKind _activeTab;
        private RelayCommand _aboutCommand;

        public TaskPaneViewModel(
            IWordApplicationContext applicationContext,
            IMessageService messageService,
            ISettingsService settingsService,
            Lazy<IProfileInitializer> profileInitializer,
            IEnumerable<ITaskPaneMessageProvider> taskPaneMessageProviders,
            IHomeTabViewModel homeTabViewModel,
            BricksTabViewModel bricksTabViewModel,
            ToolsTabViewModel toolsTabViewModel,
            Func<AboutWindowViewModel> aboutWindowViewModelCreateFunc)
            : base(applicationContext, messageService, settingsService)
        {
            _taskPaneMessageProviders = taskPaneMessageProviders;
            _aboutWindowViewModelCreateFunc = aboutWindowViewModelCreateFunc;
            _profileInitializer = profileInitializer;

            HomeTabViewModel = homeTabViewModel;
            BricksTabViewModel = bricksTabViewModel;
            ToolsTabViewModel = toolsTabViewModel;
            Messages = new ObservableCollection<TaskPaneMessageViewModel>();

            _scheduledActionWithDelay = new ScheduledActionWithDelay(applicationContext, UpdateHeartbeat);

            BricksTabViewModel.PropertyChanged += BricksTabViewModelOnPropertyChanged;

            ActiveTab = TaskPaneTabKind.Home;

            Messenger.Default.Register<ProfileCreationCompletedMessage>(this, OnProfileCreationCompleted);
            Messenger.Default.Register<LocalDataDeletedMessage>(this, OnLocalDataDeleted);
            Messenger.Default.Register<UpdateNotificationsMessage>(this, OnUpdateNotifications);
            Messenger.Default.Register<ShowCommandBrickPanelMessage>(this, OnShowCommandBrickPanel);
            Messenger.Default.Register<ShowBrickGroupPanelMessage>(this, OnShowBrickGroupPanel);
        }

        ~TaskPaneViewModel()
        {
            Dispose(false);
        }

        public DocumentViewModel DocumentViewModel
        {
            get => _documentViewModel;
            set
            {
                if (_documentViewModel == value)
                {
                    return;
                }

                _documentViewModel = value;

                if (_documentViewModel?.IsDocumentModelKnown == true)
                {
                    _scheduledActionWithDelay.RequestExecution();
                }
                else
                {
                    _scheduledActionWithDelay.StopScheduler();
                }

                BricksTabViewModel.DocumentViewModel = value;
                ToolsTabViewModel.DocumentViewModel = value;

                RaisePropertyChanged(() => DocumentViewModel);
                RaisePropertyChanged(() => ActiveTab);
                RaisePropertyChanged(() => IsEditEnabled);
                RaisePropertyChanged(() => IsBricksTabVisible);
                RaisePropertyChanged(() => IsToolsTabVisible);

                UpdateMessages();
            }
        }

        [NotNull]
        public IHomeTabViewModel HomeTabViewModel { get; }

        [NotNull]
        public BricksTabViewModel BricksTabViewModel { get; }

        [NotNull]
        public ToolsTabViewModel ToolsTabViewModel { get; }

        public bool IsEditEnabled => _documentViewModel != null
                                     && _documentViewModel.IsDocumentModelKnown
                                     && !_documentViewModel.IsClassicDocument
                                     && !_documentViewModel.IsLockedForEditing;

        public bool IsBricksTabVisible => BricksTabViewModel.BricksByCategory.Any();

        public bool IsToolsTabVisible => ToolsTabViewModel.Tiles.Any();

        private bool IsActiveTabVisible => (_activeTab == TaskPaneTabKind.Bricks && IsBricksTabVisible) ||
                                           (_activeTab == TaskPaneTabKind.Tools && IsToolsTabVisible);

        public ObservableCollection<TaskPaneMessageViewModel> Messages { get; set; }

        public TaskPaneTabKind ActiveTab
        {
            get => IsEditEnabled && IsActiveTabVisible ? _activeTab : TaskPaneTabKind.Home;
            set
            {
                Set(() => ActiveTab, ref _activeTab, value);

                if (ActiveTab == TaskPaneTabKind.Bricks)
                {
                    // close any open panel when the "Bricks" tab is clicked
                    BricksTabViewModel.LeaveBrickGroupPanel();
                    BricksTabViewModel.LeaveCommandBrickPanel();
                }
            }
        }

        public RelayCommand AboutCommand => _aboutCommand ??= new RelayCommand(ShowAbout);

        public bool TrySwitchingToDefaultTab()
        {
            if (IsEditEnabled && IsToolsTabVisible && DocumentViewModel.DefaultTab == TaskPaneTabKind.Tools)
            {
                    ActiveTab = TaskPaneTabKind.Tools;
                    return true;
            }

            if (IsEditEnabled && IsBricksTabVisible && DocumentViewModel.DefaultTab == TaskPaneTabKind.Bricks)
            {
                ActiveTab = TaskPaneTabKind.Bricks;
                return true;
            }

            return false;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        [SuppressMessage(
            "ReSharper",
            "UnusedParameter.Local",
            Justification = "This method signature is according to the IDisposable pattern.")]
        protected virtual void Dispose(bool disposing)
        {
            BricksTabViewModel.Dispose();
            ToolsTabViewModel.Dispose();

            _scheduledActionWithDelay?.Dispose();
        }

        private void OnShowBrickGroupPanel(ShowBrickGroupPanelMessage message)
        {
            if (message.EurolookDocument != DocumentViewModel)
            {
                return;
            }

            if (!message.GroupId.HasValue)
            {
                return;
            }

            var brickGroupVm = GetBrickGroupViewModel(message.GroupId.Value);
            if (brickGroupVm != null)
            {
                ActiveTab = TaskPaneTabKind.Bricks;
                UpdateBrickGroupState(brickGroupVm.Bricks);
                BricksTabViewModel.OpenGroup = brickGroupVm;
            }
        }

        private void UpdateBrickGroupState(IEnumerable<BrickViewModel> bricks)
        {
            var brickCheckmarkUpdater = new BrickCheckmarkUpdater();

            var brickContainers = DocumentViewModel.Document.GetAllBrickContainers().ToList();
            brickCheckmarkUpdater.UpdateCheckmark(DocumentViewModel, brickContainers, bricks);
        }

        private BrickGroupViewModel GetBrickGroupViewModel(Guid groupId)
        {
            foreach (var categoryVm in BricksTabViewModel.DocumentBrickCategories)
            {
                foreach (var tileVm in categoryVm.Bricks)
                {
                    if (tileVm is BrickGroupViewModel brickGroupVm && brickGroupVm.GroupId == groupId)
                    {
                        return brickGroupVm;
                    }
                }
            }

            return null;
        }

        private void UpdateMessages()
        {
            Execute.OnUiThread(() => Messages.Clear());
            if (!_profileInitializer.Value.IsDatabaseInitialized())
            {
                return;
            }

            foreach (var provider in _taskPaneMessageProviders)
            {
                foreach (var message in provider.GetMessages(_documentViewModel).Where(m => m != null))
                {
                    message.ParentTaskPaneViewModel = this;
                    Execute.OnUiThread(() => Messages.Insert(0, message));
                }
            }
        }

        private void OnProfileCreationCompleted(ProfileCreationCompletedMessage obj)
        {
            UpdateMessages();
        }

        private void OnUpdateNotifications(UpdateNotificationsMessage obj)
        {
            UpdateMessages();
        }

        private void OnLocalDataDeleted(LocalDataDeletedMessage obj)
        {
            Execute.OnUiThread(() => Messages?.Clear());
        }

        private void OnShowCommandBrickPanel(ShowCommandBrickPanelMessage message)
        {
            if (message.CommandBrickPanel.EurolookDocument == DocumentViewModel)
            {
                // do not use the ActiveTab setter here because the command brick should stay open
                _activeTab = TaskPaneTabKind.Bricks;
                RaisePropertyChanged(() => ActiveTab);
            }
        }

        private void BricksTabViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            RaisePropertyChanged();
        }

        private void ShowAbout()
        {
            var win = new AboutWindow(_aboutWindowViewModelCreateFunc());
            MessageService.ShowSingleton(win);
        }

        private void UpdateHeartbeat()
        {
            try
            {
                // Skip heartbeat if document is not active
                if (DocumentViewModel == null || !DocumentViewModel.IsActive)
                {
                    return;
                }

                UpdateState();
            }
            catch (Exception ex)
            {
                this.LogErrorFormat("Updating Bricks Tab failed.\n{0}", ex);
            }
        }

        private void UpdateState()
        {
            if (IsBricksTabVisible)
            {
                BricksTabViewModel.UpdateState();
            }

            RaisePropertyChanged(() => IsEditEnabled);
            RaisePropertyChanged(() => IsBricksTabVisible);
            RaisePropertyChanged(() => IsToolsTabVisible);
            RaisePropertyChanged(() => Messages);
        }
    }
}
