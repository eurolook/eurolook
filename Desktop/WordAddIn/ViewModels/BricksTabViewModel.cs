using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.AddIn.Common.Messages;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data;
using Eurolook.Data.Constants;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.OfficeNotificationBar;
using Eurolook.WordAddIn.Database;
using Eurolook.WordAddIn.Messages;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;

namespace Eurolook.WordAddIn.ViewModels
{
    public class BricksTabViewModel : BaseViewModel, IDisposable
    {
        private readonly IWordBrickRepository _brickRepository;
        private readonly IBrickVisibilityChecker _brickVisibilityChecker;
        private readonly ColorManager _colorManager;

        private RelayCommand _leaveBrickGroupPanelCommand;
        private ICommandBrickPanel _commandBrickPanel;
        private BrickGroupViewModel _openGroup;
        private DocumentViewModel _documentViewModel;
        private string _documentType;
        private bool _isUpdatingState;

        public BricksTabViewModel(
            IWordApplicationContext applicationContext,
            IMessageService messageService,
            ISettingsService settingsService,
            IWordBrickRepository brickRepository,
            IBrickVisibilityChecker brickVisibilityChecker,
            ColorManager colorManager)
            : base(applicationContext, messageService, settingsService)
        {
            Messenger.Default.Register<ShowCommandBrickPanelMessage>(this, OnShowCommandBrickPanel);
            Messenger.Default.Register<LeaveCommandBrickPanelMessage>(this, OnLeaveCommandBrickPanel);
            Messenger.Default.Register<LeaveBrickGroupPanelMessage>(this, OnLeaveBrickGroupPanel);
            Messenger.Default.Register<ColorSchemeUpdatedMessage>(this, OnColorSchemeUpdated);
            Messenger.Default.Register<ExpansionStateChangedMessage>(this, OnExpansionStateChanged);
            Messenger.Default.Register<CustomBricksModifiedMessage>(this, OnCustomBricksModified);

            _brickRepository = brickRepository;
            _brickVisibilityChecker = brickVisibilityChecker;
            _colorManager = colorManager;
        }

        ~BricksTabViewModel()
        {
            Dispose(false);
        }

        public DocumentViewModel DocumentViewModel
        {
            get => _documentViewModel;
            set
            {
                if (_documentViewModel == value)
                {
                    return;
                }

                _documentViewModel = value;

                HandleDocumentViewModelChangeAsync().FireAndForgetSafeAsync(this.LogError);
            }
        }

        public List<IGrouping<BrickCategory, Brick>> BricksByCategory { get; private set; } =
            new List<IGrouping<BrickCategory, Brick>>();

        public ObservableCollection<BrickCategoryViewModel> DocumentBrickCategories { get; } =
            new ObservableCollection<BrickCategoryViewModel>();

        public bool IsGroupPanelOpen => _openGroup != null;

        public bool IsCommandBrickPanelOpen => _commandBrickPanel != null;

        public ICommandBrickPanel CommandBrickPanel
        {
            get => _commandBrickPanel;
            set
            {
                _commandBrickPanel = value;
                RaisePropertyChanged(() => CommandBrickPanel);
                RaisePropertyChanged(() => IsCommandBrickPanelOpen);
            }
        }

        public BrickGroupViewModel OpenGroup
        {
            get => _openGroup;
            set
            {
                _openGroup = value;
                RaisePropertyChanged(() => OpenGroup);
                RaisePropertyChanged(() => IsGroupPanelOpen);
            }
        }

        public string DocumentType
        {
            get => _documentType;
            set { Set(() => DocumentType, ref _documentType, value); }
        }

        public RelayCommand LeaveBrickGroupPanelCommand =>
            _leaveBrickGroupPanelCommand ??= new RelayCommand(LeaveBrickGroupPanel);

        public void LeaveCommandBrickPanel()
        {
            CommandBrickPanel = null;
        }

        public void LeaveBrickGroupPanel()
        {
            OpenGroup = null;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void UpdateState()
        {
            if (_isUpdatingState)
            {
                return;
            }

            try
            {
                _isUpdatingState = true;
                Execute.OnUiThread(
                    () =>
                    {
                        CommandManager.InvalidateRequerySuggested();
                        UpdateBrickState();
                    });
            }
            catch (Exception ex)
            {
                this.LogErrorFormat("Updating Bricks Tab failed.\n{0}", ex);
            }
            finally
            {
                _isUpdatingState = false;
            }
        }

        public void UpdateCustomBricksCategory()
        {
            UpdateCustomBricksCategory(
                DocumentBrickCategories.FirstOrDefault(
                    x => x.Category.Id == CommonDataConstants.CustomBricksCategoryId));
        }

        public void UpdateCustomBricksCategory(BrickCategoryViewModel categoryViewModel)
        {
            if (categoryViewModel == null)
            {
                return;
            }

            // Remove all UserBricks
            foreach (var tileViewModel in categoryViewModel.Bricks.ToArray())
            {
                if (tileViewModel is BrickViewModel brickViewModel)
                {
                    if (brickViewModel.Brick is BuildingBlockBrick)
                    {
                        categoryViewModel.Bricks.Remove(tileViewModel);
                    }
                }
            }

            // reload UserBricks via database
            DocumentViewModel.UserBricks = _brickRepository.GetUserBricks(DocumentViewModel.DocumentModel);
            foreach (var tileViewModel in GetTileViewModels(DocumentViewModel.UserBricks.Select(x => x.Brick))
                                          .OrderBy(x => x.UIPositionIndex).ThenBy(x => x.Title).Reverse())
            {
                tileViewModel.ColorBrush = categoryViewModel.ColorBrush;
                categoryViewModel.Bricks.InsertAt(tileViewModel, 0);
            }
        }

        [SuppressMessage("ReSharper", "UnusedParameter.Local", Justification = "Method signature implements the IDisposable pattern.")]
        protected virtual void Dispose(bool disposing)
        {
            foreach (var brickCategoryViewModel in DocumentBrickCategories)
            {
                foreach (var tileViewModel in brickCategoryViewModel.Bricks)
                {
                    tileViewModel.Dispose();
                }
            }
        }

        private void OnCustomBricksModified(CustomBricksModifiedMessage message)
        {
            UpdateCustomBricksCategory();
        }

        private void OnShowCommandBrickPanel(ShowCommandBrickPanelMessage message)
        {
            if (message.CommandBrickPanel.EurolookDocument == DocumentViewModel)
            {
                if (IsCommandBrickPanelOpen && CommandBrickPanel != message.CommandBrickPanel)
                {
                    // panel is replaced with another one, e.g. because of a short cut being processed
                    // that means that the current panel needs to be cancelled
                    CommandBrickPanel.Cancel();
                }

                CommandBrickPanel = message.CommandBrickPanel;
            }
        }

        private void OnLeaveCommandBrickPanel(LeaveCommandBrickPanelMessage message)
        {
            if (message.CommandBrickPanel.EurolookDocument == DocumentViewModel && IsCommandBrickPanelOpen)
            {
                LeaveCommandBrickPanel();
                SetFocusToDocumentFrame();
            }
        }

        private void OnLeaveBrickGroupPanel(LeaveBrickGroupPanelMessage message)
        {
            if (message.DocumentViewModel == DocumentViewModel)
            {
                LeaveBrickGroupPanel();
            }
        }

        private void SetFocusToDocumentFrame()
        {
            if (DocumentViewModel == null || !DocumentViewModel.IsValid)
            {
                return;
            }

            // set focus to document
            var hWndWord = DocumentViewModel.Document.ActiveWindow.GetWordWindowHandle();
            var hWnd = WordWindowFinder.GetWordWwGHandle(hWndWord);
            SafeNativeMethods.SetFocus(hWnd);

            // Activate and deactivate Ribbon: a workaround to deactivate the CustomTaskPane
            // and activate the document so that keyboard input goes to the document
            // and that subsequently using the Shift+F6 shortcut still activates the
            // _Insert Name_ command (we currently have no better way than using SendKeys)
            SendKeys.SendWait("%");
            SendKeys.SendWait("%");
        }

        private async Task HandleDocumentViewModelChangeAsync()
        {
            if (_documentViewModel?.IsDocumentModelKnown == true)
            {
                DocumentType = _documentViewModel.DocumentModel?.DisplayName;
                await LoadBricksAsync();
            }
            else
            {
                BricksByCategory.Clear();
            }

            LeaveCommandBrickPanel();
            LeaveBrickGroupPanel();

            UpdateState();

            RaisePropertyChanged(() => DocumentViewModel);
            RaisePropertyChanged(() => DocumentViewModel.IsBrickSelected);
            RaisePropertyChanged(() => DocumentViewModel.CanNavigate);
            RaisePropertyChanged(() => DocumentViewModel.HasSelectedBrickAuthorMapping);
            RaisePropertyChanged(() => DocumentViewModel.HasNormalSelection);
        }

        private async Task LoadBricksAsync()
        {
            BricksByCategory = LoadBricksByCategory();
            var expansionStates = await SettingsService.LoadExpansionStateAsync();

            Execute.OnUiThread(
                () =>
                {
                    DocumentBrickCategories.Clear();
                    foreach (var categoryViewModel in GetBrickCategoryViewModels(BricksByCategory, expansionStates))
                    {
                        DocumentBrickCategories.Add(categoryViewModel);
                    }

                    UpdateColorScheme();
                });
        }

        private List<IGrouping<BrickCategory, Brick>> LoadBricksByCategory()
        {
            return DocumentViewModel.DocumentStructures
                                    .Select(ds => ds.Brick)
                                    .Where(
                                        b => _brickVisibilityChecker.IsBrickVisible(b)
                                             && b.CategoryId != CommonDataConstants.ToolsBrickCategoryId)
                                    .Distinct(new IdentifiableComparer<Brick>())
                                    .GroupBy(b => b.Category, new IdentifiableComparer<BrickCategory>())
                                    .OrderBy(g => g.Key.UiPositionIndex)
                                    .ToList();
        }

        private IEnumerable<BrickCategoryViewModel> GetBrickCategoryViewModels(
            IEnumerable<IGrouping<BrickCategory, Brick>> bricksByCategory,
            IReadOnlyDictionary<Guid, bool> expansionStates)
        {
            foreach (var bricksInCategory in bricksByCategory)
            {
                if (!bricksInCategory.Any())
                {
                    continue;
                }

                var category = bricksInCategory.Key;

                if (!expansionStates.TryGetValue(category.Id, out bool isExpanded))
                {
                    isExpanded = true;
                }

                var categoryViewModel = new BrickCategoryViewModel(category, isExpanded);
                if (categoryViewModel.Category.Id == CommonDataConstants.CustomBricksCategoryId)
                {
                    UpdateCustomBricksCategory(categoryViewModel);
                }

                foreach (var tileViewModel in GetTileViewModels(bricksInCategory)
                                              .OrderBy(x => x.UIPositionIndex).ThenBy(x => x.Title))
                {
                    categoryViewModel.Bricks.Add(tileViewModel);
                }

                yield return categoryViewModel;
            }
        }

        private IEnumerable<TileViewModel> GetTileViewModels(IEnumerable<Brick> bricksInCategory)
        {
            foreach (var bricksInBrickGroup in bricksInCategory.GroupBy(b => b.Group))
            {
                if (bricksInBrickGroup.Key != null && bricksInBrickGroup.Count() > 1)
                {
                    // multiple bricks grouped together
                    var brickGroup = bricksInBrickGroup.Key;
                    brickGroup.Bricks = bricksInBrickGroup.ToList();
                    yield return new BrickGroupViewModel(brickGroup, DocumentViewModel);
                }
                else
                {
                    // ECADOCS-301: Special handling if there is a single brick in a group:
                    // use the acronym and display name of the brick group instead of the brick's display name and acronym
                    bool isSingleBrickInGroup = bricksInBrickGroup.Key != null;

                    // bricks without group or a group with just one brick
                    foreach (var brick in bricksInBrickGroup)
                    {
                        if (brick.CategoryId == CommonDataConstants.CustomBricksCategoryId)
                        {
                            yield return new CustomBrickViewModel(
                                brick,
                                SettingsService,
                                MessageService,
                                _brickRepository,
                                DocumentViewModel,
                                ApplicationContext,
                                isSingleBrickInGroup);
                        }
                        else
                        {
                            yield return new BrickViewModel(brick, DocumentViewModel, isSingleBrickInGroup);
                        }
                    }
                }
            }
        }

        private void OnColorSchemeUpdated(ColorSchemeUpdatedMessage message)
        {
            Execute.OnUiThread(UpdateColorScheme);
        }

        [SuppressMessage("SonarQube", "S3168:async methods should not return void", Justification = "Method is an event handler")]
        private async void OnExpansionStateChanged(ExpansionStateChangedMessage message)
        {
            await SettingsService.SaveExpansionStateAsync(DocumentBrickCategories);
        }

        private void UpdateColorScheme()
        {
            int i = 1;
            foreach (var categoryVm in DocumentBrickCategories)
            {
                // Fixes EUROLOOK-3095 Rotate colors of categories
                if (i > 3)
                {
                    i = 1;
                }

                var categoryColor = _colorManager.GetColorFromScheme(_colorManager.CurrentColorScheme, i);
                categoryVm.ColorBrush = categoryColor.ToBrush();
                var luminanceValues = _colorManager.GetColorLuminancesFromScheme(_colorManager.CurrentColorScheme, i);
                foreach (var tileVm in categoryVm.Bricks)
                {
                    var tileColor = _colorManager.GetShade(categoryColor, luminanceValues, tileVm.ColorLuminance);
                    tileVm.ColorBrush = tileColor.ToBrush();

                    if (tileVm is BrickGroupViewModel brickGroupVm)
                    {
                        foreach (var innerBrickVm in brickGroupVm.Bricks)
                        {
                            var tileColor2 = _colorManager.GetShade(categoryColor, luminanceValues, innerBrickVm.ColorLuminance);
                            innerBrickVm.ColorBrush = tileColor2.ToBrush();
                        }
                    }
                }

                i++;
            }
        }

        private void UpdateBrickState()
        {
            if (DocumentViewModel == null || !ApplicationContext.IsObjectValid(DocumentViewModel.Document))
            {
                return;
            }

            try
            {
                var brickContainers = DocumentViewModel.Document.GetAllBrickContainers().ToList();
                var bricks = DocumentBrickCategories
                             .Where(categoryVm => categoryVm.IsExpanded)
                             .SelectMany(categoryVm => categoryVm.Bricks);

                var brickCheckmarkUpdater = new BrickCheckmarkUpdater();

                brickCheckmarkUpdater.UpdateCheckmark(DocumentViewModel, brickContainers, bricks);
            }
            catch (COMException)
            {
                // ignore COM exceptions
            }
        }
    }
}
