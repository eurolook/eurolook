using System;
using System.Windows.Media;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.Data.Models;

namespace Eurolook.WordAddIn.ViewModels
{
    public class ColorSchemeViewModel
    {
        public ColorSchemeViewModel()
        {
        }

        public ColorSchemeViewModel(ColorScheme colorScheme)
        {
            ColorScheme = colorScheme;
            Id = colorScheme.Id;
            DisplayName = colorScheme.Name;
            PrimaryColor = new SolidColorBrush(colorScheme.PrimaryColor.ToWpfColor());
            SecondaryColor = new SolidColorBrush(colorScheme.SecondaryColor.ToWpfColor());
            Tile1Color = new SolidColorBrush(colorScheme.TileColor1.ToWpfColor());
            Tile2Color = new SolidColorBrush(colorScheme.TileColor2.ToWpfColor());
            Tile3Color = new SolidColorBrush(colorScheme.TileColor3.ToWpfColor());
        }

        public string DisplayName { get; set; }

        public Guid? Id { get; set; }

        public ColorScheme ColorScheme { get; }

        public SolidColorBrush PrimaryColor { get; set; }

        public SolidColorBrush SecondaryColor { get; set; }

        public SolidColorBrush Tile1Color { get; set; }

        public SolidColorBrush Tile2Color { get; set; }

        public SolidColorBrush Tile3Color { get; set; }

        public override string ToString()
        {
            return DisplayName;
        }
    }
}
