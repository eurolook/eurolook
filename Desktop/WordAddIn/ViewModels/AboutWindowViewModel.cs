﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Text;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.ProfileInitializer;
using Eurolook.AddIn.Common.Properties;
using Eurolook.Data;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.ViewModels
{
    public class AboutWindowViewModel : ViewModelBase
    {
        private readonly Lazy<IProfileInitializer> _profileInitializer;
        private readonly Lazy<DataSyncScheduler> _dataSyncScheduler;

        public AboutWindowViewModel(
            DataSyncStatusViewModel dataSyncStatusViewModel,
            Lazy<IProfileInitializer> profileInitializer,
            Lazy<DataSyncScheduler> dataSyncScheduler,
            IConfigurationInfo configurationInfo,
            ISettingsService settingsService,
            IResourceManager resourceManager)
        {
            DataSyncStatusVm = dataSyncStatusViewModel;

            _profileInitializer = profileInitializer;
            _dataSyncScheduler = dataSyncScheduler;

            var assembly = typeof(AboutWindowViewModel).Assembly;
            var fileInfo = new FileInfo(assembly.Location);
            var fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
            ProductVersion = fileVersionInfo.ProductName;

            var clientBuildVersion = configurationInfo.ClientVersion;
            string productCustomizationInfo = GetProductCustomizationInfo(configurationInfo, settingsService);
            var buildDate = fileInfo.LastWriteTime;
            BuildVersion = string.IsNullOrWhiteSpace(productCustomizationInfo)
                ? $"{clientBuildVersion} {buildDate.ToShortDateString()}"
                : $"{clientBuildVersion} {buildDate.ToShortDateString()}\t({productCustomizationInfo})";

            IsStandaloneMode = settingsService?.IsStandaloneMode ?? false;

            DataSyncCommand = new RelayCommand(StartDataSync);
            PrivacyStatementUrl = settingsService?.GetOnlineHelpLink("PrivacyStatement");

            string privateStatementDisplayText =
                resourceManager.GetString("AboutWindowPrivacyStatementLinkDisplayText");
            IsPrivacyStatementVisible = !string.IsNullOrWhiteSpace(privateStatementDisplayText);
        }

        [UsedImplicitly]
        public string ProductVersion { get; }

        [UsedImplicitly]
        public string BuildVersion { get; }

        public DataSyncStatusViewModel DataSyncStatusVm { get; }

        public RelayCommand DataSyncCommand { get; }

        public bool IsStandaloneMode { get; set; }

        [UsedImplicitly]
        public string PrivacyStatementUrl { get; }

        [UsedImplicitly]
        public bool IsPrivacyStatementVisible { get; }

        private string GetProductCustomizationInfo(
            IConfigurationInfo configurationInfo,
            ISettingsService settingsService)
        {
            string customizationId = configurationInfo.ProductCustomizationId;
            var customizationInfo = new StringBuilder();
            if (!string.IsNullOrWhiteSpace(customizationId))
            {
                customizationInfo.Append(customizationId);
            }

            if (settingsService.IsStandaloneMode)
            {
                if (!string.IsNullOrWhiteSpace(customizationId))
                {
                    customizationInfo.Append(", ");
                }

                customizationInfo.Append("Standalone");
            }

            return customizationInfo.ToString();
        }

        [SuppressMessage("Sonar", "S3168:async methods should not return void", Justification = "Method acts an event handler")]
        private async void StartDataSync()
        {
            if (_profileInitializer.Value.IsDatabaseInitialized())
            {
                await _dataSyncScheduler.Value.RunDataSyncAsync(false);
            }
            else
            {
                await _profileInitializer.Value.InitializeProfileIfNecessary();
            }
        }
    }
}
