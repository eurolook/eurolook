﻿using System;
using System.Text;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.Messages;
using Eurolook.AddIn.Common.ProfileInitializer;
using Eurolook.Common.Log;
using Eurolook.DataSync;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;

namespace Eurolook.WordAddIn.ViewModels
{
    public class DataSyncStatusViewModel : ViewModelBase, ICanLog
    {
        private readonly Lazy<DataSyncScheduler> _dataSyncScheduler;
        private readonly Lazy<IProfileInitializer> _profileInitializer;
        private readonly IDatabaseStateRepository _databaseStateRepository;
        private bool _isOnline;
        private bool _isOffline;
        private bool _isRunning;
        private bool _isError;
        private string _statusText;

        public DataSyncStatusViewModel(
            Lazy<DataSyncScheduler> dataSyncScheduler,
            Lazy<IProfileInitializer> profileInitializer,
            IDatabaseStateRepository databaseStateRepository)
        {
            _dataSyncScheduler = dataSyncScheduler;
            _profileInitializer = profileInitializer;
            _databaseStateRepository = databaseStateRepository;
            IsOnline = true;

            Messenger.Default?.Register<DataSyncStartedMessage>(this, OnDataSyncStarted);
            Messenger.Default?.Register<DataSyncCompletedMessage>(this, OnDataSyncCompleted);

            Messenger.Default?.Register<LocalDataDeletedMessage>(this, OnLocalDataDeleted);
        }

        public bool IsOnline
        {
            get => _isOnline;
            set
            {
                Set(() => IsOnline, ref _isOnline, value);
                if (_isOnline)
                {
                    Set(() => IsOffline, ref _isOffline, false);
                    Set(() => IsRunning, ref _isRunning, false);
                }
            }
        }

        public bool IsOffline
        {
            get => _isOffline;
            set
            {
                Set(() => IsOffline, ref _isOffline, value);
                if (_isOffline)
                {
                    Set(() => IsOnline, ref _isOnline, false);
                    Set(() => IsRunning, ref _isRunning, false);
                }
            }
        }

        public bool IsRunning => DataSyncScheduler.IsRunning;

        public bool IsError
        {
            get => _isError;
            set { Set(() => IsError, ref _isError, value); }
        }

        public bool IsInitialised => ProfileInitializer.IsDatabaseInitialized();

        public string StatusText
        {
            get => _statusText ??= GetLastRunStatusText();
            private set { Set(() => StatusText, ref _statusText, value); }
        }

        private IProfileInitializer ProfileInitializer => _profileInitializer.Value;

        private DataSyncScheduler DataSyncScheduler => _dataSyncScheduler.Value;

        private static string GetNiceDate(DateTime dateTime)
        {
            int diffDays = (int)(DateTime.Today - dateTime.Date).TotalDays;
            switch (diffDays)
            {
                case 0:
                    return $"today at {dateTime:HH:mm}";
                case 1:
                    return $"yesterday at {dateTime:HH:mm}";
                default:
                    return $"{diffDays} days ago";
            }
        }

        private string GetLastRunStatusText()
        {
            if (IsRunning)
            {
                return "Synchronising ...";
            }

            var lastRunStatusResult = DataSyncScheduler.LastRun?.Result;
            if (lastRunStatusResult == DataSyncResult.RemoteWipe)
            {
                return "Your data will be refreshed as soon as you restart Word.";
            }

            if (lastRunStatusResult == DataSyncResult.NoConnection)
            {
                return "You are offline.";
            }

            if (lastRunStatusResult == DataSyncResult.LoginFailed)
            {
                return "Login failed.";
            }

            var lastUpdate = _databaseStateRepository.GetLastUpdate();
            if (lastUpdate == null || !IsInitialised)
            {
                return "Not initialised.";
            }

            var lastRunDate = _databaseStateRepository.GetLastDataSyncRunDate();
            if (lastRunDate == null)
            {
                return "Never synchronised before.";
            }

            var text = new StringBuilder();
            var lastSuccessfulRunDate = _databaseStateRepository.GetLastSuccessfulDataSyncDate();
            if (lastRunDate != lastSuccessfulRunDate)
            {
                text.Append("Synchronisation failed. ");
            }

            if (lastSuccessfulRunDate != null)
            {
                text.Append($"Last synchronised {GetNiceDate(lastSuccessfulRunDate.Value)}");
            }

            return text.ToString();
        }

        private void SetStatus()
        {
            try
            {
                IsError = false;
                IsOnline = true;
                StatusText = GetLastRunStatusText();
                if (!IsRunning)
                {
                    var lastRunStatusResult = DataSyncScheduler.LastRun?.Result;
                    if (lastRunStatusResult == DataSyncResult.NoConnection)
                    {
                        IsOffline = true;
                    }
                    else if (lastRunStatusResult != DataSyncResult.Success &&
                             lastRunStatusResult != DataSyncResult.RemoteWipe)
                    {
                        IsError = true;
                    }
                }

                RaisePropertyChanged(() => IsInitialised);
                RaisePropertyChanged(() => IsRunning);
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private void OnDataSyncStarted(DataSyncStartedMessage message)
        {
            SetStatus();
        }

        private void OnDataSyncCompleted(DataSyncCompletedMessage message)
        {
            SetStatus();
        }

        private void OnLocalDataDeleted(LocalDataDeletedMessage message)
        {
            SetStatus();
        }
    }
}
