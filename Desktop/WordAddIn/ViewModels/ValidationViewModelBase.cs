﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using GalaSoft.MvvmLight;

namespace Eurolook.WordAddIn.ViewModels
{
    public abstract class ValidationViewModelBase : ViewModelBase, IDataErrorInfo
    {
        protected const string ErrorIncomplete = "All fields with a red border are mandatory.";

        private string _error;
        private Dictionary<string, string> _errors;

        public string Error
        {
            get => _error;
            set => Set(() => Error, ref _error, value);
        }

        protected bool SubmitAttempted;
        protected Dictionary<string, string> Errors { get => _errors ??= new Dictionary<string, string>(); }
        protected abstract Dictionary<string, Action> ValidationMap { get; }

        public string this[string columnName] => ValidateProperty(columnName);

        public bool Validate(bool attemptSubmit = true)
        {
            if (attemptSubmit && !SubmitAttempted)
            {
                SubmitAttempted = true;
            }

            foreach (var property in ValidationMap.Keys)
            {
                _ = this[property];
                RaisePropertyChanged(property);
            }

            return Error == null;
        }

        protected string ValidateProperty(string propertyName)
        {
            if (SubmitAttempted == false)
            {
                return null;
            }

            if (ValidationMap != null && ValidationMap.ContainsKey(propertyName))
            {
                ValidationMap[propertyName]?.Invoke();
            }

            Error = Errors.Any() ? ErrorIncomplete : null;
            return Errors.ContainsKey(propertyName) ? ErrorIncomplete : null;
        }

        protected void ValidateNullOrWhiteSpace(string propertyName, string property)
        {
            if (string.IsNullOrWhiteSpace(property))
            {
                Errors[propertyName] = ErrorIncomplete;
            }
            else
            {
                Errors.Remove(propertyName);
            }
        }

        protected void ValidateNull(string propertyName, object property)
        {
            if (property == null)
            {
                Errors[propertyName] = ErrorIncomplete;
            }
            else
            {
                Errors.Remove(propertyName);
            }
        }
    }
}
