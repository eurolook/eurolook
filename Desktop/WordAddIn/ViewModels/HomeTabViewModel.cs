﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Authors;
using Eurolook.AddIn.Common.Email;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.AddIn.Common.Messages;
using Eurolook.AddIn.Common.ProfileInitializer;
using Eurolook.AddIn.Common.SystemConfiguration;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.DataSync;
using Eurolook.WordAddIn.DocumentCreation;
using Eurolook.WordAddIn.Options;
using Eurolook.WordAddIn.Views;
using Eurolook.WordAddIn.Wizard;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;

namespace Eurolook.WordAddIn.ViewModels
{
    public class HomeTabViewModel : BaseViewModel, IHomeTabViewModel
    {
        private readonly Lazy<IProfileInitializer> _profileInitializer;
        private readonly IEnumerable<IBeforeDocumentCreationTask> _beforeNewDocumentCreationTasks;
        private readonly IEnumerable<IBeforeDocumentCreationTask> _beforeNewDocumentFromTemplateStoreCreationTasks;
        private readonly IEnumerable<IAfterDocumentCreationTask> _afterNewDocumentCreationTasks;
        private readonly IEnumerable<IAfterDocumentCreationTask> _afterNewDocumentFromTemplateStoreCreationTasks;
        private readonly Func<CreateDocumentViewModel> _createDocumentViewModelFunc;
        private readonly Func<CreateDocumentWizardViewModel> _createDocumentWizardViewModelFunc;
        private readonly ISystemConfigurationService _systemConfigurationService;
        private readonly Lazy<IEmailService> _emailService;
        private readonly Func<AuthorManagerViewModel2> _authorManagerViewModel2Func;
        private readonly Func<OptionsViewModel> _optionsViewModelCreateFunc;
        private readonly ColorManager _colorManager;
        private readonly IDocumentCreationService _documentCreationService;

        private bool? _isInitialized;
        private ObservableCollection<InfoTileViewModel> _tiles;

        protected HomeTabViewModel(
            IWordApplicationContext applicationContext,
            IMessageService messageService,
            ISettingsService settingsService,
            Lazy<IProfileInitializer> profileInitializer,
            InitializationStatusViewModel initializationStatusVm,
            IEnumerable<IBeforeNewDocumentCreationTask> beforeNewDocumentCreationTasks,
            IEnumerable<IBeforeNewDocumentFromTemplateStoreCreationTask> beforeNewDocumentFromTemplateStoreCreationTasks,
            IEnumerable<IAfterNewDocumentCreationTask> afterNewDocumentCreationTasks,
            IEnumerable<IAfterNewDocumentFromTemplateStoreCreationTask> afterNewDocumentFromTemplateStoreCreationTasks,
            Func<CreateDocumentViewModel> createDocumentViewModelFunc,
            Func<CreateDocumentWizardViewModel> createDocumentWizardViewModelFunc,
            IDocumentCreationService documentCreationService,
            ISystemConfigurationService systemConfigurationService,
            Lazy<IEmailService> emailService,
            Func<AuthorManagerViewModel2> authorManagerViewModel2Func,
            Func<OptionsViewModel> optionsViewModelCreateFunc,
            ColorManager colorManager)
            : base(applicationContext, messageService, settingsService)
        {
            _profileInitializer = profileInitializer;
            _beforeNewDocumentCreationTasks = beforeNewDocumentCreationTasks;
            _beforeNewDocumentFromTemplateStoreCreationTasks = beforeNewDocumentFromTemplateStoreCreationTasks;
            _afterNewDocumentCreationTasks = afterNewDocumentCreationTasks;
            _afterNewDocumentFromTemplateStoreCreationTasks = afterNewDocumentFromTemplateStoreCreationTasks;
            _createDocumentViewModelFunc = createDocumentViewModelFunc;
            _createDocumentWizardViewModelFunc = createDocumentWizardViewModelFunc;
            _documentCreationService = documentCreationService;
            _systemConfigurationService = systemConfigurationService;
            _emailService = emailService;
            _authorManagerViewModel2Func = authorManagerViewModel2Func;
            _optionsViewModelCreateFunc = optionsViewModelCreateFunc;
            _colorManager = colorManager;

            InitializationStatusVm = initializationStatusVm;

            CreateDocumentPlaceholderTile = new InfoTileViewModel
            {
                HeroIconPath = "/Graphics/heroicons-outline/white/document.svg",
                Title = "Create Document",
                Description = "Create a new Eurolook document from scratch.",
                TileCommand = new InitializationRelayCommand(CreateDocument, () => false, _profileInitializer.Value),
                AutomationId = "CreateNewDocumentPlaceholder",
            };

            CreateDocumentTile = new InfoTileViewModel
            {
                HeroIconPath = "/Graphics/heroicons-outline/white/document.svg",
                Title = "Create Document",
                Description = "Create a new Eurolook document from scratch.",
                TileCommand = new InitializationRelayCommand(CreateDocument, IsInitialized, _profileInitializer.Value),
                AutomationId = "CreateNewDocument",
            };

            CreateDocumentWithWizardTile = new InfoTileViewModel
            {
                HeroIconPath = "/Graphics/heroicons-outline/white/document.svg",
                Title = "Create Document",
                Description = "Create a new Eurolook document from scratch.",
                TileCommand = new InitializationRelayCommand(CreateDocumentWithWizard, IsInitialized, _profileInitializer.Value),
                AutomationId = "CreateNewDocumentWizard",
            };

            ManageAuthorsTile = new InfoTileViewModel
            {
                HeroIconPath = "/Graphics/heroicons-outline/white/users.svg",
                Title = "Manage Authors",
                Description = "Edit your author information and add new authors to your profile.",
                TileCommand = new InitializationRelayCommand(ManageAuthors, IsInitialized, _profileInitializer.Value),
                AutomationId = "ManageAuthors2",
            };

            ChooseOptionsTile = new InfoTileViewModel
            {
                HeroIconPath = "/Graphics/heroicons-outline/white/cog-8-tooth.svg",
                Title = "Choose Options",
                Description = "Edit your personal Eurolook settings.",
                TileCommand =
                    new InitializationRelayCommand(ChooseOptions, IsInitialized, _profileInitializer.Value),
                AutomationId = "CustomizeEurolook",
            };

            SendFeedbackTile = new InfoTileViewModel
            {
                HeroIconPath = "/Graphics/heroicons-outline/white/chat-bubble-oval-left-ellipsis.svg",
                Title = "Send Feedback",
                Description = "Help to improve Eurolook, provide feedback.",
                TileCommand = new InitializationRelayCommand(SendFeedback, IsInitialized, _profileInitializer.Value),
                AutomationId = "SendFeedback",
            };

            OpenEurolookHelpTile = new InfoTileViewModel
            {
                HeroIconPath = "/Graphics/heroicons-outline/white/question-mark-circle.svg",
                Title = "Get Help",
                Description = "Open the Eurolook Online Help.",
                TileCommand = new RelayCommand(OpenEurolookHelp, CanOpenEurolookHelp),
                AutomationId = "OpenEurolookHelp",
            };

            WhatsNewTile = new InfoTileViewModel
            {
                HeroIconPath = "/Graphics/heroicons-outline/white/megaphone.svg",
                Title = "What's New?",
                Description = "Check out what is new in this version of Eurolook.",
                TileCommand = new RelayCommand(OpenWhatsNew),
                AutomationId = "WhatsNew",
            };

            Messenger.Default.Register<ColorSchemeUpdatedMessage>(this, OnColorSchemeUpdated);
            Messenger.Default.Register<ProfileCreationStartedMessage>(this, OnProfileCreationStarted);
            Messenger.Default.Register<ProfileCreationCompletedMessage>(this, OnProfileCreationCompleted);
            Messenger.Default.Register<DataSyncCompletedMessage>(this, OnDataSyncCompleted);
            Messenger.Default.Register<LocalDataDeletedMessage>(this, OnLocalDataDeletedMessage);
        }

        public InitializationStatusViewModel InitializationStatusVm { get; set; }

        public virtual ObservableCollection<InfoTileViewModel> Tiles
        {
            get
            {
                if (_tiles != null)
                {
                    return _tiles;
                }

                if (SettingsService.IsStandaloneMode)
                {
                    OpenEurolookHelpTile.Description = "Open the Eurolook Help.";

                    _tiles = new ObservableCollection<InfoTileViewModel>
                    {
                        CreateDocumentPlaceholderTile,
                        ManageAuthorsTile,
                        ChooseOptionsTile,
                        OpenEurolookHelpTile,
                    };
                }
                else
                {
                    _tiles = new ObservableCollection<InfoTileViewModel>
                    {
                        CreateDocumentPlaceholderTile,
                        ManageAuthorsTile,
                        ChooseOptionsTile,
                        SendFeedbackTile,
                        OpenEurolookHelpTile,
                    };
                }

                return _tiles;
            }
        }

        protected InfoTileViewModel CreateDocumentPlaceholderTile { get; }

        protected InfoTileViewModel CreateDocumentTile { get; }

        protected InfoTileViewModel CreateDocumentWithWizardTile { get; }

        protected InfoTileViewModel ManageAuthorsTile { get; }

        protected InfoTileViewModel ChooseOptionsTile { get; }

        protected InfoTileViewModel SendFeedbackTile { get; }

        protected InfoTileViewModel OpenEurolookHelpTile { get; }

        protected InfoTileViewModel WhatsNewTile { get; }

        protected bool IsInitialized()
        {
            return _isInitialized ?? RefreshIsInitialized();
        }

        private void CreateDocument()
        {
            try
            {
                var viewModel = _createDocumentViewModelFunc();
                viewModel.Title = "Create a new Eurolook document";
                viewModel.HelpReference = "CreateDocument";
                viewModel.CreateDocumentFunc =
                    async () =>
                    {
                        viewModel.IsCreating = true;
                        var documentCreationInfo = viewModel.GetDocumentCreationInfo();

                        var beforeDocumentCreationTasks = documentCreationInfo.IsTemplateFromTemplateStore
                            ? _beforeNewDocumentFromTemplateStoreCreationTasks
                            : _beforeNewDocumentCreationTasks;

                        var afterDocumentCreationTasks = documentCreationInfo.IsTemplateFromTemplateStore
                            ? _afterNewDocumentFromTemplateStoreCreationTasks
                            : _afterNewDocumentCreationTasks;

                        await _documentCreationService.CreateDocumentAsync(
                            documentCreationInfo,
                            beforeDocumentCreationTasks,
                            afterDocumentCreationTasks,
                            ProgressReporter<int>.Empty);
                    };

                var win = new CreateDocumentWindow(viewModel);
                MessageService.ShowSingleton(win);
            }
            catch (Exception ex)
            {
                this.LogError("Failed executing CreateDocumentCommand.\n{0}", ex);
            }
        }

        private async Task CreateDocumentWithWizard()
        {
            try
            {
                var wizardViewModel = _createDocumentWizardViewModelFunc();
                var window = wizardViewModel.GetWindow();
                await window.ShowWizard(wizardViewModel, MessageService);
            }
            catch (Exception ex)
            {
                this.LogError($"Failed executing {nameof(CreateDocumentWithWizard)}.\n{ex}");
            }
        }

        private void ManageAuthors()
        {
            try
            {
                var window = new AuthorManagerWindow2(_authorManagerViewModel2Func());
                MessageService.ShowSingleton(window, true, false, true);
            }
            catch (Exception ex)
            {
                this.LogError($"Failed executing {nameof(ManageAuthors)}.\n{ex}");
            }
        }

        private void ChooseOptions()
        {
            try
            {
                var win = new OptionsWindow(_optionsViewModelCreateFunc());
                MessageService.ShowSingleton(win, true, false, true);
            }
            catch (Exception ex)
            {
                this.LogError($"Failed executing {nameof(ChooseOptions)}.\n{ex}");
            }
        }

        private void SendFeedback()
        {
            _emailService.Value.SendFeedback().FireAndForgetSafeAsync(this.LogError);
        }

        private bool CanOpenEurolookHelp()
        {
            return SettingsService.IsStandaloneMode || !string.IsNullOrEmpty(SettingsService.OnlineHelpUrl);
        }

        private void OpenEurolookHelp()
        {
            try
            {
                if (!CanOpenEurolookHelp())
                {
                    return;
                }

                SettingsService.OpenHelpLink();
            }
            catch (Exception ex)
            {
                this.LogError($"Failed executing OpenEurolookHelp.\n{ex}");
            }
        }

        protected async Task InsertDynamicTiles()
        {
            if (!await _profileInitializer.Value.InitializeProfileIfNecessary())
            {
                return;
            }

            await InsertWhatsNewTile();
            UpdateColorScheme();
        }

        protected async Task InsertCreateDocumentTile()
        {
            if (!await _profileInitializer.Value.InitializeProfileIfNecessary())
            {
                return;
            }

            var isWizardActive = await _systemConfigurationService.IsCreationWizardEnabled();
            Execute.OnUiThread(
                () =>
                {
                    Tiles.Remove(CreateDocumentPlaceholderTile);
                    Tiles.Insert(0, isWizardActive ? CreateDocumentWithWizardTile : CreateDocumentTile);

                    UpdateColorScheme();
                });
        }

        private async Task InsertWhatsNewTile(bool highlightTile = false)
        {
            if (Tiles == null || SettingsService.IsStandaloneMode)
            {
                return;
            }

            var whatsNewConfiguration = await _systemConfigurationService.GetWhatsNewConfiguration();
            if (whatsNewConfiguration.IsVisible)
            {
                Execute.OnUiThread(
                    () =>
                    {
                        if (!Tiles.Contains(WhatsNewTile))
                        {
                            Tiles.Add(WhatsNewTile);
                        }

                        if (highlightTile)
                        {
                            WhatsNewTile.ColorLuminance = Luminance.Highlighted;
                        }
                    });
            }
        }

        private async void OpenWhatsNew()
        {
            try
            {
                var conf = await _systemConfigurationService.GetWhatsNewConfiguration();
                if (conf.HasCustomUrl)
                {
                    Uri uriResult;
                    bool isWebUrl = Uri.TryCreate(conf.CustomUrl, UriKind.Absolute, out uriResult)
                        && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
                    if (isWebUrl)
                    {
                        Process.Start(conf.CustomUrl);
                    }
                    else
                    {
                        this.LogError($"Failed executing OpenWhatsNew.\nThe custom URL {conf.CustomUrl} is no valid web address.");
                    }
                }
                else
                {
                    SettingsService.OpenHelpLink("WhatsNew");
                }
            }
            catch (Exception ex)
            {
                this.LogError("Failed executing OpenWhatsNew.\n{0}", ex);
            }
        }

        private void OnProfileCreationCompleted(ProfileCreationCompletedMessage message)
        {
            ProfileCreationCompleted(message).FireAndForgetSafeAsync(this.LogError);
        }

        private async Task ProfileCreationCompleted(ProfileCreationCompletedMessage message)
        {
            this.LogTrace("Profile initialization completed; refreshing Home tab.");
            await RefreshIsInitializedDelayed();

            // add the What's New tile
            if (_isInitialized == true)
            {
                await InsertWhatsNewTile(true);
            }

            CommandManager.InvalidateRequerySuggested();
        }

        private void OnLocalDataDeletedMessage(LocalDataDeletedMessage message)
        {
            LocalDataDeletedAsync(message).FireAndForgetSafeAsync(this.LogError);
        }

        private async Task LocalDataDeletedAsync(LocalDataDeletedMessage message)
        {
            this.LogTrace("Local data deleted; refreshing Home tab.");
            await RefreshIsInitializedDelayed();
        }

        private void OnDataSyncCompleted(DataSyncCompletedMessage message)
        {
            DataSyncCompletedAsync(message).FireAndForgetSafeAsync(this.LogError);
        }

        private async Task DataSyncCompletedAsync(DataSyncCompletedMessage message)
        {
            if (message.DataSyncStatus.Result == DataSyncResult.Success)
            {
                this.LogTrace("Data sync completed; refreshing Home tab.");
                await RefreshIsInitializedDelayed();
            }
        }

        /// <summary>
        /// Refreshes the IsInitialized state with a delay.
        /// This is useful because the messages like OnDataSyncCompleted or OnProfileCreationCompleted
        /// often arrive before the SQLite database is ready.
        /// </summary>
        /// <returns>The value of the _isInitialized field.</returns>
        private async Task<bool> RefreshIsInitializedDelayed(int ms = 1000)
        {
            await Task.Delay(ms);
            return RefreshIsInitialized();
        }

        private bool RefreshIsInitialized()
        {
            _isInitialized = _profileInitializer.Value.IsDatabaseInitialized();
            this.LogDebug($"Initialized: {_isInitialized}");
            return (bool)_isInitialized;
        }

        private void OnProfileCreationStarted(ProfileCreationStartedMessage message)
        {
            CommandManager.InvalidateRequerySuggested();
        }

        private void OnColorSchemeUpdated(ColorSchemeUpdatedMessage message)
        {
            Execute.OnUiThread(() => UpdateColorScheme(message.NewColorScheme));
        }

        protected void UpdateColorScheme()
        {
            UpdateColorScheme(_colorManager.CurrentColorScheme);
        }

        private void UpdateColorScheme(ColorScheme colorScheme)
        {
            if (colorScheme == null)
            {
                return;
            }

            foreach (var tile in Tiles)
            {
                tile.ColorBrush = colorScheme.PrimaryColor.ToBrush();
            }
        }
    }
}
