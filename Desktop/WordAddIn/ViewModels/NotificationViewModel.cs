﻿using Eurolook.WordAddIn.Views;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Eurolook.WordAddIn.ViewModels
{
    public abstract class NotificationViewModel : ViewModelBase
    {
        private RelayCommand _defaultCommand;
        private string _defaultButtonText;
        private string _notificationText;

        public string NotificationText
        {
            get { return _notificationText; }
            set { Set(() => NotificationText, ref _notificationText, value); }
        }

        public string DefaultButtonText
        {
            get { return _defaultButtonText; }
            set { Set(() => DefaultButtonText, ref _defaultButtonText, value); }
        }

        public RelayCommand DefaultCommand
        {
            get { return _defaultCommand; }
            set
            {
                Set(() => DefaultCommand, ref _defaultCommand, value);
                RaisePropertyChanged(() => IsDefaultButtonVisible);
            }
        }

        public GenericNotificationBar NotificationBar { get; set; }

        public bool IsDefaultButtonVisible
        {
            get { return DefaultCommand != null; }
        }
    }
}
