﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Media.Imaging;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing;
using Eurolook.WordAddIn.BrickEngine;

namespace Eurolook.WordAddIn.ViewModels
{
    public class BrickGroupViewModel : TileViewModel
    {
        public BrickGroupViewModel(BrickGroup group, IEurolookWordDocument eurolookDocument)
            : base("")
        {
            GroupId = group.Id;

            // create view models for bricks within the group
            if (group.Bricks != null)
            {
                Bricks = group.Bricks.OrderBy(b => b.UiPositionIndex)
                              .ThenBy(x => x.DisplayName)
                              .Select(brick => new BrickViewModel(brick, eurolookDocument, false))
                              .ToList();
            }

            Title = group.DisplayName;
            Description = group.Description;
            ShowDisclosureIndicator = true;
            Acronym = group.Acronym;
            ColorLuminance = group.ColorLuminance;
            AutomationId = group.Id.ToString("B");
            UIPositionIndex = group.UiPositionIndex;
            if (HeroIcon.IsSvgFile(group.VectorIcon))
            {
                HeroIconSource = HeroIcon.CreateImageFromBytes(group.VectorIcon);
            }
            else if (group.Icon != null && group.Icon.Length > 0)
            {
                Icon = new BitmapImage().InitAndFreeze(group.Icon);
            }
        }

        public override int UIPositionIndex { get; }

        public List<BrickViewModel> Bricks { get; set; }

        public Guid GroupId { get; set; }
    }

    public class BrickCheckmarkUpdater
    {
        public void UpdateCheckmark(IEurolookWordDocument eurolookDocument, List<BrickContainer> brickContainers, IEnumerable<TileViewModel> tiles)
        {
            try
            {
                foreach (var tileVm in tiles)
                {
                    if (tileVm is BrickViewModel bvm)
                    {
                        UpdateBrickViewModel(eurolookDocument, brickContainers, bvm);
                    }
                    else if (tileVm is BrickGroupViewModel bgvm)
                    {
                        bool isBrickGroupInDocument = false;
                        foreach (var brickVm in bgvm.Bricks)
                        {
                            UpdateBrickViewModel(eurolookDocument, brickContainers, brickVm);
                            if (brickVm.ShowCheckmark)
                            {
                                isBrickGroupInDocument = true;
                            }
                        }

                        bgvm.ShowCheckmark = isBrickGroupInDocument;
                    }
                }
            }
            catch (COMException)
            {
                // ignore COM exceptions
            }
        }

        private static void UpdateBrickViewModel(
            IEurolookWordDocument eurolookDocument,
            List<BrickContainer> brickContainers,
            BrickViewModel brickViewModel)
        {
            if (brickViewModel.Brick.IsMultiInstance)
            {
                return;
            }

            if (brickViewModel.Brick is CommandBrick && eurolookDocument?.BrickEngine != null)
            {
                if (eurolookDocument.BrickEngine.GetOrCreateBrickInstance(brickViewModel.Brick) is CommandBrickInstance
                        brickInstance
                    && brickInstance.BrickCommand != null)
                {
                    brickViewModel.ShowCheckmark = brickInstance.BrickCommand.ShowCheckmark(brickContainers);
                }
            }
            else
            {
                brickViewModel.ShowCheckmark = brickContainers.Any(bc => bc.Id == brickViewModel.Brick.Id);
            }
        }
    }
}
