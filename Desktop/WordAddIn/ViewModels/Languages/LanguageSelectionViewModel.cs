﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Data.Models;
using GalaSoft.MvvmLight;

namespace Eurolook.WordAddIn.ViewModels.Languages
{
    public class LanguageSelectionViewModel : ViewModelBase
    {
        private readonly Action _validate;
        private readonly Func<IEnumerable<Language>> _loadFavoriteLanguages;
        private readonly Func<IEnumerable<Language>> _loadAllLanguages;

        private readonly LanguageViewModel _languageMore = new LanguageViewModel
        {
            DisplayName = "Show more...",
        };

        private LanguageViewModel _selectedLanguage;

        public ObservableCollectionEx<LanguageViewModel> Languages { get; }

        public bool IsFavoriteListEnabled { get; set; }

        public User User { get; set; }

        public LanguageViewModel SelectedLanguage
        {
            get => _selectedLanguage;
            set
            {
                if (value == _selectedLanguage)
                {
                    return;
                }

                Set(() => SelectedLanguage, ref _selectedLanguage, value);
                _validate?.Invoke();
                if (value == _languageMore)
                {
                    LoadLanguages(true);
                }
            }
        }

        public LanguageSelectionViewModel(
            Func<IEnumerable<Language>> loadFavoriteLanguages,
            Func<IEnumerable<Language>> loadAllLanguages,
            Action validate)
        {
            _validate = validate;
            _loadFavoriteLanguages = loadFavoriteLanguages;
            _loadAllLanguages = loadAllLanguages;
            Languages = new ObservableCollectionEx<LanguageViewModel>();
        }

        public void LoadLanguages(bool forceLoadAll)
        {
            Languages.Clear();
            if ((!IsFavoriteListEnabled || forceLoadAll) && _loadAllLanguages != null)
            {
                Languages.AddRange(_loadAllLanguages().Select(l => new LanguageViewModel(l)));
            }
            else if (_loadFavoriteLanguages != null)
            {
                var favourites = _loadFavoriteLanguages();
                if (favourites.Any())
                {
                    Languages.AddRange(favourites.Select(l => new LanguageViewModel(l)));
                }
                else if (_loadAllLanguages != null)
                {
                    forceLoadAll = true;
                    Languages.AddRange(_loadAllLanguages().Select(l => new LanguageViewModel(l)));
                }
            }

            if (IsFavoriteListEnabled && !forceLoadAll)
            {
                Languages.Add(_languageMore);
            }
            else if (forceLoadAll)
            {
                Languages.Remove(_languageMore);
            }
        }

        public void InitDocumentLanguage()
        {
            SelectedLanguage =
                Languages.FirstOrDefault(l => l.Id == SelectedLanguage?.Id)
                ?? Languages.FirstOrDefault(l => l.Id == User?.Settings.LastDocumentLanguageId)
                ?? Languages.FirstOrDefault(
                    l => string.Equals(
                        l.Name,
                        "EN",
                        StringComparison.InvariantCultureIgnoreCase))
                ?? Languages.FirstOrDefault(
                    l => string.Equals(
                        l.Name,
                        "FR",
                        StringComparison.InvariantCultureIgnoreCase))
                ?? Languages.FirstOrDefault();
        }
    }
}
