using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;

namespace Eurolook.WordAddIn.ViewModels.Languages
{
    public class LanguageLoader : ILanguageLoader
    {
        public IEnumerable<Language> GetAllLanguages(DocumentModel documentModel)
        {
            return documentModel?.DocumentLanguages
                                ?.Where(dl => !dl.Deleted)
                                .OrderBy(l => l.DocumentLanguage?.DisplayName ?? "")
                                .Select(lang => lang.DocumentLanguage)
                   ?? Enumerable.Empty<Language>();
        }

        public IEnumerable<Language> GetAllLanguages(Template template)
        {
            return template.TemplateFiles
                           .Where(f => !f.Deleted)
                           .Select(f => f.Language)
                           .OrderBy(l => l.DisplayName);
        }

        public IEnumerable<Language> GetFavoriteLanguages(DocumentModel documentModel, UserSettings userSettings)
        {
            return GetAllLanguages(documentModel).Where(l => userSettings?.GetFavoriteLanguageIds().Contains(l.Id) == true);
        }

        public IEnumerable<Language> GetFavoriteLanguages(Template template, UserSettings userSettings)
        {
            return GetAllLanguages(template).Where(l => userSettings?.GetFavoriteLanguageIds().Contains(l.Id) == true);
        }
    }
}
