using System.Collections.Generic;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;

namespace Eurolook.WordAddIn.ViewModels.Languages
{
    public interface ILanguageLoader
    {
        IEnumerable<Language> GetAllLanguages(DocumentModel documentModel);

        IEnumerable<Language> GetAllLanguages(Template template);

        IEnumerable<Language> GetFavoriteLanguages(DocumentModel documentModel, UserSettings userSettings);

        IEnumerable<Language> GetFavoriteLanguages(Template template, UserSettings userSettings);
    }
}
