using System;
using Eurolook.Data.Models;
using GalaSoft.MvvmLight;

namespace Eurolook.WordAddIn.ViewModels.Languages
{
    public class LanguageViewModel : ViewModelBase
    {
        private bool _isSelected;
        private bool _isFavorite;

        public LanguageViewModel()
        {
        }

        public LanguageViewModel(Language language)
        {
            Model = language;
            Id = Model.Id;
            Name = Model.Name;
            DisplayName = Model.DisplayName;
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set { Set(() => IsSelected, ref _isSelected, value); }
        }

        public bool IsFavorite
        {
            get { return _isFavorite; }
            set { Set(() => IsFavorite, ref _isFavorite, value); }
        }

        public Language Model { get; }

        public Guid Id { get; }

        public string Name { get; }

        public string DisplayName { get; set; }

        public override string ToString()
        {
            return DisplayName;
        }
    }
}
