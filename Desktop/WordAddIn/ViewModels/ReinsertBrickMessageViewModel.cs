using Eurolook.AddIn.Common.ViewModels;

namespace Eurolook.WordAddIn.ViewModels
{
    public class ReinsertBrickMessageViewModel : MessageViewModel
    {
        public ReinsertBrickMessageViewModel()
        {
            Title = "Insert Brick";
            Message = "The brick is already in the document but in a deleted revision.\n"
                      + "Do you prefer to insert a new brick instead of rejecting the deletion?";
            YesButtonText = "Yes";
            CancelButtonText = "No";
        }
    }
}
