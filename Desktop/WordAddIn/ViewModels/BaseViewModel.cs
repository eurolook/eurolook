using System;
using Eurolook.AddIn.Common;
using Eurolook.Common.Log;
using GalaSoft.MvvmLight;

namespace Eurolook.WordAddIn.ViewModels
{
    public abstract class BaseViewModel : ViewModelBase, ICanLog
    {
        protected BaseViewModel(IWordApplicationContext applicationContext, IMessageService messageService, ISettingsService settingsService)
        {
            ApplicationContext = applicationContext;
            MessageService = messageService;
            SettingsService = settingsService;
        }

        public IWordApplicationContext ApplicationContext { get; }

        public IMessageService MessageService { get; }

        public ISettingsService SettingsService { get; }

        public Action CloseAction { get; set; }
    }
}
