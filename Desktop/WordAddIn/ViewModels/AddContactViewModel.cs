﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Authors;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.Messages;
using Eurolook.AddIn.Common.SystemConfiguration;
using Eurolook.Common.Extensions;
using Eurolook.Data.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.ViewModels
{
    [Flags]
    public enum ContactOptions
    {
        None = 0,
        Phone = 1,
        Fax = 2,
        Email = 4,
        Service = 8,
        Office = 16,
        Dg = 32,
        Directorate = 64,
        Unit = 128,
    }

    public class AddContactViewModel : ViewModelBase, IDataErrorInfo
    {
        private readonly IUserDataRepository _userDataRepository;
        private readonly IAuthorRepository _authorRepository;
        // ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
        private readonly ISettingsService _settingsService;
        private readonly ISystemConfigurationService _systemConfigurationService;
        private readonly Language _documentLanguage;
        private readonly Func<Author, IJobAssignment, ContactOptions, Language, string> _createContactInfoFunc;

        private UserSettings _settings;
        private bool _isPickerActive;
        private bool _isLightboxEnabled;
        private bool _useTileBasedPicker;

        private string _previewText;
        private Author _previewAuthor;
        private IJobAssignment _previewJobAssignment;

        private AuthorAndJob _lastSelectedAuthorAndJob;
        private AuthorAndJob _selectedJob;
        private AuthorAndAllJobs _authorAndAllJobs;

        public AddContactViewModel(
            ISettingsService settingsService,
            ISystemConfigurationService systemConfigurationService,
            IAuthorRepository authorRepository,
            IUserDataRepository userDataRepository,
            Language documentLanguage,
            Func<Author, IJobAssignment, ContactOptions, Language, string> createContactInfoFunc,
            Func<Action<AuthorAndAllJobs>, Action, AuthorSearchLightBoxViewModel> authorSearchLightBoxViewModelFunc,
            Func<Action<AuthorAndAllJobs>, Action, Action<bool>, AuthorPickerLightBoxViewModel>
                authorPickerLightBoxViewModelFunc)
        {
            _userDataRepository = userDataRepository;
            _authorRepository = authorRepository;
            _systemConfigurationService = systemConfigurationService;
            _settingsService = settingsService;
            _documentLanguage = documentLanguage;
            _createContactInfoFunc = createContactInfoFunc;

            GetUserOptions();

            AuthorsAndJobsViewModel = new AuthorsAndJobsViewModel(settingsService);
            AuthorSearchLightBoxViewModel = authorSearchLightBoxViewModelFunc(PickResult, CancelSearch);
            AuthorPickerLightBoxViewModel = authorPickerLightBoxViewModelFunc(
                PickResult,
                CancelPicker,
                singleResultPerAuthor =>
                {
                    _lastSelectedAuthorAndJob = SelectedJob;
                    AuthorSearchLightBoxViewModel.Show(singleResultPerAuthor);
                });

            ShowAuthorPickerCommand = new RelayCommand(ShowAuthorPicker);
            GetHelpCommand = new RelayCommand(() => _settingsService.OpenHelpLink("AddContacts"));
        }

        public bool IsValid => SelectedJob != null;

        public bool UseTileBasedAuthorPicker
        {
            get => _useTileBasedPicker;
            set => Set(() => UseTileBasedAuthorPicker, ref _useTileBasedPicker, value);
        }

        public bool IsLightboxEnabled
        {
            get => _isLightboxEnabled;
            set => Set(() => IsLightboxEnabled, ref _isLightboxEnabled, value);
        }

        public string PreviewText
        {
            get => _previewText;
            set => Set(() => PreviewText, ref _previewText, value);
        }

        public ContactOptions Options
        {
            get { return (ContactOptions)_settings.ContactOptions; }
        }

        public bool Result { get; set; }

        public bool IncludePhone
        {
            get { return Options.HasFlag(ContactOptions.Phone); }
            set { SetOptionFlag(ContactOptions.Phone, value); }
        }

        public bool IncludeFax
        {
            get { return Options.HasFlag(ContactOptions.Fax); }
            set { SetOptionFlag(ContactOptions.Fax, value); }
        }

        public bool IncludeEmail
        {
            get { return Options.HasFlag(ContactOptions.Email); }
            set { SetOptionFlag(ContactOptions.Email, value); }
        }

        public bool IncludeService
        {
            get { return Options.HasFlag(ContactOptions.Service); }
            set { SetOptionFlag(ContactOptions.Service, value); }
        }

        public bool IncludeOffice
        {
            get { return Options.HasFlag(ContactOptions.Office); }
            set { SetOptionFlag(ContactOptions.Office, value); }
        }

        public bool IncludeDg
        {
            get { return Options.HasFlag(ContactOptions.Dg); }
            set { SetOptionFlag(ContactOptions.Dg, value); }
        }

        public bool IncludeDirectorate
        {
            get { return Options.HasFlag(ContactOptions.Directorate); }
            set { SetOptionFlag(ContactOptions.Directorate, value); }
        }

        public bool IncludeUnit
        {
            get { return Options.HasFlag(ContactOptions.Unit); }
            set { SetOptionFlag(ContactOptions.Unit, value); }
        }

        public AuthorAndAllJobs AuthorAndAllJobs
        {
            get => _authorAndAllJobs;
            set
            {
                PreviewText = string.Empty;

                _authorAndAllJobs = value;
                _previewAuthor = null;
                RaisePropertyChanged(() => AuthorAndAllJobs);
                RaisePropertyChanged(() => AvailableJobs);
                RaisePropertyChanged(() => HasMultipleJobs);

                if ((value == null) || !value.AdditionalJobs.IsNullOrEmpty())
                {
                    SelectedJob = null;
                }
                else
                {
                    SelectedJob = value.All.First();
                }
            }
        }

        public IEnumerable<AuthorAndJob> AvailableJobs => _authorAndAllJobs?.All ?? new List<AuthorAndJob>();

        public bool HasMultipleJobs => AvailableJobs.Count() > 1;

        public AuthorAndJob SelectedJob
        {
            get => _selectedJob;
            set
            {
                _previewAuthor = null;
                _previewJobAssignment = null;

                _lastSelectedAuthorAndJob = SelectedJob;
                _selectedJob = value;
                if (value?.Author.IsDummyForSearch == true)
                {
                    AuthorSearchLightBoxViewModel.Show();
                }

                RaisePropertyChanged(() => SelectedJob);
                RaisePropertyChanged(() => IsValid);
                PreviewText = CreatePreview();
            }
        }

        public AuthorsAndJobsViewModel AuthorsAndJobsViewModel { get; }

        public AuthorSearchLightBoxViewModel AuthorSearchLightBoxViewModel { get; }

        public AuthorPickerLightBoxViewModel AuthorPickerLightBoxViewModel { get; }

        public RelayCommand ShowAuthorPickerCommand { get; }

        public RelayCommand AddContactPersonCommand
        {
            get { return new RelayCommand(AddContactPerson); }
        }

        public RelayCommand GetHelpCommand { get; }

        public Action CloseAction { get; set; }

        internal async Task LoadAsync()
        {
            var user = await _userDataRepository.GetUserIncludingAuthorsAndJobAssignmentsAsync();
            var authorsAndJobs = AuthorAndAllJobs.CreateAll(user);

            Execute.OnUiThread(
                () =>
                {
                    AuthorsAndJobsViewModel.InitAuthorsAndAllJobs(authorsAndJobs);
                });

            UseTileBasedAuthorPicker = await _systemConfigurationService.IsCreationWizardEnabled();
        }

        private void ShowAuthorPicker()
        {
            _isPickerActive = true;
            IsLightboxEnabled = true;
            AuthorPickerLightBoxViewModel.Show(AuthorsAndJobsViewModel.AuthorsAndAllJobs, "Contact");
        }

        private void PickResult([NotNull]AuthorAndAllJobs authorAndJob)
        {
            AuthorAndAllJobs = AuthorsAndJobsViewModel.AddIfNotPresent(authorAndJob);
            if (!HasMultipleJobs)
            {
                SelectedJob = AuthorAndAllJobs.All.First();
            }
            else if (authorAndJob.All.Count() == 1)
            {
                // Result comes from a search with separate entries per job
                SelectedJob = AuthorsAndJobsViewModel.AuthorsAndJobs.FirstOrDefault(
                    aj => (aj.Author.Author?.Id.Equals(authorAndJob.Author.Author?.Id) == true)
                          && !aj.Author.IsOriginatingFromDocumentCustomXml
                          && (authorAndJob.MainJobAssignment?.JobAssignmentId == aj.JobAssignment?.JobAssignmentId));
            }

            IsLightboxEnabled = false;
        }

        private void CancelSearch()
        {
            SelectedJob = _lastSelectedAuthorAndJob;
            if (_isPickerActive)
            {
                ShowAuthorPicker();
            }
        }

        private void CancelPicker()
        {
            _isPickerActive = false;
            IsLightboxEnabled = false;
        }

        private void SetOptionFlag(ContactOptions flag, bool val)
        {
            if (val)
            {
                _settings.ContactOptions = (int)((ContactOptions)_settings.ContactOptions | flag);
            }
            else
            {
                _settings.ContactOptions = (int)((ContactOptions)_settings.ContactOptions & ~flag);
            }

            PreviewText = CreatePreview();
        }

        private void GetUserOptions()
        {
            _settings = _userDataRepository.GetUserSettings();
        }

        private void AddContactPerson()
        {
            Result = true;
            _userDataRepository.UpdateUserSettings(_settings);

            CloseAction?.Invoke();
        }

        private string CreatePreview()
        {
            if (SelectedJob?.Author.Author == null || SelectedJob.Author.IsDummyForSearch)
            {
                return string.Empty;
            }

            if (_userDataRepository.AddAuthorToUser(_userDataRepository.GetUser(), SelectedJob.Author.Author))
            {
                Messenger.Default.Send(new AuthorAddedMessage(SelectedJob.Author.Author.Id));
            }

            _previewAuthor ??= _authorRepository.GetAuthorForDocumentCreation(SelectedJob.Author.Author.Id, _documentLanguage);

            if (_previewJobAssignment == null)
            {
                var jobAssignmentId = SelectedJob?.JobAssignment?.JobAssignment?.Id;
                _previewJobAssignment = jobAssignmentId == null
                    ? _previewAuthor
                    : _authorRepository.GetJobAssignmentForDocumentCreation(
                        jobAssignmentId.Value,
                        _documentLanguage);
            }

            if (_previewAuthor == null)
            {
                return "Cannot add this author as contact.";
            }

            return _createContactInfoFunc(
                _previewAuthor,
                _previewJobAssignment,
                Options,
                _documentLanguage);
        }

        public string this[string columnName] => IsValid ? "" : "Invalid Author or Job Assignment";

        public string Error { get; }
    }
}
