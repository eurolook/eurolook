using System;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;
using Eurolook.DocumentProcessing.DocumentCreation;

namespace Eurolook.WordAddIn.ViewModels
{
    public class DocumentType : ISearchableGroupableItem, IDocumentType
    {
        private const string TemplateStoreCategoryName = "Template Store Templates";
        private const string TemplateManagerCategoryName = "Template Manager Templates";

        private readonly string _userTemplateCategoryName;

        public DocumentType(DocumentModel documentModel)
        {
            DocumentModel = documentModel;
        }

        public DocumentType(Template template, bool isAvailableOffline, bool isStandalone = false)
        {
            Template = template;
            DocumentModel = template.BaseDocumentModel;
            IsAvailableOffline = isAvailableOffline;
            _userTemplateCategoryName = isStandalone ? TemplateManagerCategoryName : TemplateStoreCategoryName;
        }

        public DocumentModel DocumentModel { get; }

        public Template Template { get; }

        public bool IsTemplateFromTemplateStore => Template != null;

        public bool IsAvailableOffline { get; }

        public Guid Id => DocumentModel.Id;

        public string Name => DocumentModel.Name;

        public string DisplayName => Template?.Name ?? DocumentModel.DisplayName;

        public string Description => Template?.Description ?? DocumentModel.Description;

        public string Keywords => Template?.Tags ?? DocumentModel.Keywords;

        public string GroupBy => Template != null ? _userTemplateCategoryName : DocumentModel.DocumentCategory.Name;

        public string SortBy => Template != null ? _userTemplateCategoryName : DocumentModel.DocumentCategory.Name + DocumentModel.UiPositionIndex.ToString().PadLeft(10, '0');

        public string ThenBy => Template?.Name ?? DocumentModel.DisplayName;
    }
}
