﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.AddIn.Common.Messages;
using Eurolook.AddIn.Common.ProfileInitializer;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Data;
using Eurolook.Data.Constants;
using Eurolook.Data.Models;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.ViewModels
{
    [UsedImplicitly]
    public sealed class ToolsTabViewModel : BaseViewModel, IDisposable
    {
        private readonly IProfileInitializer _profileInitializer;
        private readonly IBrickVisibilityChecker _brickVisibilityChecker;
        private readonly ColorManager _colorManager;
        private DocumentViewModel _documentViewModel;

        public ToolsTabViewModel(
            IWordApplicationContext applicationContext,
            IMessageService messageService,
            ISettingsService settingsService,
            IProfileInitializer profileInitializer,
            IBrickVisibilityChecker brickVisibilityChecker,
            ColorManager colorManager)
            : base(applicationContext, messageService, settingsService)
        {
            _profileInitializer = profileInitializer;
            _brickVisibilityChecker = brickVisibilityChecker;
            _colorManager = colorManager;
            Tiles = new List<InfoTileViewModel>();
            Messenger.Default.Register<ColorSchemeUpdatedMessage>(this, OnColorSchemeUpdated);
        }

        ~ToolsTabViewModel()
        {
            Dispose(false);
        }

        public List<InfoTileViewModel> Tiles { get; set; }

        public DocumentViewModel DocumentViewModel
        {
            get => _documentViewModel;
            set
            {
                if (_documentViewModel == value)
                {
                    return;
                }

                _documentViewModel = value;
                RaisePropertyChanged(() => DocumentViewModel);

                if (_documentViewModel?.IsDocumentModelKnown == true)
                {
                    LoadTiles();
                    UpdateColorScheme();

                    foreach (var infoTileViewModel in Tiles)
                    {
                        if (infoTileViewModel.TileCommand is InitializationRelayCommand initCommand)
                        {
                            initCommand.RaiseCanExecuteChanged();
                        }
                        else if (infoTileViewModel.TileCommand is RelayCommand wpfCommand)
                        {
                            wpfCommand.RaiseCanExecuteChanged();
                        }
                        else if (infoTileViewModel.TileCommand is GalaSoft.MvvmLight.Command.RelayCommand command)
                        {
                            command.RaiseCanExecuteChanged();
                        }
                    }
                }
                else
                {
                    Tiles.Clear();
                }

                RaisePropertyChanged(() => Tiles);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void LoadTiles()
        {
            var toolsBricks = DocumentViewModel.DocumentStructures
                                               .Select(ds => ds.Brick)
                                               .Where(
                                                   b => _brickVisibilityChecker.IsBrickVisible(b) &&
                                                        b.CategoryId == CommonDataConstants.ToolsBrickCategoryId)
                                               .Distinct(new IdentifiableComparer<Brick>())
                                               .OrderBy(b => b.UiPositionIndex);

            Tiles = toolsBricks.Select(
                                   b => new InfoTileViewModel(b)
                                   {
                                       TileCommand = new InitializationRelayCommand(
                                           async () => await _documentViewModel.ExecuteToolCommand(b),
                                           () => _documentViewModel?.BrickEngine?.CanExecute(b) ?? false,
                                           _profileInitializer,
                                           true),
                                   })
                               .ToList();
        }

        private void OnColorSchemeUpdated(ColorSchemeUpdatedMessage message)
        {
            Execute.OnUiThread(() => UpdateColorScheme(message.NewColorScheme));
        }

        private void UpdateColorScheme()
        {
            UpdateColorScheme(_colorManager.CurrentColorScheme);
        }

        private void UpdateColorScheme(ColorScheme colorScheme)
        {
            if (colorScheme == null)
            {
                return;
            }

            foreach (var tile in Tiles)
            {
                tile.ColorBrush = colorScheme.PrimaryColor.ToBrush();
            }
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                foreach (var infoTileViewModel in Tiles)
                {
                    infoTileViewModel.Dispose();
                }
            }
        }
    }
}
