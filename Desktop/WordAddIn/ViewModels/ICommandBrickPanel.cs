using System;

namespace Eurolook.WordAddIn.ViewModels
{
    public interface ICommandBrickPanel
    {
        string Title { get; }

        IEurolookWordDocument EurolookDocument { get; }

        void Show(Action commitAction, Action cancelAction = null);

        void Cancel();

        void Commit();

        bool CanCommit();
    }
}
