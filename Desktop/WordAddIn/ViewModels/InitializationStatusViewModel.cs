using System.Windows.Input;
using Eurolook.AddIn.Common.Messages;
using Eurolook.Common.Log;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;

namespace Eurolook.WordAddIn.ViewModels
{
    public class InitializationStatusViewModel : ViewModelBase, ICanLog
    {
        public InitializationStatusViewModel()
        {
            Messenger.Default.Register<ProfileCreationStartedMessage>(this, OnProfileCreationStarted);
            Messenger.Default.Register<ProfileCreationCompletedMessage>(this, OnProfileCreationCompleted);
            Messenger.Default.Register<ProfileCreationFailedMessage>(this, OnProfileCreationFailed);
        }

        public bool IsInitializing { get; set; }

        public int SlidePosition { get; set; }

        public string UpdateDetailsMessage { get; set; }

        public bool IsInitialized { get; set; }

        public bool IsFailed { get; set; }

        private void OnProfileCreationStarted(ProfileCreationStartedMessage message)
        {
            SlidePosition = 1;
            IsInitializing = true;
            IsFailed = false;
            UpdateDetailsMessage = "Updating Eurolook...";
            UpdateView();
        }

        private void OnProfileCreationCompleted(ProfileCreationCompletedMessage message)
        {
            IsInitialized = false;
            IsInitializing = false;
            IsFailed = false;
            SlidePosition = 2;
            UpdateView();
        }

        private void OnProfileCreationFailed(ProfileCreationFailedMessage message)
        {
            IsInitialized = false;
            IsInitializing = false;
            IsFailed = true;
            UpdateDetailsMessage = "Eurolook failed to update.";
            UpdateView();
        }

        private void UpdateView()
        {
            // ReSharper disable once ExplicitCallerInfoArgument
            RaisePropertyChanged("");
            CommandManager.InvalidateRequerySuggested();
        }
    }
}
