using System.Collections.Generic;

namespace Eurolook.WordAddIn.ViewModels
{
    public interface ISearchableGroupableCollection
    {
        IEnumerable<ISearchableGroupableItem> Items { get; }

        ISearchableGroupableItem SelectedItem { get; set; }
    }
}
