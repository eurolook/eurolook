﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Eurolook.Data.Models;
using GalaSoft.MvvmLight;

namespace Eurolook.WordAddIn.ViewModels
{
    public class UserBrickChoiceViewModel : ViewModelBase
    {
        private readonly UserBrickChoiceViewModel _groupVm;
        private bool _isSelected;
        private bool _isExpanded;
        private string _automationId;

        public UserBrickChoiceViewModel()
        {
            Items = new ObservableCollection<UserBrickChoiceViewModel>();
        }

        public UserBrickChoiceViewModel(BrickGroup brickGroup)
            : this()
        {
            GroupId = brickGroup.Id;
            DisplayName = brickGroup.DisplayName;
            Tooltip = brickGroup.Description;
            IsMultipleChoice = brickGroup.IsMultipleChoice;
            AutomationId = brickGroup.Id.ToString("B");
        }

        public UserBrickChoiceViewModel(
            DocumentStructure documentStructure,
            IReadOnlyDictionary<Guid, bool> userBricks,
            UserBrickChoiceViewModel groupVm)
            : this()
        {
            BrickId = documentStructure.Brick.Id;
            DisplayName = documentStructure.Brick.DisplayName;
            Tooltip = documentStructure.Brick.Description;
            AutomationId = documentStructure.Brick.Id.ToString("B");

            _groupVm = groupVm;

            if (userBricks != null && userBricks.ContainsKey(BrickId.Value))
            {
                IsSelected = userBricks[BrickId.Value];
            }
            else
            {
                IsSelected = documentStructure.AutoBrickCreationType == AutoBrickCreationType.Always;
            }
        }

        public ObservableCollection<UserBrickChoiceViewModel> Items { get; set; }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                Set(() => IsSelected, ref _isSelected, value);
                if (value && _groupVm != null && !_groupVm.IsMultipleChoice)
                {
                    foreach (var brickChoiceVm in _groupVm.Items.Where(x => x != this))
                    {
                        brickChoiceVm.IsSelected = false;
                    }
                }
            }
        }

        public bool IsExpanded
        {
            get { return _isExpanded; }
            set { Set(() => IsExpanded, ref _isExpanded, value); }
        }

        public string DisplayName { get; set; }

        public Guid? GroupId { get; set; }

        public bool IsGroup => GroupId != null;

        public Guid? BrickId { get; set; }

        public string AutomationId
        {
            get { return _automationId; }
            set { Set(() => AutomationId, ref _automationId, value); }
        }

        public bool IsBrick => BrickId != null;

        public bool IsMultipleChoice { get; set; }

        public string Tooltip { get; set; }
    }
}
