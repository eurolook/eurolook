using Eurolook.AddIn.Common;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn
{
    public interface IWordApplicationContext : IApplicationContext<Application>
    {
        bool IsObjectValid(object obj);

        bool IsObjectValid(ContentControl contentControl);
    }
}
