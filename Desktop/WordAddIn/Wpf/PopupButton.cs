using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using Eurolook.AddIn.Common.Messages;
using GalaSoft.MvvmLight.Messaging;

namespace Eurolook.WordAddIn.Wpf
{
    public class PopupButton : Button
    {
        public static readonly DependencyProperty PopupProperty = DependencyProperty.Register(
            "Popup",
            typeof(Popup),
            typeof(PopupButton),
            new PropertyMetadata(default(Popup)));

        public Popup Popup
        {
            get { return (Popup)GetValue(PopupProperty); }
            set { SetValue(PopupProperty, value); }
        }

        protected override void OnClick()
        {
            if (Popup == null)
            {
                throw new InvalidOperationException("Popup dependency property of PopupButton is not set");
            }

            Popup.PlacementTarget = this;
            Popup.IsOpen = true;

            Messenger.Default.Register<RequestClosingMessage>(this, OnClose);
        }

        private void OnClose(RequestClosingMessage message)
        {
            Popup.IsOpen = false;
            Messenger.Default.Unregister<RequestClosingMessage>(this);
        }
    }
}
