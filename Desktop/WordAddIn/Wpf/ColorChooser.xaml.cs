using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.Wpf
{
    public partial class ColorChooser
    {
        public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register(
            nameof(ItemsSource),
            typeof(IEnumerable<ColorChooserItem>),
            typeof(ColorChooser),
            new UIPropertyMetadata(null));

        public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register(
            nameof(SelectedItem),
            typeof(ColorChooserItem),
            typeof(ColorChooser),
            new PropertyMetadata(null));

        public ColorChooser()
        {
            InitializeComponent();
        }

        public IEnumerable<ColorChooserItem> ItemsSource
        {
            get => (IEnumerable<ColorChooserItem>)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

        public ColorChooserItem SelectedItem
        {
            get => (ColorChooserItem)GetValue(SelectedItemProperty);
            set => SetValue(SelectedItemProperty, value);
        }
    }

    public class ColorChooserItem
    {
        public SolidColorBrush ColorBrush { get; set; }

        public string DisplayName { get; set; }

        public WdColorIndex ColorIndex { get; set; }

        public override string ToString()
        {
            return DisplayName;
        }
    }
}
