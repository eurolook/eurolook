using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Eurolook.WordAddIn.Wpf
{
    public class DarkerColorConverter : IValueConverter
    {
        private const double Coefficient = 0.66;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is SolidColorBrush brush)
            {
                var darkerColor = Darken(brush.Color);
                return new SolidColorBrush(darkerColor);
            }

            if (value is Color color)
            {
                var darkerColor = Darken(color);
                return new SolidColorBrush(darkerColor);
            }

            return new SolidColorBrush(Color.FromRgb(0, 0, 0));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        private Color Darken(Color color)
        {
            return Color.FromArgb(
                color.A,
                (byte)(color.R * Coefficient),
                (byte)(color.G * Coefficient),
                (byte)(color.B * Coefficient));
        }
    }
}
