using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace Eurolook.WordAddIn.Wizard
{
    public class ExecuteCommandAnimation : AnimationTimeline
    {
        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof(ICommand), typeof(ExecuteCommandAnimation), new UIPropertyMetadata(null));

        public static readonly DependencyProperty CommandParameterProperty =
            DependencyProperty.Register("CommandParameter", typeof(object), typeof(ExecuteCommandAnimation), new PropertyMetadata(null));

        public ExecuteCommandAnimation()
        {
            Completed += CommandAnimation_Completed;
        }

        public ICommand Command
        {
            get
            {
                return (ICommand)GetValue(CommandProperty);
            }
            set
            {
                SetValue(CommandProperty, value);
            }
        }

        public object CommandParameter
        {
            get
            {
                return GetValue(CommandParameterProperty);
            }
            set
            {
                SetValue(CommandParameterProperty, value);
            }
        }

        private void CommandAnimation_Completed(object sender, EventArgs e)
        {
            if (Command != null && Command.CanExecute(CommandParameter))
            {
                Command.Execute(CommandParameter);
            }
        }

        protected override Freezable CreateInstanceCore()
        {
            return new ExecuteCommandAnimation();
        }

        public override Type TargetPropertyType
        {
            get
            {
                return typeof(object);
            }
        }

        public override object GetCurrentValue(object defaultOriginValue, object defaultDestinationValue, AnimationClock animationClock)
        {
            return defaultOriginValue;
        }
    }
}
