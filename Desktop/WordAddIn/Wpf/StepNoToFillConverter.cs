﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Eurolook.WordAddIn.Wpf
{
    internal class StepNoToFillConverter : IValueConverter
    {
        private const string ErrorMessage = "StepNoToFillConverter can only convert int to SolidColorBrush";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (value is int && targetType == typeof(Brush))
                {
                    int stepNo = (int)value;
                    int dotNo = int.Parse(parameter.ToString());
                    if (stepNo == dotNo)
                    {
                        return Application.Current.Resources["PrimaryColorBrush"] as SolidColorBrush;
                    }

                    return new SolidColorBrush(Colors.Transparent);
                }

                throw new ArgumentException(ErrorMessage);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ErrorMessage, ex);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException(ErrorMessage);
        }
    }
}
