﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Eurolook.WordAddIn.Wpf
{
    public class ContrastColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is SolidColorBrush brush)
            {
                return GetContrastBrush(brush.Color);
            }

            if (value is Color color)
            {
                return GetContrastBrush(color);
            }

            return Brushes.Black;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        private static object GetContrastBrush(Color color)
        {
            // To decide between black or white, you need to know the brightness of the background. In order to get this:
            // 1. Convert RGB to gray: R*0.299 + G*0.587 + B*0.114.
            var gray = color.R * 0.299 + color.G * 0.587 + color.B * 0.114;

            // 2. Check if the gray is bright or dark. The mid of 0-255 would be 128. However experience shows that 186 is a better treshhold.
            if (gray > 186)
            {
                return Brushes.Black;
            }

            return Brushes.White;
        }
    }
}
