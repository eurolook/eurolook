﻿using System.Windows;

namespace Eurolook.WordAddIn.Wpf
{
    public class SlidingPanelHelper : DependencyObject
    {
        public static readonly DependencyProperty IsPanelVisibleProperty = DependencyProperty.Register(
            "IsPanelVisible",
            typeof(bool),
            typeof(SlidingPanelHelper),
            new PropertyMetadata(default(bool)));

        public static bool GetIsPanelVisible(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsPanelVisibleProperty);
        }

        public static void SetIsPanelVisible(DependencyObject obj, bool value)
        {
            obj.SetValue(IsPanelVisibleProperty, value);
        }
    }
}
