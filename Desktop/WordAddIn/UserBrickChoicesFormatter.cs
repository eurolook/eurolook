using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.ViewModels;

namespace Eurolook.WordAddIn
{
    public static class UserBrickChoicesFormatter
    {
        private const string Version2Prefix = "[v2]";
        private const string CurrentVersionPrefix = Version2Prefix;
        private const char EntrySeparator = ';';
        private const char ValueDelimiter = ':';
        private const string TrueValue = "1";
        private const string FalseValue = "0";

        internal static string ToString(HashSet<DocumentStructure> defaultStructure, Dictionary<Guid, bool> userBrickChoices)
        {
            if (defaultStructure == null)
            {
                throw new ArgumentNullException(nameof(userBrickChoices));
            }

            if (userBrickChoices == null)
            {
                throw new ArgumentNullException(nameof(userBrickChoices));
            }

            var stringBuilder = new StringBuilder(CurrentVersionPrefix);
            foreach (var brickId in userBrickChoices.Keys)
            {
                var documentStructure = defaultStructure.FirstOrDefault(d => d.BrickId == brickId);
                if (documentStructure != null)
                {
                    string defaultValue = GetDefaultValue(documentStructure);
                    string value = userBrickChoices[brickId] ? TrueValue : FalseValue;
                    if (defaultValue != value)
                    {
                        stringBuilder.Append($"{brickId}{ValueDelimiter}{value}{EntrySeparator}");
                    }
                }
            }

            string result = stringBuilder.ToString();
            return result != CurrentVersionPrefix ? result : null;
        }

        internal static Dictionary<Guid, bool> ToDictionary(HashSet<DocumentStructure> defaultStructure, string userBrickChoices)
        {
            var result = new Dictionary<Guid, bool>();
            if (string.IsNullOrWhiteSpace(userBrickChoices))
            {
                return result;
            }

            if (userBrickChoices.StartsWith(Version2Prefix))
            {
                ReadVersion2(defaultStructure, userBrickChoices, result);
            }
            else
            {
                ReadVersion1(defaultStructure, userBrickChoices, result);
            }

            return result;
        }

        internal static Dictionary<Guid, bool> ToDictionary(IEnumerable<UserBrickChoiceViewModel> userBrickChoices)
        {
            if (userBrickChoices == null)
            {
                throw new ArgumentNullException(nameof(userBrickChoices));
            }

            var result = new Dictionary<Guid, bool>();
            GetUserBricksDictionaryRecursive(userBrickChoices, result);
            return result;
        }

        private static void ReadVersion2(HashSet<DocumentStructure> defaultStructure, string userBrickChoices, Dictionary<Guid, bool> result)
        {
            ReadVersion1(defaultStructure, userBrickChoices.Replace(Version2Prefix, ""), result);
        }

        private static void ReadVersion1(HashSet<DocumentStructure> defaultStructure, string userBrickChoices, Dictionary<Guid, bool> result)
        {
            foreach (string entry in userBrickChoices.Split(new[] { EntrySeparator }, StringSplitOptions.RemoveEmptyEntries))
            {
                int i = entry.IndexOf(ValueDelimiter);
                if (i > 0)
                {
                    string idString = entry.Substring(0, i);
                    var brickId = Guid.Parse(idString);
                    var documentStructure = defaultStructure.FirstOrDefault(d => d.BrickId == brickId);
                    if (documentStructure != null)
                    {
                        string defaultValue = GetDefaultValue(documentStructure);
                        string value = entry.Substring(i + 1);
                        if (defaultValue != value)
                        {
                            result[brickId] = string.Equals(value, TrueValue, StringComparison.InvariantCultureIgnoreCase);
                        }
                    }
                }
            }
        }

        private static void GetUserBricksDictionaryRecursive(IEnumerable<UserBrickChoiceViewModel> userBrickOptions, IDictionary<Guid, bool> result)
        {
            foreach (var userBrickOptionVm in userBrickOptions)
            {
                if (userBrickOptionVm.BrickId != null)
                {
                    result[userBrickOptionVm.BrickId.Value] = userBrickOptionVm.IsSelected;
                }
                else if (userBrickOptionVm.IsGroup)
                {
                    GetUserBricksDictionaryRecursive(userBrickOptionVm.Items, result);
                }
            }
        }

        private static string GetDefaultValue(DocumentStructure documentStructure)
        {
            return documentStructure.AutoBrickCreationType == AutoBrickCreationType.Always ? TrueValue : FalseValue;
        }
    }
}
