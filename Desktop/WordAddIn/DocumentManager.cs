﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.AddIn;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.Messages;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.OfficeNotificationBar.Extensions;
using Eurolook.WordAddIn.HotKeys;
using Eurolook.WordAddIn.Messages;
using Eurolook.WordAddIn.Ribbon;
using Eurolook.WordAddIn.TaskPane;
using Eurolook.WordAddIn.ViewModels;
using GalaSoft.MvvmLight.Messaging;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Microsoft.Win32;
using NotNullAttribute = JetBrains.Annotations.NotNullAttribute;

namespace Eurolook.WordAddIn
{
    public class DocumentManager : IDocumentManager, ICanLog
    {
        private static readonly object Lock = new object();
        private readonly IClientDatabaseManager _clientDatabaseManager;

        private readonly IWordAddinContext _addinContext;
        private readonly Application _application;
        private readonly IMessageService _messageService;
        private readonly IWordRibbonService _ribbonService;
        private readonly ITaskPaneManager _taskPaneManager;
        private readonly IHotKeyManager _hotKeyManager;
        private readonly IEnumerable<IDocumentOnOpenHandler> _documentOnOpenHandlers;
        private readonly Func<Document, DocumentViewModel> _documentViewModelCreateFunc;
        private readonly HashSet<DocumentViewModel> _documents = new HashSet<DocumentViewModel>();

        public DocumentManager(
            IWordAddinContext addinContext,
            IWordApplicationContext applicationContext,
            IMessageService messageService,
            IWordRibbonService ribbonService,
            ITaskPaneManager taskPaneManager,
            IHotKeyManager hotKeyManager,
            IClientDatabaseManager clientDatabaseManager,
            IEnumerable<IDocumentOnOpenHandler> documentOnOpenHandlers,
            Func<Document, DocumentViewModel> documentViewModelCreateFunc)
        {
            _addinContext = addinContext;
            _application = applicationContext.Application;
            _messageService = messageService;
            _ribbonService = ribbonService;
            _taskPaneManager = taskPaneManager;
            _hotKeyManager = hotKeyManager;
            _clientDatabaseManager = clientDatabaseManager;
            _documentOnOpenHandlers = documentOnOpenHandlers;
            _documentViewModelCreateFunc = documentViewModelCreateFunc;

            Messenger.Default.Register<ProfileCreationCompletedMessage>(this, OnProfileCreationCompleted);
            Messenger.Default.Register<DocumentUpgradedMessage>(this, OnDocumentUpgraded);
            Messenger.Default.Register<CurrentDocumentLanguageChangedMessage>(this, OnCurrentDocumentLanguageChanged);
            Messenger.Default.Register<DataSyncCompletedMessage>(this, OnDataSyncCompleted);

            RegisterApplicationEvents();
            ConfigureTrustedLocationsForTemplates();
        }

        ~DocumentManager()
        {
            Dispose(false);
        }

        [NotNull]
        public IEnumerable<DocumentViewModel> DocumentViewModels
        {
            get
            {
                lock (Lock)
                {
                    RemoveClosedDocuments();
                    return _documents;
                }
            }
        }

        [CanBeNull]
        public DocumentViewModel GetOrCreateDocumentViewModel(Document document)
        {
            lock (Lock)
            {
                RemoveClosedDocuments();

                if (document == null)
                {
                    return null;
                }

                // return existing view model
                var documentViewModel = _documents.FirstOrDefault(d => d.IsValid && string.Equals(d.Document.FullName, document.FullName));
                if (documentViewModel != null)
                {
                    return documentViewModel;
                }

                this.LogDebugFormat("Creating Eurolook document for document {0}", document.FullName);

                // create a new view model
                documentViewModel = _documentViewModelCreateFunc(document);
                _documents.Add(documentViewModel);

                if (documentViewModel.IsEurolookDocument)
                {
                    AttachEurolookTemplate(document);
                    _hotKeyManager.ActivateHotKeysForDocument(document);
                    ExecuteOnOpenHandlers(documentViewModel);
                }

                _hotKeyManager.ActivateGlobalHotKeys(_addinContext, _application);

                return documentViewModel;
            }
        }

        private void ExecuteOnOpenHandlers(DocumentViewModel documentViewModel)
        {
            foreach (var documentOnOpenHandlers in _documentOnOpenHandlers.OrderBy(h => h.ExecutionOrder))
            {
                try
                {
                    documentOnOpenHandlers.HandleAsync(documentViewModel);
                }
                catch (Exception ex)
                {
                    this.LogError(ex);
                }
            }
        }

        [CanBeNull]
        public DocumentViewModel GetActiveDocumentViewModel()
        {
            var document = _application.GetActiveDocument();
            return GetOrCreateDocumentViewModel(document);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        [SuppressMessage(
            "ReSharper",
            "UnusedParameter.Local",
            Justification = "This method signature is according to the IDisposable pattern.")]
        protected virtual void Dispose(bool disposing)
        {
            UnregisterApplicationEvents();
        }

        private void OnProfileCreationCompleted(ProfileCreationCompletedMessage message)
        {
            lock (Lock)
            {
                try
                {
                    RemoveClosedDocuments();
                    RecreateDocumentViewModels(true);
                    ActivateDocumentViewModel();
                }
                catch (COMException ex)
                {
                    this.LogError(ex);
                }
            }
        }

        private void OnDocumentUpgraded(DocumentUpgradedMessage message)
        {
            lock (Lock)
            {
                try
                {
                    RemoveClosedDocuments();
                    RecreateDocumentViewModel(message.DocumentViewModel, message.ActivateBricksTab);
                    ActivateDocumentViewModel();
                }
                catch (COMException ex)
                {
                    this.LogError(ex);
                }
            }
        }

        private void OnCurrentDocumentLanguageChanged(CurrentDocumentLanguageChangedMessage message)
        {
            lock (Lock)
            {
                try
                {
                    RecreateDocumentViewModel(GetActiveDocumentViewModel(), false);
                    ActivateDocumentViewModel();
                }
                catch (COMException ex)
                {
                    this.LogError(ex);
                }
            }
        }

        private void OnDataSyncCompleted(DataSyncCompletedMessage message)
        {
            ////lock (Lock)
            ////{
            ////    try
            ////    {
            ////        RemoveClosedDocuments();
            ////        RecreateDocumentViewModels(false);
            ////    }
            ////    catch (COMException ex)
            ////    {
            ////        this.LogError(ex);
            ////    }
            ////}
        }

        /// <summary>
        /// The Application.DocumentChanged is fired every time the user switches between documents.<br />
        /// It is also fired when a new document is created. Because there is no dedicated "DocumentCreated" event
        /// this method serves as "DocumentCreated" event.
        /// </summary>
        private void OnApplicationDocumentChanged()
        {
            try
            {
                AsyncHelper.EnsureSynchronizationContext();

                _clientDatabaseManager.EnsureCreated();

                _ribbonService.InvalidateRibbon();
                var activeWindow = _application.GetActiveWindow();
                var eurolookTaskPane = _taskPaneManager.GetOrCreateTaskPane(activeWindow);

                if (_application.IsProtectedView() && eurolookTaskPane != null)
                {
                    eurolookTaskPane.IsVisible = false;
                    return;
                }

                var previousDocumentViewModel = eurolookTaskPane?.TaskPaneViewModel.DocumentViewModel;

                var documentViewModel = GetActiveDocumentViewModel();
                ActivateDocumentViewModel(documentViewModel);

                if (eurolookTaskPane != null)
                {
                    eurolookTaskPane.TaskPaneViewModel.DocumentViewModel = documentViewModel;

                    // switch to Bricks tab if the task pane belongs to a newly opened document
                    bool isNewlyOpened = previousDocumentViewModel == null || !previousDocumentViewModel.IsValid;
                    if (isNewlyOpened)
                    {
                        if (eurolookTaskPane.TaskPaneViewModel.TrySwitchingToDefaultTab()
                            && documentViewModel != null
                            && documentViewModel.IsNewlyCreated)
                        {
                            eurolookTaskPane.IsVisible = true;
                        }

                        if (documentViewModel != null && documentViewModel.IsLockedForEditing)
                        {
                            // document is opened by another user
                            eurolookTaskPane.IsVisible = false;
                        }
                    }

                    if (!activeWindow.CanHaveTaskPane())
                    {
                        eurolookTaskPane.IsVisible = false;
                    }
                }
            }
            catch (COMException comEx) when (comEx.IsLockedOrReadonlyException())
            {
                // Ignoring
                this.LogWarn(comEx);
            }
            catch (COMException comEx) when (comEx.HasObjectBeenDeletedException())
            {
                // Ignoring
                this.LogWarn(comEx);
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                Execute.OnUiThread(
                    () =>
                    {
                        _messageService.ShowDebugError(ex);
                    });
            }
        }

        /// <summary>
        /// The Application.DocumentOpen is fired when a document is opened from within Word.<br />
        /// It is also fired when you double-click a document in Windows explorer AND an instance of Word is already
        /// running.
        /// <br />
        /// It is NOT fired when you double-click a document in Windows explorer and Word isn't running.
        /// In this case a new Word process is started, it loads a new instance of the addin and the OnStartup event is
        /// fired.
        /// <br />
        /// The event also fires when the user "enables" a document in read-only mode (Protected View).
        /// </summary>
        private async void OnApplicationDocumentOpen(Document document)
        {
            try
            {
                AsyncHelper.EnsureSynchronizationContext();

                this.LogDebugFormat("Document opened: {0}", document.FullName);

                _clientDatabaseManager.EnsureCreated();

                if (_application.IsProtectedView())
                {
                    // When the current Window is in Protected View, the user clicked "Enable Editing"
                    // Word recycles the window of the default document (which already has a task pane)
                    // and opens this document in the recycled window.

                    // Remove the "wrong" task pane, it will be added in the
                    // following Application.DocumentChanged event in a proper way.
                    _taskPaneManager.RemoveTaskPane(document);
                }

                // The template-based add-ins might not have been loaded in case the document was previously
                // shown in reading layout. Retry to load them here. Unfortunately, there is no event when Word
                // leaves reading layout, so this is not optimal, but we do as best as we can.
                await _ribbonService.TryLoadGlobalAddInAsync();
                await _ribbonService.TryLoadQuickAccessToolbarAsync();
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                Execute.OnUiThread(
                    () =>
                    {
                        _messageService.ShowDebugError(ex);
                    });
            }
        }

        /// <summary>
        /// The Application.DocumentBeforeClose event is fired before a document is closed.
        /// </summary>
        /// <param name="doc">The document about to be closed.</param>
        /// <param name="cancel">A ref parameter to cancel closing of the document.</param>
        /// <remarks>
        /// When a Eurolook document is closed we make sure that the attached Eurolook.dotm is flagged as not
        /// modified. The attached template might have been modified by other third-party add-ins that do not use their
        /// own CustomizationContext but instead pollute the attached Eurolook.dotm template.
        /// </remarks>
        private void OnApplicationDocumentBeforeClose(Document doc, ref bool cancel)
        {
            try
            {
                AsyncHelper.EnsureSynchronizationContext();

                if (_application.IsObjectValid[doc])
                {
                    string templatePath = _addinContext.GlobalTemplatePath;
                    string templateName = Path.GetFileName(templatePath);

                    // ReSharper disable once UseIndexedProperty
                    var attachedTemplate = (Template)doc.get_AttachedTemplate();
                    var attachedTemplateName = attachedTemplate.Name;

                    if (string.Equals(templateName, attachedTemplateName, StringComparison.CurrentCultureIgnoreCase))
                    {
                        attachedTemplate.Saved = true;
                    }

                    // make sure none of the Eurolook templates pops up a message to be saved
                    foreach (var template in
                        doc.Application.Templates.OfType<Template>()
                                   .Where(t => t.Name.StartsWith("EL_Brick", StringComparison.InvariantCultureIgnoreCase)
                                               || t.Name.StartsWith("EurolookQuickAccessToolbar", StringComparison.InvariantCultureIgnoreCase)
                                               || t.Name.StartsWith("EurolookGlobalAddin", StringComparison.InvariantCultureIgnoreCase)
                                               || Guid.TryParse(Path.GetFileNameWithoutExtension(t.Name), out _)))
                    {
                        if (!template.Saved)
                        {
                            template.Saved = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private void ConfigureTrustedLocationsForTemplates()
        {
            string key = $@"Software\Microsoft\Office\{_application.Version}\Word\Security\Trusted Locations";
            using (var regKey = Registry.CurrentUser.OpenSubKey(key, true))
            {
                if (regKey == null)
                {
                    return;
                }

                foreach (string subKeyName in regKey.GetSubKeyNames())
                {
                    using (var subKey = regKey.OpenSubKey(subKeyName, true))
                    {
                        if (subKey != null && (string)subKey.GetValue("Path") == _addinContext.GlobalTemplateFolder)
                        {
                            return;
                        }
                    }
                }

                using (var location = regKey.CreateSubKey("Location" + regKey.SubKeyCount))
                {
                    if (location != null)
                    {
                        location.SetValue("Path", _addinContext.GlobalTemplateFolder, RegistryValueKind.String);
                        location.SetValue("Description", "", RegistryValueKind.String);
                        location.SetValue("AllowSubfolders", 1, RegistryValueKind.DWord);
                    }
                }
            }
        }

        private void AttachEurolookTemplate(Document document)
        {
            string templatePath = _addinContext.GlobalTemplatePath;
            document.AttachTemplate(templatePath);
        }

        private void RegisterApplicationEvents()
        {
            _application.DocumentOpen += OnApplicationDocumentOpen;
            _application.DocumentChange += OnApplicationDocumentChanged;
            _application.DocumentBeforeClose += OnApplicationDocumentBeforeClose;
        }

        private void UnregisterApplicationEvents()
        {
            _application.DocumentOpen -= OnApplicationDocumentOpen;
            _application.DocumentChange -= OnApplicationDocumentChanged;
            _application.DocumentBeforeClose -= OnApplicationDocumentBeforeClose;
        }

        private void RecreateDocumentViewModels(bool activateDefaultTab)
        {
            foreach (var document in _documents.ToList())
            {
                RecreateDocumentViewModel(document, activateDefaultTab);
            }
        }

        private void RecreateDocumentViewModel(DocumentViewModel documentViewModel, bool activateDefaultTab)
        {
            if (!documentViewModel.IsValid)
            {
                return;
            }

            var document = documentViewModel.Document;

            var oldDocumentViewModel = documentViewModel;
            var newDocumentViewModel = _documentViewModelCreateFunc(document);

            _documents.Remove(documentViewModel);
            _documents.Add(newDocumentViewModel);

            foreach (var window in document.Windows.OfType<Window>())
            {
                var eurolookTaskPane = _taskPaneManager.GetOrCreateTaskPane(window);
                if (eurolookTaskPane == null)
                {
                    continue;
                }

                eurolookTaskPane.TaskPaneViewModel.DocumentViewModel = newDocumentViewModel;
                if (activateDefaultTab)
                {
                    eurolookTaskPane.TaskPaneViewModel.TrySwitchingToDefaultTab();
                }
            }

            oldDocumentViewModel.Dispose();
        }

        private void RemoveClosedDocuments()
        {
            try
            {
                var closedDocumentViewModels = _documents.Where(d => !d.IsValid).ToList();
                foreach (var closedDocumentViewModel in closedDocumentViewModels)
                {
                    closedDocumentViewModel.Dispose();
                    _documents.Remove(closedDocumentViewModel);
                }
            }
            catch (COMException ex)
            {
                this.LogError(ex);
            }
        }

        private void ActivateDocumentViewModel(DocumentViewModel documentViewModelToActivate = null)
        {
            documentViewModelToActivate ??= GetActiveDocumentViewModel();

            foreach (var documentViewModel in _documents)
            {
                documentViewModel.IsActive = documentViewModelToActivate == documentViewModel;
            }
        }
    }
}
