using System;
using Eurolook.AddIn.Common;

namespace Eurolook.WordAddIn.Messages
{
    public class ShowBrickGroupPanelMessage
    {
        public IEurolookDocument EurolookDocument { get; set; }

        public Guid? GroupId { get; set; }
    }
}
