using Eurolook.WordAddIn.ViewModels;

namespace Eurolook.WordAddIn.Messages
{
    public class LeaveBrickGroupPanelMessage
    {
        public DocumentViewModel DocumentViewModel { get; set; }
    }
}
