using Eurolook.WordAddIn.ViewModels;

namespace Eurolook.WordAddIn.Messages
{
    public class DocumentUpgradedMessage
    {
        public DocumentUpgradedMessage(DocumentViewModel documentViewModel, bool activateBricksTab = false)
        {
            DocumentViewModel = documentViewModel;
            ActivateBricksTab = activateBricksTab;
        }

        public DocumentViewModel DocumentViewModel { get; }

        public bool ActivateBricksTab { get; }
    }
}
