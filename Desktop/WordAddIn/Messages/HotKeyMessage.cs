using System;
using Eurolook.WordAddIn.ViewModels;

namespace Eurolook.WordAddIn.Messages
{
    public class HotKeyMessage
    {
        public DocumentViewModel DocumentViewModel { get; set; }

        public Type CommandBrickType { get; set; }

        public string Parameter { get; set; }

        public bool IsGlobalHotKey { get; set; }
    }
}
