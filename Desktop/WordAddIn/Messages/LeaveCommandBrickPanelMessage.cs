using Eurolook.WordAddIn.ViewModels;

namespace Eurolook.WordAddIn.Messages
{
    public class LeaveCommandBrickPanelMessage
    {
        public ICommandBrickPanel CommandBrickPanel { get; set; }
    }
}
