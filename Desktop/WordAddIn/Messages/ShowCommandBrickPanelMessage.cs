using Eurolook.WordAddIn.ViewModels;

namespace Eurolook.WordAddIn.Messages
{
    public class ShowCommandBrickPanelMessage
    {
        public ICommandBrickPanel CommandBrickPanel { get; set; }
    }
}
