using System.Threading.Tasks;
using Eurolook.AddIn.Common.Database;

namespace Eurolook.WordAddIn
{
    internal class SessionContext : ISessionContext
    {
        private readonly IStatisticsRepository _statisticsRepository;

        public SessionContext(IStatisticsRepository statisticsRepository)
        {
            _statisticsRepository = statisticsRepository;
        }

        public bool IsSessionActive { get; private set; }

        public async Task SetSessionActive()
        {
            if (!IsSessionActive)
            {
                IsSessionActive = true;
                await _statisticsRepository.IncreaseSessionCountAsync();
            }
        }
    }
}
