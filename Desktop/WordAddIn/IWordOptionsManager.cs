﻿namespace Eurolook.WordAddIn
{
    public interface IWordOptionsManager
    {
        void ApplyAllOptions();
    }
}
