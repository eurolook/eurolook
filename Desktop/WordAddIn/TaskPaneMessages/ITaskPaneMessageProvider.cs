using System.Collections.Generic;
using Eurolook.WordAddIn.ViewModels;

namespace Eurolook.WordAddIn.TaskPaneMessages
{
    public interface ITaskPaneMessageProvider
    {
        IEnumerable<TaskPaneMessageViewModel> GetMessages(DocumentViewModel documentViewModel);
    }
}
