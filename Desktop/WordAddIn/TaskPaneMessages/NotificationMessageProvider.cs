﻿using System;
using System.Collections.Generic;
using System.Windows.Media.Imaging;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.ViewModels;

namespace Eurolook.WordAddIn.TaskPaneMessages
{
    public class NotificationMessageProvider : ITaskPaneMessageProvider, ICanLog
    {
        private readonly INotificationCompatibilityChecker _notificationCompatibilityChecker;
        private readonly INotificationRepository _notificationRepository;

        public NotificationMessageProvider(
            INotificationCompatibilityChecker notificationCompatibilityChecker,
            INotificationRepository notificationRepository)
        {
            _notificationCompatibilityChecker = notificationCompatibilityChecker;
            _notificationRepository = notificationRepository;
        }

        public IEnumerable<TaskPaneMessageViewModel> GetMessages(DocumentViewModel documentViewModel)
        {
            var result = new List<TaskPaneMessageViewModel>();
            foreach (var notification in _notificationRepository.GetActiveNotifications(_notificationCompatibilityChecker))
            {
                var vm = new TaskPaneMessageViewModel
                {
                    ReferenceId = notification.Id,
                    Subject = notification.DisplayName,
                    Body = notification.Content,
                    Priority = notification.Priority,
                    IsHighlighted = notification.Priority == NotificationPriority.High,
                    ShowClearButton = true,
                    OnClearAction = OnClear,
                };

                if (notification.ThumbnailBytes != null && notification.ThumbnailBytes.Length > 0)
                {
                    vm.Thumbnail = new BitmapImage().InitAndFreeze(notification.ThumbnailBytes);
                }

                result.Add(vm);
            }

            return result;
        }

        public void OnClear(TaskPaneMessageViewModel taskPaneViewModel)
        {
            _notificationRepository.TryClearNotification(taskPaneViewModel.ReferenceId);
        }
    }
}
