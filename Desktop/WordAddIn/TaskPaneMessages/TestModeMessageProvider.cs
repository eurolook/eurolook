﻿using System.Collections.Generic;
using Eurolook.AddIn.Common;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.ViewModels;

namespace Eurolook.WordAddIn.TaskPaneMessages
{
    public class TestModeMessageProvider : ITaskPaneMessageProvider
    {
        private readonly ISettingsService _settingsService;

        public TestModeMessageProvider(ISettingsService settingsService)
        {
            _settingsService = settingsService;
        }

        public IEnumerable<TaskPaneMessageViewModel> GetMessages(DocumentViewModel documentViewModel)
        {
            if (_settingsService.IsInTestMode)
            {
                yield return new TaskPaneMessageViewModel
                {
                    Subject = "TEST MODE ENABLED",
                    Body = "Test Mode has been enabled for this Eurolook session. Eurolook will behave differently in this mode. You can disable this mode in the Tools tab.",
                    Priority = NotificationPriority.Low,
                };
            }
        }
    }
}
