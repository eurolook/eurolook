﻿using System;
using System.Collections.Generic;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing.OpenXml;

namespace Eurolook.WordAddIn.TaskPaneMessages;

public class TaskPaneMessageProvider : ICanLog
{
    private readonly Func<Language, ILocalisedResourceResolver> _localisedResourceResolverCreateFunc;

    private readonly Dictionary<string, string> _resolvedTexts = new Dictionary<string, string>();

    public TaskPaneMessageProvider(
        Func<Language, ILocalisedResourceResolver> localisedResourceResolverCreateFunc)
    {
        _localisedResourceResolverCreateFunc = localisedResourceResolverCreateFunc;
    }

    public string ResolveText(string alias)
    {
        if (_resolvedTexts.ContainsKey(alias))
        {
            return _resolvedTexts[alias];
        }

        try
        {
            var resourceResolver = _localisedResourceResolverCreateFunc(null);
            var text = resourceResolver.ResolveTranslation(alias)?.Value;
            if (text != null)
            {
                _resolvedTexts[alias] = text;
            }

            return text;
        }
        catch (Exception ex)
        {
            this.LogWarn(ex);
        }

        return null;
    }
}
