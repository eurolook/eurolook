using System.Collections.Generic;
using Eurolook.WordAddIn.ViewModels;

namespace Eurolook.WordAddIn.TaskPaneMessages
{
    public class DefaultTaskPaneMessageProvider : ITaskPaneMessageProvider
    {
        public IEnumerable<TaskPaneMessageViewModel> GetMessages(DocumentViewModel documentViewModel)
        {
            // no message by default
            yield break;
        }
    }
}
