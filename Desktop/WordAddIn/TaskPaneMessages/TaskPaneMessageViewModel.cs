﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;
using Eurolook.AddIn.Common.ClipboardTools;
using Eurolook.AddIn.Common.Properties;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.ViewModels;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Eurolook.WordAddIn.TaskPaneMessages
{
    public class TaskPaneMessageViewModel : ViewModelBase
    {
        public TaskPaneMessageViewModel()
        {
            ClearCommand = new RelayCommand(Clear);
        }

        public BitmapImage Thumbnail { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public bool ShowClearButton { get; set; }

        public RelayCommand ClearCommand { get; }

        public Action<TaskPaneMessageViewModel> OnClearAction { get; set; }

        public Guid ReferenceId { get; internal set; }

        public TaskPaneViewModel ParentTaskPaneViewModel { get; set; }

        public bool IsHighlighted { get; set; }

        public NotificationPriority Priority { get; set; }

        public void Clear()
        {
            if (ParentTaskPaneViewModel?.Messages != null && ParentTaskPaneViewModel.Messages.Contains(this))
            {
                ParentTaskPaneViewModel.Messages.Remove(this);
                OnClearAction?.Invoke(this);
            }
        }
    }
}
