﻿using System;
using System.Collections.Generic;
using Eurolook.AddIn.Common;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.Startup;
using Eurolook.WordAddIn.ViewModels;

namespace Eurolook.WordAddIn.TaskPaneMessages
{
    public class RegionalSettingsMessageProvider : ITaskPaneMessageProvider
    {
        private readonly RegionalSettingsCheck _regionalSettingsCheck;
        private readonly string _helpLink;

        public RegionalSettingsMessageProvider(
            ISettingsService settingsService,
            RegionalSettingsCheck regionalSettingsCheck)
        {
            _regionalSettingsCheck = regionalSettingsCheck;
            _helpLink = settingsService.GetOnlineHelpLink("RegionalSettings");
        }

        public IEnumerable<TaskPaneMessageViewModel> GetMessages(DocumentViewModel documentViewModel)
        {
            var link = Uri.IsWellFormedUriString(_helpLink, UriKind.Absolute) ? new Uri(_helpLink) : new Uri("file://");
            _regionalSettingsCheck.Execute();
            if (!_regionalSettingsCheck.IsExpectedListSeparator)
            {
                yield return new TaskPaneMessageViewModel
                {
                    ShowClearButton = true,
                    Subject = "Language Settings Check",
                    Body = $"Problems with Word fields can be solved by [changing your regional settings.|{link}]",
                    Priority = NotificationPriority.Low,
                };
            }
        }
    }
}
