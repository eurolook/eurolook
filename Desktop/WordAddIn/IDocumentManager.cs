using System;
using System.Collections.Generic;
using Eurolook.WordAddIn.ViewModels;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn
{
    public interface IDocumentManager : IDisposable
    {
        IEnumerable<DocumentViewModel> DocumentViewModels { get; }

        DocumentViewModel GetOrCreateDocumentViewModel(Document document);

        DocumentViewModel GetActiveDocumentViewModel();
    }
}
