using System;
using System.Threading;
using Eurolook.Common.Log;

namespace Eurolook.WordAddIn
{
    public class ScheduledActionWithDelay : IDisposable, ICanLog
    {
        private const int DefaultMinDelayMs = 1000;
        private const int DefaultMaxDelayMs = 32000;

        private readonly IWordApplicationContext _applicationContext;
        private readonly Action _action;
        private Timer _updateTimer;
        private int _currentDelayMs = 1000;

        public ScheduledActionWithDelay(IWordApplicationContext applicationContext, Action action)
        {
            _applicationContext = applicationContext;
            _action = action;

            _updateTimer = new Timer(TimerCallback);

            _applicationContext.InputDetector.InputDetectedEvent += OnInputDetected;
        }

        public void RequestExecution()
        {
            ////this.LogTrace("Requesting execution.");
            TriggerNextUpdate(DefaultMinDelayMs);
        }

        public void Dispose()
        {
            _updateTimer?.Dispose();
            _updateTimer = null;
            _applicationContext.InputDetector.InputDetectedEvent -= OnInputDetected;
        }

        public void StopScheduler()
        {
            ////this.LogTrace("Stopping scheduler.");
            _updateTimer?.Change(Timeout.Infinite, Timeout.Infinite);
        }

        private void OnInputDetected(object sender, EventArgs e)
        {
            TriggerNextUpdate(DefaultMinDelayMs);
        }

        private void TriggerNextUpdate(int dueTime)
        {
            try
            {
                if (dueTime > 0)
                {
                    ////this.LogTrace($"Postponing execution by {dueTime} ms.");
                    _currentDelayMs = dueTime;
                }

                _updateTimer?.Change(dueTime, Timeout.Infinite);
            }
            catch (ObjectDisposedException)
            {
                // viewmodel has been disposed
            }
        }

        private void TimerCallback(object state)
        {
            try
            {
                ////this.LogTrace("Executing action.");
                TriggerNextUpdate(Timeout.Infinite);

                _action();
            }
            catch (Exception ex)
            {
                this.LogErrorFormat("Scheduled action failed.\n{0}", ex);
            }
            finally
            {
                TriggerNextUpdate(Math.Min(_currentDelayMs * 2, DefaultMaxDelayMs));
            }
        }
    }
}
