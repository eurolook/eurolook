﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.Extensions;
using JetBrains.Annotations;
using Microsoft.CSharp.RuntimeBinder;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn
{
    [Flags]
    public enum DocumentAutomationOption
    {
        // ReSharper disable once UnusedMember.Global
        None = 0x0,
        PreventScreenUpdating = 0x1,
        EnableUndo = 0x2,
        RestoreSelection = 0x4,
        HandleMultiSelection = 0x8,
        HandleSelectionCoversContentControl = 0x10,
        HandleViewType = 0x20,
        ShowWaitCursor = 0x40,
        ScrollIntoView = 0x80,
        UnlockContents = 0x100,
        DisableTrackRevisions = 0x200,
        HideRevisionsAndComments = 0x400,
        SetLanguageDetected = 0x800,
        HandleReadingView = 0x1000,
        CreateDummyDocumentInProtectedOrReadingView = 0x2000,
        HandleContentControlPlaceholderText = 0x4000,
        DisableAlerts = 0x8000,

        Default = EnableUndo | PreventScreenUpdating | HandleMultiSelection | HandleReadingView
                  | ShowWaitCursor | ScrollIntoView | SetLanguageDetected,
    }

    public sealed class DocumentAutomationHelper : IDisposable, ICanLog
    {
        private static readonly Stack<DocumentAutomationHelper> DocumentAutomationHelperStack =
            new Stack<DocumentAutomationHelper>();

        private readonly Application _application;

        [CanBeNull]
        private readonly Document _document;

        [CanBeNull]
        private readonly Document _dummyDocument;

        private readonly DocumentAutomationOption _flags;
        private readonly bool _originalScreenUpdating;
        private readonly WdAlertLevel _originalAlertLevel;
        private readonly Range _originalSelection;
        private readonly WdCursorType? _originalCursor;
        private readonly ContentControlUnlocker _contentControlUnlocker;
        private readonly ContentControlPlaceholderBackup _contentControlPlaceholderBackup;

        private readonly bool _originalTrackRevisions;
        private readonly List<RevisionViewSettings> _originalRevisionViewSettings;
        private readonly List<Window> _windowsInReadingView;

        private readonly WdViewType _preferredViewType = WdViewType.wdPrintView;

        private readonly UndoRecord _undoRecord;

        private Range _selectRangeWhenDone;

        public DocumentAutomationHelper(
            [NotNull] Document document,
            string undoRecordName = null,
            DocumentAutomationOption flags = DocumentAutomationOption.Default)
            : this(document.Application, document, undoRecordName, flags)
        {
        }

        public DocumentAutomationHelper(
            [NotNull] Application application,
            string undoRecordName = null,
            DocumentAutomationOption flags = DocumentAutomationOption.Default)
            : this(application, application.GetSelection()?.Document, undoRecordName, flags)
        {
        }

        public DocumentAutomationHelper(
            [NotNull] Range range,
            string undoRecordName = null,
            DocumentAutomationOption flags = DocumentAutomationOption.Default)
            : this(range.Application, range.Document, undoRecordName, flags)
        {
        }

        public DocumentAutomationHelper(
            [NotNull] IBrickExecutionContext context,
            string undoRecordName = null,
            DocumentAutomationOption flags = DocumentAutomationOption.Default)
            : this(context.Application, context.Document, undoRecordName, flags)
        {
        }

        public DocumentAutomationHelper(
            Application application,
            Document document,
            string undoRecordName = null,
            DocumentAutomationOption flags = DocumentAutomationOption.Default)
        {
            _application = application;
            _document = document;
            _flags = flags;
            _originalScreenUpdating = _application.ScreenUpdating;
            _originalSelection = _application.GetSelection()?.Range.Duplicate;
            _originalAlertLevel = _application.DisplayAlerts;

            try
            {
                if (_document?.TryGetVariableAsEnum("PreferredViewType", out _preferredViewType) == false
                    && _document?.Windows.Count > 0)
                {
                    _preferredViewType = _document.Windows[1].View.Type;
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }

            DisableScreenUpdating();
            DisableAlerts();

            _dummyDocument = CreateDummyDocumentInProtectedOrReadingView();

            if (_flags.HasFlag(DocumentAutomationOption.HandleReadingView))
            {
                // Switch all windows in reading view to normal view
                // because of this Word issue:
                // https://blogs.msmvps.com/wordmeister/2013/02/22/word2013bug-not-available-for-reading/
                _windowsInReadingView = GetWindowsInReadingView();

                foreach (var window in _windowsInReadingView)
                {
                    window.View.Type = WdViewType.wdNormalView;
                }
            }

            if (_flags.HasFlag(DocumentAutomationOption.DisableTrackRevisions) && document.TrackRevisions)
            {
                _originalTrackRevisions = document.TrackRevisions;
                document.TrackRevisions = false;
            }

            if (_flags.HasFlag(DocumentAutomationOption.HideRevisionsAndComments))
            {
                _originalRevisionViewSettings = HideRevisionsAndComments();
            }

            if (_flags.HasFlag(DocumentAutomationOption.ShowWaitCursor))
            {
                _originalCursor = _application.System.Cursor;
                _application.System.Cursor = WdCursorType.wdCursorWait;
            }

            if (_flags.HasFlag(DocumentAutomationOption.UnlockContents))
            {
                _contentControlUnlocker = new ContentControlUnlocker(document);
            }

            HandleDiscontiguousSelection();

            HandleSelectionPartiallyCoversPlaintextContentControl();

            // start undo record but avoid nested transactions
            if (_flags.HasFlag(DocumentAutomationOption.EnableUndo) && !IsInUndoTransaction)
            {
                _undoRecord = _application.UndoRecord;
                _undoRecord.StartCustomRecord(undoRecordName ?? "Automation action");
            }

            if (_flags.HasFlag(DocumentAutomationOption.HandleContentControlPlaceholderText))
            {
                _contentControlPlaceholderBackup =
                    new ContentControlPlaceholderBackup(_originalSelection?.ParentContentControl);
            }

            DocumentAutomationHelperStack.Push(this);
        }

        private List<Window> GetWindowsInReadingView()
        {
            var windowsInReadingView = _application
                                       .WindowsEnumerable()
                                       .Where(w => w.View.Type == WdViewType.wdReadingView);

            var protectedWindowsInReadingView = _application
                                                .ProtectedViewWindows.OfType<ProtectedViewWindow>()
                                                .Select(w => w.Document.ActiveWindow).Where(
                                                    w => w.View.Type == WdViewType.wdReadingView);

            return windowsInReadingView.Union(protectedWindowsInReadingView).ToList();
        }

        /// <summary>
        /// Gets a value indicating whether a DocumentAutomationHelper is currently used,
        /// i.e. whether a document is being automated.
        /// </summary>
        public static bool IsActive
        {
            get { return DocumentAutomationHelperStack.Any(); }
        }

        public WdViewType PreferredViewType
        {
            get { return _preferredViewType; }
        }

        private bool IsInUndoTransaction
        {
            get { return DocumentAutomationHelperStack.Any(dah => dah._undoRecord != null); }
        }

        /// <summary>
        /// The range passed is selected when the <see cref="DocumentAutomationHelper" /> instance is disposed.
        /// </summary>
        /// <param name="range">The range to be selected.</param>
        public void SelectWhenDone(Range range)
        {
            // just remember the range to select it later
            _selectRangeWhenDone = range;
        }

        public void Dispose()
        {
            DocumentAutomationHelperStack.Pop();

            HandleViewType();
            RestoreScreenUpdating();
            RestoreAlerts();
            RestoreSelection();
            RestoreCursor();
            ScrollIntoView();
            SetLanguageDetected();
            RestoreReadingView();
            CloseDummyDocument();

            _contentControlUnlocker?.Dispose();
            _contentControlPlaceholderBackup?.Dispose();

            _undoRecord?.EndCustomRecord();

            RestoreTrackRevisions();
            RestoreRevisionsAndComments();
        }

        private void RestoreReadingView()
        {
            if (_flags.HasFlag(DocumentAutomationOption.HandleReadingView))
            {
                // NOTE: Changing the view type of an inactive window might activate that window.
                // Therefore we keep track of the currently active window and make sure that is gets
                // activated again.
                var hWndForegroundWindow = SafeNativeMethods.GetForegroundWindow();
                foreach (var window in _windowsInReadingView)
                {
                    window.View.Type = WdViewType.wdReadingView;
                }

                if (hWndForegroundWindow != IntPtr.Zero)
                {
                    SafeNativeMethods.SetForegroundWindow(hWndForegroundWindow);
                }
            }
        }

        private void SetLanguageDetected()
        {
            if (_flags.HasFlag(DocumentAutomationOption.SetLanguageDetected))
            {
                _document?.ApplyToAllStoryRanges(r => r.LanguageDetected = true);
            }
        }

        private List<RevisionViewSettings> HideRevisionsAndComments()
        {
            var result = _document?.Windows.OfType<Window>().Select(w => new RevisionViewSettings(w)).ToList()
                         ?? new List<RevisionViewSettings>();
            foreach (var revisionViewSettings in result)
            {
                revisionViewSettings.Hide();
            }

            return result;
        }

        private void RestoreRevisionsAndComments()
        {
            if (_originalRevisionViewSettings == null)
            {
                return;
            }

            foreach (var originalRevisionViewSetting in _originalRevisionViewSettings)
            {
                try
                {
                    originalRevisionViewSetting.Restore();
                }
                catch (Exception ex)
                {
                    this.LogDebug(ex);
                }
            }
        }

        [CanBeNull]
        private Document CreateDummyDocumentInProtectedOrReadingView()
        {
            if (_flags.HasFlag(DocumentAutomationOption.CreateDummyDocumentInProtectedOrReadingView))
            {
                // Create a dummy document to prevent the following exception if Word is started in protected mode:
                //     This method or property is not available because this command is not available for reading.
                //     System.Runtime.InteropServices.COMException (0x800A11FD)
                int windowsCount = _application.Windows.Count;
                int protectedWindowsCount = _application.ProtectedViewWindows.Count;
                this.LogDebug($"Open windows: {windowsCount}; protected windows: {protectedWindowsCount}");

                if ((windowsCount == 0 && _application.Application.ProtectedViewWindows.Count > 0)
                    || GetWindowsInReadingView().Any())
                {
                    this.LogDebug("Adding dummy document in protected/reading view");
                    var dummyDocument = _application.Documents.Add(Visible: false);
                    dummyDocument.Activate();
                    return dummyDocument;
                }
            }

            return null;
        }

        private void HandleDiscontiguousSelection()
        {
            try
            {
                if (_flags.HasFlag(DocumentAutomationOption.HandleMultiSelection))
                {
                    // Word has limited functionality when working on discontiguous selections
                    // see http://support.microsoft.com/kb/288424
                    // detection of such a selection is unfortunately not possible in a reliable manner
                    _application.GetSelection()?.ShrinkDiscontiguousSelection();
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private void HandleSelectionPartiallyCoversPlaintextContentControl()
        {
            // handle an automation error if the selection partially covers a plaintext content control
            if (_document != null
                && _flags.HasFlag(DocumentAutomationOption.HandleSelectionCoversContentControl)
                && _application.GetSelection()?.Range.IsCoveringContentControl() == true)
            {
                var range = _document.Range();
                range.Collapse();
                range.Select();
            }
        }

        private void DisableScreenUpdating()
        {
            if (_flags.HasFlag(DocumentAutomationOption.PreventScreenUpdating))
            {
                _application.ScreenUpdating = false;
            }
        }

        private void DisableAlerts()
        {
            if (_flags.HasFlag(DocumentAutomationOption.DisableAlerts))
            {
                _application.DisplayAlerts = WdAlertLevel.wdAlertsNone;
            }
        }

        private void HandleViewType()
        {
            try
            {
                if (_document != null && _flags.HasFlag(DocumentAutomationOption.HandleViewType))
                {
                    var currentView = _document.Windows[1].View.Type;
                    if (currentView == PreferredViewType)
                    {
                        _document.DeleteVariable("PreferredViewType");
                    }
                    else if (currentView != PreferredViewType)
                    {
                        // view has been changed by an automation action, e.g. because a header on a page
                        // not present in the document has been selected
                        // we remember the "old" view and try to restore it next time
                        _document.SetVariable("PreferredViewType", PreferredViewType);
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private void CloseDummyDocument()
        {
            try
            {
                _dummyDocument?.Close(false);
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private void RestoreScreenUpdating()
        {
            try
            {
                if (_application != null && _flags.HasFlag(DocumentAutomationOption.PreventScreenUpdating))
                {
                    _application.ScreenUpdating = _originalScreenUpdating;

                    // NOTE: Commented _application.ScreenRefresh() as it can cause a Word crash when accessing
                    // Application.Documents afterwards (which is done in _application.GetSelection())
                    // this triggers a screen refresh
                    //// _application.ScreenRefresh();
                    _application.GetSelection()?.Select();
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private void RestoreAlerts()
        {
            try
            {
                if (_application != null && _flags.HasFlag(DocumentAutomationOption.DisableAlerts))
                {
                    _application.DisplayAlerts = _originalAlertLevel;
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private void RestoreSelection()
        {
            try
            {
                if (_application != null && _selectRangeWhenDone != null)
                {
                    _selectRangeWhenDone.SelectInPreferredView(PreferredViewType);
                    return;
                }

                if (_application != null && _originalSelection != null && _application.IsObjectValid[_originalSelection]
                    && _flags.HasFlag(DocumentAutomationOption.RestoreSelection))
                {
                    _originalSelection.SelectInPreferredView(PreferredViewType);
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private void RestoreCursor()
        {
            try
            {
                if (_application != null && _originalCursor != null
                                         && _flags.HasFlag(DocumentAutomationOption.ShowWaitCursor))
                {
                    _application.System.Cursor = _originalCursor.Value;
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private void RestoreTrackRevisions()
        {
            try
            {
                if (_document != null && _originalTrackRevisions
                                      && _flags.HasFlag(DocumentAutomationOption.DisableTrackRevisions))
                {
                    _document.TrackRevisions = _originalTrackRevisions;
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private void ScrollIntoView()
        {
            if (_document != null && _flags.HasFlag(DocumentAutomationOption.ScrollIntoView))
            {
                _application.Selection.Range.ScrollIntoViewIfNeeded();
            }
        }

        private sealed class ContentControlPlaceholderBackup : IDisposable
        {
            private readonly ContentControl _contentControl;
            private readonly BuildingBlock _contentControlPlaceholder;

            public ContentControlPlaceholderBackup(ContentControl contentControl)
            {
                _contentControl = contentControl;

                if (contentControl != null && contentControl.ShowingPlaceholderText && !contentControl.Temporary)
                {
                    _contentControlPlaceholder = contentControl.PlaceholderText;
                    _contentControl.SetPlaceholderText();
                }
            }

            public void Dispose()
            {
                try
                {
                    if (_contentControlPlaceholder != null)
                    {
                        _contentControl.SetPlaceholderText(_contentControlPlaceholder);
                    }
                }
                catch (COMException)
                {
                    // ignore
                }
            }
        }

        private class RevisionViewSettings
        {
            private readonly Window _window;
            private readonly bool _showRevisionsAndComments;
            private readonly int _revisionsFilterMarkup;
            private readonly int _revisionsFilterView;

            public RevisionViewSettings(Window window)
            {
                _window = window;
                _showRevisionsAndComments = _window.View.ShowRevisionsAndComments;

                try
                {
                    dynamic view = _window.View;

                    _revisionsFilterMarkup = view.RevisionsFilter.Markup;
                    _revisionsFilterView = view.RevisionsFilter.View;
                }
                catch (RuntimeBinderException)
                {
                    // this exception is expected on Word < 2013
                }
            }

            public void Hide()
            {
                try
                {
                    dynamic view = _window.View;

                    view.RevisionsFilter.Markup = 0; // wdRevisionsMarkupNone
                    view.RevisionsFilter.View = 0; // wdRevisionsViewFinal
                }
                catch (RuntimeBinderException)
                {
                    // this exception is expected on Word < 2013
                }

                _window.View.ShowRevisionsAndComments = false;
            }

            public void Restore()
            {
                _window.View.ShowRevisionsAndComments = _showRevisionsAndComments;

                try
                {
                    dynamic view = _window.View;

                    view.RevisionsFilter.Markup = _revisionsFilterMarkup;
                    view.RevisionsFilter.View = _revisionsFilterView;
                }
                catch (RuntimeBinderException)
                {
                    // this exception is expected on Word < 2013
                }
            }
        }
    }
}
