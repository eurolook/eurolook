using System;

namespace Eurolook.WordAddIn.Events
{
    public interface IWindowSelectionChangeEventHelper
    {
        event EventHandler<WindowSelectionChangeEventHelper.WindowSelectionChangeEventArgs> WindowSelectionChangeDelayed;

        int Delay { get; set; }
    }
}
