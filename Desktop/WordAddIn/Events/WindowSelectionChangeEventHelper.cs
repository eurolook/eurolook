using System;
using System.Runtime.InteropServices;
using System.Threading;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.AddIn;
using Eurolook.Common.Log;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.Events
{
    public class WindowSelectionChangeEventHelper : IWindowSelectionChangeEventHelper, IDisposable, ICanLog
    {
        private readonly object _lockSelectionChanged = new object();

        private readonly Application _application;
        private readonly Timer _updateTimer;

        private Selection _currentSelection;
        private bool _isTriggered;

        private EventHandler<WindowSelectionChangeEventArgs> _windowSelectionChangeEventHandler;

        public WindowSelectionChangeEventHelper(IWordApplicationContext applicationContext, int delay = 300)
        {
            _application = applicationContext.Application;
            Delay = delay;

            _updateTimer = new Timer(OnWindowSelectionChangeDelayed);
        }

        ~WindowSelectionChangeEventHelper()
        {
            Dispose(false);
        }

        public event EventHandler<WindowSelectionChangeEventArgs> WindowSelectionChangeDelayed
        {
            add
            {
                if (_windowSelectionChangeEventHandler == null)
                {
                    Register();
                }

                _windowSelectionChangeEventHandler += value;
            }
            remove
            {
                _windowSelectionChangeEventHandler -= value;
                if (_windowSelectionChangeEventHandler == null)
                {
                    Unregister();
                }
            }
        }

        public int Delay { get; set; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void OnRaiseWindowSelectionChangeEvent(WindowSelectionChangeEventArgs e)
        {
            _windowSelectionChangeEventHandler?.Invoke(_application, new WindowSelectionChangeEventArgs(_currentSelection));
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _updateTimer.Change(Timeout.Infinite, Timeout.Infinite);
                _updateTimer.Dispose();
            }
        }

        private void Register()
        {
            _application.WindowSelectionChange += OnWindowSelectionChange;
        }

        private void Unregister()
        {
            _application.WindowSelectionChange -= OnWindowSelectionChange;
        }

        private void OnWindowSelectionChange(Selection sel)
        {
            try
            {
                AsyncHelper.EnsureSynchronizationContext();

                lock (_lockSelectionChanged)
                {
                    _updateTimer.Change(Delay, Timeout.Infinite);

                    // remember the selection so that it can be accessed from the timer callback
                    _currentSelection = sel;
                }
            }
            catch (COMException)
            {
                // Silently handle exception
            }
        }

        private void OnWindowSelectionChangeDelayed(object state)
        {
            Execute.OnUiThread(
                () =>
                {
                    if (!_isTriggered)
                    {
                        lock (_lockSelectionChanged)
                        {
                            if (!_isTriggered)
                            {
                                try
                                {
                                    _isTriggered = true;
                                    Unregister();
                                    OnRaiseWindowSelectionChangeEvent(new WindowSelectionChangeEventArgs(_currentSelection));
                                }
                                catch (Exception ex)
                                {
                                    this.LogDebug(ex);
                                }
                                finally
                                {
                                    _isTriggered = false;
                                    Register();
                                }
                            }
                        }
                    }
                });
        }

        public class WindowSelectionChangeEventArgs : EventArgs
        {
            public WindowSelectionChangeEventArgs(Selection selection)
            {
                Selection = selection;
            }

            public Selection Selection { get; }
        }
    }
}
