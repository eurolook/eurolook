using System;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.Events
{
    public class OutlineLevelChangedEventArgs : EventArgs
    {
        public OutlineLevelChangedEventArgs(WdOutlineLevel oldOutlineLevel, WdOutlineLevel newOutlineLevel)
        {
            OldOutlineLevel = oldOutlineLevel;
            NewOutlineLevel = newOutlineLevel;
        }

        public WdOutlineLevel OldOutlineLevel { get; set; }

        public WdOutlineLevel NewOutlineLevel { get; set; }
    }
}
