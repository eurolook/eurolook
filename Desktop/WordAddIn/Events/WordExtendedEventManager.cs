using System;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Events;
using Microsoft.Office.Interop.Word;
using Point = System.Drawing.Point;

namespace Eurolook.WordAddIn.Events
{
    public sealed class WordExtendedEventManager : IDisposable
    {
        private readonly Application _application;

        private double _currentSelectionHorizontalPositionRelativeToPage = double.MinValue;
        private double _currentSelectionVerticalPositionRelativeToPage = double.MinValue;
        private double _currentSelectionHorizontalPositionRelativeToTextBoundary = double.MinValue;
        private double _currentSelectionVerticalPositionRelativeToTextBoundary = double.MinValue;
        private Point _currentCaretPosition = new Point(int.MinValue, int.MinValue);

        public WordExtendedEventManager(Application application)
        {
            _application = application;

            InputDetector = new InputDetector();
            InputDetector.InputDetectedEvent += OnInputDetected;
            InputDetector.PreviewMouseWheel += OnPreviewMouseWheel;
            InputDetector.MouseWheel += OnMouseWheel;
            InputDetector.PreviewKeyDown += OnPreviewKeyDown;
            InputDetector.KeyDown += OnKeyDown;
            InputDetector.PreviewKeyUp += OnPreviewKeyUp;
            InputDetector.KeyUp += OnKeyUp;
        }

        ~WordExtendedEventManager()
        {
            Dispose(false);
        }

        public delegate void SelectionPositionChangedEventHandler(object sender, SelectionPositionChangedEventArgs e);

        public delegate void CaretPositionChangedEventHandler(object sender, CaretPositionChangedEventArgs e);

        public delegate void MouseWheelEventHandler(object sender, MouseWheelEventArgs e);

        public delegate void KeyEventHandler(object sender, KeyEventArgs e);

        public event SelectionPositionChangedEventHandler SelectionPositionChangedEvent;

        public event CaretPositionChangedEventHandler CaretPositionChangedEvent;

        public event MouseWheelEventHandler PreviewMouseWheel;

        public event MouseWheelEventHandler MouseWheel;

        public event KeyEventHandler PreviewKeyDown;

        public event KeyEventHandler KeyDown;

        public event KeyEventHandler PreviewKeyUp;

        public event KeyEventHandler KeyUp;

        public InputDetector InputDetector { get; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void OnMouseWheel(object sender, MouseWheelEventArgs mouseWheelEventArgs)
        {
            var handler = MouseWheel;
            handler?.Invoke(this, mouseWheelEventArgs);
        }

        private void OnPreviewMouseWheel(object sender, MouseWheelEventArgs mouseWheelEventArgs)
        {
            var handler = PreviewMouseWheel;
            handler?.Invoke(this, mouseWheelEventArgs);
        }

        private void OnPreviewKeyUp(object sender, KeyEventArgs keyEventArgs)
        {
            var handler = PreviewKeyUp;
            handler?.Invoke(this, keyEventArgs);
        }

        private void OnKeyUp(object sender, KeyEventArgs keyEventArgs)
        {
            var handler = KeyUp;
            handler?.Invoke(this, keyEventArgs);
        }

        private void OnPreviewKeyDown(object sender, KeyEventArgs keyEventArgs)
        {
            var handler = PreviewKeyDown;
            handler?.Invoke(this, keyEventArgs);
        }

        private void OnKeyDown(object sender, KeyEventArgs keyEventArgs)
        {
            var handler = KeyDown;
            handler?.Invoke(this, keyEventArgs);
        }

        private void RaiseSelectionPositionChangedEvent()
        {
            if (SelectionPositionChangedEvent == null)
            {
                return;
            }

            Execute.OnUiThread(
                () =>
                {
                    try
                    {
                        var selection = _application.Selection;
                        if (selection == null)
                        {
                            return;
                        }

                        double xToPage = Math.Round(
                            (double)selection.Information[WdInformation.wdHorizontalPositionRelativeToPage] * (2.54 / 72.0),
                            2,
                            MidpointRounding.AwayFromZero);
                        double xToTextBoundary = Math.Round(
                            (double)selection.Information[WdInformation.wdHorizontalPositionRelativeToTextBoundary] * (2.54 / 72.0),
                            2,
                            MidpointRounding.AwayFromZero);

                        double yToPage = Math.Round(
                            (double)selection.Information[WdInformation.wdVerticalPositionRelativeToPage] * (2.54 / 72.0),
                            2,
                            MidpointRounding.AwayFromZero);
                        double yToTextBoundary = Math.Round(
                            (double)selection.Information[WdInformation.wdVerticalPositionRelativeToTextBoundary] * (2.54 / 72.0),
                            2,
                            MidpointRounding.AwayFromZero);

                        if (Math.Abs(xToPage - _currentSelectionHorizontalPositionRelativeToPage) > 0.00001
                            || Math.Abs(xToTextBoundary - _currentSelectionHorizontalPositionRelativeToTextBoundary) > 0.00001
                            || Math.Abs(yToPage - _currentSelectionVerticalPositionRelativeToPage) > 0.00001
                            || Math.Abs(yToTextBoundary - _currentSelectionVerticalPositionRelativeToTextBoundary) > 0.00001)
                        {
                            SelectionPositionChangedEvent(
                                this,
                                new SelectionPositionChangedEventArgs(
                                    _currentSelectionHorizontalPositionRelativeToPage,
                                    _currentSelectionHorizontalPositionRelativeToTextBoundary,
                                    _currentSelectionVerticalPositionRelativeToPage,
                                    _currentSelectionVerticalPositionRelativeToTextBoundary,
                                    xToPage,
                                    xToTextBoundary,
                                    yToPage,
                                    yToTextBoundary));
                            _currentSelectionHorizontalPositionRelativeToPage = xToPage;
                            _currentSelectionHorizontalPositionRelativeToTextBoundary = xToTextBoundary;
                            _currentSelectionVerticalPositionRelativeToPage = yToPage;
                            _currentSelectionVerticalPositionRelativeToTextBoundary = yToTextBoundary;
                        }
                    }
                    catch (Exception)
                    {
                        // no not care about errors here
                    }
                });
        }

        private void RaiseCaretPositionChangedEvent()
        {
            if (CaretPositionChangedEvent == null)
            {
                return;
            }

            // NOTE: GetCaretPos must be executed on the UI thread
            Execute.OnUiThread(
                () =>
                {
                    try
                    {
                        SafeNativeMethods.GetCaretPos(out var pt);

                        ////var ptCursorPositionRelativeToScreen = new System.Drawing.Point(
                        ////    System.Windows.Forms.Cursor.Position.X,
                        ////    System.Windows.Forms.Cursor.Position.Y);

                        var ptCaretPositionRelativeToClientHwnd = (Point)pt;

                        if (ptCaretPositionRelativeToClientHwnd.X != _currentCaretPosition.X
                            || ptCaretPositionRelativeToClientHwnd.Y != _currentCaretPosition.Y)
                        {
                            CaretPositionChangedEvent(
                                this,
                                new CaretPositionChangedEventArgs(
                                    _currentCaretPosition,
                                    ptCaretPositionRelativeToClientHwnd));
                            _currentCaretPosition = ptCaretPositionRelativeToClientHwnd;
                        }
                    }
                    catch (Exception)
                    {
                        // no not care about errors here
                    }
                });
        }

        private void OnInputDetected(object sender, EventArgs selectionChangedEventArgs)
        {
            RaiseSelectionPositionChangedEvent();
            RaiseCaretPositionChangedEvent();
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                InputDetector?.Dispose();
            }
        }
    }
}
