﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.SystemConfiguration;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data.Compatibility;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;
using Eurolook.WordAddIn.Database;
using Eurolook.WordAddIn.TemplateStore;
using Eurolook.WordAddIn.TemplateStore.TemplateDownload;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;

namespace Eurolook.WordAddIn.Options
{
    public class DocumentTypesOptionsViewModel : NavigationListItem, IOptionsScreen, ICanLog
    {
        private readonly ITemplateRepository _templateRepository;
        private readonly ISharedTemplateRepository _sharedTemplateRepository;
        private readonly IDocumentModelRepository _documentModelRepository;
        private readonly ITemplateDownloader _templateDownloader;
        private readonly IDocumentModelVersionCompatibilityTester _documentModelVersionCompatibilityTester;
        private readonly IBrickVersionCompatibilityTester _brickVersionCompatibilityTester;
        private readonly ISystemConfigurationService _systemConfigurationService;
        private readonly ISettingsService _settingsService;
        private readonly ITemplateStoreManager _templateStoreManager;
        private readonly bool _isCreationWizardEnabled;
        private readonly bool _isTemplateStoreEnabled;

        public DocumentTypesOptionsViewModel(
            ITemplateRepository templateRepository,
            ISharedTemplateRepository sharedTemplateRepository,
            IDocumentModelRepository documentModelRepository,
            IDocumentModelVersionCompatibilityTester documentModelVersionCompatibilityTester,
            IBrickVersionCompatibilityTester brickVersionCompatibilityTester,
            ISystemConfigurationService systemConfigurationService,
            ISettingsService settingsService,
            ITemplateDownloader templateDownloader = null,
            ITemplateStoreManager templateStoreManager = null)
        {
            _templateRepository = templateRepository;
            _sharedTemplateRepository = sharedTemplateRepository;
            _documentModelRepository = documentModelRepository;
            _templateDownloader = templateDownloader;
            _documentModelVersionCompatibilityTester = documentModelVersionCompatibilityTester;
            _brickVersionCompatibilityTester = brickVersionCompatibilityTester;
            _systemConfigurationService = systemConfigurationService;
            _settingsService = settingsService;
            _templateStoreManager = templateStoreManager;
            Title = "Document Types";
            IconPath = "/Graphics/heroicons-outline/white/document-duplicate.svg";
            Templates = new ObservableCollectionEx<TemplateViewModel>();
            RemovedTemplates = new List<TemplateViewModel>();
            DocumentTypeCategories = new ObservableCollectionEx<DocumentTypeCategoryViewModel>();
            TemplateStoreCommand = new RelayCommand(OpenTemplateStore);
            RemoveCommand = new RelayCommand<TemplateViewModel>(RemoveTemplate);
            DownloadCommand = new RelayCommand<TemplateViewModel>(DownloadTemplate);
            IsVisible = false;
            _isCreationWizardEnabled = _systemConfigurationService.IsCreationWizardEnabled().Result;
            _isTemplateStoreEnabled = !_settingsService.IsStandaloneMode && _systemConfigurationService.IsTemplateStoreEnabledForUser().Result;
        }

        private void OpenTemplateStore()
        {
            _templateStoreManager.OpenTemplateStore();
        }

        private void RemoveTemplate(TemplateViewModel templateViewModel)
        {
            try
            {
                Templates.Remove(templateViewModel);
                RemovedTemplates.Add(templateViewModel);
                RaisePropertyChanged(() => HasTemplates);
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private void DownloadTemplate(TemplateViewModel templateViewModel)
        {
            DownloadTemplateAsync(templateViewModel).FireAndForgetSafeAsync(this.LogError);
        }

        private async Task DownloadTemplateAsync(TemplateViewModel templateViewModel)
        {
            try
            {
                var downloadRequest = new TemplateDownloadRequest
                {
                    TemplateId = templateViewModel.TemplateId,
                    FinishAction = () =>
                                   {
                                       _templateRepository.MakeUserTemplateAvailableOffline(
                                           templateViewModel.UserTemplateId);
                                       templateViewModel.IsAvailableOffline = true;
                                   },
                    CloseAction = () =>
                                  {
                                      templateViewModel.IsDownloadButtonEnabled = true;
                                  },
                    ErrorAction =
                        () =>
                        {
                            templateViewModel.IsAvailableOffline = false;
                        },
                };
                templateViewModel.IsDownloadButtonEnabled = false;
                await _templateDownloader.DownloadTemplate(downloadRequest);
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        public RelayCommand TemplateStoreCommand { get; }

        public RelayCommand<TemplateViewModel> RemoveCommand { get; }

        public RelayCommand<TemplateViewModel> DownloadCommand { get; }

        public ObservableCollectionEx<TemplateViewModel> Templates { get; set; }

        public ObservableCollectionEx<DocumentTypeCategoryViewModel> DocumentTypeCategories { get; set; }

        public List<TemplateViewModel> RemovedTemplates { get; set; }

        public bool HasTemplates => Templates.Any();

        public bool IsTemplateStoreEnabled { get; set; }

        public int PositionIndex => 30;

        public bool IsAvailable => !_settingsService.IsStandaloneMode && !(!_isTemplateStoreEnabled && _isCreationWizardEnabled);

        public async Task LoadAsync(User user, UserSettings userSettings)
        {
            if (!IsAvailable)
            {
                return;
            }

            if (!_isCreationWizardEnabled)
            {
                var documentModels =
                    await _documentModelRepository.GetVisibleDocumentModelsAsync(_documentModelVersionCompatibilityTester, _brickVersionCompatibilityTester);
                foreach (var grouping in documentModels.GroupBy(x => x.DocumentCategory?.Name).OrderBy(x => x.Key))
                {
                    var documentTypeCategory =
                        new DocumentTypeCategoryViewModel { DisplayName = grouping.Key ?? "Other Templates" };
                    foreach (var documentModel in grouping)
                    {
                        var modelSettings = userSettings.GetSettingsForDocumentModel(documentModel.Id);
                        documentTypeCategory.DocumentTypes.Add(new DocumentTypeViewModel(documentModel, modelSettings, !_isCreationWizardEnabled));
                    }

                    DocumentTypeCategories.Add(documentTypeCategory);
                }
            }

            IsTemplateStoreEnabled = await _systemConfigurationService.IsTemplateStoreEnabledForUser(user);
            if (IsTemplateStoreEnabled)
            {
                var userTemplates = await _sharedTemplateRepository.GetUserTemplatesAsync(user.Id);
                foreach (var userTemplate in userTemplates.OrderBy(x => x.Template.Name))
                {
                    var templateSettings = userSettings.GetSettingsForTemplate(userTemplate.TemplateId);
                    Templates.Add(new TemplateViewModel(userTemplate, templateSettings));
                }

                RemovedTemplates.Clear();
            }

            RaisePropertyChanged(() => IsTemplateStoreEnabled);
            RaisePropertyChanged(() => HasTemplates);
        }

        public async Task<bool> SaveAsync(UserSettings userSettings, IWordUserDataRepository userDataRepository)
        {
            var modifiedDocumentTypes = new List<DocumentTypeViewModel>();
            foreach (var category in DocumentTypeCategories)
            {
                modifiedDocumentTypes.AddRange(category.DocumentTypes.Where(x => x.IsModified).ToArray());
            }

            if (modifiedDocumentTypes.Any())
            {
                await userDataRepository.UpdateDocumentModelSettingsAsync(userSettings.Id, modifiedDocumentTypes);
            }

            var modifiedTemplates = Templates.Where(x => x.IsModified).ToArray();
            var deletedTemplates = RemovedTemplates.ToArray();
            await userDataRepository.UpdateUserTemplateSettingsAsync(modifiedTemplates, deletedTemplates);
            return true;
        }
    }

    public class DocumentTypeViewModel : ViewModelBase
    {
        private bool _isVisible;

        public string DisplayName { get; set; }

        public bool IsVisible
        {
            get => _isVisible;
            set
            {
                Set(() => IsVisible, ref _isVisible, value);
                IsModified = IsVisible != WasVisible;
            }
        }

        public bool ShowVisibleToggle { get; set; }

        public Guid DocumentModelId { get; set; }

        public bool WasVisible { get; set; }

        public bool IsModified { get; set; }

        public DocumentTypeViewModel()
        {
        }

        public DocumentTypeViewModel(DocumentModel model, DocumentModelSettings modelSettings, bool showVisibleToggle)
        {
            DocumentModelId = model.Id;
            DisplayName = model.DisplayName;
            WasVisible = modelSettings?.IsHidden != true;
            IsVisible = WasVisible;
            ShowVisibleToggle = showVisibleToggle;
        }
    }

    public class TemplateViewModel : DocumentTypeViewModel
    {
        private bool _isAvailableOffline;
        private bool _isDownloadButtonEnabled;

        public TemplateViewModel()
        {
            IsDownloadButtonEnabled = true;
        }

        public TemplateViewModel(UserTemplate userTemplate, DocumentModelSettings templateSettings)
        {
            TemplateId = userTemplate.TemplateId;
            UserTemplateId = userTemplate.Id;
            DisplayName = userTemplate.Template?.Name;
            IsAvailableOffline = userTemplate.IsOfflineAvailable;
            IsDownloadButtonEnabled = true;
            WasVisible = !templateSettings?.IsHidden ?? true;
            IsVisible = WasVisible;
        }

        public Guid TemplateId { get; set; }

        public Guid UserTemplateId { get; set; }

        public bool IsDownloadButtonEnabled
        {
            get => _isDownloadButtonEnabled;
            set
            {
                Set(() => IsDownloadButtonEnabled, ref _isDownloadButtonEnabled, value);
            }
        }

        public bool IsAvailableOffline
        {
            get => _isAvailableOffline;
            set
            {
                Set(() => IsAvailableOffline, ref _isAvailableOffline, value);
            }
        }
    }

    public class DocumentTypeCategoryViewModel : ViewModelBase
    {
        public string DisplayName { get; set; }
        public ObservableCollectionEx<DocumentTypeViewModel> DocumentTypes { get; set; }
        public DocumentTypeCategoryViewModel()
        {
            DocumentTypes = new ObservableCollectionEx<DocumentTypeViewModel>();
        }
    }
}
