﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.Email;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.Common.AsyncCommands;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.Database;
using Eurolook.WordAddIn.ViewModels;
using GalaSoft.MvvmLight.Command;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.Options
{
    public class GeneralOptionsViewModel : NavigationListItem, IOptionsScreen, ICanLog
    {
        private const string Never = "never";

        private readonly IDatabaseStateRepository _databaseStateRepository;
        private readonly Lazy<IMessageService> _messageService;
        private readonly Lazy<IDocumentManager> _documentManager;
        private readonly Lazy<IClientDatabaseManager> _clientDatabaseManager;
        private readonly Lazy<IEmailService> _emailservice;

        private bool _isCustomerExperienceProgramEnabled;
        private bool _isDeletingDatabase;
        private bool _isInsertNameInline;
        private string _lastUpdateTime;
        private UserSettings _originalUserSettings;

        public GeneralOptionsViewModel(
            IDatabaseStateRepository databaseStateRepository,
            Lazy<IMessageService> messageService,
            Lazy<IDocumentManager> documentManager,
            Lazy<IClientDatabaseManager> clientDatabaseManager,
            Lazy<IEmailService> emailservice)
        {
            _databaseStateRepository = databaseStateRepository;
            _messageService = messageService;
            _documentManager = documentManager;
            _clientDatabaseManager = clientDatabaseManager;
            _emailservice = emailservice;
            Title = "General";
            IconPath = "/Graphics/heroicons-outline/white/cog-8-tooth.svg";
            DeleteDataCommand = new RelayCommand(DeleteData);
            IsVisible = false;
        }

        public RelayCommand DeleteDataCommand { get; set; }

        public string LastUpdateTime
        {
            get => _lastUpdateTime;
            set { Set(() => LastUpdateTime, ref _lastUpdateTime, value); }
        }

        public bool IsCustomerExperienceProgramEnabled
        {
            get => _isCustomerExperienceProgramEnabled;
            set { Set(() => IsCustomerExperienceProgramEnabled, ref _isCustomerExperienceProgramEnabled, value); }
        }

        public bool IsInsertNameInline
        {
            get => _isInsertNameInline;
            set { Set(() => IsInsertNameInline, ref _isInsertNameInline, value); }
        }

        public bool IsDeletingDatabase
        {
            get => _isDeletingDatabase;
            set
            {
                _isDeletingDatabase = value;
                RaisePropertyChanged(() => IsDeletingDatabase);
                RaisePropertyChanged(() => IsNotDeletingDatabase);
            }
        }

        public bool IsNotDeletingDatabase => !IsDeletingDatabase;

        public bool IsDatabaseDropped { get; private set; }

        public Task LoadAsync(User user, UserSettings userSettings)
        {
            _originalUserSettings = userSettings;
            var lastUpdate = _databaseStateRepository.GetLastUpdate();
            IsCustomerExperienceProgramEnabled = _originalUserSettings.IsCepEnabled;
            IsInsertNameInline = _originalUserSettings.IsInsertNameInline;
            LastUpdateTime = lastUpdate != null ? lastUpdate.ToString() : Never;
            return Task.CompletedTask;
        }

        [SuppressMessage("SonarQube", "S3168:async methods should not return void", Justification = "Method is a RelayCommand Action")]
        public async void DeleteData()
        {
            await DeleteData(false);
        }

        /// <summary>
        /// Try to delete the local database. When <paramref name="force" /> is set to false the user is asked for confirmation.<br />
        /// The result indicates if the database has been deleted.
        /// </summary>
        /// <param name="force">If force is false the user is asked for confirmation.</param>
        /// <returns>Indicates if the database has been deleted</returns>
        public async Task<bool> DeleteData(bool force)
        {
            if (!EnsureAllDocumentsClosed())
            {
                return false;
            }

            if (force || ShowMessageDeleteLocalDatabase() == true)
            {
                IsDeletingDatabase = true;
                await _clientDatabaseManager.Value.DropDatabaseAsync();
                LastUpdateTime = Never;
                IsDatabaseDropped = true;
                IsDeletingDatabase = false;
                return true;
            }

            return false;
        }

        public ICommand SendLogFilesCommand => new AsyncCommand(() => _emailservice.Value.SendLogFiles(), errorHandler: this.LogError);

        public int PositionIndex => 10;

        public bool IsAvailable => true;

        private bool EnsureAllDocumentsClosed()
        {
            var documentViewModels = _documentManager.Value.DocumentViewModels.ToList();
            if (!documentViewModels.Any())
            {
                return true;
            }

            if (ShowDocumentsClosedMessage() == true)
            {
                if (!TryCloseAllDocuments(documentViewModels))
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

            return !_documentManager.Value.DocumentViewModels.Any();
        }

        private bool? ShowDocumentsClosedMessage()
        {
            return _messageService.Value.ShowMessageWindow(
                new MessageViewModel
                {
                    Title = "Delete Local Data",
                    Message = "All open documents will be closed! Proceed?",
                    IsShowCancelButton = true,
                },
                this);
        }

        private static bool TryCloseAllDocuments(List<DocumentViewModel> documentViewModels)
        {
            foreach (var documentViewModel in documentViewModels)
            {
                try
                {
                    documentViewModel.Document.Close(WdSaveOptions.wdPromptToSaveChanges);
                }
                catch (Exception)
                {
                    return false;
                }
            }

            return true;
        }

        private bool? ShowMessageDeleteLocalDatabase()
        {
            return _messageService.Value.ShowMessageWindow(
                new MessageViewModel
                {
                    Title = "Delete Local Data",
                    Message = "Do you really want to delete your local database?",
                    YesButtonText = "Yes",
                    CancelButtonText = "No",
                },
                this);
        }

        public Task<bool> SaveAsync(UserSettings userSettings, IWordUserDataRepository userDataRepository)
        {
            userSettings.IsCepEnabled = IsCustomerExperienceProgramEnabled;
            userSettings.IsInsertNameInline = IsInsertNameInline;
            return Task.FromResult(true);
        }
    }
}
