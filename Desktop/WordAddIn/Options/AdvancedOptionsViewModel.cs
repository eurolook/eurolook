﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.Data.Constants;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.Database;

namespace Eurolook.WordAddIn.Options
{
    public class AdvancedOptionsViewModel : NavigationListItem, IOptionsScreen
    {
        private readonly ISettingsService _settingsService;
        private readonly IUserDataRepository _userDataRepository;
        private readonly IUserGroupRepository _userGroupRepository;
        private readonly bool _hasUserRights;
        private DataSyncEnvironment _selectedEnvironment;

        public AdvancedOptionsViewModel(
            ISettingsService settingsService,
            IUserDataRepository userDataRepository,
            IUserGroupRepository userGroupRepository)
        {
            _settingsService = settingsService;
            _userDataRepository = userDataRepository;
            _userGroupRepository = userGroupRepository;
            Title = "Advanced";
            IconPath = "/Graphics/heroicons-outline/white/beaker.svg";
            Environments = new ObservableCollection<DataSyncEnvironment>();
            _hasUserRights = HasUserRights().Result;
            IsVisible = false;
        }

        public ObservableCollection<DataSyncEnvironment> Environments { get; set; }

        public DataSyncEnvironment SelectedEnvironment
        {
            get => _selectedEnvironment;
            set { Set(() => SelectedEnvironment, ref _selectedEnvironment, value); }
        }

        public int PositionIndex => 100;

        public bool IsAvailable => _hasUserRights;

        public async Task LoadAsync(User user, UserSettings userSettings)
        {
            if (!IsAvailable)
            {
                return;
            }

            Environments.Clear();

            var isContributorOnly = await _userGroupRepository.IsMemberOfUserGroupOrMappedAdGroupAsync(user, CommonDataConstants.ContributorsUserGroupId)
                                    && !(await _userGroupRepository.IsMemberOfUserGroupOrMappedAdGroupAsync(user, CommonDataConstants.AdministratorsUserGroupId));
            foreach (var environment in AppSettings.Instance.DataSyncEnvironments.Where(environment => !(isContributorOnly && environment.Name.Equals("TEST", StringComparison.InvariantCultureIgnoreCase))))
            {
                Environments.Add(environment);
            }

            SelectedEnvironment = Environments.FirstOrDefault(e => e.DataSyncUrl == _settingsService.DataSyncUrl);
            return;
        }

        public Task<bool> SaveAsync(UserSettings userSettings, IWordUserDataRepository userDataRepository)
        {
            return Task.FromResult(true);
        }

        private async Task<bool> HasUserRights()
        {
            var user = _userDataRepository.GetUser();
            return await _userGroupRepository.IsMemberOfUserGroupOrMappedAdGroupAsync(user, CommonDataConstants.ContributorsUserGroupId) ||
                await _userGroupRepository.IsMemberOfUserGroupOrMappedAdGroupAsync(user, CommonDataConstants.AdministratorsUserGroupId);
        }
    }
}
