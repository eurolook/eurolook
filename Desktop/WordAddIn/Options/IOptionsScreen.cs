﻿using System.Threading.Tasks;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.Database;

namespace Eurolook.WordAddIn.Options
{
    public interface IOptionsScreen
    {
        string Title { get; set; }
        string IconPath { get; set; }
        bool IsVisible { get; set; }
        bool IsAvailable { get; }
        bool IsIconVisible { get; }
        Task LoadAsync(User user, UserSettings userSettings);
        Task<bool> SaveAsync(UserSettings userSettings, IWordUserDataRepository userDataRepository);
        int PositionIndex{ get; }
    }
}
