﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.ProfileInitializer;
using Eurolook.AddIn.Common.UserConfiguration;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.Database;
using GalaSoft.MvvmLight.CommandWpf;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.Options
{
    public class OptionsViewModel : DialogViewModelBase
    {
        private readonly ColorManager _colorManager;
        private readonly IWordUserDataRepository _userDataRepository;
        private readonly ISettingsService _settingsService;
        private readonly IDataSyncEnvironmentConfiguration _dataSyncEnvironmentConfiguration;
        private readonly DataSyncScheduler _dataSyncScheduler;
        private readonly ColorScheme _originalColorScheme;
        private readonly Lazy<IProfileInitializer> _profileInitializer;
        private IOptionsScreen _selectedSubOptionsVm;

        [UsedImplicitly]
        public OptionsViewModel(
            Lazy<IProfileInitializer> profileInitializer,
            ColorManager colorManager,
            IWordUserDataRepository userDataRepository,
            DataSyncScheduler dataSyncScheduler,
            IEnumerable<IOptionsScreen> optionScreens,
            ISettingsService settingsService,
            IDataSyncEnvironmentConfiguration dataSyncEnvironmentConfiguration)
        {
            _profileInitializer = profileInitializer;
            _colorManager = colorManager;
            _userDataRepository = userDataRepository;
            _settingsService = settingsService;
            _dataSyncScheduler = dataSyncScheduler;
            _dataSyncEnvironmentConfiguration = dataSyncEnvironmentConfiguration;
            _originalColorScheme = _colorManager.CurrentColorScheme;

            var optionsScreens = optionScreens as IOptionsScreen[] ?? optionScreens.ToArray();
            SubOptions = new ObservableCollection<IOptionsScreen>();
            foreach (var screen in optionsScreens.Where(x => x.IsAvailable).OrderBy(x => x.PositionIndex))
            {
                SubOptions.Add(screen);
            }

            SelectedSubOptions = SubOptions.First();

            PersonalisationOptions = SubOptions.OfType<PersonalisationOptionsViewModel>().FirstOrDefault();
            GeneralOptions = SubOptions.OfType<GeneralOptionsViewModel>().FirstOrDefault();
            DocumentTypesOptions = SubOptions.OfType<DocumentTypesOptionsViewModel>().FirstOrDefault();
            NotificationOptions = SubOptions.OfType<NotificationOptionsViewModel>().FirstOrDefault();
            AdvancedOptions = SubOptions.OfType<AdvancedOptionsViewModel>().FirstOrDefault();

            GetHelpCommand = new RelayCommand(() => _settingsService.OpenHelpLink("Settings"));
            LoadAsync().FireAndForgetSafeAsync(this.LogError);
        }

        public override string Title => "Eurolook - Options";

        public ObservableCollection<IOptionsScreen> SubOptions { get; set; }

        public IOptionsScreen SelectedSubOptions
        {
            get { return _selectedSubOptionsVm; }
            set
            {
                Set(() => SelectedSubOptions, ref _selectedSubOptionsVm, value);
                foreach (var s in SubOptions.Where(s => s != value))
                {
                    s.IsVisible = false;
                }

                _selectedSubOptionsVm.IsVisible = true;
            }
        }

        public GeneralOptionsViewModel GeneralOptions { get; }

        public PersonalisationOptionsViewModel PersonalisationOptions { get; }

        public AdvancedOptionsViewModel AdvancedOptions { get; protected set; }

        public NotificationOptionsViewModel NotificationOptions { get; }

        public DocumentTypesOptionsViewModel DocumentTypesOptions { get; set; }

        public RelayCommand GetHelpCommand { get; }

        public override void Cancel()
        {
            DialogResult = false;
            ResetOptions();
        }

        public void ResetOptions()
        {
            if (_colorManager.CurrentColorScheme != _originalColorScheme)
            {
                _colorManager.ApplyColorScheme();
            }
        }

        public override async void Commit()
        {
            try
            {
                IsBusy = true;
                if (await SaveAsync())
                {
                    DialogResult = true;
                }
            }
            finally
            {
                IsBusy = false;
            }
        }

        public async Task LoadAsync()
        {
            var user = _userDataRepository.GetUser();
            var userSettings = _userDataRepository.GetUserSettings();
            foreach (var option in SubOptions)
            {
                await option.LoadAsync(user, userSettings);
            }
        }

        private async Task<bool> SaveAsync()
        {
            // handle databased drop
            if (GeneralOptions.IsDatabaseDropped)
            {
                return false;
            }

            // handle environment switch
            if (AdvancedOptions?.SelectedEnvironment != null
                && AdvancedOptions?.SelectedEnvironment.DataSyncUrl != _dataSyncEnvironmentConfiguration.GetDataSyncUrl())
            {
                if (await GeneralOptions.DeleteData(true))
                {
                    string newUrl = AdvancedOptions.SelectedEnvironment.DataSyncUrl;
                    _dataSyncEnvironmentConfiguration.SetDataSyncUrl(newUrl);

                    _dataSyncScheduler.Server = newUrl;

                    await _profileInitializer.Value.InitializeProfileIfNecessary();
                    return true;
                }

                return false;
            }

            // save settings
            var userSettings = _userDataRepository.GetUserSettings();
            if (userSettings == null)
            {
                return false;
            }

            foreach (var option in SubOptions)
            {
                var goOn = await option.SaveAsync(userSettings, _userDataRepository);
                if (!goOn)
                {
                    return false;
                }
            }

            _userDataRepository.UpdateUserSettings(userSettings);
            return true;
        }
    }
}
