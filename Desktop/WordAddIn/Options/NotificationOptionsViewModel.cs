﻿using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.Database;
using Eurolook.WordAddIn.Messages;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace Eurolook.WordAddIn.Options
{
    public class NotificationOptionsViewModel : NavigationListItem, IOptionsScreen
    {
        private readonly INotificationRepository _notificationRepository;
        private readonly ISettingsService _settingsService;
        private int _hiddenNotificationsCount;

        public NotificationOptionsViewModel(
            INotificationRepository notificationRepository,
            ISettingsService settingsService)
        {
            _notificationRepository = notificationRepository;
            _settingsService = settingsService;
            Title = "Notifications";
            IconPath = "/Graphics/heroicons-outline/white/chat-bubble-left-ellipsis.svg";
            RestoreNotificationsCommand = new RelayCommand(RestoreNotifications);
            IsVisible = false;
        }

        public RelayCommand RestoreNotificationsCommand { get; set; }

        public bool IsResetEnabled => HiddenNotificationsCount > 0;

        public int HiddenNotificationsCount
        {
            get => _hiddenNotificationsCount;
            set
            {
                Set(() => HiddenNotificationsCount, ref _hiddenNotificationsCount, value);
                RaisePropertyChanged(() => IsResetEnabled);
            }
        }

        public int PositionIndex => 20;

        public bool IsAvailable => !_settingsService.IsStandaloneMode;

        public async Task LoadAsync(User user, UserSettings userSettings)
        {
            HiddenNotificationsCount = await _notificationRepository.GetHiddenNotificationsCountAsync();
        }

        public Task<bool> SaveAsync(UserSettings userSettings, IWordUserDataRepository userDataRepository)
        {
            return Task.FromResult(true);
        }

        private void RestoreNotifications()
        {
            _notificationRepository.RestoreActiveNotifications();
            HiddenNotificationsCount = 0;
            Messenger.Default.Send(new UpdateNotificationsMessage());
        }
    }
}
