﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Media;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.AddIn.Common.Ribbon;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.Database;
using Eurolook.WordAddIn.ViewModels;
using Eurolook.WordAddIn.ViewModels.Languages;

namespace Eurolook.WordAddIn.Options
{
    public class PersonalisationOptionsViewModel : NavigationListItem, IOptionsScreen
    {
        private readonly IEurolookDataRepository _eurolookDataRepository;
        private readonly ILanguageRepository _languageRepository;
        private readonly IRibbonService _ribbonService;
        private readonly ColorManager _colorManager;
        private ColorSchemeViewModel _selectedColorScheme;
        private bool _isFavoriteLanguageListEnabled;
        private bool _isStyleBoxEnabled;

        public PersonalisationOptionsViewModel(
            IEurolookDataRepository eurolookDataRepository,
            ILanguageRepository languageRepository,
            IRibbonService ribbonService,
            ColorManager colorManager)
        {
            Title = "Personalisation";
            IconPath = "/Graphics/heroicons-outline/white/swatch.svg";
            ColorSchemes = new ObservableCollection<ColorSchemeViewModel>();
            LanguageVms = new ObservableCollection<LanguageViewModel>();
            _eurolookDataRepository = eurolookDataRepository;
            _languageRepository = languageRepository;
            _ribbonService = ribbonService;
            _colorManager = colorManager;
            IsVisible = false;
        }

        public ObservableCollection<ColorSchemeViewModel> ColorSchemes { get; set; }

        public ColorSchemeViewModel SelectedColorScheme
        {
            get => _selectedColorScheme;
            set
            {
                Set(() => SelectedColorScheme, ref _selectedColorScheme, value);
                if (_colorManager != null && value != null)
                {
                    if (_selectedColorScheme?.Id == null)
                    {
                        _colorManager.ApplyDefaultColorScheme();
                    }
                    else
                    {
                        _colorManager.ApplyColorScheme(_selectedColorScheme.ColorScheme);
                    }
                }
            }
        }

        public bool IsFavoriteLanguageListEnabled
        {
            get => _isFavoriteLanguageListEnabled;
            set { Set(() => IsFavoriteLanguageListEnabled, ref _isFavoriteLanguageListEnabled, value); }
        }

        public bool IsStyleBoxEnabled
        {
            get => _isStyleBoxEnabled;
            set { Set(() => IsStyleBoxEnabled, ref _isStyleBoxEnabled, value); }
        }

        public ObservableCollection<LanguageViewModel> LanguageVms { get; set; }

        public int PositionIndex => 0;

        public bool IsAvailable => true;

        public async Task LoadAsync(User user, UserSettings userSettings)
        {
            var colorSchemes = new ColorScheme[] { };
            var languages = new List<Language>();
            await Task.Run(
                () =>
                {
                    colorSchemes = _eurolookDataRepository.GetAllColorSchemes();
                    languages = _languageRepository.GetAllLanguages();
                });

            // Color Scheme
            ColorSchemes.Clear();
            ColorSchemes.Add(
                new ColorSchemeViewModel
                {
                    Id = null,
                    DisplayName = "Default",
                    PrimaryColor = new SolidColorBrush(_colorManager.DefaultColorScheme.PrimaryColor.ToWpfColor()),
                    SecondaryColor = new SolidColorBrush(_colorManager.DefaultColorScheme.SecondaryColor.ToWpfColor()),
                    Tile1Color = new SolidColorBrush(_colorManager.DefaultColorScheme.TileColor1.ToWpfColor()),
                    Tile2Color = new SolidColorBrush(_colorManager.DefaultColorScheme.TileColor2.ToWpfColor()),
                    Tile3Color = new SolidColorBrush(_colorManager.DefaultColorScheme.TileColor3.ToWpfColor()),
                });
            foreach (var colorScheme in colorSchemes)
            {
                ColorSchemes.Add(new ColorSchemeViewModel(colorScheme));
            }

            if (userSettings.ColorSchemeId != null)
            {
                SelectedColorScheme = ColorSchemes.FirstOrDefault(vm => vm.Id == userSettings.ColorSchemeId);
            }

            IsStyleBoxEnabled = !userSettings.IsStyleBoxDisabled;

            if (SelectedColorScheme == null)
            {
                SelectedColorScheme = ColorSchemes.First();
            }

            foreach (var language in languages.OrderBy(l => l.DisplayName))
            {
                var vm = new LanguageViewModel(language);
                vm.IsSelected = userSettings.GetFavoriteLanguageIds().Contains(vm.Id);
                LanguageVms.Add(vm);
            }

            IsFavoriteLanguageListEnabled = userSettings.IsFavoriteLanguageListEnabled;
        }

        public async Task<bool> SaveAsync(UserSettings userSettings, IWordUserDataRepository userDataRepository)
        {
            bool styleBoxOptionChanged = !IsStyleBoxEnabled != userSettings.IsStyleBoxDisabled;
            userSettings.ColorSchemeId = SelectedColorScheme.Id;
            userSettings.IsFavoriteLanguageListEnabled = IsFavoriteLanguageListEnabled;
            userSettings.IsStyleBoxDisabled = !IsStyleBoxEnabled;
            var favoriteLanguageIds = LanguageVms
                                                    .Where(l => l.IsSelected).Select(l => l.Id)
                                                    .ToArray();
            userSettings.UpdateFavoriteLanguageIdsRaw(favoriteLanguageIds);

            if (styleBoxOptionChanged)
            {
                if (IsStyleBoxEnabled)
                {
                    await _ribbonService.TryLoadQuickAccessToolbarAsync();
                }
                else
                {
                    _ribbonService.UnloadQuickAccessToolbar();
                }
            }

            return true;
        }
    }
}
