﻿using System.ComponentModel;
using Eurolook.Common.Log;

namespace Eurolook.WordAddIn.Options
{
    public partial class OptionsWindow : ICanLog
    {
        public OptionsWindow(OptionsViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
            Closing += WindowClosing;
        }

        public OptionsViewModel ViewModel => DataContext as OptionsViewModel;

        private void WindowClosing(object sender, CancelEventArgs e)
        {
            if (ViewModel.DialogResult == null)
            {
                ViewModel.ResetOptions();
            }
        }
    }
}
