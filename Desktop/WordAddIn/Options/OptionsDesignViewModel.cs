﻿namespace Eurolook.WordAddIn.Options
{
    public class OptionsDesignViewModel : OptionsViewModel
    {
        public OptionsDesignViewModel()
            : base(null, null, null, null, null, null, null)
        {
            if (PersonalisationOptions != null)
            {
                PersonalisationOptions.IsVisible = true;
            }
        }
    }
}
