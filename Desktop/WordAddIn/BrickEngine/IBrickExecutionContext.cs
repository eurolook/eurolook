using Eurolook.Data.Xml;
using Eurolook.DocumentProcessing;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.BrickEngine
{
    public interface IBrickExecutionContext
    {
        Application Application { get; }

        Document Document { get; }

        Selection Selection { get; }

        IReadOnlyEurolookProperties EurolookProperties { get; }

        IEurolookWordDocument EurolookDocument { get; }

        AuthorInformation AuthorInformation { get; set; }
    }
}
