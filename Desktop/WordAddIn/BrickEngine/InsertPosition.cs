﻿using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickEngine
{
    /// <summary>
    /// A helper class for managing the range where bricks are inserted.
    /// </summary>
    /// <remarks>
    ///     <para>
    ///     When inserting a brick into the document, we need to prepare a region where the brick can be inserted
    ///     without interfering with the already existing document contents.
    ///     </para>
    ///     <para>
    ///     In certain cases an additional paragraph is inserted that needs to be removed again after the brick has
    ///     been inserted. Whether the brick has to be removed is stored in the <see cref="HasAdditionalParagraph" />
    ///     property.
    ///     </para>
    /// </remarks>
    public class InsertPosition
    {
        public InsertPosition(Range range, bool hasAdditionalParagraph = false)
        {
            Range = range;
            HasAdditionalParagraph = hasAdditionalParagraph;
        }

        public Range Range { get; }

        /// <summary>
        /// Gets a value indicating whether an additional paragraph has been inserted with the brick.
        /// </summary>
        /// <remarks>
        /// In certain cases an additional paragraph is inserted that needs to be removed again after the brick has been
        /// inserted. Whether the brick has to be removed is stored in the <see cref="HasAdditionalParagraph" /> property.
        /// </remarks>
        public bool HasAdditionalParagraph { get; }
    }
}
