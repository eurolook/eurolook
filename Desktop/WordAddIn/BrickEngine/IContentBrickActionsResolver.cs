using Eurolook.Data.Models;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.BrickEngine
{
    public interface IContentBrickActionsResolver
    {
        IContentBrickActions Resolve(ContentBrick contentBrick);

        IContentBrickActions Resolve(ContentControl contentControl);
    }
}
