﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Eurolook.Common;
using Eurolook.Data.Models;
using Eurolook.Data.TermStore;
using Eurolook.DocumentProcessing.OpenXml;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Language = Eurolook.Data.Models.Language;
using Range = Microsoft.Office.Interop.Word.Range;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickEngine
{
    public class BuildingBlockBrickInstance : BrickInstanceBase, IBrickInstance
    {
        private static readonly string _buildingBlockName = "Custom Crick";
        private static readonly string _buildingBlockCategory = "Eurolook Custom Bricks";

        private readonly ILocalisedResourceResolver _resourceResolver;
        private readonly ITermStoreCache _termStoreCache;
        private readonly IStylesExtractor _stylesExtractor;
        private readonly DocumentModel _documentModel;
        [CanBeNull]
        private readonly DocumentModelLanguage _documentModelLanguage;
        private readonly Language _language;

        public BuildingBlockBrickInstance(
            BuildingBlockBrick buildingBlockBrick,
            ILocalisedResourceResolver resourceResolver,
            ITermStoreCache termStoreCache,
            IStylesExtractor stylesExtractor,
            DocumentModel documentModel,
            DocumentModelLanguage documentModelLanguage,
            Language language)
        {
            Brick = buildingBlockBrick;
            _resourceResolver = resourceResolver;
            _termStoreCache = termStoreCache;
            _stylesExtractor = stylesExtractor;
            _documentModel = documentModel;
            _documentModelLanguage = documentModelLanguage;
            _language = language;
        }

        public Brick Brick { get; }

        public bool HasAuthorBinding => false;

        public IEnumerable<string> RequiredStyles => Enumerable.Empty<string>();

        public static byte[] CreateBuildingBlock(
            Application application,
            byte[] templateBytes,
            Range content)
        {
            if (application == null || content == null || templateBytes == null)
            {
                return null;
            }

            using (new DocumentAutomationHelper(application, null, DocumentAutomationOption.PreventScreenUpdating))
            {
                using (var tempFile1 = new TemporaryFile(".dotx"))
                {
                    File.WriteAllBytes(tempFile1.FullName, templateBytes);
                    var document = application.Documents.Add(tempFile1.FullName, Visible: false);
                    document.Content.FormattedText = content.FormattedText;
                    foreach (ContentControl cc in document.Content.ContentControls)
                    {
                        if (!cc.Temporary)
                        {
                            cc.Delete();
                        }
                    }

                    using (var tempFile2 = new TemporaryFile(".dotx"))
                    {
                        File.WriteAllBytes(tempFile2.FullName, templateBytes);
                        var addin = application.AddIns.Add(tempFile2.FullName);
                        var template = application.Templates.OfType<Template>()
                                                  .FirstOrDefault(t => t.Name == addin.Name);
                        if (template != null)
                        {
                            // exclude final paragraph mark
                            var buildingBlockRange = document.Content;
                            buildingBlockRange.MoveEnd(WdUnits.wdCharacter, -1);

                            template.BuildingBlockEntries.Add(
                                _buildingBlockName,
                                WdBuildingBlockTypes.wdTypeQuickParts,
                                _buildingBlockCategory,
                                buildingBlockRange);
                            template.Save();
                            addin.Delete();
                            document.Close(false);
                            return File.ReadAllBytes(tempFile2.FullName);
                        }

                        return null;
                    }
                }
            }
        }

        public Task ApplyAsync(IBrickExecutionContext context, string argument = null)
        {
            using (var documentAutomationHelper = new DocumentAutomationHelper(
                context.Application,
                $"Apply {Brick.DisplayName} Brick"))
            {
                var inserter = new BrickInserter(
                    context,
                    _resourceResolver,
                    _termStoreCache,
                    _stylesExtractor,
                    _documentModel,
                    _language,
                    _documentModelLanguage);

                var insertRange = context.Selection.Range;
                var insertPositions = new List<InsertPosition> { new InsertPosition(insertRange) };
                inserter.Insert(
                    insertPositions,
                    Brick as BuildingBlockBrick,
                    _buildingBlockCategory,
                    _buildingBlockName);

                SelectInsertedBrick(
                    documentAutomationHelper,
                    insertPositions.Select(x => x.Range).ToList(),
                    null);

                return Task.CompletedTask;
            }
        }

        public bool TrySelect(IBrickExecutionContext context, string argument = null)
        {
            return false;
        }
    }
}
