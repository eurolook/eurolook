﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing.OpenXml;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.BrickEngine
{
    public class CommandBrickInstance : IBrickInstance, ICanLog
    {
        private readonly CommandBrick _commandBrick;

        public CommandBrickInstance(
            CommandBrick commandBrick,
            ICommandBrickResolver resolver,
            ILocalisedResourceResolver localisedResourceResolver)
        {
            _commandBrick = commandBrick;

            BrickCommand = resolver.Resolve(commandBrick);

            if (BrickCommand != null)
            {
                BrickCommand.DisplayName = _commandBrick.DisplayName;
                BrickCommand.Brick = _commandBrick;
                BrickCommand.LocalisedResourceResolver = localisedResourceResolver;
            }
            else
            {
                // hide unresolved command bricks in the UI
                _commandBrick.IsHidden = true;
            }
        }

        [CanBeNull]
        public IBrickCommand BrickCommand { get; }

        public Brick Brick
        {
            get { return _commandBrick; }
        }

        public bool HasAuthorBinding
        {
            get { return BrickCommand != null && BrickCommand.HasAuthorBinding; }
        }

        public IEnumerable<string> RequiredStyles
        {
            get { return BrickCommand != null ? BrickCommand.RequiredStyles : Enumerable.Empty<string>(); }
        }

        /// <summary>
        /// Checks if the brick is already applied to the document and if so selects the brick.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="argument"></param>
        /// <returns>True, if the brick has been selected, false otherwise.</returns>
        public bool TrySelect(IBrickExecutionContext context, string argument = null)
        {
            return false;
        }

        public async Task ApplyAsync(IBrickExecutionContext context, string argument = null)
        {
            if (BrickCommand == null)
            {
                this.LogWarnFormat("Cannot apply command brick: name={0} ({1})", _commandBrick.Name, _commandBrick.Id);
                return;
            }

            this.LogInfoFormat($"Apply command brick: name={_commandBrick.Name} ({_commandBrick.Id}), argument={argument}");
            string commandBrickArgument = argument ?? _commandBrick.Argument1;
            await BrickCommand.ExecuteAsync(context, commandBrickArgument);
        }

        public bool CanExecute(IBrickExecutionContext context, string argument = null)
        {
            if (BrickCommand == null)
            {
                return false;
            }

            return BrickCommand.CanExecute(context, argument);
        }
    }
}
