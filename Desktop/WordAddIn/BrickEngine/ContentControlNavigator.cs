﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing.Extensions;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickEngine
{
    /// <inheritdoc />
    /// <summary>
    /// Provides functionality to select the previous/next input fields in the document.
    /// </summary>
    public class ContentControlNavigator : ICanLog
    {
        private readonly Document _document;
        private readonly Application _application;

        public ContentControlNavigator(Document document)
        {
            _document = document ?? throw new ArgumentNullException(nameof(document));
            _application = document.Application;
        }

        public bool CanNavigate()
        {
            try
            {
                return _application.IsObjectValid[_document]
                       && _document
                           .StoryRanges.OfType<Range>()
                           .SelectMany(r => r.ContentControls.OfType<ContentControl>())
                           .Any(cc => !cc.LockContents && cc.ShowingPlaceholderText && cc.Range.ContentControls.Count == 0);
            }
            catch (COMException ex)
            {
                this.LogWarn(ex);
            }

            return false;
        }

        public void SelectPreviousContentControl()
        {
            using (
                var documentAutomationHelper = new DocumentAutomationHelper(
                    _application,
                    _document,
                    "Go to the previous input field",
                    DocumentAutomationOption.EnableUndo | DocumentAutomationOption.PreventScreenUpdating
                    | DocumentAutomationOption.ShowWaitCursor))
            {
                var contentControls = GetContentControlsShowingPlaceholder(_document);

                var searchRange = _application.Selection.Range;
                var previousContentControl = contentControls.LastOrDefault(
                    cc =>
                        (cc.Range.StoryType == searchRange.StoryType && cc.Range.End < searchRange.Start) // previous content control in same story
                        || cc.Range.StoryType < searchRange.StoryType); // last content control in any of the previous stories

                if (previousContentControl == null)
                {
                    // beginning reached, start from end
                    previousContentControl = contentControls.LastOrDefault(
                        cc =>
                            (cc.Range.StoryType == searchRange.StoryType && cc.Range.Start > searchRange.End)
                            || cc.Range.StoryType > searchRange.StoryType);
                }

                if (previousContentControl == null)
                {
                    // no content control found that precedes the current selection (could happen if user pressed Ctrl+A)
                    // so we take the last one in the document
                    previousContentControl = contentControls.LastOrDefault();
                }

                previousContentControl?.Range.SelectInPreferredView(documentAutomationHelper.PreferredViewType);
            }
        }

        public void SelectNextContentControl()
        {
            using (
                var documentAutomationHelper = new DocumentAutomationHelper(
                    _application,
                    _document,
                    "Go to the next input field",
                    DocumentAutomationOption.EnableUndo | DocumentAutomationOption.PreventScreenUpdating
                    | DocumentAutomationOption.ShowWaitCursor))
            {
                var contentControls = GetContentControlsShowingPlaceholder(_document);

                var searchRange = _application.Selection.Range;
                var nextContentControl = contentControls.FirstOrDefault(
                    cc =>
                        (cc.Range.StoryType == searchRange.StoryType && cc.Range.Start > searchRange.End) // following content control in same story
                        || cc.Range.StoryType > searchRange.StoryType); // first content control in any of the following stories

                if (nextContentControl == null)
                {
                    // end reached, start from beginning
                    nextContentControl = contentControls.FirstOrDefault(
                        cc =>
                            (cc.Range.StoryType == searchRange.StoryType && cc.Range.End < searchRange.Start)
                            || cc.Range.StoryType < searchRange.StoryType);
                }

                if (nextContentControl == null)
                {
                    // no content control found that follows the current selection (could happen if user pressed Ctrl+A)
                    // so we take the first one in the document
                    nextContentControl = contentControls.FirstOrDefault();
                }

                nextContentControl?.Range.SelectInPreferredView(documentAutomationHelper.PreferredViewType);
            }
        }

        private static List<ContentControl> GetContentControlsShowingPlaceholder(Document document)
        {
            var contentControls = document
                .StoryRanges.OfType<Range>()
                .SelectMany(r => r.ContentControls.OfType<ContentControl>())
                .Where(cc => !cc.LockContents && cc.ShowingPlaceholderText && cc.Range.ContentControls.Count == 0)
                .OrderBy(cc => cc.Range.StoryType)
                .ThenBy(cc => cc.Range.Start)
                .ToList();

            return contentControls;
        }
    }
}
