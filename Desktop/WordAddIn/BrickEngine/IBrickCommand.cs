using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.OpenXml;

namespace Eurolook.WordAddIn.BrickEngine
{
    public interface IBrickCommand
    {
        string DisplayName { get; set; }

        CommandBrick Brick { get; set; }

        ILocalisedResourceResolver LocalisedResourceResolver { get; set; }

        bool HasCustomUI { get; }

        bool HasAuthorBinding { get; }

        IEnumerable<string> RequiredStyles { get; }

        bool CanExecute(IBrickExecutionContext context, string argument);

        Task ExecuteAsync(IBrickExecutionContext context, string argument);

        bool ShowCheckmark(List<BrickContainer> brickContainers);
    }
}
