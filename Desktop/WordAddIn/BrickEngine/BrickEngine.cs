﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.SystemResourceTracer;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data.ActionLogs;
using Eurolook.Data.Models;
using Eurolook.Data.TermStore;
using Eurolook.Data.Xml;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.DocumentProcessing.OpenXml;
using Eurolook.OfficeNotificationBar;
using Eurolook.WordAddIn.Extensions;
using Eurolook.WordAddIn.ViewModels;
using JetBrains.Annotations;
using Microsoft.Office.Core;

namespace Eurolook.WordAddIn.BrickEngine
{
    /// <summary>
    /// A factory class for creating a brick instance depending on the type of brick. Creates as
    /// <see cref="ContentBrickInstance" /> for content bricks and a <see cref="CommandBrickInstance" /> for command
    /// bricks.
    /// </summary>
    public class BrickEngine : IBrickEngine, ICanLog
    {
        private readonly Dictionary<Guid, IBrickInstance> _brickInstancesCache = new Dictionary<Guid, IBrickInstance>();
        private readonly List<DocumentStructure> _structures;
        private readonly ILocalisedResourceResolver _resourceResolver;
        private readonly ITermStoreCache _termStoreCache;
        private readonly IStylesExtractor _stylesExtractor;
        private readonly DocumentModel _documentModel;
        [CanBeNull]
        private readonly DocumentModelLanguage _documentModelLanguage;
        private readonly ICommandBrickResolver _resolver;
        private readonly Func<IBrickExecutionContext> _brickExecutionContextFactory;
        private readonly IContentBrickActionsResolver _contentBrickActionsResolver;
        private readonly ISystemResourceTracer _systemResourceTracer;
        private readonly IStatisticsRepository _statisticsRepository;
        private readonly IMessageService _messageService;
        private readonly ISettingsService _settingsService;

        public BrickEngine(
            List<DocumentStructure> structures,
            ILocalisedResourceResolver resourceResolver,
            ITermStoreCache termStoreCache,
            IStylesExtractor stylesExtractor,
            DocumentModel documentModel,
            DocumentModelLanguage documentModelLanguage,
            Language language,
            ICommandBrickResolver resolver,
            IAuthorManager authorManager,
            Func<IBrickExecutionContext> brickExecutionContextFactory,
            IContentBrickActionsResolver contentBrickActionsResolver,
            ISystemResourceTracer systemResourceTracer,
            IStatisticsRepository statisticsRepository,
            IMessageService messageService,
            ISettingsService settingsService)
        {
            _structures = structures;
            _resourceResolver = resourceResolver;
            _termStoreCache = termStoreCache;
            _stylesExtractor = stylesExtractor;
            _documentModel = documentModel;
            _documentModelLanguage = documentModelLanguage;
            Language = language;
            _resolver = resolver;
            _brickExecutionContextFactory = brickExecutionContextFactory;
            _contentBrickActionsResolver = contentBrickActionsResolver;
            _systemResourceTracer = systemResourceTracer;
            _statisticsRepository = statisticsRepository;
            _messageService = messageService;
            _settingsService = settingsService;
            AuthorManager = authorManager;

            // pre-initialize command bricks before showing the task pane
            // to be able to hide all command bricks unknown to the client in the task pane
            foreach (var commandBrickStructure in structures.Where(ds => ds.Brick is CommandBrick).ToArray())
            {
                try
                {
                    GetOrCreateBrickInstance(commandBrickStructure.Brick);
                }
                catch (ArgumentException aex)
                {
                    // remove a defect CommandBrick form the task pane
                    structures.Remove(commandBrickStructure);
                    this.LogError($"Cannot create CommandBrick: {commandBrickStructure.Brick.Name}", aex);
                }
            }

            // pre-initialize content bricks
            foreach (var contentBrick in _structures.Select(ds => ds.Brick).OfType<ContentBrick>())
            {
                GetOrCreateBrickInstance(contentBrick);
            }
        }

        protected Language Language { get; }

        protected IAuthorManager AuthorManager { get; }

        public async Task ApplyBrickAsync(Guid id, string argument = null)
        {
            var brick = _structures.FirstOrDefault(ds => ds.Brick.Id == id)?.Brick;
            await ApplyBrickAsync(brick, argument);
        }

        public async Task ApplyBrickAsync(string brickName, string argument = null)
        {
            var brick = _structures.FirstOrDefault(ds => ds.Brick.Name == brickName)?.Brick;
            await ApplyBrickAsync(brick, argument);
        }

        public async Task ApplyBrickAsync(Brick brick, string argument = null)
        {
            if (brick == null)
            {
                return;
            }

            await ApplyAsync(brick, _brickExecutionContextFactory(), argument);
        }

        public async Task ApplyBrickWithCheckForDeletedRevisionsAsync(Brick brick)
        {
            if (brick == null)
            {
                return;
            }

            var context = _brickExecutionContextFactory();

            await HandleActiveModernCommentOrReply(context);

            // Check for Track Changes
            bool reInsertBrick = false;

            if (context.Document.TrackRevisions && !(brick is CommandBrick) && !brick.IsMultiInstance)
            {
                // when Track Changes are activated and the brick is already in the document
                // check if the brick is in a deleted revision, then ask the user whether the brick shall be inserted again
                // EUROLOOK-1352
                var brickInDocument = context.Document.GetAllBrickContainers().FirstOrDefault(b => b.Id == brick.Id);
                if (brickInDocument != null && brickInDocument.ContentControl.Range.IsInDeletedRevision())
                {
                    context.Document.ShowRevisions = true;
                    var result = _messageService.ShowMessageWindow(new ReinsertBrickMessageViewModel(), null);
                    if (result != null)
                    {
                        reInsertBrick = result.Value;
                    }
                }
            }

            await ApplyAsync(brick, context, null, reInsertBrick);
        }

        public async Task ApplyBrickAndLogInsertionAsync(Type commandBrickType, string parameter, string insertionMethod)
        {
            try
            {
                _systemResourceTracer.TraceMemoryUsage();

                var commandBrick = ResolveCommandBrick(commandBrickType, parameter, insertionMethod);
                if (commandBrick != null)
                {
                    await ApplyBrickAsync(commandBrick, parameter);

                    await _statisticsRepository.LogBrickInsertionAsync(
                        _documentModel,
                        new BrickInsertionLog
                        {
                            BrickId = commandBrick.Id,
                            Method = insertionMethod,
                        });
                }
            }
            catch (Exception ex)
            {
                this.LogWarn($"Could not execute {insertionMethod}.", ex);
            }
        }

        public bool CanExecute(Brick brick, string argument = null)
        {
            return CanExecute(brick, _brickExecutionContextFactory(), argument);
        }

        public void RecreateDynamicBricks(
            IBrickExecutionContext brickContext,
            IEnumerable<BrickContainer> brickContainers)
        {
            var dynamicBricks = brickContainers.Select(bc => bc.Brick).OfType<DynamicBrick>().ToList();
            ChangeAuthor(dynamicBricks, brickContext);
        }

        public void RecreateDynamicBricksRetainingAuthor(
            IBrickExecutionContext context,
            IEnumerable<BrickContainer> brickContainers)
        {
            var authorInformationBackup = context.AuthorInformation;

            foreach (var brickContainer in brickContainers)
            {
                RecreateDynamicBrickRetainingAuthor(context, brickContainer);
            }

            context.AuthorInformation = authorInformationBackup;
        }

        private void RecreateDynamicBrickRetainingAuthor(IBrickExecutionContext context, BrickContainer brickContainer)
        {
            var authorCustomXmlStoreInfos = AuthorManager.GetAuthorCustomXmlStoreInfos(
                brickContainer.ContentControl,
                context.Document);
            context.AuthorInformation = new AuthorInformation(
                authorCustomXmlStoreInfos,
                context.EurolookProperties);
            context.EurolookDocument.BrickEngine.RecreateDynamicBricks(context, brickContainer.CreateList());
        }

        private void ChangeAuthor(
            IEnumerable<DynamicBrick> dynamicBricks,
            IBrickExecutionContext context)
        {
            foreach (var brick in dynamicBricks)
            {
                var brickInstance = Create(brick, context.AuthorInformation.CombinedPropertiesXml);
                if (brickInstance != null)
                {
                    _brickInstancesCache[brick.Id] = brickInstance;
                    brickInstance.Renew(context);
                }
            }
        }

        public CommandBrick ResolveCommandBrick(Type commandBrickType, string parameter, string insertionMethod)
        {
            // First match by full type name, then by name; bricks with matching parameters win.
            // When called from a hot key, bricks with a hot key win.
            DocumentStructure structure;
            if (string.Equals(insertionMethod, "HotKey", StringComparison.InvariantCultureIgnoreCase))
            {
                structure =
                    _structures.FirstOrDefault(
                        ds => ds.Brick is CommandBrick commandBrick
                              && (commandBrick.CommandClass == commandBrickType.FullName)
                              && commandBrick.Argument1 == parameter
                              && !string.IsNullOrEmpty(commandBrick.HotKey))
                    ?? _structures.FirstOrDefault(
                        ds => ds.Brick is CommandBrick commandBrick
                              && commandBrick.CommandClass.Split('.').Last() == commandBrickType.Name
                              && commandBrick.Argument1 == parameter
                              && !string.IsNullOrEmpty(commandBrick.HotKey));
            }
            else
            {
                structure = _structures.FirstOrDefault(
                                ds => ds.Brick is CommandBrick commandBrick
                                      && commandBrick.CommandClass.Split('.').Last() == commandBrickType.FullName
                                      && commandBrick.Argument1 == parameter)
                            ?? _structures.FirstOrDefault(
                                ds => ds.Brick is CommandBrick commandBrick
                                      && commandBrick.CommandClass.Split('.').Last() == commandBrickType.Name
                                      && commandBrick.Argument1 == parameter);
            }

            if (structure == null)
            {
                structure = _structures.FirstOrDefault(
                                ds => ds.Brick is CommandBrick commandBrick
                                      && commandBrick.CommandClass == commandBrickType.FullName)
                            ?? _structures.FirstOrDefault(
                                ds => ds.Brick is CommandBrick commandBrick
                                      && commandBrick.CommandClass.Split('.').Last() == commandBrickType.Name);
            }

            return structure?.Brick as CommandBrick;
        }

        public DocumentStructure GetDocumentStructure(Guid brickId)
        {
            return _structures.FirstOrDefault(ds => ds.BrickId == brickId);
        }

        public void RemoveCachedBrickInstance(Guid brickId)
        {
            if (_brickInstancesCache.ContainsKey(brickId))
            {
                _brickInstancesCache.Remove(brickId);
            }
        }

        public IBrickInstance GetOrCreateBrickInstance(Brick brick)
        {
            IBrickInstance instance;
            if (!_brickInstancesCache.ContainsKey(brick.Id))
            {
                instance = Create(brick);
                _brickInstancesCache[brick.Id] = instance;
            }
            else
            {
                instance = _brickInstancesCache[brick.Id];
            }

            return instance;
        }

        protected virtual async Task ApplyAsync(
            Brick brick,
            IBrickExecutionContext context,
            string argument = null,
            bool forceInsertion = false)
        {
            try
            {
                var brickInstance = GetOrCreateBrickInstance(brick);
                if (brickInstance != null)
                {
                    bool hasBeenSelected = brickInstance.TrySelect(context, argument);
                    if (!hasBeenSelected || forceInsertion)
                    {
                        if (context.EurolookDocument.IsEurolookDocument
                            && context.EurolookDocument.IsDocumentModelKnown
                            && !context.EurolookDocument.IsStandardEurolookDocument)
                        {
                            // assert that all styles required by the brick are contained in the document
                            AssertStyles(brickInstance, context);

                            // assert that all bound texts are contained in the CustomXML part.
                            AssertTextBindings(brickInstance, context);
                        }

                        await HandleActiveModernCommentOrReply(context);

                        await brickInstance.ApplyAsync(context, argument);
                    }
                }
            }
            catch (COMException comEx) when (comEx.IsWarning())
            {
                this.LogWarn($"Failed to execute brick {brick.DisplayName}", comEx);
            }
            catch (Exception ex)
            {
                this.LogError($"Failed to execute brick {brick.DisplayName}", ex);
            }
        }

        protected bool CanExecute(Brick brick, IBrickExecutionContext context, string argument = null)
        {
            if (brick is CommandBrick && GetOrCreateBrickInstance(brick) is CommandBrickInstance brickInstance)
            {
                return brickInstance.CanExecute(context, argument);
            }

            return true;
        }

        /// <summary>
        /// Moves the focus to the main document if a comment or reply is active.
        /// </summary>
        /// <param name="context">An instance of <see cref="IBrickExecutionContext" />.</param>
        /// <returns>A <see cref="Task" /> to be awaited.</returns>
        /// <remarks>
        /// Modern comments are in conflict with automation. Certain methods and properties
        /// cannot be used while a modern comment is being edited. This method uses a work-around to
        /// deactivate the comment and move the focus back to the document.
        /// </remarks>
        private static async Task HandleActiveModernCommentOrReply(IBrickExecutionContext context)
        {
            var retryCounter = 0;
            while (retryCounter < 15 && context.Document.IsModernCommentActive())
            {
                retryCounter++;

                var hWndWord = context.Document.ActiveWindow.GetWordWindowHandle();

                var inputs = new[]
                {
                    new SafeNativeMethods.INPUT
                    {
                        type = SafeNativeMethods.InputType.KEYBOARD,
                        U = new SafeNativeMethods.InputUnion
                        {
                            ki = new SafeNativeMethods.KEYBDINPUT
                            {
                                wVk = SafeNativeMethods.VirtualKeyShort.ESCAPE,
                            },
                        },
                    },
                };

                // send ESC to move focus from comment (2x ESC needed) or reply (3x ESC needed) back to document
                SafeNativeMethods.SetForegroundWindow(hWndWord);

                SafeNativeMethods.SendInput((uint)inputs.Length, inputs, SafeNativeMethods.INPUT.Size);
                SafeNativeMethods.SendInput((uint)inputs.Length, inputs, SafeNativeMethods.INPUT.Size);
                SafeNativeMethods.SendInput((uint)inputs.Length, inputs, SafeNativeMethods.INPUT.Size);

                // wait until input is processed
                await Task.Delay(100);
            }
        }

        private IBrickInstance Create(Brick brick)
        {
            var contentBrick = brick as ContentBrick;
            if (contentBrick != null)
            {
                var combinedPropertiesXml = _brickExecutionContextFactory().AuthorInformation.CombinedPropertiesXml;
                return Create(contentBrick, combinedPropertiesXml);
            }

            var buildingBlockBrick = brick as BuildingBlockBrick;
            if (buildingBlockBrick != null)
            {
                return Create(buildingBlockBrick);
            }

            var commandBrick = brick as CommandBrick;
            if (commandBrick != null)
            {
                return Create(commandBrick);
            }

            throw new ArgumentException(
                $"Type of brick {brick.Name} ({brick.Id}) not recognized: {brick.GetType()}",
                nameof(brick));
        }

        private ContentBrickInstance Create(ContentBrick contentBrick, CombinedPropertiesXml combinedPropertiesXml)
        {
            if (contentBrick == null)
            {
                return null;
            }

            contentBrick.Initialize(combinedPropertiesXml);
            return new ContentBrickInstance(
                contentBrick,
                _structures,
                _resourceResolver,
                _settingsService,
                _termStoreCache,
                _stylesExtractor,
                _documentModel,
                _documentModelLanguage,
                Language,
                AuthorManager,
                _contentBrickActionsResolver);
        }

        private BuildingBlockBrickInstance Create(BuildingBlockBrick buildingBlockBrick)
        {
            if (buildingBlockBrick == null)
            {
                return null;
            }

            return new BuildingBlockBrickInstance(
                buildingBlockBrick,
                _resourceResolver,
                _termStoreCache,
                _stylesExtractor,
                _documentModel,
                _documentModelLanguage,
                Language);
        }

        private CommandBrickInstance Create(CommandBrick commandBrick)
        {
            if (commandBrick == null)
            {
                return null;
            }

            return new CommandBrickInstance(commandBrick, _resolver, _resourceResolver);
        }

        private void AssertStyles(IBrickInstance brickInstance, IBrickExecutionContext context)
        {
            // when copying styles using Application.OrganizerCopy we need to provide the display name of the style
            // and not the internal style name used by the BDL --> use the StyleNameMapper class to convert names
            var styleNameMapper = new StyleNameMapper(_documentModelLanguage?.Template ?? _documentModel.Template);

            var alwaysRequiredStyles =
                new[] { "BodyPlaceholderText", "PlaceholderText" }.Select(s => styleNameMapper.ToDisplayName(s));
            var requiredStylesAsDisplayNames =
                brickInstance.RequiredStyles
                             .Concat(alwaysRequiredStyles)
                             .Select(s => styleNameMapper.ToDisplayName(s))
                             .Distinct().ToList();

            var stylesNotInDocument =
                requiredStylesAsDisplayNames.Where(
                    styleName => !LanguageIndependentStyle.IsStyleAvailable(
                        styleName,
                        context.Document)).ToList();

            if (!stylesNotInDocument.Any())
            {
                return;
            }

            context.Document.CopyStylesFromDocumentModel(stylesNotInDocument, _documentModel, _documentModelLanguage);
        }

        private void AssertTextBindings(IBrickInstance brickInstance, IBrickExecutionContext context)
        {
            var customXmlPart = context.Document.CustomXMLParts.Cast<CustomXMLPart>()
                                       .FirstOrDefault(
                                           part => part.DocumentElement.BaseName == TextsCustomXml.RootName);

            // NOTE: We only add missing texts here and do not fully refresh the entiry CustomXML part as this
            //   might have undesired effects (i.e. texts changing in other places of the document.
            //   Texts might be missing if new bricks are added to the document model or existing bricks are
            //   modified to contain new texts.
            if (customXmlPart != null)
            {
                var texts = brickInstance?.Brick?.Texts;
                if (texts == null)
                {
                    return; // EUROLOOK-1495: For some reason texts was null instead of empty.
                }

                foreach (var brickText in texts)
                {
                    string alias = brickText.Text.Alias;
                    var node = customXmlPart.SelectSingleNode($"/Texts/{alias}");
                    if (node == null)
                    {
                        this.LogTrace($"Referenced text {alias} isn't present in TextsXml, reloading it from local database.");

                        // translation is not contained in TextsCustomXml
                        var localizedText = _resourceResolver.ResolveTranslation(alias);
                        if (localizedText != null)
                        {
                            customXmlPart.DocumentElement.AppendChildNode(
                                alias,
                                "",
                                MsoCustomXMLNodeType.msoCustomXMLNodeElement,
                                localizedText.Value);
                        }
                        else
                        {
                            this.LogWarn($"Referenced text {alias} doesn't exist in local database.");
                        }
                    }
                }
            }
        }
    }
}
