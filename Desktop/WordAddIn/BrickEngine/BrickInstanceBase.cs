﻿using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickEngine
{
    public abstract class BrickInstanceBase
    {
        protected static void SelectInsertedBrick(
            DocumentAutomationHelper documentAutomationHelper,
            IReadOnlyList<Range> insertPositions,
            IEnumerable<DocumentStructure> structures)
        {
            var firstOccurrence = GetFirstVisibleRangeOrDefault(insertPositions);

            if (firstOccurrence == null)
            {
                return;
            }

            // first try to select the first placeholder
            if (firstOccurrence.SelectFirstPlaceholderContentControl(documentAutomationHelper.PreferredViewType))
            {
                return;
            }

            // then try to select the first input control (e.g. a drop-down list, date field, etc.)
            if (firstOccurrence.SelectFirstEditableContentControl(documentAutomationHelper.PreferredViewType))
            {
                return;
            }

            // otherwise put insertion pointer behind brick
            if (structures != null && structures.Any(s => s.Position == PositionType.Cursor))
            {
                firstOccurrence.Collapse(WdCollapseDirection.wdCollapseEnd);
            }

            firstOccurrence.SelectInPreferredView(documentAutomationHelper.PreferredViewType);
        }

        protected static Range GetFirstVisibleRangeOrDefault(IReadOnlyList<Range> ranges)
        {
            // try to avoid selecting headers/footers that are not visible in print layout
            return ranges.FirstOrDefault(r => r.StoryType == WdStoryType.wdMainTextStory)
                   ?? ranges.FirstOrDefault(
                       r => r.Sections.First.PageSetup.DifferentFirstPageHeaderFooter == -1
                            && (r.StoryType == WdStoryType.wdFirstPageFooterStory
                                || r.StoryType == WdStoryType.wdFirstPageHeaderStory))
                   ?? ranges.FirstOrDefault(
                       r => r.Sections.First.PageSetup.OddAndEvenPagesHeaderFooter == 0
                            && (r.StoryType == WdStoryType.wdPrimaryFooterStory
                                || r.StoryType == WdStoryType.wdPrimaryHeaderStory))
                   ?? ranges.OrderBy(r => r.StoryType, new StoryTypeComparer()).FirstOrDefault();
        }
    }
}
