using System.Threading.Tasks;

namespace Eurolook.WordAddIn.BrickEngine
{
    public interface IContentBrickActions
    {
        Task AfterDocumentCreationAsync(IEurolookWordDocument eurolookDocument);

        Task AfterBrickInsertionAsync(IBrickExecutionContext context);

        Task DeleteBrickAsync(IBrickExecutionContext context);
    }
}
