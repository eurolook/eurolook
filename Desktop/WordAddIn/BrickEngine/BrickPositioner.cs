﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Common.Log;
using Eurolook.Data;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing.Extensions;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickEngine
{
    public class BrickPositioner : ICanLog
    {
        /// <summary>
        /// Subset of <see cref="DocumentStructure" /> where the brick reference
        /// points to a <see cref="ContentBrick" />.
        /// </summary>
        private readonly List<DocumentStructure> _documentStructures;

        /// <summary>
        /// Initializes a new instance of the <see cref="BrickPositioner" /> class.
        /// </summary>
        /// <param name="documentStructure">The structure for one specific document model.</param>
        /// <remarks>Each document model needs its own <see cref="BrickPositioner" />.</remarks>
        public BrickPositioner(List<DocumentStructure> documentStructure)
        {
            _documentStructures = documentStructure;
        }

        public IEnumerable<Range> SearchForBrick(Brick brick, Document doc)
        {
            var cc = doc.GetAllBrickContainers().Where(bc => bc.Id == brick.Id).Select(bc => bc.ContentControl).ToList();
            if (cc.Any())
            {
                return cc.Select(
                    c =>
                    {
                        var range = c.Range;
                        range.Start = range.Start - 1;
                        range.End = range.End + 1;
                        if (!range.IsEndOfStory())
                        {
                            range.Expand(WdUnits.wdParagraph);
                        }

                        return range;
                    });
            }

            return Enumerable.Empty<Range>();
        }

        public List<InsertPosition> FindInsertPositions(DocumentStructure documentStructure, IBrickExecutionContext context)
        {
            return documentStructure.Position == PositionType.Cursor
                ? FindCursorInsertPositions(context)
                : FindFixedInsertPositions(documentStructure, context);
        }

        [NotNull]
        private static Range CreateFallbackPosition(DocumentStructure documentStructure, Range range)
        {
            switch (documentStructure.Position)
            {
                case PositionType.HeaderNone:
                case PositionType.HeaderFirst:
                case PositionType.HeaderOdd:
                case PositionType.HeaderEven:
                case PositionType.HeaderFirstOdd:
                case PositionType.HeaderFirstEven:
                case PositionType.HeaderOddEven:
                case PositionType.HeaderAll:
                    {
                        // use the first paragraph in the range
                        var firstP = range.Paragraphs.First.Range;
                        return !string.IsNullOrWhiteSpace(firstP.Text?.Trim('\r')) ? CreateParagraphBefore(firstP) : firstP;
                    }

                case PositionType.Begin:
                case PositionType.Body:
                    {
                        // use the first paragraph in the range
                        var firstP = range.Paragraphs.First.Range;
                        return CreateParagraphBefore(firstP);
                    }

                case PositionType.FooterNone:
                case PositionType.FooterFirst:
                case PositionType.FooterOdd:
                case PositionType.FooterEven:
                case PositionType.FooterFirstOdd:
                case PositionType.FooterFirstEven:
                case PositionType.FooterOddEven:
                case PositionType.FooterAll:
                    {
                        // use the last paragraph in the range
                        var lastP = range.Paragraphs.Last.Range;
                        return !string.IsNullOrWhiteSpace(lastP.Text?.Trim('\r')) ? CreateParagraphAfter(lastP) : lastP;
                    }

                case PositionType.End:
                    {
                        // use the last paragraph in the range
                        var lastP = range.Paragraphs.Last.Range;
                        return CreateParagraphAfter(lastP);
                    }

                default:
                    {
                        // use the last paragraph in the range
                        var lastP = range.Paragraphs.Last.Range;
                        return CreateParagraphAfter(lastP);
                    }
            }
        }

        [NotNull]
        private static Range CreateParagraphBefore(Range range)
        {
            var result = range.Paragraphs.First.Range;
            result.InsertParagraphBeforeAndHandleLockedContentControls();
            result.Collapse(WdCollapseDirection.wdCollapseStart);
            return result;
        }

        [NotNull]
        private static Range CreateParagraphAfter(Range range)
        {
            var result = range.InsertParagraphAfterAndHandleLockedContentControls();
            result.Collapse(WdCollapseDirection.wdCollapseStart);
            return result;
        }

        private List<InsertPosition> FindCursorInsertPositions(IBrickExecutionContext context)
        {
            var originalRange = context.Selection.Range;
            var range = originalRange.GetCursorBrickInsertionRange();
            return new List<InsertPosition> { new InsertPosition(range, true) };
        }

        private List<InsertPosition> FindFixedInsertPositions(DocumentStructure documentStructure, IBrickExecutionContext context)
        {
            var result = new List<InsertPosition>();
            var documentRanges = GetDocumentRanges(documentStructure, context);
            foreach (var range in documentRanges)
            {
                Range insertRange;
                var workingRange = range.Duplicate;
                var anchor = FindAnchorBefore(documentStructure, workingRange);
                if (anchor != null)
                {
                    insertRange = CreateParagraphAfter(anchor);
                }
                else
                {
                    anchor = FindAnchorAfter(documentStructure, workingRange);
                    insertRange = anchor != null ? CreateParagraphBefore(anchor) : CreateFallbackPosition(documentStructure, workingRange);
                }

                result.Add(new InsertPosition(insertRange, true));
            }

            return result;
        }

        [CanBeNull]
        private Range FindAnchorAfter(DocumentStructure documentStructure, Range range)
        {
            // find all bricks of the same content type (begin, end, etc)
            // sort the bricks ascending, that way, the first brick in the result list is the best match
            var candidates = from cb in _documentStructures
                             where cb.Brick is ContentBrick
                                   && EnumHelper.IsPositionMatch(documentStructure.Position, cb.Position)
                                   && cb.VerticalPositionIndex > documentStructure.VerticalPositionIndex
                             orderby cb.VerticalPositionIndex ascending
                             select cb;

            return FindAnchor(candidates, range);
        }

        [CanBeNull]
        private Range FindAnchorBefore(DocumentStructure documentStructure, Range range)
        {
            // find all bricks of the same content type (begin, end, etc)
            // sort the bricks descending, that way, the first brick in the result list is the best match

            // special handling for body bricks
            var position = documentStructure.Position != PositionType.Body ? documentStructure.Position : PositionType.Begin;
            int verticalPositionIndex = documentStructure.Position != PositionType.Body
                ? documentStructure.VerticalPositionIndex
                : int.MaxValue;

            var candidates = from cb in _documentStructures
                             where cb.Brick is ContentBrick
                                   && EnumHelper.IsPositionMatch(position, cb.Position)
                                   && cb.VerticalPositionIndex <= verticalPositionIndex
                             orderby cb.Position descending, cb.VerticalPositionIndex descending
                             select cb;

            return FindAnchor(candidates, range);
        }

        [CanBeNull]
        private Range FindAnchor(IEnumerable<DocumentStructure> candidates, Range range)
        {
            foreach (var candidate in candidates)
            {
                var cc = range.ContentControls.OfType<ContentControl>()
                              .Select(c => c.GetBrickContainer()).Where(bc => bc != null && bc.Id == candidate.Brick.Id)
                              .Select(bc => bc.ContentControl)
                              .LastOrDefault();

                if (cc == null)
                {
                    continue;
                }

                var r = cc.Range;
                r.Start = r.Start - 1;
                r.End = r.End + 1;
                r.Expand(WdUnits.wdParagraph);

                // special case: the content control contains a table
                if (r.HasTables())
                {
                    int endOfLastTable = r.Tables.OfType<Table>().LastOrDefault()?.Range.End ?? r.End;
                    r.End = Math.Max(r.End, endOfLastTable);
                }

                return r;
            }

            return null;
        }

        /// <summary>
        /// Gets the entire ranges of the document such as headers or footers where the DocumentStructure belongs to.
        /// </summary>
        /// <param name="ds">The DocumentStructure.</param>
        /// <param name="context">The context object providing helpful information about the current document.</param>
        /// <returns>Returns the ranges of the document, headers or footers where the DocumentStructure belongs to.</returns>
        private IEnumerable<Range> GetDocumentRanges(DocumentStructure ds, IBrickExecutionContext context)
        {
            var result = new List<Range>();
            var sections = context.Selection.Sections.OfType<Section>().ToList();

            // first page header
            if (ds.Position == PositionType.HeaderFirst || ds.Position == PositionType.HeaderAll)
            {
                result.AddRange(sections.Select(s => s.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range));
            }

            // odd pages header
            if (ds.Position == PositionType.HeaderOdd || ds.Position == PositionType.HeaderAll || ds.Position == PositionType.HeaderOddEven)
            {
                result.AddRange(sections.Select(s => s.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range));
            }

            // even pages header
            if (ds.Position == PositionType.HeaderEven || ds.Position == PositionType.HeaderAll || ds.Position == PositionType.HeaderOddEven)
            {
                result.AddRange(sections.Select(s => s.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].Range));
            }

            // first page footer
            if (ds.Position == PositionType.FooterFirst || ds.Position == PositionType.FooterAll)
            {
                result.AddRange(sections.Select(s => s.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range));
            }

            // odd pages footer
            if (ds.Position == PositionType.FooterOdd || ds.Position == PositionType.FooterAll || ds.Position == PositionType.FooterOddEven)
            {
                result.AddRange(sections.Select(s => s.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range));
            }

            // even pages footer
            if (ds.Position == PositionType.FooterEven || ds.Position == PositionType.FooterAll || ds.Position == PositionType.FooterOddEven)
            {
                result.AddRange(sections.Select(s => s.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].Range));
            }

            // main story
            if (ds.Position == PositionType.Begin || ds.Position == PositionType.Body || ds.Position == PositionType.End)
            {
                result.Add(context.Document.Range());
            }

            return result;
        }
    }
}
