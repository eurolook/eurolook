using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data.Models;

namespace Eurolook.WordAddIn.BrickEngine
{
    public interface IBrickInstance
    {
        Brick Brick { get; }

        bool HasAuthorBinding { get; }

        IEnumerable<string> RequiredStyles { get; }

        /// <summary>
        /// Checks if the brick is already applied to the document and if so selects the brick.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="argument"></param>
        /// <returns>True, if the brick has been selected, false otherwise.</returns>
        bool TrySelect(IBrickExecutionContext context, string argument = null);

        Task ApplyAsync(IBrickExecutionContext context, string argument = null);
    }
}
