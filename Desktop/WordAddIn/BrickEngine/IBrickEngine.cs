﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing;

namespace Eurolook.WordAddIn.BrickEngine
{
    public interface IBrickEngine
    {
        Task ApplyBrickAsync(Guid id, string argument = null);

        Task ApplyBrickAsync(string brickName, string argument = null);

        Task ApplyBrickAsync(Brick brick, string argument = null);

        Task ApplyBrickWithCheckForDeletedRevisionsAsync(Brick brick);

        Task ApplyBrickAndLogInsertionAsync(Type commandBrickType, string parameter, string insertionMethod);

        bool CanExecute(Brick brick, string argument = null);

        void RecreateDynamicBricks(IBrickExecutionContext context, IEnumerable<BrickContainer> brickContainers);

        void RecreateDynamicBricksRetainingAuthor(IBrickExecutionContext context, IEnumerable<BrickContainer> brickContainers);

        CommandBrick ResolveCommandBrick(Type commandBrickType, string parameter, string insertionMethod);

        DocumentStructure GetDocumentStructure(Guid brickId);

        IBrickInstance GetOrCreateBrickInstance(Brick brick);

        void RemoveCachedBrickInstance(Guid brickId);
    }
}
