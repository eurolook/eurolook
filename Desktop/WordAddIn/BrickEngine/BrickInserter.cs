﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Eurolook.Common;
using Eurolook.Data.Models;
using Eurolook.Data.TermStore;
using Eurolook.Data.Xml;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.DocumentProcessing.OpenXml;
using Eurolook.WordAddIn.Extensions;
using JetBrains.Annotations;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using Category = DocumentFormat.OpenXml.Wordprocessing.Category;
using Document = Microsoft.Office.Interop.Word.Document;
using Language = Eurolook.Data.Models.Language;
using Paragraph = DocumentFormat.OpenXml.Wordprocessing.Paragraph;
using Style = DocumentFormat.OpenXml.Wordprocessing.Style;

namespace Eurolook.WordAddIn.BrickEngine
{
    public class BrickInserter
    {
        private readonly IBrickExecutionContext _context;
        private readonly ILocalisedResourceResolver _resourceResolver;
        private readonly ITermStoreCache _termStoreCache;
        private readonly IStylesExtractor _stylesExtractor;
        private readonly DocumentModel _documentModel;
        [CanBeNull]
        private readonly DocumentModelLanguage _documentModelLanguage;
        private readonly Language _language;
        private BrickConverter _brickConverter;

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times", Justification = "Reviewed")]
        public BrickInserter(
            IBrickExecutionContext context,
            ILocalisedResourceResolver resourceResolver,
            ITermStoreCache termStoreCache,
            IStylesExtractor stylesExtractor,
            DocumentModel documentModel,
            Language language,
            DocumentModelLanguage documentModelLanguage = null)
        {
            _context = context;
            _resourceResolver = resourceResolver;
            _termStoreCache = termStoreCache;
            _stylesExtractor = stylesExtractor;
            _documentModel = documentModel;
            _language = language;
            _documentModelLanguage = documentModelLanguage;
        }

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times", Justification = "Reviewed")]
        public IEnumerable<InsertPosition> Insert(DocumentStructure documentStructure, IList<InsertPosition> positions, AuthorInformation authorInformation)
        {
            if (positions == null || !positions.Any())
            {
                return Enumerable.Empty<InsertPosition>();
            }

            string addInName = $"EL_Brick_{Guid.NewGuid()}.dotx";
            string addInFullName = Path.Combine(Path.GetTempPath(), addInName);
            string buildBlockCategory = "General";
            string buildBlockName = "Brick";
            CreateBuildingBlockTemplate(documentStructure, addInFullName, authorInformation, buildBlockCategory, buildBlockName);
            InsertBuildingBlock(positions, addInFullName, buildBlockCategory, buildBlockName);

            WriteDocumentProperties(_brickConverter.Context.CustomDocumentProperties, _context.Document);
            WriteEurolookProperties(_brickConverter.Context.CustomEurolookProperties, _context.Document);

            return positions;
        }

        public IEnumerable<InsertPosition> Insert(
            IList<InsertPosition> positions,
            BuildingBlockBrick buildingBlockBrick,
            string buildingBlockCategory,
            string buildingBlockName)
        {
            if (positions == null || !positions.Any())
            {
                return Enumerable.Empty<InsertPosition>();
            }

            using (var temporaryFile = new TemporaryFile(".dotx"))
            {
                File.WriteAllBytes(temporaryFile.FullName, buildingBlockBrick.BuildingBlockBytes);
                InsertBuildingBlock(positions, temporaryFile.FullName, buildingBlockCategory, buildingBlockName);
            }

            return positions;
        }

        private void WriteEurolookProperties(Dictionary<string, string> properties, Document document)
        {
            if (!properties.Any())
            {
                return;
            }

            var eurolookCustomXml = document.GetCustomXmlAsXDocument(EurolookPropertiesCustomXml.RootName);
            if (eurolookCustomXml == null)
            {
                return;
            }

            foreach (var property in properties)
            {
                var element = eurolookCustomXml.XPathSelectElement(property.Key);
                element?.SetValue(property.Value);
            }

            document.ReplaceCustomXml(EurolookPropertiesCustomXml.RootName, eurolookCustomXml);
        }

        private void WriteDocumentProperties(Dictionary<string, string> properties, Document document)
        {
            foreach (var brickProperty in properties)
            {
                bool isSet = false;
                foreach (DocumentProperty documentProperty in ((dynamic)document.CustomDocumentProperties))
                {
                    if (documentProperty.Name == brickProperty.Key)
                    {
                        documentProperty.Type = MsoDocProperties.msoPropertyTypeString;
                        documentProperty.Value = brickProperty.Value;
                        isSet = true;
                    }
                }

                if (!isSet)
                {
                    ((dynamic)document.CustomDocumentProperties).Add(
                        brickProperty.Key,
                        false,
                        MsoDocProperties.msoPropertyTypeString,
                        brickProperty.Value);
                }
            }
        }

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times", Justification = "Reviewed")]
        private void CreateBuildingBlockTemplate(
            DocumentStructure documentStructure,
            string addInFullName,
            AuthorInformation authorInformation,
            string buildingBlockCategory,
            string buildingBlockName)
        {
            using (var stream = new MemoryStream())
            {
                var template = _documentModelLanguage?.Template ?? _documentModel.Template;
                stream.Write(template, 0, template.Length);
                using (var docx = WordprocessingDocument.Open(stream, true))
                {
                    docx.CleanBody();
                    docx.CleanupDocument();

                    CopyCustomXmlPart(docx, TextsCustomXml.RootName, TextsCustomXml.DatastoreItemId);
                    CopyCustomXmlParts(docx, AuthorCustomXml.RootName);
                    CopyCustomXmlPart(docx, EurolookPropertiesCustomXml.RootName, EurolookPropertiesCustomXml.DatastoreItemId);

                    // create glossary document
                    var glossary = docx.GetOrCreateGlossaryDocumentPart();
                    glossary.GlossaryDocument.AddDefaultNamespaceDeclarations();
                    if (glossary.GlossaryDocument.DocParts == null)
                    {
                        glossary.GlossaryDocument.AppendChild(new DocParts());
                    }

                    // replace styles in glossary document with the ones from the main document
                    var documentStyleDefinitionsPart = docx.MainDocumentPart.StyleDefinitionsPart;
                    var glossaryStyleDefinitionsPart = glossary.StyleDefinitionsPart ?? glossary.AddNewPart<StyleDefinitionsPart>();
                    OpenXmlUtils.CopyPart(documentStyleDefinitionsPart, glossaryStyleDefinitionsPart);

                    AddMissingStyles(glossary);

                    // replace numbering in glossary document with the one form the main document
                    var documentNumberingDefinitionsPart = docx.MainDocumentPart.NumberingDefinitionsPart;
                    var glossaryNumberingDefinitionsPart = glossary.NumberingDefinitionsPart
                                                           ?? glossary.AddNewPart<NumberingDefinitionsPart>();
                    OpenXmlUtils.CopyPart(documentNumberingDefinitionsPart, glossaryNumberingDefinitionsPart);

                    // convert the brick into the document
                    _brickConverter = new BrickConverter(
                        new OpenXmlConverterContext(docx, _documentModel, authorInformation.AuthorCustomXmlStoreInfos)
                        {
                            Language = _language,
                            ResourceResolver = _resourceResolver,
                            Options = new OpenXmlConversionOptions
                            {
                                DocumentMode = WordprocessingDocumentMode.GlossaryDocument,
                                CreateTableOfContentsContainer = false,
                            },
                            CurrentPart = docx.MainDocumentPart.GlossaryDocumentPart,
                            TermStoreCache = _termStoreCache,
                        });
                    var openXml = _brickConverter.ToOpenXml(documentStructure);

                    string guid = Guid.NewGuid().ToString("B").ToUpperInvariant();
                    string partName = buildingBlockName; // "_" + guid; //_documentStructure.Brick.DisplayName

                    // setup properties
                    var docPartProperties = new DocPartProperties
                    {
                        DocPartName = new DocPartName { Val = partName },
                        Category = new Category(
                            new Name { Val = buildingBlockCategory },
                            new Gallery { Val = DocPartGalleryValues.DocumentPart }),
                        Behaviors = new Behaviors(new Behavior { Val = DocPartBehaviorValues.Content }),
                        DocPartId = new DocPartId { Val = guid },
                    };

                    // setup body
                    var body = new DocPartBody();
                    body.Append(openXml);
                    body.AppendChild(new Paragraph());

                    var newPart = new DocPart();
                    newPart.AppendChild(docPartProperties);
                    newPart.AppendChild(body);

                    glossary.GlossaryDocument.DocParts.AppendChild(newPart);
                    glossary.GlossaryDocument.Save();
                }

                using (var fileStream = new FileStream(addInFullName, FileMode.Create, FileAccess.Write))
                {
                    stream.WriteTo(fileStream);
                }
            }
        }

        private void InsertBuildingBlock(
            IList<InsertPosition> positions,
            string addInFullName,
            string buildingBlockCategory,
            string buildingBlockName)
        {
            var addIn = _context.Application.AddIns.Add(addInFullName);
            string addInName = Path.GetFileName(addInFullName);

            try
            {
                var template = _context.Application.Templates
                                       .OfType<Template>()
                                       .FirstOrDefault(t => t.Name == addInName);
                if (template == null)
                {
                    return;
                }

                var buildingBlock = template.BuildingBlockTypes.Item(WdBuildingBlockTypes.wdTypeQuickParts)
                                            .Categories
                                            .Item(buildingBlockCategory).BuildingBlocks.Item(buildingBlockName);

                foreach (var insertPosition in positions)
                {
                    var range = insertPosition.Range;
                    var actualPosition = buildingBlock.Insert(range, true);

                    // set pos range to the actual brick
                    range.Start = actualPosition.Start;
                    range.End = actualPosition.End;

                    if (insertPosition.HasAdditionalParagraph)
                    {
                        actualPosition.DeleteAdditionalParagraphAtEnd();
                    }
                }

                if (!template.Saved)
                {
                    // Make sure the building block template is not flagged as modified
                    // to avoid any prompts to save this file.
                    template.Saved = true;
                }
            }
            finally
            {
                addIn.Delete();
                File.Delete(addInFullName);
            }
        }

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times", Justification = "Reviewed")]
        private void CopyCustomXmlPart(WordprocessingDocument docx, string rootName, Guid datastoreItemId)
        {
            using (var ms = new MemoryStream())
            {
                using (var writer = XmlWriter.Create(ms))
                {
                    _context.Document.GetCustomXmlAsXDocument(rootName)?.WriteTo(writer);
                }

                docx.AddCustomXml(ms, datastoreItemId);
            }
        }

        private void CopyCustomXmlParts(WordprocessingDocument docx, string rootName)
        {
            foreach (var customXmlPart in _context.Document.GetCustomXmlParts(rootName))
            {
                using (var ms = new MemoryStream())
                {
                    using (var writer = XmlWriter.Create(ms))
                    {
                        XDocument.Parse(customXmlPart.XML).WriteTo(writer);
                    }

                    docx.AddCustomXml(ms, Guid.Parse(customXmlPart.Id));
                }
            }
        }

        private void AddMissingStyles(GlossaryDocumentPart glossary)
        {
            var currentStyles = glossary.StyleDefinitionsPart.Styles.Descendants<Style>();
            var stylesInTemplate = _stylesExtractor.ExtractStyles(_documentModel, _documentModelLanguage);
            var missingStyles = stylesInTemplate.Except(currentStyles, new StyleByNameComparer());
            foreach (var style in missingStyles)
            {
                glossary.StyleDefinitionsPart.Styles.AppendChild(style.CloneNode(true));
            }
        }
    }
}
