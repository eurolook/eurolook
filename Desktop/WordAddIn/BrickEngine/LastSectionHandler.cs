﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickEngine
{
    public class LastSectionHandler
    {
        public void RemoveEmptyLastSection(IEnumerable<InsertPosition> insertPositions)
        {
            foreach (var insertPosition in insertPositions)
            {
                RemoveEmptyLastSection(insertPosition.Range);
            }
        }

        public void RemoveEmptyLastSection(Range range)
        {
            // move start backwards by two characters to cross the content control border
            var rangeTmp = range.CollapseToEnd();
            rangeTmp.MoveStart(WdUnits.wdCharacter, -2);
            if (rangeTmp.Text?.Length == 2)
            {
                rangeTmp.MoveStart(WdUnits.wdCharacter, 1);
            }

            if (rangeTmp.Text == "\f" && range.IsEndOfStory())
            {
                var section = rangeTmp.Sections.First;
                var pageSetup = new PageSetupProperties(section.PageSetup);

                var lastSection = range.Sections.First;
                lastSection.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].LinkToPrevious = true;
                lastSection.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].LinkToPrevious = true;
                lastSection.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].LinkToPrevious = true;
                lastSection.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].LinkToPrevious = true;
                lastSection.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].LinkToPrevious = true;
                lastSection.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].LinkToPrevious = true;

                rangeTmp.Text = "";

                rangeTmp.Delete();

                section = rangeTmp.Sections.First;
                pageSetup.ApplyTo(section.PageSetup);
            }
        }

        public void ReInsertSectionBreak(
            IEnumerable<InsertPosition> insertPositions,
            List<DocumentStructure> documentStructures)
        {
            foreach (var insertPosition in insertPositions)
            {
                ReInsertSectionBreak(insertPosition, documentStructures);
            }
        }

        public void ReInsertSectionBreak(InsertPosition insertPosition, List<DocumentStructure> documentStructures)
        {
            if (insertPosition.Range.StoryType != WdStoryType.wdMainTextStory
                || !insertPosition.Range.IsEndOfStory())
            {
                return;
            }

            var brickContainers = insertPosition.Range.Document.GetAllBrickContainers();
            var lastBrickContainer = brickContainers.LastOrDefault(
                b => b.ContentControl.Range.StoryType == WdStoryType.wdMainTextStory);
            if (lastBrickContainer == null)
            {
                return;
            }

            var rangeLastBrick = lastBrickContainer.ContentControl.Range;

            var rangeTmp = insertPosition.Range.Duplicate;
            rangeTmp.Start = rangeLastBrick.End;

            if (!string.IsNullOrWhiteSpace(rangeTmp.Text))
            {
                return;
            }

            var contentBrick = documentStructures.Where(ds => ds.BrickId == lastBrickContainer.Id)
                                                 .Select(ds => ds.Brick)
                                                 .OfType<ContentBrick>()
                                                 .FirstOrDefault();
            if (contentBrick == null)
            {
                return;
            }

            var lastBdlElement = contentBrick.Content.Root?.Elements().LastOrDefault();
            if (IsLastBdlElementSectionBreak(lastBdlElement))
            {
                lastBrickContainer.ContentControl.Delete();
                var endOfLastBrick = rangeLastBrick.CollapseToEnd();
                endOfLastBrick.Move(WdUnits.wdCharacter, 1);
                endOfLastBrick.InsertBreak(WdBreakType.wdSectionBreakNextPage);

                endOfLastBrick.Start -= 1;
                endOfLastBrick.set_Style(WdBuiltinStyle.wdStyleNormal);

                rangeLastBrick.End = endOfLastBrick.End;

                var newBrickContainer = new BrickContainer(contentBrick);
                var newBrickContentControl = rangeLastBrick.ContentControls.Add();
                newBrickContentControl.Title = newBrickContainer.ContainerDisplayName;
                newBrickContentControl.Tag = lastBrickContainer.ToTag();

                insertPosition.Range.Start = endOfLastBrick.End;
            }
        }

        private static bool IsLastBdlElementSectionBreak(XElement bdlElement)
        {
            return bdlElement != null && bdlElement.Name == "div"
                                                   && ((string)bdlElement.Attribute("data-type"))
                                                   == "section-break";
        }

        public class PageSetupProperties
        {
            public PageSetupProperties(PageSetup pageSetup)
            {
                LeftMargin = pageSetup.LeftMargin;
                TopMargin = pageSetup.TopMargin;
                RightMargin = pageSetup.RightMargin;
                BottomMargin = pageSetup.BottomMargin;
                DifferentFirstPageHeaderFooter = pageSetup.DifferentFirstPageHeaderFooter;
                OddAndEvenPagesHeaderFooter = pageSetup.OddAndEvenPagesHeaderFooter;
                FooterDistance = pageSetup.FooterDistance;
                HeaderDistance = pageSetup.HeaderDistance;

                Gutter = pageSetup.Gutter;
                GutterOnTop = pageSetup.GutterOnTop;
                GutterPos = pageSetup.GutterPos;
                GutterStyle = pageSetup.GutterStyle;
                CharsLine = pageSetup.CharsLine;
                MirrorMargins = pageSetup.MirrorMargins;
                Orientation = pageSetup.Orientation;

                PageWidth = pageSetup.PageWidth;
                PageHeight = pageSetup.PageHeight;

                VerticalAlignment = pageSetup.VerticalAlignment;
                SectionStart = pageSetup.SectionStart;
                SuppressEndnotes = pageSetup.SuppressEndnotes;
                //pageSetup.LineNumbering
                //pageSetup.TextColumns
                PaperSize = pageSetup.PaperSize;
                TwoPagesOnOne = pageSetup.TwoPagesOnOne;
                LinesPage = pageSetup.LinesPage;
                SectionDirection = pageSetup.SectionDirection;
                LayoutMode = pageSetup.LayoutMode;
                BookFoldPrinting = pageSetup.BookFoldPrinting;
                BookFoldRevPrinting = pageSetup.BookFoldRevPrinting;
                BookFoldPrintingSheets = pageSetup.BookFoldPrintingSheets;
            }

            public int BookFoldPrintingSheets { get; set; }

            public bool BookFoldRevPrinting { get; set; }

            public bool BookFoldPrinting { get; set; }

            public WdLayoutMode LayoutMode { get; set; }

            public WdSectionDirection SectionDirection { get; set; }

            public float LinesPage { get; set; }

            public bool TwoPagesOnOne { get; set; }

            public WdPaperSize PaperSize { get; set; }

            public int SuppressEndnotes { get; set; }

            public WdSectionStart SectionStart { get; set; }

            public WdVerticalAlignment VerticalAlignment { get; set; }

            public float PageHeight { get; set; }

            public float PageWidth { get; set; }

            public WdOrientation Orientation { get; set; }

            public int MirrorMargins { get; set; }

            public float CharsLine { get; set; }

            public WdGutterStyleOld GutterStyle { get; set; }

            public WdGutterStyle GutterPos { get; set; }

            public bool GutterOnTop { get; set; }

            public float Gutter { get; set; }

            public float HeaderDistance { get; set; }

            public float FooterDistance { get; set; }

            public int OddAndEvenPagesHeaderFooter { get; set; }

            public int DifferentFirstPageHeaderFooter { get; set; }

            public float LeftMargin { get; }

            public float TopMargin { get; }

            public float RightMargin { get; }

            public float BottomMargin { get; }

            public void ApplyTo(PageSetup pageSetup)
            {
                pageSetup.LeftMargin = LeftMargin;
                pageSetup.TopMargin = TopMargin;
                pageSetup.RightMargin = RightMargin;
                pageSetup.BottomMargin = BottomMargin;

                pageSetup.DifferentFirstPageHeaderFooter = DifferentFirstPageHeaderFooter;
                pageSetup.OddAndEvenPagesHeaderFooter = OddAndEvenPagesHeaderFooter;
                pageSetup.FooterDistance = FooterDistance;
                pageSetup.HeaderDistance = HeaderDistance;

                pageSetup.Gutter = Gutter;
                pageSetup.GutterOnTop = GutterOnTop;
                pageSetup.GutterPos = GutterPos;
                pageSetup.GutterStyle = GutterStyle;
                pageSetup.CharsLine = CharsLine;
                pageSetup.MirrorMargins = MirrorMargins;
                pageSetup.Orientation = Orientation;

                pageSetup.PageWidth = PageWidth;
                pageSetup.PageHeight = PageHeight;

                pageSetup.VerticalAlignment = VerticalAlignment;
                pageSetup.SectionStart = SectionStart;
                pageSetup.SuppressEndnotes = SuppressEndnotes;
                //pageSetup.LineNumbering
                //pageSetup.TextColumns
                pageSetup.PaperSize = PaperSize;
                pageSetup.TwoPagesOnOne = TwoPagesOnOne;
                pageSetup.LinesPage = LinesPage;
                pageSetup.SectionDirection = SectionDirection;
                pageSetup.LayoutMode = LayoutMode;
                pageSetup.BookFoldPrinting = BookFoldPrinting;
                pageSetup.BookFoldRevPrinting = BookFoldRevPrinting;
                pageSetup.BookFoldPrintingSheets = BookFoldPrintingSheets;
            }
        }
    }
}
