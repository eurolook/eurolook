﻿using System;
using System.Linq;
using Autofac.Features.Indexed;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.BrickEngine
{
    public interface ICommandBrickResolver
    {
        IBrickCommand Resolve(CommandBrick commandBrick);
    }

    public class CommandBrickResolver : ICommandBrickResolver
    {
        private readonly IIndex<string, IBrickCommand> _brickCommands;

        public CommandBrickResolver(IIndex<string, IBrickCommand> brickCommands)
        {
            _brickCommands = brickCommands;
        }

        [CanBeNull]
        public IBrickCommand Resolve(CommandBrick commandBrick)
        {
            if (string.IsNullOrWhiteSpace(commandBrick.CommandClass))
            {
                throw new ArgumentException("Command class is not set", nameof(commandBrick));
            }

            var typeNameWithoutNamespace = commandBrick.CommandClass.Split('.').Last();

            if (_brickCommands.TryGetValue(typeNameWithoutNamespace, out var brickCommand))
            {
                return brickCommand;
            }

            return null;
        }
    }
}
