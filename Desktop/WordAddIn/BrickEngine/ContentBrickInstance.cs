﻿using System.Collections.Generic;
using System.Linq;
using Eurolook.AddIn.Common;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.Data.TermStore;
using Eurolook.Data.Xml;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.DocumentProcessing.OpenXml;
using Eurolook.WordAddIn.Extensions;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Language = Eurolook.Data.Models.Language;
using Range = Microsoft.Office.Interop.Word.Range;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickEngine
{
    public class ContentBrickInstance : BrickInstanceBase, IBrickInstance, ICanLog
    {
        private readonly ContentBrick _contentBrick;
        private readonly List<DocumentStructure> _documentStructures;
        private readonly ILocalisedResourceResolver _resourceResolver;
        private readonly ITermStoreCache _termStoreCache;
        private readonly IStylesExtractor _stylesExtractor;
        private readonly ISettingsService _settingsService;
        private readonly DocumentModel _documentModel;
        [CanBeNull]
        private readonly DocumentModelLanguage _documentModelLanguage;
        private readonly Language _language;
        private readonly BrickPositioner _positioner;
        private readonly IAuthorManager _authorManager;
        private readonly IContentBrickActionsResolver _contentBrickActionsResolver;

        public ContentBrickInstance(
            ContentBrick brick,
            List<DocumentStructure> documentStructures,
            ILocalisedResourceResolver resourceResolver,
            ISettingsService settingsService,
            ITermStoreCache termStoreCache,
            IStylesExtractor stylesExtractor,
            DocumentModel documentModel,
            DocumentModelLanguage documentModelLanguage,
            Language language,
            IAuthorManager authorManager,
            IContentBrickActionsResolver contentBrickActionsResolver)
        {
            _contentBrick = brick;
            _documentStructures = documentStructures;
            _resourceResolver = resourceResolver;
            _settingsService = settingsService;
            _termStoreCache = termStoreCache;
            _stylesExtractor = stylesExtractor;
            _documentModel = documentModel;
            _documentModelLanguage = documentModelLanguage;
            _language = language;
            _authorManager = authorManager;
            _contentBrickActionsResolver = contentBrickActionsResolver;

            _positioner = new BrickPositioner(_documentStructures);
        }

        public Brick Brick => _contentBrick;

        public bool HasAuthorBinding => _contentBrick.HasAuthorBinding;

        public IEnumerable<string> RequiredStyles => _contentBrick.RequiredStyles;

        /// <summary>
        /// Checks if the brick is already applied to the document and if so selects the brick.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="argument"></param>
        /// <returns>True, if the brick has been selected, false otherwise.</returns>
        public bool TrySelect(IBrickExecutionContext context, string argument = null)
        {
            using (
                var documentAutomationHelper = new DocumentAutomationHelper(
                    context.Application,
                    $"Selecting {Brick.DisplayName}",
                    DocumentAutomationOption.PreventScreenUpdating
                    | DocumentAutomationOption.HandleMultiSelection
                    | DocumentAutomationOption.ShowWaitCursor
                    | DocumentAutomationOption.ScrollIntoView))
            {
                var structures = _documentStructures.Where(e => e.Brick.Id == Brick.Id).ToList();
                if (structures.Count == 0)
                {
                    this.LogWarnFormat("brick {0} ({1}) was not found", Brick.Name, Brick.Id);
                    return false;
                }

                if (!Brick.IsMultiInstance)
                {
                    var brickRanges = _positioner.SearchForBrick(Brick, context.Document).ToList();
                    var firstBrickRange = GetFirstVisibleRangeOrDefault(brickRanges);
                    if (firstBrickRange != null)
                    {
                        // select brick and return
                        SelectPresentBrick(documentAutomationHelper, firstBrickRange);
                        return true;
                    }
                }
            }

            return false;
        }

        public async Task ApplyAsync(IBrickExecutionContext context, string argument = null)
        {
            var structures = _documentStructures.Where(e => e.Brick.Id == Brick.Id).ToList();
            if (structures.Count == 0)
            {
                this.LogWarnFormat("brick {0} ({1}) was not found", Brick.Name, Brick.Id);
                return;
            }

            // assert that header/footer properties are set as required by the bricks to be inserted
            AssertSectionProperties(structures, context);

            using (var documentAutomationHelper = new DocumentAutomationHelper(
                context.Application,
                $"Apply {Brick.DisplayName} Brick"))
            {
                IList<AuthorCustomXmlStoreInfo> groupAuthorCustomXmlStoreInfos = null;

                if (!_settingsService.IsInTestMode && !Brick.IsMultiInstance)
                {
                    // first remove existing brick(s)
                    foreach (
                        var documentStructure in
                        structures.Where(
                            s => s.InsertionBehavior == InsertionBehavior.ReplaceExisting && s.Brick.GroupId != null))
                    {
                        var structure = documentStructure;
                        var bricksInThisGroup =
                            context.Document.GetAllBrickContainers()
                                   .Where(info => info.GroupId == structure.Brick.GroupId).ToList();

                        foreach (var brickContainer in bricksInThisGroup)
                        {
                            if (groupAuthorCustomXmlStoreInfos == null)
                            {
                                groupAuthorCustomXmlStoreInfos = _authorManager
                                                                 .GetAuthorCustomXmlStoreInfos(
                                                                     brickContainer.ContentControl,
                                                                     context.Document).ToList();
                            }

                            // changing the selection makes sure the contents of bound content controls
                            // is written to Custom XML before deleting a brick
                            brickContainer.ContentControl.Range.Document.UpdateCustomXmlBindings();
                            brickContainer.ContentControl.DeleteBrick();
                        }
                    }
                }

                context.AuthorInformation = CreateAuthorInformationForBrick(context, groupAuthorCustomXmlStoreInfos);

                var insertPositions = InsertAndUpdate(structures, context);
                SelectInsertedBrick(documentAutomationHelper, insertPositions, structures);

                await _contentBrickActionsResolver.Resolve(_contentBrick).AfterBrickInsertionAsync(context);
            }
        }

        public void Renew(IBrickExecutionContext context)
        {
            var structures = _documentStructures.Where(e => e.Brick.Id == Brick.Id).ToList();
            if (structures.Count == 0)
            {
                this.LogWarnFormat("Brick {0} ({1}) was not found", Brick.Name, Brick.Id);
                return;
            }

            var positions = new Dictionary<InsertPosition, DocumentStructure>();
            foreach (var brickContainer in context.Document.GetAllBrickContainers().Where(bc => bc.Id == Brick.Id))
            {
                var structure = structures.FirstOrDefault(x => x.BrickId == brickContainer.Id);
                if (structure != null)
                {
                    var insertRange = brickContainer.ContentControl.Range.Duplicate.CollapseToStart();
                    positions.Add(new InsertPosition(insertRange), structure);
                    brickContainer.ContentControl.DeleteBrick();
                }
            }

            var inserter = new BrickInserter(
                context,
                _resourceResolver,
                _termStoreCache,
                _stylesExtractor,
                _documentModel,
                _language);
            foreach (var position in positions.Keys)
            {
                var documentStructure = positions[position];
                inserter.Insert(documentStructure, new[] { position }, context.AuthorInformation);
            }
        }

        private AuthorInformation CreateAuthorInformationForBrick(
            IBrickExecutionContext context,
            IList<AuthorCustomXmlStoreInfo> currentAuthorCustomXmlStoreInfos)
        {
            if (currentAuthorCustomXmlStoreInfos?.Any() != true)
            {
                return context.AuthorInformation;
            }

            AddMissingAuthorRoles(currentAuthorCustomXmlStoreInfos, context);

            return new AuthorInformation(
                _authorManager.SortByMainAuthor(currentAuthorCustomXmlStoreInfos, context.Document),
                context.EurolookProperties);
        }

        /// <summary>
        /// Adds author roles from the document that are not present in the current AuthorCustomXmlStoreInfos.
        /// This is necessary when switching from a brick with a single author binding to a brick with two author bindings.
        /// </summary>
        private void AddMissingAuthorRoles(IList<AuthorCustomXmlStoreInfo> currentAuthorCustomXmlStoreInfos, IBrickExecutionContext context)
        {
            foreach (var storeInfo in context.AuthorInformation.AuthorCustomXmlStoreInfos.Where(
                a => a.AuthorCustomXml.AuthorRoleId.HasValue))
            {
                if (currentAuthorCustomXmlStoreInfos.Any(
                    a =>
                    {
                        var authorRoleId = storeInfo.AuthorCustomXml.AuthorRoleId;
                        return a.AuthorCustomXml.AuthorRoleId == authorRoleId
                               || a.AuthorCustomXml.OriginalAuthorRoleId == authorRoleId;
                    }))
                {
                    continue;
                }

                currentAuthorCustomXmlStoreInfos.Add(storeInfo);
            }
        }

        private static void SelectPresentBrick(
            DocumentAutomationHelper documentAutomationHelper,
            Range brickRange)
        {
            if (brickRange == null)
            {
                return;
            }

            // first try to select the first placeholder
            if (brickRange.SelectFirstPlaceholderContentControl(documentAutomationHelper.PreferredViewType))
            {
                return;
            }

            // otherwise select the whole brick
            brickRange.SelectInPreferredView(documentAutomationHelper.PreferredViewType);
        }

        private List<Range> InsertAndUpdate(List<DocumentStructure> structures, IBrickExecutionContext context)
        {
            // insert the brick
            var insertPositions = structures.SelectMany(s => Insert(s, context)).ToList();

            UpdateFields(insertPositions);

            return insertPositions;
        }

        private void UpdateFields(List<Range> insertPositions)
        {
            foreach (var pos in insertPositions)
            {
                pos.Fields.Update();
                foreach (var section in pos.Sections.OfType<Section>())
                {
                    UpdateHeaderAndFooterFields(section);
                }
            }
        }

        private void UpdateHeaderAndFooterFields(Section section)
        {
            section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.Fields.Update();
            section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].Range.Fields.Update();
            section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.Fields.Update();
            section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.Fields.Update();
            section.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].Range.Fields.Update();
            section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.Fields.Update();
        }

        private void AssertSectionProperties(List<DocumentStructure> structures, IBrickExecutionContext context)
        {
            foreach (var documentStructure in structures)
            {
                var section = context.Document.Sections.Count <= documentStructure.SectionId
                    ? context.Document.Sections[documentStructure.SectionId]
                    : null;
                if (section == null)
                {
                    return;
                }

                var pageSetup = section.PageSetup;
                if (documentStructure.RequiresDifferentFirstPageHeaderFooter && pageSetup.DifferentFirstPageHeaderFooter == 0)
                {
                    pageSetup.DifferentFirstPageHeaderFooter = -1;
                }

                if (documentStructure.RequiresOddAndEvenPagesHeaderFooter && pageSetup.OddAndEvenPagesHeaderFooter == 0)
                {
                    pageSetup.OddAndEvenPagesHeaderFooter = -1;
                }
            }
        }

        private IEnumerable<Range> Insert(DocumentStructure documentStructure, IBrickExecutionContext context)
        {
            var positions = _positioner.FindInsertPositions(documentStructure, context);

            if (documentStructure.Position == PositionType.End)
            {
                var lastSectionHandler = new LastSectionHandler();
                lastSectionHandler.ReInsertSectionBreak(positions, _documentStructures);
            }

            var inserter = new BrickInserter(
                context,
                _resourceResolver,
                _termStoreCache,
                _stylesExtractor,
                _documentModel,
                _language);
            inserter.Insert(documentStructure, positions, context.AuthorInformation);

            if (documentStructure.Position == PositionType.End)
            {
                var lastSectionHandler = new LastSectionHandler();
                lastSectionHandler.RemoveEmptyLastSection(positions);
            }

            // for cursor-positioned bricks return the ranges where the brick has been inserted
            if (documentStructure.Position == PositionType.Cursor)
            {
                return positions.Select(p => p.Range);
            }

            // otherwise search for the first occurrence of the brick
            return _positioner.SearchForBrick(documentStructure.Brick, context.Document);
        }
    }
}
