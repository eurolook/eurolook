﻿using System;
using System.Linq;
using Eurolook.AddIn.Common.Database;
using Eurolook.Common.Extensions;
using Eurolook.Data.Compatibility;
using Eurolook.Data.Constants;
using Eurolook.Data.Database;
using Eurolook.Data.Models;

namespace Eurolook.WordAddIn
{
    public class BrickVisibilityChecker : IBrickVisibilityChecker
    {
        private readonly IBrickVersionCompatibilityTester _brickVersionCompatibilityTester;
        private readonly IUserDataRepository _userDataRepository;
        private readonly IUserGroupRepository _userGroupRepository;
        private User _user;

        public BrickVisibilityChecker(
            IBrickVersionCompatibilityTester brickVersionCompatibilityTester,
            IUserDataRepository userDataRepository,
            IUserGroupRepository userGroupRepository)
        {
            _brickVersionCompatibilityTester = brickVersionCompatibilityTester;
            _userDataRepository = userDataRepository;
            _userGroupRepository = userGroupRepository;
        }

        private User User => _user ??= _userDataRepository.GetUser();

        public bool IsBrickVisible(Brick brick)
        {
            if (brick.IsHidden)
            {
                return false;
            }

            bool isBrickVisibleToUser = IsBrickAccessibleByCurrentUser(brick);
            return isBrickVisibleToUser && _brickVersionCompatibilityTester.IsBrickCompatibleWithAddinVersion(brick);
        }

        public bool IsBrickAccessibleByCurrentUser(Brick brick)
        {
            if (User == null)
            {
                return false;
            }

            bool isBrickVisibleToUser =
                brick.IsVisibleToUserGroup(CommonDataConstants.DefaultUserGroupId) ||
                User.UserGroupIds.Split(";", StringSplitOptions.RemoveEmptyEntries).Any(brick.IsVisibleToUserGroup) ||
                _userGroupRepository.IsMemberOfUserGroupOrMappedAdGroup(User, CommonDataConstants.AdministratorsUserGroupId);

            return isBrickVisibleToUser;
        }
    }
}
