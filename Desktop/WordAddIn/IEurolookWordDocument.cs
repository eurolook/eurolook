﻿using System.Collections.Generic;
using Eurolook.AddIn.Common;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.BrickEngine;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn
{
    public interface IEurolookWordDocument : IEurolookDocument
    {
        bool IsLegacyEurolookDocument { get; }

        bool IsDocumentModelKnown { get; }

        bool IsStandardEurolookDocument { get; }

        Task InsertBrick(Brick brick, bool activateApplication);

        IBrickEngine BrickEngine { get; }

        Document Document { get; }

        List<DocumentStructure> DocumentStructures { get; }
    }
}
