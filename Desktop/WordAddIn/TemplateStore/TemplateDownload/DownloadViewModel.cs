﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.Common.Log;
using Eurolook.Data.Models.TemplateStore;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Eurolook.WordAddIn.TemplateStore.TemplateDownload
{
    public class DownloadViewModel : ViewModelBase, ICanLog
    {
        private readonly ITemplateRepository _templateRepository;
        private readonly ITemplateStoreManager _templateStoreManager;
        private readonly TemplateDownloadRequest _downloadRequest;
        private readonly HttpClient _httpClient;

        private string _statusText;
        private double _downloadProgress;
        private bool _isIndeterminate;
        private SolidColorBrush _progressColor;
        private bool _isRetryButtonVisible;
        private bool _isCancelButtonVisible;

        public string Title
        {
            get => _statusText;
            set => Set(() => StatusText, ref _statusText, value);
        }

        public string WindowTitle => $"Eurolook - {Title}";

        public string StatusText
        {
            get => _statusText;
            set => Set(() => StatusText, ref _statusText, value);
        }

        public double DownloadProgress
        {
            get => _downloadProgress;
            set => Set(() => DownloadProgress, ref _downloadProgress, value);
        }

        public bool IsIndeterminate
        {
            get => _isIndeterminate;
            set => Set(() => IsIndeterminate, ref _isIndeterminate, value);
        }

        public bool IsRetryButtonVisible
        {
            get => _isRetryButtonVisible;
            set => Set(() => IsRetryButtonVisible, ref _isRetryButtonVisible, value);
        }

        public bool IsCancelButtonVisible
        {
            get => _isCancelButtonVisible;
            set => Set(() => IsCancelButtonVisible, ref _isCancelButtonVisible, value);
        }

        public SolidColorBrush ProgressColor
        {
            get => _progressColor;
            set => Set(() => ProgressColor, ref _progressColor, value);
        }

        public RelayCommand CancelCommand { get; set; }

        public RelayCommand RetryCommand { get; set; }

        public Action CloseAction { get; set; }

        public Action FinishAction { get; set; }

        public Action ErrorAction { get; set; }

        public DownloadViewModel(
            ITemplateRepository templateRepository,
            ITemplateStoreManager templateStoreManager,
            TemplateDownloadRequest downloadRequest)
        {
            _templateRepository = templateRepository;
            _templateStoreManager = templateStoreManager;
            _downloadRequest = downloadRequest;
            _httpClient = new HttpClient(new HttpClientHandler { UseDefaultCredentials = true });
            CancelCommand = new RelayCommand(CancelDownload);
            RetryCommand = new RelayCommand(async () => await StartDownload());
            Title = "Template Download";
        }

        public async Task<bool> StartDownload()
        {
            if (_downloadRequest.BeforeDownloadValidation?.Validate() == false)
            {
                Execute.OnUiThread(() => SetErrorState(_downloadRequest.BeforeDownloadValidation.FailureMessage));
                return false;
            }

            SetDownloadingState();
            var template = await DownloadTemplate();
            if (template == null)
            {
                Execute.OnUiThread(
                    () =>
                    {
                        SetErrorState("Download failed.");
                        ErrorAction?.Invoke();
                    });

                return false;
            }

            await _templateRepository.SaveTemplateDownload(template);

            if (_downloadRequest.AfterDownloadValidation?.Validate(template) == false)
            {
                Execute.OnUiThread(() => SetErrorState(_downloadRequest.AfterDownloadValidation.FailureMessage));
                return false;
            }

            Execute.OnUiThread(
                () =>
                {
                    CloseAction?.Invoke();
                    FinishAction?.Invoke();
                });

            return true;
        }

        private async Task<Template> DownloadTemplate()
        {
            try
            {
                this.LogTrace($"Downloading template {_downloadRequest.TemplateId}...");
                var template = await _templateStoreManager.DownloadTemplateFromServer(20, _downloadRequest.TemplateId);
                this.LogTrace("Download completed.");
                return template;
            }
            catch (WebException wex) when (wex.Message.Contains("The request was canceled"))
            {
                this.LogTrace("Download canceled by user.");
                return null;
            }
            catch (Exception ex)
            {
                this.LogError("Download failed.", ex);
                return null;
            }
        }

        private void CancelDownload()
        {
            _httpClient?.CancelPendingRequests();
            CloseAction?.Invoke();
        }

        private void SetDownloadingState()
        {
            IsCancelButtonVisible = true;
            IsRetryButtonVisible = false;
            ProgressColor = Application.Current.Resources["GreenColor"] as SolidColorBrush;
            IsIndeterminate = true;
            StatusText = "Downloading Template...";
        }

        private void SetErrorState(string message)
        {
            IsRetryButtonVisible = true;
            IsCancelButtonVisible = true;
            ProgressColor = Application.Current.Resources["RedColor"] as SolidColorBrush;
            IsIndeterminate = false;
            StatusText = message;
            DownloadProgress = 100.0;
        }
    }
}
