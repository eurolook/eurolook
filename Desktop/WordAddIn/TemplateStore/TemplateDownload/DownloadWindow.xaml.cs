using System.Windows;

namespace Eurolook.WordAddIn.TemplateStore.TemplateDownload
{
    public partial class DownloadWindow : Window
    {
        public DownloadWindow(DownloadViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }
    }
}
