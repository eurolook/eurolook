﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.Common.Log;
using Eurolook.WordAddIn.EurolookLink;
using Eurolook.WordAddIn.Wizard;

namespace Eurolook.WordAddIn.TemplateStore.TemplateDownload
{
    public class TemplateDownloadHandler : ILinkHandler, ICanLog
    {
        private readonly ITemplateDownloader _templateDownloader;
        private readonly IMessageService _messageService;
        private readonly ISettingsService _settingsService;
        private readonly Func<CreateDocumentWizardViewModel> _createDocumentWizardViewModelFunc;
        private readonly ITemplateStoreManager _templateStoreManager;

        public string HandlerName => "TemplateDownload";

        public TemplateDownloadHandler(
            ITemplateDownloader templateDownloader,
            IMessageService messageService,
            ISettingsService settingsService,
            Func<CreateDocumentWizardViewModel> createDocumentWizardViewModelFunc,
            ITemplateStoreManager templateStoreManager)
        {
            _templateDownloader = templateDownloader;
            _messageService = messageService;
            _settingsService = settingsService;
            _createDocumentWizardViewModelFunc = createDocumentWizardViewModelFunc;
            _templateStoreManager = templateStoreManager;
        }

        public async Task Execute(string[] args)
        {
            if (args.Length < 1 || !Guid.TryParse(args[0], out var templateId))
            {
                return;
            }

            bool addToUserProfileUponDocumentCreation = true;

            Uri serverAddress = null;
            if (args.Length >= 2)
            {
                var url = args[1];
                if (url != null && !url.StartsWith("http", StringComparison.InvariantCultureIgnoreCase))
                {
                    url = $"https://{url}";
                }

                Uri.TryCreate(url, UriKind.RelativeOrAbsolute, out serverAddress);
            }

            if (args.Length >= 3)
            {
                addToUserProfileUponDocumentCreation = args[2] != "0";
            }

            var downloadRequest = new TemplateDownloadRequest
            {
                TemplateId = templateId,
                BeforeDownloadValidation = new BeforeDownloadValidation
                {
                    Validate = () => !IsUrlMismatch(serverAddress),
                    FailureMessage = "Server address of link does not match client environment",
                },
                FinishAction = async () =>
                {
                    await ShowCreateDialog(templateId, addToUserProfileUponDocumentCreation);
                },
            };

            try
            {
                await _templateDownloader.DownloadTemplate(downloadRequest);
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private bool IsUrlMismatch(Uri serverAddress)
        {
            if (serverAddress == null)
            {
                return false;
            }

            if (Uri.TryCreate(
                _settingsService.TemplateStoreUrl,
                UriKind.Absolute,
                out var settingsUrl))
            {
                if (settingsUrl.Host == serverAddress.Host && settingsUrl.Port == serverAddress.Port)
                {
                    return false;
                }
            }

            return true;
        }

        [SuppressMessage("SonarQube", "S2259: Null pointers should not be dereferenced", Justification = "Not null")]
        private async Task ShowCreateDialog(Guid templateId, bool addToUserProfileUponDocumentCreation)
        {
            try
            {
                _messageService.ActivateApplicationWindow();

                var wizardViewModel = _createDocumentWizardViewModelFunc();
                var window = wizardViewModel.GetWindow();

                var basicsPageViewModel = wizardViewModel.Pages.FirstOrDefault(x => x is BasicsPageViewModel) as BasicsPageViewModel;
                basicsPageViewModel.TemplateChooserViewModel.OpenCustomTemplateFromLink = new OpenCustomTemplateFromLink
                {
                    TemplateId = templateId,
                    AddToUserProfileUponDocumentCreation = addToUserProfileUponDocumentCreation
                };

                await window.ShowWizard(wizardViewModel, _messageService);
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }
    }
}
