using System.Threading.Tasks;

namespace Eurolook.WordAddIn.TemplateStore.TemplateDownload
{
    public interface ITemplateDownloader
    {
        Task<bool> DownloadTemplate(TemplateDownloadRequest downloadRequest);
    }
}
