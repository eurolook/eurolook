using System;

namespace Eurolook.WordAddIn.TemplateStore.TemplateDownload
{
    public class BeforeDownloadValidation
    {
        public Func<bool> Validate { get; set; }

        public string FailureMessage { get; set; }
    }
}
