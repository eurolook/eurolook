using System;
using Eurolook.Data.Models.TemplateStore;

namespace Eurolook.WordAddIn.TemplateStore.TemplateDownload
{
    public class AfterDownloadValidation
    {
        public Func<Template, bool> Validate { get; set; }

        public string FailureMessage { get; set; }
    }
}
