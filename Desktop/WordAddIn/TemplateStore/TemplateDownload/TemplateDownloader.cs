using System;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;

namespace Eurolook.WordAddIn.TemplateStore.TemplateDownload
{
    public class TemplateDownloader : ITemplateDownloader
    {
        private readonly IMessageService _messageService;
        private readonly Func<TemplateDownloadRequest, DownloadViewModel> _downloadViewModelFunc;

        public TemplateDownloader(
            IMessageService messageService,
            Func<TemplateDownloadRequest, DownloadViewModel> downloadViewModelFunc)
        {
            _messageService = messageService;
            _downloadViewModelFunc = downloadViewModelFunc;
        }

        public async Task<bool> DownloadTemplate(TemplateDownloadRequest downloadRequest)
        {
            var viewModel = _downloadViewModelFunc(downloadRequest);
            var downloadWindow = new DownloadWindow(viewModel);
            viewModel.CloseAction = () =>
                                    {
                                        downloadRequest.CloseAction?.Invoke();
                                        downloadWindow.Close();
                                    };
            viewModel.FinishAction = downloadRequest.FinishAction;
            viewModel.ErrorAction = downloadRequest.ErrorAction;
            downloadWindow.Topmost = true;
            _messageService.ShowWindowActivated(downloadWindow);
            return await viewModel.StartDownload();
        }
    }
}
