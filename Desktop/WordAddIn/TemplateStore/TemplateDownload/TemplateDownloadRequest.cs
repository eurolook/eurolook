using System;

namespace Eurolook.WordAddIn.TemplateStore.TemplateDownload
{
    public class TemplateDownloadRequest
    {
        public Guid TemplateId { get; set; }

        public BeforeDownloadValidation BeforeDownloadValidation { get; set; }

        public AfterDownloadValidation AfterDownloadValidation { get; set; }

        public Action FinishAction { get; set; }

        public Action CloseAction { get; set; }

        public Action ErrorAction { get; set; }
    }
}
