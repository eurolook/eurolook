﻿using System;
using System.Threading.Tasks;
using Eurolook.Data.Models.TemplateStore;
using Eurolook.Data.TemplateStore;

namespace Eurolook.WordAddIn.TemplateStore
{
    public interface ITemplateStoreManager
    {
        void OpenTemplateStore();
        Task<TemplateOwnerInfo[]> GetOwnersOfTemplate(Guid templateId, uint timeoutSeconds);
        Task<UserTemplate[]> GetUserTemplatesFromServer(uint timeoutSeconds, uint modifiedSinceHours);
        Task DeleteUserTemplateOnServer(uint timeoutSeconds, Guid templateId);
        Task SaveAsUserTemplateOnServer(uint timeoutSeconds, Guid templateId);
        Task<Template> DownloadTemplateFromServer(uint timeoutSeconds, Guid templateId);
    }
}
