﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.UserConfiguration;
using Eurolook.Common.Log;
using Eurolook.Data.Models.TemplateStore;
using Eurolook.Data.TemplateStore;
using Eurolook.DataSync.HttpClient;

namespace Eurolook.WordAddIn.TemplateStore
{
    public class TemplateStoreManager : ITemplateStoreManager, ICanLog
    {
        private readonly ISettingsService _settingsService;
        private readonly IUserIdentity _userIdentity;

        public TemplateStoreManager(ISettingsService settingsService, IUserIdentity userIdentity)
        {
            _settingsService = settingsService;
            _userIdentity = userIdentity;
        }

        public void OpenTemplateStore()
        {
            try
            {
                string impersonationParameter = _userIdentity.IsImpersonated
                    ? $"?impersonate={Uri.EscapeDataString(_userIdentity.GetUserName())}"
                    : "";

                string url = $"{_settingsService.TemplateStoreUrl}/IntegratedMode{impersonationParameter}";

                if (!url.StartsWith("http://") && !url.StartsWith("https://"))
                {
                    throw new Exception("Wrong template store configuration. Abort due to security.");
                }

                Process.Start(url);
            }
            catch (Exception ex)
            {
                this.LogError("Failed executing OpenTemplateStore.\n{0}", ex);
            }
        }

        public async Task<TemplateOwnerInfo[]> GetOwnersOfTemplate(Guid templateId, uint timeoutSeconds)
        {
            try
            {
                var templateStoreSyncClient = new TemplateStoreSyncClient(_settingsService.DataSyncUrl, timeoutSeconds);
                var login = await templateStoreSyncClient.Login();
                if (!login.IsSuccess)
                {
                    throw login.Error;
                }

                return await templateStoreSyncClient.GetOwners(templateId, CancellationToken.None) ?? Array.Empty<TemplateOwnerInfo>();
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                return Array.Empty<TemplateOwnerInfo>();
            }
        }

        public async Task<UserTemplate[]> GetUserTemplatesFromServer(uint timeoutSeconds, uint modifiedSinceHours)
        {
            try
            {
                var templateStoreSyncClient = new TemplateStoreSyncClient(_settingsService.DataSyncUrl, timeoutSeconds);
                var login = await templateStoreSyncClient.Login();
                if (!login.IsSuccess)
                {
                    throw login.Error;
                }

                return await templateStoreSyncClient.GetUserTemplates(_userIdentity.GetUserName(), modifiedSinceHours, CancellationToken.None) ?? Array.Empty<UserTemplate>();
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                return Array.Empty<UserTemplate>();
            }
        }

        public async Task SaveAsUserTemplateOnServer(uint timeoutSeconds, Guid templateId)
        {
            try
            {
                var templateStoreSyncClient = new TemplateStoreSyncClient(_settingsService.DataSyncUrl, timeoutSeconds);
                var login = await templateStoreSyncClient.Login();
                if (!login.IsSuccess)
                {
                    throw login.Error;
                }

                await templateStoreSyncClient.SaveAsUserTemplate(_userIdentity.GetUserName(), templateId);
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        public async Task DeleteUserTemplateOnServer(uint timeoutSeconds, Guid templateId)
        {
            try
            {
                var templateStoreSyncClient = new TemplateStoreSyncClient(_settingsService.DataSyncUrl, timeoutSeconds);
                var login = await templateStoreSyncClient.Login();
                if (!login.IsSuccess)
                {
                    throw login.Error;
                }

                await templateStoreSyncClient.DeleteFromUserTemplates(_userIdentity.GetUserName(), templateId);
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        public async Task<Template> DownloadTemplateFromServer(uint timeoutSeconds, Guid templateId)
        {
            try
            {
                var templateStoreSyncClient = new TemplateStoreSyncClient(_settingsService.DataSyncUrl, timeoutSeconds);
                var login = await templateStoreSyncClient.Login();
                if (!login.IsSuccess)
                {
                    throw login.Error;
                }

                return await templateStoreSyncClient.DownloadTemplate(_userIdentity.GetUserName(), templateId, CancellationToken.None)
                    ?? throw new Exception("Template download returned null.");
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                throw;
            }
        }
    }
}
