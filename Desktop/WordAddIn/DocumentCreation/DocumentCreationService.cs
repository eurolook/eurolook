﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing.DocumentCreation;

namespace Eurolook.WordAddIn.DocumentCreation
{
    public class DocumentCreationService : IDocumentCreationService, ICanLog
    {
        private readonly IDocumentCreator _documentCreator;
        private readonly ISessionContext _sessionContext;

        public DocumentCreationService(
            IDocumentCreator documentCreator,
            ISessionContext sessionContext)
        {
            _documentCreator = documentCreator;
            _sessionContext = sessionContext;
        }

        public async Task<DocumentCreationResult> CreateDocumentAsync(
            DocumentCreationInfo documentCreationInfo,
            IEnumerable<IBeforeDocumentCreationTask> beforeDocumentCreationTasks,
            IEnumerable<IAfterDocumentCreationTask> afterDocumentCreationTasks,
            IProgressReporter<int> progressReporter)
        {
            try
            {
                int progress = 20;

                await progressReporter.ReportProgressAsync(progress);
                foreach (var beforeDocumentCreationTask in beforeDocumentCreationTasks.OrderBy(t => t.ExecutionOrder))
                {
                    string taskName = beforeDocumentCreationTask.GetType().Name;
                    this.LogInfo($"Executing {taskName}");
                    await beforeDocumentCreationTask.BeforeDocumentCreation(documentCreationInfo);
                    this.LogInfo($"Finished Executing {taskName}");
                    progress += 20 / beforeDocumentCreationTasks.Count();
                    await progressReporter.ReportProgressAsync(progress);
                }

                progress = 40;
                await progressReporter.ReportProgressAsync(progress);
                var documentCreationResult = await _documentCreator.CreateAndOpenDocumentAsync(documentCreationInfo);
                progress = 60;
                await progressReporter.ReportProgressAsync(progress);

                foreach (var afterDocumentCreationTask in afterDocumentCreationTasks.OrderBy(t => t.ExecutionOrder))
                {
                    string taskName = afterDocumentCreationTask.GetType().Name;
                    this.LogInfo($"Executing {taskName}");
                    documentCreationResult =
                        await afterDocumentCreationTask.AfterDocumentCreation(documentCreationResult);
                    this.LogInfo($"Finished Executing {taskName}");
                    progress += 20 / afterDocumentCreationTasks.Count();
                    await progressReporter.ReportProgressAsync(progress);
                }

                progress = 80;
                await progressReporter.ReportProgressAsync(progress);

                await _sessionContext.SetSessionActive();

                return documentCreationResult;
            }
            catch (TemplateOrLanguageNoLongerAvailableException ex)
            {
                this.LogError(ex);
                throw;
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                return null;
            }
        }
    }
}
