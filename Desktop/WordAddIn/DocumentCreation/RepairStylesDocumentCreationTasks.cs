﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.DocumentProcessing.DocumentCreation;
using Eurolook.WordAddIn.BrickCommands.RepairStyles;
using Eurolook.WordAddIn.BrickCommands.RepairStyles.StyleRepairTasks;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.DocumentCreation
{
    public class RepairStylesDocumentCreationTasks : IAfterConvertToEurolookDocumentCreationTask
    {
        private readonly Lazy<IStyleRepairer> _styleRepairer;
        private readonly IEnumerable<IStyleRepairTask> _styleRepairTasks;
        private readonly IDocumentManager _documentManager;

        public RepairStylesDocumentCreationTasks(
            Lazy<IStyleRepairer> styleRepairer,
            IEnumerable<IStyleRepairTask> styleRepairTasks,
            IDocumentManager documentManager)
        {
            _styleRepairer = styleRepairer;
            _styleRepairTasks = styleRepairTasks;
            _documentManager = documentManager;
        }

        public float ExecutionOrder => 13;

        public async Task<DocumentCreationResult> AfterDocumentCreation(DocumentCreationResult documentCreationResult)
        {
            var createdDocument = documentCreationResult.Document;
            Document repairedDocument = null;
            Document newDocument;
            using (new DocumentAutomationHelper(createdDocument, "Convert to Eurolook"))
            {
                // Remove legacy controls as these cause IPersistFile.Save calls to fail
                var controlsToDelete = createdDocument
                                       .Fields.OfType<Field>().Where(f => f.Type == WdFieldType.wdFieldOCX).ToList();
                foreach (var field in controlsToDelete)
                {
                    field.Delete();
                }

                // Repair Styles
                if (createdDocument.SaveFormat == (int)WdSaveFormat.wdFormatXMLDocument)
                {
                    var documentModel = _documentManager.GetActiveDocumentViewModel();
                    repairedDocument = await _styleRepairer.Value.RepairStylesAsync(documentModel, _styleRepairTasks);
                }
            }

            if (repairedDocument != null)
            {
                createdDocument.Close(false);
                newDocument = repairedDocument;
            }
            else
            {
                newDocument = createdDocument;
            }

            newDocument.Saved = false;
            newDocument.Activate();

            documentCreationResult.Document = newDocument;
            return documentCreationResult;
        }
    }
}
