﻿using System.Threading.Tasks;
using Eurolook.DocumentProcessing.DocumentCreation;

namespace Eurolook.WordAddIn.DocumentCreation
{
    public interface IBeforeDocumentCreationTask
    {
        float ExecutionOrder { get; }

        Task BeforeDocumentCreation(DocumentCreationInfo documentCreationInfo);
    }

    public interface IAfterDocumentCreationTask
    {
        float ExecutionOrder { get; }

        Task<DocumentCreationResult> AfterDocumentCreation(DocumentCreationResult documentCreationResult);
    }

    public interface IBeforeNewDocumentCreationTask : IBeforeDocumentCreationTask
    {
    }

    public interface IAfterNewDocumentCreationTask : IAfterDocumentCreationTask
    {
    }

    public interface IBeforeConvertToEurolookDocumentCreationTask : IBeforeDocumentCreationTask
    {
    }

    public interface IAfterConvertToEurolookDocumentCreationTask : IAfterDocumentCreationTask
    {
    }

    public interface IBeforeNewDocumentFromTemplateStoreCreationTask : IBeforeDocumentCreationTask
    {
    }

    public interface IAfterNewDocumentFromTemplateStoreCreationTask : IAfterDocumentCreationTask
    {
    }
}
