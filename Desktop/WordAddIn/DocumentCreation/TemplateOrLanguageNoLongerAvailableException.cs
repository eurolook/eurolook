using System;
using System.Runtime.Serialization;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.DocumentCreation
{
    public class TemplateOrLanguageNoLongerAvailableException : Exception
    {
        public TemplateOrLanguageNoLongerAvailableException()
        {
        }

        public TemplateOrLanguageNoLongerAvailableException(string message)
            : base(message)
        {
        }

        public TemplateOrLanguageNoLongerAvailableException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected TemplateOrLanguageNoLongerAvailableException([NotNull] SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
