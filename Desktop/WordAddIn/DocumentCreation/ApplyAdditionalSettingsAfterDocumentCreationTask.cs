﻿using System.Threading.Tasks;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.DocumentCreation;

namespace Eurolook.WordAddIn.DocumentCreation
{
    public class ApplyAdditionalSettingsAfterDocumentCreationTask : IAfterNewDocumentCreationTask
    {
        private readonly IDocumentManager _documentManager;

        public ApplyAdditionalSettingsAfterDocumentCreationTask(IDocumentManager documentManager)
        {
            _documentManager = documentManager;
        }

        public float ExecutionOrder => 15;

        public Task<DocumentCreationResult> AfterDocumentCreation(DocumentCreationResult documentCreationResult)
        {
            var creationInfo = documentCreationResult.DocumentCreationInfo;
            if (creationInfo.HasAdditionalSettingsToApply)
            {
                bool screenUpdating = documentCreationResult.Document.Application.ScreenUpdating;
                try
                {
                    var documentViewModel = _documentManager.GetOrCreateDocumentViewModel(documentCreationResult.Document);
                    if (creationInfo.DefaultTableStyle != string.Empty)
                    {
                        var style = LanguageIndependentStyle.FromNeutralStyleName(creationInfo.DefaultTableStyle, documentViewModel.Document);
                        documentCreationResult.Document.SetDefaultTableStyle(style?.Style, false);
                    }
                }
                finally
                {
                    documentCreationResult.Document.Application.ScreenUpdating = screenUpdating;
                }
            }

            return Task.FromResult(documentCreationResult);
        }
    }
}
