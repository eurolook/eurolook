using System.Linq;
using Eurolook.DocumentProcessing.DocumentCreation;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.DocumentCreation
{
    public class CloseEmptyWordDocumentNewDocumentCreationTask :
        IBeforeNewDocumentCreationTask,
        IBeforeNewDocumentFromTemplateStoreCreationTask
    {
        private readonly IWordApplicationContext _applicationContext;

        public CloseEmptyWordDocumentNewDocumentCreationTask(IWordApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public float ExecutionOrder => 2;

        public Task BeforeDocumentCreation(DocumentCreationInfo documentCreationInfo)
        {
            CloseEmptyWordStartupDocument();
            return Task.CompletedTask;
        }

        private void CloseEmptyWordStartupDocument()
        {
            if (_applicationContext.Application.Documents.Count != 1)
            {
                return;
            }

            var newDoc = _applicationContext.Application.Documents.OfType<Document>().FirstOrDefault(
                d => string.IsNullOrWhiteSpace(d.Path)
                     && d.Saved
                     && string.IsNullOrWhiteSpace(d.Range().Text));

            newDoc?.Close(true);
        }
    }
}
