﻿using System.Threading.Tasks;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing.DocumentCreation;
using Eurolook.WordAddIn.TemplateStore;

namespace Eurolook.WordAddIn.DocumentCreation
{
    public class AddAsUserTemplateBeforeDocumentCreationTask : IBeforeNewDocumentFromTemplateStoreCreationTask, ICanLog
    {
        private readonly ITemplateStoreManager _templateStoreManager;

        public AddAsUserTemplateBeforeDocumentCreationTask(
            ITemplateStoreManager templateStoreManager)
        {
            _templateStoreManager = templateStoreManager;
        }

        public float ExecutionOrder => 1;

        public Task BeforeDocumentCreation(DocumentCreationInfo documentCreationInfo)
        {
            if (documentCreationInfo.AddToUserProfileUponDocumentCreation)
            {
                _ = Task.Run(() => _templateStoreManager.SaveAsUserTemplateOnServer(3, documentCreationInfo.TemplateStoreTemplate.Id).FireAndForgetSafeAsync(this.LogError));
            }

            return Task.CompletedTask;
        }
    }
}
