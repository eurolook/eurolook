using System;
using System.Threading.Tasks;
using Eurolook.DocumentProcessing.DocumentCreation;
using Eurolook.WordAddIn.Authors;
using Eurolook.WordAddIn.BrickEngine;

namespace Eurolook.WordAddIn.DocumentCreation
{
    public class ChangeAuthorsDocumentCreationTask : IAfterNewDocumentFromTemplateStoreCreationTask
    {
        private readonly IDocumentManager _documentManager;
        private readonly Func<IEurolookWordDocument, IBrickExecutionContext> _brickExecutionContextFunc;
        private readonly IAuthorChangeExecutorForCurrentDocuments _authorChangeExecutor;

        public ChangeAuthorsDocumentCreationTask(
            IDocumentManager documentManager,
            Func<IEurolookWordDocument, IBrickExecutionContext> brickExecutionContextFunc,
            IAuthorChangeExecutorForCurrentDocuments authorChangeExecutor)
        {
            _documentManager = documentManager;
            _brickExecutionContextFunc = brickExecutionContextFunc;
            _authorChangeExecutor = authorChangeExecutor;
        }

        public float ExecutionOrder { get; set; }

        public Task<DocumentCreationResult> AfterDocumentCreation(DocumentCreationResult documentCreationResult)
        {
            bool screenUpdating = documentCreationResult.Document.Application.ScreenUpdating;
            try
            {
                documentCreationResult.Document.Application.ScreenUpdating = false;
                var documentViewModel = _documentManager.GetOrCreateDocumentViewModel(documentCreationResult.Document);
                var context = _brickExecutionContextFunc(documentViewModel);
                _authorChangeExecutor.ChangeDocumentAuthor(
                    context,
                    documentCreationResult.DocumentCreationInfo.AuthorRolesMappingList,
                    !documentCreationResult.DocumentCreationInfo.IsSignerFixed);
            }
            finally
            {
                documentCreationResult.Document.Application.ScreenUpdating = screenUpdating;
            }

            return Task.FromResult(documentCreationResult);
        }
    }
}
