using System.Threading.Tasks;
using Eurolook.AddIn.Common.Database;
using Eurolook.Data.ActionLogs;
using Eurolook.DocumentProcessing.DocumentCreation;

namespace Eurolook.WordAddIn.DocumentCreation
{
    public class AfterNewDocumentCreationTask :
        IAfterNewDocumentCreationTask
    {
        private readonly IStatisticsRepository _statisticsRepository;

        public AfterNewDocumentCreationTask(
            IStatisticsRepository statisticsRepository)
        {
            _statisticsRepository = statisticsRepository;
        }

        public float ExecutionOrder => 15;

        public async Task<DocumentCreationResult> AfterDocumentCreation(DocumentCreationResult documentCreationResult)
        {
            await _statisticsRepository.LogDocumentCreationAsync(
                documentCreationResult.DocumentCreationInfo.DocumentModel,
                new DocumentCreationLog
                {
                    Language = documentCreationResult.DocumentCreationInfo.Language.Locale,
                });

            return documentCreationResult;
        }
    }
}
