﻿using System.Threading.Tasks;
using Eurolook.DocumentProcessing.DocumentCreation;
using Eurolook.WordAddIn.BrickEngine;

namespace Eurolook.WordAddIn.DocumentCreation
{
    public class CopyCorePropertiesDocumentCreationTasks : IAfterConvertToEurolookDocumentCreationTask
    {
        private readonly IBrickExecutionContext _context;

        public CopyCorePropertiesDocumentCreationTasks(IBrickExecutionContext context)
        {
            _context = context;
        }

        public float ExecutionOrder => 12;

        public Task<DocumentCreationResult> AfterDocumentCreation(DocumentCreationResult documentCreationResult)
        {
            // access to BuiltInDocumentProperties modifies the Saved state of the document.
            bool isSaved = _context.Document.Saved;

            dynamic sourceBuiltInDocumentProperties = _context.Document.BuiltInDocumentProperties;
            dynamic targetBuiltInDocumentProperties = documentCreationResult.Document.BuiltInDocumentProperties;

            var title = sourceBuiltInDocumentProperties["Title"].Value;
            targetBuiltInDocumentProperties["Title"].Value = title;

            var subject = sourceBuiltInDocumentProperties["Subject"].Value;
            targetBuiltInDocumentProperties["Subject"].Value = subject;

            _context.Document.Saved = isSaved;

            return Task.FromResult(documentCreationResult);
        }
    }
}
