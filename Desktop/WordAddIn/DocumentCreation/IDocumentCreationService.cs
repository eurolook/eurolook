using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.DocumentProcessing.DocumentCreation;

namespace Eurolook.WordAddIn.DocumentCreation
{
    public interface IDocumentCreationService
    {
        Task<DocumentCreationResult> CreateDocumentAsync(
            DocumentCreationInfo documentCreationInfo,
            IEnumerable<IBeforeDocumentCreationTask> beforeDocumentCreationTasks,
            IEnumerable<IAfterDocumentCreationTask> afterDocumentCreationTasks,
            IProgressReporter<int> progressReporter);
    }
}
