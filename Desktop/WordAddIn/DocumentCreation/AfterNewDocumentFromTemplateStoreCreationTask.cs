﻿using System.Threading.Tasks;
using Eurolook.AddIn.Common.Database;
using Eurolook.Data.ActionLogs;
using Eurolook.DocumentProcessing.DocumentCreation;

namespace Eurolook.WordAddIn.DocumentCreation
{
    public class AfterNewDocumentFromTemplateStoreCreationTask :
        IAfterNewDocumentFromTemplateStoreCreationTask
    {
        private readonly IDocumentManager _documentManager;
        private readonly IStatisticsRepository _statisticsRepository;

        public AfterNewDocumentFromTemplateStoreCreationTask(
            IDocumentManager documentManager,
            IStatisticsRepository statisticsRepository)
        {
            _documentManager = documentManager;
            _statisticsRepository = statisticsRepository;
        }

        public float ExecutionOrder => 15;

        public async Task<DocumentCreationResult> AfterDocumentCreation(DocumentCreationResult documentCreationResult)
        {
            var documentViewModel = _documentManager.GetOrCreateDocumentViewModel(documentCreationResult.Document);
            documentViewModel.SetEurolookProperties(
                p =>
                {
                    p.CustomTemplateId = documentCreationResult.DocumentCreationInfo.TemplateStoreTemplate.Id;
                    p.CustomTemplateName = documentCreationResult.DocumentCreationInfo.TemplateStoreTemplate.Name;
                });

            await _statisticsRepository.LogDocumentFromTemplateStoreCreationAsync(
                documentCreationResult.DocumentCreationInfo.DocumentModel,
                new TemplateStoreDocumentCreationLog
                {
                    Language = documentCreationResult.DocumentCreationInfo.Language.Locale,
                    TemplateId = documentCreationResult.DocumentCreationInfo.TemplateStoreTemplate.Id,
                });

            return documentCreationResult;
        }
    }
}
