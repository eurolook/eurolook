﻿using System.Linq;
using System.Threading.Tasks;
using Eurolook.Data.Constants;
using Eurolook.DocumentProcessing.DocumentCreation;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.DocumentCreation
{
    public class CopyContentDocumentCreationTasks : IAfterConvertToEurolookDocumentCreationTask
    {
        private readonly IBrickExecutionContext _context;

        public CopyContentDocumentCreationTasks(IBrickExecutionContext context)
        {
            _context = context;
        }

        public float ExecutionOrder => 10;

        public Task<DocumentCreationResult> AfterDocumentCreation(DocumentCreationResult documentCreationResult)
        {
            // InsertXML copies the content and new styles to the document, existing styles are not replaced.
            var sourceRange = GetSourceRange(_context.Document);
            var targetRange = GetTargetRange(_context.Application.ActiveDocument);
            targetRange.InsertXML(sourceRange.XML);
            return Task.FromResult(documentCreationResult);
        }

        private static Range GetSourceRange(Document contextDocument)
        {
            if (contextDocument.Tables.Count > 0 && IsLetterheadTable(contextDocument.Tables[1]))
            {
                var tableEnd = contextDocument.Tables[1].Range.End;
                var range = contextDocument.Content;
                range.SetRange(tableEnd, contextDocument.Content.End);
                return range;
            }

            return contextDocument.Content;
        }

        private static Range GetTargetRange(Document activeDocument)
        {
            Range targetRange = null;
            var bodyPlaceholder = activeDocument.GetAllBrickContainers()
                                                .FirstOrDefault(x => x.GroupId == CommonDataConstants.BodyGroupId);
            if (bodyPlaceholder != null)
            {
                targetRange = bodyPlaceholder.ContentControl.Range;

                // Check if the paragraph after the body placeholder is a table
                if (targetRange.NextParagraphRange().IsAtBeginningOfTable())
                {
                    // Insert an extra paragraph to create a distance to that table,
                    // then insert the XML content before that paragraph.
                    // You will have an additional paragraph in the document, but no insertion problems.
                    // (workaround for EUROLOOK-2820)
                    targetRange.InsertParagraphAfter();
                    targetRange.Collapse(WdCollapseDirection.wdCollapseStart);
                }
                else
                {
                    // No table, just "select" the body placeholder.
                    targetRange.Expand(WdUnits.wdParagraph);
                }
            }

            if (targetRange == null)
            {
                targetRange = activeDocument.Content;
                targetRange.Collapse(WdCollapseDirection.wdCollapseEnd);

                if (targetRange.IsEndOfStory())
                {
                    // Insert the content into a newly created empty paragraph formatted with the Normal style,
                    // otherwise Word uses the style of the last paragraph in the Word document to format the first
                    // paragraph of the newly inserted content.
                    targetRange = targetRange.InsertParagraphAfterAndHandleLockedContentControls();
                    targetRange.set_Style(WdBuiltinStyle.wdStyleNormal);
                }
            }

            return targetRange;
        }

        private static bool IsLetterheadTable(Table table)
        {
            foreach (Paragraph paragraph in table.Range.Paragraphs)
            {
                string styleName = paragraph.GetStyleNameLocal();
                if (styleName == "Z_Com" || styleName == "Z_Flag")
                {
                    return true;
                }
            }

            return false;
        }
    }
}
