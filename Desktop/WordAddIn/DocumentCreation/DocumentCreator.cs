﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Authors;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.PerformanceLog;
using Eurolook.Common;
using Eurolook.Common.Log;
using Eurolook.Data;
using Eurolook.DocumentProcessing.DocumentCreation;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.DocumentProcessing.OpenXml;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.Extensions;
using Microsoft.Office.Interop.Word;
using Language = Eurolook.Data.Models.Language;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.DocumentCreation
{
    public class DocumentCreator : IDocumentCreator, ICanLog
    {
        private readonly IWordAddinContext _addinContext;
        private readonly IDocumentManager _documentManager;
        private readonly IWordApplicationContext _applicationContext;
        private readonly IEnumerable<IDocumentCreationHandler> _documentCreationHandlers;
        private readonly IConfigurationInfo _configurationInfo;
        private readonly IEnumerable<IDocumentFactory> _documentFactories;
        private readonly ITextsRepository _textsRepository;
        private readonly IAuthorDataLoader _authorDataLoader;
        private readonly Func<Language, ILocalisedResourceResolver> _localisedResourceResolverFunc;
        private readonly IPerformanceLogService _performanceLogService;

        public DocumentCreator(
            ITextsRepository textsRepository,
            IDocumentManager documentManager,
            IWordApplicationContext applicationContext,
            IEnumerable<IDocumentCreationHandler> documentCreationHandlers,
            IWordAddinContext addinContext,
            IConfigurationInfo configurationInfo,
            IEnumerable<IDocumentFactory> documentFactories,
            IAuthorDataLoader authorDataLoader,
            Func<Language, ILocalisedResourceResolver> localisedResourceResolverFunc,
            IPerformanceLogService performanceLogService)
        {
            _textsRepository = textsRepository;
            _documentManager = documentManager;
            _applicationContext = applicationContext;
            _documentCreationHandlers = documentCreationHandlers;
            _addinContext = addinContext;
            _configurationInfo = configurationInfo;
            _documentFactories = documentFactories;
            _authorDataLoader = authorDataLoader;
            _localisedResourceResolverFunc = localisedResourceResolverFunc;
            _performanceLogService = performanceLogService;
        }

        public async Task<DocumentCreationResult> CreateAndOpenDocumentAsync(DocumentCreationInfo documentCreationInfo)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            CompleteDocumentCreationInfo(documentCreationInfo);

            var documentFactory = _documentFactories.First(d => d.CanHandle(documentCreationInfo));

            using (var tempFile = new TemporaryFile(documentFactory.FileExtension))
            {
                string defaultSaveFormat = _applicationContext.Application.DefaultSaveFormat;
                bool screenUpdating = _applicationContext.Application.ScreenUpdating;
                var displayAlerts = _applicationContext.Application.DisplayAlerts;
                try
                {
                    _applicationContext.Application.ScreenUpdating = false;
                    _applicationContext.Application.DisplayAlerts = WdAlertLevel.wdAlertsNone;

                    await documentFactory.CreateDocument(tempFile.FullName, documentCreationInfo);

                    // FIX: default save format must be OpenXml, otherwise content controls are removed
                    // when the document is opened, see #EUROLOOK-383
                    _applicationContext.Application.DefaultSaveFormat = "Docx";

                    Document document;

                    // NOTE: Open the created document as a new document in Word
                    try
                    {
                        document = _applicationContext.Application.Documents.Add(tempFile.FullName, false, 0);
                    }
                    catch (COMException ex)
                    {
                        LogManager.GetLogger().Error("Failed to open {0}", documentCreationInfo.DocumentModel.DisplayName);
                        LogManager.GetLogger().Error(ex.Message, ex);
                        var errors = OpenXmlUtils.ValidateDocument(tempFile.FullName);
                        var message = new StringBuilder();
                        foreach (var error in errors)
                        {
                            message.AppendLine(error.ToDisplayString());
                        }

                        LogManager.GetLogger().Error(message.ToString());
                        throw;
                    }

                    await ExecuteDocumentCreationHandlers(document);

                    // attach Eurolook.dotm (otherwise cleaning up the generated template dotx fails)
                    document.AttachTemplate(documentCreationInfo.TemplatePath);

                    // TODO: Field update does not seem to work, the field collection is empty
                    document.ApplyToAllStoryRanges(r => r.Fields.Update());

                    // select first editable CC
                    var navigator = new ContentControlNavigator(document);
                    navigator.SelectNextContentControl();

                    // NOTE: Setting the author property is a workaround to make that a newly created document is not flagged as
                    //   modified. By setting the author here we make sure that Word does not update the author *after* we clear
                    //   the undo history and set document.Saved = true. Without this workaround we might get an entry
                    //   "Property Update" in the undo history because the author has been modified.
#pragma warning disable S1656 // Variables should not be self-assigned
                    ((dynamic)document.BuiltInDocumentProperties)["Author"].Value = ((dynamic)document.BuiltInDocumentProperties)["Author"].Value;
#pragma warning restore S1656 // Variables should not be self-assigned

                    document.UndoClear();
                    document.Saved = true;

                    stopwatch.Stop();
                    await _performanceLogService.LogDocumentCreatedAsync(stopwatch.ElapsedMilliseconds);

                    return new DocumentCreationResult
                    {
                        DocumentCreationInfo = documentCreationInfo,
                        Document = document,
                    };
                }
                finally
                {
                    _applicationContext.Application.DefaultSaveFormat = defaultSaveFormat;
                    _applicationContext.Application.ScreenUpdating = screenUpdating;
                    _applicationContext.Application.DisplayAlerts = displayAlerts;
                }
            }
        }

        private void CompleteDocumentCreationInfo(DocumentCreationInfo documentCreationInfo)
        {
            var language = documentCreationInfo.Language;
            foreach (var authorRoleMapping in documentCreationInfo.AuthorRolesMappingList)
            {
                _authorDataLoader.LoadCompleteAuthorData(authorRoleMapping, language);
            }

            documentCreationInfo.Translations = _textsRepository.GetTranslations(documentCreationInfo.DocumentModel, language);
            documentCreationInfo.ResourceResolver = _localisedResourceResolverFunc(language);
            documentCreationInfo.ClientVersion = _configurationInfo.ClientVersion;
            documentCreationInfo.ProductCustomizationId = _configurationInfo.ProductCustomizationId;
            documentCreationInfo.TemplatePath = _addinContext.GlobalTemplatePath;
            documentCreationInfo.TimestampUtc = DateTime.UtcNow;
        }

        private async Task ExecuteDocumentCreationHandlers(Document document)
        {
            var documentViewModel = _documentManager.GetOrCreateDocumentViewModel(document);

            foreach (var handler in _documentCreationHandlers.OrderBy(h => h.ExecutionOrder))
            {
                try
                {
                    await handler.HandleAsync(documentViewModel);
                }
                catch (Exception ex)
                {
                    this.LogError(ex);
                }
            }
        }
    }
}
