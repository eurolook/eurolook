using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.Database;
using Eurolook.DocumentProcessing.DocumentCreation;
using Eurolook.WordAddIn.TemplateStore.TemplateDownload;

namespace Eurolook.WordAddIn.DocumentCreation
{
    public class TemplateStoreDocumentFactory : IDocumentFactory
    {
        private readonly ITemplateRepository _templateRepository;
        private readonly ITemplateDownloader _templateDownloader;

        public TemplateStoreDocumentFactory(
            ITemplateRepository templateRepository,
            ITemplateDownloader templateDownloader)
        {
            _templateRepository = templateRepository;
            _templateDownloader = templateDownloader;
        }

        public async Task CreateDocument(string file, DocumentCreationInfo documentCreationInfo)
        {
            if (!documentCreationInfo.IsTemplateAvailableOffline)
            {
                var downloadRequest = new TemplateDownloadRequest
                {
                    TemplateId = documentCreationInfo.TemplateStoreTemplate.Id,
                    AfterDownloadValidation = new AfterDownloadValidation
                    {
                        Validate = (t) => !t.Deleted && t.TemplateFiles.Any(
                                       f => f.LanguageId == documentCreationInfo.Language.Id && !f.Deleted),
                        FailureMessage = "Selected template or language does no longer exist",
                    },
                };

                bool downloadResult = await _templateDownloader.DownloadTemplate(downloadRequest);
                if (!downloadResult)
                {
                    throw new TemplateOrLanguageNoLongerAvailableException();
                }
            }

            WriteTemplateFromDatabaseToFile(file, documentCreationInfo);
        }

        public bool CanHandle(DocumentCreationInfo documentCreationInfo)
        {
            return documentCreationInfo.IsTemplateFromTemplateStore;
        }

        public string FileExtension => ".docx";

        private void WriteTemplateFromDatabaseToFile(string filePath, DocumentCreationInfo documentCreationInfo)
        {
            var templateFile = _templateRepository.GetTemplateFile(
                documentCreationInfo.TemplateStoreTemplate.Id,
                documentCreationInfo.Language);

            var templateFileBytes = templateFile?.Bytes;

            if (templateFileBytes == null)
            {
                throw new TemplateOrLanguageNoLongerAvailableException();
            }

            using (var stream = new FileStream(filePath, FileMode.Create, FileAccess.Write))
            {
                stream.Write(templateFileBytes, 0, templateFileBytes.Length);
            }
        }
    }
}
