using System.Threading.Tasks;
using Eurolook.AddIn.Common.Database;
using Eurolook.Data.ActionLogs;
using Eurolook.DocumentProcessing.DocumentCreation;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.Database;

namespace Eurolook.WordAddIn.DocumentCreation
{
    public class ConvertToEurolookDocumentCreationTasks : IAfterConvertToEurolookDocumentCreationTask
    {
        private readonly IDocumentManager _documentManager;
        private readonly IStatisticsRepository _statisticsRepository;
        private readonly IWordUserDataRepository _userDataRepository;

        public ConvertToEurolookDocumentCreationTasks(
            IDocumentManager documentManager,
            IStatisticsRepository statisticsRepository,
            IWordUserDataRepository userDataRepository)
        {
            _documentManager = documentManager;
            _statisticsRepository = statisticsRepository;
            _userDataRepository = userDataRepository;
        }

        public IBrickExecutionContext Context { get; set; }

        public float ExecutionOrder => 20;

        public async Task<DocumentCreationResult> AfterDocumentCreation(DocumentCreationResult documentCreationResult)
        {
            var newDocumentModel = _documentManager.GetOrCreateDocumentViewModel(documentCreationResult.Document);
            newDocumentModel.SetEurolookProperties(p => p.CreationNote = "Created using 'Convert To Eurolook'");

            await _userDataRepository.UpdateDocumentCreationSettingsAsync(documentCreationResult.DocumentCreationInfo);
            await _statisticsRepository.LogDocumentConversionAsync(
                documentCreationResult.DocumentCreationInfo.DocumentModel,
                new DocumentConversionLog
                {
                    Language = documentCreationResult.DocumentCreationInfo.Language.Locale,
                });

            return documentCreationResult;
        }
    }
}
