using System.Threading.Tasks;
using Eurolook.DocumentProcessing.DocumentCreation;
using Eurolook.WordAddIn.Database;

namespace Eurolook.WordAddIn.DocumentCreation
{
    public class UpdateSettingsAfterDocumentCreationTask :
        IAfterNewDocumentCreationTask,
        IAfterNewDocumentFromTemplateStoreCreationTask
    {
        private readonly IWordUserDataRepository _userDataRepository;

        public UpdateSettingsAfterDocumentCreationTask(
            IWordUserDataRepository userDataRepository)
        {
            _userDataRepository = userDataRepository;
        }

        public float ExecutionOrder => 10;

        public async Task<DocumentCreationResult> AfterDocumentCreation(DocumentCreationResult documentCreationResult)
        {
            await _userDataRepository.UpdateDocumentCreationSettingsAsync(documentCreationResult.DocumentCreationInfo);

            return documentCreationResult;
        }
    }
}
