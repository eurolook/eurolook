﻿using System.Threading.Tasks;
using Eurolook.Data.Database;
using Eurolook.DocumentProcessing.DocumentCreation;

namespace Eurolook.WordAddIn.DocumentCreation
{
    public class AddRecentTemplatesAfterDocumentCreationTask : IAfterNewDocumentFromTemplateStoreCreationTask, IAfterNewDocumentCreationTask
    {
        private readonly IRecentTemplateRepository _recentTemplateRepository;

        public AddRecentTemplatesAfterDocumentCreationTask(IRecentTemplateRepository recentTemplateRepository)
        {
            _recentTemplateRepository = recentTemplateRepository;
        }

        public float ExecutionOrder => 1;

        public async Task<DocumentCreationResult> AfterDocumentCreation(DocumentCreationResult documentCreationResult)
        {
            var documentCreationInfo = documentCreationResult.DocumentCreationInfo;

            if (documentCreationInfo.IsTemplateFromTemplateStore)
            {
                await _recentTemplateRepository.SaveAsRecentTemplateAsync(documentCreationInfo.User.Id, documentCreationInfo.TemplateStoreTemplate.Id, false);
            }
            else
            {
                await _recentTemplateRepository.SaveAsRecentTemplateAsync(documentCreationInfo.User.Id, documentCreationInfo.DocumentModel.Id, true);
            }

            return documentCreationResult;
        }
    }
}
