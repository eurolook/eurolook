using System.Threading.Tasks;
using Eurolook.DocumentProcessing.DocumentCreation;

namespace Eurolook.WordAddIn.DocumentCreation
{
    public interface IDocumentCreator
    {
        Task<DocumentCreationResult> CreateAndOpenDocumentAsync(DocumentCreationInfo documentCreationInfo);
    }
}
