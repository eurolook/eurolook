﻿using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.Authors;
using Eurolook.DocumentProcessing.DocumentCreation;

namespace Eurolook.WordAddIn.DocumentCreation
{
    public class AddFavoritesBeforeDocumentCreationTask :
        IBeforeNewDocumentCreationTask,
        IBeforeConvertToEurolookDocumentCreationTask,
        IBeforeNewDocumentFromTemplateStoreCreationTask
    {
        private readonly IFavoriteService _favoriteService;

        public AddFavoritesBeforeDocumentCreationTask(IFavoriteService favoriteService)
        {
            _favoriteService = favoriteService;
        }

        public float ExecutionOrder => 1;

        public Task BeforeDocumentCreation(DocumentCreationInfo documentCreationInfo)
        {
            foreach (var author in documentCreationInfo.AuthorRolesMappingList.Select(r => r.Author))
            {
                _favoriteService.AddFavoriteIfNotExisting(author);
            }

            return Task.CompletedTask;
        }
    }
}
