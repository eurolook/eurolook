using Eurolook.DocumentProcessing.DocumentCreation;

namespace Eurolook.WordAddIn.DocumentCreation
{
    public interface ITemplateStoreDocumentFactory
    {
        void WriteTemplateFromDatabaseToFile(string filePath, DocumentCreationInfo documentCreationInfo);
    }
}
