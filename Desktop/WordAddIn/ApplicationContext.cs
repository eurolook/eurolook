using System;
using Eurolook.AddIn.Common.Events;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn
{
    public sealed class ApplicationContext : IWordApplicationContext
    {
        public ApplicationContext([NotNull] Application application)
        {
            Application = application;
            InputDetector = new InputDetector();
        }

        ~ApplicationContext()
        {
            Dispose(false);
        }

        [NotNull]
        public Application Application { get; }

        public InputDetector InputDetector { get; }

        public bool IsObjectValid(object obj)
        {
            try
            {
                return Application.IsObjectValid[obj];
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool IsObjectValid(ContentControl contentControl)
        {
            try
            {
                // check whether the content control is valid; also try to access the range of the content control
                return Application.IsObjectValid[contentControl]
                       && Application.IsObjectValid[contentControl.Range]
                       && contentControl.Range != null;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                InputDetector?.Dispose();
            }
        }
    }
}
