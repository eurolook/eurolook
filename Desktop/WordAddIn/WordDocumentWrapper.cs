﻿using Eurolook.AddIn.Common;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.Extensions;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn
{
    public class WordDocumentWrapper : IOfficeDocumentWrapper
    {
        private readonly Document _document;

        public WordDocumentWrapper(Document document)
        {
            _document = document;
        }

        public CustomXMLParts CustomXMLParts => _document.CustomXMLParts;

        public string Name => _document.Name;

        public string FullName => _document.FullName;

        public string FileExtension => ".docx";

        public bool Saved
        {
            get => _document.Saved;
            set => _document.Saved = value;
        }

        public void Save()
        {
            _document.Save();
        }

        public void SaveAs(string fileName)
        {
            _document.SaveAs(fileName);
        }

        public void SaveCopyAs(string fileName)
        {
            _document.SaveCopyAs(fileName);
        }

        public dynamic GetDocument() => _document;

        public dynamic Open(string fileName) => _document.Application.Documents.Open(fileName);

        public dynamic BuiltInDocumentProperties => _document.BuiltInDocumentProperties;

        public MetaProperties ContentTypeProperties => _document.ContentTypeProperties;

        public string GetCustomXml(string rootName)
        {
            return _document.GetCustomXml(rootName);
        }

        public string GetHttpContentType => "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    }
}
