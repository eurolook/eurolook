using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.AddIn
{
    public interface ITaskPaneFactory
    {
        bool IsReady { get; }

        CustomTaskPane CreateCustomTaskPane(Window window);
    }
}
