﻿using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.HotKeys;

[ComVisible(true)]
[Guid("B7C1350F-D05E-4B75-BF04-743180471362")]
[InterfaceType(ComInterfaceType.InterfaceIsDual)]
public interface IHotKeyManager
{
    void Execute(string key);

    bool ActivateGlobalHotKeys(IWordAddinContext addinContext, Application application);

    void ActivateHotKeysForDocument(Document document);
}
