﻿using System;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using Eurolook.Common.Log;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.HotKeys;

/// <summary>
/// This class exposes Eurolook functions to be accessible via Word shortcuts.
/// </summary>
[ComVisible(true)]
[Guid("95DF4A5B-E2A3-4921-A217-AFD5F11C796B")]
[ClassInterface(ClassInterfaceType.AutoDual)]
public class HotKeyManager : IHotKeyManager, ICanLog
{
    private readonly IHotKeys _hotKeys;

    public HotKeyManager(IHotKeys hotKeys)
    {
        _hotKeys = hotKeys;
    }

    public void Execute(string key)
    {
        var methodInfo = _hotKeys.GetType().GetMethod(key);
        methodInfo?.Invoke(_hotKeys, null);
    }

    public bool ActivateGlobalHotKeys(IWordAddinContext addinContext, Application application)
    {
        var globalTemplate = application.Templates.OfType<Template>().FirstOrDefault(
            x => x.Type == WdTemplateType.wdGlobalTemplate && x.FullName == addinContext.GlobalAddInPath);
        if (globalTemplate == null)
        {
            return false;
        }

        ActivateHotKeysInTemplate(application, globalTemplate, true);
        return true;
    }

    public void ActivateHotKeysForDocument(Document document)
    {
        var application = document.Application;
        var attachedTemplate = (Template)document.get_AttachedTemplate();

        ActivateHotKeysInTemplate(application, attachedTemplate, false);
    }

    private void ActivateHotKeysInTemplate(Application application, Template template, bool globalHotKeys)
    {
        object originalCustomizationContext = null;

        try
        {
            // hot keys are stored in the attached template, but the VBA macros are in a global template
            originalCustomizationContext = application.CustomizationContext;
            application.CustomizationContext = template;

            // add hot key bindings
            ActivateHotKeys(application, globalHotKeys);

            // avoid a message box asking the user to save changes in the attached template
            template.Saved = true;
        }
        finally
        {
            if (originalCustomizationContext != null)
            {
                application.CustomizationContext = originalCustomizationContext;
            }
        }
    }

    private void ActivateHotKeys(Application application, bool globalHotKeys)
    {
        foreach (var methodInfo in _hotKeys.GetType().GetMethods(BindingFlags.Instance | BindingFlags.Public))
        {
            var attr =
                methodInfo.GetCustomAttributes(typeof(DefaultHotKeys.HotKeyAttribute), true).FirstOrDefault() as
                    DefaultHotKeys.HotKeyAttribute;
            if (attr == null || attr.IsGlobalHotKey != globalHotKeys)
            {
                continue;
            }

            try
            {
                // REMARK: existing bindings can be retrieved via
                //   <c>var binding = application.FindKey[attr.KeyCode];</c>
                if (
                    !application.KeyBindings.Cast<KeyBinding>()
                                .Any(
                                    x => x.Command.EndsWith(
                                        "." + methodInfo.Name,
                                        StringComparison.OrdinalIgnoreCase)))
                {
                    application.KeyBindings.Add(
                        WdKeyCategory.wdKeyCategoryMacro,
                        methodInfo.Name,
                        (int)attr.KeyCode);
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger().Error(
                    $"Cannot add shortcut {attr.KeyCode} for method {methodInfo.Name}.",
                    ex);
            }
        }
    }
}
