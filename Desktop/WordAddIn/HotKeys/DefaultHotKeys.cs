﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Windows.Forms;
using System.Windows.Input;
using Eurolook.AddIn.Common;
using Eurolook.Common.Log;
using Eurolook.WordAddIn.BrickCommands;
using Eurolook.WordAddIn.BrickCommands.AdjustBelowCommand;
using Eurolook.WordAddIn.BrickCommands.ApplyStyleViaShortcut;
using Eurolook.WordAddIn.BrickCommands.EurolookLauncher;
using Eurolook.WordAddIn.BrickCommands.InsertName;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.Messages;
using Eurolook.WordAddIn.ViewModels;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.HotKeys;

public class DefaultHotKeys : IHotKeys, ICanLog
{
    private readonly Lazy<IDocumentManager> _documentManager;

    public DefaultHotKeys(Lazy<IDocumentManager> documentManager)
    {
        _documentManager = documentManager;
    }

    [HotKey(false, WdKey.wdKeyF6)]
    public virtual void F6()
    {
        ExecuteBrickCommand<ApplyStyleViaShortcutCommand>();
    }

    [HotKey(false, WdKey.wdKeyF11)]
    public virtual void F11()
    {
        ExecuteDocumentCommand(dvm => dvm.NextBrickCommand);
    }

    [HotKey(false, WdKey.wdKeyShift, WdKey.wdKeyF6)]
    public virtual void Shift_F6()
    {
        ExecuteBrickCommand<InsertNameCommand>();
    }

    [HotKey(false, WdKey.wdKeyF11, WdKey.wdKeyShift)]
    public virtual void Shift_F11()
    {
        ExecuteDocumentCommand(dvm => dvm.PreviousBrickCommand);
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKeyShift, WdKey.wdKeyH)]
    public virtual void Control_Shift_H()
    {
        ExecuteBrickCommand<ApplyHeadingStyleCommand>("Heading");
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKeyShift, WdKey.wdKey1)]
    public virtual void Control_Shift_1()
    {
        ExecuteBrickCommand<ApplyHeadingStyleCommand>("Heading 1");
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKeyShift, WdKey.wdKey2)]
    public virtual void Control_Shift_2()
    {
        ExecuteBrickCommand<ApplyHeadingStyleCommand>("Heading 2");
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKeyShift, WdKey.wdKey3)]
    public virtual void Control_Shift_3()
    {
        ExecuteBrickCommand<ApplyHeadingStyleCommand>("Heading 3");
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKeyShift, WdKey.wdKey4)]
    public virtual void Control_Shift_4()
    {
        ExecuteBrickCommand<ApplyHeadingStyleCommand>("Heading 4");
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKey0)]
    public virtual void Control_0()
    {
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKey1)]
    public virtual void Control_1()
    {
        ExecuteBrickCommand<ApplyHeadingStyleCommand>("Numbered Heading 1");
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKey2)]
    public virtual void Control_2()
    {
        ExecuteBrickCommand<ApplyHeadingStyleCommand>("Numbered Heading 2");
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKey3)]
    public virtual void Control_3()
    {
        ExecuteBrickCommand<ApplyHeadingStyleCommand>("Numbered Heading 3");
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKey4)]
    public virtual void Control_4()
    {
        ExecuteBrickCommand<ApplyHeadingStyleCommand>("Numbered Heading 4");
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKey5)]
    public virtual void Control_5()
    {
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKey6)]
    public virtual void Control_6()
    {
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKey7)]
    public virtual void Control_7()
    {
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKey8)]
    public virtual void Control_8()
    {
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKeyAlt, WdKey.wdKeySpacebar)]
    public virtual void Control_Alt_Spacebar()
    {
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKeyShift, WdKey.wdKeyP)]
    public virtual void Control_Shift_P()
    {
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKeyShift, WdKey.wdKeyY)]
    public virtual void Control_Shift_Y()
    {
        ExecuteBrickCommand<ApplyListStyleCommand>("List Bullet");
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKeyShift, WdKey.wdKeyZ)]
    public virtual void Control_Shift_Z()
    {
        ExecuteBrickCommand<ApplyListStyleCommand>("List Dash");
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKeyShift, WdKey.wdKeyL)]
    public virtual void Control_Shift_L()
    {
        ExecuteBrickCommand<ApplyListStyleCommand>("List Number");
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKeyShift, WdKey.wdKeyR)]
    public virtual void Control_Shift_R()
    {
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKeyShift, WdKey.wdKeyX)]
    public virtual void Control_Shift_X()
    {
    }

    [HotKey(false, WdKey.wdKeyAlt, WdKey.wdKeyShift, (WdKey)Keys.Left)]
    public virtual void Shift_Alt_Left()
    {
        ExecuteBrickCommand<ApplyPromoteCommand>();
    }

    [HotKey(false, WdKey.wdKeyAlt, WdKey.wdKeyShift, (WdKey)Keys.Right)]
    public virtual void Shift_Alt_Right()
    {
        ExecuteBrickCommand<ApplyDemoteCommand>();
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKeyShift, WdKey.wdKeyB)]
    public virtual void Control_Shift_B()
    {
        ExecuteBrickCommand<AdjustBelowCommand>();
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKeyShift, WdKey.wdKeyJ)]
    public virtual void Control_Shift_J()
    {
        ExecuteBrickCommand<AdjustSelectionCommand>();
    }

    [HotKey(false, WdKey.wdKeyAlt, WdKey.wdKeyShift, WdKey.wdKeyS)]
    public virtual void Shift_Alt_S()
    {
    }

    [HotKey(false, WdKey.wdKeyControl, WdKey.wdKeyAlt, WdKey.wdKeyA)]
    public virtual void Control_Alt_A()
    {
    }

    [HotKey(true, WdKey.wdKeyAlt, WdKey.wdKeyShift, WdKey.wdKeyE)]
    public virtual void Shift_Alt_E()
    {
        ExecuteGlobalBrickCommand<EurolookLauncherCommand>();
    }

    [HotKey(false, WdKey.wdKeyAlt, WdKey.wdKeyS)]
    public virtual void Alt_S()
    {
    }

    /// <summary>
    /// Executes a command on the active DocumentViewModel.
    /// </summary>
    /// <param name="commandFunc">A func that returns the command to be executed on the DocumentViewModel.</param>
    /// <param name="parameter">An additional optional parameter passed to the command.</param>
    protected void ExecuteDocumentCommand(Func<DocumentViewModel, ICommand> commandFunc, object parameter = null)
    {
        Execute.OnUiThread(
            () =>
            {
                try
                {
                    var documentViewModel = _documentManager.Value.GetActiveDocumentViewModel();
                    var command = commandFunc(documentViewModel);
                    if (command.CanExecute(parameter))
                    {
                        command.Execute(parameter);
                    }
                }
                catch (Exception ex)
                {
                    this.LogError("Could not execute document command.", ex);
                }
            });
    }

    protected void ExecuteGlobalBrickCommand<T>(string parameter = null)
        where T : IBrickCommand
    {
        ExecuteBrickCommandCommon<T>(parameter, true);
    }

    protected void ExecuteBrickCommand<T>(string parameter = null)
        where T : IBrickCommand
    {
        ExecuteBrickCommandCommon<T>(parameter, false);
    }

    private void ExecuteBrickCommandCommon<T>(string parameter, bool isGlobalHotKey)
        where T : IBrickCommand
    {
        Execute.OnUiThread(
            () =>
            {
                try
                {
                    var documentViewModel = _documentManager.Value.GetActiveDocumentViewModel();
                    if (documentViewModel != null)
                    {
                        Messenger.Default.Send(
                            new HotKeyMessage
                            {
                                DocumentViewModel = documentViewModel,
                                CommandBrickType = typeof(T),
                                Parameter = parameter,
                                IsGlobalHotKey = isGlobalHotKey,
                            });
                    }
                }
                catch (Exception ex)
                {
                    this.LogError("Could not execute brick command.", ex);
                }
            });
    }

    [AttributeUsage(AttributeTargets.Method)]
    [SuppressMessage(
        "Sonar",
        "S1144:Unused private types or members should be removed",
        Justification = "Constructor used implicitly")]
    public class HotKeyAttribute : Attribute
    {
        public HotKeyAttribute(bool isGlobalHotKey, params WdKey[] keys)
        {
            IsGlobalHotKey = isGlobalHotKey;
            if (keys != null)
            {
                foreach (var wdKey in keys)
                {
#pragma warning disable S3265 // Non-flags enums should not be used in bitwise operations
                    // ReSharper disable once BitwiseOperatorOnEnumWithoutFlags
                    KeyCode |= wdKey;
#pragma warning restore S3265 // Non-flags enums should not be used in bitwise operations
                }
            }
        }

        public WdKey KeyCode { get; }

        public bool IsGlobalHotKey { get; }
    }
}
