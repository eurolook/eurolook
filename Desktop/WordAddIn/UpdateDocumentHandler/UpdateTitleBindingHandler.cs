using System;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.DocumentMetadata;
using Eurolook.Common.Log;
using Eurolook.Data.Models.Metadata;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.Extensions;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;
using Version = System.Version;

namespace Eurolook.WordAddIn.UpdateDocumentHandler
{
    public class UpdateTitleBindingHandler : IDocumentOnOpenHandler, ICanLog
    {
        private const string TitlePartBindingXpath = "/EurolookProperties/DocumentMetadata[1]/TitlePart[1]";
        private readonly IMetadataPropertyFactory _metadataPropertyFactory;
        private readonly IMetadataReaderWriter _metadataReaderWriter;

        private readonly Version _lastVersionWhichNeedsMigration = new Version("10.0.39209.0");

        public UpdateTitleBindingHandler(
            IMetadataPropertyFactory metadataPropertyFactory,
            IMetadataReaderWriter metadataReaderWriter)
        {
            _metadataPropertyFactory = metadataPropertyFactory;
            _metadataReaderWriter = metadataReaderWriter;
        }

        public float ExecutionOrder => 10f;

        public async Task HandleAsync(IEurolookDocument eurolookDocument)
        {
            if (IsUpdateRequired(eurolookDocument))
            {
                await UpdateTitleBinding(eurolookDocument);
            }
        }

        private bool IsUpdateRequired(IEurolookDocument eurolookDocument)
        {
            var eurolookProperties = eurolookDocument.GetEurolookProperties();
            if (!eurolookDocument.IsEurolookDocument)
            {
                return false;
            }

            var createVersion = ParseVersion(eurolookProperties.CreationVersion);
            var editVersion = ParseVersion(eurolookProperties.EditVersion);

            return createVersion <= _lastVersionWhichNeedsMigration && editVersion <= _lastVersionWhichNeedsMigration;
        }

        private async Task UpdateTitleBinding(IEurolookDocument eurolookDocument)
        {
            var document = eurolookDocument.GetDocument();
            bool isSaved = document.Saved;
            try
            {
                using (new DocumentAutomationHelper(document, "Update Title Binding"))
                {
                    await ProcessContentControls(eurolookDocument);
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
            finally
            {
                document.Saved = isSaved;
            }
        }

        private Version ParseVersion(string version)
        {
            return string.IsNullOrEmpty(version)
                ? new Version()
                : new Version(version);
        }

        private async Task ProcessContentControls(IEurolookDocument eurolookDocument)
        {
            bool useTitlePartBinding = await InitTitlePart(eurolookDocument);

            var plainTextContentControls = eurolookDocument
                                           .GetDocument().GetStoryRange(WdStoryType.wdMainTextStory).ContentControls
                                           .OfType<ContentControl>()
                                           .Where(cc => cc.Type == WdContentControlType.wdContentControlText);

            foreach (var contentControl in plainTextContentControls)
            {
                ProcessContentControlBoundToTitleOrTitlePart(contentControl, useTitlePartBinding);
            }
        }

        private async Task<bool> InitTitlePart(IEurolookDocument eurolookDocument)
        {
            _metadataReaderWriter.Init(eurolookDocument);
            var titlePartMetadataDefinition = _metadataReaderWriter.GetMetadataDefinition("TitlePart");
            bool useTitlePartBinding = titlePartMetadataDefinition != null;
            var metadataProperties = eurolookDocument.GetEurolookProperties().DocumentMetadata;
            if (useTitlePartBinding && !metadataProperties.ContainsKey("TitlePart"))
            {
                await WriteEmptyMetadata(eurolookDocument, titlePartMetadataDefinition);
            }

            return useTitlePartBinding;
        }

        private async Task WriteEmptyMetadata(IEurolookDocument eurolookDocument, MetadataDefinition metadataDefinition)
        {
            await _metadataPropertyFactory.InitAsync(eurolookDocument.Language.CultureInfo.TextInfo.LCID);
            var metadataProperty = _metadataPropertyFactory.Create(metadataDefinition, "");

            _metadataReaderWriter.ReplaceMetadataProperty(metadataDefinition, metadataProperty);
        }

        private void ProcessContentControlBoundToTitleOrTitlePart(
            ContentControl contentControl,
            bool useTitlePartBinding)
        {
            bool IsBoundToTitle(ContentControl cc)
            {
                string xPath = cc.XMLMapping.XPath;
                return xPath.Contains("coreProperties") && xPath.Contains("title");
            }

            bool IsBoundToTitlePart(ContentControl cc) => cc.XMLMapping.XPath == TitlePartBindingXpath;

            if (!contentControl.ShowingPlaceholderText && IsBoundToTitle(contentControl))
            {
                string title = contentControl.Range.Text;
                ConvertToRichTextContentControl(contentControl, useTitlePartBinding);
                contentControl.Range.Text = title;
            }
            else if (IsBoundToTitle(contentControl) || IsBoundToTitlePart(contentControl))
            {
                ConvertToRichTextContentControl(contentControl, useTitlePartBinding);
            }
        }

        private void ConvertToRichTextContentControl(ContentControl contentControl, bool useTitlePartBinding)
        {
            contentControl.Type = WdContentControlType.wdContentControlRichText;

            if (useTitlePartBinding)
            {
                contentControl.XMLMapping.SetMapping(TitlePartBindingXpath);
            }
            else
            {
                contentControl.Temporary = true;
            }
        }
    }
}
