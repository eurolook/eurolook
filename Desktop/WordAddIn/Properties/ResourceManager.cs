﻿using System.Globalization;
using System.IO;
using Eurolook.AddIn.Common.Properties;

namespace Eurolook.WordAddIn.Properties;

public class ResourceManager : IResourceManager
{
    private readonly Eurolook.AddIn.Common.Properties.ResourceManager _addInCommonResourceManager;

    public ResourceManager(Eurolook.AddIn.Common.Properties.ResourceManager addInCommonResourceManager)
    {
        _addInCommonResourceManager = addInCommonResourceManager;
    }

    public string GetString(string name)
    {
        return Resources.ResourceManager.GetString(name)
               ?? _addInCommonResourceManager.GetString(name);
    }

    public string GetString(string name, CultureInfo culture)
    {
        return Resources.ResourceManager.GetString(name, culture)
               ?? _addInCommonResourceManager.GetString(name, culture);
    }

    public object GetObject(string name)
    {
        return Resources.ResourceManager.GetObject(name)
               ?? _addInCommonResourceManager.GetObject(name);
    }

    public object GetObject(string name, CultureInfo culture)
    {
        return Resources.ResourceManager.GetObject(name, culture)
               ?? _addInCommonResourceManager.GetObject(name, culture);
    }

    public UnmanagedMemoryStream GetStream(string name)
    {
        return Resources.ResourceManager.GetStream(name)
               ?? _addInCommonResourceManager.GetStream(name);
    }

    public UnmanagedMemoryStream GetStream(string name, CultureInfo culture)
    {
        return Resources.ResourceManager.GetStream(name, culture)
               ?? _addInCommonResourceManager.GetStream(name, culture);
    }
}
