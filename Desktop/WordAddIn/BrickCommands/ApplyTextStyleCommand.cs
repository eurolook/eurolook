using System.Threading.Tasks;
using Eurolook.WordAddIn.BrickEngine;

namespace Eurolook.WordAddIn.BrickCommands
{
    public class ApplyTextStyleCommand : ApplyParagraphStyleCommand
    {
        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            await base.OnExecuteAsync(context, argument + ",Type your paragraph contents here.");
        }
    }
}
