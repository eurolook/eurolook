using System;
using System.Globalization;
using Eurolook.WordAddIn.BrickEngine;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    /// <summary>
    /// Inserts the number of pages into the fax table in Fax documents.
    /// </summary>
    [UsedImplicitly]
    public class InsertNumberOfPagesCommand : EditCommandBase
    {
        public override bool CanExecute(IBrickExecutionContext context, string argument)
        {
            var document = context.Document;
            if (document == null)
            {
                return false;
            }

            if (!string.Equals(context.EurolookProperties.DocumentModelName, "Fax", StringComparison.InvariantCultureIgnoreCase))
            {
                return false;
            }

            return base.CanExecute(context, argument);
        }

        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (new DocumentAutomationHelper(context, $"Apply {DisplayName} Brick"))
            {
                var document = context.Document;
                if (document == null)
                {
                    return Task.FromResult(0);
                }

                var bookmarks = document.Bookmarks;
                if (bookmarks.Exists("ELFaxPageNumber"))
                {
                    var range = bookmarks["ELFaxPageNumber"].Range;
                    range.Text = GetNumberOfPages(document).ToString(CultureInfo.InvariantCulture);

                    bookmarks.Add("ELFaxPageNumber", range);
                }
                else
                {
                    // the document either starts with the header table (i.e. logo) or
                    // with the date line (i.e. no logo)
                    var tblFaxTable = document.Tables[1].Range.Start == 0 ? document.Tables[2] : document.Tables[1];
                    var celPageNumber = tblFaxTable.Cell(7, 2);
                    var range = celPageNumber.Range;

                    range.Text = GetNumberOfPages(document).ToString(CultureInfo.InvariantCulture);
                    range.Font.Size = 11;
                }

                return Task.FromResult(0);
            }
        }

        private int GetNumberOfPages(Document document)
        {
            // NOTE: in WebView, the number of pages is returned as -1 in older Word versions
            int numberOfPages = (int)document.Range().Information[WdInformation.wdNumberOfPagesInDocument];
            if (numberOfPages == -1)
            {
                // make sure to not run in WebView in W9 or W10
                var view = document.ActiveWindow.View;
                var originalViewType = view.Type;
                view.Type = WdViewType.wdPrintView;

                // W9 needs repagination being triggered
                document.Repaginate();
                numberOfPages = (int)document.Range().Information[WdInformation.wdNumberOfPagesInDocument];
                view.Type = originalViewType;
            }

            return numberOfPages;
        }
    }
}
