﻿using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.Extensions;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    public class DeleteBrickCommand : EditCommandBase
    {
        private readonly IBrickEngine _brickEngine;

        public DeleteBrickCommand(IBrickEngine brickEngine)
        {
            _brickEngine = brickEngine;
        }

        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            var contentControl = context.Selection.Range.FindBrickContentControl();
            if (contentControl != null)
            {
                string undoRecordName = !string.IsNullOrEmpty(contentControl.Title)
                    ? $"Delete {contentControl.Title} Brick"
                    : "Delete Brick";

                using (var documentAutomationHelper = new DocumentAutomationHelper(context, undoRecordName))
                {
                    var brickContainer = contentControl.GetBrickContainer();
                    var documentStructure = _brickEngine.GetDocumentStructure(brickContainer.Id);

                    IEnumerable<BrickContainer> containersToDelete;
                    if (documentStructure != null && documentStructure.Position != PositionType.Cursor && !documentStructure.Brick.IsMultiInstance)
                    {
                        containersToDelete = contentControl.Range.Document.GetAllBrickContainers().Where(c => c.Id == brickContainer.Id);
                    }
                    else
                    {
                        containersToDelete = new[] { brickContainer };
                    }

                    bool isBodyBrick = documentStructure != null && documentStructure.Position == PositionType.Body;

                    foreach (var container in containersToDelete)
                    {
                        if (isBodyBrick)
                        {
                            container.ContentControl.DeleteBodyBrick();
                        }
                        else
                        {
                            container.ContentControl.DeleteBrick();
                        }
                    }

                    if (!isBodyBrick)
                    {
                        // select following content control
                        var rangeAfter = context.Selection.Range;
                        rangeAfter.Move(WdUnits.wdCharacter);
                        var followingContentControl = rangeAfter.FindBrickContentControl();
                        if (followingContentControl != null
                            && !followingContentControl.Range.SelectFirstEditableContentControl(documentAutomationHelper.PreferredViewType))
                        {
                            followingContentControl.Range.SelectInPreferredView(documentAutomationHelper.PreferredViewType);
                        }
                    }
                }
            }

            context.Application.ActiveWindow.SetFocus();
            return Task.FromResult(0);
        }
    }
}
