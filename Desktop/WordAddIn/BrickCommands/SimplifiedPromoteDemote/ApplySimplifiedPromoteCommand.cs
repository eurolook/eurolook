﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.AddIn.Common.BrickCommands.Configuration;
using Eurolook.DocumentProcessing;
using Eurolook.WordAddIn.BrickEngine;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands.SimplifiedPromoteDemote
{
    public class ApplySimplifiedPromoteCommand : ApplyStyleCommandBase
    {
        private readonly Func<List<string>, Document, ISimplifiedPromoteDemoteService> _promoteDemoteService;
        private PromoteDemoteConfigurationV1 _configuration;

        public ApplySimplifiedPromoteCommand(Func<List<string>, Document, ISimplifiedPromoteDemoteService> promoteDemoteService)
        {
            _promoteDemoteService = promoteDemoteService;
        }

        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (new DocumentAutomationHelper(
                    context,
                    "Outline Demote",
                    DocumentAutomationOption.Default | DocumentAutomationOption.RestoreSelection))
            {
                LoadConfiguration();

                Promote(context, _configuration.Lists, _configuration.IgnoreHiddenStyles);
            }

            return Task.CompletedTask;
        }

        private void Promote(IBrickExecutionContext context, List<List<string>> stylesList, bool ignoreHiddenStyles)
        {
            var range = context.Selection.Range;
            foreach (Paragraph paragraph in range.Paragraphs)
            {
                var neutralStyle = LanguageIndependentStyle.FromParagraph(paragraph);
                var styleName = neutralStyle.NameNeutral;
                var styles = stylesList.Where(l => l.Contains(styleName)).FirstOrDefault();

                if (styles == null)
                {
                    continue;
                }

                var promoteService = _promoteDemoteService(styles, context.Document);
                var newStyle = promoteService.SimplifiedPromote(styleName, ignoreHiddenStyles);

                if (newStyle == null)
                {
                    continue;
                }

                promoteService.ApplyStyleToParagraph(newStyle, paragraph);
            }
        }

        private void LoadConfiguration()
        {
            if (_configuration == null)
            {
                var brickConfigurationReader = new BrickConfigurationReader();
                _configuration = brickConfigurationReader.Read<PromoteDemoteConfigurationV1>(Brick.Configuration)
                                 ?? new PromoteDemoteConfigurationV1();
            }
        }
    }
}
