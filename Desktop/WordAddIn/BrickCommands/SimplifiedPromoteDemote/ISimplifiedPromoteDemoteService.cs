﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eurolook.DocumentProcessing;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.BrickCommands.SimplifiedPromoteDemote
{
    public interface ISimplifiedPromoteDemoteService
    {
        LanguageIndependentStyle SimplifiedPromote(string styleName, bool igoreHiddenStyles);

        LanguageIndependentStyle SimplifiedDemote(string styleName, bool igoreHiddenStyles);

        void ApplyStyleToParagraph(LanguageIndependentStyle style, Paragraph paragraph);
    }
}
