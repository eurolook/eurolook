﻿using System.Collections.Generic;
using System.Linq;
using Eurolook.DocumentProcessing;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.BrickCommands.SimplifiedPromoteDemote
{
    public class SimplifiedPromoteDemoteService : ISimplifiedPromoteDemoteService
    {
        private readonly List<string> _styles;
        private readonly Document _document;

        public SimplifiedPromoteDemoteService(List<string> styles, Document document)
        {
            _styles = styles;
            _document = document;
        }

        public LanguageIndependentStyle SimplifiedPromote(string styleName, bool igoreHiddenStyles)
        {
            var index = _styles.FindIndex(s => s == styleName);
            return igoreHiddenStyles
                            ? PreviousAvailableStyle(index)
                            : PreviousStyle(index);
        }

        public LanguageIndependentStyle SimplifiedDemote(string styleName, bool igoreHiddenStyles)
        {
            var index = _styles.FindIndex(s => s == styleName);
            return igoreHiddenStyles
                ? NextAvailableStyle(index)
                : NextStyle(index);
        }

        private LanguageIndependentStyle PreviousAvailableStyle(int index)
        {
            var reversedStyles = _styles.AsEnumerable().Reverse().ToList();
            var styleName = FindAvailableStyle(reversedStyles, _styles.Count - index);
            return LanguageIndependentStyle.FromNeutralStyleName(styleName, _document);
        }

        private LanguageIndependentStyle NextAvailableStyle(int index)
        {
            var styleName = FindAvailableStyle(_styles, index + 1);
            return LanguageIndependentStyle.FromNeutralStyleName(styleName, _document);
        }

        private string FindAvailableStyle(List<string> styles, int index)
        {
            return styles.Skip(index).FirstOrDefault(s => StyleConceptNew.IsStyleAvailable(s, _document));
        }

        private LanguageIndependentStyle PreviousStyle(int index)
        {
            if (index - 1 >= 0)
            {
                FindStyle(index - 1);
            }

            return null;
        }

        private LanguageIndependentStyle NextStyle(int index)
        {
            if (index + 1 < _styles.Count)
            {
                FindStyle(index + 1);
            }

            return null;
        }

        private LanguageIndependentStyle FindStyle(int index)
        {
            var styleName = _styles[index];
            if (!StyleConceptNew.IsStyleAvailable(styleName, _document))
            {
                return null;
            }

            return LanguageIndependentStyle.FromNeutralStyleName(styleName, _document);
        }

        public void ApplyStyleToParagraph(LanguageIndependentStyle style, Paragraph paragraph)
        {
            if (style == null)
            {
                return;
            }

            var rangeStart = paragraph.Range.Duplicate;
            rangeStart.Collapse(WdCollapseDirection.wdCollapseStart);
            if (rangeStart.IsEndOfRowMark)
            {
                return;
            }

            if (LanguageIndependentStyle.FromParagraph(paragraph).NameNeutral != style.NameNeutral)
            {
                // ReSharper disable once UseIndexedProperty
                paragraph.set_Style(style.Style);
            }
        }
    }
}
