﻿using System.Collections.Generic;

namespace Eurolook.WordAddIn.BrickCommands.SimplifiedPromoteDemote
{
    public class PromoteDemoteConfigurationV1
    {
        public List<List<string>> Lists { get; set; }

        public bool IgnoreHiddenStyles { get; set; } = true;
    }
}
