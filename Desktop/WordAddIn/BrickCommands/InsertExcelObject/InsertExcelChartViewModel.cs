﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.ClipboardTools;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.Common;
using GalaSoft.MvvmLight.CommandWpf;
using Microsoft.Office.Interop.Excel;

namespace Eurolook.WordAddIn.BrickCommands.InsertExcelObject
{
    public class InsertExcelChartViewModel : InsertExcelObjectViewModelBase<ExcelChart>
    {
        private readonly ISettingsService _settingsService;

        public InsertExcelChartViewModel(IEurolookWordDocument eurolookDocument, ISettingsService settingsService)
            : base(eurolookDocument)
        {
            _settingsService = settingsService;

            GetHelpCommand = new RelayCommand(() => _settingsService.OpenHelpLink("ExcelTableExcelChart"));
        }

        public RelayCommand GetHelpCommand { get; }

        public override string Title => "Insert Excel Chart";

        public override string ItemsLabel => "Charts:";

        public override bool HasAddress => false;

        public override string Message
        {
            get
            {
                if (!ExcelWorkbooks?.IsCompleted == true)
                {
                    return "Loading...";
                }

                if (!ExcelWorkbooks?.IsSuccessfullyCompleted == true)
                {
                    return "An error while accessing Excel.";
                }

                if (ExcelWorkbooks?.Result?.Any() == false)
                {
                    return "Open an Excel workbook first.";
                }

                if (SelectedItem == null)
                {
                    return "Select a chart from the list on the left.";
                }

                if (!SelectedItem.IsAccessible)
                {
                    return "One of the open Excel workbooks is currently in cell-editing mode or a dialog box is open. "
                           + "Close any open dialogs or quit cell editing to insert a chart.";
                }

                if (ExcelWorkbooks?.Result?.SelectMany(w => w.Objects).Any() == false)
                {
                    return "The open Excel workbooks do not contain any charts.";
                }

                if (SelectedWorkbook?.Objects.Any() == false)
                {
                    return "The selected workbook does not contain any charts.";
                }

                if (SelectedObject == null)
                {
                    return "Select a chart from the list on the left.";
                }

                if (!HasPreview)
                {
                    return "No preview available.";
                }

                return null;
            }
        }

        protected override ImageSource GetPreviewImage()
        {
            if (SelectedObject == null)
            {
                return null;
            }

            try
            {
                // Charts must be activated before copying or exporting
                SelectedObject.Activate();

                ImageSource result = null;

                if (SelectedObject.Shape != null)
                {
                    Execute.OnUiThread(
                        () =>
                        {
                            SelectedObject.Shape.CopyPicture(XlPictureAppearance.xlScreen, XlCopyPictureFormat.xlBitmap);
                            result = ClipboardImageHelper.ImageFromClipboardDib();
                        });
                }
                else
                {
                    using (var temporaryFile = new TemporaryFile(".png"))
                    {
                        SelectedObject.Chart.Export(temporaryFile.FullName, "PNG", true);

                        var buffer = File.ReadAllBytes(temporaryFile.FullName);

                        result = new BitmapImage().InitAndFreeze(buffer);
                    }
                }

                return result;
            }
            catch (COMException)
            {
                // ignore exception while Excel is in cell editing mode
            }

            return null;
        }

        protected override IExcelWorkbook CreateObjectWrapper(Process process, IntPtr w)
        {
            var workbookCharts = new ExcelWorkbookCharts(process, w);
            workbookCharts.Update();
            return workbookCharts;
        }
    }
}
