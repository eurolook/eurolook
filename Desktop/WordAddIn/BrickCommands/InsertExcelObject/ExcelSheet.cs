﻿using System;
using Eurolook.Common.Log;
using Microsoft.Office.Interop.Excel;
using Range = Microsoft.Office.Interop.Excel.Range;

namespace Eurolook.WordAddIn.BrickCommands.InsertExcelObject
{
    public class ExcelSheet : ExcelObject
    {
        public ExcelSheet(ExcelWorkbookSheets excelWorkbookSheets, Worksheet worksheet)
        {
            ExcelWorkbookSheets = excelWorkbookSheets;
            Worksheet = worksheet;
            Range = AssessUsedRange(worksheet);
            Update();
        }

        public Range Range { get; set; }

        public Worksheet Worksheet { get; }

        public ExcelWorkbookSheets ExcelWorkbookSheets { get; }

        public override string Key => $"{ExcelWorkbookSheets.Key}-{Name}";

        public override bool IsAccessible => ExcelWorkbookSheets.IsAccessible;

        public string Name { get; set; }

        public void Update()
        {
            Name = Worksheet?.Name;

            RaisePropertyChanged("");
        }

        private Range AssessUsedRange(Worksheet worksheet)
        {
            int lastRow = AssessLastRowWithContent(worksheet);
            int lastColumn = AssessLastColumnWithContent(worksheet);
            return worksheet?.Range[worksheet.Cells[1, 1], worksheet.Cells[lastRow, lastColumn]];
        }

        private int AssessLastRowWithContent(Worksheet worksheet)
        {
            try
            {
                for (int columnIndex = 1; columnIndex <= 10; columnIndex++)
                {
                    int lastRow = GetLastRowWithContent(worksheet, columnIndex);
                    if (lastRow > 1)
                    {
                        return Math.Min(lastRow, 50);
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }

            return 1;
        }

        private int AssessLastColumnWithContent(Worksheet worksheet)
        {
            try
            {
                for (int rowIndex = 1; rowIndex <= 10; rowIndex++)
                {
                    var lastColumn = GetLastColumnWithContent(worksheet, rowIndex);
                    if (lastColumn > 1)
                    {
                        return Math.Min(lastColumn, 20);
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }

            return 1;
        }

        private int GetLastRowWithContent(Worksheet worksheet, int columnIndex)
        {
            return ((dynamic)worksheet?.Cells[worksheet.Rows.Count, columnIndex]).End(XlDirection.xlUp).Row;
        }

        private int GetLastColumnWithContent(Worksheet worksheet, int rowIndex)
        {
            return ((dynamic)worksheet?.Cells[rowIndex, worksheet.Columns.Count]).End(XlDirection.xlToLeft).Column;
        }
    }
}
