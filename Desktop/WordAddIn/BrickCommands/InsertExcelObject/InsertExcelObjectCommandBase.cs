﻿using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.Data.Models;
using GalaSoft.MvvmLight.CommandWpf;

namespace Eurolook.WordAddIn.BrickCommands.InsertExcelObject
{
    public abstract class InsertExcelObjectCommandBase<TExcelObject, TSettingsObject> : InsertObjectCommandBase
        where TExcelObject : class, IExcelObject
        where TSettingsObject : InsertExcelObjectSettings, new()
    {
        protected virtual bool IncludeTitlePlaceholderDefault { get; set; } = false;
        protected virtual bool IncludeSourcePlaceholderDefault { get; set; } = false;

        protected ISettingsService SettingsService { get; }
        protected IUserDataRepository UserDataRepository { get; }
        protected IBrickRepository BrickRepository { get; }
        protected TSettingsObject BrickSettings { get; set; }

        protected bool HasTitlePlaceholder { get; set; } = true;
        protected bool HasSourcePlaceholder { get; set; } = true;

        protected bool IncludeTitlePlaceholder { get; set; }
        protected bool IncludeSourcePlaceholder { get; set; }
        protected bool SettingsSaved { get; set; } = false;

        private UserSettings _userSettings;

        protected InsertExcelObjectCommandBase(
            IDocumentManager documentManager,
            IMessageService messageService,
            ISettingsService settingsService,
            IUserDataRepository userDataRepository,
            IBrickRepository brickRepository)
            : base(documentManager, messageService)
        {
            SettingsService = settingsService;
            UserDataRepository = userDataRepository;
            BrickRepository = brickRepository;
            InitDefaults();
        }

        private void InitDefaults()
        {
            IncludeTitlePlaceholder = IncludeTitlePlaceholderDefault;
            IncludeSourcePlaceholder = IncludeSourcePlaceholderDefault;
        }

        protected virtual void ApplySettingsOrDefault(InsertExcelObjectViewModelBase<TExcelObject> viewModel)
        {
            if (!SettingsSaved)
            {
                return;
            }

            viewModel.SettingsSaved = SettingsSaved;
            viewModel.RestoreDefaultSettingsCommand = new RelayCommand<InsertExcelObjectViewModelBase<TExcelObject>>(RestoreSettings);

            viewModel.IncludeTitlePlaceholder =
                BrickSettings.IncludeTitlePlaceholder ?? IncludeTitlePlaceholderDefault;
            viewModel.IncludeSourcePlaceholder =
                BrickSettings.IncludeSourcePlaceholder ?? IncludeSourcePlaceholderDefault;
        }

        protected virtual void RestoreSettings(InsertExcelObjectViewModelBase<TExcelObject> viewModel)
        {
            viewModel.IncludeTitlePlaceholder = IncludeTitlePlaceholderDefault;
            viewModel.IncludeSourcePlaceholder = IncludeSourcePlaceholderDefault;
        }

        protected virtual async Task SaveSettings(InsertExcelObjectViewModelBase<TExcelObject> viewModel)
        {
            if (!SettingsSaved)
            {
                return;
            }

            if (viewModel.HasTitlePlaceholder)
            {
                BrickSettings.IncludeTitlePlaceholder = viewModel.IncludeTitlePlaceholder;
            }

            if (viewModel.HasSourcePlaceholder)
            {
                BrickSettings.IncludeSourcePlaceholder = viewModel.IncludeSourcePlaceholder;
            }

            await BrickRepository.SaveBrickSettings(_userSettings.Id, Brick.Id, BrickSettings);
        }

        protected virtual async Task LoadSettings()
        {
            _userSettings = await UserDataRepository.GetUserSettingsAsync();
            BrickSettings = await BrickRepository.GetBrickSettingsAsync<TSettingsObject>(
                    _userSettings.Id,
                    Brick.Id)
                ?? new TSettingsObject();
        }
    }

    public class InsertExcelObjectSettings
    {
        public bool? IncludeTitlePlaceholder { get; set; }
        public bool? IncludeSourcePlaceholder { get; set; }
    }
}
