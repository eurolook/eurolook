using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using Eurolook.OfficeNotificationBar;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Excel;

namespace Eurolook.WordAddIn.BrickCommands.InsertExcelObject
{
    public abstract class ExcelWorkbookBase<T> : ExcelObject, IExcelWorkbook
        where T : IExcelObject
    {
        protected ExcelWorkbookBase(Process process, IntPtr hWnd)
        {
            Process = process;
            HWnd = hWnd;

            Workbook = GetWorkbook();
        }

        public IntPtr HWnd { get; }

        public Process Process { get; }

        public string FullName { get; protected set; }

        public string Name { get; protected set; }

        public override bool IsAccessible
        {
            get
            {
                try
                {
                    if (Workbook == null)
                    {
                        return false;
                    }

#pragma warning disable S1481 // Unused local variables should be removed
                    var dummy = Workbook.Sheets.OfType<object>().FirstOrDefault();
#pragma warning restore S1481 // Unused local variables should be removed
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public bool IsValid
        {
            get { return !Process.HasExited && SafeNativeMethods.IsWindow(HWnd); }
        }

        public override string Key
        {
            get { return FullName; }
        }

        public Window Window
        {
            get { return IsValid ? ExcelHelper.GetExcelWindow(HWnd) : null; }
        }

        public ObservableCollection<T> Objects { get; } = new ObservableCollection<T>();

        [CanBeNull]
        public Workbook Workbook { get; }

        IEnumerable<IExcelObject> IExcelWorkbook.Objects
        {
            get { return Objects.OfType<IExcelObject>(); }
        }

        public abstract void Update();

        [NotNull]
        protected string GetName()
        {
            return GetWorkbookName() ?? GetWindowTitle();
        }

        [CanBeNull]
        protected string GetWorkbookFullName()
        {
            try
            {
                return Workbook?.FullName;
            }
            catch (COMException)
            {
                return null;
            }
        }

        [NotNull]
        private string GetWindowTitle()
        {
            var window = new NativeWindowWrapper(HWnd);
            string windowTitle = window.Title ?? "";
            string result = Regex.Replace(windowTitle, "- Excel$", "");
            result = Regex.Replace(result, @":\d+", "");
            return result.Trim();
        }

        [CanBeNull]
        private Workbook GetWorkbook()
        {
            try
            {
                if (Process.HasExited || !SafeNativeMethods.IsWindow(HWnd))
                {
                    return null;
                }

                return Window?.Parent as Workbook;
            }
            catch (Exception)
            {
                return null;
            }
        }

        [CanBeNull]
        private string GetWorkbookName()
        {
            try
            {
                return Workbook?.Name;
            }
            catch (COMException)
            {
                return null;
            }
        }
    }
}
