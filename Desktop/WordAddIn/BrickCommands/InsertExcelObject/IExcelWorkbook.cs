using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Office.Interop.Excel;

namespace Eurolook.WordAddIn.BrickCommands.InsertExcelObject
{
    public interface IExcelWorkbook : IExcelObject
    {
        IntPtr HWnd { get; }

        Process Process { get; }

        string FullName { get; }

        string Name { get; }

        bool IsValid { get; }

        Window Window { get; }

        IEnumerable<IExcelObject> Objects { get; }

        void Update();
    }
}
