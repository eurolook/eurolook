using System;
using Eurolook.Common.Log;
using GalaSoft.MvvmLight;

namespace Eurolook.WordAddIn.BrickCommands.InsertExcelObject
{
    public abstract class ExcelObject : ViewModelBase, IEquatable<ExcelObject>, IExcelObject, ICanLog
    {
        private bool _isSelected;

        public bool IsSelected
        {
            get { return _isSelected; }
            set { Set(() => IsSelected, ref _isSelected, value); }
        }

        public abstract string Key { get; }

        public abstract bool IsAccessible { get; }

        public static bool operator ==(ExcelObject myItem1, ExcelObject myItem2)
        {
            return Equals(myItem1, myItem2);
        }

        public static bool operator !=(ExcelObject myItem1, ExcelObject myItem2)
        {
            return !(myItem1 == myItem2);
        }

        public bool Equals(ExcelObject other)
        {
            return other != null && Key == other.Key;
        }

        public sealed override bool Equals(object obj)
        {
            var otherMyItem = obj as ExcelObject;
            if (ReferenceEquals(otherMyItem, null))
            {
                return false;
            }

            return otherMyItem.Equals(this);
        }

        public override int GetHashCode()
        {
            return Key.GetHashCode();
        }
    }
}
