﻿using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;

namespace Eurolook.WordAddIn.BrickCommands.InsertExcelObject
{
    public abstract class InsertExcelObjectOpCommandBase<TExcelObject> : InsertExcelObjectCommandBase<TExcelObject, InsertExcelObjectOpSettings>
        where TExcelObject : class, IExcelObject
    {
        protected override bool IncludeTitlePlaceholderDefault { get; set; } = true;
        protected virtual bool IncludeSubtitlePlaceholderDefault { get; set; } = false;
        protected virtual bool IncludeCaptionPlaceholderDefault { get; set; } = false;
        protected virtual bool IncludeCopyrightPlaceholderDefault { get; set; } = false;
        protected virtual bool IncludeFigureNotesPlaceholderDefault { get; set; } = false;

        protected InsertExcelObjectOpCommandBase(
            IDocumentManager documentManager,
            IMessageService messageService,
            ISettingsService settingsService,
            IUserDataRepository userDataRepository,
            IBrickRepository brickRepository)
            : base(documentManager, messageService, settingsService, userDataRepository, brickRepository)
        {
            SettingsSaved = true;
            InitDefaults();
        }

        private void InitDefaults()
        {
            IncludeTitlePlaceholder = IncludeTitlePlaceholderDefault;
            IncludeSubtitlePlaceholder = IncludeSubtitlePlaceholderDefault;
            IncludeCaptionPlaceholder = IncludeCaptionPlaceholderDefault;
            IncludeCopyrightPlaceholder = IncludeCopyrightPlaceholderDefault;
            IncludeFigureNotesPlaceholder = IncludeFigureNotesPlaceholderDefault;
        }

        protected bool IncludeSubtitlePlaceholder { get; set; }
        protected bool IncludeCaptionPlaceholder { get; set; }
        protected bool IncludeCopyrightPlaceholder { get; set; }
        protected bool IncludeFigureNotesPlaceholder { get; set; }

        protected bool HasSubtitlePlaceholder { get; set; } = true;
        protected bool HasCaptionPlaceholder { get; set; } = true;
        protected bool HasCopyrightPlaceholder { get; set; } = true;
        protected bool HasFigureNotesPlaceholder { get; set; } = true;

        protected override void ApplySettingsOrDefault(InsertExcelObjectViewModelBase<TExcelObject> viewModel)
        {
            base.ApplySettingsOrDefault(viewModel);
            viewModel.IncludeSubtitlePlaceholder =
                BrickSettings.IncludeSubtitlePlaceholder ?? IncludeSubtitlePlaceholderDefault;
            viewModel.IncludeCaptionPlaceholder =
                BrickSettings.IncludeCaptionPlaceholder ?? IncludeCaptionPlaceholderDefault;
            viewModel.IncludeCopyrightPlaceholder =
                BrickSettings.IncludeCopyrightPlaceholder ?? IncludeCopyrightPlaceholderDefault;
            viewModel.IncludeFigureNotesPlaceholder =
                BrickSettings.IncludeFigureNotesPlaceholder ?? IncludeFigureNotesPlaceholderDefault;
        }

        protected override void RestoreSettings(InsertExcelObjectViewModelBase<TExcelObject> viewModel)
        {
            base.RestoreSettings(viewModel);
            viewModel.IncludeSubtitlePlaceholder = IncludeSubtitlePlaceholderDefault;
            viewModel.IncludeCaptionPlaceholder = IncludeCaptionPlaceholderDefault;
            viewModel.IncludeCopyrightPlaceholder = IncludeCopyrightPlaceholderDefault;
            viewModel.IncludeFigureNotesPlaceholder = IncludeFigureNotesPlaceholderDefault;
        }

        protected override async Task SaveSettings(InsertExcelObjectViewModelBase<TExcelObject> viewModel)
        {
            if (!SettingsSaved)
            {
                return;
            }

            if (viewModel.HasSubtitlePlaceholder)
            {
                BrickSettings.IncludeSubtitlePlaceholder = viewModel.IncludeSubtitlePlaceholder;
            }

            if (viewModel.HasCaptionPlaceholder)
            {
                BrickSettings.IncludeCaptionPlaceholder = viewModel.IncludeCaptionPlaceholder;
            }

            if (viewModel.HasCopyrightPlaceholder)
            {
                BrickSettings.IncludeCopyrightPlaceholder = viewModel.IncludeCopyrightPlaceholder;
            }

            if (viewModel.HasFigureNotesPlaceholder)
            {
                BrickSettings.IncludeFigureNotesPlaceholder = viewModel.IncludeFigureNotesPlaceholder;
            }

            await base.SaveSettings(viewModel);
        }
    }

    public class InsertExcelObjectOpSettings : InsertExcelObjectSettings
    {
        public bool? IncludeSubtitlePlaceholder { get; set; }
        public bool? IncludeCaptionPlaceholder { get; set; }
        public bool? IncludeCopyrightPlaceholder { get; set; }
        public bool? IncludeFigureNotesPlaceholder { get; set; }
    }
}
