﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Media;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.ClipboardTools;
using GalaSoft.MvvmLight.CommandWpf;

namespace Eurolook.WordAddIn.BrickCommands.InsertExcelObject
{
    public class InsertExcelSheetViewModel : InsertExcelObjectViewModelBase<ExcelSheet>
    {
        private readonly ISettingsService _settingsService;

        private bool _isInSelectionMode;

        public InsertExcelSheetViewModel(IEurolookWordDocument eurolookDocument, ISettingsService settingsService)
            : base(eurolookDocument)
        {
            _settingsService = settingsService;

            GetHelpCommand = new RelayCommand(() => _settingsService.OpenHelpLink("ExcelTableExcelChart"));
        }

        public RelayCommand GetHelpCommand { get; }

        public override string Title => "Insert Excel Table";

        public override string ItemsLabel => "Sheets:";

        public override bool HasAddress => true;

        public override string Message
        {
            get
            {
                if (!ExcelWorkbooks?.IsCompleted == true)
                {
                    return "Loading...";
                }

                if (!ExcelWorkbooks?.IsSuccessfullyCompleted == true)
                {
                    return "An error while accessing Excel.";
                }

                if (ExcelWorkbooks?.Result?.Any() == false)
                {
                    return "Open an Excel workbook first.";
                }

                if (SelectedItem == null)
                {
                    return "Select a sheet from the list on the left.";
                }

                if (!SelectedItem.IsAccessible)
                {
                    return "One of the open Excel workbooks is currently in cell-editing mode or a dialog box is open. "
                           + "Close any open dialogs or quit cell editing to insert a table.";
                }

                if (ExcelWorkbooks?.Result?.SelectMany(w => w.Objects).Any() == false)
                {
                    return "The open Excel workbooks do not contain any sheets.";
                }

                if (SelectedWorkbook?.Objects.Any() == false)
                {
                    return "The selected workbook does not contain any sheets.";
                }

                if (SelectedObject == null)
                {
                    return "Select a worksheet from the list on the left.";
                }

                if (!HasPreview)
                {
                    return "No preview available.";
                }

                return null;
            }
        }

        public override bool IsInSelectionMode
        {
            get => _isInSelectionMode;
            set
            {
                _isInSelectionMode = value;
                if (value && SelectedObject?.ExcelWorkbookSheets != null && SelectedObject.IsAccessible)
                {
                    SafeNativeMethods.SetForegroundWindow(SelectedObject.ExcelWorkbookSheets.HWnd);
                    SelectedObject.Worksheet.Activate();
                }

                RaisePropertyChanged(() => IsInSelectionMode);
            }
        }

        protected override ImageSource GetPreviewImage()
        {
            if (SelectedObject == null)
            {
                return null;
            }

            try
            {
                ImageSource result = null;

                Execute.OnUiThread(
                    () =>
                    {
                        SelectedObject.Range.Copy();

                        result = ClipboardImageHelper.ImageFromClipboardDib();
                    });

                return result;
            }
            catch (COMException)
            {
                // ignore exception while Excel is in cell editing mode
            }

            return null;
        }

        protected override IExcelWorkbook CreateObjectWrapper(Process process, IntPtr w)
        {
            var workbookSheets = new ExcelWorkbookSheets(process, w);
            workbookSheets.Update();
            return workbookSheets;
        }
    }
}
