using Microsoft.Office.Interop.Excel;

namespace Eurolook.WordAddIn.BrickCommands.InsertExcelObject
{
    public class ExcelChart : ExcelObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExcelChart"/> class from an Excel chart sheet.
        /// </summary>
        /// <param name="excelWorkbookCharts">The parent object containing all charts/shapes of the workbook.</param>
        /// <param name="chart">An Excel <see cref="Chart"/> object.</param>
        public ExcelChart(ExcelWorkbookCharts excelWorkbookCharts, Chart chart)
            : this(excelWorkbookCharts, null, null, chart)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExcelChart"/> class from an Excel chart sheet.
        /// </summary>
        /// <param name="excelWorkbookCharts">The parent object containing all charts/shapes of the workbook.</param>
        /// <param name="worksheet">The worksheet containing the shape.</param>
        /// <param name="shape">An Excel <see cref="Shape"/> object referring to a chart, shape, or group shape.</param>
        public ExcelChart(ExcelWorkbookCharts excelWorkbookCharts, Worksheet worksheet, Shape shape)
            : this(excelWorkbookCharts, worksheet, shape, null)
        {
        }

        private ExcelChart(
            ExcelWorkbookCharts excelWorkbookCharts,
            Worksheet worksheet,
            Shape shape,
            Chart chart)
        {
            ExcelWorkbookCharts = excelWorkbookCharts;
            Worksheet = worksheet;
            Shape = shape;
            Chart = chart;

            Update();
        }

        public Chart Chart { get; }

        public Shape Shape { get; }

        public Worksheet Worksheet { get; }

        public ExcelWorkbookCharts ExcelWorkbookCharts { get; }

        public override string Key
        {
            get { return $"{ExcelWorkbookCharts.Key}-{Name}"; }
        }

        public override bool IsAccessible
        {
            get { return ExcelWorkbookCharts.IsAccessible; }
        }

        public string Name { get; set; }

        public void Update()
        {
            string chartName = Shape?.Name ?? Chart.Name;
            string worksheetName = Worksheet?.Name;

            dynamic sheet = Worksheet?.Application.ActiveSheet;
            var activeWorkbookName = sheet?.Name;

            if (activeWorkbookName != null && chartName.StartsWith(activeWorkbookName))
            {
                chartName = chartName.Remove(0, activeWorkbookName.Length).Trim();
            }

            Name = !string.IsNullOrEmpty(worksheetName) ? $"{worksheetName} - {chartName}" : chartName;

            // ReSharper disable once ExplicitCallerInfoArgument
            RaisePropertyChanged("");
        }

        public void Activate()
        {
            if (Shape != null)
            {
                Worksheet?.Activate();
                Shape.Select();
            }
            else
            {
                Chart?.Activate();
            }
        }
    }
}
