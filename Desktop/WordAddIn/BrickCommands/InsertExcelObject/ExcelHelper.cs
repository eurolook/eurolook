using System;
using System.Runtime.InteropServices;
using System.Text;
using Eurolook.Common.Log;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Excel;

namespace Eurolook.WordAddIn.BrickCommands.InsertExcelObject
{
    public class ExcelHelper
    {
        private delegate bool EnumChildCallback(int hWnd, ref int lParam);

        [CanBeNull]
        public static Window GetExcelWindow(IntPtr hWndMainWindow)
        {
            if (hWndMainWindow == IntPtr.Zero)
            {
                return null;
            }

            try
            {
                // Search the accessible child window (it has class name "EXCEL7")
                EnumChildCallback cb = EnumChildProc;
                int hWndChild = 0;
                EnumChildWindows(hWndMainWindow, cb, ref hWndChild);

                if (hWndChild == 0)
                {
                    return null;
                }

                // ReSharper disable InconsistentNaming

                // We call AccessibleObjectFromWindow, passing the constant OBJID_NATIVEOM (defined in winuser.h)
                // and IID_IDispatch - we want an IDispatch pointer into the native object model.
#pragma warning disable SA1312 // Variable names must begin with lower-case letter
                const uint OBJID_NATIVEOM = 0xFFFFFFF0;
                var IID_IDispatch = new Guid("{00020400-0000-0000-C000-000000000046}");

                int hr = AccessibleObjectFromWindow(
                    hWndChild,
                    OBJID_NATIVEOM,
                    IID_IDispatch.ToByteArray(),
                    out var ptr);
                return hr >= 0 ? ptr : null;

                // ReSharper restore InconsistentNaming
#pragma warning restore SA1312 // Variable names must begin with lower-case letter
            }
            catch (Exception ex)
            {
                LogManager.GetLogger().Error(ex.Message, ex);
                return null;
            }
        }

        [DllImport("Oleacc.dll")]
        private static extern int AccessibleObjectFromWindow(int hwnd, uint dwObjectId, byte[] riid, out Window ptr);

        [DllImport("User32.dll")]
        private static extern bool EnumChildWindows(IntPtr hWndParent, EnumChildCallback lpEnumFunc, ref int lParam);

        [DllImport("User32.dll")]
        private static extern int GetClassName(int hWnd, StringBuilder lpClassName, int nMaxCount);

        private static bool EnumChildProc(int hWndChild, ref int lParam)
        {
            var buf = new StringBuilder(128);
            GetClassName(hWndChild, buf, 128);
            if (buf.ToString() == "EXCEL7")
            {
                lParam = hWndChild;
                return false;
            }

            return true;
        }
    }
}
