using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using Eurolook.Common.Log;
using Microsoft.Office.Interop.Excel;

namespace Eurolook.WordAddIn.BrickCommands.InsertExcelObject
{
    public class ExcelWorkbookSheets : ExcelWorkbookBase<ExcelSheet>
    {
        public ExcelWorkbookSheets(Process process, IntPtr hWnd)
            : base(process, hWnd)
        {
            Update();
        }

        public override void Update()
        {
            Name = GetName();
            FullName = GetWorkbookFullName() ?? Name;

            var newSheets = GetAvailableSheets();

            var itemsToRemove = Objects.Except(newSheets).ToList();
            foreach (var sheet in itemsToRemove)
            {
                Objects.Remove(sheet);
            }

            foreach (var sheet in Objects)
            {
                sheet.Update();
            }

            for (int index = 0; index < newSheets.Count; index++)
            {
                var chart = newSheets[index];
                if (Objects.Count <= index || Objects[index] != chart)
                {
                    Objects.Insert(index, chart);
                }
            }

            RaisePropertyChanged("");

            Debug.Assert(Name != null, "Name is null");
            Debug.Assert(FullName != null, "FullName is null");
        }

        private List<ExcelSheet> GetAvailableSheets()
        {
            var result = new List<ExcelSheet>();

            if (Workbook == null || !IsAccessible)
            {
                return result;
            }

            try
            {
                foreach (var worksheet in Workbook.Sheets.OfType<Worksheet>())
                {
                    try
                    {
                        result.Add(new ExcelSheet(this, worksheet));
                    }
                    catch (COMException ex)
                    {
                        this.LogDebug(ex);
                    }
                }
            }
            catch (COMException ex)
            {
                this.LogDebug(ex);
            }

            return result;
        }
    }
}
