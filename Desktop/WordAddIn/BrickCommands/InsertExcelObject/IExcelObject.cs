namespace Eurolook.WordAddIn.BrickCommands.InsertExcelObject
{
    public interface IExcelObject
    {
        string Key { get; }

        bool IsAccessible { get; }

        bool IsSelected { get; set; }
    }
}
