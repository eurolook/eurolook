﻿using System;
using System.Windows;

namespace Eurolook.WordAddIn.BrickCommands.InsertExcelObject
{
    public partial class InsertExcelObjectView
    {
        public InsertExcelObjectView(BrickCommandViewModelBase viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }

        private void OnOkClick(object sender, RoutedEventArgs e)
        {
            try
            {
                DialogResult = true; // will close the dialog automatically
            }
            catch (InvalidOperationException)
            {
                // when not opened as dialog
                Close();
            }
        }
    }
}
