using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;
using Shape = Microsoft.Office.Interop.Excel.Shape;

namespace Eurolook.WordAddIn.BrickCommands.InsertExcelObject
{
    public class ExcelWorkbookCharts : ExcelWorkbookBase<ExcelChart>
    {
        public ExcelWorkbookCharts(Process process, IntPtr hWnd)
            : base(process, hWnd)
        {
            Update();
        }

        public override void Update()
        {
            Name = GetName();
            FullName = GetWorkbookFullName() ?? Name;

            var newCharts = GetAvailableCharts();

            var itemsToRemove = Objects.Except(newCharts).ToList();
            foreach (var chart in itemsToRemove)
            {
                Objects.Remove(chart);
            }

            foreach (var chart in Objects)
            {
                chart.Update();
            }

            for (int index = 0; index < newCharts.Count; index++)
            {
                var chart = newCharts[index];
                if (Objects.Count <= index || Objects[index] != chart)
                {
                    Objects.Insert(index, chart);
                }
            }

            // ReSharper disable once ExplicitCallerInfoArgument
            RaisePropertyChanged("");

            Debug.Assert(Name != null, "Name is null");
            Debug.Assert(FullName != null, "FullName is null");
        }

        private List<ExcelChart> GetAvailableCharts()
        {
            var result = new List<ExcelChart>();

            if (Workbook == null || !IsAccessible)
            {
                return result;
            }

            try
            {
                foreach (var sheet in Workbook.Sheets)
                {
                    if (sheet is Worksheet worksheet)
                    {
                        try
                        {
                            var shapes = worksheet.Shapes.OfType<Shape>();
                            foreach (var shape in shapes)
                            {
                                if (shape.Type == MsoShapeType.msoChart
                                    || shape.Type == MsoShapeType.msoAutoShape
                                    || shape.Type == MsoShapeType.msoSmartArt
                                    || shape.Type == MsoShapeType.msoGroup)
                                {
                                    result.Add(new ExcelChart(this, worksheet, shape));
                                }
                            }
                        }
                        catch (COMException)
                        {
                        }
                    }

                    if (sheet is Chart chartSheet)
                    {
                        result.Add(new ExcelChart(this, chartSheet));
                    }
                }
            }
            catch (COMException)
            {
            }

            return result;
        }
    }
}
