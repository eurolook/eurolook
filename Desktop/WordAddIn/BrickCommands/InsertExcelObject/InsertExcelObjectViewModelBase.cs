﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Common.Log;
using Eurolook.OfficeNotificationBar;
using GalaSoft.MvvmLight.Command;

namespace Eurolook.WordAddIn.BrickCommands.InsertExcelObject
{
    public abstract class InsertExcelObjectViewModelBase<TObject> : BrickCommandViewModelBase, IDisposable
        where TObject : class, IExcelObject
    {
        private const int UpdateInterval = 1000;

        private Timer _timer;
        private IExcelObject _selectedItem;

        private bool _includeTitlePlaceholder;
        private bool _includeSubtitlePlaceholder;
        private bool _includeCaptionPlaceholder;
        private bool _includeCopyrightPlaceholder;
        private bool _includeSourcePlaceholder;
        private bool _includeFigureNotesPlaceholder;

        protected InsertExcelObjectViewModelBase(IEurolookWordDocument eurolookDocument)
            : base(eurolookDocument)
        {
            _timer = new Timer(OnTimer, null, Timeout.Infinite, Timeout.Infinite);

            ExcelWorkbooks =
                new NotifyTask<ObservableCollection<IExcelWorkbook>>()
                    .NotifyOnCompletion(this)
                    .Start(() => Task.Run(() => Initialize()));
        }

        ~InsertExcelObjectViewModelBase()
        {
            Dispose(false);
        }

        public string WindowTitle => "Eurolook - " + Title;

        public ICommand StartExcelCommand => new RelayCommand(StartExcel);

        public ICommand RestoreDefaultSettingsCommand { get; set; }

        public NotifyTask<ObservableCollection<IExcelWorkbook>> ExcelWorkbooks { get; set; }

        public bool IsValid => SelectedObject != null;

        public abstract string ItemsLabel { get; }

        public bool HasItems => ExcelWorkbooks.IsCompleted && ExcelWorkbooks.Result.Any();

        public string Address
        {
            get
            {
                var sheet = SelectedItem as ExcelSheet;
                return sheet?.Range.Address;
            }
            set
            {
                try
                {
                    var sheet = SelectedItem as ExcelSheet;
                    if (sheet == null)
                    {
                        return;
                    }

                    var range = sheet.Worksheet.Range[value];
                    sheet.Range = range;
                    UpdatePreviewImage();
                }
                catch (Exception ex)
                {
                    this.LogDebug(ex);
                }
            }
        }

        public virtual bool IsInSelectionMode { get; set; }

        public abstract bool HasAddress { get; }

        public NotifyTask<ImageSource> PreviewImage { get; set; }

        public bool HasTitlePlaceholder { get; set; }

        public bool HasSubtitlePlaceholder { get; set; }

        public bool HasCaptionPlaceholder { get; set; }

        public bool HasCopyrightPlaceholder { get; set; }

        public bool HasSourcePlaceholder { get; set; }

        public bool HasFigureNotesPlaceholder { get; set; }

        public bool IncludeTitlePlaceholder
        {
            get => _includeTitlePlaceholder;
            set
            {
                Set(() => IncludeTitlePlaceholder, ref _includeTitlePlaceholder, value);
                RaisePropertyChanged(() => IncludeSubtitlePlaceholder);
            }
        }

        public bool IncludeSubtitlePlaceholder
        {
            // The selection of the subtitle is only allowed if the title is selected. It also remembers if the subtitle was selected when selecting the title.
            get => _includeSubtitlePlaceholder && IncludeTitlePlaceholder;
            set => Set(() => IncludeSubtitlePlaceholder, ref _includeSubtitlePlaceholder, value);
        }

        public bool IncludeCaptionPlaceholder
        {
            get => _includeCaptionPlaceholder;
            set => Set(() => IncludeCaptionPlaceholder, ref _includeCaptionPlaceholder, value);
        }

        public bool IncludeCopyrightPlaceholder
        {
            get => _includeCopyrightPlaceholder;
            set => Set(() => IncludeCopyrightPlaceholder, ref _includeCopyrightPlaceholder, value);
        }

        public bool IncludeSourcePlaceholder
        {
            get => _includeSourcePlaceholder;
            set => Set(() => IncludeSourcePlaceholder, ref _includeSourcePlaceholder, value);
        }

        public bool IncludeFigureNotesPlaceholder
        {
            get => _includeFigureNotesPlaceholder;
            set => Set(() => IncludeFigureNotesPlaceholder, ref _includeFigureNotesPlaceholder, value);
        }

        public string AlternativeText { get; set; }

        public bool SettingsSaved { get; set; } = false;

        public ExcelWorkbookBase<TObject> SelectedWorkbook => _selectedItem as ExcelWorkbookBase<TObject>;

        public IExcelObject SelectedItem
        {
            get => _selectedItem;
            set
            {
                _selectedItem = value;
                UpdatePreviewImage();
                RaisePropertyChanged(() => IsValid);
            }
        }

        public TObject SelectedObject => _selectedItem as TObject;

        public bool HasPreview => PreviewImage?.Result != null;

        public bool HasMessage => !string.IsNullOrEmpty(Message);

        public abstract string Message { get; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            _timer.Change(Timeout.Infinite, Timeout.Infinite);
            _timer.Dispose();
            _timer = null;
        }

        protected abstract IExcelWorkbook CreateObjectWrapper(Process process, IntPtr w);

        protected abstract ImageSource GetPreviewImage();

        private void UpdatePreviewImage()
        {
            PreviewImage = new NotifyTask<ImageSource>()
                           .NotifyOnCompletion(this, () => PreviewImage)
                           .NotifyOnCompletion(this, () => HasPreview)
                           .NotifyOnCompletion(this, () => HasMessage)
                           .NotifyOnCompletion(this, () => Message)
                           .Start(() => Task.Run(() => GetPreviewImage()));
        }

        private List<IExcelWorkbook> GetExcelWorkbooks()
        {
            var result = new List<IExcelWorkbook>();

            var processes = Process.GetProcessesByName("excel");
            foreach (var process in processes)
            {
                var windowFinder = new WindowFinder(process) { FindVisibleOnly = true };
                var windows = windowFinder
                              .Where(w => w.ClassName == "XLMAIN")
                              .Select(w => CreateObjectWrapper(process, w)).ToList();
                result.AddRange(windows);
            }

            // if a workbook is shown in multiple windows only include the first one
            var filteredResult = result.Where(w => w.Window?.WindowNumber == 1).OrderBy(w => w.Name).ToList();
            return filteredResult;
        }

        private ObservableCollection<IExcelWorkbook> Initialize()
        {
            var result = GetExcelWorkbooks();

            var firstChart = result.SelectMany(w => w.Objects).FirstOrDefault(c => c.IsAccessible);
            if (firstChart != null)
            {
                firstChart.IsSelected = true;
            }

            _timer?.Change(UpdateInterval, Timeout.Infinite);
            return new ObservableCollection<IExcelWorkbook>(result);
        }

        private void StartExcel()
        {
            try
            {
                Process.Start("excel");
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private void OnTimer(object state)
        {
            try
            {
                if (!WordAddinContext.IsApplicationInForeground())
                {
                    return;
                }

                _timer?.Change(Timeout.Infinite, Timeout.Infinite);
                var currentWorkbooks = ExcelWorkbooks?.Result;

                if (currentWorkbooks == null || ExcelWorkbooks?.Status != TaskStatus.RanToCompletion)
                {
                    return;
                }

                var workbooks = GetExcelWorkbooks();

                Execute.OnUiThread(
                    () =>
                    {
                        try
                        {
                            var itemsToRemove = currentWorkbooks.Where(w => !w.IsValid).ToList();
                            foreach (var excelWorkbook in itemsToRemove)
                            {
                                currentWorkbooks.Remove(excelWorkbook);
                            }

                            foreach (var excelWorkbook in currentWorkbooks)
                            {
                                excelWorkbook.Update();
                            }

                            var itemsToAdd = workbooks.Except(currentWorkbooks).ToList();
                            foreach (var excelWorkbook in itemsToAdd)
                            {
                                currentWorkbooks.Add(excelWorkbook);
                            }

                            RaisePropertyChanged("");
                        }
                        catch (Exception ex)
                        {
                            this.LogError(ex);
                        }
                    });
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
            finally
            {
                _timer?.Change(UpdateInterval, Timeout.Infinite);
            }
        }
    }
}
