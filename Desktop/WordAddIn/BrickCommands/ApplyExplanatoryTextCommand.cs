﻿using System;
using System.Diagnostics.CodeAnalysis;
using Eurolook.Common;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    [UsedImplicitly]
    internal class ApplyExplanatoryTextCommand : ApplyStyleCommandBase
    {
        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (new DocumentAutomationHelper(context, $"Apply {DisplayName} Brick"))
            {
                var selection = context.Selection;
                var range = selection.Range.Duplicate;
                range.HandleVanishingContentControl();
                var style = GetLanguageIndependentStyle(context, argument);
                if (style == null || IsSelectionContainedInExplanatoryText(range))
                {
                    return Task.FromResult(0);
                }

                ExpandMultipleParagraphSelection(range);
                HandleSelectionEqualToAContentControl(range, selection);
                RemoveContentControlsInRange(range);
                RemoveWhiteSpaceAtTheEndOfTheSelection(range, selection);

                var contentControl = CreateContentControl(range);
                CreateBrickContainer(contentControl);
                ApplyStyleToContent(range, contentControl, style);

                ResetSelectionRangeToRangeForWholeParagraph(selection, range);

                return Task.FromResult(0);
            }
        }

        private bool IsSelectionContainedInExplanatoryText(Range range)
        {
            var contentControl = range.ParentContentControl;
            var tag = contentControl?.Tag;
            if (tag != null && ShortGuid.TryParse(tag, out var shortGuid))
            {
                return shortGuid.Equals(Brick.Id);
            }

            return false;
        }

        private void ExpandMultipleParagraphSelection(Range range)
        {
            if (range.Text != null && range.Text.Contains("\r"))
            {
                range.Expand(WdUnits.wdParagraph);
            }
        }

        private void HandleSelectionEqualToAContentControl(Range range, Selection selection)
        {
            var contentControls = range.ContentControls;
            foreach (ContentControl cc in contentControls)
            {
                if (cc.Range.Start == range.Start + 1 && cc.Range.End == range.End - 1)
                {
                    range.SetRange(range.Start + 1, range.End - 1);
                    selection.SetRange(range.Start, range.End);
                    break;
                }
            }
        }

        private void RemoveWhiteSpaceAtTheEndOfTheSelection(Range range, Selection selection)
        {
            if (range.Text != null && range.Text.EndsWith(" "))
            {
                range.MoveEnd(WdUnits.wdCharacter, -1);
                selection.SetRange(range.Start, range.End);
            }
        }

        private void ResetSelectionRangeToRangeForWholeParagraph(Selection selection, Range range)
        {
            if (range.Text != null && range.Text.EndsWith("\r"))
            {
                selection.SetRange(range.Start, range.End);
            }
        }

        [SuppressMessage("SonarQube", "S2259: Null pointers should not be dereferenced", Justification = "Not null")]
        private void RemoveContentControlsInRange(Range range)
        {
            foreach (ContentControl contentControl in range.ContentControls)
            {
                contentControl.Delete();
            }

            var cc = range.ParentContentControl;
            if (range.Text != null && cc?.Range.Text == range.Text)
            {
                cc.set_DefaultTextStyle(WdBuiltinStyle.wdStyleDefaultParagraphFont);
                cc.Delete();
            }
        }

        private ContentControl CreateContentControl(Range range)
        {
            var cc = range.ContentControls.Add(WdContentControlType.wdContentControlRichText, range);

            AddPlaceholderWhenNoInputText(range, cc);

            return cc;
        }

        private void ApplyStyleToContent(Range range, ContentControl contentControl, LanguageIndependentStyle style)
        {
            if (range.Start != range.End)
            {
                ApplyStyleToSelection(range, style);
            }
            else
            {
                ApplyStyleToContentControl(contentControl, style);
            }
        }

        private void ApplyStyleToSelection(Range range, LanguageIndependentStyle style)
        {
            range.set_Style(style.Style);
        }

        private void ApplyStyleToContentControl(ContentControl contentControl, LanguageIndependentStyle style)
        {
            contentControl.set_DefaultTextStyle(style.Style);
        }

        private void CreateBrickContainer(ContentControl contentControl)
        {
            var brickContainer = new BrickContainer(Brick);
            contentControl.Tag = brickContainer.ToTag();
            brickContainer.ContentControl = contentControl;
        }

        private void AddPlaceholderWhenNoInputText(Range range, ContentControl cc)
        {
            if (range.Text == null || range.Text == "")
            {
                cc.SetPlaceholderText(null, null, "Type your explanatory text here.");
            }
        }
    }
}
