using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    [UsedImplicitly]
    internal class ApplyStyleCommand : ApplyParagraphStyleCommand
    {
        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (new DocumentAutomationHelper(context, $"Apply {DisplayName} Brick"))
            {
                var range = context.Selection.Range.Duplicate;
                range.HandleVanishingContentControl();

                var style = LanguageIndependentStyle.FromNeutralStyleName(argument, context.Document);
                if (style == null)
                {
                    return;
                }

                bool isCharacterStyle = style.Style.Linked || style.Style.Type == WdStyleType.wdStyleTypeLinked
                                                           || style.Style.Type == WdStyleType.wdStyleTypeCharacter;

                bool hasSelectionText = context.Selection.Text != null && !string.IsNullOrEmpty(context.Selection.Text.Trim('\a', '\r'))
                                                                       && context.Selection.Start < context.Selection.End;

                if (isCharacterStyle && hasSelectionText)
                {
                    ApplyCharacterStyle(context.Selection, style);
                }
                else
                {
                    await base.OnExecuteAsync(context, argument);
                }
            }
        }
    }
}
