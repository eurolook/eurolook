﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    public class ApplyLegalNumberingCommand : ApplyStyleCommandBase
    {
        private static readonly List<Range> ItalicParts = new List<Range>();

        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (new DocumentAutomationHelper(context, $"Apply {DisplayName} Brick"))
            {
                var range = context.Selection.Range.Duplicate;
                range.HandleVanishingContentControl();

                SaveItalicRanges(range);

                string newStyleNameNeutral = argument;
                if (IsLegalNumParStyle(range, argument))
                {
                    newStyleNameNeutral = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel("Text", range);
                }

                CopyLegalNumberedStyles(newStyleNameNeutral, context);

                ApplyStyleWithPlaceholder(range, newStyleNameNeutral, "Type your paragraph contents here.", true);

                RestoreItalicRanges();
            }

            return Task.FromResult(0);
        }

        private void CopyLegalNumberedStyles(string baseName, IBrickExecutionContext context)
        {
            const int maxListLevelToCopy = 4;
            if (!LanguageIndependentStyle.IsStyleAvailable(baseName, context.Document))
            {
                var stylesToCopy = new List<string>() { baseName };

                for (int listlevel = 2; listlevel <= maxListLevelToCopy; listlevel++)
                {
                    var stylename = $"{baseName} (Level {listlevel})";
                    if (!LanguageIndependentStyle.IsStyleAvailable(stylename, context.Document))
                    {
                        stylesToCopy.Add(stylename);
                    }
                }

                // TODO: should this be the language specific template?
                context.Document.CopyStylesFromDocumentModel(stylesToCopy, context.EurolookDocument.DocumentModel);
            }
        }

        private static void SaveItalicRanges(Range preserveRange)
        {
            ItalicParts.Clear();
            var workingRange = preserveRange.Duplicate;
            workingRange.Start = workingRange.Paragraphs.First.Range.Start;
            workingRange.End = workingRange.Paragraphs.Last.Range.End;
            int endOfRange = workingRange.End;
            workingRange.Collapse(WdCollapseDirection.wdCollapseStart);

            workingRange.Find.Text = "";
            workingRange.Find.ClearFormatting();
            workingRange.Find.Font.Italic = -1;
            workingRange.Find.Wrap = WdFindWrap.wdFindStop;
            while (workingRange.Find.Execute() && workingRange.Start < endOfRange)
            {
                if (workingRange.End > endOfRange)
                {
                    workingRange.End = endOfRange;
                    ItalicParts.Add(workingRange.Duplicate);
                    break;
                }

                ItalicParts.Add(workingRange.Duplicate);
            }
        }

        private static void RestoreItalicRanges()
        {
            foreach (var currentRange in ItalicParts)
            {
                try
                {
                    currentRange.Font.Italic = -1;
                }
                catch (Exception ex)
                {
                    LogManager.GetLogger().Debug("Error in restoring italic ranges");
                    LogManager.GetLogger().Debug(ex.ToString());
                }
            }

            ItalicParts.Clear();
        }

        private bool IsLegalNumParStyle(Range range, string argument)
        {
            return range.Paragraphs.OfType<Paragraph>().All(p => LanguageIndependentStyle.FromParagraph(p).NameNeutral.StartsWith(argument));
        }
    }
}
