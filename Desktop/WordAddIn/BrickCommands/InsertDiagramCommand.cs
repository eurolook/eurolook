﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.BrickCommands.Configuration;
using Eurolook.AddIn.Common.Database;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickCommands.ContentInserter;
using Eurolook.WordAddIn.BrickCommands.InsertPicture;
using Eurolook.WordAddIn.BrickEngine;
using GalaSoft.MvvmLight.CommandWpf;
using JetBrains.Annotations;
using Range = Microsoft.Office.Interop.Word.Range;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    [UsedImplicitly]
    public class InsertDiagramCommand : InsertObjectCommandBase
    {
        private readonly IUserDataRepository _userDataRepository;
        private readonly IBrickRepository _brickRepository;
        private readonly Func<IEurolookWordDocument, InsertPictureViewModel> _viewModelCreateFunc;

        private bool _includeTitlePlaceholder = true;
        private bool _includeSourcePlaceholder = true;

        private InsertPictureConfigurationV1 _configuration;
        private UserSettings _userSettings;
        private InsertPictureSettings _brickSettings;

        public InsertDiagramCommand(
            IDocumentManager documentManager,
            IMessageService messageService,
            IUserDataRepository userDataRepository,
            IBrickRepository brickRepository,
            Func<IEurolookWordDocument, InsertPictureViewModel> viewModelCreateFunc)
            : base(documentManager, messageService)
        {
            _userDataRepository = userDataRepository;
            _brickRepository = brickRepository;
            _viewModelCreateFunc = viewModelCreateFunc;
        }

        public override IEnumerable<string> RequiredStyles
        {
            get
            {
                LoadConfiguration();

                yield return _configuration.PictureTitleStyle;
                yield return _configuration.PictureBodyStyle;
                yield return _configuration.PictureSourceStyle;
            }
        }

        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            try
            {
                using var viewModel = _viewModelCreateFunc(DocumentManager.GetActiveDocumentViewModel());

                LoadConfiguration();
                await LoadSettings();

                viewModel.ConfigureOptions(_configuration);

                ApplySettings(viewModel);
                viewModel.WindowTitle = "Insert Diagram";
                viewModel.AllowBrowseForFile = false;
                viewModel.EmptyClipboardMessage =
                    "The Clipboard does not contain a diagram. First copy a diagram from Visio or PowerPoint.";

                var win = new InsertPictureView(viewModel);

                if (MessageService.ShowDialog(win) == true)
                {
                    _includeTitlePlaceholder = viewModel.IsTitleSelected;
                    _includeSourcePlaceholder = viewModel.IsSourceSelected;

                    using var documentAutomationHelper = new DocumentAutomationHelper(context, DisplayName);
                    var format = viewModel.GetPreferredDataFormat();
                    var source = PictureInserter.PictureSource.Clipboard;
                    Enum.TryParse(argument, true, out TitleInsertionPosition insertionPosition);
                    var range = InsertPicture(
                        context.Selection.Range,
                        source,
                        format,
                        insertionPosition,
                        viewModel.AlternativeText);
                    FixScaleTooLarge(range, range.InlineShapes[1]);
                    range.SelectFirstPlaceholderContentControl(documentAutomationHelper.PreferredViewType);
                    await SaveSettings(viewModel);
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        protected override string ResolveLabelAlias(Range range)
        {
            if (IsInAgencyChapter(range))
            {
                return _configuration.LabelTitleWithAgencyChapter;
            }

            return HasChapterNumber(range) ? _configuration.LabelTitleWithChapter : _configuration.LabelTitle;
        }

        private Range InsertPicture(Range range, PictureInserter.PictureSource source, ClipboardFormat format, TitleInsertionPosition insertionPosition, string altText)
        {
            range = range.GetCursorBrickInsertionRange();
            range.HandleVanishingContentControl();
            range.Select();

            string titleStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(_configuration.PictureTitleStyle, range);
            string bodyStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(_configuration.PictureBodyStyle, range);
            string sourceStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(_configuration.PictureSourceStyle, range);

            var titlePlaceholderInserter = _includeTitlePlaceholder
                ? new ParagraphInserter(
                    titleStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                        LabelAlias = ResolveLabelAlias(range),
                    },
                    new PlaceholderInserter("Type your title here."))
                : null;

            var pictureInserter = new ParagraphInserter(
                bodyStyle,
                new PictureInserter
                {
                    FileName = null,
                    Format = format,
                    Source = source,
                    AlternativeText = altText ?? string.Empty,
                });

            var sourcePlaceholderInserter = _includeSourcePlaceholder
                ? new ParagraphInserter(
                    sourceStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                        LabelAlias = _configuration.LabelSource,
                        IsItalic = true,
                    },
                    new TextInserter
                    {
                        Text = ":",
                        IsItalic = true,
                    },
                    new TextInserter { Text = " " },
                    new PlaceholderInserter("Type your source here."))
                : null;

            var itemsToInsert = new IContentInserter[]
            {
                titlePlaceholderInserter, pictureInserter, sourcePlaceholderInserter,
            }.Where(c => c != null).ToArray();

            if (titlePlaceholderInserter != null && insertionPosition == TitleInsertionPosition.TitleBelowInsertedContent)
            {
                itemsToInsert.Swap(0, 1);
            }

            itemsToInsert.OfType<ParagraphInserter>().Last().Content.First().InsertAt =
                ContentInsertionPosition.Replace;

            var contentInserter = new BrickContainerInserter(Brick, itemsToInsert);

            var positionProvider = new RangePositionProvider(range);
            return contentInserter.Insert(positionProvider);
        }

        private void ApplySettings(InsertPictureViewModel viewModel)
        {
            if (!_configuration.SettingsSaved)
            {
                return;
            }

            viewModel.SettingsSaved = true;
            viewModel.RestoreDefaultSettingsCommand = new RelayCommand<InsertPictureViewModel>(RestoreSettings);

            viewModel.IsTitleSelected = _brickSettings.AddPictureTitle ?? viewModel.IsTitleSelected;
            viewModel.IsSourceSelected = _brickSettings.AddSourceLine ?? viewModel.IsSourceSelected;
        }

        private void RestoreSettings(InsertPictureViewModel viewModel)
        {
            viewModel.ConfigureOptions(_configuration);
        }

        private async Task SaveSettings(InsertPictureViewModel viewModel)
        {
            if (!_configuration.SettingsSaved)
            {
                return;
            }

            if (_configuration.AllowConfiguration)
            {
                _brickSettings.AddPictureTitle = viewModel.ShowTitleCheckbox ? viewModel.IsTitleSelected : _brickSettings.AddPictureTitle;
                _brickSettings.AddSourceLine = viewModel.HasSourceLine ? viewModel.IsSourceSelected : _brickSettings.AddSourceLine;
            }

            await _brickRepository.SaveBrickSettings(_userSettings.Id, Brick.Id, _brickSettings);
        }

        private async Task LoadSettings()
        {
            _userSettings = await _userDataRepository.GetUserSettingsAsync();
            _brickSettings =
                await _brickRepository.GetBrickSettingsAsync<InsertPictureSettings>(
                    _userSettings.Id,
                    Brick.Id)
                ?? new InsertPictureSettings();
        }

        private void LoadConfiguration()
        {
            if (_configuration == null)
            {
                var brickConfigurationReader = new BrickConfigurationReader();
                _configuration = brickConfigurationReader.Read<InsertPictureConfigurationV1>(Brick.Configuration)
                                 ?? new InsertPictureConfigurationV1();
            }
        }
    }
}
