using System;
using System.Windows.Input;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.Ribbon;
using Eurolook.WordAddIn.ViewModels;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;

namespace Eurolook.WordAddIn.BrickCommands.EurolookLauncher
{
    public class CommandViewModel : ViewModelBase, ICanLog
    {
        private readonly CommandBrick _commandBrick;
        private readonly IRibbonLauncher _ribbonLauncher;

        public string CommandName { get; }

        public ICommand Command { get; }

        public string Description { get; }

        public string TabName { get; set; }

        public string CategoryName { get; set; }

        public string GroupName { get; set; }

        public bool IsGroupNameVisible { get; set; }

        public CommandViewModel(TileViewModel tile)
        {
            CommandName = tile.Title;
            Description = tile.Description;
            if (tile is BrickViewModel brickViewModel)
            {
                CategoryName = brickViewModel.Brick?.Category?.Name;
                GroupName = brickViewModel.Brick?.Group?.DisplayName;
                Command = brickViewModel.InsertBrickCommand;
            }
            else if (tile is InfoTileViewModel)
            {
                Command = tile.TileCommand;
            }

            IsGroupNameVisible = !string.IsNullOrWhiteSpace(GroupName);
        }

        public CommandViewModel(string groupName, CommandBrick commandBrick, IRibbonLauncher ribbonLauncher)
        {
            _commandBrick = commandBrick;
            _ribbonLauncher = ribbonLauncher;
            CommandName = commandBrick.DisplayName;
            Description = commandBrick.Description;
            GroupName = groupName;
            IsGroupNameVisible = !string.IsNullOrWhiteSpace(GroupName);
            Command = new RelayCommand(
                () => _ribbonLauncher.ExecuteCommandBrick(_commandBrick),
                () => true);
        }

        public bool CanExecute()
        {
            return Command?.CanExecute(null) == true;
        }

        public void Execute()
        {
            try
            {
                Command?.Execute(null);
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        public override string ToString()
        {
            return CommandName;
        }
    }
}
