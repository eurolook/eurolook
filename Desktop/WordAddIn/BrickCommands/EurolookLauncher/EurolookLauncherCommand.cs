﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.OpenXml;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.Ribbon;
using Eurolook.WordAddIn.TaskPane;

namespace Eurolook.WordAddIn.BrickCommands.EurolookLauncher
{
    public class EurolookLauncherCommand : EditCommandBase
    {
        private readonly ISettingsService _settingsService;
        private readonly IMessageService _messageService;
        private readonly ITaskPaneManager _taskPaneManager;
        private readonly IRibbonLauncher _ribbonLauncher;

        public EurolookLauncherCommand(
            ISettingsService settingsService,
            IMessageService messageService,
            ITaskPaneManager taskPaneManager,
            IRibbonLauncher ribbonLauncher)
        {
            _settingsService = settingsService;
            _messageService = messageService;
            _taskPaneManager = taskPaneManager;
            _ribbonLauncher = ribbonLauncher;
        }

        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            var viewModel = new EurolookLauncherViewModel(_settingsService) { DialogTitle = DisplayName };
            var window = new EurolookLauncherWindow(viewModel);
            viewModel.UpdateCommands(_taskPaneManager, _ribbonLauncher, Brick);
            _messageService.ShowPopup(window);
            return Task.CompletedTask;
        }
    }
}
