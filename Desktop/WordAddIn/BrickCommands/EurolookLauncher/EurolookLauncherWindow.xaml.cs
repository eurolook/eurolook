using System.Windows;
using System.Windows.Input;

namespace Eurolook.WordAddIn.BrickCommands.EurolookLauncher
{
    public partial class EurolookLauncherWindow : Window
    {
        public EurolookLauncherWindow(EurolookLauncherViewModel viewModel)
        {
            InitializeComponent();
            viewModel.CloseAction = Close;
            DataContext = viewModel;
            _viewModel = viewModel;
        }

        private readonly EurolookLauncherViewModel _viewModel;

        private void OnListItemDoubleClick(object sender, MouseButtonEventArgs e)
        {
           _viewModel.MouseDoubleClickExecution(e);
        }
    }
}
