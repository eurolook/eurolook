﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Common.Extensions;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.Ribbon;
using Eurolook.WordAddIn.TaskPane;
using Eurolook.WordAddIn.ViewModels;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Eurolook.WordAddIn.BrickCommands.EurolookLauncher
{
    public class EurolookLauncherViewModel
        : ViewModelBase
    {
        private readonly ISettingsService _settingsService;
        private CommandViewModel[] _availableCommands;
        private string _searchTerm;
        private ObservableCollectionEx<CommandViewModel> _commandSuggestions;
        private int _selectedIndex;
        private bool _isCommandBoxFocused;
        private string _dialogTitle;

        public RelayCommand<EventArgs> WindowActivatedEvent { get; }
        public RelayCommand<KeyEventArgs> WindowKeyUpEvent { get; }
        public RelayCommand<KeyEventArgs> CommandBoxKeyDownEvent { get; }
        public RelayCommand<SelectionChangedEventArgs> SelectionChangedEvent { get; }

        public string WindowTitle => $"Eurolook - {DialogTitle}";

        public string DialogTitle
        {
            get => _dialogTitle;
            set => Set(() => DialogTitle, ref _dialogTitle, value);
        }

        public string SearchTerm
        {
            get => _searchTerm;
            set
            {
                Set(() => SearchTerm, ref _searchTerm, value);
                SearchCommands();
            }
        }

        public ObservableCollectionEx<CommandViewModel> CommandSuggestions
        {
            get => _commandSuggestions;
            set
            {
                Set(() => CommandSuggestions, ref _commandSuggestions, value);
            }
        }

        public int SelectedIndex
        {
            get => _selectedIndex;
            set
            {
                Set(() => SelectedIndex, ref _selectedIndex, value);
            }
        }

        public bool IsCommandBoxFocused
        {
            get => _isCommandBoxFocused;
            set
            {
                Set(() => IsCommandBoxFocused, ref _isCommandBoxFocused, value);
            }
        }

        public RelayCommand GetHelpCommand { get; }

        public Action CloseAction { get; set; }

        public CommandViewModel SelectedCommand =>
            CommandSuggestions.Count > _selectedIndex ? CommandSuggestions.ElementAt(_selectedIndex) : null;

        public EurolookLauncherViewModel(ISettingsService settingsService)
        {
            _settingsService = settingsService;
            _commandSuggestions = new ObservableCollectionEx<CommandViewModel>();
            WindowActivatedEvent = new RelayCommand<EventArgs>(WindowActivated);
            WindowKeyUpEvent = new RelayCommand<KeyEventArgs>(WindowKeyUp);
            CommandBoxKeyDownEvent = new RelayCommand<KeyEventArgs>(CommandBoxKeyDown);
            SelectionChangedEvent = new RelayCommand<SelectionChangedEventArgs>(SelectionChanged);

            GetHelpCommand = new RelayCommand(() => _settingsService.OpenHelpLink("CommandLauncher"));
        }

        public void MouseDoubleClickExecution(MouseButtonEventArgs obj)
        {
                ExecuteSelectedCommand();
        }

        private void SelectionChanged(SelectionChangedEventArgs e)
        {
            if (e.Source is ListView list)
            {
                list.ScrollIntoView(list.SelectedItem);
            }
        }

        private void CommandBoxKeyDown(KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Down:
                    e.Handled = true;
                    SelectDown();
                    break;
                case Key.Up:
                    e.Handled = true;
                    SelectUp();
                    break;
            }
        }

        private void WindowKeyUp(KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape:
                    e.Handled = true;
                    Quit();
                    break;
                case Key.Enter:
                    e.Handled = true;
                    ExecuteSelectedCommand();
                    break;
            }
        }

        private void WindowActivated(EventArgs args)
        {
            IsCommandBoxFocused = true;
        }

        public void ExecuteSelectedCommand()
        {
            if (SelectedCommand?.CanExecute() == true)
            {
                CloseAction?.Invoke();
                SelectedCommand.Execute();
            }
        }

        private void SearchCommands()
        {
            CommandSuggestions.Clear();
            if (SearchTerm != null && SearchTerm.Length > 1)
            {
                var results = _availableCommands
                    .Where(
                        x => x.CommandName?.Contains(SearchTerm, StringComparison.InvariantCultureIgnoreCase) == true
                             || x.GroupName?.Contains(SearchTerm, StringComparison.InvariantCultureIgnoreCase) == true);
                CommandSuggestions.AddRange(results.OrderBy(x => x.GroupName).ThenBy(x => x.CommandName));
            }

            SelectedIndex = 0;
        }

        public void UpdateCommands(ITaskPaneManager taskPaneManager, IRibbonLauncher ribbonLauncher, Brick launcherBrick)
        {
            var taskPaneVm = taskPaneManager.GetActiveTaskPane()?.TaskPaneViewModel;
            if (taskPaneVm == null)
            {
                _availableCommands = null;
                return;
            }

            var result = new List<CommandViewModel>();
            result.AddRange(taskPaneVm.HomeTabViewModel.Tiles
                                      .Where(x => x.BrickId != launcherBrick.Id)
                                      .Select(x => new CommandViewModel(x) { TabName = "Home" }));
            result.AddRange(taskPaneVm.ToolsTabViewModel.Tiles
                                      .Where(x => x.BrickId != launcherBrick.Id)
                                      .Select(x => new CommandViewModel(x) { TabName = "Tools" }));
            foreach (var categoryViewModel in taskPaneVm.BricksTabViewModel.DocumentBrickCategories)
            {
                foreach (var tile in categoryViewModel.Bricks.Where(x => x.BrickId != launcherBrick.Id))
                {
                    if (tile is BrickGroupViewModel brickGroupViewModel)
                    {
                        foreach (var brick in brickGroupViewModel.Bricks)
                        {
                            result.Add(new CommandViewModel(brick) { TabName = "Bricks" });
                        }
                    }
                    else
                    {
                        result.Add(new CommandViewModel(tile) { TabName = "Bricks" });
                    }
                }
            }

            var launcherCommands = ribbonLauncher.GetLauncherCommands();
            foreach (var groupName in launcherCommands.Keys)
            {
                foreach (var commandBrick in launcherCommands[groupName])
                {
                    result.Add(new CommandViewModel(groupName, commandBrick, ribbonLauncher) { TabName = "Ribbon" });
                }
            }

            _availableCommands = result.ToArray();
        }

        public void SelectDown()
        {
            var newIndex = _selectedIndex + 1;
            if (CommandSuggestions.Count > newIndex)
            {
                SelectedIndex = newIndex;
            }
        }

        public void SelectUp()
        {
            var newIndex = _selectedIndex - 1;
            if (newIndex >= 0)
            {
                SelectedIndex = newIndex;
            }
        }

        public void Quit()
        {
            CloseAction?.Invoke();
        }
    }
}
