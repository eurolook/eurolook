using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Authors;
using Eurolook.Common.Extensions;
using Eurolook.DocumentProcessing;
using Eurolook.WordAddIn.Authors;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.Extensions;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.BrickCommands
{
    [UsedImplicitly]
    public class ChangeDocumentAuthorCommand : EditCommandBase
    {
        private readonly IMessageService _messageService;
        private readonly IEnumerable<IChangeAuthorHandler> _changeAuthorHandlers;
        private readonly Func<string, ChangeAuthorViewModel> _changeAuthorViewModelFunc;
        private readonly IAuthorManager _authorManager;
        private readonly IFavoriteService _favoriteService;
        private readonly IEnumerable<IAuthorChangeExecutor> _authorChangeExecutors;
        private readonly ICoAuthoringService _coAuthoringService;

        public ChangeDocumentAuthorCommand(
            IMessageService messageService,
            IEnumerable<IChangeAuthorHandler> changeAuthorHandlers,
            Func<string, ChangeAuthorViewModel> changeAuthorViewModelFunc,
            IAuthorManager authorManager,
            IFavoriteService favoriteService,
            IEnumerable<IAuthorChangeExecutor> authorChangeExecutors,
            ICoAuthoringService coAuthoringService)
        {
            _messageService = messageService;
            _changeAuthorHandlers = changeAuthorHandlers;
            _changeAuthorViewModelFunc = changeAuthorViewModelFunc;
            _authorManager = authorManager;
            _favoriteService = favoriteService;
            _authorChangeExecutors = authorChangeExecutors;
            _coAuthoringService = coAuthoringService;
        }

        public override bool CanExecute(IBrickExecutionContext context, string argument)
        {
            return context.EurolookDocument.IsEurolookDocument && base.CanExecute(context, argument);
        }

        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            if (_coAuthoringService.IsCoAuthoring(context.EurolookDocument.GetDocument(), DisplayName))
            {
                return;
            }

            var authorCustomXmlStoreInfos =
                _authorManager.GetAuthorCustomXmlStoreInfos(context.EurolookDocument.GetDocument()).ToList();
            var authorCustomXmlDocs = authorCustomXmlStoreInfos.Select(i => i.AuthorCustomXml).ToList();

            authorCustomXmlStoreInfos = _authorManager.HasAuthorRoles(authorCustomXmlDocs)
                ? authorCustomXmlStoreInfos.Where(i => i.AuthorCustomXml.AuthorRoleId.HasValue).ToList()
                : authorCustomXmlStoreInfos.First().CreateList();

            string authorSingularOrPlural = authorCustomXmlStoreInfos.Count > 1 ? "Authors" : "Author";
            string title = $"Change Document {authorSingularOrPlural}";

            var viewModel = _changeAuthorViewModelFunc(title);
            await viewModel.InitAsync(context.EurolookDocument, authorCustomXmlStoreInfos);
            var win = new ChangeAuthorWindow(viewModel) { Title = $"Eurolook - {title}" };

            if (_messageService.ShowDialog(win) == true)
            {
                var authorRoleViewModels = viewModel.AuthorRolesSelectionViewModel.AuthorRoleViewModels;

                _favoriteService.AddFavoritesIfNotExisting(authorRoleViewModels.Where(vm => !vm.Author.IsOriginatingFromDocumentCustomXml)
                                                                               .Select(vm => vm.Author.Author));

                var executor = _authorChangeExecutors.FirstOrDefault(e => e.CanHandle(authorCustomXmlDocs));
                using (
                    new DocumentAutomationHelper(
                        context,
                        "Change Document Author",
                        DocumentAutomationOption.Default | DocumentAutomationOption.RestoreSelection
                                                         | DocumentAutomationOption.DisableTrackRevisions))
                {
                    executor?.ChangeDocumentAuthor(context, authorRoleViewModels);

                    foreach (var changeAuthorHandler in _changeAuthorHandlers.OrderBy(h => h.ExecutionOrder))
                    {
                        await changeAuthorHandler.HandleAsync(context.EurolookDocument);
                    }
                }
            }
        }
    }
}
