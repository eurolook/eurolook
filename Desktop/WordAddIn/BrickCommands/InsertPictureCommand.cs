﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.BrickCommands.Configuration;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.DocumentMetadata;
using Eurolook.Common;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickCommands.ContentInserter;
using Eurolook.WordAddIn.BrickCommands.InsertPicture;
using Eurolook.WordAddIn.BrickEngine;
using GalaSoft.MvvmLight.CommandWpf;
using JetBrains.Annotations;
using Range = Microsoft.Office.Interop.Word.Range;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    [UsedImplicitly]
    public class InsertPictureCommand : InsertObjectCommandBase
    {
        public const string MetaDataName = "PublishingFormat";

        private readonly Func<IEurolookWordDocument, InsertPictureViewModel> _viewModelCreateFunc;
        private readonly IMetadataReaderWriter _metaDataReaderWriter;
        private readonly IUserDataRepository _userDataRepository;
        private readonly IBrickRepository _brickRepository;
        private bool _includeAlternativeText = true;
        private bool _includeTitlePlaceholder = true;
        private bool _includeSubtitlePlaceholder;
        private bool _includeCaptionPlaceholder;
        private bool _includeCopyrightPlaceholder;
        private bool _includeSourcePlaceholder = true;
        private bool _includeNotesPlaceholder;

        private bool _markAsDecorative;
        private InsertPictureConfigurationV1 _configuration;
        private UserSettings _userSettings;
        private InsertPictureSettings _brickSettings;

        public InsertPictureCommand(
            IDocumentManager documentManager,
            IMessageService messageService,
            IMetadataReaderWriter metaDataReaderWriter,
            IUserDataRepository userDataRepository,
            IBrickRepository brickRepository,
            Func<IEurolookWordDocument, InsertPictureViewModel> viewModelCreateFunc)
            : base(documentManager, messageService)
        {
            _metaDataReaderWriter = metaDataReaderWriter;
            _userDataRepository = userDataRepository;
            _brickRepository = brickRepository;
            _viewModelCreateFunc = viewModelCreateFunc;
        }

        public override IEnumerable<string> RequiredStyles
        {
            get
            {
                LoadConfiguration();

                yield return _configuration.PictureTitleStyle;
                yield return _configuration.PictureSubtitleStyle;
                yield return _configuration.PictureBodyStyle;
                yield return _configuration.PictureCaptionStyle;
                yield return _configuration.PictureSourceStyle;
                yield return _configuration.PictureCopyrightStyle;
                yield return _configuration.PictureNotesStyle;
            }
        }

        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            try
            {
                using (var viewModel = _viewModelCreateFunc(DocumentManager.GetActiveDocumentViewModel()))
                {
                    LoadConfiguration();
                    await LoadSettings();

                    viewModel.WindowTitle = "Insert Picture";
                    viewModel.ConfigureOptions(_configuration);
                    if (viewModel.ShowAutoInsertPositions || viewModel.ShowAllInsertPositions)
                    {
                        ConfigureLayoutOptions(context, viewModel);
                    }

                    ApplySettings(viewModel);
                    viewModel.AllowBrowseForFile = true;
                    viewModel.EmptyClipboardMessage = "The Clipboard does not contain a picture. "
                                                      + "Copy a picture first or click on browse to select a picture.";

                    var win = new InsertPictureView(viewModel);

                    if (MessageService.ShowDialog(win, true, true) == true)
                    {
                        _includeTitlePlaceholder = _configuration.AllowConfiguration
                            ? viewModel.IsTitleSelected
                            : _configuration.IsTitleSelected;
                        _includeSubtitlePlaceholder = _configuration.AllowConfiguration
                            ? viewModel.IsSubtitleSelected
                            : _configuration.IsSubtitleSelected;
                        _includeCaptionPlaceholder = _configuration.AllowConfiguration
                            ? viewModel.IsCaptionSelected
                            : _configuration.IsCaptionSelected;
                        _includeCopyrightPlaceholder = _configuration.AllowConfiguration
                            ? viewModel.IsCopyrightSelected
                            : _configuration.IsCopyrightLineSelected;
                        _includeSourcePlaceholder = _configuration.AllowConfiguration
                            ? viewModel.IsSourceSelected
                            : _configuration.IsSourceLineSelected;
                        _includeNotesPlaceholder = _configuration.AllowConfiguration
                            ? viewModel.IsNotesSelected
                            : _configuration.IsNotesLineSelected;
                        _includeAlternativeText = _configuration.AllowConfiguration
                            ? viewModel.ShowAlternativeText
                            : _configuration.HasAlternativeTextBox;
                        _markAsDecorative = viewModel.IsMarkAsDecorativeSelected;

                        using var documentAutomationHelper = new DocumentAutomationHelper(context, DisplayName);
                        string fileName = viewModel.FileName;
                        string altText = viewModel.AlternativeText;
                        var format = viewModel.GetPreferredDataFormat();
                        var layoutOption = viewModel.SelectedLayoutOption;
                        UpdateMaximumDimensions(viewModel, layoutOption);
                        var source = viewModel.IsInClipboardMode
                            ? PictureInserter.PictureSource.Clipboard
                            : PictureInserter.PictureSource.File;

                        if ((viewModel.IsInBrowseMode && string.IsNullOrEmpty(fileName))
                            || (viewModel.IsInClipboardMode && (format == ClipboardFormat.None)))
                        {
                            return;
                        }

                        var range = InsertPicture(
                            context.Selection.Range,
                            source,
                            fileName,
                            altText,
                            format,
                            layoutOption);

                        if (range.InlineShapes.Count > 0)
                        {
                            FixScaleTooLarge(range, range.InlineShapes[1]);
                        }

                        range.SelectFirstPlaceholderContentControl(documentAutomationHelper.PreferredViewType);

                        await SaveSettings(viewModel);
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        protected override string ResolveLabelAlias(Range range)
        {
            string titleLabel = _configuration.LabelTitle;
            string titleWithChapterLabel = _configuration.LabelTitleWithChapter;
            string titleWithAgencyChapterLabel = _configuration.LabelTitleWithAgencyChapter;

            if (IsInAgencyChapter(range))
            {
                return titleWithAgencyChapterLabel.IsNullOrEmpty() ? titleLabel : titleWithAgencyChapterLabel;
            }

            if (HasChapterNumber(range))
            {
                return titleWithChapterLabel.IsNullOrEmpty() ? titleLabel : titleWithChapterLabel;
            }

            return titleLabel;
        }

        private string ResolveCaptionLabelAlias(Range range)
        {
            string captionLabel = _configuration.LabelCaption;
            string captionWithChapterLabel = _configuration.LabelCaptionWithChapter;

            if (HasChapterNumber(range))
            {
                return captionWithChapterLabel.IsNullOrEmpty()
                    ? captionLabel
                    : captionWithChapterLabel;
            }

            return captionLabel;
        }

        private void UpdateMaximumDimensions(InsertPictureViewModel viewModel, LayoutOptionsViewModel layoutOption)
        {
            if (layoutOption == null)
            {
                return;
            }

            if (double.TryParse(viewModel.MaximumHeight, NumberStyles.Any, CultureInfo.InvariantCulture, out var val))
            {
                layoutOption.MaxHeight = UnitConverter.CmToPoints(val);
            }

            if (double.TryParse(viewModel.MaximumWidth, NumberStyles.Any, CultureInfo.InvariantCulture, out val))
            {
                layoutOption.MaxWidth = UnitConverter.CmToPoints(val);
            }
        }

        private Range InsertPicture(
            Range range,
            PictureInserter.PictureSource source,
            string fileName,
            string altText,
            ClipboardFormat format,
            LayoutOptionsViewModel layoutOption)
        {
            if (layoutOption == null || (layoutOption?.IsInlineOption() == true))
            {
                return InsertInlinePicture(range, source, fileName, altText, format);
            }

            return InsertFloatingPicture(range, source, fileName, altText, format, layoutOption);
        }

        private Range InsertInlinePicture(
            Range range,
            PictureInserter.PictureSource source,
            string fileName,
            string altText,
            ClipboardFormat format)
        {
            range = range.GetCursorBrickInsertionRange();
            range.HandleVanishingContentControl();
            range.Select();

            var itemsToInsert = CreateContentInsertersForInlinePicture(range, source, fileName, altText, format);
            itemsToInsert = itemsToInsert.Where(c => c != null).ToArray();
            itemsToInsert.OfType<ParagraphInserter>().Last().Content.First().InsertAt =
                ContentInsertionPosition.Replace;

            ContentInserterBase contentInserter = _configuration.IsInContentControl
                ? new BrickContainerInserter(Brick, itemsToInsert)
                : new RangeInserter(itemsToInsert);

            var positionProvider = new RangePositionProvider(range);
            return contentInserter.Insert(positionProvider);
        }

        private Range InsertFloatingPicture(
            Range range,
            PictureInserter.PictureSource source,
            string fileName,
            string altText,
            ClipboardFormat format,
            LayoutOptionsViewModel layoutOption)
        {
            //range = range.GetCursorBrickInsertionRange();
            //range.HandleVanishingContentControl();
            var contentInserter = new RangeInserter(
                CreateContentInsertersForFloatingPicture(range, source, fileName, altText, format, layoutOption));
            var positionProvider = new RangePositionProvider(range);
            return contentInserter.Insert(positionProvider);
        }

        private IContentInserter[] CreateContentInsertersForInlinePicture(
            Range range,
            PictureInserter.PictureSource source,
            string fileName,
            string altText,
            ClipboardFormat format)
        {
            string titleStyle =
                StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(_configuration.PictureTitleStyle, range);
            string subtitleStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(
                _configuration.PictureSubtitleStyle,
                range);
            string bodyStyle =
                StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(_configuration.PictureBodyStyle, range);
            string captionStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(
                _configuration.PictureCaptionStyle,
                range);
            string copyrightStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(
                _configuration.PictureCopyrightStyle,
                range);
            string sourceStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(
                _configuration.PictureSourceStyle,
                range);
            string notesStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(
                _configuration.PictureNotesStyle,
                range);
            string altTextStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(
                _configuration.PictureAltTextStyle,
                range);

            var titlePlaceholderInserter = _includeTitlePlaceholder
                ? new ParagraphInserter(
                    titleStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                        LabelAlias = ResolveLabelAlias(range),
                    },
                    new PlaceholderInserter("Type your title here."))
                : null;

            var subtitlePlaceholderInserter = _includeSubtitlePlaceholder
                ? new ParagraphInserter(
                    subtitleStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                    },
                    new PlaceholderInserter("Type your subtitle here."))
                : null;

            var pictureInserter = new ParagraphInserter(
                bodyStyle,
                new PictureInserter
                {
                    FileName = fileName,
                    Format = format,
                    Source = source,
                    AlternativeText =
                        _configuration.HasAlternativeTextBox && _includeAlternativeText ? string.Empty : altText,
                    MarkAsDecorative = _markAsDecorative,
                    LockAspectRatio = true,
                });

            var alternativeTextInserter =
                _configuration.HasAlternativeTextBox && _includeAlternativeText && !_markAsDecorative
                    ? new ParagraphInserter(
                        altTextStyle,
                        new LabelInserter
                        {
                            LocalisedResourceResolver = LocalisedResourceResolver,
                            LabelAlias = _configuration.LabelAlternativeText,
                            IsItalic = _configuration.HasItalicSourceLabel,
                        },
                        altText.IsNullOrEmpty()
                            ? new PlaceholderInserter(
                                "Type your alternative text here.")
                            : new TextInserter
                            {
                                Text = altText,
                                IsItalic = false,
                            })
                    : null;

            var captionPlaceholderInserter = _includeCaptionPlaceholder
                ? new ParagraphInserter(
                    captionStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                        LabelAlias = ResolveCaptionLabelAlias(range),
                    },
                    new PlaceholderInserter("Type your caption here.", false))
                : null;

            var copyrightPlaceholderInserter = _includeCopyrightPlaceholder
                ? new ParagraphInserter(
                    copyrightStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                    },
                    new TextInserter
                    {
                        Text = "© ",
                    },
                    new PlaceholderInserter("Type your Copyrights information here."))
                : null;

            var sourcePlaceholderInserter = _includeSourcePlaceholder
                ? new ParagraphInserter(
                    sourceStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                        LabelAlias = _configuration.LabelSource,
                        IsItalic = _configuration.HasItalicSourceLabel,
                    },
                    new TextInserter
                    {
                        Text = ":",
                        IsItalic = _configuration.HasItalicSourceLabel,
                    },
                    new TextInserter { Text = " " },
                    new PlaceholderInserter("Type your source here."))
                : null;

            var notesPlaceholderInserter = _includeNotesPlaceholder
                ? new ParagraphInserter(
                    notesStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                    },
                    new PlaceholderInserter("Type your notes here."))
                : null;

            // EUROLOOK-3566: if the brick is inserted in the last paragraph of a story outside of a content control, the source line breaks
            var paragraphAfterInserter = !_configuration.IsInContentControl && range.IsLastParagraphInStory()
                ? new ParagraphInserter(
                    range.Paragraphs.Last.GetStyleNameLocal(),
                    new TextInserter { Text = string.Empty })
                : null;

            return new IContentInserter[]
            {
                titlePlaceholderInserter,
                subtitlePlaceholderInserter,
                pictureInserter,
                alternativeTextInserter,
                copyrightPlaceholderInserter,
                sourcePlaceholderInserter,
                captionPlaceholderInserter,
                notesPlaceholderInserter,
                paragraphAfterInserter,
            }.Where(c => c != null).ToArray();
        }

        private IContentInserter[] CreateContentInsertersForFloatingPicture(
            Range range,
            PictureInserter.PictureSource source,
            string fileName,
            string altText,
            ClipboardFormat format,
            LayoutOptionsViewModel layoutOption)
        {
            string titleStyle =
                StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(_configuration.PictureTitleStyle, range);
            string subtitleStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(
                _configuration.PictureSubtitleStyle,
                range);
            string bodyStyle =
                StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(_configuration.PictureBodyStyle, range);
            string captionStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(
                _configuration.PictureCaptionStyle,
                range);
            string copyrightStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(
                _configuration.PictureCopyrightStyle,
                range);
            string sourceStyle =
                StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(_configuration.PictureSourceStyle, range);
            string notesStyle =
                StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(_configuration.PictureNotesStyle, range);
            string altTextStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(
                _configuration.PictureAltTextStyle,
                range);

            var alternativeTextInserter =
                _configuration.HasAlternativeTextBox && _includeAlternativeText && !_markAsDecorative
                    ? new ParagraphInserter(
                        altTextStyle,
                        new LabelInserter
                        {
                            LocalisedResourceResolver = LocalisedResourceResolver,
                            LabelAlias = _configuration.LabelAlternativeText,
                            IsItalic = _configuration.HasItalicSourceLabel,
                        },
                        altText.IsNullOrEmpty()
                            ? new PlaceholderInserter(
                                "Type your alternative text here.")
                            : new TextInserter
                            {
                                Text = altText,
                                IsItalic = false,
                            })
                    : null;

            var captionPlaceholderInserter = _includeCaptionPlaceholder
                ? new ParagraphInserter(
                    captionStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                        LabelAlias = ResolveCaptionLabelAlias(range),
                    },
                    new PlaceholderInserter("Type your caption here.", false))
                : null;

            var copyrightPlaceholderInserter = _includeCopyrightPlaceholder
                ? new ParagraphInserter(
                    copyrightStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                    },
                    new TextInserter
                    {
                        Text = "© ",
                    },
                    new PlaceholderInserter("Type your Copyrights information here."))
                : null;

            var sourcePlaceholderInserter = _includeSourcePlaceholder
                ? new ParagraphInserter(
                    sourceStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                        LabelAlias = _configuration.LabelSource,
                        IsItalic = _configuration.HasItalicSourceLabel,
                    },
                    new TextInserter
                    {
                        Text = ":",
                        IsItalic = _configuration.HasItalicSourceLabel,
                    },
                    new TextInserter { Text = " " },
                    new PlaceholderInserter("Type your source here."))
                : null;

            var notesPlaceholderInserter = _includeNotesPlaceholder
                ? new ParagraphInserter(
                    notesStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                    },
                    new PlaceholderInserter("Type your notes here."))
                : null;

            var pictureInserter = new PictureWithEditableTextboxInserter
            {
                FileName = fileName,
                Format = format,
                LayoutSettings = layoutOption,
                Source = source,
                AlternativeText =
                    _configuration.HasAlternativeTextBox && _includeAlternativeText ? string.Empty : altText,
                MarkAsDecorative = _markAsDecorative,
                LockAspectRatio = layoutOption.KeepAspectRatio,
                TextBoxContentInserters = new()
                {
                    alternativeTextInserter,
                    copyrightPlaceholderInserter,
                    sourcePlaceholderInserter,
                    captionPlaceholderInserter,
                    notesPlaceholderInserter,
                },
            };

            var paragraphAfterInserter = !_configuration.IsInContentControl && range.IsLastParagraphInStory()
                ? new ParagraphInserter(
                    range.Paragraphs.Last.GetStyleNameLocal(),
                    new TextInserter { Text = string.Empty })
                : null;

            return new IContentInserter[] { pictureInserter, paragraphAfterInserter }.Where(c => c != null).ToArray();
        }

        private void ApplySettings(InsertPictureViewModel viewModel)
        {
            if (!_configuration.SettingsSaved)
            {
                return;
            }

            viewModel.SettingsSaved = true;
            viewModel.RestoreDefaultSettingsCommand = new RelayCommand<InsertPictureViewModel>(RestoreSettings);

            if (_configuration.AllowConfiguration)
            {
                viewModel.IsTitleSelected = _brickSettings.AddPictureTitle ?? viewModel.IsTitleSelected;
                viewModel.IsSubtitleSelected = _brickSettings.AddPictureSubtitle ?? viewModel.IsSubtitleSelected;
                viewModel.IsCopyrightSelected = _brickSettings.AddCopyrightLine ?? viewModel.IsCopyrightSelected;
                viewModel.IsSourceSelected = _brickSettings.AddSourceLine ?? viewModel.IsSourceSelected;
                viewModel.IsCaptionSelected = _brickSettings.AddCaptionLine ?? viewModel.IsCaptionSelected;
                viewModel.IsNotesSelected = _brickSettings.AddNotesLine ?? viewModel.IsNotesSelected;
            }

            if (!string.IsNullOrEmpty(_brickSettings.LayoutOptionName) && viewModel.ShowInsertPositionsContentOptions)
            {
                viewModel.SelectedLayoutOption =
                    viewModel.LayoutOptions.FirstOrDefault(o => o.Name.Equals(_brickSettings.LayoutOptionName))
                    ?? viewModel.SelectedLayoutOption;
            }

            viewModel.AnchoredToParagraphLastMaxHeight = _brickSettings.MaximumHeight ?? viewModel.AnchoredToParagraphLastMaxHeight;
            viewModel.AnchoredToParagraphLastMaxWidth = _brickSettings.MaximumWidth ?? viewModel.AnchoredToParagraphLastMaxWidth;
            if (_brickSettings.MaximumHeight != null && viewModel.SelectedLayoutOption?.IsAnchoredToParagraphOption() == true)
            {
                viewModel.MaximumHeight = _brickSettings.MaximumHeight;
            }

            if (_brickSettings.MaximumWidth != null && viewModel.SelectedLayoutOption?.IsAnchoredToParagraphOption() == true)
            {
                viewModel.MaximumWidth = _brickSettings.MaximumWidth;
            }
        }

        private void RestoreSettings(InsertPictureViewModel viewModel)
        {
            viewModel.ConfigureOptions(_configuration);
            viewModel.ResetAnchoredToParagraphDimensions();
            viewModel.IsMarkAsDecorativeSelected = _configuration.IsMarkAsDecorativeSelected;

            if (viewModel.LayoutOptions.Any())
            {
                viewModel.SelectedLayoutOption = viewModel.LayoutOptions.First();
            }
        }

        private async Task SaveSettings(InsertPictureViewModel viewModel)
        {
            if (!_configuration.SettingsSaved)
            {
                return;
            }

            if (_configuration.AllowConfiguration)
            {
                _brickSettings.AddPictureTitle = viewModel.ShowTitleCheckbox ? viewModel.IsTitleSelected : _brickSettings.AddPictureTitle;
                _brickSettings.AddPictureSubtitle = viewModel.ShowSubtitleCheckbox ? viewModel.IsSubtitleSelected : _brickSettings.AddPictureSubtitle;
                _brickSettings.AddCopyrightLine = viewModel.HasCopyrightLine ? viewModel.IsCopyrightSelected : _brickSettings.AddCopyrightLine;
                _brickSettings.AddSourceLine = viewModel.HasSourceLine ? viewModel.IsSourceSelected : _brickSettings.AddSourceLine;
                _brickSettings.AddCaptionLine = viewModel.HasCaptionLine ? viewModel.IsCaptionSelected : _brickSettings.AddCaptionLine;
                _brickSettings.AddNotesLine = viewModel.HasNotesLine ? viewModel.IsNotesSelected : _brickSettings.AddNotesLine;
            }

            if (viewModel.ShowAnchoredToParagraphDimensions)
            {
                _brickSettings.MaximumHeight = viewModel.MaximumHeight ?? _brickSettings.MaximumHeight;
                _brickSettings.MaximumWidth = viewModel.MaximumWidth ?? _brickSettings.MaximumWidth;
            }

            if (viewModel.ShowInsertPositionsContentOptions)
            {
                _brickSettings.LayoutOptionName = viewModel.SelectedLayoutOption?.Name;
            }

            await _brickRepository.SaveBrickSettings(_userSettings.Id, Brick.Id, _brickSettings);
        }

        private async Task LoadSettings()
        {
            _userSettings = await _userDataRepository.GetUserSettingsAsync();
            _brickSettings =
                await _brickRepository.GetBrickSettingsAsync<InsertPictureSettings>(
                    _userSettings.Id,
                    Brick.Id)
                ?? new InsertPictureSettings();
        }

        private void LoadConfiguration()
        {
            if (_configuration == null)
            {
                var brickConfigurationReader = new BrickConfigurationReader();
                _configuration = brickConfigurationReader.Read<InsertPictureConfigurationV1>(Brick.Configuration)
                                 ?? new InsertPictureConfigurationV1();
            }
        }

        private void ConfigureLayoutOptions(IBrickExecutionContext context, InsertPictureViewModel viewModel)
        {
            _metaDataReaderWriter.Init(context.EurolookDocument);
            var metaDataDefinition = _metaDataReaderWriter.GetMetadataDefinition(MetaDataName);
            string metaDataValue = ""; // Interpret as Draft when Metadata does not exist
            if (metaDataDefinition != null)
            {
                metaDataValue = _metaDataReaderWriter.ReadMetadataProperty(metaDataDefinition)?.GetSimpleValue();
            }

            viewModel.ConfigureLayoutOptions(metaDataValue, _configuration.LayoutOptionsSettings?.ToList());
        }
    }
}
