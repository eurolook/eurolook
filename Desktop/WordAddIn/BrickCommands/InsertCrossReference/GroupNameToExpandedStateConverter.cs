using System;
using System.Globalization;
using System.Windows.Data;

namespace Eurolook.WordAddIn.BrickCommands.InsertCrossReference
{
    public class GroupNameToExpandedStateConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var group = (CollectionViewGroup)values[0];
            string userPreferredReferenceType = (string)values[1];

            return (string)group.Name == userPreferredReferenceType;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
