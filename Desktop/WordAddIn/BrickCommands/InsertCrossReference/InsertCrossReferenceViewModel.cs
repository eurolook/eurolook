﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing.Extensions;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands.InsertCrossReference
{
    public class InsertCrossReferenceViewModel : BrickCommandPanelViewModelBase
    {
        private static readonly object StaticLock = new object();
        private static string _preferredCrossReferenceType = "Heading";
        private static bool _userPreferredIncludePosition;
        private readonly IWordApplicationContext _applicationContext;
        private CrossReferenceViewModel _selectedCrossReference;
        private CrossReferenceKindViewModel _selectedReferenceKind;

        public InsertCrossReferenceViewModel(IEurolookWordDocument eurolookDocument, IWordApplicationContext applicationContext)
            : base(eurolookDocument)
        {
            _applicationContext = applicationContext;
            IncludePosition = UserPreferredIncludePosition;

            CrossReferences = new NotifyTask<ObservableCollectionEx<CrossReferenceViewModel>>()
                              .NotifyOnCompletion(this)
                              .Start(() => Task.Run(() => Initialize()));
        }

        public override string Title
        {
            get { return "Insert Cross Reference"; }
        }

        [NotNull]
        public NotifyTask<ObservableCollectionEx<CrossReferenceViewModel>> CrossReferences { get; }

        public bool HasItems
        {
            get { return CrossReferences.IsCompleted && CrossReferences.Result.Any(); }
        }

        public bool IsAutomaticallyRefreshing { get; set; }

        public CrossReferenceViewModel SelectedCrossReference
        {
            get { return _selectedCrossReference; }
            set
            {
                if (_selectedCrossReference == value || value == null)
                {
                    return;
                }

                _selectedCrossReference = value;

                Execute.OnUiThread(
                    () =>
                    {
                        AvailableReferenceKinds.ReplaceRange(
                            _selectedCrossReference?.AvailableReferenceKinds
                            ?? Enumerable.Empty<CrossReferenceKindViewModel>());
                    });

                string selectedCrossReferenceKindDisplayName =
                    _selectedCrossReference?.PreferredReferenceKind?.DisplayName;

                SelectedReferenceKind =
                    AvailableReferenceKinds.FirstOrDefault(
                        k => selectedCrossReferenceKindDisplayName != null
                             && k?.DisplayName == selectedCrossReferenceKindDisplayName)
                    ?? AvailableReferenceKinds.FirstOrDefault();

                RaisePropertyChanged(() => SelectedCrossReference);
                RaisePropertyChanged(() => CanSelectReferenceKind);
                RaisePropertyChanged(() => CanIncludePosition);
                CommitCommand.RaiseCanExecuteChanged();
            }
        }

        public ObservableCollectionEx<CrossReferenceKindViewModel> AvailableReferenceKinds { get; } =
            new ObservableCollectionEx<CrossReferenceKindViewModel>();

        public bool CanSelectReferenceKind
        {
            get
            {
                return SelectedCrossReference != null && AvailableReferenceKinds != null
                                                      && AvailableReferenceKinds.Any();
            }
        }

        public CrossReferenceKindViewModel SelectedReferenceKind
        {
            get { return _selectedReferenceKind; }
            set
            {
                Set(() => SelectedReferenceKind, ref _selectedReferenceKind, value);
                if (SelectedCrossReference != null && value != null)
                {
                    SelectedCrossReference.PreferredCrossReferenceKind = value;
                }

                RaisePropertyChanged(() => CanIncludePosition);
            }
        }

        public bool CanIncludePosition
        {
            get
            {
                return CanSelectReferenceKind
                       && SelectedReferenceKind != null
                       && !SelectedReferenceKind.ReferenceKind.Contains(WdReferenceKind.wdPosition)
                       && !SelectedReferenceKind.ReferenceKind.Contains(WdReferenceKind.wdContentText)
                       && !SelectedReferenceKind.ReferenceKind.Contains(WdReferenceKind.wdEntireCaption)
                       && !SelectedReferenceKind.ReferenceKind.Contains(WdReferenceKind.wdOnlyCaptionText)
                       && !SelectedReferenceKind.ReferenceKind.Contains(WdReferenceKind.wdOnlyLabelAndNumber)
                       && !SelectedReferenceKind.ReferenceKind.Contains(WdReferenceKind.wdPageNumber);
            }
        }

        public bool IncludePosition { get; set; }

        public string PreferredCrossReferenceType
        {
            get { return _preferredCrossReferenceType; }
            set
            {
                lock (StaticLock)
                {
#pragma warning disable S2696 // Instance members should not write to "static" fields
                    // NOTE: We explicitly use a static field to remember the global user preference,
                    // but we need an instance property to enable binding to XAML.
                    _preferredCrossReferenceType = value;
#pragma warning restore S2696 // Instance members should not write to "static" fields
                }
            }
        }

        public bool UserPreferredIncludePosition
        {
            get { return _userPreferredIncludePosition; }
            set
            {
                lock (StaticLock)
                {
#pragma warning disable S2696 // Instance members should not write to "static" fields
                    // NOTE: We explicitly use a static field to remember the global user preference.
                    _userPreferredIncludePosition = value;
#pragma warning restore S2696 // Instance members should not write to "static" fields
                }
            }
        }

        public override bool CanCommit()
        {
            return SelectedCrossReference != null;
        }

        public override void Commit()
        {
            PreferredCrossReferenceType = SelectedCrossReference?.ReferenceTypeDisplayName;
            UserPreferredIncludePosition = IncludePosition;
            base.Commit();
        }

        public ObservableCollectionEx<CrossReferenceViewModel> Initialize()
        {
            var crossReferenceItems = GetCrossReferenceItems();
            var result = new ObservableCollectionEx<CrossReferenceViewModel>(crossReferenceItems);
            SelectedCrossReference = GetSelectedCrossReference(result, null);
            return result;
        }

        public void OnOpening()
        {
            WireEvents();
            IsAutomaticallyRefreshing = true;
            RefreshCrossReferences();
        }

        protected override void OnClosing()
        {
            UnwireEvents();
        }

        [CanBeNull]
        private static string NormalizeText(string input)
        {
            return input?.Replace('\u00A0', ' ').Replace('\v', ' ');
        }

        private void WireEvents()
        {
            _applicationContext.InputDetector.InputDetectedEvent += OnInputDetectedDelayed;
        }

        private void UnwireEvents()
        {
            _applicationContext.InputDetector.InputDetectedEvent -= OnInputDetectedDelayed;
        }

        private CrossReferenceViewModel GetSelectedCrossReference(
            IReadOnlyCollection<CrossReferenceViewModel> currentCrossReferences,
            CrossReferenceViewModel preferredItem)
        {
            string preferredReferenceType = preferredItem?.ReferenceTypeDisplayName ?? PreferredCrossReferenceType;
            var result = currentCrossReferences.FirstOrDefault(
                             r => r.ReferenceTypeDisplayName.Equals(preferredReferenceType)
                                  && r.ReferenceItem.Equals(preferredItem?.ReferenceItem)
                                  && r.Text.Equals(preferredItem?.Text))
                         ?? currentCrossReferences.FirstOrDefault(
                             r => r.ReferenceTypeDisplayName.Equals(preferredReferenceType)
                                  && r.ReferenceItem.Equals(preferredItem?.ReferenceItem))
                         ?? currentCrossReferences.FirstOrDefault(
                             r => r.ReferenceTypeDisplayName.Equals(preferredReferenceType)
                                  && r.Text.Equals(preferredItem?.Text))
                         ?? currentCrossReferences.FirstOrDefault(
                             r => r.ReferenceTypeDisplayName.Equals(preferredReferenceType)
                                  && r.Text.StartsWith(preferredItem?.Text ?? ""))
                         ?? currentCrossReferences.FirstOrDefault(
                             r => r.ReferenceTypeDisplayName.Equals(preferredReferenceType)
                                  && preferredItem?.Text.StartsWith(r.Text) == true)
                         ?? currentCrossReferences.FirstOrDefault(
                             r => r.ReferenceTypeDisplayName.Equals(preferredReferenceType))
                         ?? currentCrossReferences.FirstOrDefault();
            return result;
        }

        private void RefreshCrossReferences()
        {
            try
            {
                if (CrossReferences.Status != TaskStatus.RanToCompletion)
                {
                    return;
                }

                var currentCrossReferences = CrossReferences.Result;

                if (currentCrossReferences == null)
                {
                    return;
                }

                var result = GetCrossReferenceItems();

                if (!currentCrossReferences.SequenceEqual(result, new CrossReferenceViewModelEqualityComparer()))
                {
                    var selectedItem = SelectedCrossReference;

                    PreferredCrossReferenceType =
                        result.FirstOrDefault(r => r.ReferenceType.Equals(selectedItem?.ReferenceType))
                              ?.ReferenceTypeDisplayName
                        ?? result.FirstOrDefault(r => r.ReferenceTypeDisplayName == PreferredCrossReferenceType)
                                 ?.ReferenceTypeDisplayName
                        ?? result.FirstOrDefault(r => r.ReferenceType.Equals(WdReferenceType.wdRefTypeHeading))
                                 ?.ReferenceTypeDisplayName
                        ?? result.FirstOrDefault()?.ReferenceTypeDisplayName
                        ?? "Heading";

                    Execute.OnUiThread(() => { currentCrossReferences.ReplaceRange(result); });

                    SelectedCrossReference = GetSelectedCrossReference(currentCrossReferences, selectedItem);
                    RaisePropertyChanged(() => HasItems);
                }
            }
            catch (Exception ex)
            {
                this.LogWarn(ex);
            }
        }

        private List<CrossReferenceViewModel> GetCrossReferenceItems()
        {
            var result = new List<CrossReferenceViewModel>();

            if (EurolookDocument == null)
            {
                return result;
            }

            foreach (var itemType in Enum.GetValues(typeof(WdReferenceType)).OfType<WdReferenceType>())
            {
                var items = GetCrossReferenceItems(itemType);
                result.AddRange(
                    items.Select(
                        (text, i) => CrossReferenceViewModel.Create(
                            itemType,
                            i + 1,
                            text,
                            EurolookDocument.Document)));
            }

            var sequenceIdentifiers = GetSequenceIdentifiers();

            // first add CaptionLabels for any SEQ fields
            ////var captionLabels = _applicationContext.Application.CaptionLabels.OfType<CaptionLabel>().Select(cl => cl.Name).ToList();
            ////var labels = sequenceIdentifiers.Except(captionLabels).Distinct().ToList();
            ////foreach (string label in labels)
            ////{
            ////    _applicationContext.Application.CaptionLabels.Add(label);
            ////}

            var range = EurolookDocument.Document.Range();
            range.TextRetrievalMode.IncludeFieldCodes = false;
            string documentText = NormalizeText(range.Text);

            foreach (string captionLabel in sequenceIdentifiers)
            {
                var items = GetCrossReferenceItems(captionLabel);

                var sanitizedItems = GetSanitizedItemText(items, documentText);

                result.AddRange(
                    sanitizedItems.Select(
                        (text, i) => CrossReferenceViewModel.Create(
                            captionLabel,
                            i + 1,
                            text,
                            EurolookDocument.Document)));
            }

            return result;
        }

        /// <summary>
        /// Gets the names of all the automatically numbered sequences defined by SEQ fields.
        /// </summary>
        /// <returns>An enumerable of the identifiers of the SEQ fields in a Word document.</returns>
        private IEnumerable<string> GetSequenceIdentifiers()
        {
            var range = EurolookDocument.Document.Range();
            var sequenceIdentifiers = new HashSet<string>();

            int fieldCount = range.Fields.Count;
            for (int i = 1; i <= fieldCount; i++)
            {
                var field = range.Fields[i];
                var fieldType = field.Type;
                if (fieldType == WdFieldType.wdFieldTOC)
                {
                    // skip TOC fields because they may contain a large number of nested fields
                    range.Start = field.Result.End + 1;
                    i = 1;
                    fieldCount = range.Fields.Count;
                }
                else if (fieldType == WdFieldType.wdFieldSequence)
                {
                    string identifier = field.GetSequenceIdentifier();
                    if (!string.IsNullOrWhiteSpace(identifier))
                    {
                        sequenceIdentifiers.Add(identifier);
                    }
                }
            }

            return sequenceIdentifiers;
        }

        private IEnumerable<string> GetSanitizedItemText(IEnumerable<string> items, string documentText)
        {
            foreach (string item in items)
            {
                // NOTE: Content control boundaries appear as brackets in cross reference items
                // We remove these brackets if not found in the document.
                string escapedString = Regex.Escape(item).Replace(">", ">?").Replace("<", "<?");

                var match = Regex.Match(documentText, escapedString);
                if (match.Success)
                {
                    yield return match.Value;
                }
                else
                {
                    yield return item;
                }
            }
        }

        private void OnInputDetectedDelayed(object sender, EventArgs e)
        {
            try
            {
                if (!IsAutomaticallyRefreshing)
                {
                    return;
                }

                if (!EurolookDocument.IsActive)
                {
                    return;
                }

                RefreshCrossReferences();
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private IEnumerable<string> GetCrossReferenceItems(object itemType)
        {
            try
            {
                // NOTE: In .NET a string[] is a vector-based array. GetCrossReferenceItems returns a string[*] which we need to treat as an Array:
                var items = (Array)(object)EurolookDocument.Document.GetCrossReferenceItems(itemType);
                return items.OfType<string>();
            }
            catch (COMException)
            {
                return Enumerable.Empty<string>();
            }
        }
    }

    public class CrossReferenceViewModelEqualityComparer : IEqualityComparer<CrossReferenceViewModel>
    {
        public bool Equals(CrossReferenceViewModel x, CrossReferenceViewModel y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
            {
                return false;
            }

            return x.ReferenceType.Equals(y.ReferenceType)
                   && x.ReferenceItem.Equals(y.ReferenceItem)
                   && x.Text.Equals(y.Text);
        }

        public int GetHashCode(CrossReferenceViewModel obj)
        {
            return obj.GetHashCode();
        }
    }
}
