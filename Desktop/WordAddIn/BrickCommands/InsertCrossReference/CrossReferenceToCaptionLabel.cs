using System.Collections.Generic;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.BrickCommands.InsertCrossReference
{
    public class CrossReferenceToCaptionLabel : CrossReferenceViewModel
    {
        private static readonly CrossReferenceKindViewModel CrossReferenceKindEntireCaption =
            new CrossReferenceKindViewModel("Entire caption", WdReferenceKind.wdEntireCaption);

        private static readonly CrossReferenceKindViewModel CrossReferenceKindOnlyLabelAndNumber =
            new CrossReferenceKindViewModel("Only label and number", WdReferenceKind.wdOnlyLabelAndNumber);

        private static readonly CrossReferenceKindViewModel CrossReferenceKindOnlyCaption =
            new CrossReferenceKindViewModel("Only caption", WdReferenceKind.wdOnlyCaptionText);

        private static readonly List<CrossReferenceKindViewModel> CrossReferenceKinds =
            new List<CrossReferenceKindViewModel>
            {
                CrossReferenceKindEntireCaption,
                CrossReferenceKindOnlyLabelAndNumber,
                CrossReferenceKindOnlyCaption,
                CrossReferenceKindPageNumber,
                CrossReferenceKindAboveBelow,
            };

        private static CrossReferenceKindViewModel _preferredCrossReferenceKind = CrossReferenceKindOnlyLabelAndNumber;

        public CrossReferenceToCaptionLabel(string captionLabel, int referenceItem, string text, Document document)
            : base(captionLabel, referenceItem, text, document)
        {
        }

        public override List<CrossReferenceKindViewModel> AvailableReferenceKinds
        {
            get { return CrossReferenceKinds; }
        }

        public override CrossReferenceKindViewModel PreferredCrossReferenceKind
        {
            get { return _preferredCrossReferenceKind; }
            set { _preferredCrossReferenceKind = value; }
        }
    }
}
