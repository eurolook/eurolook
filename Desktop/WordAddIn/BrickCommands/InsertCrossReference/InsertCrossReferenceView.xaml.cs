using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Eurolook.WordAddIn.Messages;
using GalaSoft.MvvmLight.Messaging;

namespace Eurolook.WordAddIn.BrickCommands.InsertCrossReference
{
    public partial class InsertCrossReferenceView
    {
        public InsertCrossReferenceView()
        {
            InitializeComponent();
            Messenger.Default.Register<ShowCommandBrickPanelMessage>(this, OnShowCommandBrickPanel);

            void Handler(object sender, RoutedEventArgs args)
            {
                SetInitialFocus();
                CrossReferencesList.Loaded -= Handler;
            }

            CrossReferencesList.Loaded += Handler;
        }

        private void OnShowCommandBrickPanel(ShowCommandBrickPanelMessage obj)
        {
            if (CrossReferencesList.IsLoaded)
            {
                SetInitialFocus();
            }
        }

        private void SetInitialFocus()
        {
            CrossReferencesList.Focus();
        }

        private void OnReferenceKindComboBoxLoaded(object sender, RoutedEventArgs e)
        {
            // FIX: We need to change the popup direction because items that appear in the popup part outside the Word window
            // cannot be selected via mouse click:
            // https://connect.microsoft.com/VisualStudio/feedback/details/769994/mouse-capture-problem-with-wpf-usercontrol-in-office-addin-taskpane-window
            var controlTemplate = ReferenceKindComboBox.Template;
            var popup = controlTemplate.FindName("PART_Popup", ReferenceKindComboBox) as Popup
                        ?? controlTemplate.FindName("Popup", ReferenceKindComboBox) as Popup;
            if (popup != null)
            {
                popup.Placement = PlacementMode.Top;
            }
        }

        private void OnPreviewGotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            StopWatchingForDocumentChanges();
        }

        private void OnLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            var newFocusElement = e.NewFocus;
            if (newFocusElement == null)
            {
                // focus is no longer in user control; start automatically refreshing
                // the list of cross reference items again
                StartWatchingForDocumentChanges();
            }
        }

        private void StartWatchingForDocumentChanges()
        {
            if (DataContext is InsertCrossReferenceViewModel viewModel)
            {
                viewModel.IsAutomaticallyRefreshing = true;
            }
        }

        private void StopWatchingForDocumentChanges()
        {
            if (DataContext is InsertCrossReferenceViewModel viewModel)
            {
                viewModel.IsAutomaticallyRefreshing = false;
            }
        }
    }
}
