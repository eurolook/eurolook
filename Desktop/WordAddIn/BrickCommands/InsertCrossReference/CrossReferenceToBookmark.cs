using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.BrickCommands.InsertCrossReference
{
    public class CrossReferenceToBookmark : CrossReferenceViewModel
    {
        private static readonly List<CrossReferenceKindViewModel> CrossReferenceKinds =
            new List<CrossReferenceKindViewModel>
            {
                CrossReferenceKindText,
                CrossReferenceKindPageNumber,
                CrossReferenceKindParagraphNumber,
                CrossReferenceKindAboveBelow,
            };

        private static CrossReferenceKindViewModel _preferredCrossReferenceKind = CrossReferenceKindText;

        public CrossReferenceToBookmark(string text, Document document)
            : base(WdReferenceType.wdRefTypeBookmark, text, text, document)
        {
        }

        public bool CanInsertAsText
        {
            get
            {
                try
                {
                    return !string.IsNullOrEmpty(Document.Bookmarks[Text].Range.Text?.Trim());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public bool CanInsertAsParagraphNumber
        {
            get
            {
                try
                {
                    return !string.IsNullOrWhiteSpace(Document.Bookmarks[Text].Range.ListFormat.ListString);
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public override List<CrossReferenceKindViewModel> AvailableReferenceKinds
        {
            get
            {
                var result = CrossReferenceKinds.ToList();
                if (!CanInsertAsText)
                {
                    result.RemoveAll(k => k.ReferenceKind.Contains(WdReferenceKind.wdContentText));
                }

                if (!CanInsertAsParagraphNumber)
                {
                    result.RemoveAll(k => k.ReferenceKind.Contains(WdReferenceKind.wdNumberFullContext));
                    result.RemoveAll(k => k.ReferenceKind.Contains(WdReferenceKind.wdNumberNoContext));
                    result.RemoveAll(k => k.ReferenceKind.Contains(WdReferenceKind.wdNumberRelativeContext));
                }

                return result;
            }
        }

        public override CrossReferenceKindViewModel PreferredCrossReferenceKind
        {
            get { return _preferredCrossReferenceKind; }
            set { _preferredCrossReferenceKind = value; }
        }
    }
}
