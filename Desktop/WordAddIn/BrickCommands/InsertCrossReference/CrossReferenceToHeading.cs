using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.BrickCommands.InsertCrossReference
{
    public class CrossReferenceToHeading : CrossReferenceViewModel
    {
        private static readonly List<CrossReferenceKindViewModel> CrossReferenceKinds =
            new List<CrossReferenceKindViewModel>
            {
                CrossReferenceKindEntireTextWithNumber,
                CrossReferenceKindTextOnly,
                CrossReferenceKindPageNumber,
                CrossReferenceKindParagraphNumber,
                CrossReferenceKindAboveBelow,
            };

        private static CrossReferenceKindViewModel
            _preferredCrossReferenceKind = CrossReferenceKindEntireTextWithNumber;

        public CrossReferenceToHeading(WdReferenceType referenceType, int referenceItem, string text, Document document)
            : base(referenceType, referenceItem, text, document)
        {
        }

        public override List<CrossReferenceKindViewModel> AvailableReferenceKinds
        {
            get
            {
                var result = CrossReferenceKinds.ToList();
                if (!IsHeadingNumbered)
                {
                    result.Remove(CrossReferenceKindEntireTextWithNumber);
                    result.Remove(CrossReferenceKindParagraphNumber);
                }

                return result;
            }
        }

        public override CrossReferenceKindViewModel PreferredCrossReferenceKind
        {
            get => _preferredCrossReferenceKind;
            set => _preferredCrossReferenceKind = value;
        }

        private bool IsHeadingNumbered
        {
            get
            {
                try
                {
                    return Document.Styles[WdBuiltinStyle.wdStyleHeading1].ListLevelNumber > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
    }
}
