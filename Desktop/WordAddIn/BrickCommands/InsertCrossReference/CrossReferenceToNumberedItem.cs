using System.Collections.Generic;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.BrickCommands.InsertCrossReference
{
    public class CrossReferenceToNumberedItem : CrossReferenceViewModel
    {
        private static readonly List<CrossReferenceKindViewModel> CrossReferenceKinds =
            new List<CrossReferenceKindViewModel>
            {
                CrossReferenceKindEntireTextWithNumber,
                CrossReferenceKindTextOnly,
                CrossReferenceKindPageNumber,
                CrossReferenceKindParagraphNumber,
                CrossReferenceKindAboveBelow,
            };

        private static CrossReferenceKindViewModel _preferredCrossReferenceKind = CrossReferenceKindParagraphNumber;

        public CrossReferenceToNumberedItem(
            WdReferenceType referenceType,
            int referenceItem,
            string text,
            Document document)
            : base(referenceType, referenceItem, text, document)
        {
        }

        public override List<CrossReferenceKindViewModel> AvailableReferenceKinds
        {
            get { return CrossReferenceKinds; }
        }

        public override CrossReferenceKindViewModel PreferredCrossReferenceKind
        {
            get { return _preferredCrossReferenceKind; }
            set { _preferredCrossReferenceKind = value; }
        }
    }
}
