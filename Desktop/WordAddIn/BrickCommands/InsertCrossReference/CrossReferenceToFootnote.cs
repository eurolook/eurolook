using System.Collections.Generic;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.BrickCommands.InsertCrossReference
{
    public class CrossReferenceToFootnote : CrossReferenceViewModel
    {
        private static readonly CrossReferenceKindViewModel CrossReferenceKindFootnoteNumberFormatted
            = new CrossReferenceKindViewModel("Footnote number (formatted)", WdReferenceKind.wdFootnoteNumberFormatted);

        private static readonly CrossReferenceKindViewModel CrossReferenceKindFootnoteNumber
            = new CrossReferenceKindViewModel("Footnote number", WdReferenceKind.wdFootnoteNumber);

        private static readonly List<CrossReferenceKindViewModel> CrossReferenceKinds =
            new List<CrossReferenceKindViewModel>
            {
                CrossReferenceKindFootnoteNumberFormatted,
                CrossReferenceKindFootnoteNumber,
                CrossReferenceKindPageNumber,
                CrossReferenceKindAboveBelow,
            };

        private static CrossReferenceKindViewModel _preferredCrossReferenceKind =
            CrossReferenceKindFootnoteNumberFormatted;

        public CrossReferenceToFootnote(
            WdReferenceType referenceType,
            int referenceItem,
            string text,
            Document document)
            : base(referenceType, referenceItem, text, document)
        {
        }

        public override List<CrossReferenceKindViewModel> AvailableReferenceKinds
        {
            get { return CrossReferenceKinds; }
        }

        public override CrossReferenceKindViewModel PreferredCrossReferenceKind
        {
            get { return _preferredCrossReferenceKind; }
            set { _preferredCrossReferenceKind = value; }
        }
    }
}
