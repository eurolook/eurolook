﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands.InsertCrossReference
{
    [UsedImplicitly]
    public class InsertCrossReferenceCommand : UIBrickCommandBase
    {
        private readonly IDocumentManager _documentManager;

        public InsertCrossReferenceCommand(IDocumentManager documentManager)
        {
            _documentManager = documentManager;
        }

        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            var selection = context.Selection;
            if (selection == null)
            {
                return Task.FromResult(0);
            }

            var viewModel = GetViewModel();
            viewModel.OnOpening();
            viewModel.Show(
                () =>
                {
                    if (viewModel.SelectedCrossReference == null)
                    {
                        return;
                    }

                    var document = context.Document;
                    try
                    {
                        Insert(context, viewModel);
                    }
                    catch (Exception ex)
                    {
                        this.LogError(ex);
                    }
                    finally
                    {
                        document.ActiveWindow.ActivePane.Previous.Activate();
                        document.ActiveWindow.SetFocus();
                    }
                });

            return Task.FromResult(0);
        }

        private static void ResetFormatAfter(Selection selection)
        {
            var range = selection.Range.Duplicate;
            range.MoveEnd(WdUnits.wdCharacter, 1);
            if (Regex.IsMatch(range.Text ?? "", @"\W") && range.Text != "\r")
            {
                // skip inserting a blank if we are at a word boundary
                selection.Font.Reset();
                return;
            }

            // ECADOCS-313: Insert a separating whitespace before performing ECADOCS-192 font reset
            // to prevent loss of formatting on the last cross reference word.
            selection.InsertAfter(" ");
            selection.Font.Reset();

            selection.Collapse(WdCollapseDirection.wdCollapseEnd);
        }

        private void Insert(IBrickExecutionContext context, InsertCrossReferenceViewModel crossReference)
        {
            var selection = context.Selection;
            if (selection == null)
            {
                return;
            }

            using (new DocumentAutomationHelper(context, "Insert Cross Reference"))
            {
                selection.Range.HandleVanishingContentControl();

                const string separator = "\u00a0";
                bool first = true;

                var range = selection.Range.Duplicate;
                range.Collapse(WdCollapseDirection.wdCollapseStart);

                foreach (var referenceKind in crossReference.SelectedReferenceKind.ReferenceKind)
                {
                    if (!first)
                    {
                        selection.InsertBefore(separator);
                        selection.Collapse(WdCollapseDirection.wdCollapseEnd);
                    }

                    bool includePosition = crossReference.CanIncludePosition && crossReference.IncludePosition;
                    selection.InsertCrossReference(
                        crossReference.SelectedCrossReference.ReferenceType,
                        referenceKind,
                        crossReference.SelectedCrossReference.ReferenceItem,
                        true,
                        includePosition,
                        false);

                    first = false;
                }

                range.End = selection.End;
                foreach (var field in range.Fields.OfType<Field>())
                {
                    // Make REF fields keep their formatting when updated
                    if (field.Type == WdFieldType.wdFieldRef
                        && !Regex.IsMatch(field.Code.Text, @"\s+\\\*\s+MERGEFORMAT\b", RegexOptions.IgnoreCase))
                    {
                        string fieldText = field.Code.Text + @" \* MERGEFORMAT ";
                        field.Code.Text = fieldText;
                    }

                    field.Update();
                }

                AdjustTargetRange(context, crossReference, range);

                // if the document contains a _Cross Reference_ style use that style to format the reference
                var style = LanguageIndependentStyle.FromNeutralStyleName("Cross Reference", context.Document);
                if (style != null)
                {
                    // ReSharper disable once UseIndexedProperty
                    range.set_Style(style.Style);

                    // ECADOCS-192: Reset formatting so that typing after the reference uses the formatting
                    // of the paragraph and not the _Cross Reference_ style.
                    ResetFormatAfter(selection);
                }
            }
        }

        private static void AdjustTargetRange(
            IBrickExecutionContext context,
            InsertCrossReferenceViewModel crossReference,
            Range range)
        {
            // ECADOCS-442: exclude separator chars from the beginning of caption labels
            // if the cross reference uses only the caption text
            if (crossReference.SelectedReferenceKind.ReferenceKind.Length == 1
                && crossReference.SelectedReferenceKind.ReferenceKind.First() == WdReferenceKind.wdOnlyCaptionText)
            {
                var field = range.Fields.OfType<Field>().FirstOrDefault(f => f.Type == WdFieldType.wdFieldRef);
                if (field != null)
                {
                    var bookmarkNameMatch = Regex.Match(field.Code.Text, @"REF\s+(?<bookmark>\S+)");
                    if (bookmarkNameMatch.Success)
                    {
                        var bookmarkName = bookmarkNameMatch.Groups["bookmark"].Value;
                        var targetRange = context.Document.Bookmarks[bookmarkName].Range;

                        // exclude blank, non-breaking whitespace, and dashes used to separate the caption
                        // label from the caption text
                        var separatorChars = " \u00a0\u002D\u2010\u2012\u2013\u2014";
                        targetRange.MoveStartWhileSafe(separatorChars);
                        context.Document.Bookmarks.Add(bookmarkName, targetRange);
                        field.Update();
                    }
                }
            }
        }

        private InsertCrossReferenceViewModel GetViewModel()
        {
            var documentViewModel = _documentManager.GetActiveDocumentViewModel();
            return documentViewModel.InsertCrossReferenceViewModel;
        }
    }
}
