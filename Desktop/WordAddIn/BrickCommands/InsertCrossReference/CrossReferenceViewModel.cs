using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.BrickCommands.InsertCrossReference
{
    public abstract class CrossReferenceViewModel
    {
        protected static readonly CrossReferenceKindViewModel CrossReferenceKindEntireTextWithNumber =
            new CrossReferenceKindViewModel(
                "Entire text with number",
                WdReferenceKind.wdNumberRelativeContext,
                WdReferenceKind.wdContentText);

        protected static readonly CrossReferenceKindViewModel CrossReferenceKindText =
            new CrossReferenceKindViewModel("Text", WdReferenceKind.wdContentText);

        protected static readonly CrossReferenceKindViewModel CrossReferenceKindTextOnly =
            new CrossReferenceKindViewModel("Text only", WdReferenceKind.wdContentText);

        protected static readonly CrossReferenceKindViewModel CrossReferenceKindPageNumber =
            new CrossReferenceKindViewModel("Page Number", WdReferenceKind.wdPageNumber);

        protected static readonly CrossReferenceKindViewModel CrossReferenceKindParagraphNumber =
            new CrossReferenceKindViewModel("Paragraph Number", WdReferenceKind.wdNumberRelativeContext);

        protected static readonly CrossReferenceKindViewModel CrossReferenceKindAboveBelow =
            new CrossReferenceKindViewModel("Above/below", WdReferenceKind.wdPosition);

        private static readonly Dictionary<object, string> ReferenceTypeDisplayNames = new Dictionary<object, string>
        {
            { WdReferenceType.wdRefTypeBookmark, "Bookmark" },
            { WdReferenceType.wdRefTypeHeading, "Heading" },
            { WdReferenceType.wdRefTypeFootnote, "Footnote" },
            { WdReferenceType.wdRefTypeEndnote, "Endnote" },
            { WdReferenceType.wdRefTypeNumberedItem, "Numbered item" },
        };

        protected CrossReferenceViewModel(object referenceType, object referenceItem, string text, Document document)
        {
            ReferenceType = referenceType;
            ReferenceItem = referenceItem;
            Text = text;
            Document = document;
            Indent = new Thickness(10, 3, 10, 3);
            Level = 0;

            if (CanHaveLevel)
            {
                var match = Regex.Match(text, @"^(?<indent>\s+)");
                if (match.Success)
                {
                    int paddingLeft = 10 + match.Groups["indent"].Value.Length * 5;
                    Indent = new Thickness(paddingLeft, 3, 10, 3);
                    Level = match.Groups["indent"].Value.Length / 2;
                }
            }
        }

        [UsedImplicitly]
        public string Text { get; }

        public object ReferenceType { get; }

        [UsedImplicitly]
        public string ReferenceTypeDisplayName
        {
            get
            {
                if (ReferenceTypeDisplayNames.ContainsKey(ReferenceType))
                {
                    return ReferenceTypeDisplayNames[ReferenceType];
                }

                // SEQ fields may use numberings like Picture, Picture1, Picture2
                // or Picture3.2, Picture3.3 etc to enable per-chapter/section numbering
                // However, in the task pane we want to show all of these sequences under the same
                // "Picture" group. Therefore all numbering suffixes are removed from the
                // sequence name and only the base name is used as the display name.
                const string sequenceBaseNamePattern = @"^(?<sequenceBaseName>[^0-9]+)";
                var match = new Regex(sequenceBaseNamePattern).Match(ReferenceType.ToString());
                if (match.Success)
                {
                    var sequenceBaseName = match.Groups["sequenceBaseName"].Value;
                    sequenceBaseName = ConvertPascalCaseToBlankSeparatedString(sequenceBaseName);

                    return sequenceBaseName;
                }

                return ReferenceType.ToString();
            }
        }

        private static string ConvertPascalCaseToBlankSeparatedString(string input)
        {
            const string pascalCaseRegexPattern = "^[A-Z][a-z]+(?:[A-Z][a-z]+)*$";
            if (Regex.IsMatch(input, pascalCaseRegexPattern))
            {
                input = Regex.Replace(input, "([A-Z])", " $1").Trim();
            }

            return input;
        }

        public abstract List<CrossReferenceKindViewModel> AvailableReferenceKinds { get; }

        [CanBeNull]
        public CrossReferenceKindViewModel PreferredReferenceKind
        {
            get
            {
                return AvailableReferenceKinds.FirstOrDefault(k => k == PreferredCrossReferenceKind)
                       ?? AvailableReferenceKinds.FirstOrDefault();
            }
        }

        public object ReferenceItem { get; }

        public Thickness Indent { get; protected set; }

        public bool CanHaveLevel
        {
            get
            {
                var referenceType = ReferenceType as WdReferenceType?;
                return referenceType == WdReferenceType.wdRefTypeHeading
                       || referenceType == WdReferenceType.wdRefTypeNumberedItem;
            }
        }

        public int Level { get; protected set; }

        public abstract CrossReferenceKindViewModel PreferredCrossReferenceKind { get; set; }

        protected Document Document { get; }

        public static CrossReferenceViewModel Create(
            WdReferenceType referenceType,
            int index,
            string text,
            Document document)
        {
            switch (referenceType)
            {
                case WdReferenceType.wdRefTypeNumberedItem:
                    return new CrossReferenceToNumberedItem(referenceType, index, text, document);

                case WdReferenceType.wdRefTypeHeading:
                    return new CrossReferenceToHeading(referenceType, index, text, document);

                case WdReferenceType.wdRefTypeFootnote:
                    return new CrossReferenceToFootnote(referenceType, index, text, document);

                case WdReferenceType.wdRefTypeEndnote:
                    return new CrossReferenceToEndnote(referenceType, index, text, document);

                case WdReferenceType.wdRefTypeBookmark:
                    return new CrossReferenceToBookmark(text, document);

                default:
                    return null;
            }
        }

        public static CrossReferenceViewModel Create(string captionLabel, int index, string text, Document document)
        {
            return new CrossReferenceToCaptionLabel(captionLabel, index, text, document);
        }

        public override string ToString()
        {
            return (Text ?? "").Trim();
        }
    }
}
