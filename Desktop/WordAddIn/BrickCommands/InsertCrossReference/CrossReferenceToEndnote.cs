using System.Collections.Generic;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.BrickCommands.InsertCrossReference
{
    public class CrossReferenceToEndnote : CrossReferenceViewModel
    {
        private static readonly CrossReferenceKindViewModel CrossReferenceKindEndnoteNumberFormatted
            = new CrossReferenceKindViewModel("Endnote number (formatted)", WdReferenceKind.wdEndnoteNumberFormatted);

        private static readonly CrossReferenceKindViewModel CrossReferenceKindEndnoteNumber
            = new CrossReferenceKindViewModel("Endnote number", WdReferenceKind.wdEndnoteNumber);

        private static readonly List<CrossReferenceKindViewModel> CrossReferenceKinds =
            new List<CrossReferenceKindViewModel>
            {
                CrossReferenceKindEndnoteNumberFormatted,
                CrossReferenceKindEndnoteNumber,
                CrossReferenceKindPageNumber,
                CrossReferenceKindAboveBelow,
            };

        private static CrossReferenceKindViewModel _preferredCrossReferenceKind =
            CrossReferenceKindEndnoteNumberFormatted;

        public CrossReferenceToEndnote(WdReferenceType referenceType, int referenceItem, string text, Document document)
            : base(referenceType, referenceItem, text, document)
        {
        }

        public override List<CrossReferenceKindViewModel> AvailableReferenceKinds => CrossReferenceKinds;

        public override CrossReferenceKindViewModel PreferredCrossReferenceKind
        {
            get => _preferredCrossReferenceKind;
            set => _preferredCrossReferenceKind = value;
        }
    }
}
