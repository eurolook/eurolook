using System;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.BrickCommands.InsertCrossReference
{
    public class CrossReferenceKindViewModel : IEquatable<CrossReferenceKindViewModel>
    {
        public CrossReferenceKindViewModel(string displayName, params WdReferenceKind[] referenceKind)
        {
            ReferenceKind = referenceKind;
            DisplayName = displayName;
        }

        public WdReferenceKind[] ReferenceKind { get; }

        [UsedImplicitly]
        public string DisplayName { get; set; }

        public bool Equals(CrossReferenceKindViewModel other)
        {
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            if (other == null)
            {
                return false;
            }

            return DisplayName == other.DisplayName;
        }

        public override string ToString()
        {
            return DisplayName;
        }
    }
}
