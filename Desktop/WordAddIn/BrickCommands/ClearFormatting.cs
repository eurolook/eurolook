using System.Threading.Tasks;
using Eurolook.WordAddIn.BrickEngine;

namespace Eurolook.WordAddIn.BrickCommands
{
    public class ClearFormatting : ApplyStyleCommandBase
    {
        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (new DocumentAutomationHelper(context, $"Apply {DisplayName} Brick"))
            {
                // context.Selection.ClearFormatting();
                context.Selection.Font.Reset();
            }

            return Task.FromResult(0);
        }
    }
}
