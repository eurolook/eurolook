﻿using Eurolook.DocumentProcessing;
using Eurolook.WordAddIn.BrickEngine;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    public class ApplyPromoteCommand : ApplyStyleCommandBase
    {
        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (
                new DocumentAutomationHelper(
                    context,
                    "Outline Promote",
                    DocumentAutomationOption.Default | DocumentAutomationOption.RestoreSelection))
            {
                foreach (Paragraph paragraph in context.Selection.Paragraphs)
                {
                    StyleConceptNew.HandleListEnding(paragraph);
                }

                if (IsListSelection(context))
                {
                    await PromoteListLevelAsync(context);
                }
                else
                {
                    await PromoteTextLevelAsync(context);
                }
            }
        }

        private async Task PromoteListLevelAsync(IBrickExecutionContext context)
        {
            var range = context.Selection.Range;
            foreach (Paragraph paragraph in range.Paragraphs)
            {
                var neutralStyle = LanguageIndependentStyle.FromParagraph(paragraph);
                if (neutralStyle == null)
                {
                    continue;
                }

                var nameInfo = StyleNameInfo.FromStyleName(neutralStyle.NameNeutral);
                var newName = nameInfo.ListPromote();

                if (!StyleConceptNew.IsStyleAvailable(newName.StyleName, context.Document))
                {
                    continue;
                }

                await ApplyStyleRespectListContinue(paragraph, newName.StyleName);
            }
        }

        private async Task PromoteTextLevelAsync(IBrickExecutionContext context)
        {
            var range = context.Selection.Range;
            var listRestarts = StyleConceptListRestart.GetListRestarts(range);
            foreach (Paragraph paragraph in range.Paragraphs)
            {
                var neutralStyle = LanguageIndependentStyle.FromParagraph(paragraph);
                if (neutralStyle == null)
                {
                    continue;
                }

                var nameInfo = StyleNameInfo.FromStyleName(neutralStyle.NameNeutral);
                var newName = nameInfo?.TextPromote();

                if (newName == null || !StyleConceptNew.IsStyleAvailable(newName.StyleName, context.Document))
                {
                    continue;
                }

                await ApplyStyleRespectListContinue(paragraph, newName.StyleName);
            }

            foreach (Table table in range.Tables)
            {
                var neutralStyle = LanguageIndependentStyle.FromTable(table);
                if (neutralStyle == null)
                {
                    continue;
                }

                var nameInfo = StyleNameInfo.FromStyleName(neutralStyle.NameNeutral);
                var newName = nameInfo?.TextPromote();

                if (newName == null || !StyleConceptNew.IsStyleAvailable(newName.StyleName, context.Document))
                {
                    continue;
                }

                await ApplyTableStyle(table, newName.StyleName);
            }

            StyleConceptListRestart.ApplyListRestarts(listRestarts);
        }
    }
}
