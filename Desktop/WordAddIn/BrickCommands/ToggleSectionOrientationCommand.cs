using Eurolook.AddIn.Common;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    [UsedImplicitly]
    public class ToggleSectionOrientationCommand : EditCommandBase
    {
        private readonly IMessageService _messageService;

        public ToggleSectionOrientationCommand(IMessageService messageService)
        {
            _messageService = messageService;
        }

        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (new DocumentAutomationHelper(context, $"Apply {DisplayName} Brick"))
            {
                context.Selection.Range.HandleVanishingContentControl();

                var range = context.Selection.Range.Duplicate;

                if (range.Sections.Count > 1)
                {
                    _messageService.ShowSimpleMessage(
                        "The section page orientation cannot be changed because the current selection contains more than one section.",
                        DisplayName);
                    return Task.CompletedTask;
                }

                var currentSection = range.Sections.First;

                var newPageOrientation = currentSection.PageSetup.Orientation == WdOrientation.wdOrientLandscape
                    ? WdOrientation.wdOrientPortrait
                    : WdOrientation.wdOrientLandscape;

                currentSection.PageSetup.Orientation = newPageOrientation;

                return Task.CompletedTask;
            }
        }
    }
}
