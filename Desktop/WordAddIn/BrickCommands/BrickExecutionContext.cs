using Eurolook.Data.Xml;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.BrickCommands
{
    public class BrickExecutionContext : IBrickExecutionContext
    {
        private readonly IAuthorManager _authorManager;
        private AuthorInformation _authorInformation;

        public BrickExecutionContext(
            IWordApplicationContext applicationContext,
            IAuthorManager authorManager,
            [NotNull] IEurolookWordDocument eurolookDocument)
        {
            _authorManager = authorManager;
            Application = applicationContext.Application;
            EurolookDocument = eurolookDocument;
        }

        [NotNull]
        public Application Application { get; }

        public Document Document => EurolookDocument.Document;

        [CanBeNull]
        public Selection Selection => Application.GetSelection();

        public IReadOnlyEurolookProperties EurolookProperties => EurolookDocument.GetEurolookProperties();

        public IEurolookWordDocument EurolookDocument { get; }

        public AuthorInformation AuthorInformation
        {
            get
            {
                return _authorInformation ??= new AuthorInformation(
                    _authorManager.GetAuthorCustomXmlStoreInfos(Document),
                    EurolookProperties);
            }
            set => _authorInformation = value;
        }
    }
}
