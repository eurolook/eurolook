﻿using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.BrickCommands
{
    [UsedImplicitly]
    public class InsertFilenameCommand : EditCommandBase
    {
        private readonly IMessageService _messageService;

        public InsertFilenameCommand(IMessageService messageService)
        {
            _messageService = messageService;
        }

        public override bool CanExecute(IBrickExecutionContext context, string argument)
        {
            if (context.Selection == null)
            {
                return false;
            }

            if (context.Selection.Range.IsLocked())
            {
                return false;
            }

            return base.CanExecute(context, argument);
        }

        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            if (string.IsNullOrEmpty(context.Document.Path))
            {
                _messageService.ShowSimpleMessage(
                    "The current document has not been saved yet.\n\nSave the document and then insert the file name.",
                    "Insert File Name");
                return Task.FromResult(0);
            }

            using (new DocumentAutomationHelper(context, $"Apply {DisplayName} Brick"))
            {
                context.Selection.Range.HandleVanishingContentControl();

                // we use TypeText here to make sure that the insertion pointer
                // is after the inserted text
                context.Selection.TypeText(context.Document.FullName);
            }

            return Task.FromResult(0);
        }
    }
}
