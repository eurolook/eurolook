﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Eurolook.WordAddIn.BrickCommands.InsertPicture
{
    public partial class InsertPictureView
    {
        public InsertPictureView(InsertPictureViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;

            Width = viewModel.ShowInsertPositionsContentOptions ? 1080 : 700;
        }

        private void OnOkClick(object sender, RoutedEventArgs e)
        {
            try
            {
                DialogResult = true; // will close the dialog automatically
            }
            catch (InvalidOperationException)
            {
                // when not opened as dialog
                Close();
            }
        }

        private void TextBoxNumberValidation(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !e.Text.All(c => char.IsDigit(c) || c.Equals(',') || c.Equals('.'));
        }
    }

    public class IsIntegerValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (string.IsNullOrEmpty(value?.ToString()))
            {
                return new ValidationResult(false, null);
            }

            if (value?.ToString().All(c => char.IsDigit(c) || c.Equals(',') || c.Equals('.')) == false)
            {
                return new ValidationResult(false, null);
            }

            return new ValidationResult(true, null);
        }
    }
}
