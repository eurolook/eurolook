﻿namespace Eurolook.WordAddIn.BrickCommands.InsertPicture
{
    public class LayoutOptionConfiguration
    {
        public string Name { get; set; } = string.Empty;

        public string IconResource { get; set; } = string.Empty;

        public bool IsInline { get; set; } = false;

        public bool IsAnchoredToParagraph { get; set; } = false;

        public string MaxWidth { get; set; } = string.Empty;

        public string MaxHeight { get; set; } = string.Empty;

        public string ImageWidth { get; set; } = string.Empty;

        public string ImageHeight { get; set; } = string.Empty;

        public string HorizontalPosition { get; set; } = string.Empty;

        public string RelativeHorizontalPosition { get; set; } = string.Empty;

        public string VerticalPosition { get; set; } = string.Empty;

        public string RelativeVerticalPosition { get; set; } = string.Empty;

        public string TextWrappingStyle { get; set; } = string.Empty;

        public string TextWrappingDistance { get; set; } = string.Empty;

        public bool KeepAspectRatio { get; set; } = false;

        public bool MoveWithText { get; set; } = false;

        public bool LockAnchor { get; set; } = false;
    }
}
