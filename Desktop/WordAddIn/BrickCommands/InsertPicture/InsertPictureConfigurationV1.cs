﻿namespace Eurolook.WordAddIn.BrickCommands.InsertPicture
{
    public class InsertPictureConfigurationV1
    {
        public string LabelTitle { get; set; } = string.Empty;

        public string LabelTitleWithChapter { get; set; } = string.Empty;

        public string LabelTitleWithAgencyChapter { get; set; } = string.Empty;

        public string LabelCaption { get; set; } = string.Empty;

        public string LabelCaptionWithChapter { get; set; } = string.Empty;

        public string LabelSource { get; set; } = string.Empty;

        public string LabelAlternativeText { get; set; } = string.Empty;

        public string PictureTitleStyle { get; set; } = string.Empty;

        public string PictureSubtitleStyle { get; set; } = string.Empty;

        public string PictureBodyStyle { get; set; } = string.Empty;

        public string PictureCaptionStyle { get; set; } = string.Empty;

        public string PictureSourceStyle { get; set; } = string.Empty;

        public string PictureCopyrightStyle { get; set; } = string.Empty;

        public string PictureNotesStyle { get; set; } = string.Empty;

        public string PictureAltTextStyle { get; set; } = string.Empty;

        public string InsertPositions { get; set; } = string.Empty;

        public LayoutOptionConfiguration[] LayoutOptionsSettings { get; set; } = null;

        public bool HasItalicSourceLabel { get; set; } = false;

        public bool AllowConfiguration { get; set; } = true;

        public bool HasTitle { get; set; } = true;

        public bool HasSubtitle { get; set; } = false;

        public bool HasAlternativeTextBox { get; set; } = false;

        public bool HasCaption { get; set; } = false;

        public bool HasSourceLine { get; set; } = true;

        public bool HasCopyrightLine { get; set; } = false;

        public bool HasMarkAsDecorativeBox { get; set; } = false;

        public bool HasNotesLine { get; set; } = false;

        public bool IsTitleSelected { get; set; } = true;

        public bool IsSubtitleSelected { get; set; } = false;

        public bool IsCaptionSelected { get; set; } = false;

        public bool IsCopyrightLineSelected { get; set; } = false;

        public bool IsSourceLineSelected { get; set; } = true;

        public bool IsNotesLineSelected { get; set; } = false;

        public bool IsMarkAsDecorativeSelected { get; set; } = false;

        public bool IsInContentControl { get; set; } = false;

        public bool SettingsSaved { get; set; } = false;
    }
}
