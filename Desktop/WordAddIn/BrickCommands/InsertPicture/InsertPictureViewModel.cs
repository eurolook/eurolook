﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.ClipboardTools;
using Eurolook.AddIn.Common.ClipboardTools.Formats;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.AddIn.Common.Properties;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Common.Log;
using Eurolook.WordAddIn.BrickCommands.ContentInserter;
using Eurolook.WordAddIn.Extensions;
using GalaSoft.MvvmLight.CommandWpf;
using Microsoft.Win32;

namespace Eurolook.WordAddIn.BrickCommands.InsertPicture
{
    public sealed class InsertPictureViewModel : BrickCommandViewModelBase, IDisposable
    {
        private const string InsertPositionsOff = "off";
        private const string InsertPositionsAuto = "auto";
        private const string InsertPositionsAll = "all";

        private const string PublishingFormatPdf = "PDF";
        private const string PublishingFormatAccessibleWord = "Word";
        private const string PublishingFormatDraft = "";

        private const string AnchoredToParagraphDefaultMaxDimensions = "10.0";

        private readonly ISettingsService _settingsService;
        private readonly IResourceManager _resourceManager;
        private readonly IClipboardAnalyzer _clipboardAnalyzer;
        private bool _isInClipboardMode;

        private LayoutOptionsViewModel _selectedLayoutOption;
        private string _alternativeText;
        private string _publishingFormat;
        private string _maximumHeight;
        private string _maximumWidth;
        private bool _isAnchoredImageSelected;
        private bool _isTitleSelected;
        private bool _isSubtitleSelected;
        private bool _isCaptionSelected;
        private bool _isCopyrightSelected;
        private bool _isSourceSelected;
        private bool _isNotesSelected;
        private bool _isMarkAsDecorativeSelected;

        public InsertPictureViewModel(
            IEurolookWordDocument eurolookDocument,
            IClipboardAnalyzer clipboardAnalyzer,
            ISettingsService settingsService,
            IResourceManager resourceManager)
            : base(eurolookDocument)
        {
            _settingsService = settingsService;
            _resourceManager = resourceManager;
            _clipboardAnalyzer = clipboardAnalyzer;
            Initialize();

            GetHelpCommand = new RelayCommand(() => _settingsService.OpenHelpLink("PictureBrick"));
        }

        public RelayCommand GetHelpCommand { get; }

        public ICommand RestoreDefaultSettingsCommand { get; set; }

        public string WindowTitle { get; set; }

        public bool AllowBrowseForFile { get; set; }

        public string EmptyClipboardMessage { get; set; }

        public override string Title => WindowTitle;

        public string FileName { get; set; }

        public NotifyTask<ImageSource> PreviewImage { get; set; }

        public bool ShowAdditionalContentOptions { get; set; }

        public string PublishingFormat
        {
            get => _publishingFormat;
            set => Set(() => PublishingFormat, ref _publishingFormat, value);
        }

        public string AnchoredToParagraphLastMaxHeight { get; set; } = AnchoredToParagraphDefaultMaxDimensions;
        public string MaximumHeight
        {
            get => _maximumHeight;
            set
            {
                Set(() => MaximumHeight, ref _maximumHeight, value);
                if (value != null)
                {
                    AnchoredToParagraphLastMaxHeight = value;
                }
            }
        }

        public string AnchoredToParagraphLastMaxWidth { get; set; } = AnchoredToParagraphDefaultMaxDimensions;
        public string MaximumWidth
        {
            get => _maximumWidth;
            set
            {
                Set(() => MaximumWidth, ref _maximumWidth, value);
                if (value != null)
                {
                    AnchoredToParagraphLastMaxWidth = value;
                }
            }
        }

        public bool IsTitleSelectedLastValue { get; set; }
        public bool IsTitleSelected
        {
            get => _isTitleSelected;
            set
            {
                Set(() => IsTitleSelected, ref _isTitleSelected, value);
                IsTitleSelectedLastValue = value;
                RaisePropertyChanged(() => IsSubtitleSelected);
            }
        }

        public bool IsSubtitleSelectedLastValue { get; set; }
        public bool IsSubtitleSelected
        {
            // The selection of the subtitle is only allowed if the table is selected.
            // It is also necessary to remember if the subtitle was selected when selecting the table title.
            get => _isSubtitleSelected && IsTitleSelected;
            set
            {
                Set(() => IsSubtitleSelected, ref _isSubtitleSelected, value);
                IsSubtitleSelectedLastValue = value;
            }
        }

        public bool IsMarkAsDecorativeSelected
        {
            get => _isMarkAsDecorativeSelected;
            set => Set(() => IsMarkAsDecorativeSelected, ref _isMarkAsDecorativeSelected, value);
        }

        public bool MarkAsDecorativeAvailable
        {
            get => HasMarkAsDecorativeBox && EurolookDocument.Document.IsDecorativeAvailable();
        }

        public bool IsCaptionSelected
        {
            get => _isCaptionSelected;
            set => Set(() => IsCaptionSelected, ref _isCaptionSelected, value);
        }

        public bool IsCopyrightSelected
        {
            get => _isCopyrightSelected;
            set => Set(() => IsCopyrightSelected, ref _isCopyrightSelected, value);
        }

        public bool IsSourceSelected
        {
            get => _isSourceSelected;
            set => Set(() => IsSourceSelected, ref _isSourceSelected, value);
        }

        public bool IsNotesSelected
        {
            get => _isNotesSelected;
            set => Set(() => IsNotesSelected, ref _isNotesSelected, value);
        }

        public bool ShowAlternativeText { get; set; }

        public bool HasTitle { get; set; }

        public bool HasSubtitle { get; set; }

        public bool HasCopyrightLine { get; set; }

        public bool HasSourceLine { get; set; }

        public bool HasCaptionLine { get; set; }

        public bool HasNotesLine { get; set; }

        public bool HasMarkAsDecorativeBox { get; set; }

        public bool ShowAutoInsertPositions { get; set; }

        public bool ShowAllInsertPositions { get; set; }

        public bool ShowInsertPositionsContentOptions { get; set; }

        public string AlternativeText
        {
            get => _alternativeText;
            set => Set(() => AlternativeText, ref _alternativeText, value);
        }

        public bool SettingsSaved { get; set; }

        public bool HasPreview => IsValid;

        public bool IsValid => PreviewImage is { IsSuccessfullyCompleted: true, Result: { } };

        public bool HasMessage => !string.IsNullOrEmpty(Message);

        public string Message
        {
            get
            {
                if (IsValid)
                {
                    return null;
                }

                if (IsInClipboardMode)
                {
                    return EmptyClipboardMessage;
                }

                if (AllowBrowseForFile)
                {
                    return "Click on browse to select a picture.";
                }

                return null;
            }
        }

        public bool ShowTitleCheckbox
        {
            get => HasTitle && !IsAnchoredImageSelected;
        }

        public bool ShowSubtitleCheckbox
        {
            get => HasSubtitle && !IsAnchoredImageSelected;
        }

        public bool ShowAnchoredToParagraphDimensions
        {
            get => SelectedLayoutOption?.IsAnchoredToParagraphOption() == true;
        }

        public ObservableCollectionEx<LayoutOptionsViewModel> LayoutOptions { get; } = new();

        public LayoutOptionsViewModel SelectedLayoutOption
        {
            get => _selectedLayoutOption;
            set
            {
                Set(() => SelectedLayoutOption, ref _selectedLayoutOption, value);

                var wasTitleSelected = IsTitleSelected;
                var wasSubtitleSelected = IsSubtitleSelected;
                var wasAnchoredImageSelected = IsAnchoredImageSelected;
                IsAnchoredImageSelected = value.IsAnchoredToParagraphOption() || value.IsPredefinedPositionOption();

                if (IsAnchoredImageSelected && !wasAnchoredImageSelected)
                {
                    IsTitleSelectedLastValue = wasTitleSelected;
                    IsSubtitleSelectedLastValue = wasSubtitleSelected;
                }
                else if (!IsAnchoredImageSelected)
                {
                    IsTitleSelected = IsTitleSelectedLastValue;
                    IsSubtitleSelected = IsSubtitleSelectedLastValue;
                }
            }
        }

        public bool IsAnchoredImageSelected
        {
            get => _isAnchoredImageSelected;
            set
            {
                var wasAnchoredImageSelected = _isAnchoredImageSelected;
                Set(() => IsAnchoredImageSelected, ref _isAnchoredImageSelected, value);

                if (value && !wasAnchoredImageSelected)
                {
                    IsTitleSelected = false;
                    IsSubtitleSelected = false;
                }

                if (value && SelectedLayoutOption?.IsAnchoredToParagraphOption() == true)
                {
                    MaximumHeight = AnchoredToParagraphLastMaxHeight;
                    MaximumWidth = AnchoredToParagraphLastMaxWidth;
                }
                else
                {
                    MaximumHeight = MaximumWidth = null;
                }

                RaisePropertyChanged(() => ShowTitleCheckbox);
                RaisePropertyChanged(() => ShowSubtitleCheckbox);
                RaisePropertyChanged(() => ShowAnchoredToParagraphDimensions);
            }
        }

        public bool IsInClipboardMode
        {
            get => _isInClipboardMode;
            private set
            {
                if (!value)
                {
                    return;
                }

                _isInClipboardMode = true;
                RaisePropertyChanged(() => IsInBrowseMode);
                RaisePropertyChanged(() => IsInClipboardMode);

                RegisterClipboardWatcher();
            }
        }

        public bool IsInBrowseMode
        {
            get => !_isInClipboardMode;
            private set
            {
                if (!value)
                {
                    return;
                }

                _isInClipboardMode = false;
                RaisePropertyChanged(() => IsInBrowseMode);
                RaisePropertyChanged(() => IsInClipboardMode);

                UnregisterClipboardWatcher();
            }
        }

        public ICommand UpdateClipboardCommand => new RelayCommand(UpdateFromClipboard);

        public ICommand BrowseForFileCommand => new RelayCommand(BrowseForFile);

        public ClipboardFormat GetPreferredDataFormat()
        {
            // Note: Use metafile only for Excel or PowerPoint sources to avoid quality loss that may happen
            // if images copied inside Word are pasted as metafile
            _clipboardAnalyzer.Analyze();

            if (Clipboard.GetData(DataFormats.EnhancedMetafile) != null
                && (_clipboardAnalyzer.IsExcelChart || _clipboardAnalyzer.IsExcelRange
                                                    || _clipboardAnalyzer.IsOfficeShape))
            {
                return ClipboardFormat.EnhancedMetafile;
            }

            return ClipboardFormat.Default;
        }

        public void ConfigureOptions(InsertPictureConfigurationV1 configuration)
        {
            ConfigureOptions(
                configuration.AllowConfiguration,
                configuration.HasTitle,
                configuration.HasSubtitle,
                configuration.HasCaption,
                configuration.HasCopyrightLine,
                configuration.HasSourceLine,
                configuration.HasNotesLine,
                configuration.IsTitleSelected,
                configuration.IsSubtitleSelected,
                configuration.IsCaptionSelected,
                configuration.IsCopyrightLineSelected,
                configuration.IsSourceLineSelected,
                configuration.IsNotesLineSelected,
                configuration.HasMarkAsDecorativeBox,
                configuration.InsertPositions);
        }

        public void ConfigureOptions(
            bool canConfigure = false,
            bool allowTitle = false,
            bool allowSubtitle = false,
            bool allowCaption = false,
            bool allowCopyright = false,
            bool allowSource = false,
            bool allowNotes = false,
            bool selectTitle = false,
            bool selectSubtitle = false,
            bool selectCaption = false,
            bool selectCopyright = false,
            bool selectSource = false,
            bool selectNotes = false,
            bool hasMarkAsDecorativeBox = false,
            string insertPositions = InsertPositionsOff)
        {
            AlternativeText = string.Empty;

            HasTitle = allowTitle;
            HasSubtitle = allowSubtitle;
            HasCaptionLine = allowCaption;
            HasCopyrightLine = allowCopyright;
            HasSourceLine = allowSource;
            HasNotesLine = allowNotes;
            HasMarkAsDecorativeBox = hasMarkAsDecorativeBox;

            IsTitleSelected = selectTitle;
            IsSubtitleSelected = selectSubtitle;
            IsCaptionSelected = selectCaption;
            IsCopyrightSelected = selectCopyright;
            IsSourceSelected = selectSource;
            IsNotesSelected = selectNotes;

            ShowAutoInsertPositions = insertPositions.Equals(InsertPositionsAuto, StringComparison.OrdinalIgnoreCase);
            ShowAllInsertPositions = insertPositions.Equals(InsertPositionsAll, StringComparison.OrdinalIgnoreCase);
            ShowAdditionalContentOptions =
                canConfigure && (HasTitle || HasCaptionLine || HasCopyrightLine || HasSourceLine || HasNotesLine);
        }

        public void ConfigureLayoutOptions(string format, List<LayoutOptionConfiguration> layoutOptionConfigurations)
        {
            PublishingFormat = format;
            ShowInsertPositionsContentOptions = ShowAllInsertPositions
                                                || (ShowAutoInsertPositions
                                                    && !PublishingFormat.Equals(PublishingFormatAccessibleWord));
            ShowAlternativeText = PublishingFormat.Equals(PublishingFormatDraft);
            if (layoutOptionConfigurations != null)
            {
                LayoutOptions.AddRange(CreateLayoutOptionsViewModels(layoutOptionConfigurations));
                SelectedLayoutOption = LayoutOptions.FirstOrDefault();
            }
        }

        public void ResetAnchoredToParagraphDimensions()
        {
            AnchoredToParagraphLastMaxHeight = AnchoredToParagraphLastMaxWidth = AnchoredToParagraphDefaultMaxDimensions;
            MaximumHeight = MaximumWidth = AnchoredToParagraphDefaultMaxDimensions;
        }

        public void Dispose()
        {
            UnregisterClipboardWatcher();
        }

        private static ImageSource GetPreviewImageFromClipboard()
        {
            return ClipboardImageHelper.ImageFromClipboardDib();
        }

        private void Initialize()
        {
            IsInClipboardMode = true;
            if (ClipboardImageHelper.ClipboardContainsImage())
            {
                UpdateFromClipboard();
            }
            else if (AllowBrowseForFile)
            {
                IsInClipboardMode = false;
                BrowseForFile();
            }
        }

        private void UpdateFromClipboard()
        {
            FileName = null;

            PreviewImage = new NotifyTask<ImageSource>()
                .NotifyOnCompletion(this);
            PreviewImage.Start(() => Task.FromResult(GetPreviewImageFromClipboard()));
        }

        private void BrowseForFile()
        {
            var dlg = new OpenFileDialog
            {
                Filter = "All Pictures (*.emf;*.wmf;*.jpg;*.jpeg;*.png;*.gif)|*.emf;*.wmf;*.jpg;*.jpeg;*.png;*.gif|" +
                         "Windows Enhanced Metafile (*.emf)|*.emf|" +
                         "Windows Metafile (*.wmf)|*.wmf|" +
                         "JPEG File Interchange Format (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                         "Portable Network Graphics (*.png)|*.png|" +
                         "Graphics Interchange Format (*.gif)|*.gif",
                CheckFileExists = true,
                Multiselect = false,
                Title = "Insert Picture",
            };

            var result = dlg.ShowDialog();
            PreviewImage = new NotifyTask<ImageSource>().NotifyOnCompletion(this);

            if (result == true)
            {
                IsInBrowseMode = true;
                FileName = dlg.FileName;
                PreviewImage.Start(() => Task.Run(() => GetPreviewImageFromFile(dlg.FileName)));
            }
            else
            {
                PreviewImage.Start(() => Task.FromResult<ImageSource>(null));
            }
        }

        private ImageSource GetPreviewImageFromFile(string fileName)
        {
            try
            {
                using (var image = Image.FromFile(fileName))
                {
                    image.ApplyRotationFromExifMetadata();
                    return image.ToBitmapSource();
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }

            return null;
        }

        private void OnClipboardUpdate(object sender, EventArgs e)
        {
            if (!IsInClipboardMode)
            {
                return;
            }

            try
            {
                UnregisterClipboardWatcher();
                Execute.OnUiThread(UpdateFromClipboard);
            }
            finally
            {
                RegisterClipboardWatcher();
            }
        }

        private void RegisterClipboardWatcher()
        {
            ClipboardWatcher.ClipboardUpdate += OnClipboardUpdate;
        }

        private void UnregisterClipboardWatcher()
        {
            ClipboardWatcher.ClipboardUpdate -= OnClipboardUpdate;
        }

        private IEnumerable<LayoutOptionsViewModel> CreateLayoutOptionsViewModels(
            List<LayoutOptionConfiguration> configurations)
        {
            foreach (var configuration in configurations)
            {
                var icon = _resourceManager.GetObject(configuration.IconResource);
                if (icon != null)
                {
                    yield return new LayoutOptionsViewModel((Bitmap)icon, configuration);
                }
            }
        }
    }
}
