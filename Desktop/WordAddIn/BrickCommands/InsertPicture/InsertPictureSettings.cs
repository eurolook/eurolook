﻿namespace Eurolook.WordAddIn.BrickCommands.InsertPicture
{
    public class InsertPictureSettings
    {
        public bool? AddPictureTitle { get; set; }

        public bool? AddPictureSubtitle { get; set; }

        public bool? AddCopyrightLine { get; set; }

        public bool? AddSourceLine { get; set; }

        public bool? AddCaptionLine { get; set; }

        public bool? AddNotesLine { get; set; }

        public string MaximumHeight { get; set; }

        public string MaximumWidth { get; set; }

        public string LayoutOptionName { get; set; }
    }
}
