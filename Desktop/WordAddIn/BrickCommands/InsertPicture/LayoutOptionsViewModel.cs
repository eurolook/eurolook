﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Windows.Media.Imaging;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.Common;
using GalaSoft.MvvmLight;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.BrickCommands.InsertPicture
{
    public enum LayoutOptionInsertionType
    {
        Inline,
        AnchoredToParagraph,
        PredefinedPosition,
    }

    public class LayoutOptionsViewModel : ViewModelBase
    {
        private const string FullDimensionIdentifier = "Full";
        private const string DimensionWithMarginIdentifier = "WithMargin";

        private static readonly Dictionary<string, WdWrapType> WdWrapTypeMap
            = new Dictionary<string, WdWrapType>
            {
                { "Inline", WdWrapType.wdWrapInline },
                { "Square", WdWrapType.wdWrapSquare },
                { "Tight", WdWrapType.wdWrapTight },
                { "Through", WdWrapType.wdWrapThrough },
                { "TopAndBottom", WdWrapType.wdWrapTopBottom },
                { "Behind", WdWrapType.wdWrapBehind },
                { "InFront", WdWrapType.wdWrapFront },
            };

        private static readonly Dictionary<string, WdShapePosition> WdShapePositionMap
            = new Dictionary<string, WdShapePosition>
            {
                { "Bottom", WdShapePosition.wdShapeBottom },
                { "Center", WdShapePosition.wdShapeCenter },
                { "Inside", WdShapePosition.wdShapeInside },
                { "Left", WdShapePosition.wdShapeLeft },
                { "Outside", WdShapePosition.wdShapeOutside },
                { "Right", WdShapePosition.wdShapeRight },
                { "Top", WdShapePosition.wdShapeTop },
            };

        private static readonly Dictionary<string, WdRelativeHorizontalPosition> WdRelativeHorizontalPositionMap
            = new Dictionary<string, WdRelativeHorizontalPosition>
            {
                { "Character", WdRelativeHorizontalPosition.wdRelativeHorizontalPositionCharacter },
                { "Column", WdRelativeHorizontalPosition.wdRelativeHorizontalPositionColumn },
                { "Margin", WdRelativeHorizontalPosition.wdRelativeHorizontalPositionMargin },
                { "Page", WdRelativeHorizontalPosition.wdRelativeHorizontalPositionPage },
                { "InsideMargin", WdRelativeHorizontalPosition.wdRelativeHorizontalPositionInnerMarginArea },
                { "LeftMargin", WdRelativeHorizontalPosition.wdRelativeHorizontalPositionLeftMarginArea },
                { "OutsideMargin", WdRelativeHorizontalPosition.wdRelativeHorizontalPositionOuterMarginArea },
                { "RightMargin", WdRelativeHorizontalPosition.wdRelativeHorizontalPositionRightMarginArea },
            };

        private static readonly Dictionary<string, WdRelativeVerticalPosition> WdRelativeVerticalPositionMap
            = new Dictionary<string, WdRelativeVerticalPosition>
            {
                { "Line", WdRelativeVerticalPosition.wdRelativeVerticalPositionLine },
                { "Margin", WdRelativeVerticalPosition.wdRelativeVerticalPositionMargin },
                { "Page", WdRelativeVerticalPosition.wdRelativeVerticalPositionPage },
                { "Paragraph", WdRelativeVerticalPosition.wdRelativeVerticalPositionParagraph },
                { "BottomMargin", WdRelativeVerticalPosition.wdRelativeVerticalPositionBottomMarginArea },
                { "InsideMargin", WdRelativeVerticalPosition.wdRelativeVerticalPositionInnerMarginArea },
                { "OutsideMargin", WdRelativeVerticalPosition.wdRelativeVerticalPositionOuterMarginArea },
                { "TopMargin", WdRelativeVerticalPosition.wdRelativeVerticalPositionTopMarginArea },
            };

        private readonly LayoutOptionConfiguration _configuration;

        private readonly Bitmap _resource;

        private bool _isSelected;
        private LayoutOptionInsertionType _type;

        public LayoutOptionsViewModel(Bitmap resource, LayoutOptionConfiguration configuration)
        {
            _configuration = configuration;
            _resource = resource;
            Icon = resource.ToBitmapSource();

            Init();
        }

        public string Name { get; set; }

        public float? ImageWidth { get; set; }

        public bool IsFullWidth { get; set; }

        public bool WidthIsContentArea { get; set; }

        public float? ImageHeight { get; set; }

        public bool IsFullHeight { get; set; }

        public bool HeightIsContentArea { get; set; }

        public float? MaxWidth { get; set; }

        public float? MaxHeight { get; set; }

        public float? HorizontalPosition { get; set; }

        public WdRelativeHorizontalPosition RelativeHorizontalPosition { get; set; }

        public float? VerticalPosition { get; set; }

        public WdRelativeVerticalPosition RelativeVerticalPosition { get; set; }

        public WdWrapType? TextWrappingStyle { get; set; }

        public float? TextWrappingDistanceLeft { get; set; }

        public float? TextWrappingDistanceTop { get; set; }

        public float? TextWrappingDistanceRight { get; set; }

        public float? TextWrappingDistanceBottom { get; set; }

        public bool KeepAspectRatio { get; set; }

        public bool MoveWithText { get; set; }

        public bool LockAnchor { get; set; }

        public BitmapSource Icon { get; }

        public bool IsSelected
        {
            get => _isSelected;
            set => Set(() => IsSelected, ref _isSelected, value);
        }

        public bool IsInlineOption()
        {
            return _type.Equals(LayoutOptionInsertionType.Inline);
        }

        public bool IsAnchoredToParagraphOption()
        {
            return _type.Equals(LayoutOptionInsertionType.AnchoredToParagraph);
        }

        public bool IsPredefinedPositionOption()
        {
            return _type.Equals(LayoutOptionInsertionType.PredefinedPosition);
        }

        private void Init()
        {
            Name = _configuration.Name;
            KeepAspectRatio = _configuration.KeepAspectRatio;
            MoveWithText = _configuration.MoveWithText;
            LockAnchor = _configuration.LockAnchor;

            InitPosition();
            InitDimensions();
            InitType();

            if (!string.IsNullOrEmpty(_configuration.TextWrappingStyle))
            {
                TextWrappingStyle = WdWrapTypeMap[_configuration.TextWrappingStyle];
            }

            InitTextWrappingDistance();
        }

        private void InitPosition()
        {
            if (_configuration.IsInline)
            {
                RelativeVerticalPosition = WdRelativeVerticalPosition.wdRelativeVerticalPositionLine;
                return;
            }

            if (_configuration.IsAnchoredToParagraph)
            {
                RelativeVerticalPosition = WdRelativeVerticalPosition.wdRelativeVerticalPositionParagraph;
                return;
            }

            double value;

            if (double.TryParse(
                    _configuration.HorizontalPosition,
                    NumberStyles.Any,
                    CultureInfo.InvariantCulture,
                    out value))
            {
                HorizontalPosition = UnitConverter.CmToPoints(value);
            }
            else
            {
                HorizontalPosition = (float)WdShapePositionMap[_configuration.HorizontalPosition];
            }

            if (double.TryParse(
                    _configuration.VerticalPosition,
                    NumberStyles.Any,
                    CultureInfo.InvariantCulture,
                    out value))
            {
                VerticalPosition = UnitConverter.CmToPoints(value);
            }
            else
            {
                VerticalPosition = (float)WdShapePositionMap[_configuration.VerticalPosition];
            }

            RelativeHorizontalPosition = WdRelativeHorizontalPositionMap[_configuration.RelativeHorizontalPosition];
            RelativeVerticalPosition = WdRelativeVerticalPositionMap[_configuration.RelativeVerticalPosition];
        }

        private void InitDimensions()
        {
            double value;

            IsFullWidth = _configuration.ImageWidth.Equals(
                FullDimensionIdentifier,
                StringComparison.OrdinalIgnoreCase);
            IsFullHeight = _configuration.ImageHeight.Equals(
                FullDimensionIdentifier,
                StringComparison.OrdinalIgnoreCase);

            WidthIsContentArea = _configuration.ImageWidth.Equals(
                DimensionWithMarginIdentifier,
                StringComparison.OrdinalIgnoreCase);
            HeightIsContentArea = _configuration.ImageHeight.Equals(
                DimensionWithMarginIdentifier,
                StringComparison.OrdinalIgnoreCase);

            if (!IsFullWidth && !WidthIsContentArea)
            {
                if (double.TryParse(
                        _configuration.ImageWidth,
                        NumberStyles.Any,
                        CultureInfo.InvariantCulture,
                        out value))
                {
                    ImageWidth = UnitConverter.CmToPoints(value);
                }
            }

            if (!IsFullHeight && !HeightIsContentArea)
            {
                if (double.TryParse(
                        _configuration.ImageHeight,
                        NumberStyles.Any,
                        CultureInfo.InvariantCulture,
                        out value))
                {
                    ImageHeight = UnitConverter.CmToPoints(value);
                }
            }

            if (double.TryParse(_configuration.MaxWidth, NumberStyles.Any, CultureInfo.InvariantCulture, out value))
            {
                MaxWidth = UnitConverter.CmToPoints(value);
            }

            if (double.TryParse(_configuration.MaxHeight, NumberStyles.Any, CultureInfo.InvariantCulture, out value))
            {
                MaxHeight = UnitConverter.CmToPoints(value);
            }
        }

        private void InitTextWrappingDistance()
        {
            if (string.IsNullOrWhiteSpace(_configuration.TextWrappingDistance))
            {
                return;
            }

            double value;
            var ltrb = _configuration.TextWrappingDistance.Split();

            if (ltrb.Length == 4)
            {
                if (double.TryParse(ltrb[0], NumberStyles.Any, CultureInfo.InvariantCulture, out value))
                {
                    TextWrappingDistanceLeft = UnitConverter.CmToPoints(value);
                }

                if (double.TryParse(ltrb[1], NumberStyles.Any, CultureInfo.InvariantCulture, out value))
                {
                    TextWrappingDistanceTop = UnitConverter.CmToPoints(value);
                }

                if (double.TryParse(ltrb[2], NumberStyles.Any, CultureInfo.InvariantCulture, out value))
                {
                    TextWrappingDistanceRight = UnitConverter.CmToPoints(value);
                }

                if (double.TryParse(ltrb[3], NumberStyles.Any, CultureInfo.InvariantCulture, out value))
                {
                    TextWrappingDistanceBottom = UnitConverter.CmToPoints(value);
                }
            }
            else if (ltrb.Length == 1)
            {
                if (double.TryParse(ltrb[0], NumberStyles.Any, CultureInfo.InvariantCulture, out value))
                {
                    TextWrappingDistanceLeft = TextWrappingDistanceTop = TextWrappingDistanceRight =
                        TextWrappingDistanceBottom = UnitConverter.CmToPoints(value);
                }
            }
        }

        private void InitType()
        {
            if (_configuration.IsInline)
            {
                _type = LayoutOptionInsertionType.Inline;
            }
            else if (_configuration.IsAnchoredToParagraph)
            {
                _type = LayoutOptionInsertionType.AnchoredToParagraph;
            }
            else
            {
                _type = LayoutOptionInsertionType.PredefinedPosition;
            }
        }
    }
}
