using System;
using System.Linq;
using Eurolook.Common.Extensions;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.DocumentProcessing.OpenXml;
using Eurolook.WordAddIn.BrickEngine;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    [UsedImplicitly]
    public class PasteAndFormatCommand : EditCommandBase
    {
        private readonly IDocumentManager _documentManager;
        private readonly IStylesExtractor _stylesExtractor;

        public PasteAndFormatCommand(IDocumentManager documentManager, IStylesExtractor stylesExtractor)
        {
            _documentManager = documentManager;
            _stylesExtractor = stylesExtractor;
        }

        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (new DocumentAutomationHelper(context.Document, "Paste and Format"))
            {
                var documentModel = _documentManager.GetActiveDocumentViewModel().DocumentModel;
                var stylesInTemplate = _stylesExtractor.ExtractStyles(documentModel).ToList();
                var stylesInDocument = context.Document.Styles.OfType<Style>().ToList();

                bool pasteIntoInlineContentControl = context.Selection.ParentContentControl?.IsBlockLevel() == false;

                var destinationStyle = (Style)context.Selection.Paragraphs.First.get_Style();
                var characterDestinationStyle = (Style)context.Selection.Characters.First.get_Style();
                var pastedRange = PasteFromClipboard(context.Selection);

                foreach (Paragraph paragraph in pastedRange.Paragraphs)
                {
                    string styleNameLocal = paragraph.GetStyleNameLocal();

                    bool IsStyleAllowedInDocument() =>
                        stylesInDocument.Any(s => s.NameLocal.EqualsIgnoreCase(styleNameLocal)) ||
                        stylesInTemplate.Any(s => s.StyleName.Val.Value.EqualsIgnoreCase(styleNameLocal));

                    if (!IsStyleAllowedInDocument())
                    {
                        DeleteStyleUsedInParagraph(paragraph);
                    }

                    paragraph.set_Style(destinationStyle);
                }

                if (characterDestinationStyle.Type == WdStyleType.wdStyleTypeCharacter && pasteIntoInlineContentControl)
                {
                    pastedRange.set_Style(characterDestinationStyle);
                }
            }

            return Task.FromResult(0);
        }

        private Range PasteFromClipboard(Selection selection)
        {
            var range = selection.Range;
            selection.PasteAndFormat(WdRecoveryType.wdUseDestinationStylesRecovery);
            range.End = selection.End;
            return range;
        }

        private void DeleteStyleUsedInParagraph(Paragraph paragraph)
        {
            try
            {
                var style = (Style)paragraph.get_Style();
                style.Delete();
            }
            catch (Exception)
            {
                // ignored
            }
        }
    }
}
