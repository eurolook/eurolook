﻿using System;
using Eurolook.Common.Log;
using Range = Microsoft.Office.Interop.Word.Range;
using XlPictureAppearance = Microsoft.Office.Interop.Excel.XlPictureAppearance;

namespace Eurolook.WordAddIn.BrickCommands.ContentInserter
{
    public class ExcelChartInserter : ContentInserterBase, ICanLog
    {
        /// <summary>
        /// Gets or sets the Excel Chart or ChartObject instance to be inserted.
        /// </summary>
        public dynamic Chart { get; set; }

        public string AlternativeText { get; set; }

        protected override Range DoInsert(Range range)
        {
            // copy as EMF (vector format)
            try
            {
                Chart.CopyPicture(XlPictureAppearance.xlPrinter);
                range.Paste();

                if (range.InlineShapes.Count == 1)
                {
                    range.InlineShapes[1].AlternativeText = AlternativeText;
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }

            // alternative: insert chart as PNG
            ////using (var temporaryFile = new TemporaryFile(".png"))
            ////{
            ////    Chart.Export(temporaryFile.FullName, "PNG", true);
            ////    range.InlineShapes.AddPicture(temporaryFile.FullName);
            ////}

            return range;
        }
    }
}
