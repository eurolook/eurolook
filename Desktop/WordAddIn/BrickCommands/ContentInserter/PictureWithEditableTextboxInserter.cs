﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.IO;
using System.Linq;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.Common;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickCommands.InsertPicture;
using Microsoft.CSharp.RuntimeBinder;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using static Eurolook.WordAddIn.BrickCommands.ContentInserter.PictureInserter;
using Range = Microsoft.Office.Interop.Word.Range;
using Shape = Microsoft.Office.Interop.Word.Shape;

namespace Eurolook.WordAddIn.BrickCommands.ContentInserter
{
    public class PictureWithEditableTextboxInserter : ContentInserterBase, ICanLog
    {
        public string FileName { get; set; }

        public string AlternativeText { get; set; }

        public bool LockAspectRatio { get; set; }

        public bool MarkAsDecorative { get; set; }

        public PictureSource Source { get; set; } = PictureSource.File;

        public ClipboardFormat Format { get; set; } = ClipboardFormat.Bitmap;

        public LayoutOptionsViewModel LayoutSettings { get; set; } = null;

        public List<IContentInserter> TextBoxContentInserters { get; set; } = null;

        protected override Range DoInsert(Range range)
        {
            if (range == null)
            {
                this.LogDebug($"Cannot insert picture, then given range is null.");
                return range;
            }

            var anchorRange = range.Paragraphs.First.Range.CollapseToStart();
            if (anchorRange == null)
            {
                this.LogDebug($"Cannot insert picture, could not find a paragraph in the given range.");
                return range;
            }

            Shape picture = null;
            if (Source == PictureSource.File)
            {
                this.LogDebug($"Inserting picture from file...");
                using var tempImageFile = new TemporaryFile(Path.GetExtension(FileName));
                using var image = Image.FromFile(FileName);
                image.ApplyRotationFromExifMetadata();
                image.Save(tempImageFile.FullName);
                picture = range.Document.Shapes.AddPicture(
                    FileName: tempImageFile.FullName,
                    Anchor: anchorRange);
            }
            else if (Format == ClipboardFormat.EnhancedMetafile)
            {
                this.LogDebug($"Pasting EMF from clipboard...");
                anchorRange.PasteSpecial(DataType: WdPasteDataType.wdPasteEnhancedMetafile);
                picture = GetPicture(anchorRange);
            }
            else
            {
                this.LogDebug($"Pasting Bitmap from clipboard...");
                anchorRange.Paste();
                picture = GetPicture(anchorRange);
            }

            var shape = picture;
            if (picture != null)
            {
                SetAltTextOrDecorative(picture);
                SetPictureSize(picture);

                if (IsTextBoxDisplayed)
                {
                    var textBox = InsertTextboxAtPicture(picture);
                    SetTextBoxSize(textBox, picture.Width);
                    NormalizedPositionForGrouping(picture);
                    NormalizedPositionForGrouping(textBox);
                    var group = GroupTogether(picture, textBox);
                    OffsetShapesInGroup(group);
                    shape = group;
                }

                SetFinalPosition(shape);
                shape.LockAnchor = (int)(LayoutSettings.LockAnchor ? MsoTriState.msoTrue : MsoTriState.msoFalse);
            }
            else
            {
                this.LogDebug("Failed to insert picture properly. Could not find the pasted shape.");
            }

            return shape?.Anchor ?? range;
        }

        private Shape GetPicture(Range anchorRange)
        {
            var inlineShape = GetInlineShapeAtRange(anchorRange.Paragraphs.First.Range);
            if (inlineShape != null)
            {
                return inlineShape.ConvertToShape();
            }
            else
            {
                return GetShapeAtRange(anchorRange);
            }
        }

        private void SetTextBoxSize(Shape textBox, float width)
        {
            textBox.Width = width;
            textBox.TextFrame.AutoSize = (int)MsoTriState.msoTrue;
        }

        private static void NormalizedPositionForGrouping(Shape shape)
        {
            // Position slightly below the current paragraph.
            // Otherwise Word will move the anchor tho the paragraph above when grouping the shapes.
            shape.RelativeHorizontalPosition = WdRelativeHorizontalPosition.wdRelativeHorizontalPositionColumn;
            shape.RelativeVerticalPosition = WdRelativeVerticalPosition.wdRelativeVerticalPositionParagraph;
            shape.LeftRelative = 0;
            shape.TopRelative = 0.5f;
        }

        private void SetPictureSize(Shape picture)
        {
            picture.LockAspectRatio = LockAspectRatio ? MsoTriState.msoTrue : MsoTriState.msoFalse;
            if (!ScaleToFit(picture))
            {
                ApplySizeRestrictions(picture);
            }
        }

        private Shape GetShapeAtRange(Range range)
        {
            foreach (Shape shape in range.Document.Shapes)
            {
                if (shape.Anchor.Start == range.Start)
                {
                    return shape;
                }
            }

            return null;
        }

        [SuppressMessage("SonarQube", "S1751: Loops with at most one iteration should be refactored", Justification = "Word.Interop specific")]
        private InlineShape GetInlineShapeAtRange(Range range)
        {
            foreach (InlineShape inlineShape in range.InlineShapes)
            {
                return inlineShape;
            }

            return null;
        }

        private static Shape GroupTogether(Shape picture, Shape textBox)
        {
            picture.Select();
            textBox.Select(false);
            return picture.Application.Selection.ShapeRange.Group();
        }

        private Shape InsertTextboxAtPicture(Shape picture)
        {
            var range = picture.Anchor.Duplicate;
            range.Move(WdUnits.wdCharacter, 1);
            range.Text = "A";
            range.Expand(WdUnits.wdCharacter);
            range.Select();
            picture.Application.Selection.CreateTextbox();
            var textBox = GetShapeAtRange(range);
            textBox.WrapFormat.Type = WdWrapType.wdWrapFront;
            SetTextBoxText(textBox);
            SetTextBoxAppearance(textBox);
            return textBox;
        }

        private bool IsTextBoxDisplayed
        {
            get
            {
                if (TextBoxContentInserters == null || TextBoxContentInserters.All(c => c == null))
                {
                    return false;
                }

                return true;
            }
        }

        private void SetTextBoxAppearance(Shape textBox)
        {
            // EUROLOOK-3857: create white background fill to make the textbox appear in the right object-order when exporting to PDF
            textBox.Fill.BackColor.RGB = ColorTranslator.ToOle(Color.White);
            textBox.Fill.Visible = MsoTriState.msoTrue;
            textBox.Fill.Transparency = 0f;

            textBox.Line.Visible = MsoTriState.msoFalse;
        }

        private void SetTextBoxText(Shape textBox)
        {
            var range = textBox.TextFrame.TextRange;
            range.Text = "";
            var contentInserter = new RangeInserter(TextBoxContentInserters?.Where(c => c != null).ToArray());
            contentInserter.Insert(new RangePositionProvider(range));
            RemoveEmptyParagraph(textBox);
        }

        private static void RemoveEmptyParagraph(Shape textBox)
        {
            var lastParagraph = textBox.TextFrame.TextRange.Paragraphs.Last;
            if (lastParagraph != null && lastParagraph.Range.Text == "\r")
            {
                var style = (Style)lastParagraph.get_Style();
                if (style?.NameLocal == "Normal")
                {
                    lastParagraph.Range.Delete();
                }
            }
        }

        private void OffsetShapesInGroup(Shape group)
        {
            if (group.GroupItems.Count != 2)
            {
                return;
            }

            var picture = group.GroupItems[1].Type == MsoShapeType.msoTextBox
                ? group.GroupItems[2]
                : group.GroupItems[1];
            var textBox = group.GroupItems[1].Type == MsoShapeType.msoTextBox
                ? group.GroupItems[1]
                : group.GroupItems[2];

            if (LayoutSettings.VerticalPosition == (float)WdShapePosition.wdShapeBottom)
            {
                picture.IncrementTop(textBox.Height);
            }
            else
            {
                textBox.IncrementTop(picture.Height);
            }
        }

        private bool ScaleToFit(Shape shape)
        {
            bool result = false;
            var pageSetup = shape.Anchor.Sections.First.PageSetup;
            if (LayoutSettings.IsFullWidth)
            {
                shape.Width = pageSetup.PageWidth;
                result = true;
            }
            else if (LayoutSettings.WidthIsContentArea)
            {
                // Can not be Document.PageSetup if there are multiple margin variants per document
                shape.Width = pageSetup.PageWidth - pageSetup.LeftMargin - pageSetup.RightMargin;
                result = true;
            }
            else if (LayoutSettings.ImageWidth.HasValue)
            {
                shape.Width = LayoutSettings.ImageWidth.Value;
                result = true;
            }

            if (LayoutSettings.IsFullHeight)
            {
                shape.Height = pageSetup.PageHeight;
                result = true;
            }
            else if (LayoutSettings.HeightIsContentArea)
            {
                // Can not be Document.PageSetup if there are multiple margin variants per document
                shape.Height = pageSetup.PageHeight - pageSetup.TopMargin - pageSetup.BottomMargin;
                result = true;
            }
            else if (LayoutSettings.ImageHeight.HasValue)
            {
                shape.Height = LayoutSettings.ImageHeight.Value;
                result = true;
            }

            return result;
        }

        private void ApplySizeRestrictions(Shape shape)
        {
            if (!LayoutSettings.MaxHeight.HasValue && !LayoutSettings.MaxWidth.HasValue)
            {
                return;
            }

            if (shape.Width > shape.Height)
            {
                if (LayoutSettings.MaxHeight.HasValue && shape.Height > LayoutSettings.MaxHeight)
                {
                    shape.Height = LayoutSettings.MaxHeight.Value;
                }

                if (LayoutSettings.MaxWidth.HasValue && shape.Width > LayoutSettings.MaxWidth)
                {
                    shape.Width = LayoutSettings.MaxWidth.Value;
                }
            }
            else
            {
                if (LayoutSettings.MaxWidth.HasValue && shape.Width > LayoutSettings.MaxWidth)
                {
                    shape.Width = LayoutSettings.MaxWidth.Value;
                }

                if (LayoutSettings.MaxHeight.HasValue && shape.Height > LayoutSettings.MaxHeight)
                {
                    shape.Height = LayoutSettings.MaxHeight.Value;
                }
            }
        }

        private void SetFinalPosition(Shape shape)
        {
            if (LayoutSettings.TextWrappingStyle.HasValue)
            {
                shape.WrapFormat.Type = WdWrapType.wdWrapSquare;

                shape.WrapFormat.DistanceLeft = (float)LayoutSettings.TextWrappingDistanceLeft;
                shape.WrapFormat.DistanceTop = (float)LayoutSettings.TextWrappingDistanceTop;
                shape.WrapFormat.DistanceRight = (float)LayoutSettings.TextWrappingDistanceRight;
                shape.WrapFormat.DistanceBottom = (float)LayoutSettings.TextWrappingDistanceBottom;
            }

            shape.RelativeHorizontalPosition = LayoutSettings.RelativeHorizontalPosition;
            shape.Left = LayoutSettings.HorizontalPosition.HasValue ? LayoutSettings.HorizontalPosition.Value : 0;

            shape.RelativeVerticalPosition = LayoutSettings.RelativeVerticalPosition;
            shape.Top = LayoutSettings.VerticalPosition.HasValue ? LayoutSettings.VerticalPosition.Value : 0;
        }

        private void SetAltTextOrDecorative(dynamic shape)
        {
            if (!MarkAsDecorative)
            {
                shape.AlternativeText = AlternativeText;
            }
            else
            {
                try
                {
                    shape.Decorative = MsoTriState.msoTrue;
                }
                catch (RuntimeBinderException)
                {
                    // silent catch: Property does not exist in Word 2016 or prior
                }
            }
        }
    }
}
