﻿using Eurolook.DocumentProcessing.Extensions;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickCommands.ContentInserter
{
    public class ContentControlWithBindingInserter : ContentInserterBase
    {
        public ContentControlWithBindingInserter(WdContentControlType contentControlType)
        {
            ContentControlType = contentControlType;
        }

        public WdContentControlType ContentControlType { get; }

        public string Title { get; set; }

        [NotNull]
        public string PlaceholderText { get; set; } = "";

        [NotNull]
        public string BindingPrefixMapping { get; set; } = "";

        public string BindingXpath { get; set; } = "";

        protected override Range DoInsert(Range range)
        {
            var contentControl = range.ContentControls.Add(ContentControlType, range);
            contentControl.SetPlaceholderText(Text: PlaceholderText);
            contentControl.Title = Title;
            if (!string.IsNullOrEmpty(BindingXpath))
            {
                contentControl.XMLMapping.SetMapping(BindingXpath, BindingPrefixMapping);
            }

            return contentControl.GetControlRange();
        }
    }
}
