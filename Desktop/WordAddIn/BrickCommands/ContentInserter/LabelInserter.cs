﻿using Eurolook.DocumentProcessing.OpenXml;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickCommands.ContentInserter
{
    public class LabelInserter : TextInserter
    {
        public string LabelAlias { get; set; }

        public ILocalisedResourceResolver LocalisedResourceResolver { get; set; }

        protected override Range DoInsert(Range range)
        {
            Text = GetLocalizedLabel();

            base.DoInsert(range);

            return range;
        }

        private string GetLocalizedLabel()
        {
            string alias = LabelAlias;
            return LocalisedResourceResolver.ResolveTranslation(alias)?.Value ?? "";
        }
    }
}
