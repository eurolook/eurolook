﻿namespace Eurolook.WordAddIn.BrickCommands.ContentInserter
{
    public enum ClipboardFormat
    {
        None,
        Bitmap,
        EnhancedMetafile,

        /// <summary>
        /// The format used by Word when using standard paste
        /// </summary>
        Default,
    }
}
