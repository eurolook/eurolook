﻿using System;
using Eurolook.DocumentProcessing.Extensions;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickCommands.ContentInserter
{
    public class ContentControlPositionProvider : IInsertionPositionProvider
    {
        public ContentControlPositionProvider(ContentControl contentControl)
        {
            if (contentControl == null)
            {
                throw new ArgumentNullException(nameof(contentControl));
            }

            ContentControl = contentControl;
        }

        public ContentControl ContentControl { get; }

        public int Start => ContentControl.Range.Start;

        public int End => ContentControl.Range.End;

        public Range GetInsertionPosition(ContentInsertionPosition insertAt)
        {
            var result = ContentControl.Range;

            switch (insertAt)
            {
                case ContentInsertionPosition.End:
                    result.Collapse(WdCollapseDirection.wdCollapseEnd);
                    break;
                case ContentInsertionPosition.Begin:
                    result.Collapse(WdCollapseDirection.wdCollapseStart);
                    break;
                case ContentInsertionPosition.Before:
                    result.Collapse(WdCollapseDirection.wdCollapseStart);
                    result.Move(WdUnits.wdCharacter, -1);
                    break;
                case ContentInsertionPosition.After:
                    result.Collapse(WdCollapseDirection.wdCollapseEnd);
                    result.Move(WdUnits.wdCharacter, 1);
                    break;
                case ContentInsertionPosition.Replace:
                    result = ContentControl.GetControlRange();
                    break;
                case ContentInsertionPosition.ReplaceContent:
                    break;
                default:
                    throw new NotSupportedException();
            }

            return result;
        }
    }
}
