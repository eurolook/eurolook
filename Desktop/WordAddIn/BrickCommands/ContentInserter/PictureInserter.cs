﻿using System.Collections.Generic;
using Microsoft.CSharp.RuntimeBinder;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickCommands.ContentInserter
{
    public class PictureInserter : ContentInserterBase
    {
        private static readonly Dictionary<ClipboardFormat, WdPasteDataType> PictureFormatMap
            = new Dictionary<ClipboardFormat, WdPasteDataType>
            {
                { ClipboardFormat.Bitmap, WdPasteDataType.wdPasteBitmap },
                { ClipboardFormat.EnhancedMetafile, WdPasteDataType.wdPasteEnhancedMetafile },
            };

        public enum PictureSource
        {
            File,
            Clipboard,
        }

        public string FileName { get; set; }

        public string AlternativeText { get; set; }

        public bool LockAspectRatio { get; set; }

        public bool MarkAsDecorative { get; set; }

        public PictureSource Source { get; set; } = PictureSource.File;

        public ClipboardFormat Format { get; set; } = ClipboardFormat.Bitmap;

        protected override Range DoInsert(Range range)
        {
            if (Source == PictureSource.File)
            {
                range.Text = "";
                var shape = range.InlineShapes.AddPicture(FileName);
                SetInlineShapeProperties(shape);
            }
            else
            {
                if (PictureFormatMap.ContainsKey(Format))
                {
                    var wdPasteDataType = PictureFormatMap[Format];
                    range.PasteSpecial(DataType: wdPasteDataType, Placement: WdOLEPlacement.wdInLine);
                }
                else
                {
                    range.PasteSpecial(Placement: WdOLEPlacement.wdInLine);
                }

                if (range.InlineShapes.Count == 0)
                {
                    range.MoveEnd(WdUnits.wdCharacter, 1);
                }

                if (range.InlineShapes.Count == 0)
                {
                    range.MoveStart(WdUnits.wdCharacter, -1);
                }

                if (range.InlineShapes.Count == 1)
                {
                    SetInlineShapeProperties(range.InlineShapes[1]);
                }
            }

            return range;
        }

        private void SetInlineShapeProperties(InlineShape shape)
        {
            if (!MarkAsDecorative)
            {
                shape.AlternativeText = AlternativeText;
            }
            else
            {
                try
                {
                    ((dynamic)shape).Decorative = MsoTriState.msoTrue;
                }
                catch (RuntimeBinderException)
                {
                    // silent catch: Property does not exist in Word 2016 or prior
                }
            }

            shape.LockAspectRatio = LockAspectRatio ? MsoTriState.msoTrue : MsoTriState.msoFalse;
        }
    }
}
