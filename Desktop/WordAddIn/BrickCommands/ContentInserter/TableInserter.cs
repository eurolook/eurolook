﻿using System;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Microsoft.CSharp.RuntimeBinder;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickCommands.ContentInserter
{
    public class TableInserter : ContentInserterBase, ICanLog
    {
        public TableInserter(string neutralStyleName)
        {
            TableStyleName = neutralStyleName;
        }

        public string TableStyleName { get; set; }
        public string TableTextStyleName { get; set; }

        public int Columns { get; set; }
        public int Rows { get; set; }
        public bool HasHeaderRow { get; set; }
        public bool HasTotalRow { get; set; }
        public bool HasFirstColumn { get; set; }
        public bool HasBandingRows { get; set; }
        public string Description { get; set; }

        protected override Range DoInsert(Range range)
        {
            range = HandleBlockLevelContentControls(range);
            bool isInContentControl = range.Paragraphs.Count > 0 && range.IsCoveringContentControl();
            if (isInContentControl)
            {
                range.InsertParagraphAfterSafe();
            }

            var table = range.Tables.Add(range, Rows, Columns, WdDefaultTableBehavior.wdWord9TableBehavior);
            var style = LanguageIndependentStyle.FromNeutralStyleName(TableStyleName, range.Document);
            if (style != null)
            {
                table.set_Style(style.Style);
            }

            style = LanguageIndependentStyle.FromNeutralStyleName(TableTextStyleName, range.Document);
            if (style != null)
            {
                foreach (Paragraph p in table.Range.Paragraphs)
                {
                    p.set_Style(style.Style);
                }
            }

            table.ApplyStyleHeadingRows = HasHeaderRow;
            table.Rows.First.HeadingFormat = HasHeaderRow ? -1 : 0;         // Accessibility requirement: repeat header row on each page
            table.Rows.AllowBreakAcrossPages = 0;                           // Accessibility requirement: disallow rows breaking across pages
            table.ApplyStyleRowBands = HasBandingRows;
            table.ApplyStyleLastRow = HasTotalRow;
            table.ApplyStyleFirstColumn = HasFirstColumn;

            try
            {
                ((dynamic)table).Descr = Description;
            }
            catch (NotImplementedException)
            {
                // ignore on old Word versions
            }
            catch (RuntimeBinderException)
            {
                // ignore on old Word versions
            }

            if (isInContentControl)
            {
                // remove the unnecessary paragraph
                var r = range.Paragraphs.First.Range;
                r.MoveStart(WdUnits.wdCharacter, 1); // move after content control border
                r.Delete();
            }

            return table.Range;
        }

        private static Range HandleBlockLevelContentControls(Range range)
        {
            return range.GetPositionRelativeToBlockLevelContentControl() switch
            {
                RangeExtensions.RangePosition.BeforeBlockLevelContentControl => range.InsertParagraphBeforeAndHandleLockedContentControls(),
                RangeExtensions.RangePosition.AfterBlockLevelContentControl => range.InsertParagraphAfterAndHandleLockedContentControls(),
                _ => range,
            };
        }
    }
}
