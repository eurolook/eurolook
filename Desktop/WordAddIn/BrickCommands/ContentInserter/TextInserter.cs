﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickCommands.ContentInserter
{
    public class TextInserter : ContentInserterBase
    {
        public string Text { get; set; }

        public bool IsItalic { get; set; }

        protected override Range DoInsert(Range range)
        {
            var textSegmentParser = new TextSegmentParser();
            var textParts = textSegmentParser.ParseFields(Text);

            var rangeTmp = range.Duplicate;
            foreach (var textPart in textParts)
            {
                rangeTmp = textPart.Insert(rangeTmp);
                rangeTmp.Collapse(WdCollapseDirection.wdCollapseEnd);
            }

            range.End = rangeTmp.End;
            range.Font.Reset();

            if (IsItalic)
            {
                range.Font.Italic = 1;
            }

            return range;
        }

        public interface ITextSegment
        {
            Range Insert(Range range);
        }

        public class TextSegmentParser
        {
            public List<ITextSegment> ParseFields(string text)
            {
                var result = new List<ITextSegment>();
                var currentTextSegment = new StringBuilder();
                int fieldCount = 0;
                for (int i = 0; i < text.Length; i++)
                {
                    char c = text[i];
                    switch (c)
                    {
                        case '{':
                            if ((fieldCount == 0) && (currentTextSegment.Length > 0))
                            {
                                result.Add(new TextSegment(currentTextSegment.ToString()));
                                currentTextSegment = new StringBuilder();
                            }
                            else if (fieldCount > 0)
                            {
                                currentTextSegment.Append(c);
                            }

                            fieldCount++;
                            break;
                        case '}':
                            fieldCount--;

                            if ((fieldCount == 0) && (currentTextSegment.Length > 0))
                            {
                                result.Add(new FieldSegment(currentTextSegment.ToString()));
                                currentTextSegment = new StringBuilder();
                            }
                            else if (fieldCount > 0)
                            {
                                currentTextSegment.Append(c);
                            }

                            break;
                        case '\\':
                            // backslash escapes curly braces
                            char nextChar = i < text.Length - 1 ? text[i + 1] : '\0';
                            if (nextChar == '{' || nextChar == '}')
                            {
                                currentTextSegment.Append(nextChar);
                                i++;
                            }
                            else
                            {
                                currentTextSegment.Append(c);
                            }

                            break;
                        default:
                            currentTextSegment.Append(c);
                            break;
                    }
                }

                if (currentTextSegment.Length > 0)
                {
                    result.Add(new TextSegment(currentTextSegment.ToString()));
                }

                return result;
            }
        }

        public class TextSegment : ITextSegment
        {
            public TextSegment(string text)
            {
                Text = text;
            }

            public string Text { get; }

            public Range Insert(Range range)
            {
                range.Text = Text;
                return range;
            }
        }

        public class FieldSegment : ITextSegment
        {
            public FieldSegment(string text)
            {
                string fieldText = text;
                var parts = fieldText.Split(" ".ToCharArray(), 2, StringSplitOptions.RemoveEmptyEntries);

                if (parts.Length > 0)
                {
                    FieldCode = parts[0];
                }

                var textSegmentParser = new TextSegmentParser();

                FieldContent = parts.Length > 1
                    ? textSegmentParser.ParseFields(parts[1].TrimStart())
                    : Enumerable.Empty<ITextSegment>();
            }

            public string FieldCode { get; }

            public IEnumerable<ITextSegment> FieldContent { get; }

            public Range Insert(Range range)
            {
                var rangeTmp = range.Duplicate;
                var field = rangeTmp.Fields.Add(range, WdFieldType.wdFieldEmpty, FieldCode, false);
                rangeTmp = field.Code;
                rangeTmp.Collapse(WdCollapseDirection.wdCollapseEnd);

                foreach (var segment in FieldContent)
                {
                    rangeTmp = segment.Insert(rangeTmp);
                    rangeTmp.Collapse(WdCollapseDirection.wdCollapseEnd);
                }

                field.Update();
                range.End = field.Result.End;

                if (!range.Text.EndsWith("\u0015"))
                {
                    range.End = range.End + 1;
                }

                return range;
            }
        }
    }
}
