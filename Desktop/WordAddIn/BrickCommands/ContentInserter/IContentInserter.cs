﻿using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickCommands.ContentInserter
{
    public interface IContentInserter
    {
        Range Insert(IInsertionPositionProvider insertionPositionProvider);

        ContentInsertionPosition InsertAt { get; set; }
    }
}
