﻿using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickCommands.ContentInserter
{
    public interface IInsertionPositionProvider
    {
        int Start { get; }

        int End { get; }

        Range GetInsertionPosition(ContentInsertionPosition insertAt);
    }
}
