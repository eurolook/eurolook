﻿using System.Runtime.InteropServices;
using Eurolook.Common;
using Eurolook.Common.Log;
using Microsoft.Office.Interop.Excel;
using Range = Microsoft.Office.Interop.Excel.Range;

namespace Eurolook.WordAddIn.BrickCommands.ContentInserter
{
    public class ExcelRangeInserter : ContentInserterBase, ICanLog
    {
        public Range Range { get; set; }

        public string AlternativeText { get; set; }

        protected override Microsoft.Office.Interop.Word.Range DoInsert(Microsoft.Office.Interop.Word.Range range)
        {
            // copy as EMF (vector format)
            Range.CopyPicture(XlPictureAppearance.xlPrinter);
            Retry.OnException<COMException>(() => { PasteExcelImage(range); });

            // alternative: insert chart as PNG
            ////using (var temporaryFile = new TemporaryFile(".png"))
            ////{
            ////    Chart.Export(temporaryFile.FullName, "PNG", true);
            ////    range.InlineShapes.AddPicture(temporaryFile.FullName);
            ////}

            return range;
        }

        private void PasteExcelImage(Microsoft.Office.Interop.Word.Range range)
        {
            range.Paste();
            if (range.InlineShapes.Count == 1)
            {
                range.InlineShapes[1].AlternativeText = AlternativeText;
            }
        }
    }
}
