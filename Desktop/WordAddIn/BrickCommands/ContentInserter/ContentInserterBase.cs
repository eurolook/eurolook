﻿using System;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickCommands.ContentInserter
{
    public abstract class ContentInserterBase : IContentInserter
    {
        public ContentInsertionPosition InsertAt { get; set; } = ContentInsertionPosition.End;

        private IInsertionPositionProvider InsertionPositionProvider { get; set; }

        public Range Insert(IInsertionPositionProvider insertionPositionProvider)
        {
            InsertionPositionProvider = insertionPositionProvider
                                        ?? throw new ArgumentNullException(nameof(insertionPositionProvider));
            var insertionRange = GetInsertionPosition();
            var range = DoInsert(insertionRange.Duplicate);

            return range;
        }

        protected abstract Range DoInsert(Range range);

        protected Range GetInsertionPosition()
        {
            return InsertionPositionProvider.GetInsertionPosition(InsertAt);
        }
    }
}
