﻿using System.Collections.Generic;
using System.Linq;
using Eurolook.DocumentProcessing.Extensions;
using JetBrains.Annotations;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickCommands.ContentInserter
{
    public class ParagraphInserter : ContentInserterBase
    {
        public ParagraphInserter(string neutralStyleName, params IContentInserter[] content)
        {
            ParagraphStyleName = neutralStyleName;
            Content = content ?? Enumerable.Empty<IContentInserter>();
        }

        public string ParagraphStyleName { get; set; }

        public int? SpaceBefore { get; set; }

        public int? SpaceAfter { get; set; }

        public int? FontSize { get; set; }

        [NotNull]
        public IEnumerable<IContentInserter> Content { get; }

        protected override Range DoInsert(Range range)
        {
            range.InsertParagraphAfterSafe();

            var paragraph = range.Paragraphs.First;

            // Set the paragraph style first so that all formatting is already applied when we insert content
            // This is important e.g. when creating the TOC where direct formatting needs to be retained
            // when adding entries to the TOC using `Range.FormattedText`.
            // Applying the style after inserting content might override the formatting of the content
            // because of Word's 50 % rule.
            paragraph.ApplyStyle(ParagraphStyleName);

            var positionProvider = new ParagraphPositionProvider(paragraph, ParagraphStyleName);
            foreach (var contentInserter in Content)
            {
                contentInserter.Insert(positionProvider);
            }

            if (SpaceBefore.HasValue)
            {
                paragraph.SpaceBefore = (float)SpaceBefore;
            }

            if (SpaceAfter.HasValue)
            {
                paragraph.SpaceAfter = (float)SpaceAfter;
            }

            if (FontSize.HasValue)
            {
                paragraph.Range.Font.Size = (float)FontSize;
            }

            return paragraph.Range;
        }
    }
}
