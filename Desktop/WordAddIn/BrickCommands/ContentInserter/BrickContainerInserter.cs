﻿using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickCommands.ContentInserter
{
    public class BrickContainerInserter : ContentInserterBase
    {
        public BrickContainerInserter(CommandBrick brick, params IContentInserter[] content)
        {
            Brick = brick;
            Content = content ?? Enumerable.Empty<IContentInserter>();
        }

        public CommandBrick Brick { get; }

        [NotNull]
        public IEnumerable<IContentInserter> Content { get; }

        protected override Range DoInsert(Range range)
        {
            var container = CreateBrickContainer(range);

            var positionProvider = new ContentControlPositionProvider(container);

            foreach (var contentInserter in Content)
            {
                contentInserter.Insert(positionProvider);
            }

            var containerRange = container.GetControlRange();

            return containerRange;
        }

        private ContentControl CreateBrickContainer(Range range)
        {
            var cc = range.ContentControls.Add(WdContentControlType.wdContentControlRichText, range);
            var brickContainer = new BrickContainer(Brick);
            cc.Title = brickContainer.ContainerDisplayName;
            cc.Tag = brickContainer.ToTag();

            // empty placeholder text
            cc.SetPlaceholderText();
            brickContainer.ContentControl = cc;
            return cc;
        }
    }
}
