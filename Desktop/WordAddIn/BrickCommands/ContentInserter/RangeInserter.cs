﻿using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickCommands.ContentInserter
{
    public class RangeInserter : ContentInserterBase
    {
        public RangeInserter(params IContentInserter[] content)
        {
            Content = content ?? Enumerable.Empty<IContentInserter>();
        }

        [NotNull]
        public IEnumerable<IContentInserter> Content { get; }

        protected override Range DoInsert(Range range)
        {
            var resultRange = range;
            foreach (var contentInserter in Content)
            {
                var positionProvider = new RangePositionProvider(range);
                range = contentInserter.Insert(positionProvider);
            }

            resultRange.SetRange(resultRange.Start, range.End);
            return resultRange;
        }
    }
}
