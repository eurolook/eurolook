﻿using System;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickCommands.ContentInserter
{
    public class RangePositionProvider : IInsertionPositionProvider
    {
        public RangePositionProvider(Range range)
        {
            Range = range ?? throw new ArgumentNullException(nameof(range));
        }

        public Range Range { get; }

        public int Start => Range.Start;

        public int End => Range.End;

        public Range GetInsertionPosition(ContentInsertionPosition insertAt)
        {
            var result = Range.Duplicate;

            switch (insertAt)
            {
                case ContentInsertionPosition.After:
                case ContentInsertionPosition.End:
                    result.Collapse(WdCollapseDirection.wdCollapseEnd);
                    break;
                case ContentInsertionPosition.Before:
                case ContentInsertionPosition.Begin:
                    result.Collapse(WdCollapseDirection.wdCollapseStart);
                    break;
                case ContentInsertionPosition.Replace:
                case ContentInsertionPosition.ReplaceContent:
                    break;
                default:
                    throw new NotSupportedException();
            }

            return result;
        }
    }
}
