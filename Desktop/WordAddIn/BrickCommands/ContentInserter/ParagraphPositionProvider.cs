using System;
using Eurolook.DocumentProcessing.Extensions;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickCommands.ContentInserter
{
    public class ParagraphPositionProvider : IInsertionPositionProvider
    {
        public ParagraphPositionProvider(Paragraph paragraph, string paragraphStyleName)
        {
            if (paragraph == null)
            {
                throw new ArgumentNullException(nameof(paragraph));
            }

            Paragraph = paragraph;
            ParagraphStyleName = paragraphStyleName;
        }

        public Paragraph Paragraph { get; }

        public string ParagraphStyleName { get; }

        public int Start => Paragraph.Range.Start;

        public int End => Paragraph.Range.End;

        public Range GetInsertionPosition(ContentInsertionPosition insertAt)
        {
            var result = Paragraph.Range;

            switch (insertAt)
            {
                case ContentInsertionPosition.End:
                    result.Collapse(WdCollapseDirection.wdCollapseEnd);
                    result.Move(WdUnits.wdCharacter, -1);
                    if (result.IsBeforeOrAfterBlockLevelContentControl())
                    {
                        result.Move(WdUnits.wdCharacter, -1);
                    }

                    break;
                case ContentInsertionPosition.Begin:
                    result.Collapse(WdCollapseDirection.wdCollapseStart);
                    break;
                case ContentInsertionPosition.Before:
                    result.Collapse(WdCollapseDirection.wdCollapseStart);
                    result.Move(WdUnits.wdCharacter, -1);
                    break;
                case ContentInsertionPosition.After:
                    result.Collapse(WdCollapseDirection.wdCollapseEnd);
                    break;
                case ContentInsertionPosition.Replace:
                    // Replace the paragraph first with empty text and set the paragraph style (again)
                    // otherwise the style of the replaced paragraph is not retained and instead the style
                    // of the following paragraph would be used.
                    result.Text = "";
                    result.Paragraphs.First.ApplyStyle(ParagraphStyleName);
                    result.Collapse(WdCollapseDirection.wdCollapseStart);
                    break;
                case ContentInsertionPosition.ReplaceContent:
                    result.MoveEnd(WdUnits.wdCharacter, -1);
                    break;
                default:
                    throw new NotSupportedException();
            }

            return result;
        }
    }
}
