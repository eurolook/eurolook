namespace Eurolook.WordAddIn.BrickCommands.ContentInserter
{
    public enum ContentInsertionPosition
    {
        Begin,
        End,
        Before,
        After,
        Replace,
        ReplaceContent,
    }
}
