﻿using Eurolook.DocumentProcessing.Extensions;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickCommands.ContentInserter
{
    public class PlaceholderInserter : ContentInserterBase
    {
        public PlaceholderInserter(string placeholderText, bool isTemporary = true)
        {
            PlaceholderText = placeholderText;
            IsTemporary = isTemporary;
        }

        public string PlaceholderText { get; }

        public bool IsTemporary { get; }

        protected override Range DoInsert(Range range)
        {
            var placeholderContentControl = range.ContentControls.Add();
            placeholderContentControl.SetPlaceholderText(Text: PlaceholderText);
            placeholderContentControl.Temporary = IsTemporary;

            return placeholderContentControl.GetControlRange();
        }
    }
}
