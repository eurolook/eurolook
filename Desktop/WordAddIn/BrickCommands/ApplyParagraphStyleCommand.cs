﻿using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.BrickCommands
{
    [UsedImplicitly]
    public class ApplyParagraphStyleCommand : ApplyStyleCommandBase
    {
        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (new DocumentAutomationHelper(context, $"Apply {DisplayName} Brick"))
            {
                string styleName = GetStyleName(argument);
                string placeholderText = GetPlaceholderText(argument, $"Type your {DisplayName.ToLower(CultureInfo.CurrentUICulture)} here.");

                var range = context.Selection.Range.Duplicate;
                range.HandleVanishingContentControl();

                if (ForceStyle(argument))
                {
                    if (!LanguageIndependentStyle.IsStyleAvailable(styleName, context.Document))
                    {
                        // TODO: should this be the language specific template?
                        context.Document.CopyStylesFromDocumentModel(new List<string>() { styleName }, context.EurolookDocument.DocumentModel);
                    }

                    await
                        ApplyStyleWithPlaceholder(
                            range,
                            styleName,
                            placeholderText);
                }
                else
                {
                    string styleNameNeutral = IgnoreTextLevels(argument)
                        ? styleName
                        : StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(styleName, range);
                    await
                        ApplyStyleWithPlaceholder(
                            range,
                            styleNameNeutral,
                            placeholderText,
                            true);
                }
            }
        }
    }
}
