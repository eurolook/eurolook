﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.Messages;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.ViewModels;
using Eurolook.WordAddIn.Views;
using GalaSoft.MvvmLight.Messaging;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Language = Eurolook.Data.Models.Language;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    public class AddContactCommand : EditCommandBase
    {
        public static readonly Guid ContactBrickId = new("524CE759-8D54-45A8-8442-ACDF840E1F18");
        private readonly IEurolookWordDocument _eurolookDocument;
        private readonly IAuthorManager _authorManager;
        private readonly IMessageService _messageService;
        private readonly IAuthorRepository _authorRepository;
        private readonly IUserDataRepository _userDataRepository;
        private readonly ITextsRepository _textsRepository;
        private readonly Func<Func<Author, IJobAssignment, ContactOptions, Language, string>, Language, AddContactViewModel> _addContactViewModelFunc;

        public AddContactCommand(
            IEurolookWordDocument eurolookDocument,
            IAuthorManager authorManager,
            IMessageService messageService,
            IAuthorRepository authorRepository,
            IUserDataRepository userDataRepository,
            ITextsRepository textsRepository,
            Func<Func<Author, IJobAssignment, ContactOptions, Language, string>, Language, AddContactViewModel> addContactViewModelFunc)
        {
            _eurolookDocument = eurolookDocument;
            _authorManager = authorManager;
            _messageService = messageService;
            _authorRepository = authorRepository;
            _userDataRepository = userDataRepository;
            _textsRepository = textsRepository;
            _addContactViewModelFunc = addContactViewModelFunc;
        }

        public async Task ExecuteAsync(IBrickExecutionContext context, ContentControl selectedBrick)
        {
            await base.ExecuteAsync(context, null);
        }

        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            var viewModel = _addContactViewModelFunc(CreateContactInfo, _eurolookDocument.Language);
            await viewModel.LoadAsync();
            var addContactWindow = new AddContactWindow(viewModel);

            SelectWriterOrMainAuthor(viewModel, context);

            // show the window
            _messageService.ShowDialog(addContactWindow);
            var author = viewModel.SelectedJob?.Author.Author;
            if (!viewModel.Result || (author == null))
            {
                return;
            }

            using (var documentAutomationHelper = new DocumentAutomationHelper(context, DisplayName))
            {
                var contactContentControl = FindContactContentControl(context);
                if (contactContentControl == null)
                {
                    var documentStructure =
                        _eurolookDocument.DocumentStructures.Find(x => x.Brick.Id == ContactBrickId);
                    if (documentStructure != null)
                    {
                        await _eurolookDocument.InsertBrick(documentStructure.Brick, true);
                        contactContentControl = FindContactContentControl(context);
                    }
                }

                if (contactContentControl != null)
                {
                    DeleteTemporaryChildren(contactContentControl);

                    if (_userDataRepository.AddAuthorToUser(_userDataRepository.GetUser(), author))
                    {
                        Messenger.Default.Send(new AuthorAddedMessage(author.Id));
                    }

                    author = _authorRepository.GetAuthorForDocumentCreation(author.Id, _eurolookDocument.Language);
                    if (author == null)
                    {
                        this.LogError("Cannot add contact as author is null.");
                        return;
                    }

                    var jobAssignmentId = viewModel.SelectedJob?.JobAssignment?.JobAssignment?.Id;
                    var jobAssignment = jobAssignmentId == null
                        ? (IJobAssignment)author
                        : _authorRepository.GetJobAssignmentForDocumentCreation(
                            jobAssignmentId.Value,
                            _eurolookDocument.Language);
                    var contactInfo = CreateContactInfo(
                        author,
                        jobAssignment,
                        viewModel.Options,
                        _eurolookDocument.Language);
                    AddContactText(contactContentControl, contactInfo);

                    UpdateComboList(contactContentControl);

                    var contactBrickRange = contactContentControl.Range;
                    contactBrickRange.Start = contactBrickRange.Start - 1;
                    contactBrickRange.End = contactBrickRange.End + 1;
                    contactBrickRange.SelectInPreferredView(documentAutomationHelper.PreferredViewType);
                }
            }
        }

        private static void UpdateComboList(ContentControl parentControl)
        {
            if ((parentControl.Range.Text.Split((char)11).Length <= 2) && (parentControl.Range.Paragraphs.Count < 2))
            {
                return;
            }

            foreach (ContentControl cc in parentControl.Range.ContentControls)
            {
                if ((cc.Type == WdContentControlType.wdContentControlComboBox) && (cc.DropdownListEntries.Count > 1) &&
                    (cc.Range.Text == cc.DropdownListEntries[1].Value))
                {
                    cc.DropdownListEntries[2].Select();
                    return;
                }
            }
        }

        private static void DeleteTemporaryChildren(ContentControl parent)
        {
            foreach (ContentControl cc in parent.Range.ContentControls)
            {
                if (cc.Temporary)
                {
                    cc.Delete();
                }
            }
        }

        [CanBeNull]
        private static ContentControl FindContactContentControl(IBrickExecutionContext context)
        {
            // in range
            foreach (ContentControl cc in context.Selection.Range.ContentControls)
            {
                if (IsContactBrick(cc))
                {
                    return cc;
                }
            }

            // parent
            var contentControl = context.Selection.Range.ParentContentControl;
            while (contentControl != null)
            {
                if (IsContactBrick(contentControl))
                {
                    return contentControl;
                }

                contentControl = contentControl.ParentContentControl;
            }

            // whole document
            return context.Document.Range().ContentControls.Cast<ContentControl>().FirstOrDefault(IsContactBrick);
        }

        private static bool IsContactBrick(ContentControl contentControl)
        {
            if ((contentControl == null) || string.IsNullOrEmpty(contentControl.Tag))
            {
                return false;
            }

            var currentBrickContainer = BrickContainer.FromTag(contentControl.Tag);
            return (currentBrickContainer != null) && (currentBrickContainer.Id == ContactBrickId);
        }

        private static string ConcatenateText(string first, string second, string delimiter = " ")
        {
            if (string.IsNullOrEmpty(first))
            {
                return string.IsNullOrEmpty(second) ? string.Empty : second;
            }

            if (string.IsNullOrEmpty(second))
            {
                return first;
            }

            return first + delimiter + second;
        }

        private static string GetEntityName(IJobAssignment jobAssignment, int level)
        {
            var entity = jobAssignment.OrgaEntity;
            while (entity != null)
            {
                if (entity.LogicalLevel == level)
                {
                    return entity.Header?.ToString().Replace("<>", " ") ?? "";
                }

                entity = entity.SuperEntity;
            }

            return "";
        }

        private static Workplace GetMainWorkplace(Author author)
        {
            return author.Workplaces.FirstOrDefault(wp => wp.Id == author.MainWorkplaceId);
        }

        private static string GetFunction(IJobAssignment jobAssignment, Language lang)
        {
            if (jobAssignment.PredefinedFunction?.Function != null)
            {
                return jobAssignment.PredefinedFunction.Function.Value;
            }

            var authorFunction = jobAssignment.Functions.FirstOrDefault(f => f.LanguageId == lang.Id);
            if ((authorFunction == null) || string.IsNullOrEmpty(authorFunction.Function))
            {
                authorFunction = jobAssignment.Functions.FirstOrDefault(af => af.Language.Name == "EN");
            }

            var function = authorFunction != null ? authorFunction.Function : "";
            return function;
        }

        private static string RemoveContentGroup(string contact, string grouptext)
        {
            var replaceContent = new Regex($@"\[([^\]]*){grouptext}([^\]]*)\]");
            return replaceContent.Replace(contact, "");
        }

        private static string ReplaceContentGroup(string contact, string grouptext, string replacement)
        {
            var replaceContent = new Regex($@"\[([^\]]*){grouptext}([^\]]*)\]");
            return replaceContent.Replace(contact, "$1" + replacement + "$2");
        }

        private void SelectWriterOrMainAuthor(AddContactViewModel viewModel, IBrickExecutionContext context)
        {
            var authors = _authorManager.GetAuthorCustomXmlDocs(context.Document).ToArray();
            var authorXml = authors.FirstOrDefault(
                                a => string.Equals(
                                    a.AuthorRoleName,
                                    "Writer",
                                    StringComparison.InvariantCultureIgnoreCase))
                            ?? authors.FirstOrDefault();

            if (authorXml != null)
            {
                var authorsAndJobs = viewModel.AuthorsAndJobsViewModel.AuthorsAndAllJobs;
                var creator = authorsAndJobs.FirstOrDefault(a => a.Author.Author?.Id == authorXml.AuthorId);
                viewModel.AuthorAndAllJobs = creator ?? authorsAndJobs.First();
                viewModel.SelectedJob =
                    viewModel.AuthorAndAllJobs.All.FirstOrDefault(
                        j => j.JobAssignment.JobAssignmentId == authorXml.JobAssignmentId)
                    ?? viewModel.AuthorAndAllJobs.All.First();
            }
        }

        private void AddContactText(ContentControl contactContentControl, string contactInfo)
        {
            if (contactContentControl.Range.Text.EndsWith(((char)11).ToString(CultureInfo.InvariantCulture)))
            {
                contactContentControl.Range.InsertAfter(contactInfo);
            }
            else
            {
                contactContentControl.Range.InsertAfter((char)11 + contactInfo);
            }
        }

        private string CreateContactInfo(
            Author author,
            [NotNull] IJobAssignment jobAssignment,
            ContactOptions contactOptions,
            Language language)
        {
            var contactSkeleton = GetText("ContactTextPattern");

            var name = string.Empty;
            if (language.Name.Equals("BG", StringComparison.InvariantCultureIgnoreCase))
            {
                name = author.BulgarianFullNameWithLastNameFirst;
            }
            else if (language.Name.Equals("EL", StringComparison.InvariantCultureIgnoreCase))
            {
                name = author.GreekFullNameWithLastNameFirst;
            }

            if (name.IsNullOrEmpty())
            {
                name = author.FullNameWithLastNameFirst;
            }

            var contact = contactSkeleton.Replace("%Name%", name);

            var wp = GetMainWorkplace(author);
            var office = ConcatenateText(GetText("ContactOffice"), wp.Office);

            var phone = string.IsNullOrEmpty(wp.PhoneExtension)
                ? ""
                : ConcatenateText(GetText("ContactTel"), wp.Address.PhoneNumberPrefix + wp.PhoneExtension);

            var fax = string.IsNullOrEmpty(wp.FaxExtension)
                ? ""
                : ConcatenateText(GetText("ContactFax"), wp.Address.PhoneNumberPrefix + wp.FaxExtension);

            var function = GetFunction(jobAssignment, _eurolookDocument.Language);
            var dg = GetEntityName(jobAssignment, 1);
            var dir = GetEntityName(jobAssignment, 2);
            var unit = GetEntityName(jobAssignment, 3);

            contact = contactOptions.HasFlag(ContactOptions.Service) && !string.IsNullOrEmpty(function)
                ? ReplaceContentGroup(contact, "%Function%", function)
                : RemoveContentGroup(contact, "%Function%");
            contact = contactOptions.HasFlag(ContactOptions.Office) && !string.IsNullOrEmpty(office)
                ? ReplaceContentGroup(contact, "%Office%", office)
                : RemoveContentGroup(contact, "%Office%");
            contact = contactOptions.HasFlag(ContactOptions.Phone) && !string.IsNullOrEmpty(phone)
                ? ReplaceContentGroup(contact, "%Phone%", phone)
                : RemoveContentGroup(contact, "%Phone%");
            contact = contactOptions.HasFlag(ContactOptions.Fax) && !string.IsNullOrEmpty(fax)
                ? ReplaceContentGroup(contact, "%Fax%", fax)
                : RemoveContentGroup(contact, "%Fax%");
            contact = contactOptions.HasFlag(ContactOptions.Email) && !string.IsNullOrEmpty(author.Email)
                ? ReplaceContentGroup(contact, "%Email%", author.Email)
                : RemoveContentGroup(contact, "%Email%");
            contact = contactOptions.HasFlag(ContactOptions.Dg) && !string.IsNullOrEmpty(dg)
                ? ReplaceContentGroup(contact, "%Dg%", dg)
                : RemoveContentGroup(contact, "%Dg%");
            contact = contactOptions.HasFlag(ContactOptions.Directorate) && !string.IsNullOrEmpty(dir)
                ? ReplaceContentGroup(contact, "%Directorate%", dir)
                : RemoveContentGroup(contact, "%Directorate%");
            contact = contactOptions.HasFlag(ContactOptions.Unit) && !string.IsNullOrEmpty(unit)
                ? ReplaceContentGroup(contact, "%Unit%", unit)
                : RemoveContentGroup(contact, "%Unit%");

            return contact;
        }

        private string GetText(string textAlias)
        {
            var t = _textsRepository.GetTranslation(textAlias, _eurolookDocument.Language);
            return t == null ? string.Empty : t.Value;
        }
    }
}
