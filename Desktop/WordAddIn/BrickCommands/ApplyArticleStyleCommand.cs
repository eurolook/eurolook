using System.Linq;
using Eurolook.Data.Xml;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using JetBrains.Annotations;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    [UsedImplicitly]
    public class ApplyArticleStyleCommand : ApplyStyleCommandBase
    {
        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (new DocumentAutomationHelper(context, $"Apply {DisplayName} Brick"))
            {
                var range = context.Selection.Range.Duplicate;
                range.HandleVanishingContentControl();

                ApplyStyleWithPlaceholder(range, "Article", "Type your article here.");

                var paragraphs = range.Paragraphs.Cast<Paragraph>().ToList();

                foreach (var paragraph in paragraphs)
                {
                    var rangeStart = paragraph.Range.Duplicate;
                    rangeStart.Collapse(WdCollapseDirection.wdCollapseStart);
                    if (rangeStart.IsEndOfRowMark)
                    {
                        continue;
                    }

                    InsertContentControlAndListNumberField(paragraph);
                }
            }

            return Task.FromResult(0);
        }

        private static void InsertContentControlAndListNumberField(Paragraph paragraph)
        {
            var startOfParagraph = paragraph.Range.Duplicate;

            startOfParagraph.Collapse(WdCollapseDirection.wdCollapseStart);
            startOfParagraph.InsertAfter("\t");

            startOfParagraph.Collapse(WdCollapseDirection.wdCollapseStart);
            startOfParagraph.Document.Fields.Add(startOfParagraph, WdFieldType.wdFieldEmpty, "LISTNUM  \"EurolookHeading\" \\l 1 ", false);

            startOfParagraph.Collapse(WdCollapseDirection.wdCollapseStart);
            startOfParagraph.InsertAfter(" ");

            startOfParagraph.Collapse(WdCollapseDirection.wdCollapseStart);

            if (startOfParagraph.Document.HasContentControlSupport())
            {
                var selectedRange = paragraph.Application.Selection.Range.Duplicate;
                try
                {
                    startOfParagraph.Select();

                    var articleTextControl = startOfParagraph.ContentControls.Add(
                        WdContentControlType.wdContentControlText,
                        startOfParagraph);

                    articleTextControl.XMLMapping.SetMapping("/Texts/Article");
                    articleTextControl.MultiLine = false;
                    articleTextControl.LockContents = false;
                    articleTextControl.Title = "Article";
                }
                finally
                {
                    selectedRange.Select();
                }
            }
            else
            {
                // NOTE: fallback for documents in binary *.doc format because for binary documents
                // ContentControls.Add is not supported. We resolve the article text from the binding
                // and insert the value as text
                var customXmlPart = startOfParagraph.Document.CustomXMLParts.Cast<CustomXMLPart>()
                                                    .FirstOrDefault(part => part.DocumentElement.BaseName == TextsCustomXml.RootName);
                if (customXmlPart != null)
                {
                    string alias = "ArticleText";
                    var node = customXmlPart.SelectSingleNode($"/Texts/{alias}");
                    if (node != null)
                    {
                        startOfParagraph.Text = node.Text;
                    }
                }
            }
        }
    }
}
