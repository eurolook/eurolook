using System.Windows;

namespace Eurolook.WordAddIn.BrickCommands.Placeholders.Text
{
    public partial class InsertTextPlaceholderWindow : Window
    {
        public InsertTextPlaceholderWindow(InsertTextPlaceholderViewModel viewModel)
        {
            ViewModel = viewModel;
            InitializeComponent();
            DataContext = ViewModel;
            ViewModel.OkAction = () =>
                                 {
                                     DialogResult = true;
                                     Close();
                                 };
            ViewModel.CancelAction = Close;
        }

        public InsertTextPlaceholderViewModel ViewModel { get; }
    }
}
