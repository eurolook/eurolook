﻿using Eurolook.AddIn.Common;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.BrickCommands.Placeholders.Text
{
    // Dummy class for backwards compatibility - can be removed when every client in use includes InsertTextPlaceholderCommand
    [UsedImplicitly]
    public class InsertPlaceholderCommand : InsertTextPlaceholderCommand
    {
        public InsertPlaceholderCommand(IDocumentManager documentManager, IMessageService messageService)
            : base(documentManager, messageService)
        {
        }
    }
}
