﻿using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.WordAddIn.BrickEngine;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.BrickCommands.Placeholders.Text
{
    [UsedImplicitly]
    public class InsertTextPlaceholderCommand : PlaceholderCommandBase
    {
        private readonly IDocumentManager _documentManager;
        private readonly IMessageService _messageService;

        public InsertTextPlaceholderCommand(IDocumentManager documentManager, IMessageService messageService)
        {
            _documentManager = documentManager;
            _messageService = messageService;
        }

        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            var viewModel = GetViewModel();
            viewModel.Reset();
            viewModel.WindowTitle = $"Eurolook - Insert {DisplayName}";
            viewModel.DialogTitle = $"Insert {DisplayName}";

            var window = new InsertTextPlaceholderWindow(viewModel);

            if (_messageService.ShowDialog(window) == true)
            {
                InsertPlaceholder(
                    context,
                    PlaceholderType.PlainText,
                    $"[{viewModel.PlaceholderText}]",
                    viewModel.IsTextHighlighted);
            }

            return Task.CompletedTask;
        }

        private InsertTextPlaceholderViewModel GetViewModel()
        {
            var documentViewModel = _documentManager.GetActiveDocumentViewModel();
            return documentViewModel.InsertTextPlaceholderViewModel;
        }
    }
}
