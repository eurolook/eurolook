﻿using System;
using System.Collections.Generic;
using Eurolook.AddIn.Common;
using Eurolook.WordAddIn.ViewModels;
using GalaSoft.MvvmLight.CommandWpf;

namespace Eurolook.WordAddIn.BrickCommands.Placeholders.Text
{
    public class InsertTextPlaceholderViewModel : ValidationViewModelBase
    {
        // ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
        private readonly ISettingsService _settingsService;

        private string _placeholderText;
        private string _dialogTitle;
        private string _windowTitle;
        private bool _isTextHighlighted;

        public InsertTextPlaceholderViewModel(ISettingsService settingsService)
        {
            _settingsService = settingsService;

            ValidationMap = new Dictionary<string, Action>
            {
                {
                    nameof(PlaceholderText), () => ValidateNullOrWhiteSpace(nameof(PlaceholderText), PlaceholderText)
                }
            };
            OkCommand = new RelayCommand(OkClick);
            CancelCommand = new RelayCommand(CancelClick);

            GetHelpCommand = new RelayCommand(() => _settingsService.OpenHelpLink("TemplateDesignBricks"));
        }

        public string PlaceholderText
        {
            get => _placeholderText;
            set => Set(() => PlaceholderText, ref _placeholderText, value);
        }

        public string WindowTitle
        {
            get => _windowTitle;
            set => Set(() => WindowTitle, ref _windowTitle, value);
        }

        public string DialogTitle
        {
            get => _dialogTitle;
            set => Set(() => DialogTitle, ref _dialogTitle, value);
        }

        public bool IsTextHighlighted
        {
            get => _isTextHighlighted;
            set => Set(() => IsTextHighlighted, ref _isTextHighlighted, value);
        }

        public RelayCommand OkCommand { get; set; }

        public RelayCommand CancelCommand { get; set; }

        public Action OkAction { get; set; }

        public Action CancelAction { get; set; }

        public RelayCommand GetHelpCommand { get; }

        protected override Dictionary<string, Action> ValidationMap { get; }

        public void Reset()
        {
            _placeholderText = string.Empty;
            Error = null;
            SubmitAttempted = false;
        }

        private void CancelClick()
        {
            CancelAction?.Invoke();
        }

        private void OkClick()
        {
            if (Validate())
            {
                OkAction?.Invoke();
            }
        }
    }
}
