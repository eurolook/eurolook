using System.Windows;

namespace Eurolook.WordAddIn.BrickCommands.Placeholders.Date
{
    public partial class InsertDatePlaceholderWindow : Window
    {
        public InsertDatePlaceholderWindow(InsertDatePlaceholderViewModel viewModel)
        {
            ViewModel = viewModel;
            InitializeComponent();
            DataContext = ViewModel;
            ViewModel.OkAction = () =>
                                 {
                                     DialogResult = true;
                                     Close();
                                 };
            ViewModel.CancelAction = Close;
        }

        public InsertDatePlaceholderViewModel ViewModel { get; }
    }
}
