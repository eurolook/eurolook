﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Eurolook.AddIn.Common;
using Eurolook.WordAddIn.ViewModels;
using GalaSoft.MvvmLight.CommandWpf;

namespace Eurolook.WordAddIn.BrickCommands.Placeholders.Date
{
    public class InsertDatePlaceholderViewModel : ValidationViewModelBase
    {
        // ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
        private readonly ISettingsService _settingsService;

        private string _placeholderText;
        private PlaceholderTypeViewModel _selectedPlaceholderType;
        private string _dialogTitle;
        private string _windowTitle;
        private bool _isTextHighlighted;

        public InsertDatePlaceholderViewModel(IEurolookWordDocument eurolookDocument, ISettingsService settingsService)
        {
            _settingsService = settingsService;

            ValidationMap = new Dictionary<string, Action>
            {
                {
                    nameof(PlaceholderText), () => ValidateNullOrWhiteSpace(nameof(PlaceholderText), PlaceholderText)
                }
            };

            var documentLanguageCultureInfo = eurolookDocument.Language.CultureInfo;
            DateFormats = new ObservableCollection<PlaceholderTypeViewModel>
            {
                new(
                    $"Short - {DateTime.Now.ToString("dd/MM/yyyy", documentLanguageCultureInfo)}",
                    PlaceholderType.ShortDate),
                new(
                    $"Long - {DateTime.Now.ToString("d MMMM yyyy", documentLanguageCultureInfo)}",
                    PlaceholderType.LongDate)
            };

            SelectedPlaceholderType = DateFormats.Last();

            OkCommand = new RelayCommand(OkClick);
            CancelCommand = new RelayCommand(CancelClick);

            GetHelpCommand = new RelayCommand(() => _settingsService.OpenHelpLink("TemplateDesignBricks"));
        }

        public string PlaceholderText
        {
            get => _placeholderText;
            set => Set(() => PlaceholderText, ref _placeholderText, value);
        }

        public string WindowTitle
        {
            get => _windowTitle;
            set => Set(() => WindowTitle, ref _windowTitle, value);
        }

        public string DialogTitle
        {
            get => _dialogTitle;
            set => Set(() => DialogTitle, ref _dialogTitle, value);
        }

        public ObservableCollection<PlaceholderTypeViewModel> DateFormats { get; set; }

        public PlaceholderTypeViewModel SelectedPlaceholderType
        {
            get => _selectedPlaceholderType;
            set => Set(() => SelectedPlaceholderType, ref _selectedPlaceholderType, value);
        }

        public bool IsTextHighlighted
        {
            get => _isTextHighlighted;
            set => Set(() => IsTextHighlighted, ref _isTextHighlighted, value);
        }

        public RelayCommand OkCommand { get; set; }

        public RelayCommand CancelCommand { get; set; }

        public Action OkAction { get; set; }

        public Action CancelAction { get; set; }

        public RelayCommand GetHelpCommand { get; }

        protected override Dictionary<string, Action> ValidationMap { get; }

        private void CancelClick()
        {
            CancelAction?.Invoke();
        }

        private void OkClick()
        {
            if (Validate())
            {
                OkAction?.Invoke();
            }
        }
    }
}
