﻿using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.WordAddIn.BrickEngine;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.BrickCommands.Placeholders.Date
{
    [UsedImplicitly]
    public class InsertDatePlaceholderCommand : PlaceholderCommandBase
    {
        private readonly IDocumentManager _documentManager;
        private readonly IMessageService _messageService;

        public InsertDatePlaceholderCommand(
            IDocumentManager documentManager,
            IMessageService messageService)
        {
            _documentManager = documentManager;
            _messageService = messageService;
        }

        public override bool CanExecute(IBrickExecutionContext context, string argument)
        {
            return context.EurolookDocument.IsEurolookDocument && base.CanExecute(context, argument);
        }

        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            var viewModel = GetViewModel();
            viewModel.WindowTitle = $"Eurolook - Insert {DisplayName}";
            viewModel.DialogTitle = $"Insert {DisplayName}";

            var window = new InsertDatePlaceholderWindow(viewModel);

            if (_messageService.ShowDialog(window) == true)
            {
                using (new DocumentAutomationHelper(context, DisplayName))
                {
                    InsertPlaceholder(
                        context,
                        viewModel.SelectedPlaceholderType.Type,
                        $"[{viewModel.PlaceholderText}]",
                        viewModel.IsTextHighlighted,
                        false);
                }
            }

            return Task.CompletedTask;
        }

        private InsertDatePlaceholderViewModel GetViewModel()
        {
            var documentViewModel = _documentManager.GetActiveDocumentViewModel();
            return documentViewModel.InsertDatePlaceholderViewModel;
        }
    }
}
