﻿namespace Eurolook.WordAddIn.BrickCommands.Placeholders
{
    public enum PlaceholderType
    {
        PlainText,
        ShortDate,
        LongDate
    }

    public class PlaceholderTypeViewModel
    {
        public PlaceholderTypeViewModel(string displayName, PlaceholderType type)
        {
            DisplayName = displayName;
            Type = type;
        }

        public string DisplayName { get; }

        public PlaceholderType Type { get; }

        public override string ToString()
        {
            return DisplayName;
        }
    }
}
