﻿using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.BrickCommands.Configuration;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.BrickEngine;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.BrickCommands.Placeholders.Linked
{
    [UsedImplicitly]
    public class InsertLinkedPlaceholderCommand : PlaceholderCommandBase
    {
        private const string CustomPropertyTag = "EL_CustomProp";

        private readonly IDocumentManager _documentManager;
        private readonly IMessageService _messageService;
        private readonly IUserDataRepository _userDataRepository;
        private readonly IBrickRepository _brickRepository;

        private LinkedPlaceholderConfigurationV1 _configuration;
        private UserSettings _userSettings;
        private InsertLinkedPlaceholderSettings _brickSettings;

        public InsertLinkedPlaceholderCommand(
            IDocumentManager documentManager,
            IMessageService messageService,
            IUserDataRepository userDataRepository,
            IBrickRepository brickRepository)
        {
            _documentManager = documentManager;
            _messageService = messageService;
            _userDataRepository = userDataRepository;
            _brickRepository = brickRepository;
        }

        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            var viewModel = await GetViewModel();
            viewModel.Initialize(_configuration, CustomPropertyTag, _brickSettings);
            viewModel.WindowTitle = $"Eurolook - Insert {DisplayName}";
            viewModel.DialogTitle = $"Insert {DisplayName}";

            var window = new InsertLinkedPlaceholderWindow(viewModel);

            if (_messageService.ShowDialog(window) == true)
            {
                InsertPlaceholder(context, viewModel.SelectedProperty, viewModel.IsTextHighlighted);
            }

            await SaveSettings(viewModel);
        }

        private void InsertPlaceholder(
            IBrickExecutionContext context,
            LinkedPropertyViewModel property,
            bool isTextHighlighted)
        {
            using (new DocumentAutomationHelper(context.Document, DisplayName))
            {
                var customXmlNode = context.Document.CustomXMLParts.GetCustomXmlNode(
                    property.PartId,
                    property.XPath);
                if (customXmlNode == null)
                {
                    this.LogError($"Failed to find custom property node \"{property.XPath}\"");
                }

                var contentControl = InsertPlaceholder(
                    context,
                    property.Type,
                    $"[{property.Name}]",
                    isTextHighlighted,
                    false);
                contentControl.Title = property.Name;

                if (contentControl.XMLMapping.SetMappingByNode(customXmlNode))
                {
                    if (property.IsCustom)
                    {
                        contentControl.Tag = CustomPropertyTag;
                    }

                    this.LogError($"Failed to bind content control to custom property node \"{property.XPath}\"");
                }
            }
        }

        private async Task<InsertLinkedPlaceholderViewModel> GetViewModel()
        {
            LoadConfiguration();
            await LoadSettings();

            var documentViewModel = _documentManager.GetActiveDocumentViewModel();
            return documentViewModel.InsertLinkedPlaceholderViewModel;
        }

        private void LoadConfiguration()
        {
            _configuration ??=
                new BrickConfigurationReader().Read<LinkedPlaceholderConfigurationV1>(Brick.Configuration)
                ?? new LinkedPlaceholderConfigurationV1();
        }

        private async Task SaveSettings(InsertLinkedPlaceholderViewModel vm)
        {
            _brickSettings.RecentlyCreated = vm.RecentlyCreatedProperties
                                               .Select(RecentlyUsedLinkedPropertyDto.FromViewModel)
                                               .ToArray();

            await _brickRepository.SaveBrickSettings(_userSettings.Id, Brick.Id, _brickSettings);
        }

        private async Task LoadSettings()
        {
            _userSettings = await _userDataRepository.GetUserSettingsAsync();
            _brickSettings =
                await _brickRepository.GetBrickSettingsAsync<InsertLinkedPlaceholderSettings>(
                    _userSettings.Id,
                    Brick.Id)
                ?? new InsertLinkedPlaceholderSettings();
        }
    }
}
