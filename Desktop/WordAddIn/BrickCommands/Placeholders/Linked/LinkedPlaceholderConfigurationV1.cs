﻿using System.Collections.Generic;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.BrickCommands.Placeholders.Linked
{
    public class LinkedPlaceholderConfigurationV1
    {
        [UsedImplicitly]
        public List<DocumentProperty> DocumentProperties { get; set; } = new();
    }

    public class DocumentProperty
    {
        public string DisplayName { get; [UsedImplicitly] set; }

        public PlaceholderType Type { get; set; } = PlaceholderType.PlainText;

        public string PartId { get; [UsedImplicitly] set; }

        public string XPath { get; [UsedImplicitly] set; }
    }
}
