﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.BrickCommands.Placeholders.Linked
{
    public class InsertLinkedPlaceholderSettings
    {
        [CanBeNull]
        public RecentlyUsedLinkedPropertyDto[] RecentlyCreated { get; set; } = default;
    }
}
