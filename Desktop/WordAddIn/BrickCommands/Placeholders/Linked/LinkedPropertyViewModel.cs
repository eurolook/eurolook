﻿using System;

namespace Eurolook.WordAddIn.BrickCommands.Placeholders.Linked
{
    public class LinkedPropertyViewModel
    {
        public string Name { get; set; }

        public string Category { get; set; }

        public PlaceholderType Type { get; set; }

        public string XPath { get; set; } = string.Empty;

        public string PartId { get; set; } = string.Empty;

        public bool IsCustom { get; set; }

        public bool IsUsed { get; set; }

        public bool IsDummy { get; set; }

        public bool IsSameProperty(LinkedPropertyViewModel other)
        {
            return IsSameProperty(other.Name, other.Type);
        }

        public bool IsSameProperty(string propertyName, PlaceholderType propertyType)
        {
            return Name.Equals(propertyName, StringComparison.InvariantCulture) && Type.Equals(propertyType);
        }
    }
}
