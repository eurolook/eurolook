﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eurolook.WordAddIn.BrickCommands.Placeholders.Linked
{
    public class RecentlyUsedLinkedPropertyViewModel : LinkedPropertyViewModel
    {
        public DateTime LastUsed { get; set; } = DateTime.MinValue;

        public static RecentlyUsedLinkedPropertyViewModel FromProperty(LinkedPropertyViewModel property, string categoryName = "Recently Created")
        {
            return new RecentlyUsedLinkedPropertyViewModel
            {
                Category = categoryName,
                Name = property.Name,
                Type = property.Type,
                LastUsed = DateTime.UtcNow,
                IsCustom = true,
                IsDummy = true,
            };
        }
    }

    public class RecentlyUsedLinkedPropertyDto
    {
        public string Name { get; set; }
        public PlaceholderType Type { get; set; }
        public DateTime LastUsed { get; set; }

        public static RecentlyUsedLinkedPropertyDto FromViewModel(RecentlyUsedLinkedPropertyViewModel viewModel)
        {
            return new RecentlyUsedLinkedPropertyDto
            {
                Name = viewModel.Name,
                Type = viewModel.Type,
                LastUsed = viewModel.LastUsed,
            };
        }

        public static RecentlyUsedLinkedPropertyViewModel ToViewModel(RecentlyUsedLinkedPropertyDto dto, string categoryName = "Recently Created")
        {
            return new RecentlyUsedLinkedPropertyViewModel
            {
                Category = categoryName,
                Name = dto.Name,
                Type = dto.Type,
                LastUsed = dto.LastUsed,
                IsCustom = true,
                IsDummy = true,
            };
        }
    }
}
