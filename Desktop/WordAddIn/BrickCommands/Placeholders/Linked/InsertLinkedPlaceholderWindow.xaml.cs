﻿using System.Windows;

namespace Eurolook.WordAddIn.BrickCommands.Placeholders.Linked
{
    public partial class InsertLinkedPlaceholderWindow : Window
    {
        public InsertLinkedPlaceholderWindow(InsertLinkedPlaceholderViewModel viewModel)
        {
            ViewModel = viewModel;
            InitializeComponent();
            DataContext = ViewModel;
            ViewModel.OkAction = () =>
                                 {
                                     DialogResult = true;
                                     Close();
                                 };
            ViewModel.CancelAction = Close;
        }

        public InsertLinkedPlaceholderViewModel ViewModel { get; }
    }
}
