﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.WordAddIn.Extensions;
using Eurolook.WordAddIn.Utils;
using Eurolook.WordAddIn.ViewModels;
using GalaSoft.MvvmLight.CommandWpf;
using JetBrains.Annotations;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.BrickCommands.Placeholders.Linked
{
    public class InsertLinkedPlaceholderViewModel : ValidationViewModelBase, ICanLog
    {
        private new const string ErrorIncomplete = "Property name may not be empty.";
        private const string ErrorDuplicate = "A property of the same name already exists.";
        private const string RootElement = "EurolookCustomProperties";

        // ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
        private readonly ISettingsService _settingsService;
        private readonly IMessageService _messageService;
        private readonly IEurolookWordDocument _eurolookDocument;
        private readonly StringNormalizer _stringNormalizer;
        private string _dialogTitle;
        private string _windowTitle;
        private string _searchExpression;
        private string _customPropertyBrickTag;
        private string _newPropertyName;
        private bool _isTextHighlighted;
        private bool _isCreateDialogEnabled;
        private List<LinkedPropertyViewModel> _builtinProperties;
        private List<LinkedPropertyViewModel> _customProperties;
        private List<RecentlyUsedLinkedPropertyViewModel> _recentlyCreatedProperties;
        private TabViewKindModel _selectedTab;
        private LinkedPropertyViewModel _selectedProperty;
        private PlaceholderTypeViewModel _newPropertyType;

        public InsertLinkedPlaceholderViewModel(
            ISettingsService settingsService,
            IMessageService messageService,
            IEurolookDataRepository eurolookDataRepository,
            IEurolookWordDocument eurolookDocument)
        {
            _settingsService = settingsService;
            _messageService = messageService;
            _searchExpression = string.Empty;
            _customPropertyBrickTag = string.Empty;
            _eurolookDocument = eurolookDocument;

            _stringNormalizer = new StringNormalizer(eurolookDataRepository.GetCharacterMappings());

            FilteredProperties = new NotifyTask<List<LinkedPropertyViewModel>>().NotifyOnCompletion(this)
                .Start(UpdateSearchResultAsync);

            var documentLanguageCultureInfo = eurolookDocument.Language.CultureInfo;
            PropertyTypes = new ObservableCollection<PlaceholderTypeViewModel>
            {
                new("Plain Text", PlaceholderType.PlainText),
                new(
                    $"Short Date - {DateTime.Now.ToString("dd/MM/yyyy", documentLanguageCultureInfo)}",
                    PlaceholderType.ShortDate),
                new(
                    $"Long Date - {DateTime.Now.ToString("d MMMM yyyy", documentLanguageCultureInfo)}",
                    PlaceholderType.LongDate)
            };

            NewPropertyType = PropertyTypes.First();

            TabViewKinds = new List<TabViewKindModel>
            {
                new TabViewKindModel
                {
                    DisplayName = "Properties in Document",
                    GetProperties = () => _builtinProperties?.Concat(
                                              _customProperties ?? Enumerable.Empty<LinkedPropertyViewModel>())
                                          ?? new List<LinkedPropertyViewModel>()
                },
                new TabViewKindModel
                {
                    DisplayName = "Recently Created",
                    IsRecentlyUsed = true,
                    GetProperties = () => _recentlyCreatedProperties?.Select(p => p as LinkedPropertyViewModel) ?? new List<LinkedPropertyViewModel>()
                }
            };

            GetHelpCommand = new RelayCommand(() => _settingsService.OpenHelpLink("TemplateDesignBricks"));

            OkCommand = new RelayCommand(OkClick);
            CancelCommand = new RelayCommand(CancelClick);

            ConfirmCreatePropertyCommand = new RelayCommand(() => AddProperty(NewPropertyName, NewPropertyType.Type));
            CancelCreatePropertyCommand = new RelayCommand(CancelCreation);
            ShowCreatePropertyCommand = new RelayCommand(() => IsCreateDialogEnabled = true);

            DeleteCommand = new RelayCommand<LinkedPropertyViewModel>(
                property =>
                {
                    if (SelectedTab.IsRecentlyUsed)
                    {
                        DeletePropertyFromRecentlyCreated(property as RecentlyUsedLinkedPropertyViewModel);
                    }
                    else
                    {
                        DeletePropertyFromDocument(property);
                    }
                });
        }

        public string WindowTitle
        {
            get => _windowTitle;
            set => Set(() => WindowTitle, ref _windowTitle, value);
        }

        public string DialogTitle
        {
            get => _dialogTitle;
            set => Set(() => DialogTitle, ref _dialogTitle, value);
        }

        public string NewPropertyName
        {
            get => _newPropertyName;
            set => Set(() => NewPropertyName, ref _newPropertyName, value);
        }

        public bool IsTextHighlighted
        {
            get => _isTextHighlighted;
            set => Set(() => IsTextHighlighted, ref _isTextHighlighted, value);
        }

        public bool IsCreateDialogEnabled
        {
            get => _isCreateDialogEnabled;
            set => Set(() => IsCreateDialogEnabled, ref _isCreateDialogEnabled, value);
        }

        public RelayCommand GetHelpCommand { get; }

        public RelayCommand OkCommand { get; set; }

        public RelayCommand CancelCommand { get; set; }

        public RelayCommand ShowCreatePropertyCommand { get; set; }

        public RelayCommand ConfirmCreatePropertyCommand { get; set; }

        public RelayCommand CancelCreatePropertyCommand { get; set; }

        public RelayCommand<LinkedPropertyViewModel> DeleteCommand { get; }

        public IEnumerable<LinkedPropertyViewModel> AllProperties =>
            SelectedTab?.GetProperties() ?? new List<LinkedPropertyViewModel>();

        public IEnumerable<RecentlyUsedLinkedPropertyViewModel> RecentlyCreatedProperties => _recentlyCreatedProperties;

        public IEnumerable<TabViewKindModel> TabViewKinds { get; set; }

        public TabViewKindModel SelectedTab
        {
            get => _selectedTab;
            set
            {
                SelectedProperty = null;
                SearchExpression = string.Empty;

                Set(() => SelectedTab, ref _selectedTab, value);
                UpdateSearchResults();
            }
        }

        public string SearchExpression
        {
            get => _searchExpression ?? "";
            set
            {
                Set(() => SearchExpression, ref _searchExpression, value);
                UpdateSearchResults();
            }
        }

        [NotNull]
        public NotifyTask<List<LinkedPropertyViewModel>> FilteredProperties { get; set; }

        public LinkedPropertyViewModel SelectedProperty
        {
            get => _selectedProperty;
            set
            {
                if (_selectedProperty == value)
                {
                    return;
                }

                _selectedProperty = value;

                RaisePropertyChanged(() => SelectedProperty);
                RaisePropertyChanged(() => CanCommit);
            }
        }

        public ObservableCollection<PlaceholderTypeViewModel> PropertyTypes { get; set; }

        public PlaceholderTypeViewModel NewPropertyType
        {
            get => _newPropertyType;
            set => Set(() => NewPropertyType, ref _newPropertyType, value);
        }

        public Action OkAction { get; set; }

        public Action CancelAction { get; set; }

        public bool CanCommit => SelectedProperty is not null;

        protected override Dictionary<string, Action> ValidationMap => new();

        public void Initialize(
            LinkedPlaceholderConfigurationV1 configuration,
            string tag,
            InsertLinkedPlaceholderSettings brickSettings)
        {
            _customPropertyBrickTag = tag;
            if (_builtinProperties == null)
            {
                _builtinProperties = new List<LinkedPropertyViewModel>();
                foreach (var property in configuration.DocumentProperties)
                {
                    _builtinProperties.Add(
                        new LinkedPropertyViewModel
                        {
                            Category = "Builtin Properties",
                            Name = property.DisplayName,
                            Type = property.Type,
                            PartId = property.PartId,
                            XPath = property.XPath,
                            IsCustom = false,
                            IsUsed = false
                        });
                }
            }

            _customProperties = LoadCustomProperties();

            _recentlyCreatedProperties =
                brickSettings.RecentlyCreated?
                    .Select(p => RecentlyUsedLinkedPropertyDto.ToViewModel(p))
                    .ToList() ?? new List<RecentlyUsedLinkedPropertyViewModel>();

            SelectedTab = TabViewKinds.FirstOrDefault();

            SearchExpression = string.Empty;
            Error = null;
            SubmitAttempted = false;
        }

        private static bool IsMatch(string name, IEnumerable<string> searchTerms)
        {
            return searchTerms.All(name.ContainsInvariantIgnoreCase);
        }

        private void UpdateSearchResults()
        {
            FilteredProperties = new NotifyTask<List<LinkedPropertyViewModel>>().NotifyOnCompletion(this)
                .Start(UpdateSearchResultAsync);
            RaisePropertyChanged(() => FilteredProperties);
        }

#pragma warning disable CS1998
        private async Task<List<LinkedPropertyViewModel>> UpdateSearchResultAsync()
#pragma warning restore CS1998
        {
            var normalizedSearchExpression = GetNormalizedSearchExpression();
            var searchTerms = normalizedSearchExpression.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            var result =
                AllProperties.Where(p => !searchTerms.Any() || IsMatch(p.Name, searchTerms))
                             .OrderBy(p => p.Category)
                             .ThenBy(p => p.Name)
                             .ToList();

            var currentSelection = SelectedProperty;

            var orderedResult = result.OrderBy(p => p.Category).ThenBy(p => p.Name).ToList();
            SelectedProperty = orderedResult.FirstOrDefault(p => p.Name == currentSelection?.Name)
                               ?? orderedResult.FirstOrDefault();
            return orderedResult;
        }

        private string GetNormalizedSearchExpression()
        {
            return _stringNormalizer.Normalize(SearchExpression);
        }

        private List<LinkedPropertyViewModel> LoadCustomProperties()
        {
            var result = new List<LinkedPropertyViewModel>();
            var document = _eurolookDocument.Document;

            if (document == null)
            {
                return result;
            }

            var customPropertiesXml = GetCustomPropertiesXml();
            var contentControls = document.ContentControls.OfType<ContentControl>()
                                          .Where(cc => cc.Tag?.Equals(_customPropertyBrickTag) ?? false)
                                          .ToList();

            foreach (var node in customPropertiesXml.DocumentElement.ChildNodes.OfType<CustomXMLNode>().ToList())
            {
                if (node.NodeType != MsoCustomXMLNodeType.msoCustomXMLNodeElement)
                {
                    continue;
                }

                var name = node.GetAttribute("name")?.NodeValue ?? node.BaseName;
                if (!Enum.TryParse(node.GetAttribute("type")?.NodeValue, true, out PlaceholderType type))
                {
                    type = PlaceholderType.PlainText;
                }

                result.Add(
                    new LinkedPropertyViewModel
                    {
                        Name = name,
                        Category = "Custom Properties",
                        Type = type,
                        IsCustom = true,
                        IsUsed = contentControls.Any(
                            cc => cc.XMLMapping.IsMapped && cc.XMLMapping.CustomXMLNode.IsIdenticalWith(node)),
                        PartId = customPropertiesXml.Id,
                        XPath = node.XPath
                    });
            }

            return result;
        }

        private void CancelClick()
        {
            CancelAction?.Invoke();
        }

        private void OkClick()
        {
            if (CanCommit)
            {
                if (SelectedProperty.IsDummy)
                {
                    SelectedProperty = _customProperties.FirstOrDefault(p => p.IsSameProperty(SelectedProperty))
                                       ?? CreateProperty(SelectedProperty.Name, SelectedProperty.Type);
                }

                OkAction?.Invoke();
            }
        }

        private void AddProperty(string propertyName, PlaceholderType propertyType)
        {
            if (Validate())
            {
                var createdProperty = CreateProperty(propertyName, propertyType);

                SearchExpression = string.Empty;
                IsCreateDialogEnabled = false;
                NewPropertyName = string.Empty;
                NewPropertyType = PropertyTypes.First();

                SelectedProperty = createdProperty;
            }
        }

        private LinkedPropertyViewModel CreateProperty(string propertyName, PlaceholderType propertyType)
        {
            var normalizedName = new string(
                _stringNormalizer.Normalize(propertyName).Where(char.IsLetterOrDigit).ToArray());

            var xmlNode = new XElement(
                $"CProp_{normalizedName}");
            xmlNode.Add(new XAttribute("name", propertyName));
            xmlNode.Add(new XAttribute("type", propertyType));
            GetCustomPropertiesXml().DocumentElement.AppendChildSubtree(xmlNode.ToString());
            _customProperties = LoadCustomProperties();

            var property = _customProperties.First(p => p.IsSameProperty(propertyName, propertyType));

            UpdateRecentlyCreatedList(property);

            return property;
        }

        private void UpdateRecentlyCreatedList(LinkedPropertyViewModel property)
        {
            var recentProperty = _recentlyCreatedProperties.FirstOrDefault(p => p.IsSameProperty(property));
            if (recentProperty != null)
            {
                recentProperty.LastUsed = DateTime.UtcNow;
            }
            else
            {
                _recentlyCreatedProperties.Add(RecentlyUsedLinkedPropertyViewModel.FromProperty(property));
                _recentlyCreatedProperties = _recentlyCreatedProperties
                                             .OrderByDescending(p => p.LastUsed)
                                             .Take(20)
                                             .ToList();
            }
        }

        private void CancelCreation()
        {
            IsCreateDialogEnabled = false;
            NewPropertyName = string.Empty;
        }

        private void DeletePropertyFromDocument(LinkedPropertyViewModel linkedProperty)
        {
            if (_messageService.ShowMessageWindow(
                    new MessageViewModel
                    {
                        Title = "Delete Custom Property",
                        Message =
                            $"Do you really want to permanently delete the custom property \"{linkedProperty.Name}\" from this document?",
                        YesButtonText = "Yes",
                        NoButtonText = "No",
                        IsShowNoButton = true,
                        IsShowCancelButton = false
                    },
                    this) == true)
            {
                try
                {
                    var node = _eurolookDocument.Document.CustomXMLParts.GetCustomXmlNode(
                        linkedProperty.PartId,
                        linkedProperty.XPath);
                    if (node != null)
                    {
                        node.Delete();
                        _customProperties.Remove(linkedProperty);
                    }
                }
                catch (Exception ex)
                {
                    this.LogError(ex);
                }
            }

            UpdateSearchResults();
        }

        private void DeletePropertyFromRecentlyCreated(RecentlyUsedLinkedPropertyViewModel linkedProperty)
        {
            _recentlyCreatedProperties.Remove(linkedProperty);
            UpdateSearchResults();
        }

        private bool Validate()
        {
            if (string.IsNullOrWhiteSpace(NewPropertyName))
            {
                Error = ErrorIncomplete;
                return false;
            }

            if (_customProperties.Any(p => p.Name.Equals(NewPropertyName, StringComparison.InvariantCultureIgnoreCase)))
            {
                Error = ErrorDuplicate;
                return false;
            }

            Error = null;
            return true;
        }

        private CustomXMLPart GetCustomPropertiesXml()
        {
            return _eurolookDocument.Document.GetCustomXmlParts(RootElement)
                                    .FirstOrDefault()
                   ?? _eurolookDocument.Document.CustomXMLParts.Add(new XElement(RootElement).ToString());
        }
    }

    public class TabViewKindModel
    {
        public string DisplayName { get; set; }

        public bool IsRecentlyUsed { get; set; }

        public Func<IEnumerable<LinkedPropertyViewModel>> GetProperties { get; set; }

        public override string ToString()
        {
            return DisplayName;
        }

        public override bool Equals(object obj)
        {
            return obj is TabViewKindModel vm
                   && IsRecentlyUsed == vm.IsRecentlyUsed
                   && DisplayName.Equals(vm.DisplayName);
        }

        public override int GetHashCode()
        {
            return DisplayName.GetHashCode();
        }
    }
}
