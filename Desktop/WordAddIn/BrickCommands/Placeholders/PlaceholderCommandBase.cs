﻿using System;
using System.Xml.XPath;
using Eurolook.Common.Log;
using Eurolook.Data.Xml;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.Extensions;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickCommands.Placeholders
{
    public abstract class PlaceholderCommandBase : EditCommandBase, ICanLog
    {
        protected ContentControl InsertPlaceholder(
            IBrickExecutionContext context,
            PlaceholderType type,
            string placeholderText,
            bool highlightPlaceholder,
            bool isVanishing = true)
        {
            var selection = context.Application.Selection;
            if (selection.Range.IsLocked())
            {
                this.LogError("Tried to insert placeholder in locked range.");
                return null;
            }

            if (selection.End - selection.Start > 0)
            {
                selection.Range.Delete();
            }

            // check if the current selection is in another DatePicker content control
            if (selection.Range.IsCoveringContentControl()
                && (selection.Range.ParentContentControl.Type == WdContentControlType.wdContentControlDate))
            {
                selection.Range.ParentContentControl?.GetControlRange().Delete();
            }

            var insertPosition = context.Application.Selection.Range;
            var contentControl = type switch
            {
                PlaceholderType.PlainText => InsertTextContentControl(context, insertPosition),
                PlaceholderType.ShortDate => InsertDateContentControl(context, insertPosition, "Short"),
                PlaceholderType.LongDate => InsertDateContentControl(context, insertPosition, "Long"),
                _ => throw new ArgumentOutOfRangeException(),
            };

            contentControl.Temporary = isVanishing;
            contentControl.SetPlaceholderText(Text: placeholderText);

            if (highlightPlaceholder && contentControl.ShowingPlaceholderText)
            {
                contentControl.Range.HighlightColorIndex = WdColorIndex.wdYellow;
            }

            return contentControl;
        }

        private static string GetLocalizedDateFormat(IBrickExecutionContext context, string format)
        {
            var dateFormatString = "/Texts/DateFormat" + format;
            return context.EurolookDocument
                          .Document
                          .GetCustomXmlAsXDocument(TextsCustomXml.RootName)
                          .XPathSelectElement(dateFormatString)
                          ?.Value ?? "";
        }

        private static ContentControl InsertTextContentControl(IBrickExecutionContext context, Range insertPosition)
        {
            return context.Document.ContentControls.Add(
                WdContentControlType.wdContentControlText,
                insertPosition);
        }

        private static ContentControl InsertDateContentControl(
            IBrickExecutionContext context,
            Range insertPosition,
            string format)
        {
            var contentControl = context.Document.ContentControls.Add(
                WdContentControlType.wdContentControlDate,
                insertPosition);
            contentControl.DateCalendarType = WdCalendarType.wdCalendarWestern;
            contentControl.DateDisplayLocale = context.EurolookDocument.Language.AsWdLanguageId();
            contentControl.DateDisplayFormat = GetLocalizedDateFormat(context, format);

            return contentControl;
        }
    }
}
