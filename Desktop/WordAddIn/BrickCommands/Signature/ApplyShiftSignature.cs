﻿using System;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;

namespace Eurolook.WordAddIn.BrickCommands.Signature
{
    public class ApplyShiftSignature : ApplyStyleCommandBase
    {
        public static readonly Guid SignatureBrickId = new Guid("5f475e9e-166d-4aac-97b3-faffc5be7b09");
        private readonly IMessageService _messageService;

        public ApplyShiftSignature(IMessageService messageService)
        {
            _messageService = messageService;
        }

        public override bool CanExecute(IBrickExecutionContext context, string argument)
        {
            return true;
        }

        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (new DocumentAutomationHelper(context, $"Apply Shift Signature: {argument}"))
            {
                var signatureBrick = context.Document.GetBrickContainer(SignatureBrickId);
                if (signatureBrick != null)
                {
                    var style = LanguageIndependentStyle.FromNeutralStyleName(argument, context.Document);
                    StyleConceptNew.ApplyStyleToRange(style, signatureBrick.ContentControl.Range);
                    foreach (var signatureShape in AddSignatureContentBase.FindSignatureShapes(context))
                    {
                        AddSignatureContentBase.AlignSignatureShapeHorizontally(signatureShape, signatureBrick.ContentControl.Range);
                    }
                }
                else
                {
                    _messageService.ShowSimpleMessage(
                        "Cannot shift the signature. There is no standard signature brick in the document.",
                        DisplayName);
                }
            }

            return Task.FromResult(0);
        }
    }
}
