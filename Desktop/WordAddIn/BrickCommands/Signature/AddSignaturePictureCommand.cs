﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using Eurolook.AddIn.Common;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using JetBrains.Annotations;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using Microsoft.Win32;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickCommands.Signature
{
    [UsedImplicitly]
    public class AddSignaturePictureCommand : AddSignatureContentBase
    {
        protected static readonly string ShapeTag = "EL10 Signature Picture";
        protected override string NoSignatureErrorMessage => "Cannot insert a signature picture. There is no Signature brick in the document.";

        public AddSignaturePictureCommand(IMessageService messageService)
            : base(messageService)
        {
            CommandName = "Insert Signature Picture";
        }

        protected override void InsertContent(IBrickExecutionContext context, Range anchor, string title)
        {
            string pictureFile = BrowseForFile();
            if (pictureFile == null)
            {
                return;
            }

            var image = TryGetImage(pictureFile);
            if (image == null)
            {
                return;
            }

            GetTargetDimension(image, out float heightPt, out float widthPt);

            // try to anchor the picture to the CC hosting the author name, not to the surrounding brick CC.
            // that caused problems in Word 365 (EUROLOOK-4674)
            var contentControls = anchor.Paragraphs.First.Range.ContentControls;
            if (contentControls.Count > 1)
            {
                var nameControl = contentControls[2];
                anchor = nameControl.GetControlRange();
            }

            var inlinePicture = anchor.InlineShapes.AddPicture(pictureFile);
            var shape = inlinePicture.ConvertToShape();
            shape.Width = widthPt;
            shape.Height = heightPt;
            shape.WrapFormat.Type = WdWrapType.wdWrapFront;
            shape.RelativeVerticalPosition = WdRelativeVerticalPosition.wdRelativeVerticalPositionParagraph;
            shape.Top = 0;
            shape.Left = 0;
            shape.Title = title;
            shape.AlternativeText = ShapeTag;
            shape.Line.Visible = MsoTriState.msoFalse;

            AlignSignatureShapeHorizontally(shape, anchor);
        }

        private static void GetTargetDimension(Image image, out float heightPt, out float widthPt)
        {
            double imageHeightCm = PxToCm(image.Height);
            if (imageHeightCm > SignatureAreaHeightCm)
            {
                // when the image height is larger than the max height of the signature area, scale it down
                heightPt = CmToPt(SignatureAreaHeightCm);
                widthPt = GetTargetWidth(image, heightPt);
            }
            else
            {
                heightPt = CmToPt(PxToCm(image.Height));
                widthPt = CmToPt(PxToCm(image.Width));
            }
        }

        private Image TryGetImage(string pictureFile)
        {
            try
            {
                Image picture;
                if (pictureFile.EndsWith(".emf", StringComparison.InvariantCultureIgnoreCase) ||
                    pictureFile.EndsWith(".wmf", StringComparison.InvariantCultureIgnoreCase))
                {
                    picture = new Metafile(pictureFile);
                }
                else
                {
                    picture = new Bitmap(pictureFile);
                }

                return picture;
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                return null;
            }
        }

        private static float GetTargetWidth([NotNull] Image image, float height)
        {
            float factor = image.Width / (float)image.Height;
            return factor * height;
        }

        private static string BrowseForFile()
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = "All Pictures (*.emf;*.wmf;*.jpg;*.jpeg;*.png;*.gif;*.bmp)|*.emf;*.wmf;*.jpg;*.jpeg;*.png;*.gif;*.bmp|" +
                         "Windows Enhanced Metafile (*.emf)|*.emf|" +
                         "Windows Metafile (*.wmf)|*.wmf|" +
                         "JPEG File Interchange Format (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                         "Portable Network Graphics (*.png)|*.png|" +
                         "Graphics Interchange Format (*.gif)|*.gif|" +
                         "Bitmap Format (*.bmp)|*.bmp",
                CheckFileExists = true,
                Multiselect = false,
                Title = "Choose a Signature Picture",
            };

            var result = openFileDialog.ShowDialog();
            return result == true ? openFileDialog.FileName : null;
        }
    }
}
