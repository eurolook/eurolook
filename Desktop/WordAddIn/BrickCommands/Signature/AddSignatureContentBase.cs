﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.AddIn.Common;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands.Signature
{
    public abstract class AddSignatureContentBase : EditCommandBase
    {
        protected const double SignatureAreaHeightCm = 2.0;

        public static readonly string Id0 = "E-Signature";
        public static readonly string Id1 = "E-Signature 1";
        public static readonly string Id2 = "E-Signature 2";
        public static readonly string Id3 = "E-Signature 3";

        public static readonly Guid OldSignatureBrickId = new Guid("5f475e9e-166d-4aac-97b3-faffc5be7b09");
        public static readonly Guid NewSignatureBrickId = new Guid("746af7b3-f86a-4574-98cf-a33ca8c76e77");
        public static readonly Guid DoubleSignatureLeftId = new Guid("f64a88e8-af32-49d0-903c-6c9903482ad1");
        public static readonly Guid DoubleSignatureRightId = new Guid("9a79393c-cb23-4a2e-9d18-36128e32ada8");
        public static readonly Guid ThreeSignatoriesId = new Guid("c2d3db5e-0d21-41bf-a616-f116439950ef");

        public static readonly Guid DecisionLetterSignatureGeneratedId = new Guid("4cccc037-3350-45bb-ae21-2f615e20f3da");
        public static readonly Guid DecisionLetterSignatureDlId = new Guid("36553fdd-ed1b-45f5-858c-65528afdd737");
        public static readonly Guid DecisionLetterSignaturePePoPhId = new Guid("995d8dee-a00d-4017-83a7-62bfed24225d");
        private readonly IMessageService _messageService;

        protected string CommandName { get; set; }

        protected abstract string NoSignatureErrorMessage { get; }

        public AddSignatureContentBase(IMessageService messageService)
        {
            _messageService = messageService;
        }

        public static Shape[] FindSignatureShapes(IBrickExecutionContext context)
        {
            var names = new[] { Id0, Id1, Id2, Id3 };
            var result = new List<Shape>();
            foreach (string name in names)
            {
                result.AddRange(context.Document.Shapes.Cast<Shape>().Where(shape => TryHasName(shape, name)));
            }

            return result.ToArray();
        }

        public static void AlignSignatureShapeHorizontally(Shape shape, Range anchor)
        {
            if (TryHasName(shape, Id0))
            {
                float signatureIndentPt = 0;
                var signatureAlignment = WdParagraphAlignment.wdAlignParagraphCenter;

                if (anchor.Paragraphs.Count > 0)
                {
                    var signatureLine = anchor.Paragraphs.First;
                    if (signatureLine != null)
                    {
                        signatureIndentPt = signatureLine.Format.LeftIndent;
                        signatureAlignment = signatureLine.Format.Alignment;
                    }
                }

                if (signatureIndentPt > 0)
                {
                    // The signature is right-aligned.
                    // We want to position the shape centrally above the signature
                    float areaOffsetPt = signatureIndentPt;
                    float signatureAreaWidthPt = anchor.PageSetup.PageWidth - anchor.PageSetup.LeftMargin
                                                                            - anchor.PageSetup.RightMargin
                                                                            - signatureIndentPt;
                    float shapeWidthPt = shape.Width;

                    // Signature in Decision Letter is left-aligned
                    float imageOffsetPt = 0;
                    if (signatureAlignment != WdParagraphAlignment.wdAlignParagraphLeft)
                    {
                        imageOffsetPt = (signatureAreaWidthPt - shapeWidthPt) / 2;
                    }

                    float indentPt = areaOffsetPt + imageOffsetPt;

                    shape.RelativeHorizontalPosition = WdRelativeHorizontalPosition.wdRelativeHorizontalPositionColumn;
                    shape.Left = indentPt;
                    TrySetParagraphAlignment(shape, WdParagraphAlignment.wdAlignParagraphCenter);
                    return;
                }
            }

            // Default alignment is always left to the character
            shape.RelativeHorizontalPosition = WdRelativeHorizontalPosition.wdRelativeHorizontalPositionCharacter;
            shape.Left = 0;
            TrySetParagraphAlignment(shape, WdParagraphAlignment.wdAlignParagraphLeft);
        }

        protected static BrickContainer FindSignatureBrick(IBrickExecutionContext context)
        {
            var lookupIds = new[]
            {
                OldSignatureBrickId,
                NewSignatureBrickId,
                DoubleSignatureLeftId,
                DoubleSignatureRightId,
                ThreeSignatoriesId,
                DecisionLetterSignatureGeneratedId,
                DecisionLetterSignatureDlId,
                DecisionLetterSignaturePePoPhId,
            };
            return context.Document.GetAllBrickContainers().FirstOrDefault(c => lookupIds.Contains(c.Id));
        }

        protected static float CmToPt(double value)
        {
            return (float)(value * 28.3464567);
        }

        protected static double PxToCm(int pixels)
        {
            return pixels * 2.54 / 96;
        }

        protected static bool ContainsSignatureContent(Document document, string name)
        {
            return document.Shapes.Cast<Shape>().Any(shape => TryHasName(shape, name));
        }

        protected static void RemoveSignatureContent(Document document, string name)
        {
            document.Shapes.Cast<Shape>().First(shape => TryHasName(shape, name)).Delete();
        }

        protected static bool TryHasName(Shape shape, string name)
        {
            try
            {
                if (shape.Name == name)
                {
                    return true;
                }
            }
            catch (Exception)
            {
                // ignored
            }

            try
            {
                // accessing the title causes an COMException in newer Word versions
                if (shape.Title == name)
                {
                    return true;
                }
            }
            catch (Exception)
            {
                // ignored
            }

            return false;
        }

        protected static void TrySetName(Shape shape, string name)
        {
            try
            {
                shape.Name = name;
            }
            catch (Exception)
            {
                // ignored
            }

            try
            {
                shape.Title = name;
            }
            catch (Exception)
            {
                // ignored
            }
        }

        protected abstract void InsertContent(IBrickExecutionContext context, Range anchor, string title);

        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (new DocumentAutomationHelper(context, CommandName))
            {
                var signatureBrick = FindSignatureBrick(context);
                if (signatureBrick == null)
                {
                    _messageService.ShowSimpleMessage(NoSignatureErrorMessage, DisplayName);
                    return Task.FromResult(0);
                }

                if (signatureBrick.ContentControl.Range.Tables.Count > 0)
                {
                    // it's a double signature or a three signatories brick
                    var table = signatureBrick.ContentControl.Range.Tables[1];
                    if ((table.Rows.Count > 0) && (table.Rows[1].Cells.Count > 1))
                    {
                        if (ContainsSignatureContent(context.Document, Id1))
                        {
                            RemoveSignatureContent(context.Document, Id1);
                        }

                        var anchor1 = table.Rows[1].Cells[1].Range.Paragraphs.First.Range;
                        anchor1.Collapse(WdCollapseDirection.wdCollapseStart);

                        // Anchor the text box to the first character inside of the paragraph in the cell -> Move(1).
                        // Otherwise the document might be corrupted (EUROLOOK-2048)
                        anchor1.Move(WdUnits.wdCharacter, 1);
                        InsertContent(context, anchor1, Id1);

                        if (ContainsSignatureContent(context.Document, Id2))
                        {
                            RemoveSignatureContent(context.Document, Id2);
                        }

                        var anchor2 = table.Rows[1].Cells[2].Range.Paragraphs.First.Range;
                        anchor2.Collapse(WdCollapseDirection.wdCollapseStart);
                        InsertContent(context, anchor2, Id2);

                        // return if double signature
                        if (table.Rows[1].Cells.Count <= 2)
                        {
                            return Task.CompletedTask;
                        }

                        // three signatories
                        if (ContainsSignatureContent(context.Document, Id3))
                        {
                            RemoveSignatureContent(context.Document, Id3);
                        }

                        var anchor3 = table.Rows[1].Cells[3].Range.Paragraphs.First.Range;
                        anchor3.Collapse(WdCollapseDirection.wdCollapseStart);
                        InsertContent(context, anchor3, Id3);
                    }
                }
                else
                {
                    // single signature brick
                    if (ContainsSignatureContent(context.Document, Id0))
                    {
                        RemoveSignatureContent(context.Document, Id0);
                    }

                    // Anchor the text box to the first character outside of the content control -> Move(-1).
                    // Otherwise the document might be corrupted (EUROLOOK-2048)
                    var anchor = signatureBrick.ContentControl.Range.Duplicate;
                    anchor.Collapse(WdCollapseDirection.wdCollapseStart);
                    anchor.Move(WdUnits.wdCharacter, -1);
                    InsertContent(context, anchor, Id0);
                }

                return Task.FromResult(0);
            }
        }

        private static void TrySetParagraphAlignment(Shape shape, WdParagraphAlignment alignment)
        {
            try
            {
                shape.TextFrame.TextRange.ParagraphFormat.Alignment = alignment;
            }
            catch (Exception)
            {
                // API throws axception when the shape doesn't support a TextFrame (picture shape)
            }
        }
    }
}
