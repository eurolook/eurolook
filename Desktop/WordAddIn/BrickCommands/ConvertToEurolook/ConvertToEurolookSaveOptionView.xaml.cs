﻿namespace Eurolook.WordAddIn.BrickCommands.ConvertToEurolook
{
    public partial class ConvertToEurolookSaveOptionWindow
    {
        public ConvertToEurolookSaveOptionWindow(ConvertToEurolookSaveOptionViewModel viewmodel)
        {
            InitializeComponent();
            DataContext = viewmodel;
        }
    }
}
