﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.BrickCommands.Configuration;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing.DocumentCreation;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.DocumentCreation;
using Eurolook.WordAddIn.ViewModels;
using Eurolook.WordAddIn.Views;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands.ConvertToEurolook
{
    [UsedImplicitly]
    public class ConvertToEurolookCommand : EditCommandBase
    {
        private readonly IMessageService _messageService;
        private readonly IEnumerable<IBeforeConvertToEurolookDocumentCreationTask> _beforeDocumentCreationTasks;
        private readonly IEnumerable<Func<IBrickExecutionContext, IAfterConvertToEurolookDocumentCreationTask>> _afterDocumentCreationTasks;
        private readonly Func<CreateDocumentViewModel> _createDocumentViewModelFunc;
        private readonly Func<Document, ConvertToEurolookSaveOptionViewModel> _saveOptionViewModelCreateFunc;
        private readonly IDocumentCreationService _documentCreationService;

        private ConvertToEurolookSaveOption _saveOption;
        private ConvertToEurolookConfigurationV1 _configuration;

        public ConvertToEurolookCommand(
            IMessageService messageService,
            IEnumerable<IBeforeConvertToEurolookDocumentCreationTask> beforeDocumentCreationTasks,
            IEnumerable<Func<IBrickExecutionContext, IAfterConvertToEurolookDocumentCreationTask>> afterDocumentCreationTasks,
            Func<CreateDocumentViewModel> createDocumentViewModelFunc,
            Func<Document, ConvertToEurolookSaveOptionViewModel> saveOptionViewModelCreateFunc,
            IDocumentCreationService documentCreationService)
        {
            _messageService = messageService;
            _beforeDocumentCreationTasks = beforeDocumentCreationTasks;
            _afterDocumentCreationTasks = afterDocumentCreationTasks;
            _createDocumentViewModelFunc = createDocumentViewModelFunc;
            _saveOptionViewModelCreateFunc = saveOptionViewModelCreateFunc;
            _documentCreationService = documentCreationService;
        }

        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            try
            {
                LoadConfiguration();

                if (_configuration.HasSaveOption)
                {
                    var saveOptionViewModel = _saveOptionViewModelCreateFunc(context.Document);
                    saveOptionViewModel.DisplayName = $"{DisplayName} - Saving Options";
                    saveOptionViewModel.HelpReference = "ConvertToEurolook";
                    var saveOptionWindow = new ConvertToEurolookSaveOptionWindow(saveOptionViewModel) { Title = $"Eurolook - {DisplayName}" };

                    if (_messageService.ShowDialog(saveOptionWindow) == true)
                    {
                        _saveOption = saveOptionViewModel.SaveOption;
                    }
                    else
                    {
                        return Task.CompletedTask;
                    }
                }

                var viewModel = _createDocumentViewModelFunc();
                viewModel.Title = DisplayName;
                viewModel.HelpReference = "ConvertToEurolook";
                viewModel.LoadTemplateStoreTemplates = false;
                viewModel.CreateDocumentFunc =
                    async () =>
                    {
                        viewModel.IsCreating = true;
                        var documentCreationInfo = viewModel.GetDocumentCreationInfo();
                        documentCreationInfo.SaveOption = _saveOption;

                        var afterDocumentCreationTasks = _afterDocumentCreationTasks.Select(t => t(context)).ToList();

                        await _documentCreationService.CreateDocumentAsync(
                            documentCreationInfo,
                            _beforeDocumentCreationTasks,
                            afterDocumentCreationTasks,
                            ProgressReporter<int>.Empty);
                    };

                var win = new CreateDocumentWindow(viewModel) { Title = $"Eurolook - {DisplayName}" };
                _messageService.ShowSingleton(win);
                return Task.CompletedTask;
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                return Task.CompletedTask;
            }
        }

        private void LoadConfiguration()
        {
            _configuration ??= new BrickConfigurationReader().Read<ConvertToEurolookConfigurationV1>(Brick.Configuration) ?? new ConvertToEurolookConfigurationV1();
        }
    }
}
