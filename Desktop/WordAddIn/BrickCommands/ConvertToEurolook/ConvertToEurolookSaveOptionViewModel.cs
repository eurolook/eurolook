﻿using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.DocumentProcessing.DocumentCreation;
using GalaSoft.MvvmLight.Command;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.BrickCommands.ConvertToEurolook
{
    public class ConvertToEurolookSaveOptionViewModel : DialogViewModelBase
    {
        private readonly ICoAuthoringService _coAuthoringService;
        private readonly Document _document;
        private ConvertToEurolookSaveOption _saveOption;
        private ISettingsService _settingsService;

        public ConvertToEurolookSaveOptionViewModel(
            ICoAuthoringService coAuthoringService,
            ISettingsService settingsService,
            Document document)
        {
            _saveOption = ConvertToEurolookSaveOption.NewDocument;
            _coAuthoringService = coAuthoringService;
            _settingsService = settingsService;
            _document = document;

            GetHelpCommand = new RelayCommand(GetHelp);
        }

        public override string DisplayName { get; set; }

        public RelayCommand GetHelpCommand { get; }

        private void GetHelp()
        {
            _settingsService.OpenHelpLink(HelpReference);
        }

        public ConvertToEurolookSaveOption SaveOption
        {
            get => _saveOption;
            set => Set(() => SaveOption, ref _saveOption, value);
        }

        public string CreateNewDocumentText { get; set; } = "Your original document will not be modified.";

        public string OverwriteDescriptionText { get; set; } = "Your original document will be overwritten by a converted Eurolook document. \n" +
            "This operation cannot be undone.";

        public string HelpReference { get; set; }

        public override void Commit()
        {
            if (SaveOption == ConvertToEurolookSaveOption.CurrentDocument && _coAuthoringService.IsCoAuthoring(_document, DisplayName))
            {
                return;
            }

            base.Commit();
        }
    }
}
