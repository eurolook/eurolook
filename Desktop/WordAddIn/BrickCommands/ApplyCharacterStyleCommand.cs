﻿using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using JetBrains.Annotations;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    [UsedImplicitly]
    internal class ApplyCharacterStyleCommand : ApplyStyleCommandBase
    {
        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (new DocumentAutomationHelper(context, $"Apply {DisplayName} Brick"))
            {
                var range = context.Selection.Range.Duplicate;
                range.HandleVanishingContentControl();

                var style = GetLanguageIndependentStyle(context, argument);
                if (style == null)
                {
                    return Task.FromResult(0);
                }

                ApplyCharacterStyle(context.Selection, style);

                return Task.FromResult(0);
            }
        }
    }
}
