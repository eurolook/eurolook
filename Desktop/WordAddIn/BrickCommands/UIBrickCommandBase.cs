namespace Eurolook.WordAddIn.BrickCommands
{
    public abstract class UIBrickCommandBase : EditCommandBase
    {
        public override bool HasCustomUI
        {
            get { return true; }
        }
    }
}
