using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.DocumentMetadata.CalculationCommands;
using Eurolook.AddIn.Common.DocumentMetadata.MetadataViewModels;
using Eurolook.Common.Log;
using Eurolook.WordAddIn.Extensions;

namespace Eurolook.WordAddIn.BrickCommands.EditDocumentMetadata.CalculationCommands
{
    public class CalculationCommandRunner : ICalculationCommandRunner, ICanLog
    {
        public void RefreshAllCalculatedValues(IEurolookDocument eurolookDocument, List<IMetadataViewModel> metadata)
        {
            using (new DocumentAutomationHelper(
                eurolookDocument.GetDocument(),
                string.Empty,
                DocumentAutomationOption.Default | DocumentAutomationOption.HideRevisionsAndComments))
            {
                var calculatedMetadata = metadata.Where(m => m.CalculationCommand != null);
                foreach (var metadataViewModel in calculatedMetadata)
                {
                    try
                    {
                        metadataViewModel.CalculationCommand.CalculateValue(metadata);
                    }
                    catch (Exception ex)
                    {
                        this.LogError(ex);
                    }
                }
            }
        }
    }
}
