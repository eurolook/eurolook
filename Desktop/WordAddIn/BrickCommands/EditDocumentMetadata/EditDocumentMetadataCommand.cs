using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.DocumentMetadata;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;

namespace Eurolook.WordAddIn.BrickCommands.EditDocumentMetadata
{
    public class EditDocumentMetadataCommand : EditCommandBase
    {
        private readonly IMetadataDialog _metadataDialog;

        public EditDocumentMetadataCommand(IMetadataDialog metadataDialog)
        {
            _metadataDialog = metadataDialog;
        }

        public override bool CanExecute(IBrickExecutionContext context, string argument)
        {
            return base.CanExecute(context, argument)
                   && context.EurolookDocument?.DocumentModel?.DocumentModelMetadataDefinitions?.Any() == true;
        }

        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (new DocumentAutomationHelper(
                context,
                DisplayName,
                DocumentAutomationOption.Default
                | DocumentAutomationOption.HandleViewType
                | DocumentAutomationOption.RestoreSelection))
            {
                context.EurolookDocument.Document.UpdateCustomXmlBindings();
                await _metadataDialog.ShowAsync("Eurolook - Edit Metadata", context.EurolookDocument);
            }
        }
    }
}
