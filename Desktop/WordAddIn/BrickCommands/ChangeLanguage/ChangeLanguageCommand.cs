﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.XPath;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Authors;
using Eurolook.AddIn.Common.Database;
using Eurolook.Common;
using Eurolook.Common.Log;
using Eurolook.Data;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.Data.Xml;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.BrickDefinitionLanguage;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.DocumentProcessing.OpenXml;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.Extensions;
using Eurolook.WordAddIn.Messages;
using Eurolook.WordAddIn.ViewModels.Languages;
using GalaSoft.MvvmLight.Messaging;
using JetBrains.Annotations;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using Language = Eurolook.Data.Models.Language;
using PictureFormat = Microsoft.Office.Interop.Word.PictureFormat;
using Range = Microsoft.Office.Interop.Word.Range;
using Shape = Microsoft.Office.Interop.Word.Shape;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands.ChangeLanguage
{
    [UsedImplicitly]
    public class ChangeLanguageCommand : UIBrickCommandBase
    {
        private readonly IMessageService _messageService;
        private readonly ISettingsService _settingsService;
        private readonly IUserDataRepository _userDataRepository;
        private readonly ILanguageRepository _languageRepository;
        private readonly ITextsRepository _textsRepository;
        private readonly IBrickRepository _brickRepository;
        private readonly IEnumerable<IChangeLanguageHandler> _changeLanguageHandlers;
        private readonly ILanguageLoader _languageLoader;
        private readonly Func<List<AuthorUpdateInfo>, ChangeLanguageAuthorDialogViewModel> _changeLanguageAuthorDialogViewModelFunc;
        private readonly Func<Language, ILocalisedResourceResolver> _localisedResourceResolverFunc;
        private readonly ICoAuthoringService _coAuthoringService;
        private readonly IAuthorManager _authorManager;
        private readonly IAuthorDataLoader _authorDataLoader;

        private UserSettings _userSettings;

        public ChangeLanguageCommand(
            IMessageService messageService,
            ISettingsService settingsService,
            IUserDataRepository userDataRepository,
            ILanguageRepository languageRepository,
            ITextsRepository textsRepository,
            IBrickRepository brickRepository,
            IAuthorManager authorManager,
            IAuthorDataLoader authorDataLoader,
            IEnumerable<IChangeLanguageHandler> changeLanguageHandlers,
            ILanguageLoader languageLoader,
            Func<List<AuthorUpdateInfo>, ChangeLanguageAuthorDialogViewModel> changeLanguageAuthorDialogViewModelFunc,
            Func<Language, ILocalisedResourceResolver> localisedResourceResolverFunc,
            ICoAuthoringService coAuthoringService)
        {
            _messageService = messageService;
            _settingsService = settingsService;
            _userDataRepository = userDataRepository;
            _languageRepository = languageRepository;
            _textsRepository = textsRepository;
            _brickRepository = brickRepository;
            _authorManager = authorManager;
            _authorDataLoader = authorDataLoader;
            _changeLanguageHandlers = changeLanguageHandlers;
            _languageLoader = languageLoader;
            _changeLanguageAuthorDialogViewModelFunc = changeLanguageAuthorDialogViewModelFunc;
            _localisedResourceResolverFunc = localisedResourceResolverFunc;
            _coAuthoringService = coAuthoringService;
        }

        public override bool CanExecute(IBrickExecutionContext context, string argument)
        {
            return context.EurolookDocument.IsEurolookDocument && base.CanExecute(context, argument);
        }

        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            var eurolookDocument = context.EurolookDocument;
            try
            {
                if (_coAuthoringService.IsCoAuthoring(context.EurolookDocument.Document, DisplayName))
                {
                    return;
                }

                _userSettings = _userDataRepository.GetUserSettings();
                var viewModel = new ChangeLanguageViewModel(
                    _settingsService,
                    eurolookDocument,
                    GetAllLanguages(eurolookDocument),
                    GetFavoriteLanguages(eurolookDocument),
                    _userSettings?.IsFavoriteLanguageListEnabled ?? false);
                var win = new ChangeLanguageView(viewModel);

                if (_messageService.ShowDialog(win, true, true) == true)
                {
                    var sourceLanguage = eurolookDocument.Language;
                    var targetLanguage = viewModel.LanguageSelectionVm.SelectedLanguage.Model;
                    if (viewModel.IsStyleUpdate)
                    {
                        ReplaceDocumentStyles(eurolookDocument, targetLanguage);
                    }

                    using (new DocumentAutomationHelper(context, DisplayName))
                    {
                        await BeforeChangeLanguage(context, sourceLanguage, targetLanguage);
                        await ChangeLanguage(context, sourceLanguage, targetLanguage);
                        await AfterChangeLanguage(context, sourceLanguage, targetLanguage);
                    }

                    eurolookDocument.Document.UndoClear();
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private async Task BeforeChangeLanguage(IBrickExecutionContext context, Language sourceLanguage, Language targetLanguage)
        {
            foreach (var handler in _changeLanguageHandlers.OrderBy(h => h.ExecutionOrder))
            {
                try
                {
                    await handler.BeforeHandleAsync(context, sourceLanguage, targetLanguage);
                }
                catch (Exception ex)
                {
                    this.LogError(ex);
                }
            }
        }

        private async Task AfterChangeLanguage(IBrickExecutionContext context, Language sourceLanguage, Language targetLanguage)
        {
            foreach (var handler in _changeLanguageHandlers.OrderBy(h => h.ExecutionOrder))
            {
                try
                {
                    await handler.AfterHandleAsync(context, sourceLanguage, targetLanguage);
                }
                catch (Exception ex)
                {
                    this.LogError(ex);
                }
            }
        }

        [CanBeNull]
        private static InlineShape GetInlineShape(Range range, Image image)
        {
            double width = CssUnitConverter.ConvertToPoints(image.Width);
            double height = CssUnitConverter.ConvertToPoints(image.Height);

            var inlineShape = range.InlineShapes.OfType<InlineShape>()
                                   .FirstOrDefault(
                                       shp => Math.Abs(shp.Width - width) < 2
                                              && Math.Abs(shp.Height - height) < 2);
            return inlineShape;
        }

        [CanBeNull]
        private static Shape GetShape(Range range, Image image)
        {
            double width = CssUnitConverter.ConvertToPoints(image.Width);
            double height = CssUnitConverter.ConvertToPoints(image.Height);

            var shape = range.ShapeRange.OfType<Shape>()
                             .FirstOrDefault(
                                 shp => Math.Abs(shp.Width - width) < 2
                                        && Math.Abs(shp.Height - height) < 2);
            return shape;
        }

        private async Task ChangeLanguage(
            IBrickExecutionContext context,
            Language sourceLanguage,
            Language targetLanguage)
        {
            if (sourceLanguage == null || targetLanguage == null)
            {
                return;
            }

            // set Language property
            var eurolookDocument = context.EurolookDocument;
            var document = context.Document;
            eurolookDocument.Language = targetLanguage;

            Messenger.Default.Send(new CurrentDocumentLanguageChangedMessage());

            // Save the current Texts Custom XML
            string oldTextsXmlString = document.GetCustomXml(TextsCustomXml.RootName);
            var oldTextsCustomXml = new TextsCustomXml(
                XDocument.Parse(oldTextsXmlString ?? throw new InvalidOperationException()));

            // replace Texts Custom XML
            var translations = _textsRepository.GetTranslations(eurolookDocument.DocumentModel, targetLanguage);
            var newTextsCustomXml = new TextsCustomXml(translations, targetLanguage);
            document.ReplaceCustomXml(TextsCustomXml.RootName, newTextsCustomXml);

            var textMappingSourceToTarget = GetTextMappingSourceToTarget(oldTextsCustomXml, newTextsCustomXml);

            var authorUpdateInfos = await UpdateAuthorXmls(targetLanguage, context);

            var authorBricks = document.LoadAllBricks(_brickRepository)
                                       .Where(b => _authorManager.GetAuthorXmlMappings(b.ContentControl).Any());
            eurolookDocument.BrickEngine.RecreateDynamicBricksRetainingAuthor(context, authorBricks);

            // update specific bricks in the document
            UpdateDateControls(eurolookDocument, textMappingSourceToTarget, targetLanguage);

            UpdateSpellCheckLanguage(eurolookDocument, sourceLanguage, targetLanguage);
            ReplaceImageResource(eurolookDocument, sourceLanguage, targetLanguage);

            foreach (var handler in _changeLanguageHandlers.OrderBy(h => h.ExecutionOrder))
            {
                try
                {
                    await handler.HandleAsync(context, sourceLanguage, targetLanguage);
                }
                catch (Exception ex)
                {
                    this.LogError(ex);
                }
            }

            var failedAuthorTranslations = authorUpdateInfos.Where(a => !a.IsOk).ToList();
            if (failedAuthorTranslations.Any())
            {
                var viewModel = _changeLanguageAuthorDialogViewModelFunc(failedAuthorTranslations);
                var window = new ChangeLanguageAuthorDialog { DataContext = viewModel };
                _messageService.ShowDialog(window);
            }
        }

        private async Task<List<AuthorUpdateInfo>> UpdateAuthorXmls(Language targetLanguage, IBrickExecutionContext context)
        {
            var document = context.Document;
            var authorUpdateInfos = new List<AuthorUpdateInfo>();
            foreach (var authorStoreInfo in _authorManager.GetAuthorCustomXmlStoreInfos(document).ToList())
            {
                var authorUpdateInfo = await _authorDataLoader.GetAuthorInfoFromDbOrServerAsync(authorStoreInfo, targetLanguage);
                authorUpdateInfos.Add(authorUpdateInfo);
                ReplaceAuthorXml(authorUpdateInfo, document);
            }

            return authorUpdateInfos;
        }

        private void ReplaceAuthorXml(AuthorUpdateInfo authorUpdateInfo, Document document)
        {
            var xmlPart = document.GetCustomXmlPart(authorUpdateInfo.StoreId);
            var authorCustomXml = authorUpdateInfo.AuthorCustomXml;
            _authorManager.ReplaceAuthorXmlPart(xmlPart, authorCustomXml);
        }

        private static void UpdateSpellCheckLanguage(
            IEurolookWordDocument eurolookDocument,
            Language sourceLanguage,
            Language targetLanguage)
        {
            var sourceLanguageId = sourceLanguage.AsWdLanguageId();
            var targetLanguageId = targetLanguage.AsWdLanguageId();

            foreach (var style in eurolookDocument.Document.Styles.OfType<Style>())
            {
                try
                {
                    if (style.Type == WdStyleType.wdStyleTypeList)
                    {
                        // list styles do not have a language
                        continue;
                    }

                    if (style.LanguageID == sourceLanguageId)
                    {
                        style.LanguageID = targetLanguageId;
                    }
                }
                catch (Exception)
                {
                    Debug.WriteLine(style.Type.ToString());
                }
            }

            foreach (var range in eurolookDocument.Document.StoryRanges.OfType<Range>())
            {
                range.LanguageID = targetLanguageId;

                var find = range.Find;
                find.ClearFormatting();
                find.LanguageID = sourceLanguageId;
                find.Replacement.ClearFormatting();
                find.Replacement.LanguageID = targetLanguageId;

                find.Text = "";
                find.Replacement.Text = "";
                find.Forward = true;
                find.Wrap = WdFindWrap.wdFindContinue;
                find.Format = true;
                find.MatchCase = false;
                find.MatchWholeWord = false;
                find.MatchWildcards = false;
                find.MatchSoundsLike = false;
                find.MatchAllWordForms = false;
                find.Execute(Replace: WdReplace.wdReplaceAll);
            }
        }

        private void UpdateDateControls(
            IEurolookWordDocument eurolookDocument,
            IReadOnlyDictionary<string, string> textMappingSourceToTarget,
            Language targetLanguage)
        {
            try
            {
                foreach (var brickContainer in eurolookDocument.Document.GetAllBrickContainers())
                {
                    foreach (var contentControl in brickContainer
                                                   .ContentControl.Range.ContentControls.OfType<ContentControl>().Where(
                                                       cc => cc.Type == WdContentControlType.wdContentControlDate))
                    {
                        try
                        {
                            // NOTE: when using the long date format (with the month spelled out), Word isn't recognizing the content as a valid date (e.g. März and March)
                            // Therefore, display locale and format must be set in the following order for the changes:
                            // 1. setting date format to a neutral format
                            // 2. setting display locale
                            // 3. setting date format to desired new format
                            string currentDateFormat = contentControl.DateDisplayFormat;
                            if (textMappingSourceToTarget.ContainsKey(currentDateFormat))
                            {
                                contentControl.DateDisplayFormat = "d-M-yyyy";
                            }

                            contentControl.DateDisplayLocale = targetLanguage.AsWdLanguageId();

                            if (textMappingSourceToTarget.ContainsKey(currentDateFormat))
                            {
                                contentControl.DateDisplayFormat = textMappingSourceToTarget[currentDateFormat];
                            }
                        }
                        catch (COMException ex)
                        {
                            this.LogError(ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private static IReadOnlyDictionary<string, string> GetTextMappingSourceToTarget(TextsCustomXml oldTextsCustomXml, TextsCustomXml newTextsCustomXml)
        {
            if (oldTextsCustomXml.Root == null || newTextsCustomXml.Root == null)
            {
                return new Dictionary<string, string>();
            }

            return oldTextsCustomXml.Root.Elements().GroupBy(p => p.Value, StringComparer.OrdinalIgnoreCase)
                                    .ToDictionary(
                                        g => g.Key,
                                        g => newTextsCustomXml.Root.Element(g.First().Name)?.Value,
                                        StringComparer.OrdinalIgnoreCase);
        }

        private List<Style> GetStylesToCopy(IEurolookWordDocument eurolookDocument)
        {
            var styles = new List<Style>();
            foreach (Style style in eurolookDocument.Document.Styles)
            {
                if (style.Type != WdStyleType.wdStyleTypeList
                    && style.Type != WdStyleType.wdStyleTypeTable
                    && (!style.BuiltIn || (style.BuiltIn && (!style.Visibility || style.UnhideWhenUsed))))
                {
                    styles.Add(style);
                }
            }

            return styles;
        }

        private byte[] GetDocumentTemplate(DocumentModel documentModel, Language targetLanguage)
        {
            var documentModelLanguage = _languageRepository.GetDocumentModelLanguage(documentModel.Id, targetLanguage.Id);
            return documentModelLanguage?.Template ?? documentModel.Template;
        }

        private void ReplaceDocumentStyles(IEurolookWordDocument eurolookDocument, Language targetLanguage)
        {
            // create a local copy of targetLanguage template
            using (var tempFile = new TemporaryFile(".dotx"))
            {
                Document templateDocument;
                try
                {
                    var templateData = GetDocumentTemplate(eurolookDocument.DocumentModel, targetLanguage);
                    File.WriteAllBytes(tempFile.FullName, templateData);

                    // open the local copy in Word
                    templateDocument = eurolookDocument.Document.Application.Documents.Open(
                        tempFile.FullName,
                        ReadOnly: true,
                        AddToRecentFiles: false,
                        Visible: false);
                }
                catch (Exception ex)
                {
                    this.LogError("Could not open target language template.", ex);
                    return;
                }

                // get styles to copy from template and copy to the document
                using (var numberingStartValueRetainer =
                       new NumberingStartValueRetainer(eurolookDocument.Document))
                {
                    var stylesToCopy = GetStylesToCopy(eurolookDocument);
                    foreach (var style in stylesToCopy)
                    {
                        try
                        {
                            eurolookDocument.Document.Application.OrganizerCopy(
                                templateDocument.FullName,
                                eurolookDocument.Document.FullName,
                                style.NameLocal,
                                WdOrganizerObject.wdOrganizerObjectStyles);
                        }
                        catch (Exception ex)
                        {
                            if (!style.BuiltIn)
                            {
                                this.LogDebug(
                                    $"Could not copy style from target language template: {style.NameLocal}",
                                    ex);
                            }
                        }
                    }
                }

                // cleanup
                try
                {
                    templateDocument.Close(false);
                }
                catch
                {
                    // ignored
                }
            }
        }

        private void ReplaceImageResource(
            IEurolookWordDocument eurolookDocument,
            Language sourceLanguage,
            Language targetLanguage)
        {
            var brickContainers = eurolookDocument.Document.GetAllBrickContainers();
            var bricks = eurolookDocument.DocumentStructures.Select(ds => ds.Brick)
                                         .Distinct(new IdentifiableComparer<Brick>())
                                         .ToDictionary(b => b.Id, b => b);

            var sourceLanguageResourceResolver = _localisedResourceResolverFunc(sourceLanguage);
            var targetLanguageResourceResolver = _localisedResourceResolverFunc(targetLanguage);

            foreach (var brickContainer in brickContainers.Where(c => bricks.ContainsKey(c.Id)))
            {
                var brick = bricks.ContainsKey(brickContainer.Id) ? bricks[brickContainer.Id] as ContentBrick : null;
                if (brick == null)
                {
                    continue;
                }

                // parse images and their properties from the brick's contents
                var images = ((IEnumerable)brick.Content.XPathEvaluate("//img")).Cast<XElement>().Select(
                    el =>
                    {
                        var image = new Image();
                        image.Parse(el);
                        return image;
                    }).ToList();

                foreach (var image in images)
                {
                    var sourceResource = sourceLanguageResourceResolver.ResolveLocalisedResource(image.Source);
                    var targetResource = targetLanguageResourceResolver.ResolveLocalisedResource(image.Source);

                    if (new ByteArrayComparer().Equals(sourceResource.RawData, targetResource.RawData))
                    {
                        // skip images that are not different in the target language
                        continue;
                    }

                    string extension = targetResource.GetFileExtension();
                    using (var temporaryFile = new TemporaryFile(extension))
                    {
                        File.WriteAllBytes(temporaryFile.FullName, targetResource.RawData);

                        var inlineShape = GetInlineShape(brickContainer.ContentControl.Range, image);
                        if (inlineShape != null)
                        {
                            var range = inlineShape.Range;
                            bool isShapeRangeEqualToContentControl = range.IsEqual(brickContainer.ContentControl.Range);
                            var inlineShapeProperties = new InlineShapeProperties(inlineShape);

                            inlineShape.Delete();

                            // EUROLOOK-4631: use the range of the content control when the image is the only content
                            var newShape = isShapeRangeEqualToContentControl
                                ? brickContainer.ContentControl.Range.InlineShapes.AddPicture(temporaryFile.FullName)
                                : range.InlineShapes.AddPicture(temporaryFile.FullName);
                            inlineShapeProperties.ApplyTo(newShape);
                        }

                        var shape = GetShape(brickContainer.ContentControl.Range, image);
                        if (shape != null)
                        {
                            var range = shape.Anchor;
                            var shapeProperties = new ShapeProperties(shape);

                            shape.Delete();
                            var newInlineShape = range.InlineShapes.AddPicture(temporaryFile.FullName);
                            var newShape = newInlineShape.ConvertToShape();
                            shapeProperties.ApplyTo(newShape);
                        }
                    }
                }
            }
        }

        [NotNull]
        private IEnumerable<Language> GetAllLanguages(IEurolookDocument eurolookDocument)
        {
            try
            {
                var result = _languageLoader.GetAllLanguages(eurolookDocument?.DocumentModel).ToList();
                return InsertLanguageIfNotIncluded(result, eurolookDocument?.Language);
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }

            return Enumerable.Empty<Language>();
        }

        [NotNull]
        private IEnumerable<Language> GetFavoriteLanguages(IEurolookDocument eurolookDocument)
        {
            try
            {
                var result = _languageLoader.GetFavoriteLanguages(eurolookDocument?.DocumentModel, _userSettings)
                                            .ToList();
                return InsertLanguageIfNotIncluded(result, eurolookDocument?.Language);
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }

            return Enumerable.Empty<Language>();
        }

        private List<Language> InsertLanguageIfNotIncluded(List<Language> languageList, Language language)
        {
            if (language != null && languageList.All(l => l.Id != language.Id))
            {
                // make sure the current document language is included in the list
                languageList.Add(language);
                return languageList.OrderBy(l => l.DisplayName).ToList();
            }

            return languageList;
        }

        public class ShapeProperties
        {
            public ShapeProperties(Shape shape)
            {
                // NOTE: the following properties are not supported:
                // - shape.ZOrderPosition
                Height = shape.Height;
                Width = shape.Width;

                Left = shape.Left;
                LeftRelative = shape.LeftRelative;

                Top = shape.Top;
                TopRelative = shape.TopRelative;

                RelativeHorizontalPosition = shape.RelativeHorizontalPosition;
                RelativeHorizontalSize = shape.RelativeHorizontalSize;
                RelativeVerticalPosition = shape.RelativeVerticalPosition;
                RelativeVerticalSize = shape.RelativeVerticalSize;

                LayoutInCell = shape.LayoutInCell;
                LockAnchor = shape.LockAnchor;
                LockAspectRatio = shape.LockAspectRatio;

                PictureFormat = new ShapePictureFormat(shape.PictureFormat);

                AlternativeText = shape.AlternativeText;
                Title = shape.Title;
                Name = shape.Name;
            }

            public float Height { get; }

            public float Width { get; }

            public float Left { get; }

            public float LeftRelative { get; }

            public float Top { get; }

            public float TopRelative { get; }

            public WdRelativeHorizontalPosition RelativeHorizontalPosition { get; }

            public WdRelativeHorizontalSize RelativeHorizontalSize { get; }

            public WdRelativeVerticalPosition RelativeVerticalPosition { get; }

            public WdRelativeVerticalSize RelativeVerticalSize { get; }

            public int LayoutInCell { get; }

            public int LockAnchor { get; }

            public MsoTriState LockAspectRatio { get; }

            public ShapePictureFormat PictureFormat { get; }

            public string AlternativeText { get; }

            public string Title { get; }

            public string Name { get; }

            public void ApplyTo(Shape shape)
            {
                shape.Left = Left;
                shape.LeftRelative = LeftRelative;
                shape.Top = Top;
                shape.TopRelative = TopRelative;

                shape.RelativeHorizontalPosition = RelativeHorizontalPosition;
                shape.RelativeHorizontalSize = RelativeHorizontalSize;
                shape.RelativeVerticalPosition = RelativeVerticalPosition;
                shape.RelativeVerticalSize = RelativeVerticalSize;

                shape.LayoutInCell = LayoutInCell;

                shape.LockAspectRatio = MsoTriState.msoFalse;
                shape.Height = Height;
                shape.Width = Width;

                shape.Left = Left;
                shape.LeftRelative = LeftRelative;
                shape.Top = Top;
                shape.TopRelative = TopRelative;

                shape.LockAspectRatio = LockAspectRatio;
                shape.AlternativeText = AlternativeText;
                shape.Title = Title;
                shape.Name = Name;

                shape.ZOrder(MsoZOrderCmd.msoSendBehindText);

                // NOTE: Setting picture format may change the shapes anchor, therefore it is not done right now.
                ////PictureFormat.ApplyTo(shape.PictureFormat);
            }
        }

        public class InlineShapeProperties
        {
            public InlineShapeProperties(InlineShape shape)
            {
                // NOTE: the following properties are not supported:
                // - InlineShape.TextEffect
                Height = shape.Height;
                Width = shape.Width;
                ScaleHeight = shape.ScaleHeight;
                ScaleWidth = shape.ScaleWidth;

                Borders = new ShapeBorders(shape.Borders);

                LockAspectRatio = shape.LockAspectRatio;

                PictureFormat = new ShapePictureFormat(shape.PictureFormat);

                AlternativeText = shape.AlternativeText;
                Title = shape.Title;
            }

            public ShapeBorders Borders { get; }

            public float Height { get; }

            public float Width { get; }

            public float ScaleHeight { get; }

            public float ScaleWidth { get; }

            public MsoTriState LockAspectRatio { get; }

            public ShapePictureFormat PictureFormat { get; }

            public string AlternativeText { get; }

            public string Title { get; }

            public void ApplyTo(InlineShape shape)
            {
                shape.Height = Height;
                shape.Width = Width;
                shape.ScaleHeight = ScaleHeight;
                shape.ScaleWidth = ScaleWidth;
                shape.LockAspectRatio = LockAspectRatio;
                shape.AlternativeText = AlternativeText;
                shape.Title = Title;

                Borders.ApplyTo(shape.Borders);
                PictureFormat.ApplyTo(shape.PictureFormat);
            }
        }

        public class ShapeBorders
        {
            private readonly int _distanceFromLeft;
            private readonly int _distanceFromTop;
            private readonly int _distanceFromRight;
            private readonly int _distanceFromBottom;

            private readonly WdColor _outsideColor;
            private readonly WdColorIndex _outsideColorIndex;
            private readonly WdLineStyle _outsideLineStyle;
            private readonly WdLineWidth _outsideLineWidth;

            private readonly bool _shadow;

            private readonly ShapeBorder _borderLeft;
            private readonly ShapeBorder _borderTop;
            private readonly ShapeBorder _borderRight;
            private readonly ShapeBorder _borderBottom;

            public ShapeBorders(Borders borders)
            {
                // NOTE: the following properties are only valid for page borders:
                // - Borders.AlwaysInFront
                // - Borders.DistanceFrom
                // - Borders.EnableFirstPageInSection
                // - Borders.EnableOtherPagesInSection
                // - Borders.JoinBorders
                // - Borders.SurroundFooter
                // - Borders.SurroundHeader
                // - Borders.InsideColor
                // - Borders.InsideColorIndex
                // - Borders.InsideLineStyle
                // - Borders.InsideLineWidth
                _distanceFromLeft = borders.DistanceFromLeft;
                _distanceFromTop = borders.DistanceFromTop;
                _distanceFromRight = borders.DistanceFromRight;
                _distanceFromBottom = borders.DistanceFromBottom;

                _outsideColor = borders.OutsideColor;
                _outsideColorIndex = borders.OutsideColorIndex;
                _outsideLineStyle = borders.OutsideLineStyle;
                _outsideLineWidth = borders.OutsideLineWidth;

                _shadow = borders.Shadow;
                _borderLeft = new ShapeBorder(borders[WdBorderType.wdBorderLeft]);
                _borderTop = new ShapeBorder(borders[WdBorderType.wdBorderTop]);
                _borderRight = new ShapeBorder(borders[WdBorderType.wdBorderRight]);
                _borderBottom = new ShapeBorder(borders[WdBorderType.wdBorderBottom]);
            }

            public void ApplyTo(Borders borders)
            {
                if (_outsideLineStyle != WdLineStyle.wdLineStyleNone)
                {
                    borders.OutsideLineStyle = _outsideLineStyle;
                    borders.OutsideLineWidth = _outsideLineWidth;
                    borders.OutsideColorIndex = _outsideColorIndex;
                    borders.OutsideColor = _outsideColor;

                    borders.Shadow = _shadow;

                    borders.DistanceFromLeft = _distanceFromLeft;
                    borders.DistanceFromTop = _distanceFromTop;
                    borders.DistanceFromRight = _distanceFromRight;
                    borders.DistanceFromBottom = _distanceFromBottom;

                    _borderLeft.ApplyTo(borders[WdBorderType.wdBorderLeft]);
                    _borderTop.ApplyTo(borders[WdBorderType.wdBorderTop]);
                    _borderRight.ApplyTo(borders[WdBorderType.wdBorderRight]);
                    _borderBottom.ApplyTo(borders[WdBorderType.wdBorderBottom]);
                }
            }
        }

        public class ShapeBorder
        {
            private readonly WdColor _color;
            private readonly WdColorIndex _colorIndex;
            private readonly WdLineStyle _lineStyle;
            private readonly WdLineWidth _lineWidth;

            public ShapeBorder(Border border)
            {
                _color = border.Color;
                _colorIndex = border.ColorIndex;

                _lineStyle = border.LineStyle;
                _lineWidth = border.LineWidth;
            }

            public void ApplyTo(Border border)
            {
                if (_lineStyle != WdLineStyle.wdLineStyleNone)
                {
                    border.LineWidth = _lineWidth;
                    border.LineStyle = _lineStyle;
                    border.ColorIndex = _colorIndex;
                    border.Color = _color;
                }
            }
        }

        public class ShapePictureFormat
        {
            private readonly float _brightness;
            private readonly MsoPictureColorType _colorType;
            private readonly float _contrast;
            private readonly PictureCrop _crop;
            private readonly float _cropLeft;
            private readonly float _cropTop;
            private readonly float _cropRight;
            private readonly float _cropBottom;
            private readonly int _transparencyColor;
            private readonly MsoTriState _transparentBackground;

            public ShapePictureFormat(PictureFormat pictureFormat)
            {
                _brightness = pictureFormat.Brightness;
                _colorType = pictureFormat.ColorType;
                _contrast = pictureFormat.Contrast;
                _crop = new PictureCrop(pictureFormat.Crop);
                _cropLeft = pictureFormat.CropLeft;
                _cropTop = pictureFormat.CropTop;
                _cropRight = pictureFormat.CropRight;
                _cropBottom = pictureFormat.CropBottom;
                _transparencyColor = pictureFormat.TransparencyColor;
                _transparentBackground = pictureFormat.TransparentBackground;
            }

            public void ApplyTo(PictureFormat pictureFormat)
            {
                pictureFormat.Brightness = _brightness;

                pictureFormat.ColorType = _colorType;
                pictureFormat.Contrast = _contrast;
                _crop.ApplyTo(pictureFormat.Crop);
                pictureFormat.CropLeft = _cropLeft;
                pictureFormat.CropTop = _cropTop;
                pictureFormat.CropRight = _cropRight;
                pictureFormat.CropBottom = _cropBottom;
                pictureFormat.TransparencyColor = _transparencyColor;
                pictureFormat.TransparentBackground = _transparentBackground;
            }
        }

        public class PictureCrop
        {
            private readonly float _pictureHeight;
            private readonly float _pictureOffsetX;
            private readonly float _pictureOffsetY;
            private readonly float _pictureWidth;
            private readonly float _shapeHeight;
            private readonly float _shapeWidth;

            public PictureCrop(Crop crop)
            {
                // NOTE: the following properties are not only valid for picture crop:
                // - Crop.ShapeLeft
                // - Crop.ShapeTop
                _pictureHeight = crop.PictureHeight;
                _pictureOffsetX = crop.PictureOffsetX;
                _pictureOffsetY = crop.PictureOffsetY;
                _pictureWidth = crop.PictureWidth;
                _shapeHeight = crop.ShapeHeight;

                _shapeWidth = crop.ShapeWidth;
            }

            public void ApplyTo(Crop crop)
            {
                crop.PictureHeight = _pictureHeight;
                crop.PictureOffsetX = _pictureOffsetX;
                crop.PictureOffsetY = _pictureOffsetY;
                crop.PictureWidth = _pictureWidth;
                crop.ShapeHeight = _shapeHeight;
                crop.ShapeWidth = _shapeWidth;
            }
        }
    }
}
