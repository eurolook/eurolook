﻿using System;
using System.Windows;

namespace Eurolook.WordAddIn.BrickCommands.ChangeLanguage
{
    public partial class ChangeLanguageView
    {
        public ChangeLanguageView(ChangeLanguageViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }

        private void OnOkClick(object sender, RoutedEventArgs e)
        {
            try
            {
                DialogResult = true; // will close the dialog automatically
            }
            catch (InvalidOperationException)
            {
                // when not opened as dialog
                Close();
            }
        }
    }
}
