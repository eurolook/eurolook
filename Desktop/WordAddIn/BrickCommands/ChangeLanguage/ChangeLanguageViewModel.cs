using System.Collections.Generic;
using System.Linq;
using Eurolook.AddIn.Common;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.ViewModels.Languages;
using GalaSoft.MvvmLight.CommandWpf;

namespace Eurolook.WordAddIn.BrickCommands.ChangeLanguage
{
    public class ChangeLanguageViewModel : BrickCommandViewModelBase
    {
        private readonly ISettingsService _settingsService;
        private readonly Language[] _allLanguages;
        private readonly Language[] _favoriteLanguages;
        private bool _isStyleUpdate;

        public LanguageSelectionViewModel LanguageSelectionVm { get; }

        public ChangeLanguageViewModel(
            ISettingsService settingsService,
            IEurolookWordDocument eurolookDocument,
            IEnumerable<Language> allLanguages,
            IEnumerable<Language> favoriteLanguages,
            bool isFavoriteListEnabled)
            : base(eurolookDocument)
        {
            _settingsService = settingsService;
            _allLanguages = allLanguages as Language[] ?? allLanguages.ToArray();
            _favoriteLanguages = favoriteLanguages as Language[] ?? favoriteLanguages.ToArray();
            LanguageSelectionVm = new LanguageSelectionViewModel(LoadFavoriteLanguages, LoadAllLanguages, Validate)
            {
                IsFavoriteListEnabled = isFavoriteListEnabled,
            };

            var documentLanguage = eurolookDocument?.Language;
            bool forceLoadAll = isFavoriteListEnabled && _favoriteLanguages.All(l => l.Id != documentLanguage?.Id);
            LanguageSelectionVm.LoadLanguages(forceLoadAll);
            LanguageSelectionVm.SelectedLanguage =
                LanguageSelectionVm.Languages.FirstOrDefault(l => l.Id == documentLanguage?.Id)
                ?? LanguageSelectionVm.Languages.FirstOrDefault();

            GetHelpCommand = new RelayCommand(() => _settingsService.OpenHelpLink("ChangeLanguage"));
        }

        public bool IsStyleUpdate
        {
            get => _isStyleUpdate;
            set { Set(() => IsStyleUpdate, ref _isStyleUpdate, value); }
        }

        public RelayCommand GetHelpCommand { get; }

        private IEnumerable<Language> LoadAllLanguages()
        {
            return _allLanguages;
        }

        private IEnumerable<Language> LoadFavoriteLanguages()
        {
            return _favoriteLanguages;
        }

        private void Validate()
        {
            RaisePropertyChanged(() => IsValid);
        }

        public bool IsValid => LanguageSelectionVm.SelectedLanguage != null;

        public override string Title => "Change Language";
    }
}
