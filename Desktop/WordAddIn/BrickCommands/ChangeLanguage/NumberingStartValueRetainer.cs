﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.WordAddIn.Extensions;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickCommands.ChangeLanguage
{
    /// <summary>
    /// A helper class to remember custom start values of lists that would be lost when
    /// updating the underlying paragraph style.
    /// </summary>
    /// <remarks>
    /// When setting a custom start value on the first paragraph based on a certain list template, Word will modify the
    /// respective list template immediately. This modification is lost when a new version of that style is copied to
    /// the document, e.g. when the Change Language tool updates language-specific style.
    /// Note: When another but the first paragraph using a certain list template is configured to use a custom start
    /// value, Word will _not_ modify the underlying list template but instead create a copy with the custom start
    /// value. So in this case the custom start value will always be retained by Word, because the copy of the list
    /// template won't be overriden when updating the paragraph styles.
    /// </remarks>
    public sealed class NumberingStartValueRetainer : IDisposable
    {
        private readonly List<ParagraphWrapper> _paragraphsWithCustomNumberingStart = new();

        public NumberingStartValueRetainer(Document document)
        {
            var storyRanges = document.AllStoryRanges().ToList();

            foreach (var storyRange in storyRanges)
            {
                // collect all paragraph with a list template that defines a custom list start value
                var result = storyRange.Paragraphs.OfType<Paragraph>().Select(p => new ParagraphWrapper(p))
                                       .Where(p => p.ListTemplate != null && p.ListStartAt > 1).ToList();
                _paragraphsWithCustomNumberingStart.AddRange(result);
            }
        }

        public void Dispose()
        {
            foreach (var paragraphWrapper in _paragraphsWithCustomNumberingStart)
            {
                // restore the customized list start value
                paragraphWrapper.ListTemplate.ListLevels[paragraphWrapper.ListLevel].StartAt =
                    paragraphWrapper.ListStartAt;
            }
        }

        private sealed class ParagraphWrapper
        {
            private ListTemplate _listTemplate;
            private ListFormat _listFormat;
            private int? _listLevel;
            private int? _listStartAt;
            private Range _range;

            public ParagraphWrapper(Paragraph paragraph)
            {
                Paragraph = paragraph;
            }

            public ListTemplate ListTemplate => _listTemplate ??= ListFormat.ListTemplate;

            public ListFormat ListFormat => _listFormat ??= Range.ListFormat;

            public int ListLevel => _listLevel ??= ListFormat.ListLevelNumber;

            public int ListStartAt => _listStartAt ??= ListTemplate.ListLevels[ListLevel].StartAt;

            public Range Range => _range ??= Paragraph.Range;

            public Paragraph Paragraph { get; }
        }
    }
}
