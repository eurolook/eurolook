using System.Collections.Generic;
using Eurolook.AddIn.Common.Authors;
using Eurolook.AddIn.Common.ViewModels;

namespace Eurolook.WordAddIn.BrickCommands.ChangeLanguage
{
    public class ChangeLanguageAuthorDialogViewModel : DialogViewModelBase
    {
        public ChangeLanguageAuthorDialogViewModel(List<AuthorUpdateInfo> authorUpdateInfos)
        {
            AuthorUpdateInfos = authorUpdateInfos;
        }

        public override string Title => "Change Language - Authors Translation";

        public List<AuthorUpdateInfo> AuthorUpdateInfos { get; }
    }
}
