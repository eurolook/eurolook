﻿using System;
using System.Linq;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.DocumentMetadata;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickCommands.ContentInserter;
using Eurolook.WordAddIn.BrickCommands.InsertExcelObject;
using Eurolook.WordAddIn.BrickEngine;
using JetBrains.Annotations;
using Range = Microsoft.Office.Interop.Word.Range;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    [UsedImplicitly]
    public class InsertExcelChartOpCommand : InsertExcelObjectOpCommandBase<ExcelChart>
    {
        private const string MetaDataName = "PublishingFormat";
        private const string PublishingFormatDraft = "";

        private readonly IMetadataReaderWriter _metaDataReaderWriter;

        public InsertExcelChartOpCommand(
            IDocumentManager documentManager,
            IMessageService messageService,
            ISettingsService settingsService,
            IMetadataReaderWriter metaDataReaderWriter,
            IUserDataRepository userDataRepository,
            IBrickRepository brickRepository)
            : base(documentManager, messageService, settingsService, userDataRepository, brickRepository)
        {
            LabelTextAlias = "LabelFigureSeqOP";
            HasItalicLabels = true;

            _metaDataReaderWriter = metaDataReaderWriter;
        }

        public string LabelTextAlias { get; set; }

        public bool HasItalicLabels { get; set; }

        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            try
            {
                using var viewModel = CreateViewModelByConfiguration();
                await LoadSettings();
                ApplySettingsOrDefault(viewModel);

                var win = new InsertExcelObjectView(viewModel);

                if (MessageService.ShowDialog(win, true, true) == true)
                {
                    string altText = viewModel.AlternativeText;
                    IncludeTitlePlaceholder = viewModel.IncludeTitlePlaceholder;
                    IncludeSubtitlePlaceholder = viewModel.IncludeSubtitlePlaceholder;
                    IncludeCaptionPlaceholder = viewModel.IncludeCaptionPlaceholder;
                    IncludeCopyrightPlaceholder = viewModel.IncludeCopyrightPlaceholder;
                    IncludeSourcePlaceholder = viewModel.IncludeSourcePlaceholder;
                    IncludeFigureNotesPlaceholder = viewModel.IncludeFigureNotesPlaceholder;

                    using (var documentAutomationHelper = new DocumentAutomationHelper(context, DisplayName))
                    {
                        Enum.TryParse(argument, true, out TitleInsertionPosition insertionPosition);
                        var range = InsertExcelObject(
                            context,
                            viewModel.SelectedObject,
                            context.Selection.Range,
                            insertionPosition,
                            altText);
                        FixScaleTooLarge(range, range.InlineShapes[1]);
                        range?.SelectFirstPlaceholderContentControl(documentAutomationHelper.PreferredViewType);
                    }

                    await SaveSettings(viewModel);
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        private InsertExcelChartViewModel CreateViewModelByConfiguration()
        {
            var vm = new InsertExcelChartViewModel(DocumentManager.GetActiveDocumentViewModel(), SettingsService)
            {
                HasTitlePlaceholder = HasTitlePlaceholder,
                HasSubtitlePlaceholder = HasSubtitlePlaceholder,
                HasCaptionPlaceholder = HasCaptionPlaceholder,
                HasCopyrightPlaceholder = HasCopyrightPlaceholder,
                HasSourcePlaceholder = HasSourcePlaceholder,
                HasFigureNotesPlaceholder = HasFigureNotesPlaceholder,
            };
            RestoreSettings(vm);

            return vm;
        }

        protected override string ResolveLabelAlias(Range range)
        {
            return LabelTextAlias;
        }

        private string ResolvePublishingFormatMetadata(IBrickExecutionContext context)
        {
            _metaDataReaderWriter.Init(context.EurolookDocument);
            var metaDataDefinition = _metaDataReaderWriter.GetMetadataDefinition(MetaDataName);
            if (metaDataDefinition == null)
            {
                return "";
            }

            return _metaDataReaderWriter.ReadMetadataProperty(metaDataDefinition)?.GetSimpleValue() ?? "";
        }

        private bool InsertAlternativeTextInDocument(IBrickExecutionContext context)
        {
            return ResolvePublishingFormatMetadata(context).Equals(PublishingFormatDraft);
        }

        private Range InsertExcelObject(
            IBrickExecutionContext context,
            ExcelChart excelChart,
            Range range,
            TitleInsertionPosition insertionPosition,
            string altText)
        {
            if (excelChart == null)
            {
                return null;
            }

            range = range.GetCursorBrickInsertionRange();
            range.HandleVanishingContentControl();
            range.Select();

            string titleStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel("Image Title", range);
            string subtitleStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel("Image Subtitle", range);
            string bodyStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel("Image Placeholder", range);
            string altTextStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel("Image Alternative", range);
            string captionStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel("Image Caption", range);
            string copyrightStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel("Image Copyrights", range);
            string sourceStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel("Image Source", range);
            string notesStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel("Image Notes", range);

            var titlePlaceholderInserter = IncludeTitlePlaceholder
                ? new ParagraphInserter(
                    titleStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                        LabelAlias = ResolveLabelAlias(range),
                    },
                    new PlaceholderInserter("Type your title here."))
                : null;

            var subtitlePlaceholderInserter = IncludeSubtitlePlaceholder
                ? new ParagraphInserter(
                    subtitleStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                    },
                    new PlaceholderInserter("Type your subtitle here."))
                : null;

            var chartInserter = new ParagraphInserter(
                bodyStyle,
                new ExcelChartInserter
                {
                    Chart = excelChart.Shape ?? (dynamic)excelChart.Chart,
                    AlternativeText = InsertAlternativeTextInDocument(context) ? string.Empty : altText,
                });

            var alternativeTextInserter =
                InsertAlternativeTextInDocument(context)
                    ? new ParagraphInserter(
                        altTextStyle,
                        altText.IsNullOrEmpty()
                            ? new PlaceholderInserter(
                                "Type your alternative text here.")
                            : new TextInserter
                            {
                                Text = altText,
                                IsItalic = false,
                            })
                    : null;

            var copyrightPlaceholderInserter = IncludeCopyrightPlaceholder
                ? new ParagraphInserter(
                    copyrightStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                    },
                    new TextInserter
                    {
                        Text = "© ",
                    },
                    new PlaceholderInserter("Type your Copyrights information here."))
                : null;

            var sourcePlaceholderInserter = IncludeSourcePlaceholder
                ? new ParagraphInserter(
                    sourceStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                        LabelAlias = "LabelSource",
                        IsItalic = HasItalicLabels,
                    },
                    new TextInserter
                    {
                        Text = ":",
                        IsItalic = HasItalicLabels,
                    },
                    new TextInserter { Text = " " },
                    new PlaceholderInserter("Type your source here."))
                : null;

            var captionPlaceholderInserter = IncludeCaptionPlaceholder
                ? new ParagraphInserter(
                    captionStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                        LabelAlias = "",
                    },
                    new PlaceholderInserter("Type your caption here.", false))
                : null;

            var notesPlaceholderInserter = IncludeFigureNotesPlaceholder
                ? new ParagraphInserter(
                    notesStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                    },
                    new PlaceholderInserter("Type your notes here."))
                : null;

            // EUROLOOK-3566: if the brick is inserted in the last paragraph of a story outside of a content control, the source line breaks
            var paragraphAfterInserter = range.IsLastParagraphInStory()
                ? new ParagraphInserter(
                    range.Paragraphs.Last.GetStyleNameLocal(),
                    new TextInserter { Text = string.Empty })
                : null;

            var itemsToInsert = new IContentInserter[]
            {
                titlePlaceholderInserter,
                subtitlePlaceholderInserter,
                chartInserter,
                alternativeTextInserter,
                copyrightPlaceholderInserter,
                sourcePlaceholderInserter,
                captionPlaceholderInserter,
                notesPlaceholderInserter,
                paragraphAfterInserter,
            }.Where(c => c != null).ToArray();

            if (titlePlaceholderInserter != null
                && insertionPosition == TitleInsertionPosition.TitleBelowInsertedContent)
            {
                itemsToInsert.Swap(0, 1);
            }

            itemsToInsert.OfType<ParagraphInserter>().Last().Content.First().InsertAt =
                ContentInsertionPosition.Replace;

            var contentInserter = new BrickContainerInserter(Brick, itemsToInsert);

            var positionProvider = new RangePositionProvider(range);
            return contentInserter.Insert(positionProvider);
        }
    }
}
