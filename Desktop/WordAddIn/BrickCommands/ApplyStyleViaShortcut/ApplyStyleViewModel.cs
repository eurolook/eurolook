using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands.ApplyStyleViaShortcut
{
    public class ApplyStyleViewModel : BrickCommandViewModelBase
    {
        private static readonly Dictionary<Document, List<StyleShortcut>> AvailableStyleShortcutsCache =
            new Dictionary<Document, List<StyleShortcut>>();

        private readonly IWordApplicationContext _applicationContext;
        private StyleShortcut _selectedStyle;

        public ApplyStyleViewModel(
            List<StyleShortcut> styleShortcuts,
            IEurolookWordDocument eurolookDocument,
            IWordApplicationContext applicationContext)
            : base(eurolookDocument)
        {
            _applicationContext = applicationContext;

            AvailableStyleShortcuts = new NotifyTask<List<StyleShortcut>>()
                                      .NotifyOnCompletion(this)
                                      .Start(
                                          () => Task.Run(
                                              () => GetAvailableStyleShortcuts(
                                                  styleShortcuts,
                                                  eurolookDocument.Document)));
        }

        public StyleShortcut SelectedStyle
        {
            get
            {
                if ((_selectedStyle == null) && (AvailableStyleShortcuts?.IsCompleted == true))
                {
                    _selectedStyle = AvailableStyleShortcuts.Result.FirstOrDefault();
                }

                return _selectedStyle;
            }
            set { Set(() => SelectedStyle, ref _selectedStyle, value); }
        }

        public string Result { get; private set; }

        public NotifyTask<List<StyleShortcut>> AvailableStyleShortcuts { get; }

        public Action CloseAction { get; set; }

        public override string Title
        {
            get { return "Quick Style"; }
        }

        public override void Commit()
        {
            Result = SelectedStyle?.StyleName;
            RemoveClosedDocuments();
            CloseAction();
        }

        public override void Cancel()
        {
            Result = null;
            CloseAction();
        }

        private static List<StyleShortcut> GetAvailableStyleShortcuts(List<StyleShortcut> list, Document document)
        {
            if (AvailableStyleShortcutsCache.ContainsKey(document))
            {
                return AvailableStyleShortcutsCache[document];
            }

            var availableShortCuts = list.Where(s => LanguageIndependentStyle.IsStyleVisible(s.StyleName, document))
                                         .OrderBy(s => s.StyleName).ToList();
            AvailableStyleShortcutsCache[document] = availableShortCuts;
            return availableShortCuts;
        }

        private void RemoveClosedDocuments()
        {
            try
            {
                var closedDocuments = AvailableStyleShortcutsCache
                                      .Keys.Where(d => !_applicationContext.IsObjectValid(d)).ToList();
                foreach (var document in closedDocuments)
                {
                    AvailableStyleShortcutsCache.Remove(document);
                }
            }
            catch (COMException ex)
            {
                this.LogError(ex);
            }
        }
    }
}
