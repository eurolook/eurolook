﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.AddIn.Common.Database;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands.ApplyStyleViaShortcut
{
    public class ApplyStyleViaShortcutCommand : ApplyStyleCommandBase
    {
        private static List<StyleShortcut> _sStyleShortcuts;
        private readonly Func<List<StyleShortcut>, IEurolookWordDocument, ApplyStyleViewModel> _applyStyleViewModelCreateFunc;
        private readonly IEurolookDataRepository _eurolookDataRepository;
        private readonly IWordMessageService _messageService;

        public ApplyStyleViaShortcutCommand(
            Func<List<StyleShortcut>, IEurolookWordDocument, ApplyStyleViewModel> applyStyleViewModelCreateFunc,
            IEurolookDataRepository eurolookDataRepository,
            IWordMessageService messageService)
        {
            _applyStyleViewModelCreateFunc = applyStyleViewModelCreateFunc;
            _eurolookDataRepository = eurolookDataRepository;
            _messageService = messageService;
        }

        private List<StyleShortcut> StyleShortcuts
        {
            get { return _sStyleShortcuts ??= _eurolookDataRepository.GetStyleShortcuts(); }
        }

        public override bool CanExecute(IBrickExecutionContext context, string argument)
        {
            if (!base.CanExecute(context, argument))
            {
                return false;
            }

            var selection = context.Selection;
            if (selection == null)
            {
                return false;
            }

            if (selection.Range.IsLocked())
            {
                return false;
            }

            if (selection.Range.IsBeforeOrAfterBlockLevelContentControl())
            {
                return false;
            }

            return true;
        }

        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            try
            {
                var selection = context.Selection;
                var shortcutRange = GetLastTypedCharactersFromDocument(selection.Range, 2);
                if (shortcutRange == null)
                {
                    return;
                }

                var styleShortcut = GetStyleShortcutFromRange(shortcutRange);
                string newStyleNameInvariant =
                    styleShortcut == null ? AskUserForStyle(context.EurolookDocument, selection) : styleShortcut.StyleName;

                if (string.IsNullOrEmpty(newStyleNameInvariant))
                {
                    return;
                }

                using (new DocumentAutomationHelper(
                    context.Document,
                    "Apply Style",
                    DocumentAutomationOption.Default | DocumentAutomationOption.UnlockContents))
                {
                    var range = selection.Range.Duplicate;
                    range.HandleVanishingContentControl();

                    if (styleShortcut != null)
                    {
                        shortcutRange.Delete();
                    }

                    var style = LanguageIndependentStyle.FromNeutralStyleName(newStyleNameInvariant, context.Document);
                    StyleConceptNew.ApplyStyleToRange(style, selection.Range);

                    string placeholderText = GetPlaceholderText(
                        newStyleNameInvariant,
                        $"Type your {newStyleNameInvariant.ToLower()} here.");

                    await ApplyStyleWithPlaceholder(range, newStyleNameInvariant, placeholderText);
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger().Error($"Failed to execute ApplyStyleViaShortcutCommand.", ex);
            }
        }

        private StyleShortcut GetStyleShortcutFromRange(Range shortcutRange)
        {
            string shortcutName = shortcutRange.TextWithoutPlaceholders();

            return StyleShortcuts.FirstOrDefault(
                x =>
                    string.Equals(x.Shortcut, shortcutName, StringComparison.InvariantCultureIgnoreCase)
                    && LanguageIndependentStyle.IsStyleVisible(x.StyleName, shortcutRange.Document));
        }

        [CanBeNull]
        private static Range GetLastTypedCharactersFromDocument(Range range, int length)
        {
            try
            {
                range = range.Duplicate;
                range.Collapse(WdCollapseDirection.wdCollapseEnd);

                if (range.Start == range.End)
                {
                    range.MoveStart(WdUnits.wdCharacter, -length);
                }

                return range;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private string AskUserForStyle(IEurolookWordDocument eurolookDocument, Selection selection)
        {
            var viewModel = _applyStyleViewModelCreateFunc(StyleShortcuts, eurolookDocument);
            var window = new ApplyStyleWindow(viewModel);
            _messageService.ShowDialogNextToRange(window, selection.Paragraphs.First.Range, 20);
            return viewModel.Result;
        }
    }
}
