namespace Eurolook.WordAddIn.BrickCommands.ApplyStyleViaShortcut
{
    public partial class ApplyStyleWindow
    {
        public ApplyStyleWindow(ApplyStyleViewModel viewModel)
        {
            DataContext = viewModel;
            InitializeComponent();
            if (viewModel.CloseAction == null)
            {
                viewModel.CloseAction = Close;
            }
        }
    }
}
