using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Authors;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing;
using Eurolook.WordAddIn.Authors;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.Extensions;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    public class ChangeBrickAuthorCommand : EditCommandBase
    {
        private readonly IMessageService _messageService;
        private readonly IAuthorManager _authorManager;
        private readonly IFavoriteService _favoriteService;
        private readonly IEnumerable<IAuthorChangeExecutor> _authorChangeExecutors;
        private readonly Func<string, bool, ChangeAuthorViewModel> _changeAuthorViewModelFunc;
        private readonly IEnumerable<IChangeAuthorHandler> _changeAuthorHandlers;
        private readonly ICoAuthoringService _coAuthoringService;
        private ContentControl _selectedContentControl;

        public ChangeBrickAuthorCommand(
            IMessageService messageService,
            IAuthorManager authorManager,
            IFavoriteService favoriteService,
            IEnumerable<IAuthorChangeExecutor> authorChangeExecutors,
            Func<string, bool, ChangeAuthorViewModel> changeAuthorViewModelFunc,
            IEnumerable<IChangeAuthorHandler> changeAuthorHandlers,
            ICoAuthoringService coAuthoringService)
        {
            _messageService = messageService;
            _authorManager = authorManager;
            _favoriteService = favoriteService;
            _authorChangeExecutors = authorChangeExecutors;
            _changeAuthorViewModelFunc = changeAuthorViewModelFunc;
            _changeAuthorHandlers = changeAuthorHandlers;
            _coAuthoringService = coAuthoringService;
        }

        public async Task ExecuteAsync(IBrickExecutionContext context, ContentControl selectedContentControl)
        {
            _selectedContentControl = selectedContentControl;
            await base.ExecuteAsync(context, null);
        }

        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            try
            {
                if (_coAuthoringService.IsCoAuthoring(context.EurolookDocument.GetDocument(), "Change Author"))
                {
                    return;
                }

                var brickAuthorCustomXmlStoreInfos = _authorManager
                                                     .GetAuthorCustomXmlStoreInfos(
                                                         _selectedContentControl,
                                                         context.Document).ToList();
                if (!brickAuthorCustomXmlStoreInfos.Any())
                {
                    return;
                }

                string authorSingularOrPlural = brickAuthorCustomXmlStoreInfos.Count > 1 ? "Authors" : "Author";
                string title = $"Change {authorSingularOrPlural} of {_selectedContentControl.Title}";

                var viewModel = _changeAuthorViewModelFunc(title, false);
                await viewModel.InitAsync(context.EurolookDocument, brickAuthorCustomXmlStoreInfos);
                var win = new ChangeAuthorWindow(viewModel) { Title = $"Eurolook - {title}" };

                var authorRoleViewModels = viewModel.AuthorRolesSelectionViewModel.AuthorRoleViewModels;

                if (_messageService.ShowDialog(win) == true && authorRoleViewModels.Any(vm => vm.IsModified))
                {
                    _favoriteService.AddFavoritesIfNotExisting(authorRoleViewModels.Where(vm => !vm.Author.IsOriginatingFromDocumentCustomXml)
                                                                                   .Select(vm => vm.Author.Author));

                    var brickAuthorCustomXmlDocs = brickAuthorCustomXmlStoreInfos.Select(i => i.AuthorCustomXml);
                    var executor =
                        _authorChangeExecutors.FirstOrDefault(e => e.CanHandle(brickAuthorCustomXmlDocs));

                    using (new DocumentAutomationHelper(
                        context,
                        "Change Brick Author(s)",
                        DocumentAutomationOption.Default |
                        DocumentAutomationOption.RestoreSelection |
                        DocumentAutomationOption.DisableTrackRevisions))
                    {
                        executor?.ChangeBrickAuthors(context, authorRoleViewModels, _selectedContentControl);

                        foreach (var changeAuthorHandler in _changeAuthorHandlers.OrderBy(h => h.ExecutionOrder))
                        {
                            await changeAuthorHandler.HandleAsync(context.EurolookDocument);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger().Error("Failed to execute ChangeBrickAuthorCommand.", ex);
            }
        }
    }
}
