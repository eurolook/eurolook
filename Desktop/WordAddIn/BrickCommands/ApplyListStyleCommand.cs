﻿using System.Linq;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    public class ApplyListStyleCommand : ApplyStyleCommandBase
    {
        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (new DocumentAutomationHelper(context, $"Apply {DisplayName} Brick"))
            {
                var range = context.Selection.Range.Duplicate;
                range.HandleVanishingContentControl();

                string stylename = GetStyleName(argument);
                string placeholderText = GetPlaceholderText(argument, "Type your list item here.");

                if (ForceStyle(argument))
                {
                    await ApplyListStyleAndCorrectNextListRestart(range, stylename, placeholderText);
                }
                else
                {
                    if (HasListStyle(range, stylename))
                    {
                        if (ContainsListContinue(range))
                        {
                            RemoveListContinue(range);
                            return;
                        }

                        string textStyleName = IgnoreTextLevels(argument)
                            ? "Normal"
                            : StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel("Text", range);
                        if (string.IsNullOrEmpty(textStyleName))
                        {
                            textStyleName = "Normal";
                        }

                        await ApplyStyleWithPlaceholder(range, textStyleName, "Type your paragraph contents here.");
                        return;
                    }

                    string newListStyleName = IgnoreTextLevels(argument)
                        ? stylename
                        : StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(stylename, range);
                    var newStyle = LanguageIndependentStyle.FromNeutralStyleName(newListStyleName, context.Document);
                    await ApplyListStyleAndCorrectNextListRestart(range, placeholderText, newStyle.NameLocal);
                }
            }
        }

        /// <summary>
        /// ApplyStyleWithPlaceholder and Restart current paragraph if necessary.
        /// In addition corrects list restart for the next list of the same style in case.
        /// <list type="number">
        /// <item>
        /// <description>The list restart of the next list is changed, and</description>
        /// </item>
        /// <item>
        /// <description>
        /// There is not other list of the same type in the document before the
        /// paragraph(s) the command is executed on.
        /// </description>
        /// </item>
        /// </list>
        /// </summary>
        private async Task ApplyListStyleAndCorrectNextListRestart(Range range, string placeholderText, string newListStyleName)
        {
            var lastParagraph = range.Paragraphs.Last;
            var firstParagraph = range.Paragraphs.First;

            Paragraph nextListStartParagraph = null;
            if (!firstParagraph.ExistsParagraphOfListType(newListStyleName, false))
            {
                nextListStartParagraph = range.Paragraphs.Last.GetNextListRestart(newListStyleName);
            }

            await ApplyListStyle(range, newListStyleName, placeholderText, true);

            var nextParagraph = lastParagraph.Next();
            if (!nextParagraph.IsSameParagraph(nextListStartParagraph))
            {
                nextListStartParagraph.RestartListNumbering();
            }

            StyleConceptListRestart.ReStartListWhenPreviousParagraphFeaturesDifferentListTemplate(range);
        }

        private Task ApplyListStyle(Range range, string newListStyleName, string placeholderText, bool forceApply)
        {
            var newListStyle = StyleNameInfo.FromStyleName(newListStyleName);
            var document = range.Document;

            foreach (Paragraph paragraph in range.Paragraphs)
            {
                if (StyleConceptNew.IsListParagraph(paragraph))
                {
                    var currentStyle = LanguageIndependentStyle.FromParagraph(paragraph);
                    var currentStyleNameInfo = StyleNameInfo.FromStyleName(currentStyle.NameNeutral);

                    var listStyle = new StyleNameInfo(newListStyle.Category, newListStyle.TextLevel, currentStyleNameInfo.ListLevel);

                    listStyle = GetAvailableListStyleWithMaximumListLevel(listStyle, document);

                    ApplyStyleRespectListContinue(paragraph, listStyle.StyleName);
                }
                else
                {
                    ApplyStyleWithPlaceholder(paragraph.Range, newListStyleName, placeholderText, forceApply);
                }
            }

            return Task.FromResult(0);
        }

        private StyleNameInfo GetAvailableListStyleWithMaximumListLevel(StyleNameInfo listStyle, Document document)
        {
            while (listStyle.ListLevel > 1)
            {
                if (StyleConceptNew.IsStyleAvailable(listStyle.StyleName, document))
                {
                    return listStyle;
                }

                listStyle = listStyle.ListPromote();
            }

            return listStyle;
        }

        private void RemoveListContinue(Range range)
        {
            var listContinueParagraphs = range.Paragraphs.OfType<Paragraph>().Where(p => StyleConceptNew.IsListContinue(p));
            foreach (var p in listContinueParagraphs)
            {
                var style = LanguageIndependentStyle.FromParagraph(p);
                StyleConceptNew.ApplyStyleToParagraph(style, p, true);
            }
        }

        private bool ContainsListContinue(Range range)
        {
            return range.Paragraphs.OfType<Paragraph>().Any(p => StyleConceptNew.IsListContinue(p));
        }

        private bool HasListStyle(Range range, string styleNameNeutral)
        {
            foreach (Paragraph paragraph in range.Paragraphs)
            {
                var paragraphRange = paragraph.Range;
                paragraphRange.Collapse(WdCollapseDirection.wdCollapseStart);
                if ((bool)paragraphRange.Information[WdInformation.wdAtEndOfRowMarker])
                {
                    continue;
                }

                if (!HasListStyle(paragraph, styleNameNeutral))
                {
                    return false;
                }
            }

            return true;
        }

        private static bool HasListStyle(Paragraph paragraph, string styleNameNeutral)
        {
            return LanguageIndependentStyle.FromParagraph(paragraph).NameNeutral.StartsWith(styleNameNeutral);
        }
    }
}
