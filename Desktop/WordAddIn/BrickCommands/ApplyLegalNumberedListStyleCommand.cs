using System.Linq;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    public class ApplyLegalNumberedListStyleCommand : ApplyStyleCommandBase
    {
        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (new DocumentAutomationHelper(context, $"Apply {DisplayName} Brick"))
            {
                var range = context.Selection.Range.Duplicate;
                range.HandleVanishingContentControl();

                string styleNameNeutral = GetStyleName(argument);
                string placeholderText = GetPlaceholderText(argument, "Type your list item here.");

                if (HasListStyle(range, styleNameNeutral))
                {
                    string textStyleName = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel("Text", range);
                    if (string.IsNullOrEmpty(textStyleName))
                    {
                        textStyleName = "Normal";
                    }

                    await ApplyStyleWithPlaceholder(
                        range,
                        textStyleName,
                        "Type your paragraph contents here.");
                    return;
                }

                string newListStyleName = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(styleNameNeutral, range);
                var newStyle = LanguageIndependentStyle.FromNeutralStyleName(newListStyleName, context.Document);
                await ApplyStyleWithPlaceholder(range, newStyle.NameLocal, placeholderText, true);
            }
        }

        private static bool HasListStyle(Range range, string styleNameNeutral)
        {
            return range.Paragraphs.OfType<Paragraph>().All(
                p => LanguageIndependentStyle.FromParagraph(p)?.NameNeutral.StartsWith(styleNameNeutral)
                     == true);
        }
    }
}
