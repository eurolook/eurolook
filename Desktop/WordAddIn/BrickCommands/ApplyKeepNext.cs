using Eurolook.WordAddIn.BrickEngine;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    public class ApplyKeepNext : ApplyStyleCommandBase
    {
        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (
                new DocumentAutomationHelper(
                    context,
                    "Keep Next",
                    DocumentAutomationOption.Default | DocumentAutomationOption.RestoreSelection))
            {
                foreach (Paragraph paragraph in context.Selection.Paragraphs)
                {
                    SetKeepWithNext(paragraph);
                }
            }

            return Task.CompletedTask;
        }

        private void SetKeepWithNext(Paragraph paragraph)
        {
            if (paragraph.KeepWithNext == 0)
            {
                paragraph.KeepWithNext = -1;
            }
            else
            {
                paragraph.KeepWithNext = 0;
            }
        }
    }
}
