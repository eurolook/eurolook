using System.Linq;
using System.Xml.Linq;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.AddIn.Common.Wpf;

namespace Eurolook.WordAddIn.BrickCommands.ViewOpenXmlPackageParts
{
    public class OpenXmlPackageViewerViewModel : DialogViewModelBase
    {
        private PackagePartViewModel _selectedCustomXml;

        public OpenXmlPackageViewerViewModel(XDocument xDocument)
        {
            var packageParts = xDocument.Descendants(XName.Get("part", xDocument.Root?.Name.NamespaceName ?? ""));
            PackagePartViewModels = new ObservableCollectionEx<PackagePartViewModel>();

            var packagePartViewModels = packageParts
                                        .Select(p => new PackagePartViewModel(p))
                                        .OrderBy(p => p, new PackagePartViewModelComparer());

            PackagePartViewModels.AddRange(packagePartViewModels);
            SelectedCustomXml = PackagePartViewModels.FirstOrDefault();
        }

        public override string Title => "View OpenXML Package Parts";

        public ObservableCollectionEx<PackagePartViewModel> PackagePartViewModels { get; }

        public PackagePartViewModel SelectedCustomXml
        {
            get => _selectedCustomXml;
            set
            {
                Set(() => SelectedCustomXml, ref _selectedCustomXml, value);
            }
        }
    }
}
