using System;
using System.Threading.Tasks;
using System.Xml.Linq;
using Eurolook.AddIn.Common;
using Eurolook.WordAddIn.BrickEngine;

namespace Eurolook.WordAddIn.BrickCommands.ViewOpenXmlPackageParts
{
    public class ViewOpenXmlPackagePartsCommand : EditCommandBase
    {
        private readonly Func<XDocument, OpenXmlPackageViewerViewModel> _viewModelFunc;
        private readonly IMessageService _messageService;

        public ViewOpenXmlPackagePartsCommand(
            Func<XDocument, OpenXmlPackageViewerViewModel> viewModelFunc,
            IMessageService messageService)
        {
            _viewModelFunc = viewModelFunc;
            _messageService = messageService;
        }

        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            var xDocument = XDocument.Parse(context.Document.WordOpenXML);
            var viewModel = _viewModelFunc(xDocument);
            var window = new OpenXmlPackageViewerWindow(viewModel);
            _messageService.ShowDialog(window, true, true);
            return Task.CompletedTask;
        }
    }
}
