using System.Linq;
using System.Xml.Linq;
using Eurolook.Common.Log;
using ICSharpCode.AvalonEdit.Document;

namespace Eurolook.WordAddIn.BrickCommands.ViewOpenXmlPackageParts
{
    public class PackagePartViewModel : ICanLog
    {
        public PackagePartViewModel(XElement packagePart)
        {
            var contentNode = packagePart.Elements().FirstOrDefault();
            contentNode = contentNode?.Elements().FirstOrDefault() ?? contentNode;
            var ns = packagePart.Name.NamespaceName;
            PackagePath = packagePart.Attributes(XName.Get("name", ns))
                                     .FirstOrDefault()?.Value;
            ContentType = packagePart.Attributes(XName.Get("contentType", ns))
                                     .FirstOrDefault()?.Value;

            Title = contentNode?.Name.LocalName ?? "";

            Document = new TextDocument(contentNode?.ToString() ?? "");
        }

        public TextDocument Document { get; }

        public string Title { get; }

        public string PackagePath { get; }

        public string ContentType { get; }
    }
}
