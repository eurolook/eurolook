using System;
using System.Collections.Generic;

namespace Eurolook.WordAddIn.BrickCommands.ViewOpenXmlPackageParts
{
    public class PackagePartViewModelComparer : IComparer<PackagePartViewModel>
    {
        private static readonly List<string> ContentTypeOrdering = new List<string>
        {
            // ReSharper disable StringLiteralTypo
            "application/xml",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.header+xml",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.footer+xml",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.footnotes+xml",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.endnotes+xml",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.numbering+xml",
            "application/vnd.openxmlformats-package.core-properties+xml",
            "application/vnd.openxmlformats-officedocument.extended-properties+xml",
            "application/vnd.openxmlformats-officedocument.custom-properties+xml",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.settings+xml",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.webSettings+xml",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.fontTable+xml",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document.glossary+xml",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.fontTable+xml",
            "application/vnd.openxmlformats-officedocument.theme+xml",
            "application/vnd.openxmlformats-officedocument.customXmlProperties+xml",
            "application/vnd.openxmlformats-package.relationships+xml",

            // ReSharper restore StringLiteralTypo
        };

        public int Compare(PackagePartViewModel firstPart, PackagePartViewModel secondPart)
        {
            var result = GetContentTypeOrdering(firstPart, secondPart);
            if (result != 0)
            {
                return result;
            }

            result = GetTitleOrdering(firstPart, secondPart);
            if (result != 0)
            {
                return result;
            }

            return GetPackagePathOrdering(firstPart, secondPart);
        }

        private int GetContentTypeOrdering(PackagePartViewModel firstPart, PackagePartViewModel secondPart)
        {
            return GetOrderingIndex(firstPart).CompareTo(GetOrderingIndex(secondPart));
        }

        private int GetTitleOrdering(PackagePartViewModel firstPart, PackagePartViewModel secondPart)
        {
            return string.Compare(firstPart?.Title, secondPart?.Title, StringComparison.InvariantCultureIgnoreCase);
        }

        private int GetPackagePathOrdering(PackagePartViewModel firstPart, PackagePartViewModel secondPart)
        {
            return string.Compare(firstPart?.PackagePath, secondPart?.PackagePath, StringComparison.InvariantCultureIgnoreCase);
        }

        private int GetOrderingIndex(PackagePartViewModel part)
        {
            var orderIndex = ContentTypeOrdering.IndexOf(part?.ContentType);
            return orderIndex < 0 ? int.MaxValue : orderIndex;
        }
    }
}
