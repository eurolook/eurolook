﻿using System.Windows;
using ICSharpCode.AvalonEdit.Search;

namespace Eurolook.WordAddIn.BrickCommands.ViewOpenXmlPackageParts
{
    public partial class OpenXmlPackageViewerWindow : Window
    {
        public OpenXmlPackageViewerWindow(OpenXmlPackageViewerViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;

            SearchPanel.Install(AvalonEdit);
        }
    }
}
