﻿using System;
using Eurolook.AddIn.Common;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    [UsedImplicitly]
    public class InsertSectionCommand : EditCommandBase
    {
        private readonly IMessageService _messageService;

        public InsertSectionCommand(IMessageService messageService)
        {
            _messageService = messageService;
        }

        public override bool CanExecute(IBrickExecutionContext context, string argument)
        {
            if (context.Selection == null)
            {
                return false;
            }

            if (context.Selection.StoryType != WdStoryType.wdMainTextStory)
            {
                return false;
            }

            ////if (context.Selection.Range.IsLocked())
            ////    return false;

            ////if (context.Selection.Range.HasPlainTextContentControl())
            ////    return false;

            ////if (context.Selection.Range.IsInPlaceholderText() && !context.Selection.Range.IsVanishingContentControl())
            ////    return false;

            ////if (context.Selection.Range.IsBeforeOrAfterBlockLevelContentControl())
            ////    return false;

            return base.CanExecute(context, argument);
        }

        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            var newPageOrientation = argument?.IndexOf("portrait", StringComparison.InvariantCultureIgnoreCase) >= 0
                ? WdOrientation.wdOrientPortrait
                : WdOrientation.wdOrientLandscape;

            using (var documentAutomationHelper = new DocumentAutomationHelper(context, $"Apply {DisplayName} Brick"))
            {
                context.Selection.Range.HandleVanishingContentControl();

                var range = context.Selection.Range.Duplicate;

                if (range.Sections.Count > 1)
                {
                    _messageService.ShowSimpleMessage(
                        "A new section cannot be inserted because the current selection contains more than one section.",
                        DisplayName);
                    return Task.CompletedTask;
                }

                if (!IsRangeNonNestedTable(range) && StartsOrEndsInTable(range))
                {
                    _messageService.ShowSimpleMessage(
                        "A new section cannot be inserted because the selection is inside a table.",
                        DisplayName);
                    return Task.CompletedTask;
                }

                // cursor is at end of paragraph --> move to next line
                if (range.IsAtEndOfParagraph() && range.Start == range.End)
                {
                    range.MoveEnd(WdUnits.wdCharacter, 1);
                }

                var rangeBegin = range.Duplicate;
                rangeBegin.Collapse(WdCollapseDirection.wdCollapseStart);

                var rangeEnd = range.Duplicate;
                rangeEnd.Collapse(WdCollapseDirection.wdCollapseEnd);

                var currentSectionIndex = range.Sections.First.Index;

                if (!rangeEnd.IsEndOfStory())
                {
                    InsertSectionAndHandleBlockLevelContentControls(rangeEnd);
                }

                InsertSectionAndHandleBlockLevelContentControls(rangeBegin);

                var newSection = range.Document.Sections[currentSectionIndex + 1];
                newSection.PageSetup.Orientation = newPageOrientation;

                if (newSection.Index < range.Document.Sections.Count)
                {
                    var followingSection = range.Document.Sections[newSection.Index + 1];

                    followingSection.PageSetup.DifferentFirstPageHeaderFooter = 0;
                    SetRestartNumberingAtSection(followingSection, false);
                }

                newSection.PageSetup.DifferentFirstPageHeaderFooter = 0;

                SetRestartNumberingAtSection(newSection, false);

                AssertSectionEndsWithParagraphMark(range.Document.Sections[currentSectionIndex]);
                AssertSectionEndsWithParagraphMark(newSection);

                var rangeToSelect = AddPlaceholderToEmptySection(newSection);

                documentAutomationHelper.SelectWhenDone(rangeToSelect);
                context.Document.ActiveWindow.ScrollIntoView(rangeToSelect);
                return Task.CompletedTask;
            }
        }

        private void InsertSectionAndHandleBlockLevelContentControls(Range range)
        {
            switch (range.GetPositionRelativeToBlockLevelContentControl())
            {
                case RangeExtensions.RangePosition.BeforeBlockLevelContentControl:
                    range = range.InsertParagraphBeforeAndHandleLockedContentControls();
                    range.InsertBreak(WdBreakType.wdSectionBreakContinuous);
                    range.Paragraphs.First.Range.Delete();
                    break;

                case RangeExtensions.RangePosition.AfterBlockLevelContentControl:
                    range = range.InsertParagraphAfterAndHandleLockedContentControls();
                    range.InsertBreak(WdBreakType.wdSectionBreakContinuous);
                    break;

                default:
                    range.InsertBreak(WdBreakType.wdSectionBreakContinuous);
                    break;
            }
        }

        private static bool IsRangeNonNestedTable(Range range)
        {
            var table = range.Tables.Count > 0 ? range.Tables[1] : null;
            if (table == null)
            {
                return false;
            }

            return table.NestingLevel == 1 && range.Start == table.Range.Start && range.End == table.Range.End;
        }

        private static bool StartsOrEndsInTable(Range range)
        {
            var rangeBegin = range.CollapseToStart();
            var rangeEnd = range.CollapseToEnd();

            return rangeBegin.HasTables() || rangeEnd.HasTables();
        }

        private static Range AddPlaceholderToEmptySection(Section section)
        {
            var range = section.Range;
            range.MoveEndWhileSafe("\r\f", WdConstants.wdBackward);

            if ((range.Text != null) && ((range.Text?.Trim().Length != 0)
                                         || (range.Start != range.Paragraphs[1].Range.Start)
                                         || (range.End != range.Paragraphs[1].Range.End - 1)))
            {
                return range;
            }

            // EUROLOOK-1299: The range does not contain text but it might contain page/line/section/column breaks.
            //   To avoid that the break is included in content control the range is collapsed to the start.
            range.Collapse();
            range.ParagraphFormat.set_Style(WdBuiltinStyle.wdStyleNormal);

            if (range.Document.HasContentControlSupport())
            {
                try
                {
                    // We need an inline content control so we add a dummy text to avoid that a block-level
                    // content control is created. With block-level content controls it is not possible to insert elements
                    // such as another section break programmatically directly after the control.
                    range.Text = "DUMMY";
                    range.Collapse();

                    // EUROLOOK-1298: depending on the kind of content control we use for the placeholder text, Word has
                    //   a different paste behavior:
                    //
                    //     (1) When using a rich text content control, direct formatting is retained,
                    //         but if the pasted text ends with a paragraph mark, the text is pasted before the placeholder
                    //         and the placeholder is not removed (this is standard Word behavior when pasting text that
                    //         ends with a paragraph mark
                    //     (2) When using a plain text content control, pasting text ending with a paragraph mark replaces
                    //         the placeholder. However, any rich formatting and any paragraph marks are removed.
                    //
                    //   We decide to use option (1) because losing formatting and line breaks is unexpected to the user
                    //   and restoring direct formatting and line breaks is more work than simply removing a placeholder.
                    var cc = range.ContentControls.Add();
                    cc.SetPlaceholderText(null, null, "Type your text here.");
                    cc.Temporary = true;
                    range = cc.Range;
                }
                finally
                {
                    range.Paragraphs.First.Range.Find.Execute(
                        FindText: "DUMMY",
                        ReplaceWith: "",
                        Replace: WdReplace.wdReplaceOne);
                }
            }

            return range;
        }

        private static void AssertSectionEndsWithParagraphMark(Section section)
        {
            var range = section.Range;
            if ((range.Text?.EndsWith("\f") == true) && (range.Text?.EndsWith("\r\f") == false))
            {
                var rangeTmp = range.Duplicate;
                rangeTmp.Collapse(WdCollapseDirection.wdCollapseEnd);
                rangeTmp.Move(WdUnits.wdCharacter, -1);
                rangeTmp.InsertParagraph();
            }
        }

        private static void SetRestartNumberingAtSection(Section section, bool restartNumbering)
        {
            ApplyToAllHeadersAndFooters(section, hf => hf.PageNumbers.RestartNumberingAtSection = restartNumbering);
        }

        private static void SetLinkToPrevious(Section section, bool linkToPrevious)
        {
            ApplyToAllHeadersAndFooters(section, hf => hf.LinkToPrevious = linkToPrevious);
        }

        private static void ApplyToAllHeadersAndFooters(Section section, Action<HeaderFooter> action)
        {
            action(section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary]);
            action(section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage]);
            action(section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages]);
            action(section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary]);
            action(section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage]);
            action(section.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages]);
        }

        private static bool PageSetupEquals(PageSetup ps1, PageSetup ps2)
        {
            return (ps1.BottomMargin == ps2.BottomMargin) &&
                   (ps1.FooterDistance == ps2.FooterDistance) &&
                   (ps1.HeaderDistance == ps2.HeaderDistance) &&
                   (ps1.LeftMargin == ps2.LeftMargin) &&
                   (ps1.MirrorMargins == ps2.MirrorMargins) &&
                   (ps1.Orientation == ps2.Orientation) &&
                   (ps1.PageHeight == ps2.PageHeight) &&
                   (ps1.PageWidth == ps2.PageWidth) &&
                   (ps1.RightMargin == ps2.RightMargin) &&
                   (ps1.TextColumns.Count == ps2.TextColumns.Count) &&
                   (ps1.TopMargin == ps2.TopMargin) &&
                   (ps1.SectionStart == ps2.SectionStart);
        }

        private static bool SectionEquals(Section s1, Section s2)
        {
            return PageSetupEquals(s1.PageSetup, s2.PageSetup);
        }
    }
}
