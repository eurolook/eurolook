using Eurolook.WordAddIn.BrickEngine;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    [UsedImplicitly]
    public class ApplyKeepParagraphTogether : ApplyStyleCommandBase
    {
        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (
                new DocumentAutomationHelper(
                    context,
                    "Keep Together",
                    DocumentAutomationOption.Default | DocumentAutomationOption.RestoreSelection))
            {
                foreach (Paragraph paragraph in context.Selection.Paragraphs)
                {
                    SetKeepParagraphTogether(paragraph);
                }
            }

            return Task.CompletedTask;
        }

        private static void SetKeepParagraphTogether(Paragraph paragraph)
        {
            if (paragraph.KeepTogether == 0)
            {
                paragraph.KeepTogether = -1;
            }
            else
            {
                paragraph.KeepTogether = 0;
            }
        }
    }
}
