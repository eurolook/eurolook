﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.OpenXml;
using Eurolook.WordAddIn.BrickEngine;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    public abstract class EditCommandBase : IBrickCommand, ICanLog, IDisposable
    {
        private readonly SemaphoreSlim _semaphoreSlim = new(1, 1);
        private bool _isExecuting;

        ~EditCommandBase()
        {
            Dispose(false);
        }

        public string DisplayName { get; set; }

        public CommandBrick Brick { get; set; }

        public ILocalisedResourceResolver LocalisedResourceResolver { get; set; }

        public virtual bool HasCustomUI => false;

        public virtual bool HasAuthorBinding => false;

        public virtual IEnumerable<string> RequiredStyles => Enumerable.Empty<string>();

        public virtual bool CanExecute(IBrickExecutionContext context, string argument)
        {
            return context != null;
        }

        public async Task ExecuteAsync(IBrickExecutionContext context, string argument)
        {
            // NOTE: using double-checked locking to avoid parallel execution
            if (!_isExecuting)
            {
                try
                {
                    await _semaphoreSlim.WaitAsync();

                    if (!_isExecuting)
                    {
                        try
                        {
                            if (!CanExecute(context, argument))
                            {
                                return;
                            }

                            _isExecuting = true;

                            await OnExecuteAsync(context, argument);
                        }
                        finally
                        {
                            _isExecuting = false;
                        }
                    }
                }
                finally
                {
                    _semaphoreSlim.Release();
                }
            }
        }

        public virtual bool ShowCheckmark(List<BrickContainer> brickContainers)
        {
            return false;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected abstract Task OnExecuteAsync(IBrickExecutionContext context, string argument);

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _semaphoreSlim?.Dispose();
            }
        }
    }
}
