using System;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.BrickEngine;

namespace Eurolook.WordAddIn.BrickCommands
{
    public class InsertMarkingCommand : EditCommandBase
    {
        private readonly IDocumentManager _documentManager;

        public InsertMarkingCommand(IDocumentManager documentManager)
        {
            _documentManager = documentManager;
        }

        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            var ids = argument?.Split(',', ';');
            if (ids == null || ids.Length != 2)
            {
                this.LogError($"{nameof(InsertMarkingCommand)} failed. Argument doesn't contain two brick ids: '{argument}'.");
                return;
            }

            string id1 = ids[0].Trim();
            string id2 = ids[1].Trim();
            if (!Guid.TryParse(id1, out var brickId1) || !Guid.TryParse(id2, out var brickId2))
            {
                this.LogError($"{nameof(InsertMarkingCommand)} failed. Argument doesn't contain two brick ids: '{argument}'.");
                return;
            }

            var documentViewModel = _documentManager.GetActiveDocumentViewModel();
            var structure1 = documentViewModel.DocumentStructures.FirstOrDefault(x => x.BrickId == brickId1);
            var structure2 = documentViewModel.DocumentStructures.FirstOrDefault(x => x.BrickId == brickId2);
            if (structure1 == null || structure2 == null)
            {
                this.LogError($"{nameof(InsertMarkingCommand)} failed. Could not find bricks '{id1}', '{id2}' in the structure of '{documentViewModel.DocumentModel?.DisplayName}'.");
                return;
            }

            using (new DocumentAutomationHelper(context, DisplayName))
            {
                if (structure1.Position == PositionType.Begin)
                {
                    // insert the footer brick first, then the marking
                    // the marking will be selected by InsertBrick
                    await documentViewModel.InsertBrick(structure2.Brick, true);
                    await documentViewModel.InsertBrick(structure1.Brick, true);
                }
                else
                {
                    await documentViewModel.InsertBrick(structure1.Brick, true);
                    await documentViewModel.InsertBrick(structure2.Brick, true);
                }
            }
        }
    }
}
