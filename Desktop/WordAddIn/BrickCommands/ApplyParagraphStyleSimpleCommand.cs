﻿using System.Threading.Tasks;
using Eurolook.WordAddIn.BrickEngine;

namespace Eurolook.WordAddIn.BrickCommands
{
    public class ApplyParagraphStyleSimpleCommand : ApplyParagraphStyleCommand
    {
        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            return base.OnExecuteAsync(context, argument + ", ignoreTextLevels");
        }
    }
}
