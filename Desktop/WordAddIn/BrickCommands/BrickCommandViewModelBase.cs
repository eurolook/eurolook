using Eurolook.AddIn.Common.ViewModels;

namespace Eurolook.WordAddIn.BrickCommands
{
    public abstract class BrickCommandViewModelBase : DialogViewModelBase
    {
        protected BrickCommandViewModelBase(IEurolookWordDocument eurolookDocument)
        {
            EurolookDocument = eurolookDocument;
        }

        public IEurolookWordDocument EurolookDocument { get; protected set; }
    }
}
