﻿using System;
using Eurolook.AddIn.Common;
using Eurolook.Common.Log;
using Eurolook.Data.Constants;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    public class ApplyShiftClosing : ApplyStyleCommandBase
    {
        private readonly string _styleNameLeft = "ClosingL";
        private readonly string _styleNameRight = "Closing";
        private readonly IMessageService _messageService;

        public ApplyShiftClosing(IMessageService messageService)
        {
            _messageService = messageService;
        }

        public override bool CanExecute(IBrickExecutionContext context, string argument)
        {
            return true;
        }

        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            if (string.IsNullOrEmpty(argument))
            {
                return Task.CompletedTask;
            }

            using (new DocumentAutomationHelper(context, $"Apply Shift Closing: {argument}"))
            {
                var closingBrick = FindFirstClosingBrick(context.Document);
                if (closingBrick == null)
                {
                    _messageService.ShowSimpleMessage(
                        "Cannot shift the closing. There is no closing brick in the document.",
                        DisplayName);
                    return Task.CompletedTask;
                }

                if (argument.Equals(_styleNameLeft, StringComparison.InvariantCultureIgnoreCase))
                {
                    var styleLeft = LanguageIndependentStyle.FromNeutralStyleName(_styleNameLeft, context.Document);
                    StyleConceptNew.ApplyStyleToRange(styleLeft, closingBrick.ContentControl.Range);
                }
                else if (argument.Equals(_styleNameRight, StringComparison.InvariantCultureIgnoreCase))
                {
                    var styleRight = LanguageIndependentStyle.FromNeutralStyleName(_styleNameRight, context.Document);
                    StyleConceptNew.ApplyStyleToRange(styleRight, closingBrick.ContentControl.Range);
                }
                else
                {
                    this.LogWarn($"Invalid argument: {argument}.");
                }
            }

            return Task.CompletedTask;
        }

        private BrickContainer FindFirstClosingBrick(Document document)
        {
            foreach (var brickContainer in document.GetAllBrickContainers())
            {
                if (brickContainer.GroupId == CommonDataConstants.ClosingGroupId)
                {
                    return brickContainer;
                }
            }

            return null;
        }
    }
}
