using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;

namespace Eurolook.WordAddIn.BrickCommands
{
    public class ApplyHeadingStyleCommand : ApplyStyleCommandBase
    {
        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (new DocumentAutomationHelper(context, $"Apply {argument} Brick"))
            {
                string styleName = GetStyleName(argument);
                string placeholderText = GetPlaceholderText(argument, "Type your heading here.");

                var range = context.Selection.Range.Duplicate;
                var document = range.Document;

                var newStyleNameInfo = StyleNameInfo.FromStyleName(styleName);
                if (newStyleNameInfo == null)
                {
                    this.LogError($"Style {styleName} does not have a valid naming pattern.");
                    return;
                }

                var currentStyle = LanguageIndependentStyle.FromRange(range.Paragraphs.First.Range);
                // in case that
                // a) ApplyHeadingStyleCommand is used on an existing heading and
                // b) the given style name has no level defined
                // the text level of the previous heading is kept
                if (StyleConceptNew.IsHeadingStyle(currentStyle) && newStyleNameInfo.TextLevel == 0)
                {
                    var currentStyleNameInfo = StyleNameInfo.FromStyleName(currentStyle.NameNeutral);
                    int textLevel = currentStyleNameInfo.TextLevel;
                    newStyleNameInfo = new StyleNameInfo(newStyleNameInfo.Category, textLevel, newStyleNameInfo.ListLevel);
                }

                string styleNameNeutral;
                if (newStyleNameInfo.TextLevel == 0)
                {
                    styleNameNeutral = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(styleName, range);
                }
                else
                {
                    newStyleNameInfo = StyleConceptNew.GetAvailableStyleWithMaximumTextLevel(newStyleNameInfo, document);
                    if (newStyleNameInfo == null)
                    {
                        this.LogError($"Style with maximum text level for style {styleName} not found in document");
                        return;
                    }

                    var style = LanguageIndependentStyle.FromNeutralStyleName(newStyleNameInfo.StyleName, document);
                    styleNameNeutral = style?.NameNeutral;
                }

                if (styleNameNeutral == null)
                {
                    this.LogError($"Style to be applied for style {styleName} not found in document.");
                    return;
                }

                range.HandleVanishingContentControl();
                await ApplyStyleWithPlaceholder(range, styleNameNeutral, placeholderText, true);
            }
        }
    }
}
