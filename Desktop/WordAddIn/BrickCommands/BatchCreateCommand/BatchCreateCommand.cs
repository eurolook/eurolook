using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Eurolook.AddIn.Common;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.DocumentCreation;
using Eurolook.WordAddIn.ViewModels;
using Eurolook.WordAddIn.Views;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Language = Eurolook.Data.Models.Language;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands.BatchCreateCommand
{
    [UsedImplicitly]
    public class BatchCreateCommand : EditCommandBase, ICanLog
    {
        private readonly IMessageService _messageService;
        private readonly IEnumerable<IBeforeNewDocumentCreationTask> _beforeDocumentCreationTasks;
        private readonly IEnumerable<Func<IBrickExecutionContext, IAfterNewDocumentCreationTask>> _afterDocumentCreationTasks;
        private readonly Func<CreateDocumentViewModel> _createDocumentViewModelFunc;
        private readonly IDocumentCreationService _documentCreationService;

        public BatchCreateCommand(
            IMessageService messageService,
            IEnumerable<IBeforeNewDocumentCreationTask> beforeDocumentCreationTasks,
            IEnumerable<Func<IBrickExecutionContext, IAfterNewDocumentCreationTask>> afterDocumentCreationTasks,
            Func<CreateDocumentViewModel> createDocumentViewModelFunc,
            IDocumentCreationService documentCreationService)
        {
            _messageService = messageService;
            _beforeDocumentCreationTasks = beforeDocumentCreationTasks;
            _afterDocumentCreationTasks = afterDocumentCreationTasks;
            _createDocumentViewModelFunc = createDocumentViewModelFunc;
            _documentCreationService = documentCreationService;
        }

        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            try
            {
                var viewModel = _createDocumentViewModelFunc();
                viewModel.Title = DisplayName;
                viewModel.HelpReference = "CreateDocument";
                viewModel.CreateDocumentFunc =
                    async () =>
                    {
                        var folderPath = _messageService.ShowBrowseForFolderDialog("Select Folder");

                        if (string.IsNullOrEmpty(folderPath) || !Directory.Exists(folderPath))
                        {
                            return;
                        }

                        viewModel.IsCreating = true;
                        var documentCreationInfo = viewModel.GetDocumentCreationInfo();
                        var afterDocumentCreationTasks =
                            _afterDocumentCreationTasks.Select(t => t(context)).ToList();

                        var languages = viewModel.LanguageSelectionVm.Languages.Select(vm => vm.Model);
                        foreach (var language in languages)
                        {
                            documentCreationInfo.Language = language;
                            var result = await _documentCreationService.CreateDocumentAsync(
                                documentCreationInfo,
                                _beforeDocumentCreationTasks,
                                afterDocumentCreationTasks,
                                ProgressReporter<int>.Empty);

                            SaveAndCloseDocument(
                                result.Document,
                                documentCreationInfo.DocumentModel,
                                language,
                                folderPath);
                        }
                    };
                var win = new CreateDocumentWindow(viewModel) { Title = $"Eurolook - {DisplayName}" };
                _messageService.ShowSingleton(win);
                return Task.CompletedTask;
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                return Task.CompletedTask;
            }
        }

        private static void SaveAndCloseDocument(
            Document document,
            DocumentModel documentModel,
            Language language,
            string folderPath)
        {
            var fileName = $"{documentModel.DisplayName}_{language.Name}.docx";
            var fullPath = Path.Combine(folderPath, fileName);
            document.SaveAs(fullPath);
            document.Close();
        }
    }
}
