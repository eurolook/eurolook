﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Common.Extensions;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    public abstract class ApplyStyleCommandBase : EditCommandBase
    {
        public override bool CanExecute(IBrickExecutionContext context, string argument)
        {
            if (!base.CanExecute(context, argument))
            {
                return false;
            }

            var selection = context.Selection;
            if (selection == null)
            {
                return false;
            }

            if (selection.Range.IsLocked())
            {
                return false;
            }

            // TODO: Add flag to brick whether formatting may be applied
            ////var brickContentControl = selection.Range.GetContentControl().GetBrickContentControl();
            ////if (brickContentControl != null && !brickContentControl.Temporary)
            ////{
            ////    var brickContainer = brickContentControl.GetBrickContainer();
            ////    bool isCursorBrick = context.EurolookDocument.DocumentStructures.Where(ds => ds.BrickId == brickContainer.Id)
            ////                                .Any(ds => ds.Position == PositionType.Cursor);
            ////    return isCursorBrick;
            ////}

            return true;
        }

        protected string GetStyleName(string argument)
        {
            if (string.IsNullOrWhiteSpace(argument))
            {
                return null;
            }

            var args = argument.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (args.Length <= 0)
            {
                return null;
            }

            return args[0].Trim();
        }

        protected string GetPlaceholderText(string argument, string defaultPlaceholder)
        {
            if (string.IsNullOrWhiteSpace(argument))
            {
                return defaultPlaceholder;
            }

            var args = argument
                .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(s => s.Trim())
                .Where(s => !s.EqualsIgnoreCase("force") && !s.EqualsIgnoreCase("ignoretextlevels"))
                .ToArray();

            string styleName = args.Length > 0 ? args[0].Trim() : null;
            string placeholderFromArgument = args.Length > 1 ? args[1].Trim() : null;

            if (string.Equals(styleName, "Normal", StringComparison.InvariantCultureIgnoreCase) || string.Equals(styleName, "Text", StringComparison.InvariantCultureIgnoreCase))
            {
                return "Type your text here.";
            }

            if (styleName?.IndexOf("list", StringComparison.InvariantCultureIgnoreCase) >= 0)
            {
                return "Type your list item here.";
            }

            return placeholderFromArgument ?? defaultPlaceholder;
        }

        protected bool ForceStyle(string argument)
        {
            if (string.IsNullOrWhiteSpace(argument))
            {
                return false;
            }

            var args = argument.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            return args.Length > 0 && args[args.Length - 1].Trim().Equals("force", StringComparison.InvariantCultureIgnoreCase);
        }

        protected bool IgnoreTextLevels(string argument)
        {
            if (string.IsNullOrWhiteSpace(argument))
            {
                return false;
            }

            var args = argument.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            return args.Length > 0 && args[args.Length - 1].Trim().Equals("ignoretextlevels", StringComparison.InvariantCultureIgnoreCase);
        }

        protected Task ApplyStyleRespectListContinue(Paragraph paragraph, string styleNameNeutral)
        {
            var style = LanguageIndependentStyle.FromNeutralStyleName(styleNameNeutral, paragraph.Range.Document);
            StyleConceptNew.ApplyStyleToParagraphRespectListContinue(style, paragraph);

            return Task.FromResult(0);
        }

        protected Task ApplyTableStyle(Table table, string styleNameNeutral)
        {
            var style = LanguageIndependentStyle.FromNeutralStyleName(styleNameNeutral, table.Range.Document);
            StyleConceptNew.ApplyStyleToTable(style, table);

            return Task.FromResult(0);
        }

        protected void ApplyCharacterStyle(Selection selection, LanguageIndependentStyle style)
        {
            selection.Range.HandleVanishingContentControl();
            try
            {
                var charStyle = (Style)selection.Range.CharacterStyle;
                var currentStyle = LanguageIndependentStyle.FromStyle(charStyle, selection.Document);
                if (style.NameNeutral == currentStyle.NameNeutral)
                {
                    if (selection.Start == selection.End)
                    {
                        var defaultParagraphFont = LanguageIndependentStyle.FromNeutralStyleName("Default Paragraph Font", selection.Document);
                        selection.set_Style(defaultParagraphFont.Style);
                    }
                    else
                    {
                        selection.ClearCharacterStyle();
                    }
                }
                else
                {
                    // ReSharper disable once UseIndexedProperty
                    selection.set_Style(style.Style);
                }
            }
            catch
            {
                selection.set_Style(style.Style);
            }
        }

        protected Task ApplyStyleWithPlaceholder(Range rangeOrig, string styleNameNeutral, string placeholderText, bool forceApply = true)
        {
            rangeOrig.HandleVanishingContentControl();

            // NOTE: does not always work, e.g. when there is a partial selection within a content control
            var range = rangeOrig.Duplicate;

            var style = LanguageIndependentStyle.FromNeutralStyleName(styleNameNeutral, rangeOrig.Document);
            StyleConceptNew.ApplyStyleToRange(style, range, forceApply);

            range.Expand(WdUnits.wdParagraph);

            // remove any existing placeholder, i.e. only one cc from start to end of paragraph
            if (range.Paragraphs.Count == 1 && range.Paragraphs[1].Range.ContentControls.Count == 1)
            {
                var p = range.Paragraphs[1];
                var cc = p.Range.ContentControls[1];

                if (cc.Temporary && p.Range.Start + 1 == cc.Range.Start && p.Range.End - 2 == cc.Range.End)
                {
                    range.Paragraphs[1].Range.ContentControls[1].Range.Delete();
                }
            }

            if (range.Text.Trim().Length == 0 &&
                range.Start == range.Paragraphs[1].Range.Start
                && range.End == range.Paragraphs[1].Range.End)
            {
                // EUROLOOK-1299: The range does not contain text but it might contain page/line/section/column breaks.
                //   To avoid that the break is included in content control the range is collapsed to the start.
                range.Collapse();

                if (range.Document.HasContentControlSupport())
                {
                    // EUROLOOK-1298: depending on the kind of content control we use for the placeholder text, Word has
                    //   a different paste behavior:
                    //
                    //     (1) When using a rich text content control, direct formatting is retained,
                    //         but if the pasted text ends with a paragraph mark, the text is pasted before the placeholder
                    //         and the placeholder is not removed (this is standard Word behavior when pasting text that ends
                    //         with a paragraph mark
                    //     (2) When using a plain text content control, pasting text ending with a paragraph mark replaces the
                    //         the placeholder. However, any rich formatting and any paragraph marks are removed.
                    //
                    //   We decide to use option (1) because losing formatting and line breaks is unexpected to the user and
                    //   restoring direct formatting and line breaks is more work than simply removing a placeholder.
                    var cc = range.ContentControls.Add();
                    cc.SetPlaceholderText(null, null, placeholderText);
                    cc.Temporary = true;
                    cc.Range.Select();
                }
            }

            return Task.FromResult(0);
        }

        protected bool IsListSelection(IBrickExecutionContext context)
        {
            foreach (Paragraph paragraph in context.Selection.Range.Paragraphs)
            {
                bool isListStyle = StyleConceptNew.IsListParagraph(paragraph);
                if (!isListStyle)
                {
                    return false;
                }
            }

            return true;
        }

        protected LanguageIndependentStyle GetLanguageIndependentStyle(IBrickExecutionContext context, string argument)
        {
            if (!LanguageIndependentStyle.IsStyleAvailable(argument, context.Document))
            {
                // TODO: should this be the language specific template?
                context.Document.CopyStylesFromDocumentModel(new List<string>() { argument }, context.EurolookDocument.DocumentModel);
            }

            return LanguageIndependentStyle.FromNeutralStyleName(argument, context.Document);
        }
    }
}
