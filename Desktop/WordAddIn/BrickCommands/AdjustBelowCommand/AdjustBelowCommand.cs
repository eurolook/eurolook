﻿using Eurolook.WordAddIn.BrickEngine;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands.AdjustBelowCommand
{
    public class AdjustBelowCommand : AdjustCommandBase
    {
        public AdjustBelowCommand(IWordMessageService messageService)
            : base(messageService)
        {
        }

        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (new DocumentAutomationHelper(context, DisplayName))
            {
                if (context.Selection.StoryType != WdStoryType.wdMainTextStory)
                {
                    return;
                }

                int start = context.Selection.Start;
                int end = context.Document.StoryRanges[context.Selection.StoryType].End;

                await ExecuteAsync(context.Document.Range(start, end));
            }
        }
    }
}
