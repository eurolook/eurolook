using System;
using System.Collections.ObjectModel;
using Eurolook.DocumentProcessing;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;

namespace Eurolook.WordAddIn.BrickCommands.AdjustBelowCommand
{
    public class AdjustCommandAskUserViewModel : ViewModelBase
    {
        private string _selectectedStyle;

        public AdjustCommandAskUserViewModel()
        {
            AvailableStyles = new ObservableCollection<string>();
        }

        public ObservableCollection<string> AvailableStyles { get; }

        public string SelectedStyle
        {
            get { return _selectectedStyle; }
            set
            {
                _selectectedStyle = value;
                RaisePropertyChanged(() => SelectedStyle);
                SetTextLevelCommand.RaiseCanExecuteChanged();
            }
        }

        public int? Level { get; private set; }

        public Action CloseAction { get; set; }

        public RelayCommand SetTextLevelCommand
        {
            get { return new RelayCommand(SetTextLevel, CanExecuteSetTextLevel); }
        }

        public RelayCommand IgnoreTextLevelCommand
        {
            get { return new RelayCommand(IgnoreTextLevel); }
        }

        private bool CanExecuteSetTextLevel()
        {
            return !string.IsNullOrEmpty(SelectedStyle);
        }

        private void SetTextLevel()
        {
            try
            {
                Level = StyleNameInfo.FromStyleName(SelectedStyle).TextLevel;
            }
            catch (Exception)
            {
            }

            CloseAction?.Invoke();
        }

        private void IgnoreTextLevel()
        {
            Level = null;
            CloseAction?.Invoke();
        }
    }
}
