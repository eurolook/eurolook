﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.DocumentProcessing;
using Paragraph = Microsoft.Office.Interop.Word.Paragraph;
using Range = Microsoft.Office.Interop.Word.Range;
using Table = Microsoft.Office.Interop.Word.Table;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands.AdjustBelowCommand
{
    public abstract class AdjustCommandBase : EditCommandBase
    {
        private readonly IWordMessageService _messageService;
        private int _currentTextLevel;

        public AdjustCommandBase(IWordMessageService messageService)
        {
            _messageService = messageService;
        }

        protected Task ExecuteAsync(Range rangeToAdjust)
        {
            _currentTextLevel = StyleConceptNew.RetrieveTextLevel(rangeToAdjust);

            var style = LanguageIndependentStyle.FromParagraph(rangeToAdjust.Paragraphs.First);
            if (_currentTextLevel == 0 && StyleConceptNew.IsHeadingStyle(style))
            {
                _currentTextLevel = StyleNameInfo.FromStyleName(style.NameNeutral).TextLevel;
            }

            var listRestarts = StyleConceptListRestart.GetListRestarts(rangeToAdjust);

            foreach (Paragraph paragraph in rangeToAdjust.Paragraphs)
            {
                AdjustParagraphStyle(paragraph);
            }

            StyleConceptListRestart.ApplyListRestarts(listRestarts);

            return Task.FromResult(0);
        }

        private void AdjustParagraphStyle(Paragraph paragraph)
        {
            if (paragraph == null)
            {
                return;
            }

            if (paragraph.Range.Tables.Count > 0)
            {
                foreach (Table table in paragraph.Range.Tables)
                {
                    AdjustTableStyle(table);
                }

                return;
            }

            var style = LanguageIndependentStyle.FromParagraph(paragraph);
            if (style == null)
            {
                return;
            }

            var document = paragraph.Range.Document;

            // correct list endings
            if (StyleConceptNew.IsListEnding(paragraph))
            {
                int currentTextLevel = StyleConceptNew.RetrieveTextLevel(paragraph.Range);
                var textStyleInfo = new StyleNameInfo("Text", currentTextLevel);
                textStyleInfo = StyleConceptNew.GetAvailableStyleWithMaximumTextLevel(textStyleInfo, document);
                var textStyle = LanguageIndependentStyle.FromNeutralStyleName(textStyleInfo.StyleName, document);
                StyleConceptNew.ApplyStyleToParagraph(textStyle, paragraph);
                return;
            }

            var styleNameInfo = StyleNameInfo.FromStyleName(style.NameNeutral);
            if (styleNameInfo.TextLevel == _currentTextLevel)
            {
                return;
            }

            if (StyleConceptNew.IsHeadingStyle(style))
            {
                _currentTextLevel = ValidateHeadingLevel(paragraph.Range, style, styleNameInfo);
            }

            styleNameInfo = new StyleNameInfo(styleNameInfo.Category, _currentTextLevel, styleNameInfo.ListLevel);
            styleNameInfo = StyleConceptNew.GetAvailableStyleWithMaximumTextLevel(styleNameInfo, document);

            if (styleNameInfo?.StyleName != null && !string.Equals(style.NameNeutral, styleNameInfo.StyleName))
            {
                var newStyle = LanguageIndependentStyle.FromNeutralStyleName(
                    styleNameInfo.StyleName,
                    paragraph.Range.Document);
                StyleConceptNew.ApplyStyleToParagraphRespectListContinue(newStyle, paragraph);
            }
        }

        private void AdjustTableStyle(Table table)
        {
            if (table == null)
            {
                return;
            }

            var style = LanguageIndependentStyle.FromTable(table);
            if (style == null)
            {
                return;
            }

            var styleNameInfo = StyleNameInfo.FromStyleName(style.NameNeutral);
            if (styleNameInfo.TextLevel == _currentTextLevel)
            {
                return;
            }

            var document = table.Range.Document;

            styleNameInfo = new StyleNameInfo(styleNameInfo.Category, _currentTextLevel, styleNameInfo.ListLevel);
            styleNameInfo = StyleConceptNew.GetAvailableStyleWithMaximumTextLevel(styleNameInfo, document);

            if (styleNameInfo?.StyleName != null && !string.Equals(style.NameNeutral, styleNameInfo.StyleName))
            {
                var newStyle = LanguageIndependentStyle.FromNeutralStyleName(
                    styleNameInfo.StyleName,
                    document);
                StyleConceptNew.ApplyStyleToTable(newStyle, table);
            }
        }

        private int ValidateHeadingLevel(Range range, LanguageIndependentStyle style, StyleNameInfo styleNameInfo)
        {
            int newTextLevel = StyleConceptNew.GetHeadingLevel(style);
            if (newTextLevel <= _currentTextLevel + 1)
            {
                return Math.Max(0, newTextLevel);
            }

            var availableStyles = StyleConceptNew.GetAvailableStyles(
                styleNameInfo.TextDemote(),
                range.Document,
                _currentTextLevel + 1);
            return AskUser(range, availableStyles) ?? styleNameInfo.TextLevel;
        }

        private int? AskUser(Range range, List<StyleNameInfo> availableStyles)
        {
            var viewModel = new AdjustCommandAskUserViewModel();
            foreach (var styleName in availableStyles)
            {
                viewModel.AvailableStyles.Add(styleName.StyleName);
            }

            viewModel.SelectedStyle = viewModel.AvailableStyles.FirstOrDefault();

            range.Document.ActiveWindow.ScrollIntoView(range);
            bool screenUpdating = range.Application.ScreenUpdating;
            range.Application.ScreenUpdating = true;
            range.Select();
            range.Application.ScreenRefresh();
            range.Application.ScreenUpdating = screenUpdating;
            var window = new AdjustCommandAskUserWindow(viewModel);
            _messageService.ShowDialogNextToRange(window, range, 150);
            return viewModel.Level;
        }
    }
}
