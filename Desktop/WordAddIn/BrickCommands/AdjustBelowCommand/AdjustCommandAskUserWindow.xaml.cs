namespace Eurolook.WordAddIn.BrickCommands.AdjustBelowCommand
{
    public partial class AdjustCommandAskUserWindow
    {
        public AdjustCommandAskUserWindow(AdjustCommandAskUserViewModel viewModel)
        {
            DataContext = viewModel;
            if (viewModel.CloseAction == null)
            {
                viewModel.CloseAction = Close;
            }

            InitializeComponent();
        }
    }
}
