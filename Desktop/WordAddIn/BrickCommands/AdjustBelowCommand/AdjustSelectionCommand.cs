﻿using System.Threading.Tasks;
using Eurolook.WordAddIn.BrickEngine;

namespace Eurolook.WordAddIn.BrickCommands.AdjustBelowCommand
{
    public class AdjustSelectionCommand : AdjustCommandBase
    {
        public AdjustSelectionCommand(IWordMessageService messageService)
            : base(messageService)
        {
        }

        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (new DocumentAutomationHelper(context, DisplayName))
            {
                await ExecuteAsync(context.Selection.Range.Duplicate);
            }
        }
    }
}
