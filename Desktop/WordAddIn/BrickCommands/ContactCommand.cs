using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.DocumentProcessing;
using Eurolook.WordAddIn.BrickEngine;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.BrickCommands
{
    [UsedImplicitly]
    public class ContactCommand : EditCommandBase
    {
        private readonly Func<IEurolookWordDocument, AddContactCommand> _addContactCommandFunc;

        public ContactCommand(Func<IEurolookWordDocument, AddContactCommand> addContactCommandFunc)
        {
            _addContactCommandFunc = addContactCommandFunc;
        }

        public override bool ShowCheckmark(List<BrickContainer> brickContainers)
        {
            return brickContainers.Any(b => b.Id == AddContactCommand.ContactBrickId);
        }

        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            var addContactCommand = _addContactCommandFunc(context.EurolookDocument);
            await addContactCommand.ExecuteAsync(context, null);
        }
    }
}
