using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    public class InsertPageBreakCommand : EditCommandBase
    {
        public override bool CanExecute(IBrickExecutionContext context, string argument)
        {
            if (context.Selection == null)
            {
                return false;
            }

            if (context.Selection.StoryType != WdStoryType.wdMainTextStory)
            {
                return false;
            }

            ////if (context.Selection.Range.IsLocked())
            ////    return false;

            ////if (context.Selection.Range.HasPlainTextContentControl())
            ////    return false;

            ////if (context.Selection.Range.IsInPlaceholderText() && !context.Selection.Range.IsVanishingContentControl())
            ////    return false;

            ////if (context.Selection.Range.IsBeforeOrAfterBlockLevelContentControl())
            ////    return false;

            return base.CanExecute(context, argument);
        }

        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (new DocumentAutomationHelper(context, $"Apply {DisplayName} Brick"))
            {
                context.Selection.Range.HandleVanishingContentControl();

                var brick = context.Selection.Range.FindBrickContentControl();
                if (brick != null && !brick.Temporary
                    && (brick.LockContents || brick.IsBlockLevel() || brick.Type != WdContentControlType.wdContentControlRichText))
                {
                    InsertPageBreakAfterContentControl(context, brick);
                }
                else
                {
                    InsertPageBreak(context);
                }

                context.Document.ActiveWindow.ScrollIntoView(context.Selection.Range);
                return Task.FromResult(0);
            }
        }

        private static void InsertPageBreak(IBrickExecutionContext context)
        {
            context.Selection.InsertBreak(WdBreakType.wdPageBreak);
        }

        private static void InsertPageBreakAfterContentControl(IBrickExecutionContext context, ContentControl brick)
        {
            // NOTE: do *not* use Range.InsertBreak(WdBreakType.wdPageBreak) but use \f\r instead to create
            //    the page break after block-level content controls.
            string pagebreak = "\f\r";

            // if inside brick insert the page break after the brick
            context.Selection.Start = brick.Range.Paragraphs.Last.Range.End - 1;
            context.Selection.End = context.Selection.Start;

            if (context.Selection.Range.HasTables())
            {
                // is inside a brick table move selection after the table
                context.Selection.Range.Tables[1].Range.Select();
                context.Selection.Collapse(WdCollapseDirection.wdCollapseEnd);
            }
            else
            {
                // only insert \f because there is already a line break after the content control
                pagebreak = "\f";
            }

            context.Selection.Text = pagebreak;

            // move selection *after* the pagebreak
            context.Selection.Move(WdUnits.wdParagraph, 1);
        }
    }
}
