using System;
using System.Threading.Tasks;
using Eurolook.WordAddIn.BrickEngine;

namespace Eurolook.WordAddIn.BrickCommands
{
    public class InsertLetterBackgroundCommand : EditCommandBase
    {
        public static readonly Guid HeaderBrickGuid = new Guid("67239E97-6B52-4FFF-A07A-AB5F7F522A93");
        public static readonly Guid FooterBrickGuid = new Guid("708BC4B2-26A9-4F8E-AAC2-2F92791549E1");
        private readonly IDocumentManager _documentManager;

        public InsertLetterBackgroundCommand(IDocumentManager documentManager)
        {
            _documentManager = documentManager;
        }

        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            var documentViewModel = _documentManager.GetActiveDocumentViewModel();
            var headerBrickStructure = documentViewModel.DocumentStructures.Find(x => x.BrickId == HeaderBrickGuid);
            var footerBrickStructure = documentViewModel.DocumentStructures.Find(x => x.BrickId == FooterBrickGuid);

            using (new DocumentAutomationHelper(context, DisplayName))
            {
                if (headerBrickStructure != null && footerBrickStructure != null)
                {
                    await documentViewModel.InsertBrick(headerBrickStructure.Brick, true);
                    await documentViewModel.InsertBrick(footerBrickStructure.Brick, true);
                }
            }
        }
    }
}
