using System;
using Eurolook.AddIn.Common;
using Eurolook.WordAddIn.Messages;
using Eurolook.WordAddIn.ViewModels;
using GalaSoft.MvvmLight.Messaging;

namespace Eurolook.WordAddIn.BrickCommands
{
    public abstract class BrickCommandPanelViewModelBase : BrickCommandViewModelBase, ICommandBrickPanel
    {
        private Action _cancelAction;
        private Action _commitAction;

        protected BrickCommandPanelViewModelBase(IEurolookWordDocument eurolookDocument)
            : base(eurolookDocument)
        {
        }

        public void Show(Action commitAction, Action cancelAction = null)
        {
            Messenger.Default.Send(new ShowCommandBrickPanelMessage { CommandBrickPanel = this });

            _commitAction = commitAction;
            _cancelAction = cancelAction;
        }

        public override void Cancel()
        {
            Execute.OnUiThread(
                () =>
                {
                    Close();
                    _cancelAction?.Invoke();
                });
        }

        public override void Commit()
        {
            Execute.OnUiThread(
                () =>
                {
                    _commitAction?.Invoke();
                    Close();
                });
        }

        protected virtual void OnClosing()
        {
        }

        private void Close()
        {
            Messenger.Default.Send(new LeaveCommandBrickPanelMessage { CommandBrickPanel = this });
            OnClosing();
        }
    }
}
