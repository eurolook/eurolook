﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Eurolook.AddIn.Common;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.BrickCommands
{
    public abstract class InsertObjectCommandBase : UIBrickCommandBase
    {
        public enum TitleInsertionPosition
        {
            TitleAboveInsertedContent = 0,
            TitleBelowInsertedContent,
        }

        protected InsertObjectCommandBase(IDocumentManager documentManager, IMessageService messageService)
        {
            DocumentManager = documentManager;
            MessageService = messageService;
        }

        public override IEnumerable<string> RequiredStyles
        {
            get
            {
                yield return "Figure Title";
                yield return "Figure Body";
                yield return "Figure Source";
            }
        }

        protected IDocumentManager DocumentManager { get; }

        protected IMessageService MessageService { get; }

        public override bool CanExecute(IBrickExecutionContext context, string argument)
        {
            var document = context.Document;
            if (document == null)
            {
                return false;
            }

            var selection = context.Selection;
            if (selection == null)
            {
                return false;
            }

            if (selection.Range.IsLocked())
            {
                return false;
            }

            if (selection.Range.HasPlainTextContentControl())
            {
                return false;
            }

            if ((bool)selection.Range.Information[WdInformation.wdAtEndOfRowMarker])
            {
                return false;
            }

            return base.CanExecute(context, argument);
        }

        protected virtual string ResolveLabelAlias(Range range)
        {
            return null;
        }

        protected void FixScaleTooLarge(Range range, InlineShape shape)
        {
            for (int i = 1; i < 100; i++)
            {
                var selectionBegin = range.Duplicate;
                selectionBegin.Collapse(WdCollapseDirection.wdCollapseStart);
                int startPage = (int)selectionBegin.Information[WdInformation.wdActiveEndAdjustedPageNumber];
                int endPage = (int)range.Information[WdInformation.wdActiveEndAdjustedPageNumber];

                // NOTE: The horizontal position is a boxed float value which needs to be unboxed to
                // float before being cast to double, hence the double cast. Another option would be Convert.ToDouble
                var leftPosition = (double)(float)range.Information[WdInformation.wdHorizontalPositionRelativeToPage];
                var rightPosition = leftPosition + shape.Width;

                var rightMarginPosition = range.PageSetup.PageWidth - range.PageSetup.RightMargin;

                // When comparing the right margin we need to consider rounding issues,
                // a difference less than a tenth of a point is considered as equal.
                if ((startPage == endPage && (rightPosition - rightMarginPosition < 0.1))
                    || shape.ScaleWidth < 2 || shape.ScaleHeight < 2)
                {
                    break;
                }

                // Rescale by -1% of original image dimension
                shape.ScaleHeight = shape.ScaleHeight - 1f;
                shape.ScaleWidth = shape.ScaleWidth - 1f;
            }
        }

        protected bool HasChapterNumber(Range range)
        {
            try
            {
                // ECADOCS-315: we decide based on presence of the style, not based on
                // instance of the style iterating the paragraphs would be too costly
#pragma warning disable S1481 // Unused local variables should be removed
                // ReSharper disable once UnusedVariable
                var style = range.Document.Styles["Chapter Number"];
#pragma warning restore S1481 // Unused local variables should be removed
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected bool HasAgencyChapterNumber(Range range)
        {
            try
            {
                // ECADOCS-315: we decide based on presence of the style, not based on
                // instance of the style iterating the paragraphs would be too costly
#pragma warning disable S1481 // Unused local variables should be removed
                // ReSharper disable once UnusedVariable
                var style = range.Document.Styles["Agency Chapter Number"];
#pragma warning restore S1481 // Unused local variables should be removed
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected bool IsInAgencyChapter(Range range)
        {
            if (range == null)
            {
                return false;
            }

            if (!HasAgencyChapterNumber(range))
            {
                return false;
            }

            var agencyChapterStyles = new List<string>
            {
                "^Agency Heading .*",
                "^Agency Chapter Number",
                "^Agency Numbered Paragraph",
                "^Agency Division Title",
            };

            var otherChapterStyles = new List<string>
            {
                "^Heading .*",
                "Chapter Number",
                "Numbered Paragraph",
            };

            var paragraph = range.Paragraphs.First;
            while (paragraph != null)
            {
                if (paragraph.Range.HasTables())
                {
                    paragraph = paragraph.Range.Tables[1].Range.Paragraphs.First;
                }

                // check if the current paragraph has a text level
                string styleNameNeutral = LanguageIndependentStyle.FromParagraph(paragraph)?.NameNeutral;

                if ((styleNameNeutral != null) && agencyChapterStyles.Any(s => Regex.IsMatch(styleNameNeutral, s)))
                {
                    return true;
                }

                if ((styleNameNeutral != null) && otherChapterStyles.Any(s => Regex.IsMatch(styleNameNeutral, s)))
                {
                    return false;
                }

                paragraph = paragraph.Previous();
            }

            return false;
        }
    }
}
