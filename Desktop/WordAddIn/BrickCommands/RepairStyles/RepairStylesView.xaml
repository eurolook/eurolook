<Window x:Class="Eurolook.WordAddIn.BrickCommands.RepairStyles.RepairStylesView"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:local="clr-namespace:Eurolook.WordAddIn.BrickCommands.RepairStyles"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:wpf="clr-namespace:Eurolook.AddIn.Common.Wpf;assembly=Eurolook.AddIn.Common"
        Title="{Binding Title}"
        Width="500"
        MinHeight="150"
        d:DataContext="{d:DesignInstance d:Type=local:RepairStyleViewModel}"
        wpf:DialogResultExtension.DialogResult="{Binding DialogResult}"
        ResizeMode="NoResize"
        SizeToContent="Height"
        mc:Ignorable="d">

    <Grid>
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto" />
            <RowDefinition Height="*" />
            <RowDefinition Height="Auto" />
        </Grid.RowDefinitions>

        <Border Height="60"
                Padding="20 0"
                Background="{DynamicResource PrimaryColorBrush}">
            <Grid Name="DialogTitleBar">
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="*"></ColumnDefinition>
                    <ColumnDefinition Width="auto"></ColumnDefinition>
                </Grid.ColumnDefinitions>
                <TextBlock Name="TitleText"
                           VerticalAlignment="Center"
                           Foreground="#fff"
                           Style="{StaticResource PureContentHeader}"
                           Text="{Binding DisplayName}" />
                <Button Grid.Column="1" Style="{StaticResource RoundButton}"
                        Margin="0 0 0 0"
                        ToolTip="Open the Online Help to learn more about the Repair Styles command."
                        Command="{Binding GetHelpCommand}">?</Button>
            </Grid>
        </Border>

        <Grid Grid.Row="1" x:Name="LayoutRoot" Margin="20">
            <Grid.RowDefinitions>
                <RowDefinition Height="Auto" />
                <RowDefinition Height="Auto" />
                <RowDefinition Height="Auto" />
                <RowDefinition Height="*" />
                <RowDefinition Height="Auto" />
            </Grid.RowDefinitions>

            <StackPanel
                Grid.Row="0"
                        Margin="0 0 0 8"
                        Visibility="{Binding HasSaveOption, Converter={StaticResource BooleanToVisibilityConverter}}">
                <Grid>
                    <Grid.RowDefinitions>
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="Auto" />
                    </Grid.RowDefinitions>
                    
                    <RadioButton Margin="0 0 0 4"
                                GroupName="Save"
                                IsChecked="{Binding Path=SaveOption,
                                                    Mode=TwoWay,
                                                    Converter={StaticResource EnumToBoolean},
                                                    ConverterParameter={x:Static local:SaveOption.NewDocument}}"
                                Style="{StaticResource SimpleRadioButtonStyle}">
                        <RadioButton.Content>
                            <TextBlock>
                                    Create a new and repaired document
                            </TextBlock>
                        </RadioButton.Content>
                    </RadioButton>
                    <TextBlock Grid.Row="1"
                               Margin="26 0 0 8"
                               Foreground="#666666"
                               Text="{Binding CreateNewDocumentText, Mode=OneWay}">
                    </TextBlock>
                </Grid>

                <Grid>
                    <Grid.RowDefinitions>
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="Auto" />
                    </Grid.RowDefinitions>
                    <RadioButton Margin="0 0 0 4"
                                GroupName="Save"
                                IsChecked="{Binding Path=SaveOption,
                                                    Mode=TwoWay,
                                                    Converter={StaticResource EnumToBoolean},
                                                    ConverterParameter={x:Static local:SaveOption.CurrentDocument}}"
                                Style="{StaticResource SimpleRadioButtonStyle}">
                        <RadioButton.Content>
                            <TextBlock Grid.Row="0">
                                    <Run> Repair your current document (recommended for working in</Run>
                                    <Run FontWeight="Bold">SharePoint</Run><Run>)</Run>
                            </TextBlock>
                        </RadioButton.Content>
                    </RadioButton>
                    <TextBlock Grid.Row="1"
                               Margin="26 0 0 8"
                               Text="{Binding OverwriteDescriptionText, Mode=OneWay}">
                        <TextBlock.Style>
                            <Style BasedOn="{StaticResource PureContentText}" TargetType="{x:Type TextBlock}">
                                <Style.Triggers>
                                    <DataTrigger Binding="{Binding Path=SaveOption, Mode=OneWay, Converter={StaticResource EnumToBoolean}, ConverterParameter={x:Static local:SaveOption.CurrentDocument}}" Value="True">
                                        <Setter Property="Foreground" Value="{DynamicResource RedColor}" />
                                    </DataTrigger>
                                    <DataTrigger Binding="{Binding Path=SaveOption, Mode=OneWay, Converter={StaticResource EnumToBoolean}, ConverterParameter={x:Static local:SaveOption.CurrentDocument}}" Value="False">
                                        <Setter Property="Foreground" Value="#666666" />
                                    </DataTrigger>
                                </Style.Triggers>
                            </Style>
                        </TextBlock.Style>
                    </TextBlock>
                </Grid>
            </StackPanel>

            <StackPanel Grid.Row="1"
                        Margin="0"
                        Visibility="{Binding HasSaveOption, Converter={StaticResource BooleanToInvisibilityConverter}}">
                <TextBlock TextWrapping="Wrap">
                    <Run>If you continue, a new and cleaned version of your document will be created.</Run>
                    <LineBreak/>
                    <Run>Your original document will not be modified.</Run>
                </TextBlock>
            </StackPanel>

            <StackPanel Margin="0 10 0 0" Grid.Row="1" Visibility="{Binding IsBasedOnCustomTemplate, Converter={StaticResource BooleanToVisibilityConverter}}">
                <TextBlock TextWrapping="Wrap">
                    Your document uses a custom template. Select if you want to use the custom template or the underlying Eurolook template for repairing your document.
                </TextBlock>
                <ComboBox Margin="0 8 0 0"
                          Focusable="True" Style="{StaticResource SimpleComboBox}"
                          ItemsSource="{Binding AvailableTemplates}"
                          SelectedItem="{Binding SelectedTemplate}">
                </ComboBox>
            </StackPanel>

            <ToggleButton Grid.Row="2"
                      Margin="0 10 0 0"
                      HorizontalAlignment="Left"
                      Foreground="{DynamicResource PrimaryColorBrush}"
                      IsChecked="{Binding IsShowingAdvancedOptions}"
                      Style="{StaticResource LinkButtonUnderlined}"
                      Visibility="{Binding HasAdvancedOptions, Converter={StaticResource BooleanToVisibilityConverter}}">
                <TextBlock Text="{Binding ShowAdvancedOptionsText}" />
            </ToggleButton>

            <Grid Grid.Row="3" Visibility="{Binding IsShowingAdvancedOptions, Converter={StaticResource BooleanToVisibilityConverter}}">
                <Grid.RowDefinitions>
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="*" />
                </Grid.RowDefinitions>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="*" />
                    <ColumnDefinition Width="Auto" />
                    <ColumnDefinition Width="Auto" />
                </Grid.ColumnDefinitions>

                <TextBlock Style="{StaticResource ContentHeader}" Text="Advanced Options:" />

                <Button Grid.Row="0"
                    Grid.Column="1"
                    Width="80"
                    Height="25"
                    Margin="8 0 12 8"
                    VerticalAlignment="Bottom"
                    Command="{Binding ActivateAllTasks}"
                    IsEnabled="{Binding IsBusy, Converter={StaticResource BooleanInverseConverter}}"
                    Style="{StaticResource SimpleButton}">
                    <TextBlock Style="{StaticResource ButtonText}" Text="Select All" />
                </Button>
                <Button Grid.Row="0"
                    Grid.Column="2"
                    Width="80"
                    Height="25"
                    Margin="0 0 0 8"
                    VerticalAlignment="Bottom"
                    Command="{Binding DeactivateAllTasks}"
                    IsEnabled="{Binding IsBusy, Converter={StaticResource BooleanInverseConverter}}"
                    Style="{StaticResource SimpleButton}">
                    <TextBlock Style="{StaticResource ButtonText}" Text="Select None" />
                </Button>
                <Border Grid.Row="1"
                    Grid.Column="0"
                    Grid.ColumnSpan="3"
                    Margin="0 0 0 0"
                    BorderBrush="{StaticResource ControlBorder}"
                    BorderThickness="1">
                    <ScrollViewer HorizontalScrollBarVisibility="Disabled" VerticalScrollBarVisibility="Auto">
                        <ItemsControl x:Name="TaskListBox"
                                  IsEnabled="{Binding IsBusy, Converter={StaticResource BooleanInverseConverter}}"
                                  ItemsSource="{Binding StyleRepairTasks}"
                                  ScrollViewer.HorizontalScrollBarVisibility="Hidden">
                            <ItemsControl.ItemTemplate>
                                <DataTemplate>
                                    <Grid Margin="4">
                                        <Grid.RowDefinitions>
                                            <RowDefinition />
                                            <RowDefinition />
                                        </Grid.RowDefinitions>
                                        <CheckBox Grid.Column="0"
                                              Margin="0"
                                              IsChecked="{Binding IsActive}">
                                            <TextBlock Margin="0 0 4 0" Text="{Binding DisplayName, Mode=OneWay}">
                                                <TextBlock.Style>
                                                    <Style BasedOn="{StaticResource PureContentText}" TargetType="{x:Type TextBlock}">
                                                        <Style.Triggers>
                                                            <Trigger Property="IsMouseOver" Value="True">
                                                                <Setter Property="Foreground" Value="{DynamicResource PrimaryColorBrush}" />
                                                            </Trigger>
                                                        </Style.Triggers>
                                                    </Style>
                                                </TextBlock.Style>
                                            </TextBlock>
                                        </CheckBox>

                                        <TextBlock Grid.Row="1"
                                               Grid.Column="0"
                                               Margin="20 5"
                                               Foreground="#666666"
                                               Text="{Binding Description}"
                                               TextWrapping="Wrap"
                                               Visibility="{Binding Description, Converter={StaticResource EmptyStringToVisibilityConverter}}" />
                                    </Grid>
                                </DataTemplate>
                            </ItemsControl.ItemTemplate>
                        </ItemsControl>
                    </ScrollViewer>
                </Border>
            </Grid>

            <StackPanel Grid.Row="4"
                    Grid.Column="0"
                    MinHeight="30"
                    Margin="0 20 0 0">
                <TextBlock Margin="0"
                       Text="{Binding BusyMessage, FallbackValue=Ready.}"
                       TextWrapping="Wrap" />

                <ProgressBar Height="10"
                         Margin="0 2 0 0"
                         VerticalAlignment="Bottom"
                         Background="{DynamicResource ControlBackground}"
                         BorderThickness="0"
                         Foreground="{DynamicResource GreenColor}"
                         IsEnabled="{Binding IsBusy}"
                         IsIndeterminate="False"
                         Value="{Binding Progress}" />
            </StackPanel>

        </Grid>

        <Border Grid.Row="2" Background="#f0f0f0">
            <StackPanel HorizontalAlignment="right" Orientation="Horizontal">
                <Button Width="80"
                        Margin="12"
                        Command="{Binding CommitCommand}"
                        IsDefault="True"
                        Style="{StaticResource SimpleButton}">
                    <TextBlock Style="{StaticResource ButtonText}" Text="OK" />
                </Button>
                <Button Width="80"
                        Margin="0 12 20 12"
                        Command="{Binding CancelCommand}"
                        IsCancel="True"
                        Style="{StaticResource SimpleButton}">
                    <TextBlock Style="{StaticResource ButtonText}" Text="Cancel" />
                </Button>
            </StackPanel>
        </Border>
    </Grid>    

</Window>