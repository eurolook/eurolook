using Eurolook.WordAddIn.BrickCommands.RepairStyles.StyleRepairTasks;
using GalaSoft.MvvmLight;

namespace Eurolook.WordAddIn.BrickCommands.RepairStyles
{
    public class StyleRepairTaskViewModel : ViewModelBase
    {
        private bool _isActive;

        public StyleRepairTaskViewModel(IStyleRepairTask styleRepairTask)
        {
            StyleRepairTask = styleRepairTask;
            _isActive = styleRepairTask.IsActive;
        }

        public IStyleRepairTask StyleRepairTask { get; }

        public string DisplayName => StyleRepairTask.DisplayName;

        public string Description => StyleRepairTask.Description;

        public bool IsActive
        {
            get => _isActive;
            set { Set(() => IsActive, ref _isActive, value); }
        }
    }
}
