namespace Eurolook.WordAddIn.BrickCommands.RepairStyles
{
    public partial class RepairStylesView
    {
        public RepairStylesView(RepairStyleViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }
    }
}
