﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.Common;
using Eurolook.Common.Log;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.StyleCleanUpShell;
using Eurolook.WordAddIn.BrickCommands.RepairStyles.StyleRepairTasks;
using Microsoft.Office.Interop.Word;
using Language = Eurolook.Data.Models.Language;

namespace Eurolook.WordAddIn.BrickCommands.RepairStyles
{
    public class StyleRepairer : IStyleRepairer, ICanLog
    {
        private readonly ILanguageRepository _languageRepository;
        private readonly IDocumentManager _documentManager;
        private readonly ITemplateRepository _templateRepository;

        public StyleRepairer(
            ILanguageRepository languageRepository,
            IDocumentManager documentManager,
            ITemplateRepository templateRepository)
        {
            _languageRepository = languageRepository;
            _documentManager = documentManager;
            _templateRepository = templateRepository;
        }

        public async Task<Document> RepairStylesAsync(IEurolookWordDocument eurolookDocument)
        {
            return await RepairStylesAsync(eurolookDocument, Enumerable.Empty<IStyleRepairTask>());
        }

        public async Task<Document> RepairStylesAsync(
            IEurolookWordDocument eurolookDocument,
            IEnumerable<IStyleRepairTask> styleRepairTasks)
        {
            return await RepairStylesAsync(eurolookDocument, styleRepairTasks, ProgressReporter<int>.Empty);
        }

        public async Task<Document> RepairStylesAsync(
            IEurolookWordDocument eurolookDocument,
            IEnumerable<IStyleRepairTask> styleRepairTasks,
            IProgressReporter<int> progressReporter)
        {
            return await RepairStylesAsync(eurolookDocument, styleRepairTasks, progressReporter, null);
        }

        public async Task<Document> RepairStylesAsync(
            IEurolookWordDocument eurolookDocument,
            IEnumerable<IStyleRepairTask> styleRepairTasks,
            IProgressReporter<int> progressReporter,
            Guid? customTemplateId)
        {
            await progressReporter.ReportProgressAsync(0, "Starting style cleanup...");

            var styleRepairTaskList = styleRepairTasks.OrderBy(t => t.ExecutionOrder).ToList();

            int progress = 0;
            int progressSteps = styleRepairTaskList.Count + 1;
            int progressIncrement = 100 / progressSteps;

            this.LogDebug("Starting style cleanup...");

            // Create temporary file for template reference
            using (var temporaryTemplateFile = new TemporaryFile(".dotx"))
            {
                bool isSaved = eurolookDocument.Document.Saved;
                try
                {
                    if (customTemplateId.HasValue)
                    {
                        var properties = eurolookDocument.GetEurolookProperties();
                        if (properties.CustomTemplateId.HasValue)
                        {
                            SaveCustomTemplate(
                                customTemplateId.Value,
                                eurolookDocument.Language,
                                temporaryTemplateFile);
                        }
                        else
                        {
                            throw new Exception("Document is not based on a custom template");
                        }
                    }
                    else
                    {
                        SaveEurolookTemplate(eurolookDocument, temporaryTemplateFile);
                    }

                    using (var temporaryDocumentToCleanFile = new TemporaryFile(".docx"))
                    {
                        SaveDocument(eurolookDocument, temporaryDocumentToCleanFile);

                        using (var outputFile = new TemporaryFile(".repaired.docx"))
                        {
                            int exitCode = RunCleanUpLauncher(
                                temporaryTemplateFile,
                                temporaryDocumentToCleanFile,
                                outputFile);
                            if (exitCode != 0)
                            {
                                return null;
                            }

                            // NOTE: The template used as the basis of the cleanup does not have a language
                            //    set in the document defaults. If we do not set the language now,
                            //    Word will try to detect the language which we want to avoid.
                            SetLanguage(outputFile, eurolookDocument.Language);

                            var cleanedDocument =
                                eurolookDocument.Document.Application.Documents.Add(outputFile.FullName, false, 0);
                            var cleanedEurolookDocument =
                                _documentManager.GetOrCreateDocumentViewModel(cleanedDocument);

                            foreach (var styleRepairTask in styleRepairTaskList)
                            {
                                progress += progressIncrement;
                                await progressReporter.ReportProgressAsync(
                                    progress,
                                    $"{styleRepairTask.DisplayName}...");
                                try
                                {
                                    await styleRepairTask.HandleAsync(eurolookDocument, cleanedEurolookDocument);
                                }
                                catch (Exception ex)
                                {
                                    this.LogError(ex);
                                }
                            }

                            cleanedDocument.UndoClear();
                            cleanedDocument.Saved = false;
                            cleanedDocument.Activate();

                            this.LogDebug("Finished style cleanup of the document.");
                            return cleanedDocument;
                        }
                    }
                }
                finally
                {
                    await progressReporter.ReportProgressAsync(100, "Done.");
                    eurolookDocument.Document.Saved = isSaved;
                }
            }
        }

        private static void SaveDocument(
            IEurolookWordDocument eurolookDocument,
            TemporaryFile temporaryDocumentToCleanFile)
        {
            eurolookDocument.Document.SaveCopyAs(temporaryDocumentToCleanFile.FullName);
        }

        private static string GetLauncherExe()
        {
            string assemblyLocation = Path.GetDirectoryName(typeof(Program).Assembly.Location) ?? "";
            string cleanUpLauncher = Path.Combine(assemblyLocation, "StyleCleanUpShell.exe");
            if (!File.Exists(cleanUpLauncher))
            {
                throw new FileNotFoundException("StyleCleanUpShell.exe could not be found.");
            }

            return cleanUpLauncher;
        }

        private byte[] GetDocumentTemplate(DocumentModel documentModel, Language targetLanguage)
        {
            var documentModelLanguage =
                _languageRepository.GetDocumentModelLanguage(documentModel.Id, targetLanguage.Id);
            return documentModelLanguage?.Template ?? documentModel.Template;
        }

        private byte[] GetCustomTemplate(Guid templateId, Language targetLanguage)
        {
            var templateFile = _templateRepository.GetTemplateFile(templateId, targetLanguage);
            return templateFile?.Bytes;
        }

        private void SetLanguage(TemporaryFile outputFile, Language language)
        {
            using (var dotx = WordprocessingDocument.Open(outputFile.FullName, true))
            {
                dotx.SetLanguage(language);
            }
        }

        private void SaveEurolookTemplate(IEurolookDocument eurolookDocument, TemporaryFile templateFile)
        {
            try
            {
                this.LogDebug("Saving template of the current language.");
                var templateData = GetDocumentTemplate(eurolookDocument.DocumentModel, eurolookDocument.Language);
                File.WriteAllBytes(templateFile.FullName, templateData);
            }
            catch (Exception ex)
            {
                throw new FileNotFoundException("No matching template could be found.", ex);
            }
        }

        private void SaveCustomTemplate(Guid templateId, Language targetLanguage, TemporaryFile templateFile)
        {
            try
            {
                this.LogDebug("Saving custom template of the current language.");
                var templateData = GetCustomTemplate(templateId, targetLanguage);
                File.WriteAllBytes(templateFile.FullName, templateData);
            }
            catch (Exception ex)
            {
                throw new FileNotFoundException("No matching template could be found.", ex);
            }
        }

        private int RunCleanUpLauncher(
            TemporaryFile templateFile,
            TemporaryFile documentToRepair,
            TemporaryFile outputRepaired)
        {
            var startInfo = new ProcessStartInfo
            {
                FileName = GetLauncherExe(),
                Arguments =
                    $"-t \"{templateFile.FullName}\" -i \"{documentToRepair.FullName}\" -o \"{outputRepaired.FullName}\"",
                UseShellExecute = false,
                RedirectStandardOutput = true,
                CreateNoWindow = true,
            };

            string repairStylesLog = Path.Combine(Path.GetTempPath(), "LegisWriteLoader.log");
            DeletedExternalLog(repairStylesLog);

            this.LogDebug($"Launching style cleanup {startInfo.FileName} {startInfo.Arguments}");

            var process = Process.Start(startInfo);
            if (process == null)
            {
                throw new InvalidOperationException("External process could not be started.");
            }

            string cleanupLog = process.StandardOutput.ReadToEnd();
            process.WaitForExit();

            this.LogDebug($"StyleCleanup exited with code {process.ExitCode}.");
            this.LogDebug($"Repaired styles: {cleanupLog}");

            CaptureExternalLog(repairStylesLog);

            return process.ExitCode;
        }

        private void DeletedExternalLog(string repairStylesLog)
        {
            try
            {
                if (!File.Exists(repairStylesLog))
                {
                    return;
                }

                File.Delete(repairStylesLog);
            }
            catch (Exception ex)
            {
                // not critical if deleting previous log files failed
                this.LogWarn(ex);
            }
        }

        private void CaptureExternalLog(string repairStylesLog)
        {
            try
            {
                if (!File.Exists(repairStylesLog))
                {
                    return;
                }

                var lines = File.ReadAllLines(repairStylesLog);
                var regex = new Regex(@"^(?<prefix>.*)(?<level>Debug|Info|Warning|Error)::(?<message>.*)$");
                foreach (string line in lines)
                {
                    var match = regex.Match(line);
                    if (match.Success)
                    {
                        string level = match.Groups["level"].Value;
                        string message = match.Groups["message"].Value;
                        switch (level)
                        {
                            case "Error":
                                this.LogError(message);
                                break;

                            default:
                                this.LogDebug(message);
                                break;
                        }
                    }
                }

                DeletedExternalLog(repairStylesLog);
            }
            catch (Exception ex)
            {
                // not critical if adding log fails
                this.LogWarn(ex);
            }
        }
    }
}
