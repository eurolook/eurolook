using System;
using System.Linq;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands.RepairStyles.StyleRepairTasks
{
    [UsedImplicitly]
    public class ClearRestartFromContinuouslyNumberedList : IStyleRepairTask
    {
        public string DisplayName => "Make main text continuously numbered";

        public string Description =>
            "Set the numbering of all paragraphs in the main part of the document to be continuous throughout the document.";

        public bool IsActive => true;

        public float ExecutionOrder => 30;

        public Task HandleAsync(IEurolookWordDocument sourceDocument, IEurolookWordDocument repairedDocument)
        {
            var numberedParagraphStyle = LanguageIndependentStyle.FromNeutralStyleName(
                "Numbered Paragraph",
                repairedDocument.Document);

            if (numberedParagraphStyle == null)
            {
                return Task.CompletedTask;
            }

            var listParagraphs = repairedDocument.Document.ListParagraphs.OfType<Paragraph>()
                                                 .Where(
                                                     p =>
                                                         string.Equals(
                                                             LanguageIndependentStyle.FromParagraph(p)?.NameNeutral,
                                                             numberedParagraphStyle.NameNeutral,
                                                             StringComparison.InvariantCultureIgnoreCase)).ToList();
            foreach (var paragraph in listParagraphs)
            {
                paragraph.ContinueListNumbering();

                var range = paragraph.Range;
                range.Collapse(WdCollapseDirection.wdCollapseEnd);
                range.MoveStart(WdUnits.wdCharacter, -1);
                range.Font.Reset();
            }

            return Task.CompletedTask;
        }
    }
}
