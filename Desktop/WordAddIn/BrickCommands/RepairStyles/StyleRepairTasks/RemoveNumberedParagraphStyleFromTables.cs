using System;
using System.Linq;
using Eurolook.DocumentProcessing;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands.RepairStyles.StyleRepairTasks
{
    [UsedImplicitly]
    public class RemoveNumberedParagraphStyleFromTables : IStyleRepairTask
    {
        public string DisplayName => "Don’t use 'Numbered Paragraph' formatting in tables";

        public string Description => "Replace the style 'Numbered Paragraph' in tables with the 'Normal' style.";

        public bool IsActive => true;

        public float ExecutionOrder => 10;

        public Task HandleAsync(IEurolookWordDocument sourceDocument, IEurolookWordDocument repairedDocument)
        {
            var numberedParagraphStyle = LanguageIndependentStyle.FromNeutralStyleName(
                "Numbered Paragraph",
                repairedDocument.Document);

            if (numberedParagraphStyle == null)
            {
                return Task.CompletedTask;
            }

            var tables = repairedDocument.Document.Tables.OfType<Table>().ToList();
            var sourceTables = sourceDocument.Document.Tables.OfType<Table>().ToList();

            for (int tableIndex = 0; tableIndex < tables.Count && tables.Count == sourceTables.Count; tableIndex++)
            {
                var table = tables[tableIndex];
                var paragraphs = table.Range.Paragraphs.OfType<Paragraph>().ToList();

                var sourceTable = sourceTables[tableIndex];
                var sourceParagraphs = sourceTable.Range.Paragraphs.OfType<Paragraph>().ToList();

                for (int paragraphIndex = 0; paragraphIndex < paragraphs.Count && paragraphs.Count == sourceParagraphs.Count; paragraphIndex++)
                {
                    var numberedParagraph = paragraphs[paragraphIndex];
                    if (!string.Equals(
                        LanguageIndependentStyle.FromParagraph(numberedParagraph)?.NameNeutral,
                        numberedParagraphStyle.NameNeutral,
                        StringComparison.InvariantCultureIgnoreCase))
                    {
                        continue;
                    }

                    var sourceParagraph = sourceParagraphs[paragraphIndex];
                    if (string.IsNullOrEmpty(sourceParagraph.Range.ListFormat.ListString))
                    {
                        numberedParagraph.set_Style(WdBuiltinStyle.wdStyleNormal);
                    }
                }
            }

            return Task.CompletedTask;
        }
    }
}
