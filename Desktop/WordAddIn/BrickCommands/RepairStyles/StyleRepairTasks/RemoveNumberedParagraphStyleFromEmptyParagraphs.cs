using System;
using System.Linq;
using Eurolook.DocumentProcessing;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands.RepairStyles.StyleRepairTasks
{
    [UsedImplicitly]
    public class RemoveNumberedParagraphStyleFromEmptyParagraphs : IStyleRepairTask
    {
        public string DisplayName => "Don’t use 'Numbered Paragraph' formatting for empty paragraphs";

        public string Description =>
            "Replace the style 'Numbered Paragraph' with 'Normal' style on paragraphs that don’t contain text.";

        public bool IsActive => true;

        public float ExecutionOrder => 20;

        public Task HandleAsync(IEurolookWordDocument sourceDocument, IEurolookWordDocument repairedDocument)
        {
            var numberedParagraphStyle = LanguageIndependentStyle.FromNeutralStyleName(
                "Numbered Paragraph",
                repairedDocument.Document);

            if (numberedParagraphStyle == null)
            {
                return Task.CompletedTask;
            }

            // find paragraphs without text that have the style "Numbered Paragraph"
            var emptyNumberedParagraphs =
                repairedDocument.Document.Paragraphs.OfType<Paragraph>()
                                .Select(
                                    (p, i) => new
                                    {
                                        Index = i,
                                        Paragraph = p,
                                    })
                                .Where(
                                    p => p.Paragraph.Range.Text == "\r" && string.Equals(
                                        LanguageIndependentStyle.FromParagraph(p.Paragraph)?.NameNeutral,
                                        numberedParagraphStyle.NameNeutral,
                                        StringComparison.InvariantCultureIgnoreCase))
                                .ToList();

            var sourceParagraphs = sourceDocument.Document.Paragraphs.OfType<Paragraph>().ToList();

            foreach (var emptyNumberedParagraph in emptyNumberedParagraphs)
            {
                if (emptyNumberedParagraph.Index >= sourceParagraphs.Count)
                {
                    break;
                }

                // the Repair tool might have restored numbering so we check whether the source paragraph is numbered
                var sourceParagraph = sourceParagraphs[emptyNumberedParagraph.Index];

                if (string.IsNullOrEmpty(sourceParagraph.Range.ListFormat.ListString))
                {
                    // apply Normal style if source paragraph is not numbered
                    emptyNumberedParagraph.Paragraph.set_Style(WdBuiltinStyle.wdStyleNormal);
                }
            }

            return Task.CompletedTask;
        }
    }
}
