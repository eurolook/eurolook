using System.Threading.Tasks;

namespace Eurolook.WordAddIn.BrickCommands.RepairStyles.StyleRepairTasks
{
    public interface IStyleRepairTask
    {
        string DisplayName { get; }

        string Description { get; }

        bool IsActive { get; }

        float ExecutionOrder { get; }

        Task HandleAsync(IEurolookWordDocument sourceDocument, IEurolookWordDocument repairedDocument);
    }
}
