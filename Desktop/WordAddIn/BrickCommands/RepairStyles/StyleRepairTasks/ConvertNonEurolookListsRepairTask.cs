﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands.RepairStyles.StyleRepairTasks
{
    /// <summary>
    /// Retains lists applied to the Numbered Paragraph style using direct formatting.
    /// When doing that, the list template of the Numbered Paragraph style is no longer intact.
    /// </summary>
    [UsedImplicitly]
    public class ConvertNonEurolookListsRepairTask : IStyleRepairTask, ICanLog
    {
        public string DisplayName => "Standardise lists";

        public string Description => "Unify formatting of numbered, dashed, and bulleted lists.";

        public bool IsActive => true;

        public float ExecutionOrder => 80;

        public virtual Task HandleAsync(IEurolookWordDocument sourceDocument, IEurolookWordDocument repairedDocument)
        {
            var numberedParagraphStyle = LanguageIndependentStyle.FromNeutralStyleName(
                "Numbered Paragraph",
                repairedDocument.Document);

            if (numberedParagraphStyle == null)
            {
                return Task.CompletedTask;
            }

            var listDash = LanguageIndependentStyle.FromNeutralStyleName("List Dash", repairedDocument.Document);
            var listBullet = LanguageIndependentStyle.FromNeutralStyleName("List Bullet", repairedDocument.Document);
            var listNumber = LanguageIndependentStyle.FromNeutralStyleName("List Number", repairedDocument.Document);
            var listRoman = LanguageIndependentStyle.FromNeutralStyleName("List Roman", repairedDocument.Document);
            var listAbc = LanguageIndependentStyle.FromNeutralStyleName("List abc", repairedDocument.Document);

            var numberStyleToListStyleMapping =
                new Dictionary<WdListNumberStyle, Func<string, LanguageIndependentStyle>>
                {
                    { WdListNumberStyle.wdListNumberStyleArabic, _ => listNumber },
                    { WdListNumberStyle.wdListNumberStyleArabic1, _ => listNumber },
                    { WdListNumberStyle.wdListNumberStyleArabic2, _ => listNumber },
                    {
                        WdListNumberStyle.wdListNumberStyleBullet,

                        // NOTE: We map lists of style wdListNumberStyleBullet to either "List Dash" or to "List Bullet".
                        // If the list uses a dash, we apply "List Dash" and otherwise - no matter what symbol is used -
                        // we apply "List Bullet".
                        listString => listDash != null && "--–—−".Contains(listString) ? listDash : listBullet
                    },
                    { WdListNumberStyle.wdListNumberStylePictureBullet, _ => listBullet },
                    { WdListNumberStyle.wdListNumberStyleLowercaseRoman, _ => listRoman },
                    { WdListNumberStyle.wdListNumberStyleUppercaseRoman, _ => listRoman },
                    { WdListNumberStyle.wdListNumberStyleLowercaseLetter, _ => listAbc },
                    { WdListNumberStyle.wdListNumberStyleUppercaseLetter, _ => listAbc },
                    { WdListNumberStyle.wdListNumberStyleLowercaseGreek, _ => listAbc },
                    { WdListNumberStyle.wdListNumberStyleUppercaseGreek, _ => listAbc },
                    { WdListNumberStyle.wdListNumberStyleLowercaseBulgarian, _ => listAbc },
                    { WdListNumberStyle.wdListNumberStyleUppercaseBulgarian, _ => listAbc },
                };

            // get all paragraphs where the list is applied as direct formatting and the paragraph is not a heading
            var sourceParagraphs = GetAllNonHeadingParagraphsWithDirectListFormatting(sourceDocument);

            var paragraphsToBeRestarted = new List<Paragraph>();
            foreach (var sourceParagraph in sourceParagraphs)
            {
                if (sourceParagraph.NumberStyle == null)
                {
                    continue;
                }

                var listStyle = numberStyleToListStyleMapping.ContainsKey(sourceParagraph.NumberStyle.Value)
                    ? numberStyleToListStyleMapping[(WdListNumberStyle)sourceParagraph.NumberStyle](
                        sourceParagraph.ListString)
                    : null;

                if (listStyle == null)
                {
                    continue;
                }

                var paragraph = repairedDocument.Document.Paragraphs[sourceParagraph.Index];
                paragraph.set_Style(listStyle.Style);

                if (sourceParagraph.ListLevel.HasValue)
                {
                    SetListLevelNumberSafe(paragraph, sourceParagraph.ListLevel.Value);
                }

                if (sourceParagraph.IsRestartingListNumbering)
                {
                    // collect all paragraphs that start a new list
                    paragraphsToBeRestarted.Add(paragraph);
                }
            }

            // fix restart
            foreach (var paragraph in paragraphsToBeRestarted)
            {
                var listFormat = paragraph.Range.ListFormat;
                listFormat.ApplyListTemplateWithLevel(
                    listFormat.ListTemplate,
                    false,
                    WdListApplyTo.wdListApplyToWholeList,
                    WdDefaultListBehavior.wdWord10ListBehavior);
            }

            return Task.CompletedTask;
        }

        protected static List<ParagraphWrapper> GetAllNonHeadingParagraphsWithDirectListFormatting(
            IEurolookWordDocument sourceDocument)
        {
            return sourceDocument.Document.Paragraphs
                                 .OfType<Paragraph>()
                                 .Select(
                                     (p, i) => new ParagraphWrapper(p, i + 1))
                                 .Where(
                                     pw => !string.IsNullOrEmpty(pw.ListString)
                                           && ((Style)pw.Paragraph.get_Style()).ListTemplate == null
                                           && pw.Paragraph.OutlineLevel == WdOutlineLevel.wdOutlineLevelBodyText
                                           && !ResetHeadingsRepairTask.IsHeadingStyle(
                                               pw.LanguageIndependentStyle)).ToList();
        }

        protected void SetListLevelNumberSafe(Paragraph paragraph, int listLevelNumber)
        {
            try
            {
                paragraph.Range.ListFormat.ListLevelNumber = listLevelNumber;
            }

            // Value out of range COM exception was observed in the error logs,
            // possibly this is caused if the target list is no longer a multi-level list after the repair.
            catch (COMException e) when ((uint)e.HResult == 0x80020009)
            {
                this.LogWarn(e);
            }
        }

        protected class ParagraphWrapper
        {
            private string _styleNameNeutral;

            private string _listString;

            private int? _listLevel;

            private WdListNumberStyle? _numberStyle;

            private bool? _isRestartingListNumbering;

            public ParagraphWrapper(Paragraph paragraph, int index)
            {
                Paragraph = paragraph;
                Index = index;
                LanguageIndependentStyle = LanguageIndependentStyle.FromParagraph(Paragraph);
            }

            public LanguageIndependentStyle LanguageIndependentStyle { get; }

            public Paragraph Paragraph { get; }

            public int Index { get; }

            public string StyleNameNeutral => _styleNameNeutral ??= LanguageIndependentStyle?.NameNeutral;

            public string ListString => _listString ??= Paragraph.Range.ListFormat.ListString;

            public int? ListLevel => _listLevel ??= Paragraph.Range.ListFormat.ListLevelNumber;

            public WdListNumberStyle? NumberStyle =>
                _numberStyle ??= Paragraph.Range.ListFormat.ListTemplate?.ListLevels[1].NumberStyle;

            public bool IsRestartingListNumbering =>
                _isRestartingListNumbering ??= Paragraph.Range.ListFormat.ListValue == 1;
        }
    }
}
