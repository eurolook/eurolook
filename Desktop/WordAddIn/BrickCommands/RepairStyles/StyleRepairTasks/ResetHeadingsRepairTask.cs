using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Eurolook.DocumentProcessing;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands.RepairStyles.StyleRepairTasks
{
    [UsedImplicitly]
    public class ResetHeadingsRepairTask : IStyleRepairTask
    {
        private static readonly List<Regex> HeadingStylePatterns = new List<Regex>
        {
            new Regex(@"Heading \d", RegexOptions.IgnoreCase),
            new Regex(@"TOC Heading", RegexOptions.IgnoreCase),
            new Regex(@"annex heading \d", RegexOptions.IgnoreCase),
            new Regex(@"AnnexTitle", RegexOptions.IgnoreCase),
        };

        public string DisplayName => "Reset headings";

        public string Description => "Remove any direct formatting from headings.";

        public bool IsActive => true;

        public float ExecutionOrder => 70;

        public Task HandleAsync(IEurolookWordDocument sourceDocument, IEurolookWordDocument repairedDocument)
        {
            var paragraphs = repairedDocument.Document.Paragraphs.OfType<Paragraph>()
                                             .Where(
                                                 p =>
                                                 {
                                                     var style = LanguageIndependentStyle.FromParagraph(p);
                                                     return IsHeadingStyle(style);
                                                 })
                                             .ToList();

            foreach (var paragraph in paragraphs)
            {
                paragraph.Range.Font.Reset();
                paragraph.Reset();
            }

            return Task.CompletedTask;
        }

        public static bool IsHeadingStyle(LanguageIndependentStyle languageIndependentStyle)
        {
            int loopSafeGuard = 0;
            while (loopSafeGuard < 10)
            {
                if (languageIndependentStyle == null)
                {
                    return false;
                }

                if (string.IsNullOrEmpty(languageIndependentStyle.NameNeutral))
                {
                    return false;
                }

                foreach (string styleName in languageIndependentStyle.NameNeutralAliases)
                {
                    if (HeadingStylePatterns.Any(r => r.IsMatch(styleName)))
                    {
                        return true;
                    }
                }

                languageIndependentStyle = languageIndependentStyle.GetBaseStyle();
                loopSafeGuard++;
            }

            return false;
        }
    }
}
