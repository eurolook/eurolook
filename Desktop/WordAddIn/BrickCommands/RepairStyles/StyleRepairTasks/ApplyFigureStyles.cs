using System;
using System.Linq;
using System.Text.RegularExpressions;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.DocumentProcessing.OpenXml;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Language = Eurolook.Data.Models.Language;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands.RepairStyles.StyleRepairTasks
{
    [UsedImplicitly]
    public class ApplyFigureStyles : IStyleRepairTask
    {
        private readonly Func<Language, ILocalisedResourceResolver> _localisedResourceResolverCreateFunc;

        public ApplyFigureStyles(Func<Language, ILocalisedResourceResolver> localisedResourceResolverCreateFunc)
        {
            _localisedResourceResolverCreateFunc = localisedResourceResolverCreateFunc;
        }

        public string DisplayName => "Repair figure formatting";

        public string Description =>
            "Format figures, including their title and source information, correctly by applying the styles 'Figure Title', 'Figure Body', and 'Figure Source'.";

        public bool IsActive => true;

        public float ExecutionOrder => 50;

        public Task HandleAsync(IEurolookWordDocument sourceDocument, IEurolookWordDocument repairedDocument)
        {
            var document = repairedDocument.Document;

            var figureTitleStyle = LanguageIndependentStyle.FromNeutralStyleName("Figure Title", document);
            var figureBodyStyle = LanguageIndependentStyle.FromNeutralStyleName("Figure Body", document);
            var figureSourceStyle = LanguageIndependentStyle.FromNeutralStyleName("Figure Source", document);

            if (figureTitleStyle == null || figureBodyStyle == null || figureSourceStyle == null)
            {
                return Task.CompletedTask;
            }

            var localisedResourceResolver = _localisedResourceResolverCreateFunc(repairedDocument.Language);
            string sourcePrefix = localisedResourceResolver.ResolveTranslation("LabelSource")?.Value;
            if (string.IsNullOrEmpty(sourcePrefix))
            {
                return Task.CompletedTask;
            }

            var regexWordField = new Regex(@"\{.*?\}");
            var regexNonWordChars = new Regex(@"\W");

            string titlePrefixFigure = localisedResourceResolver.ResolveTranslation("LabelFigureSeq")?.Value ?? "";
            titlePrefixFigure = regexWordField.Replace(titlePrefixFigure, "");
            titlePrefixFigure = regexNonWordChars.Replace(titlePrefixFigure, "");

            string titlePrefixPicture = localisedResourceResolver.ResolveTranslation("LabelPictureSeq")?.Value ?? "";
            titlePrefixPicture = regexWordField.Replace(titlePrefixPicture, "");
            titlePrefixPicture = regexNonWordChars.Replace(titlePrefixPicture, "");

            foreach (var inlineShape in document.InlineShapes.OfType<InlineShape>())
            {
                var paragraphFigureBody = inlineShape.Range.Paragraphs.First;
                var paragraphFigureTitle = paragraphFigureBody.Previous();
                var paragraphFigureSource = paragraphFigureBody.Next();

                if (paragraphFigureTitle == null
                    || paragraphFigureSource == null
                    || !IsParagraphFigureSource(paragraphFigureSource, sourcePrefix)
                    || !IsParagraphFigureTitle(paragraphFigureTitle, titlePrefixFigure, titlePrefixPicture))
                {
                    continue;
                }

                paragraphFigureTitle.set_Style(figureTitleStyle.Style);
                paragraphFigureBody.set_Style(figureBodyStyle.Style);
                paragraphFigureSource.set_Style(figureSourceStyle.Style);

                // format 'Source:' prefix italic
                var rangeSource = paragraphFigureSource.Range;
                rangeSource.Collapse(WdCollapseDirection.wdCollapseStart);
                rangeSource.MoveEndUntilSafe(":\f\r\n");
                if (rangeSource.Italic == 0)
                {
                    rangeSource.Italic = -1;
                }
            }

            return Task.CompletedTask;
        }

        private static bool IsParagraphFigureTitle(
            Paragraph paragraph,
            string titlePrefixFigure,
            string titlePrefixPicture)
        {
            return paragraph.Range.Fields.OfType<Field>().Any(f => f.Type == WdFieldType.wdFieldSequence)
                   && (ParagraphContains(paragraph, titlePrefixFigure)
                       || ParagraphContains(paragraph, titlePrefixPicture));
        }

        private static bool IsParagraphFigureSource(Paragraph paragraph, string sourcePrefix)
        {
            return paragraph.Range?.Text?.StartsWith(
                sourcePrefix,
                StringComparison.InvariantCultureIgnoreCase) == true;
        }

        private static bool ParagraphContains(Paragraph paragraph, string figureTitlePrefix)
        {
            return paragraph.Range?.Text?.IndexOf(figureTitlePrefix, StringComparison.InvariantCultureIgnoreCase) >= 0;
        }
    }
}
