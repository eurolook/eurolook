using System;
using System.Collections.Generic;
using Eurolook.DocumentProcessing;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands.RepairStyles.StyleRepairTasks
{
    /// <summary>
    /// Retains lists applied to the Numbered Paragraph style using direct formatting.
    /// When doing that, the list template of the Numbered Paragraph style is no longer intact.
    /// </summary>
    [UsedImplicitly]
    public class EcConvertNonEurolookListsRepairTask : ConvertNonEurolookListsRepairTask
    {
        public override Task HandleAsync(IEurolookWordDocument sourceDocument, IEurolookWordDocument repairedDocument)
        {
            var listDash = LanguageIndependentStyle.FromNeutralStyleName("List Dash", repairedDocument.Document);
            var listBullet = LanguageIndependentStyle.FromNeutralStyleName("List Bullet", repairedDocument.Document);
            var listNumber = LanguageIndependentStyle.FromNeutralStyleName("List Number", repairedDocument.Document);

            var numberStyleToListStyleMapping =
                new Dictionary<WdListNumberStyle, Func<string, LanguageIndependentStyle>>
                {
                    { WdListNumberStyle.wdListNumberStyleArabic, _ => listNumber },
                    { WdListNumberStyle.wdListNumberStyleArabic1, _ => listNumber },
                    { WdListNumberStyle.wdListNumberStyleArabic2, _ => listNumber },
                    {
                        WdListNumberStyle.wdListNumberStyleBullet,

                        // NOTE: We map lists of style wdListNumberStyleBullet to either "List Dash" or to "List Bullet".
                        // If the list uses a dash, we apply "List Dash" and otherwise - no matter what symbol is used -
                        // we apply "List Bullet".
                        listString => listDash != null && "--–—−".Contains(listString) ? listDash : listBullet
                    },
                    { WdListNumberStyle.wdListNumberStylePictureBullet, _ => listBullet },
                };

            // get all paragraphs where the list is applied as direct formatting and the paragraph is not a heading
            var sourceParagraphs = GetAllNonHeadingParagraphsWithDirectListFormatting(sourceDocument);

            var paragraphsToBeRestarted = new List<Paragraph>();
            foreach (var sourceParagraph in sourceParagraphs)
            {
                if (sourceParagraph.NumberStyle == null)
                {
                    continue;
                }

                var listStyle = numberStyleToListStyleMapping.ContainsKey(sourceParagraph.NumberStyle.Value)
                    ? numberStyleToListStyleMapping[(WdListNumberStyle)sourceParagraph.NumberStyle](
                        sourceParagraph.ListString)
                    : null;

                if (listStyle == null)
                {
                    continue;
                }

                var paragraph = repairedDocument.Document.Paragraphs[sourceParagraph.Index];

                string newListStyleName =
                    StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(listStyle.NameNeutral, paragraph.Range);
                var newListStyle = LanguageIndependentStyle.FromNeutralStyleName(
                    newListStyleName,
                    paragraph.Range.Document);

                if (newListStyle == null)
                {
                    continue;
                }

                paragraph.set_Style(newListStyle.Style);

                if (sourceParagraph.ListLevel.HasValue)
                {
                    SetListLevelNumberSafe(paragraph, sourceParagraph.ListLevel.Value);
                }

                if (sourceParagraph.IsRestartingListNumbering)
                {
                    // collect all paragraphs that start a new list
                    paragraphsToBeRestarted.Add(paragraph);
                }
            }

            // fix restart
            foreach (var paragraph in paragraphsToBeRestarted)
            {
                var listFormat = paragraph.Range.ListFormat;
                listFormat.ApplyListTemplateWithLevel(
                    listFormat.ListTemplate,
                    false,
                    WdListApplyTo.wdListApplyToWholeList,
                    WdDefaultListBehavior.wdWord10ListBehavior);
            }

            return Task.CompletedTask;
        }
    }
}
