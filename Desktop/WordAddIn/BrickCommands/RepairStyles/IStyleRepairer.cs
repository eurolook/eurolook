﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.WordAddIn.BrickCommands.RepairStyles.StyleRepairTasks;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.BrickCommands.RepairStyles
{
    public interface IStyleRepairer
    {
        Task<Document> RepairStylesAsync(IEurolookWordDocument eurolookDocument);

        Task<Document> RepairStylesAsync(
            IEurolookWordDocument eurolookDocument,
            IEnumerable<IStyleRepairTask> styleRepairTasks);

        Task<Document> RepairStylesAsync(
            IEurolookWordDocument eurolookDocument,
            IEnumerable<IStyleRepairTask> styleRepairTasks,
            IProgressReporter<int> progressReporter);

        Task<Document> RepairStylesAsync(
            IEurolookWordDocument eurolookDocument,
            IEnumerable<IStyleRepairTask> styleRepairTasks,
            IProgressReporter<int> progressReporter,
            Guid? customTemplateId);
    }
}
