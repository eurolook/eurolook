﻿using System;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.BrickCommands.Configuration;
using Eurolook.Common.Log;
using Eurolook.WordAddIn.BrickEngine;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands.RepairStyles
{
    [UsedImplicitly]
    public class RepairStylesCommand : UIBrickCommandBase
    {
        private readonly IMessageService _messageService;
        private readonly Func<IEurolookWordDocument, RepairStyleViewModel> _viewModelCreateFunc;

        private RepairStylesConfigurationV1 _configuration;

        public RepairStylesCommand(
            IMessageService messageService,
            Func<IEurolookWordDocument, RepairStyleViewModel> viewModelCreateFunc)
        {
            _messageService = messageService;
            _viewModelCreateFunc = viewModelCreateFunc;
        }

        public override bool CanExecute(IBrickExecutionContext context, string argument)
        {
            return base.CanExecute(context, argument) && context.EurolookDocument.IsEurolookDocument;
        }

        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            LoadConfiguration();

            var eurolookDocument = context.EurolookDocument;
            try
            {
                if (eurolookDocument.Document.SaveFormat != (int)WdSaveFormat.wdFormatXMLDocument)
                {
                    _messageService.ShowSimpleMessage(
                        "Cannot repair the document’s styles.\n"
                        + "\nSave your document in the most recent file format (*.docx) using the Save As dialog box first.",
                        DisplayName);

                    return Task.FromResult(0);
                }

                var viewModel = _viewModelCreateFunc(context.EurolookDocument);
                viewModel.DisplayName = DisplayName;
                viewModel.HasSaveOption = _configuration.HasSaveOption;

                var window = new RepairStylesView(viewModel);
                _messageService.ShowDialog(window);
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                _messageService.ShowDebugError("An error occurred while repairing the styles.");
            }

            return Task.FromResult(0);
        }

        private void LoadConfiguration()
        {
            _configuration ??= new BrickConfigurationReader().Read<RepairStylesConfigurationV1>(Brick.Configuration) ?? new RepairStylesConfigurationV1();
        }
    }
}
