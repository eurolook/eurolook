﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.Common.Log;
using Eurolook.WordAddIn.BrickCommands.RepairStyles.StyleRepairTasks;
using Eurolook.WordAddIn.Extensions;
using Eurolook.WordAddIn.TemplateManager;
using Eurolook.WordAddIn.TemplateStore.TemplateDownload;
using GalaSoft.MvvmLight.Command;
using Microsoft.Office.Interop.Word;
using Language = Eurolook.Data.Models.Language;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands.RepairStyles
{
    public class RepairStyleViewModel : DialogViewModelBase
    {
        private const string ShowAdvancedOptionsLabel = "Show advanced options";
        private const string HideAdvancedOptionsLabel = "Hide advanced options";

        private readonly IStyleRepairer _styleRepairer;
        private readonly IEurolookWordDocument _eurolookDocument;
        private readonly ISettingsService _settingsService;
        private readonly IMessageService _messageService;
        private readonly ITemplateRepository _templateRepository;
        private readonly ITemplateDownloader _templateDownloader;
        private readonly ILocalTemplatesService _localTemplatesService;
        private readonly ICoAuthoringService _coAuthoringService;

        private bool _isShowingAdvancedOptions;
        private string _showAdvancedOptionsText;
        private SaveOption _saveOption;

        public RepairStyleViewModel(
            IStyleRepairer styleRepairer,
            IEnumerable<IStyleRepairTask> styleRepairTasks,
            IEurolookWordDocument eurolookDocument,
            ISettingsService settingsService,
            IMessageService messageService,
            ITemplateRepository templateRepository,
            ICoAuthoringService coAuthoringService,
            ITemplateDownloader templateDownloader = null,
            ILocalTemplatesService localTemplatesService = null)
        {
            _saveOption = SaveOption.NewDocument;
            _styleRepairer = styleRepairer;
            _eurolookDocument = eurolookDocument;
            _settingsService = settingsService;
            _messageService = messageService;
            _templateRepository = templateRepository;
            _coAuthoringService = coAuthoringService;
            _templateDownloader = templateDownloader;
            _localTemplatesService = localTemplatesService;

            StyleRepairTasks = styleRepairTasks.OrderBy(t => t.ExecutionOrder).ThenBy(t => t.DisplayName)
                                               .Select(t => new StyleRepairTaskViewModel(t)).ToList();

            ShowAdvancedOptionsText = ShowAdvancedOptionsLabel;

            GetHelpCommand = new RelayCommand(GetHelp);

            var properties = _eurolookDocument.GetEurolookProperties();
            AvailableTemplates = new ObservableCollection<BaseTemplateViewModel>
            {
                new()
                {
                    DisplayName = properties.DocumentModelName,
                    IsCustomTemplate = false,
                },
            };

            if (_eurolookDocument.IsBasedOnCustomTemplate)
            {
                AvailableTemplates.Insert(
                    0,
                    new BaseTemplateViewModel
                    {
                        DisplayName = properties.CustomTemplateName,
                        IsCustomTemplate = true,
                    });
            }

            SelectedTemplate = AvailableTemplates.First();
        }

        public override string DisplayName { get; set; }

        public bool IsShowingAdvancedOptions
        {
            get => _isShowingAdvancedOptions;
            set
            {
                Set(() => IsShowingAdvancedOptions, ref _isShowingAdvancedOptions, value);
                ShowAdvancedOptionsText = value ? HideAdvancedOptionsLabel : ShowAdvancedOptionsLabel;
            }
        }

        public bool HasAdvancedOptions => StyleRepairTasks.Any();

        public bool IsBasedOnCustomTemplate => _eurolookDocument.IsBasedOnCustomTemplate;

        public string ShowAdvancedOptionsText
        {
            get => _showAdvancedOptionsText;
            private set => Set(() => ShowAdvancedOptionsText, ref _showAdvancedOptionsText, value);
        }

        public bool HasSaveOption { get; set; }

        public SaveOption SaveOption
        {
            get => _saveOption;
            set => Set(() => SaveOption, ref _saveOption, value);
        }

        public string CreateNewDocumentText { get; set; } = "Your original document will not be modified.";

        public string OverwriteDescriptionText { get; set; } = "Your original document will be overwritten by a repaired version. \n" +
            "This operation cannot be undone.";

        public BaseTemplateViewModel SelectedTemplate { get; set; }

        public ObservableCollection<BaseTemplateViewModel> AvailableTemplates { get; }

        public List<StyleRepairTaskViewModel> StyleRepairTasks { get; }

        public RelayCommand ActivateAllTasks => new(SetInspectionTasksActive);

        public RelayCommand DeactivateAllTasks => new(SetInspectionTasksInactive);

        public RelayCommand GetHelpCommand { get; }

        [SuppressMessage("SonarQube", "S2259: Null pointers should not be dereferenced", Justification = "Null check afterwards")]
        public override async void Commit()
        {
            Document repairedDocument = null;
            Guid? customTemplateId = null;

            if (SaveOption == SaveOption.CurrentDocument && _coAuthoringService.IsCoAuthoring(_eurolookDocument.Document, DisplayName))
            {
                return;
            }

            if (SelectedTemplate.IsCustomTemplate)
            {
                customTemplateId = _eurolookDocument.GetEurolookProperties().CustomTemplateId;
                if (_settingsService.IsStandaloneMode)
                {
                    customTemplateId = await _localTemplatesService?.GetLocalTemplateId(_eurolookDocument);
                    if (!customTemplateId.HasValue)
                    {
                        _messageService.ShowSimpleMessage(
                            $"The template {SelectedTemplate.DisplayName} could not be found in your custom templates. Import the template using the Template Manager.",
                            DisplayName,
                            this);
                        return;
                    }
                }
                else
                {
                    if (!_templateRepository.IsTemplateAvailableOffline(customTemplateId))
                    {
                        var downloadResult = await DownloadTemplate(
                            customTemplateId,
                            _eurolookDocument.Language);
                        if (!downloadResult)
                        {
                            return;
                        }
                    }
                }
            }

            try
            {
                var tasks = StyleRepairTasks.Where(t => t.IsActive).Select(t => t.StyleRepairTask);
                repairedDocument = await _styleRepairer.RepairStylesAsync(
                    _eurolookDocument,
                    tasks,
                    this,
                    customTemplateId);
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
            finally
            {
                IsBusy = false;
                await Task.Delay(400);
                base.Commit();

                if (repairedDocument != null && SaveOption == SaveOption.CurrentDocument)
                {
                    OverwriteEurolookDocument(repairedDocument);
                }

                repairedDocument?.BringToForeground();
            }
        }

        private void OverwriteEurolookDocument(Document repairedDocument)
        {
            try
            {
                string fullName = _eurolookDocument.Document.FullName;
                _eurolookDocument.Document.Saved = true;
                _eurolookDocument.Document.Close();
                repairedDocument.Activate();
                repairedDocument.SaveAs2(fullName);
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        // RelayCommands are weakly bound and cannot use local variable from constructor
        private void GetHelp()
        {
            _settingsService.OpenHelpLink("RepairStyles");
        }

        private void SetInspectionTasksActive()
        {
            foreach (var inspectionTaskViewModel in StyleRepairTasks)
            {
                inspectionTaskViewModel.IsActive = true;
            }
        }

        private void SetInspectionTasksInactive()
        {
            foreach (var inspectionTaskViewModel in StyleRepairTasks)
            {
                inspectionTaskViewModel.IsActive = false;
            }
        }

        private async Task<bool> DownloadTemplate(Guid? templateId, Language targetLanguage)
        {
            if (_templateDownloader == null || !templateId.HasValue)
            {
                return false;
            }

            try
            {
                var downloadRequest = new TemplateDownloadRequest
                {
                    TemplateId = templateId.Value,
                    AfterDownloadValidation = new AfterDownloadValidation
                    {
                        Validate = t => !t.Deleted && t.TemplateFiles.Any(
                                       f => f.LanguageId == targetLanguage.Id && !f.Deleted),
                        FailureMessage = "Selected template or language does no longer exist",
                    },
                };

                return await _templateDownloader.DownloadTemplate(downloadRequest);
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                return false;
            }
        }
    }

    public class BaseTemplateViewModel
    {
        public string DisplayName { get; set; }

        public bool IsCustomTemplate { get; set; }

        public override string ToString()
        {
            return DisplayName
                   + " - "
                   + (IsCustomTemplate ? "Custom Template" : "Eurolook Template");
        }
    }
}
