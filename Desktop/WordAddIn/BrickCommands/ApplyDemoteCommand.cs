﻿using Eurolook.DocumentProcessing;
using Eurolook.WordAddIn.BrickEngine;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    public class ApplyDemoteCommand : ApplyStyleCommandBase
    {
        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (
                new DocumentAutomationHelper(
                    context,
                    "Outline Demote",
                    DocumentAutomationOption.Default | DocumentAutomationOption.RestoreSelection))
            {
                foreach (Paragraph paragraph in context.Selection.Paragraphs)
                {
                    StyleConceptNew.HandleListEnding(paragraph);
                }

                if (IsListSelection(context))
                {
                    await DemoteListLevelAsync(context);
                }
                else
                {
                    await DemoteTextLevelAsync(context);
                }
            }
        }

        private async Task DemoteListLevelAsync(IBrickExecutionContext context)
        {
            var range = context.Selection.Range;
            foreach (Paragraph paragraph in range.Paragraphs)
            {
                var neutralStyle = LanguageIndependentStyle.FromParagraph(paragraph);
                var nameInfo = StyleNameInfo.FromStyleName(neutralStyle.NameNeutral);
                var newName = nameInfo.ListDemote();

                if (!StyleConceptNew.IsStyleAvailable(newName.StyleName, context.Document))
                {
                    continue;
                }

                await ApplyStyleRespectListContinue(paragraph, newName.StyleName);
            }
        }

        private async Task DemoteTextLevelAsync(IBrickExecutionContext context)
        {
            var range = context.Selection.Range;
            Bookmark[] listRestarts = null;

            try
            {
                listRestarts = StyleConceptListRestart.GetListRestarts(range);
                foreach (Paragraph paragraph in range.Paragraphs)
                {
                    var neutralStyle = LanguageIndependentStyle.FromParagraph(paragraph);
                    if (neutralStyle == null)
                    {
                        continue;
                    }

                    var nameInfo = StyleNameInfo.FromStyleName(neutralStyle.NameNeutral);
                    var newName = nameInfo?.TextDemote();

                    if (newName == null || !StyleConceptNew.IsStyleAvailable(newName.StyleName, context.Document))
                    {
                        continue;
                    }

                    await ApplyStyleRespectListContinue(paragraph, newName.StyleName);
                }

                foreach (Table table in range.Tables)
                {
                    var neutralStyle = LanguageIndependentStyle.FromTable(table);
                    if (neutralStyle == null)
                    {
                        continue;
                    }

                    var nameInfo = StyleNameInfo.FromStyleName(neutralStyle.NameNeutral);
                    var newName = nameInfo?.TextDemote();

                    if (newName == null || !StyleConceptNew.IsStyleAvailable(newName.StyleName, context.Document))
                    {
                        continue;
                    }

                    await ApplyTableStyle(table, newName.StyleName);
                }
            }
            finally
            {
                StyleConceptListRestart.ApplyListRestarts(listRestarts);
            }
        }
    }
}
