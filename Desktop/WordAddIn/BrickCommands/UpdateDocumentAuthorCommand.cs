using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.DocumentProcessing;
using Eurolook.WordAddIn.Authors;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.Extensions;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Language = Eurolook.Data.Models.Language;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    [UsedImplicitly]
    public class UpdateDocumentAuthorCommand : EditCommandBase
    {
        private readonly IMessageService _messageService;
        private readonly IAuthorManager _authorManager;
        private readonly IBrickRepository _brickRepository;
        private readonly Func<Document, Language, UpdateDocumentAuthorsViewModel> _updateDocumentAuthorsViewModelFunc;
        private readonly IEnumerable<IChangeAuthorHandler> _changeAuthorHandlers;
        private readonly ICoAuthoringService _coAuthoringService;

        public UpdateDocumentAuthorCommand(
            IMessageService messageService,
            IAuthorManager authorManager,
            IBrickRepository brickRepository,
            Func<Document, Language, UpdateDocumentAuthorsViewModel> updateDocumentAuthorsViewModelFunc,
            IEnumerable<IChangeAuthorHandler> changeAuthorHandlers,
            ICoAuthoringService coAuthoringService)
        {
            _messageService = messageService;
            _authorManager = authorManager;
            _brickRepository = brickRepository;
            _updateDocumentAuthorsViewModelFunc = updateDocumentAuthorsViewModelFunc;
            _changeAuthorHandlers = changeAuthorHandlers;
            _coAuthoringService = coAuthoringService;
            DisplayName = "Update Author Information in Document";
        }

        public override bool CanExecute(IBrickExecutionContext context, string argument)
        {
            return context.EurolookDocument.IsEurolookDocument && base.CanExecute(context, argument);
        }

        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            if (_coAuthoringService.IsCoAuthoring(context.EurolookDocument.GetDocument(), DisplayName))
            {
                return;
            }

            var language = context.EurolookDocument.Language;
            var document = context.Document;

            var viewModel = _updateDocumentAuthorsViewModelFunc(document, language);
            var win = new UpdateDocumentAuthorsWindow(viewModel);
            if (_messageService.ShowDialog(win) != true)
            {
                return;
            }

            using (new DocumentAutomationHelper(
                context,
                DisplayName,
                DocumentAutomationOption.Default | DocumentAutomationOption.RestoreSelection
                                                 | DocumentAutomationOption.DisableTrackRevisions))
            {
                UpdateAuthorXmlParts(viewModel, context);
                RecreateDynamicAuthorBricks(context);
                _authorManager.CleanAuthorXmlParts(document);

                foreach (var changeAuthorHandler in _changeAuthorHandlers.OrderBy(h => h.ExecutionOrder))
                {
                    await changeAuthorHandler.HandleAsync(context.EurolookDocument);
                }
            }
        }

        private void UpdateAuthorXmlParts(UpdateDocumentAuthorsViewModel viewModel, IBrickExecutionContext context)
        {
            foreach (var authorUpdateInfo in viewModel.AuthorUpdateInfos.Where(ai => ai.IsOk))
            {
                var authorXmlPart = context.Document.GetCustomXmlPart(authorUpdateInfo.StoreId);
                var authorCustomXml = authorUpdateInfo.AuthorCustomXml;
                _authorManager.ReplaceAuthorXmlPart(authorXmlPart, authorCustomXml);
            }
        }

        private void RecreateDynamicAuthorBricks(IBrickExecutionContext context)
        {
            var brickContainers = context.Document.LoadAllBricks(_brickRepository).ToList();
            var authorBricks = brickContainers.Where(b => _authorManager.GetAuthorXmlMappings(b.ContentControl).Any());
            context.EurolookDocument.BrickEngine.RecreateDynamicBricksRetainingAuthor(context, authorBricks);
        }
    }
}
