using System.Collections.Generic;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.BrickCommands
{
    [UsedImplicitly]
    public class ApplyTableListStyleCommand : ApplyListStyleCommand
    {
        private readonly IEnumerable<string> _tableListStyleNames = new List<string>
        {
            "Table List Bullet",
            "Table List Dash",
            "Table List Number",
        };

        public override IEnumerable<string> RequiredStyles
        {
            get
            {
                // Table List styles
                foreach (string listStyleName in _tableListStyleNames)
                {
                    yield return listStyleName;
                    yield return listStyleName + " (Level 2)";
                    yield return listStyleName + " (Level 3)";
                    yield return listStyleName + " (Level 4)";
                }
            }
        }
    }
}
