﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.BrickCommands.InsertTable
{
    public class InsertTableConfigurationV1
    {
        public string LabelSource { get; set; } = "LabelSource";

        public string LabelTitle { get; set; } = "LabelTableSeqEC";

        public string LabelCaption { get; set; } = string.Empty;

        public string LabelAlternativeText { get; set; } = "LabelAlternativeText";

        public string TableTitleStyle { get; set; } = "Table Title";

        public string TableCaptionStyle { get; set; } = string.Empty;

        public string TableCopyrightsStyle { get; set; } = string.Empty;

        public string TableNotesStyle { get; set; } = string.Empty;

        public string TableSourceStyle { get; set; } = "Table Source";

        public string TableTextStyle { get; set; } = "Table Text";

        public string TableSubtitleStyle { get; set; } = "Table Subtitle";

        public string AlternativeTextStyle { get; set; } = "Table Alternative";

        public int TableDialogHeight { get; set; } = 490;

        public bool HasItalicSourceLabel { get; set; } = false;

        public bool HasAlternativeText { get; set; } = false;

        public bool HasTableTitle { get; set; } = true;

        public bool HasTableSubtitle { get; set; } = false;

        public bool HasCaption { get; set; } = false;

        public bool HasCopyrights { get; set; } = false;

        public bool HasSourceLine { get; set; } = true;

        public bool HasTableNotes { get; set; } = false;

        public bool HasFirstColumn { get; set; } = true;

        public bool HasTotalRow { get; set; } = true;

        public bool HasBandingRows { get; set; } = true;

        public bool HasHeaderRows { get; set; } = true;

        public bool IsTableTitleSelected { get; set; } = true;

        public bool IsTableSubtitleSelected { get; set; } = false;

        public bool IsCaptionSelected { get; set; } = false;

        public bool IsSourceLineSelected { get; set; } = false;

        public bool IsCopyrightsSelected { get; set; } = false;

        public bool IsTableNotesSelected { get; set; } = false;

        public bool IsHeaderRowsSelected { get; set; } = true;

        public bool IsBandingRowsSelected { get; set; } = true;

        public bool IsFirstColumnSelected { get; set; } = false;

        public bool IsTotalRowSelected { get; set; } = false;

        public bool IsInContentControl { get; set; } = false;

        public bool SettingsSaved { get; set; } = false;

        public bool LoadUserDefinedTableStyles { get; set; } = false;

        public int SpaceBeforeTable { get; set; } = 0;

        public int SpaceAfterTable { get; set; } = 0;

        [UsedImplicitly]
        public List<TableStyleConfiguration> TableStyles { get; set; } = new();
    }

    [SuppressMessage(
        "StyleCop.CSharp.MaintainabilityRules",
        "SA1402:File may only contain a single type",
        Justification = "TableStyleConfiguration is only used implicitly in InsertTableConfigurationV1.")]
    public class TableStyleConfiguration
    {
        public string StyleName { get; set; }

        public string DisplayName { get; set; }

        public bool HasTextLevels { get; set; }

        [UsedImplicitly]
        public List<string> ColorNames { get; set; }
    }
}
