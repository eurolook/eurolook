﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Input;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Common.Extensions;
using GalaSoft.MvvmLight.CommandWpf;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.BrickCommands.InsertTable
{
    public class InsertTableViewModel : BrickCommandViewModelBase, IDataErrorInfo
    {
        private const string PublishingFormatDraft = "";

        private const int DefaultColumnNumber = 3;
        private const int DefaultRowNumber = 3;

        private readonly ISettingsService _settingsService;

        private readonly Dictionary<string, string> _errors = new();
        private string _error;
        private TableStyleViewModel _selectedTableStyle;

        private int _columns;
        private int _rows;
        private bool _isHeaderRowsSelected;
        private bool _isBandingRowsSelected;
        private bool _isTotalRowSelected;
        private bool _isFirstColumnSelected;
        private bool _isTableTitleSelected;
        private bool _isTableSubtitleSelected;
        private bool _isTableCaptionSelected;
        private bool _isSourceLineSelected;
        private bool _isCopyrightsSelected;
        private bool _isTableNotesSelected;

        public InsertTableViewModel(IEurolookWordDocument eurolookWordDocument, ISettingsService settingsService)
            : base(eurolookWordDocument)
        {
            _settingsService = settingsService;
            Columns = DefaultColumnNumber;
            Rows = DefaultRowNumber;
            GetHelpCommand = new RelayCommand(GetHelp);
        }

        public override string Title
        {
            get { return "Insert Table"; }
        }

        public string WindowTitle
        {
            get { return "Eurolook - Insert Table"; }
        }

        public RelayCommand GetHelpCommand { get; }

        public ICommand RestoreDefaultSettingsCommand { get; set; }

        public bool ShowAlternativeText { get; set; }

        public int WindowHeight { get; set; }

        public bool IsAltTextEnabled { get; set; }

        public bool IsTableTitleEnabled { get; set; }

        public bool IsTableSubtitleEnabled { get; set; }

        public bool IsTableCaptionEnabled { get; set; }

        public bool IsSourceLineEnabled { get; set; }

        public bool IsCopyrightsEnabled { get; set; }

        public bool IsTableNotesEnabled { get; set; }

        public bool IsFirstColumnEnabled { get; set; }

        public bool IsTotalRowEnabled { get; set; }

        public bool IsBandingRowsEnabled { get; set; }

        public bool IsHeaderRowsEnabled { get; set; }

        public bool LoadUserDefinedTableStyles { get; set; }

        public int Columns
        {
            get => _columns;
            set => Set(() => Columns, ref _columns, value);
        }

        public int Rows
        {
            get => _rows;
            set => Set(() => Rows, ref _rows, value);
        }

        public bool SettingsSaved { get; set; } = false;

        public bool IsTableTitleSelected
        {
            get => _isTableTitleSelected;
            set
            {
                Set(() => IsTableTitleSelected, ref _isTableTitleSelected, value);
                RaisePropertyChanged(() => IsTableSubtitleSelected);
            }
        }

        public bool IsTableSubtitleSelected
        {
            // The selection of the subtitle is only allowed if the table is selected. It also remembers if the subtitle was selected when selecting the table title.
            get => _isTableSubtitleSelected && IsTableTitleSelected;
            set => Set(() => IsTableSubtitleSelected, ref _isTableSubtitleSelected, value);
        }

        public bool IsTableCaptionSelected
        {
            get => _isTableCaptionSelected;
            set => Set(() => IsTableCaptionSelected, ref _isTableCaptionSelected, value);
        }

        public bool IsSourceLineSelected
        {
            get => _isSourceLineSelected;
            set => Set(() => IsSourceLineSelected, ref _isSourceLineSelected, value);
        }

        public bool IsCopyrightsSelected
        {
            get => _isCopyrightsSelected;
            set => Set(() => IsCopyrightsSelected, ref _isCopyrightsSelected, value);
        }

        public bool IsTableNotesSelected
        {
            get => _isTableNotesSelected;
            set => Set(() => IsTableNotesSelected, ref _isTableNotesSelected, value);
        }

        public bool IsHeaderRowsSelected
        {
            get => _isHeaderRowsSelected;
            set
            {
                _isHeaderRowsSelected = value;
                RegeneratePreview();
                RaisePropertyChanged(() => IsHeaderRowsSelected);
            }
        }

        public bool IsFirstColumnSelected
        {
            get => _isFirstColumnSelected;
            set
            {
                _isFirstColumnSelected = value;
                RegeneratePreview();
                RaisePropertyChanged(() => IsFirstColumnSelected);
            }
        }

        public bool IsTotalRowSelected
        {
            get => _isTotalRowSelected;
            set
            {
                _isTotalRowSelected = value;
                RegeneratePreview();
                RaisePropertyChanged(() => IsTotalRowSelected);
            }
        }

        public bool IsBandingRowsSelected
        {
            get => _isBandingRowsSelected;
            set
            {
                _isBandingRowsSelected = value;
                RegeneratePreview();
                RaisePropertyChanged(() => IsBandingRowsSelected);
            }
        }

        public string AlternativeText { get; set; }

        public ObservableCollection<TableStyleViewModel> TableStyles { get; set; }

        public ObservableCollectionEx<TableStyleColorThemeViewModel> ColorThemes { get; set; }

        public TableStyleViewModel SelectedTableStyle
        {
            get => _selectedTableStyle;
            set
            {
                var oldThemes = _selectedTableStyle?.ColorThemes ?? new List<TableStyleColorThemeViewModel>();
                ColorThemes.AddRange(value.ColorThemes);

                _selectedTableStyle = value;
                RegeneratePreview();
                RaisePropertyChanged(() => SelectedTableStyle);
                RaisePropertyChanged(() => SelectedColorTheme);

                ColorThemes.RemoveRange(oldThemes);
                RaisePropertyChanged(() => HasColorThemesSelection);
            }
        }

        public TableStyleColorThemeViewModel SelectedColorTheme
        {
            get => SelectedTableStyle.SelectedColorTheme;
            set
            {
                SelectedTableStyle.SelectedColorTheme = value;
                RegeneratePreview();
                RaisePropertyChanged(() => SelectedColorTheme);
            }
        }

        public int StyleSelectionColumnSpan
        {
            get
            {
                return ShowColorThemesSelection ? 1 : 3;
            }
        }

        public bool ShowColorThemesSelection => TableStyles.Select(ts => ts.ColorThemes.Count).Any(count => count > 1);

        public bool HasColorThemesSelection
        {
            get
            {
                return ColorThemes.Count() > 1;
            }
        }

        public bool HasStyleSelection
        {
            get
            {
                return TableStyles.Count() > 1;
            }
        }

        public TablePreviewViewModel TablePreview { get; set; }

        public string Error
        {
            get => _error;
            private set
            {
                Set(() => Error, ref _error, value);
                RaisePropertyChanged(() => IsValid);
            }
        }

        public bool IsValid
        {
            get { return _errors.Count == 0; }
        }

        public bool DontRegeneratePreview { get; set; }

        public string this[string propertyName]
        {
            get
            {
                string error = null;

                switch (propertyName)
                {
                    case "Columns" when Columns >= 1 && Columns <= 63:
                        _errors.Remove(propertyName);
                        break;
                    case "Columns":
                        error = "The number of columns must be between 1 and 63";
                        _errors[propertyName] = error;
                        break;
                    case "Rows" when Rows >= 1 && Rows <= 32767:
                        _errors.Remove(propertyName);
                        break;
                    case "Rows":
                        error = "The number of rows must be between 1 and 32767";
                        _errors[propertyName] = error;
                        break;
                }

                Error = error;
                return error;
            }
        }

        public void LoadStylesFromConfig(IEnumerable<TableStyleConfiguration> configurations)
        {
            if (TableStyles != null)
            {
                return;
            }

            TableStyles = new ObservableCollection<TableStyleViewModel>();

            foreach (var config in configurations)
            {
                var tableStyleViewModel = new TableStyleViewModel(config.DisplayName, config.StyleName);
                if (config.ColorNames.IsNullOrEmpty())
                {
                    var style = GetStyle(config.StyleName);
                    if (style != null)
                    {
                        tableStyleViewModel.ColorThemes.Add(
                            new TableStyleColorThemeViewModel(string.Empty, style, EurolookDocument.Document));
                    }
                }
                else
                {
                    foreach (var color in config.ColorNames)
                    {
                        var style = GetStyle(config.StyleName + " " + color);
                        if (style != null)
                        {
                            tableStyleViewModel.ColorThemes.Add(
                                new TableStyleColorThemeViewModel(color, style, EurolookDocument.Document));
                        }
                    }
                }

                TableStyles.Add(tableStyleViewModel);
            }

            ColorThemes = new ObservableCollectionEx<TableStyleColorThemeViewModel>();
            TablePreview = new TablePreviewViewModel();

            SelectedTableStyle = TableStyles.First();
        }

        public void LoadStylesFromUserDefinedTableStyles(IEnumerable<Style> tableStyles, IEnumerable<TableStyleConfiguration> configurations)
        {
            TableStyles = new ObservableCollection<TableStyleViewModel>();
            foreach (var style in tableStyles)
            {
                var tableStyleViewModel = new TableStyleViewModel(style.NameLocal, style.NameLocal);
                tableStyleViewModel.ColorThemes.Add(
                new TableStyleColorThemeViewModel(string.Empty, style, EurolookDocument.Document));
                TableStyles.Add(tableStyleViewModel);
            }

            ColorThemes = new ObservableCollectionEx<TableStyleColorThemeViewModel>();
            TablePreview = new TablePreviewViewModel();

            SetDefaultTableStyleAtFirstPosition(configurations);
            SelectedTableStyle = TableStyles.First();
        }

        private void SetDefaultTableStyleAtFirstPosition(IEnumerable<TableStyleConfiguration> configurations)
        {
            var defaultTableStyle = GetDefaultTableStyle(configurations);
            var index = TableStyles.ToList().IndexOf(defaultTableStyle);
            TableStyles.Move(index, 0);
        }

        private TableStyleViewModel GetDefaultTableStyle(IEnumerable<TableStyleConfiguration> configurations)
        {
            var config = configurations.FirstOrDefault();
            if (config != null)
            {
                return TableStyles.Where(ts => ts.DisplayName.Equals(config.DisplayName)).FirstOrDefault();
            }

            return TableStyles.First();
        }

        public void RestoreDefaultDimensions()
        {
            Columns = DefaultColumnNumber;
            Rows = DefaultRowNumber;
        }

        public void RegeneratePreview()
        {
            if (DontRegeneratePreview)
            {
                return;
            }

            TablePreview?.RegeneratePreview(
                SelectedTableStyle,
                IsHeaderRowsSelected,
                IsBandingRowsSelected,
                IsTotalRowSelected,
                IsFirstColumnSelected);
        }

        public void ShowAlternativeTextForPublishingFormatDraft(string publicationFormat)
        {
            ShowAlternativeText = publicationFormat.Equals(PublishingFormatDraft);
        }

        private Style GetStyle(string name)
        {
            try
            {
                // ReSharper disable once UseIndexedProperty
                return EurolookDocument.Document.Styles.get_Item(name);
            }
            catch (COMException)
            {
                return null;
            }
        }

        // RelayCommands are weakly bound
        private void GetHelp()
        {
            _settingsService.OpenHelpLink("TableBrick");
        }
    }
}
