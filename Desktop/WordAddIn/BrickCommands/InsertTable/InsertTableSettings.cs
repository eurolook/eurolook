﻿namespace Eurolook.WordAddIn.BrickCommands.InsertTable
{
    public class InsertTableSettings
    {
        public int? Columns { get; set; }

        public int? Rows { get; set; }

        public bool? AddTableTitle { get; set; }

        public bool? AddTableSubtitle { get; set; }

        public bool? AddTableCaption { get; set; }

        public bool? AddSourceLine { get; set; }

        public bool? AddCopyrights { get; set; }

        public bool? AddTableNotes { get; set; }

        public bool? EnableHeaderRow { get; set; }

        public bool? EnableTotalRow { get; set; }

        public bool? EnableFirstColumn { get; set; }

        public bool? EnableBandedRows { get; set; }

        public string TableStyleName { get; set; }

        public string ColorThemeName { get; set; }
    }
}
