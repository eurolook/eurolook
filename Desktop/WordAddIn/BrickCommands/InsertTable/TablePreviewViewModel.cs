﻿using GalaSoft.MvvmLight;

namespace Eurolook.WordAddIn.BrickCommands.InsertTable
{
    public class TablePreviewViewModel : ViewModelBase
    {
        public TablePreviewViewModel()
        {
            TopBorder = new BorderModel();
            LeftBorder = new BorderModel();
            RightBorder = new BorderModel();
            BottomBorder = new BorderModel();

            HeaderRowBorder = new BorderModel();
            TotalRowBorder = new BorderModel();
            FirstColumnBorder = new BorderModel();
            VerticalBorder = new BorderModel();
            HorizontalBorder = new BorderModel();
        }

        public BorderModel TopBorder { get; set; }

        public BorderModel LeftBorder { get; set; }

        public BorderModel RightBorder { get; set; }

        public BorderModel BottomBorder { get; set; }

        public BorderModel HeaderRowBorder { get; set; }

        public BorderModel TotalRowBorder { get; set; }

        public BorderModel FirstColumnBorder { get; set; }

        public BorderModel VerticalBorder { get; set; }

        public BorderModel HorizontalBorder { get; set; }

        public string HeaderRowColor { get; set; }

        public string HeaderRowTextColor { get; set; }

        public string Row2Color { get; set; }

        public string Row2TextColor { get; set; }

        public string Row3Color { get; set; }

        public string Row3TextColor { get; set; }

        public string Row4Color { get; set; }

        public string Row4TextColor { get; set; }

        public string TotalRowColor { get; set; }

        public string TotalRowTextColor { get; set; }

        //public string FirstColumnColor { get; set; }
        public string FirstColumnTextColor { get; set; }

        public string HeaderRowFirstCellTextColor { get; set; }

        public string TotalRowFirstCellTextColor { get; set; }

        public void RegeneratePreview(
            TableStyleViewModel selectedStyle,
            bool hasHeaderRow,
            bool hasBandingRows,
            bool hasTotalRow,
            bool hasFirstColumn)
        {
            var colorTheme = selectedStyle.SelectedColorTheme;
            if (colorTheme != null)
            {
                colorTheme.LoadPropertiesFromStyle();

                TopBorder.UpdateFromBorder(colorTheme.Default.TopBorder);
                LeftBorder.UpdateFromBorder(colorTheme.Default.LeftBorder);
                RightBorder.UpdateFromBorder(colorTheme.Default.RightBorder);
                BottomBorder.UpdateFromBorder(colorTheme.Default.BottomBorder);

                VerticalBorder.UpdateFromBorder(colorTheme.Default.VerticalBorder);
                HorizontalBorder.UpdateFromBorder(colorTheme.Default.HorizontalBorder);

                HeaderRowBorder.UpdateFromBorderConditionally(
                    hasHeaderRow,
                    colorTheme.HeaderRow.BottomBorder,
                    colorTheme.Default.HorizontalBorder);
                TotalRowBorder.UpdateFromBorderConditionally(
                    hasTotalRow,
                    colorTheme.TotalRow.TopBorder,
                    colorTheme.Default.HorizontalBorder);
                FirstColumnBorder.UpdateFromBorderConditionally(
                    hasFirstColumn,
                    colorTheme.FirstColumn.RightBorder,
                    colorTheme.Default.VerticalBorder);

                HeaderRowColor = hasHeaderRow
                    ? colorTheme.HeaderRow.ShadingColor
                    : colorTheme.OddRow.ShadingColor;

                HeaderRowTextColor = hasHeaderRow
                    ? colorTheme.HeaderRow.TextColor
                    : colorTheme.OddRow.TextColor;

                Row2Color = hasHeaderRow ? colorTheme.OddRow.ShadingColor : colorTheme.EvenRow.ShadingColor;
                Row2TextColor = hasHeaderRow ? colorTheme.OddRow.TextColor : colorTheme.EvenRow.TextColor;

                Row3Color = hasHeaderRow ? colorTheme.EvenRow.ShadingColor : colorTheme.OddRow.ShadingColor;
                Row3TextColor = hasHeaderRow ? colorTheme.EvenRow.TextColor : colorTheme.OddRow.TextColor;

                Row4Color = Row2Color;
                Row4TextColor = Row2TextColor;

                TotalRowColor = hasTotalRow
                    ? colorTheme.TotalRow.ShadingColor
                    : Row3Color;
                TotalRowTextColor = hasTotalRow
                    ? colorTheme.TotalRow.TextColor
                    : Row3TextColor;

                FirstColumnTextColor = hasFirstColumn
                    ? colorTheme.FirstColumn.TextColor
                    : colorTheme.Default.TextColor;
                HeaderRowFirstCellTextColor = (hasFirstColumn && !hasHeaderRow)
                    ? FirstColumnTextColor
                    : HeaderRowTextColor;
                TotalRowFirstCellTextColor = hasFirstColumn
                    ? FirstColumnTextColor
                    : TotalRowTextColor;

                RaisePropertiesChanged();
            }
        }

        private void RaisePropertiesChanged()
        {
            RaisePropertyChanged(() => TopBorder);
            RaisePropertyChanged(() => LeftBorder);
            RaisePropertyChanged(() => RightBorder);
            RaisePropertyChanged(() => BottomBorder);

            RaisePropertyChanged(() => HeaderRowBorder);
            RaisePropertyChanged(() => TotalRowBorder);
            RaisePropertyChanged(() => FirstColumnBorder);
            RaisePropertyChanged(() => HorizontalBorder);
            RaisePropertyChanged(() => VerticalBorder);

            RaisePropertyChanged(() => HeaderRowColor);
            RaisePropertyChanged(() => HeaderRowTextColor);

            RaisePropertyChanged(() => Row2Color);
            RaisePropertyChanged(() => Row3Color);
            RaisePropertyChanged(() => Row4Color);

            RaisePropertyChanged(() => Row2TextColor);
            RaisePropertyChanged(() => Row3TextColor);
            RaisePropertyChanged(() => Row4TextColor);

            RaisePropertyChanged(() => TotalRowColor);
            RaisePropertyChanged(() => TotalRowTextColor);

            RaisePropertyChanged(() => FirstColumnTextColor);
            RaisePropertyChanged(() => HeaderRowFirstCellTextColor);
            RaisePropertyChanged(() => TotalRowFirstCellTextColor);
        }
    }

    public class BorderModel
    {
        public bool HasBorder { get; set; }

        public string BorderColor { get; set; }

        public double BorderSize { get; set; }

        public void UpdateFromBorder(Border border)
        {
            HasBorder = border.Visible;
            BorderColor = border.Color;
            BorderSize = border.Size / 3.0;
        }

        public void UpdateFromBorderConditionally(bool condition, Border border, Border fallback)
        {
            if (condition && border.Visible)
            {
                UpdateFromBorder(border);
            }
            else
            {
                UpdateFromBorder(fallback);
            }
        }
    }
}
