using System.Collections.Generic;
using System.Linq;
using GalaSoft.MvvmLight;

namespace Eurolook.WordAddIn.BrickCommands.InsertTable
{
    public class TableStyleViewModel : ObservableObject
    {
        private readonly string _styleName;
        private TableStyleColorThemeViewModel _selectedColorThemeViewModel;

        public TableStyleViewModel(string displayName, string styleName = null)
        {
            DisplayName = displayName;
            _styleName = styleName ?? displayName;

            ColorThemes = new List<TableStyleColorThemeViewModel>();
        }

        public string DisplayName { get; set; }

        public List<TableStyleColorThemeViewModel> ColorThemes { get; set; }

        public TableStyleColorThemeViewModel SelectedColorTheme
        {
            get => _selectedColorThemeViewModel ?? ColorThemes.FirstOrDefault();
            set
            {
                _selectedColorThemeViewModel = value;
                RaisePropertyChanged(() => SelectedColorTheme);
            }
        }

        public string StyleName
        {
            get
            {
                if (SelectedColorTheme != null && SelectedColorTheme.Name != string.Empty)
                {
                    return _styleName + " " + SelectedColorTheme.Name;
                }

                return _styleName;
            }
        }

        public override string ToString()
        {
            return DisplayName;
        }
    }
}
