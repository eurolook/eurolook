﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.BrickCommands.Configuration;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.DocumentMetadata;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickCommands.ContentInserter;
using Eurolook.WordAddIn.BrickEngine;
using GalaSoft.MvvmLight.CommandWpf;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands.InsertTable
{
    [UsedImplicitly]
    public class InsertTableCommand : EditCommandBase
    {
        public const string MetaDataName = "PublishingFormat";

        private readonly IDocumentManager _documentManager;
        private readonly ISettingsService _settingsService;
        private readonly IMessageService _messageService;
        private readonly IUserDataRepository _userDataRepository;
        private readonly IBrickRepository _brickRepository;
        private readonly IMetadataReaderWriter _metadataReaderWriter;

        private InsertTableConfigurationV1 _configuration;
        private UserSettings _userSettings;
        private InsertTableSettings _brickSettings;

        public InsertTableCommand(
            IDocumentManager documentManager,
            ISettingsService settingsService,
            IMessageService messageService,
            IUserDataRepository userDataRepository,
            IBrickRepository brickRepository,
            IMetadataReaderWriter metadataReaderWriter)
        {
            _documentManager = documentManager;
            _settingsService = settingsService;
            _messageService = messageService;
            _userDataRepository = userDataRepository;
            _brickRepository = brickRepository;
            _metadataReaderWriter = metadataReaderWriter;
        }

        public override IEnumerable<string> RequiredStyles
        {
            get
            {
                LoadConfiguration();

                bool areTextLevelsAvailable = false;
                
                yield return _configuration.TableTitleStyle;
                yield return _configuration.TableSourceStyle;
                yield return _configuration.TableTextStyle;
                yield return _configuration.AlternativeTextStyle;
                yield return _configuration.TableSubtitleStyle;
                yield return _configuration.TableCaptionStyle;
                yield return _configuration.TableCopyrightsStyle;
                yield return _configuration.TableNotesStyle;

                // Table styles
                foreach (var tableStyle in _configuration.TableStyles)
                {
                    if (tableStyle.ColorNames.IsNullOrEmpty())
                    {
                        yield return tableStyle.StyleName;
                        if (tableStyle.HasTextLevels)
                        {
                            areTextLevelsAvailable = true;
                            for (int i = 1; i < 5; ++i)
                            {
                                yield return tableStyle.StyleName + " " + i;
                            }
                        }
                    }
                    else
                    {
                        foreach (string name in tableStyle.ColorNames.Select(
                                     color => tableStyle.StyleName + " " + color))
                        {
                            yield return name;
                            if (tableStyle.HasTextLevels)
                            {
                                areTextLevelsAvailable = true;
                                for (int i = 1; i < 5; ++i)
                                {
                                    yield return name + " " + i;
                                }
                            }
                        }
                    }
                }

                // Missing Text Levels
                if (areTextLevelsAvailable)
                {
                    for (int i = 1; i < 5; ++i)
                    {
                        yield return _configuration.TableTitleStyle + " " + i;
                        yield return _configuration.TableSourceStyle + " " + i;
                        yield return _configuration.TableTextStyle + " " + i;
                        yield return _configuration.AlternativeTextStyle + " " + i;
                        yield return _configuration.TableSubtitleStyle + " " + i;
                        yield return _configuration.TableCaptionStyle + " " + i;
                        yield return _configuration.TableCopyrightsStyle + " " + i;
                        yield return _configuration.TableNotesStyle + " " + i;
                    }
                }
            }
        }

        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            var document = context.Document;

            try
            {
                var viewModel = await GetViewModel();
                var window = new InsertTableView(viewModel);
                if (viewModel.IsAltTextEnabled)
                {
                    ShowAlternativeTextForPublishingFormatDraft(context, viewModel);
                }

                window.Height = viewModel.WindowHeight;

                if (_messageService.ShowDialog(window, true, true) == true)
                {
                    using var documentAutomationHelper = new DocumentAutomationHelper(context, DisplayName);
                    var range = Insert(context, viewModel);
                    range?.SelectFirstPlaceholderContentControl(documentAutomationHelper.PreferredViewType);

                    await SaveSettings(viewModel);
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
            finally
            {
                document.ActiveWindow.ActivePane.Previous.Activate();
                document.ActiveWindow.SetFocus();
            }
        }

        private Range Insert(IBrickExecutionContext context, InsertTableViewModel table)
        {
            var selection = context.Selection;
            if (selection == null)
            {
                return null;
            }

            var range = selection.Range.GetCursorBrickInsertionRange();
            range.HandleVanishingContentControl();
            range.Select();

            string tableTitleStyle =
                StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(_configuration.TableTitleStyle, range);

            string tableSourceStyle =
                StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(_configuration.TableSourceStyle, range);

            string tableStyle =
                StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(table.SelectedTableStyle.StyleName, range);

            string alternativeTextStyle =
                StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(_configuration.AlternativeTextStyle, range);

            string tableSubtitleStyle =
                StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(_configuration.TableSubtitleStyle, range);

            string tableCaptionStyle =
                StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(_configuration.TableCaptionStyle, range);

            string tableCopyrightsStyle =
                StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(_configuration.TableCopyrightsStyle, range);

            string tableNotesStyle =
                StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel(_configuration.TableNotesStyle, range);

            string altText = table.AlternativeText;

            var titlePlaceholderInserter = table.IsTableTitleSelected
                ? new ParagraphInserter(
                    tableTitleStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                        LabelAlias = _configuration.LabelTitle,
                    },
                    new PlaceholderInserter("Type your title here."))
                : null;

            var subtitlePlaceholderInserter = table.IsTableSubtitleSelected
                ? new ParagraphInserter(
                    tableSubtitleStyle,
                    new PlaceholderInserter("Table subtitle"))
                : null;

            var tableInserter = new TableInserter(tableStyle)
            {
                Columns = table.Columns,
                Rows = table.Rows,
                HasHeaderRow = table.IsHeaderRowsSelected,
                HasTotalRow = table.IsTotalRowSelected,
                HasFirstColumn = table.IsFirstColumnSelected,
                HasBandingRows = table.IsBandingRowsSelected,
                TableTextStyleName = _configuration.TableTextStyle,
                Description = table.IsAltTextEnabled && table.ShowAlternativeText ? string.Empty : altText,
            };

            var alternativeTextInserter = table.IsAltTextEnabled && table.ShowAlternativeText
                ? new ParagraphInserter(
                    alternativeTextStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                        LabelAlias = _configuration.LabelAlternativeText,
                        IsItalic = _configuration.HasItalicSourceLabel,
                    },
                    altText.IsNullOrEmpty()
                        ? new PlaceholderInserter(
                            "Type your alternative text here.")
                        : new TextInserter
                        {
                            Text = altText,
                            IsItalic = false,
                        })
                : null;

            var captionPlaceholderInserter = table.IsTableCaptionSelected
                ? new ParagraphInserter(
                    tableCaptionStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                        LabelAlias = _configuration.LabelCaption,
                    },
                    new PlaceholderInserter("Type your caption here."))
                : null;

            var copyrightsPlaceholderInserter = table.IsCopyrightsSelected
                ? new ParagraphInserter(
                    tableCopyrightsStyle,
                    new TextInserter
                    {
                        Text = "\u00a9",
                        IsItalic = false,
                    },
                    new PlaceholderInserter("Type your Copyrights information here."))
                : null;

            var notesPlaceholderInserter = table.IsTableNotesSelected
                ? new ParagraphInserter(
                    tableNotesStyle,
                    new PlaceholderInserter("Type your notes here."))
                : null;

            var sourcePlaceholderInserter = table.IsSourceLineSelected
                ? new ParagraphInserter(
                    tableSourceStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                        LabelAlias = _configuration.LabelSource,
                        IsItalic = _configuration.HasItalicSourceLabel,
                    },
                    new TextInserter
                    {
                        Text = ":",
                        IsItalic = _configuration.HasItalicSourceLabel,
                    },
                    new TextInserter
                    {
                        Text = " ",
                    },
                    new PlaceholderInserter("Type your source here."))
                : _configuration.IsInContentControl
                    ? new ParagraphInserter(
                        "Normal",
                        new TextInserter
                        {
                            Text = " ",
                            InsertAt = ContentInsertionPosition.Replace,
                        })
                    {
                        SpaceBefore = 0,
                        SpaceAfter = 0,
                        FontSize = 2,
                    }
                    : null;

            // EUROLOOK-3566: if the brick is inserted in the last paragraph of a story outside of a content control, the source line breaks
            var paragraphAfterInserter = !_configuration.IsInContentControl && range.IsLastParagraphInStory()
                ? new ParagraphInserter(
                    range.Paragraphs.Last.GetStyleNameLocal(),
                    new TextInserter { Text = string.Empty })
                : null;

            var itemsBeforeTable = new[]
            {
                titlePlaceholderInserter, subtitlePlaceholderInserter,
                // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            }.Where(c => c != null).ToArray();

            // EUROLOOK-3661
            var itemsAfterTable = new[]
            {
                alternativeTextInserter, copyrightsPlaceholderInserter, sourcePlaceholderInserter, captionPlaceholderInserter, notesPlaceholderInserter, paragraphAfterInserter,
                // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            }.Where(c => c != null).ToArray();

            if (itemsBeforeTable.Any() && _configuration.SpaceBeforeTable != 0)
            {
                itemsBeforeTable.Last().SpaceAfter = _configuration.SpaceBeforeTable;
            }

            if (itemsAfterTable.Any())
            {
                itemsAfterTable.Last().Content.First().InsertAt = ContentInsertionPosition.Replace;

                if (_configuration.SpaceAfterTable != 0)
                {
                    itemsAfterTable.First().SpaceBefore = _configuration.SpaceAfterTable;
                }
            }

            var itemsToInsert = new List<IContentInserter>();
            itemsToInsert.AddRange(itemsBeforeTable);
            itemsToInsert.Add(tableInserter);
            itemsToInsert.AddRange(itemsAfterTable);

            ContentInserterBase inserter = _configuration.IsInContentControl
                ? new BrickContainerInserter(Brick, itemsToInsert.ToArray())
                : new RangeInserter(itemsToInsert.ToArray());

            var insertRangeProvider = new RangePositionProvider(range);

            return inserter.Insert(insertRangeProvider);
        }

        private async Task<InsertTableViewModel> GetViewModel()
        {
            LoadConfiguration();
            await LoadSettings();

            var documentViewModel = _documentManager.GetActiveDocumentViewModel();
            var vm = documentViewModel.InsertTableViewModel;
            vm.DontRegeneratePreview = true;

            SetTableConfiguration(vm);

            if (vm.LoadUserDefinedTableStyles)
            {
                var tableStyles = documentViewModel.Document.Styles.OfType<Style>().Where(s => s.Type == WdStyleType.wdStyleTypeTable && !s.BuiltIn);
                vm.LoadStylesFromUserDefinedTableStyles(tableStyles, _configuration.TableStyles);
            }
            else
            {
                vm.LoadStylesFromConfig(_configuration.TableStyles);
            }

            ApplySettings(vm);

            vm.RestoreDefaultSettingsCommand = new RelayCommand<InsertTableViewModel>(RestoreSettings);

            vm.DontRegeneratePreview = false;
            vm.RegeneratePreview(); // refresh already loaded styles

            return vm;
        }

        private void SetTableConfiguration(InsertTableViewModel viewModel)
        {
            viewModel.WindowHeight = _configuration.TableDialogHeight;
            viewModel.AlternativeText = string.Empty; // reset the alternative text
            viewModel.IsAltTextEnabled = _configuration.HasAlternativeText;
            viewModel.IsTableTitleEnabled = _configuration.HasTableTitle;
            viewModel.IsTableSubtitleEnabled = _configuration.HasTableSubtitle;
            viewModel.IsTableCaptionEnabled = _configuration.HasCaption;
            viewModel.IsSourceLineEnabled = _configuration.HasSourceLine;
            viewModel.IsCopyrightsEnabled = _configuration.HasCopyrights;
            viewModel.IsTableNotesEnabled = _configuration.HasTableNotes;
            viewModel.IsFirstColumnEnabled = _configuration.HasFirstColumn;
            viewModel.IsTotalRowEnabled = _configuration.HasTotalRow;
            viewModel.IsBandingRowsEnabled = _configuration.HasBandingRows;
            viewModel.IsHeaderRowsEnabled = _configuration.HasHeaderRows;
            viewModel.IsTableTitleSelected = _configuration.IsTableTitleSelected;
            viewModel.IsTableSubtitleSelected = _configuration.IsTableSubtitleSelected;
            viewModel.IsTableCaptionSelected = _configuration.IsCaptionSelected;
            viewModel.IsSourceLineSelected = _configuration.IsSourceLineSelected;
            viewModel.IsCopyrightsSelected = _configuration.IsCopyrightsSelected;
            viewModel.IsTableNotesSelected = _configuration.IsTableNotesSelected;
            viewModel.IsHeaderRowsSelected = _configuration.IsHeaderRowsSelected;
            viewModel.IsBandingRowsSelected = _configuration.IsBandingRowsSelected;
            viewModel.IsFirstColumnSelected = _configuration.IsFirstColumnSelected;
            viewModel.IsTotalRowSelected = _configuration.IsTotalRowSelected;
            viewModel.LoadUserDefinedTableStyles = _configuration.LoadUserDefinedTableStyles;
        }

        private void ApplySettings(InsertTableViewModel viewModel)
        {
            if (!_configuration.SettingsSaved)
            {
                return;
            }

            viewModel.SettingsSaved = true;

            viewModel.Columns = _brickSettings.Columns ?? viewModel.Columns;
            viewModel.Rows = _brickSettings.Rows ?? viewModel.Rows;

            viewModel.IsTableTitleSelected = _brickSettings.AddTableTitle ?? viewModel.IsTableTitleSelected;
            viewModel.IsTableSubtitleSelected = _brickSettings.AddTableSubtitle ?? viewModel.IsTableSubtitleSelected;
            viewModel.IsTableCaptionSelected = _brickSettings.AddTableCaption ?? viewModel.IsTableCaptionSelected;
            viewModel.IsSourceLineSelected = _brickSettings.AddSourceLine ?? viewModel.IsSourceLineSelected;
            viewModel.IsCopyrightsSelected = _brickSettings.AddCopyrights ?? viewModel.IsCopyrightsSelected;
            viewModel.IsTableNotesSelected = _brickSettings.AddTableNotes ?? viewModel.IsTableNotesSelected;
            viewModel.IsHeaderRowsSelected = _brickSettings.EnableHeaderRow ?? viewModel.IsHeaderRowsSelected;
            viewModel.IsTotalRowSelected = _brickSettings.EnableTotalRow ?? viewModel.IsTotalRowSelected;
            viewModel.IsFirstColumnSelected = _brickSettings.EnableFirstColumn ?? viewModel.IsFirstColumnSelected;
            viewModel.IsBandingRowsSelected = _brickSettings.EnableBandedRows ?? viewModel.IsBandingRowsSelected;

            if (!string.IsNullOrEmpty(_brickSettings.TableStyleName))
            {
                viewModel.SelectedTableStyle =
                    viewModel.TableStyles.FirstOrDefault(s => s.DisplayName.Equals(_brickSettings.TableStyleName))
                    ?? viewModel.SelectedTableStyle;
            }

            if (!string.IsNullOrEmpty(_brickSettings.ColorThemeName))
            {
                viewModel.SelectedColorTheme =
                    viewModel.ColorThemes.FirstOrDefault(t => t.Name.Equals(_brickSettings.ColorThemeName))
                    ?? viewModel.SelectedColorTheme;
            }
        }

        private void RestoreSettings(InsertTableViewModel viewModel)
        {
            SetTableConfiguration(viewModel);
            viewModel.RestoreDefaultDimensions();

            foreach (var style in viewModel.TableStyles)
            {
                style.SelectedColorTheme = style.ColorThemes?.FirstOrDefault();
            }

            viewModel.SelectedTableStyle = viewModel.TableStyles?.First();
            viewModel.SelectedColorTheme = viewModel.ColorThemes?.FirstOrDefault();
        }

        private async Task SaveSettings(InsertTableViewModel viewModel)
        {
            if (!_configuration.SettingsSaved)
            {
                return;
            }

            _brickSettings.Columns = viewModel.Columns;
            _brickSettings.Rows = viewModel.Rows;
            _brickSettings.AddTableTitle = viewModel.IsTableTitleEnabled ? viewModel.IsTableTitleSelected : _brickSettings.AddTableTitle;
            _brickSettings.AddTableSubtitle = viewModel.IsTableSubtitleEnabled ? viewModel.IsTableSubtitleSelected : _brickSettings.AddTableSubtitle;
            _brickSettings.AddTableCaption = viewModel.IsTableCaptionEnabled ? viewModel.IsTableCaptionSelected : _brickSettings.AddTableCaption;
            _brickSettings.AddSourceLine = viewModel.IsSourceLineEnabled ? viewModel.IsSourceLineSelected : _brickSettings.AddSourceLine;
            _brickSettings.AddCopyrights = viewModel.IsCopyrightsEnabled ? viewModel.IsCopyrightsSelected : _brickSettings.AddCopyrights;
            _brickSettings.AddTableNotes = viewModel.IsTableNotesEnabled ? viewModel.IsTableNotesSelected : _brickSettings.AddTableNotes;
            _brickSettings.EnableHeaderRow = viewModel.IsHeaderRowsEnabled ? viewModel.IsHeaderRowsSelected : _brickSettings.EnableHeaderRow;
            _brickSettings.EnableTotalRow = viewModel.IsTotalRowEnabled ? viewModel.IsTotalRowSelected : _brickSettings.EnableTotalRow;
            _brickSettings.EnableFirstColumn = viewModel.IsFirstColumnEnabled ? viewModel.IsFirstColumnSelected : _brickSettings.EnableFirstColumn;
            _brickSettings.EnableBandedRows = viewModel.IsBandingRowsEnabled ? viewModel.IsBandingRowsSelected : _brickSettings.EnableBandedRows;

            _brickSettings.TableStyleName = viewModel.SelectedTableStyle.DisplayName;
            _brickSettings.ColorThemeName = viewModel.SelectedColorTheme.Name;

            await _brickRepository.SaveBrickSettings(_userSettings.Id, Brick.Id, _brickSettings);
        }

        private async Task LoadSettings()
        {
            _userSettings = await _userDataRepository.GetUserSettingsAsync();
            _brickSettings =
                await _brickRepository.GetBrickSettingsAsync<InsertTableSettings>(
                    _userSettings.Id,
                    Brick.Id)
                ?? new InsertTableSettings();
        }

        private void LoadConfiguration()
        {
            if (_configuration == null)
            {
                var brickConfigurationReader = new BrickConfigurationReader();
                _configuration = brickConfigurationReader.Read<InsertTableConfigurationV1>(Brick.Configuration)
                                 ?? new InsertTableConfigurationV1();
            }
        }

        private void ShowAlternativeTextForPublishingFormatDraft(IBrickExecutionContext context, InsertTableViewModel viewModel)
        {
            _metadataReaderWriter.Init(context.EurolookDocument);
            var metaDataDefinition = _metadataReaderWriter.GetMetadataDefinition(MetaDataName);
            string metaDataValue = ""; // Interpret as Draft when Metadata does not exist
            if (metaDataDefinition != null)
            {
                metaDataValue = _metadataReaderWriter.ReadMetadataProperty(metaDataDefinition)?.GetSimpleValue();
            }

            viewModel.ShowAlternativeTextForPublishingFormatDraft(metaDataValue);
        }
    }
}
