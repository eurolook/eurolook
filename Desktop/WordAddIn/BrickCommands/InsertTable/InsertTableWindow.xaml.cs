﻿using System;
using System.Windows;
using System.Windows.Input;

namespace Eurolook.WordAddIn.BrickCommands.InsertTable
{
    public partial class InsertTableView
    {
        public InsertTableView(InsertTableViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }

        private void OnOkClick(object sender, RoutedEventArgs e)
        {
            try
            {
                DialogResult = true; // will close the dialog automatically
            }
            catch (InvalidOperationException)
            {
                // when not opened as dialog
                Close();
            }
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!int.TryParse(e.Text, out int value))
            {
                e.Handled = true;
            }
        }

        private void TextBox_PreviewExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (e.Command == ApplicationCommands.Copy ||
                e.Command == ApplicationCommands.Cut ||
                e.Command == ApplicationCommands.Paste)
            {
                e.Handled = true;
            }
        }

        private void TextBox_PreviewKeyDownColumns(object sender, KeyEventArgs e)
        {
            var viewModel = (InsertTableViewModel)this.DataContext;

            PreventSpaceKey(e);

            if (IsDeleteKeyPressedWhenSingleDigitInput(e, viewModel.Columns))
            {
                viewModel.Columns = 0;
                InputColumns.Text = "";
            }
        }

        private void TextBox_PreviewKeyDownRows(object sender, KeyEventArgs e)
        {
            var viewModel = (InsertTableViewModel)this.DataContext;

            PreventSpaceKey(e);

            if (IsDeleteKeyPressedWhenSingleDigitInput(e, viewModel.Rows))
            {
                viewModel.Rows = 0;
                InputRows.Text = "";
            }
        }

        private static void PreventSpaceKey(KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        private bool IsDeleteKeyPressedWhenSingleDigitInput(KeyEventArgs e, int input)
        {
            return (e.Key == Key.Back || e.Key == Key.Delete) && input < 10;
        }
    }
}
