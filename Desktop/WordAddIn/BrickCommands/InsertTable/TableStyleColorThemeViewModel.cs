﻿using Eurolook.DocumentProcessing.Extensions;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using WordBorder = Microsoft.Office.Interop.Word.Border;

namespace Eurolook.WordAddIn.BrickCommands.InsertTable
{
    public class TableStyleColorThemeViewModel
    {
        private readonly Style _style;
        private readonly ThemeColorScheme _colorScheme;
        private bool _isLoaded;

        public TableStyleColorThemeViewModel(string name, Style style, Document document)
        {
            Name = name;
            _style = style;
            _colorScheme = document.DocumentTheme.ThemeColorScheme;
        }

        public string Name { get; }

        public TablePart Default { get; set; }

        public TablePart HeaderRow { get; set; }

        public TablePart FirstColumn { get; set; }

        public TablePart TotalRow { get; set; }

        public TablePart EvenRow { get; set; }

        public TablePart OddRow { get; set; }

        public override string ToString()
        {
            return Name;
        }

        public void LoadPropertiesFromStyle()
        {
            if (_isLoaded)
            {
                return;
            }

            Default = LoadTablePartInformation(_style.Font, _style.Table.Shading, _style.Table.Borders);

            HeaderRow = LoadTablePartInformation(_style.Table.Condition(WdConditionCode.wdFirstRow));
            FirstColumn = LoadTablePartInformation(_style.Table.Condition(WdConditionCode.wdFirstColumn));
            TotalRow = LoadTablePartInformation(_style.Table.Condition(WdConditionCode.wdLastRow));
            EvenRow = LoadTablePartInformation(_style.Table.Condition(WdConditionCode.wdEvenRowBanding));
            OddRow = LoadTablePartInformation(_style.Table.Condition(WdConditionCode.wdOddRowBanding));

            _isLoaded = true;
        }

        private TablePart LoadTablePartInformation(ConditionalStyle conditionalStyle)
        {
            return LoadTablePartInformation(conditionalStyle.Font, conditionalStyle.Shading, conditionalStyle.Borders);
        }

        private TablePart LoadTablePartInformation(Font font, Shading shading, Borders borders)
        {
            return new TablePart
            {
                TextSize = 8,
                TextColor = font.Color.ToHexString(_colorScheme),
                ShadingColor = shading.BackgroundPatternColor.ToHexString(_colorScheme, "#ffffff"),
                TopBorder = LoadBorderInformation(borders[WdBorderType.wdBorderTop]),
                LeftBorder = LoadBorderInformation(borders[WdBorderType.wdBorderLeft]),
                RightBorder = LoadBorderInformation(borders[WdBorderType.wdBorderRight]),
                BottomBorder = LoadBorderInformation(borders[WdBorderType.wdBorderBottom]),
                VerticalBorder = LoadBorderInformation(borders[WdBorderType.wdBorderVertical]),
                HorizontalBorder = LoadBorderInformation(borders[WdBorderType.wdBorderHorizontal]),
            };
        }

        private Border LoadBorderInformation(WordBorder border)
        {
            return new Border
            {
                Visible = border.Visible,
                Color = border.Color.ToHexString(_colorScheme),
                Size = (int)border.LineWidth,
            };
        }
    }

    public class TablePart
    {
        public int TextSize { get; set; }

        public string TextColor { get; set; }

        public string ShadingColor { get; set; }

        public Border TopBorder { get; set; }

        public Border LeftBorder { get; set; }

        public Border RightBorder { get; set; }

        public Border BottomBorder { get; set; }

        public Border VerticalBorder { get; set; }

        public Border HorizontalBorder { get; set; }
    }

    public class Border
    {
        public bool Visible { get; set; }

        public string Color { get; set; }

        public int Size { get; set; }
    }
}
