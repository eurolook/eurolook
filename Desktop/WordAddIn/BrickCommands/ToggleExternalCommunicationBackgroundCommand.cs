using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.Database;
using Eurolook.Common.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.Extensions;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.BrickCommands
{
    [UsedImplicitly]
    public class ToggleExternalCommunicationBackgroundCommand : EditCommandBase
    {
        private static readonly Guid HeaderDialogikaBrick = new Guid("67239e97-6b52-4fff-a07a-ab5f7f522a93");
        private static readonly Guid HeaderDokumentaBrick = new Guid("afb7c636-d08e-4111-899a-00a33c25ada9");
        private static readonly Guid HeaderOffBrick = new Guid("ea8ad999-458f-427c-a05f-88eabd77f338");

        private static readonly Guid FooterDialogikaBrick = new Guid("708bc4b2-26a9-4f8e-aac2-2f92791549e1");
        private static readonly Guid FooterDokumentaBrick = new Guid("37a19a11-cc56-4e20-88f7-978f7bdec5fa");
        private static readonly Guid FooterOffBrick = new Guid("d8755860-362d-4b4c-9773-2874f1f75be2");

        private static readonly Guid ReturnAddressDialogikaBrick = new Guid("57c84baa-f161-4935-8ddf-9230cec9e42b");
        private static readonly Guid ReturnAddressDokumentaBrick = new Guid("65d37228-6e19-4877-96e8-020bc793c0b3");
        private static readonly Guid ReturnAddressOffBrick = new Guid("d057bc72-71e4-410b-a27e-8892b94b9006");

        private static readonly List<Guid> OnBricks = new List<Guid>()
        {
            HeaderDialogikaBrick,
            HeaderDokumentaBrick,
            FooterDialogikaBrick,
            FooterDokumentaBrick,
            ReturnAddressDialogikaBrick,
            ReturnAddressDokumentaBrick,
        };

        private static readonly List<Guid> OffBricks = new List<Guid>()
        {
            HeaderOffBrick,
            FooterOffBrick,
            ReturnAddressOffBrick,
        };

        private readonly IBrickRepository _brickRepository;
        private readonly IDocumentManager _documentManager;

        public ToggleExternalCommunicationBackgroundCommand(
            IBrickRepository brickRepository,
            IDocumentManager documentManager)
        {
            _brickRepository = brickRepository;
            _documentManager = documentManager;
        }

        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            var documentViewModel = _documentManager.GetActiveDocumentViewModel();

            using (new DocumentAutomationHelper(context, DisplayName))
            {
                var bricksInDocumentModel = documentViewModel.DocumentModel?.BrickReferences.Select(bR => bR.BrickId).ToList() ?? new List<Guid>();
                var enabledOnControls = documentViewModel.Document.LoadAllBricks(_brickRepository).Where(c => OnBricks.Contains(c.Id));

                if (enabledOnControls.IsNullOrEmpty())
                {
                    foreach (var brick in OnBricks.Intersect(bricksInDocumentModel))
                    {
                        documentViewModel.BrickEngine.ApplyBrickAsync(brick);
                    }
                }
                else
                {
                    foreach (var brick in OffBricks)
                    {
                        documentViewModel.BrickEngine.ApplyBrickAsync(brick);
                    }
                }
            }

            return Task.FromResult(0);
        }
    }
}
