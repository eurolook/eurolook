﻿using System.IO;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.Common;
using Eurolook.Common.Log;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.Messages;
using GalaSoft.MvvmLight.Messaging;

namespace Eurolook.WordAddIn.BrickCommands
{
    public class ToggleTestModeCommand : EditCommandBase
    {
        private const string FileName = ".TESTMODE";
        private readonly ISettingsService _settingsService;

        public ToggleTestModeCommand(ISettingsService settingsService)
        {
            _settingsService = settingsService;
        }

        public override bool CanExecute(IBrickExecutionContext context, string argument)
        {
            var dir = Directories.GetApplicationDataDirectory(_settingsService.UseRoamingDataDirectory);
            return base.CanExecute(context, argument) && File.Exists(Path.Combine(dir.FullName, FileName));
        }

        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            if (_settingsService.IsInTestMode)
            {
                _settingsService.DisableTestMode();
                this.LogInfo("Test mode has been disabled.");
            }
            else
            {
                _settingsService.EnableTestMode();
                this.LogInfo("Test mode has been enabled.");
            }

            Messenger.Default.Send(new UpdateNotificationsMessage());
            return Task.CompletedTask;
        }
    }
}
