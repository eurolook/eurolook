﻿namespace Eurolook.WordAddIn.BrickCommands.InsertName;

public class InsertNameConfiguration
{
    public bool IncludeEurolookAuthorsInSearch { get; set; } = false;

    public string EurolookAuthorCategoryName { get; set; } = "";
}
