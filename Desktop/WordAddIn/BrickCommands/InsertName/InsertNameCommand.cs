﻿using System;
using System.Linq;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.BrickCommands.Configuration;
using Eurolook.AddIn.Common.Database;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands.InsertName
{
    public class InsertNameCommand : UIBrickCommandBase
    {
        private readonly IDocumentManager _documentManager;
        private readonly IUserDataRepository _userDataRepository;
        private readonly ISettingsService _settingsService;

        private readonly Func<IEurolookWordDocument, InsertNameConfiguration, InsertNameViewModel>
            _insertNameViewModelCreateFunc;

        public InsertNameCommand(
            IDocumentManager documentManager,
            IUserDataRepository userDataRepository,
            ISettingsService settingsService,
            Func<IEurolookWordDocument, InsertNameConfiguration, InsertNameViewModel> insertNameViewModelCreateFunc)
        {
            _documentManager = documentManager;
            _userDataRepository = userDataRepository;
            _settingsService = settingsService;
            _insertNameViewModelCreateFunc = insertNameViewModelCreateFunc;
        }

        public override bool CanExecute(IBrickExecutionContext context, string argument)
        {
            var document = context.Document;
            if (document == null)
            {
                return false;
            }

            var selection = context.Selection;
            if (selection == null)
            {
                return false;
            }

            if (selection.Range.IsLocked())
            {
                return false;
            }

            if (selection.Range.HasPlainTextContentControl())
            {
                return false;
            }

            if ((bool)selection.Range.Information[WdInformation.wdAtEndOfRowMarker])
            {
                return false;
            }

            return base.CanExecute(context, argument);
        }

        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            var selection = context.Selection;
            if (selection == null)
            {
                return;
            }

            var jsonSettings = Brick.Configuration;
            var brickConfigurationReader = new BrickConfigurationReader();
            var config = jsonSettings != null
                ? brickConfigurationReader.Read<InsertNameConfiguration>(jsonSettings)
                : new InsertNameConfiguration();

            var settings = _userDataRepository.GetUserSettings();

            var viewModel = _insertNameViewModelCreateFunc(context.EurolookDocument, config);

            // IsInsertNameInlineEnabled => get prefix; otherwise always work on selection
            string searchExpression = (settings?.IsInsertNameInline == true
                                          ? GetLastTypedWordFromDocument(selection)
                                          : selection.Range.TextWithoutPlaceholders()) ?? "";

            viewModel.SearchExpression = searchExpression.Trim();

            var filteredPersonNames = (await viewModel.FilteredPersonNames.Task).ToList();

            var singleMatch = filteredPersonNames.Count == 1 ? filteredPersonNames.First() : null;
            if ((singleMatch != null) && (settings?.IsInsertNameInline == true))
            {
                Insert(context, singleMatch);

                viewModel.Cancel();
                return;
            }

            viewModel.Show(
                () =>
                {
                    if (viewModel.SelectedPerson == null)
                    {
                        return;
                    }

                    var document = context.Document;
                    try
                    {
                        Insert(context, viewModel.SelectedPerson);
                    }
                    finally
                    {
                        document.ActiveWindow.ActivePane.Previous.Activate();
                        document.ActiveWindow.SetFocus();
                    }
                });
        }

        private void Insert(IBrickExecutionContext context, PersonName personName)
        {
            var selection = context.Selection;
            if (selection == null)
            {
                return;
            }

            using (new DocumentAutomationHelper(context, "Insert Name"))
            {
                selection.Range.HandleVanishingContentControl();
                selection.Text = personName.InsertName;
            }
        }

        private string GetLastTypedWordFromDocument(Selection selection)
        {
            string result;
            Range originalSelection = null;
            try
            {
                originalSelection = selection.Range;

                if (selection.Start != selection.End)
                {
                    result = selection.Range.TextWithoutPlaceholders();
                }
                else
                {
                    selection.MoveStart(WdUnits.wdWord, -1);
                    result = selection.Range.TextWithoutPlaceholders();
                    if ((result == null) || !char.IsLetter(result.Last()))
                    {
                        result = "";
                        selection.Collapse(WdCollapseDirection.wdCollapseEnd);
                    }
                }

                result = result.Trim();
            }
            catch (Exception)
            {
                originalSelection?.Select();

                result = "";
            }

            return result;
        }
    }
}
