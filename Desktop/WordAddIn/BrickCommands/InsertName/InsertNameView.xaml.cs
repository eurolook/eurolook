using System.Windows;
using Eurolook.WordAddIn.Messages;
using GalaSoft.MvvmLight.Messaging;

namespace Eurolook.WordAddIn.BrickCommands.InsertName
{
    public partial class InsertNameView
    {
        public InsertNameView()
        {
            InitializeComponent();

            Messenger.Default.Register<ShowCommandBrickPanelMessage>(this, OnShowCommandBrickPanel);

            RoutedEventHandler handler = null;
            handler = (sender, args) =>
                      {
                          SetInitialFocus();
                          SearchTextBox.Loaded -= handler;
                      };
            SearchTextBox.Loaded += handler;
        }

        private void OnShowCommandBrickPanel(ShowCommandBrickPanelMessage obj)
        {
            if (SearchTextBox.IsLoaded)
            {
                SetInitialFocus();
            }
        }

        private void SetInitialFocus()
        {
            SearchTextBox.CaretIndex = SearchTextBox.Text.Length;
            SearchTextBox.Focus();
        }
    }
}
