﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Data.Models;
using Eurolook.DataSync.HttpClient;
using Eurolook.WordAddIn.Utils;
using GalaSoft.MvvmLight.CommandWpf;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.BrickCommands.InsertName;

public class InsertNameViewModel : BrickCommandPanelViewModelBase
{
    private readonly StringNormalizer _stringNormalizer;
    private readonly ISettingsService _settingsService;
    private readonly IEurolookDataRepository _eurolookDataRepository;
    private readonly InsertNameConfiguration _configuration;
    private string _searchExpression;
    private List<PersonName> _allPersonNames;

    private PersonName _selectedPerson;

    public InsertNameViewModel(
        ISettingsService settingsService,
        IEurolookDataRepository eurolookDataRepository,
        IEurolookWordDocument eurolookDocument,
        InsertNameConfiguration configuration)
        : base(eurolookDocument)
    {
        _settingsService = settingsService;
        _eurolookDataRepository = eurolookDataRepository;
        _configuration = configuration;
        _searchExpression = "";

        _stringNormalizer = new StringNormalizer(_eurolookDataRepository.GetCharacterMappings());

        FilteredPersonNames =
            new NotifyTask<List<PersonName>>().NotifyOnCompletion(this).Start(UpdateSearchResultAsync);

        GetHelpCommand = new RelayCommand(() => _settingsService.OpenHelpLink("NameBrick"));
    }

    public override string Title
    {
        get { return "Insert Name"; }
    }

    public RelayCommand GetHelpCommand { get; }

    public IEnumerable<PersonName> AllPersonNames
    {
        get
        {
            if (_allPersonNames == null)
            {
                _allPersonNames = _eurolookDataRepository.GetPersonNames();
                foreach (var personName in _allPersonNames)
                {
                    personName.SearchName = _stringNormalizer.Normalize(personName.LastName)
                                            + Environment.NewLine
                                            + _stringNormalizer.Normalize(personName.FirstName);
                }
            }

            return _allPersonNames;
        }
    }

    public string SearchExpression
    {
        get { return _searchExpression ?? ""; }
        set
        {
            Set(() => SearchExpression, ref _searchExpression, value);
            UpdateSearchResults();
        }
    }

    [NotNull]
    public NotifyTask<List<PersonName>> FilteredPersonNames { get; set; }

    public PersonName SelectedPerson
    {
        get
        {
            return _selectedPerson;
        }
        set
        {
            if (_selectedPerson == value)
            {
                return;
            }

            _selectedPerson = value;

            RaisePropertyChanged(() => SelectedPerson);
            CommitCommand.RaiseCanExecuteChanged();
        }
    }

    public override bool CanCommit()
    {
        return SelectedPerson != null && !SelectedPerson.Deleted;
    }

    public void UpdateSearchResults()
    {
        FilteredPersonNames =
            new NotifyTask<List<PersonName>>().NotifyOnCompletion(this).Start(UpdateSearchResultAsync);
        RaisePropertyChanged(() => FilteredPersonNames);
    }

    public async Task<List<PersonName>> UpdateSearchResultAsync()
    {
        string normalizedSearchExpression = GetNormalizedSearchExpression();
        var searchTerms = normalizedSearchExpression.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
        var result =
            AllPersonNames.Where(pn => !searchTerms.Any() || IsMatch(pn.SearchName, searchTerms))
                          .OrderBy(pn => pn.Category)
                          .ThenBy(pn => pn.DisplayName)
                          .ToList();

        var currentSelection = SelectedPerson;

        if (_configuration.IncludeEurolookAuthorsInSearch)
        {
            // search author database on server
            try
            {
                const int pageSize = 50;
                var searchClient = new SearchClient(_settingsService.DataSyncUrl);
                await searchClient.Login();
                var searchResults = await searchClient.SearchAuthors(
                    CancellationToken.None,
                    normalizedSearchExpression,
                    0,
                    pageSize);

                var authors =
                    searchResults.Select(
                                     r => new PersonName
                                     {
                                         Category = _configuration.EurolookAuthorCategoryName,
                                         FirstName = r.LatinFirstName,
                                         LastName = r.LatinLastName,
                                         Id = r.Id,
                                     })
                                 .Where(pn => !string.IsNullOrWhiteSpace(pn.LastName))
                                 .ToList();

                if (authors.Count == pageSize)
                {
                    authors.Add(
                        new PersonName
                        {
                            Category = _configuration.EurolookAuthorCategoryName,
                            LastName = "More...",
                            Deleted = true,
                        });
                }

                result = authors.Concat(result).ToList();
            }
            catch (HttpRequestException)
            {
                var pn = new PersonName
                {
                    Category = _configuration.EurolookAuthorCategoryName,
                    LastName = "Search not available.",
                    Deleted = true,
                };
                result = result.Prepend(pn).ToList();
            }
        }

        var orderedResult = result.OrderBy(pn => pn.Category).ThenBy(pn => pn.DisplayName).ToList();
        SelectedPerson = orderedResult.FirstOrDefault(pn => pn.Id == currentSelection?.Id)
                         ?? orderedResult.FirstOrDefault();
        return orderedResult;
    }

    private static bool IsMatch(string name, string[] searchTerms)
    {
        foreach (string searchTerm in searchTerms)
        {
            if (!name.Contains(searchTerm))
            {
                return false;
            }
        }

        return true;
    }

    private string GetNormalizedSearchExpression()
    {
        return _stringNormalizer.Normalize(SearchExpression);
    }
}
