﻿using System;
using System.Linq;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickCommands.ContentInserter;
using Eurolook.WordAddIn.BrickCommands.InsertExcelObject;
using Eurolook.WordAddIn.BrickEngine;
using JetBrains.Annotations;
using Range = Microsoft.Office.Interop.Word.Range;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    [UsedImplicitly]
    public class InsertExcelTableCommand : InsertExcelObjectCommandBase<ExcelSheet, InsertExcelObjectSettings>
    {
        protected override bool IncludeTitlePlaceholderDefault { get; set; } = true;
        protected override bool IncludeSourcePlaceholderDefault { get; set; } = true;

        public InsertExcelTableCommand(
            IDocumentManager documentManager,
            IMessageService messageService,
            ISettingsService settingsService,
            IUserDataRepository userDataRepository,
            IBrickRepository brickRepository)
            : base(documentManager, messageService, settingsService, userDataRepository, brickRepository)
        {
            SettingsSaved = true;
            LabelTextAlias = "LabelTableSeq";
            HasItalicLabels = true;
        }

        public string LabelTextAlias { get; set; }

        public bool HasItalicLabels { get; set; }

        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            try
            {
                using var viewModel =
                    new InsertExcelSheetViewModel(DocumentManager.GetActiveDocumentViewModel(), SettingsService)
                    {
                        HasTitlePlaceholder = HasTitlePlaceholder,
                        HasSourcePlaceholder = HasSourcePlaceholder,
                        IncludeTitlePlaceholder = IncludeTitlePlaceholder,
                        IncludeSourcePlaceholder = IncludeSourcePlaceholder,
                    };
                await LoadSettings();
                ApplySettingsOrDefault(viewModel);

                var win = new InsertExcelObjectView(viewModel);

                if (MessageService.ShowDialog(win, true, true) == true)
                {
                    IncludeTitlePlaceholder = viewModel.IncludeTitlePlaceholder;
                    IncludeSourcePlaceholder = viewModel.IncludeSourcePlaceholder;

                    string altText = viewModel.AlternativeText;

                    using (var documentAutomationHelper = new DocumentAutomationHelper(context, DisplayName))
                    {
                        Enum.TryParse(argument, true, out TitleInsertionPosition insertionPosition);
                        var range = InsertExcelObject(
                            viewModel.SelectedObject,
                            context.Selection.Range,
                            insertionPosition,
                            altText);
                        FixScaleTooLarge(range, range.InlineShapes[1]);
                        range?.SelectFirstPlaceholderContentControl(documentAutomationHelper.PreferredViewType);
                    }

                    await SaveSettings(viewModel);
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        protected override string ResolveLabelAlias(Range range)
        {
            if (IsInAgencyChapter(range))
            {
                return "SAR_LabelTableSeqWAgencyChapter";
            }

            return HasChapterNumber(range) ? "LabelTableSeqWChapter" : LabelTextAlias;
        }

        private Range InsertExcelObject(
            ExcelSheet excelRange,
            Range range,
            TitleInsertionPosition insertionPosition,
            string altText)
        {
            if (excelRange == null)
            {
                return null;
            }

            range = range.GetCursorBrickInsertionRange();
            range.HandleVanishingContentControl();
            range.Select();

            string titleStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel("Figure Title", range);
            string bodyStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel("Figure Body", range);
            string sourceStyle = StyleConceptNew.GetNeutralStyleNameBasedOnTextLevel("Figure Source", range);

            var titlePlaceholderInserter = IncludeTitlePlaceholder
                ? new ParagraphInserter(
                    titleStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                        LabelAlias = ResolveLabelAlias(range),
                    },
                    new PlaceholderInserter("Type your title here."))
                : null;

            var excelTableInserter = new ParagraphInserter(
                bodyStyle,
                new ExcelRangeInserter
                {
                    Range = excelRange.Range,
                    AlternativeText = altText,
                });

            var sourcePlaceholderInserter = IncludeSourcePlaceholder
                ? new ParagraphInserter(
                    sourceStyle,
                    new LabelInserter
                    {
                        LocalisedResourceResolver = LocalisedResourceResolver,
                        LabelAlias = "LabelSource",
                        IsItalic = HasItalicLabels,
                        InsertAt = ContentInsertionPosition.Replace,
                    },
                    new TextInserter
                    {
                        Text = ":",
                        IsItalic = HasItalicLabels,
                    },
                    new TextInserter
                    {
                        Text = " ",
                    },
                    new PlaceholderInserter("Type your source here."))
                : null;

            var itemsToInsert = new IContentInserter[]
            {
                titlePlaceholderInserter, excelTableInserter, sourcePlaceholderInserter,
            }.Where(c => c != null).ToArray();

            if (titlePlaceholderInserter != null
                && insertionPosition == TitleInsertionPosition.TitleBelowInsertedContent)
            {
                itemsToInsert.Swap(0, 1);
            }

            itemsToInsert.OfType<ParagraphInserter>().Last().Content.First().InsertAt =
                ContentInsertionPosition.Replace;

            var contentInserter = new BrickContainerInserter(Brick, itemsToInsert);

            var insertionRangeProvider = new RangePositionProvider(range);
            return contentInserter.Insert(insertionRangeProvider);
        }
    }
}
