﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.ViewModels;
using GalaSoft.MvvmLight.Command;
using Microsoft.Office.Interop.Word;
using Color = System.Drawing.Color;
using Range = Microsoft.Office.Interop.Word.Range;
using Rectangle = System.Drawing.Rectangle;

namespace Eurolook.WordAddIn.BrickCommands.CustomBricks
{
    public class CreateCustomBrickViewModel : ValidationViewModelBase
    {
        private readonly IWordApplicationContext _applicationContext;
        private readonly ISettingsService _settingsService;
        private readonly IMessageService _messageService;
        private readonly IEurolookWordDocument _document;
        private string _dialogTitle;
        private string _acronym;
        private string _name;
        private string _description;
        private byte[] _previewBytes;
        private string _contentPreviewTooltip;

        public CreateCustomBrickViewModel(
            IWordApplicationContext applicationContext,
            ISettingsService settingsService,
            IMessageService messageService,
            IEurolookWordDocument document,
            bool isEditMode)
        {
            _applicationContext = applicationContext;
            _settingsService = settingsService;
            _messageService = messageService;
            _document = document;
            CreateCommand = new RelayCommand(Create);
            SaveCommand = new RelayCommand(Save);
            CancelCommand = new RelayCommand(Cancel);
            UpdateContentCommand = new RelayCommand(UpdateContentAndValidate);
            GetHelpCommand = new RelayCommand(() => _settingsService.OpenHelpLink("CreateCustomBrick"));
            Tile = new DummyTileViewModel();
            IsEditMode = isEditMode;
            DialogTitle = isEditMode ? "Edit Custom Brick" : "Create Custom Brick";
            EmptyStageText =
                "You haven’t selected any content for your new brick.\nGo back to your document, select the desired content and click the Update button below.";
            ContentPreviewTooltip =
                "The content of your brick is created from the selection in your Word document.\nTo change the content go back to your document, select the desired content and click the Update button below.";

            ValidationMap = new Dictionary<string, Action>
            {
                { nameof(Acronym), () => ValidateNullOrWhiteSpace(nameof(Acronym), Acronym) },
                { nameof(Name), () => ValidateNullOrWhiteSpace(nameof(Name), Name) },
                { nameof(ContentValidation), ValidateContent },
            };

            if (!isEditMode)
            {
                UpdateTile();
                UpdateContentFromSelection();
            }
        }

        public RelayCommand CreateCommand { get; }
        public RelayCommand SaveCommand { get; }
        public RelayCommand CancelCommand { get; }
        public RelayCommand UpdateContentCommand { get; }
        public RelayCommand GetHelpCommand { get; }
        public Action CreateAction { get; set; }
        public Action UpdateAction { get; set; }
        public Action CancelAction { get; set; }
        public Action CloseAction { get; set; }
        public TileViewModel Tile { get; }
        public bool IsEditMode { get; }
        public string WindowTitle => $"Eurolook - {DialogTitle}";
        public string EmptyStageText { get; }
        public Range Content { get; set; }
        public string ContentValidation { get; set; }
        protected sealed override Dictionary<string, Action> ValidationMap { get; }

        public string DialogTitle
        {
            get => _dialogTitle;
            set => Set(() => DialogTitle, ref _dialogTitle, value);
        }

        public string ContentPreviewTooltip
        {
            get => _contentPreviewTooltip;
            set => Set(() => ContentPreviewTooltip, ref _contentPreviewTooltip, value);
        }

        public int PreviewMaxWidth => 600;

        public byte[] PreviewBytes
        {
            get => _previewBytes;
            set
            {
                Set(() => PreviewBytes, ref _previewBytes, value);
                ContentPreview = value != null ? new BitmapImage().InitAndFreeze(value) : null;
                RaisePropertyChanged(() => ContentPreview);
                RaisePropertyChanged(() => IsContentPreviewVisible);
            }
        }

        public ImageSource ContentPreview { get; private set; }
        public bool IsContentPreviewVisible => ContentPreview != null;

        public string Acronym
        {
            get => _acronym;
            set
            {
                Set(() => Acronym, ref _acronym, value);
                Tile.Acronym = value;
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                Set(() => Name, ref _name, value);
                Tile.Title = value;
            }
        }

        public string Description
        {
            get => _description;
            set
            {
                Set(() => Description, ref _description, value);
                Tile.Description = value;
            }
        }

        private void UpdateTile()
        {
            var selection = GetValidSelection();
            if (selection == null)
            {
                return;
            }

            if (!string.IsNullOrWhiteSpace(selection.Text))
            {
                var text = selection.Text.Trim();
                string invalid = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
                foreach (char c in invalid)
                {
                    text = text.Replace(c.ToString(), " ");
                }

                if (string.IsNullOrEmpty(Acronym))
                {
                    Acronym = text.Substring(0, text.Length >= 3 ? 3 : text.Length);
                }

                if (string.IsNullOrEmpty(Name))
                {
                    Name = text.Substring(0, text.Length >= 12 ? 12 : text.Length);
                }
            }
        }

        private void UpdateContentAndValidate()
        {
            if (!CheckValidSelection(out var selection))
            {
                return;
            }

            if (CheckPageOrSectionBreak(selection))
            {
                return;
            }

            UpdateContentFromSelection();
            UpdateTile();
            Validate(false);
        }

        private bool CheckValidSelection(out Selection selection)
        {
            selection = GetValidSelection();
            if (selection == null)
            {
                ShowDocumentClosedWarning();
                return false;
            }

            return true;
        }

        public bool CheckPageOrSectionBreak(Selection selection)
        {
            if (HasPageBreak(selection) || HasSectionBreak(selection))
            {
                ShowMultiPageWarning();
                return true;
            }

            return false;
        }

        private Selection GetValidSelection()
        {
            if (_applicationContext.IsObjectValid(_document?.Document)
                && _document?.Document != null
                && _applicationContext.IsObjectValid(_document?.Document?.Application)
                && _document?.Document?.Application != null
                && _applicationContext.IsObjectValid(_document?.Document?.Application?.Selection)
                && _document?.Document?.Application.Selection != null)
            {
                return _document?.Document?.Application.Selection;
            }

            return null;
        }

        private void UpdateContentFromSelection()
        {
            var selection = GetValidSelection();
            if (selection == null
                || string.IsNullOrWhiteSpace(selection.Text)
                || (selection.End - selection.Start) <= 0)
            {
                PreviewBytes = null;
                Content = null;
                return;
            }

            // when the selection ends in the middle of a table, select the whole table
            if (selection.Tables.Count > 0)
            {
                var lastTable = selection.Tables[selection.Tables.Count];
                if (lastTable.Range.End > selection.Range.End)
                {
                    selection.Expand(WdUnits.wdTable);
                }
            }

            PreviewBytes = GetPreviewFromMetaBits(out var selectedContent);
            Content = selectedContent;
        }

        private bool HasSectionBreak(Selection selection)
        {
            var range = selection.Range.Duplicate;
            range.Find.Text = "\b";
            return range.Find.Execute();
        }

        private bool HasPageBreak(Selection selection)
        {
            var range = selection.Range.Duplicate;
            range.Find.Text = "\f";
            return range.Find.Execute();
        }

        private byte[] GetPreviewFromMetaBits(out Range selectedContent)
        {
            var selection = GetValidSelection();
            if (selection == null)
            {
                selectedContent = null;
                return null;
            }

            var emfBytes = GetSelectionEmfBytes(selection);
            if (emfBytes == null || emfBytes.Length <= 0)
            {
                selectedContent = null;
                return null;
            }

            Image emfImage;
            using (var emfStream = new MemoryStream(emfBytes))
            {
                emfImage = Image.FromStream(emfStream);
            }

            // draw the EMF into a white bitmap
            var bitmap = new Bitmap(emfImage);
            var graphics = Graphics.FromImage(bitmap);
            graphics.Clear(Color.White);
            graphics.DrawImage(emfImage, 0, 0, emfImage.Width, emfImage.Height);

            // scale the bitmap
            var targetSize = GetScaledTargetSize(emfImage, PreviewMaxWidth, null);
            var scaledBitmap = bitmap.GetThumbnailImage(
            targetSize.Width,
            targetSize.Height,
            ThumbnailCallback,
            IntPtr.Zero);

            // save bitmap bytes
            using (var memoryStream = new MemoryStream())
            {
                scaledBitmap.Save(memoryStream, ImageFormat.Png);
                selectedContent = selection.Range.Duplicate;
                return memoryStream.ToArray();
            }
        }

        private static byte[] GetSelectionEmfBytes(Selection selection)
        {
            var range = selection.Range.Duplicate;
            using (new DocumentAutomationHelper(range))
            {
                var addedParagraph = range.InsertParagraphAfterAndHandleLockedContentControls();
                var bytes = range.EnhMetaFileBits as byte[];
                addedParagraph.Delete();
                return bytes;
            }
        }

        private Rectangle GetScaledTargetSize(Image image, int? maxWidth, int? maxHeight)
        {
            int w, h;
            if (maxWidth != null && image.Width > maxWidth)
            {
                // shrink to the width
                w = maxWidth.Value;
                h = (int)((image.Height * w) / (double)image.Width);
            }
            else if (maxHeight != null && image.Height > maxHeight)
            {
                // shrink to the height
                h = maxHeight.Value;
                w = (int)((image.Width * h) / (double)image.Height);
            }
            else
            {
                w = image.Width;
                h = image.Height;
            }

            return new Rectangle(0, 0, w, h);
        }

        private bool ThumbnailCallback()
        {
            return false;
        }

        private void ValidateContent()
        {
            var propertyName = nameof(ContentValidation);
            if ((!IsEditMode || PreviewBytes == null) && Content == null)
            {
                Errors[propertyName] = ErrorIncomplete;
            }
            else
            {
                Errors.Remove(propertyName);
            }
        }

        private void Save()
        {
            if (Validate() && CheckValidSelection(out _))
            {
                UpdateAction?.Invoke();
                CloseAction?.Invoke();
            }
        }

        private void Create()
        {
            if (Validate() && CheckValidSelection(out _))
            {
                CreateAction?.Invoke();
                CloseAction?.Invoke();
            }
        }

        private void Cancel()
        {
            CancelAction?.Invoke();
            CloseAction?.Invoke();
        }

        private void ShowDocumentClosedWarning()
        {
            var close = _messageService.ShowMessageWindow(
                new MessageViewModel
                {
                    IsShowNoButton = true,
                    IsShowCancelButton = false,
                    YesButtonText = "Yes",
                    NoButtonText = "No",
                    WindowTitle = $"Eurolook - {DialogTitle}",
                    Title = "Document Closed",
                    Message = "Your document has been closed. Do you want to close this window too?",
                },
                this);

            if (close == true)
            {
                CloseAction?.Invoke();
            }
        }

        private void ShowMultiPageWarning()
        {
            _messageService.ShowMessageWindow(
                new MessageViewModel
                {
                    IsShowNoButton = false,
                    IsShowCancelButton = false,
                    YesButtonText = "Ok",
                    WindowTitle = $"Eurolook - {DialogTitle}",
                    Title = "Page Break Warning",
                    Message = "You cannot create Custom Bricks containing page or section breaks.",
                },
                this);
        }
    }
}
