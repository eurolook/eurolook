using Eurolook.AddIn.Common.ViewModels;

namespace Eurolook.WordAddIn.BrickCommands.CustomBricks
{
    public class DummyTileViewModel : TileViewModel
    {
        public override int UIPositionIndex => 1;

        //public override void UpdateCheckmark(List<BrickContainer> brickContainers)
        //{
        //}
    }
}
