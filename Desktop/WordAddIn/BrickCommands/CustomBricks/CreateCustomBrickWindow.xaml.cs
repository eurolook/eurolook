using System.Windows;

namespace Eurolook.WordAddIn.BrickCommands.CustomBricks
{
    public partial class CreateCustomBrickWindow : Window
    {
        public CreateCustomBrickWindow(CreateCustomBrickViewModel viewModel)
        {
            InitializeComponent();
            viewModel.CloseAction = Close;
            DataContext = viewModel;
        }
    }
}
