﻿using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.Database;
using Eurolook.WordAddIn.Messages;
using GalaSoft.MvvmLight.Messaging;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.BrickCommands.CustomBricks
{
    [UsedImplicitly]
    public class CreateCustomBrickCommand : EditCommandBase
    {
        private readonly ColorManager _colorManager;
        private readonly ISettingsService _settingsService;
        private readonly IMessageService _messageService;
        private readonly IWordBrickRepository _brickRepository;
        private readonly IWordApplicationContext _applicationContext;

        public CreateCustomBrickCommand(
            ColorManager colorManager,
            ISettingsService settingsService,
            IMessageService messageService,
            IWordBrickRepository brickRepository,
            IWordApplicationContext applicationContext)
        {
            _colorManager = colorManager;
            _settingsService = settingsService;
            _messageService = messageService;
            _brickRepository = brickRepository;
            _applicationContext = applicationContext;
        }

        protected override Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            var viewModel = new CreateCustomBrickViewModel(_applicationContext, _settingsService, _messageService, context.EurolookDocument, false);
            viewModel.Tile.ColorBrush = _colorManager.CurrentColorScheme.PrimaryColor.ToBrush();
            viewModel.CreateAction = () => Create(context.EurolookDocument, viewModel);
            if (!viewModel.CheckPageOrSectionBreak(context.Application.Selection))
            {
                _messageService.ShowSingleton(new CreateCustomBrickWindow(viewModel), false, false, true);
            }

            return Task.CompletedTask;
        }

        public void Create(IEurolookWordDocument eurolookDocument, CreateCustomBrickViewModel viewModel)
        {
            var buildingBlockBytes = BuildingBlockBrickInstance.CreateBuildingBlock(
                eurolookDocument.Document.Application,
                eurolookDocument.DocumentModel.Template,
                viewModel.Content);

            _brickRepository.CreateCustomBrick(viewModel, eurolookDocument.DocumentModel, buildingBlockBytes);
            Messenger.Default.Send(new CustomBricksModifiedMessage());
        }
    }
}
