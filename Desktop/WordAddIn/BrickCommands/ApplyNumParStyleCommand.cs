﻿using Eurolook.Common.Log;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using JetBrains.Annotations;
using Range = Microsoft.Office.Interop.Word.Range;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.BrickCommands
{
    public class ApplyNumParStyleCommand : ApplyHeadingStyleCommand
    {
        protected override async Task OnExecuteAsync(IBrickExecutionContext context, string argument)
        {
            using (new DocumentAutomationHelper(context, $"Apply {argument} Brick"))
            {
                string styleName = GetStyleName(argument);
                string placeholderText = GetPlaceholderText(argument, "Type your numbered paragraph here.");

                var range = context.Selection.Range.Duplicate;
                var document = range.Document;

                var newStyleNameInfo = StyleNameInfo.FromStyleName(styleName);
                if (newStyleNameInfo == null)
                {
                    this.LogError($"Style {styleName} does not have a valid naming pattern.");
                    return;
                }

                var currentStyle = LanguageIndependentStyle.FromRange(range.Paragraphs.First.Range);

                // in case that
                // a) ApplyHeadingStyleCommand is used on an existing heading and
                // b) the given style name has no level defined
                // the text level of the previous heading is kept
                if (StyleConceptNew.IsHeadingStyle(currentStyle) && newStyleNameInfo.TextLevel == 0)
                {
                    var currentStyleNameInfo = StyleNameInfo.FromStyleName(currentStyle.NameNeutral);
                    int textLevel = currentStyleNameInfo.TextLevel;
                    newStyleNameInfo = new StyleNameInfo(
                        newStyleNameInfo.Category,
                        textLevel,
                        newStyleNameInfo.ListLevel);
                }

                string styleNameNeutral;
                if (newStyleNameInfo.TextLevel == 0)
                {
                    styleNameNeutral = GetNeutralStyleNameBasedOnTextLevel(styleName, range);
                }
                else
                {
                    newStyleNameInfo =
                        StyleConceptNew.GetAvailableStyleWithMaximumTextLevel(newStyleNameInfo, document);
                    if (newStyleNameInfo == null)
                    {
                        this.LogError($"Style with maximum text level for style {styleName} not found in document");
                        return;
                    }

                    var style = LanguageIndependentStyle.FromNeutralStyleName(newStyleNameInfo.StyleName, document);
                    styleNameNeutral = style?.NameNeutral;
                }

                if (styleNameNeutral == null)
                {
                    this.LogError($"Style to be applied for style {styleName} not found in document.");
                    return;
                }

                range.HandleVanishingContentControl();
                await ApplyStyleWithPlaceholder(range, styleNameNeutral, placeholderText);
            }
        }

        [CanBeNull]
        private static string GetNeutralStyleNameBasedOnTextLevel(
            string styleCategory,
            Range range)
        {
            var document = range.Document;
            int currentTextLevel = RetrieveTextLevelForNumPar(range);

            var styleNameInfo = new StyleNameInfo(styleCategory, currentTextLevel);
            styleNameInfo = StyleConceptNew.GetAvailableStyleWithMaximumTextLevel(styleNameInfo, document);
            if (styleNameInfo != null)
            {
                var style = LanguageIndependentStyle.FromNeutralStyleName(styleNameInfo.StyleName, document);
                return style?.NameNeutral;
            }

            return null;
        }

        private static int RetrieveTextLevelForNumPar(Range range)
        {
            if (range == null)
            {
                return 0;
            }

            var paragraph = range.Paragraphs.First;
            var previousParagraph = range.Paragraphs.First.Previous();
            while (previousParagraph != null)
            {
                if (previousParagraph.Range.HasTables()
                    && !StyleConceptNew.IsInSameTableCell(paragraph, previousParagraph))
                {
                    previousParagraph = previousParagraph.Range.Tables[1].Range.Paragraphs.First;
                }

                // check if the previous paragraph is a heading and return it's text level
                var style = LanguageIndependentStyle.FromParagraph(previousParagraph);
                if (StyleConceptNew.IsHeadingStyle(style))
                {
                    var styleInfo = StyleNameInfo.FromStyleName(style.NameNeutral);
                    switch (styleInfo.Category)
                    {
                        case "NumPar":
                            return styleInfo.TextLevel;
                        case "Article":
                            return 2;
                        default:
                            return styleInfo.TextLevel + 1;
                    }
                }

                paragraph = previousParagraph;
                previousParagraph = previousParagraph.Previous();
            }

            return 0;
        }
    }
}
