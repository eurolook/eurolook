﻿using Eurolook.AddIn.Common;

namespace Eurolook.WordAddIn;

public interface IWordAddinContext : IAddinContext
{
    /// <summary>
    /// Gets the full path of the folder where Eurolook Word template files are located.
    /// </summary>
    string GlobalTemplateFolder { get; }

    string GlobalTemplatePath { get; }

    string GlobalAddInPath { get; }

    /// <summary>
    /// Gets the full path of to a Word template file containing customizations of Word's Quick Access Toolbar.
    /// </summary>
    string QuickAccessToolbarTemplatePath { get; }
}
