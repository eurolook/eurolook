using Eurolook.AddIn.Common;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.Extensions
{
    public static class EurolookDocumentExtensions
    {
        public static Document GetDocument(this IEurolookDocument eurolookDocument)
        {
            return ((IEurolookWordDocument)eurolookDocument).Document;
        }

        [CanBeNull]
        public static IEurolookWordDocument AsEurolookWordDocument(this IEurolookDocument eurolookDocument)
        {
            return eurolookDocument as IEurolookWordDocument;
        }
    }
}
