﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Xml.Linq;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.OfficeNotificationBar;
using JetBrains.Annotations;
using Microsoft.CSharp.RuntimeBinder;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.Extensions
{
    public static class DocumentExtensions
    {
        public static IEnumerable<Range> AllStoryRanges(this Document document)
        {
            // Capture all story ranges upfront; if removing the local variable and enumerating as part of
            // the foreach loop, Word might return a range twice (this was noticed with the primary footer).
            var storyRanges = document.StoryRanges.OfType<Range>().ToList();
            foreach (var storyRange in storyRanges)
            {
                var range = storyRange;
                yield return range;

                while ((range = range.NextStoryRange) != null)
                {
                    yield return range;
                }
            }
        }

        public static void ApplyToAllStoryRanges(this Document document, Action<Range> action)
        {
            // Capture all story ranges upfront; if removing the local variable and enumerating as part of
            // the foreach loop, Word might return a range twice (this was noticed with the primary footer).
            var storyRanges = document.StoryRanges.OfType<Range>().ToList();
            foreach (var storyRange in storyRanges)
            {
                var range = storyRange;
                action(range);

                while ((range = range.NextStoryRange) != null)
                {
                    action(range);
                }
            }
        }

        internal static IEnumerable<BrickContainer> LoadAllBricks(
            this Document document,
            IBrickRepository brickRepository)
        {
            var allContentControls = document.GetAllContentControls().ToArray();
            return document.LoadAllBricksOf(brickRepository, allContentControls);
        }

        internal static IEnumerable<BrickContainer> LoadAllBricksOf(
            this Document document,
            IBrickRepository brickRepository,
            params ContentControl[] controls)
        {
            var result = new List<BrickContainer>();
            foreach (var cc in controls)
            {
                var container = cc.GetBrickContainer();
                if (container != null)
                {
                    container.Brick = brickRepository.GetBrick(container.Id);
                    result.Add(container);
                }
            }

            return result;
        }

        public static List<Bookmark> BookmarksIncludingHidden(this Document document)
        {
            var bookmarks = document.Bookmarks;
            var showHidden = bookmarks.ShowHidden;
            try
            {
                if (showHidden)
                {
                    // toggle might be necessary
                    bookmarks.ShowHidden = false;
                }

                bookmarks.ShowHidden = true;
                return bookmarks.OfType<Bookmark>().ToList();
            }
            finally
            {
                bookmarks.ShowHidden = showHidden;
            }
        }

        public static bool GetIsInAutosave(this Document document)
        {
            // use dynamic and try whether the `IsInAutosave` property (introduced with Word 2013) is available
            dynamic doc = document;
            try
            {
                return doc.IsInAutosave;
            }
            catch (RuntimeBinderException)
            {
                // silent catch: Property IsInAutosave does not exist in Word 2010 or older
                return false;
            }
        }

        public static bool IsModernCommentActive(this Document document)
        {
            try
            {
                _ = document.TrackRevisions;
                return false;
            }
            catch (COMException e) when ((uint)e.HResult == 0x800a11fd)
            {
                // comment card is selected
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsDecorativeAvailable(this Document document)
        {
            dynamic doc = document;
            try
            {
                _ = doc.Range().ShapeRange.Decorative;
                return true;
            }
            catch (RuntimeBinderException)
            {
                // silent catch: Property Decorative does not exist in Word 2016 or older
                return false;
            }
            catch (Exception)
            {
                // RuntimeBinderException would have been thrown even if the ShapeRange is empty.
                // This means that the binding is in fact available.
                return true;
            }
        }

        public static void ChangeDocumentToAvoidUndoRecordIssue(this Document document)
        {
            // Word crashes when undo/redo stack is empty while using find.Execute()
            // in a custom undo record (cf. ECADOCS-205)
            // --> Adding and removing a bookmark is a safe way that will not modify the contents of the document
            // (cf. ECADOCS-542)
            string bookmarkId = "_" + Guid.NewGuid().ToString("N");
            var bookmark = document.Bookmarks.Add(bookmarkId, document.Range());
            bookmark.Delete();
        }

        public static void BringToForeground(this Document document)
        {
            var hWnd = WordWindowFinder.GetWordWindowHandle(document?.Windows[1]);
            SafeNativeMethods.SetForegroundWindow(hWnd);
        }

        public static void ReplaceCustomXml(this Document document, string rootName, XDocument newContent)
        {
            document.CustomXMLParts.ReplaceCustomXml(rootName, newContent);
        }

        [CanBeNull]
        public static string GetCustomXml(this Document document, string rootName, string rootNamespaceUri = null)
        {
            return document.CustomXMLParts.GetCustomXml(rootName, rootNamespaceUri);
        }

        public static CustomXMLPart GetCustomXmlPart(this Document document, string partId)
        {
            return document.CustomXMLParts.GetCustomXmlPart(partId);
        }

        public static IEnumerable<CustomXMLPart> GetCustomXmlParts(this Document document, string rootName)
        {
            return document.CustomXMLParts.GetCustomXmlParts(rootName);
        }

        public static XDocument GetCustomXmlAsXDocument(this Document document, string rootName)
        {
            return document.CustomXMLParts.GetCustomXmlAsXDocument(rootName);
        }
    }
}
