﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using Microsoft.Office.Interop.Word;

namespace Eurolook.DocumentProcessing.Extensions
{
    public static class PageExtensions
    {
        public static Bitmap AsBitmap(this Page page)
        {
            return AsBitmap(page, Color.White);
        }

        public static Bitmap AsBitmap(this Page page, Color backgroundColor)
        {
            using (var ms = new MemoryStream((byte[])page.EnhMetaFileBits))
            {
                using (var currentImage = Image.FromStream(ms))
                {
                    var bitmap = new Bitmap(currentImage.Width, currentImage.Height);

                    bitmap.SetResolution(currentImage.HorizontalResolution, currentImage.VerticalResolution);

                    using (var g = Graphics.FromImage(bitmap))
                    {
                        g.SmoothingMode = SmoothingMode.HighQuality;
                        g.CompositingQuality = CompositingQuality.HighQuality;
                        g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                        g.Clear(backgroundColor);
                        g.DrawImage(currentImage, 0, 0);
                    }

                    return bitmap;
                }
            }
        }
    }
}
