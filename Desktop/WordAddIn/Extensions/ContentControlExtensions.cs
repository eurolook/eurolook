﻿using System.Linq;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.Extensions
{
    public static class ContentControlExtensions
    {
        public static void DeleteBodyBrick(this ContentControl contentControl)
        {
            var range = contentControl.Range;

            contentControl.Delete();
            range.Text = "";

            // ReSharper disable once UseIndexedProperty
            range.ParagraphFormat.set_Style(WdBuiltinStyle.wdStyleNormal);
        }

        public static void DeleteBrick(this ContentControl contentControl)
        {
            if (contentControl.IsInDeletedRevision())
            {
                return;
            }

            var range = contentControl.Range;

            // It is an inline content control with other text in the same paragraph,
            // so just delete the content control, leave the paragraph alone.
            // EUROLOOK-1289
            if (!contentControl.IsBlockLevel() && range.Paragraphs.Count == 1 &&
                range.Paragraphs.First.Range.Text?.Trim() != range.Text?.Trim())
            {
                contentControl.Delete(true);
                return;
            }

            if (contentControl.Tag.Contains("CCOnly"))
            {
                range = contentControl.GetControlRange();
            }
            else
            {
                range.Expand(WdUnits.wdParagraph);

                // check if there is an empty paragraph at the end of the content control
                // which might not yet be fully included in our range
                var rangeEnd = range.Duplicate;
                rangeEnd.Collapse(WdCollapseDirection.wdCollapseEnd);
                if (rangeEnd.ParentContentControl.IsSame(contentControl))
                {
                    // if still inside the content control then extend the range
                    range.End = rangeEnd.Paragraphs.First.Range.End;
                }
            }

            foreach (var cc in range.ContentControls.OfType<ContentControl>())
            {
                cc.LockContents = false;
            }

            range.DeleteRangeAndContent();

            var lastSectionHandler = new LastSectionHandler();
            lastSectionHandler.RemoveEmptyLastSection(range);
        }
    }
}
