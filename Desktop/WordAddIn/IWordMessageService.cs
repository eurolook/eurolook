﻿using Eurolook.AddIn.Common;
using Eurolook.OfficeNotificationBar;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn
{
    public interface IWordMessageService : IMessageService
    {
        void ShowNotificationBar(INotificationBarContent notificationBarContent, Window window);

        void CloseNotificationBar(Window window);

        bool? ShowDialogNextToRange(System.Windows.Window window, Range range, int distanceFromRange);
    }
}
