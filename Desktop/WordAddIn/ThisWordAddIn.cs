﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;
using Autofac;
using Eurolook.AddIn.Common.AddIn;
using Eurolook.AddIn.Common.ProfileInitializer;
using Eurolook.AddIn.Common.Ribbon;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.AddIn;
using Eurolook.WordAddIn.Autofac;
using Eurolook.WordAddIn.HotKeys;
using Eurolook.WordAddIn.Ribbon;
using Eurolook.WordAddIn.TaskPane;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using Application = Microsoft.Office.Interop.Word.Application;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn
{
    [ComVisible(true)]
    [ProgId("Eurolook.WordAddIn.10")]
    [Guid("8D26C024-DAEE-4B32-ABEF-20B7769979AE")]
    public sealed class ThisWordAddIn : AddInBase, ICanLog, IOfficeAddIn, ITaskPaneFactory, IDisposable
    {
        private static readonly string TaskPaneTitle = " ";

        private ComAddInAutomationService _comAddInAutomationService;
        private IContainer _container;
        private IRibbonCallbackHandler _ribbonCallbackHandler;
        private ITaskPaneManager _taskPaneManager;
        private IWordRibbonService _ribbonService;
        private IWordAddinContext _addinContext;
        private IDocumentThemesManager _documentThemesManager;

        public ThisWordAddIn(Application application)
        {
            Application = application;
        }

        ~ThisWordAddIn()
        {
            Dispose(false);
        }

        public Application Application { get; }

        public IRibbonCallbackHandler RibbonCallbackHandler =>
            _ribbonCallbackHandler ??= _container?.Resolve<IRibbonCallbackHandler>();

        public IRibbonService RibbonService => _ribbonService ??= _container?.Resolve<IWordRibbonService>();

        private ITaskPaneManager TaskPaneManager => _taskPaneManager ??= _container?.Resolve<ITaskPaneManager>();

        private IWordAddinContext AddinContext => _addinContext ??= _container?.Resolve<IWordAddinContext>();

        private IDocumentThemesManager DocumentThemesManager =>
            _documentThemesManager ??= _container?.Resolve<IDocumentThemesManager>();

        private ICTPFactory TaskPaneFactory { get; set; }

        public IContainer SetupContainer()
        {
            _container = AutofacRegistration.SetupContainer(this);
            this.LogDebug("Dependency injection container set up.");
            return _container;
        }

        #region Addin Events

        public override void OnStartup(object application)
        {
            try
            {
                this.LogDebug("OnStartup started.");
                AsyncHelper.EnsureSynchronizationContext();

                MessageFilter.Register();

                if (application is not Application _)
                {
                    MessageBox.Show("Add-in must be loaded into Microsoft Word.");
                    return;
                }

                if (System.Windows.Application.Current != null)
                {
                    System.Windows.Application.Current.Resources.MergedDictionaries.Add(
                        new ResourceDictionary
                        {
                            Source = new Uri(
                                "pack://application:,,,/Eurolook.WordAddIn;component/Views/TaskPaneResources.xaml",
                                UriKind.Absolute),
                        });
                }

                if (_container.IsRegistered<IWordOptionsManager>())
                {
                    var registryManager = _container.Resolve<IWordOptionsManager>();
                    registryManager.ApplyAllOptions();
                    this.LogDebug("Word options applied.");
                }
            }
            catch (Exception e)
            {
                this.LogError(e);
            }
        }

        public override async Task OnStartupLongRunningAsync(IProfileInitializer profileInitializer)
        {
            try
            {
                // Clearing disabled add-ins might only take effect after Word is relaunched
                AddinContext?.ClearDisabledAddins();
                await _container.Resolve<IWordRibbonService>().TryLoadGlobalAddInAsync();
                await _container.Resolve<IWordRibbonService>().TryLoadQuickAccessToolbarAsync();

                DocumentThemesManager.InstallOfficeThemes();
            }
            catch (Exception ex)
            {
                this.LogError("Error on startup", ex);
            }
        }

        public override void OnShutdown()
        {
            Dispose();
            MessageFilter.Unregister();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        [SuppressMessage("ReSharper", "UnusedParameter.Local", Justification = "This method signature is according to the IDisposable pattern.")]
        private void Dispose(bool disposing)
        {
            this.LogDebug("Shutting down Eurolook");

            if (TaskPaneManager != null)
            {
                foreach (var customTaskPane in TaskPaneManager.TaskPanes)
                {
                    customTaskPane?.Dispose();
                }
            }

            // clean up here
            _comAddInAutomationService?.Dispose();

            // make sure none of the Eurolook templates pops up a message to be saved
            foreach (var template in
                Application.Templates.OfType<Template>()
                           .Where(t => t.Name.StartsWith("EL_Brick", StringComparison.InvariantCultureIgnoreCase)
                                       || t.Name.StartsWith("EurolookQuickAccessToolbar", StringComparison.InvariantCultureIgnoreCase)
                                       || t.Name.StartsWith("EurolookGlobalAddin", StringComparison.InvariantCultureIgnoreCase)
                                       || Guid.TryParse(Path.GetFileNameWithoutExtension(t.Name), out _)))
            {
                if (!template.Saved)
                {
                    template.Saved = true;
                }
            }

            // make sure that no temporarily added add-ins remain loaded
            foreach (var addIn in Application.AddIns.OfType<Microsoft.Office.Interop.Word.AddIn>())
            {
                if (addIn.Name.StartsWith("EL_Brick", StringComparison.InvariantCultureIgnoreCase)
                    || addIn.Name.StartsWith("EurolookQuickAccessToolbar", StringComparison.InvariantCultureIgnoreCase)
                    || addIn.Name.StartsWith("EurolookGlobalAddin", StringComparison.InvariantCultureIgnoreCase)
                    || Guid.TryParse(Path.GetFileNameWithoutExtension(addIn.Name), out _))
                {
                    addIn.Delete();
                }
            }

            AddinContext?.ClearDisabledAddins();
        }

        public override void OnTaskPaneFactoryAvailable(ICTPFactory ctpFactoryInst)
        {
            AsyncHelper.EnsureSynchronizationContext();

            TaskPaneFactory = ctpFactoryInst;

            // IMPORTANT: This method is in a call chain initiated from
            //   the ICustomTaskPaneConsumer.CTPFactoryAvailable(Office.ICTPFactory ctpFactoryInst) callback.
            //   The callback happens while Word is displaying its splash screen with a "Processing..." message.
            //   No long-running task or other heavy operations such a database access should be done here.
            if (!Application.IsProtectedView())
            {
                TaskPaneManager.GetOrCreateTaskPane(Application.GetActiveWindow());
            }
        }

        #endregion

        public override object RequestComAddInAutomationService()
        {
            if (_comAddInAutomationService == null)
            {
                var hotKeyManager = _container.Resolve<IHotKeyManager>();
                _comAddInAutomationService = new ComAddInAutomationService(hotKeyManager);
            }

            return _comAddInAutomationService;
        }

        #region ITaskPaneFactory Members

        CustomTaskPane ITaskPaneFactory.CreateCustomTaskPane(Microsoft.Office.Interop.Word.Window window)
        {
            AsyncHelper.EnsureSynchronizationContext();

            if (!((ITaskPaneFactory)this).IsReady)
            {
                return null;
            }

            return window != null
                ? TaskPaneFactory.CreateCTP(ComIdentifiers.TaskPaneContentControl.ProgId, TaskPaneTitle, window)
                : TaskPaneFactory.CreateCTP(ComIdentifiers.TaskPaneContentControl.ProgId, TaskPaneTitle);
        }

        bool ITaskPaneFactory.IsReady
        {
            get { return TaskPaneFactory != null; }
        }

        #endregion
    }
}
