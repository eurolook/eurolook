using Eurolook.AddIn.Common.Authors;
using Eurolook.Data.Models;
using Eurolook.Data.Xml;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.DocumentCreation;

namespace Eurolook.WordAddIn.Authors
{
    public abstract class BaseAuthorChangeExecutor
    {
        protected BaseAuthorChangeExecutor(IAuthorDataLoader authorDataLoader, IAuthorManager authorManager)
        {
            AuthorDataLoader = authorDataLoader;
            AuthorManager = authorManager;
        }

        protected AuthorCustomXmlCreationInfo CreateAuthorCustomXmlCreationInfo(AuthorRoleMapping authorRoleMapping, Language language)
        {
            AuthorDataLoader.LoadCompleteAuthorData(authorRoleMapping, language);

            var author = authorRoleMapping.Author;
            var jobAssignment = authorRoleMapping.JobAssignment ?? (IJobAssignment)author;

            return new AuthorCustomXmlCreationInfo(author, jobAssignment, language);
        }

        protected IAuthorDataLoader AuthorDataLoader { get; }

        protected IAuthorManager AuthorManager { get; }
    }
}
