using System.Windows;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;

namespace Eurolook.WordAddIn.Authors
{
    public partial class UpdateDocumentAuthorsWindow : ICanLog
    {
        public UpdateDocumentAuthorsWindow(DialogViewModelBase viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
            Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            var viewModel = DataContext as UpdateDocumentAuthorsViewModel;
            viewModel?.LoadAsync().FireAndForgetSafeAsync(this.LogError);
        }
    }
}
