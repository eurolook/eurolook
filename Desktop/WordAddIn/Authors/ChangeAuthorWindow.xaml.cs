using Eurolook.Common.Log;

namespace Eurolook.WordAddIn.Authors
{
    public partial class ChangeAuthorWindow : ICanLog
    {
        public ChangeAuthorViewModel ViewModel { get; set; }

        public ChangeAuthorWindow()
        {
            InitializeComponent();
        }

        public ChangeAuthorWindow(ChangeAuthorViewModel viewModel)
        {
            InitializeComponent();
            ViewModel = viewModel;
            DataContext = ViewModel;
        }
    }
}
