using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Authors;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.DocumentProcessing;
using GalaSoft.MvvmLight.CommandWpf;
using Microsoft.Office.Interop.Word;
using Language = Eurolook.Data.Models.Language;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.Authors
{
    public class UpdateDocumentAuthorsViewModel : DialogViewModelBase
    {
        private readonly IAuthorManager _authorManager;
        private readonly IAuthorDataLoader _authorDataLoader;
        // ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
        private readonly ISettingsService _settingsService;
        private readonly Document _document;
        private readonly Language _language;
        private bool _isLoaded;
        private string _warning;

        public UpdateDocumentAuthorsViewModel(
            IAuthorManager authorManager,
            IAuthorDataLoader authorDataLoader,
            ISettingsService settingsService,
            Document document,
            Language language)
        {
            _authorManager = authorManager;
            _authorDataLoader = authorDataLoader;
            _settingsService = settingsService;
            _document = document;
            _language = language;

            AuthorUpdateInfos = new ObservableCollection<AuthorUpdateInfo>();
            GetHelpCommand = new RelayCommand(() => _settingsService.OpenHelpLink("UpdateAuthor"));
        }

        public override string Title => "Update Document Authors";

        public bool IsLoaded
        {
            get { return _isLoaded; }
            set { Set(() => IsLoaded, ref _isLoaded, value); }
        }

        public string Warning
        {
            get { return _warning; }
            set { Set(() => Warning, ref _warning, value); }
        }

        public ObservableCollection<AuthorUpdateInfo> AuthorUpdateInfos { get; set; }

        public RelayCommand GetHelpCommand { get; }

        public async Task LoadAsync()
        {
            await Task.Run(Load);
        }

        private async Task Load()
        {
            var authorInfos = new List<AuthorUpdateInfo>();

            var authorCustomXmlStoreInfos = _authorManager.GetAuthorCustomXmlStoreInfos(_document).ToList();
            foreach (var authorCustomXmlStoreInfo in authorCustomXmlStoreInfos)
            {
                var authorInfo = await _authorDataLoader.GetAuthorInfoFromDbOrServerAsync(authorCustomXmlStoreInfo, _language);
                authorInfos.Add(authorInfo);
            }

            Execute.OnUiThread(() => UpdateUi(authorInfos.OrderBy(ai => ai.DisplayName).ToList()));
        }

        private void UpdateUi(List<AuthorUpdateInfo> authorInfos)
        {
            AuthorUpdateInfos.Clear();
            foreach (var authorInfo in authorInfos)
            {
                AuthorUpdateInfos.Add(authorInfo);
            }

            IsLoaded = true;
        }
    }
}
