using System.Collections.Generic;
using Eurolook.AddIn.Common.Authors;
using Eurolook.Data.Xml;
using Eurolook.DocumentProcessing.DocumentCreation;
using Eurolook.WordAddIn.BrickEngine;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.Authors
{
    public interface IAuthorChangeExecutor
    {
        void ChangeDocumentAuthor(
            IBrickExecutionContext context,
            IEnumerable<AuthorRoleViewModel> authorRoleViewModels);

        void ChangeBrickAuthors(
            IBrickExecutionContext context,
            IEnumerable<AuthorRoleViewModel> authorRoleViewModels,
            ContentControl selectedContentControl);

        bool CanHandle(IEnumerable<AuthorCustomXml> authorCustomXmlDocs);
    }

    public interface IAuthorChangeExecutorForCurrentDocuments
    {
        void ChangeDocumentAuthor(IBrickExecutionContext context, List<AuthorRoleMapping> authorRoleMappings, bool updateSignerRole);
    }
}
