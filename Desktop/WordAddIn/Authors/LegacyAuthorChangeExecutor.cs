using System.Collections.Generic;
using System.Linq;
using Eurolook.AddIn.Common.Authors;
using Eurolook.AddIn.Common.Database;
using Eurolook.Common.Extensions;
using Eurolook.Data.Models;
using Eurolook.Data.Xml;
using Eurolook.DocumentProcessing;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.Extensions;
using JetBrains.Annotations;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.Authors
{
    [UsedImplicitly]
    public class LegacyAuthorChangeExecutor : BaseAuthorChangeExecutor, IAuthorChangeExecutor
    {
        private readonly IBrickRepository _brickRepository;

        public LegacyAuthorChangeExecutor(
            IAuthorDataLoader authorDataLoader,
            IAuthorManager authorManager,
            IBrickRepository brickRepository)
            : base(authorDataLoader, authorManager)
        {
            _brickRepository = brickRepository;
        }

        public void ChangeDocumentAuthor(
            IBrickExecutionContext context,
            IEnumerable<AuthorRoleViewModel> authorRoleViewModels)
        {
            var document = context.Document;
            var eurolookDocument = context.EurolookDocument;

            CustomXMLPart authorCustomXmlPart;
            var authorRoleViewModel = authorRoleViewModels.First();
            if (authorRoleViewModel.IsModified)
            {
                var authorCustomXmlCreationInfo = CreateAuthorCustomXmlCreationInfo(
                    authorRoleViewModel.ToAuthorRoleMapping(),
                    eurolookDocument.Language);
                authorCustomXmlCreationInfo.IsMainAuthor = true;

                var authorCustomXml = new AuthorCustomXml(authorCustomXmlCreationInfo);
                authorCustomXmlPart = document.GetCustomXmlPart(authorRoleViewModel.CustomXmlStoreId);
                AuthorManager.ReplaceAuthorXmlPart(authorCustomXmlPart, authorCustomXml);
            }
            else
            {
                authorCustomXmlPart = document.GetCustomXmlPart(authorRoleViewModel.CustomXmlStoreId);
            }

            var brickContainers = document.LoadAllBricks(_brickRepository).ToList();
            UpdateBricksAndCleanup(context, brickContainers, authorCustomXmlPart);
        }

        public void ChangeBrickAuthors(
            IBrickExecutionContext context,
            IEnumerable<AuthorRoleViewModel> authorRoleViewModels,
            ContentControl selectedContentControl)
        {
            var document = context.Document;
            var eurolookDocument = context.EurolookDocument;

            CustomXMLPart authorCustomXmlPart;
            var authorRoleViewModel = authorRoleViewModels.First();
            if (authorRoleViewModel.IsModified)
            {
                var authorCustomXmlCreationInfo = CreateAuthorCustomXmlCreationInfo(
                    authorRoleViewModel.ToAuthorRoleMapping(),
                    eurolookDocument.Language);
                authorCustomXmlCreationInfo.ContentControlTitle = selectedContentControl.Title;

                var authorCustomXml = new AuthorCustomXml(authorCustomXmlCreationInfo);
                authorCustomXmlPart = AuthorManager.AddAuthorXmlPart(document, authorCustomXml);
            }
            else
            {
                authorCustomXmlPart = document.GetCustomXmlPart(authorRoleViewModel.CustomXmlStoreId);
            }

            var brickContainers = document.LoadAllBricksOf(_brickRepository, selectedContentControl).ToList();
            UpdateBricksAndCleanup(context, brickContainers, authorCustomXmlPart);
        }

        public bool CanHandle(IEnumerable<AuthorCustomXml> authorCustomXmlDocs)
        {
            return authorCustomXmlDocs.All(xml => xml.AuthorRoleId == null && xml.OriginalAuthorRoleId == null);
        }

        private void UpdateBricksAndCleanup(
            IBrickExecutionContext context,
            List<BrickContainer> brickContainers,
            CustomXMLPart authorCustomXmlPart)
        {
            context.AuthorInformation = AuthorManager.GenerateAuthorInformation(
                authorCustomXmlPart.CreateList(),
                context.EurolookProperties);

            var authorBricks = brickContainers.Where(b => AuthorManager.GetAuthorXmlMappings(b.ContentControl).Any());
            context.EurolookDocument.BrickEngine.RecreateDynamicBricks(context, authorBricks);
            UpdateStaticBricks(brickContainers, authorCustomXmlPart);

            AuthorManager.CleanAuthorXmlParts(context.Document);
        }

        private void UpdateStaticBricks(
            List<BrickContainer> brickContainers,
            CustomXMLPart newCustomXmlPart)
        {
            // update the mappings
            foreach (var container in brickContainers.Where(c => c.Brick is ContentBrick && !(c.Brick is DynamicBrick)))
            {
                foreach (var mapping in AuthorManager.GetAuthorXmlMappings(container.ContentControl))
                {
                    mapping.SetMapping(mapping.XPath, Source: newCustomXmlPart);
                }
            }
        }
    }
}
