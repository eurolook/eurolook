﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Authors;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.SystemConfiguration;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.Data.Models;
using Eurolook.Data.Models.AuthorRoles;
using Eurolook.Data.Xml;
using Eurolook.WordAddIn.Wizard;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;

namespace Eurolook.WordAddIn.Authors
{
    public class ChangeAuthorViewModel : DialogViewModelBase
    {
        private readonly IUserDataRepository _userDataRepository;
        private readonly IAuthorRepository _authorRepository;
        private readonly IAuthorRoleViewModelValidator _authorRoleViewModelValidator;
        private readonly ISystemConfigurationService _systemConfigurationService;

        // ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
        private readonly ISettingsService _settingsService;

        private bool _useTileBasedPicker;
        private bool _isLightboxEnabled;

        public ChangeAuthorViewModel(
            IUserDataRepository userDataRepository,
            IAuthorRepository authorRepository,
            ISystemConfigurationService systemConfigurationService,
            ISettingsService settingsService,
            IAuthorRoleViewModelValidator authorRoleViewModelValidator,
            Func<bool, AuthorRolesSelectionViewModel> authorRolesSelectionViewModelFunc,
            string title,
            bool showAuthorRolesLabels = true)
        {
            Title = title;
            _userDataRepository = userDataRepository;
            _authorRepository = authorRepository;
            _systemConfigurationService = systemConfigurationService;
            _settingsService = settingsService;
            _authorRoleViewModelValidator = authorRoleViewModelValidator;
            AuthorRolesSelectionViewModel = authorRolesSelectionViewModelFunc(showAuthorRolesLabels);
            Messenger.Default?.Register<AuthorRoleViewModelChangedMessage>(this, OnAuthorRoleViewModelChangedMessage);

            GetHelpCommand = new RelayCommand(() => _settingsService.OpenHelpLink("ChangeAuthor"));
        }

        public AuthorRolesSelectionViewModel AuthorRolesSelectionViewModel { get; set; }

        public override string Title { get; }

        public bool UseTileBasedAuthorPicker
        {
            get => _useTileBasedPicker;
            set => Set(() => UseTileBasedAuthorPicker, ref _useTileBasedPicker, value);
        }

        public bool IsLightboxEnabled
        {
            get => _isLightboxEnabled;
            set => Set(() => IsLightboxEnabled, ref _isLightboxEnabled, value);
        }

        public string ValidationMessage { get; set; }

        public RelayCommand GetHelpCommand { get; }

        public override bool CanCommit()
        {
            return base.CanCommit() && string.IsNullOrEmpty(ValidationMessage);
        }

        public async Task InitAsync(
            IEurolookDocument eurolookDocument,
            List<AuthorCustomXmlStoreInfo> authorCustomXmlStoreInfos)
        {
            var authorRoles = await _authorRepository.GetAuthorRolesAsync(eurolookDocument.DocumentModel.Id);

            var authorRoleViewModels = GetAuthorRoleViewModels(authorCustomXmlStoreInfos, authorRoles).ToList();

            var authorCustomXmlDocs = authorCustomXmlStoreInfos.Select(i => i.AuthorCustomXml).ToList();
            var authorRoleSettings = GetAuthorRoleSettings(authorCustomXmlDocs).ToList();

            var user = await _userDataRepository.GetUserIncludingAuthorsAndJobAssignmentsAsync();
            var authorsAndJobs = GetAuthorAndJobsFromFavoritesAndDocument(user, authorCustomXmlStoreInfos);

            AuthorRolesSelectionViewModel.InitAuthorsAndJobs(authorsAndJobs);
            AuthorRolesSelectionViewModel.InitAuthorRoles(authorRoleViewModels, authorRoleSettings);

            UseTileBasedAuthorPicker = await _systemConfigurationService.IsCreationWizardEnabled();

            AuthorRolesSelectionViewModel.AuthorSearchLightBoxViewModel.PropertyChanged +=
                OnAuthorSearchPropertyChanged;
            AuthorRolesSelectionViewModel.AuthorPickerLightBoxViewModel.PropertyChanged +=
                OnAuthorPickerPropertyChanged;
        }

        private void OnAuthorSearchPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(AuthorRolesSelectionViewModel.AuthorSearchLightBoxViewModel.IsSearchVisible))
            {
                IsLightboxEnabled = AuthorRolesSelectionViewModel.AuthorSearchLightBoxViewModel.IsSearchVisible;
            }
        }

        private void OnAuthorPickerPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(AuthorRolesSelectionViewModel.AuthorPickerLightBoxViewModel.IsPickerVisible))
            {
                IsLightboxEnabled = AuthorRolesSelectionViewModel.AuthorPickerLightBoxViewModel.IsPickerVisible;
            }
        }

        private void OnAuthorRoleViewModelChangedMessage(AuthorRoleViewModelChangedMessage message)
        {
            try
            {
                ValidationMessage = "";
                _authorRoleViewModelValidator.Validate(
                    AuthorRolesSelectionViewModel.AuthorRoleViewModels.Cast<AuthorRoleViewModel>().ToList());
            }
            catch (EurolookValidationException ex)
            {
                ValidationMessage = ex.Message;
            }

            RaisePropertyChanged(() => ValidationMessage);
        }

        private IEnumerable<AuthorAndAllJobs> GetAuthorAndJobsFromFavoritesAndDocument(
            User user,
            List<AuthorCustomXmlStoreInfo> authorCustomXmlStoreInfos)
        {
            var result = AuthorAndAllJobs.CreateAll(user).ToList();

            foreach (var authorCustomXmlStoreInfo in authorCustomXmlStoreInfos)
            {
                var authorCustomXmlDoc = authorCustomXmlStoreInfo.AuthorCustomXml;

                if (result.Select(aj => aj.Author.Author?.Id)
                          .Where(id => id != null)
                          .Contains(authorCustomXmlDoc.AuthorId))
                {
                    continue;
                }

                var author = new Author
                {
                    Id = authorCustomXmlDoc.AuthorId,
                    LatinFirstName = authorCustomXmlDoc.AuthorLatinFirstName,
                    LatinLastName = authorCustomXmlDoc.AuthorLatinLastName,
                    OrgaEntity = new OrgaEntity { Name = authorCustomXmlDoc.OrgaEntity1Name },
                    Service = authorCustomXmlDoc.Service
                };

                if (Guid.TryParse(authorCustomXmlDoc.OrgaEntity1Id, out var orgaEntityId))
                {
                    author.OrgaEntityId = orgaEntityId;
                }

                author.Functions.Add(new JobFunction { Function = authorCustomXmlDoc.FunctionText });

                result.Add(AuthorAndAllJobs.Create(new AuthorViewModel(author, authorCustomXmlStoreInfo.StoreId)));
            }

            return result;
        }

        private IEnumerable<AuthorRoleViewModel> GetAuthorRoleViewModels(
            List<AuthorCustomXmlStoreInfo> authorCustomXmlStoreInfo,
            List<DocumentModelAuthorRole> authorRoles)
        {
            return authorCustomXmlStoreInfo.Select(
                xml =>
                {
                    var authorCustomXml = xml.AuthorCustomXml;
                    var existingAuthorRole =
                        authorRoles.FirstOrDefault(r => r.AuthorRoleId == authorCustomXml.AuthorRoleId);
                    return new AuthorRoleViewModel(
                        new AuthorRole
                        {
                            Name = authorCustomXml.AuthorRoleName ?? authorCustomXml.OriginalAuthorRoleName ?? "Main",
                            Id = authorCustomXml.AuthorRoleId ?? authorCustomXml.OriginalAuthorRoleId ?? Guid.Empty,
                            DisplayName = existingAuthorRole?.AuthorRole?.DisplayName,
                            Description = string.IsNullOrEmpty(existingAuthorRole?.Description)
                                ? existingAuthorRole?.AuthorRole?.Description
                                : existingAuthorRole.Description
                        },
                        xml.StoreId);
                });
        }

        private IEnumerable<AuthorRoleSetting> GetAuthorRoleSettings(List<AuthorCustomXml> authorCustomXmlDocs)
        {
            return authorCustomXmlDocs.Select(
                xml => new AuthorRoleSetting
                {
                    AuthorRoleId = xml.AuthorRoleId ?? xml.OriginalAuthorRoleId ?? Guid.Empty,
                    AuthorId = xml.AuthorId,
                    JobAssignmentId = xml.JobAssignmentId
                });
        }
    }

    public class ChangeAuthorDesignViewModel : ChangeAuthorViewModel
    {
        public ChangeAuthorDesignViewModel()
            : base(null, null, null, null, new AuthorRoleViewModelValidator(), b => null, "Change Document Author(s)")
        {
            AuthorRolesSelectionViewModel = new AuthorRolesSelectionDesignViewModel();
            ValidationMessage = "Validation failed message";
        }
    }
}
