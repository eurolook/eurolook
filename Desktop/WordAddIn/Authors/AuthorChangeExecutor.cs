using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.AddIn.Common.Authors;
using Eurolook.AddIn.Common.Database;
using Eurolook.Data.Constants;
using Eurolook.Data.Models;
using Eurolook.Data.Xml;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.DocumentCreation;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.BrickEngine;
using Eurolook.WordAddIn.Extensions;
using JetBrains.Annotations;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.Authors
{
    [UsedImplicitly]
    public class AuthorChangeExecutor : BaseAuthorChangeExecutor, IAuthorChangeExecutor, IAuthorChangeExecutorForCurrentDocuments
    {
        private readonly IBrickRepository _brickRepository;

        public AuthorChangeExecutor(
            IAuthorDataLoader authorDataLoader,
            IAuthorManager authorManager,
            IBrickRepository brickRepository)
            : base(authorDataLoader, authorManager)
        {
            _brickRepository = brickRepository;
        }

        public bool CanHandle(IEnumerable<AuthorCustomXml> authorCustomXmlDocs)
        {
            return authorCustomXmlDocs.Any(xml => xml.AuthorRoleId != null || xml.OriginalAuthorRoleId != null);
        }

        public void ChangeDocumentAuthor(
            IBrickExecutionContext context,
            IEnumerable<AuthorRoleViewModel> authorRoleViewModels)
        {
            var document = context.Document;

            var customXmlParts = new Dictionary<Guid, CustomXMLPart>();
            foreach (var authorRoleViewModel in authorRoleViewModels)
            {
                var authorRoleId = authorRoleViewModel.AuthorRole.Id;
                if (authorRoleViewModel.IsModified)
                {
                    var authorRoleMapping = authorRoleViewModel.ToAuthorRoleMapping();
                    var authorCustomXmlPart = CreateCustomXmlPart(context, authorRoleMapping);
                    customXmlParts.Add(authorRoleId, authorCustomXmlPart);
                }
                else
                {
                    var existingCustomXmlPart = document.GetCustomXmlPart(authorRoleViewModel.CustomXmlStoreId);
                    customXmlParts.Add(authorRoleId, existingCustomXmlPart);
                }

                var allBrickContainers = document.LoadAllBricks(_brickRepository).ToList();
                UpdateBricksAndCleanup(context, allBrickContainers, customXmlParts);
            }
        }

        public void ChangeDocumentAuthor(IBrickExecutionContext context, List<AuthorRoleMapping> authorRoleMappings, bool updateSignerRole)
        {
            var document = context.Document;
            var customXmlParts = new Dictionary<Guid, CustomXMLPart>();
            foreach (var authorRoleMapping in authorRoleMappings)
            {
                var authorCustomXmlPart = CreateCustomXmlPart(context, authorRoleMapping);
                var authorRoleId = authorRoleMapping.AuthorRole.Id;
                customXmlParts.Add(authorRoleId, authorCustomXmlPart);
            }

            var allBrickContainers = document.LoadAllBricks(_brickRepository).ToList();
            UpdateBricksAndCleanup(context, allBrickContainers, customXmlParts, updateSignerRole);
        }

        public void ChangeBrickAuthors(
            IBrickExecutionContext context,
            IEnumerable<AuthorRoleViewModel> authorRoleViewModels,
            ContentControl selectedContentControl)
        {
            var document = context.Document;

            var customXmlParts = new Dictionary<Guid, CustomXMLPart>();
            foreach (var authorRoleViewModel in authorRoleViewModels)
            {
                var authorRoleId = authorRoleViewModel.AuthorRole.Id;
                if (authorRoleViewModel.IsModified)
                {
                    var authorRoleMapping = authorRoleViewModel.ToAuthorRoleMapping();
                    var language = context.EurolookDocument.Language;
                    var authorCustomXmlCreationInfo = CreateAuthorCustomXmlCreationInfo(authorRoleMapping, language);
                    authorCustomXmlCreationInfo.IsMainAuthor = false;
                    authorCustomXmlCreationInfo.OriginalAuthorRole = authorRoleMapping.AuthorRole;
                    authorCustomXmlCreationInfo.ContentControlTitle = selectedContentControl.Title;

                    var authorCustomXml = new AuthorCustomXml(authorCustomXmlCreationInfo);
                    var newCustomXmlPart = AuthorManager.AddAuthorXmlPart(document, authorCustomXml);
                    customXmlParts.Add(authorRoleId, newCustomXmlPart);
                }
                else
                {
                    var existingCustomXmlPart = document.GetCustomXmlPart(authorRoleViewModel.CustomXmlStoreId);
                    customXmlParts.Add(authorRoleId, existingCustomXmlPart);
                }
            }

            var brickContainers = document.LoadAllBricksOf(_brickRepository, selectedContentControl).ToList();
            UpdateBricksAndCleanup(context, brickContainers, customXmlParts);
        }

        private CustomXMLPart CreateCustomXmlPart(IBrickExecutionContext context, AuthorRoleMapping authorRoleMapping)
        {
            var language = context.EurolookDocument.Language;
            var authorCustomXmlCreationInfo = CreateAuthorCustomXmlCreationInfo(authorRoleMapping, language);
            var authorCustomXmlPart = AuthorManager.GetAuthorXmlPart(context.Document, authorRoleMapping.AuthorRole.Id);
            authorCustomXmlCreationInfo.IsMainAuthor = AuthorManager.HasCreatorAttribute(authorCustomXmlPart);
            authorCustomXmlCreationInfo.AuthorRole = authorRoleMapping.AuthorRole;

            var authorCustomXml = new AuthorCustomXml(authorCustomXmlCreationInfo);
            AuthorManager.ReplaceAuthorXmlPart(authorCustomXmlPart, authorCustomXml);
            return authorCustomXmlPart;
        }

        private void UpdateBricksAndCleanup(
            IBrickExecutionContext context,
            List<BrickContainer> brickContainers,
            Dictionary<Guid, CustomXMLPart> customXmlParts,
            bool updateSignerRole = true)
        {
            context.AuthorInformation = AuthorManager.GenerateAuthorInformation(
                customXmlParts.Values,
                context.EurolookProperties);

            var authorBricks = brickContainers.Where(b => AuthorManager.GetAuthorXmlMappings(b.ContentControl).Any()).ToList();
            if (!updateSignerRole)
            {
                authorBricks.RemoveAll(b => ContainsBindingToSigner(context, b));
            }

            context.EurolookDocument.BrickEngine.RecreateDynamicBricks(context, authorBricks);
            UpdateStaticBricksInDocument(authorBricks, customXmlParts);

            AuthorManager.CleanAuthorXmlParts(context.Document);
        }

        private bool ContainsBindingToSigner(IBrickExecutionContext context, BrickContainer b)
        {
            var authorCustomXmlStoreInfos = AuthorManager.GetAuthorCustomXmlStoreInfos(b.ContentControl, context.Document);
            return authorCustomXmlStoreInfos.Any(info => info.AuthorCustomXml.AuthorRoleId == CommonDataConstants.SignatoryAuthorRoleId);
        }

        private void UpdateStaticBricksInDocument(
            IEnumerable<BrickContainer> brickContainers,
            Dictionary<Guid, CustomXMLPart> newCustomPartMapping)
        {
            // update the mappings
            foreach (var container in brickContainers.Where(c => c.Brick is ContentBrick && !(c.Brick is DynamicBrick)))
            {
                foreach (var mapping in AuthorManager.GetAuthorXmlMappings(container.ContentControl))
                {
                    var authorCustomXml = mapping.CustomXMLPart.ToAuthorCustomXml();
                    var mappedRoleId = authorCustomXml.AuthorRoleId ?? authorCustomXml.OriginalAuthorRoleId;

                    if (mappedRoleId.HasValue && newCustomPartMapping.ContainsKey(mappedRoleId.Value))
                    {
                        mapping.SetMapping(mapping.XPath, Source: newCustomPartMapping[mappedRoleId.Value]);
                    }
                }
            }
        }
    }
}
