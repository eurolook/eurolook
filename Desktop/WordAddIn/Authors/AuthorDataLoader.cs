using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Authors;
using Eurolook.AddIn.Common.Database;
using Eurolook.Common.Log;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.Data.Models.AuthorRoles;
using Eurolook.Data.Xml;
using Eurolook.DataSync.HttpClient;
using Eurolook.DocumentProcessing.DocumentCreation;

namespace Eurolook.WordAddIn.Authors
{
    public class AuthorDataLoader : IAuthorDataLoader, ICanLog
    {
        private const string ErrorIncomplete = "Author is incomplete";
        private const string ErrorNotFound = "No longer available";
        private const string ErrorNotFoundLong = "There isn’t any information about this author in Eurolook’s central repository. The author might have been deleted.";

        private const string ErrorNoConnection = "Currently not available";
        private const string ErrorNoConnectionLong = "Information on this author is not available on your PC and you are currently not connected to Eurolook’s central repository.";
        public static readonly string ErrorIncompleteMySelf = "Some required information is missing from your author profile. Complete the missing information.";

        public static readonly string ErrorIncompleteDetailed = "Some required information is missing for the selected author.";

        private readonly ISettingsService _settingsService;
        private readonly IUserDataRepository _userDataRepository;
        private readonly IAuthorRepository _authorRepository;
        private readonly ILanguageRepository _languageRepository;

        public AuthorDataLoader(
            ISettingsService settingsService,
            IUserDataRepository userDataRepository,
            IAuthorRepository authorRepository,
            ILanguageRepository languageRepository)
        {
            _settingsService = settingsService;
            _userDataRepository = userDataRepository;
            _authorRepository = authorRepository;
            _languageRepository = languageRepository;
        }

        public void LoadCompleteAuthorData(AuthorRoleMapping authorRolesMapping, Language language)
        {
            authorRolesMapping.Author = _authorRepository.GetAuthorForDocumentCreation(authorRolesMapping.Author.Id, language);
            authorRolesMapping.JobAssignment = authorRolesMapping.JobAssignment?.Id == null
                ? null
                : _authorRepository.GetJobAssignmentForDocumentCreation(authorRolesMapping.JobAssignment.Id, language);
        }

        public async Task<AuthorUpdateInfo> GetAuthorInfoFromDbOrServerAsync(
            AuthorCustomXmlStoreInfo authorCustomXmlStoreInfo,
            Language language)
        {
            var authorCustomXml = authorCustomXmlStoreInfo.AuthorCustomXml;

            var searchClient = new SearchClient(_settingsService.DataSyncUrl);
            var authorId = authorCustomXml.AuthorId;
            var author = _authorRepository.GetAuthorForDocumentCreation(authorId, language);
            if (author == null)
            {
                // search online
                try
                {
                    var login = await searchClient.Login();
                    if (!login.IsSuccess)
                    {
                        throw new Exception("Author search client login not successful.");
                    }

                    var searchResults = await searchClient.SearchAuthors(CancellationToken.None, authorId.ToString());
                    if (searchResults.Any())
                    {
                        author = searchResults.First();
                        _authorRepository.LoadTranslationsForAuthor(author, language);
                    }
                    else
                    {
                        return new AuthorUpdateInfo(authorCustomXmlStoreInfo, ErrorNotFound, ErrorNotFoundLong);
                    }
                }
                catch (Exception ex)
                {
                    this.LogWarn(ex);
                    return new AuthorUpdateInfo(authorCustomXmlStoreInfo, ErrorNoConnection, ErrorNoConnectionLong);
                }
            }

            JobAssignment jobAssignment = null;
            var user = _userDataRepository.GetUser();
            bool isJobSelected = authorCustomXml.JobAssignmentId != null;
            if (author.IsIncomplete(!isJobSelected))
            {
                return CreateIncompleteAuthorUpdateInfo(user, author, authorCustomXmlStoreInfo);
            }

            if (isJobSelected)
            {
                jobAssignment = _authorRepository.GetJobAssignmentForDocumentCreation(authorCustomXml.JobAssignmentId.Value, language);
                if (jobAssignment != null && jobAssignment.IsIncomplete())
                {
                    return CreateIncompleteAuthorUpdateInfo(user, author, authorCustomXmlStoreInfo);
                }
            }

            var authorUpdateInfo = new AuthorUpdateInfo(authorCustomXmlStoreInfo, author, jobAssignment);
            authorUpdateInfo.JobAssignment = CreateJobAssignmentWithInitializedFunctions(authorUpdateInfo);
            authorUpdateInfo.AuthorCustomXml = CreateUpdatedAuthorCustomXml(authorUpdateInfo, language);
            return authorUpdateInfo;
        }

        private IJobAssignment CreateJobAssignmentWithInitializedFunctions(AuthorUpdateInfo authorUpdateInfo)
        {
            var jobAssignment = authorUpdateInfo.JobAssignment ?? authorUpdateInfo.Author;

            // attach JobFunction languages (needed by AuthorCustomXml constructor)
            foreach (var jobFunction in jobAssignment.Functions)
            {
                jobFunction.Language = _languageRepository.GetLanguage(jobFunction.LanguageId);
            }

            return jobAssignment;
        }

        private AuthorCustomXml CreateUpdatedAuthorCustomXml(AuthorUpdateInfo authorUpdateInfo, Language language)
        {
            var authorCustomXml = authorUpdateInfo.AuthorCustomXml;
            var authorRoleId = authorCustomXml.AuthorRoleId;
            var authorRole = authorRoleId.HasValue
                ? new AuthorRole
                {
                    Id = authorRoleId.Value,
                    Name = authorCustomXml.AuthorRoleName,
                }
                : null;

            var originalAuthorRoleId = authorCustomXml.OriginalAuthorRoleId;
            var originalAuthorRole = originalAuthorRoleId.HasValue
                ? new AuthorRole
                {
                    Id = originalAuthorRoleId.Value,
                    Name = authorCustomXml.OriginalAuthorRoleName,
                }
                : null;

            var authorCustomXmlCreationInfo =
                new AuthorCustomXmlCreationInfo(authorUpdateInfo.Author, authorUpdateInfo.JobAssignment, language)
                {
                    IsMainAuthor = authorCustomXml.HasCreatorAttribute,
                    AuthorRole = authorRole,
                    OriginalAuthorRole = originalAuthorRole,
                    ContentControlTitle = authorCustomXml.ContentControlTitle,
                };

            return new AuthorCustomXml(authorCustomXmlCreationInfo);
        }

        private AuthorUpdateInfo CreateIncompleteAuthorUpdateInfo(User user, Author author, AuthorCustomXmlStoreInfo authorCustomXmlStoreInfo)
        {
            return user.SelfId == author.Id
                ? new AuthorUpdateInfo(
                    authorCustomXmlStoreInfo,
                    ErrorIncomplete,
                    string.Format(ErrorIncompleteMySelf, author.FullNameWithLastNameLast))
                : new AuthorUpdateInfo(authorCustomXmlStoreInfo, ErrorIncomplete, ErrorIncompleteDetailed);
        }
    }
}
