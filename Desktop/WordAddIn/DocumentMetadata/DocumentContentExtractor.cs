﻿using System.Linq;
using Eurolook.Common.Extensions;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.WordAddIn.DocumentMetadata.CalculationCommands;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.WordAddIn.DocumentMetadata
{
    public class DocumentContentExtractor : IDocumentContentExtractor
    {
        public ContentControl GetContentControlByNamePrefix(
            Document document,
            string contentControlNameStart)
        {
            return document.GetAllContentControls()
                                   .FirstOrDefault(
                                       cc => cc.Title?.StartsWith(contentControlNameStart) == true);
        }

        public string ExtractTextFromContentControl(
            Document document,
            string titleContentControlNameStart,
            string excludeStyleName = null)
        {
            return GetContentControlByNamePrefix(document, titleContentControlNameStart)
                   ?.Range.Paragraphs.OfType<Paragraph>()
                   .Where(p => p.GetStyleNameLocal() != excludeStyleName)
                   .Select(p => p.Range.Text)
                   .Aggregate((s1, s2) => s1 + s2);
        }

        public string ExtractTextFromContentControlWithoutPlaceholderText(
            Document document,
            string titleContentControlNameStart,
            string excludeStyleName = null)
        {
            return GetContentControlByNamePrefix(document, titleContentControlNameStart)
                   ?.Range.Paragraphs.OfType<Paragraph>()
                   .Where(p => p.GetStyleNameLocal() != excludeStyleName)
                   .Select(p => p.Range.TextWithoutPlaceholders())
                   .Aggregate((s1, s2) => s1 + s2);
        }

        public string ExtractTextFromFirstMatchingParagraph(Document document, string paragraphStyleName)
        {
            return ExtractTextFromFirstMatchingParagraph(document.Content, paragraphStyleName);
        }

        public string ExtractTextFromFirstMatchingParagraph(ContentControl contentControl, string paragraphStyleName)
        {
            return ExtractTextFromFirstMatchingParagraph(contentControl?.Range, paragraphStyleName);
        }

        public string ExtractTextFromFirstMatchingParagraph(Range range, string paragraphStyleName)
        {
            return range?.FirstOrDefaultParagraph(paragraphStyleName)
                        ?.Range
                        .Text
                        .ReplaceAllConsecutiveControlCharacters(" ")
                        .Trim();
        }
    }
}
