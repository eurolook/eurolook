using Eurolook.AddIn.Common;
using Eurolook.WordAddIn.Extensions;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.DocumentMetadata.MetadataUpdatedHandlers
{
    [UsedImplicitly]
    public class WordTitleFieldUpdater : IMetadataUpdatedHandler
    {
        private readonly IWordFieldUpdater _wordFieldUpdater;

        public WordTitleFieldUpdater(IWordFieldUpdater wordFieldUpdater)
        {
            _wordFieldUpdater = wordFieldUpdater;
        }

        public float ExecutionOrder => 20.0f;

        public Task HandleAsync(IEurolookDocument eurolookDocument)
        {
            _wordFieldUpdater.UpdateAllFields(eurolookDocument.GetDocument(), WdFieldType.wdFieldTitle);
            return Task.FromResult(0);
        }
    }
}
