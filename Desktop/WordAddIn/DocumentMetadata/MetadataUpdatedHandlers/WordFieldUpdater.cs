using System.Linq;
using Eurolook.WordAddIn.Extensions;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.DocumentMetadata.MetadataUpdatedHandlers
{
    public class WordFieldUpdater : IWordFieldUpdater
    {
        public void UpdateAllFields(Document document, WdFieldType wdFieldType)
        {
            using (new DocumentAutomationHelper(document))
            {
                var filenameFields = document.AllStoryRanges()
                                             .SelectMany(r => r.Fields.OfType<Field>())
                                             .Where(f => f.Type == wdFieldType).ToList();

                foreach (var filenameField in filenameFields)
                {
                    filenameField.Update();
                }
            }
        }
    }
}
