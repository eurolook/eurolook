using Eurolook.AddIn.Common;
using Eurolook.WordAddIn.Extensions;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;

namespace Eurolook.WordAddIn.DocumentMetadata.MetadataUpdatedHandlers
{
    [UsedImplicitly]
    public class WordFilenameFieldUpdater : IMetadataUpdatedHandler
    {
        private readonly IWordFieldUpdater _wordFieldUpdater;

        public WordFilenameFieldUpdater(IWordFieldUpdater wordFieldUpdater)
        {
            _wordFieldUpdater = wordFieldUpdater;
        }

        public float ExecutionOrder => 10.0f;

        public Task HandleAsync(IEurolookDocument eurolookDocument)
        {
            _wordFieldUpdater.UpdateAllFields(eurolookDocument.GetDocument(), WdFieldType.wdFieldFileName);
            return Task.FromResult(0);
        }
    }
}
