using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.DocumentMetadata.MetadataUpdatedHandlers
{
    public interface IWordFieldUpdater
    {
        void UpdateAllFields(Document document, WdFieldType wdFieldType);
    }
}
