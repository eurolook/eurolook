using System.Threading.Tasks;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.BrickEngine;

namespace Eurolook.WordAddIn
{
    public interface IChangeLanguageHandler
    {
        /// <summary>
        /// Gets a value influencing in what order several handlers will be executed. Lower values are executed first.
        /// </summary>
        float ExecutionOrder { get; }

        Task BeforeHandleAsync(IBrickExecutionContext context, Language sourceLanguage, Language targetLanguage);

        Task HandleAsync(IBrickExecutionContext context, Language sourceLanguage, Language targetLanguage);

        Task AfterHandleAsync(IBrickExecutionContext context, Language sourceLanguage, Language targetLanguage);
    }
}
