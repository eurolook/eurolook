﻿using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn
{
    public interface IUpdateTocService
    {
        void UpdateToc(TableOfContents tableOfContent);
    }
}
