﻿using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data.TemplateStore;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;

namespace Eurolook.WordAddIn.TemplateManager
{
    public class ImportFileViewModel : ViewModelBase, ICanLog
    {
        private readonly IMessageService _messageService;
        private readonly ILocalTemplatesService _templatesService;

        private string _selectedFileName;
        private string _selectedFile;
        private TemplatePackage _package;

        private string _templateName;
        private string _templateCreator;
        private string _modificationDate;
        private string _baseDocumentModel;
        private string _templateLanguages;
        private string _templateFileSize;

        private bool _isInvalid;
        private string _errorMessage;

        private bool _isAlreadyPresent;

        public ImportFileViewModel(ILocalTemplatesService templatesService, IMessageService messageService)
        {
            _templatesService = templatesService;
            _messageService = messageService;
            BrowseCommand = new RelayCommand(SelectImportFile);

            ResetViewModel();
        }

        public RelayCommand BrowseCommand { get; set; }

        public string TemplateName
        {
            get => _templateName;
            set => Set(() => TemplateName, ref _templateName, value);
        }

        public string TemplateCreator
        {
            get => _templateCreator;
            set => Set(() => TemplateCreator, ref _templateCreator, value);
        }

        public string ModificationDate
        {
            get => _modificationDate;
            set => Set(() => ModificationDate, ref _modificationDate, value);
        }

        public string BaseDocumentType
        {
            get => _baseDocumentModel;
            set => Set(() => BaseDocumentType, ref _baseDocumentModel, value);
        }

        public string TemplateLanguages
        {
            get => _templateLanguages;
            set => Set(() => TemplateLanguages, ref _templateLanguages, value);
        }

        public string TemplateFileSize
        {
            get => _templateFileSize;
            set => Set(() => TemplateFileSize, ref _templateFileSize, value);
        }

        public bool CanSave { get => !IsInvalid && SelectedFileName != string.Empty; }

        public string SelectedFileName
        {
            get => _selectedFileName;
            set
            {
                Set(() => SelectedFileName, ref _selectedFileName, value);
                RaisePropertyChanged(() => CanSave);
            }
        }

        public string ErrorMessage
        {
            get => _errorMessage;
            set
            {
                Set(() => ErrorMessage, ref _errorMessage, value);
                RaisePropertyChanged(() => CanSave);
            }
        }

        public bool IsInvalid
        {
            get => _isInvalid;
            set => Set(() => IsInvalid, ref _isInvalid, value);
        }

        public bool Apply()
        {
            if (!_isAlreadyPresent || (_messageService.ShowMessageWindow(
                    new MessageViewModel
                    {
                        Title = "Template Already Present",
                        Message =
                            $"An earlier version of {TemplateName} is already installed. This import will overwrite any changed you made to the template. Do you want to continue?",
                        YesButtonText = "Continue",
                        NoButtonText = "Cancel",
                        IsShowNoButton = true,
                        IsShowCancelButton = false,
                    },
                    this) == true))
            {
                var result = _templatesService.ImportTemplate(_package);
                if (!result)
                {
                    _messageService.ShowSimpleMessage("Failed to import template.", "Error", this);
                    return false;
                }

                ResetViewModel();
                return true;
            }

            return false;
        }

        public void Cancel()
        {
            ResetViewModel();
        }

        private void ResetViewModel()
        {
            _selectedFile = string.Empty;
            SelectedFileName = string.Empty;

            TemplateName = "N/A";
            TemplateCreator = "N/A";
            ModificationDate = "N/A";
            BaseDocumentType = "N/A";
            TemplateLanguages = "N/A";
            TemplateFileSize = "N/A";
        }

        private void SelectImportFile()
        {
            var dlg = new OpenFileDialog
            {
                Filter = "Eurolook template package (*.xml)|*.xml",
                CheckFileExists = true,
                Multiselect = false,
                Title = "Select Template Package to Import",
            };

            var result = dlg.ShowDialog();
            if (result.HasValue && result.Value)
            {
                _selectedFile = dlg.FileName;
                SelectedFileName = dlg.SafeFileName;
                ValidateImportFile().FireAndForgetSafeAsync(this.LogError);
            }
        }

        private async Task ValidateImportFile()
        {
            var validation = await _templatesService.ValidateTemplatePackageAsync(_selectedFile);

            IsInvalid = !validation.IsValid;
            ErrorMessage = validation.ErrorMessage;

            TemplateName = validation.TemplateName ?? "N/A";
            TemplateCreator = validation.TemplateCreator ?? "N/A";
            ModificationDate = validation.LastModified?.ToUIString() ?? "N/A";
            BaseDocumentType = validation.BaseDocumentModelName ?? "N/A";
            TemplateLanguages = validation.Languages ?? "N/A";
            TemplateFileSize = validation.FileSize ?? "N/A";

            _isAlreadyPresent = validation.IsAlreadyPresent;
            _package = validation.TemplatePackage;
        }
    }
}
