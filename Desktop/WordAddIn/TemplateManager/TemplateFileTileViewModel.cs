using Eurolook.Common.Extensions;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Eurolook.WordAddIn.TemplateManager
{
    public class TemplateFileTileViewModel : ViewModelBase
    {
        private bool _hasTemplate;
        private bool _isValidInDocumentModel = true;

        public TemplateFileTileViewModel(TemplateFile templateFile, Language language)
        {
            TemplateFile = templateFile;
            Language = language;

            HasTemplate = !templateFile?.Bytes.IsNullOrEmpty() ?? false;
        }

        public Language Language { get; }

        public TemplateFile TemplateFile { get; }

        public string LanguageName
        {
            get => Language.Name;
        }

        public bool HasTemplate
        {
            get => _hasTemplate;
            set => Set(() => HasTemplate, ref _hasTemplate, value);
        }

        public bool IsValidInDocumentModel
        {
            get => _isValidInDocumentModel;
            set => Set(() => IsValidInDocumentModel, ref _isValidInDocumentModel, value);
        }

        public RelayCommand<TemplateFileTileViewModel> AddFileCommand { get; set; }

        public RelayCommand<TemplateFileTileViewModel> ReplaceFileCommand { get; set; }

        public RelayCommand<TemplateFileTileViewModel> DeleteFileCommand { get; set; }
    }
}
