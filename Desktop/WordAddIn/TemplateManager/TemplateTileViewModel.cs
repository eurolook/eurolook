﻿using System;
using Eurolook.Common.AsyncCommands;
using Eurolook.Common.Extensions;
using Eurolook.Data.Models.TemplateStore;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using JetBrains.Annotations;

namespace Eurolook.WordAddIn.TemplateManager;

public class TemplateTileViewModel : ViewModelBase
{
    public TemplateTileViewModel(Template template)
    {
        Init(template);
    }

    public Guid TemplateId { get; private set; }

    public string TemplateName { get; private set; }

    [UsedImplicitly]
    public string TemplateCreator { get; private set; }

    public string ModificationDate { get; private set; }

    public RelayCommand<TemplateTileViewModel> EditTemplateCommand { get; set; }

    public AsyncCommand<TemplateTileViewModel> ExportTemplateCommand { get; set; }

    public RelayCommand<TemplateTileViewModel> DeleteTemplateCommand { get; set; }

    public void Init(Template template)
    {
        TemplateId = template.Id;
        TemplateName = template.Name;
        TemplateCreator = template.Tags;
        ModificationDate = template.ServerModificationTimeUtc.ToUIString();
    }
}
