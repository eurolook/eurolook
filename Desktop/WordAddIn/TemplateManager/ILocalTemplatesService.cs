﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;
using Eurolook.Data.TemplateStore;

namespace Eurolook.WordAddIn.TemplateManager
{
    public interface ILocalTemplatesService
    {
        public string GetUserName();

        public Task<IEnumerable<DocumentModel>> GetAllowedDocumentModelsAsync();

        public Task<TemplateFileValidationResult> ValidateTemplateFileAsync(
            string fileName,
            Guid? expectedBaseDocumentModelId);

        public Task<TemplatePackageValidationResult> ValidateTemplatePackageAsync(string fileName);

        public IEnumerable<Template> GetAllTemplates();

        public Task<Template> GetTemplateAsync(Guid templateId);

        public bool ImportTemplate(TemplatePackage package);

        public void SaveTemplate(
            Template template,
            IEnumerable<TemplateFile> modifiedTemplateFiles,
            IEnumerable<TemplateFile> deletedTemplateFiles);

        public void DeleteTemplate(Guid templateId);

        public void CreateTemplateDataBackupXml();

        public Task<Guid?> GetLocalTemplateId(IEurolookDocument eurolookDocument);
    }
}
