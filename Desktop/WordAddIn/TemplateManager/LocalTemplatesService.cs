﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.StandaloneMode;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data;
using Eurolook.Data.Compatibility;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;
using Eurolook.Data.TemplateStore;
using Eurolook.Data.Xml;
using Eurolook.DocumentProcessing.Extensions;

namespace Eurolook.WordAddIn.TemplateManager
{
    public class LocalTemplatesService : ILocalTemplatesService, ICanLog
    {
        private readonly IAuthorRepository _authorRepository;
        private readonly IDocumentModelRepository _documentModelRepository;
        private readonly ILanguageRepository _languageRepository;
        private readonly IUserDataRepository _userDataRepository;
        private readonly ILocalTemplatesRepository _localTemplatesRepository;
        private readonly ISharedTemplateRepository _sharedTemplateRepository;
        private readonly ISettingsService _settingsService;

        private readonly IDocumentModelVersionCompatibilityTester _documentModelVersionCompatibilityTester;
        private readonly IBrickVersionCompatibilityTester _brickVersionCompatibilityTester;

        private readonly Dictionary<Guid, DocumentModel> _availableDocumentModels = new();

        public LocalTemplatesService(
            IAuthorRepository authorRepository,
            IDocumentModelRepository documentModelRepository,
            ILanguageRepository languageRepository,
            IUserDataRepository userDataRepository,
            ISettingsService settingsService,
            ILocalTemplatesRepository localTemplatesRepository,
            ISharedTemplateRepository sharedTemplateRepository,
            IDocumentModelVersionCompatibilityTester documentModelVersionCompatibilityTester,
            IBrickVersionCompatibilityTester brickVersionCompatibilityTester)
        {
            _authorRepository = authorRepository;
            _documentModelRepository = documentModelRepository;
            _userDataRepository = userDataRepository;
            _languageRepository = languageRepository;
            _localTemplatesRepository = localTemplatesRepository;
            _sharedTemplateRepository = sharedTemplateRepository;
            _documentModelVersionCompatibilityTester = documentModelVersionCompatibilityTester;
            _brickVersionCompatibilityTester = brickVersionCompatibilityTester;

            _settingsService = settingsService;
        }

        public string GetUserName()
        {
            var user = _userDataRepository.GetUser();
            var author = _authorRepository.GetAuthor(user.SelfId);
            return author.FullNameWithLastNameLast;
        }

        public async Task<IEnumerable<DocumentModel>> GetAllowedDocumentModelsAsync()
        {
            await LoadDocumentModelsAsync();
            return _availableDocumentModels.Values;
        }

        public async Task<TemplateFileValidationResult> ValidateTemplateFileAsync(
            string fileName,
            Guid? expectedDocumentModelId)
        {
            var result = new TemplateFileValidationResult();

            try
            {
                // workaround to allow reading while template is open in Word
                using var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using var wordDocument = WordprocessingDocument.Open(fileStream, false);
                var fileInfo = new FileInfo(fileName);

                await LoadDocumentModelsAsync();

                var eurolookPropertiesPart = wordDocument.MainDocumentPart?.CustomXmlParts.FirstOrDefault(
                    p => p.GetXDocument()?.Root?.Name == EurolookPropertiesCustomXml.RootName);
                if (eurolookPropertiesPart != null)
                {
                    result.IsEurolookDocument = true;
                    result.FileSize = fileInfo.HumanReadableFileSize();

                    var eurolookProperties = new EurolookPropertiesCustomXml(eurolookPropertiesPart.GetXDocument());
                    result.Language = eurolookProperties.CreationLanguage;
                    result.BaseDocumentModelId = eurolookProperties.DocumentModelId;
                    result.BaseDocumentModelName = eurolookProperties.DocumentModelName;
                    result.IsExpectedDocumentModel = !expectedDocumentModelId.HasValue
                                                     || eurolookProperties.DocumentModelId == expectedDocumentModelId;
                    result.IsAllowedDocumentModel = eurolookProperties.DocumentModelId.HasValue
                                                    && _availableDocumentModels.ContainsKey(
                                                        eurolookProperties.DocumentModelId.Value);

                    var trackRevisions = wordDocument.MainDocumentPart?.DocumentSettingsPart?.Settings.Descendants<TrackRevisions>();
                    result.IsTrackChangesEnabled = !trackRevisions.IsNullOrEmpty();
                    result.HasTrackedChanges = wordDocument.MainDocumentPart?.HasTrackedRevisions() ?? false;
                }
            }
            catch (Exception ex)
            {
                this.LogError("Failed to open file for template validation.", ex);
                result.HasThrownException = true;
            }

            return result;
        }

        public async Task<TemplatePackageValidationResult> ValidateTemplatePackageAsync(string fileName)
        {
            var result = new TemplatePackageValidationResult();

            TemplatePackage package;
            FileInfo fileInfo;

            try
            {
                fileInfo = new FileInfo(fileName);
                package = TemplatePackage.FromFile(fileName);
            }
            catch (Exception ex)
            {
                this.LogError("Failed to open template package.", ex);
                result.IsValid = false;
                result.ErrorMessage = "The provided file is not a valid Eurolook template package.";
                return result;
            }

            await LoadDocumentModelsAsync();

            if (package.Template.BaseDocumentModelId.HasValue
                && _availableDocumentModels.ContainsKey(package.Template.BaseDocumentModelId.Value))
            {
                result.TemplateName = package.Template.Name;
                result.TemplateDescription = package.Template.Description;
                result.TemplateCreator = package.GetTemplateCreator();
                result.IsBaseDocumentModelAllowed = true;
                result.BaseDocumentModelName =
                    _availableDocumentModels[package.Template.BaseDocumentModelId.Value].DisplayName;
                result.LastModified = package.Template.ServerModificationTimeUtc;
                result.FileSize = fileInfo.HumanReadableFileSize();

                var availableLanguages = (await _languageRepository.GetAllLanguagesAsync())
                    .OrderBy(x => x, new EnFrDeAlphabeticalLanguagesComparer());
                var templateLanguages = package.TemplateFiles.Select(l => l.LanguageId).ToList();

                result.Languages = string.Join(
                    ", ",
                    availableLanguages.Where(l => templateLanguages.Contains(l.Id)).Select(l => l.Name));

                var template = _localTemplatesRepository.GetTemplateByName(package.Template.Name);
                result.IsAlreadyPresent = template != null;

                result.TemplatePackage = package;
            }
            else
            {
                result.IsBaseDocumentModelAllowed = false;
                result.IsValid = false;
                result.ErrorMessage = "The provided file is not compatible with this Eurolook version.";
            }

            return result;
        }

        public IEnumerable<Template> GetAllTemplates()
        {
            return _localTemplatesRepository.GetAllLocalTemplates();
        }

        public Task<Template> GetTemplateAsync(Guid templateId)
        {
            return _localTemplatesRepository.GetTemplateForEditingAsync(templateId);
        }

        public bool ImportTemplate(TemplatePackage package)
        {
            try
            {
                var existingTemplate = _localTemplatesRepository.GetTemplateByName(package.Template.Name);
                if (existingTemplate != null)
                {
                    DeleteTemplate(existingTemplate.Id);
                }

                var importingTemplate = package.Template;
                importingTemplate.Tags = package.GetTemplateCreator();

                var importingTemplateFiles = package.TemplateFiles;

                importingTemplate.Id = Guid.NewGuid();
                importingTemplateFiles.ForEach(
                    file =>
                    {
                        file.Id = Guid.NewGuid();
                        file.TemplateId = importingTemplate.Id;
                    });

                SaveTemplate(importingTemplate, importingTemplateFiles, new List<TemplateFile>());
            }
            catch (Exception ex)
            {
                this.LogError($"Failed to import template package for {package?.TemplateName}.", ex);
                return false;
            }

            return true;
        }

        public void SaveTemplate(
            Template template,
            IEnumerable<TemplateFile> modifiedTemplateFiles,
            IEnumerable<TemplateFile> deletedTemplateFiles)
        {
            template.BaseDocumentModel = null;
            template.TemplateFiles?.Clear();
            _localTemplatesRepository.CreateOrUpdateLocalTemplate(template);

            foreach (var modifiedTemplateFile in modifiedTemplateFiles)
            {
                modifiedTemplateFile.TemplateId = template.Id;
                modifiedTemplateFile.Language = null;
                _localTemplatesRepository.CreateOrUpdateLocalTemplateFile(modifiedTemplateFile);
            }

            foreach (var deletedTemplateFile in deletedTemplateFiles)
            {
                _localTemplatesRepository.DeleteLocalTemplateFile(deletedTemplateFile.Id);
            }

            var user = _userDataRepository.GetUser();
            _localTemplatesRepository.AddToUserTemplates(template.Id, user.Id);
        }

        public void DeleteTemplate(Guid templateId)
        {
            _localTemplatesRepository.DeleteLocalTemplate(templateId);
        }

        public void CreateTemplateDataBackupXml()
        {
            var templates = _localTemplatesRepository.GetAllLocalTemplates();
            var templateFiles = _localTemplatesRepository.GetAllLocalTemplateFiles();

            var backup = new TemplateDataBackupXml(templates, templateFiles);
            backup.Save(_settingsService);
        }

        public async Task<Guid?> GetLocalTemplateId(IEurolookDocument eurolookDocument)
        {
            var properties = eurolookDocument.GetEurolookProperties();
            if (properties.CustomTemplateId.HasValue)
            {
                var template = await _sharedTemplateRepository.GetTemplate(properties.CustomTemplateId.Value);
                if (template == null)
                {
                    template = _localTemplatesRepository.GetTemplateByName(properties.CustomTemplateName);
                    if (template != null && template.BaseDocumentModelId == eurolookDocument.DocumentModel.Id)
                    {
                        return template.Id;
                    }
                }
                else
                {
                    return template.Id;
                }
            }

            return null;
        }

        private async Task LoadDocumentModelsAsync()
        {
            if (_availableDocumentModels.IsNullOrEmpty())
            {
                var documentModels = await _documentModelRepository.GetVisibleDocumentModelsAsync(
                    _documentModelVersionCompatibilityTester,
                    _brickVersionCompatibilityTester);
                documentModels.ForEach(dm => _availableDocumentModels.Add(dm.Id, dm));
            }
        }
    }
}
