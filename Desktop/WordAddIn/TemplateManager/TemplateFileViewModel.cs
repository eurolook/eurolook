﻿using System;
using System.IO;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;

namespace Eurolook.WordAddIn.TemplateManager
{
    public class TemplateFileViewModel : CancelableViewModel<TemplateFile>, ICanLog
    {
        private readonly ILocalTemplatesService _templatesService;

        private string _detectedDocumentModel;
        private string _detectedLanguage;
        private string _detectedFileSize;

        private Language _expectedLanguage;

        private string _selectedFileName;
        private string _selectedFile;

        private bool _isInvalid;
        private string _errorMessage;

        public TemplateFileViewModel(ILocalTemplatesService templatesService)
        {
            _templatesService = templatesService;
            BrowseCommand = new RelayCommand(SelectTemplateFile);
        }

        public bool IsNew { get; private set; }

        public bool CanSave { get => !IsInvalid && SelectedFileName != string.Empty; }

        public RelayCommand BrowseCommand { get; set; }

        public Guid DetectedDocumentModelId { get; set; }

        public string DetectedDocumentModel
        {
            get => _detectedDocumentModel;
            set => Set(() => DetectedDocumentModel, ref _detectedDocumentModel, value);
        }

        public string DetectedLanguage
        {
            get => _detectedLanguage;
            set => Set(() => DetectedLanguage, ref _detectedLanguage, value);
        }

        public string DetectedFileSize
        {
            get => _detectedFileSize;
            set => Set(() => DetectedFileSize, ref _detectedFileSize, value);
        }

        public string SelectedFileName
        {
            get => _selectedFileName;
            set
            {
                Set(() => SelectedFileName, ref _selectedFileName, value);
                RaisePropertyChanged(() => CanSave);
            }
        }

        public string ErrorMessage
        {
            get => _errorMessage;
            set => Set(() => ErrorMessage, ref _errorMessage, value);
        }

        public bool IsInvalid
        {
            get => _isInvalid;
            set
            {
                Set(() => IsInvalid, ref _isInvalid, value);
                RaisePropertyChanged(() => CanSave);
            }
        }

        public Guid? ExpectedBaseDocumentModelId { get; set; }

        public string ExpectedBaseDocumentModelName { get; set; }

        public Language ExpectedLanguage
        {
            get => _expectedLanguage;
            set
            {
                Set(() => ExpectedLanguage, ref _expectedLanguage, value);
                RaisePropertyChanged(() => ExpectedLanguageName);
                RaisePropertyChanged(() => ExpectedLanguageDisplayName);
            }
        }

        public string ExpectedLanguageName => ExpectedLanguage?.Name;

        public string ExpectedLanguageDisplayName => ExpectedLanguage?.DisplayName;

        public override void Apply()
        {
            if (Model == null)
            {
                return;
            }

            if (IsNew)
            {
                Model.Init();
            }

            Model.LanguageId = ExpectedLanguage.Id;
            Model.Language = ExpectedLanguage;
            Model.Bytes = ReadFile(_selectedFile);
        }

        public override void Cancel()
        {
        }

        protected override void InitViewModel(TemplateFile model)
        {
            if (model == null)
            {
                return;
            }

            IsNew = model.Id == Guid.Empty;

            DetectedDocumentModel = "N/A";
            DetectedLanguage = "N/A";
            DetectedFileSize = "N/A";

            _selectedFile = string.Empty;
            SelectedFileName = string.Empty;
            ErrorMessage = string.Empty;
            IsInvalid = false;
        }

        private void SelectTemplateFile()
        {
            var dlg = new OpenFileDialog
            {
                Filter = "Word documents (*.docx)|*.docx",
                CheckFileExists = true,
                Multiselect = false,
                Title = "Select Template File",
            };

            var result = dlg.ShowDialog();

            if (result.HasValue && result.Value)
            {
                _selectedFile = dlg.FileName;
                SelectedFileName = dlg.SafeFileName;
                ValidateFile().FireAndForgetSafeAsync(this.LogError);
            }
        }

        private async Task ValidateFile()
        {
            var validation = await _templatesService.ValidateTemplateFileAsync(
                _selectedFile,
                ExpectedBaseDocumentModelId);
            if (validation.HasThrownException)
            {
                IsInvalid = true;
                ErrorMessage = "Failed to open the selected file.";
            }
            else if (!validation.IsEurolookDocument)
            {
                IsInvalid = true;
                ErrorMessage = "The selected file is not a valid Eurolook document.";
            }
            else if (!validation.IsExpectedDocumentModel)
            {
                IsInvalid = true;
                ErrorMessage =
                    $"The file does not match the required Eurolook template {ExpectedBaseDocumentModelName}.";
            }
            else if (!validation.IsAllowedDocumentModel)
            {
                IsInvalid = true;
                ErrorMessage = "The detected Eurolook template is incompatible with this Eurolook version.";
            }
            else if (!validation.Language.Equals(ExpectedLanguage.Name, StringComparison.InvariantCultureIgnoreCase))
            {
                IsInvalid = true;
                ErrorMessage =
                    $"The detected language {validation.Language} does not match the required language {ExpectedLanguage.Name}.";
            }
            else if (validation.HasTrackedChanges || validation.IsTrackChangesEnabled)
            {
                IsInvalid = true;
                ErrorMessage = validation.HasTrackedChanges
                    ? "The selected file has unresolved tracked changes. Accept or reject the unresolved changes and "
                    : "The selected file has \"Track Changes\" turned on. ";

                ErrorMessage += "Turn off \"Track Changes\" before adding the file to the Template Manager.";
            }
            else
            {
                IsInvalid = false;
            }

            DetectedDocumentModelId = validation.BaseDocumentModelId ?? Guid.Empty;
            DetectedDocumentModel = validation.BaseDocumentModelName ?? "N/A";
            DetectedLanguage = validation.Language ?? "N/A";
            DetectedFileSize = validation.FileSize ?? "N/A";
        }

        private static byte[] ReadFile(string filePath)
        {
            // workaround to allow reading while template is open in Word
            using var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            return fileStream.ToByteArray();
        }
    }
}
