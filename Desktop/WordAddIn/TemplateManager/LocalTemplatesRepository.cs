﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.Models.TemplateStore;
using Microsoft.EntityFrameworkCore;

namespace Eurolook.WordAddIn.TemplateManager
{
    public class LocalTemplatesRepository : EurolookClientDatabase, ILocalTemplatesRepository
    {
        public IEnumerable<Template> GetAllLocalTemplates()
        {
            using var context = GetContext();
            return context.Templates
                          .Where(t => !t.Deleted)
                          .OrderBy(t => t.Name)
                          .ToList();
        }

        public IEnumerable<TemplateFile> GetAllLocalTemplateFiles()
        {
            using var context = GetContext();
            return context.TemplateFiles
                          .Where(t => !t.Deleted)
                          .ToList();
        }

        public async Task<Template> GetTemplateForEditingAsync(Guid templateId)
        {
            await using var context = GetContext();
            return await context.Templates
                                .Where(t => t.Id == templateId && !t.Deleted)
                                .Include(t => t.BaseDocumentModel)
                                .ThenInclude(dm => dm.DocumentLanguages)
                                .ThenInclude(dl => dl.DocumentLanguage)
                                .Include(t => t.TemplateFiles)
                                .ThenInclude(f => f.Language)
                                .FirstOrDefaultAsync();
        }

        public Template GetTemplateByName(string templateName)
        {
            using var context = GetContext();
            return context.Templates.FirstOrDefault(t => t.Name == templateName && !t.Deleted);
        }

        public void CreateOrUpdateLocalTemplate(Template template)
        {
            using var context = GetContext();
            var dbTemplate = context.Templates.FirstOrDefault(t => t.Id == template.Id);
            if (dbTemplate == null)
            {
                var newTemplate = new Template()
                {
                    Id = template.Id,
                    Name = template.Name,
                    Description = template.Description,
                    Tags = template.Tags,
                    IsPublic = true,
                    BaseDocumentModelId = template.BaseDocumentModelId,
                    PreviewPage1 = template.PreviewPage1,
                    PreviewPage2 = template.PreviewPage2,
                };

                newTemplate.UpdateServerModificationTime();
                context.Templates.Add(newTemplate);
            }
            else
            {
                dbTemplate.Name = template.Name;
                dbTemplate.Description = template.Description;
                dbTemplate.Tags = template.Tags;
                dbTemplate.BaseDocumentModelId = template.BaseDocumentModelId;
                dbTemplate.UpdateServerModificationTime();
            }

            context.SaveChanges();
        }

        public void DeleteLocalTemplate(Guid templateId)
        {
            using var context = GetContext();
            var template = context.Templates.FirstOrDefault(t => t.Id == templateId);
            if (template != null)
            {
                var userSettings = context.UserSettings.FirstOrDefault();
                if (userSettings != null && userSettings.LastTemplateId == template.Id)
                {
                    userSettings.LastTemplateId = null;
                }

                var documentModelSettings = context.DocumentModelSettings.Where(s => s.TemplateId == template.Id).ToList();
                foreach (var setting in documentModelSettings)
                {
                    context.DocumentModelSettings.Remove(setting);
                }

                var templateFiles = context.TemplateFiles.Where(f => f.TemplateId == template.Id).ToList();
                foreach (var templateFile in templateFiles)
                {
                    context.TemplateFiles.Remove(templateFile);
                }

                var userTemplates = context.UserTemplates.Where(ut => ut.TemplateId == template.Id).ToList();
                foreach (var userTemplate in userTemplates)
                {
                    context.UserTemplates.Remove(userTemplate);
                }

                context.Templates.Remove(template);
                context.SaveChanges();
            }
        }

        public void CreateOrUpdateLocalTemplateFile(TemplateFile templateFile)
        {
            using var context = GetContext();
            var dbTemplateFile = context.TemplateFiles.FirstOrDefault(f => f.Id == templateFile.Id);
            if (dbTemplateFile == null)
            {
                context.TemplateFiles.Add(templateFile);
            }
            else
            {
                dbTemplateFile.TemplateId = templateFile.TemplateId;
                dbTemplateFile.LanguageId = templateFile.LanguageId;
                dbTemplateFile.Bytes = templateFile.Bytes;
                dbTemplateFile.UpdateServerModificationTime();
            }

            context.SaveChanges();
        }

        public void DeleteLocalTemplateFile(Guid templateFileId)
        {
            using var context = GetContext();
            var templateFile = context.TemplateFiles.FirstOrDefault(t => t.Id == templateFileId);
            if (templateFile != null)
            {
                context.TemplateFiles.Remove(templateFile);
                context.SaveChanges();
            }
        }

        public void AddToUserTemplates(Guid templateId, Guid userId)
        {
            using var context = GetContext();
            if (context.Templates.Any(t => t.Id == templateId && !t.Deleted))
            {
                var userTemplate = context.UserTemplates.FirstOrDefault(
                    t => t.TemplateId == templateId && t.UserId == userId && !t.Deleted);
                if (userTemplate == null)
                {
                    userTemplate = new UserTemplate();
                    userTemplate.Init();
                    context.UserTemplates.Add(userTemplate);
                }

                userTemplate.TemplateId = templateId;
                userTemplate.UserId = userId;
                userTemplate.IsOfflineAvailable = true;
            }

            context.SaveChanges();
        }
    }
}
