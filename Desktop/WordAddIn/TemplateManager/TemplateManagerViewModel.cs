﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Common.AsyncCommands;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;
using Eurolook.Data.TemplateStore;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using JetBrains.Annotations;
using Microsoft.Win32;

namespace Eurolook.WordAddIn.TemplateManager
{
    public class TemplateManagerViewModel : ViewModelBase, ICanLog
    {
        private readonly ILanguageRepository _languageRepository;
        private readonly ILocalTemplatesService _localTemplatesService;
        private readonly ISharedTemplateService _sharedTemplateService;
        private readonly IMessageService _messageService;
        private readonly ISettingsService _settingsService;

        private readonly List<Language> _availableLanguages = new();

        private bool _isTemplateEditorVisible;
        private bool _isFileSelectorVisible;
        private bool _isImportDialogVisible;

        private bool _canCreateOrImport;

        public TemplateManagerViewModel(
            ILocalTemplatesService localTemplatesService,
            ISharedTemplateService sharedTemplateService,
            ILanguageRepository languageRepository,
            IMessageService messageService,
            ISettingsService settingsService)
        {
            _languageRepository = languageRepository;
            _localTemplatesService = localTemplatesService;
            _sharedTemplateService = sharedTemplateService;
            _messageService = messageService;
            _settingsService = settingsService;

            TemplateViewModel = new TemplateViewModel();
            Templates = new ObservableCollectionEx<TemplateTileViewModel>();

            TemplateFileViewModel = new TemplateFileViewModel(_localTemplatesService);
            ImportFileViewModel = new ImportFileViewModel(_localTemplatesService, _messageService);

            GetHelpCommand = new RelayCommand(GetHelp);

            CloseCommand = new RelayCommand(Close);
            CreateTemplateCommand = new RelayCommand(CreateNewTemplate);
            ImportTemplateCommand = new RelayCommand(ImportTemplate);
            SaveTemplateEditorCommand = new RelayCommand(SaveTemplateEditor);
            CancelTemplateEditorCommand = new RelayCommand(CancelTemplateEditor);
            SaveTemplateFileSelectorCommand = new RelayCommand(SaveTemplateFileSelector);
            CancelTemplateFileSelectorCommand = new RelayCommand(CancelTemplateFileSelector);
            ConfirmImportTemplatePackageCommand = new RelayCommand(ConfirmImportFile);
            CancelImportTemplatePackageCommand = new RelayCommand(CancelTemplatePackageSelector);
        }

        public Action CloseAction { get; set; }

        public RelayCommand GetHelpCommand { get; }

        public RelayCommand SaveTemplateEditorCommand { get; set; }

        public RelayCommand CancelTemplateEditorCommand { get; set; }

        public RelayCommand CloseCommand { get; set; }

        public RelayCommand CreateTemplateCommand { get; set; }

        public RelayCommand ImportTemplateCommand { get; set; }

        public RelayCommand SaveTemplateFileSelectorCommand { get; set; }

        public RelayCommand CancelTemplateFileSelectorCommand { get; set; }

        public RelayCommand ConfirmImportTemplatePackageCommand { get; set; }

        public RelayCommand CancelImportTemplatePackageCommand { get; set; }

        public ObservableCollectionEx<TemplateTileViewModel> Templates { get; set; }

        public TemplateViewModel TemplateViewModel { get; }

        public TemplateFileViewModel TemplateFileViewModel { get; }

        public ImportFileViewModel ImportFileViewModel { get; }

        public bool IsBaseFormEnabled
        {
            get => !_isTemplateEditorVisible && !_isFileSelectorVisible && !_isImportDialogVisible;
        }

        public bool AreTemplatesAvailable { get => Templates.Any(); }

        public bool IsTemplateEditorVisible
        {
            get => _isTemplateEditorVisible;
            set
            {
                Set(() => IsTemplateEditorVisible, ref _isTemplateEditorVisible, value);
                RaisePropertyChanged(() => IsTemplateEditorEnabled);
                RaisePropertyChanged(() => IsBaseFormEnabled);
            }
        }

        public bool CanCreateOrImport
        {
            get => _canCreateOrImport;
            set => Set(() => CanCreateOrImport, ref _canCreateOrImport, value);
        }

        public bool IsTemplateEditorEnabled => _isTemplateEditorVisible && !_isFileSelectorVisible;

        [UsedImplicitly]
        public bool IsFileSelectorVisible
        {
            get => _isFileSelectorVisible;
            set
            {
                Set(() => IsFileSelectorVisible, ref _isFileSelectorVisible, value);
                RaisePropertyChanged(() => IsTemplateEditorEnabled);
                RaisePropertyChanged(() => IsBaseFormEnabled);
            }
        }

        [UsedImplicitly]
        public bool IsImportDialogVisible
        {
            get => _isImportDialogVisible;
            set
            {
                Set(() => IsImportDialogVisible, ref _isImportDialogVisible, value);
                RaisePropertyChanged(() => IsTemplateEditorEnabled);
                RaisePropertyChanged(() => IsBaseFormEnabled);
            }
        }

        public async Task LoadData()
        {
            CanCreateOrImport = false;
            var languages = await _languageRepository.GetAllLanguagesAsync();
            _availableLanguages.Clear();
            _availableLanguages.AddRange(languages.OrderBy(x => x, new EnFrDeAlphabeticalLanguagesComparer()));

            TemplateViewModel.InitAllowedBaseDocumentModelIds(await _localTemplatesService.GetAllowedDocumentModelsAsync());

            LoadTemplates();
            CanCreateOrImport = true;
        }

        private void Close()
        {
            _localTemplatesService.CreateTemplateDataBackupXml();

            CloseAction?.Invoke();
        }

        // RelayCommands are weakly bound
        private void GetHelp()
        {
            _settingsService.OpenHelpLink("TemplateManager");
        }

        private void CreateNewTemplate()
        {
            TemplateViewModel.Init(new Template());
            TemplateViewModel.SetFileTileViewModels(
                _availableLanguages.Select(l => CreateTemplateFileTileViewModel(new TemplateFile(), l)));
            IsTemplateEditorVisible = true;
        }

        private void ImportTemplate()
        {
            IsImportDialogVisible = true;
        }

        private void EditTemplate(TemplateTileViewModel viewModel)
        {
            EditTemplateAsync(viewModel).FireAndForgetSafeAsync(this.LogError);
        }

        private async Task ExportTemplate(TemplateTileViewModel viewModel)
        {
            var dlg = new SaveFileDialog
            {
                Filter = "XML File (*.xml)|*.xml",
                AddExtension = true,
                DefaultExt = "xml",
                Title = "Export Template",
                FileName = $"{viewModel.TemplateName}.xml",
            };

            var result = dlg.ShowDialog();

            if (result.HasValue && result.Value)
            {
                if (!await _sharedTemplateService.ExportTemplate(viewModel.TemplateId, dlg.FileName))
                {
                    _messageService.ShowSimpleMessage("Failed to export template.", "Error", this);
                }
            }
        }

        private async Task EditTemplateAsync(TemplateTileViewModel tileViewModel)
        {
            var template = await _localTemplatesService.GetTemplateAsync(tileViewModel.TemplateId);
            TemplateViewModel.Init(template);

            var tiles = new List<TemplateFileTileViewModel>();
            foreach (var language in _availableLanguages)
            {
                var templateFile = template.TemplateFiles.FirstOrDefault(f => f.LanguageId == language.Id);
                tiles.Add(
                    templateFile != null
                        ? CreateTemplateFileTileViewModel(templateFile, language)
                        : CreateTemplateFileTileViewModel(new TemplateFile(), language));
            }

            TemplateViewModel.SetFileTileViewModels(tiles);
            IsTemplateEditorVisible = true;
        }

        private void DeleteTemplate(TemplateTileViewModel tileViewModel)
        {
            if (_messageService.ShowMessageWindow(
                    new MessageViewModel
                    {
                        Title = "Delete Template",
                        Message = "Do you really want to permanently delete this template?",
                        YesButtonText = "Yes",
                        NoButtonText = "No",
                        IsShowNoButton = true,
                        IsShowCancelButton = false,
                    },
                    this) == true)
            {
                _localTemplatesService.DeleteTemplate(tileViewModel.TemplateId);
                Templates.Remove(tileViewModel);
                RaisePropertyChanged(() => AreTemplatesAvailable);
            }
        }

        private void SaveTemplateEditor()
        {
            if (TemplateViewModel.Validate())
            {
                if ((TemplateViewModel.IsNew || TemplateViewModel.IsNameChanged) && Templates.Any(t => t.TemplateName.Equals(TemplateViewModel.TemplateName)))
                {
                    _messageService.ShowSimpleMessage($"A template with the name \"{TemplateViewModel.TemplateName}\" already exists.", "Error", this);
                    return;
                }

                TemplateViewModel.TemplateCreator = _localTemplatesService.GetUserName();
                TemplateViewModel.Apply();
                _localTemplatesService.SaveTemplate(
                    TemplateViewModel.Model,
                    TemplateViewModel.ModifiedTemplateFiles,
                    TemplateViewModel.DeletedTemplateFiles);

                var templates = Templates.ToList();
                var model = TemplateViewModel.Model;

                if (TemplateViewModel.IsNew)
                {
                    templates.Add(CreateTemplateTileViewModel(model));
                }
                else
                {
                    templates.FirstOrDefault(t => t.TemplateId == model.Id)?.Init(model);
                }

                Templates.ReplaceRange(templates.OrderBy(t => t.TemplateName));
                RaisePropertyChanged(() => AreTemplatesAvailable);

                IsTemplateEditorVisible = false;
            }
            else
            {
                _messageService.ShowSimpleMessage(TemplateViewModel.Error, "Error", this);
            }
        }

        private void CancelTemplateEditor()
        {
            TemplateViewModel.Cancel();
            IsTemplateEditorVisible = false;
        }

        private void AddOrReplaceTemplateFile(TemplateFileTileViewModel tileViewModel)
        {
            TemplateFileViewModel.Init(tileViewModel.TemplateFile);
            TemplateFileViewModel.ExpectedLanguage = tileViewModel.Language;
            TemplateFileViewModel.ExpectedBaseDocumentModelId = TemplateViewModel.BaseDocumentModelId;
            TemplateFileViewModel.ExpectedBaseDocumentModelName = TemplateViewModel.BaseDocumentModelName;
            IsFileSelectorVisible = true;
        }

        private void DeleteTemplateFile(TemplateFileTileViewModel tileViewModel)
        {
            if (_messageService.ShowMessageWindow(
                    new MessageViewModel
                    {
                        Title = "Delete Template File",
                        Message =
                            $"Do you really want to permanently delete the {tileViewModel.Language.DisplayName} file?",
                        YesButtonText = "Yes",
                        NoButtonText = "No",
                        IsShowNoButton = true,
                        IsShowCancelButton = false,
                    },
                    this) == true)
            {
                TemplateViewModel.DeleteTemplateFile(tileViewModel.TemplateFile);
            }
        }

        private void SaveTemplateFileSelector()
        {
            TemplateFileViewModel.Apply();

            if (TemplateFileViewModel.IsNew && !TemplateViewModel.BaseDocumentModelId.HasValue)
            {
                TemplateViewModel.SetBaseDocumentModel(TemplateFileViewModel.DetectedDocumentModelId);
            }

            TemplateViewModel.AddOrUpdateTemplateFile(TemplateFileViewModel.Model);

            IsFileSelectorVisible = false;
        }

        private void CancelTemplateFileSelector()
        {
            TemplateFileViewModel.Cancel();

            IsFileSelectorVisible = false;
        }

        private void ConfirmImportFile()
        {
            if (ImportFileViewModel.Apply())
            {
                LoadTemplates();
                IsImportDialogVisible = false;
            }
        }

        private void CancelTemplatePackageSelector()
        {
            ImportFileViewModel.Cancel();
            IsImportDialogVisible = false;
        }

        private void LoadTemplates()
        {
            Templates.Clear();
            Templates.AddRange(
                _localTemplatesService
                    .GetAllTemplates()
                    .Select(CreateTemplateTileViewModel)
                    .OrderBy(t => t.TemplateName));
            RaisePropertyChanged(() => AreTemplatesAvailable);
        }

        private TemplateTileViewModel CreateTemplateTileViewModel(Template template)
        {
            return new TemplateTileViewModel(template)
            {
                EditTemplateCommand = new RelayCommand<TemplateTileViewModel>(EditTemplate),
                ExportTemplateCommand = new AsyncCommand<TemplateTileViewModel>(ExportTemplate, null, this.LogError),
                DeleteTemplateCommand = new RelayCommand<TemplateTileViewModel>(DeleteTemplate),
            };
        }

        private TemplateFileTileViewModel CreateTemplateFileTileViewModel(TemplateFile templateFile, Language language)
        {
            return new TemplateFileTileViewModel(templateFile, language)
            {
                AddFileCommand = new RelayCommand<TemplateFileTileViewModel>(AddOrReplaceTemplateFile),
                ReplaceFileCommand = new RelayCommand<TemplateFileTileViewModel>(AddOrReplaceTemplateFile),
                DeleteFileCommand = new RelayCommand<TemplateFileTileViewModel>(DeleteTemplateFile),
            };
        }
    }
}
