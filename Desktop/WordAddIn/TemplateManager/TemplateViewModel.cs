using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.AddIn.Common.ViewModels;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Common.Extensions;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;

namespace Eurolook.WordAddIn.TemplateManager
{
    public class TemplateViewModel : CancelableViewModel<Template>
    {
        private readonly Dictionary<string, string> _errors = new();
        private readonly Dictionary<Guid, DocumentModel> _allowedBaseDocumentModels = new();
        private readonly List<TemplateFileTileViewModel> _languageFileTilesViewModels = new();
        private readonly Dictionary<Guid, TemplateFile> _modifiedTemplateFilesByLanguage = new();
        private readonly Dictionary<Guid, TemplateFile> _deletedTemplateFilesByLanguage = new();

        private string _templateName;
        private string _templateCreator;
        private string _templateDescription;
        private string _error;
        private bool _isNew;

        public TemplateViewModel()
        {
            MainLanguageFiles = new ObservableCollectionEx<TemplateFileTileViewModel>();
            SecondaryLanguageFiles = new ObservableCollectionEx<TemplateFileTileViewModel>();
        }

        public string TemplateName
        {
            get => _templateName;
            set => Set(() => TemplateName, ref _templateName, value);
        }

        public string TemplateCreator
        {
            get => _templateCreator;
            set => Set(() => TemplateCreator, ref _templateCreator, value);
        }

        public string TemplateDescription
        {
            get => _templateDescription;
            set => Set(() => TemplateDescription, ref _templateDescription, value);
        }

        public ObservableCollectionEx<TemplateFileTileViewModel> MainLanguageFiles { get; }

        public ObservableCollectionEx<TemplateFileTileViewModel> SecondaryLanguageFiles { get; }

        public string Error
        {
            get => _error;
            set
            {
                Set(() => Error, ref _error, value);
            }
        }

        public bool IsNew
        {
            get => _isNew;
            private set
            {
                Set(() => IsNew, ref _isNew, value);
                RaisePropertyChanged(() => SaveButtonText);
            }
        }

        public bool IsNameChanged { get; private set; }

        public bool CanDelete => _languageFileTilesViewModels.Count(vm => vm.HasTemplate) > 1;

        public Guid? BaseDocumentModelId { get; private set; }

        public string BaseDocumentModelName => BaseDocumentModelId.HasValue
            ? _allowedBaseDocumentModels[BaseDocumentModelId.Value].DisplayName
            : string.Empty;

        public string SaveButtonText => IsNew ? "Create" : "Save";

        public IEnumerable<TemplateFile> ModifiedTemplateFiles => _modifiedTemplateFilesByLanguage.Values;

        public IEnumerable<TemplateFile> DeletedTemplateFiles => _deletedTemplateFilesByLanguage.Values;

        public bool Validate()
        {
            if (_templateName.IsNullOrEmpty())
            {
                Error = "A template name is required.";
                return false;
            }

            if (!_languageFileTilesViewModels.Any(vm => vm.HasTemplate))
            {
                Error = "At least one template file must be added.";
                return false;
            }

            IsNameChanged = !TemplateName.Equals(Model.Name);

            return true;
        }

        public void InitAllowedBaseDocumentModelIds(IEnumerable<DocumentModel> allowedBaseDocumentModels)
        {
            _allowedBaseDocumentModels.Clear();
            allowedBaseDocumentModels.ForEach(dm => _allowedBaseDocumentModels.Add(dm.Id, dm));
        }

        public void SetFileTileViewModels(IEnumerable<TemplateFileTileViewModel> viewModels)
        {
            _languageFileTilesViewModels.Clear();
            _languageFileTilesViewModels.AddRange(viewModels);

            UpdateAllTemplateFileTileViewModels();
            UpdateMainAndSecondaryLanguageFiles();
        }

        public override void Apply()
        {
            if (Model == null)
            {
                return;
            }

            if (IsNew)
            {
                Model.Init();
            }

            Model.Name = TemplateName;
            Model.Description = TemplateDescription;
            Model.Tags = TemplateCreator;
            Model.BaseDocumentModelId = BaseDocumentModelId;
            Model.UpdateServerModificationTime();
        }

        public override void Cancel()
        {
            if (Model == null)
            {
                return;
            }

            TemplateName = Model.Name;
            TemplateCreator = Model.Tags;
            TemplateDescription = Model.Description;
        }

        public void SetBaseDocumentModel(Guid? documentModelId)
        {
            if (!documentModelId.HasValue || _allowedBaseDocumentModels.ContainsKey(documentModelId.Value))
            {
                BaseDocumentModelId = documentModelId;
                UpdateAllTemplateFileTileViewModels();
            }
        }

        public void AddOrUpdateTemplateFile(TemplateFile templateFile)
        {
            _modifiedTemplateFilesByLanguage[templateFile.LanguageId] = templateFile;

            if (_deletedTemplateFilesByLanguage.ContainsKey(templateFile.LanguageId))
            {
                _deletedTemplateFilesByLanguage.Remove(templateFile.LanguageId);
            }

            UpdateTemplateFileTileViewModel(templateFile.LanguageId, true);
        }

        public void DeleteTemplateFile(TemplateFile templateFile)
        {
            _deletedTemplateFilesByLanguage[templateFile.LanguageId] = templateFile;

            if (_modifiedTemplateFilesByLanguage.ContainsKey(templateFile.LanguageId))
            {
                _modifiedTemplateFilesByLanguage.Remove(templateFile.LanguageId);
            }

            UpdateTemplateFileTileViewModel(templateFile.LanguageId, false);
        }

        protected override void InitViewModel(Template model)
        {
            if (model == null)
            {
                return;
            }

            IsNew = model.Id == Guid.Empty;
            TemplateName = model.Name;
            TemplateCreator = model.Tags;
            TemplateDescription = model.Description;

            BaseDocumentModelId = model.BaseDocumentModelId;

            _modifiedTemplateFilesByLanguage.Clear();
            _deletedTemplateFilesByLanguage.Clear();
            RaisePropertyChanged(() => CanDelete);
        }

        private void UpdateAllTemplateFileTileViewModels()
        {
            if (BaseDocumentModelId.HasValue && _allowedBaseDocumentModels.ContainsKey(BaseDocumentModelId.Value))
            {
                var allowedLanguages = _allowedBaseDocumentModels[BaseDocumentModelId.Value].DocumentLanguages
                    .Select(l => l.DocumentLanguageId);
                _languageFileTilesViewModels.ForEach(
                    vm => vm.IsValidInDocumentModel = allowedLanguages.Contains(vm.Language.Id));
            }
            else
            {
                _languageFileTilesViewModels.ForEach(vm => vm.IsValidInDocumentModel = true);
            }
        }

        private void UpdateTemplateFileTileViewModel(Guid languageId, bool hasTemplate)
        {
            var tileViewModel = _languageFileTilesViewModels.FirstOrDefault(vm => vm.Language.Id == languageId);
            if (tileViewModel != null)
            {
                tileViewModel.HasTemplate = hasTemplate;
                RaisePropertyChanged(() => CanDelete);
            }
        }

        private void UpdateMainAndSecondaryLanguageFiles()
        {
            MainLanguageFiles.ReplaceRange(_languageFileTilesViewModels.Take(3));
            SecondaryLanguageFiles.ReplaceRange(_languageFileTilesViewModels.Skip(3));
        }
    }
}
