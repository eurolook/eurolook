﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data.Models.TemplateStore;

namespace Eurolook.WordAddIn.TemplateManager
{
    public interface ILocalTemplatesRepository
    {
        public IEnumerable<Template> GetAllLocalTemplates();

        public IEnumerable<TemplateFile> GetAllLocalTemplateFiles();

        public Task<Template> GetTemplateForEditingAsync(Guid templateId);

        public Template GetTemplateByName(string templateName);

        public void CreateOrUpdateLocalTemplate(Template template);

        public void DeleteLocalTemplate(Guid templateId);

        public void CreateOrUpdateLocalTemplateFile(TemplateFile templateFile);

        public void DeleteLocalTemplateFile(Guid templateFileId);

        public void AddToUserTemplates(Guid templateId, Guid userId);
    }
}
