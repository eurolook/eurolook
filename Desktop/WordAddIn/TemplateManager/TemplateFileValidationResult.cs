using System;

namespace Eurolook.WordAddIn.TemplateManager
{
    public class TemplateFileValidationResult
    {
        public bool HasThrownException { get; set; } = false;

        public bool IsEurolookDocument { get; set; }

        public bool IsExpectedDocumentModel { get; set; }

        public bool IsAllowedDocumentModel { get; set; }

        public bool IsTrackChangesEnabled { get; set; }

        public bool HasTrackedChanges { get; set; }

        public string Language { get; set; }

        public Guid? BaseDocumentModelId { get; set; }

        public string BaseDocumentModelName { get; set; }

        public string FileSize { get; set; }
    }
}
