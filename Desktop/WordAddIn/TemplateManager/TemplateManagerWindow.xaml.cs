﻿using System.Windows;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;

namespace Eurolook.WordAddIn.TemplateManager
{
    /// </summary>
    public partial class TemplateManagerWindow : Window, ICanLog
    {
        public TemplateManagerWindow(TemplateManagerViewModel viewModel)
        {
            ViewModel = viewModel;
            ViewModel.CloseAction = Close;
            InitializeComponent();
            DataContext = ViewModel;
        }

        public TemplateManagerViewModel ViewModel { get; set; }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            ViewModel.LoadData().FireAndForgetSafeAsync(this.LogError);
        }
    }
}
