﻿using System;
using Eurolook.Data.TemplateStore;

namespace Eurolook.WordAddIn.TemplateManager
{
    public class TemplatePackageValidationResult
    {
        public bool IsValid { get; set; } = true;

        public bool IsAlreadyPresent { get; set; }

        public bool IsBaseDocumentModelAllowed { get; set; }

        public string TemplateName { get; set; }

        public string TemplateDescription { get; set; }

        public string TemplateCreator { get; set; }

        public string BaseDocumentModelName { get; set; }

        public string Languages { get; set; }

        public string FileSize { get; set; }

        public string ErrorMessage { get; set; }

        public DateTime? LastModified { get; set; }

        public TemplatePackage TemplatePackage { get; set; }
    }
}
