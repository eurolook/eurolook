﻿using System;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn
{
    public class UpdateTocService : IUpdateTocService
    {
        private readonly char _expectedListSeparator = ';';

        public void UpdateToc(TableOfContents toc)
        {
            if (toc == null)
            {
                return;
            }

            var listSeparator = GetCurrentListSeparator(toc);

            var tocCode = GetCurrentTocCode(toc);
            ReplaceListSeparatorInToc(toc, listSeparator);
            toc.Update();
            ResetListSeparatorInToc(toc, tocCode);
        }

        private static char GetCurrentListSeparator(TableOfContents toc)
        {
            return toc.Range.Application.International[WdInternationalIndex.wdListSeparator].ToString()[0];
        }

        private string GetCurrentTocCode(TableOfContents toc)
        {
            var field = toc.Range.Fields[1];
            return field.Code.Text;
        }

        private void ReplaceListSeparatorInToc(TableOfContents toc, char listSeparator)
        {
            var field = toc.Range.Fields[1];
            var tocCode = field.Code.Text;
            var newCode = tocCode.Replace(_expectedListSeparator, listSeparator);
            field.Code.Text = newCode;
        }

        private static void ResetListSeparatorInToc(TableOfContents toc, string tocCode)
        {
            var field = toc.Range.Fields[1];
            field.Code.Text = tocCode;
        }
    }
}
