﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.Database;
using Eurolook.DocumentProcessing.DocumentCreation;
using Eurolook.WordAddIn.Options;

namespace Eurolook.WordAddIn.Database
{
    public interface IWordUserDataRepository : IUserDataRepository
    {
        Task UpdateDocumentModelSettingsAsync(Guid userSettingId, IEnumerable<DocumentTypeViewModel> modifiedDocumentTypes);

        Task UpdateDocumentCreationSettingsAsync(DocumentCreationInfo documentCreationInfo);

        Task UpdateDocumentCreationAuthorRolesSettingsAsync(DocumentCreationInfo documentCreationInfo);

        Task UpdateUserTemplateSettingsAsync(TemplateViewModel[] modifiedTemplates, TemplateViewModel[] deletedTemplates);
    }
}
