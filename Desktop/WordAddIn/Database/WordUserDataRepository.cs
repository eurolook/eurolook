﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.Authors;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.UserConfiguration;
using Eurolook.Common.Extensions;
using Eurolook.Data;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing.DocumentCreation;
using Eurolook.WordAddIn.Options;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Eurolook.WordAddIn.Database
{
    public class WordUserDataRepository : UserDataRepository, IWordUserDataRepository
    {
        public WordUserDataRepository(IAuthorRepository authorRepository, IUserIdentity userIdentity)
            : base(authorRepository, userIdentity)
        {
        }

        public async Task UpdateDocumentModelSettingsAsync(
            Guid userSettingId,
            IEnumerable<DocumentTypeViewModel> modifiedDocumentTypes)
        {
            using (var context = GetContext())
            {
                foreach (var documentTypeVm in modifiedDocumentTypes)
                {
                    var documentModelSettings = await context.DocumentModelSettings
                                                             .IgnoreQueryFilters()
                                                             .Where(x => x.UserSettingsId == userSettingId
                                                                         && x.DocumentModelId == documentTypeVm.DocumentModelId
                                                                         && x.TemplateId == null)
                                                             .FirstOrDefaultAsync();
                    if (documentModelSettings == null)
                    {
                        documentModelSettings = new DocumentModelSettings
                        {
                            DocumentModelId = documentTypeVm.DocumentModelId,
                            UserSettingsId = userSettingId,
                        };
                        documentModelSettings.Init();
                        await context.DocumentModelSettings.AddAsync(documentModelSettings);
                    }

                    documentModelSettings.Deleted = false;
                    documentModelSettings.IsHidden = !documentTypeVm.IsVisible;
                    documentModelSettings.SetModification(ModificationType.ClientModification);
                }

                await context.SaveChangesAsync();
            }
        }

        public async Task UpdateDocumentCreationSettingsAsync(DocumentCreationInfo documentCreationInfo)
        {
            using (var context = GetContext())
            {
                var userSettingsId = documentCreationInfo.User.SettingsId;
                var documentModelId = documentCreationInfo.DocumentModel.Id;

                var userSettings = await context.UserSettings.FirstAsync(us => us.Id == userSettingsId);
                userSettings.LastDocumentLanguageId = documentCreationInfo.Language.Id;
                userSettings.LastDocumentModelId = documentModelId;
                userSettings.LastTemplateId = documentCreationInfo.TemplateStoreTemplate?.Id;
                userSettings.LastSelectedTab = documentCreationInfo.SelectedTemplateViewKind;

                var docModelSettings = await GetOrCreateDocumentModelSettings(context, documentCreationInfo);

                // dynamically create a new favorite language
                var favoriteLanguageIds = userSettings.GetFavoriteLanguageIds();
                if (userSettings.IsFavoriteLanguageListEnabled && !favoriteLanguageIds.Contains(documentCreationInfo.Language.Id))
                {
                    favoriteLanguageIds.Add(documentCreationInfo.Language.Id);
                    userSettings.UpdateFavoriteLanguageIdsRaw(favoriteLanguageIds);
                }

                if (documentCreationInfo.DocumentModel.BrickReferences != null && !documentCreationInfo.UserBrickChoices.IsNullOrEmpty())
                {
                    docModelSettings.UserBrickChoices = UserBrickChoicesFormatter.ToString(documentCreationInfo.DocumentModel.BrickReferences, documentCreationInfo.UserBrickChoices);
                }

                SetLastUsedAuthorRoles(docModelSettings, documentCreationInfo);

                Update(context.DocumentModelSettings, docModelSettings, ModificationType.ClientModification);
                Update(context.UserSettings, userSettings, ModificationType.ClientModification);
                await context.SaveChangesAsync();
            }
        }

        public async Task UpdateDocumentCreationAuthorRolesSettingsAsync(DocumentCreationInfo documentCreationInfo)
        {
            using (var context = GetContext())
            {
                var docModelSettings = await GetOrCreateDocumentModelSettings(context, documentCreationInfo);
                SetLastUsedAuthorRoles(docModelSettings, documentCreationInfo);

                Update(context.DocumentModelSettings, docModelSettings, ModificationType.ClientModification);
                await context.SaveChangesAsync();
            }
        }

        public async Task UpdateUserTemplateSettingsAsync(
            TemplateViewModel[] modifiedTemplates,
            TemplateViewModel[] removedTemplates)
        {
            using (var context = GetContext())
            {
                foreach (var templateVm in modifiedTemplates)
                {
                    var templateSettings = await context.DocumentModelSettings.FirstOrDefaultAsync(x => x.TemplateId == templateVm.TemplateId);
                    if (templateSettings != null)
                    {
                        templateSettings.IsHidden = !templateVm.IsVisible;
                        templateSettings.SetModification(ModificationType.ClientModification);
                    }
                }

                foreach (var templateVm in removedTemplates)
                {
                    var userTemplate = await context.UserTemplates.FirstOrDefaultAsync(x => x.Id == templateVm.UserTemplateId);
                    if (userTemplate != null)
                    {
                        userTemplate.IsOfflineAvailable = false;
                        userTemplate.SetDeletedFlag(ModificationType.ClientModification);
                    }
                }

                await context.SaveChangesAsync();
            }
        }

        private async Task<DocumentModelSettings> GetOrCreateDocumentModelSettings(EurolookClientContext context, DocumentCreationInfo documentCreationInfo)
        {
            var documentModelId = documentCreationInfo.DocumentModel.Id;
            var templateId = documentCreationInfo.TemplateStoreTemplate?.Id;

            var settings = documentCreationInfo.User.Settings;

            var docModelSettings =
                documentCreationInfo.IsTemplateFromTemplateStore
                    ? settings.GetSettingsForTemplate(templateId.Value)
                    : settings.GetSettingsForDocumentModel(documentModelId);

            // If the user never created a DocumentModel of this type the DocumentModelSettings will be null.
            if (docModelSettings == null)
            {
                docModelSettings = new DocumentModelSettings();
                docModelSettings.Init();
                docModelSettings.ClientModification = true;
                docModelSettings.DocumentModelId = documentModelId;
                docModelSettings.UserSettingsId = documentCreationInfo.User.SettingsId;
                docModelSettings.TemplateId = templateId;
                await context.DocumentModelSettings.AddAsync(docModelSettings);
            }

            return docModelSettings;
        }

        private void SetLastUsedAuthorRoles(
            DocumentModelSettings documentModelSettings,
            DocumentCreationInfo documentCreationInfo)
        {
            var authorRoleSettings = documentCreationInfo.AuthorRolesMappingList.Select(ar => new AuthorRoleSetting
            {
                AuthorRoleId = ar.AuthorRole.Id,
                AuthorId = ar.Author?.Id,
                JobAssignmentId = ar.JobAssignment?.Id,
            });

            documentModelSettings.LastUsedAuthorRoles = JsonConvert.SerializeObject(authorRoleSettings);
        }
    }
}
