using Eurolook.AddIn.Common.Database;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing.OpenXml;

namespace Eurolook.WordAddIn.Database
{
    public class LocalisedResourceResolver : ILocalisedResourceResolver
    {
        private readonly IEurolookDataRepository _eurolookDataRepository;
        private readonly ITextsRepository _textsRepository;
        private readonly Language _language;

        public LocalisedResourceResolver(
            IEurolookDataRepository eurolookDataRepository,
            ITextsRepository textsRepository,
            Language language)
        {
            _eurolookDataRepository = eurolookDataRepository;
            _textsRepository = textsRepository;
            _language = language;
        }

        public LocalisedResource ResolveLocalisedResource(string alias)
        {
            return _eurolookDataRepository.GetLocalisedResource(alias, _language);
        }

        public Translation ResolveTranslation(string alias)
        {
            return _textsRepository.GetTranslation(alias, _language);
        }
    }
}
