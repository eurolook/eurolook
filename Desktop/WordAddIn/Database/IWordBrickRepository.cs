using System;
using Eurolook.AddIn.Common.Database;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.BrickCommands.CustomBricks;

namespace Eurolook.WordAddIn.Database
{
    public interface IWordBrickRepository : IBrickRepository
    {
        UserBrick CreateCustomBrick(
            CreateCustomBrickViewModel viewModel,
            DocumentModel documentModel,
            byte[] buildingBlockBytes);

        BuildingBlockBrick UpdateUserBrick(
            Guid brickId,
            CreateCustomBrickViewModel viewModel,
            byte[] buildingBlockBytes);
    }
}
