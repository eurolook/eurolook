using System;
using System.Linq;
using Eurolook.AddIn.Common.Database;
using Eurolook.Data.Constants;
using Eurolook.Data.Models;
using Eurolook.WordAddIn.BrickCommands.CustomBricks;

namespace Eurolook.WordAddIn.Database
{
    public class WordBrickRepository : BrickRepository, IWordBrickRepository
    {
        private readonly IUserDataRepository _userDataRepository;

        public WordBrickRepository(IUserDataRepository userDataRepository)
            : base(userDataRepository)
        {
            _userDataRepository = userDataRepository;
        }

        public BuildingBlockBrick UpdateUserBrick(
            Guid brickId,
            CreateCustomBrickViewModel viewModel,
            byte[] buildingBlockBytes)
        {
            using (var context = GetContext())
            {
                var dbBrick = context.Bricks.OfType<BuildingBlockBrick>().FirstOrDefault(x => x.Id == brickId);
                if (dbBrick != null)
                {
                    dbBrick.Acronym = viewModel.Acronym;
                    dbBrick.Name = viewModel.Name;
                    dbBrick.DisplayName = viewModel.Name;
                    dbBrick.Description = viewModel.Description;
                    if (buildingBlockBytes != null && viewModel.PreviewBytes != null)
                    {
                        dbBrick.BuildingBlockBytes = buildingBlockBytes;
                        dbBrick.PreviewImageBytes = viewModel.PreviewBytes;
                    }

                    context.SaveChanges();
                    return dbBrick;
                }

                return null;
            }
        }

        public UserBrick CreateCustomBrick(
            CreateCustomBrickViewModel viewModel,
            DocumentModel documentModel,
            byte[] buildingBlockBytes)
        {
            var user = _userDataRepository.GetUser();
            using (var context = GetContext())
            {
                var brick = new BuildingBlockBrick();
                brick.Init();
                brick.Name = viewModel.Name;
                brick.DisplayName = viewModel.Name;
                brick.Description = viewModel.Description;
                brick.Acronym = viewModel.Acronym;
                brick.ColorLuminance = Luminance.Medium;
                brick.CategoryId = CommonDataConstants.CustomBricksCategoryId;
                brick.IsMultiInstance = true;
                brick.BuildingBlockBytes = buildingBlockBytes;
                brick.PreviewImageBytes = viewModel.PreviewBytes;
                brick.ClientModification = true;
                context.Bricks.Add(brick);

                var userBrick = new UserBrick();
                userBrick.Init();
                userBrick.Brick = brick;
                userBrick.BrickId = brick.Id;
                userBrick.UserId = user.Id;
                userBrick.DocumentModelId = documentModel.Id;
                userBrick.ClientModification = true;
                context.UserBricks.Add(userBrick);

                context.SaveChanges();

                return userBrick;
            }
        }
    }
}
