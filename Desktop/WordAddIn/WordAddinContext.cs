﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.AddIn;
using Microsoft.Win32;

namespace Eurolook.WordAddIn;

internal class WordAddinContext : AddinContextBase, IWordAddinContext
{
    public WordAddinContext(IDisabledAddInsManager disabledAddInsManager)
        : base(disabledAddInsManager)
    {
    }

    public string GlobalTemplateFolder
    {
        get { return Path.Combine(InstallRoot, "Templates"); }
    }

    public string GlobalTemplatePath => Path.Combine(GlobalTemplateFolder, "Eurolook.dotm");

    public string GlobalAddInPath => Path.Combine(GlobalTemplateFolder, "EurolookGlobalAddin.dotm");

    public string QuickAccessToolbarTemplatePath =>
        Path.Combine(GlobalTemplateFolder, "EurolookQuickAccessToolbar.dotx");

    public override IEnumerable<TimeSpan> LoadTimes()
    {
        // TODO: Make sure this works across different versions of Office
        const string key = @"Software\Microsoft\Office\16.0\Word\AddInLoadTimes";

        string eurolookOfficeAddInProgId = GetAddInShimProgId();

        using (var addInLoadTimesKey = Registry.CurrentUser.OpenSubKey(key))
        {
            var value = (byte[])addInLoadTimesKey?.GetValue(eurolookOfficeAddInProgId);
            if (value != null)
            {
                int length = BitConverter.ToInt32(value, 0);
                for (int i = 0; i < length; i++)
                {
                    int loadTimeMilliseconds = BitConverter.ToInt32(value, (i + 1) * 4);
                    yield return TimeSpan.FromMilliseconds(loadTimeMilliseconds);
                }
            }
        }
    }

    protected override string GetAddInProgId()
    {
        var progIdAttribute = (ProgIdAttribute)typeof(ThisWordAddIn).GetCustomAttribute(typeof(ProgIdAttribute));
        string eurolookAddInProgId = progIdAttribute.Value;
        return eurolookAddInProgId;
    }
}
