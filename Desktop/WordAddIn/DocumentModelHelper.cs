using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Data;
using Eurolook.Data.Compatibility;
using Eurolook.Data.Models;

namespace Eurolook.WordAddIn
{
    public class DocumentModelHelper : IDocumentModelHelper
    {
        private readonly IBrickVisibilityChecker _brickVisibilityChecker;
        private readonly IBrickVersionCompatibilityTester _brickVersionCompatibilityTester;
        private readonly DocumentModel _documentModel;

        public DocumentModelHelper(
            IBrickVisibilityChecker brickVisibilityChecker,
            IBrickVersionCompatibilityTester brickVersionCompatibilityTester,
            DocumentModel documentModel)
        {
            _brickVisibilityChecker = brickVisibilityChecker;
            _brickVersionCompatibilityTester = brickVersionCompatibilityTester;
            _documentModel = documentModel;
        }

        public Brick GetBrick(Guid brickId)
        {
            var brickReferences = _documentModel.BrickReferences;
            return brickReferences
                   .Select(ds => ds.Brick)
                   .FirstOrDefault(b => _brickVersionCompatibilityTester.IsBrickCompatibleWithAddinVersion(b) && b.Id == brickId);
        }

        public List<Brick> GetBricksOfGroup(Guid groupId)
        {
            var brickReferences = _documentModel.BrickReferences;
            return brickReferences
                   .Select(ds => ds.Brick)
                   .Where(b => _brickVisibilityChecker.IsBrickVisible(b) && b.GroupId == groupId)
                   .Distinct(new IdentifiableComparer<Brick>())
                   .OrderBy(b => b.UiPositionIndex)
                   .ToList();
        }
    }
}
