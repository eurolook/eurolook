﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using Eurolook.AddIn.Common.AddIn;
using Eurolook.WordAddIn.HotKeys;

namespace Eurolook.WordAddIn
{
    /// <summary>
    /// This class is exposed by the add-in to be used from VBA
    /// using the Application.COMAddIns("Eurolook.WordAddIn.10").Object object.
    /// </summary>
    [ComVisible(true)]
    [ProgId(ComIdentifiers.ComAddInAutomationService.ProgId)]
    [Guid(ComIdentifiers.ComAddInAutomationService.ClsId)]
    [ClassInterface(ClassInterfaceType.AutoDual)]
#if NET6_0_OR_GREATER
    public class ComAddInAutomationService : IDisposable
#else
    public class ComAddInAutomationService : StandardOleMarshalObject, IDisposable
#endif 
    {
        private readonly IHotKeyManager _hotKeyManager;

        private bool _isDisposed;

        public ComAddInAutomationService(IHotKeyManager hotKeyManager)
        {
            _hotKeyManager = hotKeyManager;
        }

        ~ComAddInAutomationService()
        {
            Dispose(false);
        }

        public IHotKeyManager HotKeyManager => _hotKeyManager;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        [SuppressMessage("ReSharper", "UnusedParameter.Local", Justification = "Signature is according to the IDisposable pattern.")]
        protected virtual void Dispose(bool disposing)
        {
            if (_isDisposed)
            {
                return;
            }

            _isDisposed = true;
        }
    }
}
