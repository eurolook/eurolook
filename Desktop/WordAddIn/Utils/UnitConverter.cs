﻿namespace Eurolook.WordAddIn.Utils
{
    public static class UnitConverter
    {
        public static float CmToPoints(double value)
        {
            return (float)(value * 72 / 2.54);
        }

        public static float PointsToCm(double value)
        {
            return (float)(value / 72 * 2.54);
        }
    }
}
