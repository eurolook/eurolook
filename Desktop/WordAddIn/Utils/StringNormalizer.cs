﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Eurolook.Data.Models;

namespace Eurolook.WordAddIn.Utils
{
    public class StringNormalizer
    {
        private readonly List<CharacterMapping> _characterMappings;

        public StringNormalizer(IEnumerable<CharacterMapping> characterMappings)
        {
            _characterMappings = characterMappings.ToList();
        }

        public string Normalize(string text)
        {
            string result = RemoveDiacritics(text.Trim()).ToLowerInvariant();
            result = ApplyCharacterMapping(result).ToLowerInvariant();
            return result.Trim();
        }

        private static string RemoveDiacritics(string text)
        {
            string normalizedText = text.Normalize(NormalizationForm.FormKD);
            var sb = new StringBuilder();

            foreach (char c in normalizedText)
            {
                if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(c);
                }
            }

            return sb.ToString().Normalize(NormalizationForm.FormC);
        }

        private string ApplyCharacterMapping(string text)
        {
            string result = text;
            foreach (var mapping in _characterMappings)
            {
                result = result.Replace(mapping.Original, mapping.Mapped);
            }

            return result;
        }
    }
}
