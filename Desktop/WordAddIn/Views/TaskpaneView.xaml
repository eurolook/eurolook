<wpf:ThemedUserControl x:Class="Eurolook.WordAddIn.Views.TaskPaneView"
                         xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
                         xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
                         xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
                         xmlns:insertCrossReference="clr-namespace:Eurolook.WordAddIn.BrickCommands.InsertCrossReference"
                         xmlns:insertName="clr-namespace:Eurolook.WordAddIn.BrickCommands.InsertName"
                         xmlns:insertTable="clr-namespace:Eurolook.WordAddIn.BrickCommands.InsertTable"
                         xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                         xmlns:taskPaneMessages="clr-namespace:Eurolook.WordAddIn.TaskPaneMessages"
                         xmlns:views="clr-namespace:Eurolook.WordAddIn.Views"
                         xmlns:vm="clr-namespace:Eurolook.WordAddIn.ViewModels"
                         xmlns:wpf="clr-namespace:Eurolook.AddIn.Common.Wpf;assembly=Eurolook.AddIn.Common"
                         xmlns:viewModels="clr-namespace:Eurolook.AddIn.Common.ViewModels;assembly=Eurolook.AddIn.Common"
                         xmlns:models="clr-namespace:Eurolook.Data.Models;assembly=Eurolook.Data"
                         x:Name="EurolookTaskPane"
                         d:DataContext="{d:DesignInstance Type=vm:TaskPaneViewModel}"
                         d:DesignHeight="800"
                         d:DesignWidth="300"
                         Background="White"
                         TextOptions.TextFormattingMode="Display"
                         mc:Ignorable="d">

    <UserControl.Resources>
        <ResourceDictionary>
            <SolidColorBrush x:Key="PastelColorBrush" Opacity="0.3" Color="{DynamicResource PrimaryColor}" />
            <Storyboard x:Key="SlideIn">
                <ThicknessAnimation Storyboard.TargetProperty="Margin"
                                    To="0,0,0,0"
                                    Duration="0:0:0.33" />
            </Storyboard>
            <Storyboard x:Key="SlideOut">
                <ThicknessAnimation Storyboard.TargetProperty="Margin"
                                    To="0,0,0,-60"
                                    Duration="0:0:0.33" />
            </Storyboard>
            <ControlTemplate x:Key="TaskpaneHeader">
                <Grid Background="{DynamicResource PrimaryColorBrush}">
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="*" />
                        <ColumnDefinition Width="Auto" />
                    </Grid.ColumnDefinitions>
                    <WrapPanel Grid.Column="0" Orientation="Horizontal">
                        <Border Name="LogoContainer"
                                Width="60"
                                Height="60">
                            <Image Width="32"
                                   Height="32"
                                   Source="..\Graphics\logo_64.png" />
                        </Border>
                        <TextBlock Name="Title"
                                   Margin="0 0 0 2"
                                   Style="{StaticResource AppHeader}"
                                   Text="{Binding Converter={StaticResource ResourceConverter}, ConverterParameter=ProductName}" />
                    </WrapPanel>
                    <Button Name="AboutButton"
                            Grid.Column="1"
                            Margin="0 0 12 0"
                            HorizontalAlignment="Center"
                            VerticalAlignment="Center"
                            Command="{Binding AboutCommand}"
                            Style="{StaticResource NoButton}"
                            ToolTip="About Eurolook">
                        <wpf:HeroIcon IconPath="/Graphics/heroicons-outline/white/information-circle.svg" />
                    </Button>
                </Grid>
                <ControlTemplate.Triggers>
                    <DataTrigger Binding="{Binding RelativeSource={RelativeSource Self}, Path=ActualWidth, Converter={StaticResource LessThan}, ConverterParameter=200}" Value="True">
                        <Setter TargetName="Title" Property="Visibility" Value="Collapsed" />
                        <Setter TargetName="AboutButton" Property="Visibility" Value="Collapsed" />
                    </DataTrigger>
                </ControlTemplate.Triggers>
            </ControlTemplate>

            <ControlTemplate x:Key="TaskpaneTabBar">
                <Grid Name="TabButtons" Background="{StaticResource ToolbarBackground}">
                    <StackPanel Name="TabPanel" Orientation="Horizontal">
                        <RadioButton Name="HomeButton"
                                     GroupName="TaskpaneTabs"
                                     IsChecked="{Binding Path=ActiveTab, Mode=TwoWay, Converter={StaticResource EnumToBoolean}, ConverterParameter={x:Static vm:TaskPaneTabKind.Home}}"
                                     Style="{StaticResource RadioTabButton}">
                            Home
                        </RadioButton>
                        <RadioButton Name="BricksButton"
                                     GroupName="TaskpaneTabs"
                                     IsChecked="{Binding Path=ActiveTab, Mode=TwoWay, Converter={StaticResource EnumToBoolean}, ConverterParameter={x:Static vm:TaskPaneTabKind.Bricks}}"
                                     Style="{StaticResource RadioTabButton}"
                                     Visibility="{Binding IsBricksTabVisible, Mode=OneWay, Converter={StaticResource BooleanToVisibilityConverter}}">
                            Bricks
                        </RadioButton>
                        <RadioButton Name="ToolsButton"
                                     GroupName="TaskpaneTabs"
                                     IsChecked="{Binding Path=ActiveTab, Mode=TwoWay, Converter={StaticResource EnumToBoolean}, ConverterParameter={x:Static vm:TaskPaneTabKind.Tools}}"
                                     Style="{StaticResource RadioTabButton}"
                                     Visibility="{Binding IsToolsTabVisible, Mode=OneWay, Converter={StaticResource BooleanToVisibilityConverter}}">
                            Tools
                        </RadioButton>
                    </StackPanel>
                </Grid>
                <ControlTemplate.Triggers>
                    <DataTrigger Binding="{Binding RelativeSource={RelativeSource Self}, Path=ActualWidth, Converter={StaticResource LessThan}, ConverterParameter=200}" Value="True">
                        <Setter TargetName="TabPanel" Property="Orientation" Value="Vertical" />
                        <Setter TargetName="HomeButton" Property="HorizontalAlignment" Value="Stretch" />
                        <Setter TargetName="BricksButton" Property="HorizontalAlignment" Value="Stretch" />
                        <Setter TargetName="ToolsButton" Property="HorizontalAlignment" Value="Stretch" />
                    </DataTrigger>
                </ControlTemplate.Triggers>
            </ControlTemplate>

            <DataTemplate DataType="{x:Type viewModels:InfoTileViewModel}">
                <Button AutomationProperties.AutomationId="{Binding AutomationId}"
                        Command="{Binding TileCommand}"
                        Style="{StaticResource InfoTileButtonStyle}"
                        ToolTipService.BetweenShowDelay="0"
                        ToolTipService.InitialShowDelay="1200"
                        ToolTipService.Placement="Bottom"
                        ToolTipService.ShowDuration="5000">
                    <Button.ToolTip>
                        <Control Template="{StaticResource TileToolTipContent}" />
                    </Button.ToolTip>
                </Button>
            </DataTemplate>

            <DataTemplate DataType="{x:Type vm:BricksTabViewModel}">
                <views:BricksTabView />
            </DataTemplate>

            <DataTemplate DataType="{x:Type vm:ToolsTabViewModel}">
                <views:ToolsTabView />
            </DataTemplate>

            <DataTemplate DataType="{x:Type insertName:InsertNameViewModel}">
                <insertName:InsertNameView />
            </DataTemplate>
            
            <DataTemplate DataType="{x:Type insertCrossReference:InsertCrossReferenceViewModel}">
                <insertCrossReference:InsertCrossReferenceView />
            </DataTemplate>

            <DataTemplate DataType="{x:Type insertTable:InsertTableViewModel}">
                <insertTable:InsertTableView />
            </DataTemplate>

        </ResourceDictionary>
    </UserControl.Resources>

    <Grid Focusable="False">
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="*" />
        </Grid.ColumnDefinitions>
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto" />
            <RowDefinition Height="Auto" />
            <RowDefinition Height="*" />
            <RowDefinition Height="Auto" />
        </Grid.RowDefinitions>

        <Control Grid.Row="0"
                 Focusable="False"
                 Template="{StaticResource TaskpaneHeader}" />

        <Control Grid.Row="1" Template="{StaticResource TaskpaneTabBar}" />

        <Grid Name="TabPanels"
              Grid.Row="2"
              Background="{StaticResource BackgroundColor}">

            <!--  HOME Tab  -->
            <Grid Visibility="{Binding Path=ActiveTab, Mode=OneWay, Converter={StaticResource EnumToVisibility}, ConverterParameter={x:Static vm:TaskPaneTabKind.Home}}">
                <ScrollViewer Name="ScrollViewerHome"
                              HorizontalScrollBarVisibility="Disabled"
                              PanningMode="Both"
                              VerticalScrollBarVisibility="Auto">
                    <ItemsControl Margin="12" ItemsSource="{Binding HomeTabViewModel.Tiles}" />
                </ScrollViewer>
            </Grid>

            <!--  BRICKS tab  -->
            <ContentControl Content="{Binding BricksTabViewModel}" Visibility="{Binding Path=ActiveTab, Mode=OneWay, Converter={StaticResource EnumToVisibility}, ConverterParameter={x:Static vm:TaskPaneTabKind.Bricks}}" />

            <!--  TOOLS tab  -->
            <ContentControl Content="{Binding ToolsTabViewModel}" Visibility="{Binding Path=ActiveTab, Mode=OneWay, Converter={StaticResource EnumToVisibility}, ConverterParameter={x:Static vm:TaskPaneTabKind.Tools}}" />
        </Grid>

        <!-- Notifications -->
        <ItemsControl ItemsSource="{Binding Messages}" Grid.Row="3"
                      VerticalAlignment="Bottom" Margin="14 8"
                      Visibility="{Binding Messages, Converter={StaticResource NullOrEmptyToVisibilityConverter}}">
            <ItemsControl.ItemsPanel>
                <ItemsPanelTemplate>
                    <StackPanel VerticalAlignment="Bottom" />
                </ItemsPanelTemplate>
            </ItemsControl.ItemsPanel>
            <ItemsControl.ItemTemplate>
                <DataTemplate DataType="taskPaneMessages:TaskPaneMessageViewModel">
                    <Border Name="PART_Base" Margin="0 8 0 0">
                        <Border.Style>
                            <Style TargetType="Border">
                                <Style.Triggers>
                                    <DataTrigger Binding="{Binding Priority}" Value="{x:Static models:NotificationPriority.Low}">
                                        <Setter Property="Background" Value="{DynamicResource ControlBackground}" />
                                    </DataTrigger>
                                    <DataTrigger Binding="{Binding Priority}" Value="{x:Static models:NotificationPriority.Normal}">
                                        <Setter Property="Background" Value="{DynamicResource PastelColorBrush}" />
                                    </DataTrigger>
                                    <DataTrigger Binding="{Binding Priority}" Value="{x:Static models:NotificationPriority.High}">
                                        <Setter Property="Background" Value="{DynamicResource Highlighted}" />
                                    </DataTrigger>
                                    <EventTrigger RoutedEvent="Loaded">
                                        <EventTrigger.Actions>
                                            <BeginStoryboard>
                                                <Storyboard>
                                                    <DoubleAnimation Duration="0:0:1"
                                                                     Storyboard.TargetProperty="Opacity"
                                                                     From="0.0"  To="1.0" />
                                                </Storyboard>
                                            </BeginStoryboard>
                                        </EventTrigger.Actions>
                                    </EventTrigger>
                                </Style.Triggers>
                            </Style>
                        </Border.Style>
                        <Grid>
                            <Grid.RowDefinitions>
                                <RowDefinition Height="Auto" />
                                <RowDefinition Height="Auto" />
                                <RowDefinition Height="Auto" />
                            </Grid.RowDefinitions>

                            <Grid Grid.Row="0" Margin="12 8">
                                <Grid.ColumnDefinitions>
                                    <ColumnDefinition Width="Auto" />
                                    <ColumnDefinition Width ="*" />
                                    <ColumnDefinition Width="Auto" />
                                </Grid.ColumnDefinitions>

                                <TextBlock Name="PART_Subject"
                                           Grid.Column="0"
                                           FontSize="16" FontWeight="ExtraLight"
                                           HorizontalAlignment="Stretch"
                                           Foreground="{DynamicResource PrimaryColorBrush}"
                                           Background="Transparent"
                                           Style="{StaticResource PureContentText}"
                                           Text="{Binding Subject, FallbackValue=Subject}"
                                           TextTrimming="CharacterEllipsis" TextWrapping="NoWrap"
                                           Visibility="{Binding Subject, Converter={StaticResource EmptyStringToVisibilityConverter}}" />
                                <Button Name="PART_CloseButton"
                                        Grid.Column="2"
                                        Padding="0"
                                        Margin="0 2 0 0"
                                        Height="20" Width="20"
                                        Style="{StaticResource HollowButton}"
                                        HorizontalAlignment="Right"
                                        VerticalAlignment="Top"
                                        Visibility="{Binding ShowClearButton, Converter={StaticResource BooleanToVisibilityConverter}}"
                                        Command="{Binding ClearCommand}">
                                    <Grid>
                                        <wpf:HeroIcon Name="PART_CloseIcon_Black" IconPath="/Graphics/heroicons-outline/black/x-mark.svg" Size="16" />
                                        <wpf:HeroIcon Name="PART_CloseIcon_White" IconPath="/Graphics/heroicons-outline/white/x-mark.svg" Size="16" Visibility="Collapsed" />
                                    </Grid>
                                </Button>
                            </Grid>

                            <ScrollViewer Grid.Row="1" Margin="0 0 0 12"
                                          MaxHeight="80"
                                          VerticalScrollBarVisibility="Auto">
                                <TextBlock  Name="PART_Message"
                                            Style="{StaticResource PureContentText}"
                                            Background="Transparent" Margin="12 0"
                                            Visibility="{Binding Body, Converter={StaticResource EmptyStringToVisibilityConverter}}"
                                            wpf:TextBlockLinkableTextExtension.Message="{Binding Body, FallbackValue=Message}" />
                            </ScrollViewer>

                            <TextBlock Name="PART_Thumbnail"
                                       Grid.Row="2" Padding="12 0 12 12"
                                       MaxHeight="80"
                                       Visibility="{Binding Thumbnail, Converter={StaticResource NullOrEmptyToVisibilityConverter}}">
                                <Image HorizontalAlignment="Left"
                                        Source="{Binding Thumbnail}" />
                            </TextBlock>

                            <Polygon Grid.RowSpan="3"
                                     Points="0,0 0,20 20,0"
                                     Margin="0"
                                     VerticalAlignment="Bottom"
                                     HorizontalAlignment="Right"
                                     StrokeThickness="1"
                                     Stroke="{DynamicResource ControlBackground}"
                                     Fill="{DynamicResource ControlBackground}">
                                <Polygon.Style>
                                    <Style TargetType="Polygon">
                                        <Style.Triggers>
                                            <DataTrigger Binding="{Binding Priority}" Value="{x:Static models:NotificationPriority.Low}">
                                                <Setter Property="Visibility" Value="Hidden" />
                                            </DataTrigger>
                                        </Style.Triggers>
                                    </Style>
                                </Polygon.Style>
                            </Polygon>
                            <Polygon Grid.RowSpan="3"
                                     Points="0,20 20,20 20,0"
                                     Margin="0"
                                     VerticalAlignment="Bottom"
                                     HorizontalAlignment="Right"
                                     StrokeThickness="1"
                                     Stroke="White"
                                     Fill="White">
                                <Polygon.Style>
                                    <Style TargetType="Polygon">
                                        <Style.Triggers>
                                            <DataTrigger Binding="{Binding Priority}" Value="{x:Static models:NotificationPriority.Low}">
                                                <Setter Property="Visibility" Value="Hidden" />
                                            </DataTrigger>
                                        </Style.Triggers>
                                    </Style>
                                </Polygon.Style>
                            </Polygon>
                        </Grid>

                    </Border>
                    <DataTemplate.Triggers>
                        <DataTrigger Binding="{Binding IsHighlighted}" Value="True">
                            <Setter TargetName="PART_Base" Property="Background" Value="{StaticResource Highlighted}" />
                            <Setter TargetName="PART_CloseButton" Property="Style" Value="{StaticResource InverseHollowButton}" />
                            <Setter TargetName="PART_Subject" Property="Foreground" Value="#ffffff" />
                            <Setter TargetName="PART_Message" Property="Foreground" Value="#ffffff" />
                            <Setter TargetName="PART_Thumbnail" Property="Foreground" Value="#ffffff" />
                            <Setter TargetName="PART_CloseIcon_Black" Property="Visibility" Value="Collapsed" />
                            <Setter TargetName="PART_CloseIcon_White" Property="Visibility" Value="Visible" />
                        </DataTrigger>
                    </DataTemplate.Triggers>
                </DataTemplate>
            </ItemsControl.ItemTemplate>
        </ItemsControl>

        <!-- Profile Initialization -->
        <Border Grid.Row="3" Height="60" Margin="0 0 0 -60"
                        VerticalAlignment="Bottom" Background="{DynamicResource PrimaryColorBrush}">
            <Border.Style>
                <Style TargetType="Border">
                    <Style.Triggers>
                        <DataTrigger Binding="{Binding HomeTabViewModel.InitializationStatusVm.SlidePosition}" Value="1">
                            <DataTrigger.EnterActions>
                                <RemoveStoryboard BeginStoryboardName="SlideInBegin" />
                                <RemoveStoryboard BeginStoryboardName="SlideOutBegin" />
                                <BeginStoryboard Name="SlideInBegin" Storyboard="{StaticResource SlideIn}" />
                            </DataTrigger.EnterActions>
                        </DataTrigger>
                        <DataTrigger Binding="{Binding HomeTabViewModel.InitializationStatusVm.SlidePosition}" Value="2">
                            <DataTrigger.EnterActions>
                                <RemoveStoryboard BeginStoryboardName="SlideInBegin" />
                                <RemoveStoryboard BeginStoryboardName="SlideOutBegin" />
                                <BeginStoryboard Name="SlideOutBegin" Storyboard="{StaticResource SlideOut}" />
                            </DataTrigger.EnterActions>
                        </DataTrigger>
                    </Style.Triggers>
                </Style>
            </Border.Style>
            <StackPanel Margin="15 0">
                <TextBlock Margin="0 12 0 6"
                                   Foreground="White"
                                   Style="{StaticResource PureContentText}"
                                   Text="{Binding HomeTabViewModel.InitializationStatusVm.UpdateDetailsMessage}" />
                <ProgressBar Height="10"
                                     VerticalAlignment="Top"
                                     BorderThickness="0"
                                     Foreground="{DynamicResource GreenColor}"
                                     IsIndeterminate="{Binding HomeTabViewModel.InitializationStatusVm.IsInitializing}">
                    <ProgressBar.Style>
                        <Style TargetType="ProgressBar">
                            <Setter Property="Background" Value="{StaticResource ControlBackground}" />
                            <Style.Triggers>
                                <DataTrigger Binding="{Binding HomeTabViewModel.InitializationStatusVm.IsFailed}" Value="True">
                                    <Setter Property="Background" Value="{StaticResource RedColor}" />
                                </DataTrigger>
                            </Style.Triggers>
                        </Style>
                    </ProgressBar.Style>
                </ProgressBar>
            </StackPanel>
        </Border>
    </Grid>
</wpf:ThemedUserControl>