using System;
using System.Windows.Controls;
using Eurolook.OfficeNotificationBar;

namespace Eurolook.WordAddIn.Views
{
    public partial class BusyIndicatorLightBox : ILightBoxContent
    {
        public BusyIndicatorLightBox()
        {
            InitializeComponent();
        }

        public event RequestClosingEvent RequestClosing;

        public UserControl UserControl
        {
            get { return this; }
        }

        public void OnRequestClosing()
        {
            RequestClosing?.Invoke(this, new EventArgs());
        }
    }
}
