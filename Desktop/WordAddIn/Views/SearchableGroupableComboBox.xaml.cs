﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.Common.Extensions;
using Eurolook.WordAddIn.ViewModels;

namespace Eurolook.WordAddIn.Views
{
    public partial class SearchableGroupableComboBox
    {
        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register(
                nameof(SelectedItem),
                typeof(object),
                typeof(SearchableGroupableComboBox),
                new FrameworkPropertyMetadata(
                    default(object),
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault | FrameworkPropertyMetadataOptions.Inherits));

        public static readonly DependencyProperty FilterButtonIconProperty =
            DependencyProperty.Register(
                nameof(FilterButtonIcon),
                typeof(string),
                typeof(SearchableGroupableComboBox),
                new FrameworkPropertyMetadata(
                    default(string),
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault | FrameworkPropertyMetadataOptions.Inherits));

        public static readonly DependencyProperty FilterButtonTooltipProperty =
            DependencyProperty.Register(
                nameof(FilterButtonTooltip),
                typeof(string),
                typeof(SearchableGroupableComboBox),
                new FrameworkPropertyMetadata(
                    default(string),
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault | FrameworkPropertyMetadataOptions.Inherits));

        public static readonly DependencyProperty IsFilterAppliedProperty =
            DependencyProperty.Register(
                nameof(IsFilterApplied),
                typeof(bool),
                typeof(SearchableGroupableComboBox),
                new FrameworkPropertyMetadata(
                    default(bool),
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault | FrameworkPropertyMetadataOptions.Inherits));

        public static readonly DependencyProperty IsFilterButtonVisibleProperty =
            DependencyProperty.Register(
                nameof(IsFilterButtonVisible),
                typeof(bool),
                typeof(SearchableGroupableComboBox),
                new FrameworkPropertyMetadata(
                    default(bool),
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault | FrameworkPropertyMetadataOptions.Inherits));

        public bool IsFilterButtonVisible
        {
            get { return (bool)GetValue(IsFilterButtonVisibleProperty); }
            set { SetValue(IsFilterButtonVisibleProperty, value); }
        }

        public bool IsFilterApplied
        {
            get { return (bool)GetValue(IsFilterAppliedProperty); }
            set { SetValue(IsFilterAppliedProperty, value); }
        }

        public string FilterButtonIcon
        {
            get { return GetValue(FilterButtonIconProperty) as string; }
            set { SetValue(FilterButtonIconProperty, value); }
        }

        public string FilterButtonTooltip
        {
            get { return GetValue(FilterButtonTooltipProperty) as string; }
            set { SetValue(FilterButtonTooltipProperty, value); }
        }

        public SearchableGroupableComboBox()
        {
            InitializeComponent();
        }

        public object SelectedItem
        {
            get { return GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.F4:
                    {
                        if (!PopHeadingList.IsOpen)
                        {
                            OpenPopupToggleButton.IsChecked = true;
                        }

                        break;
                    }

                case Key.Down:
                    {
                        var cvs = CollectionViewSource.GetDefaultView(DocumentModelListBox.ItemsSource);
                        cvs.MoveCurrentToNext();
                        if (cvs.IsCurrentAfterLast)
                        {
                            cvs.MoveCurrentToLast();
                        }

                        AssertGroupIsExpanded(DocumentModelListBox.SelectedItem as ISearchableGroupableItem);

                        DocumentModelListBox.ScrollIntoView(DocumentModelListBox.SelectedItem);
                        break;
                    }

                case Key.Up:
                    {
                        var cvs = CollectionViewSource.GetDefaultView(DocumentModelListBox.ItemsSource);
                        cvs.MoveCurrentToPrevious();
                        if (cvs.IsCurrentBeforeFirst)
                        {
                            // make sure the group header of the first group is made visible
                            DocumentModelListBox.FindVisualChildren<ScrollViewer>().FirstOrDefault()?.ScrollToTop();

                            cvs.MoveCurrentToFirst();
                        }

                        AssertGroupIsExpanded(DocumentModelListBox.SelectedItem as ISearchableGroupableItem);

                        DocumentModelListBox.ScrollIntoView(DocumentModelListBox.SelectedItem);
                        break;
                    }

                case Key.Escape:
                    {
                        if (PopHeadingList.IsOpen)
                        {
                            CancelPopup();
                            e.Handled = true;
                        }

                        break;
                    }

                case Key.Return:
                    {
                        if (PopHeadingList.IsOpen)
                        {
                            CommitPopup();
                            e.Handled = true;
                        }

                        break;
                    }
            }

            base.OnPreviewKeyDown(e);
        }

        private void OpenPopupToggleButton_OnChecked(object sender, RoutedEventArgs e)
        {
            OpenPopup();
        }

        private void ListBox_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            CommitPopup();
        }

        private void DocumentModelsCollectionViewSource_OnFilter(object sender, FilterEventArgs e)
        {
            var item = (ISearchableGroupableItem)e.Item;
            string searchText = SearchBox.Text.Trim();

            if (string.IsNullOrWhiteSpace(searchText))
            {
                e.Accepted = true;
                return;
            }

            var searchTerms = searchText.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            e.Accepted = searchTerms.All(
                t => item.DisplayName?.Contains(t, StringComparison.CurrentCultureIgnoreCase) == true
                     || item.Keywords?.Contains(t, StringComparison.CurrentCultureIgnoreCase) == true);
        }

        private void SearchBox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(DocumentModelListBox.ItemsSource).Refresh();
        }

        private void OpenPopup()
        {
            SearchBox.Text = string.Empty;
            SearchBox.Focus();
            DocumentModelListBox.SelectedItem = SelectedItem;
            if (SelectedItem != null)
            {
                DocumentModelListBox.ScrollIntoView(DocumentModelListBox.SelectedItem);
            }
        }

        private void CancelPopup()
        {
            ClosePopup();
        }

        private void CommitPopup()
        {
            SelectedItem = DocumentModelListBox.SelectedItem;
            ClosePopup();
        }

        private void ClosePopup()
        {
            OpenPopupToggleButton.IsChecked = false;
            SearchBox.Text = string.Empty;
        }

        private void AssertGroupIsExpanded(ISearchableGroupableItem item)
        {
            if (item == null)
            {
                return;
            }

            var collectionViewGroup = DocumentModelListBox.Items.Groups?.OfType<CollectionViewGroup>()
                                            .FirstOrDefault(
                                                g => Equals(g.Name, item.GroupBy));
            if (collectionViewGroup == null)
            {
                return;
            }

            {
                var groupItem = DocumentModelListBox.ItemContainerGenerator.ContainerFromItem(collectionViewGroup);
                var exp = groupItem.FindVisualChildren<Expander>().FirstOrDefault();
                if (exp != null)
                {
                    exp.IsExpanded = true;
                }
            }
        }
    }
}
