﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.WordAddIn.ViewModels;

namespace Eurolook.WordAddIn.Views
{
    public partial class TaskPaneView
    {
        public TaskPaneView(IWordApplicationContext applicationContext, TaskPaneViewModel taskPaneViewModel)
        {
            InitializeComponent();

            DataContext = taskPaneViewModel;

            // FIX: Prevent task pane from getting keyboard focus
            //   (otherwise certain shortcut won't work)
            PreviewGotKeyboardFocus += (_, args) =>
                                       {
                                           if (args.NewFocus is ButtonBase)
                                           {
                                               args.Handled = true;
                                           }
                                       };

            PreviewMouseWheel += OnPreviewMouseWheelTaskPane;
            applicationContext.InputDetector.PreviewMouseWheel += OnPreviewMouseWheelWord;
        }

        private void OnPreviewMouseWheelTaskPane(object sender, MouseWheelEventArgs mouseWheelEventArgs)
        {
            foreach (var scrollViewer in this.FindVisualChildren<ScrollViewer>())
            {
                if (scrollViewer.IsVisible && scrollViewer.IsMouseOver)
                {
                    double offset = scrollViewer.VerticalOffset;
                    scrollViewer.ScrollToVerticalOffset(offset - (mouseWheelEventArgs.Delta / 5.0));
                    mouseWheelEventArgs.Handled = true;
                    break;
                }
            }
        }

        private void OnPreviewMouseWheelWord(
            object sender,
            Eurolook.AddIn.Common.Events.MouseWheelEventArgs mouseWheelEventArgs)
        {
            if (IsMouseOver)
            {
                foreach (var scrollViewer in this.FindVisualChildren<ScrollViewer>())
                {
                    if (scrollViewer.IsVisible && scrollViewer.IsMouseOver)
                    {
                        double offset = scrollViewer.VerticalOffset;
                        scrollViewer.ScrollToVerticalOffset(offset - (mouseWheelEventArgs.Delta / 5.0));
                        mouseWheelEventArgs.Handled = true;
                        break;
                    }
                }
            }
        }

        private void LinkClicked(object sender, RoutedEventArgs e)
        {
            if (sender is Hyperlink hyperlink
                && (hyperlink.NavigateUri.Scheme == "http" || hyperlink.NavigateUri.Scheme == "https"))
            {
                Process.Start(hyperlink.NavigateUri.ToString());
            }
        }
    }
}
