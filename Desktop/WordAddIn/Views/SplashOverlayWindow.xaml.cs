using System.Windows;
using System.Windows.Input;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.OfficeNotificationBar;
using Application = Microsoft.Office.Interop.Word.Application;

namespace Eurolook.WordAddIn.Views
{
    public partial class SplashOverlayWindow : LightBoxWindow
    {
        public SplashOverlayWindow()
        {
            InitializeComponent();
        }

        public SplashOverlayWindow(Application application)
            : base(application, application.GetActiveWindow(), null, WindowExtents.DockRight | WindowExtents.DockLeft)
        {
            InitializeComponent();

            // load splash image from disk
            ////var splashFilePath = Path.GetDirectoryName(typeof(SplashOverlayWindow).Assembly.CodeBase);
            ////var splashFileName = Path.Combine(splashFilePath, "Graphics", "ELTaskPaneButton.png");

            ////var bmImage = new BitmapImage();
            ////bmImage.BeginInit();
            ////bmImage.UriSource = new Uri(splashFileName, UriKind.Absolute);
            ////bmImage.EndInit();
            ////imgSplash.MaxHeight = bmImage.Height;
            ////imgSplash.MaxWidth = bmImage.Width;
            ////imgSplash.Source = bmImage;
        }

        private void SplashOverlayWindowOnKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape:
                case Key.Return:
                    CloseAndFadeOut();
                    break;
            }
        }

        private void OnSkipClick(object sender, RoutedEventArgs e)
        {
            CloseAndFadeOut();
        }
    }
}
