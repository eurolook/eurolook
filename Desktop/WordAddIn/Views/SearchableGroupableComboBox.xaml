<UserControl x:Class="Eurolook.WordAddIn.Views.SearchableGroupableComboBox"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:componentModel="clr-namespace:System.ComponentModel;assembly=WindowsBase"
             xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
             xmlns:viewModels="clr-namespace:Eurolook.WordAddIn.ViewModels"
             xmlns:views="clr-namespace:Eurolook.WordAddIn.Views"
             xmlns:i="http://schemas.microsoft.com/expression/2010/interactivity"
             xmlns:wpf="clr-namespace:Eurolook.AddIn.Common.Wpf;assembly=Eurolook.AddIn.Common"
             x:Name="UserControl"
             d:DataContext="{d:DesignInstance d:Type=viewModels:CreateDocumentViewModel}"
             d:DesignHeight="30"
             d:DesignWidth="300"
             mc:Ignorable="d">

    <UserControl.Resources>
        <ResourceDictionary>
            <ResourceDictionary.MergedDictionaries>
                <ResourceDictionary>
                    <CollectionViewSource x:Key="DocumentModelsCollectionViewSource"
                                          Filter="DocumentModelsCollectionViewSource_OnFilter"
                                          Source="{Binding Items}">
                        <CollectionViewSource.GroupDescriptions>
                            <PropertyGroupDescription PropertyName="GroupBy" />
                        </CollectionViewSource.GroupDescriptions>
                        <CollectionViewSource.SortDescriptions>
                            <componentModel:SortDescription PropertyName="SortBy" />
                            <componentModel:SortDescription PropertyName="ThenBy" />
                        </CollectionViewSource.SortDescriptions>
                    </CollectionViewSource>

                    <SolidColorBrush x:Key="Item.MouseOver.Background" Color="#3DDADADA" />
                    <SolidColorBrush x:Key="Item.MouseOver.Border" Color="{DynamicResource PrimaryColor}" />
                    <SolidColorBrush x:Key="Item.SelectedInactive.Background" Color="#3DDADADA" />
                    <SolidColorBrush x:Key="Item.SelectedInactive.Border" Color="#FFDADADA" />

                </ResourceDictionary>
            </ResourceDictionary.MergedDictionaries>
        </ResourceDictionary>
    </UserControl.Resources>

    <Grid>
        <Popup x:Name="PopHeadingList"
               MaxHeight="350"
               AllowsTransparency="True"
               IsOpen="True"
               Placement="Bottom"
               PopupAnimation="Slide"
               StaysOpen="False"
               VerticalOffset="-1">
            <Border BorderThickness="1,0,1,1" BorderBrush="{DynamicResource PrimaryColorBrush}"
                    MinWidth="{Binding ActualWidth, ElementName=UserControl}"
                    MaxHeight="{Binding MaxHeight, ElementName=UserControl}">
                <Grid Background="{StaticResource ControlBackground}">
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="*" />
                        <ColumnDefinition Width="Auto" />
                    </Grid.ColumnDefinitions>
                    <Grid.RowDefinitions>
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="*" />
                    </Grid.RowDefinitions>

                    <TextBox x:Name="SearchBox" Grid.Column="0" Grid.Row="0" Margin="4"
                             Style="{StaticResource SearchBox}"
                             TextChanged="SearchBox_OnTextChanged" />

                    <ToggleButton x:Name="FilterButton" Grid.Column="1" Grid.Row="0" Margin="0 0 4 0" Padding="4 0"
                                  ToolTip="{Binding FilterButtonTooltip, RelativeSource={RelativeSource AncestorType=views:SearchableGroupableComboBox, Mode=FindAncestor}}"
                                  IsChecked="{Binding IsFilterApplied, RelativeSource={RelativeSource AncestorType=views:SearchableGroupableComboBox, Mode=FindAncestor}}"
                                  Visibility="{Binding IsFilterButtonVisible, Converter={StaticResource BooleanToVisibilityConverter}, RelativeSource={RelativeSource AncestorType=views:SearchableGroupableComboBox, Mode=FindAncestor}}"
                                  Style="{StaticResource HollowToggleButton}">
                        <wpf:HeroIcon IconPath="{Binding FilterButtonIcon, RelativeSource={RelativeSource AncestorType=views:SearchableGroupableComboBox, Mode=FindAncestor}}" Size="16" />
                    </ToggleButton>

                    <ListBox x:Name="DocumentModelListBox" Grid.ColumnSpan="2"
                         Grid.Row="1" Grid.Column="0" BorderThickness="0"
                         ItemsSource="{Binding Source={StaticResource DocumentModelsCollectionViewSource}}"
                         MouseUp="ListBox_OnMouseUp">
                        <ListBox.GroupStyle>
                            <GroupStyle>
                                <GroupStyle.ContainerStyle>
                                    <Style TargetType="{x:Type GroupItem}">
                                        <Setter Property="Template">
                                            <Setter.Value>
                                                <ControlTemplate>
                                                    <Expander FontSize="18px"
                                                              Header="{Binding Name}"
                                                              SnapsToDevicePixels="True"
                                                              IsExpanded="True" Style="{StaticResource CategoryExpander}">
                                                        <ItemsPresenter />
                                                    </Expander>
                                                </ControlTemplate>
                                            </Setter.Value>
                                        </Setter>
                                    </Style>
                                </GroupStyle.ContainerStyle>
                            </GroupStyle>
                        </ListBox.GroupStyle>
                        <ListBox.ItemContainerStyle>
                            <Style TargetType="ListBoxItem">
                                <Setter Property="BorderThickness" Value="0" />
                                <Setter Property="Margin" Value="0" />
                                <Setter Property="Background" Value="White" />
                            </Style>
                        </ListBox.ItemContainerStyle>
                        <ListBox.ItemTemplate>
                            <DataTemplate DataType="viewModels:ISearchableGroupableItem">
                                <Border x:Name="ItemBorder"
                                        AutomationProperties.AutomationId="{Binding Id}"
                                        Height="Auto"
                                        MinHeight="30"
                                        MaxHeight="{TemplateBinding MaxHeight}"
                                        HorizontalAlignment="Stretch"
                                        Background="Transparent"
                                        SnapsToDevicePixels="true">
                                    <TextBlock x:Name="ItemTextBlock"
                                               Height="Auto"
                                               Padding="27 0 0 0"
                                               HorizontalAlignment="Stretch"
                                               FontSize="12"
                                               Style="{StaticResource ButtonText}"
                                               Text="{Binding DisplayName}"
                                               ToolTip="{Binding Description}" />
                                </Border>
                            </DataTemplate>
                        </ListBox.ItemTemplate>
                    </ListBox>
                </Grid>
            </Border>
        </Popup>

        <ToggleButton x:Name="OpenPopupToggleButton"
                      Width="{Binding ElementName=UserControl, Path=Width}"
                      Height="30"
                      Margin="0"
                      Background="{StaticResource ControlBackground}"
                      BorderBrush="{StaticResource ControlBorder}"
                      Checked="OpenPopupToggleButton_OnChecked"
                      Foreground="{StaticResource ControlForeground}"
                      IsChecked="{Binding ElementName=PopHeadingList, Path=IsOpen, Mode=OneWayToSource}">
            <ToggleButton.Template>
                <ControlTemplate TargetType="{x:Type ToggleButton}">
                    <Grid>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="*" />
                            <ColumnDefinition Width="30" />
                        </Grid.ColumnDefinitions>

                        <Border x:Name="PART_Base"
                                Grid.Column="0"
                                Grid.ColumnSpan="2"
                                Background="{TemplateBinding Background}"
                                BorderBrush="{TemplateBinding BorderBrush}"
                                BorderThickness="1"
                                RenderOptions.BitmapScalingMode="NearestNeighbor"
                                SnapsToDevicePixels="True"
                                Style="{StaticResource BorderWithHighlighting}" />

                        <TextBlock Grid.Column="0"
                                   Padding="8 0"
                                   HorizontalAlignment="Stretch"
                                   FontSize="12"
                                   Style="{StaticResource ButtonText}"
                                   Text="{Binding Path=SelectedItem.DisplayName}" />
                        <wpf:HeroIcon Grid.Column="1" IconPath="/Graphics/heroicons-outline/black/chevron-down.svg" Size="16" />
                    </Grid>
                    <ControlTemplate.Triggers>
                        <Trigger Property="IsFocused" Value="True">
                            <Setter TargetName="PART_Base" Property="BorderBrush" Value="{DynamicResource PrimaryColorBrush}" />
                        </Trigger>
                        <Trigger Property="IsChecked" Value="True">
                            <Setter TargetName="PART_Base" Property="BorderBrush" Value="{DynamicResource PrimaryColorBrush}" />
                        </Trigger>
                        <Trigger Property="IsEnabled" Value="false">
                            <Setter Property="Foreground" Value="{StaticResource ControlForegroundDisabled}" />
                            <Setter TargetName="PART_Base" Property="BorderBrush" Value="{StaticResource ControlBorderDisabled}" />
                            <Setter TargetName="PART_Base" Property="Background" Value="{StaticResource ControlBackgroundDisabled}" />
                        </Trigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </ToggleButton.Template>
        </ToggleButton>
    </Grid>
</UserControl>