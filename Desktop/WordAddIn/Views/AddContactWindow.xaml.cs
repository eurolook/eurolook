using Eurolook.WordAddIn.ViewModels;

namespace Eurolook.WordAddIn.Views
{
    public partial class AddContactWindow
    {
        public AddContactWindow()
        {
            InitializeComponent();
        }

        public AddContactWindow(AddContactViewModel viewModel)
        {
            DataContext = viewModel;

            if (viewModel.CloseAction == null)
            {
                viewModel.CloseAction = Close;
            }

            InitializeComponent();
        }
    }
}
