﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Documents;
using Eurolook.WordAddIn.ViewModels;

namespace Eurolook.WordAddIn.Views
{
    public partial class AboutWindow : Window
    {
        public AboutWindow(AboutWindowViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void LinkClicked(object sender, RoutedEventArgs e)
        {
            if (sender is Hyperlink hyperlink)
            {
                Process.Start(hyperlink.NavigateUri.ToString());
            }
        }
    }
}
