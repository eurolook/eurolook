using System.Threading.Tasks;
using System.Windows;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.WordAddIn.DocumentCreation;
using Eurolook.WordAddIn.ViewModels;

namespace Eurolook.WordAddIn.Views
{
    public partial class CreateDocumentWindow : ICanLog
    {
        public CreateDocumentViewModel ViewModel => DataContext as CreateDocumentViewModel;

        public CreateDocumentWindow(CreateDocumentViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
            Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            ViewModel.LoadAsync().FireAndForgetSafeAsync(this.LogError);
        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OkButtonClick(object sender, RoutedEventArgs e)
        {
            CreateDocumentAndCloseWindowAsync().FireAndForgetSafeAsync(this.LogError);
        }

        private async Task CreateDocumentAndCloseWindowAsync()
        {
            // make window topmost so that we have the illusion that the new document is created in the background
            Topmost = true;
            try
            {
                await ViewModel.CreateDocumentFunc();
                Close();
            }
            catch (TemplateOrLanguageNoLongerAvailableException)
            {
                ViewModel.IsCreating = false;
            }
        }
    }
}
