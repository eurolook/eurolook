<ResourceDictionary xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
                    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
                    xmlns:i="http://schemas.microsoft.com/expression/2010/interactivity"
                    xmlns:wpf="clr-namespace:Eurolook.AddIn.Common.Wpf;assembly=Eurolook.AddIn.Common">

    <!--  Tiles  -->

    <Style x:Key="TileGroup" TargetType="{x:Type WrapPanel}">
        <Setter Property="Margin" Value="0 0 0 10px" />
        <Setter Property="Focusable" Value="False" />
    </Style>

    <Style x:Key="TileText" TargetType="{x:Type TextBlock}">
        <Setter Property="Foreground" Value="{StaticResource TileForeground}" />
        <Setter Property="Background" Value="Transparent" />
        <Setter Property="FontFamily" Value="Segoe UI" />
        <Setter Property="FontSize" Value="12" />
        <Setter Property="LineHeight" Value="14" />
        <Setter Property="LineStackingStrategy" Value="BlockLineHeight" />
        <Setter Property="FontWeight" Value="Normal" />
        <Setter Property="HorizontalAlignment" Value="Left" />
        <Setter Property="VerticalAlignment" Value="Bottom" />
        <Setter Property="Focusable" Value="False" />
    </Style>

    <Style x:Key="Tile" TargetType="{x:Type Button}">
        <Setter Property="SnapsToDevicePixels" Value="True" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type Button}">
                    <Border x:Name="PART_Border"
                            Padding="{TemplateBinding Padding}"
                            Background="{TemplateBinding Background}"
                            BorderBrush="Transparent"
                            BorderThickness="2"
                            ClipToBounds="True"
                            SnapsToDevicePixels="True">
                        <ContentPresenter Content="{TemplateBinding Content}" />
                    </Border>
                    <ControlTemplate.Triggers>
                        <Trigger Property="IsMouseOver" Value="True">
                            <Setter TargetName="PART_Border" Property="BorderBrush"
                                    Value="{StaticResource TileBorderOver}" />
                        </Trigger>
                        <Trigger Property="IsEnabled" Value="False">
                            <Setter TargetName="PART_Border" Property="Background"
                                    Value="{StaticResource ControlBackgroundDisabled}" />
                        </Trigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <Style x:Key="InfoTileButtonStyle"
           BasedOn="{StaticResource Tile}"
           TargetType="{x:Type Button}">
        <Setter Property="HorizontalAlignment" Value="Stretch" />
        <Setter Property="Height" Value="64" />
        <Setter Property="Background" Value="{StaticResource BackgroundColor}" />
        <Setter Property="Margin" Value="0 0 0 4" />
        <Setter Property="ToolTipService.Placement" Value="Right" />
        <Setter Property="ToolTipService.IsEnabled" Value="True" />
        <Setter Property="ToolTipService.InitialShowDelay" Value="1800" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="Button">
                    <Border x:Name="PART_Border"
                            Padding="0"
                            Background="{TemplateBinding Background}"
                            BorderBrush="Transparent"
                            BorderThickness="2"
                            ClipToBounds="True">
                        <Grid Margin="0">
                            <Grid.ColumnDefinitions>
                                <ColumnDefinition Width="60" />
                                <ColumnDefinition Width="*" />
                            </Grid.ColumnDefinitions>
                            <Grid.RowDefinitions>
                                <RowDefinition Height="Auto" />
                                <RowDefinition Height="*" />
                                <RowDefinition Height="Auto" />
                            </Grid.RowDefinitions>
                            <Grid x:Name="IconContainer"
                                  Grid.RowSpan="3"
                                  Background="{Binding ColorBrush}">

                                <Image x:Name="TileIcon"
                                       Width="40"
                                       Height="40"
                                       RenderOptions.BitmapScalingMode="HighQuality"
                                       Source="{Binding Icon}"
                                       Visibility="Collapsed" />

                                <wpf:HeroIcon x:Name="HeroIconByPath" Size="40" Visibility="Collapsed"
                                              IconPath="{Binding HeroIconPath}" />

                                <wpf:HeroIcon x:Name="HeroIconBySource" Size="40" Visibility="Collapsed"
                                              Icon="{Binding HeroIconSource}" />

                                <TextBlock x:Name="TileAcronym"
                                           Margin="4 -2 0 0"
                                           HorizontalAlignment="Left"
                                           FontSize="32"
                                           Opacity="0.2"
                                           Style="{StaticResource AcronymText}"
                                           Text="{Binding Acronym}"
                                           Visibility="Visible" />
                                <Border x:Name="Badge"
                                        Width="16"
                                        Height="16"
                                        Margin="4"
                                        HorizontalAlignment="Right"
                                        VerticalAlignment="Top"
                                        Background="{StaticResource RedColor}"
                                        CornerRadius="8"
                                        Visibility="{Binding IsBadgeCountVisible, Converter={StaticResource BooleanToVisibilityConverter}}">
                                    <TextBlock HorizontalAlignment="Center"
                                               FontSize="11"
                                               Foreground="White"
                                               Style="{StaticResource PureContentText}"
                                               Text="{Binding BadgeCount.Result}" />
                                </Border>
                            </Grid>
                            <TextBlock x:Name="Title"
                                       Grid.Row="0"
                                       Grid.Column="1"
                                       Margin="10 0 10 2"
                                       FontFamily="Segoe UI"
                                       FontSize="16"
                                       FontWeight="ExtraLight"
                                       Foreground="{Binding ColorBrush}"
                                       Text="{Binding Title}"
                                       TextTrimming="CharacterEllipsis"
                                       TextWrapping="NoWrap" />
                            <TextBlock x:Name="Description" Grid.Row="1" Grid.Column="1"
                                       Margin="10 0" FontFamily="Segoe UI" FontSize="12"
                                       Foreground="#666666"
                                       Text="{Binding SingleLineDescription}"
                                       TextTrimming="CharacterEllipsis" TextWrapping="Wrap" />
                            <TextBlock x:Name="Description2" Grid.Row="2" Grid.Column="1"
                                       Margin="10 0 10 4" FontFamily="Segoe UI" FontSize="12"
                                       Foreground="#666666"
                                       Visibility="{Binding SecondLineDescription, Converter={StaticResource NullOrEmptyToVisibilityConverter}}"
                                       Text="{Binding SecondLineDescription}"
                                       TextTrimming="CharacterEllipsis" TextWrapping="NoWrap" />

                            <wpf:HeroIcon IconPath="/Graphics/heroicons-outline/white/check.svg" Size="12"
                                          Margin="0,2,2,0"
                                          HorizontalAlignment="Right" VerticalAlignment="Top"
                                          Visibility="{Binding ShowCheckmark, Converter={StaticResource BooleanToVisibilityConverter}}" />

                            <ContentPresenter Grid.Column="1" Grid.Row="0" Grid.RowSpan="3" />
                        </Grid>
                    </Border>
                    <ControlTemplate.Triggers>
                        <DataTrigger
                            Binding="{Binding ActualWidth, Converter={StaticResource LessThan}, ConverterParameter=150, RelativeSource={RelativeSource Self}}"
                            Value="True">
                            <Setter TargetName="PART_Border" Property="Width" Value="64" />
                            <Setter TargetName="PART_Border" Property="HorizontalAlignment" Value="Left" />
                            <Setter TargetName="Title" Property="Visibility" Value="Collapsed" />
                            <Setter TargetName="Description" Property="Visibility" Value="Collapsed" />
                        </DataTrigger>
                        <DataTrigger Binding="{Binding HasIcon}" Value="True">
                            <Setter TargetName="TileIcon" Property="Visibility" Value="Visible" />
                            <Setter TargetName="HeroIconBySource" Property="Visibility" Value="Collapsed" />
                            <Setter TargetName="HeroIconByPath" Property="Visibility" Value="Collapsed" />
                            <Setter TargetName="TileAcronym" Property="Visibility" Value="Collapsed" />
                        </DataTrigger>
                        <DataTrigger Binding="{Binding HasHeroIconPath}" Value="True">
                            <Setter TargetName="HeroIconByPath" Property="Visibility" Value="Visible" />
                            <Setter TargetName="HeroIconBySource" Property="Visibility" Value="Collapsed" />
                            <Setter TargetName="TileIcon" Property="Visibility" Value="Collapsed" />
                            <Setter TargetName="TileAcronym" Property="Visibility" Value="Collapsed" />
                        </DataTrigger>
                        <DataTrigger Binding="{Binding HasHeroIconSource}" Value="True">
                            <Setter TargetName="HeroIconBySource" Property="Visibility" Value="Visible" />
                            <Setter TargetName="HeroIconByPath" Property="Visibility" Value="Collapsed" />
                            <Setter TargetName="TileIcon" Property="Visibility" Value="Collapsed" />
                            <Setter TargetName="TileAcronym" Property="Visibility" Value="Collapsed" />
                        </DataTrigger>
                        <DataTrigger Binding="{Binding IsHighlighted}" Value="True">
                            <Setter TargetName="IconContainer" Property="Background"
                                    Value="{StaticResource Highlighted}" />
                        </DataTrigger>
                        <Trigger Property="IsMouseOver" Value="True">
                            <Setter TargetName="PART_Border" Property="BorderBrush"
                                    Value="{StaticResource TileBorderOver}" />
                        </Trigger>
                        <Trigger Property="IsEnabled" Value="False">
                            <Setter TargetName="PART_Border" Property="Background" Value="Transparent" />
                            <Setter TargetName="IconContainer" Property="Background"
                                    Value="{StaticResource ControlBackgroundDisabled}" />
                            <Setter TargetName="Title" Property="Foreground"
                                    Value="{StaticResource ControlForegroundDisabled}" />
                            <Setter TargetName="Description" Property="Foreground"
                                    Value="{StaticResource ControlForegroundDisabled}" />
                        </Trigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
        <Setter Property="ContextMenu">
            <Setter.Value>
                <ContextMenu ItemsSource="{Binding ContextMenuEntries}"
                             Visibility="{Binding HasContextMenu, Converter={StaticResource BooleanToVisibilityConverter}}"
                             Template="{StaticResource SimpleContextMenu}"
                             ItemContainerStyle="{StaticResource SimpleContextMenuItemContainer}"
                             ItemTemplate="{StaticResource SimpleContextMenuItem}" />
            </Setter.Value>
        </Setter>
    </Style>

    <Style x:Key="AuthorTileButtonStyle"
           BasedOn="{StaticResource InfoTileButtonStyle}"
           TargetType="{x:Type Button}">
        <Setter Property="MinHeight" Value="64" />
        <Setter Property="Height" Value="Auto" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="Button">
                    <Border x:Name="PART_Border"
                            Padding="0"
                            Background="{TemplateBinding Background}"
                            BorderBrush="Transparent"
                            BorderThickness="2"
                            ClipToBounds="True">
                        <Grid Margin="0">
                            <Grid.ColumnDefinitions>
                                <ColumnDefinition Width="60" />
                                <ColumnDefinition Width="*" />
                            </Grid.ColumnDefinitions>
                            <Grid.RowDefinitions>
                                <RowDefinition Height="Auto" />
                                <RowDefinition Height="*" />
                            </Grid.RowDefinitions>
                            <Grid x:Name="IconContainer"
                                  Grid.RowSpan="2"
                                  Background="{Binding ColorBrush}">

                                <wpf:HeroIcon x:Name="HeroIconByPath" Size="40" IconPath="{Binding HeroIconPath}" />
                            </Grid>
                            <TextBlock x:Name="Title"
                                       Grid.Row="0"
                                       Grid.Column="1"
                                       Margin="10 0 10 2"
                                       FontFamily="Segoe UI"
                                       FontSize="16"
                                       FontWeight="ExtraLight"
                                       Foreground="{Binding ColorBrush}"
                                       Text="{Binding Title}"
                                       TextTrimming="CharacterEllipsis"
                                       TextWrapping="NoWrap" />
                            <StackPanel  Grid.Row="1" Grid.Column="1" Margin="10 2 10 4">
                                <TextBlock x:Name="Description"
                                           FontFamily="Segoe UI" FontSize="12"
                                           Foreground="#666666"
                                           Text="{Binding SingleLineDescription}"
                                           TextTrimming="CharacterEllipsis" TextWrapping="NoWrap" />
                                <TextBlock x:Name="Description2"
                                           FontFamily="Segoe UI" FontSize="12"
                                           Foreground="#666666"
                                           Visibility="{Binding SecondLineDescription, Converter={StaticResource NullOrEmptyToVisibilityConverter}}"
                                           Text="{Binding SecondLineDescription}"
                                           TextTrimming="CharacterEllipsis" TextWrapping="NoWrap" />
                            </StackPanel>
                            <ContentPresenter Grid.Column="1" Grid.Row="0" Grid.RowSpan="2" />
                        </Grid>
                    </Border>
                    <ControlTemplate.Triggers>
                        <Trigger Property="IsMouseOver" Value="True">
                            <Setter TargetName="PART_Border" Property="BorderBrush"
                                    Value="{StaticResource TileBorderOver}" />
                        </Trigger>
                        <Trigger Property="IsEnabled" Value="False">
                            <Setter TargetName="PART_Border" Property="Background" Value="Transparent" />
                            <Setter TargetName="IconContainer" Property="Background"
                                    Value="{StaticResource ControlBackgroundDisabled}" />
                            <Setter TargetName="Title" Property="Foreground"
                                    Value="{StaticResource ControlForegroundDisabled}" />
                            <Setter TargetName="Description" Property="Foreground"
                                    Value="{StaticResource ControlForegroundDisabled}" />
                        </Trigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>


    <Style x:Key="GroupedTileButton"
           BasedOn="{StaticResource Tile}"
           TargetType="{x:Type Button}">
        <Setter Property="HorizontalAlignment" Value="Stretch" />
        <Setter Property="Height" Value="64" />
        <Setter Property="Background" Value="{StaticResource BackgroundColor}" />
        <Setter Property="Margin" Value="0" />
        <Setter Property="ToolTipService.Placement" Value="Right" />
        <Setter Property="ToolTipService.IsEnabled" Value="True" />
        <Setter Property="ToolTipService.InitialShowDelay" Value="1800" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="Button">
                    <Border x:Name="PART_Border"
                            Padding="0"
                            Background="{TemplateBinding Background}"
                            BorderBrush="Transparent"
                            BorderThickness="2"
                            ClipToBounds="True">
                        <Grid Margin="0">
                            <Grid.ColumnDefinitions>
                                <ColumnDefinition Width="60" />
                                <ColumnDefinition Width="*" />
                            </Grid.ColumnDefinitions>
                            <Grid.RowDefinitions>
                                <RowDefinition Height="Auto" />
                                <RowDefinition Height="*" />
                            </Grid.RowDefinitions>
                            <Grid x:Name="IconContainer"
                                  Grid.RowSpan="2"
                                  Background="{Binding ColorBrush}">
                                <Image x:Name="TileIcon"
                                       HorizontalAlignment="Stretch"
                                       VerticalAlignment="Stretch"
                                       RenderOptions.BitmapScalingMode="HighQuality"
                                       Source="{Binding Icon}"
                                       Visibility="Collapsed" />
                                <TextBlock x:Name="TileAcronym"
                                           Margin="4 -2 0 0"
                                           HorizontalAlignment="Left"
                                           FontSize="32"
                                           Opacity="0.2"
                                           Style="{StaticResource AcronymText}"
                                           Text="{Binding Acronym}"
                                           Visibility="Visible" />
                                <Border x:Name="Badge"
                                        Width="16"
                                        Height="16"
                                        Margin="4"
                                        HorizontalAlignment="Right"
                                        VerticalAlignment="Top"
                                        Background="{StaticResource RedColor}"
                                        CornerRadius="8"
                                        Visibility="{Binding IsBadgeCountVisible, Converter={StaticResource BooleanToVisibilityConverter}}">
                                    <TextBlock HorizontalAlignment="Center"
                                               FontSize="11"
                                               Foreground="White"
                                               Style="{StaticResource PureContentText}"
                                               Text="{Binding BadgeCount.Result}" />
                                </Border>
                            </Grid>
                            <TextBlock x:Name="Title"
                                       Grid.Row="0"
                                       Grid.Column="1"
                                       Margin="10 0 10 2"
                                       FontFamily="Segoe UI"
                                       FontSize="16"
                                       FontWeight="ExtraLight"
                                       Foreground="{Binding ColorBrush}"
                                       Text="{Binding Title}"
                                       TextTrimming="CharacterEllipsis"
                                       TextWrapping="NoWrap" />
                            <TextBlock x:Name="Description"
                                       Grid.Row="1"
                                       Grid.Column="1"
                                       Margin="10 0"
                                       FontFamily="Segoe UI"
                                       FontSize="12"
                                       Foreground="#666666"
                                       Text="{Binding SingleLineDescription}"
                                       TextTrimming="CharacterEllipsis"
                                       TextWrapping="Wrap" />
                            <wpf:HeroIcon IconPath="/Graphics/heroicons-outline/white/check.svg" Size="12"
                                          Margin="0,2,2,0"
                                          HorizontalAlignment="Right" VerticalAlignment="Top"
                                          Visibility="{Binding ShowCheckmark, Converter={StaticResource BooleanToVisibilityConverter}}" />
                        </Grid>
                    </Border>
                    <ControlTemplate.Triggers>
                        <DataTrigger
                            Binding="{Binding ActualWidth, Converter={StaticResource LessThan}, ConverterParameter=150, RelativeSource={RelativeSource Self}}"
                            Value="True">
                            <Setter TargetName="PART_Border" Property="Width" Value="64" />
                            <Setter TargetName="PART_Border" Property="HorizontalAlignment" Value="Left" />
                            <Setter TargetName="Title" Property="Visibility" Value="Collapsed" />
                            <Setter TargetName="Description" Property="Visibility" Value="Collapsed" />
                        </DataTrigger>
                        <DataTrigger Binding="{Binding HasIcon}" Value="True">
                            <Setter TargetName="TileIcon" Property="Visibility" Value="Visible" />
                            <Setter TargetName="TileAcronym" Property="Visibility" Value="Collapsed" />
                        </DataTrigger>
                        <Trigger Property="IsMouseOver" Value="True">
                            <Setter TargetName="PART_Border" Property="BorderBrush"
                                    Value="{StaticResource TileBorderOver}" />
                        </Trigger>
                        <Trigger Property="IsEnabled" Value="False">
                            <Setter TargetName="PART_Border" Property="Background" Value="Transparent" />
                            <Setter TargetName="IconContainer" Property="Background"
                                    Value="{StaticResource ControlBackgroundDisabled}" />
                            <Setter TargetName="Title" Property="Foreground"
                                    Value="{StaticResource ControlForegroundDisabled}" />
                            <Setter TargetName="Description" Property="Foreground"
                                    Value="{StaticResource ControlForegroundDisabled}" />
                        </Trigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <ControlTemplate x:Key="TileToolTipContent" TargetType="Control">
        <StackPanel Orientation="Vertical">
            <TextBlock FontWeight="Bold"
                       Foreground="{StaticResource ControlForeground}"
                       Style="{StaticResource PureSecondaryContentHeader}"
                       Text="{Binding ToolTipTitle}" />
            <TextBlock Margin="0 6 0 0"
                       Foreground="{StaticResource ControlForeground}"
                       Style="{StaticResource PureContentText}"
                       Text="{Binding Description}"
                       Visibility="{Binding IsDescriptionVisible, Converter={StaticResource BooleanToVisibilityConverter}}" />
        </StackPanel>
    </ControlTemplate>

    <Style x:Key="TileButton" TargetType="Button">
        <Setter Property="Width" Value="72" />
        <Setter Property="Height" Value="72" />
        <Setter Property="Margin" Value="0 0 4 4" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="Button">
                    <Border x:Name="PART_Border"
                            Padding="0"
                            Background="{TemplateBinding Background}"
                            BorderBrush="Transparent"
                            BorderThickness="2"
                            ClipToBounds="True">
                        <Grid Margin="0" Background="{Binding ColorBrush}">
                            <Grid.RowDefinitions>
                                <RowDefinition Height="*" />
                                <RowDefinition Height="Auto" />
                            </Grid.RowDefinitions>
                            <Image Grid.Row="0"
                                   Grid.RowSpan="2"
                                   HorizontalAlignment="Stretch"
                                   VerticalAlignment="Stretch"
                                   RenderOptions.BitmapScalingMode="HighQuality"
                                   SnapsToDevicePixels="True"
                                   Source="{Binding Icon}"
                                   Visibility="{Binding HasIcon, Converter={StaticResource BooleanToVisibilityConverter}}" />
                            <Polygon Grid.Row="1"
                                     Width="8"
                                     Height="8"
                                     Margin="1"
                                     HorizontalAlignment="Right"
                                     VerticalAlignment="Bottom"
                                     Fill="White"
                                     Points="0,10 10,0 10,10 0,10"
                                     Visibility="{Binding ShowDisclosureIndicator, Converter={StaticResource BooleanToVisibilityConverter}}" />
                            <TextBlock Grid.Row="0"
                                       Grid.RowSpan="2"
                                       Margin="4 -2 0 0"
                                       HorizontalAlignment="Left"
                                       FontSize="32"
                                       Opacity="0.2"
                                       Style="{StaticResource AcronymText}"
                                       Text="{Binding Acronym}" />
                            <TextBlock Grid.Row="0"
                                       Grid.RowSpan="2"
                                       Margin="4"
                                       Style="{StaticResource TileText}"
                                       Text="{Binding Title}"
                                       TextWrapping="WrapWithOverflow"
                                       Visibility="{Binding ShowTileText, Converter={StaticResource BooleanToVisibilityConverter}}" />
                            <TextBlock x:Name="TileText"
                                       Grid.Row="0"
                                       Grid.RowSpan="2"
                                       Margin="4"
                                       Style="{StaticResource TileText}"
                                       Text="{Binding HotKey}"
                                       TextWrapping="WrapWithOverflow"
                                       Visibility="{Binding ShowHotKeyText, Converter={StaticResource BooleanToVisibilityConverter}}" />
                            <wpf:HeroIcon IconPath="/Graphics/heroicons-outline/white/check.svg" Size="12"
                                          Margin="0,2,2,0"
                                          HorizontalAlignment="Right" VerticalAlignment="Top"
                                          Visibility="{Binding ShowCheckmark, Converter={StaticResource BooleanToVisibilityConverter}}" />
                        </Grid>
                        <i:Interaction.Triggers>
                            <i:EventTrigger EventName="MouseEnter">
                                <i:InvokeCommandAction Command="{Binding StartHotKeyTimerCmd}" />
                            </i:EventTrigger>
                            <i:EventTrigger EventName="MouseLeave">
                                <i:InvokeCommandAction Command="{Binding StopHotKeyTimerCmd}" />
                            </i:EventTrigger>
                        </i:Interaction.Triggers>
                    </Border>
                    <ControlTemplate.Triggers>
                        <Trigger Property="IsMouseOver" Value="True">
                            <Setter TargetName="PART_Border" Property="BorderBrush"
                                    Value="{StaticResource TileBorderOver}" />
                        </Trigger>
                        <Trigger Property="IsEnabled" Value="False">
                            <Setter TargetName="PART_Border" Property="Background"
                                    Value="{StaticResource ControlBackgroundDisabled}" />
                        </Trigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
        <Setter Property="ContextMenu">
            <Setter.Value>
                <ContextMenu ItemContainerStyle="{StaticResource SimpleContextMenuItemContainer}"
                             ItemTemplate="{StaticResource SimpleContextMenuItem}"
                             ItemsSource="{Binding ContextMenuEntries}"
                             Template="{StaticResource SimpleContextMenu}"
                             Visibility="{Binding HasContextMenu, Converter={StaticResource BooleanToVisibilityConverter}}" />
            </Setter.Value>
        </Setter>
        <Setter Property="ToolTip">
            <Setter.Value>
                <Control Template="{StaticResource TileToolTipContent}" />
            </Setter.Value>
        </Setter>
    </Style>

    <Style x:Key="CreateCustomBrickButton" TargetType="Button">
        <Setter Property="Width" Value="72" />
        <Setter Property="Height" Value="72" />
        <Setter Property="Margin" Value="0 0 4 4" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="Button">
                    <Border x:Name="PART_Border"
                            Padding="0"
                            BorderBrush="Transparent"
                            BorderThickness="2"
                            ClipToBounds="True">
                        <Grid Margin="0" Background="#F0F0F0">
                            <Grid.RowDefinitions>
                                <RowDefinition Height="*" />
                                <RowDefinition Height="Auto" />
                            </Grid.RowDefinitions>
                            <TextBlock Grid.Row="0"
                                       Grid.RowSpan="2"
                                       Margin="4 -2 0 0"
                                       HorizontalAlignment="Left"
                                       FontSize="32"
                                       Foreground="{Binding ColorBrush}"
                                       Opacity="0.2"
                                       Style="{StaticResource AcronymText}"
                                       Text="{Binding Acronym}" />
                            <TextBlock Grid.Row="0"
                                       Grid.RowSpan="2"
                                       Margin="4"
                                       Foreground="{Binding ColorBrush}"
                                       Style="{StaticResource TileText}"
                                       Text="{Binding Title}"
                                       TextWrapping="WrapWithOverflow"
                                       Visibility="{Binding ShowTileText, Converter={StaticResource BooleanToVisibilityConverter}}" />
                        </Grid>
                    </Border>
                    <ControlTemplate.Triggers>
                        <Trigger Property="IsMouseOver" Value="True">
                            <Setter TargetName="PART_Border" Property="BorderBrush"
                                    Value="{StaticResource TileBorderOver}" />
                        </Trigger>
                        <Trigger Property="IsEnabled" Value="False">
                            <Setter TargetName="PART_Border" Property="Background"
                                    Value="{StaticResource ControlBackgroundDisabled}" />
                        </Trigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
        <Setter Property="ToolTip">
            <Setter.Value>
                <Control Template="{StaticResource TileToolTipContent}" />
            </Setter.Value>
        </Setter>
    </Style>

</ResourceDictionary>