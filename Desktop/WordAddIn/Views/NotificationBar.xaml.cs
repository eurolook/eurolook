using System;
using System.Windows;
using System.Windows.Controls;
using Eurolook.OfficeNotificationBar;
using Eurolook.WordAddIn.ViewModels;

namespace Eurolook.WordAddIn.Views
{
    public partial class GenericNotificationBar : INotificationBarContent
    {
        public GenericNotificationBar(NotificationViewModel viewModel)
        {
            InitializeComponent();

            viewModel.NotificationBar = this;
            DataContext = viewModel;
            SizeChanged += OnSizeChanged;
        }

        public event RequestClosingEvent RequestClosing;

        public UserControl UserControl
        {
            get { return this; }
        }

        public void Close()
        {
            var handler = RequestClosing;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public void OnRequestClosing()
        {
            Close();
        }

        private void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            var margin = new Thickness(
                NotificationTextBlock.ActualWidth,
                DefaultButton.Margin.Top,
                DefaultButton.Margin.Right,
                DefaultButton.Margin.Bottom);

            DefaultButton.Margin = margin;
        }

        private void OnClose(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
