﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace Eurolook.WordAddIn.Views
{
    /// <summary>
    /// Interaction logic for RatingControl.xaml
    /// </summary>
    public partial class RatingControl : UserControl
    {
        public static readonly DependencyProperty RatingValueProperty =
            DependencyProperty.Register(
                "RatingValue",
                typeof(int),
                typeof(RatingControl),
                new FrameworkPropertyMetadata(0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, RatingValueChanged));

        private readonly int _maxValue = 5;

        public RatingControl()
        {
            InitializeComponent();
        }

        public int RatingValue
        {
            get { return (int)GetValue(RatingValueProperty); }
            set
            {
                if (value < 0)
                {
                    SetValue(RatingValueProperty, 0);
                }
                else if (value > _maxValue)
                {
                    SetValue(RatingValueProperty, _maxValue);
                }
                else
                {
                    SetValue(RatingValueProperty, value);
                }
            }
        }

        private static void RatingValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var parent = sender as RatingControl;
            int ratingValue = (int)e.NewValue;
            var children = ((Grid)parent.Content).Children;

            for (int i = 0; i < ratingValue; i++)
            {
                var button = children[i] as ToggleButton;
                if (button != null)
                {
                    button.IsChecked = true;
                }
            }

            for (int i = ratingValue; i < children.Count; i++)
            {
                var button = children[i] as ToggleButton;
                if (button != null)
                {
                    button.IsChecked = false;
                }
            }
        }

        private void RatingButtonClickEventHandler(object sender, RoutedEventArgs e)
        {
            var button = (ToggleButton)sender;

            int newRating = int.Parse((string)button.Tag);

            if ((bool)button.IsChecked || newRating < RatingValue)
            {
                RatingValue = newRating;
            }
            else
            {
                RatingValue = newRating - 1;
            }

            e.Handled = true;
        }
    }
}
