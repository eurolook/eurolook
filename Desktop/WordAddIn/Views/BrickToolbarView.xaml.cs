using System;
using System.Windows;
using System.Windows.Controls;
using Eurolook.OfficeNotificationBar;
using Eurolook.WordAddIn.ContentBrickActions.BrickToolbar;

namespace Eurolook.WordAddIn.Views
{
    public partial class BrickToolbarView : IFloatingToolbarContent
    {
        public BrickToolbarView(BrickToolbarViewModel model)
        {
            InitializeComponent();
            DataContext = model;
        }

        public event RequestClosingEvent RequestClosing;

        public UserControl UserControl
        {
            get { return this; }
        }

        public void OnRequestClosing()
        {
            RequestClosing?.Invoke(this, new EventArgs());
        }

        private void OnClose(object sender, RoutedEventArgs e)
        {
            OnRequestClosing();
        }
    }
}
