using System;
using System.Windows;
using System.Windows.Controls;
using Eurolook.Common.Log;
using Eurolook.WordAddIn.ViewModels;

namespace Eurolook.WordAddIn.Views
{
    public partial class BricksTabView
    {
        public BricksTabView()
        {
            InitializeComponent();
        }

        public BricksTabViewModel Model
        {
            get { return DataContext as BricksTabViewModel; }
        }

        private void HandleBrickGroupClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var button = sender as Button;
                if (button == null)
                {
                    return;
                }

                if (button.DataContext is BrickGroupViewModel groupViewModel)
                {
                    Model.OpenGroup = groupViewModel;
                }
            }
            catch (Exception ex)
            {
                this.LogError("BrickGroupClick failed.", ex);
            }
        }
    }
}
