using System;
using System.Windows;
using System.Windows.Controls;
using Eurolook.OfficeNotificationBar;
using Microsoft.Office.Interop.Word;

namespace Eurolook.WordAddIn.Views
{
    public partial class UpdateNotification : INotificationBarContent
    {
        private readonly IWordApplicationContext _applicationContext;

        public UpdateNotification(IWordApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
            InitializeComponent();
        }

        public event RequestClosingEvent RequestClosing;

        public UserControl UserControl
        {
            get { return this; }
        }

        public void OnRequestClosing()
        {
            RequestClosing?.Invoke(this, new EventArgs());
        }

        private void OnDefaultAction(object sender, RoutedEventArgs e)
        {
            // ReSharper disable once RedundantCast
            ((_Application)_applicationContext.Application).Quit(true);
        }

        private void OnClose(object sender, RoutedEventArgs e)
        {
            OnRequestClosing();
        }
    }
}
