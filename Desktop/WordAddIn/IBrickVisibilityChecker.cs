using Eurolook.Data.Models;

namespace Eurolook.WordAddIn
{
    public interface IBrickVisibilityChecker
    {
        bool IsBrickVisible(Brick brick);

        bool IsBrickAccessibleByCurrentUser(Brick brick);
    }
}
