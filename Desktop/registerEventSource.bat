:: This batch file can be used for locally testing the ETW feature of the WordAddInShim
::
:: For testing, make sure that the provider@resourceFileName and provider@messageFileName 
:: attributes of the instrumentation manifest (EtwInstrumentationManifest.man) contain 
:: the *full* path to the Eurolook.WordAddInShim.dll

wevtutil.exe im WordAddInShim\EtwInstrumentationManifest.man