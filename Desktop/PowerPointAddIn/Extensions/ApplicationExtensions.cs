using System.Runtime.InteropServices;
using Microsoft.Office.Interop.PowerPoint;

namespace Eurolook.PowerPointAddIn.Extensions
{
    public static class ApplicationExtensions
    {
        public static bool IsProtectedView(this Application app)
        {
            try
            {
                return app.ActiveProtectedViewWindow != null;
            }
            catch (COMException)
            {
                // COMException is thrown if not in protected view
                return false;
            }
        }
    }
}
