﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Threading;
using Autofac;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.AddIn;
using Eurolook.AddIn.Common.ProfileInitializer;
using Eurolook.AddIn.Common.Ribbon;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.PowerPointAddIn.Autofac;
using Application = Microsoft.Office.Interop.PowerPoint.Application;

namespace Eurolook.PowerPointAddIn
{
    public sealed class ThisPowerPointAddIn : AddInBase, ICanLog, IOfficeAddIn, IDisposable
    {
        private IContainer _container;
        private IRibbonCallbackHandler _ribbonCallbackHandler;
        private IRibbonService _ribbonService;
        private IAddinContext _addinContext;

        public ThisPowerPointAddIn(Application application)
        {
            Application = application;
        }

        ~ThisPowerPointAddIn()
        {
            Dispose(false);
        }

        public Application Application { get; }

        public IRibbonCallbackHandler RibbonCallbackHandler => _ribbonCallbackHandler ??= _container?.Resolve<IRibbonCallbackHandler>();

        public IRibbonService RibbonService => _ribbonService ??= _container?.Resolve<IRibbonService>();

        private IAddinContext AddinContext => _addinContext ??= _container?.Resolve<IAddinContext>();

        public IContainer SetupContainer()
        {
            _container = AutofacRegistration.SetupContainer(this);
            this.LogDebug("Dependency injection container set up.");
            return _container;
        }

        public override void OnStartup(object application)
        {
            try
            {
                this.LogDebug("OnStartup started.");
                AsyncHelper.EnsureSynchronizationContext();

                if (application is not Application _)
                {
                    MessageBox.Show("Add-in must be loaded into Microsoft PowerPoint.");
                    return;
                }
            }
            catch (Exception e)
            {
                this.LogError(e);
            }
        }

        public override void OnShutdown()
        {
            Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        [SuppressMessage(
            "ReSharper",
            "UnusedParameter.Local",
            Justification = "This method signature is according to the IDisposable pattern.")]
        private void Dispose(bool disposing)
        {
            this.LogDebug("Shutting down Eurolook");

            AddinContext?.ClearDisabledAddins();
        }
    }
}
