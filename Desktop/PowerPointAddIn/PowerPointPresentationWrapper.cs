﻿using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Extensions;
using Eurolook.Common.Log;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.PowerPoint;

namespace Eurolook.PowerPointAddIn
{
    public class PowerPointPresentationWrapper : IOfficeDocumentWrapper, ICanLog
    {
        private readonly Presentation _presentation;

        public PowerPointPresentationWrapper(Presentation presentation)
        {
            _presentation = presentation;
        }

        public string GetCustomXml(string rootName)
        {
            return _presentation.CustomXMLParts.GetCustomXml(rootName);
        }

        public CustomXMLParts CustomXMLParts => _presentation.CustomXMLParts;

        public string Name => _presentation.Name;

        public string FullName => _presentation.FullName;

        public string FileExtension => ".pptx";

        public bool Saved
        {
            get => _presentation.Saved == MsoTriState.msoTrue;
            set => _presentation.Saved = value ? MsoTriState.msoTrue : MsoTriState.msoFalse;
        }

        public void Save()
        {
            _presentation.Save();
        }

        public void SaveAs(string fileName)
        {
            _presentation.SaveAs(fileName, PpSaveAsFileType.ppSaveAsOpenXMLPresentation);
        }

        public void SaveCopyAs(string fileName)
        {
            _presentation.SaveCopyAs(fileName, PpSaveAsFileType.ppSaveAsOpenXMLPresentation);
        }

        public dynamic Open(string fileName) => _presentation.Application.Presentations.Open(fileName);

        public dynamic GetDocument() => _presentation;

        public dynamic BuiltInDocumentProperties => _presentation.BuiltInDocumentProperties;

        public MetaProperties ContentTypeProperties => _presentation.ContentTypeProperties;

        public string GetHttpContentType => "application/vnd.openxmlformats-officedocument.presentationml.presentation";
    }
}
