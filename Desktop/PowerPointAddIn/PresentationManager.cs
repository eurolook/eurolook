using System;
using Microsoft.Office.Interop.PowerPoint;

namespace Eurolook.PowerPointAddIn
{
    public class PresentationManager : IPresentationManager
    {
        private readonly IPowerPointApplicationContext _applicationContext;
        private readonly Func<Presentation, PresentationViewModel> _presentationViewModelFunc;

        public PresentationManager(
            IPowerPointApplicationContext applicationContext,
            Func<Presentation, PresentationViewModel> presentationViewModelFunc)
        {
            _applicationContext = applicationContext;
            _presentationViewModelFunc = presentationViewModelFunc;
        }

        public PresentationViewModel GetActivePresentationViewModel()
        {
            var activePresentation = _applicationContext.Application.ActivePresentation;
            return activePresentation != null ? _presentationViewModelFunc(activePresentation) : null;
        }
    }
}
