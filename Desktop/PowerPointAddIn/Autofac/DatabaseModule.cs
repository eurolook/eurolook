﻿using Autofac;
using Eurolook.AddIn.Common.Autofac;
using Eurolook.AddIn.Common.Database;
using Eurolook.Data;
using Eurolook.Data.Database;
using Eurolook.Data.TermStore;

namespace Eurolook.PowerPointAddIn.Autofac
{
    public class DatabaseModule : Module, IEurolookCommonModule
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AddinDatabase>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<DatabaseStateRepository>().As<IDatabaseStateRepository>();
            builder.RegisterType<ClientDatabaseManager>().As<IClientDatabaseManager>();
            builder.RegisterType<UserDataRepository>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<UserGroupRepository>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<TextsRepository>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<LanguageRepository>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<StatisticsRepository>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<TermStoreClientDatabase>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<DocumentModelRepository>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<AuthorRepository>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<TemplateRepository>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<SystemConfigurationRepository>().AsImplementedInterfaces().SingleInstance();

            builder.Register(_ => new EurolookClientContext(ignoreDeletedFlags: true))
                   .As<EurolookContext>()
                   .As<EurolookClientContext>()
                   .ExternallyOwned();
        }
    }
}
