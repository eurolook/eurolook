﻿using System.IO;
using System.Reflection;
using Autofac;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Autofac;
using Eurolook.AddIn.Common.Email;
using Eurolook.AddIn.Common.PerformanceLog;
using Eurolook.AddIn.Common.SystemConfiguration;
using Eurolook.AddIn.Common.Wpf;
using Eurolook.Common;
using Eurolook.Common.Extensions;
using Eurolook.Data;
using Eurolook.PowerPointAddIn.Ribbon;

namespace Eurolook.PowerPointAddIn.Autofac
{
    public static class AutofacRegistration
    {
        public static IContainer SetupContainer(ThisPowerPointAddIn thisPowerPointAddIn)
        {
            var builder = new ContainerBuilder();

            var pluginLoader = new PluginLoader(
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                AppSettings.Instance.Plugins.PowerPoint);

            builder.SetEurolookPluginAssemblies(pluginLoader.PluginAssemblies);

            // Perform registrations and build the container.
            var applicationContext = new ApplicationContext(thisPowerPointAddIn.Application);
            builder.RegisterInstance(applicationContext).AsImplementedInterfaces().SingleInstance();

            builder.RegisterType<PowerPointAddinContext>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterInstance(() => ConfigurationInfo.Instance).AsImplementedInterfaces();

            // register services
            builder.RegisterType<PowerPointRibbonService>().AsImplementedInterfaces().SingleInstance();

            var moduleAssemblies = new[] { typeof(ThisPowerPointAddIn).Assembly, typeof(IEurolookCommonModule).Assembly };

            builder.RegisterAssemblyModules<IEurolookCommonModule>(moduleAssemblies);

            builder.RegisterType<SettingsService>().As<ISettingsService>().SingleInstance();
            builder.RegisterType<PowerPointMessageService>().As<IMessageService>().SingleInstance();
            builder.RegisterType<PresentationManager>().AsImplementedInterfaces().SingleInstance().AutoActivate();
            builder.RegisterType<PresentationViewModel>().AsSelf();

            builder.RegisterType<DataSyncManager>().SingleInstance();
            builder.RegisterType<DataSyncScheduler>().SingleInstance();

            builder.RegisterType<ColorManager>().SingleInstance();
            builder.RegisterType<DeviceManager>().SingleInstance();

            builder.RegisterType<PerformanceLogInfoProvider>().AsImplementedInterfaces();
            builder.RegisterType<PerformanceLogService>().AsImplementedInterfaces();

            builder.RegisterType<SystemConfigurationService>().AsImplementedInterfaces();
            builder.RegisterType<EmailService>().AsImplementedInterfaces();

            builder.RegisterGeneric(typeof(ClassNameResolver<>)).As(typeof(IClassNameResolver<>));

            pluginLoader.RegisterPluginModules(builder);

            var container = builder.Build();

            AutofacXamlResolver.Container = container;

            return container;
        }
    }
}
