using Eurolook.AddIn.Common;
using Microsoft.Office.Interop.PowerPoint;

namespace Eurolook.PowerPointAddIn
{
    public interface IPowerPointApplicationContext : IApplicationContext<Application>
    {
    }
}
