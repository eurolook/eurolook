using System;
using System.Threading.Tasks;
using Eurolook.AddIn.Common.Database;
using Eurolook.AddIn.Common.ProfileInitializer;
using Eurolook.AddIn.Common.Ribbon;

namespace Eurolook.PowerPointAddIn.Ribbon
{
    public class PowerPointRibbonService : RibbonServiceBase<IPowerPointApplicationContext, IPowerPointAddinContext>, IPowerPointRibbonService
    {
        private readonly IPowerPointAddinContext _addinContext;

        public PowerPointRibbonService(
            IPowerPointApplicationContext applicationContext,
            IPowerPointAddinContext addinContext,
            Lazy<IProfileInitializer> profileInitializer,
            Lazy<IUserDataRepository> userDataRepository)
            : base(applicationContext, addinContext, profileInitializer, userDataRepository)
        {
            _addinContext = addinContext;
        }

        public override Task<bool> TryLoadQuickAccessToolbarAsync()
        {
            return Task.FromResult(true);
        }

        public override void UnloadQuickAccessToolbar()
        {
        }
    }
}
