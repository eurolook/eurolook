﻿using System;
using System.Drawing;
using Eurolook.AddIn.Common;
using Eurolook.AddIn.Common.Properties;
using Eurolook.AddIn.Common.Ribbon;
using Eurolook.PowerPointAddIn.Extensions;

namespace Eurolook.PowerPointAddIn.Ribbon
{
    public abstract class PowerPointRibbonBase : RibbonBase
    {
        private readonly Lazy<IPowerPointApplicationContext> _applicationContext;
        private readonly Lazy<IPresentationManager> _presentationManager;

        protected PowerPointRibbonBase(
            Lazy<IAddinContext> addinContext,
            Lazy<IPowerPointApplicationContext> applicationContext,
            Lazy<IMessageService> messageService,
            Lazy<ISettingsService> settingsService,
            Lazy<IPresentationManager> presentationManager,
            Lazy<IEmbeddedResourceManager> embeddedResourceManager)
            : base(addinContext, messageService, settingsService, embeddedResourceManager)
        {
            _applicationContext = applicationContext;
            _presentationManager = presentationManager;
        }

        protected IPresentationManager PresentationManager => _presentationManager.Value;

        protected IPowerPointApplicationContext ApplicationContext => _applicationContext.Value;

        protected override Image GetResourceImage(string resourceId)
        {
            return GetResourceImageRecursive(GetType(), resourceId);
        }

        protected bool CanEditPresentation()
        {
            // ReadOnly mode should disable the button,
            // however it cannot do because Word does not revalidate the ribbon when read-only mode is exited.
            return !ApplicationContext.Application.IsProtectedView();
        }
    }
}
