namespace Eurolook.PowerPointAddIn
{
    public interface IPresentationManager
    {
        PresentationViewModel GetActivePresentationViewModel();
    }
}
