using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using Eurolook.DocumentProcessing.OpenXml;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("a")]
    [BdlAttribute("class", "FootnoteReference")]
    [SuppressMessage("Sonar", "S3220:Method calls should not resolve ambiguously to overloads with \"params\"", Justification = "Reviewed")]
    public class FootnoteReference : BdlElement, IOpenXmlConverter
    {
        private const string FootnoteReferenceStyle = "FootnoteReference";
        private const string FootnoteTextStyle = "FootnoteText";

        public string Reference { get; set; }

        public string CustomMark { get; set; }

        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            if (Reference == null)
            {
                return new OpenXmlElement[0];
            }

            // assert FootnoteReference style is present
            AssertFootnoteReferenceStyle(context);

            if (!context.FootnoteRelationshipIds.ContainsKey(Reference))
            {
                // first occurrence: create footnote and footnote reference
                var bdlFootnote = Document.Footnotes.FirstOrDefault(fn => fn.Id == Reference);
                if (bdlFootnote == null)
                {
                    return new OpenXmlElement[0];
                }

                var footnote = GetFootnote(context, bdlFootnote);
                context.FootnoteRelationshipIds[Reference] = footnote.Id;

                // insert a "real" footnote reference
                return new OpenXmlElement[] { GetFootnoteReference(footnote.Id) };
            }

            // for duplicate footnote references insert a cross reference to the "real" footnote reference
            return GetFootnoteCrossReference(context.FootnoteRelationshipIds[Reference]).ToArray();
        }

        protected override void Parse()
        {
            base.Parse();

            string hrefAttrValue = (string)XmlElement.Attribute("href");
            if (string.IsNullOrWhiteSpace(hrefAttrValue))
            {
                throw new InvalidBdlDocumentException("Footnote reference is missing href attribute.");
            }

            CustomMark = (string)XmlElement.Attribute("data-custom-mark");

            Reference = hrefAttrValue;
        }

        private static IEnumerable<OpenXmlElement> GetFootnoteCrossReference(IntegerValue id)
        {
            string bookmarkName = $"_Ref_Footnote_{id}";
            return new OpenXmlElement[]
            {
                new Word.Run(new Word.FieldChar { FieldCharType = Word.FieldCharValues.Begin }),
                new Word.Run(
                    new Word.FieldCode
                    {
                        Space = SpaceProcessingModeValues.Preserve,
                        Text = $" NOTEREF {bookmarkName} \\f \\h ",
                    }),
                new Word.Run(new Word.FieldChar { FieldCharType = Word.FieldCharValues.Separate }),
                new Word.Run(
                    new Word.RunProperties(new Word.RunStyle { Val = "FootnoteReference" }),
                    new Word.Text { Text = "" }),
                new Word.Run(new Word.FieldChar { FieldCharType = Word.FieldCharValues.End }),
            };
        }

        private static void AssertFootnoteReferenceStyle(OpenXmlConverterContext context)
        {
            var styles = context.Document.GetOrCreateStyleDefinitionsPart(context.Options.DocumentMode).Styles;

            var styleFootnoteReference = styles.Elements<Word.Style>()
                                               .FirstOrDefault(e => e.StyleId.Value == FootnoteReferenceStyle);
            if (styleFootnoteReference == null)
            {
                // insert "Footnote reference" style
                styles.AppendChild(
                    new Word.Style
                    {
                        Type = Word.StyleValues.Character,
                        StyleId = FootnoteReferenceStyle,
                        StyleName = new Word.StyleName { Val = "Footnote Reference" },
                        BasedOn = new Word.BasedOn { Val = "DefaultParagraphFont" },
                        UIPriority = new Word.UIPriority { Val = 99 },
                        SemiHidden = new Word.SemiHidden(),
                        UnhideWhenUsed = new Word.UnhideWhenUsed(),
                        StyleRunProperties = new Word.StyleRunProperties
                        {
                            VerticalTextAlignment = new Word.VerticalTextAlignment
                            {
                                Val = Word.VerticalPositionValues.Superscript,
                            },
                        },
                    });
            }
        }

        private static int GetNewFootnoteId(FootnotesPart footnotesPart)
        {
            // NOTE: creating a new footnote with ID <nums> is fine as there are elements with ID -1 and 0
            var existingFootnotes = footnotesPart.Footnotes.Descendants<Word.Footnote>().ToList();
            int newId = existingFootnotes.Count;
            while (footnotesPart.Footnotes.Descendants<Word.Footnote>()
                                .FirstOrDefault(f => f.Id.HasValue && f.Id.Value == newId) != null)
            {
                newId++;
            }

            return newId;
        }

        private Word.Run GetFootnoteReference(IntegerValue id)
        {
            string bookmarkName = $"_Ref_Footnote_{id}";
            var footnoteReferenceRun =
                new Word.Run(
                    new Word.RunProperties(
                        new Word.RunStyle { Val = FootnoteReferenceStyle }));

            footnoteReferenceRun.AppendChild(
                new Word.BookmarkStart
                {
                    Name = bookmarkName,
                    Id = bookmarkName,
                });

            if (string.IsNullOrWhiteSpace(CustomMark))
            {
                footnoteReferenceRun.AppendChild(new Word.FootnoteReference { Id = id });
            }
            else
            {
                footnoteReferenceRun.AppendChild(
                    new Word.FootnoteReference
                    {
                        Id = id,
                        CustomMarkFollows = true,
                    });
                footnoteReferenceRun.AppendChild(new Word.Text(CustomMark));
            }

            footnoteReferenceRun.AppendChild(new Word.BookmarkEnd { Id = bookmarkName });

            return footnoteReferenceRun;
        }

        private Word.Footnote GetFootnote(OpenXmlConverterContext context, Footnote bdlFootnote)
        {
            var footnotesPart = context.Document.GetOrCreateFootnotesPart(context.Options.DocumentMode);

            int newId = GetNewFootnoteId(footnotesPart);

            return ExecuteForCurrentPart(
                footnotesPart,
                context,
                () =>
                {
                    var footnote = new Word.Footnote { Id = new IntegerValue((long)newId) };

                    var firstFootnoteParagraph = new Word.Paragraph();

                    // paragraph properties
                    string styleId = FootnoteTextStyle;
                    var firstChild = bdlFootnote.Content.FirstOrDefault() as Paragraph;
                    if (firstChild != null)
                    {
                        styleId = firstChild.Style;
                    }

                    firstFootnoteParagraph.AppendChild(
                        new Word.ParagraphProperties
                        {
                            ParagraphStyleId = new Word.ParagraphStyleId { Val = styleId },
                        });

                    // footnote reference
                    var runFootnoteReference = new Word.Run
                    {
                        RunProperties = new Word.RunProperties
                        {
                            RunStyle = new Word.RunStyle { Val = FootnoteReferenceStyle },
                        },
                    };

                    if (string.IsNullOrWhiteSpace(CustomMark))
                    {
                        runFootnoteReference.AppendChild(new Word.FootnoteReferenceMark());
                    }
                    else
                    {
                        runFootnoteReference.AppendChild(new Word.Text(CustomMark));
                    }

                    firstFootnoteParagraph.AppendChild(runFootnoteReference);

                    // footnote separator (space by default)
                    firstFootnoteParagraph.AppendChild(
                        new Word.Run(
                            new Word.Text
                            {
                                Space = SpaceProcessingModeValues.Preserve,
                                Text = "\t",
                            }));

                    // contents of first paragraph
                    var footnoteContents = bdlFootnote.Content;
                    if (firstChild != null)
                    {
                        var firstParagraphContents = firstChild.ConvertMany(firstChild.Content, context);
                        firstFootnoteParagraph.Append(firstParagraphContents);
                        footnoteContents = bdlFootnote.Content.Skip(1).ToList();
                    }

                    footnote.AppendChild(firstFootnoteParagraph);

                    // add additional content besides first paragraph
                    var footnoteContent = this.ConvertMany(footnoteContents, context);

                    footnote.Append(footnoteContent);
                    footnotesPart.Footnotes.AppendChild(footnote);

                    return footnote;
                });
        }
    }
}
