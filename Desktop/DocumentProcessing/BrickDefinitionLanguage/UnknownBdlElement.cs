using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    public class UnknownBdlElement : BdlElement, IContentElement, IOpenXmlConverter
    {
        public List<BdlElement> Content { get; private set; }

        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            if (!Content.Any(x => x is PlainTextValue))
            {
                return this.ConvertMany(Content, context);
            }

            return new OpenXmlElement[0];
        }

        protected override void Parse()
        {
            base.Parse();
            Content = ParseChildren();
        }
    }
}
