using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("span")]
    [BdlChildElements(typeof(PlainTextValue), typeof(Break))]
    public class FormattedTextRun : StyleableBdlElement, IOpenXmlConverter, IContentElement
    {
        public List<BdlElement> Content { get; set; }

        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            context.CurrentAuthorRolePriorityList = GetAuthorRolePriorityList();
            if (!OpenXmlConverterExtensions.IsVisible(GetVisibilityCondition(), context))
            {
                return new OpenXmlElement[0];
            }

            if (!Content.Any())
            {
                return new OpenXmlElement[0];
            }

            var run = new Word.Run
            {
                // Direct Formatting and Style
                RunProperties = GetRunProperties(),
            };

            // Content
            run.Append(this.ConvertMany(Content, context));

            return new OpenXmlElement[] { run };
        }

        protected override void Parse()
        {
            base.Parse();

            Content = ParseChildren();
        }
    }
}
