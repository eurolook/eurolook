﻿using System.Collections.Generic;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    public interface IContentElement
    {
        List<BdlElement> Content { get; }
    }
}
