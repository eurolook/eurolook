﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("select")]
    [BdlChildElements(typeof(ChoiceOption), typeof(BoundChoiceOption))]
    public class Choice : StyleableBdlElement, IOpenXmlConverter, IContentElement
    {
        public List<BdlElement> Content { get; set; }

        public string Title { get; set; }

        public string PlaceholderText { get; set; }

        public string SelectedText { get; set; }

        [SuppressMessage("Sonar", "S3220:Method calls should not resolve ambiguously to overloads with \"params\"", Justification = "OK")]
        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            var contentControl = new Word.SdtRun();
            var sdtProperties = new Word.SdtProperties();
            contentControl.AppendChild(sdtProperties);

            if (!string.IsNullOrEmpty(Title))
            {
                sdtProperties.AppendChild(new Word.SdtAlias { Val = Title });
            }

            sdtProperties.AppendChild(GetRunProperties());

            // remove duplicated entries from the list of Options
            var uniqueElements = RemoveDuplicatedEntries(Content, context);

            // convert the ComboBox
            var comboBox = new Word.SdtContentComboBox();
            var contents = this.ConvertMany(uniqueElements, context);
            comboBox.Append(contents);
            sdtProperties.AppendChild(comboBox);

            // run that displays the selected option
            var run = new Word.Run { RunProperties = GetRunProperties() };
            run.RunProperties.NoProof = new Word.NoProof();

            contentControl.AppendChild(new Word.SdtContentRun(run));

            AttachPlaceholder(contentControl, context);

            PreSelectContent(run, FindSelectedElement(uniqueElements), context);

            return new[] { contentControl };
        }

        protected override void Parse()
        {
            base.Parse();
            Content = ParseChildren();
            PlaceholderText = (string)XmlElement.Attribute("data-selected-item") ?? XmlElement.Value;
            Title = (string)XmlElement.Attribute("data-title");
        }

        private List<BdlElement> RemoveDuplicatedEntries(
            IEnumerable<BdlElement> options,
            OpenXmlConverterContext context)
        {
            var texts = new List<string>();
            var elements = new List<BdlElement>();
            foreach (var element in options)
            {
                string text = null;
                switch (element)
                {
                    case BoundChoiceOption option:
                        text = option.ResolveBinding(context);
                        break;
                    case ChoiceOption choiceOption:
                        text = choiceOption.Value;
                        break;
                }

                if (text != null && !texts.Contains(text))
                {
                    texts.Add(text);
                    elements.Add(element);
                }
            }

            return elements;
        }

        private void AttachPlaceholder(Word.SdtRun contentControl, OpenXmlConverterContext context)
        {
            if (string.IsNullOrEmpty(PlaceholderText))
            {
                return;
            }

            bool isShowingPlaceholder = string.IsNullOrEmpty(SelectedText);

            OpenXmlConverterExtensions.AttachPlaceholder(
                context.Document,
                contentControl,
                PlaceholderText,
                Document.DefaultPlaceholderStyle,
                context.Language.Locale,
                isShowingPlaceholder);
        }

        private void PreSelectContent(Word.Run run, BdlElement firstElement, OpenXmlConverterContext context)
        {
            if (!string.IsNullOrEmpty(SelectedText))
            {
                string resolvedText = SelectedText.StartsWith("/Texts/")
                    ? OpenXmlConverterExtensions.ResolveBinding(SelectedText, context)
                    : SelectedText;

                run.AppendChild(new Word.Text(resolvedText));
            }
            else if (string.IsNullOrEmpty(SelectedText) && firstElement != null)
            {
                switch (firstElement)
                {
                    case BoundChoiceOption boundOption:
                        run.AppendChild(new Word.Text(boundOption.ResolveBinding(context)));
                        break;
                    case ChoiceOption choiceOption:
                        run.AppendChild(new Word.Text(choiceOption.Value));
                        break;
                }
            }
        }

        private BdlElement FindSelectedElement(List<BdlElement> elements)
        {
            return elements.FirstOrDefault(
                       e => (bool?)e.GetType().GetProperty("Selected", typeof(bool))?.GetValue(e) == true)
                   ?? elements.FirstOrDefault();
        }
    }
}
