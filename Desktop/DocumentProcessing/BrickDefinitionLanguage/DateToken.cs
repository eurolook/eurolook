﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Xml.Linq;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("span")]
    [BdlAttribute("data-type", "date")]
    [BdlChildElements(ChildElements.None)]
    public class DateToken : DateTokenBase
    {
        [SuppressMessage("Sonar", "S3220:Method calls should not resolve ambiguously to overloads with \"params\"", Justification = "Reviewed")]
        public override IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            string bindDate = "/Texts/DateFormat" + Format;

            // get the binding to the language-specific text
            var xelSpan = new XElement("span");
            xelSpan.SetAttributeValue("data-type", "binding");
            xelSpan.SetAttributeValue("data-prop", bindDate);

            var boundText = new BoundText();
            boundText.Parse(xelSpan);
            var sdtDate = boundText.ToOpenXml(context);

            // insert a DATE field whose format is defined by the content of the content control
            var result = new List<OpenXmlElement>();
            result.Add(new Word.Run(new Word.FieldChar { FieldCharType = Word.FieldCharValues.Begin }));
            result.Add(new Word.Run(new Word.FieldCode { Space = SpaceProcessingModeValues.Preserve, Text = "DATE \\@ \"" }));
            result.AddRange(sdtDate);
            result.Add(new Word.Run(new Word.Text { Text = "\" MERGEFORMAT " }));
            result.Add(new Word.Run(new Word.FieldChar { FieldCharType = Word.FieldCharValues.Separate }));
            result.Add(new Word.Run(new Word.Text { Text = "" }));
            result.Add(new Word.Run(new Word.FieldChar { FieldCharType = Word.FieldCharValues.End }));
            return result.ToArray();
        }
    }
}
