﻿using System;
using System.Runtime.Serialization;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [Serializable]
    public class InvalidBdlDocumentException : Exception
    {
        public InvalidBdlDocumentException()
        {
        }

        public InvalidBdlDocumentException(string message)
            : base(message)
        {
        }

        public InvalidBdlDocumentException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected InvalidBdlDocumentException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}
