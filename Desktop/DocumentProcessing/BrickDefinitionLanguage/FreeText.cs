using System;
using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;
using W15 = DocumentFormat.OpenXml.Office2013.Word;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("span")]
    [BdlAttribute("data-type", "freetext")]
    [BdlChildElements(typeof(FormattedTextRun))]
    public class FreeText : StyleableBdlElement, IOpenXmlConverter
    {
        public string PlaceholderText { get; set; }

        public string Title { get; set; }

        public bool IsMultiLine { get; set; }

        public List<BdlElement> RichPlaceholderText { get; set; }

        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            context.CurrentAuthorRolePriorityList = GetAuthorRolePriorityList();
            if (!OpenXmlConverterExtensions.IsVisible(GetVisibilityCondition(), context))
            {
                return Array.Empty<OpenXmlElement>();
            }

            var contentControl = new Word.SdtRun();
            var sdtProperties = new Word.SdtProperties();
            contentControl.AppendChild(sdtProperties);

            if (!string.IsNullOrEmpty(Title))
            {
                sdtProperties.AppendChild(new Word.SdtAlias { Val = Title });
            }

            sdtProperties.AppendChild(GetRunProperties());

            if (context.Options.HideFreeTextContentControlContainer)
            {
                sdtProperties.AppendChild(new W15.Appearance { Val = W15.SdtAppearance.Hidden });
            }

            // Placeholder
            if (RichPlaceholderText != null && RichPlaceholderText.Any())
            {
                var richText = new List<OpenXmlElement>();
                foreach (var convertible in RichPlaceholderText.Where(e => e is IOpenXmlConverter)
                                                               .OfType<IOpenXmlConverter>())
                {
                    richText.AddRange(convertible.ToOpenXml(context));
                }

                OpenXmlConverterExtensions.AttachPlaceholder(
                    context.Document,
                    contentControl,
                    richText.ToArray(),
                    Document.DefaultPlaceholderStyle,
                    context.Language.Locale,
                    true,
                    true);
            }
            else if (!string.IsNullOrEmpty(PlaceholderText))
            {
                OpenXmlConverterExtensions.AttachPlaceholder(
                    context.Document,
                    contentControl,
                    PlaceholderText,
                    Document.DefaultPlaceholderStyle,
                    context.Language.Locale,
                    true,
                    true);
            }

            // Vanishing Content Control
            ////properties.AppendChild(new Word.TemporarySdt());

            // Don't convert freetext to plain text Content Controls.
            // You could not add formatting like carriage return (EUROLOOK-694) or footnotes (EUROLOOK-1373)
            ////properties.AppendChild(new Word.SdtContentText { MultiLine = true });

            return new OpenXmlElement[] { contentControl };
        }

        protected override void Parse()
        {
            base.Parse();

            var children = ParseChildren();
            if (children.Count == 1 && children.First() is PlainTextValue)
            {
                PlaceholderText = XmlElement.Value;
            }
            else
            {
                RichPlaceholderText = children;
            }

            Title = (string)XmlElement.Attribute("data-title");

            var dataProp = GetDataPropAttribute();
            IsMultiLine = dataProp != null && dataProp.Value == "multiline";
        }
    }
}
