using System;
using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using JetBrains.Annotations;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    public abstract class StyleableBdlElement : BdlElement
    {
        protected StyleableBdlElement()
        {
        }

        protected StyleableBdlElement(StyleableBdlElement source)
        {
            Id = source.Id;
            Style = source.Style;
            Css = source.Css;
        }

        public string Id { get; private set; }

        public string Style { get; set; }

        internal CssParser Css { get; set; }

        [NotNull]
        public RunProperties GetRunProperties()
        {
            var spanFormatting = GetSpanFormatting();
            return new RunProperties(spanFormatting);
        }

        protected override void Parse()
        {
            base.Parse();

            Id = (string)XmlElement.Attribute("id");
            Style = (string)XmlElement.Attribute("class");

            var styleAttr = XmlElement.Attribute("style");
            if (styleAttr != null)
            {
                Css = new CssParser(styleAttr.Value);
            }
        }

        [NotNull]
        protected ParagraphMarkRunProperties GetParagraphMarkRunProperties()
        {
            var spanFormatting = GetSpanFormatting();
            return new ParagraphMarkRunProperties(spanFormatting);
        }

        private static IEnumerable<OpenXmlElement> GetFontWeight(StyleableBdlElement element)
        {
            if (element.Css != null && element.Css.RunFontWeight.Any())
            {
                return element.Css.RunFontWeight;
            }

            return FindCssRecursive(element, GetFontWeight);
        }

        private static IEnumerable<OpenXmlElement> GetFontStyle(StyleableBdlElement element)
        {
            if (element.Css != null && element.Css.RunFontStyle.Any())
            {
                return element.Css.RunFontStyle;
            }

            return FindCssRecursive(element, GetFontStyle);
        }

        private static IEnumerable<OpenXmlElement> GetFontColor(StyleableBdlElement element)
        {
            if (element.Css != null && element.Css.RunFontColor.Any())
            {
                return element.Css.RunFontColor;
            }

            return FindCssRecursive(element, GetFontColor);
        }

        private static IEnumerable<OpenXmlElement> GetFontSize(StyleableBdlElement element)
        {
            if (element.Css != null && element.Css.RunFontSize.Any())
            {
                return element.Css.RunFontSize;
            }

            return FindCssRecursive(element, GetFontSize);
        }

        private static IEnumerable<OpenXmlElement> GetTextDecoration(StyleableBdlElement element)
        {
            if (element.Css != null && element.Css.RunTextDecoration.Any())
            {
                return element.Css.RunTextDecoration;
            }

            return FindCssRecursive(element, GetTextDecoration);
        }

        private static IEnumerable<OpenXmlElement> GetCaps(StyleableBdlElement element)
        {
            if (element.Css != null && element.Css.RunCaps.Any())
            {
                return element.Css.RunCaps;
            }

            return FindCssRecursive(element, GetCaps);
        }

        private static IEnumerable<OpenXmlElement> GetRunStyle(StyleableBdlElement element)
        {
            if (!string.IsNullOrEmpty(element.Style))
            {
                yield return new RunStyle { Val = element.Style };
            }
        }

        private static IEnumerable<OpenXmlElement> GetSmallCaps(StyleableBdlElement element)
        {
            if (element.Css != null && element.Css.RunSmallCaps.Any())
            {
                return element.Css.RunSmallCaps;
            }

            return FindCssRecursive(element, GetSmallCaps);
        }

        private static IEnumerable<OpenXmlElement> GetFonts(StyleableBdlElement element)
        {
            if (element.Css != null && element.Css.RunFonts.Any())
            {
                return element.Css.RunFonts;
            }

            return FindCssRecursive(element, GetFonts);
        }

        private static IEnumerable<OpenXmlElement> FindCssRecursive(
            BdlElement element,
            Func<StyleableBdlElement, IEnumerable<OpenXmlElement>> findCssPropertyFunction)
        {
            if (element.ParentElement is StyleableBdlElement styleableBdlElement)
            {
                return findCssPropertyFunction(styleableBdlElement);
            }

            if (element.ParentElement != null)
            {
                return FindCssRecursive(element.ParentElement, findCssPropertyFunction);
            }

            return Enumerable.Empty<OpenXmlElement>();
        }

        [NotNull]
        private IList<OpenXmlElement> GetSpanFormatting()
        {
            var result = new List<OpenXmlElement>();
            result.AddRange(GetRunStyle(this));
            result.AddRange(GetFonts(this));
            result.AddRange(GetFontWeight(this));
            result.AddRange(GetFontStyle(this));
            result.AddRange(GetCaps(this));
            result.AddRange(GetSmallCaps(this));
            result.AddRange(GetFontSize(this));
            result.AddRange(GetTextDecoration(this));
            result.AddRange(GetFontColor(this));
            return result;
        }
    }
}
