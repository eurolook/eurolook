using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Office2013.Word;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data.Constants;
using Eurolook.Data.Models.Metadata;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.DocumentProcessing.OpenXml;
using JetBrains.Annotations;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("span")]
    [BdlAttribute("data-type", "metadata")]
    [BdlRequiredAttributes("data-prop")]
    [BdlChildElements(ChildElements.None)]
    public class Metadata : StyleableBdlElement, IOpenXmlConverter, ICanLog
    {
        public bool IsLocked { get; set; }

        public string MetadataDefinitionName { get; set; }

        public string PlaceholderText { get; set; }

        public string Title { get; set; }

        public bool IsRichTextControl { get; private set; }

        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            context.CurrentAuthorRolePriorityList = GetAuthorRolePriorityList();
            if (!OpenXmlConverterExtensions.IsVisible(GetVisibilityCondition(), context))
            {
                return new OpenXmlElement[0];
            }

            var metadataDefinition = GetMetadataDefinition(context);
            if (metadataDefinition == null)
            {
                return new OpenXmlElement[0];
            }

            var dataBinding = GetDataBinding(context, metadataDefinition);

            var contentControl = new Word.SdtRun();
            var sdtProperties = new Word.SdtProperties();
            contentControl.AppendChild(sdtProperties);

            // Direct Formatting and Style
            var runProperties = GetRunProperties();
            sdtProperties.AppendChild(runProperties);

            // Disable spell check for bound texts.
            ////runProperties.NoProof = new Word.NoProof();

            // Alias
            if (!string.IsNullOrEmpty(Title))
            {
                sdtProperties.AppendChild(new Word.SdtAlias { Val = Title });
            }

            // Locking
            if (IsLocked)
            {
                sdtProperties.AppendChild(new Word.Lock { Val = Word.LockingValues.ContentLocked });
            }

            // Binding
            sdtProperties.AppendChild(dataBinding);

            // Placeholder
            if (!string.IsNullOrEmpty(PlaceholderText))
            {
                OpenXmlConverterExtensions.AttachPlaceholder(
                    context.Document,
                    contentControl,
                    PlaceholderText,
                    Document.DefaultPlaceholderStyle,
                    context.Language.Locale,
                    false);
            }

            if (context.Options.HideBoundTextContentControlContainer && IsLocked)
            {
                sdtProperties.AppendChild(new Appearance { Val = SdtAppearance.Hidden });
            }

            if (!IsRichTextControl)
            {
                // Allow carriage returns in bound text (see EUROLOOK-694)
                sdtProperties.AppendChild(new Word.SdtContentText { MultiLine = true });
            }

            return new OpenXmlElement[] { contentControl };
        }

        protected override void Parse()
        {
            base.Parse();

            IsLocked = false;
            if (XmlElement.TryParseAttribute("data-lock", out bool dataLock))
            {
                IsLocked = dataLock;
            }

            if (XmlElement.TryParseAttribute("data-rich-text", out bool richTextControl))
            {
                IsRichTextControl = richTextControl;
            }

            MetadataDefinitionName = (string)GetDataPropAttribute();
            PlaceholderText = XmlElement.Value;
            Title = (string)XmlElement.Attribute("data-title");
        }

        private DataBinding GetDataBinding(OpenXmlConverterContext context, MetadataDefinition metadataDefinition)
        {
            var dataBinding = new DataBinding();
            var contentTypeProperty = context.ContentTypeProperties?.Descendants()
                                             .FirstOrDefault(
                                                 e => e.Name.LocalName.EqualsIgnoreCase(MetadataDefinitionName));
            if (contentTypeProperty != null)
            {
                dataBinding.StoreItemId = context.ContentTypePropertiesStoreId;
                dataBinding.PrefixMappings =
                    $"xmlns:ns0='{Namespaces.ContentTypePropertiesNs}' " +
                    $"xmlns:ns1='{contentTypeProperty.Name.Namespace}' " +
                    $"xmlns:pc='{Namespaces.PartnerControlsNs}'";

                // NOTE: Word has built-in support to handle the XML structure of Term-Store-bound elements that
                //   automatically excluded the nodes containing guid values. Therefore we do not need to specify
                //   the full path to ns1:{MetadataDefinitionName}[1]/pc:Terms[1]/pc:TermInfo[1]/pc:TermName[1]"
                dataBinding.XPath = contentTypeProperty.Element(Namespaces.PartnerControlsNs + "Terms") != null
                    ? $"ns0:properties[1]/documentManagement[1]/ns1:{MetadataDefinitionName}[1]/pc:Terms[1]"
                    : $"ns0:properties[1]/documentManagement[1]/ns1:{MetadataDefinitionName}[1]";
            }
            else
            {
                dataBinding.StoreItemId = context.EurolookPropertiesStoreId;
                dataBinding.XPath = metadataDefinition.IsBoundToTermSet()
                    ? $"/EurolookProperties/DocumentMetadata[1]/{MetadataDefinitionName}[1]/Terms[1]/TermInfo[1]/TermName[1]"
                    : $"/EurolookProperties/DocumentMetadata[1]/{MetadataDefinitionName}[1]";
            }

            return dataBinding;
        }

        [CanBeNull]
        private MetadataDefinition GetMetadataDefinition(OpenXmlConverterContext context)
        {
            if (context.MetadataDefinitions == null
                || context.MetadataDefinitions?.ContainsKey(MetadataDefinitionName) == false)
            {
                this.LogError(
                    $"Could not convert metadata element {MetadataDefinitionName} in brick {context.Brick?.Name}");
                return null;
            }

            return context.MetadataDefinitions[MetadataDefinitionName];
        }
    }
}
