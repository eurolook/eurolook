using System.Collections.Generic;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.DocumentProcessing.OpenXml;
using W15 = DocumentFormat.OpenXml.Office2013.Word;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("span")]
    [BdlAttribute("data-type", "binding")]
    [BdlRequiredAttributes("data-prop")]
    [BdlChildElements(ChildElements.None)]
    public class BoundText : StyleableBdlElement, IOpenXmlConverter
    {
        public bool IsLocked { get; set; }

        public string Binding { get; set; }

        public string PlaceholderText { get; set; }

        public string Title { get; set; }

        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            context.CurrentAuthorRolePriorityList = GetAuthorRolePriorityList();
            if (!OpenXmlConverterExtensions.IsVisible(GetVisibilityCondition(), context))
            {
                return new OpenXmlElement[0];
            }

            var dataBinding = OpenXmlUtils.GetDataBinding(Binding, context);
            if (dataBinding == null)
            {
                return new OpenXmlElement[0];
            }

            var contentControl = new Word.SdtRun();
            var sdtProperties = new Word.SdtProperties();
            contentControl.AppendChild(sdtProperties);

            // Direct Formatting and Style
            var runProperties = GetRunProperties();
            sdtProperties.AppendChild(runProperties);

            // Disable spell check for bound texts.
            // Changed due to user feedback, see ECADOCS-492
            ////if (Binding.StartsWith("/" + TextsCustomXml.RootName, StringComparison.Ordinal)
            ////    || Binding.StartsWith("/" + AuthorCustomXml.RootName, StringComparison.Ordinal))
            ////{
            ////    runProperties.NoProof = new Word.NoProof();
            ////}

            // Alias
            if (!string.IsNullOrEmpty(Title))
            {
                sdtProperties.AppendChild(new Word.SdtAlias { Val = Title });
            }

            // Locking
            if (IsLocked)
            {
                sdtProperties.AppendChild(new Word.Lock { Val = Word.LockingValues.ContentLocked });
            }

            // Binding
            sdtProperties.AppendChild(dataBinding);

            // Placeholder
            if (!string.IsNullOrEmpty(PlaceholderText))
            {
                OpenXmlConverterExtensions.AttachPlaceholder(
                    context.Document,
                    contentControl,
                    PlaceholderText,
                    Document.DefaultPlaceholderStyle,
                    context.Language.Locale,
                    false);
            }

            if (context.Options.HideBoundTextContentControlContainer && IsLocked)
            {
                sdtProperties.AppendChild(new W15.Appearance { Val = W15.SdtAppearance.Hidden });
            }

            // Allow carriage returns in bound text (see EUROLOOK-694)
            sdtProperties.AppendChild(new Word.SdtContentText { MultiLine = true });

            return new OpenXmlElement[] { contentControl };
        }

        protected override void Parse()
        {
            base.Parse();

            IsLocked = false;
            if (XmlElement.TryParseAttribute("data-lock", out bool dataLock))
            {
                IsLocked = dataLock;
            }

            Binding = GetDataPropAttribute().Value;
            PlaceholderText = XmlElement.Value;
            Title = (string)XmlElement.Attribute("data-title");
        }
    }
}
