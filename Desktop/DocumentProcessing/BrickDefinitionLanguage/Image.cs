﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using Eurolook.DocumentProcessing.OpenXml;
using A = DocumentFormat.OpenXml.Drawing;
using A14 = DocumentFormat.OpenXml.Office2010.Drawing;
using Pic = DocumentFormat.OpenXml.Drawing.Pictures;
using Word = DocumentFormat.OpenXml.Wordprocessing;
using Wp = DocumentFormat.OpenXml.Drawing.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("img")]
    [BdlChildElements(ChildElements.None)]
    public class Image : AccessibleElementBase, IOpenXmlConverter
    {
        public string Source { get; set; }

        public uint ImageId { get; set; }

        public string Name { get; set; }

        public string AlternativeTextAlias { get; set; }

        public string Width { get; set; }

        public int WidthEmu
        {
            get { return CssUnitConverter.ConvertToEmu(Width); }
        }

        public string Height { get; set; }

        public int HeightEmu
        {
            get { return CssUnitConverter.ConvertToEmu(Height); }
        }

        public uint DistanceFromTop { get; set; }

        public uint DistanceFromBottom { get; set; }

        public uint DistanceFromLeft { get; set; }

        public uint DistanceFromRight { get; set; }

        public Wp.HorizontalRelativePositionValues HorizontalRelativePosition { get; set; }

        public int HorizontalPositionOffset { get; set; }

        public Wp.VerticalRelativePositionValues VerticalRelativePosition { get; set; }

        public int VerticalPositionOffset { get; set; }

        public uint ZOrder { get; set; }

        public OpenXmlElement Wrap { get; set; }

        public bool IsFloating { get; set; }

        public virtual IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            var run = new Word.Run();
            if (Source != null)
            {
                var resource = context.ResourceResolver.ResolveLocalisedResource(Source);
                if (resource?.RawData != null && resource.RawData.Length > 0)
                {
                    string relId = context.CurrentPart.AddImage(resource.RawData, GetImagePartType(resource.MimeType));

                    string alternativeText =
                        context.ResourceResolver.ResolveTranslation(AlternativeTextAlias)?.Value ?? AlternativeTextAlias;

                    var drawing = IsFloating
                        ? CreateFloatingImage(relId, alternativeText, context.CurrentPart)
                        : CreateInlineImage(relId, alternativeText, context.CurrentPart);

                    run.AppendChild(drawing);
                }
            }

            return new OpenXmlElement[] { run };
        }

        protected override void Parse()
        {
            base.Parse();

            var sourceAttr = XmlElement.Attribute("src");
            if (sourceAttr != null)
            {
                Source = sourceAttr.Value;
            }

            if (XmlElement.Attribute("width") == null)
            {
                // Backwards Compatibility for EC Flag
                Width = "3,81 cm";
            }
            else
            {
                Width = (string)XmlElement.Attribute("width");
            }

            if (XmlElement.Attribute("height") == null)
            {
                // Backwards Compatibility for EC Flag
                Height = "1,88 cm";
            }
            else
            {
                Height = (string)XmlElement.Attribute("height");
            }

            Name = (string)XmlElement.Attribute("name");
            AlternativeTextAlias = (string)XmlElement.Attribute("alt");

            Css?.TryApplyProperty(CssProperty.MarginTop, val => DistanceFromTop = CssUnitConverter.ConvertToUEmu(val));
            Css?.TryApplyProperty(CssProperty.MarginRight, val => DistanceFromRight = CssUnitConverter.ConvertToUEmu(val));
            Css?.TryApplyProperty(CssProperty.MarginBottom, val => DistanceFromBottom = CssUnitConverter.ConvertToUEmu(val));
            Css?.TryApplyProperty(CssProperty.MarginLeft, val => DistanceFromLeft = CssUnitConverter.ConvertToUEmu(val));

            Css?.TryApplyProperty(CssProperty.Left, val => HorizontalPositionOffset = CssUnitConverter.ConvertToEmu(val));
            Css?.TryApplyProperty(CssProperty.Top, val => VerticalPositionOffset = CssUnitConverter.ConvertToEmu(val));

            Css?.TryApplyProperty(
                CssProperty.Position,
                val =>
                {
                    IsFloating = val != "static";

                    switch (val)
                    {
                        case "absolute":
                            HorizontalRelativePosition = Wp.HorizontalRelativePositionValues.Margin;
                            VerticalRelativePosition = Wp.VerticalRelativePositionValues.Margin;
                            break;

                        case "fixed":
                            HorizontalRelativePosition = Wp.HorizontalRelativePositionValues.Page;
                            VerticalRelativePosition = Wp.VerticalRelativePositionValues.Page;
                            break;

                        case "relative":
                            HorizontalRelativePosition = Wp.HorizontalRelativePositionValues.Character;
                            VerticalRelativePosition = Wp.VerticalRelativePositionValues.Paragraph;
                            break;
                    }
                });

            ZOrder = 10000;
            Css?.TryApplyProperty(CssProperty.ZOrder, val => ZOrder = CssUnitConverter.ConvertToUInt(val));

            Wrap = ZOrder == 0
                ? (OpenXmlElement)new Wp.WrapNone()
                : new Wp.WrapSquare { WrapText = Wp.WrapTextValues.BothSides };

            ImageId = BitConverter.ToUInt32(BitConverter.GetBytes(GetHashCode()), 0);
        }

        [SuppressMessage(
            "Sonar",
            "S3220:Method calls should not resolve ambiguously to overloads with \"params\"",
            Justification = "Reviewed")]
        private Word.Drawing CreateInlineImage(
            string relationId,
            string alternativeText,
            OpenXmlPart currentPart)
        {
            return new Word.Drawing(
                new Wp.Inline(
                    new Wp.Extent
                    {
                        Cx = WidthEmu,
                        Cy = HeightEmu,
                    },
                    new Wp.EffectExtent
                    {
                        LeftEdge = 0L,
                        TopEdge = 0L,
                        RightEdge = 0L,
                        BottomEdge = 0L,
                    },
                    new Wp.DocProperties
                    {
                        Id = 1U,
                        Name = Name,
                        Description = alternativeText,
                        NonVisualDrawingPropertiesExtensionList = IsDecorative ? GetDecorativeExtensionList(currentPart) : null,
                    },
                    new Wp.NonVisualGraphicFrameDrawingProperties(
                        new A.GraphicFrameLocks { NoChangeAspect = true }),
                    new A.Graphic(
                        new A.GraphicData(
                            new Pic.Picture(
                                new Pic.NonVisualPictureProperties(
                                    new Pic.NonVisualDrawingProperties
                                    {
                                        Id = 0U,
                                        Name = Name,
                                        Description = alternativeText,
                                    },
                                    new Pic.NonVisualPictureDrawingProperties()),
                                new Pic.BlipFill(
                                    new A.Blip(
                                        new A.BlipExtensionList(
                                            new A.BlipExtension(
                                                new A14.UseLocalDpi { Val = false })
                                            {
                                                Uri = "{28A0092B-C50C-407E-A947-70E740481C1C}",
                                            }))
                                    {
                                        Embed = relationId,
                                        CompressionState = A.BlipCompressionValues.Print,
                                    },
                                    new A.Stretch(new A.FillRectangle())),
                                new Pic.ShapeProperties(
                                    new A.Transform2D(
                                        new A.Offset
                                        {
                                            X = 0L,
                                            Y = 0L,
                                        },
                                        new A.Extents
                                        {
                                            Cx = WidthEmu,
                                            Cy = HeightEmu,
                                        }),
                                    new A.PresetGeometry(new A.AdjustValueList())
                                    {
                                        Preset = A.ShapeTypeValues.Rectangle,
                                    })))
                        {
                            Uri = "http://schemas.openxmlformats.org/drawingml/2006/picture",
                        }))
                {
                    DistanceFromTop = 0U,
                    DistanceFromBottom = 0U,
                    DistanceFromLeft = 0U,
                    DistanceFromRight = 0U,
                });
        }

        [SuppressMessage(
            "Sonar",
            "S3220:Method calls should not resolve ambiguously to overloads with \"params\"",
            Justification = "Reviewed")]
        private Word.Drawing CreateFloatingImage(string relationId, string alternativeText, OpenXmlPart currentPart)
        {
            var drawing1 = new Word.Drawing
            {
                Anchor = new Wp.Anchor(
                    new Wp.SimplePosition
                    {
                        X = 0L,
                        Y = 0L,
                    },
                    new Wp.HorizontalPosition
                    {
                        RelativeFrom = HorizontalRelativePosition,
                        PositionOffset = new Wp.PositionOffset
                        {
                            Text = HorizontalPositionOffset.ToString(CultureInfo.InvariantCulture),
                        },
                    },
                    new Wp.VerticalPosition
                    {
                        RelativeFrom = VerticalRelativePosition,
                        PositionOffset = new Wp.PositionOffset
                        {
                            Text = VerticalPositionOffset.ToString(CultureInfo.InvariantCulture),
                        },
                    },
                    new Wp.Extent
                    {
                        Cx = WidthEmu,
                        Cy = HeightEmu,
                    },
                    new Wp.EffectExtent
                    {
                        LeftEdge = 0L,
                        TopEdge = 0L,
                        RightEdge = 0L,
                        BottomEdge = 0L,
                    },
                    Wrap.CloneNode(true),
                    new Wp.DocProperties
                    {
                        Id = 5U,
                        Name = Name,
                        Description = alternativeText,
                        NonVisualDrawingPropertiesExtensionList = IsDecorative ? GetDecorativeExtensionList(currentPart) : null,
                    },
                    new Wp.NonVisualGraphicFrameDrawingProperties
                    {
                        GraphicFrameLocks = new A.GraphicFrameLocks { NoChangeAspect = true },
                    },
                    new A.Graphic
                    {
                        GraphicData = new A.GraphicData(
                                new Pic.Picture(
                                    new Pic.NonVisualPictureProperties(
                                        new Pic.NonVisualDrawingProperties
                                        {
                                            Id = ImageId,
                                            Name = Name,
                                            Description = alternativeText,
                                        },
                                        new Pic.NonVisualPictureDrawingProperties
                                        {
                                            PictureLocks = new A.PictureLocks
                                            {
                                                NoChangeAspect = true,
                                                NoChangeArrowheads = true,
                                            },
                                        }),
                                    new Pic.BlipFill(
                                        new A.Blip(
                                            new A.BlipExtensionList(
                                                new A.BlipExtension(
                                                    new A14.UseLocalDpi { Val = false })
                                                {
                                                    Uri = "{28A0092B-C50C-407E-A947-70E740481C1C}",
                                                }))
                                        {
                                            Embed = relationId,
                                            CompressionState = A.BlipCompressionValues.Print,
                                        },
                                        new A.SourceRectangle(),
                                        new A.Stretch
                                        {
                                            FillRectangle = new A.FillRectangle(),
                                        }),
                                    new Pic.ShapeProperties(
                                        new A.Transform2D
                                        {
                                            Offset = new A.Offset
                                            {
                                                X = 0L,
                                                Y = 0L,
                                            },
                                            Extents = new A.Extents
                                            {
                                                Cx = WidthEmu,
                                                Cy = HeightEmu,
                                            },
                                        },
                                        new A.PresetGeometry
                                        {
                                            Preset = A.ShapeTypeValues.Rectangle,
                                            AdjustValueList = new A.AdjustValueList(),
                                        })))
                            { Uri = "http://schemas.openxmlformats.org/drawingml/2006/picture" },
                    })
                {
                    DistanceFromTop = DistanceFromTop,
                    DistanceFromBottom = DistanceFromBottom,
                    DistanceFromLeft = DistanceFromLeft,
                    DistanceFromRight = DistanceFromRight,
                    SimplePos = false,
                    RelativeHeight = ZOrder,
                    BehindDoc = ZOrder == 0,
                    Locked = false,
                    LayoutInCell = true,
                    AllowOverlap = true,
                },
            };

            drawing1.AddNamespaceDeclaration("a14", "http://schemas.microsoft.com/office/drawing/2010/main");
            drawing1.AddNamespaceDeclaration("pic", "http://schemas.openxmlformats.org/drawingml/2006/picture");
            drawing1.AddNamespaceDeclaration("a", "http://schemas.openxmlformats.org/drawingml/2006/main");

            return drawing1;
        }

        protected ImagePartType GetImagePartType(string mimeType)
        {
            if (mimeType == "image/png")
            {
                return ImagePartType.Png;
            }

            if (mimeType == "image/jpeg")
            {
                return ImagePartType.Jpeg;
            }

            if (mimeType == "image/gif")
            {
                return ImagePartType.Gif;
            }

            if (mimeType == "image/tiff")
            {
                return ImagePartType.Tiff;
            }

            if (mimeType == "image/x-icon")
            {
                return ImagePartType.Icon;
            }

            if (mimeType == "image/x-emf")
            {
                return ImagePartType.Emf;
            }

            return ImagePartType.Bmp;
        }
    }
}
