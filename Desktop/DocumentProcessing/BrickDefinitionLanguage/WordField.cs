using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("span")]
    [BdlAttribute("data-type", "field")]
    [BdlRequiredAttributes("data-prop")]
    [BdlChildElements(ChildElements.None)]
    public class WordField : StyleableBdlElement, IOpenXmlConverter
    {
        private const string HyperlinkStyle = "Hyperlink";

        public string FieldCode { get; set; }

        public string FieldText { get; set; }

        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            (bool isHyperlink, string uri) = IsHyperlink();
            if (isHyperlink)
            {
                return CreateHyperlink(context, uri);
            }

            return CreateComplexField();
        }

        protected override void Parse()
        {
            base.Parse();

            FieldCode = GetDataPropAttribute()?.Value;
            FieldText = XmlElement.Value;
        }

        private (bool isHyperlink, string uri) IsHyperlink()
        {
            var match = Regex.Match(
                FieldCode,
                @"\bHYPERLINK\b\s*""?(?<uri>https?\:\/\/[^\s""]*)""?",
                RegexOptions.IgnoreCase);
            if (match.Success)
            {
                return (true, match.Groups["uri"].Value);
            }

            return (false, null);
        }

        private IEnumerable<OpenXmlElement> CreateHyperlink(OpenXmlConverterContext context, string uri)
        {
            AssertHyperlinkStyle(context);

            var rel = context.CurrentPart.AddHyperlinkRelationship(new Uri(uri), true);

            var result = new Word.Hyperlink(
                new Word.Run(
                    new Word.RunProperties(
                        new Word.RunStyle { Val = "Hyperlink" }),
                    new Word.Text { Text = FieldText }))
            {
                History = OnOffValue.FromBoolean(true),
                Id = rel.Id,
            };

            yield return result;
        }

        private void AssertHyperlinkStyle(OpenXmlConverterContext context)
        {
            var styles = context.Document.GetOrCreateStyleDefinitionsPart(context.Options.DocumentMode).Styles;

            var styleFootnoteReference = styles.Elements<Word.Style>()
                                               .FirstOrDefault(e => e.StyleId.Value == HyperlinkStyle);
            if (styleFootnoteReference == null)
            {
                // insert "Hyperlink" style
                styles.AppendChild(
                    new Word.Style
                    {
                        Type = Word.StyleValues.Character,
                        StyleId = HyperlinkStyle,
                        StyleName = new Word.StyleName { Val = "Hyperlink" },
                        BasedOn = new Word.BasedOn { Val = "DefaultParagraphFont" },
                        UIPriority = new Word.UIPriority { Val = 99 },
                        SemiHidden = new Word.SemiHidden(),
                        UnhideWhenUsed = new Word.UnhideWhenUsed(),
                        StyleRunProperties = new Word.StyleRunProperties
                        {
                            Color = new Word.Color
                            {
                                Val = "0563C1",
                                ThemeColor = Word.ThemeColorValues.Hyperlink,
                            },
                            Underline = new Word.Underline { Val = Word.UnderlineValues.Single },
                        },
                    });
            }
        }

        private IEnumerable<OpenXmlElement> CreateComplexField()
        {
            yield return GetFormattedRun(new Word.FieldChar { FieldCharType = Word.FieldCharValues.Begin });
            yield return GetFormattedRun(
                new Word.FieldCode { Space = SpaceProcessingModeValues.Preserve, Text = FieldCode });
            yield return GetFormattedRun(new Word.FieldChar { FieldCharType = Word.FieldCharValues.Separate });
            yield return GetFormattedRun(new Word.Text { Text = FieldText });
            yield return GetFormattedRun(new Word.FieldChar { FieldCharType = Word.FieldCharValues.End });
        }

        private Word.Run GetFormattedRun(params OpenXmlElement[] children)
        {
            if (string.IsNullOrEmpty(Style) && Regex.IsMatch(FieldCode, @"\bHYPERLINK\b", RegexOptions.IgnoreCase))
            {
                Style = "Hyperlink";
            }

            var runProperties = GetRunProperties();
            return new Word.Run(children.Prepend(runProperties));
        }
    }
}
