using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using Eurolook.DocumentProcessing.OpenXml;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("option")]
    [BdlAttribute("data-type", "binding")]
    [BdlRequiredAttributes("data-prop")]
    [BdlChildElements(ChildElements.None)]
    public class BoundChoiceOption : BdlElement, IOpenXmlConverter
    {
        public string Binding { get; set; }

        public bool Selected { get; set; }

        public string ResolveBinding(OpenXmlConverterContext context)
        {
            return OpenXmlConverterExtensions.ResolveBinding(Binding, context);
        }

        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            string text = ResolveBinding(context);
            if (text != null)
            {
                return new[]
                {
                    new ListItem
                    {
                        DisplayText = text,
                        Value = text,
                    },
                };
            }

            return Enumerable.Empty<OpenXmlElement>();
        }

        protected override void Parse()
        {
            base.Parse();

            Binding = GetDataPropAttribute().Value;
            Selected = XmlElement.Attribute("selected") != null;
        }
    }
}
