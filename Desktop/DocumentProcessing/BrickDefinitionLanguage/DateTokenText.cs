using System;
using System.Collections.Generic;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using Eurolook.DocumentProcessing.OpenXml;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("span")]
    [BdlAttribute("data-type", "date-text")]
    [BdlChildElements(ChildElements.None)]
    public class DateTokenText : DateTokenBase
    {
        public override IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            context.CurrentAuthorRolePriorityList = GetAuthorRolePriorityList();
            if (!OpenXmlConverterExtensions.IsVisible(GetVisibilityCondition(), context))
            {
                yield break;
            }

            string formattedDate = GetLocalizedDate(context, DateTime.Now);

            yield return new Run(
                GetRunProperties(),
                new Text { Text = formattedDate });
        }
    }
}
