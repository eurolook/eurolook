using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("tr")]
    [BdlChildElements(typeof(TableCell))]
    public class TableRow : BdlElement, IOpenXmlConverter
    {
        public TableRow()
        {
            // default settings
            RepeatHeader = false;
            AllowBreakAcrossPages = true;
        }

        public List<TableCell> Cells { get; private set; }

        public string Height { get; set; }

        public bool RepeatHeader { get; set; }

        public bool AllowBreakAcrossPages { get; set; }

        [SuppressMessage(
            "Sonar",
            "S3220:Method calls should not resolve ambiguously to overloads with \"params\"",
            Justification = "Reviewed")]
        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            var row = new Word.TableRow();
            var props = new Word.TableRowProperties();

            if (Height != null)
            {
                uint height = (uint)Math.Abs(this.ConvertToTwips(Height));
                props.AppendChild(
                    new Word.TableRowHeight
                    {
                        HeightType = Word.HeightRuleValues.AtLeast,
                        Val = height,
                    });
            }

            if (RepeatHeader)
            {
                props.AppendChild(new Word.TableHeader { Val = Word.OnOffOnlyValues.On });
            }

            if (!AllowBreakAcrossPages)
            {
                props.AppendChild(new Word.CantSplit());
            }

            row.AppendChild(props);
            if (Cells.Any())
            {
                row.Append(this.ConvertMany(Cells, context));
            }
            else
            {
                row.AppendChild(new Word.TableCell(new Word.Paragraph()));
            }

            return new OpenXmlElement[] { row };
        }

        protected override void Parse()
        {
            base.Parse();

            if (XmlElement.Attribute("height") != null)
            {
                Height = (string)XmlElement.Attribute("height");
            }

            if (XmlElement.Attribute("data-repeat-header") != null)
            {
                RepeatHeader = (bool)XmlElement.Attribute("data-repeat-header");
            }

            var allowBreakPagesAttribute = XmlElement.Attribute("data-allow-break-pages") ??
                                           XmlElement.Parent?.Attribute("data-allow-break-pages");
            if (allowBreakPagesAttribute != null)
            {
                AllowBreakAcrossPages = (bool)allowBreakPagesAttribute;
            }

            Cells = new List<TableCell>();
            var children = ParseChildren();
            if (!children.Any())
            {
                throw new InvalidBdlDocumentException("Table row must have table cell child element.");
            }

            foreach (var child in children.OfType<TableCell>())
            {
                Cells.Add(child);
            }
        }
    }
}
