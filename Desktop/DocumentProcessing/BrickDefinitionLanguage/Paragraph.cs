using System.Collections.Generic;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.DocumentProcessing.OpenXml;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("p")]
    [BdlChildElements(
        typeof(FreeText),
        typeof(BoundText),
        typeof(FormattedText.FormattedText),
        typeof(FormattedTextRun),
        typeof(PlainTextValue),
        typeof(SpecialCharacter),
        typeof(CheckBox),
        typeof(DateToken),
        typeof(DateTokenStatic),
        typeof(DateTokenText),
        typeof(DateTokenBound),
        typeof(Link),
        typeof(Metadata),
        typeof(FootnoteReference),
        typeof(Image),
        typeof(EmbeddedObject),
        typeof(SignatureLine),
        typeof(Choice),
        typeof(TermStoreChoice),
        typeof(WordField),
        typeof(WordFieldComplex),
        typeof(Break),
        typeof(DocumentPropertyCompatibility),
        typeof(TextBox))]
    public class Paragraph : StyleableBdlElement, IOpenXmlConverter, IContentElement
    {
        public List<BdlElement> Content { get; set; }

        public int? RestartListAt { get; set; }

        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            var paragraph = new Word.Paragraph { ParagraphProperties = new Word.ParagraphProperties() };

            if (!string.IsNullOrWhiteSpace(Style))
            {
                paragraph.ParagraphProperties.ParagraphStyleId = new Word.ParagraphStyleId
                {
                    Val = this.NormalizeStyleName(Style),
                };
            }

            if (Css != null)
            {
                // paragraph formatting
                paragraph.ParagraphProperties.Append(Css.KeepWithNext);
                paragraph.ParagraphProperties.Append(Css.PageBreakBefore);
                paragraph.ParagraphProperties.Append(Css.ParagraphSpacing);
                paragraph.ParagraphProperties.Append(Css.ParagraphIndentation);
                paragraph.ParagraphProperties.Append(Css.ParagraphJustification);

                // apply font formatting to the paragraph-mark
                paragraph.ParagraphProperties.ParagraphMarkRunProperties = GetParagraphMarkRunProperties();
            }

            if (RestartListAt.HasValue && !string.IsNullOrWhiteSpace(Style))
            {
                var numberingHelper = new NumberingHelper();
                numberingHelper.RestartList(context, paragraph, RestartListAt.Value);
            }

            var contents = this.ConvertMany(Content, context);
            paragraph.Append(contents);
            return new OpenXmlElement[] { paragraph };
        }

        protected override void Parse()
        {
            base.Parse();

            Content = ParseChildren();

            if (XmlElement.TryParseAttribute("data-list-start", out int restartListAt))
            {
                RestartListAt = restartListAt;
            }
            else if (XmlElement.Attribute("data-list-start") != null
                     || XmlElement.Attribute("data-list-restart") != null)
            {
                RestartListAt = 1;
            }

            if (Content.Count == 0 && !string.IsNullOrWhiteSpace(XmlElement.Value))
            {
                Content.Add(new PlainTextValue(XmlElement.Value, this));
            }
        }
    }
}
