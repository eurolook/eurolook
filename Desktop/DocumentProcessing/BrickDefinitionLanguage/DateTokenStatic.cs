using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("span")]
    [BdlAttribute("data-type", "date-static")]
    [BdlChildElements(ChildElements.None)]
    public class DateTokenStatic : DateTokenBase
    {
        public string PlaceholderText { get; set; }

        public string Title { get; set; }

        [SuppressMessage(
            "Sonar",
            "S3220:Method calls should not resolve ambiguously to overloads with \"params\"",
            Justification = "Reviewed")]
        public override IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            context.CurrentAuthorRolePriorityList = GetAuthorRolePriorityList();
            if (!OpenXmlConverterExtensions.IsVisible(GetVisibilityCondition(), context))
            {
                yield break;
            }

            string dateFormat = GetLocalizedDateFormat(context);
            if (dateFormat == null)
            {
                yield break;
            }

            var date = DateTime.Now;
            bool hasPlaceholder = !string.IsNullOrEmpty(PlaceholderText);

            var contentControl = new Word.SdtRun();
            var sdtProperties = new Word.SdtProperties();
            contentControl.AppendChild(sdtProperties);

            sdtProperties.AppendChild(
                new Word.SdtAlias
                {
                    Val = string.IsNullOrEmpty(Title) ? "" : Title,
                });

            // 1. Run Properties
            var runProperties = GetRunProperties();
            sdtProperties.AppendChild(runProperties);

            // 2. Placeholder
            if (hasPlaceholder)
            {
                OpenXmlConverterExtensions.AttachPlaceholder(
                    context.Document,
                    contentControl,
                    PlaceholderText,
                    Document.DefaultPlaceholderStyle,
                    context.Language.Locale,
                    true);
            }

            // 3. Date Properties
            sdtProperties.AppendChild(
                new Word.SdtContentDate
                {
                    FullDate = date,
                    DateFormat = new Word.DateFormat { Val = dateFormat },
                    LanguageId = new Word.LanguageId { Val = context.Language.Locale },
                    Calendar = new Word.Calendar { Val = Word.CalendarValues.Gregorian },
                    SdtDateMappingType =
                        new Word.SdtDateMappingType { Val = Word.DateFormatValues.DateTime },
                });

            // 5. Content: current date if there is no placeholder set
            if (!hasPlaceholder)
            {
                // if placeholder is set, do not insert current date
                string formattedDate = GetLocalizedDate(context, date);
                contentControl.AppendChild(
                    new Word.SdtContentRun(
                        new Word.Run(
                            new Word.Text { Text = formattedDate })));
            }

            yield return contentControl;
        }

        protected override void Parse()
        {
            base.Parse();

            PlaceholderText = XmlElement.Value;
            Title = (string)XmlElement.Attribute("data-title");
        }
    }
}
