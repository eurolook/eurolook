using System.Collections.Generic;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.DocumentProcessing.OpenXml;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("span")]
    [BdlAttribute("data-type", "date-bound")]
    [BdlRequiredAttributes("data-prop", "data-path")]
    [BdlChildElements(ChildElements.None)]
    public class DateTokenBound : DateTokenBase
    {
        public bool IsLocked { get; set; }

        public string Binding { get; set; }

        public string PlaceholderText { get; set; }

        public string Title { get; set; }

        public override IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            context.CurrentAuthorRolePriorityList = GetAuthorRolePriorityList();
            if (!OpenXmlConverterExtensions.IsVisible(GetVisibilityCondition(), context))
            {
                yield break;
            }

            string dateFormat = GetLocalizedDateFormat(context);
            if (dateFormat == null)
            {
                yield break;
            }

            bool hasPlaceholder = !string.IsNullOrEmpty(PlaceholderText);

            var contentControl = new Word.SdtRun();
            var sdtProperties = new Word.SdtProperties();
            contentControl.AppendChild(sdtProperties);

            sdtProperties.AppendChild(
                new Word.SdtAlias
                {
                    Val = string.IsNullOrEmpty(Title) ? "" : Title,
                });

            // Lock
            if (IsLocked)
            {
                sdtProperties.AppendChild(new Word.Lock { Val = Word.LockingValues.ContentLocked });
            }

            // Run Properties
            var runProperties = GetRunProperties();
            sdtProperties.AppendChild(runProperties);

            // Placeholder
            if (hasPlaceholder)
            {
                OpenXmlConverterExtensions.AttachPlaceholder(
                    context.Document,
                    contentControl,
                    PlaceholderText,
                    Document.DefaultPlaceholderStyle,
                    context.Language.Locale,
                    true);
            }

            // Binding
            var dataBinding = OpenXmlUtils.GetDataBinding(Binding, context);
            if (dataBinding != null)
            {
                sdtProperties.AppendChild(dataBinding);
            }

            // Date Properties
            sdtProperties.AppendChild(
                new Word.SdtContentDate
                {
                    ////FullDate = date,
                    DateFormat = new Word.DateFormat { Val = dateFormat },
                    LanguageId = new Word.LanguageId { Val = context.Language.Locale },
                    Calendar = new Word.Calendar { Val = Word.CalendarValues.Gregorian },
                    SdtDateMappingType =
                        new Word.SdtDateMappingType { Val = Word.DateFormatValues.DateTime },
                });

            yield return contentControl;
        }

        protected override void Parse()
        {
            base.Parse();

            IsLocked = false;
            if (XmlElement.TryParseAttribute("data-lock", out bool dataLock))
            {
                IsLocked = dataLock;
            }

            Binding = (string)XmlElement.Attribute("data-path");
            PlaceholderText = XmlElement.Value;
            Title = (string)XmlElement.Attribute("data-title");
        }
    }
}
