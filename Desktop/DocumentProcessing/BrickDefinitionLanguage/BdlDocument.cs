using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Xml.Linq;
using DocumentFormat.OpenXml;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing.BrickDefinitionLanguage.Section;
using Eurolook.DocumentProcessing.OpenXml;
using W15 = DocumentFormat.OpenXml.Office2013.Word;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlChildElements(
        typeof(Paragraph),
        typeof(Table),
        typeof(PageBreak),
        typeof(SectionBreak),
        typeof(TableOfContents),
        typeof(Footnote),
        typeof(DocumentProperty))]
    public class BdlDocument : BdlElement, IOpenXmlConverter, IContentElement
    {
        public BdlDocument()
        {
            DefaultPlaceholderStyle = "PlaceholderText";
        }

        public List<BdlElement> Content { get; set; }

        public List<Footnote> Footnotes { get; set; }

        public ContainerType ContainerType { get; set; }

        public bool IsLocked { get; set; }

        public string DefaultPlaceholderStyle { get; set; }

        public static BdlDocument Parse(string xml)
        {
            return Parse(XDocument.Parse(xml));
        }

        public static BdlDocument Parse(XDocument xml)
        {
            if (xml.Root == null)
            {
                throw new InvalidBdlDocumentException("Document is empty");
            }

            var bdl = new BdlDocument();
            bdl.Parse(xml.Root);
            return bdl;
        }

        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            return WrapInBrickContainer(context.Brick, this.ConvertMany(Content, context), context);
        }

        public T[] FindContentOfType<T>()
        {
            var result = new List<T>();
            FindBdlElementsRecursive(this, result);
            foreach (var footnote in Footnotes)
            {
                FindBdlElementsRecursive(footnote, result);
            }

            return result.ToArray();
        }

        protected override void Parse()
        {
            base.Parse();

            if (!XmlElement.Name.ToString().Equals("html", StringComparison.InvariantCultureIgnoreCase))
            {
                throw new InvalidBdlDocumentException("Root element must be 'html'");
            }

            ContainerType = ContainerType.Permanent;
            var dataContainerAttr = XmlElement.Attribute("data-container");
            if (dataContainerAttr != null)
            {
                if (dataContainerAttr.Value == "vanishing")
                {
                    ContainerType = ContainerType.Vanishing;
                }
                else if (dataContainerAttr.Value == "hidden")
                {
                    ContainerType = ContainerType.Hidden;
                }
                else if (dataContainerAttr.Value == "none")
                {
                    ContainerType = ContainerType.None;
                }
            }

            var dataLockAttr = XmlElement.Attribute("data-lock");
            if (dataLockAttr != null)
            {
                bool.TryParse(dataLockAttr.Value, out bool isLocked);
                IsLocked = isLocked;
            }

            var children = ParseChildren();
            Footnotes = children.OfType<Footnote>().ToList();
            Content = children.Except(Footnotes).ToList();
        }

        private static void FindBdlElementsRecursive<T>(IContentElement bdlElement, List<T> result)
        {
            foreach (var child in bdlElement.Content)
            {
                if (child is T x)
                {
                    result.Add(x);
                }

                if (child is IContentElement y)
                {
                    FindBdlElementsRecursive(y, result);
                }
            }
        }

        private IEnumerable<OpenXmlElement> WrapInBrickContainer(
            ContentBrick brick,
            OpenXmlElement[] brickContents,
            OpenXmlConverterContext context)
        {
            if (ContainerType == ContainerType.None)
            {
                return brickContents;
            }

            Word.SdtProperties sdtProperties;
            OpenXmlElement[] result;

            if (IsSimpleBrick(brickContents))
            {
                sdtProperties = brickContents.First().GetFirstChild<Word.SdtRun>().SdtProperties;
                var temporary = sdtProperties.GetFirstChild<Word.TemporarySdt>();
                temporary?.Remove();

                var appearance = sdtProperties.GetFirstChild<W15.Appearance>();
                appearance?.Remove();

                result = brickContents;
            }
            else
            {
                sdtProperties = new Word.SdtProperties();
                result = new OpenXmlElement[]
                {
                    new Word.SdtBlock
                    {
                        SdtProperties = sdtProperties,
                        SdtContentBlock = new Word.SdtContentBlock(brickContents),
                    },
                };
            }

            var brickContainer = new BrickContainer(brick);

            // set alias for all non-ToC bricks (ToC already gets the built-in Word "pseudo" content control and the container label should
            // not overlap with the "Update ToC" button so we leave the display name empty for ToC bricks)
            if (!(Content.Any(el => el is TableOfContents) && context.Options.CreateTableOfContentsContainer))
            {
                sdtProperties.AppendChild(new Word.SdtAlias { Val = brickContainer.ContainerDisplayName });
            }

            // lock
            bool hasVanishingControls = brick.Content.Descendants().Attributes("data-type")
                                             .Any(attr => (string)attr == "freetext");
            if (!hasVanishingControls && IsLocked)
            {
                var existingLock = sdtProperties.GetFirstChild<Word.Lock>();
                existingLock?.Remove();

                sdtProperties.AppendChild(new Word.Lock { Val = Word.LockingValues.ContentLocked });
            }

            sdtProperties.AppendChild(
                new W15.Appearance
                {
                    Val = ContainerType == ContainerType.Hidden
                        ? W15.SdtAppearance.Hidden
                        : W15.SdtAppearance.BoundingBox,
                });

            if (ContainerType == ContainerType.Vanishing)
            {
                sdtProperties.AppendChild(new Word.TemporarySdt());
            }

            // tag with brick info
            sdtProperties.AppendChild(new Word.Tag { Val = brickContainer.ToTag() });

            if (context.Options.UseCustomBrickContainerColor)
            {
                string color = ColorTranslator.ToHtml(context.Options.BrickContainerColor);
                sdtProperties.AppendChild(new W15.Color { Val = color });
            }

            return result;
        }

        /// <summary>
        /// Checks if the contents of a generated brick is a "simple" brick. Simple bricks are bricks that consist
        /// of a single paragraph containing only a single content control.
        /// Simple bricks do not get an additional container content control, but we re-use the
        /// single content control of the brick as the container.
        /// </summary>
        /// <param name="brickElements">The OpenXmlElements of the converted brick.</param>
        /// <returns>Returns true, if the brick consists of a single content control only.</returns>
        private bool IsSimpleBrick(IEnumerable<OpenXmlElement> brickElements)
        {
            var elements = brickElements.ToList();
            if (elements.Count != 1)
            {
                return false;
            }

            var paragraph = elements.First() as Word.Paragraph;
            if (paragraph == null)
            {
                return false;
            }

            bool singleSdtRun =
                (paragraph.ChildElements.Count == 1 && paragraph.ChildElements.First() is Word.Paragraph)
                || (paragraph.ChildElements.Count == 2 && paragraph.ChildElements[0] is Word.ParagraphProperties
                                                       && paragraph.ChildElements[1] is Word.SdtRun);
            if (!singleSdtRun)
            {
                return false;
            }

            var sdt = paragraph.GetFirstChild<Word.SdtRun>();
            if (sdt == null)
            {
                return false;
            }

            return true;
        }
    }
}
