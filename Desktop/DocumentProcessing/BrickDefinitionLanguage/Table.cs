﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("table")]
    [BdlChildElements(typeof(TableRow))]
    public class Table : StyleableBdlElement, IOpenXmlConverter
    {
        public List<TableRow> Rows { get; set; }

        public string Width { get; set; }

        public string AltText { get; set; }

        public Word.TableLook TableLook { get; set; }

        [SuppressMessage("Sonar", "S3220:Method calls should not resolve ambiguously to overloads with \"params\"", Justification = "OK")]
        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            var table = new Word.Table();
            var props = new Word.TableProperties();
            var grid = new Word.TableGrid();
            table.AppendChild(props);
            table.AppendChild(grid);

            // table style
            var tableStyle = !string.IsNullOrWhiteSpace(Style)
                ? this.NormalizeStyleName(Style)
                : this.NormalizeStyleName("Table Grid");
            props.TableStyle = new Word.TableStyle
            {
                Val = tableStyle,
            };

            // table grid
            var gridInfo = GetGridInfo();
            foreach (int dxaValue in gridInfo)
            {
                grid.AppendChild(new Word.GridColumn { Width = dxaValue.ToString(CultureInfo.InvariantCulture) });
            }

            // width
            props.TableWidth = new Word.TableWidth
            {
                Type = Word.TableWidthUnitValues.Dxa,
                Width = gridInfo.Sum().ToString(CultureInfo.InvariantCulture),
            };

            // table layout
            if (Css?.TableLayout != null)
            {
                props.TableLayout = Css.TableLayout;
            }

            // floating table properties
            if (Css?.TablePositionProperties != null)
            {
                props.TablePositionProperties = Css.TablePositionProperties;
            }

            // indent
            if (Css?.TableIndent != null)
            {
                props.TableIndentation = Css.TableIndent;
            }

            if (TableLook != null)
            {
                props.TableLook = TableLook;
            }

            // alt text
            if (!string.IsNullOrEmpty(AltText))
            {
                props.TableDescription = new Word.TableDescription { Val = AltText };
            }

            // content
            if (Rows.Any())
            {
                table.Append(this.ConvertMany(Rows, context));
            }
            else
            {
                table.AppendChild(new Word.TableRow(new Word.TableCell(new Word.Paragraph())));
            }

            return new OpenXmlElement[] { table };
        }

        protected override void Parse()
        {
            base.Parse();

            var widthAttr = XmlElement.Attribute("width");
            if (widthAttr != null)
            {
                Width = widthAttr.Value;
            }

            var tableAltTextAttr = XmlElement.Attribute("alt");
            if (tableAltTextAttr != null)
            {
                AltText = tableAltTextAttr.Value;
            }

            var tableLookAttr = XmlElement.Attribute("data-table-look");
            if (tableLookAttr != null)
            {
                var tableLookParser = new TableLookParser(tableLookAttr.Value);
                TableLook = tableLookParser.TableLook;
            }

            Rows = new List<TableRow>();
            var children = ParseChildren();
            if (!children.Any())
            {
                throw new InvalidBdlDocumentException("Table must have table row child element.");
            }

            foreach (var child in children.OfType<TableRow>())
            {
                Rows.Add(child);
            }
        }

        private int[] GetGridInfo()
        {
            // determine the size of the grid
            int gridSize = 0;
            foreach (var cell in Rows.First().Cells)
            {
                if (cell.ColSpan > 1)
                {
                    gridSize += cell.ColSpan;
                }
                else
                {
                    gridSize += 1;
                }
            }

            // get the gridlines
            var gridLines = new int[gridSize];
            for (int i = 0; i < gridSize; i++)
            {
                gridLines[i] = FindGridLine(i + 1);
            }

            // calculate the grid widths
            var grid = new int[gridSize];
            for (int i = gridSize - 1; i > 0; i--)
            {
                grid[i] = gridLines[i] - gridLines[i - 1];
            }

            grid[0] = gridLines[0];
            return grid;
        }

        /// <summary>
        /// </summary>
        /// <param name="i">The 1-based index.</param>
        /// <returns>The width of the grid in dxa.</returns>
        private int FindGridLine(int i)
        {
            foreach (var row in Rows)
            {
                int virtualGridLine = 0;
                int virtualWidthDxa = 0;
                foreach (var cell in row.Cells)
                {
                    virtualWidthDxa += this.ConvertToTwips(cell.Width);

                    if (cell.ColSpan > 1)
                    {
                        virtualGridLine += cell.ColSpan;
                    }
                    else
                    {
                        virtualGridLine += 1;
                    }

                    if (virtualGridLine == i)
                    {
                        return virtualWidthDxa;
                    }
                }
            }

            return -1;
        }

        private class TableLookParser
        {
            private const string FirstRow = "first-row";
            private const string LastRow = "last-row";
            private const string FirstColumn = "first-column";
            private const string LastColumn = "last-column";
            private const string HorizontalBand = "horizontal-band";
            private const string VerticalBand = "vertical-band";

            public TableLookParser(string tableLook)
            {
                TableLookMap = new Dictionary<string, bool>();
                if (!string.IsNullOrWhiteSpace(tableLook))
                {
                    foreach (string cssInstr in tableLook.Split(';'))
                    {
                        var cssInstrParts = cssInstr.Split(':');
                        if (cssInstrParts.Length == 2)
                        {
                            string property = cssInstrParts[0].Trim().ToLowerInvariant();
                            string value = cssInstrParts[1].Trim();

                            if (bool.TryParse(value, out bool result))
                            {
                                TableLookMap[property] = result;
                            }
                        }
                    }
                }
            }

            public Dictionary<string, bool> TableLookMap { get; }

            public Word.TableLook TableLook
            {
                get
                {
                    // use the default values used by Word when no value is specified (e.g. turn banded rows on by default)
                    return new Word.TableLook
                    {
                        FirstRow = GetTableLookValue(FirstRow, true),
                        LastRow = GetTableLookValue(LastRow, false),
                        FirstColumn = GetTableLookValue(FirstColumn, true),
                        LastColumn = GetTableLookValue(LastColumn, false),

                        // Note that OpenXML uses negated boolean flags for banded rows/columns
                        NoHorizontalBand = !GetTableLookValue(HorizontalBand, true),
                        NoVerticalBand = !GetTableLookValue(VerticalBand, false),
                    };
                }
            }

            private bool GetTableLookValue(string propertyName, bool defaultValue)
            {
                return TableLookMap.ContainsKey(propertyName) ? TableLookMap[propertyName] : defaultValue;
            }
        }
    }
}
