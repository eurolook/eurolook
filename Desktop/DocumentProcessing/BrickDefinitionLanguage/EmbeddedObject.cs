using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing.OpenXml;
using Ovml = DocumentFormat.OpenXml.Vml.Office;
using V = DocumentFormat.OpenXml.Vml;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("embed")]
    [BdlChildElements(ChildElements.None)]
    public class EmbeddedObject : Image
    {
        public string PreviewSource { get; set; }

        public override IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            var run = new Word.Run();
            if (Source != null)
            {
                var resource = context.ResourceResolver.ResolveLocalisedResource(Source);
                var resourcePreview = context.ResourceResolver.ResolveLocalisedResource(PreviewSource);

                if (resource?.RawData != null && resource.RawData.Length > 0
                                              && resourcePreview != null && resourcePreview.RawData.Length > 0)
                {
                    string relIdEmbeddedObject = context.Options.DocumentMode == WordprocessingDocumentMode.MainDocument
                        ? CreateEmbeddedObjectPart(context.CurrentPart, resource)
                        : CreateEmbeddedObjectPart(context.Document.GetOrCreateGlossaryDocumentPart(), resource);

                    string relIdPreviewImage = context.CurrentPart.AddImage(
                        resourcePreview.RawData,
                        GetImagePartType(resourcePreview.MimeType));

                    string alternativeText =
                        context.ResourceResolver.ResolveTranslation(AlternativeTextAlias)?.Value
                        ?? AlternativeTextAlias;

                    var embeddedObject = IsFloating
                        ? CreateFloatingEmbeddedObject(relIdEmbeddedObject, relIdPreviewImage, alternativeText)
                        : CreateInlineEmbeddedObject(relIdEmbeddedObject, relIdPreviewImage, alternativeText);
                    run.AppendChild(embeddedObject);
                }
            }

            return new OpenXmlElement[] { run };
        }

        protected override void Parse()
        {
            PreviewSource = (string)XmlElement.Attribute("data-preview-source");

            base.Parse();
        }

        private string CreateEmbeddedObjectPart(OpenXmlPart currentPart, LocalisedResource localisedResource)
        {
            var rId = $"rId{Guid.NewGuid():N}";
            var targetPart = currentPart.AddNewPart<EmbeddedPackagePart>(
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                rId);

            using (var stream = new MemoryStream(localisedResource.RawData))
            {
                targetPart.FeedData(stream);
            }

            return currentPart.GetIdOfPart(targetPart);
        }

        private Word.EmbeddedObject CreateInlineEmbeddedObject(
            string relIdEmbeddedObject,
            string relIdPreviewImage,
            string alternativeText)
        {
            var embeddedObject = new Word.EmbeddedObject
            {
                DxaOriginal = CssUnitConverter.ConvertToTwipsString(Width),
                DyaOriginal = CssUnitConverter.ConvertToTwipsString(Height),
            };

            var shapeTypeId = Guid.NewGuid().ToString("N");
            var shapeId = Guid.NewGuid().ToString("N");

            var shapeType = new V.Shapetype
            {
                Id = shapeTypeId,
                CoordinateSize = "21600,21600",
                Filled = false,
                Stroked = false,
                OptionalNumber = 75,
                PreferRelative = true,
                EdgePath = "m@4@5l@4@11@9@11@9@5xe",
            };

            var formulas = new V.Formulas();
            formulas.AppendChild(new V.Formula { Equation = "if lineDrawn pixelLineWidth 0" });
            formulas.AppendChild(new V.Formula { Equation = "sum @0 1 0" });
            formulas.AppendChild(new V.Formula { Equation = "sum 0 0 @1" });
            formulas.AppendChild(new V.Formula { Equation = "prod @2 1 2" });
            formulas.AppendChild(new V.Formula { Equation = "prod @3 21600 pixelWidth" });
            formulas.AppendChild(new V.Formula { Equation = "prod @3 21600 pixelHeight" });
            formulas.AppendChild(new V.Formula { Equation = "sum @0 0 1" });
            formulas.AppendChild(new V.Formula { Equation = "prod @6 1 2" });
            formulas.AppendChild(new V.Formula { Equation = "prod @7 21600 pixelWidth" });
            formulas.AppendChild(new V.Formula { Equation = "sum @8 21600 0" });
            formulas.AppendChild(new V.Formula { Equation = "prod @7 21600 pixelHeight" });
            formulas.AppendChild(new V.Formula { Equation = "sum @10 21600 0" });

            shapeType.AppendChild(new V.Stroke { JoinStyle = V.StrokeJoinStyleValues.Miter });
            shapeType.AppendChild(formulas);
            shapeType.AppendChild(
                new V.Path
                {
                    AllowGradientShape = true,
                    ConnectionPointType = Ovml.ConnectValues.Rectangle,
                    AllowExtrusion = false,
                });
            shapeType.AppendChild(
                new Ovml.Lock
                {
                    Extension = V.ExtensionHandlingBehaviorValues.Edit,
                    AspectRatio = true,
                });

            var shape = new V.Shape
            {
                Id = shapeId,
                Style =
                    $"width:{CssUnitConverter.ConvertToPointsString(Width)}pt;height:{CssUnitConverter.ConvertToPointsString(Height)}pt",
                Alternate = alternativeText,
                Ole = false,
                Type = $"#{shapeTypeId}",
            };

            shape.AppendChild(
                new V.ImageData
                {
                    Title = "",
                    RelationshipId = relIdPreviewImage,
                });

            var oleObject = new Ovml.OleObject
            {
                Type = Ovml.OleValues.Embed,
                ProgId = "Excel.Sheet.12",
                ShapeId = shapeId,
                DrawAspect = Ovml.OleDrawAspectValues.Content,
                ObjectId = Guid.NewGuid().ToString("N"),
                Id = relIdEmbeddedObject,
            };

            embeddedObject.AppendChild(shapeType);
            embeddedObject.AppendChild(shape);
            embeddedObject.AppendChild(oleObject);

            return embeddedObject;
        }

        [SuppressMessage("ReSharper", "UnusedParameter.Local", Justification = "Method is not implemented.")]
        private Word.EmbeddedObject CreateFloatingEmbeddedObject(
            string relationId,
            string relIdPreviewImage,
            string alternativeText)
        {
            throw new NotSupportedException();
        }
    }
}
