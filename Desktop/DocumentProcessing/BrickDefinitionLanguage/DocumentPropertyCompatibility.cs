namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    /// <summary>
    /// This class was introduced to maintain backwards compatibility with clients older than the 1908 release.
    /// For newer brick better use the class <see cref="DocumentProperty" />.
    /// </summary>
    /// <example>
    ///     <code>
    /// <span data-type="document-property" data-prop="/customprops/MarkingType" data-value="SPECIAL HANDLING" />
    /// </code>
    /// </example>
    [BdlElement("span")]
    [BdlAttribute("data-type", "document-property")]
    [BdlRequiredAttributes("data-prop", "data-value")]
    [BdlChildElements(ChildElements.None)]
    public class DocumentPropertyCompatibility : DocumentProperty
    {
        protected override void Parse()
        {
            base.Parse();
            var dataValueAttribute = XmlElement.Attribute("data-value");
            PropertyValue = dataValueAttribute != null ? dataValueAttribute.Value : XmlElement.Value;
        }
    }
}
