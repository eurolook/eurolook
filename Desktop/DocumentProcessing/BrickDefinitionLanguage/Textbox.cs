﻿using System;
using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;
using A = DocumentFormat.OpenXml.Drawing;
using Word = DocumentFormat.OpenXml.Wordprocessing;
using Wp = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using Wp14 = DocumentFormat.OpenXml.Office2010.Word.Drawing;
using Wps = DocumentFormat.OpenXml.Office2010.Word.DrawingShape;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("div")]
    [BdlAttribute("data-type", "textbox")]
    [BdlRequiredAttributes("style")]
    [BdlChildElements(typeof(Paragraph))]
    public class TextBox : AccessibleElementBase, IOpenXmlConverter, IContentElement
    {
        private EnumValue<A.TextVerticalValues> _vertical;

        public uint TextboxId { get; set; }

        public List<BdlElement> Content { get; set; }

        public string Width { get; set; }

        public int WidthEmu
        {
            get { return CssUnitConverter.ConvertToEmu(Width); }
        }

        public string Height { get; set; }

        public int HeightEmu
        {
            get { return CssUnitConverter.ConvertToEmu(Height); }
        }

        public string RelativeFrom { get; set; }

        public string WrappingStyle { get; set; }

        public bool AnchorLock { get; set; }

        public bool AnchorMove { get; set; }

        public Wp.HorizontalRelativePositionValues HorizontalRelativePosition { get; set; }

        public string HorizontalPositionOffset { get; set; }

        public Wp.VerticalRelativePositionValues VerticalRelativePosition { get; set; }

        public string VerticalPositionOffset { get; set; }

        public int MarginLeft { get; set; }

        public int MarginRight { get; set; }

        public int MarginTop { get; set; }

        public int MarginBottom { get; set; }

        public string TextDirection { get; set; }

        public bool BehindDocument { get; set; }

        public string VerticalAlign { get; set; }

        public string HorizontalAlign { get; set; }

        public bool NoLine { get; set; }

        public bool AutoFit { get; set; } = true;

        public string BackgroundFilling { get; set; }

        public int Transparency { get; set; }

        public string BackgroundColor { get; set; }

        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            var drawing = new Word.Drawing(
                new Wp.Anchor(
                    new Wp.SimplePosition
                    {
                        X = 0L,
                        Y = 0L,
                    },
                    new Wp.HorizontalPosition(
                        HorizontalAlign != null
                            ? new Wp.HorizontalAlignment { Text = HorizontalAlign }
                            : new Wp.PositionOffset { Text = HorizontalPositionOffset })
                    {
                        RelativeFrom = HorizontalRelativePosition,
                    },
                    new Wp.VerticalPosition(
                        VerticalAlign != null
                            ? new Wp.VerticalAlignment { Text = VerticalAlign }
                            : new Wp.PositionOffset { Text = VerticalPositionOffset })
                    {
                        RelativeFrom = VerticalRelativePosition,
                    },
                    new Wp.Extent
                    {
                        Cx = WidthEmu,
                        Cy = HeightEmu,
                    },
                    new Wp.EffectExtent
                    {
                        LeftEdge = 0L,
                        TopEdge = 0L,
                        RightEdge = 0L,
                        BottomEdge = 0L,
                    },
                    WrappingStyle == "square"
                        ? new Wp.WrapSquare { WrapText = Wp.WrapTextValues.BothSides }
                        : WrappingStyle == "topbottom"
                            ? new Wp.WrapTopBottom()
                            : WrappingStyle == "through"
                                ? new Wp.WrapThrough(
                                    new Wp.WrapPolygon(
                                        new Wp.StartPoint
                                        {
                                            X = 0L,
                                            Y = 0L,
                                        },
                                        new Wp.LineTo
                                        {
                                            X = 0L,
                                            Y = 21316L,
                                        },
                                        new Wp.LineTo
                                        {
                                            X = 21316L,
                                            Y = 21316L,
                                        },
                                        new Wp.LineTo
                                        {
                                            X = 21316L,
                                            Y = 0L,
                                        },
                                        new Wp.LineTo
                                        {
                                            X = 0L,
                                            Y = 0L,
                                        }) { Edited = false }) { WrapText = Wp.WrapTextValues.BothSides }
                                : new Wp.WrapNone(),
                    new Wp.DocProperties
                    {
                        Id = TextboxId,
                        Name = "Text Box 2",
                        NonVisualDrawingPropertiesExtensionList = IsDecorative ? GetDecorativeExtensionList(context.CurrentPart) : null,
                    },
                    new Wp.NonVisualGraphicFrameDrawingProperties
                    {
                        GraphicFrameLocks = new A.GraphicFrameLocks(),
                    },
                    new A.Graphic
                    {
                        GraphicData = new A.GraphicData(
                            WordProcessingShape(context))
                        {
                            Uri = "http://schemas.microsoft.com/office/word/2010/wordprocessingShape",
                        },
                    },
                    new Wp14.RelativeWidth
                    {
                        ObjectId = Wp14.SizeRelativeHorizontallyValues.Margin,
                        PercentageWidth = new Wp14.PercentageWidth { Text = "0" },
                    },
                    new Wp14.RelativeHeight
                    {
                        RelativeFrom = Wp14.SizeRelativeVerticallyValues.Margin,
                        PercentageHeight = new Wp14.PercentageHeight { Text = "0" },
                    })
                {
                    SimplePos = false,
                    RelativeHeight = (UInt32Value)251659264U,
                    BehindDoc = BehindDocument,
                    Locked = AnchorLock,
                    LayoutInCell = true,
                    AllowOverlap = true,
                });
            drawing.AddNamespaceDeclaration("a", "http://schemas.openxmlformats.org/drawingml/2006/main");

            return new OpenXmlElement[] { new Word.Run(drawing) };
        }

        protected override void Parse()
        {
            IsValidTextboxInput();

            base.Parse();

            if (XmlElement.Attribute("data-position") == null)
            {
                RelativeFrom = "margin";
            }
            else
            {
                RelativeFrom = (string)XmlElement.Attribute("data-position");
            }

            if (XmlElement.Attribute("data-wrap") == null)
            {
                WrappingStyle = "square";
            }
            else
            {
                WrappingStyle = (string)XmlElement.Attribute("data-wrap");
            }

            if (XmlElement.Attribute("data-anchor-lock") != null)
            {
                AnchorLock = (bool)XmlElement.Attribute("data-anchor-lock");
            }

            if (XmlElement.Attribute("data-anchor-wove") != null)
            {
                AnchorMove = (bool)XmlElement.Attribute("data-anchor-move");
            }

            if (XmlElement.Attribute("data-text-direction") == null)
            {
                TextDirection = "horizontal";
            }
            else
            {
                TextDirection = (string)XmlElement.Attribute("data-text-direction");
            }

            if (XmlElement.Attribute("data-no-border") != null)
            {
                NoLine = (bool)XmlElement.Attribute("data-no-border");
            }

            if (XmlElement.Attribute("data-autofit") != null)
            {
                AutoFit = (bool)XmlElement.Attribute("data-autofit");
            }

            if (XmlElement.Attribute("data-background-fill") == null)
            {
                BackgroundFilling = "solid";
            }
            else
            {
                BackgroundFilling = (string)XmlElement.Attribute("data-background-fill");
            }

            if (XmlElement.Attribute("data-background-color") == null)
            {
                BackgroundColor = "FFFFFF";
            }
            else
            {
                BackgroundColor = (string)XmlElement.Attribute("data-background-color");
            }

            Css?.TryApplyProperty(CssProperty.Width, val => Width = val);
            Css?.TryApplyProperty(CssProperty.Height, val => Height = val);
            Css?.TryApplyProperty(
                CssProperty.Left,
                val => HorizontalPositionOffset = CssUnitConverter.ConvertToEmu(val).ToString());
            Css?.TryApplyProperty(
                CssProperty.Top,
                val => VerticalPositionOffset = CssUnitConverter.ConvertToEmu(val).ToString());
            Css?.TryApplyProperty(CssProperty.VerticalAlign, val => VerticalAlign = val);
            Css?.TryApplyProperty(CssProperty.HorizontalAlign, val => HorizontalAlign = val);
            Css?.TryApplyProperty(CssProperty.MarginLeft, val => MarginLeft = CssUnitConverter.ConvertToEmu(val));
            Css?.TryApplyProperty(CssProperty.MarginTop, val => MarginTop = CssUnitConverter.ConvertToEmu(val));
            Css?.TryApplyProperty(CssProperty.MarginRight, val => MarginRight = CssUnitConverter.ConvertToEmu(val));
            Css?.TryApplyProperty(CssProperty.MarginBottom, val => MarginBottom = CssUnitConverter.ConvertToEmu(val));
            Css?.TryApplyProperty(CssProperty.Transparency, val => Transparency = CssUnitConverter.ConvertToInt(val));

            HorizontalRelativePosition = RelativeFrom == "margin"
                ? Wp.HorizontalRelativePositionValues.Margin
                : Wp.HorizontalRelativePositionValues.Page;
            VerticalRelativePosition = RelativeFrom == "margin"
                ? Wp.VerticalRelativePositionValues.Margin
                : Wp.VerticalRelativePositionValues.Page;

            if (AnchorMove)
            {
                VerticalRelativePosition = Wp.VerticalRelativePositionValues.Paragraph;
            }

            BehindDocument = WrappingStyle == "behind";

            switch (TextDirection)
            {
                case "horizontal":
                    _vertical = A.TextVerticalValues.Horizontal;
                    break;
                case "90":
                    _vertical = A.TextVerticalValues.Vertical;
                    break;
                case "270":
                    _vertical = A.TextVerticalValues.Vertical270;
                    break;
            }

            TextboxId = BitConverter.ToUInt32(BitConverter.GetBytes(GetHashCode()), 0);

            Content = ParseChildren();
        }

        private Wps.WordprocessingShape WordProcessingShape(OpenXmlConverterContext context)
        {
            return new Wps.WordprocessingShape(
                new Wps.NonVisualDrawingShapeProperties
                {
                    TextBox = true,
                    ShapeLocks = new A.ShapeLocks { NoChangeArrowheads = true },
                },
                new Wps.ShapeProperties(
                    new A.PresetGeometry
                    {
                        Preset = A.ShapeTypeValues.Rectangle,
                        AdjustValueList = new A.AdjustValueList(),
                    },
                    BackgroundFilling == "nofill"
                        ? new A.NoFill()
                        : new A.SolidFill
                        {
                            RgbColorModelHex = new A.RgbColorModelHex(
                                new A.Alpha { Val = (100 - Transparency) * 1000 }) { Val = BackgroundColor },
                        },
                    new A.Outline(
                        NoLine
                            ? new A.NoFill()
                            : new A.SolidFill
                            {
                                RgbColorModelHex = new A.RgbColorModelHex { Val = "000000" },
                            },
                        new A.Miter { Limit = 800000 },
                        new A.HeadEnd(),
                        new A.TailEnd()))
                {
                    BlackWhiteMode = A.BlackWhiteModeValues.Auto,
                    Transform2D = new A.Transform2D
                    {
                        Offset = new A.Offset
                        {
                            X = 0L,
                            Y = 0L,
                        },
                        Extents = new A.Extents
                        {
                            Cx = WidthEmu,
                            Cy = HeightEmu,
                        },
                    },
                },
                new Wps.TextBoxInfo2(
                    new Word.TextBoxContent(this.ConvertMany(Content, context))),
                new Wps.TextBodyProperties(
                    AutoFit ? new A.ShapeAutoFit() : new A.NoAutoFit())
                {
                    Vertical = _vertical,
                    Wrap = A.TextWrappingValues.Square,
                    LeftInset = MarginLeft,
                    TopInset = MarginTop,
                    RightInset = MarginRight,
                    BottomInset = MarginBottom,
                    Anchor = A.TextAnchoringTypeValues.Top,
                    AnchorCenter = false,
                });
        }

        private void IsValidTextboxInput()
        {
            var xmlChildren = XmlElement.Elements().ToArray();
            foreach (var xmlChild in xmlChildren)
            {
                if (xmlChild.ToString().Contains("data-type=\"textbox\""))
                {
                    throw new InvalidBdlDocumentException(
                        "Textbox is not allowed inside of Textbox");
                }
            }
        }
    }
}
