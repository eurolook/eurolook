using System.Collections.Generic;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage.FormattedText
{
    public interface ITokenHandler
    {
        bool HandlesToken(string token);

        IEnumerable<OpenXmlElement> Handle(string token, FormattedText parentElement, OpenXmlConverterContext context);
    }
}
