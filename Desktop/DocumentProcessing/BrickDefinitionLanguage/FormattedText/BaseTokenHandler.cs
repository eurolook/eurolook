using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage.FormattedText
{
    public abstract class BaseTokenHandler
    {
        protected static Text CreateSpacePreservingText(string value)
        {
            return new Text
            {
                Space = SpaceProcessingModeValues.Preserve,
                Text = value,
            };
        }

        protected List<string> ParseToken(string token)
        {
            int colonIndex = token.IndexOf(':');
            return token.Substring(colonIndex + 1)
                        .TrimEnd('}')
                        .Split('|')
                        .ToList();
        }
    }
}
