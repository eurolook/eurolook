using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Xml.XPath;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage.FormattedText
{
    [BdlElement("span")]
    [BdlAttribute("data-type", "formattedText")]
    [BdlRequiredAttributes("data-prop")]
    public class FormattedText : StyleableBdlElement, IOpenXmlConverter
    {
        public readonly IEnumerable<ITokenHandler> _tokenHandlers;

        public FormattedText()
        {
            var assembly = Assembly.GetAssembly(typeof(BdlElement));
            _tokenHandlers = assembly.GetTypes()
                                     .Where(
                                         x => typeof(ITokenHandler).IsAssignableFrom(x) && !x.IsInterface
                                             && !x.IsAbstract)
                                     .Select(Activator.CreateInstance)
                                     .OfType<ITokenHandler>()
                                     .ToList();
        }

        public string BoundText { get; private set; }

        [SuppressMessage(
            "Sonar",
            "S3220:Method calls should not resolve ambiguously to overloads with \"params\"",
            Justification = "Reviewed")]
        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            var input = context.TextsXml.XPathSelectElement(BoundText);
            if (input == null)
            {
                yield break;
            }

            var tokenizedText = Tokenize(input.Value);

            foreach (string token in tokenizedText)
            {
                var tokenHandler = _tokenHandlers.FirstOrDefault(t => t.HandlesToken(token))
                                   ?? new PlaintextTokenHandler();

                var tokenHandlerResult = tokenHandler.Handle(token, this, context).ToList();
                foreach (var openXmlElement in tokenHandlerResult)
                {
                    yield return openXmlElement;
                }
            }
        }

        protected override void Parse()
        {
            base.Parse();
            BoundText = GetDataPropAttribute()?.Value;
        }

        private static IEnumerable<string> Tokenize(string formattedInput)
        {
            return Regex.Split(formattedInput, @"({.+?})").Where(s => !string.IsNullOrEmpty(s));
        }
    }
}
