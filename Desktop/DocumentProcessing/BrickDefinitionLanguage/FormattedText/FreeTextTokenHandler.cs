using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;
using JetBrains.Annotations;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage.FormattedText
{
    [UsedImplicitly]
    public class FreeTextTokenHandler : BaseTokenHandler, ITokenHandler
    {
        public bool HandlesToken(string token)
        {
            return token.StartsWith("{freetext:");
        }

        public IEnumerable<OpenXmlElement> Handle(
            string token,
            FormattedText parentElement,
            OpenXmlConverterContext context)
        {
            var parsedToken = ParseToken(token);
            yield return new FreeText
                         {
                             ParentElement = parentElement,
                             PlaceholderText = parsedToken[0],
                             Style = parentElement.Style,
                             Css = parentElement.Css,
                         }
                         .ToOpenXml(context)
                         .First();
        }
    }
}
