using System.Collections.Generic;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using Eurolook.DocumentProcessing.OpenXml;
using JetBrains.Annotations;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage.FormattedText
{
    [UsedImplicitly]
    public class ManualLineBreakTokenHandler : BaseTokenHandler, ITokenHandler
    {
        public bool HandlesToken(string token)
        {
            return token.StartsWith("{br}");
        }

        public IEnumerable<OpenXmlElement> Handle(
            string token,
            FormattedText parentElement,
            OpenXmlConverterContext context)
        {
            var runProperties = parentElement.GetRunProperties();
            yield return new Run(runProperties, new DocumentFormat.OpenXml.Wordprocessing.Break());
        }
    }
}
