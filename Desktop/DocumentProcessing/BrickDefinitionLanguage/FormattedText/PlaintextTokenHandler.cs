using System.Collections.Generic;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using Eurolook.DocumentProcessing.OpenXml;
using JetBrains.Annotations;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage.FormattedText
{
    [UsedImplicitly]
    public class PlaintextTokenHandler : BaseTokenHandler, ITokenHandler
    {
        public bool HandlesToken(string token)
        {
            return false;
        }

        public IEnumerable<OpenXmlElement> Handle(
            string token,
            FormattedText parentElement,
            OpenXmlConverterContext context)
        {
            var runProperties = parentElement.GetRunProperties();
            yield return new Run(runProperties, CreateSpacePreservingText(token));
        }
    }
}
