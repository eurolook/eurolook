using System.Collections.Generic;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing.OpenXml;
using JetBrains.Annotations;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage.FormattedText
{
    /// <summary>
    /// A formatted text token to apply a character style within a formatted text.
    /// </summary>
    /// <remarks>
    /// Formatting is applied using the following pattern
    /// <pre>{style:StyleName|text}</pre>
    /// where StyleName is the case-sensitive style ID used in Open XML (i.e. without spaces) and
    /// text is the text of the run.
    /// </remarks>
    [UsedImplicitly]
    public class CharacterStyleTokenHandler : BaseTokenHandler, ITokenHandler, ICanLog
    {
        public bool HandlesToken(string token)
        {
            return token.StartsWith("{style:");
        }

        public IEnumerable<OpenXmlElement> Handle(
            string token,
            FormattedText parentElement,
            OpenXmlConverterContext context)
        {
            var parsedToken = ParseToken(token);
            if (parsedToken.Count != 2)
            {
                this.LogError($"Invalid token format in token '{token}' used in a formattedText.");
                var runProperties = parentElement.GetRunProperties();
                yield return new Run(runProperties, CreateSpacePreservingText(token));
            }
            else
            {
                var runProperties = parentElement.GetRunProperties();
                runProperties.RunStyle = new RunStyle { Val = parsedToken[0] };
                yield return new Run(runProperties, CreateSpacePreservingText(parsedToken[1]));
            }
        }
    }
}
