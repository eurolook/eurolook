using System;
using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;
using JetBrains.Annotations;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage.FormattedText
{
    [UsedImplicitly]
    public class TermStoreChoiceTokenHandler : BaseTokenHandler, ITokenHandler
    {
        public bool HandlesToken(string token)
        {
            return token.StartsWith("{term-store-choice:");
        }

        public IEnumerable<OpenXmlElement> Handle(
            string token,
            FormattedText parentElement,
            OpenXmlConverterContext context)
        {
            var parsedToken = ParseToken(token);

            var selectedItemId = parsedToken.Count > 2 ? Guid.Parse(parsedToken[2]) : (Guid?)null;

            yield return new TermStoreChoice
                         {
                             ParentElement = parentElement,
                             PlaceholderText = parsedToken[0],
                             TermSetId = Guid.Parse(parsedToken[1]),
                             SelectedTermId = selectedItemId,
                         }
                         .ToOpenXml(context)
                         .First();
        }
    }
}
