using System;
using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;
using JetBrains.Annotations;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage.FormattedText
{
    [UsedImplicitly]
    public class DateStaticTokenHandler : BaseTokenHandler, ITokenHandler
    {
        public bool HandlesToken(string token)
        {
            return token.StartsWith("{date-static:");
        }

        public IEnumerable<OpenXmlElement> Handle(
            string token,
            FormattedText parentElement,
            OpenXmlConverterContext context)
        {
            var parsedToken = ParseToken(token);

            string format = parsedToken[1];
            if (format.Equals("short", StringComparison.OrdinalIgnoreCase))
            {
                format = "Short";
            }
            else if (format.Equals("long", StringComparison.OrdinalIgnoreCase))
            {
                format = "Long";
            }

            yield return new DateTokenStatic
                         {
                             ParentElement = parentElement,
                             PlaceholderText = parsedToken[0],
                             Format = format,
                             Style = parentElement.Style,
                             Css = parentElement.Css,
                         }
                         .ToOpenXml(context)
                         .First();
        }
    }
}
