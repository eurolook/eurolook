using System.Collections.Generic;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using Eurolook.DocumentProcessing.OpenXml;
using JetBrains.Annotations;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage.FormattedText
{
    [UsedImplicitly]
    public class UnderlineTokenHandler : BaseTokenHandler, ITokenHandler
    {
        public bool HandlesToken(string token)
        {
            return token.StartsWith("{underline:");
        }

        public IEnumerable<OpenXmlElement> Handle(
            string token,
            FormattedText parentElement,
            OpenXmlConverterContext context)
        {
            var parsedToken = ParseToken(token);
            var runProperties = parentElement.GetRunProperties();
            runProperties.Underline = new Underline { Val = UnderlineValues.Single };
            yield return new Run(runProperties, CreateSpacePreservingText(parsedToken[0]));
        }
    }
}
