using System.Collections.Generic;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;
using JetBrains.Annotations;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage.FormattedText
{
    [UsedImplicitly]
    public class FieldTokenHandler : BaseTokenHandler, ITokenHandler
    {
        public bool HandlesToken(string token)
        {
            return token.StartsWith("{field:");
        }

        public IEnumerable<OpenXmlElement> Handle(
            string token,
            FormattedText parentElement,
            OpenXmlConverterContext context)
        {
            var parsedToken = ParseToken(token);
            var field = new WordField
            {
                FieldCode = GetFieldCode(parsedToken),
                FieldText = GetFieldText(parsedToken),
                ParentElement = parentElement,
                Css = parentElement.Css,
                Style = parentElement.Style,
            };

            return field.ToOpenXml(context);
        }

        private static string GetFieldCode(List<string> parsedToken)
        {
            if (parsedToken == null || parsedToken.Count < 1)
            {
                return "";
            }

            return parsedToken[0];
        }

        private static string GetFieldText(List<string> parsedToken)
        {
            if (parsedToken == null || parsedToken.Count < 2)
            {
                return "";
            }

            return parsedToken[1];
        }
    }
}
