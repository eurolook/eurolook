using System.Collections.Generic;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using Eurolook.DocumentProcessing.OpenXml;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("option")]
    [BdlChildElements(typeof(Text))]
    public class ChoiceOption : BdlElement, IOpenXmlConverter
    {
        public string Value { get; set; }

        public bool Selected { get; set; }

        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            var item = new ListItem
            {
                DisplayText = Value,
                Value = Value,
            };
            return new[] { item };
        }

        protected override void Parse()
        {
            base.Parse();

            Value = XmlElement.Value;
            Selected = XmlElement.Attribute("selected") != null;
        }
    }
}
