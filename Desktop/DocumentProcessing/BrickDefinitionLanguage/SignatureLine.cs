using System;
using System.Collections.Generic;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.DocumentProcessing.OpenXml;
using Ovml = DocumentFormat.OpenXml.Vml.Office;
using V = DocumentFormat.OpenXml.Vml;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("span")]
    [BdlAttribute("data-type", "signature-line")]
    [BdlRequiredAttributes("width", "height", "data-preview-source")]
    [BdlChildElements(ChildElements.None)]
    public class SignatureLine : Image
    {
        public string PreviewSource { get; set; }

        public string SuggestedSigner { get; set; }

        public string SuggestedSignerTitle { get; set; }

        public string SuggestedSignerEmail { get; set; }

        public string SigningInstructions { get; set; }

        public bool AllowComments { get; set; }

        public bool ShowSignDate { get; set; }

        public override IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            var run = new Word.Run();
            if (PreviewSource != null)
            {
                var resource = context.ResourceResolver.ResolveLocalisedResource(PreviewSource);
                if (resource?.RawData != null && resource.RawData.Length > 0)
                {
                    string relId = context.CurrentPart.AddImage(resource.RawData, GetImagePartType(resource.MimeType));

                    string alternativeText =
                        context.ResourceResolver.ResolveTranslation(AlternativeTextAlias)?.Value
                        ?? AlternativeTextAlias
                        ?? "Microsoft Office Signature Line...";

                    var pict = CreateSignatureLine(relId, alternativeText);
                    run.AppendChild(pict);
                }
            }

            return new OpenXmlElement[] { run };
        }

        public Word.Picture CreateSignatureLine(string relIdPreviewImage, string alternativeText)
        {
            var picture = new Word.Picture();

            var shapeTypeId = Guid.NewGuid().ToString("N");
            var shapeId = Guid.NewGuid().ToString("N");

            var shapeType = new V.Shapetype
            {
                Id = shapeTypeId,
                CoordinateSize = "21600,21600",
                Filled = false,
                Stroked = false,
                OptionalNumber = 75,
                PreferRelative = true,
                EdgePath = "m@4@5l@4@11@9@11@9@5xe",
            };

            var formulas = new V.Formulas();
            formulas.AppendChild(new V.Formula { Equation = "if lineDrawn pixelLineWidth 0" });
            formulas.AppendChild(new V.Formula { Equation = "sum @0 1 0" });
            formulas.AppendChild(new V.Formula { Equation = "sum 0 0 @1" });
            formulas.AppendChild(new V.Formula { Equation = "prod @2 1 2" });
            formulas.AppendChild(new V.Formula { Equation = "prod @3 21600 pixelWidth" });
            formulas.AppendChild(new V.Formula { Equation = "prod @3 21600 pixelHeight" });
            formulas.AppendChild(new V.Formula { Equation = "sum @0 0 1" });
            formulas.AppendChild(new V.Formula { Equation = "prod @6 1 2" });
            formulas.AppendChild(new V.Formula { Equation = "prod @7 21600 pixelWidth" });
            formulas.AppendChild(new V.Formula { Equation = "sum @8 21600 0" });
            formulas.AppendChild(new V.Formula { Equation = "prod @7 21600 pixelHeight" });
            formulas.AppendChild(new V.Formula { Equation = "sum @10 21600 0" });

            shapeType.AppendChild(new V.Stroke { JoinStyle = V.StrokeJoinStyleValues.Miter });
            shapeType.AppendChild(formulas);
            shapeType.AppendChild(
                new V.Path
                {
                    AllowGradientShape = true,
                    ConnectionPointType = Ovml.ConnectValues.Rectangle,
                    AllowExtrusion = false,
                });
            shapeType.AppendChild(
                new Ovml.Lock
                {
                    Extension = V.ExtensionHandlingBehaviorValues.Edit,
                    AspectRatio = true,
                });

            var shape = new V.Shape
            {
                Id = shapeId, // "_x0000_i1025",
                Style =
                    $"width:{CssUnitConverter.ConvertToPointsString(Width)}pt;height:{CssUnitConverter.ConvertToPointsString(Height)}pt",
                Alternate = alternativeText,
                Type = $"#{shapeTypeId}", // "#_x0000_t75",
            };

            shape.AppendChild(
                new V.ImageData
                {
                    Title = "",
                    RelationshipId = relIdPreviewImage,
                });

            shape.AppendChild(
                new Ovml.Lock
                {
                    Extension = V.ExtensionHandlingBehaviorValues.Edit,
                    Grouping = true,
                    Ungrouping = true,
                    Rotation = true,
                    Cropping = true,
                    Verticies = true,
                    TextLock = true,
                });

            var signatureLine = new Ovml.SignatureLine
            {
                Extension = V.ExtensionHandlingBehaviorValues.Edit,
                IsSignatureLine = true,
                Id = Guid.NewGuid().ToString("B"),
                ProviderId = "{00000000-0000-0000-0000-000000000000}",
                SigningInstructionsSet = !string.IsNullOrEmpty(SigningInstructions),
                AllowComments = AllowComments,
                SuggestedSigner = SuggestedSigner,
                SuggestedSigner2 = SuggestedSignerTitle,
                SuggestedSignerEmail = SuggestedSignerEmail,
                ShowSignDate = ShowSignDate,
            };
            signatureLine.SetAttribute(
                new OpenXmlAttribute(
                    "o",
                    "signinginstructions",
                    "urn:schemas-microsoft-com:office:office",
                    SigningInstructions));
            shape.AppendChild(signatureLine);

            picture.AppendChild(shapeType);
            picture.AppendChild(shape);

            return picture;
        }

        protected override void Parse()
        {
            PreviewSource = (string)XmlElement.Attribute("data-preview-source");

            SuggestedSigner = (string)XmlElement.Attribute("data-suggested-signer");
            SuggestedSignerTitle = (string)XmlElement.Attribute("data-suggested-signer-title");
            SuggestedSignerEmail = (string)XmlElement.Attribute("data-suggested-signer-email");
            SigningInstructions = (string)XmlElement.Attribute("data-signing-instructions");

            AllowComments = false;
            if (XmlElement.TryParseAttribute("data-allow-comments", out bool allowComments))
            {
                AllowComments = allowComments;
            }

            ShowSignDate = false;
            if (XmlElement.TryParseAttribute("data-show-sign-date", out bool showSignDate))
            {
                ShowSignDate = showSignDate;
            }

            base.Parse();
        }
    }
}
