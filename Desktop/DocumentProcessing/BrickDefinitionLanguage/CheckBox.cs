using System.Collections.Generic;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using Eurolook.DocumentProcessing.OpenXml;
using W14 = DocumentFormat.OpenXml.Office2010.Word;
using W15 = DocumentFormat.OpenXml.Office2013.Word;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("input")]
    [BdlAttribute("type", "checkbox")]
    [BdlChildElements(ChildElements.None)]
    public class CheckBox : StyleableBdlElement, IOpenXmlConverter
    {
        public string Title { get; set; }

        public bool IsChecked { get; set; }

        public ContainerType ContainerType { get; set; }

        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            var contentControl = new SdtRun();
            var sdtProperties = new SdtProperties();
            contentControl.AppendChild(sdtProperties);

            if (!string.IsNullOrEmpty(Title))
            {
                sdtProperties.AppendChild(new SdtAlias { Val = Title });
            }

            var runProperties = GetRunProperties();
            runProperties.RunFonts ??= new RunFonts
            {
                Ascii = "Segoe UI Symbol",
                HighAnsi = "Segoe UI Symbol",
                ComplexScriptTheme = ThemeFontValues.MinorHighAnsi,
            };
            runProperties.FontSize ??= new FontSize { Val = "32" };
            runProperties.FontSizeComplexScript ??= new FontSizeComplexScript { Val = "32" };

            sdtProperties.AppendChild(runProperties);
            sdtProperties.AppendChild(
                new W15.Appearance
                {
                    Val = ContainerType == ContainerType.Hidden
                        ? W15.SdtAppearance.Hidden
                        : W15.SdtAppearance.BoundingBox,
                });

            if (ContainerType == ContainerType.Vanishing)
            {
                sdtProperties.AppendChild(new TemporarySdt());
            }

            var checkBox = new W14.SdtContentCheckBox(
                new W14.Checked { Val = IsChecked ? W14.OnOffValues.One : W14.OnOffValues.Zero },
                new W14.CheckedState
                {
                    Font = "Segoe UI Symbol",
                    Val = "2612",
                },
                new W14.UncheckedState
                {
                    Font = "Segoe UI Symbol",
                    Val = "2610",
                });

            contentControl.AppendChild(checkBox);

            // run that displays the selected option
            var run = new Run { RunProperties = GetRunProperties() };
            run.RunProperties.NoProof = new NoProof();
            run.RunProperties.RunFonts ??= (RunFonts)runProperties.RunFonts.CloneNode(true);
            run.RunProperties.FontSize ??= (FontSize)runProperties.FontSize.CloneNode(true);
            run.RunProperties.FontSizeComplexScript ??=
                (FontSizeComplexScript)runProperties.FontSizeComplexScript.CloneNode(true);

            run.AppendChild(new Text { Text = IsChecked ? "☒" : "☐" });

            contentControl.AppendChild(new SdtContentRun(run));

            return new[] { contentControl };
        }

        protected override void Parse()
        {
            base.Parse();
            Title = (string)XmlElement.Attribute("data-title");
            IsChecked = XmlElement.Attribute("checked") != null;

            ContainerType = ContainerType.Permanent;
            var dataContainerAttr = XmlElement.Attribute("data-container");
            if (dataContainerAttr != null)
            {
                if (dataContainerAttr.Value == "vanishing")
                {
                    ContainerType = ContainerType.Vanishing;
                }
                else if (dataContainerAttr.Value == "hidden")
                {
                    ContainerType = ContainerType.Hidden;
                }
                else if (dataContainerAttr.Value == "none")
                {
                    throw new InvalidBdlDocumentException(
                        "The data-container attribute cannot be 'none' for this element.");
                }
            }
        }
    }
}
