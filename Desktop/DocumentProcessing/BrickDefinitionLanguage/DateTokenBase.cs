using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml.XPath;
using DocumentFormat.OpenXml;
using Eurolook.Common.Extensions;
using Eurolook.DocumentProcessing.OpenXml;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    public abstract class DateTokenBase : StyleableBdlElement, IOpenXmlConverter
    {
        public string Format { get; set; }

        public abstract IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context);

        protected override void Parse()
        {
            base.Parse();

            var dataPropAttr = GetDataPropAttribute();
            if (dataPropAttr == null || dataPropAttr.Value.Equals("short", StringComparison.OrdinalIgnoreCase))
            {
                Format = "Short";
            }
            else if (dataPropAttr.Value.Equals("long", StringComparison.OrdinalIgnoreCase))
            {
                Format = "Long";
            }
            else
            {
                Format = (string)dataPropAttr;
            }
        }

        protected string GetLocalizedDateFormat(OpenXmlConverterContext context)
        {
            // NOTE: backwards compatibility supporting date format stored in Languages table
            string dateFormatString = Format.EqualsIgnoreCase("long") || Format.EqualsIgnoreCase("short")
                ? "/Texts/DateFormat" + Format
                : Format;

            return dateFormatString.StartsWith("/Texts/")
                ? context.TextsXml.XPathSelectElement(dateFormatString)?.Value
                : dateFormatString;
        }

        protected string GetLocalizedDate(OpenXmlConverterContext context, DateTime date)
        {
            string dateFormat = GetLocalizedDateFormat(context);
            if (dateFormat == null)
            {
                return date.ToShortDateString();
            }

            // NOTE: the slash has a special meaning in custom date formats
            // (see https://msdn.microsoft.com/en-us/library/8kb3ddd4%28v=vs.110%29.aspx)
            // and needs to be escaped
            string escapedDateFormatString = dateFormat.Replace("/", "\\/");

            string formattedDate = date.ToString(escapedDateFormatString, new CultureInfo(context.Language.Locale));
            return formattedDate;
        }
    }
}
