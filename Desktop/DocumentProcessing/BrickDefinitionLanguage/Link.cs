using System.Collections.Generic;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("a")]
    [BdlAttribute("data-type", "url")]
    [BdlChildElements(ChildElements.None)]
    public class Link : StyleableBdlElement, IOpenXmlConverter
    {
        public string Url { get; set; }

        public string DisplayText { get; set; }

        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            var hyperlink = new Word.Hyperlink();
            if (Url != null)
            {
                hyperlink.Anchor = Url;
            }

            return new OpenXmlElement[] { hyperlink };
        }

        protected override void Parse()
        {
            base.Parse();

            if (XmlElement.Attribute("data-type") != null)
            {
                Url = XmlElement.Value;
            }

            if (XmlElement.Attribute("data-prop") != null)
            {
                DisplayText = GetDataPropAttribute().Value;
            }
        }
    }
}
