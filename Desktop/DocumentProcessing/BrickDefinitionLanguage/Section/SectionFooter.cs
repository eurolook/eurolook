using System.Linq;
using DocumentFormat.OpenXml.Wordprocessing;
using Eurolook.DocumentProcessing.OpenXml;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage.Section
{
    [BdlElement("div")]
    [BdlAttribute("data-type", "section-footer")]
    public class SectionFooter : SectionHeaderFooterBase
    {
        protected override void AddPart(
            OpenXmlConverterContext context,
            HeaderFooterValues headerFooterValue)
        {
            var footerPart = context.Document.AddFooter(
                context.Options.DocumentMode,
                SectionProperties,
                headerFooterValue);

            ExecuteForCurrentPart(
                footerPart,
                context,
                () =>
                {
                    var content = this.ConvertMany(Content, context).ToList();
                    footerPart.Footer.Append(content);
                });
        }
    }
}
