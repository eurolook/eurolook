using System.Linq;
using DocumentFormat.OpenXml.Wordprocessing;
using Eurolook.DocumentProcessing.OpenXml;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage.Section
{
    [BdlElement("div")]
    [BdlAttribute("data-type", "section-header")]
    public class SectionHeader : SectionHeaderFooterBase
    {
        protected override void AddPart(
            OpenXmlConverterContext context,
            HeaderFooterValues headerFooterValues)
        {
            var headerPart = context.Document.AddHeader(
                context.Options.DocumentMode,
                SectionProperties,
                headerFooterValues);

            ExecuteForCurrentPart(
                headerPart,
                context,
                () =>
                {
                    var content = this.ConvertMany(Content, context).ToList();
                    headerPart.Header.Append(content);
                });
        }
    }
}
