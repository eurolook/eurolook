using System;
using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using Eurolook.Common.Log;
using Eurolook.DocumentProcessing.OpenXml;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage.Section
{
    public abstract class SectionHeaderFooterBase : BdlElement, IOpenXmlConverter, ICanLog
    {
        private const string PageTypesAttribute = "data-page-types";

        public SectionProperties SectionProperties { get; set; }

        public HashSet<HeaderFooterValues> PageTypes { get; private set; }

        protected List<BdlElement> Content { get; set; }

        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            foreach (var type in PageTypes)
            {
                AddPart(context, type);
            }

            return Enumerable.Empty<OpenXmlElement>();
        }

        protected abstract void AddPart(OpenXmlConverterContext context, HeaderFooterValues headerFooterValues);

        protected override void Parse()
        {
            base.Parse();
            PageTypes = ParsePageTypes().ToHashSet();
            if (!PageTypes.Any())
            {
                PageTypes.Add(HeaderFooterValues.Default);
            }

            Content = ParseChildren();
        }

        private IEnumerable<HeaderFooterValues> ParsePageTypes()
        {
            var pageTypesString = XmlElement.Attribute(PageTypesAttribute)?.Value;
            var pageTypesLabels = pageTypesString?.Split(';') ?? Enumerable.Empty<string>();
            foreach (string pageTypesLabel in pageTypesLabels)
            {
                if (Enum.TryParse(pageTypesLabel.Trim(), true, out HeaderFooterValues result))
                {
                    yield return result;
                }
                else
                {
                    this.LogWarn($"Attribute '{PageTypesAttribute}' contains invalid value.");
                }
            }
        }
    }
}
