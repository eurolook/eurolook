﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.DocumentProcessing.OpenXml;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage.Section
{
    [BdlElement("div")]
    [BdlAttribute("data-type", "section-break")]
    [BdlChildElements(typeof(SectionFooter), typeof(SectionHeader))]
    [SuppressMessage("Sonar", "S3220:Method calls should not resolve ambiguously to overloads with \"params\"", Justification = "Reviewed")]
    public class SectionBreak : StyleableBdlElement, IOpenXmlConverter
    {
        public List<BdlElement> Content { get; set; }

        public bool HasDifferentFirstPage { get; set; }

        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            var orientation = Css?.PageOrientation;
            var pageSizeA4 = new Word.PageSize
            {
                Width = orientation == Word.PageOrientationValues.Portrait ? 11907U : 16839U,
                Height = orientation == Word.PageOrientationValues.Portrait ? 16839U : 11907U,
                Code = (UInt16Value)9U,
                Orient = orientation,
            };

            var pageMargin = Css?.PageMargin;
            var sectionProperties = new Word.SectionProperties();

            var sectionType = Css?.SectionType;
            if (sectionType != null)
            {
                sectionProperties.AppendChild(sectionType);
            }

            sectionProperties.AppendChild(pageSizeA4);
            if (pageMargin != null)
            {
                sectionProperties.AppendChild(pageMargin);
            }

            var columns = Css?.Columns;
            if (columns != null)
            {
                sectionProperties.AppendChild(columns);
            }

            var verticalTextAlignmentOnPage = Css?.VerticalTextAlignmentOnPage;
            if (verticalTextAlignmentOnPage != null)
            {
                sectionProperties.AppendChild(verticalTextAlignmentOnPage);
            }

            if (HasDifferentFirstPage)
            {
                sectionProperties.AppendChild(new Word.TitlePage());
            }

            var settingsPart = context.Document.GetOrCreateMainDocumentSettingsPart();
            sectionProperties.AppendChild(MergeEndnotePropertiesWithSettings(settingsPart, Css?.EndnoteSettings));
            sectionProperties.AppendChild(MergeFootnotePropertiesWithSettings(settingsPart, Css?.FootnoteSettings));

            foreach (var sectionHeaderFooter in Content.OfType<SectionHeaderFooterBase>())
            {
                sectionHeaderFooter.SectionProperties = sectionProperties;
            }

            this.ConvertMany(Content, context);

            yield return new Word.Paragraph(new Word.ParagraphProperties(sectionProperties));
        }

        protected override void Parse()
        {
            base.Parse();

            Content = ParseChildren();

            HasDifferentFirstPage = false;
            if (XmlElement.TryParseAttribute("data-different-first-page", out bool hasDifferentFirstPage)
                || XmlElement.TryParseAttribute("data-title-page", out hasDifferentFirstPage))
            {
                HasDifferentFirstPage = hasDifferentFirstPage;
            }
        }

        private static Word.EndnoteProperties MergeEndnotePropertiesWithSettings(DocumentSettingsPart documentSettings, Word.EndnoteProperties properties)
        {
            var settings = documentSettings?.Settings.Descendants<Word.EndnoteDocumentWideProperties>().FirstOrDefault();

            properties ??= new Word.EndnoteProperties();
            properties.EndnotePosition ??= (Word.EndnotePosition)settings?.EndnotePosition?.CloneNode(true);
            properties.NumberingFormat ??= (Word.NumberingFormat)settings?.NumberingFormat?.CloneNode(true);
            properties.NumberingStart ??= (Word.NumberingStart)settings?.NumberingStart?.CloneNode(true);
            properties.NumberingRestart ??= (Word.NumberingRestart)settings?.NumberingRestart?.CloneNode(true);

            return properties;
        }

        private static Word.FootnoteProperties MergeFootnotePropertiesWithSettings(DocumentSettingsPart documentSettings, Word.FootnoteProperties properties)
        {
            var settings = documentSettings?.Settings.Descendants<Word.FootnoteDocumentWideProperties>().FirstOrDefault();

            properties ??= new Word.FootnoteProperties();
            properties.FootnotePosition ??= (Word.FootnotePosition)settings?.FootnotePosition?.CloneNode(true);
            properties.NumberingFormat ??= (Word.NumberingFormat)settings?.NumberingFormat?.CloneNode(true);
            properties.NumberingStart ??= (Word.NumberingStart)settings?.NumberingStart?.CloneNode(true);
            properties.NumberingRestart ??= (Word.NumberingRestart)settings?.NumberingRestart?.CloneNode(true);

            return properties;
        }
    }
}
