﻿namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    public static class CssProperty
    {
        public const string FontFamily = "font-family";
        public const string FontWeight = "font-weight";
        public const string FontSize = "font-size";
        public const string FontStyle = "font-style";
        public const string FontVariant = "font-variant";
        public const string FontColor = "font-color";

        public const string BackgroundColor = "background-color";

        public const string TextDecoration = "text-decoration";
        public const string TextAlign = "text-align";
        public const string TextTransform = "text-transform";

        public const string WritingMode = "writing-mode";

        public const string MarginLeft = "margin-left";
        public const string MarginTop = "margin-top";
        public const string MarginRight = "margin-right";
        public const string MarginBottom = "margin-bottom";
        public const string MarginHeader = "margin-header";
        public const string MarginFooter = "margin-footer";

        public const string PaddingTop = "padding-top";
        public const string PaddingBottom = "padding-bottom";

        public const string Border = "border";
        public const string BorderTop = "border-top";
        public const string BorderRight = "border-right";
        public const string BorderBottom = "border-bottom";
        public const string BorderLeft = "border-left";

        public const string TableLayout = "table-layout";
        public const string Position = "position";
        public const string VerticalPosition = "vertical-position";
        public const string HorizontalPosition = "horizontal-position";
        public const string VerticalAlign = "vertical-align";
        public const string HorizontalAlign = "horizontal-align";

        public const string PageBreakBefore = "page-break-before";
        public const string PageBreakAfter = "page-break-after";

        public const string Columns = "columns";
        public const string ColumnGap = "column-gap";

        public const string Width = "width";
        public const string Height = "height";

        public const string Left = "left";
        public const string Top = "top";

        public const string ZOrder = "z-index";

        public const string PageOrientation = "page-orientation";

        public const string Transparency = "background-transparency";

        public const string EndnoteFormat = "endnote-format";
        public const string EndnoteNumbering = "endnote-numbering";

        public const string FootnotePosition = "footnote-position";
        public const string FootnoteFormat = "footnote-format";
        public const string FootnoteNumbering = "footnote-numbering";
    }
}
