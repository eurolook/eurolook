﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("div")]
    [BdlAttribute("data-type", "page-break")]
    [BdlChildElements(ChildElements.None)]
    public class PageBreak : BdlElement, IOpenXmlConverter
    {
        [SuppressMessage("Sonar", "S3220:Method calls should not resolve ambiguously to overloads with \"params\"", Justification = "Reviewed")]
        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            return new OpenXmlElement[]
            {
                new Word.Paragraph(new Word.Run(new Word.Break { Type = Word.BreakValues.Page })),
            };
        }
    }
}
