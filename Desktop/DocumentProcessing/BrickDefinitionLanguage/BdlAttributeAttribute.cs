﻿using System;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    public class BdlAttributeAttribute : Attribute
    {
        public BdlAttributeAttribute(string name, string value)
        {
            Name = name;
            Value = value;
        }

        public string Name { get; }

        public string Value { get; }
    }
}
