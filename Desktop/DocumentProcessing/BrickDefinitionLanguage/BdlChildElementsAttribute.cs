﻿using System;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    public class BdlChildElementsAttribute : Attribute
    {
        public BdlChildElementsAttribute(ChildElements discriminator)
        {
            Discriminator = discriminator;
            Types = new Type[0];
        }

        public BdlChildElementsAttribute(params Type[] types)
        {
            Discriminator = ChildElements.Restricted;
            Types = types;
        }

        public ChildElements Discriminator { get; }

        public Type[] Types { get; }
    }
}
