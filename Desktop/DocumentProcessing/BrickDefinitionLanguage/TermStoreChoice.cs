﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("select")]
    [BdlAttribute("data-type", "termStoreBinding")]
    [BdlRequiredAttributes("data-prop")]
    [BdlChildElements(ChildElements.None)]
    public class TermStoreChoice : BdlElement, IOpenXmlConverter
    {
        public string PlaceholderText { get; set; }

        public string Title { get; set; }

        public Guid TermSetId { get; set; }

        public Guid? SelectedTermId { get; set; }

        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            int languageCode = context.Language.CultureInfo.TextInfo.LCID;
            int languageCodeEn = 1033;
            var termStore = Task.Run(async () => await context.TermStoreCache.GetTermStoreAsync()).GetAwaiter().GetResult();
            var choiceOptions = termStore.GetTermsFlat(TermSetId)
                                         .Select(
                                             t => termStore.GetDefaultTermLabel(t.SharePointId, languageCode)
                                                  ?? termStore.GetDefaultTermLabel(t.SharePointId, languageCodeEn))
                                         .Where(tl => tl != null)
                                         .Select(
                                             tl => new ChoiceOption
                                             {
                                                 Value = tl.Value,
                                             });

            var selectedTerm = termStore.GetDefaultTermLabel(SelectedTermId.GetValueOrDefault(), languageCode);

            return new Choice
                {
                    Content = choiceOptions.Cast<BdlElement>().ToList(),
                    ParentElement = ParentElement,
                    PlaceholderText = PlaceholderText,
                    SelectedText = selectedTerm?.Value,
                    Title = Title,
                }
                .ToOpenXml(context);
        }

        protected override void Parse()
        {
            base.Parse();

            PlaceholderText = XmlElement.Value;
            Title = (string)XmlElement.Attribute("data-title");
            TermSetId = Guid.Parse(GetDataPropAttribute().Value);

            string selectedItem = (string)XmlElement.Attribute("data-selected-item");
            if (Guid.TryParse(selectedItem, out var selectedTermId))
            {
                SelectedTermId = selectedTermId;
            }
        }
    }
}
