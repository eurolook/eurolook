﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.Extensions;
using Eurolook.DocumentProcessing.OpenXml;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("td")]
    [BdlChildElements(typeof(Paragraph), typeof(PlainTextValue), typeof(Table))]
    public class TableCell : StyleableBdlElement, IOpenXmlConverter, IContentElement
    {
        public List<BdlElement> Content { get; private set; }

        public string Width { get; private set; }

        public int ColSpan { get; private set; }

        public int RowSpan { get; private set; }

        public bool IsVerticalMerge { get; private set; }

        [SuppressMessage(
            "Sonar",
            "S3220:Method calls should not resolve ambiguously to overloads with \"params\"",
            Justification = "Reviewed")]
        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            var cell = new Word.TableCell();
            var props = new Word.TableCellProperties { TableCellWidth = new Word.TableCellWidth() };
            if (Width != null)
            {
                if (Width.Contains("%"))
                {
                    props.TableCellWidth.Type = Word.TableWidthUnitValues.Pct;
                    props.TableCellWidth.Width = this.ConvertPercentToPct(Width).ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    props.TableCellWidth.Type = Word.TableWidthUnitValues.Dxa;
                    props.TableCellWidth.Width = this.ConvertToTwips(Width).ToString(CultureInfo.InvariantCulture);
                }
            }
            else
            {
                props.TableCellWidth.Width = "0";
                props.TableCellWidth.Type = Word.TableWidthUnitValues.Auto;
            }

            if (ColSpan > 1)
            {
                props.GridSpan = new Word.GridSpan { Val = ColSpan };
            }

            if (RowSpan > 1)
            {
                props.VerticalMerge = new Word.VerticalMerge { Val = Word.MergedCellValues.Restart };
            }
            else if (IsVerticalMerge)
            {
                props.VerticalMerge = new Word.VerticalMerge();
            }

            if (Css?.TableCellMargin != null)
            {
                props.TableCellMargin = Css.TableCellMargin;
            }

            if (Css != null && Css.Border.Any())
            {
                var borders = new Word.TableCellBorders();
                borders.Append(Css.Border);
                props.TableCellBorders = borders;
            }

            if (Css?.TableCellVerticalAlignment != null)
            {
                props.TableCellVerticalAlignment = Css.TableCellVerticalAlignment;
            }

            if (Css?.Shading != null)
            {
                props.Shading = Css.Shading;
            }

            if (Css?.TextDirection != null)
            {
                props.TextDirection = Css.TextDirection;
            }

            cell.TableCellProperties = props;

            if (Content.Count == 1 && Content[0] is PlainTextValue)
            {
                string text = ((PlainTextValue)Content[0]).Value;
                cell.AppendChild(new Word.Paragraph(new Word.Run(new Word.Text(text))));
            }
            else if (Content.All(el => el is Table))
            {
                // make sure that the cell ends with a paragraph
                cell.Append(this.ConvertMany(Content, context));
                cell.AppendChild(new Word.Paragraph());
            }
            else if (Content.Any())
            {
                cell.Append(this.ConvertMany(Content, context));
            }
            else
            {
                cell.AppendChild(new Word.Paragraph());
            }

            return new OpenXmlElement[] { cell };
        }

        protected override void Parse()
        {
            base.Parse();

            if (XmlElement.Attribute("width") == null)
            {
                throw new InvalidBdlDocumentException("Element 'td' must have attribute 'width'.");
            }

            Width = (string)XmlElement.Attribute("width");

            if (XmlElement.TryParseAttribute("colspan", out int colspan))
            {
                ColSpan = colspan;
            }

            if (XmlElement.TryParseAttribute("rowspan", out int rowspan))
            {
                RowSpan = rowspan;
            }

            if (XmlElement.TryParseAttribute("data-vmerge", out bool verticalMerge))
            {
                IsVerticalMerge = verticalMerge;
            }

            Content = ParseChildren();
            if (Content.Count == 0 && !string.IsNullOrWhiteSpace(XmlElement.Value))
            {
                Content.Add(new PlainTextValue(XmlElement.Value, this));
            }
        }
    }
}
