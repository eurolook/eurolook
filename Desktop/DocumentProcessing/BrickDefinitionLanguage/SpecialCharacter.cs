using System;
using System.Collections.Generic;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("span")]
    [BdlAttribute("data-type", "character")]
    [BdlChildElements(ChildElements.None)]
    public class SpecialCharacter : StyleableBdlElement, IOpenXmlConverter
    {
        public string Character { get; set; }

        public string TabAlignment { get; set; }

        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            var run = new Word.Run { RunProperties = GetRunProperties() };

            if (Character == "tab")
            {
                if (Enum.TryParse(
                        TabAlignment,
                        true,
                        out Word.AbsolutePositionTabAlignmentValues tabAlignment))
                {
                    // Word Alignment Tab
                    var positionalTab = new Word.PositionalTab
                    {
                        Alignment = tabAlignment,
                        RelativeTo = Word.AbsolutePositionTabPositioningBaseValues.Margin,
                        Leader = Word.AbsolutePositionTabLeaderCharValues.None,
                    };

                    run.AppendChild(positionalTab);
                }
                else
                {
                    run.AppendChild(new Word.TabChar());
                }
            }

            yield return run;
        }

        protected override void Parse()
        {
            base.Parse();

            var dataPropAttr = GetDataPropAttribute();
            if (dataPropAttr != null)
            {
                Character = dataPropAttr.Value.ToLowerInvariant();
            }

            string tabAlignmentAttr = (string)XmlElement?.Attribute("data-tab-alignment");
            TabAlignment = tabAlignmentAttr?.Trim();
        }
    }
}
