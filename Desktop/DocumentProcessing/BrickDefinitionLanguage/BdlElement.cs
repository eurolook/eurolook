using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using DocumentFormat.OpenXml.Packaging;
using Eurolook.DocumentProcessing.OpenXml;
using JetBrains.Annotations;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    public abstract class BdlElement
    {
        public BdlElement ParentElement { get; set; }

        public BdlDocument Document
        {
            get
            {
                var root = ParentElement;
                while (root.ParentElement != null)
                {
                    root = root.ParentElement;
                }

                return root as BdlDocument;
            }
        }

        protected XElement XmlElement { get; private set; }

        [NotNull]
        public static T FindElement<T>(XElement element)
            where T : BdlElement
        {
            // get all types inherited from BdlElement
            var assembly = Assembly.GetAssembly(typeof(BdlElement));
            var subTypes = assembly.GetTypes().Where(typeof(BdlElement).IsAssignableFrom).ToArray();

            // get all types with matching element name
            var elementTypes = new List<Type>();
            foreach (var type in subTypes)
            {
                // check element name
                var nameConstraint = GetAttribute<BdlElementAttribute>(type);
                if (nameConstraint != null && nameConstraint.Name == element.Name)
                {
                    elementTypes.Add(type);
                }
            }

            // only one element found? return it
            if (elementTypes.Count == 1)
            {
                return CreateElement<T>(elementTypes[0]);
            }

            // otherwise check the attributes
            var candidates = new Dictionary<Type, BdlAttributeAttribute[]>();
            foreach (var type in elementTypes)
            {
                var attributeConstraints = GetAttributes<BdlAttributeAttribute>(type);
                candidates.Add(type, attributeConstraints);
            }

            // candidates with more attributes are more specific, compare them first
            foreach (var typePair in candidates.OrderByDescending(kvp => kvp.Value.Length))
            {
                bool attributeMatch = true;
                foreach (var attributeConstraint in typePair.Value)
                {
                    var attribute = element.Attribute(attributeConstraint.Name);
                    if (attribute == null || attribute.Value != attributeConstraint.Value)
                    {
                        attributeMatch = false;
                    }
                }

                if (attributeMatch)
                {
                    var elementType = typePair.Key;
                    var requiredAttributes = GetAttribute<BdlRequiredAttributesAttribute>(elementType);
                    if (requiredAttributes != null)
                    {
                        foreach (string requiredAttribute in requiredAttributes.Names)
                        {
                            if (element.Attribute(requiredAttribute) == null)
                            {
                                throw new InvalidBdlDocumentException(
                                    $"Element of type {elementType} must have attribute '{requiredAttribute}'.");
                            }
                        }
                    }

                    return CreateElement<T>(typePair.Key);
                }
            }

            // no element was found?
            return CreateElement<T>(typeof(UnknownBdlElement));
        }

        public void Parse(XElement element)
        {
            XmlElement = element;
            Parse();
        }

        protected static T GetAttribute<T>(Type type)
            where T : Attribute
        {
            return type.GetCustomAttributes(false).OfType<T>().FirstOrDefault();
        }

        protected static T[] GetAttributes<T>(Type type)
            where T : Attribute
        {
            return type.GetCustomAttributes(false).OfType<T>().ToArray();
        }

        protected virtual void Parse()
        {
            var type = GetType();
            var xmlChildren = XmlElement.Elements().ToArray();
            var childElementsAttr = GetAttribute<BdlChildElementsAttribute>(type);
            if (childElementsAttr != null && childElementsAttr.Discriminator == ChildElements.None && xmlChildren.Any())
            {
                throw new InvalidBdlDocumentException(
                    string.Format("Element of type {0} cannot have child elements.", type.Name));
            }
        }

        protected List<BdlElement> ParseChildren()
        {
            var currentElementType = GetType();
            var childElementsAttr = GetAttribute<BdlChildElementsAttribute>(currentElementType);
            var result = new List<BdlElement>();
            var xmlChildren = XmlElement.Elements().ToArray();
            foreach (var xmlChild in xmlChildren)
            {
                var childElement = FindElement<BdlElement>(xmlChild);

                var childType = childElement.GetType();
                if (IsInvalid(currentElementType, childType))
                {
                    throw new InvalidBdlDocumentException(
                        $"{childType.Name} is not allowed inside of {currentElementType.Name}. The following elements are allowed: {string.Join(", ", childElementsAttr.Types.Select(t => t.Name))}");
                }

                if (currentElementType == typeof(UnknownBdlElement))
                {
                    var parentElementType = ParentElement.GetType();
                    var parentTypeChildElementsAttr = GetAttribute<BdlChildElementsAttribute>(parentElementType);
                    if (IsInvalid(parentElementType, childType))
                    {
                        throw new InvalidBdlDocumentException(
                            $"{childType.Name} is not allowed inside of {parentElementType.Name}. The following elements are allowed: {string.Join(", ", parentTypeChildElementsAttr.Types.Select(t => t.Name))}");
                    }
                }

                childElement.ParentElement = this;
                childElement.Parse(xmlChild);
                result.Add(childElement);
            }

            if (xmlChildren.Length == 0 && XmlElement.Value.Length > 0)
            {
                result.Add(new PlainTextValue(XmlElement.Value, this));
            }

            return result;
        }

        [CanBeNull]
        protected XAttribute GetDataPropAttribute()
        {
            return XmlElement.Attribute("data-prop");
        }

        protected string GetVisibilityCondition()
        {
            return XmlElement?.Attribute("data-visible-if")?.Value;
        }

        [NotNull]
        protected List<string> GetAuthorRolePriorityList()
        {
            string attributeValue = XmlElement?.Attribute("data-author-roles")?.Value;
            return attributeValue?.Split('|').ToList() ?? new List<string>();
        }

        /// <summary>
        /// Temporarily change and restore the current part of the converter context while executing an action delegate.
        /// </summary>
        /// <param name="part">The <see cref="OpenXmlPart" /> to be used as the current part.</param>
        /// <param name="context">The <see cref="OpenXmlConverterContext" /> instance used for conversion.</param>
        /// <param name="action">An <see cref="Action" /> delegate to be executed for the current part.</param>
        protected void ExecuteForCurrentPart(OpenXmlPart part, OpenXmlConverterContext context, Action action)
        {
            var originalCurrentPart = context.CurrentPart;
            context.CurrentPart = part;
            try
            {
                action();
            }
            finally
            {
                context.CurrentPart = originalCurrentPart;
            }
        }

        /// <summary>
        /// Temporarily change and restore the current part of the converter context while executing a function delegate.
        /// </summary>
        /// <param name="part">The <see cref="OpenXmlPart" /> to be used as the current part.</param>
        /// <param name="context">The <see cref="OpenXmlConverterContext" /> instance used for conversion.</param>
        /// <param name="func">An <see cref="Func{TResult}" /> delegate to be executed for the current part.</param>
        /// <returns>The result of the <see cref="Func{TResult}" /> delegate.</returns>
        protected T ExecuteForCurrentPart<T>(OpenXmlPart part, OpenXmlConverterContext context, Func<T> func)
        {
            var originalCurrentPart = context.CurrentPart;
            context.CurrentPart = part;
            try
            {
                return func();
            }
            finally
            {
                context.CurrentPart = originalCurrentPart;
            }
        }

        [NotNull]
        private static T CreateElement<T>(Type type)
            where T : BdlElement
        {
            var defaultConstructor = type.GetConstructor(Type.EmptyTypes);
            if (defaultConstructor == null)
            {
                throw new InvalidBdlDocumentException($"Class '{type.Name}' has no default constructor unknown.");
            }

            return (T)defaultConstructor.Invoke(null);
        }

        private bool IsInvalid(Type parentType, Type childType)
        {
            var childElementsAttr = GetAttribute<BdlChildElementsAttribute>(parentType);
            return childElementsAttr != null
                   && childElementsAttr.Discriminator == ChildElements.Restricted
                   && !childElementsAttr.Types.Contains(childType)
                   && childType != typeof(UnknownBdlElement);
        }
    }
}
