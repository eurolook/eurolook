namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    public enum ElementType
    {
        Block,
        Inline,
    }

    public enum ContainerType
    {
        Permanent,
        Vanishing,
        Hidden,
        None,
    }

    public enum ChildElements
    {
        All,
        Restricted,
        None,
    }
}
