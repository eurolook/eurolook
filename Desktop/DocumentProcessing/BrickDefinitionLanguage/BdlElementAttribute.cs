﻿using System;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    public class BdlElementAttribute : Attribute
    {
        public BdlElementAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; }
    }
}
