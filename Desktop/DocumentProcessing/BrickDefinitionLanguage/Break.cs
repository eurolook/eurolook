using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("br")]
    [BdlChildElements(ChildElements.None)]
    public class Break : BdlElement, IOpenXmlConverter
    {
        [SuppressMessage("Sonar", "S3220:Method calls should not resolve ambiguously to overloads with \"params\"", Justification = "Reviewed")]
        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            context.CurrentAuthorRolePriorityList = GetAuthorRolePriorityList();
            if (!OpenXmlConverterExtensions.IsVisible(GetVisibilityCondition(), context))
            {
                return new OpenXmlElement[0];
            }

            var result = ParentElement is Paragraph || ParentElement is TableCell
                ? (OpenXmlElement)new Word.Run(new Word.Break())
                : new Word.Break();

            return new[] { result };
        }
    }
}
