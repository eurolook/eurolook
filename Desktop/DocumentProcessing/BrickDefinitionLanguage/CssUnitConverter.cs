﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    public static class CssUnitConverter
    {
        private static readonly Dictionary<string, Unit> SupportedUnits = new Dictionary<string, Unit>
        {
            { "cm", Unit.Cm },
            { "pt", Unit.Pt },
            { "px", Unit.Px },
            { "%", Unit.Percent },
        };

        public enum Unit
        {
            Cm,
            Px,
            Pt,
            Percent,
        }

        public static int ConvertToInt(string value)
        {
            return int.Parse(value, NumberStyles.Number, CultureInfo.InvariantCulture);
        }

        public static uint ConvertToUInt(string value)
        {
            return uint.Parse(value, NumberStyles.Number, CultureInfo.InvariantCulture);
        }

        public static string ConvertToTwipsString(string margin)
        {
            int intValue = ConvertToTwips(margin);
            return intValue.ToString(CultureInfo.InvariantCulture);
        }

        public static uint ConvertToUTwips(string length)
        {
            return (uint)ConvertToTwips(length);
        }

        public static int ConvertToTwips(string length)
        {
            if (!TryParse(length, Unit.Pt, out var unit, out double value))
            {
                return 0;
            }

            switch (unit)
            {
                case Unit.Cm:
                    return ConvertCmToTwips(value);

                case Unit.Px:
                    return ConvertPixelsToTwips(value);

                case Unit.Pt:
                    return ConvertPointsToTwips(value);

                case Unit.Percent:
                    throw new NotSupportedException($"Relative length '{length}' cannot be converted to EMU.");
            }

            throw new NotSupportedException($"Converting length '{length}' is not supported.");
        }

        public static uint ConvertToUEmu(string length)
        {
            return (uint)ConvertToEmu(length);
        }

        public static int ConvertToEmu(string length)
        {
            if (!TryParse(length, Unit.Pt, out var unit, out double value))
            {
                return 0;
            }

            switch (unit)
            {
                case Unit.Cm:
                    return ConvertCmToEmu(value);

                case Unit.Percent:
                    throw new NotSupportedException($"Relative length '{length}' cannot be converted to twips.");

                default:
                    throw new NotSupportedException($"Unit {unit} in length '{length}' is not supported.");
            }
        }

        public static double ConvertToHalfPoints(string length)
        {
            if (TryParse(length, Unit.Pt, out var unit, out double doubleValue))
            {
                switch (unit)
                {
                    case Unit.Cm:
                        return doubleValue * 72 * 2 / 2.54;
                    case Unit.Px:
                        return doubleValue * 0.75 * 2;
                    case Unit.Pt:
                        return doubleValue * 2;
                    case Unit.Percent:
                        throw new NotSupportedException(
                            $"Relative length '{length}' cannot be converted to half point.");
                }
            }

            throw new NotSupportedException($"Converting length '{length}' is not supported.");
        }

        public static string ConvertToPointsString(string length)
        {
            return ConvertToPoints(length).ToString(CultureInfo.InvariantCulture);
        }

        public static double ConvertToPoints(string length)
        {
            if (TryParse(length, Unit.Pt, out var unit, out double doubleValue))
            {
                switch (unit)
                {
                    case Unit.Cm:
                        return doubleValue * 72 / 2.54;
                    case Unit.Px:
                        return doubleValue * 0.75;
                    case Unit.Pt:
                        return doubleValue;
                    case Unit.Percent:
                        throw new NotSupportedException(
                            $"Relative length '{length}' cannot be converted to half point.");
                }
            }

            throw new NotSupportedException($"Converting length '{length}' is not supported.");
        }

        private static int ConvertPixelsToTwips(double value)
        {
            return (int)Math.Round(value * 0.75 * 20, MidpointRounding.AwayFromZero);
        }

        private static int ConvertPointsToTwips(double value)
        {
            return (int)Math.Round(20 * value, MidpointRounding.AwayFromZero);
        }

        private static int ConvertCmToTwips(double value)
        {
            return (int)Math.Round(value * 1440 / 2.54, MidpointRounding.AwayFromZero);
        }

        private static int ConvertCmToEmu(double value)
        {
            return (int)(360000 * value);
        }

        private static bool TryParse(string input, Unit defaultUnit, out Unit unit, out double value)
        {
            SplitIntoRawValueAndUnit(input, defaultUnit, out unit, out string rawValue);
            if (rawValue.Contains(','))
            {
                rawValue = rawValue.Replace(',', '.');
            }

            return double.TryParse(rawValue, NumberStyles.Number, CultureInfo.InvariantCulture, out value);
        }

        private static void SplitIntoRawValueAndUnit(string input, Unit defaultUnit, out Unit unit, out string rawValue)
        {
            unit = defaultUnit;

            int indexOfUnit = -1;
            foreach (string supportedUnit in SupportedUnits.Keys)
            {
                indexOfUnit = input.IndexOf(supportedUnit, StringComparison.InvariantCultureIgnoreCase);
                if (indexOfUnit >= 0)
                {
                    unit = SupportedUnits[supportedUnit];
                    break;
                }
            }

            rawValue = indexOfUnit >= 0 ? input.Substring(0, indexOfUnit) : input;
            rawValue = rawValue.Trim();
        }
    }
}
