using System;
using System.Collections.Generic;
using DocumentFormat.OpenXml;
using Eurolook.Data.Xml;
using Eurolook.DocumentProcessing.OpenXml;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("div")]
    [BdlAttribute("data-type", "document-property")]
    [BdlRequiredAttributes("data-prop")]
    [BdlChildElements(ChildElements.None)]
    public class DocumentProperty : BdlElement, IOpenXmlConverter
    {
        public string PropertyBinding { get; set; }

        public string PropertyName { get; set; }

        public string PropertyValue { get; set; }

        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            if (PropertyBinding.StartsWith("/customprops/", StringComparison.InvariantCultureIgnoreCase) &&
                !context.CustomDocumentProperties.ContainsKey(PropertyName))
            {
                context.CustomDocumentProperties.Add(PropertyName, PropertyValue);
            }
            else if (PropertyBinding.StartsWith("/" + EurolookPropertiesCustomXml.RootName, StringComparison.InvariantCultureIgnoreCase) &&
                     !context.CustomEurolookProperties.ContainsKey(PropertyBinding))
            {
                context.CustomEurolookProperties.Add(PropertyBinding, PropertyValue);
            }

            return new OpenXmlElement[0];
        }

        protected override void Parse()
        {
            base.Parse();
            PropertyBinding = (string)XmlElement.Attribute("data-prop");
            PropertyValue = XmlElement.Value;
            if (PropertyBinding != null)
            {
                PropertyName = PropertyBinding.Substring(PropertyBinding.LastIndexOf('/') + 1);
            }
        }
    }
}
