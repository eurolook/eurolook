using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("span")]
    [BdlAttribute("data-type", "field-complex")]
    [BdlRequiredAttributes("data-prop")]
    [BdlChildElements(ChildElements.None)]
    [SuppressMessage(
        "Sonar",
        "S3220:Method calls should not resolve ambiguously to overloads with \"params\"",
        Justification = "Reviewed")]
    public class WordFieldComplex : StyleableBdlElement, IOpenXmlConverter
    {
        public enum ComplexFieldPart
        {
            Begin,
            Separate,
            End,
            InstrText,
            Text,
        }

        public ComplexFieldPart FieldPart { get; set; }

        public string InstrText { get; set; }

        public string Text { get; set; }

        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            switch (FieldPart)
            {
                case ComplexFieldPart.Begin:
                    yield return new Word.Run(
                        new Word.FieldChar { FieldCharType = Word.FieldCharValues.Begin });

                    break;
                case ComplexFieldPart.Separate:
                    yield return new Word.Run(
                        new Word.FieldChar
                        {
                            FieldCharType = Word.FieldCharValues.Separate,
                        });

                    break;
                case ComplexFieldPart.End:
                    yield return new Word.Run(
                        new Word.FieldChar { FieldCharType = Word.FieldCharValues.End });

                    break;
                case ComplexFieldPart.InstrText:
                    yield return new Word.Run(
                        GetRunProperties(),
                        new Word.FieldCode
                        {
                            Space = SpaceProcessingModeValues.Preserve,
                            Text = InstrText,
                        });

                    break;
                case ComplexFieldPart.Text:
                    yield return new Word.Run(
                        GetRunProperties(),
                        new Word.Text
                        {
                            Space = SpaceProcessingModeValues.Preserve,
                            Text = Text,
                        });

                    break;
                default:
                    throw new InvalidBdlDocumentException(
                        $"Complex field data-prop attribute {FieldPart} is not supported.");
            }
        }

        protected override void Parse()
        {
            base.Parse();

            string dataPropAttrValue = GetDataPropAttribute()?.Value?.Replace("-", "");
            if (Enum.TryParse(dataPropAttrValue, true, out ComplexFieldPart complexFieldPart))
            {
                FieldPart = complexFieldPart;
            }
            else
            {
                throw new InvalidBdlDocumentException(
                    "Complex field data-prop attribute must have one of the following values: "
                    + $"{string.Join(", ", Enum.GetNames(typeof(ComplexFieldPart)))}");
            }

            InstrText = XmlElement.Attribute("data-instr-text")?.Value ?? "";

            Text = XmlElement.Value;
        }
    }
}
