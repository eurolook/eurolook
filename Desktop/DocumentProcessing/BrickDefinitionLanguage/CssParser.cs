﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using JetBrains.Annotations;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    public class CssParser
    {
        private TableIndentation _tableIndent;
        private TableCellMargin _tableCellMargin;

        /// <summary>
        /// Initializes a new instance of the <see cref="CssParser" /> class.
        /// </summary>
        /// <param name="css">The css string to be parsed.</param>
        public CssParser(string css)
        {
            CssMap = new Dictionary<string, string>();
            if (!string.IsNullOrWhiteSpace(css))
            {
                foreach (string cssInstr in css.Split(';'))
                {
                    var cssInstrParts = cssInstr.Split(':');
                    if (cssInstrParts.Length == 2)
                    {
                        string property = cssInstrParts[0].Trim().ToLowerInvariant();
                        string value = cssInstrParts[1].Trim();

                        ExpandShortHandProperties(property, value);
                    }
                }
            }
        }

        public enum DifferentHeadersAndFootersValues
        {
            Default,
            OddEven,
            First,
            All,
        }

        public IEnumerable<OpenXmlElement> ParagraphSpacing
        {
            get
            {
                var spacing = new SpacingBetweenLines();

                bool hasSpacingDefined = TryApplyProperty(
                    CssProperty.MarginTop,
                    val => spacing.Before = CssUnitConverter.ConvertToTwipsString(val));
                hasSpacingDefined |= TryApplyProperty(
                    CssProperty.MarginBottom,
                    val => spacing.After = CssUnitConverter.ConvertToTwipsString(val));

                if (hasSpacingDefined)
                {
                    yield return spacing;
                }
            }
        }

        public IEnumerable<OpenXmlElement> ParagraphIndentation
        {
            get
            {
                var indentation = new Indentation();
                bool hasIndentation = TryApplyProperty(
                    CssProperty.MarginLeft,
                    val => indentation.Left = CssUnitConverter.ConvertToTwipsString(val));
                hasIndentation |= TryApplyProperty(
                    CssProperty.MarginRight,
                    val => indentation.Right = CssUnitConverter.ConvertToTwipsString(val));
                if (hasIndentation)
                {
                    yield return indentation;
                }
            }
        }

        public PageMargin PageMargin
        {
            get
            {
                PageMargin result = null;

                if (CssMap.TryGetValue(CssProperty.MarginLeft, out string marginLeftVal)
                    && CssMap.TryGetValue(CssProperty.MarginTop, out string marginTopVal)
                    && CssMap.TryGetValue(CssProperty.MarginRight, out string marginRightVal)
                    && CssMap.TryGetValue(CssProperty.MarginBottom, out string marginBottomVal))
                {
                    result = new PageMargin
                    {
                        Top = CssUnitConverter.ConvertToTwips(marginTopVal),
                        Left = CssUnitConverter.ConvertToUTwips(marginLeftVal),
                        Bottom = CssUnitConverter.ConvertToTwips(marginBottomVal),
                        Right = CssUnitConverter.ConvertToUTwips(marginRightVal),
                        Header = CssMap.TryGetValue(CssProperty.MarginHeader, out string marginHeaderVal)
                            ? (uint)CssUnitConverter.ConvertToTwips(marginHeaderVal)
                            : 708U,
                        Footer = CssMap.TryGetValue(CssProperty.MarginFooter, out string marginFooterVal)
                            ? (uint)CssUnitConverter.ConvertToTwips(marginFooterVal)
                            : 708U,
                        Gutter = 0U,
                    };

                    TryApplyProperty(CssProperty.PaddingTop, val => result.Header.Value = CssUnitConverter.ConvertToUTwips(val));
                    TryApplyProperty(CssProperty.PaddingBottom, val => result.Footer.Value = CssUnitConverter.ConvertToUTwips(val));
                }

                return result;
            }
        }

        public SectionType SectionType
        {
            get
            {
                if (CssMap.TryGetValue(CssProperty.PageBreakAfter, out string pageBreakAfter))
                {
                    switch (pageBreakAfter)
                    {
                        case "auto":
                        case "always":
                            return new SectionType { Val = SectionMarkValues.NextPage };
                        case "avoid":
                            return new SectionType { Val = SectionMarkValues.Continuous };
                        case "left":
                            return new SectionType { Val = SectionMarkValues.EvenPage };
                        case "right":
                            return new SectionType { Val = SectionMarkValues.OddPage };
                    }
                }

                return null;
            }
        }

        [CanBeNull]
        public VerticalTextAlignmentOnPage VerticalTextAlignmentOnPage
        {
            get
            {
                if (CssMap.TryGetValue(CssProperty.VerticalAlign, out var vAlignment))
                {
                    var verticalTextAlignmentOnPage = new VerticalTextAlignmentOnPage();

                    switch (vAlignment)
                    {
                        case "top":
                            verticalTextAlignmentOnPage.Val = VerticalJustificationValues.Top;
                            break;

                        case "middle":
                            verticalTextAlignmentOnPage.Val = VerticalJustificationValues.Center;
                            break;

                        case "bottom":
                            verticalTextAlignmentOnPage.Val = VerticalJustificationValues.Bottom;
                            break;

                        case "both":
                            verticalTextAlignmentOnPage.Val = VerticalJustificationValues.Both;
                            break;
                    }

                    return verticalTextAlignmentOnPage;
                }

                return null;
            }
        }

        public Columns Columns
        {
            get
            {
                Columns result = null;

                if (CssMap.TryGetValue(CssProperty.Columns, out string columns))
                {
                    result = new Columns
                    {
                        ColumnCount = Convert.ToInt16(columns, CultureInfo.InvariantCulture),
                        EqualWidth = true,
                        Space = CssMap.TryGetValue(CssProperty.ColumnGap, out string columnGap)
                            ? ((uint)CssUnitConverter.ConvertToTwips(columnGap)).ToString(CultureInfo.InvariantCulture)
                            : "708",
                    };
                }

                return result;
            }
        }

        public PageOrientationValues PageOrientation
        {
            get
            {
                if (!CssMap.TryGetValue(CssProperty.PageOrientation, out string value))
                {
                    return PageOrientationValues.Portrait;
                }

                return Enum.TryParse(value, true, out PageOrientationValues result) ? result : PageOrientationValues.Portrait;
            }
        }

        public Shading Shading
        {
            get
            {
                if (!CssMap.TryGetValue(CssProperty.BackgroundColor, out string value))
                {
                    return null;
                }

                return new Shading
                {
                    Fill = ConvertColor(value),
                    Val = ShadingPatternValues.Clear,
                    Color = "auto",
                };
            }
        }

        public TextDirection TextDirection
        {
            get
            {
                if (!CssMap.TryGetValue(CssProperty.WritingMode, out string value))
                {
                    return null;
                }

                TextDirectionValues result = TextDirectionValues.LefToRightTopToBottom;
                switch (value)
                {
                    case "horizontal-tb":
                        result = TextDirectionValues.LefToRightTopToBottom;
                        break;
                    case "sideways-lr":
                        result = TextDirectionValues.BottomToTopLeftToRight;
                        break;
                    case "sideways-rl":
                        result = TextDirectionValues.TopToBottomRightToLeft;
                        break;
                }

                return new TextDirection { Val = result };
            }
        }

        public TableIndentation TableIndent
        {
            get
            {
                if (_tableIndent == null && CssMap.TryGetValue(CssProperty.MarginLeft, out string marginLeftVal))
                {
                    _tableIndent = new TableIndentation
                    {
                        Type = TableWidthUnitValues.Dxa,
                        Width = CssUnitConverter.ConvertToTwips(marginLeftVal),
                    };
                }

                return _tableIndent;
            }
        }

        public TableCellMargin TableCellMargin
        {
            get
            {
                bool isLeftMarginSet = CssMap.TryGetValue(CssProperty.MarginLeft, out string marginLeftVal);
                bool isTopMarginSet = CssMap.TryGetValue(CssProperty.MarginTop, out string marginTopVal);
                bool isRightMarginSet = CssMap.TryGetValue(CssProperty.MarginRight, out string marginRightVal);
                bool isBottomMarginSet = CssMap.TryGetValue(CssProperty.MarginBottom, out string marginBottomVal);

                if (_tableCellMargin == null && (isLeftMarginSet || isTopMarginSet || isRightMarginSet || isBottomMarginSet))
                {
                    _tableCellMargin = new TableCellMargin
                    {
                        TopMargin = new TopMargin
                        {
                            Type = TableWidthUnitValues.Dxa,
                            Width = CssUnitConverter.ConvertToTwips(marginTopVal ?? "0").ToString(),
                        },
                        LeftMargin = new LeftMargin
                        {
                            Type = TableWidthUnitValues.Dxa,
                            Width = CssUnitConverter.ConvertToTwips(marginLeftVal ?? "0").ToString(),
                        },
                        BottomMargin = new BottomMargin
                        {
                            Type = TableWidthUnitValues.Dxa,
                            Width = CssUnitConverter.ConvertToTwips(marginBottomVal ?? "0").ToString(),
                        },
                        RightMargin = new RightMargin
                        {
                            Type = TableWidthUnitValues.Dxa,
                            Width = CssUnitConverter.ConvertToTwips(marginRightVal ?? "0").ToString(),
                        },
                    };
                }

                return _tableCellMargin;
            }
        }

        [CanBeNull]
        public TableCellVerticalAlignment TableCellVerticalAlignment
        {
            get
            {
                if (CssMap.TryGetValue(CssProperty.VerticalAlign, out var vAlignment))
                {
                    var tableCellVerticalAlignment = new TableCellVerticalAlignment();

                    switch (vAlignment)
                    {
                        case "top":
                            tableCellVerticalAlignment.Val = TableVerticalAlignmentValues.Top;
                            break;

                        case "middle":
                            tableCellVerticalAlignment.Val = TableVerticalAlignmentValues.Center;
                            break;

                        case "bottom":
                            tableCellVerticalAlignment.Val = TableVerticalAlignmentValues.Bottom;
                            break;
                    }

                    return tableCellVerticalAlignment;
                }

                return null;
            }
        }

        public TableLayout TableLayout
        {
            get
            {
                if (CssMap.TryGetValue(CssProperty.TableLayout, out string tableLayoutVal))
                {
                    var tableLayoutType = tableLayoutVal == "fixed" ? TableLayoutValues.Fixed : TableLayoutValues.Autofit;
                    return new TableLayout
                    {
                        Type = tableLayoutType,
                    };
                }

                return null;
            }
        }

        public TablePositionProperties TablePositionProperties
        {
            get
            {
                string position = GetCssValue(CssProperty.Position, "static");
                string horizontalPosition = GetCssValue(CssProperty.HorizontalPosition);
                string verticalPosition = GetCssValue(CssProperty.VerticalPosition);
                if (position == "static" && horizontalPosition == null && verticalPosition == null)
                {
                    return null;
                }

                var tablePosition = new TablePositionProperties();

                switch (horizontalPosition ?? position)
                {
                    case "absolute":
                        tablePosition.HorizontalAnchor = HorizontalAnchorValues.Margin;
                        break;

                    case "fixed":
                        tablePosition.HorizontalAnchor = HorizontalAnchorValues.Page;
                        break;

                    case "relative":
                        tablePosition.HorizontalAnchor = HorizontalAnchorValues.Text;
                        break;
                }

                switch (verticalPosition ?? position)
                {
                    case "absolute":
                        tablePosition.VerticalAnchor = VerticalAnchorValues.Margin;
                        break;

                    case "fixed":
                        tablePosition.VerticalAnchor = VerticalAnchorValues.Page;
                        break;

                    case "relative":
                        tablePosition.VerticalAnchor = VerticalAnchorValues.Text;
                        break;
                }

                TryApplyProperty(CssProperty.Left, val => tablePosition.TablePositionX = CssUnitConverter.ConvertToTwips(val));
                TryApplyProperty(CssProperty.Top, val => tablePosition.TablePositionY = CssUnitConverter.ConvertToTwips(val));

                TryApplyProperty(CssProperty.MarginLeft, val => tablePosition.LeftFromText = (short)CssUnitConverter.ConvertToTwips(val));
                TryApplyProperty(CssProperty.MarginTop, val => tablePosition.TopFromText = (short)CssUnitConverter.ConvertToTwips(val));
                TryApplyProperty(CssProperty.MarginRight, val => tablePosition.RightFromText = (short)CssUnitConverter.ConvertToTwips(val));
                TryApplyProperty(
                    CssProperty.MarginBottom,
                    val => tablePosition.BottomFromText = (short)CssUnitConverter.ConvertToTwips(val));

                TryApplyProperty(
                    CssProperty.TextAlign,
                    val =>
                    {
                        switch (val)
                        {
                            case "left":
                                tablePosition.TablePositionXAlignment = HorizontalAlignmentValues.Left;
                                break;

                            case "center":
                                tablePosition.TablePositionXAlignment = HorizontalAlignmentValues.Center;
                                break;

                            case "right":
                                tablePosition.TablePositionXAlignment = HorizontalAlignmentValues.Right;
                                break;
                        }
                    });

                TryApplyProperty(
                    CssProperty.VerticalAlign,
                    val =>
                    {
                        switch (val)
                        {
                            case "top":
                                tablePosition.TablePositionYAlignment = VerticalAlignmentValues.Top;
                                break;

                            case "middle":
                                tablePosition.TablePositionYAlignment = VerticalAlignmentValues.Center;
                                break;

                            case "bottom":
                                tablePosition.TablePositionYAlignment = VerticalAlignmentValues.Bottom;
                                break;
                        }
                    });

                return tablePosition;
            }
        }

        public IEnumerable<OpenXmlElement> ParagraphJustification
        {
            get
            {
                if (CssMap.TryGetValue(CssProperty.TextAlign, out string textAlignVal))
                {
                    if (textAlignVal == "left")
                    {
                        yield return new Justification { Val = JustificationValues.Left };
                    }

                    if (textAlignVal == "center")
                    {
                        yield return new Justification { Val = JustificationValues.Center };
                    }

                    if (textAlignVal == "right")
                    {
                        yield return new Justification { Val = JustificationValues.Right };
                    }

                    if (textAlignVal == "justify")
                    {
                        yield return new Justification { Val = JustificationValues.Distribute };
                    }
                }
            }
        }

        public IEnumerable<OpenXmlElement> PageBreakBefore
        {
            get
            {
                if (CssMap.TryGetValue(CssProperty.PageBreakBefore, out string pageBreakBefore) && pageBreakBefore == "always")
                {
                    yield return new PageBreakBefore();
                }
            }
        }

        public IEnumerable<OpenXmlElement> KeepWithNext
        {
            get
            {
                if (CssMap.TryGetValue(CssProperty.PageBreakBefore, out string pageBreakBefore) && pageBreakBefore == "avoid")
                {
                    yield return new KeepNext();
                }
            }
        }

        public IEnumerable<OpenXmlElement> RunFonts
        {
            get
            {
                return TryCreateElement(
                    CssProperty.FontFamily,
                    ".+",
                    val => new RunFonts
                    {
                        Ascii = val,
                        HighAnsi = val,
                        ComplexScript = val,
                    });
            }
        }

        public IEnumerable<OpenXmlElement> RunFontWeight
        {
            get
            {
                return TryCreateElement(
                    CssProperty.FontWeight,
                    "(bold|normal)",
                    val => new Bold { Val = OnOffValue.FromBoolean(val == "bold") });
            }
        }

        public IEnumerable<OpenXmlElement> RunFontSize
        {
            get
            {
                // Längenangabe, Prozentangabe, xx-small, x-small, small, smaller, medium, large, x-large, xx-large, larger, Inherit
                return TryCreateElement(
                    CssProperty.FontSize,
                    @"\d+.+",
                    val => new FontSize { Val = CssUnitConverter.ConvertToHalfPoints(val).ToString(CultureInfo.InvariantCulture) });
            }
        }

        public IEnumerable<OpenXmlElement> RunFontStyle
        {
            get { return TryCreateElement(CssProperty.FontStyle, "italic", val => new Italic()); }
        }

        public IEnumerable<OpenXmlElement> RunFontColor
        {
            get { return TryCreateElement(CssProperty.FontColor, @"[0-9a-fA-F]{6,8}", val => new Color { Val = ConvertColor(val) }); }
        }

        public IEnumerable<OpenXmlElement> RunCaps
        {
            get { return TryCreateElement(CssProperty.TextTransform, "uppercase", val => new Caps()); }
        }

        public IEnumerable<OpenXmlElement> RunSmallCaps
        {
            get { return TryCreateElement(CssProperty.FontVariant, "small-caps", val => new SmallCaps { Val = true }); }
        }

        public IEnumerable<OpenXmlElement> RunTextDecoration
        {
            get
            {
                // none, underline, overline, line-through, blink, Inherit
                return TryCreateElement(CssProperty.TextDecoration, "line-through", val => new Strike()).Union(
                    TryCreateElement(CssProperty.TextDecoration, "underline", val => new Underline { Val = UnderlineValues.Single }));
            }
        }

        public IEnumerable<OpenXmlElement> Border
        {
            get
            {
                var result = new List<OpenXmlElement>();

                if (CssMap.TryGetValue(CssProperty.Border, out string borderVal))
                {
                    result.Add(ConvertCssBorder<TopBorder>(borderVal));
                    result.Add(ConvertCssBorder<LeftBorder>(borderVal));
                    result.Add(ConvertCssBorder<BottomBorder>(borderVal));
                    result.Add(ConvertCssBorder<RightBorder>(borderVal));
                }

                if (CssMap.TryGetValue(CssProperty.BorderTop, out borderVal))
                {
                    result.Add(ConvertCssBorder<TopBorder>(borderVal));
                }

                if (CssMap.TryGetValue(CssProperty.BorderRight, out borderVal))
                {
                    result.Add(ConvertCssBorder<RightBorder>(borderVal));
                }

                if (CssMap.TryGetValue(CssProperty.BorderBottom, out borderVal))
                {
                    result.Add(ConvertCssBorder<BottomBorder>(borderVal));
                }

                if (CssMap.TryGetValue(CssProperty.BorderLeft, out borderVal))
                {
                    result.Add(ConvertCssBorder<LeftBorder>(borderVal));
                }

                return result;
            }
        }

        public EndnoteProperties EndnoteSettings
        {
            get
            {
                var result = new EndnoteProperties();

                if (CssMap.TryGetValue(CssProperty.EndnoteFormat, out string endnoteFormat) && TryParseNumberingFormat(endnoteFormat, out NumberingFormat format))
                {
                    result.NumberingFormat = format;
                }

                if (CssMap.TryGetValue(CssProperty.EndnoteNumbering, out string endnoteNumbering))
                {
                    result.NumberingRestart = endnoteNumbering switch
                    {
                        "continuous" => new NumberingRestart { Val = RestartNumberValues.Continuous },
                        "by-section" => new NumberingRestart { Val = RestartNumberValues.EachSection },
                        _ => result.NumberingRestart,
                    };
                }

                return result;
            }
        }

        public FootnoteProperties FootnoteSettings
        {
            get
            {
                var result = new FootnoteProperties();

                if (CssMap.TryGetValue(CssProperty.FootnoteFormat, out string footnoteFormat) && TryParseNumberingFormat(footnoteFormat, out NumberingFormat format))
                {
                    result.NumberingFormat = format;
                }

                if (CssMap.TryGetValue(CssProperty.FootnotePosition, out string footnotePosition))
                {
                    result.FootnotePosition = footnotePosition switch
                    {
                        "page-bottom" => new FootnotePosition { Val = FootnotePositionValues.PageBottom },
                        "below-text" => new FootnotePosition { Val = FootnotePositionValues.BeneathText },
                        _ => result.FootnotePosition,
                    };
                }

                if (CssMap.TryGetValue(CssProperty.FootnoteNumbering, out string footnoteNumbering))
                {
                    result.NumberingRestart = footnoteNumbering switch
                    {
                        "continuous" => new NumberingRestart { Val = RestartNumberValues.Continuous },
                        "by-page" => new NumberingRestart { Val = RestartNumberValues.EachPage },
                        "by-section" => new NumberingRestart { Val = RestartNumberValues.EachSection },
                        _ => result.NumberingRestart,
                    };
                }

                return result;
            }
        }

        /// <summary>
        /// Gets a dictionary containing the css attributes and the values.<br />
        /// Attributes and values are normalized (Trim and ToLowerInvariant).
        /// </summary>
        private Dictionary<string, string> CssMap { get; }

        public bool TryApplyProperty(string propertyName, Action<string> action)
        {
            if (CssMap.TryGetValue(propertyName, out string value))
            {
                action(value);
                return true;
            }

            return false;
        }

        public IEnumerable<OpenXmlElement> TryCreateElement(
            string propertyName,
            string expectedValuePattern,
            Func<string, OpenXmlElement> action)
        {
            if (CssMap.TryGetValue(propertyName, out string propertyValue)
                && Regex.IsMatch(propertyValue, $@"\b{expectedValuePattern}\b", RegexOptions.IgnoreCase))
            {
                string value = CssMap[propertyName];
                yield return action(value);
            }
        }

        private static T ConvertCssBorder<T>(string cssBorderValue)
            where T : BorderType, new()
        {
            var result = new T();
            foreach (string val in cssBorderValue.Split(' '))
            {
                string normalizedValue = val.Trim().ToLowerInvariant();
                switch (normalizedValue)
                {
                    case "solid":
                        result.Val = BorderValues.Single;
                        break;
                    case "dotted":
                        result.Val = BorderValues.Dotted;
                        break;
                    case "dashed":
                        result.Val = BorderValues.Dashed;
                        break;
                    case "double":
                        result.Val = BorderValues.Double;
                        break;
                    case "none":
                        result.Val = BorderValues.None;
                        break;
                    default:
                        if (normalizedValue.Contains("px"))
                        {
                            string pxStr =
                                normalizedValue.Substring(0, normalizedValue.IndexOf("px", StringComparison.InvariantCulture)).Trim();
                            double.TryParse(pxStr, NumberStyles.Number, CultureInfo.InvariantCulture, out double pxVal);
                            result.Size = new UInt32Value((uint)(pxVal * 0.75 * 8)); // 1px is 0.75pt, result is in eighths of a point (eps)
                        }
                        else if (normalizedValue.Contains("pt"))
                        {
                            string ptStr =
                                normalizedValue.Substring(0, normalizedValue.IndexOf("pt", StringComparison.InvariantCulture)).Trim();
                            double.TryParse(ptStr, NumberStyles.Number, CultureInfo.InvariantCulture, out double ptVal);
                            result.Size = new UInt32Value((uint)(ptVal * 8)); // result is in eighths of a point (eps)
                        }
                        else
                        {
                            result.Color = ConvertColor(normalizedValue);
                        }

                        break;
                }

                result.Space = 0;
            }

            return result;
        }

        private static string ConvertColor(string colorValue)
        {
            string normalValue = colorValue.Trim().ToLowerInvariant();
            if (normalValue.StartsWith("#") && (normalValue.Length == 7 || normalValue.Length == 9))
            {
                // e.g. #ffffff or #00ffffff
                return normalValue.Substring(1);
            }

            if (normalValue.StartsWith("rgb"))
            {
                // TODO: convert rgb array
            }

            return "auto";
        }

        // supported values: decimal | lower-roman | upper-roman | lower-letter | upper-letter | lower-bulgarian | upper-bulgarian
        private static bool TryParseNumberingFormat(string format, out NumberingFormat value)
        {
            switch (format)
            {
                case "decimal":
                    value = new NumberingFormat { Val = NumberFormatValues.Decimal };
                    return true;
                case "lower-roman":
                    value = new NumberingFormat { Val = NumberFormatValues.LowerRoman };
                    return true;
                case "upper-roman":
                    value = new NumberingFormat { Val = NumberFormatValues.UpperRoman };
                    return true;
                case "lower-letter":
                    value = new NumberingFormat { Val = NumberFormatValues.LowerLetter };
                    return true;
                case "upper-letter":
                    value = new NumberingFormat { Val = NumberFormatValues.UpperLetter };
                    return true;
                case "lower-bulgarian":
                    value = new NumberingFormat() { Val = NumberFormatValues.Custom, Format = "\u0430, \u0439, \u043a, ..." };
                    return true;
                case "upper-bulgarian":
                    value = new NumberingFormat() { Val = NumberFormatValues.Custom, Format = "\u0410, \u0419, \u041a, ..." };
                    return true;
                default:
                    value = null;
                    return false;
            }
        }

        private void ExpandShortHandProperties(string property, string value)
        {
            var valueParts = value.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            switch (property)
            {
                // expand shorthand property "margin"
                case "margin":
                    {
                        switch (valueParts.Length)
                        {
                            case 1:
                                CssMap[CssProperty.MarginTop] = valueParts[0];
                                CssMap[CssProperty.MarginRight] = valueParts[0];
                                CssMap[CssProperty.MarginBottom] = valueParts[0];
                                CssMap[CssProperty.MarginLeft] = valueParts[0];
                                break;
                            case 2:
                                CssMap[CssProperty.MarginTop] = valueParts[0];
                                CssMap[CssProperty.MarginRight] = valueParts[1];
                                CssMap[CssProperty.MarginBottom] = valueParts[0];
                                CssMap[CssProperty.MarginLeft] = valueParts[1];
                                break;
                            case 3:
                                CssMap[CssProperty.MarginTop] = valueParts[0];
                                CssMap[CssProperty.MarginRight] = valueParts[1];
                                CssMap[CssProperty.MarginBottom] = valueParts[3];
                                CssMap[CssProperty.MarginLeft] = valueParts[1];
                                break;
                            case 4:
                                CssMap[CssProperty.MarginTop] = valueParts[0];
                                CssMap[CssProperty.MarginRight] = valueParts[1];
                                CssMap[CssProperty.MarginBottom] = valueParts[2];
                                CssMap[CssProperty.MarginLeft] = valueParts[3];
                                break;
                        }
                    }

                    break;

                default:
                    CssMap[property] = value;
                    break;
            }
        }

        private string GetCssValue(string propertyName, string defaultValue = null)
        {
            return CssMap.TryGetValue(propertyName, out string result) ? result : defaultValue;
        }
    }
}
