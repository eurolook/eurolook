﻿using System;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    public class BdlRequiredAttributesAttribute : Attribute
    {
        public BdlRequiredAttributesAttribute(params string[] names)
        {
            Names = names;
        }

        public string[] Names { get; }
    }
}
