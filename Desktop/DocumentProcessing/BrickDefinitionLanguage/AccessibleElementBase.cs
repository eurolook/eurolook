﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Packaging;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    public class AccessibleElementBase : StyleableBdlElement
    {
        public bool IsDecorative { get; set; }

        protected override void Parse()
        {
            base.Parse();
            IsDecorative = XmlElement.Attribute("decorative") != null;
        }

        protected NonVisualDrawingPropertiesExtensionList GetDecorativeExtensionList(OpenXmlPart part)
        {
            return new NonVisualDrawingPropertiesExtensionList(
                new NonVisualDrawingPropertiesExtension(
                    part.CreateUnknownElement("<adec:decorative xmlns:adec=\"http://schemas.microsoft.com/office/drawing/2017/decorative\" val=\"1\" />"))
                {
                    Uri = "{C183D7F6-B498-43B3-948B-1728B52AA6E4}",
                });
        }
    }
}
