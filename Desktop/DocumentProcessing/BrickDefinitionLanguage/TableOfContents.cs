﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("div")]
    [BdlAttribute("data-type", "toc")]
    [BdlChildElements(typeof(Paragraph))]
    public class TableOfContents : BdlElement, IOpenXmlConverter, IContentElement
    {
        public List<BdlElement> Content { get; set; }

        [SuppressMessage("Sonar", "S3220:Method calls should not resolve ambiguously to overloads with \"params\"", Justification = "Reviewed")]
        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            if (context.Options.CreateTableOfContentsContainer)
            {
                return new OpenXmlElement[]
                {
                    new Word.SdtBlock
                    {
                        SdtProperties = new Word.SdtProperties(
                            new Word.SdtContentDocPartObject
                            {
                                DocPartGallery = new Word.DocPartGallery { Val = "Table of Contents" },
                                DocPartUnique = new Word.DocPartUnique(),
                            }),
                        SdtContentBlock = new Word.SdtContentBlock(this.ConvertMany(Content, context)),
                    },
                };
            }

            return this.ConvertMany(Content, context);
        }

        protected override void Parse()
        {
            base.Parse();

            Content = ParseChildren();
        }
    }
}
