﻿using System.Collections.Generic;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlChildElements(ChildElements.None)]
    public class PlainTextValue : BdlElement, IOpenXmlConverter
    {
        public PlainTextValue(string text, BdlElement parent)
        {
            Value = text;
            ParentElement = parent;
        }

        public string Value { get; set; }

        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            return new OpenXmlElement[]
            {
                new Word.Text
                {
                    Space = SpaceProcessingModeValues.Preserve,
                    Text = Value,
                },
            };
        }
    }
}
