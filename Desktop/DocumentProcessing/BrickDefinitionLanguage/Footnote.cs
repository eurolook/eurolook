﻿using System.Collections.Generic;
using DocumentFormat.OpenXml;
using Eurolook.DocumentProcessing.OpenXml;

namespace Eurolook.DocumentProcessing.BrickDefinitionLanguage
{
    [BdlElement("div")]
    [BdlAttribute("data-type", "footnote")]
    [BdlChildElements(
        typeof(Paragraph),
        typeof(Table),
        typeof(PageBreak),
        typeof(TableOfContents))]
    public class Footnote : StyleableBdlElement, IOpenXmlConverter, IContentElement
    {
        public List<BdlElement> Content { get; set; }

        public IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context)
        {
            return this.ConvertMany(Content, context);
        }

        protected override void Parse()
        {
            base.Parse();

            if (string.IsNullOrWhiteSpace(Id))
            {
                throw new InvalidBdlDocumentException("Missing id attribute on footnote div.");
            }

            Content = ParseChildren();
        }
    }
}
