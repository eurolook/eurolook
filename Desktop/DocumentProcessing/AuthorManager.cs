using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using DocumentFormat.OpenXml.Packaging;
using Eurolook.Data.Xml;
using Eurolook.DocumentProcessing.Extensions;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;

namespace Eurolook.DocumentProcessing
{
    public class AuthorManager : IAuthorManager
    {
        public bool HasCreatorAttribute(CustomXMLPart part)
        {
            var role = part.SelectSingleNode("/Author/@Role");
            return role?.NodeValue == "Creator";
        }

        public void ReplaceAuthorXmlPart(CustomXMLPart part, AuthorCustomXml authorXml)
        {
            // clean the child nodes
            foreach (CustomXMLNode child in part.DocumentElement.ChildNodes)
            {
                part.DocumentElement.RemoveChild(child);
            }

            // rebuild the child nodes
            foreach (var child in authorXml.Root.Elements())
            {
                part.DocumentElement.AppendChildSubtree(child.ToString());
            }

            // delete root node attributes
            foreach (var customXmlNode in part.DocumentElement.Attributes.OfType<CustomXMLNode>())
            {
                customXmlNode.Delete();
            }

            // rebuild root node attributes
            foreach (var attribute in authorXml.Root.Attributes())
            {
                part.DocumentElement.AppendChildNode(
                    attribute.Name.LocalName,
                    attribute.Name.NamespaceName,
                    MsoCustomXMLNodeType.msoCustomXMLNodeAttribute,
                    attribute.Value);
            }
        }

        public CustomXMLPart GetAuthorXmlPart(Document document, Guid authorRoleId)
        {
            return document.CustomXMLParts
                           .Cast<CustomXMLPart>()
                           .Where(IsAuthorXmlPart)
                           .FirstOrDefault(part => part.ToAuthorCustomXml().AuthorRoleId == authorRoleId);
        }

        public CustomXMLPart GetMainAuthorXmlPart(Document document)
        {
            var authorXmlParts = document.CustomXMLParts
                                         .Cast<CustomXMLPart>()
                                         .Where(IsAuthorXmlPart)
                                         .ToList();

            return authorXmlParts.Count == 1
                ? authorXmlParts.First()
                : authorXmlParts.FirstOrDefault(part => part.ToAuthorCustomXml().HasCreatorAttribute);
        }

        public IEnumerable<AuthorCustomXmlStoreInfo> GetAuthorCustomXmlStoreInfos(
            ContentControl contentControl,
            Document document)
        {
            var authorCustomXmlDictionary = new Dictionary<string, AuthorCustomXml>();
            foreach (var mapping in GetAuthorXmlMappings(contentControl))
            {
                if (!authorCustomXmlDictionary.ContainsKey(mapping.CustomXMLPart.Id))
                {
                    authorCustomXmlDictionary.Add(mapping.CustomXMLPart.Id, mapping.CustomXMLPart.ToAuthorCustomXml());
                }
            }

            var result = authorCustomXmlDictionary.Select(
                kvp => new AuthorCustomXmlStoreInfo
                {
                    StoreId = kvp.Key,
                    AuthorCustomXml = kvp.Value,
                });
            return SortByMainAuthor(result, document);
        }

        public IEnumerable<AuthorCustomXmlStoreInfo> GetAuthorCustomXmlStoreInfos(Document document)
        {
            return document?.CustomXMLParts
                           .OfType<CustomXMLPart>()
                           .Where(p => p.DocumentElement.BaseName == AuthorCustomXml.RootName)
                           .Select(
                               p => new AuthorCustomXmlStoreInfo
                               {
                                   StoreId = p.Id,
                                   AuthorCustomXml = p.ToAuthorCustomXml(),
                               })
                           .OrderBy(p => p.AuthorCustomXml.HasCreatorAttribute ? 0 : 1)
                           .ToList()
                   ?? new List<AuthorCustomXmlStoreInfo>();
        }

        public IEnumerable<AuthorCustomXmlStoreInfo> GetAuthorCustomXmlStoreInfos(WordprocessingDocument document)
        {
            var authorCustomXmlParts = document.GetCustomXmlParts(AuthorCustomXml.RootName).ToList();

            return authorCustomXmlParts?.Select(
                                           p => new AuthorCustomXmlStoreInfo
                                           {
                                               StoreId = p.CustomXmlPropertiesPart.DataStoreItem.ItemId,
                                               AuthorCustomXml = new AuthorCustomXml(p.GetXDocument()),
                                           })
                                       .OrderBy(p => p.AuthorCustomXml.HasCreatorAttribute ? 0 : 1);
        }

        public IEnumerable<AuthorCustomXml> GetAuthorCustomXmlDocs(Document document)
        {
            return GetAuthorCustomXmlStoreInfos(document).Select(i => i.AuthorCustomXml);
        }

        public IEnumerable<AuthorCustomXml> GetAuthorCustomXmlDocsFromPackage(WordprocessingDocument document)
        {
            return GetAuthorCustomXmlStoreInfos(document).Select(i => i.AuthorCustomXml);
        }

        public void DeleteAuthorXmlParts(Document document)
        {
            foreach (var part in document.CustomXMLParts.Cast<CustomXMLPart>().Where(IsAuthorXmlPart))
            {
                part.Delete();
            }
        }

        public void CleanAuthorXmlParts(Document document)
        {
            var allContentControls = document.GetAllContentControls().ToList();

            foreach (var part in document.CustomXMLParts.Cast<CustomXMLPart>().Where(IsAuthorXmlPart))
            {
                if (IsMainAuthorXmlPart(part))
                {
                    continue;
                }

                if (part.SelectSingleNode($"/Author/@{AuthorCustomXml.AuthorRoleNameAttribute}") != null)
                {
                    continue;
                }

                bool isReferenced = false;
                foreach (var cc in allContentControls)
                {
                    try
                    {
                        if (cc.XMLMapping != null && cc.XMLMapping.CustomXMLPart.Id == part.Id)
                        {
                            isReferenced = true;
                            break;
                        }
                    }
                    catch (COMException)
                    {
                        // ignore any COM errors and continue with next content control
                    }
                }

                if (!isReferenced)
                {
                    part.Delete();
                }
            }
        }

        public IEnumerable<XMLMapping> GetAuthorXmlMappings(ContentControl contentControl)
        {
            if (HasAuthorXmlMappings(contentControl))
            {
                return new List<XMLMapping> { contentControl.XMLMapping };
            }

            return GetAuthorXmlMappings(contentControl.Range.ContentControls.OfType<ContentControl>());
        }

        public CustomXMLPart AddAuthorXmlPart(Document document, AuthorCustomXml authorXml)
        {
            return document.CustomXMLParts.Add(authorXml.ToString());
        }

        public bool HasAuthorRoles(IEnumerable<AuthorCustomXml> authorCustomXmlDocs)
        {
            return authorCustomXmlDocs.Any(xml => xml.AuthorRoleId.HasValue);
        }

        public AuthorInformation GenerateAuthorInformation(
            IEnumerable<CustomXMLPart> customXmlParts,
            IReadOnlyEurolookProperties eurolookProperties)
        {
            var authorCustomXmlStoreInfos = customXmlParts
                                            .Select(
                                                p => new AuthorCustomXmlStoreInfo
                                                {
                                                    AuthorCustomXml = p.ToAuthorCustomXml(),
                                                    StoreId = p.Id,
                                                })
                                            .OrderBy(p => p.AuthorCustomXml.HasCreatorAttribute ? 0 : 1);

            return new AuthorInformation(authorCustomXmlStoreInfos, eurolookProperties);
        }

        public IEnumerable<AuthorCustomXmlStoreInfo> SortByMainAuthor(
            IEnumerable<AuthorCustomXmlStoreInfo> authorCustomXmlStoreInfos,
            Document document)
        {
            var mainAuthorCustomXml = GetMainAuthorXmlPart(document).ToAuthorCustomXml();
            return authorCustomXmlStoreInfos
                   .OrderBy(i => i.AuthorCustomXml.HasCreatorAttribute ? 0 : 1)
                   .ThenBy(i => i.AuthorCustomXml.OriginalAuthorRoleId == mainAuthorCustomXml.AuthorRoleId ? 0 : 1);
        }

        private IEnumerable<XMLMapping> GetAuthorXmlMappings(IEnumerable<ContentControl> contentControls)
        {
            return contentControls.Where(HasAuthorXmlMappings).Select(cc => cc.XMLMapping).ToList();
        }

        private bool HasAuthorXmlMappings(ContentControl cc)
        {
            return cc.XMLMapping.XPath != null && cc.XMLMapping.XPath.StartsWith("/Author");
        }

        private bool IsAuthorXmlPart(CustomXMLPart part)
        {
            return part.DocumentElement != null && part.DocumentElement.BaseName != null
                                                && part.DocumentElement.BaseName == AuthorCustomXml.RootName;
        }

        private bool IsMainAuthorXmlPart(CustomXMLPart part)
        {
            string rolePath = "@" + AuthorCustomXml.RoleAttr;
            return IsAuthorXmlPart(part) && part.DocumentElement.SelectSingleNode(rolePath) != null &&
                   part.DocumentElement.SelectSingleNode(rolePath).NodeValue == "Creator";
        }
    }
}
