﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.DocumentProcessing
{
    public static class StyleConceptListRestart
    {
        public static Bookmark[] GetListRestarts(Range range)
        {
            if (range == null)
            {
                return new Bookmark[0];
            }

            var listRestarts = new List<Bookmark>();
            int bookmarkIndex = 0;
            foreach (Paragraph paragraph in range.Paragraphs)
            {
                var style = LanguageIndependentStyle.FromParagraph(paragraph);
                if (style != null)
                {
                    var styleNameInfo = StyleNameInfo.FromStyleName(style.NameNeutral);
                    if (styleNameInfo != null && (styleNameInfo.ListLevel == 1 && IsListParagraphRestarted(paragraph)))
                    {
                        bookmarkIndex++;
                        int listValue = paragraph.Range.ListFormat.ListValue;
                        var bm = paragraph.Range.Bookmarks.Add($"Restart_{bookmarkIndex}_{listValue}", paragraph.Range);
                        listRestarts.Add(bm);
                    }
                }
            }

            return listRestarts.ToArray();
        }

        public static void ApplyListRestarts(Bookmark[] listRestarts)
        {
            if (listRestarts == null)
            {
                return;
            }

            foreach (var bm in listRestarts)
            {
                try
                {
                    var p = bm.Range.Paragraphs.First;
                    if (p == null)
                    {
                        continue;
                    }

                    if (CanRestartList(p))
                    {
                        ReStartListWhenPreviousParagraphFeaturesDifferentListTemplate(p);
                    }
                }
                catch
                {
                    // do nothing
                }
                finally
                {
                    bm.Delete();
                }
            }
        }

        /// <summary>
        /// Restart the first paragraph of given range in case that given paragraph and its preceding paragraph do not belong to the same list template.
        /// </summary>
        /// <param name="range">The range to update.</param>
        public static void ReStartListWhenPreviousParagraphFeaturesDifferentListTemplate(Range range)
        {
            ReStartListWhenPreviousParagraphFeaturesDifferentListTemplate(range.Paragraphs.First);
        }

        /// <summary>
        /// Restart list of the given paragraph, if the previous paragraph does not belong to the same list template as the given paragraph.
        /// </summary>
        /// <param name="paragraph">The paragraph to restart.</param>
        public static void ReStartListWhenPreviousParagraphFeaturesDifferentListTemplate(Paragraph paragraph)
        {
            if (!PreviousParagraphHasSameListTemplate(paragraph))
            {
                RestartListParagraph(paragraph);
            }
        }

        public static bool IsListParagraphRestarted([NotNull] Paragraph paragraph)
        {
            try
            {
                // ReSharper disable once UseIndexedProperty
                Style style = paragraph.get_Style();
                var listTemplate = style.ListTemplate;
                if (listTemplate == null)
                {
                    return false;
                }

                var listParagraphs = paragraph.Range?.ListFormat?.List?.ListParagraphs;
                if (listParagraphs == null)
                {
                    return false;
                }

                var firstParagraph = listParagraphs[1];

                return firstParagraph != null && paragraph.Range.Start == firstParagraph.Range.Start;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private static bool CanRestartList(Paragraph paragraph)
        {
            var style = LanguageIndependentStyle.FromParagraph(paragraph);
            var styleNameInfo = StyleNameInfo.FromStyleName(style.NameNeutral);
            var listTemplate = style.Style.ListTemplate;
            if (listTemplate == null)
            {
                return false;
            }

            if (StyleConceptNew.IsHeadingStyle(style))
            {
                return false;
            }

            return styleNameInfo.ListLevel == 1;
        }

        private static void RestartListParagraph(Paragraph paragraph)
        {
            if (paragraph == null)
            {
                return;
            }

            Style style = paragraph.get_Style();
            var listTemplate = style.ListTemplate;
            var r = paragraph.Range;
            if (IsEndOfTableCell(paragraph))
            {
                try
                {
                    r.MoveEnd(WdUnits.wdCharacter, -1);
                }
                catch (Exception)
                {
                    return;
                }
            }

            r.ListFormat.ApplyListTemplateWithLevel(listTemplate, false, WdListApplyTo.wdListApplyToWholeList, WdDefaultListBehavior.wdWord10ListBehavior);
        }

        private static bool PreviousParagraphHasSameListTemplate(Paragraph paragraph)
        {
            if (paragraph == null)
            {
                return false;
            }

            Style style = paragraph.get_Style();
            var listTemplate = style?.ListTemplate;
            var linkedStyles = listTemplate?.ListLevels.OfType<ListLevel>().Select(ll => ll.LinkedStyle).ToList();
            var previousParagraph = paragraph.Previous();
            Style previousParagraphStyle = previousParagraph?.get_Style();
            return previousParagraphStyle != null && linkedStyles?.Contains(previousParagraphStyle.NameLocal) == true;
        }

        private static bool IsEndOfTableCell(Paragraph p)
        {
            try
            {
                if (!(bool)p.Range.Information[WdInformation.wdWithInTable])
                {
                    return false;
                }

                return p.Range.End == p.Range.Cells[p.Range.Cells.Count].Range.End;
            }
            catch
            {
                return false;
            }
        }
    }
}
