using Microsoft.Office.Interop.Word;

namespace Eurolook.DocumentProcessing.Extensions
{
    public static class TableExtensions
    {
        /// <summary>
        /// Insert a new paragraph before a table, even if the table is at the beginning of the document,
        /// contains merged cells or is surrounded by a content control.
        /// </summary>
        /// <param name="table">A Word table object. The new paragraph will be inserted before this table.</param>
        /// <returns>A range that is collapsed at the beginning of the newly inserted paragraph.</returns>
        public static Range InsertParagraphBefore(this Table table)
        {
            var range = table.Range.Duplicate;

            if (table.IsFloating())
            {
                // This inserts a new paragraph before any floating table.
                range.Collapse();
                range.Move(WdUnits.wdCharacter, -1);

                if (range.Start == 0)
                {
                    // If the table is at the beginning of the document we need to use the selection object
                    range.Select();
                    range.Application.Selection.SplitTable();
                }
                else
                {
                    range.InsertParagraph();
                    range.Collapse(WdCollapseDirection.wdCollapseEnd);
                }
            }
            else
            {
                // This inserts a new paragraph before any inline table. This works especially if the
                // table is at the beginning of the document and/or if the table contains merged cells.
                // However, if the table is surrounded by a content control, the paragraph is still
                // inside the content control. Therefore we are going to add yet another paragraph.
                table.Cell(1, 1).Range.InsertBreak(WdBreakType.wdColumnBreak);

                // Add yet another paragraph (this will be outside a content control, if the table should be surrounded by one
                range.InsertParagraphBeforeSafe();

                // Delete the additional paragraph that is right before the table.
                // Pay attention not to remove any content control, therefore the range is positioned
                // to contain just the paragraph mark
                var additionalParagraph = range.Paragraphs[2].Range;
                additionalParagraph.Start = additionalParagraph.End - 1;
                additionalParagraph.Delete();

                range.Collapse();
            }

            return range;
        }

        public static Range InsertParagraphAfter(this Table table)
        {
            var range = table.Range;
            range.Collapse(WdCollapseDirection.wdCollapseEnd);
            range.InsertParagraphAfterSafe();
            range.Collapse();
            return range;
        }

        public static bool IsFloating(this Table table)
        {
            return table.Rows.WrapAroundText == -1;
        }
    }
}
