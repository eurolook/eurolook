﻿using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Word;

namespace Eurolook.DocumentProcessing.Extensions
{
    public static class ParagraphExtensions
    {
        public static bool IsLastParagraphInStory(this Paragraph paragraph)
        {
            var storyRange = paragraph.Range.Document.StoryRanges[paragraph.Range.StoryType];
            var lastParagraphInStory = storyRange.Paragraphs.Last;

            return paragraph.Range.Start == lastParagraphInStory.Range.Start;
        }

        public static string GetStyleNameLocal(this Paragraph paragraph)
        {
            if (paragraph == null)
            {
                return null;
            }

            try
            {
                // ReSharper disable once UseIndexedProperty
                var style = paragraph.get_Style();
                return style == null ? "" : (string)style.NameLocal;
            }
            catch (COMException)
            {
                return "";
            }
        }

        public static void ApplyStyle(this Paragraph paragraph, string paragraphStyleName)
        {
            var style = LanguageIndependentStyle.FromNeutralStyleName(paragraphStyleName, paragraph.Range.Document);
            if (style != null)
            {
                // ReSharper disable once UseIndexedProperty
                paragraph.set_Style(style.Style);
            }
        }

        public static bool IsInDeletedRevision(this Paragraph paragraph)
        {
            if (paragraph?.Range?.Revisions == null || paragraph?.Range?.Revisions.Count == 0)
            {
                return false;
            }

            foreach (var r in paragraph.Range.Revisions.OfType<Revision>())
            {
                try
                {
                    if (r.Type == WdRevisionType.wdRevisionDelete && paragraph.Range.Start >= r.Range.Start
                                                                  && paragraph.Range.End - 1 <= r.Range.End)
                    {
                        return true;
                    }
                }
                catch (COMException)
                {
                    // The Range.Revisions collection sometimes contains invalid objects.
                    // In this case a COMException is thrown when accessing a property (like Type) of the object.
                    // The Revisions collection seems to be invalid and iterating causes a Word crash:
                    // Quit the method.
                    return false;
                }
            }

            return false;
        }

        public static bool HasRevisions(this Paragraph paragraph)
        {
            return paragraph?.Range?.Revisions.OfType<Revision>().Any() == true;
        }

        public static bool IsRestartingListNumbering(this Paragraph paragraph)
        {
            if (paragraph == null)
            {
                return
                    false; // just for convenience return false if paragraph is null instead of throwing an ArgumentNullException
            }

            var range = paragraph.Range;
            var listFormat = range.ListFormat;

            var firstListPara = listFormat?.List?.ListParagraphs[1];
            return (firstListPara != null) && range.Start == firstListPara.Range.Start;
        }

        /// <summary>
        /// Restarts list numbering if possible otherwise just return.
        /// </summary>
        /// <param name="paragraph">paragraph to restart</param>
        public static void RestartListNumbering(this Paragraph paragraph)
        {
            if (!paragraph.IsRestartingListNumbering())
            {
                paragraph.SetListContinue(false);
            }
        }

        /// <summary>
        /// restarts list numbering if possible otherwise just return.
        /// </summary>
        /// <param name="paragraph">paragraph to restart</param>
        public static void ContinueListNumbering(this Paragraph paragraph)
        {
            if (paragraph.IsRestartingListNumbering())
            {
                paragraph.SetListContinue(true);
            }
        }

        /// <summary>
        /// re-apply current list template setting the provided list restart value.
        /// </summary>
        /// <param name="paragraph">paragraph to reset.</param>
        /// <param name="continuePreviousList">true to restart the list, false to continue list numbering.</param>
        public static void SetListContinue(this Paragraph paragraph, bool continuePreviousList)
        {
            if (paragraph == null)
            {
                return;
            }

            var paragraphRange = paragraph.Range;
            var lf = paragraphRange.ListFormat;
            if (lf == null || lf.ListType == WdListType.wdListNoNumbering)
            {
                return;
            }

            var lt = lf.ListTemplate;
            lf.ApplyListTemplate(lt, continuePreviousList, WdListApplyTo.wdListApplyToThisPointForward);
        }

        /// <summary>
        /// Search for the first paragraph after the current one (this paragraph) having a style name belonging to the same
        /// Eurolook list type.
        /// In case that this paragraph restarts the list it is returned.
        /// </summary>
        /// <param name="paragraph">current paragraph, specifies start of the search range (paragraph itself not included).</param>
        /// <param name="listStyleName">paragraph style name to be searched.</param>
        /// <returns>the paragraph it one was found or null otherwise.</returns>
        public static Paragraph GetNextListRestart(this Paragraph paragraph, string listStyleName)
        {
            if (paragraph == null)
            {
                return null;
            }

            var styleNameInfo = StyleNameInfo.FromStyleName(listStyleName);
            if (styleNameInfo.ListLevel != 1)
            {
                styleNameInfo = new StyleNameInfo(styleNameInfo.Category, styleNameInfo.TextLevel, 1);
            }

            Paragraph nextListRestartParagraph = null;
            while (StyleConceptNew.IsStyleAvailable(styleNameInfo.StyleName, paragraph.Range.Document))
            {
                var nextParagraph = FindParagraphHavingStyle(paragraph, styleNameInfo.StyleName, true);
                if (nextParagraph.IsRestartingListNumbering())
                {
                    if (nextListRestartParagraph == null
                        || nextListRestartParagraph.Range.Start > nextParagraph.Range.Start)
                    {
                        nextListRestartParagraph = nextParagraph;
                    }
                }

                styleNameInfo = styleNameInfo.ListDemote();
            }

            return nextListRestartParagraph;
        }

        public static bool ExistsParagraphOfListType(this Paragraph paragraph, string listStyleName, bool searchForward)
        {
            var styleNameInfo = StyleNameInfo.FromStyleName(listStyleName);
            if (styleNameInfo.ListLevel != 1)
            {
                styleNameInfo = new StyleNameInfo(styleNameInfo.Category, styleNameInfo.TextLevel, 1);
            }

            while (StyleConceptNew.IsStyleAvailable(styleNameInfo.StyleName, paragraph.Range.Document))
            {
                var nextParagraph = paragraph;
                if (FindParagraphHavingStyle(nextParagraph, styleNameInfo.StyleName, searchForward) != null)
                {
                    return true;
                }

                styleNameInfo = styleNameInfo.ListDemote();
            }

            return false;
        }

        public static bool IsSameParagraph(this Paragraph paragraph, Paragraph other)
        {
            return paragraph != null && other != null && paragraph.Range.Start == other.Range.Start;
        }

        public static void SetSpaceBeforeAuto(this Paragraph paragraph, bool value)
        {
            paragraph.SpaceBeforeAuto = value ? -1 : 0;
        }

        public static void SetSpaceAfterAuto(this Paragraph paragraph, bool value)
        {
            paragraph.SpaceAfterAuto = value ? -1 : 0;
        }

        public static void SetPageBreakBefore(this Paragraph paragraph, bool value)
        {
            paragraph.PageBreakBefore = value ? -1 : 0;
        }

        public static void SetKeepWithNext(this Paragraph paragraph, bool value)
        {
            paragraph.KeepWithNext = value ? -1 : 0;
        }

        public static void SetKeepTogether(this Paragraph paragraph, bool value)
        {
            paragraph.KeepTogether = value ? -1 : 0;
        }

        /// <summary>
        /// Search for a paragraph having a specific paragraph style name.
        /// </summary>
        /// <param name="paragraph">Search is started at that paragraph (paragraph itself not included).</param>
        /// <param name="styleName">Paragraph style to be searched for.</param>
        /// <param name="forward">search direction forward (true) or backward. Search always stops at start or end of document.</param>
        /// <returns>first paragraph found or null otherwise.</returns>
        private static Paragraph FindParagraphHavingStyle(this Paragraph paragraph, string styleName, bool forward)
        {
            var searchRange = forward ? paragraph.Range.CollapseToEnd() : paragraph.Range.CollapseToStart();

            var find = searchRange.Find;
            find.ClearFormatting();
            find.Wrap = WdFindWrap.wdFindStop;
            find.Forward = forward;
            find.Text = "";
            find.set_Style(styleName);
            find.Execute();
            if (find.Found)
            {
                return forward ? searchRange.Paragraphs.First : searchRange.Paragraphs.Last;
            }

            return null;
        }
    }
}
