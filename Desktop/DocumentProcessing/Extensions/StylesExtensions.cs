﻿namespace Eurolook.DocumentProcessing.Extensions
{
    public static class StylesExtensions
    {
        public static bool IsBuiltinStyle(string styleName)
        {
            return LanguageIndependentStyle.NeutralNameToStyleIdMap.ContainsKey(styleName);
        }
    }
}
