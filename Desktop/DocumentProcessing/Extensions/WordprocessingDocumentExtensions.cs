﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing.OpenXml;
using JetBrains.Annotations;

namespace Eurolook.DocumentProcessing.Extensions
{
    public static class WordprocessingDocumentExtensions
    {
        [CanBeNull]
        public static CustomXmlPart GetCustomXmlPart(this WordprocessingDocument docx, XNamespace ns, string name)
        {
            return GetCustomXmlPart(docx, ns + name);
        }

        [CanBeNull]
        public static CustomXmlPart GetCustomXmlPart(this WordprocessingDocument docx, XName rootElementName)
        {
            var customXmlPart =
                docx.MainDocumentPart?.CustomXmlParts.FirstOrDefault(
                    p => rootElementName == p.GetXDocument()?.Root?.Name);
            return customXmlPart;
        }

        [NotNull]
        public static IEnumerable<CustomXmlPart> GetCustomXmlParts(
            this WordprocessingDocument docx,
            XName rootElementName)
        {
            return docx.MainDocumentPart.CustomXmlParts.Where(p => rootElementName == p.GetXDocument()?.Root?.Name);
        }

        [CanBeNull]
        public static XDocument GetCustomXmlPartAsXDocumentByStoreItemId(
            this WordprocessingDocument docx,
            string storeItemId)
        {
            return docx.MainDocumentPart?.CustomXmlParts
                       .Where(
                           customXmlPart => customXmlPart.CustomXmlPropertiesPart?.DataStoreItem.ItemId == storeItemId)
                       .Select(customXmlPart => customXmlPart.GetXDocument()).FirstOrDefault();
        }

        public static CustomXmlPart AddNewCustomXmlPart(this WordprocessingDocument docx, XDocument xDocument)
        {
            using var ms = new MemoryStream();
            using (var writer = XmlWriter.Create(ms))
            {
                xDocument.WriteTo(writer);
            }

            return docx.AddCustomXml(ms, Guid.NewGuid());
        }

        // set the language of the 'Normal' style according to the language of the document (using locale stored in database)
        public static void SetLanguage(this WordprocessingDocument docx, Language lang)
        {
            var stylesList = new List<Styles>
            {
                docx.GetOrCreateStyleDefinitionsPart(WordprocessingDocumentMode.MainDocument).Styles,
                docx.GetOrCreateStyleDefinitionsPart(WordprocessingDocumentMode.GlossaryDocument).Styles,
            };

            if (docx.MainDocumentPart?.StylesWithEffectsPart != null
                && docx.MainDocumentPart.StylesWithEffectsPart.Styles != null)
            {
                stylesList.Add(docx.MainDocumentPart.StylesWithEffectsPart.Styles);
            }

            foreach (var styles in stylesList)
            {
                var docDefaults = styles.DocDefaults ?? (styles.DocDefaults = new DocDefaults());
                var runPropertiesDefault = docDefaults.RunPropertiesDefault
                                           ?? (docDefaults.RunPropertiesDefault = new RunPropertiesDefault());
                var runPropertiesBaseStyle = runPropertiesDefault.RunPropertiesBaseStyle
                                             ?? (runPropertiesDefault.RunPropertiesBaseStyle =
                                                 new RunPropertiesBaseStyle());
                var languages = runPropertiesBaseStyle.Languages
                                ?? (runPropertiesBaseStyle.Languages = new Languages());

                languages.Val = lang.Locale;

                var styleElements = styles.Descendants<Style>()
                                          .Where(x => !x.StyleName.Val.ToString().StartsWith("lang_")).ToArray();
                foreach (var style in styleElements)
                {
                    foreach (var langElement in style.Descendants<Languages>())
                    {
                        langElement.Val = lang.Locale;
                        langElement.EastAsia = lang.Locale;
                    }
                }
            }
        }
    }
}
