﻿using System.Collections.Generic;
using System.Linq;
using Eurolook.DocumentProcessing.BrickDefinitionLanguage;

namespace Eurolook.DocumentProcessing.Extensions
{
    public static class ContentElementExtensions
    {
        public static IEnumerable<T> FindElementsOfType<T>(this IContentElement element)
        {
            if (element == null)
            {
                yield break;
            }

            foreach (var el in element.Content.OfType<T>())
            {
                yield return el;
            }
        }

        public static IEnumerable<T> FindDescendantsOfType<T>(this IContentElement element)
        {
            if (element == null)
            {
                yield break;
            }

            if (element is T)
            {
                yield return (T)element;
            }

            foreach (var el in element.Content.OfType<T>())
            {
                yield return el;
            }

            foreach (var el in element.Content.OfType<IContentElement>())
            {
                foreach (var child in FindDescendantsOfType<T>(el))
                {
                    yield return child;
                }
            }
        }
    }
}
