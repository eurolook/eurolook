﻿using System;
using System.Runtime.InteropServices;
using Eurolook.Common.Log;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.DocumentProcessing.Extensions
{
    public static class ContentControlExtensions
    {
        /// <summary>
        /// Determines whether the Content Control is a block-level control, i.e. it is rendered in its own paragraph.
        /// </summary>
        /// <param name="contentControl">The content control to be checked.</param>
        /// <returns>A flag indicating whether the content control is rendered at block-level and not inline inside a paragraph.</returns>
        public static bool IsBlockLevel(this ContentControl contentControl)
        {
            int start = contentControl.Range.Paragraphs.First.Range.Start;
            int end = contentControl.Range.Paragraphs.Last.Range.End;
            return contentControl.Range.Start == start + 1
                   && contentControl.Range.End >= end - 2
                   && contentControl.Range.End <= end;
        }

        public static bool IsBrick(this ContentControl contentControl)
        {
            var brickContainer = GetBrickContainer(contentControl);
            if (brickContainer != null && brickContainer.Id != Guid.Empty)
            {
                return true;
            }

            // TODO: check if the Guid actually belongs to a brick
            return contentControl != null && Guid.TryParse(contentControl.Tag, out _);
        }

        public static bool IsInDeletedRevision(this ContentControl contentControl)
        {
            if (contentControl == null)
            {
                return false;
            }

            return contentControl.Range.IsInDeletedRevision();
        }

        /// <summary>
        /// A safe way to check if two COM objects refer to the same content control, i.e. to the same position
        /// in the document. Safe means that this method does not throw an exception, if one of the content controls
        /// has been deleted.
        /// </summary>
        /// <param name="contentControl"></param>
        /// <param name="other"></param>
        /// <returns></returns>
        public static bool IsSame(this ContentControl contentControl, ContentControl other)
        {
            try
            {
                if (contentControl == other)
                {
                    return true;
                }

                if (contentControl == null || other == null)
                {
                    return false;
                }

                return contentControl.Tag == other.Tag
                       && contentControl.Range.Start == other.Range.Start
                       && contentControl.Range.End == other.Range.End;
            }
            catch (COMException ex)
            {
                LogManager.GetLogger(typeof(ContentControlExtensions)).Debug(ex.Message, ex);
            }

            return false;
        }

        /// <summary>
        /// Get the range including the actual control (contentControl.Range only gets the range inside the control.
        /// </summary>
        /// <param name="contentControl"></param>
        /// <returns></returns>
        public static Range GetControlRange(this ContentControl contentControl)
        {
            var range = contentControl.Range;
            try
            {
                range.MoveEnd(WdUnits.wdCharacter);
                range.MoveStart(WdUnits.wdCharacter, -1);
            }
            catch (COMException ex)
            {
                LogManager.GetLogger(typeof(ContentControlExtensions)).Debug(ex.Message, ex);
            }

            return range;
        }

        public static BrickContainer GetBrickContainer(this ContentControl contentControl)
        {
            if (contentControl == null)
            {
                return null;
            }

            string tag = contentControl.Tag;
            if (string.IsNullOrWhiteSpace(tag))
            {
                return null;
            }

            var brickContainer = BrickContainer.FromTag(tag);
            if (brickContainer == null)
            {
                return null;
            }

            brickContainer.ContentControl = contentControl;
            return brickContainer;
        }

        /// <summary>
        /// Gets the brick content control if the passed in content control is part of a brick.
        /// </summary>
        /// <param name="contentControl">
        /// A content control nested within the brick content control or the brick content
        /// control itself.
        /// </param>
        /// <returns>A brick content control or null.</returns>
        public static ContentControl GetBrickContentControl(this ContentControl contentControl)
        {
            if (contentControl == null)
            {
                return null;
            }

            if (contentControl.IsBrick())
            {
                return contentControl;
            }

            return GetBrickContentControl(contentControl.ParentContentControl);
        }
    }
}
