using System.Runtime.InteropServices;

namespace Eurolook.DocumentProcessing.Extensions
{
    public static class ComExceptionExtensions
    {
        public static bool IsWarning(this COMException ex)
        {
            return ex.IsLockedOrReadonlyException() ||
                   ex.CurrentSelectionPartiallyCoversPlainTextContentControl();
        }

        public static bool IsLockedOrReadonlyException(this COMException ex)
        {
            // 0xE0041818 = This command is not allowed because the document is locked
            // 0x800A11FD = This method or property is not available because this command is not available for reading
            return (uint)ex.ErrorCode == 0xE0041818 || (uint)ex.ErrorCode == 0x800A11FD;
        }

        public static bool HasObjectBeenDeletedException(this COMException ex)
        {
            return (uint)ex.HResult == 0x800A16C1;
        }

        public static bool IsWordHasEncounteredAProblemException(this COMException ex)
        {
            return (uint)ex.HResult == 0x80020009 || (uint)ex.HResult == 0x800A13E9;
        }

        public static bool CurrentSelectionPartiallyCoversPlainTextContentControl(this COMException ex)
        {
            return (uint)ex.HResult == 0x800A11FD;
        }
    }
}
