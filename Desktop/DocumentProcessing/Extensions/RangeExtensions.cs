﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using Eurolook.Common.Log;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;
using Rectangle = System.Drawing.Rectangle;

namespace Eurolook.DocumentProcessing.Extensions
{
    public static class RangeExtensions
    {
        public enum RangePosition
        {
            NotBeforeOrAfterBlockLevelContentControl = 0,
            BeforeBlockLevelContentControl,
            AfterBlockLevelContentControl,
        }

        /// <summary>
        /// Same as <see cref="Range.Select" />, but asserts that the document remains in print layout view
        /// and does not switch to a view displaying e.g. headers or footers in a separate window.
        /// </summary>
        /// <param name="range">The <see cref="Range" /> to be selected.</param>
        /// <param name="wdPreferredViewType">The view type that shall be opened.</param>
        /// <returns>The selected range.</returns>
        public static Range SelectInPreferredView(this Range range, WdViewType wdPreferredViewType)
        {
            if (range == null)
            {
                return null;
            }

            var view = range.Document.Windows[1].View;

            if (wdPreferredViewType != 0)
            {
                view.Type = wdPreferredViewType;
            }

            if (view.Type == WdViewType.wdPrintView)
            {
                try
                {
                    switch (range.StoryType)
                    {
                        case WdStoryType.wdMainTextStory:
                            view.SeekView = WdSeekView.wdSeekMainDocument;
                            break;
                        case WdStoryType.wdFootnotesStory:
                            view.SeekView = WdSeekView.wdSeekFootnotes;
                            break;
                        case WdStoryType.wdEndnotesStory:
                            view.SeekView = WdSeekView.wdSeekEndnotes;
                            break;
                        case WdStoryType.wdEvenPagesHeaderStory:
                            view.SeekView = WdSeekView.wdSeekEvenPagesHeader;
                            break;
                        case WdStoryType.wdPrimaryHeaderStory:
                            view.SeekView = WdSeekView.wdSeekPrimaryHeader;
                            break;
                        case WdStoryType.wdEvenPagesFooterStory:
                            view.SeekView = WdSeekView.wdSeekEvenPagesFooter;
                            break;
                        case WdStoryType.wdPrimaryFooterStory:
                            view.SeekView = WdSeekView.wdSeekPrimaryFooter;
                            break;
                        case WdStoryType.wdFirstPageHeaderStory:
                            view.SeekView = WdSeekView.wdSeekFirstPageHeader;
                            break;
                        case WdStoryType.wdFirstPageFooterStory:
                            view.SeekView = WdSeekView.wdSeekFirstPageFooter;
                            break;
                        case WdStoryType.wdCommentsStory:
                        case WdStoryType.wdTextFrameStory:
                        case WdStoryType.wdFootnoteSeparatorStory:
                        case WdStoryType.wdFootnoteContinuationSeparatorStory:
                        case WdStoryType.wdFootnoteContinuationNoticeStory:
                        case WdStoryType.wdEndnoteSeparatorStory:
                        case WdStoryType.wdEndnoteContinuationSeparatorStory:
                        case WdStoryType.wdEndnoteContinuationNoticeStory:
                            break;
                    }

                    view.Type = WdViewType.wdPrintView;
                }
                catch (COMException)
                {
                    // Is thrown when SeekView cannot be set, e.g. when the specified story cannot be shown:
                    // "wdEvenPagesFooterStory" is requested for a document that has one page.
                    // Such a range can still be shown in normal view
                    view.Type = wdPreferredViewType != 0 ? wdPreferredViewType : WdViewType.wdNormalView;
                }
            }

            range.Select();
            return range;
        }

        public static Range CollapseToStart(this Range range)
        {
            var rangeBegin = range.Duplicate;
            rangeBegin.Collapse(WdCollapseDirection.wdCollapseStart);
            return rangeBegin;
        }

        public static Range CollapseToEnd(this Range range)
        {
            var rangeBegin = range.Duplicate;
            rangeBegin.Collapse(WdCollapseDirection.wdCollapseEnd);
            return rangeBegin;
        }

        public static bool IsTextVisibleOnScreen(this Range range)
        {
            dynamic rng = range;
            try
            {
                return rng.TextVisibleOnScreen != 1;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static void ScrollIntoViewIfNeeded(this Range range)
        {
            if (!range.IsTextVisibleOnScreen())
            {
                range.Document.ActiveWindow.ScrollIntoView(range);
            }
        }

        public static bool HasTables(this Range range)
        {
            return range?.Tables.Count > 0;
        }

        public static bool IsLocked(this Range range)
        {
            if (range == null)
            {
                return false;
            }

            var parentControl = range.ParentContentControl;
            bool hasLockedContentControls = (parentControl != null && parentControl.LockContents)
                                            || range.ContentControls.OfType<ContentControl>()
                                                    .Any(cc => cc.LockContents);
            return hasLockedContentControls;
        }

        public static bool IsInPlaceholderText(this Range range)
        {
            var contentControl = GetContentControl(range);
            return contentControl != null && contentControl.ShowingPlaceholderText;
        }

        public static bool IsInDeletedRevision(this Range range)
        {
            if (range == null)
            {
                return false;
            }

            // NOTE: Accessing (and enumerating) the Revisions object causes the selection to be collapsed.
            //   Therefore we have to take care to restore the selection
            // UPDATE: The selection being collapsed seems no longer to be an issue. Commenting out this code, as restoring the
            //   selection causes flickering on every WindowSelectionChange event.
            return range.Revisions.OfType<Revision>()
                        .Any(r => r.Type == WdRevisionType.wdRevisionDelete && range.InRange(r.Range));
            ////bool result = false;
            ////var selectedRange = range.Application.Selection.Range;
            ////try
            ////{
            ////    result = range.Revisions.OfType<Revision>().Any(r => r.Type == WdRevisionType.wdRevisionDelete && range.InRange(r.Range));
            ////}
            ////finally
            ////{
            ////    if (!result)
            ////    {
            ////        // The selection is only restored in case there are no revisions. This is to prevent
            ////        // the WindowSelectionChange event from firing
            ////        selectedRange.Select();
            ////    }
            ////}

            ////return result;
        }

        public static string TextWithoutPlaceholders(this Range range)
        {
            if (range == null)
            {
                return null;
            }

            var style = LanguageIndependentStyle.FromNeutralStyleName("Placeholder Text", range.Document);
            if (style == null)
            {
                // no placeholder text in document
                return range.Text;
            }

            int placeholderStyleHiddenOrig = 0;
            var undoRecord = range.Application.UndoRecord;
            undoRecord.StartCustomRecord("Retrieve Text");

            string result;
            try
            {
                // hide placeholder text style
                placeholderStyleHiddenOrig = style.Style.Font.Hidden;
                style.Style.Font.Hidden = -1;

                var tmpRange = range.Duplicate;
                tmpRange.TextRetrievalMode.IncludeHiddenText = false;
                tmpRange.TextRetrievalMode.IncludeFieldCodes = false;

                result = tmpRange.Text ?? "";
            }
            finally
            {
                style.Style.Font.Hidden = placeholderStyleHiddenOrig;
                undoRecord.EndCustomRecord();
            }

            return result;
        }

        public static bool IsVanishingContentControl(this Range range)
        {
            var parentControl = range.ParentContentControl;
            return parentControl != null && parentControl.Temporary;
        }

        public static void HandleVanishingContentControl(this Range range)
        {
            if (range?.ParentContentControl?.Temporary == true)
            {
                // the placeholder text is selected
                range.ParentContentControl.Delete();
            }
            else if (range?.Paragraphs.Count == 1)
            {
                // the paragraph contains a single vanishing placeholder content control
                // and the selection is before/after the control or partially covering the control
                var paragraphRange = range.Paragraphs.First?.Range;
                var contentControl = paragraphRange?.ContentControls.OfType<ContentControl>().FirstOrDefault();
                if (contentControl != null
                    && contentControl.Range.Start == paragraphRange.Start + 1
                    && contentControl.Range.End == paragraphRange.End - 2
                    && contentControl.Temporary)
                {
                    contentControl.Delete();
                }
            }
        }

        public static bool IsBeforeOrAfterBlockLevelContentControl(this Range range)
        {
            var rangePosition = range.GetPositionRelativeToBlockLevelContentControl();
            return rangePosition == RangePosition.BeforeBlockLevelContentControl ||
                   rangePosition == RangePosition.AfterBlockLevelContentControl;
        }

        public static RangePosition GetPositionRelativeToBlockLevelContentControl(this Range range)
        {
            if (range.Start != range.End)
            {
                return RangePosition.NotBeforeOrAfterBlockLevelContentControl;
            }

            var paragraphRange = range.Paragraphs[1].Range;

            // Do not include the last character of the paragraph range. If the paragraph is the last paragraph of a
            // table cell this last character is the end-of-cell mark and including the end-of-cell mark means that the
            // range will contain all content controls of the entire table cell
            // (and not only the ones of the last paragraph).
            paragraphRange.MoveEnd(WdUnits.wdCharacter, -1);

            foreach (var cc in paragraphRange.ContentControls.OfType<ContentControl>().Where(cc => cc.IsBlockLevel()))
            {
                if (range.Start == cc.Range.Paragraphs.First.Range.Start)
                {
                    return RangePosition.BeforeBlockLevelContentControl;
                }

                if (range.Start > cc.Range.End)
                {
                    return RangePosition.AfterBlockLevelContentControl;
                }
            }

            return RangePosition.NotBeforeOrAfterBlockLevelContentControl;
        }

        public static bool IsCoveringContentControl(this Range range)
        {
            return range.ContentControls.OfType<ContentControl>().Any() || range.ParentContentControl != null;
        }

        public static bool HasPlainTextContentControl(this Range range)
        {
            return range.ContentControls.OfType<ContentControl>()
                        .Any(cc => cc.Type == WdContentControlType.wdContentControlText);
        }

        /// <summary>
        /// Returns the range of the previous paragraph.
        /// </summary>
        /// <param name="range">The <see cref="Range" /> to start the search from.</param>
        /// <returns>
        /// Returns the range of the paragraph preceding the paragraph where the current range starts.
        /// If the range starts in the first paragraph, this method returns null.
        /// </returns>
        public static Range PreviousParagraphRange(this Range range)
        {
            var firstParaInRange = range.Paragraphs.First.Range;
            if (firstParaInRange.Start == 0)
            {
                return null;
            }

            // go back two (!) paragraphs, going back only one collapses the selection
            // to the beginning of the paragraph
            firstParaInRange.Move(WdUnits.wdParagraph, -2);
            firstParaInRange.Expand(WdUnits.wdParagraph);

            return firstParaInRange;
        }

        public static Range NextParagraphRange(this Range range)
        {
            var lastParaInRange = range.Paragraphs.Last;
            var nextPara = lastParaInRange.Next();
            return nextPara?.Range;
        }

        /// <summary>
        /// Inserts a paragraph before the given range.
        /// </summary>
        /// <param name="range">The range to insert a paragraph before.</param>
        /// <remarks>
        /// The built-in Word API function <see cref="Range.InsertParagraphBefore" /> throws an exception
        /// "COMException (0x800A11FD): This method or property is not available because this command is not
        /// available for reading." if Word had one of the document windows open in reading layout (see EUROLOOK-2221).
        /// This method acts as a safe replacement with supposedly identical behavior (i.e. the original range is
        /// modified).
        /// </remarks>
        public static void InsertParagraphBeforeSafe(this Range range)
        {
            try
            {
                range.InsertParagraphBefore();
            }
            catch (COMException ex) when (ex.IsLockedOrReadonlyException())
            {
                // disable opening in reading view
                range.Application.OpenAttachmentsInFullScreen = false;

                // use 'safe' fallback
                var tmpRange = range.Duplicate;
                tmpRange.Collapse(WdCollapseDirection.wdCollapseStart);
                tmpRange.SetTextSafe("\r");
                if (range.Start == range.End)
                {
                    range.Start = tmpRange.Start;
                    range.End = tmpRange.End;
                }
                else
                {
                    range.Start = tmpRange.Start;
                }
            }
        }

        /// <summary>
        /// Inserts a paragraph after the given range.
        /// </summary>
        /// <param name="range">The range to insert a paragraph after.</param>
        /// <remarks>
        /// The built-in Word API function <see cref="Range.InsertParagraphAfter" /> throws an exception
        /// "COMException (0x800A11FD): This method or property is not available because this command is not
        /// available for reading." if Word had one of the document windows open in reading layout (see EUROLOOK-2221).
        /// This method acts as a safe replacement with supposedly identical behavior (i.e. the original range is
        /// modified).
        /// </remarks>
        public static void InsertParagraphAfterSafe(this Range range)
        {
            try
            {
                range.InsertParagraphAfter();
            }
            catch (COMException ex) when (ex.IsLockedOrReadonlyException())
            {
                // disable opening in reading view
                range.Application.OpenAttachmentsInFullScreen = false;

                // use 'safe' fallback
                var tmpRange = range.Duplicate;
                tmpRange.Collapse(WdCollapseDirection.wdCollapseEnd);

                tmpRange.SetTextSafe("\r");
            }
        }

        /// <summary>
        /// Sets the <see cref="Range.Text" /> property in a safe way.
        /// </summary>
        /// <param name="range">The range where the text is to be set.</param>
        /// <param name="text">The text to be set.</param>
        /// <remarks>
        /// Setting the built-in property <see cref="Range.Text" /> fails if the range points to the placeholder
        /// of a content control. The following exception is thrown in such a case:
        /// COMException (0x800A17EC): You are not allowed to edit this selection because it is protected.
        /// This method temporarily removes the placeholder.
        /// </remarks>
        public static void SetTextSafe(this Range range, string text)
        {
            var tmpRange = range.Duplicate;
            var parentContentControl = tmpRange.ParentContentControl;
            if (parentContentControl?.ShowingPlaceholderText == true)
            {
                var bb = parentContentControl.PlaceholderText;
                parentContentControl.SetPlaceholderText();
                tmpRange.Text = text;
                parentContentControl.SetPlaceholderText(bb);
            }
            else
            {
                tmpRange.Text = text;
            }
        }

        public static Range InsertParagraphBeforeAndHandleLockedContentControls(this Range range)
        {
            using (new ContentControlUnlocker(range.Document))
            {
                var tmpRange = range.Paragraphs.First.Range.Duplicate;

                if (tmpRange.HasTables())
                {
                    if (tmpRange.IsAtBeginningOfTable())
                    {
                        tmpRange = tmpRange.Tables[1].InsertParagraphBefore();
                    }
                    else
                    {
                        tmpRange.InsertParagraphBeforeSafe();
                    }
                }
                else
                {
                    tmpRange.Collapse(WdCollapseDirection.wdCollapseStart);
                    tmpRange.InsertParagraphBeforeSafe();

                    tmpRange.Start = range.Start;
                }

                return tmpRange;
            }
        }

        [NotNull]
        public static Range InsertParagraphAfterAndHandleLockedContentControls(this Range range)
        {
            using (new ContentControlUnlocker(range.Document))
            {
                // workaround "table at end of content control" problem
                var nextParagraphRange = range.NextParagraphRange();
                if (nextParagraphRange != null && range.End + 1 == nextParagraphRange.Start && range.HasTables())
                {
                    if (range.End + 2 == range.Tables[range.Tables.Count].Range.End)
                    {
                        nextParagraphRange = range.Tables[range.Tables.Count].Range;
                        nextParagraphRange = nextParagraphRange.NextParagraphRange();
                    }
                    else if (range.End + 2 == range.Tables[1].Range.End)
                    {
                        nextParagraphRange = range.Tables[1].Range;
                        nextParagraphRange = nextParagraphRange.NextParagraphRange();
                    }
                }

                var workingRange = range.Paragraphs.Last.Range.Duplicate;
                workingRange.Collapse(WdCollapseDirection.wdCollapseEnd);

                // ReSharper disable once UseIndexedProperty
                var style = (Style)workingRange.Paragraphs.Last.get_Style();
                Paragraph newParagraph;
                if (nextParagraphRange != null && nextParagraphRange.HasTables())
                {
                    // table follows, add paragraph before table
                    workingRange = nextParagraphRange.Tables[1].InsertParagraphBefore();
                    newParagraph = workingRange.Paragraphs.Last.Next();
                }
                else if (workingRange.HasTables())
                {
                    // inside table, add paragraph after
                    newParagraph = workingRange.Tables[1].InsertParagraphAfter().Paragraphs.Last;
                    workingRange = newParagraph.Range;
                }
                else if (workingRange.IsEndOfStory())
                {
                    // last paragraph in story
                    workingRange.InsertParagraphAfterSafe();
                    workingRange = range.Document.StoryRanges[range.StoryType].Paragraphs.Last.Range;
                    workingRange.Collapse();
                    newParagraph = workingRange.Paragraphs.Last.Next();
                }
                else
                {
                    workingRange.InsertParagraphAfterSafe();
                    workingRange.Start = range.Paragraphs.Last.Range.End;
                    workingRange.End = range.Paragraphs.Last.Range.End;
                    newParagraph = workingRange.Paragraphs.Last.Next();
                }

                // the newly inserted paragraph must have the same style as the previous paragraph
                // (and not the 'next paragraph style' defined in the style definition of the previous paragraph
                if (newParagraph != null && newParagraph.GetStyleNameLocal() != style.NameLocal)
                {
                    // ReSharper disable once UseIndexedProperty
                    newParagraph.set_Style(style);
                }

                return workingRange;
            }
        }

        /// <summary>
        /// Delete the paragraph at the end of the inserted range.
        /// </summary>
        /// <remarks>
        /// The conditional block is a workaround for a Word problem. Word does not always return
        /// the correct range when inserting building blocks
        /// cf. EUROLOOK-1367 "Track Changes: superfluous paragraph after letterhead"
        /// The number of assertions for the workaround to be effective could be reduce but have been implemented for
        /// safety reasons to be sure not to affect other parts.
        /// </remarks>
        public static void DeleteAdditionalParagraphAtEnd(this Range range)
        {
            range.Collapse(WdCollapseDirection.wdCollapseEnd);

            if (range.Revisions.Count > 0
                && range.Information[WdInformation.wdWithInTable].Equals(true))
            {
                var testRange = range.Duplicate;
                testRange.MoveEnd(WdUnits.wdCharacter, 1);
                if (testRange.Text == "\r\a")
                {
                    testRange.MoveStart(WdUnits.wdCharacter, 1);
                    testRange.MoveEnd(WdUnits.wdCharacter, 1);
                    if (testRange.Text == "\r")
                    {
                        range.MoveStart(WdUnits.wdCharacter, 1);
                    }
                }
            }

            range.Delete();
        }

        public static Range GetCursorBrickInsertionRange(this Range originalRange)
        {
            var range = originalRange;
            range.HandleVanishingContentControl();
            range.Collapse();

            // if inside brick insert before or after the brick (and not inside the brick)
            var contentControl = range.GetContentControl();
            if (contentControl != null)
            {
                range = contentControl.Range;
                var brickContentControl = contentControl.GetBrickContentControl();
                if (brickContentControl != null)
                {
                    range = brickContentControl.GetControlRange();
                }

                // special handling:
                //    when selection is at the beginning of the brick
                //    --> insert before, otherwise insert after the brick
                int start = Math.Min(originalRange.Start, range.Start);
                int end = Math.Max(originalRange.Start, range.Start);
                var rangeUntilBeginningOfContentControl = originalRange.Duplicate;
                rangeUntilBeginningOfContentControl.SetRange(start, end);
                bool isAtBeginningOfContentControl =
                    string.IsNullOrWhiteSpace(rangeUntilBeginningOfContentControl.Text);
                if (isAtBeginningOfContentControl)
                {
                    range.Collapse();
                }
                else
                {
                    range.Collapse(WdCollapseDirection.wdCollapseEnd);
                }
            }

            bool isAtEndOfRow = range.IsEndOfRowMark;
            if (isAtEndOfRow)
            {
                ////var end = range.Tables.OfType<Table>().Select(t => t.Range).Max(r => r.End);
                ////range.SetRange(end, end);
                range.Move(WdUnits.wdCharacter);
            }

            bool isAtEndOfStory = range.IsEndOfStory();
            if (isAtEndOfStory)
            {
                // Add additional paragraph at the end of the document so that the document doesn't end with a brick
                // where it would be difficult to add content behind. The new paragraph will automatically be formatted
                // with the next style of the current paragraph.
                range.InsertParagraphAfterSafe();
                range.Collapse();
            }

            bool isAtBeginningOfParagraph = range.IsAtBeginningOfParagraph();
            bool isAtEndOfParagraph = range.IsAtEndOfParagraph();
            bool isAtSectionBreak = range.IsAtBreak();

            bool isEmptyParagraph = isAtBeginningOfParagraph && isAtEndOfParagraph && !isAtSectionBreak;

            if (isEmptyParagraph)
            {
                range.Expand(WdUnits.wdParagraph);
                if (range.Text.Equals("\r\a") || range.Text.Equals("\f\r"))
                {
                    // empty table cell with no paragraph mark or page break --> insert a paragraph
                    range.Collapse();
                    range.InsertParagraphAfterSafe();
                }

                range.Collapse();
                return range;
            }

            if (isAtEndOfParagraph && !isAtSectionBreak)
            {
                // add paragraph and insert at beginning of newly inserted paragraph
                range.InsertParagraphAfterSafe();
                range.Collapse(WdCollapseDirection.wdCollapseEnd);
                return range;
            }

            if (isAtBeginningOfParagraph)
            {
                range.SetRange(range.Paragraphs.First.Range.Start, range.Start);
                range = range.InsertParagraphBeforeAndHandleLockedContentControls();
                range.Collapse();
                return range;
            }

            // somewhere in the middle of the paragraph
            range.InsertParagraphBeforeSafe();
            range.Collapse(WdCollapseDirection.wdCollapseEnd);
            range = range.InsertParagraphBeforeAndHandleLockedContentControls();
            range.Collapse();
            return range;
        }

        public static bool SelectFirstPlaceholderContentControl(this Range range, WdViewType preferredViewType)
        {
            var tmpRange = range.Duplicate;
            var control =
                tmpRange.ContentControls.OfType<ContentControl>()
                        .FirstOrDefault(cc => !cc.LockContents && cc.ShowingPlaceholderText);
            if (control != null)
            {
                control.Range.SelectInPreferredView(preferredViewType);
                return true;
            }

            return false;
        }

        public static bool SelectFirstEditableContentControl(this Range range, WdViewType preferredViewType)
        {
            var tmpRange = range.Duplicate;
            var control =
                tmpRange.ContentControls.OfType<ContentControl>()
                        .FirstOrDefault(cc => !cc.LockContents && cc.Range.ContentControls.Count == 0);
            if (control != null)
            {
                control.Range.SelectInPreferredView(preferredViewType);
                return true;
            }

            return false;
        }

        public static bool IsLastParagraphInStory(this Range range)
        {
            return range.Paragraphs.Last.IsLastParagraphInStory();
        }

        public static bool IsEndOfStory(this Range range)
        {
            var tmpRange = range.Duplicate;
            var storyRange = range.Document.StoryRanges[range.StoryType];
            return tmpRange.End + 1 >= storyRange.End;
        }

        public static bool IsAtEndOfParagraph(this Range range)
        {
            var rangeUntilEndOfParagraph = range.Duplicate;
            rangeUntilEndOfParagraph.SetRange(range.End, range.Paragraphs.Last.Range.End);
            string text = rangeUntilEndOfParagraph.Text;
            return string.IsNullOrWhiteSpace(text) || string.IsNullOrWhiteSpace(text.Trim('\a', '\r'));
        }

        public static bool IsAtBeginningOfParagraph(this Range range)
        {
            var rangeUntilBeginningOfParagraph = range.Duplicate;
            rangeUntilBeginningOfParagraph.SetRange(range.Paragraphs.First.Range.Start, range.Start);
            string text = rangeUntilBeginningOfParagraph.Text;
            return string.IsNullOrWhiteSpace(text) || string.IsNullOrWhiteSpace(text.Trim('\a', '\r'));
        }

        public static bool IsAtBreak(this Range range)
        {
            var followingCharacterRange = range.Duplicate;
            followingCharacterRange.Collapse(WdCollapseDirection.wdCollapseEnd);
            followingCharacterRange.MoveEnd(WdUnits.wdCharacter);
            var followingCharacter = followingCharacterRange.Text?.FirstOrDefault();
            return followingCharacter == '\f';
        }

        public static bool IsAtBeginningOfTable(this Range range)
        {
            if (!range.HasTables())
            {
                return false;
            }

            var rangeUntilBeginningOfTable = range.Duplicate;
            rangeUntilBeginningOfTable.SetRange(range.Tables[1].Range.Start, range.Start);
            string text = rangeUntilBeginningOfTable.Text;
            return string.IsNullOrWhiteSpace(text) || string.IsNullOrWhiteSpace(text.Trim('\r'));
        }

        /// <summary>
        /// Recursive search for a Brick Content Control.
        /// </summary>
        /// <param name="range">The search range.</param>
        public static ContentControl FindBrickContentControl(this Range range)
        {
            var cc = GetContentControl(range);
            return cc.GetBrickContentControl();
        }

        public static ContentControl GetContentControl(this Range range)
        {
            if (range == null)
            {
                return null;
            }

            ContentControl cc = null;

            try
            {
                if (range.ContentControls.Count > 0 && range.IsFittingContentControl(range.ContentControls[1]))
                {
                    // Check if the given range precisely surrounds a content control.
                    // This can happen when a users clicks on the "tab" of the content control or a brick tile on the taskpane.
                    // When you click on the "tab" of the content control the selected range is (-1 to +1).
                    // When you click on a brick tile on the taskpane the selected range is (-1 to +2).
                    cc = range.ContentControls[1];
                }

                if (cc == null)
                {
                    // the range is probably somewhere inside a content control.
                    cc = range.ParentContentControl;
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(typeof(RangeExtensions)).Error(ex.ToString());
            }

            return cc;
        }

        public static bool IsFittingContentControl(this Range range, ContentControl contentControl)
        {
            try
            {
                int start = contentControl.Range.Paragraphs.First.Range.Start;
                int end = contentControl.Range.Paragraphs.Last.Range.End;
                int tableEnd = end;
                if (contentControl.Range.HasTables())
                {
                    tableEnd = contentControl.Range.Tables[contentControl.Range.Tables.Count].Range.End;
                }

                end = Math.Max(end, tableEnd);
                return range.Start == start && (range.End == end || range.End + 1 == end);
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(typeof(RangeExtensions)).Error(ex.ToString());
                return false;
            }
        }

        public static Rectangle GetScreenRectFromObject(this Range range)
        {
            int screenPixelsLeft;
            int screenPixelsTop;
            int screenPixelsWidth;
            int screenPixelsHeight;
            var window = range.Document.ActiveWindow;
            try
            {
                window.GetPoint(
                    out screenPixelsLeft,
                    out screenPixelsTop,
                    out screenPixelsWidth,
                    out screenPixelsHeight,
                    range);
            }
            catch (COMException)
            {
                // fallback if GetPoint fails on a particular range: get position for beginning and end of range separately
                if (range.Start != range.End)
                {
                    var rangeBegin = range.Duplicate;
                    rangeBegin.Collapse();

                    var rangeEnd = range.Duplicate;
                    rangeEnd.Collapse(WdCollapseDirection.wdCollapseEnd);

                    window.GetPoint(out int left, out int top, out _, out _, rangeBegin);
                    screenPixelsLeft = left;
                    screenPixelsTop = top;

                    window.GetPoint(out left, out top, out screenPixelsWidth, out screenPixelsHeight, rangeBegin);
                    screenPixelsWidth = left - screenPixelsLeft;
                    screenPixelsHeight = top - screenPixelsTop;
                }
                else
                {
                    throw;
                }
            }

            return new Rectangle(screenPixelsLeft, screenPixelsTop, screenPixelsWidth, screenPixelsHeight);
        }

        public static Range GetRelativeRange(this Range range, int relativPositioninCharacters)
        {
            try
            {
                var resultRange = range.Duplicate;
                resultRange.MoveStart(WdUnits.wdCharacter, relativPositioninCharacters);
                resultRange.MoveEnd(WdUnits.wdCharacter, relativPositioninCharacters);
                return resultRange;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// This is a replacement for <see cref="Range.MoveStartWhile" /> which fixes an issue where Word
        /// hangs on ranges containing comments.
        /// </summary>
        /// <param name="range">A Word <see cref="Range" /> object.</param>
        /// <param name="cset">A string containing one or more characters. This argument is case sensitive.</param>
        /// <param name="direction">
        /// The direction in which to move the end of the range. Can be either the wdForward or wdBackward constant.
        /// </param>
        /// <remarks>
        /// See ECADOCS-328 and https://social.msdn.microsoft.com/Forums/sqlserver/en-US/1573099b-e57a-44f8-94d4-25d844758c5d/.
        /// </remarks>
        public static void MoveStartWhileSafe(
            this Range range,
            string cset,
            WdConstants direction = WdConstants.wdForward)
        {
            if (range == null)
            {
                throw new ArgumentNullException(nameof(range));
            }

            if (cset == null)
            {
                throw new ArgumentNullException(nameof(cset));
            }

            if (direction != WdConstants.wdForward && direction != WdConstants.wdBackward)
            {
                throw new ArgumentException("Direction must be forward or backward", nameof(direction));
            }

            char? firstChar = range.Text?.FirstOrDefault();
            while (firstChar != null && cset.Contains(firstChar.Value))
            {
                int count = direction == WdConstants.wdForward ? 1 : -1;
                range.MoveStart(WdUnits.wdCharacter, count);

                firstChar = range.Text?.FirstOrDefault();
            }
        }

        /// <summary>
        /// This is a replacement for <see cref="Range.MoveEndWhile" /> which fixes an issue where Word
        /// hangs on ranges containing comments.
        /// </summary>
        /// <param name="range">A Word <see cref="Range" /> object.</param>
        /// <param name="cset">A string containing one or more characters. This argument is case sensitive.</param>
        /// <param name="direction">
        /// The direction in which to move the end of the range. Can be either the wdForward or wdBackward constant.
        /// </param>
        /// <remarks>
        /// See ECADOCS-328 and https://social.msdn.microsoft.com/Forums/sqlserver/en-US/1573099b-e57a-44f8-94d4-25d844758c5d/.
        /// </remarks>
        public static void MoveEndWhileSafe(
            this Range range,
            string cset,
            WdConstants direction = WdConstants.wdForward)
        {
            if (range == null)
            {
                throw new ArgumentNullException(nameof(range));
            }

            if (cset == null)
            {
                throw new ArgumentNullException(nameof(cset));
            }

            if (direction != WdConstants.wdForward && direction != WdConstants.wdBackward)
            {
                throw new ArgumentException("Direction must be forward or backward", nameof(direction));
            }

            char? lastChar = range.Text?.LastOrDefault();
            while (lastChar != null && cset.Contains(lastChar.Value))
            {
                int count = direction == WdConstants.wdForward ? 1 : -1;
                range.MoveEnd(WdUnits.wdCharacter, count);

                lastChar = range.Text?.LastOrDefault();
            }
        }

        public static void MoveEndUntilSafe(
            this Range range,
            string cset,
            WdConstants direction = WdConstants.wdForward)
        {
            if (range == null)
            {
                throw new ArgumentNullException(nameof(range));
            }

            if (cset == null)
            {
                throw new ArgumentNullException(nameof(cset));
            }

            if (direction != WdConstants.wdForward && direction != WdConstants.wdBackward)
            {
                throw new ArgumentException("Direction must be forward or backward", nameof(direction));
            }

            int documentEnd = range.Document.Range().End;

            char? lastChar = range.Text?.LastOrDefault();
            while (range.End <= documentEnd && (lastChar == null || !cset.Contains(lastChar.Value)))
            {
                int count = direction == WdConstants.wdForward ? 1 : -1;
                range.MoveEnd(WdUnits.wdCharacter, count);

                lastChar = range.Text?.LastOrDefault();
            }
        }

        public static void ApplyFormattingFrom(this Range targetRange, [NotNull] Range sourceRange)
        {
            targetRange.Font.Reset();
            targetRange.Bold = sourceRange.Bold;
            targetRange.Italic = sourceRange.Italic;
            targetRange.Underline = sourceRange.Underline;
            targetRange.Font = sourceRange.Font.Duplicate;
        }

        public static Paragraph FirstOrDefaultParagraph(this Range range, string styleNameLocal)
        {
            return range?.Paragraphs.OfType<Paragraph>()
                        .FirstOrDefault(p => p.GetStyleNameLocal() == styleNameLocal);
        }

        /// <summary>
        /// Gets the first consecutive paragraphs of a given style in a given range.
        /// </summary>
        /// <param name="range">The range to be searched for paragraphs.</param>
        /// <param name="styleNameLocal">The localized name of the paragraph style to search for.</param>
        /// <returns>An enumeration of paragraphs with the given style.</returns>
        public static IEnumerable<Paragraph> FirstOrDefaultParagraphsByStyleName(
            this Range range,
            string styleNameLocal)
        {
            bool found = false;
            foreach (var paragraph in range.Paragraphs.OfType<Paragraph>())
            {
                if (paragraph.GetStyleNameLocal() == styleNameLocal)
                {
                    found = true;
                    yield return paragraph;
                }
                else if (found)
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Gets the first consecutive paragraphs of a given style in a given range.
        /// </summary>
        /// <param name="range">The range to be searched for paragraphs.</param>
        /// <param name="styleNameLocalPattern">
        /// A regular expression pattern to match the localized name
        /// of the paragraph style(s) to search for.
        /// </param>
        /// <returns>An enumeration of paragraphs with the given style.</returns>
        public static IEnumerable<Paragraph> FirstOrDefaultParagraphsByStyleNamePattern(
            this Range range,
            string styleNameLocalPattern)
        {
            bool found = false;
            foreach (var paragraph in range.Paragraphs.OfType<Paragraph>())
            {
                var style = LanguageIndependentStyle.FromParagraph(paragraph);
                if (Regex.IsMatch(
                        style?.NameNeutral ?? "",
                        styleNameLocalPattern,
                        RegexOptions.IgnoreCase))
                {
                    found = true;
                    yield return paragraph;
                }
                else if (found)
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Gets the first table with a given style in a given range.
        /// </summary>
        /// <param name="range">The range to be searched for tables.</param>
        /// <param name="styleNameNeutralPattern">
        /// A regular expression pattern to match the language independent name
        /// of the table style(s) to search for.
        /// </param>
        /// <returns>An Word table with the given style.</returns>
        public static Table FirstOrDefaultTableByStyleNamePattern(
            this Range range,
            string styleNameNeutralPattern)
        {
            foreach (var table in range.Tables.OfType<Table>())
            {
                var style = LanguageIndependentStyle.FromTable(table);
                if (Regex.IsMatch(style?.NameNeutral ?? "", styleNameNeutralPattern, RegexOptions.IgnoreCase))
                {
                    return table;
                }
            }

            return null;
        }

        public static void DeleteRangeAndContent(this Range range)
        {
            // tables cannot be deleted via Range.Delete but only via Table.Delete
            // select all tables in the current range but exclude tables that fully surround the current range
            var tablesToDelete = range
                                 .Tables
                                 .OfType<Table>()
                                 .Where(t => !(range.Start >= t.Range.Start && range.End < t.Range.End - 1))
                                 .ToList();

            foreach (var table in tablesToDelete)
            {
                table.Delete();
            }

            if (range.End - range.Start <= 0)
            {
                return;
            }

            if (range.NextParagraphRange().HasTables() && !range.PreviousParagraphRange().HasTables())
            {
                // NOTE: special handling when the brick is followed by a table. If we simply called range.Delete()
                //   an empty paragraph would remain which we don't want. When setting the range's Text to "", no
                //   empty paragraph remains. However, we also check if the brick is *between* two tables. In that
                //   we want the empty paragraph to remain in the document because otherwise the two tables
                //   would be merged, that's why we also check for !range.PreviousParagraphRange().HasTables()
                //   See also: EUROLOOK-986
                range.Text = "";
            }
            else
            {
                range.Delete();
            }
        }
    }
}
