﻿using System.Text.RegularExpressions;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;

namespace Eurolook.DocumentProcessing.Extensions
{
    public static class FieldExtensions
    {
        // NOTE: SEQ labels can be surrounded by quotes or not which is encountered for by the two alternatives in
        // this regex.
        private static readonly Regex SequenceIdentifierPattern =
            new Regex(@"(^\s*SEQ\s+""(?<sequenceIdentifier>[^""]+)"".*$|^\s*SEQ\s+(?<sequenceIdentifier>\S+?)\b.*$)");

        [CanBeNull]
        public static string GetSequenceIdentifier(this Field field)
        {
            // the sequence name may contain nested fields so we need to make sure to
            // get the values of these fields and not the field code
            var range = field.Code;
            range.TextRetrievalMode.IncludeFieldCodes = false;

            var match = SequenceIdentifierPattern.Match(range.Text ?? "");
            return match.Success ? match.Groups["sequenceIdentifier"].Value : null;
        }

        [CanBeNull]
        public static string GetBookmarkNameFromReference(this Field field)
        {
            if (field.Code.Text == null)
            {
                return null;
            }

            var regexRefField = new Regex(@"((?:_Ref|_tocEntry)[^ ""]+)", RegexOptions.IgnoreCase);
            var match = regexRefField.Match(field.Code.Text);

            return match.Success ? match.Groups[0].Value : null;
        }
    }
}
