﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.Extensions
{
    public static class OpenXmlPartExtensions
    {
        private static readonly Type[] TrackedRevisionsElements =
        {
            typeof(CellDeletion),
            typeof(CellInsertion),
            typeof(CellMerge),
            typeof(CustomXmlDelRangeEnd),
            typeof(CustomXmlDelRangeStart),
            typeof(CustomXmlInsRangeEnd),
            typeof(CustomXmlInsRangeStart),
            typeof(Deleted),
            typeof(DeletedFieldCode),
            typeof(DeletedMathControl),
            typeof(DeletedRun),
            typeof(DeletedText),
            typeof(Inserted),
            typeof(InsertedMathControl),
            typeof(InsertedMathControl),
            typeof(InsertedRun),
            typeof(MoveFrom),
            typeof(MoveFromRangeEnd),
            typeof(MoveFromRangeStart),
            typeof(MoveTo),
            typeof(MoveToRangeEnd),
            typeof(MoveToRangeStart),
            typeof(MoveToRun),
            typeof(NumberingChange),
            typeof(ParagraphMarkRunPropertiesChange),
            typeof(ParagraphPropertiesChange),
            typeof(RunPropertiesChange),
            typeof(SectionPropertiesChange),
            typeof(TableCellPropertiesChange),
            typeof(TableGridChange),
            typeof(TablePropertiesChange),
            typeof(TablePropertyExceptionsChange),
            typeof(TableRowPropertiesChange),
        };

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times", Justification = "Reviewed")]
        public static XDocument GetXDocument(this OpenXmlPart part)
        {
            var xdoc = part.Annotation<XDocument>();
            if (xdoc != null)
            {
                return xdoc;
            }

            using (var stream = part.GetStream())
            {
                using (var streamReader = new StreamReader(stream))
                {
                    using (var xr = XmlReader.Create(streamReader))
                    {
                        xdoc = XDocument.Load(xr);
                    }
                }
            }

            part.AddAnnotation(xdoc);
            return xdoc;
        }

        public static bool HasTrackedRevisions(this MainDocumentPart mainDocumentPart)
        {
            if (mainDocumentPart.HasPartTrackedRevisions())
            {
                return true;
            }

            var documentParts = new List<OpenXmlPart>();
            documentParts.AddRange(mainDocumentPart.HeaderParts);
            documentParts.AddRange(mainDocumentPart.FooterParts);
            documentParts.Add(mainDocumentPart.EndnotesPart);
            documentParts.Add(mainDocumentPart.FootnotesPart);

            return documentParts.Any(part => part.HasPartTrackedRevisions());
        }

        public static bool HasPartTrackedRevisions(this OpenXmlPart part)
        {
            return part?.RootElement?.Descendants().Any(element => TrackedRevisionsElements.Contains(element.GetType()))
                   ?? false;
        }
    }
}
