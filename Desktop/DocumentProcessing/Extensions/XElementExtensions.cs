﻿using System;
using System.Globalization;
using System.Xml.Linq;

namespace Eurolook.DocumentProcessing.Extensions
{
    public static class XElementExtensions
    {
        public static bool TryParseAttribute(this XElement element, string attributeName, out int value)
        {
            if (element == null)
            {
                throw new ArgumentNullException(nameof(element));
            }

            value = 0;

            string attributeValue = (string)element.Attribute(attributeName);
            if (attributeValue == null)
            {
                return false;
            }

            return int.TryParse(attributeValue, NumberStyles.Integer, CultureInfo.InvariantCulture, out value);
        }

        public static bool TryParseAttribute(this XElement element, string attributeName, out bool value)
        {
            if (element == null)
            {
                throw new ArgumentNullException(nameof(element));
            }

            value = false;

            string attributeValue = (string)element.Attribute(attributeName);
            if (attributeValue == null)
            {
                return false;
            }

            return bool.TryParse(attributeValue, out value);
        }
    }
}
