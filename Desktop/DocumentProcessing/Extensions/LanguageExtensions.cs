using System.Globalization;
using Microsoft.Office.Interop.Word;
using Language = Eurolook.Data.Models.Language;

namespace Eurolook.DocumentProcessing.Extensions
{
    public static class LanguageExtensions
    {
        public static WdLanguageID AsWdLanguageId(this Language language)
        {
            return (WdLanguageID)new CultureInfo(language.Locale).LCID;
        }
    }
}
