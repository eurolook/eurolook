using System.Xml.Linq;
using Eurolook.Data.Xml;
using Microsoft.Office.Core;

namespace Eurolook.DocumentProcessing.Extensions
{
    public static class CustomXMLPartExtensions
    {
        public static AuthorCustomXml ToAuthorCustomXml(this CustomXMLPart part)
        {
            return new AuthorCustomXml(XDocument.Parse(part.XML));
        }
    }
}
