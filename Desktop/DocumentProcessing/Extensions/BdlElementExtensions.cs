﻿using Eurolook.DocumentProcessing.BrickDefinitionLanguage;

namespace Eurolook.DocumentProcessing.Extensions
{
    public static class BdlElementExtensions
    {
        public static T FindAncestorOfType<T>(this BdlElement element)
            where T : BdlElement
        {
            if (element == null)
            {
                return default(T);
            }

            if (element.ParentElement is T)
            {
                return (T)element.ParentElement;
            }

            return FindAncestorOfType<T>(element.ParentElement);
        }
    }
}
