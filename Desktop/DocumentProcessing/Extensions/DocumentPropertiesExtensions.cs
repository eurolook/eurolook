using System;
using System.Linq;
using Microsoft.Office.Core;

namespace Eurolook.DocumentProcessing.Extensions
{
    public static class DocumentPropertiesExtensions
    {
        public static void Set(this DocumentProperties documentProperties, string name, string value)
        {
            documentProperties.Delete(name);
            documentProperties.Add(name, false, MsoDocProperties.msoPropertyTypeString, value, false);
        }

        public static void Delete(this DocumentProperties documentProperties, string name)
        {
            var property = documentProperties.Get(name);
            property?.Delete();
        }

        public static DocumentProperty Get(this DocumentProperties documentProperties, string name)
        {
            return documentProperties
                   .Cast<DocumentProperty>()
                   .FirstOrDefault(
                       prop => string.Equals(
                           prop.Name,
                           name,
                           StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
