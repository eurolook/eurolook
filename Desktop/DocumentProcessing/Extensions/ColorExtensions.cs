using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;

namespace Eurolook.DocumentProcessing.Extensions
{
    public static class ColorExtensions
    {
        private static readonly uint Rgb = 0X00;
        private static readonly uint Automatic = 0xFF;
        private static readonly uint System = 0x80;
        private static readonly uint ThemeLow = 0xD0;
        private static readonly uint ThemeHigh = 0xDF;

        /// <summary>
        /// Converts a WdColor value to a hexadecimal color string.
        /// </summary>
        /// <returns>A string in the format #RRGGBB.</returns>
        public static string ToHexString(this WdColor wdColor, ThemeColorScheme colorScheme)
        {
            return wdColor.ToHexString(colorScheme, "#000000");
        }

        /// <summary>
        /// Converts a WdColor value to a hexadecimal color string.
        /// </summary>
        /// <param name="wdColor">The wdColor value.</param>
        /// <param name="colorScheme">The ThemeColorScheme of the current document.</param>
        /// <param name="fallback">Fallback value for wdColorAutomatic and other negative values.</param>
        /// <returns>A string in the format #RRGGBB.</returns>
        public static string ToHexString(this WdColor wdColor, ThemeColorScheme colorScheme, string fallback)
        {
            uint colorValue = (uint)wdColor & 0xFFFFFF;
            uint colorType = (uint)wdColor >> 24;

            if (colorType == Rgb)
            {
                return ToHexString(colorValue);
            }

            if (colorType == Automatic || colorType == System)
            {
                return fallback;
            }

            if (colorType >= ThemeLow && colorType <= ThemeHigh)
            {
                uint schemeIndex = colorType & 0xF;
                return ThemeColorToRgb(schemeIndex, colorValue, colorScheme);
            }

            return fallback;
        }

        private static string ThemeColorToRgb(uint schemeIndex, uint colorValue, ThemeColorScheme colorScheme)
        {
            var colorSchemeIndex = ThemeIndexToColorSchemeIndex(schemeIndex);
            uint shade = (colorValue & 0xFF00) >> 8;
            uint tint = colorValue & 0xFF;

            var rgb = (uint)colorScheme.Colors(colorSchemeIndex).RGB;
            return ToHexString(rgb, tint, shade);
        }

        private static string ToHexString(uint color)
        {
            // Word Color strings are reversed, i.e. 00BBGGRR.
            uint red = color & 0xFF;
            color >>= 8;
            uint green = color & 0xFF;
            color >>= 8;
            uint blue = color & 0xFF;

            return "#" + (red * 0x10000 + green * 0x100 + blue).ToString("x6");
        }

        private static string ToHexString(uint color, uint tint, uint shade)
        {
            uint red = color & 0xFF;
            color >>= 8;
            uint green = color & 0xFF;
            color >>= 8;
            uint blue = color & 0xFF;

            red = (red * shade) / 0xFF;
            green = (green * shade) / 0xFF;
            blue = (blue * shade) / 0xFF;

            red += ((0xff - red) * (0xFF - tint)) / 0xFF;
            green += ((0xff - green) * (0xFF - tint)) / 0xFF;
            blue += ((0xff - blue) * (0xFF - tint)) / 0xFF;

            return "#" + (red * 0x10000 + green * 0x100 + blue).ToString("x6");
        }

        private static MsoThemeColorSchemeIndex ThemeIndexToColorSchemeIndex(uint themeIndex)
        {
            switch ((WdThemeColorIndex)themeIndex)
            {
                case WdThemeColorIndex.wdThemeColorMainDark1:
                    return MsoThemeColorSchemeIndex.msoThemeDark1;
                case WdThemeColorIndex.wdThemeColorMainLight1:
                    return MsoThemeColorSchemeIndex.msoThemeLight1;
                case WdThemeColorIndex.wdThemeColorMainDark2:
                    return MsoThemeColorSchemeIndex.msoThemeDark2;
                case WdThemeColorIndex.wdThemeColorMainLight2:
                    return MsoThemeColorSchemeIndex.msoThemeLight2;
                case WdThemeColorIndex.wdThemeColorAccent1:
                    return MsoThemeColorSchemeIndex.msoThemeAccent1;
                case WdThemeColorIndex.wdThemeColorAccent2:
                    return MsoThemeColorSchemeIndex.msoThemeAccent2;
                case WdThemeColorIndex.wdThemeColorAccent3:
                    return MsoThemeColorSchemeIndex.msoThemeAccent3;
                case WdThemeColorIndex.wdThemeColorAccent4:
                    return MsoThemeColorSchemeIndex.msoThemeAccent4;
                case WdThemeColorIndex.wdThemeColorAccent5:
                    return MsoThemeColorSchemeIndex.msoThemeAccent5;
                case WdThemeColorIndex.wdThemeColorAccent6:
                    return MsoThemeColorSchemeIndex.msoThemeAccent6;
                case WdThemeColorIndex.wdThemeColorHyperlink:
                    return MsoThemeColorSchemeIndex.msoThemeHyperlink;
                case WdThemeColorIndex.wdThemeColorHyperlinkFollowed:
                    return MsoThemeColorSchemeIndex.msoThemeFollowedHyperlink;
                case WdThemeColorIndex.wdThemeColorBackground1:
                    return MsoThemeColorSchemeIndex.msoThemeLight1;
                case WdThemeColorIndex.wdThemeColorText1:
                    return MsoThemeColorSchemeIndex.msoThemeDark1;
                case WdThemeColorIndex.wdThemeColorBackground2:
                    return MsoThemeColorSchemeIndex.msoThemeLight2;
                case WdThemeColorIndex.wdThemeColorText2:
                    return MsoThemeColorSchemeIndex.msoThemeDark2;
                case WdThemeColorIndex.wdNotThemeColor:
                default:
                    break; // should not happen
            }

            return MsoThemeColorSchemeIndex.msoThemeDark1;
        }
    }
}
