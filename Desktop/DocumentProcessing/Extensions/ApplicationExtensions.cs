using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Eurolook.Common.Log;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;

namespace Eurolook.DocumentProcessing.Extensions
{
    public static class ApplicationExtensions
    {
        [CanBeNull]
        public static Document GetActiveDocument(this Application app)
        {
            if (app.Documents.Count > 0)
            {
                try
                {
                    return app.ActiveDocument;
                }
                catch (Exception)
                {
                    return null;
                }
            }

            return null;
        }

        [CanBeNull]
        public static Window GetActiveWindow(this Application app)
        {
            if (app.Windows.Count > 0)
            {
                try
                {
                    return app.ActiveWindow;
                }
                catch (Exception)
                {
                    return null;
                }
            }

            return null;
        }

        [CanBeNull]
        public static Selection GetSelection(this Application application)
        {
            try
            {
                if (application == null)
                {
                    return null;
                }

                if (application.Documents.Count > 0)
                {
                    return application.Selection;
                }
            }
            catch (Exception)
            {
                // ignore any errors and return null
            }

            return null;
        }

        public static bool IsProtectedView(this Application app)
        {
            return app.ActiveProtectedViewWindow != null;
        }

        public static bool IsCompareView(this Application app)
        {
            var viewType = GetCurrentViewType(app);
            return viewType == WdViewType.wdWebView;
        }

        public static bool IsReadingView(this Application app)
        {
            var viewType = GetCurrentViewType(app);
            return viewType == WdViewType.wdWebView || viewType == WdViewType.wdReadingView;
        }

        public static WdViewType? GetCurrentViewType(this Application app)
        {
            var activeWindow = app.GetActiveWindow();
            if (activeWindow == null || activeWindow.View == null)
            {
                return null;
            }

            if (app.Documents.Count == 0)
            {
                return null;
            }

            return activeWindow.View.Type;
        }

        /// <summary>
        /// A safe way to enumerate the windows of the Word application.
        /// </summary>
        /// <param name="application">A Word <see cref="Application" /> object.</param>
        /// <returns>An enumerable of all application windows.</returns>
        public static IEnumerable<Window> WindowsEnumerable(this Application application)
        {
            if (application == null)
            {
                yield break;
            }

            for (int i = 1; i <= application.Windows.Count; i++)
            {
                Window window = null;
                try
                {
                    window = application.Windows[i];
                }
                catch (COMException ex) when (ex.IsWordHasEncounteredAProblemException())
                {
                    LogManager.GetLogger().Warn(ex.Message, ex);
                }

                if (window != null)
                {
                    yield return window;
                }
            }
        }
    }
}
