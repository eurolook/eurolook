﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using DocumentFormat.OpenXml.Packaging;
using Eurolook.Common;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using JetBrains.Annotations;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.DocumentProcessing.Extensions
{
    public static class DocumentExtensions
    {
        /// <summary>
        /// Updates the bound content controls in the document and writes the document content into the custom XML.
        /// </summary>
        /// <param name="document">The document where the binding is to be updated.</param>
        /// <remarks>
        /// The binding of content controls is updated only as soon as the selection leaves the content
        /// control. This method temporarily changes the selection so that the update is triggered.
        /// </remarks>
        public static void UpdateCustomXmlBindings(this Document document)
        {
            var selection = document.Application.GetSelection();
            if (selection?.ParentContentControl != null)
            {
                var originalSelection = selection.Range;
                bool screenUpdating = document.Application.ScreenUpdating;
                try
                {
                    document.Application.ScreenUpdating = false;
                    document.Range().Select();
                }
                finally
                {
                    document.Application.ScreenUpdating = screenUpdating;
                    originalSelection.Select();
                }
            }
        }

        public static void SaveCopyAs(this Document doc, string path)
        {
            // ReSharper disable once SuspiciousTypeConversion.Global
            var persistFile = (IPersistFile)doc;
            persistFile.Save(path, false);

            var fileInfo = new FileInfo(path);
            if (fileInfo.HasCompoundDocumentHeader())
            {
                // In co-authoring scenarios, the document saved via the IPersistFile interface may be
                // a compound document container with a proprietary (.asd) file format inside similar to doc format.
                //
                // As this format can neither be opened by Word directly, nor be used with the Open XML SDK
                // we save the document as Open XML by accessing the Document.WordOpenXML property.
                //
                // This approach is used as a fallback only because it is less efficient compared to calling
                // IPersistFile.Save.
                LogManager.GetLogger().Warn(
                    $"Document '{doc.FullName}' was saved in compound document format. "
                    + "Creating Open XML package using fallback.");

                using (var package = WordprocessingDocument.FromFlatOpcString(doc.WordOpenXML))
                {
                    using (var savedPackage = package.SaveAs(path))
                    {
                        savedPackage.Close();
                    }
                }
            }
        }

        public static void AttachTemplate(this Document document, string templatePath)
        {
            // ReSharper disable once UseIndexedProperty
            if (!File.Exists(templatePath)
                || document.IsTemplate()
                || document.get_AttachedTemplate().FullName == templatePath)
            {
                return;
            }

            bool isSaved = document.Saved;

            // ReSharper disable once UseIndexedProperty
            document.set_AttachedTemplate(templatePath);
            document.Saved = isSaved;
        }

        public static bool IsTemplate(this Document document)
        {
            switch (document.SaveFormat)
            {
                case (int)WdSaveFormat.wdFormatTemplate:
                case (int)WdSaveFormat.wdFormatXMLTemplate:
                case (int)WdSaveFormat.wdFormatXMLTemplateMacroEnabled:
                case (int)WdSaveFormat.wdFormatFlatXMLTemplate:
                case (int)WdSaveFormat.wdFormatFlatXMLTemplateMacroEnabled:
                    return true;

                default:
                    return false;
            }
        }

        /// <summary>
        /// Gets a flag indicating whether a document is locked for editing by another user.
        /// </summary>
        /// <param name="document">The <see cref="Document" /> to be checked.</param>
        /// <returns>A flag indicating whether a document is locked for editing by another user.</returns>
        /// <remarks>
        /// Word can open files as "read-only" in two modes:
        /// <list type="bullet">
        ///     <item>
        ///     Editable: Files that have a readonly attribute or are stored at a readonly location
        ///     are opened "read-only" but are editable
        ///     </item>
        ///     <item>
        ///     Locked for editing by another user: Files that are opened by another user
        ///     are opened "read-only" and cannot be edited (all Ribbon commands are disabled).
        ///     </item>
        /// </list>
        /// This method checks a property on the document that behaves differently in the two modes, i.e. the property
        /// returns true/false if the document can be edited but throws if the document is locked for editing.
        /// </remarks>
        public static bool IsLockedForEditing(this Document document)
        {
            try
            {
                // ReSharper disable once UnusedVariable
#pragma warning disable S1481 // Unused local variables should be removed
                bool dummy = document.ReadOnlyRecommended;
#pragma warning restore S1481

                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }

        /// <summary>
        /// Tells safely without throwing an exception if we can add content controls using range.ContentControls.Add
        /// The range.ContentControls.Add() method throws a NotSupportedException when the document is in binary/RTF format.
        /// </summary>
        /// <param name="document">The document to check.</param>
        /// <returns>True, if we can add content controls, false otherwise.</returns>
        public static bool HasContentControlSupport(this Document document)
        {
            switch ((WdSaveFormat)document.SaveFormat)
            {
                case WdSaveFormat.wdFormatDocument:
                case WdSaveFormat.wdFormatTemplate:
                case WdSaveFormat.wdFormatXML:
                case WdSaveFormat.wdFormatRTF:
                    return false;

                case WdSaveFormat.wdFormatText:
                case WdSaveFormat.wdFormatTextLineBreaks:
                case WdSaveFormat.wdFormatDOSText:
                case WdSaveFormat.wdFormatDOSTextLineBreaks:
                case WdSaveFormat.wdFormatUnicodeText:
                case WdSaveFormat.wdFormatHTML:
                case WdSaveFormat.wdFormatWebArchive:
                case WdSaveFormat.wdFormatFilteredHTML:
                case WdSaveFormat.wdFormatXMLDocument:
                case WdSaveFormat.wdFormatXMLDocumentMacroEnabled:
                case WdSaveFormat.wdFormatXMLTemplate:
                case WdSaveFormat.wdFormatXMLTemplateMacroEnabled:
                case WdSaveFormat.wdFormatDocumentDefault:
                case WdSaveFormat.wdFormatPDF:
                case WdSaveFormat.wdFormatXPS:
                case WdSaveFormat.wdFormatFlatXML:
                case WdSaveFormat.wdFormatFlatXMLMacroEnabled:
                case WdSaveFormat.wdFormatFlatXMLTemplate:
                case WdSaveFormat.wdFormatFlatXMLTemplateMacroEnabled:
                case WdSaveFormat.wdFormatOpenDocumentText:
                    return true;
                default:
                    return true;
            }
        }

        public static Range GetStoryRange(this Document document, WdStoryType storyType)
        {
            try
            {
                return document.StoryRanges.Cast<Range>()
                               .FirstOrDefault(storyRange => storyRange.StoryType == storyType);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static bool TryGetCustomDocumentProperty(this Document document, string name, out string result)
        {
            result = null;
            bool success = TryGetCustomDocumentProperty(document, name, out dynamic property);
            if (success)
            {
                result = property.Value.ToString();
            }

            return success;
        }

        public static bool TryGetCustomDocumentProperty(this Document document, string name, out DateTime result)
        {
            result = DateTime.UtcNow;
            bool success = TryGetCustomDocumentProperty(document, name, out dynamic property)
                           && property.Type == (int)MsoDocProperties.msoPropertyTypeDate;
            if (success)
            {
                result = property.Value;
            }

            return success;
        }

        public static bool TryGetBuiltInDocumentProperty(this Document document, string name, out string result)
        {
            result = null;

            bool success = TryGetBuiltInDocumentProperty(document, name, out dynamic property);
            if (success)
            {
                result = property.Value.ToString();
            }

            return success;
        }

        public static bool TryGetBuiltInDocumentProperty(this Document document, string name, out DateTime result)
        {
            result = DateTime.UtcNow;

            bool success = TryGetBuiltInDocumentProperty(document, name, out dynamic property)
                           && property.Type == (int)MsoDocProperties.msoPropertyTypeDate;
            if (success)
            {
                result = property.Value;
            }

            return success;
        }

        /// <summary>
        /// Set a custom document property to the specified value.
        /// </summary>
        /// <typeparam name="T">The type of the property value.</typeparam>
        /// <param name="doc">The document where the custom property is set.</param>
        /// <param name="name">The name of the property.</param>
        /// <param name="value">The typed value of the property.</param>
        /// <remarks>This method must be called on the main thread.</remarks>
        public static void SetCustomDocumentProperty<T>([NotNull] this Document doc, string name, T value)
        {
            try
            {
                doc.CustomDocumentProperties.Add(name, value.ToString());
            }
            catch (Exception)
            {
                doc.CustomDocumentProperties[name].Value = value.ToString();
            }
        }

        public static bool TryGetVariableAsEnum<TEnum>([NotNull] this Document doc, string name, out TEnum result)
            where TEnum : struct
        {
            result = default(TEnum);

            try
            {
                if (doc.Variables.Exists(name))
                {
                    string value = doc.Variables[name].Value;
                    if (Enum.TryParse(value, out result))
                    {
                        return true;
                    }
                }
            }
            catch (COMException)
            {
                // TryGet should not throw
            }

            return false;
        }

        public static bool TryGetVariableAsGuid([NotNull] this Document doc, string name, out Guid result)
        {
            result = Guid.Empty;
            try
            {
                if (doc.TryGetVariable(name, out string stringValue))
                {
                    return Guid.TryParse(stringValue, out result);
                }
            }
            catch (COMException)
            {
                // TryGet should not throw
            }

            return false;
        }

        public static bool TryGetVariable(this Document doc, string name, out string result)
        {
            result = null;

            try
            {
                if (doc.Variables.Exists(name))
                {
                    result = doc.Variables[name].Value;
                    return true;
                }
            }
            catch (COMException)
            {
                // TryGet should not throw
            }

            return false;
        }

        public static bool Exists(this Variables variables, string name)
        {
            return variables.OfType<Variable>()
                            .Any(v => v.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }

        public static void SetVariable<T>(this Document doc, string name, T value)
        {
            try
            {
                doc.Variables.Add(name, value.ToString());
            }
            catch (Exception)
            {
                doc.Variables[name].Value = value.ToString();
            }
        }

        public static void DeleteVariable(this Document doc, string name)
        {
            if (doc.Variables.Exists(name))
            {
                doc.Variables[name].Delete();
            }
        }

        public static IEnumerable<ContentControl> GetAllContentControls(this Document document)
        {
            return document.AllStoryRanges().SelectMany(r => r.ContentControls.OfType<ContentControl>());
        }

        public static IEnumerable<BrickContainer> GetAllBrickContainers(this Document document)
        {
            return GetAllContentControls(document).Select(cc => cc.GetBrickContainer()).Where(info => info != null);
        }

        public static BrickContainer GetBrickContainer(this Document document, Guid brickId)
        {
            return document.GetAllBrickContainers().FirstOrDefault(c => c.Id == brickId);
        }

        public static void CopyStylesFromDocumentModel(
            this Document document,
            List<string> stylesToCopy,
            DocumentModel source,
            DocumentModelLanguage sourceLanguage = null)
        {
            using (var tempFile = new TemporaryFile(".dotx"))
            {
                try
                {
                    File.WriteAllBytes(tempFile.FullName, sourceLanguage?.Template ?? source.Template);

                    var wordApp = document.Application;
                    var templateDocument = wordApp.Documents.Open(
                        tempFile.FullName,
                        ReadOnly: true,
                        AddToRecentFiles: false,
                        Visible: false);

                    try
                    {
                        foreach (string styleName in stylesToCopy)
                        {
                            if (!LanguageIndependentStyle.IsStyleAvailable(styleName, templateDocument))
                            {
                                continue;
                            }

                            wordApp.OrganizerCopy(
                                templateDocument.FullName,
                                document.FullName,
                                styleName,
                                WdOrganizerObject.wdOrganizerObjectStyles);
                        }
                    }
                    finally
                    {
                        // ReSharper disable once RedundantCast
                        ((_Document)templateDocument).Close();
                    }
                }
                catch
                {
                    // TryGet should not throw
                }
            }
        }

        internal static IEnumerable<Range> AllStoryRanges(this Document document)
        {
            foreach (var storyRange in document.StoryRanges.OfType<Range>())
            {
                var range = storyRange;
                yield return range;

                while ((range = range.NextStoryRange) != null)
                {
                    yield return range;
                }
            }
        }

        /// <summary>
        /// Applies an action to all ranges of a document.
        /// </summary>
        /// <param name="document">The document to apply the action to.</param>
        /// <param name="action">The action to be applied to each range.</param>
        /// <remarks>This method needs to be internal.</remarks>
        internal static void ApplyToAllStoryRanges(this Document document, Action<Range> action)
        {
            foreach (var storyRange in document.StoryRanges.OfType<Range>())
            {
                var range = storyRange;
                action(range);

                while ((range = range.NextStoryRange) != null)
                {
                    action(range);
                }
            }
        }

        private static bool TryGetCustomDocumentProperty(this Document document, string name, out dynamic result)
        {
            result = null;

            // NOTE: We don't cast to `DocumentProperties` here because this results in the following error
            // on a non-UI thread:
            //      This operation failed because the QueryInterface call on the COM component for the
            //      interface with IID '{2DF8D04E-5BFA-101B-BDE5-00AA0044DE52}' failed due to the following error:
            //      No such interface supported (Exception from HRESULT: 0x80004002 (E_NOINTERFACE)).'
            // Anything (also the returned result) needs to be dynamic to avoid the problem.
            var properties = document.CustomDocumentProperties;
            if (properties == null)
            {
                return false;
            }

            foreach (var customDocumentProperty in properties)
            {
                if (string.Equals(customDocumentProperty.Name, name, StringComparison.OrdinalIgnoreCase))
                {
                    result = customDocumentProperty;
                    return true;
                }
            }

            return false;
        }

        private static bool TryGetBuiltInDocumentProperty(this Document document, string name, out dynamic result)
        {
            result = null;

            // accessing the BuiltInDocumentProperties property may modify the Saved state of the document.
            bool isSaved = document.Saved;
            try
            {
                // NOTE: We don't cast to `DocumentProperties` here because this results in the following error
                // on a non-UI thread:
                //      This operation failed because the QueryInterface call on the COM component for the
                //      interface with IID '{2DF8D04E-5BFA-101B-BDE5-00AA0044DE52}' failed due to the following error:
                //      No such interface supported (Exception from HRESULT: 0x80004002 (E_NOINTERFACE)).'
                // Anything (also the returned result) needs to be dynamic to avoid the problem.
                var properties = document.BuiltInDocumentProperties;
                if (properties == null)
                {
                    return false;
                }

                foreach (var documentProperty in properties)
                {
                    if (string.Equals(documentProperty.Name, name, StringComparison.OrdinalIgnoreCase))
                    {
                        result = documentProperty;
                        return true;
                    }
                }
            }
            finally
            {
                document.Saved = isSaved;
            }

            return false;
        }
    }
}
