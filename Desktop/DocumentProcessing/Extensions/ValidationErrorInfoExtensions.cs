using DocumentFormat.OpenXml.Validation;

namespace Eurolook.DocumentProcessing.Extensions
{
    public static class ValidationErrorInfoExtensions
    {
        public static string ToDisplayString(this ValidationErrorInfo validationErrorInfo)
        {
            return
                $"{validationErrorInfo.ErrorType} Error"
                + $"\tPart: {validationErrorInfo.Part?.Uri}"
                + $"\tPath: {(validationErrorInfo.Path != null ? validationErrorInfo.Path.XPath : "")}"
                + $"\tRelated Node Type: {validationErrorInfo.RelatedNode}"
                + $"\tRelated Part: {validationErrorInfo.RelatedPart?.Uri}"
                + $"\tNode: {validationErrorInfo.Node?.LocalName}"
                + $"\tDescription: {validationErrorInfo.Description}";
        }
    }
}
