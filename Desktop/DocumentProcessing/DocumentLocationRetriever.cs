﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Eurolook.DocumentProcessing.Extensions;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.DocumentProcessing
{
    public class DocumentLocationRetriever : IDocumentLocationRetriever
    {
        private readonly List<DocumentLocationRetriever> _retrievers = new List<DocumentLocationRetriever>();

        private Func<Range, DocumentLocationResult> SearchFunc { get; set; }

        public DocumentLocationRetriever ByBrickId(Guid brickId)
        {
            _retrievers.Clear();
            _retrievers.Add(CreateByBrickId(brickId));
            return this;
        }

        public DocumentLocationRetriever ByContentControlTitle(string title)
        {
            var pattern = $"^{Regex.Escape(title)}$";
            return ByContentControlTitlePattern(pattern);
        }

        public DocumentLocationRetriever ByContentControlTitlePattern(string pattern)
        {
            _retrievers.Clear();
            _retrievers.Add(CreateByContentControlTitle(pattern));
            return this;
        }

        public DocumentLocationRetriever ByContentControlTag(string tag)
        {
            var pattern = $"^{Regex.Escape(tag)}$";
            return ByContentControlTagPattern(pattern);
        }

        public DocumentLocationRetriever ByContentControlTagPattern(string pattern)
        {
            _retrievers.Clear();
            _retrievers.Add(CreateByContentControlTag(pattern));
            return this;
        }

        public DocumentLocationRetriever ByContentControlXPathBinding(string xPathBinding)
        {
            var pattern = $"^{Regex.Escape(xPathBinding)}$";
            return ByContentControlXPathBindingPattern(pattern);
        }

        public DocumentLocationRetriever ByContentControlXPathBindingPattern(string pattern)
        {
            _retrievers.Clear();
            _retrievers.Add(CreateByContentControlXPathBinding(pattern));
            return this;
        }

        public DocumentLocationRetriever ByParagraphStyle(string styleName)
        {
            _retrievers.Clear();
            _retrievers.Add(CreateByParagraphStyle(styleName));
            return this;
        }

        public DocumentLocationRetriever ByParagraphStylePattern(string pattern)
        {
            _retrievers.Clear();
            _retrievers.Add(CreateByParagraphStylePattern(pattern));
            return this;
        }

        public DocumentLocationRetriever ByTableStylePattern(string pattern)
        {
            _retrievers.Clear();
            _retrievers.Add(CreateByTableStylePattern(pattern));
            return this;
        }

        public DocumentLocationRetriever ThenByBrickId(Guid brickId)
        {
            _retrievers.Add(CreateByBrickId(brickId));
            return this;
        }

        public DocumentLocationRetriever ThenByContentControlTitle(string title)
        {
            var pattern = $"^{Regex.Escape(title)}$";
            return ThenByContentControlTitlePattern(pattern);
        }

        public DocumentLocationRetriever ThenByContentControlTitlePattern(string pattern)
        {
            _retrievers.Add(CreateByContentControlTitle(pattern));
            return this;
        }

        public DocumentLocationRetriever ThenByContentControlTag(string tag)
        {
            var pattern = $"^{Regex.Escape(tag)}$";
            return ThenByContentControlTagPattern(pattern);
        }

        public DocumentLocationRetriever ThenByContentControlTagPattern(string pattern)
        {
            _retrievers.Add(CreateByContentControlTag(pattern));
            return this;
        }

        public DocumentLocationRetriever ThenByContentControlXPathBinding(string xPathBinding)
        {
            var pattern = $"^{Regex.Escape(xPathBinding)}$";
            return ThenByContentControlXPathBindingPattern(pattern);
        }

        public DocumentLocationRetriever ThenByContentControlXPathBindingPattern(string pattern)
        {
            _retrievers.Add(CreateByContentControlXPathBinding(pattern));
            return this;
        }

        public DocumentLocationRetriever ThenByParagraphStyle(string pattern)
        {
            _retrievers.Add(CreateByParagraphStyle(pattern));
            return this;
        }

        public DocumentLocationRetriever ThenByParagraphStylePattern(string pattern)
        {
            _retrievers.Add(CreateByParagraphStylePattern(pattern));
            return this;
        }

        public DocumentLocationRetriever ThenByTableStylePattern(string pattern)
        {
            _retrievers.Add(CreateByTableStylePattern(pattern));
            return this;
        }

        public DocumentLocationResult Retrieve(Range searchRange)
        {
            foreach (var documentLocationRetriever in _retrievers)
            {
                var result = documentLocationRetriever.ApplySearchFunc(searchRange);
                if (result.Found)
                {
                    return result;
                }
            }

            return new DocumentLocationResult
            {
                SearchRange = searchRange,
            };
        }

        private static DocumentLocationRetriever CreateByBrickId(Guid brickId)
        {
            return new DocumentLocationRetriever
            {
                SearchFunc = r =>
                             {
                                 var contentControl = r.ContentControls.OfType<ContentControl>()
                                                       .Select(cc => cc.GetBrickContainer())
                                                       .Where(info => info != null)
                                                       .FirstOrDefault(c => c.Id == brickId)?.ContentControl;

                                 return new DocumentLocationResult
                                 {
                                     SearchRange = r,
                                     ContentControl = contentControl,
                                     Range = contentControl?.Range,
                                 };
                             },
            };
        }

        private static DocumentLocationRetriever CreateByContentControlTitle(string pattern)
        {
            return new DocumentLocationRetriever
            {
                SearchFunc = r =>
                             {
                                 var contentControl = r.ContentControls.OfType<ContentControl>()
                                                       .FirstOrDefault(
                                                           cc => Regex.IsMatch(
                                                               cc.Title ?? "",
                                                               pattern,
                                                               RegexOptions.IgnoreCase));

                                 return new DocumentLocationResult
                                 {
                                     SearchRange = r,
                                     ContentControl = contentControl,
                                     Range = contentControl?.Range,
                                 };
                             },
            };
        }

        private static DocumentLocationRetriever CreateByContentControlTag(string pattern)
        {
            return new DocumentLocationRetriever
            {
                SearchFunc = r =>
                             {
                                 var contentControl = r.ContentControls.OfType<ContentControl>()
                                                       .FirstOrDefault(
                                                           cc => Regex.IsMatch(
                                                               cc.Tag ?? "",
                                                               pattern,
                                                               RegexOptions.IgnoreCase));

                                 return new DocumentLocationResult
                                 {
                                     SearchRange = r,
                                     ContentControl = contentControl,
                                     Range = contentControl?.Range,
                                 };
                             },
            };
        }

        private static DocumentLocationRetriever CreateByContentControlXPathBinding(string pattern)
        {
            return new DocumentLocationRetriever
            {
                SearchFunc = r =>
                             {
                                 var contentControl = r.ContentControls.OfType<ContentControl>()
                                                       .FirstOrDefault(
                                                           cc => cc.XMLMapping.XPath != null
                                                                 && Regex.IsMatch(
                                                                     cc.XMLMapping.XPath ?? "",
                                                                     pattern,
                                                                     RegexOptions.IgnoreCase));

                                 return new DocumentLocationResult
                                 {
                                     SearchRange = r,
                                     ContentControl = contentControl,
                                     Range = contentControl?.Range,
                                 };
                             },
            };
        }

        private static DocumentLocationRetriever CreateByParagraphStyle(string styleName)
        {
            return new DocumentLocationRetriever
            {
                SearchFunc = r =>
                             {
                                 var style = LanguageIndependentStyle.FromNeutralStyleName(styleName, r.Document);
                                 if (style == null)
                                 {
                                     return null;
                                 }

                                 var paragraphs = r.FirstOrDefaultParagraphsByStyleName(style.NameLocal).ToList();
                                 if (!paragraphs.Any())
                                 {
                                     return null;
                                 }

                                 var range = paragraphs.First().Range;
                                 range.End = paragraphs.Last().Range.End;

                                 return new DocumentLocationResult
                                 {
                                     SearchRange = r,
                                     ContentControl = null,
                                     Range = range,
                                 };
                             },
            };
        }

        private static DocumentLocationRetriever CreateByParagraphStylePattern(string pattern)
        {
            return new DocumentLocationRetriever
            {
                SearchFunc = r =>
                             {
                                 var paragraphs = r.FirstOrDefaultParagraphsByStyleNamePattern(pattern).ToList();
                                 if (!paragraphs.Any())
                                 {
                                     return null;
                                 }

                                 var range = paragraphs.First().Range;
                                 range.End = paragraphs.Last().Range.End;

                                 return new DocumentLocationResult
                                 {
                                     SearchRange = r,
                                     ContentControl = null,
                                     Range = range,
                                 };
                             },
            };
        }

        private static DocumentLocationRetriever CreateByTableStylePattern(string pattern)
        {
            return new DocumentLocationRetriever
            {
                SearchFunc = r =>
                             {
                                 var table = r.FirstOrDefaultTableByStyleNamePattern(pattern);
                                 if (table == null)
                                 {
                                     return null;
                                 }

                                 return new DocumentLocationResult
                                 {
                                     SearchRange = r,
                                     ContentControl = null,
                                     Range = table.Range,
                                 };
                             },
            };
        }

        [NotNull]
        private DocumentLocationResult ApplySearchFunc(Range searchRange)
        {
            return SearchFunc(searchRange) ?? new DocumentLocationResult
            {
                SearchRange = searchRange,
            };
        }
    }
}
