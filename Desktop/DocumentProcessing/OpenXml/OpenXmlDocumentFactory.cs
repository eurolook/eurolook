﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Eurolook.Common;
using Eurolook.Common.Log;
using Eurolook.Data.Compatibility;
using Eurolook.Data.Models;
using Eurolook.Data.TermStore;
using Eurolook.Data.Xml;
using Eurolook.DocumentProcessing.DocumentCreation;
using Eurolook.DocumentProcessing.Extensions;
using Ap = DocumentFormat.OpenXml.ExtendedProperties;
using M = DocumentFormat.OpenXml.Math;
using Vt = DocumentFormat.OpenXml.VariantTypes;

namespace Eurolook.DocumentProcessing.OpenXml
{
    [SuppressMessage("SonarQube", "S3220:Method calls should not resolve ambiguously to overloads with \"params\"", Justification = "Reviewed")]
    public class OpenXmlDocumentFactory : ICanLog, IDocumentFactory
    {
        private readonly IBrickVersionCompatibilityTester _brickVersionCompatibilityTester;
        private readonly IAuthorManager _authorManager;
        private readonly ITermStoreCache _termStoreCache;

        public OpenXmlDocumentFactory(
            IBrickVersionCompatibilityTester brickVersionCompatibilityTester,
            IAuthorManager authorManager,
            ITermStoreCache termStoreCache)
        {
            _brickVersionCompatibilityTester = brickVersionCompatibilityTester;
            _authorManager = authorManager;
            _termStoreCache = termStoreCache;
        }

        public async Task CreateDocument(string file, DocumentCreationInfo documentCreationInfo)
        {
            using (var fs = new FileStream(file, FileMode.Create, FileAccess.ReadWrite))
            {
                await CreateDocument(fs, documentCreationInfo);
            }
        }

        public bool CanHandle(DocumentCreationInfo documentCreationInfo)
        {
            return !documentCreationInfo.IsTemplateFromTemplateStore;
        }

        public string FileExtension => ".dotx";

        private static bool UserDeselectedBrick(IDictionary<Guid, bool> userBrickChoices, Guid brickId)
        {
            if (userBrickChoices == null)
            {
                return false;
            }

            if (userBrickChoices.ContainsKey(brickId) && !userBrickChoices[brickId])
            {
                return true;
            }

            return !userBrickChoices.ContainsKey(brickId);
        }

        private static bool TryGetStyleNameFromStyleId(string styleId, out string styleName, StylesPart styles)
        {
            styleName =
                (styles.Styles?.Descendants<Style>() ?? Array.Empty<Style>())
                .FirstOrDefault(s => s.StyleId == styleId)?.StyleName?.Val;
            return styleName != null;
        }

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times", Justification = "Reviewed")]
        private async Task CreateDocument(Stream target, DocumentCreationInfo documentCreationInfo)
        {
            if (documentCreationInfo.DocumentModel == null || documentCreationInfo.DocumentModel.Template == null)
            {
                throw new EurolookConfigurationException("Cannot create a document without a template.");
            }

            // Create an expandable MemoryStream (default constructor).
            // This is required because we add content to the WordprocessingDocument.
            byte[] documentBytes;
            using (var ms = new MemoryStream())
            {
                var documentModelLanguage = documentCreationInfo.DocumentModel.DocumentLanguages.FirstOrDefault(
                    dml => dml.DocumentLanguageId == documentCreationInfo.Language.Id);
                var template = documentModelLanguage?.Template ?? documentCreationInfo.DocumentModel.Template;

                // write the template in the stream
                await ms.WriteAsync(template, 0, template.Length);

                // and open it via OpenXML SDK if it's not a classic document
                if (documentCreationInfo.DocumentModel.IsClassicDocument == null
                    || documentCreationInfo.DocumentModel.IsClassicDocument == false)
                {
                    using (var dotx = WordprocessingDocument.Open(ms, true))
                    {
                        // remove any macros or customizations from the document
                        dotx.CleanupDocument();

                        var corePropertiesPart = dotx.GetOrCreateCoreFilePropertiesPart();
                        GenerateCoreFilePropertiesPart(corePropertiesPart, documentCreationInfo);

                        var extendedFilePropertiesPart = dotx.GetOrCreateExtendedFilePropertiesPart();
                        GenerateExtendedFilePropertiesPart(extendedFilePropertiesPart, documentCreationInfo);

                        // setup EL stuff
                        SetupMainDocument(dotx, documentCreationInfo);

                        // add the auto bricks
                        var converter = new BrickConverter(
                            new OpenXmlConverterContext(dotx, documentCreationInfo.DocumentModel, _authorManager.GetAuthorCustomXmlStoreInfos(dotx))
                            {
                                Language = documentCreationInfo.Language,
                                ResourceResolver = documentCreationInfo.ResourceResolver,
                                Options = new OpenXmlConversionOptions
                                {
                                    DocumentMode = WordprocessingDocumentMode.MainDocument,
                                    CreateTableOfContentsContainer = false,
                                },
                                CurrentPart = dotx.MainDocumentPart,
                                TermStoreCache = _termStoreCache,
                            });
                        SetContent(dotx, documentCreationInfo, converter);
                    }
                }

                // write the (modified) document bytes
                documentBytes = ms.ToArray();
            }

            await target.WriteAsync(documentBytes, 0, documentBytes.Length);
        }

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times", Justification = "Reviewed")]
        private void SetupMainDocument(WordprocessingDocument docx, DocumentCreationInfo documentCreationInfo)
        {
            // set custom doc props
            var customDocProps = new Dictionary<string, string>();
            if (!string.IsNullOrWhiteSpace(documentCreationInfo.DocumentModel.EL4DocType))
            {
                customDocProps["EurolookVersion"] = "10.0";
                customDocProps["ELDocType"] = documentCreationInfo.DocumentModel.EL4DocType;
            }

            docx.SetupCustomFileProperties(customDocProps);

            // set settings
            GenerateDocumentSettingsPart(docx, documentCreationInfo);

            // set glossary
            docx.GetOrCreateGlossaryDocumentPart();
            docx.MainDocumentPart.GlossaryDocumentPart.GlossaryDocument.Save();

            WriteEurolookPropertiesCustomXml(docx, documentCreationInfo);
            WriteAuthorsAsCustomXml(docx, documentCreationInfo);
            WriteTextsCustomXml(docx, documentCreationInfo);

            docx.SetLanguage(documentCreationInfo.Language);
        }

        private void WriteTextsCustomXml(WordprocessingDocument docx, DocumentCreationInfo documentCreationInfo)
        {
            // write the texts as custom xml file
            using (var ms = new MemoryStream())
            {
                using (var writer = XmlWriter.Create(ms))
                {
                    new TextsCustomXml(documentCreationInfo.Translations, documentCreationInfo.Language).WriteTo(writer);
                }

                docx.AddCustomXml(ms, TextsCustomXml.DatastoreItemId);
            }
        }

        private void WriteAuthorsAsCustomXml(WordprocessingDocument docx, DocumentCreationInfo documentCreationInfo)
        {
            var authorCustomXmlList = GenerateAllAuthorCustomXmlDocs(
                documentCreationInfo.AuthorRolesMappingList,
                documentCreationInfo.Language);

            foreach (var authorCustomXml in authorCustomXmlList)
            {
                docx.AddNewCustomXmlPart(authorCustomXml);
            }
        }

        private List<AuthorCustomXml> GenerateAllAuthorCustomXmlDocs(
            IEnumerable<AuthorRoleMapping> authorRoleMappings,
            Language language)
        {
            return authorRoleMappings
                   .Select(
                       (ar, i) =>
                       {
                           var jobAssignment = ar.JobAssignment ?? (IJobAssignment)ar.Author;
                           return new AuthorCustomXmlCreationInfo(ar.Author, jobAssignment, language)
                           {
                               IsMainAuthor = i == 0,
                               AuthorRole = ar.AuthorRole,
                           };
                       })
                   .Select(ci => new AuthorCustomXml(ci))
                   .ToList();
        }

        private static void WriteEurolookPropertiesCustomXml(
            WordprocessingDocument docx,
            DocumentCreationInfo documentCreationInfo)
        {
            // write EL properties custom xml
            using (var ms = new MemoryStream())
            {
                using (var writer = XmlWriter.Create(ms))
                {
                    var now = DateTime.Now;
                    new EurolookPropertiesCustomXml
                    {
                        ProductCustomizationId = documentCreationInfo.ProductCustomizationId,
                        CreationLanguage = documentCreationInfo.Language.Name,
                        CreationDate = now,
                        CreationVersion = documentCreationInfo.ClientVersion.ToString(),
                        DocumentModelId = documentCreationInfo.DocumentModel.Id,
                        DocumentModelName = documentCreationInfo.DocumentModel.DisplayName,
                        CompatibilityMode = EurolookCompatibilityMode.Eurolook10,
                        DocumentDate = now,
                        DocumentVersion = "0.1",
                    }.ToXDocument().WriteTo(writer);
                }

                docx.AddCustomXml(ms, EurolookPropertiesCustomXml.DatastoreItemId);
            }
        }

        private void GenerateCoreFilePropertiesPart(
            CoreFilePropertiesPart part,
            DocumentCreationInfo documentCreationInfo)
        {
            var xws = new XmlWriterSettings { Encoding = Encoding.UTF8, OmitXmlDeclaration = false };

            using (var writer = XmlWriter.Create(part.GetStream(FileMode.Create), xws))
            {
                var cp = XNamespace.Get("http://schemas.openxmlformats.org/package/2006/metadata/core-properties");
                var dc = XNamespace.Get("http://purl.org/dc/elements/1.1/");
                var dcterms = XNamespace.Get("http://purl.org/dc/terms/");
                var dcmitype = XNamespace.Get("http://purl.org/dc/dcmitype/");
                var xsi = XNamespace.Get("http://www.w3.org/2001/XMLSchema-instance");

                string authorFullName = documentCreationInfo.AuthorRolesMappingList
                                                            ?.FirstOrDefault()
                                                            ?.Author
                                                            ?.FullNameWithLastNameFirst
                                        ?? "";
                string timestamp = documentCreationInfo.TimestampUtc.ToString("yyyy-MM-ddThh:mm:ssZ", CultureInfo.InvariantCulture);

                var xdoc = new XDocument(
                    new XDeclaration("1.0", "utf-8", "yes"),
                    new XElement(
                        cp + "coreProperties",
                        new XAttribute(XNamespace.Xmlns + "cp", cp),
                        new XAttribute(XNamespace.Xmlns + "dc", dc),
                        new XAttribute(XNamespace.Xmlns + "dcterms", dcterms),
                        new XAttribute(XNamespace.Xmlns + "dcmitype", dcmitype),
                        new XAttribute(XNamespace.Xmlns + "xsi", xsi),
                        new XElement(dc + "title"),
                        new XElement(dc + "subject"),
                        new XElement(dc + "creator", authorFullName),
                        new XElement(cp + "keywords"),
                        new XElement(dc + "description"),
                        new XElement(cp + "lastModifiedBy", authorFullName),
                        new XElement(cp + "revision", "1"),
                        new XElement(
                            dcterms + "created",
                            new XAttribute(xsi + "type", "dcterms:W3CDTF"),
                            timestamp),
                        new XElement(
                            dcterms + "modified",
                            new XAttribute(xsi + "type", "dcterms:W3CDTF"),
                            timestamp)));

                xdoc.WriteTo(writer);
            }
        }

        private void GenerateExtendedFilePropertiesPart(ExtendedFilePropertiesPart part, DocumentCreationInfo documentCreationInfo)
        {
            var properties = new Ap.Properties(
                new Ap.Template { Text = Path.GetFileName(documentCreationInfo.TemplatePath) },
                new Ap.TotalTime { Text = "0" },
                new Ap.Pages { Text = "1" },
                new Ap.Words { Text = "0" },
                new Ap.Characters { Text = "0" },
                new Ap.Application { Text = "Microsoft Office Word" },
                new Ap.DocumentSecurity { Text = "0" },
                new Ap.PresentationFormat { Text = "Microsoft Word 14.0" },
                new Ap.Lines { Text = "0" },
                new Ap.Paragraphs { Text = "0" },
                new Ap.ScaleCrop { Text = "true" },
                new Ap.HeadingPairs(
                    new Vt.VTVector(
                        new Vt.Variant(new Vt.VTLPSTR { Text = "Title" }),
                        new Vt.Variant(new Vt.VTInt32 { Text = "1" }))
                    {
                        BaseType = Vt.VectorBaseValues.Variant,
                        Size = 2U,
                    }),
                new Ap.TitlesOfParts(new Vt.VTVector(new Vt.VTLPSTR { Text = "" }) { BaseType = Vt.VectorBaseValues.Lpstr, Size = 1U }),
                new Ap.Company { Text = "" },
                new Ap.LinksUpToDate { Text = "false" },
                new Ap.CharactersWithSpaces { Text = "0" },
                new Ap.SharedDocument { Text = "false" },
                new Ap.HyperlinksChanged { Text = "false" });

            properties.AddNamespaceDeclaration("vt", "http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes");
            properties.AddNamespaceDeclaration("ap", "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties");

            part.Properties = properties;
        }

        [SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1115:Parameter must follow comma", Justification = "The current formatting gives better readability")]
        private void GenerateDocumentSettingsPart(WordprocessingDocument docx, DocumentCreationInfo documentCreationInfo)
        {
            var part = docx.GetOrCreateMainDocumentSettingsPart();
            var relAttachedTemplate = part.AddExternalRelationship(
                "http://schemas.openxmlformats.org/officeDocument/2006/relationships/attachedTemplate",
                new Uri(documentCreationInfo.TemplatePath));

            var settings = new Settings(
                new View { Val = ViewValues.Print },
                ////new Zoom { Percent = "100" }, // don't set the zoom; Word will use the zoom value last used
                new AttachedTemplate { Id = relAttachedTemplate.Id },
                new DefaultTabStop { Val = part.Settings.Descendants<DefaultTabStop>().FirstOrDefault()?.Val ?? 720 },
                new HyphenationZone { Val = "425" },
                new EvenAndOddHeaders { Val = part.Settings.Descendants<EvenAndOddHeaders>().FirstOrDefault()?.Val ?? false },
                new CharacterSpacingControl { Val = CharacterSpacingValues.DoNotCompress },
                new Compatibility(
                    part.Settings.Descendants<DoNotUseHTMLParagraphAutoSpacing>().Any() ? new DoNotUseHTMLParagraphAutoSpacing()
                    {
                        Val = part.Settings.Descendants<DoNotUseHTMLParagraphAutoSpacing>().FirstOrDefault()?.Val ?? false,
                    }
                    : null,
                    part.Settings.Descendants<DoNotExpandShiftReturn>().Any() ? new DoNotExpandShiftReturn
                    {
                        Val = part.Settings.Descendants<DoNotExpandShiftReturn>().FirstOrDefault()?.Val ?? false,
                    }
                    : null,

                    new CompatibilitySetting
                    {
                        Name = CompatSettingNameValues.CompatibilityMode,
                        Uri = "http://schemas.microsoft.com/office/word",
                        Val = part.Settings.Descendants<CompatibilitySetting>().FirstOrDefault(x => x.Name == CompatSettingNameValues.CompatibilityMode)?.Val ?? "15",
                    },
                    new CompatibilitySetting
                    {
                        Name = CompatSettingNameValues.OverrideTableStyleFontSizeAndJustification,
                        Uri = "http://schemas.microsoft.com/office/word",
                        Val = part.Settings.Descendants<CompatibilitySetting>().FirstOrDefault(x => x.Name == CompatSettingNameValues.OverrideTableStyleFontSizeAndJustification)?.Val ?? "1",
                    },
                    new CompatibilitySetting
                    {
                        Name = CompatSettingNameValues.EnableOpenTypeFeatures,
                        Uri = "http://schemas.microsoft.com/office/word",
                        Val = part.Settings.Descendants<CompatibilitySetting>().FirstOrDefault(x => x.Name == CompatSettingNameValues.EnableOpenTypeFeatures)?.Val ?? "1",
                    },
                    new CompatibilitySetting
                    {
                        Name = CompatSettingNameValues.DoNotFlipMirrorIndents,
                        Uri = "http://schemas.microsoft.com/office/word",
                        Val = part.Settings.Descendants<CompatibilitySetting>().FirstOrDefault(x => x.Name == CompatSettingNameValues.DoNotFlipMirrorIndents)?.Val ?? "1",
                    }),
                new DocumentVariables(),
                new M.MathProperties(
                    new M.MathFont { Val = "Cambria Math" },
                    new M.BreakBinary { Val = M.BreakBinaryOperatorValues.Before },
                    new M.BreakBinarySubtraction { Val = M.BreakBinarySubtractionValues.MinusMinus },
                    new M.SmallFraction { Val = M.BooleanValues.Zero },
                    new M.DisplayDefaults(),
                    new M.LeftMargin { Val = 0U },
                    new M.RightMargin { Val = 0U },
                    new M.DefaultJustification { Val = M.JustificationValues.CenterGroup },
                    new M.WrapIndent { Val = 1440U },
                    new M.IntegralLimitLocation { Val = M.LimitLocationValues.SubscriptSuperscript },
                    new M.NaryLimitLocation { Val = M.LimitLocationValues.UnderOver }),
                new ThemeFontLanguages { Val = documentCreationInfo.Language.Locale },
                new ColorSchemeMapping
                {
                    Background1 = ColorSchemeIndexValues.Light1,
                    Text1 = ColorSchemeIndexValues.Dark1,
                    Background2 = ColorSchemeIndexValues.Light2,
                    Text2 = ColorSchemeIndexValues.Dark2,
                    Accent1 = ColorSchemeIndexValues.Accent1,
                    Accent2 = ColorSchemeIndexValues.Accent2,
                    Accent3 = ColorSchemeIndexValues.Accent3,
                    Accent4 = ColorSchemeIndexValues.Accent4,
                    Accent5 = ColorSchemeIndexValues.Accent5,
                    Accent6 = ColorSchemeIndexValues.Accent6,
                    Hyperlink = ColorSchemeIndexValues.Hyperlink,
                    FollowedHyperlink = ColorSchemeIndexValues.FollowedHyperlink,
                },
                part.Settings.Descendants<DoNotIncludeSubdocsInStats>().Any() ? new DoNotIncludeSubdocsInStats() : null,
                new DecimalSymbol { Val = "," },
                new ListSeparator { Val = "," },
                new StylePaneFormatFilter
                {
                    AllStyles = true,
                    CustomStyles = false,
                    LatentStyles = false,
                    StylesInUse = false,
                    HeadingStyles = true,
                    NumberingStyles = false,
                    TableStyles = false,
                    DirectFormattingOnRuns = false,
                    DirectFormattingOnParagraphs = false,
                    DirectFormattingOnNumbering = false,
                    DirectFormattingOnTables = false,
                    ClearFormatting = true,
                    Top3HeadingStyles = false,
                    VisibleStyles = false,
                    AlternateStyleNames = false,
                })
            {
                MCAttributes = new MarkupCompatibilityAttributes { Ignorable = "w14" },
            };

            var defaultTableStyle = part.Settings.Descendants<DefaultTableStyle>().FirstOrDefault();
            var endnoteSettings = part.Settings.Descendants<EndnoteDocumentWideProperties>().FirstOrDefault();
            var footnoteSettings = part.Settings.Descendants<FootnoteDocumentWideProperties>().FirstOrDefault();

            if (endnoteSettings != null)
            {
                var children = new OpenXmlElement[]
                {
                    endnoteSettings.EndnotePosition != null
                        ? new EndnotePosition { Val = endnoteSettings.EndnotePosition.Val }
                        : null,
                    endnoteSettings.NumberingFormat != null
                        ? new NumberingFormat { Val = endnoteSettings.NumberingFormat.Val }
                        : null,
                    endnoteSettings.NumberingStart != null
                        ? new NumberingStart { Val = endnoteSettings.NumberingStart.Val }
                        : null,
                    endnoteSettings.NumberingRestart != null
                        ? new NumberingRestart { Val = endnoteSettings.NumberingRestart.Val }
                        : null,
                }.Where(c => c != null).ToArray();

                settings.Append(new EndnoteDocumentWideProperties(children));
            }

            if (footnoteSettings != null)
            {
                var children = new OpenXmlElement[]
                {
                    footnoteSettings.FootnotePosition != null
                        ? new FootnotePosition { Val = footnoteSettings.FootnotePosition.Val }
                        : null,
                    footnoteSettings.NumberingFormat != null
                        ? new NumberingFormat { Val = footnoteSettings.NumberingFormat.Val }
                        : null,
                    footnoteSettings.NumberingStart != null
                        ? new NumberingStart { Val = footnoteSettings.NumberingStart.Val }
                        : null,
                    footnoteSettings.NumberingRestart != null
                        ? new NumberingRestart { Val = footnoteSettings.NumberingRestart.Val }
                        : null,
                }.Where(c => c != null).ToArray();

                settings.Append(new FootnoteDocumentWideProperties(children));
            }

            if (defaultTableStyle != null)
            {
                if (TryGetStyleNameFromStyleId(
                        defaultTableStyle.Val,
                        out string styleName,
                        docx.MainDocumentPart.StyleDefinitionsPart))
                {
                    documentCreationInfo.HasAdditionalSettingsToApply = true;
                    documentCreationInfo.DefaultTableStyle = styleName;
                }
            }

            settings.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
            settings.AddNamespaceDeclaration("o", "urn:schemas-microsoft-com:office:office");
            settings.AddNamespaceDeclaration("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
            settings.AddNamespaceDeclaration("m", "http://schemas.openxmlformats.org/officeDocument/2006/math");
            settings.AddNamespaceDeclaration("v", "urn:schemas-microsoft-com:vml");
            settings.AddNamespaceDeclaration("w10", "urn:schemas-microsoft-com:office:word");
            settings.AddNamespaceDeclaration("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
            settings.AddNamespaceDeclaration("w14", "http://schemas.microsoft.com/office/word/2010/wordml");
            settings.AddNamespaceDeclaration("sl", "http://schemas.openxmlformats.org/schemaLibrary/2006/main");

            part.Settings = settings;
        }

        private void SetContent(WordprocessingDocument docx, DocumentCreationInfo documentCreationInfo, BrickConverter conv)
        {
            var documentStructures = documentCreationInfo.DocumentModel.BrickReferences
                                                         .Where(
                                                             ds => ds.Brick is ContentBrick
                                                                   && !ds.Deleted
                                                                   && _brickVersionCompatibilityTester
                                                                       .IsBrickCompatibleWithAddinVersion(ds.Brick))
                                                         .ToList();

            var mainDocumentPart = docx.GetOrCreateMainDocumentPart();
            var mainSectionPr = mainDocumentPart.Document.Body?.Descendants<SectionProperties>().FirstOrDefault()
                                ?? new SectionProperties();

            mainDocumentPart.Document.AddDefaultNamespaceDeclarations();
            mainDocumentPart.Document.Body ??= new Body();

            var body = mainDocumentPart.Document.Body;
            body.RemoveAllChildren();

            mainDocumentPart.DeleteParts(mainDocumentPart.HeaderParts);
            mainDocumentPart.DeleteParts(mainDocumentPart.FooterParts);

            InsertBricks(body, PositionType.Begin, documentStructures, conv, documentCreationInfo.UserBrickChoices);
            InsertBricks(body, PositionType.Body, documentStructures, conv, documentCreationInfo.UserBrickChoices);
            InsertBricks(body, PositionType.End, documentStructures, conv, documentCreationInfo.UserBrickChoices);

            // Check whether last paragraph has section properties
            var lastParagraph = body.Descendants<Paragraph>().LastOrDefault();
            if (lastParagraph?.ParagraphProperties?.SectionProperties != null)
            {
                // Use last paragraph section properties for the default sectPr at the end of the document.
                var sectionProperties = lastParagraph.ParagraphProperties?.SectionProperties;
                lastParagraph.ParagraphProperties.SectionProperties = null;
                body.AppendChild(sectionProperties);
                return;
            }

            // Configure default sectPr at the end of the document
            var headerFooterBricks = documentStructures
                                     .Where(
                                         ds => !UserDeselectedBrick(documentCreationInfo.UserBrickChoices, ds.BrickId))
                                     .ToList();

            var sectionHelper = new DefaultSectionHelper(docx, conv, mainSectionPr, headerFooterBricks);
            sectionHelper.SetupDefaultSectionProperties();
        }

        private void InsertBricks(
            OpenXmlElement targetElement,
            PositionType targetType,
            List<DocumentStructure> documentStructures,
            BrickConverter conv,
            IDictionary<Guid, bool> userBrickChoices)
        {
            var structuresQuery = documentStructures.Where(ds => ds.Position == targetType).OrderBy(ds => ds.VerticalPositionIndex);
            if (structuresQuery.Any())
            {
                foreach (var structure in structuresQuery)
                {
                    // insert a a body placeholder brick when no rich body brick is present
                    if (structure.AutoBrickCreationType == AutoBrickCreationType.SimpleBodyContent)
                    {
                        var richBodyBrick = structuresQuery.FirstOrDefault(ds => ds.AutoBrickCreationType == AutoBrickCreationType.RichBodyContent);
                        if (richBodyBrick == null || UserDeselectedBrick(userBrickChoices, richBodyBrick.BrickId))
                        {
                            InsertBrick(conv, targetElement, structure);
                        }
                    }

                    // insert a normal brick when the user do no deselect it
                    else if (!UserDeselectedBrick(userBrickChoices, structure.BrickId))
                    {
                        InsertBrick(conv, targetElement, structure);
                    }
                }
            }
        }

        private void InsertBrick(BrickConverter conv, OpenXmlElement openXmlElement, DocumentStructure documentStructure)
        {
            foreach (var openXmlBrick in conv.ToOpenXml(documentStructure))
            {
                openXmlElement.AppendChild(openXmlBrick);
            }
        }

        private IList<DocumentStructure> MergeAndOrder(params List<DocumentStructure>[] bricks)
        {
            var orderedBricks = new List<DocumentStructure>();
            foreach (var brickList in bricks)
            {
                orderedBricks.AddRange(brickList);
            }

            return orderedBricks.OrderBy(x => x.VerticalPositionIndex).ToList();
        }

        private void FillHeaderFooter(
            BrickConverter conv,
            IList<DocumentStructure> documentStructures,
            OpenXmlPartRootElement root,
            Paragraph emptyFallbackContent,
            IDictionary<Guid, bool> userBrickChoices)
        {
            int count = 0;
            foreach (var structure in documentStructures)
            {
                if (UserDeselectedBrick(userBrickChoices, structure.BrickId))
                {
                    continue;
                }

                InsertBrick(conv, root, structure);
                count += 1;
            }

            if (count == 0 && emptyFallbackContent != null)
            {
                root.AppendChild(emptyFallbackContent.CloneNode(true));
            }
        }
    }
}
