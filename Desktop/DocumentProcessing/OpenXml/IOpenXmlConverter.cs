﻿using System.Collections.Generic;
using DocumentFormat.OpenXml;

namespace Eurolook.DocumentProcessing.OpenXml
{
    internal interface IOpenXmlConverter
    {
        IEnumerable<OpenXmlElement> ToOpenXml(OpenXmlConverterContext context);
    }
}
