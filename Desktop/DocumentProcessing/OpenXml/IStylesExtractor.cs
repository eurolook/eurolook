﻿using System.Collections.Generic;
using DocumentFormat.OpenXml.Wordprocessing;
using Eurolook.Data.Models;

namespace Eurolook.DocumentProcessing.OpenXml
{
    public interface IStylesExtractor
    {
        IEnumerable<Style> ExtractStyles(DocumentModel documentModel, DocumentModelLanguage documentModelLanguage = null);
    }
}
