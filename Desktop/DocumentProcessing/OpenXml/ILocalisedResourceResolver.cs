﻿using Eurolook.Data.Models;

namespace Eurolook.DocumentProcessing.OpenXml
{
    public interface ILocalisedResourceResolver
    {
        LocalisedResource ResolveLocalisedResource(string alias);

        Translation ResolveTranslation(string alias);
    }
}
