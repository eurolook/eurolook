using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Eurolook.Data.Models;

namespace Eurolook.DocumentProcessing.OpenXml
{
    /// <summary>
    /// Configures the default section break at the end of the document during document
    /// </summary>
    public class DefaultSectionHelper
    {
        private readonly WordprocessingDocument _docx;
        private readonly BrickConverter _conv;
        private readonly SectionProperties _defaultSectPr;
        private readonly IEnumerable<DocumentStructure> _documentStructures;

        private readonly Body _body;
        private readonly bool _isSingleSectionDocument;

        public DefaultSectionHelper(
            WordprocessingDocument docx,
            BrickConverter conv,
            SectionProperties defaultSectPr,
            IEnumerable<DocumentStructure> documentStructures)
        {
            _docx = docx;
            _conv = conv;
            _defaultSectPr = defaultSectPr;
            _documentStructures = documentStructures;

            _body = docx.GetOrCreateMainDocumentPart().Document.Body;
            _isSingleSectionDocument = _body?.Descendants<SectionProperties>().Any() == false;
        }

        public void SetupDefaultSectionProperties()
        {
            ClearHeadersAndFooters();

            CreateHeaderFooter(StoryType.Footer, HeaderFooterValues.Even);
            CreateHeaderFooter(StoryType.Footer, HeaderFooterValues.Default);
            CreateHeaderFooter(StoryType.Footer, HeaderFooterValues.First);

            CreateHeaderFooter(StoryType.Header, HeaderFooterValues.Even);
            CreateHeaderFooter(StoryType.Header, HeaderFooterValues.Default);
            CreateHeaderFooter(StoryType.Header, HeaderFooterValues.First);

            _body?.AppendChild(_defaultSectPr.CloneNode(true));
        }

        private void ClearHeadersAndFooters()
        {
            _defaultSectPr.RemoveAllChildren<FooterReference>();
            _defaultSectPr.RemoveAllChildren<HeaderReference>();
        }

        private void CreateHeaderFooter(StoryType storyType, HeaderFooterValues headerFooter)
        {
            var headerFooterBricks = GetHeaderFooterBricks(storyType, headerFooter);

            if (headerFooterBricks.Any() || _isSingleSectionDocument)
            {
                var part = storyType == StoryType.Header
                    ? CreateHeaderPart(headerFooter)
                    : CreateFooterPart(headerFooter);

                FillHeaderFooter(headerFooterBricks, part);
            }
        }

        private OpenXmlElement CreateFooterPart(HeaderFooterValues headerFooter)
        {
            var footerPart = _docx.AddFooter(WordprocessingDocumentMode.MainDocument, _defaultSectPr, headerFooter);
            _conv.Context.CurrentPart = footerPart;
            return footerPart.Footer;
        }

        private OpenXmlElement CreateHeaderPart(HeaderFooterValues headerFooter)
        {
            var headerPart = _docx.AddHeader(WordprocessingDocumentMode.MainDocument, _defaultSectPr, headerFooter);
            _conv.Context.CurrentPart = headerPart;
            return headerPart.Header;
        }

        private void FillHeaderFooter(
            IEnumerable<DocumentStructure> headerFooterBricks,
            OpenXmlElement headerFooterRootElement)
        {
            var content = headerFooterBricks.SelectMany(_conv.ToOpenXml).ToList();
            if (content.Any())
            {
                headerFooterRootElement.Append(content);
            }

            if (!content.Any() && _isSingleSectionDocument)
            {
                // Headers and footers are only filled with an empty paragraph if the document contains a single section
                AddEmptyParagraph(headerFooterRootElement);
            }
        }

        private List<DocumentStructure> GetHeaderFooterBricks(StoryType storyType, HeaderFooterValues headerFooter)
        {
            var occurrence = headerFooter == HeaderFooterValues.First ? Occurrence.First
                : headerFooter == HeaderFooterValues.Even ? Occurrence.Even
                : Occurrence.Odd;

            var headerFooterBricks = _documentStructures
                                     .Where(
                                         ds => ds.SectionId == 1 && ds.Position.HasFlag((PositionType)storyType)
                                                                 && ds.Position.HasFlag((PositionType)occurrence))
                                     .OrderBy(ds => ds.VerticalPositionIndex)
                                     .ToList();
            return headerFooterBricks;
        }

        private void AddEmptyParagraph(OpenXmlElement headerFooterRootElement)
        {
            var paragraphStyle = headerFooterRootElement is Header
                ? "Header"
                : "Footer";

            var paragraph = new Paragraph
            {
                ParagraphProperties = new ParagraphProperties
                {
                    ParagraphStyleId = new ParagraphStyleId { Val = paragraphStyle },
                },
            };

            headerFooterRootElement.AppendChild(paragraph);
        }
    }
}
