﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.CustomProperties;
using DocumentFormat.OpenXml.CustomXmlDataProperties;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Validation;
using DocumentFormat.OpenXml.VariantTypes;
using DocumentFormat.OpenXml.Wordprocessing;
using Eurolook.Data.Constants;
using Eurolook.Data.Xml;
using Eurolook.DocumentProcessing.Extensions;
using Word2010 = DocumentFormat.OpenXml.Office2010.Word;

namespace Eurolook.DocumentProcessing.OpenXml
{
    public static class OpenXmlUtils
    {
        private const string CorePropsStoreId = "{6C3C8BC8-F283-45AE-878A-BAB7291924A1}";
        private const string ExtPropsStoreId = "{6668398D-A668-4E3E-A5EB-62B293D839F1}";
        private const string CoverPropsStoreId = "{55AF091B-3C7A-41E3-B477-F2FDAA23CFDA}";

        private static readonly List<KeyValuePair<string, string>> DefaultNamespaces = new List<KeyValuePair<string, string>>
        {
            new KeyValuePair<string, string>("wpc", "http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"),
            new KeyValuePair<string, string>("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006"),
            new KeyValuePair<string, string>("o", "urn:schemas-microsoft-com:office:office"),
            new KeyValuePair<string, string>("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships"),
            new KeyValuePair<string, string>("m", "http://schemas.openxmlformats.org/officeDocument/2006/math"),
            new KeyValuePair<string, string>("v", "urn:schemas-microsoft-com:vml"),
            new KeyValuePair<string, string>("wp14", "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"),
            new KeyValuePair<string, string>("wp", "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"),
            new KeyValuePair<string, string>("w10", "urn:schemas-microsoft-com:office:word"),
            new KeyValuePair<string, string>("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main"),
            new KeyValuePair<string, string>("w14", "http://schemas.microsoft.com/office/word/2010/wordml"),
            new KeyValuePair<string, string>("w15", "http://schemas.microsoft.com/office/word/2012/wordml"),
            new KeyValuePair<string, string>("wpg", "http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"),
            new KeyValuePair<string, string>("wpi", "http://schemas.microsoft.com/office/word/2010/wordprocessingInk"),
            new KeyValuePair<string, string>("wne", "http://schemas.microsoft.com/office/word/2006/wordml"),
            new KeyValuePair<string, string>("wps", "http://schemas.microsoft.com/office/word/2010/wordprocessingShape"),
        };

        private static readonly string[] DefaultIgnorables = { "w14", "w15", "wp14" };

        public static MainDocumentPart GetOrCreateMainDocumentPart(this WordprocessingDocument docx)
        {
            var mainDocumentPart = docx.MainDocumentPart;
            if (mainDocumentPart == null)
            {
                mainDocumentPart = docx.AddMainDocumentPart();
                mainDocumentPart.Document = new Document();
                mainDocumentPart.Document.AddDefaultNamespaceDeclarations();
            }

            return mainDocumentPart;
        }

        public static GlossaryDocumentPart GetOrCreateGlossaryDocumentPart(this WordprocessingDocument docx)
        {
            var glossaryDocumentPart = docx.GetOrCreateMainDocumentPart().GlossaryDocumentPart;
            if (glossaryDocumentPart == null)
            {
                glossaryDocumentPart = docx.MainDocumentPart.AddNewPart<GlossaryDocumentPart>();
                glossaryDocumentPart.GlossaryDocument = new GlossaryDocument();
                glossaryDocumentPart.GlossaryDocument.AddDefaultNamespaceDeclarations();
            }

            if (!glossaryDocumentPart.GlossaryDocument.Elements<DocParts>().Any())
            {
                glossaryDocumentPart.GlossaryDocument.AppendChild(new DocParts());
            }

            return glossaryDocumentPart;
        }

        public static DocumentSettingsPart GetOrCreateMainDocumentSettingsPart(this WordprocessingDocument docx)
        {
            var documentSettingsPart = docx.GetOrCreateMainDocumentPart().DocumentSettingsPart;
            if (documentSettingsPart == null)
            {
                documentSettingsPart = docx.MainDocumentPart.AddNewPart<DocumentSettingsPart>();
                documentSettingsPart.Settings = new Settings();
            }

            return documentSettingsPart;
        }

        public static StyleDefinitionsPart GetOrCreateStyleDefinitionsPart(
            this WordprocessingDocument docx,
            WordprocessingDocumentMode mode)
        {
            return mode == WordprocessingDocumentMode.MainDocument
                ? docx.GetOrCreateMainStyleDefinitionsPart()
                : docx.GetOrCreateGlossaryStyleDefinitionsPart();
        }

        public static StyleDefinitionsPart GetOrCreateMainStyleDefinitionsPart(this WordprocessingDocument docx)
        {
            var styleDefinitionsPart = docx.GetOrCreateMainDocumentPart().StyleDefinitionsPart;
            if (styleDefinitionsPart == null)
            {
                styleDefinitionsPart = docx.MainDocumentPart.AddNewPart<StyleDefinitionsPart>();
                styleDefinitionsPart.Styles = new Styles();
                styleDefinitionsPart.Styles.Save();
            }

            return styleDefinitionsPart;
        }

        public static StyleDefinitionsPart GetOrCreateGlossaryStyleDefinitionsPart(this WordprocessingDocument docx)
        {
            var glossaryDocumentPart = GetOrCreateGlossaryDocumentPart(docx);
            if (glossaryDocumentPart.StyleDefinitionsPart == null)
            {
                var documentStyleDefinitionsPart = docx.GetOrCreateMainStyleDefinitionsPart();
                var glossaryStyleDefinitionsPart = glossaryDocumentPart.AddNewPart<StyleDefinitionsPart>();

                CopyPart(documentStyleDefinitionsPart, glossaryStyleDefinitionsPart);
                glossaryStyleDefinitionsPart.Styles.Save();
            }

            return glossaryDocumentPart.StyleDefinitionsPart;
        }

        public static NumberingDefinitionsPart GetOrCreateNumberingDefinitionsPart(
            this WordprocessingDocument docx,
            WordprocessingDocumentMode mode)
        {
            return mode == WordprocessingDocumentMode.MainDocument
                ? docx.GetOrCreateMainNumberingDefinitionsPart()
                : docx.GetOrCreateGlossaryNumberingDefinitionsPart();
        }

        public static NumberingDefinitionsPart GetOrCreateMainNumberingDefinitionsPart(this WordprocessingDocument docx)
        {
            var numberingDefinitionsPart = docx.GetOrCreateMainDocumentPart().NumberingDefinitionsPart;
            if (numberingDefinitionsPart == null)
            {
                numberingDefinitionsPart = docx.MainDocumentPart.AddNewPart<NumberingDefinitionsPart>();
                numberingDefinitionsPart.Numbering = new Numbering();
                numberingDefinitionsPart.Numbering.Save();
            }

            return numberingDefinitionsPart;
        }

        public static NumberingDefinitionsPart GetOrCreateGlossaryNumberingDefinitionsPart(this WordprocessingDocument docx)
        {
            var glossaryDocumentPart = GetOrCreateGlossaryDocumentPart(docx);
            if (glossaryDocumentPart.NumberingDefinitionsPart == null)
            {
                var documentNumberingDefinitionsPart = docx.GetOrCreateMainNumberingDefinitionsPart();
                var glossaryNumberingDefinitionsPart = glossaryDocumentPart.AddNewPart<NumberingDefinitionsPart>();

                CopyPart(documentNumberingDefinitionsPart, glossaryNumberingDefinitionsPart);
                glossaryNumberingDefinitionsPart.Numbering.Save();
            }

            return glossaryDocumentPart.NumberingDefinitionsPart;
        }

        public static FootnotesPart GetOrCreateFootnotesPart(this WordprocessingDocument docx, WordprocessingDocumentMode mode)
        {
            return mode == WordprocessingDocumentMode.MainDocument
                ? docx.GetOrCreateMainFootnotesPart()
                : docx.GetOrCreateGlossaryFootnotesPart();
        }

        public static FootnotesPart GetOrCreateMainFootnotesPart(this WordprocessingDocument docx)
        {
            var footnotesPart = docx.GetOrCreateMainDocumentPart().FootnotesPart;
            if (footnotesPart == null)
            {
                footnotesPart = docx.MainDocumentPart.AddNewPart<FootnotesPart>();
                footnotesPart.Footnotes = new Footnotes();
                footnotesPart.Footnotes.AddDefaultNamespaceDeclarations();
            }

            return footnotesPart;
        }

        public static FootnotesPart GetOrCreateGlossaryFootnotesPart(this WordprocessingDocument docx)
        {
            var glossaryDocumentPart = GetOrCreateGlossaryDocumentPart(docx);
            var footnotesPart = glossaryDocumentPart.FootnotesPart;
            if (footnotesPart == null)
            {
                footnotesPart = glossaryDocumentPart.AddNewPart<FootnotesPart>();
                footnotesPart.Footnotes = new Footnotes();
                footnotesPart.Footnotes.AddDefaultNamespaceDeclarations();
            }

            return footnotesPart;
        }

        public static CoreFilePropertiesPart GetOrCreateCoreFilePropertiesPart(this WordprocessingDocument docx)
        {
            return docx.CoreFilePropertiesPart ?? docx.AddCoreFilePropertiesPart();
        }

        public static ExtendedFilePropertiesPart GetOrCreateExtendedFilePropertiesPart(this WordprocessingDocument docx)
        {
            return docx.ExtendedFilePropertiesPart ?? docx.AddExtendedFilePropertiesPart();
        }

        public static void CopyPart(OpenXmlPart sourcePart, OpenXmlPart targetPart)
        {
            using (var streamReader = new StreamReader(sourcePart.GetStream()))
            {
                using (var streamWriter = new StreamWriter(targetPart.GetStream(FileMode.Create)))
                {
                    streamWriter.Write(streamReader.ReadToEnd());
                }
            }
        }

        public static CustomXmlPart AddCustomXml(this WordprocessingDocument docx, string filePath, Guid datastoreItemId)
        {
            using (Stream inStream = new FileStream(filePath, FileMode.Open))
            {
                return AddCustomXml(docx, inStream, datastoreItemId);
            }
        }

        public static CustomXmlPart AddCustomXml(this WordprocessingDocument docx, Stream inStream, Guid datastoreItemId)
        {
            var customXml = docx.GetOrCreateMainDocumentPart().AddCustomXmlPart(CustomXmlPartType.CustomXml);
            var customXmlProps = customXml.AddNewPart<CustomXmlPropertiesPart>();
            customXmlProps.DataStoreItem = new DataStoreItem
            {
                ItemId = datastoreItemId.ToString("B").ToUpperInvariant(),
            };
            using (var outStream = customXml.GetStream(FileMode.OpenOrCreate, FileAccess.Write))
            {
                inStream.Seek(0, SeekOrigin.Begin);
                inStream.CopyTo(outStream);
                outStream.Flush();
            }

            customXmlProps.DataStoreItem.Save();
            return customXml;
        }

        public static void CleanBody(this WordprocessingDocument docx)
        {
            docx.GetOrCreateMainDocumentPart().Document.Body.RemoveAllChildren();
        }

        /// <summary>
        /// Removes any undesired stuff from the document such as macros, customizations, Ribbon UI.
        /// </summary>
        /// <param name="docx">A <see cref="WordprocessingDocument" /> to be cleaned.</param>
        public static void CleanupDocument(this WordprocessingDocument docx)
        {
            // make sure the document is macro-free
            var macroPart = docx.MainDocumentPart.Parts
                                .FirstOrDefault(
                                    p =>
                                        p.OpenXmlPart.RelationshipType
                                        == "http://schemas.microsoft.com/office/2006/relationships/vbaProject");
            if (macroPart != null)
            {
                docx.MainDocumentPart.DeletePart(macroPart.OpenXmlPart);
            }

            // remove any toolbars or other customizations
            var customizationPart = docx.MainDocumentPart.CustomizationPart;
            if (customizationPart != null)
            {
                docx.MainDocumentPart.DeletePart(customizationPart);
            }

            var ribbonAndBackstageCustomizationPart = docx.RibbonAndBackstageCustomizationsPart;
            if (ribbonAndBackstageCustomizationPart != null)
            {
                docx.DeletePart(ribbonAndBackstageCustomizationPart);
            }

            var ribbonExtensibilityPart = docx.RibbonExtensibilityPart;
            if (ribbonExtensibilityPart != null)
            {
                docx.DeletePart(ribbonExtensibilityPart);
            }

            docx.CleanupCustomXmlParts();
        }

        [SuppressMessage(
            "Sonar",
            "S3220:Method calls should not resolve ambiguously to overloads with \"params\"",
            Justification = "Reviewed")]
        public static void SetBody(this WordprocessingDocument docx, IEnumerable<OpenXmlElement> openXmlElements)
        {
            foreach (var elem in openXmlElements)
            {
                if (elem is Paragraph ||
                    elem is SdtBlock ||
                    elem is Table ||
                    elem is BookmarkStart ||
                    elem is BookmarkEnd)
                {
                    docx.GetOrCreateMainDocumentPart().Document.Body.AppendChild(elem);
                }
                else
                {
                    // wrap in a paragraph
                    docx.GetOrCreateMainDocumentPart().Document.Body.AppendChild(new Paragraph(elem));
                }
            }
        }

        public static HeaderPart AddHeader(
            this WordprocessingDocument docx,
            WordprocessingDocumentMode mode,
            SectionProperties sectPr,
            HeaderFooterValues type)
        {
            var documentPart = mode == WordprocessingDocumentMode.MainDocument
                ? (OpenXmlPart)docx.GetOrCreateMainDocumentPart()
                : docx.GetOrCreateGlossaryDocumentPart();

            var part = documentPart.AddNewPart<HeaderPart>();

            part.Header = new Header();
            part.Header.AddDefaultNamespaceDeclarations();

            sectPr.InsertAt(
                new HeaderReference
                {
                    Type = type,
                    Id = documentPart.GetIdOfPart(part),
                },
                0);
            return part;
        }

        public static FooterPart AddFooter(
            this WordprocessingDocument docx,
            WordprocessingDocumentMode mode,
            SectionProperties sectPr,
            HeaderFooterValues type)
        {
            var documentPart = mode == WordprocessingDocumentMode.MainDocument
                ? (OpenXmlPart)docx.GetOrCreateMainDocumentPart()
                : docx.GetOrCreateGlossaryDocumentPart();

            var part = documentPart.AddNewPart<FooterPart>();

            part.Footer = new Footer();
            part.Footer.AddDefaultNamespaceDeclarations();
            sectPr.InsertAt(
                new FooterReference
                {
                    Type = type,
                    Id = documentPart.GetIdOfPart(part),
                },
                0);
            return part;
        }

        public static string AddImage(this OpenXmlPart targetPart, byte[] bytes, ImagePartType type, string id = null)
        {
            ImagePart imagePart;
            if (targetPart is MainDocumentPart mainDocumentPart)
            {
                imagePart = id != null ? mainDocumentPart.AddImagePart(type, id) : mainDocumentPart.AddImagePart(type);
            }
            else if (targetPart is GlossaryDocumentPart glossaryDocumentPart)
            {
                imagePart = id != null
                    ? glossaryDocumentPart.AddImagePart(type, id)
                    : glossaryDocumentPart.AddImagePart(type);
            }
            else if (targetPart is HeaderPart headerPart)
            {
                imagePart = id != null ? headerPart.AddImagePart(type, id) : headerPart.AddImagePart(type);
            }
            else if (targetPart is FooterPart footerPart)
            {
                imagePart = id != null ? footerPart.AddImagePart(type, id) : footerPart.AddImagePart(type);
            }
            else
            {
                throw new NotSupportedException("the property targetPart must be of type MainDocumentPart, HeaderPart or FooterPart");
            }

            using (var stream = new MemoryStream(bytes))
            {
                imagePart.FeedData(stream);
            }

            return targetPart.GetIdOfPart(imagePart);
        }

        public static void AddDefaultNamespaceDeclarations(this OpenXmlPartRootElement element)
        {
            var namespacesToAdd = DefaultNamespaces.Except(element.NamespaceDeclarations).ToList();
            foreach (var ns in namespacesToAdd)
            {
                element.AddNamespaceDeclaration(ns.Key, ns.Value);
            }

            var ignorables = element.MCAttributes != null
                             && element.MCAttributes.Ignorable != null
                             && element.MCAttributes.Ignorable.Value != null
                ? element.MCAttributes.Ignorable.Value.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                : new string[] { };

            string ignorable = string.Join(" ", DefaultIgnorables.Union(ignorables));
            element.MCAttributes = new MarkupCompatibilityAttributes { Ignorable = ignorable };
        }

        public static IEnumerable<ValidationErrorInfo> ValidateDocument(string fileName)
        {
            using (var wordDoc = WordprocessingDocument.Open(fileName, false))
            {
                return ValidatePackage(wordDoc);
            }
        }

        public static IEnumerable<ValidationErrorInfo> ValidatePackage(OpenXmlPackage wordDoc)
        {
            var validator = new OpenXmlValidator();
            return validator.Validate(wordDoc);
        }

        public static DataBinding GetDataBinding(string binding, OpenXmlConverterContext context)
        {
            string xpath = binding;
            var dataBinding = new DataBinding();
            if (xpath.StartsWith("/" + AuthorCustomXml.RootName, StringComparison.InvariantCultureIgnoreCase))
            {
                dataBinding.StoreItemId = context.GetAuthorXmlStoreInfo().StoreId;
                dataBinding.XPath = xpath;
            }
            else if (xpath.StartsWith("/" + TextsCustomXml.RootName, StringComparison.InvariantCultureIgnoreCase))
            {
                dataBinding.StoreItemId = context.TextsStoreId;
                dataBinding.XPath = xpath;
            }
            else if (xpath.StartsWith("/" + EurolookPropertiesCustomXml.RootName, StringComparison.InvariantCultureIgnoreCase))
            {
                dataBinding.StoreItemId = context.EurolookPropertiesStoreId;
                dataBinding.XPath = xpath;
            }
            else if (xpath.StartsWith("/coreprops/", StringComparison.InvariantCultureIgnoreCase))
            {
                string propName = xpath.ToLowerInvariant().Replace("/coreprops/", "");
                dataBinding.StoreItemId = CorePropsStoreId;
                dataBinding.PrefixMappings =
                    "xmlns:cp=\"http://schemas.openxmlformats.org/package/2006/metadata/core-properties\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:dcterms=\"http://purl.org/dc/terms/\"";
                if (new[] { "title", "subject", "creator", "description" }.Contains(propName))
                {
                    dataBinding.XPath = $"/cp:coreProperties[1]/dc:{propName}[1]";
                }
                else if (new[] { "created", "modified" }.Contains(propName))
                {
                    dataBinding.XPath = $"/cp:coreProperties[1]/dcterms:{propName}[1]";
                }
                else if (new[] { "keywords", "revision", "category", "lastmodifiedby" }.Contains(propName))
                {
                    dataBinding.XPath = $"/cp:coreProperties[1]/cp:{propName}[1]";
                }
            }
            else if (xpath.StartsWith("/extprops/", StringComparison.InvariantCultureIgnoreCase))
            {
                string propName = xpath.ToLowerInvariant().Replace("/extprops/", "");
                dataBinding.StoreItemId = ExtPropsStoreId;
                dataBinding.PrefixMappings = "xmlns:ep=\"http://schemas.openxmlformats.org/officeDocument/2006/extended-properties\"";
                dataBinding.XPath = $"/ep:Properties[1]/ep:{propName}[1]";
            }
            else if (xpath.StartsWith("/coverprops/", StringComparison.InvariantCultureIgnoreCase))
            {
                string propName = xpath.ToLowerInvariant().Replace("/coverprops/", "");
                dataBinding.StoreItemId = CoverPropsStoreId;
                dataBinding.PrefixMappings = "xmlns:cpp=\"http://schemas.microsoft.com/office/2006/coverPageProps\"";
                dataBinding.XPath = $"/cpp:CoverPageProperties[1]/cpp:{propName}[1]";
            }

            return dataBinding;
        }

        public static Word2010.OnOffValues ToWord2010OnOffValue(this bool value)
        {
            return value ? Word2010.OnOffValues.True : Word2010.OnOffValues.False;
        }

        public static CustomFilePropertiesPart GetOrCreateCustomFilePropertiesPart(this WordprocessingDocument docx)
        {
            if (docx.CustomFilePropertiesPart == null)
            {
                docx.AddNewPart<CustomFilePropertiesPart>();
            }

            if (docx.CustomFilePropertiesPart.Properties == null)
            {
                docx.CustomFilePropertiesPart.Properties = new Properties();
            }

            const string nsKey = "vt";
            const string nsValue = "http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes";
            if (!docx.CustomFilePropertiesPart.Properties.NamespaceDeclarations.Any(x => x.Key == nsKey || x.Value != nsValue))
            {
                docx.CustomFilePropertiesPart.Properties.AddNamespaceDeclaration(nsKey, nsValue);
            }

            return docx.CustomFilePropertiesPart;
        }

        internal static void SetupCustomFileProperties(this WordprocessingDocument docx, Dictionary<string, string> customProps)
        {
            // create a clean CustomFilePropertiesPart
            var customFilePropertiesPart = docx.GetOrCreateCustomFilePropertiesPart();

            customFilePropertiesPart.Properties.RemoveAllChildren();

            int propId = 2;
            foreach (string propName in customProps.Keys)
            {
                customFilePropertiesPart.SetCustomProperty(propId, propName, customProps[propName]);
                propId++;
            }
        }

        public static void SetCustomProperty(this CustomFilePropertiesPart customPropertiesPart, int id, string propName, string propValue)
        {
            var prop = new CustomDocumentProperty { FormatId = "{D5CDD505-2E9C-101B-9397-08002B2CF9AE}", PropertyId = id, Name = propName };
            prop.AppendChild(new VTLPWSTR(propValue));
            customPropertiesPart.Properties.Append(prop);
        }

        private static void CleanupCustomXmlParts(this WordprocessingDocument docx)
        {
            // keep SharePoint Content Type Properties and Content Type Schema, see
            // https://msdn.microsoft.com/en-us/library/office/bb447589(v=office.14).aspx and
            // https://msdn.microsoft.com/en-us/library/office/bb447539(v=office.14).aspx
            var partsToKeep = new HashSet<XName>
            {
                Namespaces.ContentTypePropertiesNs + "properties",
                Namespaces.ContentTypeSchemaNs + "contentTypeSchema",
                Namespaces.FormTemplatesNs + "FormTemplates",
                Namespaces.CustomXsnNs + "customXsn",
            };

            var customXmlParts =
                docx.MainDocumentPart.CustomXmlParts.Where(p => !partsToKeep.Contains(p.GetXDocument()?.Root?.Name)).ToList();
            docx.MainDocumentPart.DeleteParts(customXmlParts);
        }
    }
}
