using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using Eurolook.Data.Xml;
using Eurolook.DocumentProcessing.BrickDefinitionLanguage;
using Eurolook.DocumentProcessing.Extensions;
using Word = DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.OpenXml
{
    public static class OpenXmlConverterExtensions
    {
        private const string CoverPropsStoreId = "{55AF091B-3C7A-41E3-B477-F2FDAA23CFDA}";

        public static OpenXmlElement[] ConvertMany(this BdlElement self, IEnumerable<BdlElement> elements, OpenXmlConverterContext context)
        {
            var result = new List<OpenXmlElement>();
            foreach (var element in elements.ToArray())
            {
                if (element is IOpenXmlConverter)
                {
                    var convertibleElement = element as IOpenXmlConverter;
                    var conversionResults = convertibleElement.ToOpenXml(context);
                    result.AddRange(conversionResults);
                }
            }

            return result.ToArray();
        }

        public static StringValue NormalizeStyleName(this BdlElement self, string styleName)
        {
            string onlyAscii = Regex.Replace(styleName, @"[^\u0000-\u007F]+", string.Empty);
            return onlyAscii.Replace(" ", "");
        }

        public static int ConvertPercentToPct(this BdlElement self, string p)
        {
            string percent = p.Replace(" ", "").Replace(",", ".").ToLowerInvariant();
            double.TryParse(
                percent.Substring(0, percent.IndexOf("%", StringComparison.Ordinal)),
                NumberStyles.Number,
                CultureInfo.InvariantCulture,
                out double pct);
            return (int)Math.Round(pct * 50, MidpointRounding.AwayFromZero);
        }

        public static int ConvertToTwips(this BdlElement self, string value)
        {
            return CssUnitConverter.ConvertToTwips(value);
        }

        [SuppressMessage("Sonar", "S3220:Method calls should not resolve ambiguously to overloads with \"params\"", Justification = "Reviewed")]
        public static void AttachPlaceholder(
            WordprocessingDocument docxPackage,
            Word.SdtRun contentControl,
            string placeholderText,
            string placeholderStyle,
            string language,
            bool showingPlaceholder,
            bool isTemporary = false)
        {
            string partName = RegisterPlaceholderPart(docxPackage, CreatePlaceholder(placeholderText, placeholderStyle, language));
            var sdtPlaceholder = new Word.SdtPlaceholder
            {
                DocPartReference = new Word.DocPartReference { Val = partName },
            };
            contentControl.SdtProperties.AppendChild(sdtPlaceholder);

            if (isTemporary)
            {
                // Vanishing Content Control
                contentControl.SdtProperties.AppendChild(new Word.TemporarySdt());
            }

            if (showingPlaceholder)
            {
                contentControl.SdtProperties.AppendChild(new Word.ShowingPlaceholder());
                contentControl.SdtContentRun = new Word.SdtContentRun(CreatePlaceholder(placeholderText, placeholderStyle, language));
            }
        }

        public static void AttachPlaceholder(
            WordprocessingDocument docxPackage,
            Word.SdtRun contentControl,
            OpenXmlElement[] placeholderRichText,
            string placeholderStyle,
            string language,
            bool showingPlaceholder = false,
            bool isTemporary = false)
        {
            string partName = RegisterPlaceholderPart(docxPackage, CreatePlaceholder(placeholderRichText, placeholderStyle, language));
            var sdtPlaceholder = new Word.SdtPlaceholder
            {
                DocPartReference = new Word.DocPartReference { Val = partName },
            };
            contentControl.SdtProperties.AppendChild(sdtPlaceholder);

            if (isTemporary)
            {
                // Vanishing Content Control
                contentControl.SdtProperties.AppendChild(new Word.TemporarySdt());
            }

            if (showingPlaceholder)
            {
                contentControl.SdtProperties.AppendChild(new Word.ShowingPlaceholder());
                contentControl.SdtContentRun = new Word.SdtContentRun(CreatePlaceholder(placeholderRichText, placeholderStyle, language));
            }
        }

        public static bool IsVisible(string visiblityXPath, OpenXmlConverterContext context)
        {
            if (string.IsNullOrEmpty(visiblityXPath))
            {
                return true;
            }

            object result = EvaluateXPathExpression(visiblityXPath, context);
            if (result is bool booleanValue)
            {
                return booleanValue;
            }

            if (result is double doubleValue)
            {
                return Math.Abs(doubleValue) > 0.00001;
            }

            if (result is string stringValue)
            {
                return !string.IsNullOrEmpty(stringValue);
            }

            if (result is IEnumerable<object> enumerable)
            {
                return enumerable.Any();
            }

            return false;
        }

        public static string ResolveBinding(string xpath, OpenXmlConverterContext context)
        {
            if (string.IsNullOrEmpty(xpath))
            {
                return null;
            }

            var result = SelectXElement(xpath, context);
            return result?.Value;
        }

        public static XElement SelectXElement(string xpath, OpenXmlConverterContext context)
        {
            if (string.IsNullOrWhiteSpace(xpath))
            {
                return null;
            }

            var xdoc = GetXDocumentForXPathExpression(xpath, out string adjustedXPath, out var namespaceManager, context);

            return xdoc.XPathSelectElement(adjustedXPath, namespaceManager);
        }

        public static IEnumerable<XElement> SelectXElements(string xpath, OpenXmlConverterContext context)
        {
            if (string.IsNullOrWhiteSpace(xpath))
            {
                return null;
            }

            var xdoc = GetXDocumentForXPathExpression(xpath, out string adjustedXPath, out var namespaceManager, context);

            return xdoc.XPathSelectElements(adjustedXPath, namespaceManager);
        }

        public static object EvaluateXPathExpression(string xpath, OpenXmlConverterContext context)
        {
            if (string.IsNullOrWhiteSpace(xpath))
            {
                return null;
            }

            var xdoc = GetXDocumentForXPathExpression(xpath, out string adjustedXPath, out var namespaceManager, context);

            return xdoc.XPathEvaluate(adjustedXPath, namespaceManager);
        }

        private static XDocument GetXDocumentForXPathExpression(
            string xPath,
            out string adjustedXPath,
            out XmlNamespaceManager namespaceManager,
            OpenXmlConverterContext context)
        {
            adjustedXPath = xPath;
            namespaceManager = new XmlNamespaceManager(new NameTable());

            var xdoc = new XDocument();
            if (adjustedXPath.IndexOf("/" + EurolookPropertiesCustomXml.RootName, StringComparison.InvariantCultureIgnoreCase) >= 0)
            {
                xdoc = context.EurolookProperties.ToXDocument();
            }
            else if (adjustedXPath.IndexOf("/" + TextsCustomXml.RootName, StringComparison.InvariantCultureIgnoreCase) >= 0)
            {
                xdoc = context.TextsXml;
            }
            else if (adjustedXPath.IndexOf("/" + AuthorCustomXml.RootName, StringComparison.InvariantCultureIgnoreCase) >= 0)
            {
                xdoc = context.GetAuthorXmlStoreInfo().AuthorCustomXml;
            }
            else if (adjustedXPath.IndexOf("/coreprops/", StringComparison.InvariantCultureIgnoreCase) >= 0)
            {
                string propName = adjustedXPath.ToLowerInvariant().Replace("/coreprops/", "");
                adjustedXPath = $"/ns1:coreProperties[1]/ns0:{propName}[1]";

                namespaceManager.AddNamespace("ns0", "http://purl.org/dc/elements/1.1/");
                namespaceManager.AddNamespace("ns1", "http://schemas.openxmlformats.org/package/2006/metadata/core-properties");
                xdoc = context.Document.CoreFilePropertiesPart.GetXDocument();
            }
            else if (adjustedXPath.IndexOf("/extprops/", StringComparison.InvariantCultureIgnoreCase) >= 0)
            {
                string propName = adjustedXPath.ToLowerInvariant().Replace("/extprops/", "");
                adjustedXPath = $"/ns0:Properties[1]/ns0:{propName}[1]";

                namespaceManager.AddNamespace("ns0", "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties");
                xdoc = context.Document.ExtendedFilePropertiesPart.GetXDocument();
            }
            else if (adjustedXPath.IndexOf("/coverprops/", StringComparison.InvariantCultureIgnoreCase) >= 0)
            {
                string propName = adjustedXPath.ToLowerInvariant().Replace("/coverprops/", "");
                adjustedXPath = $"/ns0:CoverPageProperties[1]/ns0:{propName}[1]";

                namespaceManager.AddNamespace("ns0", "http://schemas.microsoft.com/office/2006/coverPageProps");
                xdoc = context.Document.GetCustomXmlPartAsXDocumentByStoreItemId(CoverPropsStoreId);
            }

            return xdoc;
        }

        [SuppressMessage("Sonar", "S3220:Method calls should not resolve ambiguously to overloads with \"params\"", Justification = "Reviewed")]
        private static Word.Run CreatePlaceholder(string content, string placeholderStyle, string language)
        {
            return new Word.Run(new Word.Text(content))
            {
                RunProperties = new Word.RunProperties
                {
                    RunStyle = new Word.RunStyle { Val = placeholderStyle },
                    Languages = new Word.Languages { Val = language },
                },
            };
        }

        private static Word.Run[] CreatePlaceholder(IEnumerable<OpenXmlElement> content, string placeholderStyle, string language)
        {
            var result = new List<Word.Run>();
            foreach (var elem in content)
            {
                if (elem is Word.Run)
                {
                    var run = elem.Clone() as Word.Run;
                    if (run.RunProperties == null)
                    {
                        run.RunProperties = new Word.RunProperties
                        {
                            RunStyle = new Word.RunStyle { Val = placeholderStyle },
                            Languages = new Word.Languages { Val = language },
                        };
                    }

                    result.Add(run);
                }
                else if (elem is Word.Text)
                {
                    var run = new Word.Run
                    {
                        RunProperties = new Word.RunProperties
                        {
                            RunStyle = new Word.RunStyle { Val = placeholderStyle },
                            NoProof = new Word.NoProof(),
                        },
                    };
                    run.AppendChild(elem);
                    result.Add(run);
                }
            }

            return result.ToArray();
        }

        private static string RegisterPlaceholderPart(WordprocessingDocument docxPackage, params Word.Run[] content)
        {
            string guid = Guid.NewGuid().ToString("B").ToUpperInvariant();
            string partName = "Placeholder_" + guid;

            var glossary = docxPackage.GetOrCreateGlossaryDocumentPart();
            if (glossary.GlossaryDocument.DocParts == null)
            {
                glossary.GlossaryDocument.AppendChild(new Word.DocParts());
            }

            if (glossary.StyleDefinitionsPart == null)
            {
                docxPackage.GetOrCreateStyleDefinitionsPart(WordprocessingDocumentMode.GlossaryDocument);
            }

            // setup properties
            var docPartType = new Word.DocPartTypes();
            docPartType.AppendChild(new Word.DocPartType { Val = Word.DocPartValues.SdtPlaceholder });

            var behaviors = new Word.Behaviors();
            behaviors.AppendChild(new Word.Behavior { Val = Word.DocPartBehaviorValues.Content });

            // setup body
            var body = new Word.DocPartBody();
            body.AppendChild(new Word.Paragraph(content));

            var newPart = new Word.DocPart
            {
                DocPartProperties = new Word.DocPartProperties
                {
                    DocPartId = new Word.DocPartId { Val = guid },
                    DocPartName = new Word.DocPartName { Val = partName },
                    Category = new Word.Category
                    {
                        Name = new Word.Name { Val = "General" },
                        Gallery = new Word.Gallery { Val = Word.DocPartGalleryValues.Placeholder },
                    },
                    DocPartTypes = docPartType,
                    Behaviors = behaviors,
                },
                DocPartBody = body,
            };
            glossary.GlossaryDocument.DocParts.AppendChild(newPart);
            glossary.GlossaryDocument.Save();

            return partName;
        }
    }
}
