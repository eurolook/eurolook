﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Eurolook.Data.Models;

namespace Eurolook.DocumentProcessing.OpenXml
{
    public class StylesExtractor : IStylesExtractor
    {
        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times", Justification = "Reviewed")]
        public IEnumerable<Style> ExtractStyles(DocumentModel documentModel, DocumentModelLanguage documentModelLanguage = null)
        {
            var template = documentModelLanguage?.Template ?? documentModel.Template;

            using var originalTemplateStream = new MemoryStream();
            originalTemplateStream.Write(template, 0, template.Length);

            using var originalTemplate = WordprocessingDocument.Open(originalTemplateStream, true);
            return originalTemplate.MainDocumentPart.StyleDefinitionsPart.Styles.Descendants<Style>();
        }
    }
}
