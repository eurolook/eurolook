﻿namespace Eurolook.DocumentProcessing.OpenXml
{
    public enum WordprocessingDocumentMode
    {
        MainDocument,
        GlossaryDocument,
    }
}
