using System;
using System.Collections.Generic;
using DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing.OpenXml
{
    public class StyleByNameComparer : IEqualityComparer<Style>
    {
        public bool Equals(Style x, Style y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
            {
                return false;
            }

            return string.Equals(
                x.StyleName.Val.Value,
                y.StyleName.Val.Value,
                StringComparison.InvariantCultureIgnoreCase);
        }

        public int GetHashCode(Style obj)
        {
            return obj.StyleName.Val.Value.GetHashCode();
        }
    }
}
