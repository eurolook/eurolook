using System.Drawing;

namespace Eurolook.DocumentProcessing.OpenXml
{
    public class OpenXmlConversionOptions
    {
        public OpenXmlConversionOptions()
        {
            DocumentMode = WordprocessingDocumentMode.MainDocument;
            CreateTableOfContentsContainer = false;
            HideBoundTextContentControlContainer = false;
            HideFreeTextContentControlContainer = false;
            UseCustomBrickContainerColor = false;
            BrickContainerColor = ColorTranslator.FromHtml("#0072C6");
        }

        public WordprocessingDocumentMode DocumentMode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the conversion will generate the a Word-style table of contents
        /// with a surrounding "content control" that features an update button.
        /// </summary>
        public bool CreateTableOfContentsContainer { get; set; }

        public bool HideBoundTextContentControlContainer { get; set; }

        public bool HideFreeTextContentControlContainer { get; set; }

        public Color BrickContainerColor { get; set; }

        public bool UseCustomBrickContainerColor { get; set; }
    }
}
