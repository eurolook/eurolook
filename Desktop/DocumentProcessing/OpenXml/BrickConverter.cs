using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Bdl = Eurolook.DocumentProcessing.BrickDefinitionLanguage;

namespace Eurolook.DocumentProcessing.OpenXml
{
    public class BrickConverter : ICanLog
    {
        public BrickConverter(OpenXmlConverterContext context)
        {
            Context = context;
        }

        public OpenXmlConverterContext Context { get; }

        public IEnumerable<OpenXmlElement> ToOpenXml(DocumentStructure documentStructure)
        {
            var brick = documentStructure.InitializeContentBrick(Context.AuthorInformation.CombinedPropertiesXml);
            if (!brick.HasContent || brick.Content.Root == null)
            {
                this.LogWarnFormat("Cannot convert brick {0} ({1}) without content", brick.Name, brick.Id);
                return Enumerable.Empty<OpenXmlElement>();
            }

            Context.Structure = documentStructure;
            Context.Brick = brick;
            var bdl = Bdl.BdlDocument.Parse(brick.Content);
            if (documentStructure.Position == PositionType.Body)
            {
                bdl.DefaultPlaceholderStyle = "BodyPlaceholderText";
            }

            return bdl.ToOpenXml(Context);
        }
    }
}
