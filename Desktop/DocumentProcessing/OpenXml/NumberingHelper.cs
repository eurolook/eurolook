using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using Eurolook.Common.Log;

namespace Eurolook.DocumentProcessing.OpenXml
{
    public class NumberingHelper : ICanLog
    {
        public void RestartList(OpenXmlConverterContext context, Paragraph paragraph, int restartListAt)
        {
            // we need to do the following:
            // 1. Get the paragraph style and check whether it has a numbering (i.e. a numId is set)
            // 2. If it has a numbering, get the numbering with this numId
            // 3. Get the underlying abstract numbering
            // 4. Create a new numbering instance based on the same abstract numbering
            // 5. Add an override for the list start value to this new numbering
            // 6. Add this new numbering instance to the paragraph
            var numId = GetParagraphNumId(context, paragraph);
            if (numId == null)
            {
                this.LogError("Restarting paragraph numbering failed because paragraph style has no numbering.");
                return;
            }

            var numberingPart = context.Document.GetOrCreateNumberingDefinitionsPart(context.Options.DocumentMode);

            var ilvl = GetParagraphListLevel(context, paragraph);

            var numberingInstances = numberingPart.Numbering.ChildElements.OfType<NumberingInstance>().ToList();
            var abstractNumId = GetAbstractNumId(numberingInstances, numId);
            if (abstractNumId != null)
            {
                int newNumId = GetNewNumId(numberingInstances);

                var levelOverrides = Enumerable.Range(0, 9).Select(
                    i =>
                    {
                        // higher levels will restart at 1
                        var levelStartValue = i <= ilvl.Value ? restartListAt : 1;
                        return new LevelOverride
                        {
                            StartOverrideNumberingValue = new StartOverrideNumberingValue { Val = levelStartValue },
                            LevelIndex = i,
                        };
                    });

                var restartedNumberingInstance = new NumberingInstance(levelOverrides)
                {
                    AbstractNumId = new AbstractNumId { Val = abstractNumId.Val },
                    NumberID = newNumId,
                };

                numberingPart.Numbering.AppendChild(restartedNumberingInstance);

                if (paragraph.ParagraphProperties.NumberingProperties == null)
                {
                    paragraph.ParagraphProperties.NumberingProperties = new NumberingProperties();
                }

                paragraph.ParagraphProperties.NumberingProperties.NumberingId = new NumberingId
                {
                    Val = newNumId,
                };
            }
        }

        private static Int32Value GetParagraphNumId(OpenXmlConverterContext context, Paragraph paragraph)
        {
            var stylesPart = context.Document.GetOrCreateStyleDefinitionsPart(context.Options.DocumentMode);
            var style = stylesPart.Styles.ChildElements.OfType<Style>()
                                  .FirstOrDefault(s => s.StyleId == paragraph.ParagraphProperties.ParagraphStyleId.Val);
            var numId = style?.StyleParagraphProperties?.NumberingProperties?.NumberingId?.Val;
            return numId;
        }

        private static Int32Value GetParagraphListLevel(OpenXmlConverterContext context, Paragraph paragraph)
        {
            var stylesPart = context.Document.GetOrCreateStyleDefinitionsPart(context.Options.DocumentMode);
            var style = stylesPart.Styles.ChildElements.OfType<Style>()
                                  .FirstOrDefault(s => s.StyleId == paragraph.ParagraphProperties.ParagraphStyleId.Val);
            var level = style?.StyleParagraphProperties?.NumberingProperties?.NumberingLevelReference?.Val;
            return level ?? new Int32Value(0);
        }

        private static AbstractNumId GetAbstractNumId(List<NumberingInstance> numberingInstances, Int32Value numId)
        {
            var numberingInstance = numberingInstances.FirstOrDefault(n => n.NumberID == numId);
            var abstractNumId = numberingInstance?.AbstractNumId;
            return abstractNumId;
        }

        private static int GetNewNumId(IEnumerable<NumberingInstance> numberingInstances)
        {
            var maxNumId = numberingInstances.Max(n => n.NumberID.Value);
            return maxNumId + 1;
        }
    }
}
