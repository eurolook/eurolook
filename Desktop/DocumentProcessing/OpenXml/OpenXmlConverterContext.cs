﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using Eurolook.Common.Extensions;
using Eurolook.Data.Constants;
using Eurolook.Data.Models;
using Eurolook.Data.Models.Metadata;
using Eurolook.Data.TermStore;
using Eurolook.Data.Xml;
using Eurolook.DocumentProcessing.Extensions;
using JetBrains.Annotations;

namespace Eurolook.DocumentProcessing.OpenXml
{
    public class OpenXmlConverterContext
    {
        private readonly CustomXmlPart _textsXmlPart;
        private readonly CustomXmlPart _propsXmlPart;
        private readonly CustomXmlPart _contentTypePropertiesXmlPart;
        private readonly List<AuthorCustomXmlStoreInfo> _authorCustomXmlStoreInfos;

        public OpenXmlConverterContext(
            WordprocessingDocument document,
            DocumentModel documentModel,
            IEnumerable<AuthorCustomXmlStoreInfo> authorCustomXmlStoreInfos)
        {
            Document = document;
            DocumentModel = documentModel;

            CurrentAuthorRolePriorityList = new List<string>();
            _textsXmlPart = document.GetCustomXmlPart(TextsCustomXml.RootName);
            _propsXmlPart = document.GetCustomXmlPart(EurolookPropertiesCustomXml.RootName);
            _contentTypePropertiesXmlPart = document.GetCustomXmlPart(Namespaces.ContentTypePropertiesNs, "properties");
            _authorCustomXmlStoreInfos = authorCustomXmlStoreInfos.ToList();

            FootnoteRelationshipIds = new Dictionary<string, IntegerValue>();

            TextsXml = new TextsCustomXml(_textsXmlPart.GetXDocument());
            EurolookProperties = new EurolookPropertiesCustomXml(_propsXmlPart.GetXDocument());
            AuthorInformation = new AuthorInformation(_authorCustomXmlStoreInfos, EurolookProperties);

            if (_contentTypePropertiesXmlPart != null)
            {
                ContentTypeProperties = _contentTypePropertiesXmlPart.GetXDocument();
            }

            if (DocumentModel?.DocumentModelMetadataDefinitions != null)
            {
                MetadataDefinitions =
                    DocumentModel.DocumentModelMetadataDefinitions.Select(dmmd => dmmd.MetadataDefinition)
                                 .ToDictionary(md => md.Name, md => md);
            }

            CustomDocumentProperties = new Dictionary<string, string>();
            CustomEurolookProperties = new Dictionary<string, string>();
        }

        public IDictionary<string, MetadataDefinition> MetadataDefinitions { get; }

        /// <summary>
        /// Gets or sets the brick being converted from BDL to Open XML.
        /// </summary>
        public ContentBrick Brick { get; set; }

        /// <summary>
        /// Gets or sets the structure of the brick being converted from BDL to Open XML.
        /// </summary>
        public DocumentStructure Structure { get; set; }

        public WordprocessingDocument Document { get; }

        public DocumentModel DocumentModel { get; }

        public Language Language { get; set; }

        public ILocalisedResourceResolver ResourceResolver { get; set; }

        public OpenXmlConversionOptions Options { get; set; }

        public TextsCustomXml TextsXml { get; }

        public ITermStoreCache TermStoreCache { get; set; }

        public string TextsStoreId
        {
            get { return _textsXmlPart.CustomXmlPropertiesPart.DataStoreItem.ItemId; }
        }

        public IEurolookProperties EurolookProperties { get; }

        public string EurolookPropertiesStoreId
        {
            get { return _propsXmlPart.CustomXmlPropertiesPart.DataStoreItem.ItemId; }
        }

        public List<string> CurrentAuthorRolePriorityList { get; set; }

        public AuthorInformation AuthorInformation { get; }

        public XDocument ContentTypeProperties { get; }

        public string ContentTypePropertiesStoreId
        {
            get { return _contentTypePropertiesXmlPart?.CustomXmlPropertiesPart.DataStoreItem.ItemId; }
        }

        public Dictionary<string, IntegerValue> FootnoteRelationshipIds { get; }

        public OpenXmlPart CurrentPart { get; set; }

        public Dictionary<string, string> CustomDocumentProperties { get; }

        public Dictionary<string, string> CustomEurolookProperties { get; }

        [NotNull]
        public AuthorCustomXmlStoreInfo GetAuthorXmlStoreInfo()
        {
            foreach (string authorRoleName in CurrentAuthorRolePriorityList)
            {
                var authorCustomXmlStoreInfo = _authorCustomXmlStoreInfos.FirstOrDefault(
                    a => authorRoleName.EqualsIgnoreCase(a.AuthorCustomXml.AuthorRoleName) ||
                         authorRoleName.EqualsIgnoreCase(a.AuthorCustomXml.OriginalAuthorRoleName));

                if (authorCustomXmlStoreInfo != null)
                {
                    return authorCustomXmlStoreInfo;
                }
            }

            return _authorCustomXmlStoreInfos.First();
        }

        public IEnumerable<AuthorCustomXml> GetAuthorCustomXmlDocs()
        {
            return _authorCustomXmlStoreInfos.Select(i => i.AuthorCustomXml);
        }
    }
}
