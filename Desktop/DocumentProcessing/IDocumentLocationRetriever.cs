﻿using System;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.DocumentProcessing
{
    public interface IDocumentLocationRetriever
    {
        DocumentLocationRetriever ByBrickId(Guid brickId);

        DocumentLocationRetriever ByContentControlTitle(string title);

        DocumentLocationRetriever ByContentControlTitlePattern(string pattern);

        DocumentLocationRetriever ByContentControlTag(string tag);

        DocumentLocationRetriever ByContentControlTagPattern(string pattern);

        DocumentLocationRetriever ByContentControlXPathBinding(string xPathBinding);

        DocumentLocationRetriever ByContentControlXPathBindingPattern(string pattern);

        DocumentLocationRetriever ByParagraphStyle(string styleName);

        DocumentLocationRetriever ByParagraphStylePattern(string pattern);

        DocumentLocationRetriever ByTableStylePattern(string pattern);

        DocumentLocationRetriever ThenByBrickId(Guid brickId);

        DocumentLocationRetriever ThenByContentControlTitle(string title);

        DocumentLocationRetriever ThenByContentControlTitlePattern(string pattern);

        DocumentLocationRetriever ThenByContentControlTag(string tag);

        DocumentLocationRetriever ThenByContentControlTagPattern(string pattern);

        DocumentLocationRetriever ThenByContentControlXPathBinding(string xPathBinding);

        DocumentLocationRetriever ThenByContentControlXPathBindingPattern(string pattern);

        DocumentLocationRetriever ThenByParagraphStyle(string pattern);

        DocumentLocationRetriever ThenByParagraphStylePattern(string pattern);

        DocumentLocationRetriever ThenByTableStylePattern(string pattern);

        DocumentLocationResult Retrieve(Range searchRange);
    }
}
