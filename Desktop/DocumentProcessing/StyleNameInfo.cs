using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using JetBrains.Annotations;

namespace Eurolook.DocumentProcessing
{
    public class StyleNameInfo
    {
        private static readonly Regex StyleNamePattern =
            new Regex(@"^(?<Category>[\p{L} ]+\p{L})( ?(?<TextLevel>\d))?( ?\(Level (?<ListLevel>\d)?\))?");

        private static readonly List<Regex> StyleNameParsingExceptions = new List<Regex>
        {
            new Regex(@"^(?<Category>LegalNumPar)(?<ListLevel>\d)?"),
        };

        private static readonly IReadOnlyDictionary<string, string> StyleNameExceptions = new Dictionary<string, string>
        {
            { @"^Normal (\d)$", @"Text $1" },
            { @"^Text$", "Normal" },
            { @"^LegalNumPar \(Level (\d)\)$", "LegalNumPar$1" },
            { @"^LegalNumPar (\d )?\(Level (\d)\)$", "LegalNumPar$2" },
            { @"^LegalNumPar( \d)?$", "LegalNumPar$1" },
        };

        private static readonly IReadOnlyDictionary<string, string> TextLevelExceptions = new Dictionary<string, string>
        {
            { @"^LegalNumPar ?(\d)? ?(\(Level \d\))?$", "0" },
            { @"^Article$", "1" },
        };

        private static readonly IReadOnlyDictionary<string, string> ListLevelExceptions =
            new Dictionary<string, string>();

        private static readonly IReadOnlyDictionary<string, string> CategoryNameExceptions =
            new Dictionary<string, string>
            {
                { @"^Normal$", @"Text" },
            };

        /// <summary>
        /// Initializes a new instance of the <see cref="StyleNameInfo" /> class.
        /// Create style name for given category and set text level to 0 and list level to 1.
        /// </summary>
        /// <param name="styleNameOrCategory">The style category or complete style name.</param>
        public StyleNameInfo(string styleNameOrCategory)
            : this(styleNameOrCategory, 0, 1)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StyleNameInfo" /> class.
        /// Create style name for given category and text level. Set list level to 1.
        /// </summary>
        /// <param name="styleNameOrCategory">The style category or complete style name.</param>
        /// <param name="textLevel">The 0-based text level.</param>
        public StyleNameInfo(string styleNameOrCategory, int textLevel)
            : this(styleNameOrCategory, textLevel, 1)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StyleNameInfo" /> class.
        /// Create style name for given category, text and list level.
        /// </summary>
        /// <param name="styleNameOrCategory">The style category or complete style name.</param>
        /// <param name="textLevel">The 0-based text level.</param>
        /// <param name="listLevel">The 1-based list level.</param>
        public StyleNameInfo(string styleNameOrCategory, int textLevel, int listLevel)
        {
            TextLevel = Math.Max(0, textLevel);
            TextLevel = ApplyLevelExceptions(styleNameOrCategory, TextLevelExceptions, textLevel);

            ListLevel = Math.Max(1, listLevel);
            ListLevel = ApplyLevelExceptions(styleNameOrCategory, ListLevelExceptions, listLevel);
            Category = GetStyleCategory(styleNameOrCategory);

            StyleName = BuildStyleName(Category, TextLevel, ListLevel);
        }

        public string StyleName { get; }

        public string Category { get; }

        public int TextLevel { get; }

        public int ListLevel { get; }

        [CanBeNull]
        public static StyleNameInfo FromStyleName(string styleName)
        {
            var styleNameInfo = ParseStyleName(StyleNameParsingExceptions, styleName)
                                ?? ParseStyleName(StyleNamePattern, styleName);

            return styleNameInfo;
        }

        public StyleNameInfo TextDemote()
        {
            return new StyleNameInfo(Category, TextLevel + 1, ListLevel);
        }

        public StyleNameInfo TextPromote()
        {
            return new StyleNameInfo(Category, TextLevel - 1, ListLevel);
        }

        public StyleNameInfo ListDemote()
        {
            return new StyleNameInfo(Category, TextLevel, ListLevel + 1);
        }

        public StyleNameInfo ListPromote()
        {
            return new StyleNameInfo(Category, TextLevel, ListLevel - 1);
        }

        public override string ToString()
        {
            return $"Style '{StyleName:20}': TextLevel:={TextLevel}; ListLevel:={ListLevel}; Category:={Category}";
        }

        [CanBeNull]
        private static StyleNameInfo ParseStyleName(List<Regex> styleNamePatterns, string styleName)
        {
            foreach (var regex in styleNamePatterns)
            {
                var styleNameInfo = ParseStyleName(regex, styleName);
                if (styleNameInfo != null)
                {
                    return styleNameInfo;
                }
            }

            return null;
        }

        private static StyleNameInfo ParseStyleName(Regex regex, string styleName)
        {
            string category = "Text";
            int textLevel = 0;
            int listLevel = 1;
            var m = regex.Match(styleName);
            if (m.Success)
            {
                if (m.Groups["Category"].Success)
                {
                    category = m.Groups["Category"].Value;
                }

                if (m.Groups["TextLevel"].Success)
                {
                    textLevel = int.Parse(m.Groups["TextLevel"].Value);
                }

                if (m.Groups["ListLevel"].Success)
                {
                    listLevel = int.Parse(m.Groups["ListLevel"].Value);
                }

                return new StyleNameInfo(category, textLevel, listLevel);
            }

            return null;
        }

        /// <summary>
        /// builds the name for the style specified by stylename/category, text and list level
        /// the method already handles exeptions from the style EL naming scheme
        /// "[category] [textlevel] (Level [listlevel])", e.g. StyleName 2 (Level 3).
        /// </summary>
        /// <param name="category">The style category name to be used.</param>
        /// <param name="textLevel">The 0-based text level.</param>
        /// <param name="listLevel">The 1-based list level.</param>
        /// <returns>The style name.</returns>
        private static string BuildStyleName(string category, int textLevel = 0, int listLevel = 1)
        {
            var styleName = new StringBuilder(category);

            if (textLevel > 0)
            {
                styleName.Append($" {textLevel}");
            }

            if (listLevel > 1)
            {
                styleName.Append($" (Level {listLevel})");
            }

            return ApplyStyleNameExceptions(styleName.ToString());
        }

        private static string GetStyleCategory(string styleNameOrCategory)
        {
            string category = styleNameOrCategory;
            var m = StyleNamePattern.Match(styleNameOrCategory);
            if (m.Success && m.Groups["Category"].Success)
            {
                category = m.Groups["Category"].Value;
            }

            return ApplyCategoryNameExceptions(category);
        }

        private static string ApplyStyleNameExceptions(string styleName)
        {
            return ApplyExceptions(styleName, StyleNameExceptions);
        }

        private static string ApplyCategoryNameExceptions(string styleName)
        {
            return ApplyExceptions(styleName, CategoryNameExceptions);
        }

        private static string ApplyExceptions(string styleName, IReadOnlyDictionary<string, string> namingExceptions)
        {
            foreach (var m in namingExceptions)
            {
                string result = Regex.Replace(styleName, m.Key, m.Value);
                if (!result.Equals(styleName))
                {
                    return result;
                }
            }

            return styleName;
        }

        private static int ApplyLevelExceptions(
            string styleNameOrCategory,
            IReadOnlyDictionary<string, string> levelExceptions,
            int level)
        {
            foreach (var m in levelExceptions)
            {
                if (Regex.Match(styleNameOrCategory, m.Key).Success)
                {
                    return int.Parse(Regex.Replace(styleNameOrCategory, m.Key, m.Value));
                }
            }

            return level;
        }
    }
}
