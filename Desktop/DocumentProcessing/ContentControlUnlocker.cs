﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Eurolook.DocumentProcessing.Extensions;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.DocumentProcessing
{
    /// <summary>
    /// A helper class to temporarily unlock all content controls in a document or range.
    /// </summary>
    public sealed class ContentControlUnlocker : IDisposable
    {
        private readonly List<ContentControl> _lockedContentControls;

        public ContentControlUnlocker(Document document)
        {
            _lockedContentControls = document.GetAllContentControls().Where(cc => cc.LockContents).ToList();
            foreach (var lockedContentControl in _lockedContentControls)
            {
                lockedContentControl.LockContents = false;
            }
        }

        public ContentControlUnlocker(Range range)
        {
            _lockedContentControls =
                range.ContentControls.OfType<ContentControl>().Where(cc => cc.LockContents).ToList();
            if (range.ParentContentControl != null && range.ParentContentControl.LockContents)
            {
                _lockedContentControls.Add(range.ParentContentControl);
            }

            foreach (var lockedContentControl in _lockedContentControls)
            {
                lockedContentControl.LockContents = false;
            }
        }

        public void Dispose()
        {
            foreach (var lockedContentControl in _lockedContentControls)
            {
                try
                {
                    lockedContentControl.LockContents = true;
                }
                catch (COMException)
                {
                    // ignore all COM errors here (a content control might have been deleted for example)
                }
            }
        }
    }
}
