﻿using System.Collections.Generic;
using Microsoft.Office.Interop.Word;

namespace Eurolook.DocumentProcessing
{
    public class StoryTypeComparer : IComparer<WdStoryType>
    {
        // custom sort order for Word stories so that the header/footer stories are sorted in the following order:
        // first page, even page, odd page (as it would be natural in the document) and so that the header always comes before
        // the corresponding footer
        private static readonly Dictionary<WdStoryType, int> StoryTypeIndexMap = new Dictionary<WdStoryType, int>
        {
            { WdStoryType.wdMainTextStory, 1 },
            { WdStoryType.wdFootnotesStory, 2 },
            { WdStoryType.wdEndnotesStory, 3 },
            { WdStoryType.wdCommentsStory, 4 },
            { WdStoryType.wdTextFrameStory, 5 },
            { WdStoryType.wdFirstPageHeaderStory, 6 },
            { WdStoryType.wdFirstPageFooterStory, 7 },
            { WdStoryType.wdEvenPagesHeaderStory, 8 },
            { WdStoryType.wdEvenPagesFooterStory, 9 },
            { WdStoryType.wdPrimaryHeaderStory, 10 },
            { WdStoryType.wdPrimaryFooterStory, 11 },
            { WdStoryType.wdFootnoteSeparatorStory, 12 },
            { WdStoryType.wdFootnoteContinuationSeparatorStory, 13 },
            { WdStoryType.wdFootnoteContinuationNoticeStory, 14 },
            { WdStoryType.wdEndnoteSeparatorStory, 15 },
            { WdStoryType.wdEndnoteContinuationSeparatorStory, 16 },
            { WdStoryType.wdEndnoteContinuationNoticeStory, 17 },
        };

        public int Compare(WdStoryType x, WdStoryType y)
        {
            return StoryTypeIndexMap[x].CompareTo(StoryTypeIndexMap[y]);
        }
    }
}
