using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Microsoft.Office.Interop.Word;

namespace Eurolook.DocumentProcessing
{
    public class ParagraphFormatComparer : IEqualityComparer<ParagraphFormat>
    {
        [SuppressMessage("ReSharper", "CompareOfFloatsByEqualityOperator", Justification = "Exact comparison is ok here.")]
        [SuppressMessage("SonarQube", "S1244:Floating point numbers should not be tested for equality", Justification = "Exact comparison is ok here.")]
        public bool Equals(ParagraphFormat x, ParagraphFormat y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
            {
                return false;
            }

            return x.Alignment == y.Alignment
                   && x.AutoAdjustRightIndent == y.AutoAdjustRightIndent
                   && x.BaseLineAlignment == y.BaseLineAlignment
                   && x.CharacterUnitFirstLineIndent == y.CharacterUnitFirstLineIndent
                   && x.CharacterUnitLeftIndent == y.CharacterUnitLeftIndent
                   && x.CharacterUnitRightIndent == y.CharacterUnitRightIndent
                   && x.DisableLineHeightGrid == y.DisableLineHeightGrid
                   && x.FirstLineIndent == y.FirstLineIndent
                   && x.HalfWidthPunctuationOnTopOfLine == y.HalfWidthPunctuationOnTopOfLine
                   && x.HangingPunctuation == y.HangingPunctuation
                   && x.Hyphenation == y.Hyphenation
                   && x.KeepTogether == y.KeepTogether
                   && x.KeepWithNext == y.KeepWithNext
                   && x.LeftIndent == y.LeftIndent
                   && x.LineSpacing == y.LineSpacing
                   && x.LineSpacingRule == y.LineSpacingRule
                   && x.LineUnitAfter == y.LineUnitAfter
                   && x.LineUnitBefore == y.LineUnitBefore
                   && x.MirrorIndents == y.MirrorIndents
                   && x.NoLineNumber == y.NoLineNumber
                   && x.OutlineLevel == y.OutlineLevel
                   && x.PageBreakBefore == y.PageBreakBefore
                   && x.ReadingOrder == y.ReadingOrder
                   && x.RightIndent == y.RightIndent
                   && x.SpaceAfter == y.SpaceAfter
                   && x.SpaceAfterAuto == y.SpaceAfterAuto
                   && x.SpaceBefore == y.SpaceBefore
                   && x.SpaceBeforeAuto == y.SpaceBeforeAuto
                   && x.WidowControl == y.WidowControl
                   && x.WordWrap == y.WordWrap;
        }

        public int GetHashCode(ParagraphFormat obj)
        {
            return obj.GetHashCode();
        }
    }
}
