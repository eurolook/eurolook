using System;
using Eurolook.Common;
using Eurolook.Data.Models;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Newtonsoft.Json;

namespace Eurolook.DocumentProcessing
{
    /// <summary>
    /// This class contains information that is stored in the tag of the brick content control in JSON format.
    /// </summary>
    public class BrickContainer
    {
        public BrickContainer()
        {
        }

        public BrickContainer(Brick brick)
        {
            Id = brick.Id;
            Name = brick.Name;
            DisplayName = brick.DisplayName;
            CategoryId = brick.CategoryId;
            CategoryName = brick.Category?.Name;
            GroupId = brick.GroupId;
            GroupName = brick.Group?.Name;
            GroupDisplayName = brick.Group?.DisplayName;
        }

        [JsonIgnore]
        public string Name { get; set; }

        [JsonIgnore]
        public string DisplayName { get; set; }

        [JsonIgnore]
        public Guid CategoryId { get; set; }

        [JsonIgnore]
        public string CategoryName { get; set; }

        public Guid? GroupId { get; set; }

        [JsonIgnore]
        public string GroupName { get; set; }

        [JsonIgnore]
        public string GroupDisplayName { get; set; }

        public Guid Id { get; set; }

        [JsonIgnore]
        public ContentControl ContentControl { get; set; }

        [JsonIgnore]
        public Brick Brick { get; set; }

        public string ContainerDisplayName
        {
            get
            {
                if (!string.IsNullOrEmpty(GroupDisplayName))
                {
                    return $"{GroupDisplayName} - {DisplayName}";
                }

                return DisplayName;
            }
        }

        [CanBeNull]
        public static BrickContainer FromTag(string tag)
        {
            // NOTE: the tag format used consists of Guid values separated by dash. Guid values are encodes as
            //   ShortGuid or a Guid string representation in numerical format produced by `Guid.ToString("N")`. The
            //   Guid format must not contain dashes as they are used to separate multiple guid values.
            //
            //   The tag format is as follows:
            //   - If the tag does not contain dashes, it contains a single guid value, the brick id
            //   - If the tag consists of two parts, the following structure is used:
            //     - the first part contains the group id
            //     - the second part contains the brick id
            //   - We allow the tag format to be extended by additional parts that may contain further id values, e.g.
            //     to uniquely identify brick instances in the future (e.g. we can imagine giving an id to figure bricks
            //     enabling to store additional metadata for images). In this case the tag structure is as follows:
            //     - the first part contains the group id (must be in the first part for backward compat) or contains
            //       a null guid if the brick is not in a group
            //     - the second part contains the brick id
            //     - the following parts contain additional id values
            if (tag is null)
            {
                return null;
            }

            var parts = tag.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            if (parts.Length == 1 && TryParseGuidFromTagPart(parts[0], out var brickId))
            {
                return new BrickContainer { Id = brickId };
            }

            // brick id comes at second place, group id first, other parts may be added in the future
            if (parts.Length >= 2 && TryParseGuidFromTagPart(parts[1], out brickId))
            {
                // We ignore parsing errors on the group id as parsing might fail because of the buggy ShortGuid
                // encoding that might have been used to encode the group id
                var groupId = ParseOptionalGuidFromTagPart(parts[0]);

                // tag consists of group id and brick id, Guid.Empty may be used for bricks not in a group
                return new BrickContainer
                {
                    Id = brickId,
                    GroupId = groupId,
                };
            }

            return null;
        }

        public string ToTag()
        {
            if (GroupId != null)
            {
                return new ShortGuid(GroupId.Value) + "-" + new ShortGuid(Id);
            }

            return new ShortGuid(Id);
        }

        private static Guid? ParseOptionalGuidFromTagPart(string tagPart)
        {
            if (TryParseGuidFromTagPart(tagPart, out var result))
            {
                // treat the null guid as if there is no guid set and return null
                return result != Guid.Empty ? result : null;
            }

            return null;
        }

        private static bool TryParseGuidFromTagPart(string tagPart, out Guid value)
        {
            // TODO: We add code here that first tries to parse the tag as Guid, then as ShortGuid.
            // Once all clients are updated, we can get rid of the ShortGuid class, which is not
            // fully bijective due to a buggy implementation.
            if (Guid.TryParse(tagPart, out var idFromGuid))
            {
                value = idFromGuid;
                return true;
            }

            if (ShortGuid.TryParse(tagPart, out var idFromShortGuid))
            {
                value = idFromShortGuid;
                return true;
            }

            value = Guid.Empty;
            return false;
        }
    }
}
