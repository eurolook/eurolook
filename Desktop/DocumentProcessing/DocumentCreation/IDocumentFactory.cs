using System.Threading.Tasks;

namespace Eurolook.DocumentProcessing.DocumentCreation
{
    public interface IDocumentFactory
    {
        string FileExtension { get; }

        bool CanHandle(DocumentCreationInfo documentCreationInfo);

        Task CreateDocument(string file, DocumentCreationInfo documentCreationInfo);
    }
}
