using Microsoft.Office.Interop.Word;

namespace Eurolook.DocumentProcessing.DocumentCreation
{
    public class DocumentCreationResult
    {
        public DocumentCreationInfo DocumentCreationInfo { get; set; }

        public Document Document { get; set; }
    }
}
