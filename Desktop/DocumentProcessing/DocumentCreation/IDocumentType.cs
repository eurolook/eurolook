using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;

namespace Eurolook.DocumentProcessing.DocumentCreation
{
    public interface IDocumentType
    {
        Template Template { get; }

        DocumentModel DocumentModel { get; }

        bool IsAvailableOffline { get; }
    }
}
