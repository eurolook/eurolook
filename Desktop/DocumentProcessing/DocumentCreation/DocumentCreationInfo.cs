﻿using System;
using System.Collections.Generic;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;
using Eurolook.DocumentProcessing.OpenXml;

namespace Eurolook.DocumentProcessing.DocumentCreation
{
    public class DocumentCreationInfo
    {
        public DocumentCreationInfo(Template template)
        {
            DocumentModel = template.BaseDocumentModel;
            TemplateStoreTemplate = template;
        }

        public DocumentCreationInfo(IDocumentType documentType)
        {
            DocumentModel = documentType.DocumentModel;
            TemplateStoreTemplate = documentType.Template;
            IsTemplateAvailableOffline = documentType.IsAvailableOffline;
        }

        public DocumentCreationInfo(DocumentModel documentModel)
        {
            DocumentModel = documentModel;
            IsTemplateAvailableOffline = true;
        }

        public User User { get; set; }

        public List<AuthorRoleMapping> AuthorRolesMappingList { get; set; }

        public bool IsSignerFixed { get; set; }

        public Template TemplateStoreTemplate { get; }

        public DocumentModel DocumentModel { get; }

        public bool IsTemplateFromTemplateStore => TemplateStoreTemplate != null;

        public bool IsTemplateAvailableOffline { get; set; }

        public bool AddToUserProfileUponDocumentCreation { get; set; }

        public int SelectedTemplateViewKind { get; set; }

        public Language Language { get; set; }

        public Version ClientVersion { get; set; }

        public string ProductCustomizationId { get; set; }

        public string TemplatePath { get; set; }

        public DateTime TimestampUtc { get; set; }

        public IEnumerable<Translation> Translations { get; set; }

        public ILocalisedResourceResolver ResourceResolver { get; set; }

        public Dictionary<Guid, bool> UserBrickChoices { get; set; }

        public bool HasAdditionalSettingsToApply { get; set; }

        public string DefaultTableStyle { get; set; }

        public ConvertToEurolookSaveOption SaveOption { get; set; }
    }
}
