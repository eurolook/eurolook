﻿namespace Eurolook.DocumentProcessing.DocumentCreation
{
    public enum ConvertToEurolookSaveOption
    {
        NewDocument,
        CurrentDocument,
    }
}
