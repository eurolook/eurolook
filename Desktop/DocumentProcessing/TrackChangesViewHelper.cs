using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Eurolook.Common.Log;
using Microsoft.Office.Interop.Word;

namespace Eurolook.DocumentProcessing
{
    public sealed class TrackChangesViewHelper : IDisposable, ICanLog
    {
        private readonly List<TrackChangesViewSettings>
            _originalViewSettings = new List<TrackChangesViewSettings>();

        public TrackChangesViewHelper(
            Document document,
            WdRevisionsMode revisionsMode,
            WdRevisionsView revisionsView,
            bool showRevisionsAndComments)
        {
            foreach (var window in document.Windows.OfType<Window>())
            {
                var trackChangesViewSettings = new TrackChangesViewSettings(
                    window.View,
                    revisionsMode,
                    revisionsView,
                    showRevisionsAndComments);
                _originalViewSettings.Add(trackChangesViewSettings);
            }
        }

        public void Dispose()
        {
            try
            {
                foreach (var view in _originalViewSettings)
                {
                    view.RestoreSettings();
                }
            }
            catch (COMException e)
            {
                this.LogWarn(e);
            }
        }

        private class TrackChangesViewSettings : ICanLog
        {
            public TrackChangesViewSettings(
                View view,
                WdRevisionsMode revisionsMode,
                WdRevisionsView revisionsView,
                bool showRevisionsAndComments)
            {
                View = view;
                RevisionsMode = view.RevisionsMode;
                RevisionsView = view.RevisionsView;
                ShowRevisionsAndComments = view.ShowRevisionsAndComments;

                view.RevisionsMode = revisionsMode;
                view.RevisionsView = revisionsView;
                view.ShowRevisionsAndComments = showRevisionsAndComments;
            }

            public View View { get; }

            public WdRevisionsMode RevisionsMode { get; }

            public WdRevisionsView RevisionsView { get; }

            public bool ShowRevisionsAndComments { get; }

            public void RestoreSettings()
            {
                try
                {
                    View.RevisionsMode = RevisionsMode;
                    View.RevisionsView = RevisionsView;
                    View.ShowRevisionsAndComments = ShowRevisionsAndComments;
                }
                catch (COMException e)
                {
                    this.LogWarn(e);
                }
            }
        }
    }
}
