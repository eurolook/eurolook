using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Microsoft.Office.Interop.Word;

namespace Eurolook.DocumentProcessing
{
    public class FontComparer : IEqualityComparer<Font>
    {
        [SuppressMessage("ReSharper", "CompareOfFloatsByEqualityOperator", Justification = "Exact comparison is ok here.")]
        [SuppressMessage("SonarQube", "S1244:Floating point numbers should not be tested for equality", Justification = "Exact comparison is ok here.")]
        public bool Equals(Font x, Font y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
            {
                return false;
            }

            return x.Name == y.Name
                   && x.AllCaps == y.AllCaps
                   && x.Animation == y.Animation
                   && x.Bold == y.Bold
                   && x.Color == y.Color
                   && x.ColorIndex == y.ColorIndex
                   && x.DoubleStrikeThrough == y.DoubleStrikeThrough
                   && x.Emboss == y.Emboss
                   && x.EmphasisMark == y.EmphasisMark
                   && x.Engrave == y.Engrave
                   ////&& x.Fill == y.Fill
                   && x.NumberForm == y.NumberForm
                   && x.NumberSpacing == y.NumberSpacing
                   && x.Outline == y.Outline
                   && x.Position == y.Position
                   && x.Scaling == y.Scaling
                   && x.Shadow == y.Shadow
                   && x.Size == y.Size
                   && x.SmallCaps == y.SmallCaps
                   && x.StrikeThrough == y.StrikeThrough
                   && x.StylisticSet == y.StylisticSet
                   && x.Subscript == y.Subscript
                   && x.Superscript == y.Superscript
                   && x.Underline == y.Underline
                   && x.UnderlineColor == y.UnderlineColor;
        }

        public int GetHashCode(Font obj)
        {
            return obj.GetHashCode();
        }
    }
}
