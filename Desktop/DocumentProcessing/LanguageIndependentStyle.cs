﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Eurolook.Common.Log;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.DocumentProcessing
{
    public class LanguageIndependentStyle : IEquatable<LanguageIndependentStyle>
    {
        public static readonly IReadOnlyDictionary<string, int> NeutralNameToStyleIdMap = new Dictionary<string, int>
        {
            { "List Table 7 Colorful - Accent 6", -371 },
            { "List Table 6 Colorful - Accent 6", -370 },
            { "List Table 5 Dark - Accent 6", -369 },
            { "List Table 4 - Accent 6", -368 },
            { "List Table 3 - Accent 6", -367 },
            { "List Table 2 - Accent 6", -366 },
            { "List Table 1 Light - Accent 6", -365 },
            { "List Table 7 Colorful - Accent 5", -364 },
            { "List Table 6 Colorful - Accent 5", -363 },
            { "List Table 5 Dark - Accent 5", -362 },
            { "List Table 4 - Accent 5", -361 },
            { "List Table 3 - Accent 5", -360 },
            { "List Table 2 - Accent 5", -359 },
            { "List Table 1 Light - Accent 5", -358 },
            { "List Table 7 Colorful - Accent 4", -357 },
            { "List Table 6 Colorful - Accent 4", -356 },
            { "List Table 5 Dark - Accent 4", -355 },
            { "List Table 4 - Accent 4", -354 },
            { "List Table 3 - Accent 4", -353 },
            { "List Table 2 - Accent 4", -352 },
            { "List Table 1 Light - Accent 4", -351 },
            { "List Table 7 Colorful - Accent 3", -350 },
            { "List Table 6 Colorful - Accent 3", -349 },
            { "List Table 5 Dark - Accent 3", -348 },
            { "List Table 4 - Accent 3", -347 },
            { "List Table 3 - Accent 3", -346 },
            { "List Table 2 - Accent 3", -345 },
            { "List Table 1 Light - Accent 3", -344 },
            { "List Table 7 Colorful - Accent 2", -343 },
            { "List Table 6 Colorful - Accent 2", -342 },
            { "List Table 5 Dark - Accent 2", -341 },
            { "List Table 4 - Accent 2", -340 },
            { "List Table 3 - Accent 2", -339 },
            { "List Table 2 - Accent 2", -338 },
            { "List Table 1 Light - Accent 2", -337 },
            { "List Table 7 Colorful - Accent 1", -336 },
            { "List Table 6 Colorful - Accent 1", -335 },
            { "List Table 5 Dark - Accent 1", -334 },
            { "List Table 4 - Accent 1", -333 },
            { "List Table 3 - Accent 1", -332 },
            { "List Table 2 - Accent 1", -331 },
            { "List Table 1 Light - Accent 1", -330 },
            { "List Table 7 Colorful", -329 },
            { "List Table 6 Colorful", -328 },
            { "List Table 5 Dark", -327 },
            { "List Table 4", -326 },
            { "List Table 3", -325 },
            { "List Table 2", -324 },
            { "List Table 1 Light", -323 },
            { "Grid Table 7 Colorful - Accent 6", -322 },
            { "Grid Table 6 Colorful - Accent 6", -321 },
            { "Grid Table 5 Dark - Accent 6", -320 },
            { "Grid Table 4 - Accent 6", -319 },
            { "Grid Table 3 - Accent 6", -318 },
            { "Grid Table 2 - Accent 6", -317 },
            { "Grid Table 1 Light - Accent 6", -316 },
            { "Grid Table 7 Colorful - Accent 5", -315 },
            { "Grid Table 6 Colorful - Accent 5", -314 },
            { "Grid Table 5 Dark - Accent 5", -313 },
            { "Grid Table 4 - Accent 5", -312 },
            { "Grid Table 3 - Accent 5", -311 },
            { "Grid Table 2 - Accent 5", -310 },
            { "Grid Table 1 Light - Accent 5", -309 },
            { "Grid Table 7 Colorful - Accent 4", -308 },
            { "Grid Table 6 Colorful - Accent 4", -307 },
            { "Grid Table 5 Dark - Accent 4", -306 },
            { "Grid Table 4 - Accent 4", -305 },
            { "Grid Table 3 - Accent 4", -304 },
            { "Grid Table 2 - Accent 4", -303 },
            { "Grid Table 1 Light - Accent 4", -302 },
            { "Grid Table 7 Colorful - Accent 3", -301 },
            { "Grid Table 6 Colorful - Accent 3", -300 },
            { "Grid Table 5 Dark - Accent 3", -299 },
            { "Grid Table 4 - Accent 3", -298 },
            { "Grid Table 3 - Accent 3", -297 },
            { "Grid Table 2 - Accent 3", -296 },
            { "Grid Table 1 Light - Accent 3", -295 },
            { "Grid Table 7 Colorful - Accent 2", -294 },
            { "Grid Table 6 Colorful - Accent 2", -293 },
            { "Grid Table 5 Dark - Accent 2", -292 },
            { "Grid Table 4 - Accent 2", -291 },
            { "Grid Table 3 - Accent 2", -290 },
            { "Grid Table 2 - Accent 2", -289 },
            { "Grid Table 1 Light - Accent 2", -288 },
            { "Grid Table 7 Colorful - Accent 1", -287 },
            { "Grid Table 6 Colorful - Accent 1", -286 },
            { "Grid Table 5 Dark - Accent 1", -285 },
            { "Grid Table 4 - Accent 1", -284 },
            { "Grid Table 3 - Accent 1", -283 },
            { "Grid Table 2 - Accent 1", -282 },
            { "Grid Table 1 Light - Accent 1", -281 },
            { "Grid Table 7 Colorful", -280 },
            { "Grid Table 6 Colorful", -279 },
            { "Grid Table 5 Dark", -278 },
            { "Grid Table 4", -277 },
            { "Grid Table 3", -276 },
            { "Grid Table 2", -275 },
            { "Grid Table 1 Light", -274 },
            { "Table Grid Light", -273 },
            { "Plain Table 5", -272 },
            { "Plain Table 4", -271 },
            { "Plain Table 3", -270 },
            { "Plain Table 2", -269 },
            { "Plain Table 1", -268 },
            { "TOC Heading", -267 },
            { "Bibliography", -266 },
            { "Book Title", -265 },
            { "Intense Reference", -264 },
            { "Subtle Reference", -263 },
            { "Intense Emphasis", -262 },
            { "Subtle Emphasis", -261 },
            { "Colorful Grid - Accent 6", -260 },
            { "Colorful List - Accent 6", -259 },
            { "Colorful Shading - Accent 6", -258 },
            { "Dark List - Accent 6", -257 },
            { "Medium Grid 3 - Accent 6", -256 },
            { "Medium Grid 2 - Accent 6", -255 },
            { "Medium Grid 1 - Accent 6", -254 },
            { "Medium List 2 - Accent 6", -253 },
            { "Medium List 1 - Accent 6", -252 },
            { "Medium Shading 2 - Accent 6", -251 },
            { "Medium Shading 1 - Accent 6", -250 },
            { "Light Grid - Accent 6", -249 },
            { "Light List - Accent 6", -248 },
            { "Light Shading - Accent 6", -247 },
            { "Colorful Grid - Accent 5", -246 },
            { "Colorful List - Accent 5", -245 },
            { "Colorful Shading - Accent 5", -244 },
            { "Dark List - Accent 5", -243 },
            { "Medium Grid 3 - Accent 5", -242 },
            { "Medium Grid 2 - Accent 5", -241 },
            { "Medium Grid 1 - Accent 5", -240 },
            { "Medium List 2 - Accent 5", -239 },
            { "Medium List 1 - Accent 5", -238 },
            { "Medium Shading 2 - Accent 5", -237 },
            { "Medium Shading 1 - Accent 5", -236 },
            { "Light Grid - Accent 5", -235 },
            { "Light List - Accent 5", -234 },
            { "Light Shading - Accent 5", -233 },
            { "Colorful Grid - Accent 4", -232 },
            { "Colorful List - Accent 4", -231 },
            { "Colorful Shading - Accent 4", -230 },
            { "Dark List - Accent 4", -229 },
            { "Medium Grid 3 - Accent 4", -228 },
            { "Medium Grid 2 - Accent 4", -227 },
            { "Medium Grid 1 - Accent 4", -226 },
            { "Medium List 2 - Accent 4", -225 },
            { "Medium List 1 - Accent 4", -224 },
            { "Medium Shading 2 - Accent 4", -223 },
            { "Medium Shading 1 - Accent 4", -222 },
            { "Light Grid - Accent 4", -221 },
            { "Light List - Accent 4", -220 },
            { "Light Shading - Accent 4", -219 },
            { "Colorful Grid - Accent 3", -218 },
            { "Colorful List - Accent 3", -217 },
            { "Colorful Shading - Accent 3", -216 },
            { "Dark List - Accent 3", -215 },
            { "Medium Grid 3 - Accent 3", -214 },
            { "Medium Grid 2 - Accent 3", -213 },
            { "Medium Grid 1 - Accent 3", -212 },
            { "Medium List 2 - Accent 3", -211 },
            { "Medium List 1 - Accent 3", -210 },
            { "Medium Shading 2 - Accent 3", -209 },
            { "Medium Shading 1 - Accent 3", -208 },
            { "Light Grid - Accent 3", -207 },
            { "Light List - Accent 3", -206 },
            { "Light Shading - Accent 3", -205 },
            { "Colorful Grid - Accent 2", -204 },
            { "Colorful List - Accent 2", -203 },
            { "Colorful Shading - Accent 2", -202 },
            { "Dark List - Accent 2", -201 },
            { "Medium Grid 3 - Accent 2", -200 },
            { "Medium Grid 2 - Accent 2", -199 },
            { "Medium Grid 1 - Accent 2", -198 },
            { "Medium List 2 - Accent 2", -197 },
            { "Medium List 1 - Accent 2", -196 },
            { "Medium Shading 2 - Accent 2", -195 },
            { "Medium Shading 1 - Accent 2", -194 },
            { "Light Grid - Accent 2", -193 },
            { "Light List - Accent 2", -192 },
            { "Light Shading - Accent 2", -191 },
            { "Colorful Grid - Accent 1", -190 },
            { "Colorful List - Accent 1", -189 },
            { "Colorful Shading - Accent 1", -188 },
            { "Dark List - Accent 1", -187 },
            { "Medium Grid 3 - Accent 1", -186 },
            { "Medium Grid 2 - Accent 1", -185 },
            { "Medium Grid 1 - Accent 1", -184 },
            { "Medium List 2 - Accent 1", -183 },
            { "Intense Quote", -182 },
            { "Quote", -181 },
            { "List Paragraph", -180 },
            { "Revision", -179 },
            { "Medium List 1 - Accent 1", -178 },
            { "Medium Shading 2 - Accent 1", -177 },
            { "Medium Shading 1 - Accent 1", -176 },
            { "Light Grid - Accent 1", -175 },
            { "Light List - Accent 1", -174 },
            { "Light Shading - Accent 1", -173 },
            { "Colorful Grid", -172 },
            { "Colorful List", -171 },
            { "Colorful Shading", -170 },
            { "Dark List", -169 },
            { "Medium Grid 3", -168 },
            { "Medium Grid 2", -167 },
            { "Medium Grid 1", -166 },
            { "Medium List 2", -165 },
            { "Medium List 1", -164 },
            { "Medium Shading 2", -163 },
            { "Medium Shading 1", -162 },
            { "Light Grid", -161 },
            { "Light List", -160 },
            { "Light Shading", -159 },
            { "No Spacing", -158 },
            { "Placeholder Text", -157 },
            { "Table Theme", -156 },
            { "Table Grid", -155 },
            { "Balloon Text", -154 },
            { "Table Web 3", -153 },
            { "Table Web 2", -152 },
            { "Table Web 1", -151 },
            { "Table Subtle 2", -150 },
            { "Table Subtle 1", -149 },
            { "Table Professional", -148 },
            { "Table Elegant", -147 },
            { "Table Contemporary", -146 },
            { "Table 3D effects 3", -145 },
            { "Table 3D effects 2", -144 },
            { "Table 3D effects 1", -143 },
            { "Table List 8", -142 },
            { "Table List 7", -141 },
            { "Table List 6", -140 },
            { "Table List 5", -139 },
            { "Table List 4", -138 },
            { "Table List 3", -137 },
            { "Table List 2", -136 },
            { "Table List 1", -135 },
            { "Table Grid 8", -134 },
            { "Table Grid 7", -133 },
            { "Table Grid 6", -132 },
            { "Table Grid 5", -131 },
            { "Table Grid 4", -130 },
            { "Table Grid 3", -129 },
            { "Table Grid 2", -128 },
            { "Table Grid 1", -127 },
            { "Table Columns 5", -126 },
            { "Table Columns 4", -125 },
            { "Table Columns 3", -124 },
            { "Table Columns 2", -123 },
            { "Table Columns 1", -122 },
            { "Table Colorful 3", -121 },
            { "Table Colorful 2", -120 },
            { "Table Colorful 1", -119 },
            { "Table Classic 4", -118 },
            { "Table Classic 3", -117 },
            { "Table Classic 2", -116 },
            { "Table Classic 1", -115 },
            { "Table Simple 3", -114 },
            { "Table Simple 2", -113 },
            { "Table Simple 1", -112 },
            { "Article / Section", -111 },
            { "1 / 1.1 / 1.1.1", -110 },
            { "1 / a / i", -109 },
            { "No List", -108 },
            { "Comment Subject", -107 },
            { "Table Normal", -106 },
            { "HTML Variable", -105 },
            { "HTML Typewriter", -104 },
            { "HTML Sample", -103 },
            { "HTML Preformatted", -102 },
            { "HTML Keyboard", -101 },
            { "HTML Definition", -100 },
            { "HTML Code", -99 },
            { "HTML Cite", -98 },
            { "HTML Address", -97 },
            { "HTML Acronym", -96 },
            { "Normal (Web)", -95 },
            { "z-Bottom of Form", -94 },
            { "z-Top of Form", -93 },
            { "E-mail Signature", -92 },
            { "Plain Text", -91 },
            { "Document Map", -90 },
            { "Emphasis", -89 },
            { "Strong", -88 },
            { "FollowedHyperlink", -87 },
            { "Hyperlink", -86 },
            { "Block Text", -85 },
            { "Body Text Indent 3", -84 },
            { "Body Text Indent 2", -83 },
            { "Body Text 3", -82 },
            { "Body Text 2", -81 },
            { "Note Heading", -80 },
            { "Body Text First Indent 2", -79 },
            { "Body Text First Indent", -78 },
            { "Date", -77 },
            { "Salutation", -76 },
            { "Subtitle", -75 },
            { "Message Header", -74 },
            { "List Continue 5", -73 },
            { "List Continue 4", -72 },
            { "List Continue 3", -71 },
            { "List Continue 2", -70 },
            { "List Continue", -69 },
            { "Body Text Indent", -68 },
            { "Body Text", -67 },
            { "Default Paragraph Font", -66 },
            { "Signature", -65 },
            { "Closing", -64 },
            { "Title", -63 },
            { "List Number 5", -62 },
            { "List Number 4", -61 },
            { "List Number 3", -60 },
            { "List Number 2", -59 },
            { "List Bullet 5", -58 },
            { "List Bullet 4", -57 },
            { "List Bullet 3", -56 },
            { "List Bullet 2", -55 },
            { "List 5", -54 },
            { "List 4", -53 },
            { "List 3", -52 },
            { "List 2", -51 },
            { "List Number", -50 },
            { "List Bullet", -49 },
            { "List", -48 },
            { "TOA Heading", -47 },
            { "Macro Text", -46 },
            { "Table of Authorities", -45 },
            { "Endnote Text", -44 },
            { "Endnote Reference", -43 },
            { "Page Number", -42 },
            { "Line Number", -41 },
            { "Comment Reference", -40 },
            { "Footnote Reference", -39 },
            { "Envelope Return", -38 },
            { "Envelope Address", -37 },
            { "Table of Figures", -36 },
            { "Caption", -35 },
            { "Index Heading", -34 },
            { "Footer", -33 },
            { "Header", -32 },
            { "Comment Text", -31 },
            { "Footnote Text", -30 },
            { "Normal Indent", -29 },
            { "TOC 9", -28 },
            { "TOC 8", -27 },
            { "TOC 7", -26 },
            { "TOC 6", -25 },
            { "TOC 5", -24 },
            { "TOC 4", -23 },
            { "TOC 3", -22 },
            { "TOC 2", -21 },
            { "TOC 1", -20 },
            { "Index 9", -19 },
            { "Index 8", -18 },
            { "Index 7", -17 },
            { "Index 6", -16 },
            { "Index 5", -15 },
            { "Index 4", -14 },
            { "Index 3", -13 },
            { "Index 2", -12 },
            { "Index 1", -11 },
            { "Heading 9", -10 },
            { "Heading 8", -9 },
            { "Heading 7", -8 },
            { "Heading 6", -7 },
            { "Heading 5", -6 },
            { "Heading 4", -5 },
            { "Heading 3", -4 },
            { "Heading 2", -3 },
            { "Heading 1", -2 },
            { "Normal", -1 },
        };

        private static readonly object StaticLock = new object();

        private string _nameLocal;
        private string _nameNeutralWithAliases;

        private LanguageIndependentStyle(Style style, Document document = null)
        {
            Style = style;
            InitializeStyleNameMaps(document ?? style.Parent as Document);
        }

        [NotNull]
        public static Dictionary<int, string> StyleIdToNeutralNameMap { get; } = new Dictionary<int, string>();

        [NotNull]
        public static Dictionary<string, int> LocalNameToStyleIdMap { get; } = new Dictionary<string, int>();

        [NotNull]
        public static Dictionary<int, string> StyleIdToLocalNameMap { get; } = new Dictionary<int, string>();

        /// <summary>
        /// Gets or sets the name of the style in the current Word UI language.
        /// </summary>
        public string NameLocal
        {
            get { return _nameLocal ??= Style.NameLocal; }
            set
            {
                _nameLocal = value;
                Style.NameLocal = value;

                _nameNeutralWithAliases = null;
            }
        }

        /// <summary>
        /// Gets the language-independent name of the style.
        /// </summary>
        public string NameNeutral
        {
            get
            {
                return NameNeutralAliases.FirstOrDefault();
            }
        }

        public IEnumerable<string> NameNeutralAliases
        {
            get
            {
                if (_nameNeutralWithAliases == null)
                {
                    _nameNeutralWithAliases = Style.NameLocal;

                    if (!string.IsNullOrEmpty(_nameNeutralWithAliases)
                        && Style.BuiltIn
                        && LocalNameToStyleIdMap.ContainsKey(_nameNeutralWithAliases))
                    {
                        int styleId = LocalNameToStyleIdMap[_nameNeutralWithAliases];
                        if (StyleIdToNeutralNameMap.ContainsKey(styleId))
                        {
                            _nameNeutralWithAliases = StyleIdToNeutralNameMap[styleId];
                        }
                    }
                }

                return _nameNeutralWithAliases.Split(new[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        public Style Style { get; }

        [CanBeNull]
        public LanguageIndependentStyle GetBaseStyle()
        {
            if (Style == null)
            {
                return null;
            }

            var baseStyle = (Style)Style.get_BaseStyle();
            return !string.IsNullOrEmpty(baseStyle.NameLocal) ? FromStyle(baseStyle) : null;
        }

        public static void InitializeStyleNameMaps(Document document)
        {
            try
            {
                if (!LocalNameToStyleIdMap.Any())
                {
                    lock (StaticLock)
                    {
                        if (!LocalNameToStyleIdMap.Any())
                        {
                            InitializeLocalNameToStyleIdMap(document);
                        }
                    }
                }

                if (!StyleIdToNeutralNameMap.Any())
                {
                    lock (StaticLock)
                    {
                        if (!StyleIdToNeutralNameMap.Any())
                        {
                            InitializeStyleIdToNeutralNameMap();
                        }
                    }
                }
            }
            catch (COMException ex)
            {
                LogManager.GetLogger().Error($"Could not initialize style name maps: {ex.Message}", ex);
            }
        }

        [NotNull]
        public static LanguageIndependentStyle FromStyle(Style style, Document document = null)
        {
            return new LanguageIndependentStyle(style, document);
        }

        [CanBeNull]
        public static LanguageIndependentStyle FromParagraph(Paragraph paragraph)
        {
            if (paragraph == null)
            {
                throw new ArgumentNullException(nameof(paragraph));
            }

            try
            {
                // ReSharper disable once UseIndexedProperty
                var style = paragraph.get_Style();
                return FromStyle(style, paragraph.Range.Document);
            }
            catch (COMException)
            {
                return null;
            }
        }

        [CanBeNull]
        public static LanguageIndependentStyle FromTable(Table table)
        {
            if (table == null)
            {
                throw new ArgumentNullException(nameof(table));
            }

            try
            {
                // ReSharper disable once UseIndexedProperty
                var style = table.get_Style();
                return FromStyle(style, table.Range.Document);
            }
            catch (COMException)
            {
                return null;
            }
        }

        [CanBeNull]
        public static LanguageIndependentStyle FromRange(Range range)
        {
            if (range == null)
            {
                throw new ArgumentNullException(nameof(range));
            }

            try
            {
                // ReSharper disable once UseIndexedProperty
                var style = range.get_Style();
                return FromStyle(style, range.Document);
            }
            catch (COMException)
            {
                return null;
            }
        }

        /// <summary>
        /// FromNeutralStyleName returns the style for the given neutral or localized style name.
        /// </summary>
        /// <remarks>
        /// To access built-in styles in a way that is independent of Word's UI
        /// language we map the English style name to the ID of that style.
        /// </remarks>
        /// <param name="styleName">The neutral or localized style name or the style index.</param>
        /// <param name="document">A Word document.</param>
        /// <returns>The language independent style for the given name.</returns>
        [CanBeNull]
        public static LanguageIndependentStyle FromNeutralStyleName(string styleName, Document document)
        {
            try
            {
                if (string.IsNullOrEmpty(styleName))
                {
                    return null;
                }

                object styleNameOrId = styleName;

                // ReSharper disable InconsistentlySynchronizedField
                if (NeutralNameToStyleIdMap.ContainsKey(styleName))
                {
                    styleNameOrId = NeutralNameToStyleIdMap[styleName];
                }

                // ReSharper restore InconsistentlySynchronizedField
                // ReSharper disable once UseIndexedProperty
                var style = GetStyle(document, styleNameOrId);
                return style == null ? null : FromStyle(style, document);
            }
            catch (COMException)
            {
                return null;
            }
        }

        public static bool IsStyleAvailable(string styleName, Document document)
        {
            return FromNeutralStyleName(styleName, document) != null;
        }

        public static bool IsStyleVisible(string styleName, Document document)
        {
            var style = FromNeutralStyleName(styleName, document);

            return style != null && !(!style.Style.UnhideWhenUsed && style.Style.Visibility);
        }

        public static Style GetStyle(Document document, object styleNameOrId)
        {
            return GetStyle(document.Styles, styleNameOrId);
        }

        public static Style GetStyle(Styles styles, object styleNameOrId)
        {
            try
            {
                // ReSharper disable once UseIndexedProperty
                return styles.get_Item(styleNameOrId);
            }
            catch (COMException)
            {
                return null;
            }
        }

        public static bool operator ==(LanguageIndependentStyle left, LanguageIndependentStyle right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(LanguageIndependentStyle left, LanguageIndependentStyle right)
        {
            return !Equals(left, right);
        }

        public bool Equals(LanguageIndependentStyle other)
        {
            if (other is null)
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return string.Equals(NameNeutral, other.NameNeutral, StringComparison.InvariantCultureIgnoreCase);
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            return Equals((LanguageIndependentStyle)obj);
        }

        public override int GetHashCode()
        {
            return NameNeutral != null ? StringComparer.InvariantCultureIgnoreCase.GetHashCode(NameNeutral) : 0;
        }

        private static void InitializeLocalNameToStyleIdMap(Document document)
        {
            var styles = document.Styles;
            foreach (int styleId in NeutralNameToStyleIdMap.Values)
            {
                var style = GetStyle(styles, styleId);
                if (TryGetStyleNameLocal(style, out var nameLocal))
                {
                    LocalNameToStyleIdMap[nameLocal] = styleId;
                    StyleIdToLocalNameMap[styleId] = nameLocal;
                }
            }
        }

        private static void InitializeStyleIdToNeutralNameMap()
        {
            foreach (var kvp in NeutralNameToStyleIdMap)
            {
                StyleIdToNeutralNameMap[kvp.Value] = kvp.Key;
            }
        }

        private static bool TryGetStyleNameLocal(Style style, out string nameLocal)
        {
            nameLocal = null;
            try
            {
                if (style == null)
                {
                    return false;
                }

                nameLocal = style.NameLocal;
                return true;
            }
            catch (COMException ex)
            {
                LogManager.GetLogger().Debug($"Could not get localized name from style: {ex.Message}", ex);
                return false;
            }
        }
    }
}
