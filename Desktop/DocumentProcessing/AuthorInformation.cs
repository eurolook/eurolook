using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.Xml;

namespace Eurolook.DocumentProcessing
{
    public class AuthorInformation
    {
        public AuthorInformation(
            IEnumerable<AuthorCustomXmlStoreInfo> authorCustomXmlStoreInfos,
            IReadOnlyEurolookProperties eurolookProperties)
        {
            AuthorCustomXmlStoreInfos = authorCustomXmlStoreInfos.ToList();
            CombinedPropertiesXml = new CombinedPropertiesXml(
                AuthorCustomXmlStoreInfos.Select(i => i.AuthorCustomXml).ToList(),
                eurolookProperties);
        }

        public List<AuthorCustomXmlStoreInfo> AuthorCustomXmlStoreInfos { get; }

        public CombinedPropertiesXml CombinedPropertiesXml { get; }
    }
}
