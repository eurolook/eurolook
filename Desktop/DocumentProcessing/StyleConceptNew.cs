﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Common.Extensions;
using Eurolook.DocumentProcessing.Extensions;
using JetBrains.Annotations;
using Microsoft.Office.Interop.Word;
using Range = Microsoft.Office.Interop.Word.Range;

namespace Eurolook.DocumentProcessing
{
    public static class StyleConceptNew
    {
        // StyleConcept: see how to define heading
        private static readonly HashSet<string> HeadingStyles = new HashSet<string>
        {
            "NumPar 1",
            "NumPar 2",
            "NumPar 3",
            "NumPar 4",
            "NumPar 5",
            "NumPar 6",
            "NumPar 7",
            "NumPar 8",
        };

        public static bool IsListContinue(Paragraph paragraph)
        {
            var neutralStyle = LanguageIndependentStyle.FromParagraph(paragraph);
            return !string.IsNullOrEmpty(neutralStyle?.Style?.ListTemplate?.ListLevels[neutralStyle.Style.ListLevelNumber]?.NumberFormat)
                && string.IsNullOrEmpty(paragraph.Range.ListFormat.ListString)
                && Math.Abs(paragraph.FirstLineIndent) <= 0.01;
        }

        public static bool IsListEnding(Paragraph paragraph)
        {
            var style = LanguageIndependentStyle.FromParagraph(paragraph);

            if (IsHeadingStyle(style))
            {
                return false;
            }

            return !string.IsNullOrEmpty(style?.Style?.ListTemplate?.ListLevels[style.Style.ListLevelNumber]?.NumberFormat)
                && string.IsNullOrEmpty(paragraph.Range.ListFormat.ListString)
                && FloatExtensions.AlmostEquals(paragraph.FirstLineIndent, style.Style?.ParagraphFormat.FirstLineIndent);
        }

        /// <summary>
        /// Apply text style (preserve text level if possible) in case that the given paragraph
        /// 1. Features as style with a <see cref="ListFormat"/>.
        /// 2. Does not feature a ListString.
        /// 3. Features the same FirstLineIndent as the style.
        /// </summary>
        /// <param name="paragraph">The paragraph to check.</param>
        public static void HandleListEnding(Paragraph paragraph)
        {
            if (IsListEnding(paragraph))
            {
                var style = LanguageIndependentStyle.FromParagraph(paragraph);
                var styleNameInfo = new StyleNameInfo(style.NameNeutral);

                int currentTextLevel = RetrieveTextLevel(paragraph.Range);
                var textStyleInfo = new StyleNameInfo("Text", styleNameInfo.TextLevel);

                textStyleInfo = GetAvailableStyleWithMaximumTextLevel(textStyleInfo, paragraph.Range.Document);
                var textStyle = LanguageIndependentStyle.FromNeutralStyleName(textStyleInfo.StyleName, paragraph.Range.Document);
                ApplyStyleToParagraph(textStyle, paragraph);
            }
        }

        public static bool IsListParagraph(Paragraph paragraph)
        {
            var style = LanguageIndependentStyle.FromParagraph(paragraph);
            return IsListStyle(style, paragraph.Range.Document);
        }

        public static bool IsListStyle(LanguageIndependentStyle style, Document document)
        {
            if (IsHeadingStyle(style))
            {
                return false;
            }

            var styleNameInfo = StyleNameInfo.FromStyleName(style.NameNeutral);
            if (style.Style.ListLevelNumber > 0)
            {
                return true;
            }

            var listLevel2 = new StyleNameInfo(styleNameInfo.Category, styleNameInfo.TextLevel, 2);
            return IsStyleAvailable(listLevel2.StyleName, document);
        }

        public static bool ListStyleIsText(LanguageIndependentStyle style, Paragraph paragraph)
        {
            if (!IsListStyle(style, paragraph.Range.Document))
            {
                return false;
            }

            if (!IsListContinue(paragraph))
            {
                return false;
            }

            string paraText = paragraph.Range.Text;
            return string.IsNullOrWhiteSpace(paraText);
        }

        public static bool IsHeadingStyle(LanguageIndependentStyle style)
        {
            return style.Style.Type == WdStyleType.wdStyleTypeParagraph &&
                (HeadingStyles.Contains(style.NameNeutral) || style.Style.ParagraphFormat.OutlineLevel != WdOutlineLevel.wdOutlineLevelBodyText);
        }

        public static bool IsStyleAvailable(string styleNameOrId, Document document)
        {
            return LanguageIndependentStyle.IsStyleVisible(styleNameOrId, document);
        }

        public static void ApplyStyleToRange(LanguageIndependentStyle style, Range range, bool forceApply = true)
        {
            if (style == null)
            {
                return;
            }

            var paragraphs = range.Paragraphs.Cast<Paragraph>().ToList();
            if (paragraphs.Count > 900)
            {
                // This gives a performance boost if there is a large number of paragraphs selected,
                // e.g. when a large table is selected. However, the result might not be as accurate.

                // ReSharper disable once UseIndexedProperty
                range.ParagraphFormat.set_Style(style.Style);
                return;
            }

            foreach (var paragraph in paragraphs)
            {
                ApplyStyleToParagraph(style, paragraph, forceApply);
            }
        }

        public static List<StyleNameInfo> GetAvailableStyles(StyleNameInfo styleNameInfo, Document document)
        {
            return GetAvailableStyles(styleNameInfo, document, 9, 0);
        }

        public static List<StyleNameInfo> GetAvailableStyles(StyleNameInfo styleNameInfo, Document document, int maxTextLevel)
        {
            return GetAvailableStyles(styleNameInfo, document, maxTextLevel, 0);
        }

        public static List<StyleNameInfo> GetAvailableStyles(StyleNameInfo styleNameInfo, Document document, int maxTextLevel, int minTextLevel)
        {
            var styles = new List<StyleNameInfo>();
            var styleName = new StyleNameInfo(styleNameInfo.Category, maxTextLevel, styleNameInfo.ListLevel);

            while (styleName.TextLevel >= minTextLevel)
            {
                if (IsStyleAvailable(styleName.StyleName, document))
                {
                    styles.Add(styleName);
                    styleName = styleName.ListPromote();
                }

                styleName = styleName.TextPromote();
            }

            return styles;
        }

        [CanBeNull]
        public static StyleNameInfo GetAvailableStyleWithMaximumTextLevel(StyleNameInfo styleNameInfo, Document document)
        {
            while (styleNameInfo.TextLevel >= 0)
            {
                if (IsStyleAvailable(styleNameInfo.StyleName, document))
                {
                    return styleNameInfo;
                }

                styleNameInfo = styleNameInfo.TextPromote();
            }

            // important fallback: when this is called for a style category which starts
            // at 1 (e.g. "Heading") we might have to check text level 1
            styleNameInfo = new StyleNameInfo(styleNameInfo.Category, 1);
            if (IsStyleAvailable(styleNameInfo.StyleName, document))
            {
                return styleNameInfo;
            }

            return null;
        }

        public static void ApplyStyleToParagraphRespectListContinue(LanguageIndependentStyle style, Paragraph paragraph)
        {
            if (style == null)
            {
                return;
            }

            var rangeStart = paragraph.Range.Duplicate;
            rangeStart.Collapse(WdCollapseDirection.wdCollapseStart);
            if (rangeStart.IsEndOfRowMark)
            {
                return;
            }

            if (LanguageIndependentStyle.FromParagraph(paragraph).NameNeutral != style.NameNeutral)
            {
                bool isListContinue = IsListContinue(paragraph) && IsListStyle(style, rangeStart.Document);

                // ReSharper disable once UseIndexedProperty
                paragraph.set_Style(style.Style);

                if (isListContinue)
                {
                    ApplyListContinue(paragraph);
                }
            }
        }

        public static void ApplyStyleToParagraph(LanguageIndependentStyle style, Paragraph paragraph, bool forceApply = false)
        {
            if (style == null)
            {
                return;
            }

            var rangeStart = paragraph.Range.Duplicate;
            rangeStart.Collapse(WdCollapseDirection.wdCollapseStart);
            if (rangeStart.IsEndOfRowMark)
            {
                return;
            }

            if (forceApply || !style.NameNeutral.Equals(LanguageIndependentStyle.FromParagraph(paragraph)?.NameNeutral))
            {
                paragraph.set_Style(style.Style);
            }
        }

        public static void ApplyStyleToTable(LanguageIndependentStyle style, Table table, bool forceApply = false)
        {
            if (style == null)
            {
                return;
            }

            if (forceApply || !style.NameNeutral.Equals(LanguageIndependentStyle.FromTable(table)?.NameNeutral))
            {
                table.set_Style(style.Style);
            }
        }

        public static void ApplyListContinue(Paragraph paragraph)
        {
            paragraph.Range.ListFormat.RemoveNumbers();
            paragraph.Range.ParagraphFormat.FirstLineIndent = 0;
            paragraph.Range.ParagraphFormat.LeftIndent = paragraph.get_Style().ParagraphFormat.LeftIndent;
        }

        /// <summary>
        /// Computes the text level for the specified paragraph.
        /// The text level is defined by the last preceding heading.
        /// To find the text level for a text range, the method iterates backwards
        /// over the paragraphs and returns the text level of the first heading found or 0 otherwise.
        /// </summary>
        /// <param name="range">The range.</param>
        /// <returns>The text level.</returns>
        public static int RetrieveTextLevel(Range range)
        {
            if (range == null)
            {
                return 0;
            }

            var paragraph = range.Paragraphs.First;
            var previousParagraph = range.Paragraphs.First.Previous();
            while (previousParagraph != null)
            {
                if (previousParagraph.Range.HasTables() && !IsInSameTableCell(paragraph, previousParagraph))
                {
                    previousParagraph = previousParagraph.Range.Tables[1].Range.Paragraphs.First;
                }

                // check if the previous paragraph is a heading and return it's text level
                var style = LanguageIndependentStyle.FromParagraph(previousParagraph);
                if (IsHeadingStyle(style))
                {
                    var styleInfo = StyleNameInfo.FromStyleName(style.NameNeutral);
                    return styleInfo.TextLevel;
                }

                paragraph = previousParagraph;
                previousParagraph = previousParagraph.Previous();
            }

            return 0;
        }

        public static bool IsInSameTableCell(Paragraph p1, Paragraph p2)
        {
            if (!p1.Range.HasTables() || !p2.Range.HasTables() || p1.Range.Cells == null || p2.Range.Cells == null)
            {
                return false;
            }

            return p1.Range?.Cells?.Count == 1 && p2.Range?.Cells?.Count == 1 && p1.Range.Cells[1].Range.Start == p2.Range.Cells[1].Range.Start;
        }

        /// <summary>
        /// Returns the heading level of the given style.
        /// </summary>
        /// <param name="style">The <see cref="LanguageIndependentStyle"/> to examine.</param>
        /// <returns>
        /// -1 if the style is null or if style is not a heading style,the text level of the style otherwise.
        /// </returns>
        public static int GetHeadingLevel(LanguageIndependentStyle style)
        {
            if (style == null || !IsHeadingStyle(style))
            {
                return -1;
            }

            return StyleNameInfo.FromStyleName(style.NameNeutral).TextLevel;
        }

        [CanBeNull]
        public static string GetNeutralStyleNameBasedOnTextLevel(string styleCategory, Range range)
        {
            var document = range.Document;
            int currentTextLevel = RetrieveTextLevel(range);
            var styleNameInfo = new StyleNameInfo(styleCategory, currentTextLevel);
            styleNameInfo = GetAvailableStyleWithMaximumTextLevel(styleNameInfo, document);
            if (styleNameInfo != null)
            {
                var style = LanguageIndependentStyle.FromNeutralStyleName(styleNameInfo.StyleName, document);
                return style?.NameNeutral;
            }

            return null;
        }
    }
}
