using Microsoft.Office.Interop.Word;

namespace Eurolook.DocumentProcessing
{
    public class DocumentLocationResult
    {
        public Range SearchRange { get; set; }

        public ContentControl ContentControl { get; set; }

        public Range Range { get; set; }

        public bool Found => Range != null;
    }
}
