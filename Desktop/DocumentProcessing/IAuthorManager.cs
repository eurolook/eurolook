using System;
using System.Collections.Generic;
using DocumentFormat.OpenXml.Packaging;
using Eurolook.Data.Xml;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;

namespace Eurolook.DocumentProcessing
{
    public interface IAuthorManager
    {
        CustomXMLPart GetAuthorXmlPart(Document document, Guid authorRoleId);

        CustomXMLPart GetMainAuthorXmlPart(Document document);

        /// <summary>
        /// Adds a new author CustomXML part to the document.
        /// </summary>
        CustomXMLPart AddAuthorXmlPart(Document document, AuthorCustomXml authorXml);

        /// <summary>
        /// Replaces the content of the given CustomXML part with the given AuthorCustomXml.
        /// </summary>
        void ReplaceAuthorXmlPart(CustomXMLPart part, AuthorCustomXml authorXml);

        bool HasCreatorAttribute(CustomXMLPart part);

        /// <summary>
        /// Deletes all author CustomXML parts from the document.
        /// </summary>
        void DeleteAuthorXmlParts(Document document);

        /// <summary>
        /// Deletes all author CustomXML parts from the document that are not used anymore.
        /// </summary>
        void CleanAuthorXmlParts(Document document);

        /// <summary>
        /// Gets all author CustomXML bindings from Content Controls within the range.
        /// </summary>
        IEnumerable<XMLMapping> GetAuthorXmlMappings(ContentControl contentControl);

        /// <summary>
        /// Gets all author custom XML objects from the given Content Control.
        /// </summary>
        IEnumerable<AuthorCustomXmlStoreInfo> GetAuthorCustomXmlStoreInfos(
            ContentControl contentControl,
            Document document);

        /// <summary>
        /// Gets all author custom XML objects from the given Document.
        /// </summary>
        IEnumerable<AuthorCustomXmlStoreInfo> GetAuthorCustomXmlStoreInfos(Document document);

        IEnumerable<AuthorCustomXml> GetAuthorCustomXmlDocs(Document document);

        IEnumerable<AuthorCustomXml> GetAuthorCustomXmlDocsFromPackage(WordprocessingDocument document);

        IEnumerable<AuthorCustomXmlStoreInfo> GetAuthorCustomXmlStoreInfos(WordprocessingDocument document);

        bool HasAuthorRoles(IEnumerable<AuthorCustomXml> authorCustomXmlDocs);

        AuthorInformation GenerateAuthorInformation(
            IEnumerable<CustomXMLPart> customXmlParts,
            IReadOnlyEurolookProperties eurolookProperties);

        IEnumerable<AuthorCustomXmlStoreInfo> SortByMainAuthor(
            IEnumerable<AuthorCustomXmlStoreInfo> authorCustomXmlStoreInfos,
            Document document);
    }
}
