﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;

namespace Eurolook.DocumentProcessing
{
    public class StyleNameMapper
    {
        private readonly List<Style> _styles;

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times", Justification = "Reviewed")]
        public StyleNameMapper(byte[] documentTemplate)
        {
            using (var stream = new MemoryStream())
            {
                stream.Write(documentTemplate, 0, documentTemplate.Length);
                using (var docx = WordprocessingDocument.Open(stream, true))
                {
                    var stylePart = docx.MainDocumentPart?.StyleDefinitionsPart;
                    if (stylePart == null)
                    {
                        return;
                    }

                    _styles = stylePart.Styles.ChildElements.OfType<Style>().ToList();
                }
            }
        }

        public string ToDisplayName(string internalOrDisplayName)
        {
            var style = _styles.FirstOrDefault(s => s.StyleId == internalOrDisplayName);
            return style != null ? style.StyleName.Val.Value : internalOrDisplayName;
        }

        public string ToInternalName(string internalOrDisplayName)
        {
            var style = _styles.FirstOrDefault(s => s.StyleName.Val == internalOrDisplayName);
            return style != null ? style.StyleId.Value : internalOrDisplayName;
        }
    }
}
