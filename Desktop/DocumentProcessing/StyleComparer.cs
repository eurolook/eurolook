using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Microsoft.Office.Interop.Word;

namespace Eurolook.DocumentProcessing
{
    public class StyleComparer : IEqualityComparer<Style>
    {
        [SuppressMessage("ReSharper", "CompareOfFloatsByEqualityOperator", Justification = "Exact comparison is ok here.")]
        [SuppressMessage("SonarQube", "S1244:Floating point numbers should not be tested for equality", Justification = "Exact comparison is ok here.")]
        public bool Equals(Style x, Style y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
            {
                return false;
            }

            return x.AutomaticallyUpdate == y.AutomaticallyUpdate
                   && x.BuiltIn == y.BuiltIn
                   && x.Hidden == y.Hidden
                   && x.Linked == y.Linked
                   && x.Locked == y.Locked
                   && x.NoSpaceBetweenParagraphsOfSameStyle == y.NoSpaceBetweenParagraphsOfSameStyle
                   && x.QuickStyle == y.QuickStyle
                   && x.UnhideWhenUsed == y.UnhideWhenUsed
                   && x.ListLevelNumber == y.ListLevelNumber
                   && x.Type == y.Type
                   && new ParagraphFormatComparer().Equals(x.ParagraphFormat, y.ParagraphFormat)
                   && new FontComparer().Equals(x.Font, y.Font);
        }

        public int GetHashCode(Style obj)
        {
            return obj.GetHashCode();
        }
    }
}
