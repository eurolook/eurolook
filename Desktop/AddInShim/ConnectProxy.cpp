// ConnectProxy.cpp 
#include "stdafx.h"
#include "ClrLoader.h"
#include "ConnectProxy.h"
#include <Windows.h>
#include "EtwInstrumentationManifest.h"
#include "Stopwatch.h"

// These strings are specific to the managed assembly that this shim will load.
static LPCWSTR szManagedAggregatorAssemblyName = _T("Eurolook.AddIn.Common, PublicKeyToken=a8a7ed7203d87bc9");

static LPCWSTR szAddInAssemblyName = _T("Eurolook.OfficeAddIn, PublicKeyToken=a8a7ed7203d87bc9");
static LPCWSTR szConnectClassName = _T("Eurolook.OfficeAddIn.ThisAddIn");
static LPCWSTR szAssemblyConfigName = _T("Eurolook.OfficeAddIn.dll.config");

static LPCWSTR szManagedAggregatorClassName = _T("Eurolook.AddIn.Common.AddIn.ManagedAggregator");

// "00020970-0000-0000-C000-000000000046"
DEFINE_GUID(CLSID_WORD, 0x00020970, 0x0000, 0x0000, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46);

// "000208D5-0000-0000-C000-000000000046"
DEFINE_GUID(CLSID_EXCEL, 0x000208D5, 0x0000, 0x0000, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46);

// "91493442-5A91-11CF-8700-00AA0060263B"
DEFINE_GUID(CLSID_POWERPOINT, 0x91493442, 0x5A91, 0x11CF, 0x87, 0x00, 0x00, 0xAA, 0x00, 0x60, 0x26, 0x3B);

CConnectProxy::CConnectProxy()
    : _pCLRLoader(nullptr), _pUnknownInner(nullptr)
{
}

HRESULT CConnectProxy::FinalConstruct()
{
    const auto sw = Stopwatch();

    EventRegisterDialogika_Eurolook_WordAddIn();

    EventWriteAddinFinalConstructBegin();
    LogDebugString(_T("CConnectProxy::FinalConstruct"));

    auto hr = S_OK;

    // Instantiate the CLR-loader object.
    _pCLRLoader = new (std::nothrow) CClrLoader();
    IfNullGo(_pCLRLoader);

    LogDebugString(_T("CConnectProxy::FinalConstruct completed in %lld ms"), sw.GetElapsedMilliseconds());
    EventWriteAddinFinalConstructEnd(sw.GetElapsedMilliseconds());

    return hr;
Error:
    EventWriteStartupHR(_T("CConnectProxy::FinalConstruct"), hr);
    return hr;
}

// Cache the pointer to the aggregated inner object, and make sure
// we increment the refcount on it.
HRESULT __stdcall CConnectProxy::SetInnerPointer(IUnknown* pUnkInner)
{
    if (pUnkInner == nullptr)
    {
        return E_POINTER;
    }

    if (_pUnknownInner != nullptr)
    {
        return E_UNEXPECTED;
    }

    LogDebugString(_T("CConnectProxy::SetInnerPointer"));
    _pUnknownInner = pUnkInner;
    _pUnknownInner->AddRef();
    return S_OK;
}

// IDTExtensibility2 implementation: OnConnection, OnAddInsUpdate and
// OnStartupComplete are simple pass-throughs to the proxied managed
// add-in. We only need to wrap IDTExtensibility2 because we need to
// add behavior to the OnBeginShutdown and OnDisconnection methods.
HRESULT __stdcall CConnectProxy::OnConnection(
    IDispatch* application,
    ext_ConnectMode connectMode,
    IDispatch* addInInst,
    SAFEARRAY** custom)
{
    const auto sw = Stopwatch();

    auto hr = S_OK;

    IUnknown* pUnkThis = nullptr;
    IfFailGo(this->QueryInterface(IID_IUnknown, reinterpret_cast<LPVOID*>(&pUnkThis)));

    // Load the CLR, create an AppDomain, and instantiate the target add-in
    // and the inner aggregated object of the shim.
    LogDebugString(_T("CConnectProxy::OnConnection - loading into Office"));
    IfFailGo(_pCLRLoader->CreateAggregatedAddIn(
        pUnkThis,
        szManagedAggregatorAssemblyName,
        szManagedAggregatorClassName,
        szAddInAssemblyName,
        szConnectClassName,
        szAssemblyConfigName));

    EventWriteAddinOnConnectionBegin();
    LogDebugString(_T("CConnectProxy::OnConnection"));

    // Extract the IDTExtensibility2 interface pointer from the target add-in.
    IfFailGo(_pUnknownInner->QueryInterface(__uuidof(IDTExtensibility2), reinterpret_cast<LPVOID*>(&this->_pConnect)));
    hr = _pConnect->OnConnection(application, connectMode, addInInst, custom);

    EventWriteAddinOnConnectionEnd(hr, sw.GetElapsedMilliseconds());

    return hr;

Error:
    if (pUnkThis != nullptr)
    {
        pUnkThis->Release();
    }

    EventWriteStartupHR(_T("CConnectProxy::OnConnection"), hr);
    return hr;
}

HRESULT __stdcall CConnectProxy::OnAddInsUpdate(SAFEARRAY** custom)
{
    auto sw = Stopwatch();

    EventWriteAddinOnAddInsUpdateBegin();
    LogDebugString(_T("CConnectProxy::OnAddInsUpdate"));

    const auto hr = _pConnect->OnAddInsUpdate(custom);

    EventWriteAddinOnAddInsUpdateEnd(hr, sw.GetElapsedMilliseconds());

    return hr;
}

HRESULT __stdcall CConnectProxy::OnStartupComplete(SAFEARRAY** custom)
{
    auto sw = Stopwatch();

    EventWriteAddinOnStartupCompleteBegin();
    LogDebugString(_T("CConnectProxy::OnStartupComplete"));

    const auto hr = _pConnect->OnStartupComplete(custom);

    EventWriteAddinOnStartupCompleteEnd(hr, sw.GetElapsedMilliseconds());

    return hr;
}

// When the host application shuts down, it calls OnBeginShutdown, 
// and then OnDisconnection. We must be careful to test that the add-in
// pointer is not null, to allow for the case where the add-in was
// previously disconnected prior to app shutdown.
HRESULT __stdcall CConnectProxy::OnBeginShutdown(SAFEARRAY** custom)
{
    auto sw = Stopwatch();

    EventWriteAddinOnBeginShutdownBegin();
    LogDebugString(_T("CConnectProxy::OnBeginShutdown"));

    auto hr = S_OK;
    if (_pConnect)
    {
        hr = _pConnect->OnBeginShutdown(custom);
    }

    EventWriteAddinOnBeginShutdownEnd(hr, sw.GetElapsedMilliseconds());

    return hr;
}

// OnDisconnection is called if the user disconnects the add-in via the COM
// add-ins dialog. We wrap this so that we can make sure we can clean up
// the reference we're holding to the inner object. We must also allow for 
// the possibility that the user has disconnected the add-in via the COM 
// add-ins dialog or programmatically: in this scenario, OnDisconnection is
// called first, and this add-in never gets the OnBeginShutdown call
// (because it has already been disconnected by then).
HRESULT __stdcall CConnectProxy::OnDisconnection(ext_DisconnectMode RemoveMode, SAFEARRAY** custom)
{
    auto sw = Stopwatch();

    EventWriteAddinOnDisconnectionBegin();
    LogDebugString(_T("CConnectProxy::OnDisconnection"));

    const auto hr = _pConnect->OnDisconnection(RemoveMode, custom);
    if (SUCCEEDED(hr))
    {
        _pConnect->Release();
        _pConnect = nullptr;
    }

    // Unload the AppDomain, and clean up.
    if (_pCLRLoader)
    {
        _pCLRLoader->Unload();
        delete _pCLRLoader;
        _pCLRLoader = nullptr;
    }

    EventWriteAddinOnDisconnectionEnd(hr, sw.GetElapsedMilliseconds());

    return hr;
}

// Make sure we unload the AppDomain, and clean up our references. 
// FinalRelease will be the last thing called in the shim/add-in, after
// OnBeginShutdown and OnDisconnection.
void CConnectProxy::FinalRelease()
{
    auto sw = Stopwatch();

    EventWriteAddinFinalReleaseBegin();
    LogDebugString(_T("CConnectProxy::FinalRelease Releasing aggregated inner object"));

    // Release the aggregated inner object.
    if (_pUnknownInner)
    {
        _pUnknownInner->Release();
        _pUnknownInner = nullptr;
    }

    EventWriteAddinFinalReleaseEnd(sw.GetElapsedMilliseconds());

    EventUnregisterDialogika_Eurolook_WordAddIn();
}
