// CLRLoader.h
#pragma once

class CClrLoader
{
public:
    CClrLoader();
    virtual ~CClrLoader();

    HRESULT CreateAggregatedAddIn(
        IUnknown* pOuter,
        LPCWSTR szManagedAggregatorAssemblyName,
        LPCWSTR szManagedAggregatorClassName,
        LPCWSTR szAssemblyName, 
        LPCWSTR szClassName, 
        LPCWSTR szAssemblyConfigName);
    HRESULT Unload();

private:
    HRESULT LoadClr();
    HRESULT CreateAppDomain(LPCWSTR szAssemblyConfigName);

    ICorRuntimeHost *_pCorRuntimeHost{};
    mscorlib::_AppDomain *_pAppDomain{};
};
