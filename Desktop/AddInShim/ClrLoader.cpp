#include "stdafx.h"
#include "CLRLoader.h"
#include "metahost.h"  // CLR 40 hosting interfaces
#include "EtwInstrumentationManifest.h"
#include "Stopwatch.h"

static LPCWSTR g_wszAssemblyFileName = L"Eurolook.OfficeAddIn.dll";

using namespace mscorlib;

static HRESULT GetDllDirectory(TCHAR *szPath, DWORD nPathBufferSize);
static HRESULT BindToCLR4OrAbove(ICorRuntimeHost** ppCorRuntimeHost);
static HRESULT FindLatestInstalledRuntime(ICLRMetaHost* pMetaHost, LPCWSTR wszMinVersion, ICLRRuntimeInfo** ppRuntimeInfo);
static void ParseClrVersion(LPCWSTR wszVersion, int rgiVersion[3]);
static BOOL IsClrVersionHigher(const int rgiVersion[3], const int rgiVersion2[3]);

CClrLoader::CClrLoader()
= default;

// CreateInstance: loads the CLR, creates an AppDomain, and creates an
// aggregated instance of the target managed add-in in that AppDomain.
HRESULT CClrLoader::CreateAggregatedAddIn(
    IUnknown* pOuter,
    const LPCWSTR szManagedAggregatorAssemblyName,
    const LPCWSTR szManagedAggregatorClassName,
    const LPCWSTR szAssemblyName,
    const LPCWSTR szClassName,
    const LPCWSTR szAssemblyConfigName)
{
    HRESULT hr;

    CComPtr<_ObjectHandle>                              srpObjectHandle;
    CComPtr<ManagedHelpers::IManagedAggregator >        srpManagedAggregator;
    CComPtr<IComAggregator>                             srpComAggregator;
    CComVariant                                         cvarManagedAggregator;

    // Load the CLR, and create an AppDomain for the target assembly.
    auto sw = Stopwatch();
    EventWriteClrHostingLoadingClrBegin();
    LogDebugString(_T("Loading CLR"));
    IfFailGo(LoadClr());
    EventWriteClrHostingLoadingClrEnd(sw.GetElapsedMilliseconds());

    // Create the app domain.
    sw.Restart();
    EventWriteClrHostingCreatingAppDomainBegin(szAssemblyConfigName);
    LogDebugString(_T("Creating AppDomain"));
    IfFailGo(CreateAppDomain(szAssemblyConfigName));
    EventWriteClrHostingCreatingAppDomainEnd(sw.GetElapsedMilliseconds());

    // Create the managed aggregator in the target AppDomain, and unwrap it.
    // This component needs to be in a location where fusion will find it, ie
    // either in the GAC or in the same folder as the shim and the add-in.
    sw.Restart();
    EventWriteClrHostingManagedAggregatorBegin(szManagedAggregatorAssemblyName, szManagedAggregatorClassName);
    LogDebugString(_T("Creating managed aggregator"));
    IfFailGo(_pAppDomain->CreateInstance(CComBSTR(szManagedAggregatorAssemblyName), CComBSTR(szManagedAggregatorClassName), &srpObjectHandle));
    EventWriteClrHostingManagedAggregatorEnd(sw.GetElapsedMilliseconds());
    sw.Restart();

    IfFailGo(srpObjectHandle->Unwrap(&cvarManagedAggregator));
    
    IfFailGo(cvarManagedAggregator.pdispVal->QueryInterface(&srpManagedAggregator));
    
    // Instantiate and aggregate the inner managed add-in into the outer (unmanaged, ConnectProxy) object.
    LogDebugString(_T("Instantiate and aggregate the inner managed add-in into the outer (unmanaged, ConnectProxy) object."));
    IfFailGo(pOuter->QueryInterface(__uuidof(IComAggregator), reinterpret_cast<LPVOID*>(&srpComAggregator)));
    
    IfFailGo(srpManagedAggregator->CreateAggregatedInstance(CComBSTR(szAssemblyName), CComBSTR(szClassName), srpComAggregator));
    EventWriteClrHostingManagedAggregatorEnd(sw.GetElapsedMilliseconds());

Error:
    EventWriteClrHostingHR(_T("CreateAggregatedAddIn"), hr);
    return hr;
}

// LoadCLR: loads and starts the .NET CLR.
HRESULT CClrLoader::LoadClr()
{
    auto hr = S_OK;

    // Ensure the CLR is only loaded once.
    if (_pCorRuntimeHost != nullptr)
    {
        return hr;
    }

    LogDebugString(_T("Load and start the .NET runtime"));

    hr = BindToCLR4OrAbove(&_pCorRuntimeHost);
    if (FAILED(hr))
    {
        EventWriteClrHostingHR(_T("Binding to CLR failed"), hr);
        return hr;
    }

    // Start the CLR.
    return _pCorRuntimeHost->Start();
}

// In order to securely load an assembly, its fully qualified strong name
// and not the filename must be used. To do that, the target AppDomain's
// base directory needs to point to the directory where the assembly is.
HRESULT CClrLoader::CreateAppDomain(LPCWSTR szAssemblyConfigName)
{
    USES_CONVERSION;
    auto hr = S_OK;

    // Ensure the AppDomain is created only once.
    if (_pAppDomain != nullptr)
    {
        return hr;
    }

    CComPtr<IUnknown> pUnkDomainSetup;
    CComPtr<IAppDomainSetup> pDomainSetup;
    CComPtr<IUnknown> pUnkAppDomain;
    TCHAR szDirectory[MAX_PATH + 1];
    TCHAR szAssemblyConfigPath[MAX_PATH + 1];
    CComBSTR cbstrAssemblyConfigPath;

    // Create an AppDomainSetup with the base directory pointing to the
    // location of the managed DLL. We assume that the target assembly
    // is located in the same directory.
    IfFailGo(_pCorRuntimeHost->CreateDomainSetup(&pUnkDomainSetup));
    IfFailGo(pUnkDomainSetup->QueryInterface(__uuidof(pDomainSetup), reinterpret_cast<LPVOID*>(&pDomainSetup)));

    // Get the location of the hosting shim DLL, and configure the
    // AppDomain to search for assemblies in this location.
    IfFailGo(GetDllDirectory(szDirectory, sizeof(szDirectory) / sizeof(szDirectory[0])));
    pDomainSetup->put_ApplicationBase(CComBSTR(szDirectory));

    // Set the AppDomain to use a local DLL config if there is one.
    IfFailGo(StringCchCopy(
        szAssemblyConfigPath,
        sizeof(szAssemblyConfigPath) / sizeof(szAssemblyConfigPath[0]),
        szDirectory));
    if (!PathAppend(szAssemblyConfigPath, szAssemblyConfigName))
    {
        hr = E_UNEXPECTED;
        goto Error;
    }

    IfFailGo(cbstrAssemblyConfigPath.Append(szAssemblyConfigPath));
    IfFailGo(pDomainSetup->put_ConfigurationFile(cbstrAssemblyConfigPath));

    // Create an AppDomain that will run the managed assembly, and get the
    // AppDomain's _AppDomain pointer from its IUnknown pointer.
    IfFailGo(_pCorRuntimeHost->CreateDomainEx(T2W(szDirectory), pUnkDomainSetup, nullptr, &pUnkAppDomain));
    IfFailGo(pUnkAppDomain->QueryInterface(__uuidof(_pAppDomain), reinterpret_cast<LPVOID*>(&_pAppDomain)));

Error:
    EventWriteClrHostingHR(_T("CreateAppDomain"), hr);
    return hr;
}

static HRESULT BindToCLR4OrAbove(ICorRuntimeHost** ppCorRuntimeHost)
{
    HRESULT hr;
    CComPtr<ICLRMetaHost> srpMetaHost;
    CComPtr<ICLRRuntimeInfo> srpRuntimeInfo;
    WCHAR rgwchPath[MAX_PATH + 1];
    TCHAR rgwchVersion[30];
    DWORD cwchVersion = ARRAYSIZE(rgwchVersion);

    *ppCorRuntimeHost = nullptr;

    IfFailGo(CLRCreateInstance(CLSID_CLRMetaHost, IID_ICLRMetaHost, reinterpret_cast<void**>(&srpMetaHost)));

    // Get the location of the hosting shim DLL, and retrieve the required
    // CLR runtime version from its metadata
    IfFailGo(GetDllDirectory(rgwchPath, ARRAYSIZE(rgwchPath)));
    if (!PathAppend(rgwchPath, g_wszAssemblyFileName))
    {
        hr = E_UNEXPECTED;
        goto Error;
    }

    IfFailGo(srpMetaHost->GetVersionFromFile(rgwchPath, rgwchVersion, &cwchVersion));
    LogDebugString(_T("Add-in is built for .NET runtime version %s"), rgwchVersion);

    // First try binding to the same version of CLR the add-in is built against
    hr = srpMetaHost->GetRuntime(rgwchVersion, IID_ICLRRuntimeInfo, reinterpret_cast<void**>(&srpRuntimeInfo));
    if (FAILED(hr))
    {
        // If we're here - it means the exact same version of the CLR we built against is not available.
        // In this case we will just load the highest compatible version
        EventWriteClrHostingHR(_T("ICLRMetaHost::GetRuntime"), hr);
        srpRuntimeInfo.Release();
        IfFailGo(FindLatestInstalledRuntime(srpMetaHost, rgwchVersion, &srpRuntimeInfo));
    }

    // we ignore the result of SetDefaultStartupFlags - this is not critical operation
    srpRuntimeInfo->SetDefaultStartupFlags(STARTUP_LOADER_OPTIMIZATION_MULTI_DOMAIN_HOST, nullptr);

    IfFailGo(srpRuntimeInfo->GetInterface(CLSID_CorRuntimeHost, IID_ICorRuntimeHost, reinterpret_cast<void**>(ppCorRuntimeHost)));

Error:
    EventWriteClrHostingHR(_T("BindToCLR4OrAbove"), hr);
    return hr;
}

static HRESULT FindLatestInstalledRuntime(ICLRMetaHost* pMetaHost, LPCWSTR wszMinVersion, ICLRRuntimeInfo** ppRuntimeInfo)
{
    CComPtr<IEnumUnknown> srpEnum;
    CComPtr<ICLRRuntimeInfo> srpRuntimeInfo;
    CComPtr<ICLRRuntimeInfo> srpLatestRuntimeInfo;
    ULONG cFetched;
    WCHAR rgwchVersion[30];
    DWORD cwchVersion;
    int rgiMinVersion[3]; //Major.Minor.Build
    int rgiVersion[3]; // Major.Minor.Build
    HRESULT hr;

    *ppRuntimeInfo = nullptr;

    // convert vN.N.N into an array of numbers
    ParseClrVersion(wszMinVersion, rgiMinVersion);

    IfFailGo(pMetaHost->EnumerateInstalledRuntimes(&srpEnum));
    while (true)
    {
        srpRuntimeInfo.Release();
        IfFailGo(srpEnum->Next(1, reinterpret_cast<IUnknown**>(&srpRuntimeInfo), &cFetched));
        if (hr == S_FALSE)
            break;

        cwchVersion = ARRAYSIZE(rgwchVersion);
        IfFailGo(srpRuntimeInfo->GetVersionString(rgwchVersion, &cwchVersion));

        ParseClrVersion(rgwchVersion, rgiVersion);
        if (IsClrVersionHigher(rgiVersion, rgiMinVersion) == FALSE)
        {
            continue;
        }

        rgiMinVersion[0] = rgiVersion[0];
        rgiMinVersion[1] = rgiVersion[1];
        rgiMinVersion[2] = rgiVersion[2];

        srpLatestRuntimeInfo.Attach(srpRuntimeInfo.Detach());
    }

    if (srpLatestRuntimeInfo == nullptr)
    {
        hr = E_FAIL;
        goto Error;
    }

    hr = S_OK;
    *ppRuntimeInfo = srpLatestRuntimeInfo.Detach();

Error:
    return hr;
}

// Convert "vN.N.N" into an array of numbers
static void ParseClrVersion(LPCWSTR wszVersion, int rgiVersion[3])
{
    rgiVersion[0] = rgiVersion[1] = rgiVersion[2] = 0;

    auto pwch = wszVersion;
    for (int i = 0; i < 3; i++)
    {
        // skip the first character - either 'v' or '.' and add the numbers
        for (pwch++; L'0' <= *pwch && *pwch <= L'9'; pwch++)
        {
            rgiVersion[i] = rgiVersion[i] * 10 + *pwch - L'0';
        }

        if (*pwch == 0)
        {
            break;
        }

        assert(*pwch == L'.' && _T("we should expect a period. Otherwise it is not a proper CLR version string"));
        if (*pwch != L'.')
        {
            // the input is invalid - do not parse any further
            break;
        }
    }
}

// compare order of CLR versions represented as array of numbers
static BOOL IsClrVersionHigher(const int rgiVersion[3], const int rgiVersion2[3])
{
    for (int i = 0; i < 3; i++)
    {
        if (rgiVersion[i] != rgiVersion2[i])
        {
            return rgiVersion[i] > rgiVersion2[i];
        }
    }

    return FALSE;
}

// GetDllDirectory: gets the directory location of the DLL containing this
// code - that is, the shim DLL. The target add-in DLL will also be in this
// directory.
static HRESULT GetDllDirectory(TCHAR *szPath, DWORD nPathBufferSize)
{
    // Get the shim DLL module instance, or bail.
    const auto hInstance = _AtlBaseModule.GetModuleInstance();
    if (hInstance == nullptr)
    {
        return E_FAIL;
    }

    // Get the shim DLL filename, or bail.
    TCHAR szModule[MAX_PATH + 1];
    auto dwFLen = ::GetModuleFileName(hInstance, szModule, MAX_PATH);
    if (dwFLen == 0)
    {
        return E_FAIL;
    }

    // Get the full path to the shim DLL, or bail.
    TCHAR *pszFileName;
    dwFLen = ::GetFullPathName(szModule, nPathBufferSize, szPath, &pszFileName);
    if (dwFLen == 0 || dwFLen >= nPathBufferSize)
    {
        return E_FAIL;
    }

    *pszFileName = 0;
    return S_OK;
}

// Unload the AppDomain. This will be called by the ConnectProxy in OnDisconnection.
HRESULT CClrLoader::Unload()
{
    HRESULT hr;
    IUnknown* pUnkDomain = nullptr;
    IfFailGo(_pAppDomain->QueryInterface(__uuidof(IUnknown), reinterpret_cast<LPVOID*>(&pUnkDomain)));
    hr = _pCorRuntimeHost->UnloadDomain(pUnkDomain);

    // Added in 2.0.2.0, only for Add-ins.
    _pAppDomain->Release();
    _pAppDomain = nullptr;

Error:
    if (pUnkDomain != nullptr)
    {
        pUnkDomain->Release();
    }
    EventWriteClrHostingHR(_T("Unload"), hr);
    return hr;
}

CClrLoader::~CClrLoader()
{
    if (_pAppDomain)
    {
        _pAppDomain->Release();
    }

    if (_pCorRuntimeHost)
    {
        _pCorRuntimeHost->Release();
    }
}
