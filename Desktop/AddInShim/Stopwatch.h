#pragma once

class Stopwatch
{
    LARGE_INTEGER _startingTime;
    static LARGE_INTEGER Frequency;

public:
    Stopwatch();
    ~Stopwatch();

    void Start();

    void Restart();

    LONGLONG GetElapsedMilliseconds() const;
};


