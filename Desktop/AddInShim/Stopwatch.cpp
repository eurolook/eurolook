#include "stdafx.h"
#include "Stopwatch.h"
#include "EtwInstrumentationManifest.h"

LARGE_INTEGER Stopwatch::Frequency = [] {
    HRESULT hr = S_OK;
    IfFailGo(QueryPerformanceFrequency(&Frequency));
    return Frequency;

Error:
    Frequency.QuadPart = -1;
    EventWriteStartupHR(_T("Stopwatch::Frequency"), hr);
    return Frequency;
}();

Stopwatch::Stopwatch()
{
    Start();
}


Stopwatch::~Stopwatch()
= default;

void Stopwatch::Start()
{
    HRESULT hr = S_OK;
    IfFailGo(QueryPerformanceCounter(&_startingTime));
    return;

Error:
    _startingTime.QuadPart = -1;
    EventWriteStartupHR(_T("Stopwatch::Start"), hr);
}

void Stopwatch::Restart()
{
    Start();
}

LONGLONG Stopwatch::GetElapsedMilliseconds() const
{
    LARGE_INTEGER endingTime;
    LARGE_INTEGER elapsedMilliseconds;
    const HRESULT hr = QueryPerformanceCounter(&endingTime);

    if (FAILED(hr) || _startingTime.QuadPart < 0 || Frequency.QuadPart < 0)
    {
        // return a negative value if we are not able to use the performance counter API.
        return -1;
    }

    elapsedMilliseconds.QuadPart = endingTime.QuadPart - _startingTime.QuadPart;

    // We now have the elapsed number of ticks, along with the number of ticks-per-second.
    // We use these values to convert to the number of elapsed milliseconds.
    // To guard against loss-of-precision, we convert to milliseconds *before* dividing by ticks-per-second.
    elapsedMilliseconds.QuadPart *= 1000;
    elapsedMilliseconds.QuadPart /= Frequency.QuadPart;

    return elapsedMilliseconds.QuadPart;
}
