// ConnectProxy.h
#pragma once
#include "Resource.h"
#include "AddinShim.h"

using namespace AddInDesignerObjects;

class ATL_NO_VTABLE CConnectProxy :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CConnectProxy, &CLSID_ConnectProxy>,
	public IDispatchImpl<_IDTExtensibility2, &IID__IDTExtensibility2, &LIBID_AddInDesignerObjects, 1, 0>,
	public IComAggregator
{
public:
	CConnectProxy();

	DECLARE_REGISTRY_RESOURCEID(IDR_CONNECTPROXY)
	DECLARE_PROTECT_FINAL_CONSTRUCT()

	BEGIN_COM_MAP(CConnectProxy)
		COM_INTERFACE_ENTRY(IDTExtensibility2)
		COM_INTERFACE_ENTRY(IComAggregator)
		COM_INTERFACE_ENTRY_AGGREGATE_BLIND(_pUnknownInner)
	END_COM_MAP()

	HRESULT FinalConstruct();
	void FinalRelease();

public:
	//IDTExtensibility2.
	STDMETHOD(OnConnection)(IDispatch* application, ext_ConnectMode connectMode, IDispatch* addInInst, SAFEARRAY** custom);

	STDMETHOD(OnAddInsUpdate)(SAFEARRAY** custom);

	STDMETHOD(OnStartupComplete)(SAFEARRAY** custom);

	STDMETHOD(OnBeginShutdown)(SAFEARRAY** custom);

	STDMETHOD(OnDisconnection)(ext_DisconnectMode RemoveMode, SAFEARRAY** custom);

	// IComAggregator.
	STDMETHOD(SetInnerPointer)(IUnknown* pUnkInner);

private:
	IDTExtensibility2 *_pConnect{};
	CClrLoader *_pCLRLoader;
	IUnknown *_pUnknownInner;
};

OBJECT_ENTRY_AUTO(__uuidof(ConnectProxy), CConnectProxy)
