<?xml version="1.0"?>
<instrumentationManifest xsi:schemaLocation="http://schemas.microsoft.com/win/2004/08/events eventman.xsd" xmlns="http://schemas.microsoft.com/win/2004/08/events" xmlns:win="http://manifests.microsoft.com/win/2004/08/windows/events" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:trace="http://schemas.microsoft.com/win/2004/08/events/trace">
    <instrumentation>
        <events>
            <provider name="Dialogika-Eurolook-WordAddIn" guid="{8C96EC8D-5DFC-4B71-A244-C075E8B23A83}" symbol="DialogikaEurolook" resourceFileName="C:\Projects\Eurolook\eurolook\Eurolook Anywhere\Desktop\AddInShim\bin\Release\Eurolook.AddInShim.dll" messageFileName="C:\Projects\Eurolook\eurolook\Eurolook Anywhere\WordAddInShim\bin\Release\Eurolook.WordAddInShim.dll">
                <events>
                    <event symbol="AddinFinalConstructBegin" value="10" version="1" channel="Dialogika-Eurolook-WordAddIn/Operational" level="win:Informational" task="Startup" opcode="OpCodeFinalConstruct" template="Empty" keywords="Eurolook-Addin-Loading " message="$(string.Dialogika-Eurolook-WordAddIn.event.10.message)"></event>
                    <event symbol="AddinFinalConstructEnd" value="11" version="1" channel="Dialogika-Eurolook-WordAddIn/Operational" level="win:Informational" task="Startup" opcode="OpCodeFinalConstruct" template="ElapsedMilliseconds" message="$(string.Dialogika-Eurolook-WordAddIn.event.11.message)"></event>
                    <event symbol="AddinOnConnectionBegin" value="20" version="1" channel="Dialogika-Eurolook-WordAddIn/Operational" level="win:Informational" task="Startup" opcode="OpCodeOnConnection" template="Empty" keywords="Eurolook-Addin-Loading " message="$(string.Dialogika-Eurolook-WordAddIn.event.20.message)"></event>
                    <event symbol="AddinOnConnectionEnd" value="21" version="1" channel="Dialogika-Eurolook-WordAddIn/Operational" level="win:Informational" task="Startup" opcode="OpCodeOnConnection" template="HResultWithTiming" keywords="Eurolook-Addin-Loading " message="$(string.Dialogika-Eurolook-WordAddIn.event.21.message)"></event>
                    <event symbol="AddinOnAddInsUpdateBegin" value="30" version="1" channel="Dialogika-Eurolook-WordAddIn/Operational" level="win:Informational" task="Startup" opcode="OpCodeOnAddInsUpdate" template="Empty" keywords="Eurolook-Addin-Loading " message="$(string.Dialogika-Eurolook-WordAddIn.event.30.message)"></event>
                    <event symbol="AddinOnAddInsUpdateEnd" value="31" version="1" channel="Dialogika-Eurolook-WordAddIn/Operational" level="win:Informational" task="Startup" opcode="OpCodeOnAddInsUpdate" template="HResultWithTiming" keywords="Eurolook-Addin-Loading " message="$(string.Dialogika-Eurolook-WordAddIn.event.31.message)"></event>
                    <event symbol="AddinOnStartupCompleteBegin" value="40" version="1" channel="Dialogika-Eurolook-WordAddIn/Operational" level="win:Informational" task="Startup" opcode="OpCodeOnStartupComplete" template="Empty" keywords="Eurolook-Addin-Loading " message="$(string.Dialogika-Eurolook-WordAddIn.event.40.message)"></event>
                    <event symbol="AddinOnStartupCompleteEnd" value="41" version="1" channel="Dialogika-Eurolook-WordAddIn/Operational" level="win:Informational" task="Startup" opcode="OpCodeOnStartupComplete" template="HResultWithTiming" keywords="Eurolook-Addin-Loading " message="$(string.Dialogika-Eurolook-WordAddIn.event.41.message)"></event>
                    <event symbol="AddinOnBeginShutdownBegin" value="50" version="1" channel="Dialogika-Eurolook-WordAddIn/Operational" level="win:Informational" task="Shutdown" opcode="OpCodeOnBeginShutdown" template="Empty" keywords="Eurolook-Addin-Loading " message="$(string.Dialogika-Eurolook-WordAddIn.event.50.message)"></event>
                    <event symbol="AddinOnBeginShutdownEnd" value="51" version="1" channel="Dialogika-Eurolook-WordAddIn/Operational" level="win:Informational" task="Shutdown" opcode="OpCodeOnBeginShutdown" template="HResultWithTiming" keywords="Eurolook-Addin-Loading " message="$(string.Dialogika-Eurolook-WordAddIn.event.51.message)"></event>
                    <event symbol="AddinOnDisconnectionBegin" value="60" version="1" channel="Dialogika-Eurolook-WordAddIn/Operational" level="win:Informational" task="Shutdown" opcode="OpCodeOnDisconnection" template="Empty" keywords="Eurolook-Addin-Loading " message="$(string.Dialogika-Eurolook-WordAddIn.event.60.message)"></event>
                    <event symbol="AddinOnDisconnectionEnd" value="61" version="1" channel="Dialogika-Eurolook-WordAddIn/Operational" level="win:Informational" task="Shutdown" opcode="OpCodeOnDisconnection" template="HResultWithTiming" keywords="Eurolook-Addin-Loading " message="$(string.Dialogika-Eurolook-WordAddIn.event.61.message)"></event>
                    <event symbol="AddinFinalReleaseBegin" value="70" version="1" channel="Dialogika-Eurolook-WordAddIn/Operational" level="win:Informational" task="Shutdown" opcode="OpCodeFinalRelease" template="Empty" keywords="Eurolook-Addin-Loading " message="$(string.Dialogika-Eurolook-WordAddIn.event.70.message)"></event>
                    <event symbol="AddinFinalReleaseEnd" value="71" version="1" channel="Dialogika-Eurolook-WordAddIn/Operational" level="win:Informational" task="Shutdown" opcode="OpCodeFinalRelease" template="ElapsedMilliseconds" keywords="Eurolook-Addin-Loading " message="$(string.Dialogika-Eurolook-WordAddIn.event.71.message)"></event>
                    <event symbol="ClrHostingLoadingClrBegin" value="500" version="1" channel="Dialogika-Eurolook-WordAddIn/Operational" level="win:Informational" task="ClrHosting" template="Empty" keywords="Eurolook-Addin-Loading " message="$(string.Dialogika-Eurolook-WordAddIn.event.500.message)"></event>
                    <event symbol="ClrHostingLoadingClrEnd" value="501" version="1" channel="Dialogika-Eurolook-WordAddIn/Operational" level="win:Informational" task="ClrHosting" template="ElapsedMilliseconds" keywords="Eurolook-Addin-Loading " message="$(string.Dialogika-Eurolook-WordAddIn.event.501.message)"></event>
                    <event symbol="ClrHostingCreatingAppDomainBegin" value="510" version="1" channel="Dialogika-Eurolook-WordAddIn/Operational" level="win:Informational" task="ClrHosting" template="ClrHostingAppDomain" keywords="Eurolook-Addin-Loading " message="$(string.Dialogika-Eurolook-WordAddIn.event.510.message)"></event>
                    <event symbol="ClrHostingCreatingAppDomainEnd" value="511" version="1" channel="Dialogika-Eurolook-WordAddIn/Operational" level="win:Informational" task="ClrHosting" template="ElapsedMilliseconds" keywords="Eurolook-Addin-Loading " message="$(string.Dialogika-Eurolook-WordAddIn.event.511.message)"></event>
                    <event symbol="ClrHostingManagedAggregatorBegin" value="520" version="1" channel="Dialogika-Eurolook-WordAddIn/Operational" level="win:Informational" task="ClrHosting" template="ClrHostingManagedAggregator" keywords="Eurolook-Addin-Loading " message="$(string.Dialogika-Eurolook-WordAddIn.event.520.message)"></event>
                    <event symbol="ClrHostingManagedAggregatorEnd" value="521" version="1" channel="Dialogika-Eurolook-WordAddIn/Operational" level="win:Informational" task="ClrHosting" template="ElapsedMilliseconds" keywords="Eurolook-Addin-Loading " message="$(string.Dialogika-Eurolook-WordAddIn.event.521.message)"></event>
                    <event symbol="StartupErrorHResult" value="1000" version="1" channel="Dialogika-Eurolook-WordAddIn/Operational" level="win:Error" task="Startup" template="ErrorHResult" keywords="Eurolook-Addin-Loading " message="$(string.Dialogika-Eurolook-WordAddIn.event.1000.message)"></event>
                    <event symbol="ClrHostingErrorHResult" value="1001" version="1" channel="Dialogika-Eurolook-WordAddIn/Operational" level="win:Critical" task="ClrHosting" template="ErrorHResult" keywords="Eurolook-Addin-Loading " message="$(string.Dialogika-Eurolook-WordAddIn.event.1001.message)"></event>
                </events>
                <levels></levels>
                <tasks>
                    <task name="Startup" symbol="Startup" value="1" eventGUID="{011A86FC-9C96-467A-BBF4-43775DA26392}" message="$(string.Dialogika-Eurolook-WordAddIn.task.Startup.message)"></task>
                    <task name="Shutdown" symbol="Shutdown" value="2" eventGUID="{65A168D5-6B98-4A22-BF36-852CB7A9FAE5}" message="$(string.Dialogika-Eurolook-WordAddIn.task.Shutdown.message)"></task>
                    <task name="DataSync" symbol="DataSync" value="3" eventGUID="{831277CE-9D21-4028-8D62-11F53CB0B62A}" message="$(string.Dialogika-Eurolook-WordAddIn.task.DataSync.message)"></task>
                    <task name="ClrHosting" symbol="ClrHosting" value="4" eventGUID="{A0BD0D95-B673-4D89-9867-980CBEABF04C}" message="$(string.Dialogika-Eurolook-WordAddIn.task.ClrHosting.message)"></task>
                </tasks>
                <opcodes>
                    <opcode name="OpCodeFinalConstruct" symbol="OpCodeFinalConstruct" value="10" message="$(string.Dialogika-Eurolook-WordAddIn.opcode.OpCodeFinalConstruct.message)"></opcode>
                    <opcode name="OpCodeOnConnection" symbol="OpCodeOnConnection" value="20" message="$(string.Dialogika-Eurolook-WordAddIn.opcode.OpCodeOnConnection.message)"></opcode>
                    <opcode name="OpCodeOnAddInsUpdate" symbol="OpCodeOnAddInsUpdate" value="30" message="$(string.Dialogika-Eurolook-WordAddIn.opcode.OpCodeOnAddInsUpdate.message)"></opcode>
                    <opcode name="OpCodeOnStartupComplete" symbol="OpCodeOnStartupComplete" value="40" message="$(string.Dialogika-Eurolook-WordAddIn.opcode.OpCodeOnStartupComplete.message)"></opcode>
                    <opcode name="OpCodeOnBeginShutdown" symbol="OpCodeOnBeginShutdown" value="50" message="$(string.Dialogika-Eurolook-WordAddIn.opcode.OpCodeOnBeginShutdown.message)"></opcode>
                    <opcode name="OpCodeOnDisconnection" symbol="OpCodeOnDisconnection" value="60" message="$(string.Dialogika-Eurolook-WordAddIn.opcode.OpCodeOnDisconnection.message)"></opcode>
                    <opcode name="OpCodeFinalRelease" symbol="OpCodeFinalRelease" value="70" message="$(string.Dialogika-Eurolook-WordAddIn.opcode.OpCodeFinalRelease.message)"></opcode>
                </opcodes>
                <channels>
                    <channel name="Dialogika-Eurolook-WordAddIn/Operational" chid="c1" type="Operational" enabled="true"></channel>
                </channels>
                <keywords>
                    <keyword name="Eurolook-Addin-Loading" symbol="Eurolook_Addin_Loading" mask="0x800000000000" message="$(string.Dialogika-Eurolook-WordAddIn.keyword.Eurolook_Addin_Loading.message)"></keyword>
                </keywords>
                <templates>
                    <template tid="Empty"></template>
                    <template tid="ErrorHResult">
                        <data name="methodName" inType="win:UnicodeString" outType="xs:string"></data>
                        <data name="hresult" inType="win:Int64" outType="xs:long"></data>
                    </template>
                    <template tid="ClrHostingAppDomain">
                        <data name="appDomainConfigName" inType="win:UnicodeString" outType="xs:string"></data>
                    </template>
                    <template tid="ClrHostingManagedAggregator">
                        <data name="assemblyName" inType="win:UnicodeString" outType="xs:string"></data>
                        <data name="managedAggregatorClass" inType="win:UnicodeString" outType="xs:string"></data>
                    </template>
                    <template tid="ElapsedMilliseconds">
                        <data name="elapsedMilliseconds" inType="win:Int64" outType="xs:long"></data>
                    </template>
                    <template tid="HResultWithTiming">
                        <data name="hresult" inType="win:Int64" outType="xs:long"></data>
                        <data name="elapsedMilliseconds" inType="win:Int64" outType="xs:long"></data>
                    </template>
                </templates>
            </provider>
        </events>
    </instrumentation>
    <localization>
        <resources culture="en-US">
            <stringTable>
                <string id="level.Verbose" value="Verbose"></string>
                <string id="level.Informational" value="Information"></string>
                <string id="level.Error" value="Error"></string>
                <string id="level.Critical" value="Critical"></string>
                <string id="Dialogika-Eurolook-WordAddIn.task.Startup.message" value="Startup"></string>
                <string id="Dialogika-Eurolook-WordAddIn.task.Shutdown.message" value="Shutdown"></string>
                <string id="Dialogika-Eurolook-WordAddIn.task.DataSync.message" value="DataSync"></string>
                <string id="Dialogika-Eurolook-WordAddIn.task.ClrHosting.message" value="ClrHosting"></string>
                <string id="Dialogika-Eurolook-WordAddIn.opcode.OpCodeOnStartupComplete.message" value="OnStartupComplete"></string>
                <string id="Dialogika-Eurolook-WordAddIn.opcode.OpCodeOnDisconnection.message" value="OnDisconnection"></string>
                <string id="Dialogika-Eurolook-WordAddIn.opcode.OpCodeOnConnection.message" value="OnConnection"></string>
                <string id="Dialogika-Eurolook-WordAddIn.opcode.OpCodeOnBeginShutdown.message" value="OnBeginShutdown"></string>
                <string id="Dialogika-Eurolook-WordAddIn.opcode.OpCodeOnAddInsUpdate.message" value="OnAddInsUpdate"></string>
                <string id="Dialogika-Eurolook-WordAddIn.opcode.OpCodeFinalRelease.message" value="FinalRelease"></string>
                <string id="Dialogika-Eurolook-WordAddIn.opcode.OpCodeFinalConstruct.message" value="FinalConstruct"></string>
                <string id="Dialogika-Eurolook-WordAddIn.keyword.Eurolook_Addin_Loading.message" value="Eurolook-Addin-Loading"></string>
                <string id="Dialogika-Eurolook-WordAddIn.event.10.message" value="Eurolook add-in shim is initializing."></string>
                <string id="Dialogika-Eurolook-WordAddIn.event.11.message" value="Eurolook add-in shim initialization has been completed in %1 ms."></string>
                <string id="Dialogika-Eurolook-WordAddIn.event.20.message" value="Connecting Eurolook add-in."></string>
                <string id="Dialogika-Eurolook-WordAddIn.event.21.message" value="Connecting Eurolook add-in completed in %2 ms with HRESULT %1."></string>
                <string id="Dialogika-Eurolook-WordAddIn.event.30.message" value="Application add-ins are updating."></string>
                <string id="Dialogika-Eurolook-WordAddIn.event.31.message" value="Application add-ins update completed in %2 ms with HRESULT %1."></string>
                <string id="Dialogika-Eurolook-WordAddIn.event.40.message" value="Application startup complete."></string>
                <string id="Dialogika-Eurolook-WordAddIn.event.41.message" value="Application startup complete finished in %2 ms with HRESULT %1."></string>
                <string id="Dialogika-Eurolook-WordAddIn.event.50.message" value="Application is shutting down."></string>
                <string id="Dialogika-Eurolook-WordAddIn.event.51.message" value="Application shutdown completed in %2 ms with HRESULT %1."></string>
                <string id="Dialogika-Eurolook-WordAddIn.event.60.message" value="Disconnecting Eurolook add-in."></string>
                <string id="Dialogika-Eurolook-WordAddIn.event.61.message" value="Disconnecting Eurolook add-in completed in %2 ms with HRESULT %1."></string>
                <string id="Dialogika-Eurolook-WordAddIn.event.70.message" value="Eurolook add-in shim is being released."></string>
                <string id="Dialogika-Eurolook-WordAddIn.event.71.message" value="Eurolook add-in shim released in %1 ms."></string>
                <string id="Dialogika-Eurolook-WordAddIn.event.500.message" value="Loading CLR."></string>
                <string id="Dialogika-Eurolook-WordAddIn.event.501.message" value="CLR loading completed in %1 ms."></string>
                <string id="Dialogika-Eurolook-WordAddIn.event.510.message" value="Creating AppDomain with configuration %1."></string>
                <string id="Dialogika-Eurolook-WordAddIn.event.511.message" value="AppDomain created in %1 ms."></string>
                <string id="Dialogika-Eurolook-WordAddIn.event.520.message" value="Creating managed aggregator '%2' for assembly '%1'."></string>
                <string id="Dialogika-Eurolook-WordAddIn.event.521.message" value="Managed aggregator created in %1 ms."></string>
                <string id="Dialogika-Eurolook-WordAddIn.event.1000.message" value="A startup error occurred in method %1 (HRESULT %2h)."></string>
                <string id="Dialogika-Eurolook-WordAddIn.event.1001.message" value="A CLR hosting error occurred in method %1 (HRESULT %2h)."></string>
            </stringTable>
        </resources>
    </localization>
</instrumentationManifest>
