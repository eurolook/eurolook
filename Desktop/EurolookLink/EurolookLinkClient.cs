using System;
using System.IO.Pipes;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Eurolook.Common;
using Eurolook.Common.Log;

namespace Eurolook.EurolookLink
{
    public class EurolookLinkClient : ICanLog
    {
        private readonly string _pipeName;

        public EurolookLinkClient()
        {
            _pipeName = GetPipeName();
        }

        public void SendCommand(string commandName, string[] args)
        {
            try
            {
                var pipeClient = GetClient();
                pipeClient.Connect();
                if (pipeClient.CanWrite)
                {
                    var data = $"{commandName}/{string.Join("/", args)}";
                    this.LogDebug($"Sending data: {data}");
                    var bytes = Encoding.UTF8.GetBytes(data);
                    pipeClient.Write(bytes, 0, bytes.Length);
                }

                pipeClient.Close();
            }
            catch (Exception ex)
            {
                this.LogError(ex);
            }
        }

        public static string GetPipeName()
        {
            string sessionId = WindowsLogonSid.GetLogonSid();
            return $"Pipe_EurolookLink_{sessionId}";
        }

        private NamedPipeClientStream GetClient()
        {
            var pipeClient =
                new NamedPipeClientStream(
                    ".",
                    _pipeName,
                    PipeDirection.Out,
                    PipeOptions.Asynchronous,
                    TokenImpersonationLevel.Anonymous);
            return pipeClient;
        }

        public Task<bool> WaitForServer()
        {
            return Task.Factory.StartNew(
                () =>
                {
                    int retry = 15;
                    while (retry > 0)
                    {
                        if (IsServerRunning(2500))
                        {
                            return true;
                        }

                        retry--;
                    }

                    return false;
                },
                CancellationToken.None,
                TaskCreationOptions.LongRunning,
                TaskScheduler.Default);
        }

        public bool IsServerRunning(int timeout)
        {
            try
            {
                var pipeClient = GetClient();
                pipeClient.Connect(timeout);
                pipeClient.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
