using System;
using System.Threading.Tasks;
using Eurolook.Common.Log;

namespace Eurolook.EurolookLink
{
    public static class Program
    {
        public static async Task<int> Main(string[] args)
        {
            if (args.Length < 1 || !Uri.TryCreate(args[0], UriKind.Absolute, out var uri))
            {
                LogManager.GetLogger().Error("Invalid argument. Valid URL required.");
                return 1;
            }

            return await EurolookLink.RunLink(uri);
        }
    }
}
