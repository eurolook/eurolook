﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Eurolook.Common.Log;

namespace Eurolook.EurolookLink
{
    public class WordProcess
    {
        private readonly object _processLock = new object();
        private readonly string _processName = "WinWord";

        public bool IsStartedWithGui()
        {
            // Word can be in embedded mode only (e.g. by showing a  Word document in the Explorer preview pane)
            // In this case, the MainWindowTitle appears to be empty.
            // c.f. EUROLOOK-3816
            return Process.GetProcessesByName(_processName).Any(p => p.MainWindowTitle != "");
        }

        public void Start()
        {
            try
            {
                lock (_processLock)
                {
                    Process.Start(
                        new ProcessStartInfo
                        {
                            FileName = _processName,
                            CreateNoWindow = true,
                            UseShellExecute = true,
                            RedirectStandardOutput = false,
                        });
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger().Error(ex.ToString());
            }
        }

        public Task<bool> WaitForWord()
        {
            return Task.Factory.StartNew(
                () =>
                {
                    int retry = 10;
                    while (retry > 0)
                    {
                        if (IsStartedWithGui())
                        {
                            return true;
                        }

                        Thread.Sleep(1000);
                        retry--;
                    }

                    return false;
                },
                CancellationToken.None,
                TaskCreationOptions.LongRunning,
                TaskScheduler.Default);
        }
    }
}
