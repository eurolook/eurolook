﻿using System;
using System.Threading.Tasks;
using Eurolook.Common.Log;

namespace Eurolook.EurolookLink
{
    public static class EurolookLink
    {
        public static async Task<int> RunLink(Uri uri)
        {
            try
            {
                var command = uri.Host;
                var commandArgs = uri.AbsolutePath.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries);

                // Make sure Word is started
                var wordProcess = new WordProcess();
                if (!wordProcess.IsStartedWithGui())
                {
                    wordProcess.Start();
                    var wordStarted = await wordProcess.WaitForWord();
                    if (!wordStarted)
                    {
                        LogManager.GetLogger().Info("Failed to wait for Word.");
                        return 3;
                    }
                }

                // Connect to the Named Pipe
                var linkClient = new EurolookLinkClient();
                var serverStarted = await linkClient.WaitForServer();
                if (!serverStarted)
                {
                    LogManager.GetLogger().Info("Failed to wait for server. Server was not reachable.");
                    return 4;
                }

                // Send the command
                linkClient.SendCommand(command, commandArgs);
                return 0;
            }
            catch (Exception ex)
            {
                LogManager.GetLogger().Error("Unexpected error.", ex);
                return 2;
            }
        }
    }
}
