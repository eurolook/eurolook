import { PaginatedTableViewModel } from "PaginatedTableViewModel"
import { DocumentCategoryViewModel } from "DocumentCategory/DocumentCategoryViewModel"
class DocumentCategoryIndexViewModel extends PaginatedTableViewModel {

    documentCategories: KnockoutObservableArray<DocumentCategoryViewModel> = ko.observableArray([]);
    isSelectAll = ko.observable(false);
    newDocCategoryName = ko.observable("");
    deleteModalTitle = ko.observable("");
    reviveModalTitle = ko.observable("");
    searchRequest: JQueryXHR;
    deletingDocumentCategory: DocumentCategoryViewModel;
    revivingDocumentCategory: DocumentCategoryViewModel;
    selectedDocumentCategories = ko.computed(function () {
        var result = Array();
        this.documentCategories().forEach(item => {
            if (item.isSelected())
                result.push(item);
        });
        return result;
    }, this);
    includeDependencies = ko.observable(false);

    constructor(baseUrl, isReadOnly: string) {
        super(baseUrl, "/DocumentCategory", isReadOnly);
        if (!this.orderProperty()) {
           this.orderProperty("name");
            this.orderAscending(true);
        }
        this.isSelectAll.subscribe(function (newValue) {
            this.documentCategories().forEach(docCategoryVm => {
                docCategoryVm.isSelected(newValue);
            });
        });
        this.loadPageData();
        $("#SearchBox").focus();
    }

    loadPageData() {
        if (this.searchRequest && this.searchRequest.readyState < 4) {
            this.searchRequest.abort();
        }
        this.showProgressBar();
         this.searchRequest = this.getJson(this.getPageUrl(this.baseUrl() + "/DocumentCategory"),
        this.loadPageDataSucceeded,
        this);
}

    loadPageDataSucceeded(result) {
        this.documentCategories.removeAll();
        if (result.success === true) {
            const page = result.model;
            for (let docCategory of page.data) {
                this.documentCategories.push(new DocumentCategoryViewModel(docCategory));
            }
            this.setPagination(page);
        }
        this.hideProgressBar();
    }

    createDocumentCategory() {
    this.postJson(`${this.baseUrl()}/DocumentCategory/Create`,
        ko.toJSON({ name: this.newDocCategoryName() }),
        this.createDocumentCategorySucceeded,
        this);

    }

    createDocumentCategorySucceeded(data) {
        this.closeModal();
        if (data.success === true) {
            this.showAlert({ type: "success", message: data.message });
            this.loadPageData();
        } else {
            this.showAlert({ type: "error", message: data.message });
        }
    }

    deleteDocumentCategory() {
        if (!this.deletingDocumentCategory) return;
        this.deleteJson(`${this.baseUrl()}/DocumentCategory/Delete/${this.deletingDocumentCategory.id()}`,
            ko.toJSON(this.deletingDocumentCategory),
            this.deleteDocumentCategorySucceeded,
            this);
    }

    deleteDocumentCategorySucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var deleteId = this.deletingDocumentCategory.id();
            this.documentCategories.remove(item => item.id() === deleteId);
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    deleteDocumentCategoryPermanently() {
        if (!this.deletingDocumentCategory) return;
        this.deleteJson(`${this.baseUrl()}/DocumentCategory/DeletePermanently/${this.deletingDocumentCategory.id()}`,
            ko.toJSON(this.deletingDocumentCategory),
            this.deleteDocumentCategoryPermanentlySucceeded,
            this);
    }

    deleteDocumentCategoryPermanentlySucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var deleteId = this.deletingDocumentCategory.id();
            this.documentCategories.remove(item => item.id() === deleteId);
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    reviveDocumentCategory() {
        if (!this.revivingDocumentCategory) return;
        this.postJson(`${this.baseUrl()}/DocumentCategory/Revive/${this.revivingDocumentCategory.id()}`,
            ko.toJSON(this.revivingDocumentCategory),
            this.reviveDocumentCategorySucceeded,
            this);
    }

    reviveDocumentCategorySucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var reviveId = this.revivingDocumentCategory.id();
            this.documentCategories.remove(item => item.id() === reviveId);
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    downloadDataPackage() {
        if (this.selectedDocumentCategories().length === 0) {
            this.closeModal();
            this.showAlert({ type: "error", message: "No Document Category selected." });
            return;
        }
        this.showProgressBar();
        this.postJson(`${this.baseUrl()}/DocumentCategory/Export?includeDependencies=${this.includeDependencies()}`,
            ko.toJSON(this.selectedDocumentCategories), this.downloadDataPackageSucceeded, this);
    }

    downloadDataPackageSucceeded(result) {
        if (result.success) {
            window.location.href = `${this.baseUrl()}/DataExchange/Export/Package/${result.model.packageId}`;

        } else {
            this.hideProgressBar();
            this.showAlert({ type: "error", message: result.message });
        }
    }

    showDeleteModal(viewModel: DocumentCategoryViewModel) {
        this.deletingDocumentCategory = viewModel;
        this.deleteModalTitle(viewModel.name());
        $("#DeleteModal").foundation("reveal", "open");
    }

    showDeletePermanentlyModal(viewModel: DocumentCategoryViewModel) {
        this.deletingDocumentCategory = viewModel;
        this.deleteModalTitle(viewModel.name());
        $("#DeletePermanentlyModal").foundation("reveal", "open");
    }

    showReviveModal(viewModel: DocumentCategoryViewModel) {
        this.revivingDocumentCategory = viewModel;
        this.reviveModalTitle(viewModel.name());
        $("#ReviveModal").foundation("reveal", "open");
    }
    
}

export = DocumentCategoryIndexViewModel;


