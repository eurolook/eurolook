import { PageViewModel } from 'PageViewModel'

class DocumentCategoryEditViewModel extends PageViewModel {

    id: string;
    url: string;
    name = ko.observable("");
    isSaving = ko.observable(false);

    constructor(baseUrl: string, id: string, isReadOnly: string) {
        super(baseUrl, '/DocumentCategory', isReadOnly);
        this.id = id;
        this.url = `${baseUrl}/DocumentCategory/Edit/${id}`;
        this.bindCtrlSave(this.save);
        this.load();
    }

    load() {
        this.showProgressSpinner();
        this.getJson(this.url, this.loadSucceeded, this);
    }

    loadSucceeded(result) {
        if (!result.success) {
            this.showAlert({ type: 'error', message: result.message });
        }
        else {
             this.name(result.model.name);
             this.hideProgressSpinner();
        }

}

    save() {
        if (this.isSaving()) return;
        this.isSaving(true);
        this.putJson(this.url, ko.toJSON(this), this.saveSucceeded, this);

    }

    saveSucceeded(result) {
        if (!result.success) {
            this.showAlert({ type: 'error', message: result.message });
        } else {
            this.showAlert({ type: 'success', message: result.message });
        }
        this.isSaving(false);
        this.load();
    }

}
export = DocumentCategoryEditViewModel
