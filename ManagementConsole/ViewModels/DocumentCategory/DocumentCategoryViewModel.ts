export class DocumentCategoryViewModel {

    isSelected = ko.observable(false);
    id = ko.observable();
    name = ko.observable();

    constructor(model) {
        if (!model) return;

        this.id(model.id);
        this.name(model.name);

    }
   
}
