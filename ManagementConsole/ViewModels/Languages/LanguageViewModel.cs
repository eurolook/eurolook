﻿using System;
using System.Diagnostics.CodeAnalysis;
using Eurolook.Data.Models;

namespace Eurolook.ManagementConsole.ViewModels.Languages
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    public class LanguageViewModel
    {
        public LanguageViewModel()
        {
            // default constructor is used by ASP.NET MVC for creating the model from a HTTP request
        }

        public LanguageViewModel(Language model)
        {
            Id = model.Id;
            Name = model.Name;
            DisplayName = model.DisplayName;
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string DisplayName { get; set; }

        public bool IsSelected { get; set; }
    }
}
