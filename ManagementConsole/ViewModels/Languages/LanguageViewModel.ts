export class LanguageViewModel {

    id = ko.observable();
    name = ko.observable();
    displayName = ko.observable();
    isSelected = ko.observable(false);

    constructor(model) {
        if (!model) return;

        this.id(model.id);
        this.name(model.name);
        this.displayName(model.displayName);
        this.isSelected(model.isSelected);
    }
}