﻿function LanguageViewModel(model) {
    var self = this;

    self.id = ko.observable();
    self.name = ko.observable();
    self.displayName = ko.observable();
    self.isSelected = ko.observable();

    if (model) {
        self.id(model.id);
        self.name(model.name);
        self.displayName(model.displayName);
        if (model.isSelected) {
            self.isSelected(model.isSelected);
        }
    }
}