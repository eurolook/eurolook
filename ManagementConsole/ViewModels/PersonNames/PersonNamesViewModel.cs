using System;
using Eurolook.Data.Models;

namespace Eurolook.ManagementConsole.ViewModels.PersonNames
{
    public class PersonNamesViewModel
    {
        public PersonNamesViewModel()
        {
        }

        public PersonNamesViewModel(PersonName model)
        {
            Id = model.Id;
            FirstName = model.FirstName;
            LastName = model.LastName;
            Country = model.Country;
            PoliticalGroup = model.PoliticalGroup;
            Category = model.Category;
        }

        public Guid? Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Country { get; set; }

        public string PoliticalGroup { get; set; }

        public string Category { get; set; }
    }
}
