export class PersonNameViewModel{
    
    id = ko.observable();
    isSelected = ko.observable(false);
    firstName = ko.observable();
    lastName = ko.observable();
    country = ko.observable();
    politicalGroup = ko.observable();
    category = ko.observable();
    modification = ko.observable();
    firstNameValue = ko.observable();
    lastNameValue = ko.observable();
    countryValue = ko.observable();
    politicalGroupValue = ko.observable();

    constructor(model) {
        if (!model) return;
        

        this.firstName = ko.observable(model.firstName);
        this.lastName = ko.observable(model.lastName);
        this.country = ko.observable(model.country);
        this.id = ko.observable(model.id);
        this.politicalGroup = ko.observable(model.politicalGroup);
        this.category = ko.observable(model.category);
        this.modification = ko.observable(model.Modification);
        this.firstNameValue = ko.observable(model.firstName);
        this.lastNameValue = ko.observable(model.lastName);
        this.politicalGroupValue = ko.observable(model.politicalGroup);
        this.countryValue = ko.observable(model.country);

        this.firstNameValue.subscribe(function(newValue) {
            if (newValue != this.firstName()) {
                this.isSelected(true);
                this.firstName = this.firstNameValue();
            }
        }, this);

        this.lastNameValue.subscribe(function (newValue) {
            if (newValue != this.lastName()) {
                this.isSelected(true);
                this.lastName = this.lastNameValue();
            }
        }, this);

        this.politicalGroupValue.subscribe(function (newValue) {
            if (newValue != this.politicalGroup()) {
                this.isSelected(true);
                this.politicalGroup = this.politicalGroupValue();
            }
        }, this);

        this.countryValue.subscribe(function (newValue) {
            if (newValue != this.country()) {
                this.isSelected(true);
                this.country = this.countryValue();
            }
        }, this);
    }
}
