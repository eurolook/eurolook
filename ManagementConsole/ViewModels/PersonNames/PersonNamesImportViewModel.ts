import {PaginatedTableViewModel} from "PaginatedTableViewModel"

class PersonNamesImportViewModel extends PaginatedTableViewModel {

    spreadsheetFileInfo = ko.observable();
    validationResult = ko.observable("Validation result");
    uploadId = ko.observable();
    enableConfirm = ko.observable(false);
    updateResult = ko.observable();
    spreadsheetFileFormData = new FormData();


    constructor(baseUrl, isReadOnly: string) {
        super(baseUrl, "/PersonNames", isReadOnly);
    }

    uploadSpreadsheet() {
        var fileElement = $("#excelFile")[0] as HTMLInputElement;
        var file = fileElement.files[0];

        if (!file) return;

        this.spreadsheetFileFormData.append("file", file);
        this.spreadsheetFileInfo(file.name);
        this.showUploadProgressBar();
        this.postFormDataWithCallback(this.baseUrl() + "/PersonNames/Upload", this.spreadsheetFileFormData,
            this.uploadSpreadsheetSucceeded, this.uploadSpreadsheetError,
            this);
    }

    uploadSpreadsheetSucceeded(result) {
        this.hideUploadProgressBar();
        if (result.success === true) {
            this.clearExcelFile();
            this.uploadId(result.model.uploadId);
            this.getPersonNameChanges();
        } else {
            this.clearExcelFile();
            this.uploadId("");
            this.enableConfirm(false);
            this.validationResult(result.message);
            this.updateResult("No Data");
        }
    }

    uploadSpreadsheetError(result) {
        this.hideUploadProgressBar();
        this.clearExcelFile();
        this.uploadId("");
        this.enableConfirm(false);
        this.validationResult(result.message);
        this.updateResult("");
        this.showAlert({ type: "error", message: result.message });
    }

    clearExcelFile() {
        $("#excelFile").val("");
    };

    showUploadProgressBar() {
        window.clearTimeout(this.globalProgressTimer);
        this.globalProgressTimer = window.setTimeout(() => {
                $("#uploadValidationResult").hide();
                $("#uploadProgressBar").show();
            },
            500);
    };

    hideUploadProgressBar = function() {
        window.clearTimeout(this.globalProgressTimer);
        $("#uploadProgressBar").hide();
        $("#uploadValidationResult").show();
    };
    
    showUpdateProgressBar() {
        window.clearTimeout(this.globalProgressTimer);
        $("#updateResult").hide();
        this.globalProgressTimer = window.setTimeout(() => {
                $("#updateProgressBar").show();
                $("#updateResult").show();
            },
            500);
    };
    
    hideUpdateProgressBar() {
        window.clearTimeout(this.globalProgressTimer);
        $("#updateProgressBar").hide();
        $("#updateResult").show();
    };

    getPersonNameChanges() {
        if (!this.uploadId()) return;
        this.showUploadProgressBar();
        this.getJson(this.baseUrl() + "/PersonNames/GetChanges?uploadId=" + this.uploadId(),
            this.getPersonNameChangesSucceeded,
            this);
    }

    getPersonNameChangesSucceeded(result) {
        this.hideUploadProgressBar();
        if (result.success == true) {
            this.enableConfirm(true);
            this.updateResult("Confirm data");
            this.validationResult(result.message);
        } else {
            this.enableConfirm(false);
            this.validationResult(result.message);
            this.showAlert({type: "error", message: result.message});
        }
    }

    applyChanges() {
        if (!this.uploadId() || this.enableConfirm() === false) return;
        this.showUpdateProgressBar();
        this.getJson(this.baseUrl() + "/PersonNames/ApplyChanges?uploadId=" + this.uploadId(),
            this.applyChangesSucceeded,
            this);
    }

    applyChangesSucceeded(result) {
        this.hideUpdateProgressBar();
        this.enableConfirm(false);
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            this.updateResult("Database successfully updated.");
        } else {
            this.enableConfirm(false);
            this.updateResult(result.message);
            this.showAlert({ type: "error", message: result.message });
        }
    }
}

export = PersonNamesImportViewModel
