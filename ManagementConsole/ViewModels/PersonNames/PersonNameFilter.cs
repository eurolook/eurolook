using Eurolook.Data.Models;

namespace Eurolook.ManagementConsole.ViewModels.PersonNames
{
    public class PersonNameFilter
    {
        public static readonly PersonNameFilter All = new PersonNameFilter
        {
            Id = "All",
            DisplayName = "All Persons",
        };

        public static readonly PersonNameFilter MeP = new PersonNameFilter
        {
            Id = PersonName.MepCategoryName,
            DisplayName = "Members of the European Parliament",
        };

        public static readonly PersonNameFilter Commissioners = new PersonNameFilter
        {
            Id = PersonName.CommissionCategoryName,
            DisplayName = "Commissioners",
        };

        public string Id { get; set; }

        public string DisplayName { get; set; }
    }
}
