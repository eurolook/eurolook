import { PaginatedTableViewModel } from "PaginatedTableViewModel"
import {PersonNameViewModel} from "PersonNames/PersonNameViewModel"

class PersonNamesIndexViewModel extends PaginatedTableViewModel{

    personNames: KnockoutObservableArray<PersonNameViewModel> = ko.observableArray([]);
    selectedCategory = ko.observable("All");
    categoryFilters = ko.observableArray();
    personNameCategories = ko.observableArray();
    deletingPersonName: PersonNameViewModel=null;
    searchRequest: JQueryXHR;

    newFirstName = ko.observable("");
    newLastName = ko.observable("");
    newPoliticalGroup = ko.observable("");
    newCountry = ko.observable("");
    newCategory = ko.observable("");
    newNameValidated = ko.observable(false);
    includeDependencies = ko.observable(false);

    selectedPersonNames = ko.computed({
        read: () => {
            var result = Array();
            this.personNames().forEach(function (item) {
                if (item.isSelected())
                    result.push(item);
            }, this);
            return result;
        },
        owner: this
    }); 
    isSelectedAll = ko.pureComputed({
        read: function () {
            return this.selectedPersonNames().length === this.personNames().length;
        },
        write: function (value) {
            this.personNames().forEach(function(item) {
                item.isSelected(value);
            })        },
        owner: this
    });


    hasSelection = ko.computed(function () {
        return this.selectedPersonNames().length > 0;
    }, this);

    validateNewName() {
        var result = this.newLastName() !== "" && this.newFirstName() !== "" && (this.newCategory() === "Commissioners" || (this.newCountry() !== "" && this.newPoliticalGroup() !== ""));
        this.newNameValidated(result);
    };

    constructor(baseUrl, isReadOnly: string) {
        super(baseUrl, "/PersonNames", isReadOnly);
        this.pageSizes = ko.observableArray([20, 50, 100, 500, 1000]);
        this.loadFilters().then(function() {
            this.orderProperty("lastname");
            this.orderAscending(true);
            this.selectedCategory.subscribe(this.loadPageData, this);
            this.loadPageData();
            $("#SearchBox").focus();
            
        });
        this.newFirstName.subscribe(() => this.validateNewName());
        this.newLastName.subscribe(() => this.validateNewName());
        this.newCategory.subscribe(() => this.validateNewName());
        this.newCountry.subscribe(() => this.validateNewName());
        this.newPoliticalGroup.subscribe(() => this.validateNewName());
        this.loadCategories();
        this.validateNewName();
    }

    loadFilters() {
        this.showProgressBar();
       return this.searchRequest = this.getJson(this.getPageUrl(this.baseUrl() + "/PersonNames/Filters"),
            this.loadFiltersSucceeded,
            this);
    }

    loadFiltersSucceeded(result) {
        this.personNameCategories.removeAll();
        if (result.success === true) {
            result.model.categories.forEach(function (filter) {
                this.categoryFilters.push(filter);
            }, this);
        }
        this.hideProgressBar();
    }

    loadCategories() {
        this.showProgressBar();
        this.searchRequest=this.getJson(this.getPageUrl(this.baseUrl() + "/PersonNames/Categories"),
            this.loadCategoriesSucceeded,
            this);
    }

    loadCategoriesSucceeded(result) {
        this.personNameCategories.removeAll();
        if (result.success === true) {
            result.model.categories.forEach(function(filter) {
                this.personNameCategories.push(filter);
            }, this);
        }
        this.hideProgressBar();
    }

    loadPageData() {
        if (!this.selectedCategory()) return; 
        
        this.showProgressBar();
        var q = this.searchQuery();
        if (!q) {
            q = "";
        }
        this.searchRequest = this.getJson(this.getPageUrl(this.baseUrl() + "/PersonNames") + "&category=" + this.selectedCategory() + "&searchText=" + q,
            this.loadPageDataSucceeded,
            this);
    }

    loadPageDataSucceeded(result) {
        this.personNames.removeAll();
        if (result.success === true) {
            var page = result.model;
            for (let person of page.data) {
                this.personNames.push(new PersonNameViewModel(person));
            }
            this.setPagination(page);
            this.hideProgressBar();
        } else {
            this.showAlert({ type: "error", message: result.message });
            this.hideProgressBar();
        }
    }

    editPersonNames() {
        if (!this.personNames) return;
        this.putJson(`${this.baseUrl()}/PersonNames/Edit`,
            ko.toJSON(this.personNames),
            this.editPersonNamesSucceeded,
            this);
    }

    editPersonNamesSucceeded(result) {
        if (result.success === true) {
            this.showAlert({ type: 'success', message: result.message });
            this.loadPageData();
        } else {
            this.showAlert({ type: 'error', message: result.message });
        }
    }

    deletePersonNames() {
        if (this.selectedPersonNames().length === 0) {
           this.closeModal();
          return;
        }
        this.deleteJson(`${this.baseUrl()}/PersonNames/Delete`,
                ko.toJSON(this.selectedPersonNames()),
                this.deletePersonNamesSucceeded,
                this);
    }

    deletePersonNamesSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            this.personNames.removeAll(this.selectedPersonNames());
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
        
    }

    createPersonName() {
        this.postJson(`${this.baseUrl()}/PersonNames/Create`,
            ko.toJSON({
                firstName: this.newFirstName(),
                lastName: this.newLastName(),
                politicalGroup: this.newPoliticalGroup(),
                country: this.newCountry(),
                category: this.newCategory()}),
            this.createPersonNameSucceeded,
            this);
}

    createPersonNameSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            this.loadPageData();
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    downloadDataPackage() {
        if (this.selectedPersonNames().length === 0) {
            this.closeModal();
            this.showAlert({ type: "error", message: "No Person Names selected." });
            return;
        }
        this.showProgressBar();
        this.postJson(`${this.baseUrl()}/PersonNames/Export?includeDependencies=${this.includeDependencies()}`,
            ko.toJSON(this.selectedPersonNames),
            this.downloadDataPackageSucceeded,
            this);
    }

    downloadDataPackageSucceeded(result) {
        if (result.success) {
            window.location.href = `${this.baseUrl()}/DataExchange/Export/Package/${result.model.packageId}`;
        } else {
            this.hideProgressBar();
            this.showAlert({ type: "error", message: result.message });
        }

    }

}
export = PersonNamesIndexViewModel
