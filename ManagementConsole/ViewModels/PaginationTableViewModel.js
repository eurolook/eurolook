﻿function PaginationTableViewModel() {
    var self = this;

    // Init params from url/cookie

    var q = getParameterByName("q");
    var tm = "normal";
    if (getParameterByName("deleted")) {
        tm = "deleted";
    }
    var s = parseInt(getParameterByName("s"));
    if (isNaN(s)) {
        var preferedPageSize = parseInt(getCookie("preferedPageSize"));
        if (isNaN(preferedPageSize)) {
            s = 20;
        } else {
            s = preferedPageSize;
        }
    }
    var p = parseInt(getParameterByName("p"));
    if (isNaN(p)) p = 1;
    var o = getParameterByName("o");
    var paramAsc = getParameterByName("asc");
    var asc = true;
    if (paramAsc) {
        asc = paramAsc == "true";
    }

    // properties

    this.pageNumber = ko.observable(p);
    this.pageInfos = ko.observableArray();
    this.selectedPageNumber = ko.observable();
    this.pageSizes = ko.observableArray([20, 50, 100]);
    this.pageSize = ko.observable(s);
    this.pageSize.subscribe(function (newValue) {
        if (!newValue || isNaN(newValue)) return;
        setCookie("preferedPageSize", newValue, 500);
        self.pageNumber(1);
        self.loadPageData();
    });
    this.searchQuery = ko.observable(q);
    this.orderProperty = ko.observable(o);
    this.orderAscending = ko.observable(asc);
    this.tableMode = ko.observable(tm);
    this.tableMode.subscribe(function () {
        self.loadPageData();
    });
    this.pageCount = ko.computed(function () {
        return self.pageInfos().length;
    });
    this.hasPreviousPage = ko.computed(function () {
        return self.pageNumber() > 1;
    });
    this.hasNextPage = ko.computed(function () {
        return self.pageNumber() < self.pageCount();
    });

    // overwriteable functions

    this.loadPageData = function () { }

    // functions

    this.setPagination = function (page) {
        self.pageNumber(page.pageNumber);
        if (page.pageCount != self.pageCount()) {
            var pis = Array();
            for (var i = 1; i <= page.pageCount; i++) {
                pis.push({
                    pageNo: i,
                    displayName: i + " of " + page.pageCount
                });
            }
            self.pageInfos(pis);
            self.selectedPageNumber(page.pageNumber);
            self.selectedPageNumber.subscribe(function (newValue) {
                if (!isNaN(newValue) && newValue != self.pageNumber()) {
                    self.pageNumber(newValue);
                    self.loadPageData();
                }
            });
        }
    }

    this.loadPreviousPage = function () {
        if (self.hasPreviousPage()) {
            self.selectedPageNumber(self.pageNumber() - 1);
        }
    }

    this.loadNextPage = function () {
        if (self.hasNextPage()) {
            self.selectedPageNumber(self.pageNumber() + 1);
        }
    }

    this.handleSearchInput = function(event) {
        window.clearTimeout(self.searchInputTimer);
        self.searchInputTimer = window.setTimeout(function () {
            var q = event.target.value;
            self.searchQuery(q);
            self.pageNumber(1);
            self.loadPageData();
        }, 300);
    }

    this.orderBy = function (newOrder) {
        if (newOrder) {
            if (newOrder == self.orderProperty()) {
                self.orderAscending(!self.orderAscending());
            }
            else {
                self.orderProperty(newOrder);
                self.orderAscending(true);
            }
        } 
        self.loadPageData();
    }

    this.getPageUrl = function (baseUrl) {
        var url = baseUrl + "?";
        var query = self.searchQuery();
        if (query && query != "*") {
            url += "q=" + query + "&";
        }
        var pageNo = self.pageNumber();
        if (!isNaN(pageNo) && pageNo > 0) {
            url += "p=" + pageNo;
        } else {
            url += "p=1";
        }
        url += "&s=" + self.pageSize();
        if (self.orderProperty()) {
            url += "&o=" + self.orderProperty();
        }
        url += "&asc=" + self.orderAscending();
        if (self.tableMode() == "deleted") {
            url += "&deleted=true";
        }
        return url;
    }

    this.showProgressBar = function () {
        window.clearTimeout(self.progressTimer);
        self.progressTimer = window.setTimeout(function () { $("#loadDataProgressBar").show(); }, 300);
    }

    this.hideProgressBar = function () {
        window.clearTimeout(self.progressTimer);
        $("#loadDataProgressBar").hide();
    }

    this.setBrowserHistory = function (url) {
        var currentState = window.history.state;
        window.history.pushState(currentState, document.title, url);
    }
}