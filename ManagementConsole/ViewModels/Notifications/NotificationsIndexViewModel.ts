import { PaginatedTableViewModel } from "PaginatedTableViewModel"
import { NotificationsViewModel } from "Notifications/NotificationsViewModel"

class NotificationsIndexViewModel extends PaginatedTableViewModel {

    notifications: KnockoutObservableArray<NotificationsViewModel> = ko.observableArray([]);
    selectedNotifications: KnockoutObservableArray<NotificationsViewModel> = ko.observableArray([]);
    newNotification = ko.observable("");
    deleteModalTitle = ko.observable("");
    reviveModalTitle = ko.observable("");
    searchRequest: JQueryXHR;
    deletingNotification: NotificationsViewModel;
    revivingNotification: NotificationsViewModel;
    includeDependencies = ko.observable(false);
    isSelectedAll = ko.pureComputed({
        read: function () {
            return this.selectedNotifications().length === this.notifications().length;
        },
        write: function (value) {
            this.selectedNotifications(value ? this.notifications().slice(0) : []);
        },
        owner: this
    });

    constructor(baseUrl, isReadOnly: string) {
        super(baseUrl, "/Notifications", isReadOnly);
        if (!this.orderProperty()) {
            this.orderProperty("name");
            this.orderAscending(true);
        }
        this.loadPageData();
    }

    loadPageData() {
        if (this.searchRequest && this.searchRequest.readyState < 4) {
            this.searchRequest.abort();
        }
        this.showProgressSpinner();
        this.searchRequest = this.getJson(this.getPageUrl(this.baseUrl() + "/Notifications"),
            this.loadPageDataSucceeded,
            this);
    }

    loadPageDataSucceeded(result) {
        this.selectedNotifications.removeAll();
        this.notifications.removeAll();
        if (result.success === true) {
            const page = result.model;
            for (let notif of page.data) {
                this.notifications.push(new NotificationsViewModel(notif));
            }
            this.setPagination(page);
        }
        this.hideProgressSpinner();
    }

    createNotification() {
        this.postJson(`${this.baseUrl()}/Notifications/Create`,
            ko.toJSON({
                name: this.newNotification(),
                displayName: this.newNotification()
            }),
            this.createNotificationSucceeded,
            this);
    }

    createNotificationSucceeded(data) {
        this.closeModal();
        if (data.success === true) {
            this.showAlert({ type: "success", message: data.message });
            this.loadPageData();
        } else {
            this.showAlert({ type: "error", message: data.message });
        }
    }

    deleteNotification() {
        if (!this.deletingNotification) return;

        this.deleteJson(`${this.baseUrl()}/Notifications/Delete/${this.deletingNotification.id()}`,
            ko.toJSON(this.deletingNotification),
            this.deleteNotificationSucceeded,
            this);
    }

    deleteNotificationSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var deleteId = this.deletingNotification.id();
            this.notifications.remove(item => item.id() === deleteId);
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    reviveNotification() {
        if (!this.revivingNotification) return;
        this.postJson(`${this.baseUrl()}/Notifications/Revive/${this.revivingNotification.id()}`,
            ko.toJSON(this.revivingNotification),
            this.reviveNotificationSucceeded,
            this);
    }

    reviveNotificationSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var reviveId = this.revivingNotification.id();
            this.notifications.remove(item => item.id() === reviveId);
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    downloadDataPackage() {
        if (this.selectedNotifications().length === 0) {
            this.closeModal();
            this.showAlert({ type: "error", message: "No Brick selected." });
            return;
        }
        this.showProgressBar();
        this.postJson(`${this.baseUrl()}/Notifications/Export`,
            ko.toJSON(this.selectedNotifications), this.downloadDataPackageSucceeded, this);
    }

    downloadDataPackageSucceeded(result) {
        if (result.success) {
            window.location.href = `${this.baseUrl()}/DataExchange/Export/Package/${result.model.packageId}`;

        } else {
            this.hideProgressBar();
            this.showAlert({ type: "error", message: result.message });
        }
    }

    showDeleteModal(viewModel: NotificationsViewModel) {
        this.deletingNotification = viewModel;
        this.deleteModalTitle(viewModel.name());
        // ReSharper disable once TsResolvedFromInaccessibleModule
        $("#DeleteModal").foundation("reveal", "open");
    }

    showReviveModal(viewModel: NotificationsViewModel) {
        this.revivingNotification = viewModel;
        this.reviveModalTitle(viewModel.name());
        $("#ReviveModal").foundation("reveal", "open");

    }

}
export = NotificationsIndexViewModel
