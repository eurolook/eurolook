export enum NotificationPriority {
    Normal = 0,
    Low = 1,
    High = 2,
}

export class NotificationsViewModel {

    id = ko.observable();
    name = ko.observable();
    displayName = ko.observable();
    content = ko.observable();
    thumbnailBytes = ko.observable();
    isHidden = ko.observable();
    fromDate = ko.observable();
    toDate = ko.observable();
    fromDateString = "";
    toDateString = "";
    maxVersion = ko.observable();
    minVersion = ko.observable();
    priority = ko.observable(NotificationPriority.Normal);

    constructor(model) {
        if (!model) return;

        this.id(model.id);
        this.name(model.name);
        this.displayName(model.displayName);
        this.content(model.content);
        this.thumbnailBytes(model.thumbnailBytes);
        this.isHidden(model.isHidden);
        this.fromDate(model.fromDate);
        this.toDate(model.toDate);
        this.fromDateString= new Date(model.fromDate).toLocaleString();
        this.toDateString= new Date(model.toDate).toLocaleString();
        this.maxVersion(model.maxVersion);
        this.minVersion(model.minVersion);
        this.priority(model.priority);
    }
}
