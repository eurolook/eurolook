import { NotificationPriority } from 'Notifications/NotificationsViewModel'
import { PageViewModel } from 'PageViewModel'

class NotificationsEditViewModel extends PageViewModel {

    id: string;
    url: string;
    isSaving = ko.observable(false);
    name = ko.observable('');
    displayName = ko.observable('');
    content = ko.observable('');
    thumbnailBytes = ko.observable('');
    isHidden = ko.observable('');
    fromDateString = ko.observable();
    toDateString = ko.observable();
    thumbnailFileFormData: FormData;
    thumbnailInfo = ko.observable(''); 
    thumbnailImage = ko.observable('');
    hasThumbnail = ko.observable();
    minVersion = ko.observable('');
    maxVersion = ko.observable('');

    priority = ko.observable(NotificationPriority.Normal);
    priorityOptions = ko.observableArray([
        { id: NotificationPriority.Low, displayText: "Low" },
        { id: NotificationPriority.Normal, displayText: "Normal" },
        { id: NotificationPriority.High, displayText: "High" },
    ]);


    constructor(baseUrl: string, id: string, isReadOnly: string) {
        super(baseUrl, '/Notifications', isReadOnly);
        this.id = id;
        this.url = `${baseUrl}/Notifications/Edit/${id}`;
        this.bindCtrlSave(this.save);
        this.load();
    }

    convertUtcDateToLocalDate(date) {
        const newDate = new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1000);

        const offset = date.getTimezoneOffset() / 60;
        const hours = date.getHours();

        newDate.setHours(hours - offset);

        return newDate;
    } 
    load() {
        this.showProgressSpinner();
        this.getJson(this.url, this.loadSucceeded, this);
    }

    loadSucceeded(result) {
        if (!result.success) {
            this.showAlert({ type: 'error', message: result.message });
        } else {
            this.name(result.model.name);
            this.displayName(result.model.displayName);
            this.content(result.model.content);
            this.fromDateString(result.model.fromDateString);
            this.toDateString(result.model.toDateString);
            this.thumbnailInfo(result.model.thumbnailInfo);
            this.thumbnailImage(result.model.thumbnailImage);
            this.thumbnailBytes(result.model.thumbnailBytes);
            this.hasThumbnail(result.model.thumbnailImage != null);
            this.minVersion(result.model.minVersion);
            this.maxVersion(result.model.maxVersion);
            this.priority(result.model.priority);
            this.hideProgressSpinner();
        }
    }

    
    save() {
        if (this.isSaving()) return;
        this.isSaving(true);

        const notif = ko.toJS(this);

        $.when(
            this.postFormData(`${this.baseUrl()}/Notifications/Thumbnail/${this.id}`, this.thumbnailFileFormData, this),
            this.putJson(`${this.baseUrl()}/Notifications/Edit/${this.id}`,
                ko.toJSON(notif),
                null,
                this)
        ).then(this.saveSucceeded).fail(this.saveFailed);
    }

    saveSucceeded(saveThumbnailResponse, saveNotificationResponse) {

        const that: NotificationsEditViewModel = this[0];
        const saveThumbnailResult = saveThumbnailResponse[0];
        const saveNotificationResult = saveNotificationResponse[0];
        if (saveThumbnailResult.success === true && saveNotificationResult.success === true) {
            if (saveThumbnailResult.model != null) {
                that.thumbnailImage(saveThumbnailResult.model);
            }
            that.showAlert({ type: "success", message: saveNotificationResult.message });
        } else {
            let errorMsg = "";
            if (saveThumbnailResult.success === false)
                errorMsg += saveThumbnailResult.message;
            if (saveNotificationResult.success === false)
                errorMsg += saveNotificationResult.message;
            that.showAlert({ type: "error", message: errorMsg });
        }
        that.isSaving(false);
    }

    saveFailed(jqXhr) {
        const that: NotificationsEditViewModel = this[0];
        that.showAlert({ type: "error", message: `HTTP error: ${jqXhr.statusText}` });
        that.isSaving(false);
    }

    chooseThumbnailFile() {
        $("#thumbnailFile").click();
    }

    choseThumbnailFile(file: File) {
        if (file.size / 1024 > 5000) {
            this.showAlert({type: "error", message: "The size of the file is too big. Choose another file."});
        } else {
            this.thumbnailFileFormData = new FormData();
            this.thumbnailFileFormData.append("file", file);
            this.thumbnailInfo(Math.ceil(file.size / 1024.0).toString() + " kb");
            this.hasThumbnail(true);
            const reader = new FileReader();
            reader.onload = e => {
                this.thumbnailImage(String(reader.result));
            };
            reader.readAsDataURL(file);
        }
    }

    deleteThumbnail() {
        this.thumbnailImage(null);
        this.thumbnailInfo(null);
        this.thumbnailBytes(null);
        this.hasThumbnail(false);
    }
}
export = NotificationsEditViewModel
