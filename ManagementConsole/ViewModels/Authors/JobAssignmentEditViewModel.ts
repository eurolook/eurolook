import { PageViewModel } from "PageViewModel"
import { LanguageViewModel } from "Languages/LanguageViewModel"
import { FunctionEditViewModel } from "Authors/FunctionEditViewModel"
import Utils = require("Utils")

export class JobAssignmentEditViewModel extends PageViewModel {

    controllerUrl: string;
    id: string;
    isNew: boolean;

    // orga chooser
    orgaTreeNodes: Array<any>;
    orgaEntityId = ko.observable("");

    // functions
    predefinedFunctionId = ko.observable("");
    languages = ko.observableArray();
    selectedLanguageId = ko.observable("");
    functions: KnockoutObservableArray<FunctionEditViewModel> = ko.observableArray();
    functionText = ko.observable("");

    // others
    isMainJob = ko.observable(false);
    service = ko.observable("");
    webAddress = ko.observable("");
    functionalMailbox = ko.observable("");
    isDeleted = ko.observable(false);
    canDelete = ko.computed(function() {
        // ReSharper disable once SuspiciousThisUsage
        return !this.isMainJob();
    }, this);
    isFunctionControlEnabled = ko.computed(() => {
        return !this.predefinedFunctionId();
    });

    constructor(baseUrl: string, model: any, nodes: Array<any>, isReadOnly: string) {
        super(baseUrl, "/Authors", isReadOnly);
        this.controllerUrl = baseUrl + "/Authors/";
        this.functions = ko.observableArray();
        this.orgaTreeNodes = JSON.parse(JSON.stringify(nodes)); // clone the array

        if (!model) {
            return;
        }

        this.orgaEntityId(model.orgaEntityId);
        this.predefinedFunctionId(model.predefinedFunctionId);

        for (let language of model.languages) {
            this.languages.push(new LanguageViewModel(language));
            if (language.name === "EN") {
                this.selectedLanguageId(language.id);
            }
        }
        for (let fu of model.functions) {
            const functionVm = new FunctionEditViewModel(fu);
            this.functions.push(functionVm);
            if (fu.languageId === this.selectedLanguageId()) {
                this.functionText(functionVm.text());
            }
        }

        // others
        this.id = model.id;
        this.isMainJob(model.isMainJob);
        this.service(model.service);
        this.webAddress(model.webAddress);
        this.functionalMailbox(model.functionalMailbox);
        this.subscribeEventHandler();
    }

    initNew (languages) {
        this.id = Utils.createGuid();
        this.isNew = true;
        this.isMainJob(false);
        this.languages(languages);
        this.selectedLanguageId("cd269368-bbbe-4898-806f-e261443968db"); //EN
    };

    buildJsTree() {
        // prepare the tree nodes
        this.findAndSelectTreeNode(this.orgaTreeNodes, this.orgaEntityId());
        // build the JsTree
        const treeId = `${this.id}_tree`;
        const tree = $(`#${treeId}`).jstree({
            core: {
                data: this.orgaTreeNodes
            }
        });
        var self = this;
        // event handler for selecting nodes
        tree.on("select_node.jstree", (e, data) => {
            if (!data || !data.node || !data.node.id) {
                return;
            }
            const orgaEntityId = data.node.id.substring(data.node.id.indexOf("_") + 1);
            if (orgaEntityId && orgaEntityId.length === 36) {
                self.orgaEntityId(orgaEntityId);
            }
        });
        // scroll selected node into view
        tree.on("ready.jstree", () => {
            const treeElement = document.getElementById(treeId);
            const selectedNodeElement = document.getElementById(`${self.id}_${self.orgaEntityId()}`);
            if (treeElement && selectedNodeElement) {
                this.scrollParentToChild(treeElement, selectedNodeElement);
            }
        });
    }

    findAndSelectTreeNode(nodes: any[], orgaEntityId: string) {
        for (let node of nodes) {
            if (node.id) {
                if (node.id === orgaEntityId) {
                    node.state.selected = true;
                }
                node.id = `${this.id}_${node.id}`; // create a unique id
                if (node.children) {
                    this.findAndSelectTreeNode(node.children, orgaEntityId);
                }
            }
        }
    }

    scrollParentToChild(parent: Element, child: Element) {
        // Where is the parent on page
        const parentRect = parent.getBoundingClientRect();
        // What can you see?
        const parentViewableArea = {
            height: parent.clientHeight,
            width: parent.clientWidth
        };
        // Where is the child
        const childRect = child.getBoundingClientRect();
        // Is the child viewable?
        const isViewable = (childRect.top >= parentRect.top) && (childRect.top <= parentRect.top + parentViewableArea.height);
        // if you can't see the child try to scroll parent
        if (!isViewable) {
            // scroll by offset relative to parent
            parent.scrollTop = (childRect.top + parent.scrollTop) - parentRect.top - (parentRect.height / 2);
        }
    }

    subscribeEventHandler() {
        this.selectedLanguageId.subscribe(() => {
            this.selectedLanguageChanged();
        });
        this.functionText.subscribe(() => {
            this.functionTextChanged();
        });
    };

    selectedLanguageChanged() {
        var selectedFunctionVm = this.getSelectedFunctionVm();
        if (selectedFunctionVm) {
            this.functionText(selectedFunctionVm.text());
        } else {
            this.functionText(null);
        }
    };

    functionTextChanged() {
        const selectedFunctionVm = this.getSelectedFunctionVm();
        if (!selectedFunctionVm) {
            const newFunction = new FunctionEditViewModel({
                id: Utils.createGuid(),
                text: this.functionText(),
                languageId: this.selectedLanguageId()
            });
            this.functions.push(newFunction);
        } else {
            selectedFunctionVm.text(this.functionText());
        }
    };

    getSelectedFunctionVm() {
        let result = null;
        for (let functionVm of this.functions()) {
            if (functionVm.languageId() === this.selectedLanguageId()) {
                result = functionVm;
            }
        }
        return result;
    };
}
