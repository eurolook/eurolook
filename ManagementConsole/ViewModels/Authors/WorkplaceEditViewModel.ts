import AuthorEditViewModel = require("Authors/AuthorEditViewModel");

export class WorkplaceEditViewModel {

    id = ko.observable("");
    addressId = ko.observable("");
    office = ko.observable("");
    phone = ko.observable("");
    fax = ko.observable("");
    isPrimaryWorkplace = ko.observable(false);
    phonePrefix = ko.observable("");

    constructor(model, authorsEditVm: AuthorEditViewModel) {
        this.id(model.id);
        this.office(model.office);
        this.phone(model.phone);
        this.fax(model.fax);
        this.isPrimaryWorkplace(model.isPrimaryWorkplace);
        this.isPrimaryWorkplace.subscribe((newValue: boolean) => {
            if (authorsEditVm && newValue) {
                authorsEditVm.primaryWorkplaceChanged(this);
            }
        });
        this.addressId.subscribe((newValue: string) => {
            if (!authorsEditVm) {
                return;
            }
            for (let address of authorsEditVm.addresses()) {
                if (address.id === newValue) {
                    this.phonePrefix(address.phoneNumberPrefix);
                    return;
                }
            }
        });
        this.addressId(model.addressId);
    }
}
