import { PageViewModel } from "../PageViewModel";
import { JobAssignmentEditViewModel } from "Authors/JobAssignmentEditViewModel"
import { WorkplaceEditViewModel } from "Authors/WorkplaceEditViewModel"

class AuthorEditViewModel extends PageViewModel {

    formOfAddressItems = ko.observableArray([{
        displayName: "",
        value: null
    }, {
        displayName: "Mr",
        value: "m"
    }, {
        displayName: "Ms",
        value: "f"
    }]);

    id: string;
    url: string;
    isSaving = ko.observable(false);
    latinFirstName = ko.observable("");
    latinLastName = ko.observable("");
    greekFirstName = ko.observable("");
    greekLastName = ko.observable("");
    cyrillicFirstName = ko.observable("");
    cyrillicLastName = ko.observable("");
    initials = ko.observable("");
    email = ko.observable("");
    gender = ko.observable("");
    languages = ko.observableArray();
    workplaces: KnockoutObservableArray<WorkplaceEditViewModel> = (ko.observableArray()) as KnockoutObservableArray<WorkplaceEditViewModel>;
    jobAssignments: KnockoutObservableArray<JobAssignmentEditViewModel> = (ko.observableArray()) as KnockoutObservableArray<JobAssignmentEditViewModel>;
    addresses: KnockoutObservableArray<any> = ko.observableArray();
    predefinedFunctions: KnockoutObservableArray<any> = ko.observableArray();
    orgaTreeNodes = new Array();

    sharePointAccountId: KnockoutObservable<number> = ko.observable(0);
    sharePointLoginName: KnockoutObservable<string> = ko.observable("");
    
    constructor(baseUrl: string, id: string, isReadOnly: string) {
        super(baseUrl, "/Authors", isReadOnly);
        this.id = id;
        this.url = baseUrl + "/Authors/Edit/" + id;
        this.bindCtrlSave(this.save);
        this.load();
    }

    load() {
        this.showProgressSpinner();
        this.getJson(this.url, this.loadSucceeded, this);
    }

    loadSucceeded(result) {
        this.languages(result.model.languages);
        this.latinFirstName(result.model.latinFirstName);
        this.latinLastName(result.model.latinLastName);
        this.greekFirstName(result.model.greekFirstName);
        this.greekLastName(result.model.greekLastName);
        this.cyrillicFirstName(result.model.cyrillicFirstName);
        this.cyrillicLastName(result.model.cyrillicLastName);
        this.gender(result.model.gender);
        this.initials(result.model.initials);
        this.email(result.model.email);
        this.loadOrgaTree(result.model.orgaTree, this.orgaTreeNodes);
        this.predefinedFunctions(result.model.predefinedFunctions.slice());
        this.addresses(result.model.addresses.slice());
        for (let jobAssignment of result.model.jobAssignments) {
            const jobAssignmentVm
                = new JobAssignmentEditViewModel(this.baseUrl(), jobAssignment, this.orgaTreeNodes, `${this.isReadOnlyMode()}`);
            this.jobAssignments.push(jobAssignmentVm);
        }
        for (let workplace of result.model.workplaces) {
            this.workplaces.push(new WorkplaceEditViewModel(workplace, this));
        }

        this.sharePointAccountId(result.model.sharePointAccountId);
        this.sharePointLoginName(result.model.sharePointLoginName);

        $(document).ready(() => {
            for (let jobAssignmentVm of this.jobAssignments()) {
                jobAssignmentVm.buildJsTree();
            }
            this.hideProgressSpinner();
        });
    }

    loadOrgaTree(source: Array<any>, target: Array<any>) {
        for (let orgaEntityDm of source) {
            const jsTreeNode = {
                id: orgaEntityDm.id,
                text: orgaEntityDm.displayName,
                icon: this.getTreeIcon(orgaEntityDm.logicalLevel),
                state: {
                    opened: false,
                    disabled: false,
                    selected: false,
                },
                children: [],
            };
            this.loadOrgaTree(orgaEntityDm.subEntities, jsTreeNode.children);
            target.push(jsTreeNode);
        }
    }

    getTreeIcon(level: number) {
        switch (level) {
        case 1:
            return "fi-home indigo";
        case 2:
            return "fi-folder blue";
        case 3:
            return "fi-torsos cyan";
        default:
            return "";
        }
    }

    save() {
        if (this.isSaving()) return;
        if (!this.isAuthorComplete()) {
            if (!window.confirm("Required information is missing. Do you really want to save this author?")) {
                return;
            }
        }
        if (!this.checkWorkplaceAddresses()) {
            this.showAlert({ type: "error", message: "Two workplaces cannot have the same address." });
            return;
        }
        this.isSaving(true);

        this.putJson(
            this.url,
            ko.toJSON(this, this.ignoreJsonProperties(['orgaTreeNodes', 'addresses', 'predefinedFunctions', 'languages'])),
            this.saveSucceeded, this);
    }

    saveSucceeded(result) {
        if (!result.success) {
            this.showAlert({ type: "error", message: result.message });
        } else {
            this.showAlert({ type: "success", message: result.message });
            for (let jobAssignment of this.jobAssignments()) {
                jobAssignment.isNew = false;
            }
        }
        this.isSaving(false);
    }

    checkWorkplaceAddresses() : boolean {
        const addresses = new Array<string>();
        for (let workplace of this.workplaces()) {
            if (addresses.indexOf(workplace.addressId()) < 0) {
                addresses.push(workplace.addressId());
            } else {
                return false;
            }
        }
        return true;
    }

    isAuthorComplete() {
        let isComplete = ((this.latinFirstName() != null && this.latinFirstName().length > 0) &&
                (this.latinLastName() != null && this.latinLastName().length > 0)) &&
            (this.initials() != null && this.initials().length > 0);
        for (let jobAssignment of this.jobAssignments()) {
            if (!jobAssignment.isDeleted()) {
                if (jobAssignment.orgaEntityId() == null ||
                    jobAssignment.orgaEntityId() === "00000000-0000-0000-0000-000000000000") {
                    isComplete = false;
                }
                if (jobAssignment.service() == null || jobAssignment.service().length === 0) {
                    isComplete = false;
                }
            }
        }
        return isComplete;
    }

    primaryWorkplaceChanged(newPrimaryWorkplace: WorkplaceEditViewModel) {
        for (let wp of this.workplaces()) {
            if (wp.id() !== newPrimaryWorkplace.id()) {
                wp.isPrimaryWorkplace(false);
            }
        }
    }

    createNewJobAssignment() {
        const jobAssignmentVm = new JobAssignmentEditViewModel(this.baseUrl(), null, this.orgaTreeNodes, `${this.isReadOnlyMode()}`);
        jobAssignmentVm.initNew(this.languages());
        this.jobAssignments.push(jobAssignmentVm);
        window.setTimeout(() => {
            jobAssignmentVm.buildJsTree();
        }, 500);
    }

    deleteJobAssignment(jobAssignment) {
        jobAssignment.isDeleted(true);
    }

}
export = AuthorEditViewModel
