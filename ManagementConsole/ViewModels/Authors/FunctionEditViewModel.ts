export class FunctionEditViewModel {

    id = ko.observable("");
    text = ko.observable("");
    languageId = ko.observable("");

    constructor(model) {
        this.id(model.id);
        this.text(model.text);
        this.languageId(model.languageId);
    }
}