export class UserViewModel {

    id = "";
    createdDate = "";
    login = "";
    displayName = "";
    service = "";
    email = "";
    initials = "";
    userGroups = "";
    operatingSystems = "";
    addinVersions = "";

    constructor(model) {
        if (!model) return;

        this.id = model.id;
        this.login = model.login;
        this.createdDate = new Date(model.createdDate).toLocaleString();
        this.displayName = model.displayName;
        this.service = model.service;
        this.email = model.email;
        this.initials = model.initials;
        this.userGroups = model.userGroups;
        this.operatingSystems = model.operatingSystems;
        this.addinVersions = model.addinVersions;
    }
}
