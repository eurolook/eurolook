import { PageViewModel } from "PageViewModel"
import { DeviceViewModel } from "Devices/DeviceViewModel"
import { UserGroupViewModel } from "UserGroups/UserGroupViewModel"

class UsersEditViewModel extends PageViewModel {

    id: string;
    url: string;
    login = ko.observable("");
    userGroups: KnockoutObservableArray<UserGroupViewModel> = ko.observableArray([]);
    createdDate = ko.observable("");
    authorId = ko.observable("");
    dataSyncInterval = ko.observable(0);
    dataSyncDelay = ko.observable(0);
    isCepEnabled = ko.observable(false);
    isInsertNameInline = ko.observable(false);
    isSaving = ko.observable(false);
    devices: KnockoutObservableArray<DeviceViewModel> = ko.observableArray([]);

    constructor(baseUrl: string, id: string, isReadOnly: string) {
        super(baseUrl, "/Users", isReadOnly);
        this.id = id;
        this.url = `${baseUrl}/Users/Edit/${id}`;
        this.bindCtrlSave(this.save);
        this.load();
    }

    load() {
        this.showProgressSpinner();
        this.getJson(this.url, this.loadSucceeded, this);
    }

    loadSucceeded(result) {
        this.login(result.model.login);
        this.userGroups(result.model.userGroups);
        this.createdDate(new Date(result.model.createdDate).toLocaleString());
        this.authorId(result.model.authorId);
        this.dataSyncInterval(result.model.dataSyncInterval);
        this.dataSyncDelay(result.model.dataSyncDelay);
        this.isCepEnabled(result.model.isCepEnabled);
        this.isInsertNameInline(result.model.isInsertNameInline);
        for (let ds of result.model.deviceSettings) {
            this.devices.push(new DeviceViewModel(ds));
        }
        this.devices.sort((a, b) => -(a.lastLoginDate >= b.lastLoginDate));
        this.hideProgressSpinner();
    }

    requestRemoteWipe() {
        this.postJson(`${this.baseUrl()}/Users/RemoteWipe/${this.id}`,
            null,
            this.requestRemoteWipeSucceeded,
            this);
    }

    requestRemoteWipeSucceeded(result) {
        if (result.success) {
            this.showAlert({ type: "success", message: result.message });
            this.devices().forEach(deviceVm => {
                deviceVm.isRemoteWipeRequested(true);
            });
            this.showAlert({ type: "success", message: result.message });
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
        this.closeModal();
    }

    save() {
        if (this.isSaving()) return;
        this.isSaving(true);
        this.putJson(this.url, ko.toJSON(this), this.saveSucceeded, this);
    }

    saveSucceeded(result) {
        if (!result.success) {
            this.showAlert({ type: "error", message: result.message });
        } else {
            this.showAlert({ type: "success", message: result.message });
        }
        this.isSaving(false);
    }
}
export = UsersEditViewModel;
