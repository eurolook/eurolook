import { PaginatedTableViewModel } from "PaginatedTableViewModel"
import { UserViewModel } from "Users/UserViewModel"

class UsersIndexViewModel extends PaginatedTableViewModel {

    users: KnockoutObservableArray<UserViewModel> = ko.observableArray([]);
    newUserName = ko.observable("");
    forceCreate = ko.observable(false);
    deleteModalTitle = ko.observable("");
    reviveModalTitle = ko.observable("");
    searchRequest: JQueryXHR;
    deletingUser: UserViewModel;
    revivingUser: UserViewModel;

    constructor(baseUrl: string, isReadOnly: string) {
        super(baseUrl, "/Users", isReadOnly);
        this.pageSizes = ko.observableArray([20, 50, 100, 500, 1000]);
        if (!this.orderProperty()) {
            this.orderProperty("created");
            this.orderAscending(false);
        }
        this.loadPageData();
        $("#SearchBox").focus();
    }

    loadPageData() {
        if (this.searchRequest && this.searchRequest.readyState < 4) {
            this.searchRequest.abort();
        }
        this.showProgressSpinner();
        this.searchRequest = this.getJson(this.getPageUrl(this.baseUrl() + "/Users"),
            this.loadPageDataSucceeded,
            this);
    }

    loadPageDataSucceeded(result) {
        this.users.removeAll();
        if (result.success === true) {
            const page = result.model;
            for (let user of page.data) {
                this.users.push(new UserViewModel(user));
            }
            this.setPagination(page);
        }
        this.hideProgressSpinner();
    }

    createUser() {
        this.postJson(`${this.baseUrl()}/Users/Create`,
            ko.toJSON({ userName: this.newUserName(), forceCreate: this.forceCreate() }),
            this.createUserSucceeded,
            this);
    }

    createUserSucceeded(data) {
        this.closeModal();
        if (data.success === true) {
            this.showAlert({ type: "success", message: data.message });
            this.searchInput(this.newUserName());
            this.tableMode("normal");
            this.loadPageData();
        } else {
            this.showAlert({ type: "error", message: data.message });
        }
    }

    deleteUser() {
        if (!this.deletingUser) return;
        this.deleteJson(`${this.baseUrl()}/Users/Delete/${this.deletingUser.id}`,
            ko.toJSON(this.deletingUser),
            this.deleteUserSucceeded,
            this);
    }

    deleteUserSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var deleteId = this.deletingUser.id;
            this.users.remove(item => item.id === deleteId);
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    deleteUserPermanently() {
        if (!this.deletingUser) return;
        this.deleteJson(`${this.baseUrl()}/Users/DeletePermanently/${this.deletingUser.id}`,
            ko.toJSON(this.deletingUser),
            this.deleteUserPermanentlySucceeded,
            this);
    }

    deleteUserPermanentlySucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var deleteId = this.deletingUser.id;
            this.users.remove(item => item.id === deleteId);
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    reviveUser() {
        if (!this.revivingUser) return;
        this.postJson(`${this.baseUrl()}/Users/Revive/${this.revivingUser.id}`,
            ko.toJSON(this.revivingUser),
            this.reviveUserSucceeded,
            this);
    }

    reviveUserSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var reviveId = this.revivingUser.id;
            this.users.remove(item => item.id === reviveId);
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    linkUsers() {
        this.postJson(`${this.baseUrl()}/Users/LinkAll`, null, this.genericSucceeded, this);
    }

    cleanUpUsers() {
        this.postJson(`${this.baseUrl()}/Users/CleanUpAll`, null, this.genericSucceeded, this);
    }

    genericSucceeded(result) {
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    showDeleteModal(viewModel) {
        this.deletingUser = viewModel;
        this.deleteModalTitle(viewModel.displayName);
        // ReSharper disable once TsResolvedFromInaccessibleModule
        $("#DeleteModal").foundation("reveal", "open");
    }

    showDeletePermanentlyModal(viewModel) {
        this.deletingUser = viewModel;
        this.deleteModalTitle(viewModel.displayName);
        // ReSharper disable once TsResolvedFromInaccessibleModule
        $("#DeletePermanentlyModal").foundation("reveal", "open");
    }

    showReviveModal(viewModel) {
        this.revivingUser = viewModel;
        this.reviveModalTitle(viewModel.displayName);
        // ReSharper disable once TsResolvedFromInaccessibleModule
        $("#ReviveModal").foundation("reveal", "open");
    }
}
export = UsersIndexViewModel;
