using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.ViewModels.Exceptions
{
    public class ExceptionLogDetailViewModel
    {
        [UsedImplicitly]
        public string Message { get; set; }

        [UsedImplicitly]
        public string Created { get; set; }

        [UsedImplicitly]
        public string Originator { get; set; }

        public string ClientVersion { get; set; }

        public string OfficeVersion { get; set; }
    }
}
