using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Eurolook.Common.Extensions;
using Eurolook.Data.ExceptionLog;
using Eurolook.ManagementConsole.Models;
using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.ViewModels.Exceptions
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    public class ExceptionLogDetailsViewModel
    {
        private readonly ExceptionBucket _exceptionBucket;

        public ExceptionLogDetailsViewModel()
        {
            // default constructor is used by ASP.NET MVC for creating the model from a HTTP request
        }

        public ExceptionLogDetailsViewModel(ExceptionBucket exceptionBucket, IEnumerable<(string Version, int Count)> affectedOfficeVersions, IEnumerable<(string Version, int Count)> affectedClientVersions)
        {
            _exceptionBucket = exceptionBucket;

            var affectedVersions = affectedClientVersions as IList<(string Version, int Count)> ?? affectedClientVersions.ToList();
            ClientVersions = affectedVersions.Select(
                g => new PieChartSegment
                {
                    Name = g.Version,
                    Y = g.Count,
                });

            affectedVersions = affectedOfficeVersions as IList<(string Version, int Count)> ?? affectedOfficeVersions.ToList();
            OfficeVersions = affectedVersions.Select(
                g => new PieChartSegment
                {
                    Name = g.Version,
                    Y = g.Count,
                });
        }

        [UsedImplicitly]
        public string Id
        {
            get { return _exceptionBucket.Id.ToHexString(); }
        }

        [UsedImplicitly]
        public string ShortId
        {
            get { return _exceptionBucket.Id.ToHexString().Substring(0, 7); }
        }

        [UsedImplicitly]
        public string LoggerName
        {
            get { return _exceptionBucket.LoggerName; }
        }

        [UsedImplicitly]
        public string ExceptionType
        {
            get { return _exceptionBucket.ExceptionType; }
        }

        [UsedImplicitly]
        public string ExceptionTypeShort
        {
            get
            {
                return _exceptionBucket.ExceptionType.Substring(
                    _exceptionBucket.ExceptionType.LastIndexOf(".", StringComparison.InvariantCultureIgnoreCase) + 1);
            }
        }

        [UsedImplicitly]
        public string StackTrace
        {
            get { return _exceptionBucket.StackTrace; }
        }

        [UsedImplicitly]
        public IEnumerable<PieChartSegment> ClientVersions { get; }

        [UsedImplicitly]
        public IEnumerable<PieChartSegment> OfficeVersions { get; }
    }
}
