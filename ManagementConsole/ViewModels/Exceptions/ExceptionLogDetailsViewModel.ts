import { PaginatedTableViewModel } from "PaginatedTableViewModel"
declare var Chart: typeof import("../../node_modules/chart.js/auto");

class ExceptionLogDetailsViewModel extends PaginatedTableViewModel {
    id = ko.observable("");
    shortId = ko.observable("");
    loggerName = ko.observable("");
    exceptionType = ko.observable("");
    exceptionTypeShort = ko.observable("");
    stackTrace = ko.observable("");

    exceptionLogDetails = ko.observableArray();
    updateExceptionLogRequest = null;
    hasLoaded = ko.observable(false);
    hasResult = ko.observable(false);

    filter = ko.observable();

    constructor(baseUrl, model, isReadOnly: string, chart) {
        super(baseUrl, '/Exceptions/ExceptionLogDetails/' + model.id, isReadOnly);
        Chart = chart;

        this.orderProperty("occurred");
        this.orderAscending(false);

        const rangeFilter = parseInt(this.getParameterByName("filter"));
        if (isNaN(rangeFilter)) {
            this.filter(0);
        } else {
            this.filter(rangeFilter);
        }

        this.id(model.id);
        this.shortId(model.shortId);
        this.loggerName(model.loggerName);
        this.exceptionType(model.exceptionType);
        this.exceptionTypeShort(model.exceptionTypeShort);
        this.stackTrace(model.stackTrace);

        const clientVersionChart = this.createPieChart(model.clientVersions, "clientVersionChart");
        const officeVersionChart = this.createPieChart(model.officeVersions, "officeVersionChart");

        this.loadPageData();
    }

    loadPageData() {
        if (this.updateExceptionLogRequest && this.updateExceptionLogRequest.readyState < 4) {
            this.updateExceptionLogRequest.abort();
        }
        this.hasLoaded(false);
        this.hasResult(false);
        this.exceptionLogDetails();

        this.updateExceptionLogRequest = this.getJson(
            this.getPageUrl(this.baseUrl() + "/Exceptions/ExceptionLogDetailsEntries/" + this.id()),
            this.loadPageDataSucceeded,
            this
        );
    }

    loadPageDataSucceeded(result) {
        if (result.success) {
            this.hasLoaded(true);
            this.hasResult(result && result.model.data.length > 0);
            const page = result.model;
            this.exceptionLogDetails(page.data);
            this.setPagination(page);
        } else {
            this.hasLoaded(true);
        }
    }

    getPageUrl(baseUrl) {
        return super.getPageUrl(baseUrl) + "&filter=" + this.filter();
    }

    createPieChart(data, renderTo, datasets = null) {
        const total = data.reduce((acc, row) => acc + row.y, 0);
        if (datasets == null) {
            datasets = [
                {
                    label: "Occurrences",
                    data: data.map(row => row.y),
                }
            ]
        }

        return new Chart.Chart(
            document.getElementById(renderTo) as HTMLCanvasElement,
            {
                type: 'doughnut',
                options: {
                    responsive: true,
                    maintainAspectRatio: true,
                    plugins: {
                        title: {
                            display: true,
                            text: "Source: Exception Logging"
                        },
                        legend: {
                            display: true,
                            position: "bottom",
                        },
                        tooltip: {
                            callbacks: {
                                title: context => {
                                    const result: string[] = [];

                                    for (var i = 0; i < context.length; i++) {
                                        let label = context[i].label.substring(0, context[i].label.lastIndexOf(':'));

                                        if (context[i].parsed !== null) {
                                            result.push(`${label}: ${(context[i].parsed / total * 100).toFixed(1)}%`);
                                        } else {
                                            result.push(label);
                                        }
                                    }

                                    return result;
                                },
                                label: context => {
                                    let label = context.dataset.label || '';
                                    if (label) {
                                        label += ": ";
                                    }

                                    if (context.parsed !== null) {
                                        label += `${context.parsed} out of ${total}`;
                                    }

                                    return label;
                                }
                            }
                        }
                    }
                },
                data: {
                    labels: data.map(row => `${row.name}: ${row.y}`),
                    datasets: datasets
                }
            });
    }

}
export = ExceptionLogDetailsViewModel
