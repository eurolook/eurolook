import { PaginatedTableViewModel } from "PaginatedTableViewModel";

class ExceptionLog extends PaginatedTableViewModel {
    exceptionLogData = ko.observableArray();
    updateExceptionLogRequest = null;
    isLoading = ko.observable(true);
    hasResult = ko.observable(false);
    users = ko.observable("");

    monthsFilters = ko.observableArray([
        {
            displayName: "Last Week (today minus 7 days)",
            value: 0,
        },
        {
            displayName: "Last 2 Weeks (today minus 14 days)",
            value: 1,
        },
        {
            displayName: "Last Month (today minus 1 month)",
            value: 2,
        },
        {
            displayName: "Last 3 Months (today minus 3 months)",
            value: 3,
        },
        {
            displayName: "Last 6 Months (today minus 6 months)",
            value: 4,
        },
    ]);

    selectedMonthsFilter = ko.observable(0);

    constructor(baseUrl, isReadOnly: string) {
        super(baseUrl, "/Exceptions", isReadOnly);
        this.orderProperty("count");
        this.orderAscending(false);

        const rangeFilter = parseInt(this.getParameterByName("rangeFilter"));
        if (isNaN(rangeFilter)) {
            this.selectedMonthsFilter(0);
        } else {
            this.selectedMonthsFilter(rangeFilter);
        }

        const userFilter = this.getParameterByName("users");
        if (userFilter === undefined) {
            this.users("");
        } else {
            this.users(userFilter);
        }

        this.selectedMonthsFilter.subscribe(() => {
            this.pageNumber(1);
            this.loadPageData();
        }, this);
        this.users.subscribe(() => {
            this.pageNumber(1);
            this.loadPageData();
        }, this);

        this.users.extend({ rateLimit: { timeout: 500, method: "notifyWhenChangesStop" } });
        this.loadPageData();
    }

    loadPageData() {
        if (this.updateExceptionLogRequest && this.updateExceptionLogRequest.readyState < 4) {
            this.updateExceptionLogRequest.abort();
        }
        this.isLoading(true);
        this.hasResult(false);
        this.exceptionLogData();

        this.updateExceptionLogRequest = this.getJson(
            this.getPageUrl(this.baseUrl() + "/Exceptions/exceptionloginfo/" + this.selectedMonthsFilter()),
            this.loadPageDataSucceeded,
            this
        );
    }

    loadPageDataSucceeded(result) {
        if (result.success) {
            this.isLoading(false);
            this.hasResult(result && result.model.data.length > 0);
            var page = result.model;
            this.exceptionLogData(page.data);
            this.setPagination(page);
        } else {
            this.isLoading(false);
        }
    }

    getPageUrl(baseUrl) {
        return super.getPageUrl(baseUrl) + "&rangeFilter=" + this.selectedMonthsFilter() + "&users=" + this.users();
    }
}
export = ExceptionLog;
