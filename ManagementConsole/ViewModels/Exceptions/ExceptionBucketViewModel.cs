using System;
using System.Diagnostics.CodeAnalysis;
using Eurolook.Common.Extensions;
using Eurolook.Data.ExceptionLog;

namespace Eurolook.ManagementConsole.ViewModels.Exceptions
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    public class ExceptionBucketViewModel
    {
        public ExceptionBucketViewModel(ExceptionBucket bucket)
        {
            string formattedBucketId = bucket.Id.ToHexString();
            Id = formattedBucketId;

            // take first 7 chars of SHA-1 hash (like git does)
            ShortId = formattedBucketId.Substring(0, 7);
            LoggerName = bucket.LoggerName;
            LastMessage = bucket.LastMessage;
            ExceptionType = bucket.ExceptionType;
            ExceptionTypeShort =
                bucket.ExceptionType.Substring(
                    bucket.ExceptionType.LastIndexOf(".", StringComparison.InvariantCultureIgnoreCase) + 1);
            StackTrace = bucket.StackTrace;
            Modified = bucket.ModifiedUtc.ToLocalTime().ToString(/*CultureInfo.CurrentUICulture*/);
            ModifiedDate = bucket.ModifiedUtc;
        }

        public DateTime ModifiedDate { get; set; }

        public string Id { get; }

        public string ShortId { get; }

        public string LoggerName { get; }

        public string ExceptionType { get; }

        public string ExceptionTypeShort { get; }

        public string LastMessage { get; }

        public string StackTrace { get; }

        public string Modified { get; }

        public int Count { get; set; }

        public int AffectedUsers{ get; set; }
    }
}
