import {PaginatedTableViewModel} from "PaginatedTableViewModel"
import { UserFeedbackViewModel } from "Feedback/UserFeedbackViewModel"

class FeedbackIndexViewModel extends PaginatedTableViewModel{
   
    isLoading = ko.observable(true);
    hasResult = ko.observable(false);
    searchRequest: JQueryXHR;
    feedback: KnockoutObservableArray<UserFeedbackViewModel> = ko.observableArray([]);
    selectedFeedback: KnockoutObservableArray<UserFeedbackViewModel> = ko.observableArray([]);
    selectedMonthsFilter = ko.observable(0);
    monthsFilters = ko.observableArray([{
        displayName: "Last Week (today minus 7 days)",
        value: 0
    }, {
        displayName: "Last 2 Weeks (today minus 14 days)",
        value: 1
    }, {
        displayName: "Last Month (today minus 1 month)",
        value: 2
    }, {
        displayName: "Last 3 Months (today minus 3 months)",
        value: 3
    }, {
        displayName: "All",
        value: 100
    }]);
    

    constructor(baseUrl, isReadOnly: string) {
        super(baseUrl, "/Feedback", isReadOnly);
        if (!this.orderProperty()) {
            this.orderProperty("submitted");
            this.orderAscending(false);
            this.selectedMonthsFilter.subscribe(this.loadPageData, this);
        }
        this.loadPageData();
    }

    loadPageData() {
        if (this.searchRequest && this.searchRequest.readyState < 4) {
            this.searchRequest.abort();
        }
        this.showProgressSpinner();
        this.searchRequest = this.getJson(this.getPageUrl(this.baseUrl() + "/Feedback") + '&filter=' + this.selectedMonthsFilter(),
            this.loadPageDataSucceeded,
            this);
    }

    loadPageDataSucceeded(result) {
        this.feedback.removeAll();
        if (result.success === true) {
            const page = result.model;
            for (let fb of page.data) {
                this.feedback.push(new UserFeedbackViewModel(fb));
            }
            this.setPagination(page);
        }
        this.hideProgressSpinner();

    }

    isSelectedAll = ko.pureComputed({
        read: function () {
            return this.selectedFeedback().length === this.feedback().length;
        },
        write: function (value) {
            this.selectedFeedback(value ? this.feedback().slice(0) : []);
        },
        owner: this
    });

    hasSelection = ko.computed(function () {
        return this.selectedFeedback().length > 0;
    }, this);

    deleteFeedback() {
        if (!this.selectedFeedback()) {
            this.closeModal();
            return;
        }
        this.deleteJson(`${this.baseUrl()}/Feedback/Delete`,
            ko.toJSON(this.selectedFeedback()),
            this.deleteTextsSucceeded,
            this);
    }

    deleteTextsSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.closeModal();
            this.showAlert({ type: "success", message: result.message });
            this.feedback.removeAll(this.selectedFeedback());
        } else {
            this.closeModal();
            this.showAlert({ type: "error", message: result.message });
        }
    }

}

export = FeedbackIndexViewModel;
