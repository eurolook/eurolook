export class UserFeedbackViewModel {

    id: number;
    message = ko.observable();
    version = ko.observable();
    submitted = ko.observable();
    rating = ko.observable();
    userName = ko.observable();

    constructor(model) {
        if (!model) return;

        this.id = model.id;

        this.message(model.message);
        this.version(model.version);
        const date = new Date(model.submitted);
        this.submitted(date.toLocaleString());
        if (model.rating === 1) {
            this.rating = ko.observable(":)");
        }
        else if (model.rating === 2) {
            this.rating = ko.observable(":|");
        }
        else if (model.rating === 3) {
            this.rating = ko.observable(":(");
        } else {
            this.rating = ko.observable("?");
        }
        this.userName(model.user.login);
    }
}
