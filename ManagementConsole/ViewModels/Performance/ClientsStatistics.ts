import {PageViewModel} from "../PageViewModel"
declare var Chart: typeof import("../../node_modules/chart.js/auto");

class ClientsStatistics extends PageViewModel {

    addInLoadedChart = null;
    addInLoadedDetailsChart = null;
    creationDialogLoadedChart = null;
    creationDialogLoadedDetailsChart = null;
    documentCreatedChart = null;
    documentCreatedDetailsChart = null;
    dataSyncFinishedChart = null;
    dataSyncFinishedDetailsChart = null;

    updateChartsRequest: JQueryXHR = null;

    updateAddInLoadedChartProgressTimer: number = null;
    updateCreationDialogLoadedChartProgressTimer: number = null;
    updateDocumentCreatedChartProgressTimer: number = null;
    updateDataSyncFinishedChartProgressTimer: number = null;

    chartDataLimit = ko.observable(30);

    monthsFilters = ko.observableArray([
        {
            displayName: "Last Month",
            value: 2
        }, {
            displayName: "3 Months",
            value: 3
        }, {
            displayName: "6 Months",
            value: 4
        }, {
            displayName: "12 Months",
            value: 5
        }
    ]);

    selectedMonthsFilter = ko.observable(3);


    constructor(baseUrl, isReadOnly: string, chart) {
        super(baseUrl, "/Performance", isReadOnly);

        Chart = chart;
        
        this.selectedMonthsFilter.subscribe(function(_) {
            this.updateCharts();
        }.bind(this));

        this.updateCharts();
    }

    updateCharts() {
        this.showProgressBars();
        this.destroyCharts();

        if (this.updateChartsRequest && this.updateChartsRequest.readyState < 4) {
            this.updateChartsRequest.abort();
        }
        // open a new request
        this.updateChartsRequest = this.getJson(
            this.baseUrl() + "/Performance/clientstatistics/" + this.selectedMonthsFilter(),
            this.updateChartsSucceeded,
            this);
    }

    updateChartsSucceeded(result) {
        this.documentCreatedChart = this.createHistoryChart("documentCreatedChart", "Duration of Document Creation", result.documentCreated.categories, result.documentCreated.series);
        this.documentCreatedDetailsChart = this.createDetailsChart("documentCreatedDetailsChart", "Duration of Document Creation", result.documentCreatedDetails.categories, result.documentCreatedDetails.series);

        this.addInLoadedChart = this.createHistoryChart("addInLoadedChart", "Duration of Addin Loading", result.addInLoaded.categories, result.addInLoaded.series);
        this.addInLoadedDetailsChart = this.createDetailsChart("addInLoadedDetailsChart", "Duration of Addin Loading", result.addInLoadedDetails.categories, result.addInLoadedDetails.series);

        this.creationDialogLoadedChart = this.createHistoryChart("creationDialogLoadedChart", "Duration of Creation Dialog Loading", result.creationDialogLoaded.categories, result.creationDialogLoaded.series);
        this.creationDialogLoadedDetailsChart = this.createDetailsChart("creationDialogLoadedDetailsChart", "Duration of Creation Dialog Loading", result.creationDialogLoadedDetails.categories, result.creationDialogLoadedDetails.series);

        this.dataSyncFinishedChart = this.createHistoryChart("dataSyncFinishedChart", "Duration of Data Sync", result.dataSyncFinished.categories, result.dataSyncFinished.series);
        this.dataSyncFinishedDetailsChart = this.createDetailsChart("dataSyncFinishedDetailsChart", "Duration of Data Sync", result.dataSyncFinishedDetails.categories, result.dataSyncFinishedDetails.series);

        this.hideProgressBars();
    }

    destroyCharts() {
        if (this.addInLoadedChart) {
            this.addInLoadedChart.destroy();
        }
        if (this.addInLoadedDetailsChart) {
            this.addInLoadedDetailsChart.destroy();
        }
        if (this.creationDialogLoadedChart) {
            this.creationDialogLoadedChart.destroy();
        }
        if (this.creationDialogLoadedDetailsChart) {
            this.creationDialogLoadedDetailsChart.destroy();
        }
        if (this.documentCreatedChart) {
            this.documentCreatedChart.destroy();
        }
        if (this.documentCreatedDetailsChart) {
            this.documentCreatedDetailsChart.destroy();
        }
        if (this.dataSyncFinishedChart) {
            this.dataSyncFinishedChart.destroy();
        }
        if (this.dataSyncFinishedDetailsChart) {
            this.dataSyncFinishedDetailsChart.destroy();
        }
    }

    showProgressBars() {
        window.clearTimeout(this.updateAddInLoadedChartProgressTimer);
        this.updateAddInLoadedChartProgressTimer = window.setTimeout(() => { $("#addInLoadedProgressBar").show(); }, 300);

        window.clearTimeout(this.updateCreationDialogLoadedChartProgressTimer);
        this.updateCreationDialogLoadedChartProgressTimer = window.setTimeout(() => { $("#creationDialogLoadedProgressBar").show(); }, 300);

        window.clearTimeout(this.updateDocumentCreatedChartProgressTimer);
        this.updateDocumentCreatedChartProgressTimer = window.setTimeout(() => { $("#documentCreatedProgressBar").show(); }, 300);

        window.clearTimeout(this.updateDataSyncFinishedChartProgressTimer);
        this.updateDataSyncFinishedChartProgressTimer = window.setTimeout(() => { $("#dataSyncFinishedProgressBar").show(); }, 300);
    }

    hideProgressBars() {
        window.clearTimeout(this.updateAddInLoadedChartProgressTimer);
        $("#addInLoadedProgressBar").hide();

        window.clearTimeout(this.updateCreationDialogLoadedChartProgressTimer);
        $("#creationDialogLoadedProgressBar").hide();

        window.clearTimeout(this.updateDocumentCreatedChartProgressTimer);
        $("#documentCreatedProgressBar").hide();

        window.clearTimeout(this.updateDataSyncFinishedChartProgressTimer);
        $("#dataSyncFinishedProgressBar").hide();
    }

    createHistoryChart(id, title, categories, series) {
        return new Chart.Chart(
            document.getElementById(id) as HTMLCanvasElement,
            {
                type: 'line',
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        y: {
                            title: {
                                display: true,
                                text: title
                            },
                            beginAtZero: true,
                            suggestedMin: 0,
                            ticks: {
                                precision: 0
                            }
                        }
                    },
                    plugins: {
                        title: {
                            display: false,
                        },
                        subtitle: {
                            display: true,
                            text: "Source: User Experience Program"
                        },
                        legend: {
                            display: true,
                            position: "bottom",
                        },
                        tooltip: {
                            callbacks: {
                                label: context => {
                                    let label = context.dataset.label || "";

                                    if (label) {
                                        label += ": ";
                                    }

                                    if (context.parsed.y !== null) {
                                        label += `${context.parsed.y}s`;
                                    }

                                    return label;
                                }
                            }
                        }
                    }
                },
                data: {
                    labels: categories,
                    datasets: series.map(s => ({
                        label: s.name,
                        data: s.data,
                        borderColor: s.color,
                        backgroundColor: s.color + "80"
                    }))
                }
            }
        );
    }

    createDetailsChart(id, title, categories, series) {
        return new Chart.Chart(
            document.getElementById(id) as HTMLCanvasElement,
            {
                type: 'bar',
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    indexAxis: 'x',
                    scales: {
                        y: {
                            title: {
                                display: true,
                                text: title
                            },
                            position: "left"
                        },
                    },
                    plugins: {
                        title: {
                            display: false,
                        },
                        subtitle: {
                            display: true,
                            text: "Source: User Experience Program"
                        },
                        legend: {
                            display: true,
                            position: "bottom"
                        },
                        tooltip: {
                            callbacks: {
                                label: context => {
                                    let label = context.dataset.label || "";

                                    if (label) {
                                        label += ": ";
                                    }

                                    if (context.parsed.y !== null) {
                                        label += `${context.parsed.y}s`;
                                    }

                                    return label;
                                }
                            }
                        }
                    },
                },
                data: {
                    labels: categories,
                    datasets: series.map(s => ({
                        label: s.name,
                        data: s.data,
                        backgroundColor: s.color
                    }))
                }
            }
        );
    }

}
export = ClientsStatistics
