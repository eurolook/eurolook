import { PageViewModel } from "../PageViewModel"
declare var Chart: typeof import("../../node_modules/chart.js/auto");

class ServersStatistics extends PageViewModel {

    serverInitializationChart = null;
    serverGetUserProfileChart = null;
    serverGetUpdatesChart = null;

    updateChartsRequest: JQueryXHR = null;

    updateServerInitializationChartProgressTimer: number = null;
    updateServerGetUserProfileChartProgressTimer: number = null;
    updateServerGetUpdatesChartProgressTimer: number = null;

    chartDataLimit = ko.observable(30);

    monthsFilters = ko.observableArray([
        {
            displayName: "Last Month (daily)",
            value: 2
        }, {
            displayName: "3 Months",
            value: 3
        }, {
            displayName: "6 Months",
            value: 4
        }, {
            displayName: "12 Months",
            value: 5
        }
    ]);

    selectedMonthsFilter = ko.observable(3);


    constructor(baseUrl, isReadOnly: string, chart) {
        super(baseUrl, "/Performance", isReadOnly);

        Chart = chart;

        this.selectedMonthsFilter.subscribe(function (_) {
            this.updateCharts();
        }.bind(this));

        this.updateCharts();
    }

    async updateCharts() {
        this.showProgressBars();
        this.destroyCharts();

        if (this.updateChartsRequest && this.updateChartsRequest.readyState < 4) {
            this.updateChartsRequest.abort();
        }
        // open a new request
        this.updateChartsRequest = this.getJson(
            this.baseUrl() + "/Performance/serverstatistics/" + this.selectedMonthsFilter(),
            this.updateChartsSucceeded,
            this);
    }

    updateChartsSucceeded(result) {
        this.serverGetUpdatesChart = this.createHistoryChart("serverGetUpdatesChart", "Duration of GetUpdates", result.getUpdates.categories, result.getUpdates.series);
        this.serverGetUserProfileChart = this.createHistoryChart("serverGetUserProfileChart", "Duration of GetUserProfile", result.getUserProfile.categories, result.getUserProfile.series);
        this.serverInitializationChart = this.createHistoryChart("serverInitializationChart", "Duration of Initialization", result.getInitialization.categories, result.getInitialization.series);

        this.hideProgressBars();
    }

    destroyCharts() {
        if (this.serverGetUpdatesChart) {
            this.serverGetUpdatesChart.destroy();
        }
        if (this.serverGetUserProfileChart) {
            this.serverGetUserProfileChart.destroy();
        }
        if (this.serverInitializationChart) {
            this.serverInitializationChart.destroy();
        }
    }

    showProgressBars() {
        window.clearTimeout(this.updateServerInitializationChartProgressTimer);
        this.updateServerInitializationChartProgressTimer = window.setTimeout(() => { $("#serverInitializationProgressBar").show(); }, 300);

        window.clearTimeout(this.updateServerGetUserProfileChartProgressTimer);
        this.updateServerGetUserProfileChartProgressTimer = window.setTimeout(() => { $("#serverGetUserProfileProgressBar").show(); }, 300);

        window.clearTimeout(this.updateServerGetUpdatesChartProgressTimer);
        this.updateServerGetUpdatesChartProgressTimer = window.setTimeout(() => { $("#serverGetUpdatesProgressBar").show(); }, 300);
    }

    hideProgressBars() {
        window.clearTimeout(this.updateServerInitializationChartProgressTimer);
        $("#serverInitializationProgressBar").hide();

        window.clearTimeout(this.updateServerGetUserProfileChartProgressTimer);
        $("#serverGetUserProfileProgressBar").hide();

        window.clearTimeout(this.updateServerGetUpdatesChartProgressTimer);
        $("#serverGetUpdatesProgressBar").hide();
    }

    createHistoryChart(id, title, categories, series) {
        return new Chart.Chart(
            document.getElementById(id) as HTMLCanvasElement,
            {
                type: 'line',
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        y: {
                            title: {
                                display: true,
                                text: title
                            },
                            beginAtZero: true,
                            suggestedMin: 0,
                            ticks: {
                                precision: 0
                            }
                        }
                    },
                    plugins: {
                        title: {
                            display: false,
                        },
                        subtitle: {
                            display: true,
                            text: "Source: User Experience Program"
                        },
                        legend: {
                            display: true,
                            position: "bottom",
                        },
                        tooltip: {
                            callbacks: {
                                label: context => {
                                    let label = context.dataset.label || "";

                                    if (label) {
                                        label += ": ";
                                    }

                                    if (context.parsed.y !== null) {
                                        label += `${context.parsed.y}s`;
                                    }

                                    return label;
                                }
                            }
                        }
                    }
                },
                data: {
                    labels: categories,
                    datasets: series.map(s => ({
                        label: s.name,
                        data: s.data
                    }))
                }
            }
        );
    }

}
export = ServersStatistics
