export class BrickGroupViewModel {

    id = ko.observable("");
    name = ko.observable("");
    description = ko.observable("");
    isSelected = ko.observable(false);

    constructor(model) {
        if (!model) return;

        this.id(model.id);
        this.name(model.name);
        this.description(model.description);
        this.isSelected(model.isSelected);
    }
}