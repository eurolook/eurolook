﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.Models;

namespace Eurolook.ManagementConsole.ViewModels.BrickGroups
{
    public class BrickGroupsIndexViewModel
    {
        public List<BrickGroupViewModel> BrickGroups { get; set; }

        public BrickGroupsIndexViewModel(IEnumerable<BrickGroup> brickGroups)
        {
            BrickGroups = new List<BrickGroupViewModel>();
            foreach (var brickGroup in brickGroups)
            {
                BrickGroups.Add(new BrickGroupViewModel(brickGroup));
            }
        }
    }
}
