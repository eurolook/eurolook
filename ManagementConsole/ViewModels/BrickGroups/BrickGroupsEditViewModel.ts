import { PageViewModel } from "PageViewModel"
import { BrickViewModel } from "Bricks/BrickViewModel"

class BrickGroupsEditViewModel extends PageViewModel {

    id: string;
    url: string;
    isSaving = ko.observable(false);
    name = ko.observable("");
    description = ko.observable("");
    displayName = ko.observable("");
    acronym = ko.observable("");
    uiPositionIndex = ko.observable(0);
    isMultipleChoice = ko.observable(false);
    colorLuminance = ko.observable(0);
    iconInfo = ko.observable("");
    iconImage = ko.observable("");
    hasIcon = ko.observable(false);
    isIconDeleted = ko.observable(false);
    iconFileFormData: FormData;
    members = ko.observableArray();
    luminanceValues = [
        { name: "Medium", value: 0 },
        { name: "Bright", value: 1 },
        { name: "Dark", value: 2 },
        { name: "Highlighted", value: 3}
    ];
    documentModels: KnockoutObservableArray<string> = ko.observableArray([]);

    constructor(baseUrl: string, id: string, isReadOnly: string) {
        super(baseUrl, "/BrickGroups", isReadOnly);
        this.id = id;
        this.url = baseUrl + "/BrickGroups/Edit/" + id;
        this.bindCtrlSave(this.save);
        this.load();
    }

    load() {
        this.showProgressSpinner();
        this.getJson(this.url, this.loadSucceeded, this);
    }

    loadSucceeded(result) {
        this.name(result.model.name);
        this.description(result.model.description);
        this.displayName(result.model.displayName);
        this.acronym(result.model.acronym);
        this.uiPositionIndex(result.model.uiPositionIndex);
        this.isMultipleChoice(result.model.isMultipleChoice);
        this.iconInfo(result.model.iconInfo);
        this.iconImage(result.model.iconImage);
        this.hasIcon(result.model.hasIcon);
        this.colorLuminance(result.model.colorLuminance);
        this.isIconDeleted(result.model.isIconDeleted);
        for (let brick of result.model.members) {
            this.members.push(new BrickViewModel(brick));
        }
        this.documentModels(result.model.documentModels);
        this.hideProgressSpinner();
    }

    save() {
        if (this.isSaving()) {
            return;
        }
        this.isSaving(true);
        const vm = ko.toJS(this);
        jQuery.when(
            this.postFormData(`${this.baseUrl()}/BrickGroups/Icon/${this.id}`, this.iconFileFormData, this),
            this.putJson(`${this.baseUrl()}/BrickGroups/Edit/${this.id}`, ko.toJSON(vm), null, this)
        ).then(this.saveSucceeded).fail(this.saveFailed);
    }

    saveSucceeded(saveIconResponse, saveBrickGroupResponse) {
        const that: BrickGroupsEditViewModel = this[0];
        const saveIconResult = saveIconResponse[0];
        const saveBrickGroupResult = saveBrickGroupResponse[0];
        if (saveIconResult.success === true && saveBrickGroupResult.success === true) {
            if (saveIconResult.model != null) {
                that.iconImage(saveIconResult.model);
            }
            that.showAlert({ type: "success", message: saveBrickGroupResult.message });
        } else {
            // Server side error
            let errorMsg = "";
            if (saveIconResult.success === false)
                errorMsg += saveIconResult.message;
            if (saveBrickGroupResult.success === false)
                errorMsg += saveBrickGroupResult.message;
            that.showAlert({ type: "error", message: errorMsg });
        }
        that.isSaving(false);
    }

    saveFailed(jqXhr: JQueryXHR, textStatus: string, errorThrown) {
        const that: BrickGroupsEditViewModel = this[0];
        that.showAlert({ type: "error", message: `HTTP error: ${jqXhr.statusText}` });
        that.isSaving(false);
    }

    chooseIconFile() {
        $("#iconFile").click();
    };

    choseIconFile(file) {
        this.iconFileFormData = new FormData();
        this.iconFileFormData.append("file", file);
        this.iconInfo(file.name);
        this.isIconDeleted(false);
    };

    deleteIcon() {
        this.iconImage(null);
        this.iconInfo(null);
        this.hasIcon(false);
        this.isIconDeleted(true);
    };
}
export = BrickGroupsEditViewModel
