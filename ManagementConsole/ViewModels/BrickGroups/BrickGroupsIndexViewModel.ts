import { PaginatedTableViewModel } from "PaginatedTableViewModel"
import { BrickGroupViewModel } from "BrickGroups/BrickGroupViewModel"

class BrickGroupsIndexViewModel extends PaginatedTableViewModel {

    isSelectAll = ko.observable(false);
    brickGroups: KnockoutObservableArray<BrickGroupViewModel> = ko.observableArray([]);
    searchRequest: JQueryXHR;
    newGroupName = ko.observable("");
    deleteModalTitle = ko.observable("");
    deletingGroup: BrickGroupViewModel;

    constructor(baseUrl, isReadOnly: string) {
        super(baseUrl, "/BrickGroups", isReadOnly);
        this.isSelectAll.subscribe(this.selectAllChanged, this);
        if (!this.orderProperty()) {
            this.orderProperty("name");
            this.orderAscending(true);
        }
        this.loadPageData();
        $("#SearchBox").focus();
    }

    loadPageData() {
        if (this.searchRequest && this.searchRequest.readyState < 4) {
            this.searchRequest.abort();
        }
        this.showProgressSpinner();
        this.searchRequest = this.getJson(
            this.getPageUrl(`${this.baseUrl()}/BrickGroups`),
            this.loadPageDataSucceeded,
            this);
    }

    loadPageDataSucceeded(result) {
        this.brickGroups.removeAll();
        if (result.success === true) {
            const page = result.model;
            for (let brickGroup of page.data) {
                this.brickGroups.push(new BrickGroupViewModel(brickGroup));
            }
            this.setPagination(page);
        }
        this.hideProgressSpinner();
    }

    createGroup() {
        const viewModel = new BrickGroupViewModel(null);
        viewModel.name(this.newGroupName());
        this.postJson(`${this.baseUrl()}/BrickGroups/Create`,
            ko.toJSON(viewModel),
            this.createGroupSucceeded,
            this);
    }

    createGroupSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            this.brickGroups.push(new BrickGroupViewModel(result.model));
        }
        else {
            this.showAlert({ type: "error", message: result.message, timeout: 8000 });
        }
    }

    deleteGroup() {
        if (!this.deletingGroup) return;
        this.deleteJson(`${this.baseUrl()}/BrickGroups/Delete/${this.deletingGroup.id()}`,
            ko.toJSON(this.deletingGroup),
            this.deleteGroupSucceeded,
            this);
    }

    deleteGroupSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var deleteId = this.deletingGroup.id();
            this.brickGroups.remove(item => item.id() === deleteId);
        }
        else {
            this.showAlert({ type: "error", message: result.message, timeout: 8000 });
        }
    }

    deleteGroupFailed(jqXhr: JQueryXHR, textStatus: string, errorThrown) {
        this.closeModal();
        this.showAlert({ type: "error", message: `HTTP error: ${errorThrown}` });
    }

    selectAllChanged(newValue: boolean) {
        for (let brickGroupVm of this.brickGroups()) {
            brickGroupVm.isSelected(newValue);
        }
    }

    showDeleteModal(viewModel) {
        this.deletingGroup = viewModel;
        this.deleteModalTitle(viewModel.name);
        // ReSharper disable once TsResolvedFromInaccessibleModule
        $("#DeleteModal").foundation("reveal", "open");
    }
}
export = BrickGroupsIndexViewModel
