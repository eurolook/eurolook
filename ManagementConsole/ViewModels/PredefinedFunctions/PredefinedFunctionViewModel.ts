export class PredefinedFunctionViewModel {

  id = ko.observable('');
  name = ko.observable('');
  functionTextId = ko.observable('');
  functionFemaleTextId = ko.observable('');
  functionHeaderTextId = ko.observable('');
  functionHeaderFemaleTextId = ko.observable('');
  adReference = ko.observable('');
  showInSignature = ko.observable(false);
  showInHeader = ko.observable(false);
  isSuperior = ko.observable(false);

  constructor(model) {
    if (!model) return;

    this.id(model.id);
    this.name(model.name);
    this.functionTextId(model.functionTextId);
    this.functionFemaleTextId(model.functionFemaleTextId);
    this.functionHeaderTextId(model.functionHeaderTextId);
    this.functionHeaderFemaleTextId(model.functionHeaderFemaleTextId);
    this.showInSignature(model.showInSignature);
    this.showInHeader(model.showInHeader);
    this.adReference(model.adReference);
    this.isSuperior(model.isSuperior);
  }
}
