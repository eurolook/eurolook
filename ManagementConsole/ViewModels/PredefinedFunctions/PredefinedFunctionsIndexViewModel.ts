import { PaginatedTableViewModel } from "PaginatedTableViewModel"
import { PredefinedFunctionViewModel } from "PredefinedFunctions/PredefinedFunctionViewModel"

class PredefinedFunctionsIndexViewModel extends PaginatedTableViewModel {

    predefinedFunctions: KnockoutObservableArray<PredefinedFunctionViewModel> = ko.observableArray([]);
    newPredefinedFunctionName = ko.observable("");
    deleteModalTitle = ko.observable("");
    reviveModalTitle = ko.observable("");
    searchRequest: JQueryXHR;
    deletingPredefinedFunction: PredefinedFunctionViewModel;
    revivingPredefinedFunction: PredefinedFunctionViewModel;
    selectedPredefinedFunctions: KnockoutObservableArray<PredefinedFunctionViewModel> = ko.observableArray([]);

    // ReSharper disable SuspiciousThisUsage
    isSelectedAll = ko.pureComputed({
        read: function () {
            return this.selectedPredefinedFunctions().length === this.predefinedFunctions().length;
        },
        write: function (value) {
            this.selectedPredefinedFunctions(value ? this.predefinedFunctions().slice(0) : []);
        },
        owner: this
    });
    // ReSharper enable SuspiciousThisUsage
    includeDependencies = ko.observable(false);

    constructor(baseUrl, isReadOnly: string) {
        super(baseUrl, "/PredefinedFunctions", isReadOnly);
        if (!this.orderProperty()) {
            this.orderProperty("name");
            this.orderAscending(true);
        }
        this.loadPageData();
        $("#SearchBox").focus();
    }

    loadPageData() {
        if (this.searchRequest && this.searchRequest.readyState < 4) {
            this.searchRequest.abort();
        }
        this.showProgressSpinner();
        this.searchRequest = this.getJson(this.getPageUrl(this.baseUrl() + "/PredefinedFunctions"),
            this.loadPageDataSucceeded,
            this);
    }

    loadPageDataSucceeded(result) {
        this.selectedPredefinedFunctions.removeAll();
        this.predefinedFunctions.removeAll();
        if (result.success === true) {
            const page = result.model;
            for (let predefinedFunction of page.data) {
                this.predefinedFunctions.push(new PredefinedFunctionViewModel(predefinedFunction));
            }
            this.setPagination(page);
        }
        this.hideProgressSpinner();
    }

    createPredefinedFunction() {
        this.postJson(`${this.baseUrl()}/PredefinedFunctions/Create`,
            ko.toJSON({ name: this.newPredefinedFunctionName() }),
            this.createPredefinedFunctionSucceeded,
            this);
    }

    createPredefinedFunctionSucceeded(data) {
        this.closeModal();
        if (data.success === true) {
            this.showAlert({ type: "success", message: data.message });
            this.loadPageData();
        } else {
            this.showAlert({ type: "error", message: data.message });
        }
    }

    downloadDataPackage() {
        if (this.selectedPredefinedFunctions().length === 0) {
            this.closeModal();
            this.showAlert({ type: "error", message: "No Predefined Function selected." });
            return;
        }
        this.showProgressBar();
        this.postJson(`${this.baseUrl()}/PredefinedFunctions/Export?includeDependencies=${this.includeDependencies()}`,
            ko.toJSON(this.selectedPredefinedFunctions), this.downloadDataPackageSucceeded, this);
    }

    downloadDataPackageSucceeded(result) {
        if (result.success) {
            window.location.href = `${this.baseUrl()}/DataExchange/Export/Package/${result.model.packageId}`;

        } else {
            this.hideProgressBar();
            this.showAlert({ type: "error", message: result.message });
        }
    }

    deletePredefinedFunction() {
        if (!this.deletingPredefinedFunction) return;
        this.deleteJson(`${this.baseUrl()}/PredefinedFunctions/Delete/${this.deletingPredefinedFunction.id()}`,
            ko.toJSON(this.deletingPredefinedFunction),
            this.deletePredefinedFunctionSucceeded,
            this);
    }

    deletePredefinedFunctionSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var deleteId = this.deletingPredefinedFunction.id();
            this.predefinedFunctions.remove(item => item.id() === deleteId);
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    revivePredefinedFunction() {
        if (!this.revivingPredefinedFunction) return;
        this.postJson(`${this.baseUrl()}/PredefinedFunctions/Revive/${this.revivingPredefinedFunction.id()}`,
            ko.toJSON(this.revivingPredefinedFunction),
            this.revivePredefinedFunctionSucceeded,
            this);
    }

    revivePredefinedFunctionSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var reviveId = this.revivingPredefinedFunction.id();
            this.predefinedFunctions.remove(item => item.id() === reviveId);
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    showDeleteModal(viewModel: PredefinedFunctionViewModel) {
        this.deletingPredefinedFunction = viewModel;
        this.deleteModalTitle(viewModel.name());
        // ReSharper disable once TsResolvedFromInaccessibleModule
        $("#DeleteModal").foundation("reveal", "open");
    }

    showReviveModal(viewModel: PredefinedFunctionViewModel) {
        this.revivingPredefinedFunction = viewModel;
        this.reviveModalTitle(viewModel.name());
        // ReSharper disable once TsResolvedFromInaccessibleModule
        $("#ReviveModal").foundation("reveal", "open");
    }
}
export = PredefinedFunctionsIndexViewModel
