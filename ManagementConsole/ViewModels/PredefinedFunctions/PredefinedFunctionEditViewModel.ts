import { PageViewModel } from 'PageViewModel'

class PredefinedFunctionEditViewModel extends PageViewModel {

  id: string;
  url: string;
  name = ko.observable('');
  showInSignature = ko.observable(true);
  showInHeader = ko.observable(true);
  activeDirectoryReference = ko.observable('');
  isSuperior = ko.observable(false);
  textAlias = ko.observable('');
  textFemaleAlias = ko.observable('');
  textHeaderAlias = ko.observable('');
  textHeaderFemaleAlias = ko.observable('');
  isSaving = ko.observable(false);
  hasFemaleFunction = ko.computed(function () {
    // ReSharper disable once SuspiciousThisUsage
    return this.textFemaleAlias() != null;
  }, this);
  hasHeaderFunction = ko.computed(function () {
    // ReSharper disable once SuspiciousThisUsage
    return this.textHeaderAlias() != null;
  }, this);
  hasFemaleHeaderFunction = ko.computed(function () {
    // ReSharper disable once SuspiciousThisUsage
    return this.textHeaderFemaleAlias() != null;
  }, this);

  constructor(baseUrl: string, id: string, isReadOnly: string) {
    super(baseUrl, '/PredefinedFunctions', isReadOnly);
    this.id = id;
    this.url = `${baseUrl}/PredefinedFunctions/Edit/${id}`;
    this.bindCtrlSave(this.save);
    this.load();
  }

  load() {
    this.showProgressSpinner();
    this.getJson(this.url, this.loadSucceeded, this);
  }

  loadSucceeded(result) {
    this.name(result.model.name);
    this.activeDirectoryReference(result.model.activeDirectoryReference);
    this.showInSignature(result.model.showInSignature);
    this.showInHeader(result.model.showInHeader);
    this.isSuperior(result.model.isSuperior);
    this.textAlias(result.model.textAlias);
    this.textFemaleAlias(result.model.textFemaleAlias);
    this.textHeaderAlias(result.model.textHeaderAlias);
    this.textHeaderFemaleAlias(result.model.textHeaderFemaleAlias);
    this.hideProgressSpinner();
  }

  createFemaleFunction() {
    const url = `${this.baseUrl()}/PredefinedFunctions/FemaleFunction/${this.id}`;
    this.postJson(url, null, this.createFemaleFunctionSucceeded, this);
  }

  createHeaderFunction() {
    const url = `${this.baseUrl()}/PredefinedFunctions/HeaderFunction/${this.id}`;
    this.postJson(url, null, this.createHeaderFunctionSucceeded, this);
  }

  createFemaleHeaderFunction() {
    const url = `${this.baseUrl()}/PredefinedFunctions/HeaderFemaleFunction/${this.id}`;
    this.postJson(url, null, this.createFemaleHeaderFunctionSucceeded, this);
  }

  createFemaleFunctionSucceeded(result) {
    if (!result.success) {
      this.showAlert({ type: 'error', message: result.message });
    } else {
      this.textFemaleAlias(result.model);
      this.showAlert({ type: 'success', message: result.message });
    }
  }

  createHeaderFunctionSucceeded(result) {
    if (!result.success) {
      this.showAlert({ type: 'error', message: result.message });
    } else {
      this.textHeaderAlias(result.model);
      this.showAlert({ type: 'success', message: result.message });
    }
  }

  createFemaleHeaderFunctionSucceeded(result) {
    if (!result.success) {
      this.showAlert({ type: 'error', message: result.message });
    } else {
      this.textHeaderFemaleAlias(result.model);
      this.showAlert({ type: 'success', message: result.message });
    }
  }

  save() {
    if (this.isSaving()) return;
    this.isSaving(true);
    this.putJson(this.url, ko.toJSON(this), this.saveSucceeded, this);
  }

  saveSucceeded(result) {
    if (!result.success) {
        this.showAlert({ type: 'error', message: result.message });
    } else {
        this.showAlert({ type: 'success', message: result.message });
    }
    this.isSaving(false);
  }
}
export = PredefinedFunctionEditViewModel
