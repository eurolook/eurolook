export class AuthorRoleViewModel {

    id = ko.observable("");
    name = ko.observable("");
    description = ko.observable("");
    displayName = ko.observable("");
    uiPositionIndex = ko.observable(0);

    isSelected = ko.observable(false);

    constructor(model) {
        if (!model) return;

        this.id(model.id);
        this.name(model.name);
        this.description(model.description);
        this.displayName(model.displayName);
        this.uiPositionIndex(model.uiPositionIndex);
    }
}
