import { AuthorRoleViewModel } from "./AuthorRoleViewModel";
import { PaginatedTableViewModel } from "../PaginatedTableViewModel";

class AuthorRolesIndexViewModel extends PaginatedTableViewModel {

    authorRoles: KnockoutObservableArray<AuthorRoleViewModel> = ko.observableArray([]);

    newAuthorRoleName = ko.observable("");

    deleteModalTitle = ko.observable("");
    reviveModalTitle = ko.observable("");
    searchRequest: JQueryXHR;
    deletingAuthorRole: AuthorRoleViewModel;
    revivingAuthorRole: AuthorRoleViewModel;
    selectedAuthorRoles: KnockoutObservableArray<AuthorRoleViewModel> = ko.observableArray([]);

    // ReSharper disable SuspiciousThisUsage
    isSelectedAll = ko.pureComputed({
        read: function() {
            return this.selectedAuthorRoles().length > 0 &&
                this.selectedAuthorRoles().length === this.authorRoles().length;
        },
        write: function(value) {
            this.selectedAuthorRoles(value ? this.authorRoles().slice(0) : []);
        },
        owner: this
    });
    // ReSharper enable SuspiciousThisUsage
    includeDependencies = ko.observable(false);

    constructor(baseUrl, isReadOnly: string) {
        super(baseUrl, "/AuthorRoles", isReadOnly);
        if (!this.orderProperty()) {
            this.orderProperty("name");
            this.orderAscending(true);
        }
        this.loadPageData();
        $("#SearchBox").focus();
    }

    loadPageData() {
        if (this.searchRequest && this.searchRequest.readyState < 4) {
            this.searchRequest.abort();
        }
        this.showProgressSpinner();
        this.searchRequest = this.getJson(this.getPageUrl(this.apiEndpointUrl()),
            this.loadPageDataSucceeded,
            this);
    }

    loadPageDataSucceeded(result) {
        this.selectedAuthorRoles.removeAll();
        this.authorRoles.removeAll();
        if (result.success === true) {
            const page = result.model;
            for (let authorRole of page.data) {
                this.authorRoles.push(new AuthorRoleViewModel(authorRole));
            }
            this.setPagination(page);
        }
        this.hideProgressSpinner();
    }

    createAuthorRole() {
        this.postJson(`${this.apiEndpointUrl()}/Create`,
            ko.toJSON({ name: this.newAuthorRoleName() }),
            this.createAuthorRoleSucceeded,
            this);
    }

    createAuthorRoleSucceeded(data) {
        this.closeModal();
        if (data.success === true) {
            this.showAlert({ type: "success", message: data.message });
            this.loadPageData();
        } else {
            this.showAlert({ type: "error", message: data.message });
        }
    }

    downloadDataPackage() {
        if (this.selectedAuthorRoles().length === 0) {
            this.closeModal();
            this.showAlert({ type: "error", message: "No author role selected." });
            return;
        }
        this.showProgressBar();
        this.postJson(`${this.apiEndpointUrl()}/Export?includeDependencies=${this.includeDependencies()}`,
            ko.toJSON(this.selectedAuthorRoles),
            this.downloadDataPackageSucceeded,
            this);
    }

    downloadDataPackageSucceeded(result) {
        if (result.success) {
            window.location.href = `${this.baseUrl()}/DataExchange/Export/Package/${result.model.packageId}`;

        } else {
            this.hideProgressBar();
            this.showAlert({ type: "error", message: result.message });
        }
    }

    deleteAuthorRole() {
        if (!this.deletingAuthorRole) return;
        this.deleteJson(`${this.apiEndpointUrl()}/Delete/${this.deletingAuthorRole.id()}`,
            ko.toJSON(this.deletingAuthorRole),
            this.deleteAuthorRoleSucceeded,
            this);
    }

    deleteAuthorRoleSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var deleteId = this.deletingAuthorRole.id();
            this.authorRoles.remove(item => item.id() === deleteId);
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    reviveAuthorRole() {
        if (!this.revivingAuthorRole) return;
        this.postJson(`${this.apiEndpointUrl()}/Revive/${this.revivingAuthorRole.id()}`,
            null,
            this.reviveAuthorRoleSucceeded,
            this);
    }

    reviveAuthorRoleSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var reviveId = this.revivingAuthorRole.id();
            this.authorRoles.remove(item => item.id() === reviveId);
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    showDeleteModal(viewModel: AuthorRoleViewModel) {
        this.deletingAuthorRole = viewModel;
        this.deleteModalTitle(viewModel.name());
        $("#DeleteModal").foundation("reveal", "open");
    }

    showReviveModal(viewModel: AuthorRoleViewModel) {
        this.revivingAuthorRole = viewModel;
        this.reviveModalTitle(viewModel.name());
        $("#ReviveModal").foundation("reveal", "open");
    }
}

export = AuthorRolesIndexViewModel
