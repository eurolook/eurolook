export class PageViewModel {
    baseUrl = ko.observable("");
    apiEndpointUrl = ko.observable("");
    isReadOnlyMode = ko.observable(false);
    globalProgressTimer = 0;

    constructor(baseUrl: string, apiEndpointPath: string, isReadOnlyMode: string) {
        this.baseUrl(baseUrl);
        this.apiEndpointUrl(baseUrl + apiEndpointPath);
        this.isReadOnlyMode((isReadOnlyMode === 'true' || isReadOnlyMode === 'True'));
    }

    getJson(url: string, onSuccess, context): JQueryXHR {
        return this.sendJson("GET", url, null, onSuccess, context);
    }

    postJson(url: string, data, onSuccess, context): JQueryXHR {
        return this.sendJson("POST", url, data, onSuccess, context);
    }

    putJson(url: string, data, onSuccess, context): JQueryXHR {
        return this.sendJson("PUT", url, data, onSuccess, context);
    }

    deleteJson(url: string, data, onSuccess, context): JQueryXHR {
        return this.sendJson("DELETE", url, data, onSuccess, context);
    }

    sendJson(method: string, url: string, data, onSuccess, context): JQueryXHR {
        return $.ajax({
            type: method,
            url: url,
            contentType: "application/json",
            dataType: "json",
            data: data,
            cache: false,
            context: context,
            success: onSuccess,
            error: this.showHttpError
        });
    }

    postFormData(url: string, formData: FormData, context): JQueryXHR {
        return $.ajax({
            type: "POST",
            url: url,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            context: context
        });
    }

    postFormDataWithCallback(url: string, formData: FormData, onSuccess, onError, context): JQueryXHR {
        return $.ajax({
            type: "POST",
            url: url,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            context: context,
            success: onSuccess,
            error: onError
        });
    }

    showProgressBar() {
        window.clearTimeout(this.globalProgressTimer);
        this.globalProgressTimer = window.setTimeout(() => { $("#loadDataProgressBar").show(); }, 300);
    }

    hideProgressBar() {
        window.clearTimeout(this.globalProgressTimer);
        $("#loadDataProgressBar").hide();
    }

    closeModal() {
        // ReSharper disable once TsResolvedFromInaccessibleModule
        $(".reveal-modal").foundation("reveal", "close");
    }

    showAlert(props) {
        let timeout = 15000;

        if (props.timeout instanceof Number) {
            timeout = props.timeout;
        }

        const alert = `<div data-alert class="alert-box ${props.type}" style="position: fixed; margin-left: 280px; margin-top:-55px; z-index:100;"><span style="margin-right:1em;">${props.message
            }</span><a href="#" class="close">&times;</a></div>`;

        const parent = 'section[role="main"]';
        $(parent).children(".alert-box").remove();
        // ReSharper disable once TsResolvedFromInaccessibleModule
        $(parent).prepend(alert).foundation();

        // fix dropdown panels after inserting the alert box
        // ReSharper disable once TsResolvedFromInaccessibleModule
        $(document).foundation("dropdown", "off");
        // ReSharper disable once TsResolvedFromInaccessibleModule
        $(document).foundation("dropdown");

        if (timeout > 0) {
            setTimeout(() => {
                $(`.alert-box.${props.type} .close`).click();
            }, timeout);
        }
    }

    showProgressSpinner(targetElementId?: string) {
        window.clearTimeout(this.globalProgressTimer);
        this.globalProgressTimer = window.setTimeout(() => {
            let spinner = document.getElementById("progress-spinner");
            if (targetElementId != null) {
                const target = document.getElementById(targetElementId);
                spinner = target.appendChild(spinner);
            }
            if (spinner) {
                spinner.style.display = "block";
            }
        }, 600);
    }

    hideProgressSpinner() {
        window.clearTimeout(this.globalProgressTimer);
        const spinner = document.getElementById("progress-spinner");
        if (spinner && spinner.style.display !== "none") {
            spinner.style.display = "none";
        }
    }

    showHttpError(jqXhr: JQueryXHR, textStatus: string, errorThrown) {
        this.closeModal();
        this.hideProgressBar();
        this.showAlert({ type: "error", message: `HTTP error: ${errorThrown}` });
    }

    setCookie(cname: string, cvalue: string, exdays: number) {
        const d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));

        const expires = `expires=${d.toUTCString()}`;
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }

    getCookie(cname: string) {
        const name = cname + "=";
        const ca = document.cookie.split(";");
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }

        return "";
    }

    getParameterByName(name: string): string {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        const regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
        const results = regex.exec(location.search);
        const result = results === null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));

        if (!result || result === "undefined") {
            return undefined;
        } else {
            return result;
        }
    }

    bindCtrlSave(saveFunction: Function) {
        window.addEventListener("keydown", (event: KeyboardEvent) => {
            if (event.ctrlKey || event.metaKey) {
                switch (String.fromCharCode(event.which).toLowerCase()) {
                    case "s":
                        event.preventDefault();
                        const activeElement = document.activeElement;
                        if (activeElement instanceof HTMLElement) {
                            const htmlElement = activeElement as HTMLScriptElement;
                            htmlElement.blur();
                            window.setTimeout(() => {
                                saveFunction.call(this);
                                htmlElement.focus();
                            }, 300);
                            break;
                        }
                }
            }
        });
    }

    ignoreJsonProperties(propertyNames: string[]): Function {
        return (key, value) => {
            if (propertyNames.indexOf(key) > -1) {
                return null;
            }
            else {
                return value;
            }
        }
    }
}
