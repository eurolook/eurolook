export function escapeRegExp(str) {
    return str.replace(/[-/\\^$*+?.()|[\]{}]/g, "\\$&")
        .replace(/\s+/g, "\\s*")
        .replace(/\$/g, "$$$$");
}

export function getDateForFilename(date) {
    const month = parseInt(date.getMonth()) + 1;
    return `${date.getFullYear()}-${month}-${date.getDate()}--${date.getHours()}-${date.getMinutes()}`;
}

export function createGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8); // NOSONAR
        return v.toString(16);
    });
}