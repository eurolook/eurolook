import { PaginatedTableViewModel } from "PaginatedTableViewModel"
import { DeviceViewModel } from "./DeviceViewModel"

class DevicesIndexViewModel extends PaginatedTableViewModel {

    items: KnockoutObservableArray<DeviceViewModel> = ko.observableArray([]);
    selectedItems: KnockoutObservableArray<DeviceViewModel> = ko.observableArray([]);
    // ReSharper disable SuspiciousThisUsage
    isSelectedAll = ko.pureComputed({
        read: function () {
            return this.items().length > 0 && this.selectedItems().length === this.items().length;
        },
        write: function (value) {
            this.selectedItems(value ? this.items().slice(0) : []);
        },
        owner: this
    });
    // ReSharper restore SuspiciousThisUsage
    deletingItems: KnockoutObservableArray<DeviceViewModel> = ko.observableArray([]);
    revivingItems: KnockoutObservableArray<DeviceViewModel> = ko.observableArray([]);
    deleteModalTitle = ko.observable("");
    reviveModalTitle = ko.observable("");

    searchRequest: JQueryXHR;

    constructor(baseUrl, isReadOnly: string) {
        super(baseUrl, "/Devices", isReadOnly);
        this.pageSizes = ko.observableArray([20, 50, 100, 500, 1000]);
        if (!this.orderProperty()) {
            this.orderProperty("deviceName");
            this.orderAscending(true);
        }
        this.loadPageData();
        $("#SearchBox").focus();
    }

    loadPageData() {
        if (this.searchRequest && this.searchRequest.readyState < 4) {
            this.searchRequest.abort();
        }
        this.selectedItems([]);
        this.deletingItems([]);
        this.revivingItems([]);
        this.isSelectedAll.notifySubscribers();
        this.showProgressSpinner();
        this.searchRequest = this.getJson(
            this.getPageUrl(this.baseUrl() + "/Devices"),
            this.loadPageDataSucceeded,
            this);
    }

    loadPageDataSucceeded(result) {
        this.items.removeAll();
        if (result.success === true) {
            const page = result.model;
            for (let brick of page.data) {
                this.items.push(new DeviceViewModel(brick));
            }
            this.setPagination(page);
        }
        this.hideProgressSpinner();
    }

    deleteItems() {
        if (this.deletingItems().length === 0) {
            return;
        }

        let selectedDevices = this.deletingItems().map((d) => ({ id: d.id(), deviceName: d.deviceName() }));
        this.deleteJson(`${this.baseUrl()}/Devices/Delete/`,
            ko.toJSON(selectedDevices),
            this.deleteItemsSucceeded,
            this);
        //this.deleteJson(`${this.baseUrl()}/Devices/Delete/${this.deletingItem.id()}`,
        //    ko.toJSON(this.deletingItem), this.deleteItemSucceeded, this);
    }

    deleteItemsSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });

            if (this.deletingItems().length === 1) {
                const id = this.deletingItems()[0].id();
                this.items.remove(item => item.id() === id);
                this.isSelectedAll.notifySubscribers();
                this.deletingItems([]);
            } else {
                this.loadPageData();
            }
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    deleteItemsPermanently() {
        if (this.deletingItems().length === 0) {
            return;
        }

        let selectedDevices = this.deletingItems().map((d) => ({ id: d.id(), deviceName: d.deviceName() }));
        this.deleteJson(`${this.baseUrl()}/Devices/DeletePermanently/`,
            ko.toJSON(selectedDevices),
            this.deleteItemPermanentlySucceeded,
            this);
    }

    deleteItemPermanentlySucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });

            if (this.deletingItems().length === 1) {
                const id = this.deletingItems()[0].id();
                this.items.remove(item => item.id() === id);
                this.isSelectedAll.notifySubscribers();
                this.deletingItems([]);
            } else {
                this.loadPageData();
            }
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    reviveItems() {
        if (this.revivingItems().length === 0) {
            return;
        }

        let selectedDevices = this.revivingItems().map((d) => ({ id: d.id(), deviceName: d.deviceName() }));
        this.postJson(`${this.baseUrl()}/Devices/Revive/`,
            ko.toJSON(selectedDevices),
            this.reviveItemsSucceeded,
            this);
    }

    reviveItemsSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });

            if (this.revivingItems().length === 1) {
                const id = this.revivingItems()[0].id();
                this.items.remove(item => item.id() === id);
                this.isSelectedAll.notifySubscribers();
                this.revivingItems([]);
            } else {
                this.loadPageData();
            }
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    showDeleteModal(viewModel) {
        this.deletingItems(viewModel);
        const title = this.deletingItems().length === 1 ? this.deletingItems()[0].deviceName() : "the selected device profiles";
        this.deleteModalTitle(title);
        // ReSharper disable once TsResolvedFromInaccessibleModule
        $("#DeleteModal").foundation("reveal", "open");
    }

    showDeletePermanentlyModal(viewModel) {
        this.deletingItems(viewModel);
        const title = this.deletingItems().length === 1 ? this.deletingItems()[0].deviceName() : "the selected device profiles";
        this.deleteModalTitle(title);
        // ReSharper disable once TsResolvedFromInaccessibleModule
        $("#DeletePermanentlyModal").foundation("reveal", "open");
    }

    showReviveModal(viewModel) {
        this.revivingItems(viewModel);
        const title = this.revivingItems().length === 1 ? this.revivingItems()[0].deviceName() : "the selected device profiles";
        this.reviveModalTitle(title);
        // ReSharper disable once TsResolvedFromInaccessibleModule
        $("#ReviveModal").foundation("reveal", "open");
    }
}
export = DevicesIndexViewModel
