import { PageViewModel } from "PageViewModel"
import { DeviceViewModel } from "./DeviceViewModel"

class DevicesEditViewModel extends PageViewModel {

    id: string;
    url: string;
    device: KnockoutObservable<DeviceViewModel> = ko.observable(new DeviceViewModel(null));

    constructor(baseUrl: string, id: string, isReadOnly: string) {
        super(baseUrl, "/Devices", isReadOnly);
        this.id = id;
        this.url = `${baseUrl}/Devices/Edit/${id}`;
        this.load();
    }

    load() {
        this.showProgressSpinner();
        this.getJson(this.url, this.loadSucceeded, this);
    }

    loadSucceeded(result) {
        if (!result.success) {
            this.showAlert({ type: "error", message: result.message });
        } else {
            const device = new DeviceViewModel(result.model);
            this.device(device);
        }
        this.hideProgressSpinner();
    }
}
export = DevicesEditViewModel;
