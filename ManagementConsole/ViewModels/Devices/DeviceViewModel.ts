export class DeviceViewModel {

    id = ko.observable("");
    deviceName = ko.observable("");
    login = ko.observable("");
    userId = ko.observable("");
    operatingSystem = ko.observable("");
    owner = ko.observable("");

    officeVersion = ko.observable("");

    dotNetRuntimeVersion = ko.observable("");
    dotNetFrameworkVersion = ko.observable("");
    installRoot = ko.observable("");
    addInLoadTimes = ko.observable("");
    maxAddInLoadTimeBucketSeconds = ko.observable("");
    addinVersion = ko.observable("");

    isRemoteWipeRequested = ko.observable(false);

    isTaskpaneVisible = ko.observable(false);
    taskpaneWidth = ko.observable("");
    lastUse = ko.observable("");
    lastLoginDate : Date;
    serverModificationTimeLocal = ko.observable("");

    quickStartGuideShown = ko.observable("");
    isInitialised = ko.observable(false);
    isPerUserInstall = ko.observable("");
    isPerMachineInstall = ko.observable("");

    isTouchScreen = ko.observable(false);
    hasBattery = ko.observable(false);
    screenWidth = ko.observable(0);
    screenHeight = ko.observable(0);
    screenDpi = ko.observable(0.0);

    physicallyInstalledSystemMemoryGbForDisplay = ko.observable("");
    processorName = ko.observable("");

    monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    constructor(model) {
        if (!model) return;

        this.id(model.id);
        this.deviceName(model.deviceName);
        this.login(model.user ? model.user.login : "");
        this.userId(model.user ? model.user.id : "");
        this.operatingSystem(model.operatingSystem);
        this.officeVersion(model.officeVersion);
        this.addinVersion(model.addinVersion);

        this.owner(model.owner);

        this.isTaskpaneVisible(model.isTaskpaneVisible);
        this.taskpaneWidth(model.taskpaneWidth || "<unknown>");

        this.serverModificationTimeLocal(model.serverModificationTimeLocal);
        this.isInitialised(model.isInitialised);

        this.quickStartGuideShown(!model.quickStartGuideShown ? "Not shown" : "Shown in version " + model.quickStartGuideShown);

        this.dotNetRuntimeVersion(model.dotNetRuntimeVersion || "<unknown>");
        this.dotNetFrameworkVersion(model.dotNetFrameworkVersion || "<unknown>");

        this.installRoot(model.installRoot);
        this.isPerMachineInstall(model.isPerMachineInstall);
        this.isPerUserInstall(model.isPerUserInstall);

        this.maxAddInLoadTimeBucketSeconds(model.maxAddInLoadTimeBucketSeconds);

        this.isTouchScreen(model.isTouchScreen);
        this.hasBattery(model.hasBattery);
        this.screenWidth(model.screenWidth);
        this.screenHeight(model.screenHeight);
        this.screenDpi(model.screenDpi);

        this.physicallyInstalledSystemMemoryGbForDisplay(model.physicallyInstalledSystemMemoryGb === 0
            ? "<unknown>"
            : model.physicallyInstalledSystemMemoryGb + " GB");

        this.processorName(model.processorName || "<unknown>");
        
        if (model.addInLoadTimesMilliseconds == null || model.addInLoadTimesMilliseconds.length === 0) {
            this.addInLoadTimes("no data");
        } else {
            let loadTimes = JSON.parse(model.addInLoadTimesMilliseconds);
            this.addInLoadTimes((loadTimes || [0]).join(" ms, ") + " ms");
        }

        this.isRemoteWipeRequested(model.isRemoteWipeRequested);

        if (model.lastLogin) {
            this.lastLoginDate = new Date(model.lastLogin);
            this.lastUse(this.monthNames[this.lastLoginDate.getMonth()] + " " + this.lastLoginDate.getFullYear());
        }
    }
}
