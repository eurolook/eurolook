import { PaginatedTableViewModel } from "PaginatedTableViewModel"
import { StyleShortcutViewModel } from "StyleShortcuts/StyleShortcutViewModel"
class StyleShortcutsIndexViewModel extends PaginatedTableViewModel {

    styleShortcuts: KnockoutObservableArray<StyleShortcutViewModel> = ko.observableArray([]);
    newStyleShortcutName = ko.observable("");
    newShortcut = ko.observable("");  
    deleteModalTitle = ko.observable("");
    reviveModalTitle = ko.observable("");
    searchRequest: JQueryXHR;
    deletingStyleShortcut: StyleShortcutViewModel;
    revivingStyleShortcut: StyleShortcutViewModel;
    selectedStyleShortcuts: KnockoutObservableArray<StyleShortcutViewModel> = ko.observableArray([]);
    // ReSharper disable SuspiciousThisUsage
    isSelectedAll = ko.pureComputed({
        read: function () {
            return this.selectedStyleShortcuts().length === this.styleShortcuts().length;
        },
        write: function (value) {
            this.selectedStyleShortcuts(value ? this.styleShortcuts().slice(0) : []);
        },
        owner: this
    });
    // ReSharper enable SuspiciousThisUsage
    includeDependencies = ko.observable(false);

    constructor(baseUrl, isReadOnly: string) {
        super(baseUrl, "/StyleShortcuts", isReadOnly);
        if (!this.orderProperty()) {
            this.orderProperty("styleName");
            this.orderAscending(true);
        }
        this.loadPageData();
        $("#SearchBox").focus();
    }

    loadPageData() {
        if (this.searchRequest && this.searchRequest.readyState < 4) {
            this.searchRequest.abort();
        }
        this.showProgressSpinner();
        this.searchRequest = this.getJson(this.getPageUrl(this.baseUrl() + "/StyleShortcuts"),
            this.loadPageDataSucceeded,
            this);
    }

    loadPageDataSucceeded(result) {
        this.selectedStyleShortcuts.removeAll();
        this.styleShortcuts.removeAll();
        if (result.success === true) {
            const page = result.model;
            for (let styleShortcut of page.data) {
                this.styleShortcuts.push(new StyleShortcutViewModel(styleShortcut));
            }
            this.setPagination(page);
        }
        this.hideProgressSpinner();
    }

    createStyleShortcut() {
        this.postJson(`${this.baseUrl()}/StyleShortcuts/Create`,
            ko.toJSON({
                styleName: this.newStyleShortcutName(), shortcut: this.newShortcut()}),
            this.createStyleShortcutSucceeded,
            this);
    }

    createStyleShortcutSucceeded(data) {
        this.closeModal();
        if (data.success === true) {
            this.showAlert({ type: "success", message: data.message });
            this.loadPageData();
        } else {
            this.showAlert({ type: "error", message: data.message });
        }
    }

    downloadDataPackage() {
        if (this.selectedStyleShortcuts().length === 0) {
            this.closeModal();
            this.showAlert({ type: "error", message: "No Style Shortcut selected." });
            return;
        }
        this.showProgressBar();
        this.postJson(`${this.baseUrl()}/StyleShortcuts/Export?includeDependencies=${this.includeDependencies()}`,
            ko.toJSON(this.selectedStyleShortcuts), this.downloadDataPackageSucceeded, this);
    }

    downloadDataPackageSucceeded(result) {
        if (result.success) {
            window.location.href = `${this.baseUrl()}/DataExchange/Export/Package/${result.model.packageId}`;

        } else {
            this.hideProgressBar();
            this.showAlert({ type: "error", message: result.message });
        }
    }

    deleteStyleShortcut() {
        if (!this.deletingStyleShortcut) return;
        this.deleteJson(`${this.baseUrl()}/StyleShortcuts/Delete/${this.deletingStyleShortcut.id()}`,
            ko.toJSON(this.deletingStyleShortcut),
            this.deleteStyleShortcutSucceeded,
            this);
    }

    deleteStyleShortcutSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var deleteId = this.deletingStyleShortcut.id();
            this.styleShortcuts.remove(item => item.id() === deleteId);
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    reviveStyleShortcut() {
        if (!this.revivingStyleShortcut) return;
        this.postJson(`${this.baseUrl()}/StyleShortcuts/Revive/${this.revivingStyleShortcut.id()}`,
            ko.toJSON(this.revivingStyleShortcut),
            this.reviveStyleShortcutSucceeded,
            this);
    }

    reviveStyleShortcutSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var reviveId = this.revivingStyleShortcut.id();
            this.styleShortcuts.remove(item => item.id() === reviveId);
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    showDeleteModal(viewModel: StyleShortcutViewModel) {
        this.deletingStyleShortcut = viewModel;
        this.deleteModalTitle(viewModel.styleName());
        // ReSharper disable once TsResolvedFromInaccessibleModule
        $("#DeleteModal").foundation("reveal", "open");
    }

    showReviveModal(viewModel: StyleShortcutViewModel) {
        this.revivingStyleShortcut = viewModel;
        this.reviveModalTitle(viewModel.styleName());
        // ReSharper disable once TsResolvedFromInaccessibleModule
        $("#ReviveModal").foundation("reveal", "open");
    }

}
export = StyleShortcutsIndexViewModel
