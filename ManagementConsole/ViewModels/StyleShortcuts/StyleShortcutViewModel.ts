export class StyleShortcutViewModel {

    id = ko.observable('');
    shortcut = ko.observable('');
    styleName = ko.observable('');


    constructor(model) {
        if (!model) return;

        this.id(model.id);
        this.styleName(model.styleName);
        this.shortcut(model.shortcut);      
    }
    
}
