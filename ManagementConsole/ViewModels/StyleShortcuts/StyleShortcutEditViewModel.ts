
import { PageViewModel } from 'PageViewModel'

class StyleShortcutEditViewModel extends PageViewModel {

    id: string;
    url: string;
    styleName = ko.observable('');
    shortcut = ko.observable('');
    isSaving = ko.observable(false);


    constructor(baseUrl: string, id: string, isReadOnly: string) {
        super(baseUrl, '/StyleShortcuts', isReadOnly);
        this.id = id;
        this.url = `${baseUrl}/StyleShortcuts/Edit/${id}`;
        this.bindCtrlSave(this.save);
        this.load();
    }

    load() {
        this.showProgressSpinner();
        this.getJson(this.url, this.loadSucceeded, this);
    }

    loadSucceeded(result) {
        this.styleName(result.model.styleName);
        this.shortcut(result.model.shortcut);

        this.hideProgressSpinner();
    }


    save() {
        if (this.isSaving()) return;
        this.isSaving(true);
        this.putJson(this.url, ko.toJSON(this), this.saveSucceeded, this);
    }

    saveSucceeded(result) {
        if (!result.success) {
            this.showAlert({ type: 'error', message: result.message });
        } else {
            this.showAlert({ type: 'success', message: result.message });
        }
        this.isSaving(false);
    }
}
export = StyleShortcutEditViewModel
