import { PageViewModel } from "PageViewModel"

export class PaginatedTableViewModel extends PageViewModel {

    pageNumber = ko.observable(0);
    pageInfos = ko.observableArray();
    selectedPageNumber = ko.observable(0);
    pageSizes = ko.observableArray([20, 50, 100]);
    pageSize = ko.observable(0);
    searchQuery = ko.observable("");
    searchInput = ko.observable("");
    orderProperty = ko.observable("");
    orderAscending = ko.observable(true);
    tableMode = ko.observable("");
    pageCount = ko.computed(function () {
// ReSharper disable SuspiciousThisUsage
        return this.pageInfos().length;
    }, this);
    hasPreviousPage = ko.computed(function () {
        return this.pageNumber() > 1;
    }, this);
    hasNextPage = ko.computed(function () {
        return this.pageNumber() < this.pageCount();
    }, this);
// ReSharper restore SuspiciousThisUsage
    progressTimer: number;
    searchInputTimer: number;

    constructor(baseUrl: string, apiEndpointPath: string, isReadOnly: string) {
        super(baseUrl, apiEndpointPath, isReadOnly);

        // get initial variables
        const q = this.getParameterByName("q");
        let tm = "normal";
        if (this.getParameterByName("deleted")) {
            tm = "deleted";
        }
        let s = parseInt(this.getParameterByName("s"));
        if (isNaN(s)) {
            const preferedPageSize = parseInt(this.getCookie("preferedPageSize"));
            if (isNaN(preferedPageSize)) {
                s = 50;
            } else {
                s = preferedPageSize;
            }
        }
        let p = parseInt(this.getParameterByName("p"));
        if (isNaN(p)) p = 1;
        const o = this.getParameterByName("o");
        const paramAsc = this.getParameterByName("asc");
        let asc = true;
        if (paramAsc) {
            asc = paramAsc === "true";
        }

        // set vm properties
        this.pageNumber(p);
        this.pageSize(s);
        this.pageSize.subscribe(this.pageSizeChanged, this);
        this.searchQuery(q);
        this.searchInput(q);
        this.orderProperty(o);
        this.orderAscending(asc);
        this.tableMode(tm);
        this.tableMode.subscribe(this.tableModeChanged, this);
        this.searchInput.subscribe(this.searchInputChanged, this);
        window.addEventListener('popstate', this.loadPageFromHistoryRecord);
    }

    loadPageData() {}

    setPagination(page) {
        this.pageNumber(page.pageNumber);
        if (page.pageCount !== this.pageCount()) {
            const pis = Array();

            if (page.pageCount === 0) {
                pis.push({
                    pageNo: 1,
                    displayName: "1 of 1"
                });
            } else {
                for (let i = 1; i <= page.pageCount; i++) {
                    pis.push({
                        pageNo: i,
                        displayName: i + " of " + page.pageCount
                    });
                }
            }
            this.pageInfos(pis);
            this.selectedPageNumber(page.pageNumber);
            this.selectedPageNumber.subscribe(this.selectedPageNumberChanged, this);
        }

        this.createHistoryRecordForPage();
    }

    createHistoryRecordForPage() {
        const endpoint = this.apiEndpointUrl();
        const url = this.getPageUrl(endpoint);
        if (window.history.state === null) {
            history.replaceState({ url: url }, document.title, url);
        } else {
            window.history.pushState({ url: url }, document.title, url);
        }
    }

    loadPageFromHistoryRecord(event: PopStateEvent) {
        if (event.state != null && event.state.hasOwnProperty('url')) {
            window.location.href = event.state.url;
        }
    }

    loadPreviousPage() {
        if (this.hasPreviousPage()) {
            this.selectedPageNumber(this.pageNumber() - 1);
        }
    }

    loadNextPage = function () {
        if (this.hasNextPage()) {
            this.selectedPageNumber(this.pageNumber() + 1);
        }
    }

    orderBy(newOrder) {
        if (newOrder) {
            if (newOrder === this.orderProperty()) {
                this.orderAscending(!this.orderAscending());
            }
            else {
                this.orderProperty(newOrder);
                this.orderAscending(true);
            }
        }
        this.loadPageData();
    }

   getPageUrl(baseUrl) {
        let url = baseUrl + "?";
        const query = this.searchQuery();
        if (query && query !== "*") {
            url += `q=${query}&`;
        }

        const pageNo = this.pageNumber();
        if (!isNaN(pageNo) && pageNo > 0) {
            url += `p=${pageNo}`;
        } else {
            url += "p=1";
        }

        url += `&s=${this.pageSize()}`;

        if (this.orderProperty()) {
            url += `&o=${this.orderProperty()}`;
        }

        url += `&asc=${this.orderAscending()}`;

        if (this.tableMode() === "deleted") {
            url += "&deleted=true";
        }

        return url;
    }
    
    searchInputChanged(newValue) {
        var that = this;
        window.clearTimeout(this.searchInputTimer);
        this.searchInputTimer = window.setTimeout(() => {
            that.searchQuery(newValue);
            that.pageNumber(1);
            that.loadPageData();
        }, 200);
    }

    pageSizeChanged(newValue: number) {
        if (!newValue || isNaN(newValue)) {
            return;
        }
        this.setCookie("preferedPageSize", newValue.toString(), 500);
        this.pageNumber(1);
        this.loadPageData();
    }

    tableModeChanged() {
        this.loadPageData();
    }

    selectedPageNumberChanged(newValue: number) {
        if (!isNaN(newValue) && newValue != this.pageNumber()) {
            this.pageNumber(newValue);
            this.loadPageData();
        }
    }
}
