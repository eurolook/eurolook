function OrgaChartIndexViewModel(dgs, languages) {
    var self = this;

    // Properties

    self.allDgs = ko.observableArray(dgs);
    self.allLanguages = ko.observableArray(languages);
    self.allLocations = ko.observableArray();
    self.selectedDg = ko.observable();
    self.selectedLanguage = ko.observable();
    self.selectedPrimaryLocation = ko.observable();
    self.selectedSecondaryLocation = ko.observable();
    self.isSelectedDgShown = ko.observable(false);
    self.treeReader = new JsTreeReader(ko.baseUrl);
    self.newDgAcronym = ko.observable();    

    // Events

    self.selectedDg.subscribe(function () {
        self.loadOrgaTree();
    });

    self.selectedLanguage.subscribe(function () {
        self.loadOrgaTree();
    });

    // Functions

    self.init = function () {
        languages.forEach(function (lang) {
            if (lang.name === "EN") {
                self.selectedLanguage(lang);
            }
        });
        self.loadAddresses();
    }

    self.revealAuthors = function (id) {
        jQuery.ajax({
            url: ko.baseUrl + "/OrgaChart/RevealAuthors/" + id,
            dataType: "json",
            cache: false,
            success: function (data) {
                if (data.success === true && data.model.length > 0) {
                    var tree = $('#tree').jstree();
                    // delete existing nodes
                    tree.delete_node(data.model);
                    tree.delete_node(tree.get_selected());
                    // add new nodes
                  var parentNode = tree.get_node(id);
                  var i = 0;
                    data.model.forEach(function (nodeModel) {
                        var newNode = self.treeReader.read(nodeModel);
                      tree.create_node(parentNode, newNode, i);
                      i++;
                    });
                } else {
                    showAlert({ type: "error", message: data.message });
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                showAlert({ type: "error", message: "HTTP error: " + errorThrown });
            }
        });
  }

  self.showAuthor = function(id) {
    window.open(ko.baseUrl + '/Authors/Edit/' + id, '_blank');
  }

    self.loadOrgaTree = function () {
        if (!self.selectedDg() || !self.selectedLanguage())
        {
            self.isSelectedDgShown(false);
            $("#tree").jstree("destroy").empty();
            return;
        }

        self.showTreeProgressBar();
        self.isSelectedDgShown(false);
        $("#tree").jstree("destroy").empty();

        jQuery.ajax({
            url: ko.baseUrl + "/OrgaChart/OrgaTree?dgId=" + self.selectedDg().id + "&languageId=" + self.selectedLanguage().id + "&noCache=",
            dataType: "json",
            cache: false,
            success: function(data) {
                self.hideTreeProgressBar();
                if (data.success === true) {                    
                  var tree = self.treeReader.read(data.model);
                  $("#tree").jstree(
                    {
                      core: {
                        data: [tree],
                        check_callback: true
                      }
                    });
                  self.isSelectedDgShown(true);
                } else {
                    showAlert({ type: "error", message: data.message });
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                self.hideTreeProgressBar();
                showAlert({ type: "error", message: "HTTP error: " + errorThrown });
            }
        });
    }

    self.loadAddresses = function () {
        return jQuery.ajax({
            url: ko.baseUrl + "/Address/All",
            dataType: "json",
            cache: false,
            success: function (data) {
                if (data.success === true) {
                    data.model.forEach(function(address) {
                        self.allLocations.push(address);
                    });
                } else {
                    showAlert({ type: "error", message: data.message });
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                showAlert({ type: "error", message: "HTTP error: " + errorThrown });
            }
        });
    }

    self.exportOrgaChart = function () {
        window.location = ko.baseUrl + "/OrgaChart/Export?dgId=" + self.selectedDg().id + "&languageId=" + self.selectedLanguage().id;
    }

    self.exportAllOrgaChartsAsPackage = function () {        
        var url = ko.baseUrl + "/OrgaChart/ExportAllOrgaChartsAsPackage";
        self.showDownloadProgressBar();
        jQuery.ajax({
            type: "GET",
            url: url,
            data: {},
            success: function (data) {
                if (data.success === true) {
                    window.location.href = ko.baseUrl + "/DataExchange/Export/Package/" + data.model.packageId;
                } else {
                    showAlert({ type: "error", message: data.message });
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                self.hideImportProgressBar();
                showAlert({ type: "error", message: "HTTP error: " + errorThrown });
            }
        });
    }

    self.exportDataPackage = function () {
        var url = ko.baseUrl + "/OrgaChart/ExportDataPackage/" + self.selectedDg().id;
        self.showDownloadProgressBar();
        jQuery.ajax({
            type: "GET",
            url: url,
            data: {},
            success: function (data) {
                if (data.success === true) {
                    window.location.href = ko.baseUrl + "/DataExchange/Export/Package/" + data.model.packageId;
                } else {
                    showAlert({ type: "error", message: data.message });
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                self.hideImportProgressBar();
                showAlert({ type: "error", message: "HTTP error: " + errorThrown });
            }
        });
    }

    self.createNewDg = function () {
        if (self.selectedPrimaryLocation() == null) { return; }
        var primaryAddressId = self.selectedPrimaryLocation().id;
        var secondaryAddressId = null;
        if (self.selectedSecondaryLocation() != null) {
            secondaryAddressId = self.selectedSecondaryLocation().id;
        }

        // post the view model to the server
        jQuery.ajax({
            type: "POST",
            url: ko.baseUrl + "/OrgaChart/CreateDg",
            dataType: "json",
            data: {
                acronym: self.newDgAcronym(),
                primaryAddressId: primaryAddressId,
                secondaryAddressId: secondaryAddressId
            },
            success: function (data) {
                self.closeModal();
                if (data.success === true) {
                    showAlert({ type: "success", message: data.message });
                    self.allDgs.push(data.model);
                    self.selectedDg(data.model);
                }
                else {
                    showAlert({ type: "error", message: data.message });
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                self.closeModal();
                showAlert({ type: "error", message: "HTTP error: " + errorThrown });
            }
        });
    }

    self.deleteSelectedDg = function () {
        var deleteId = self.selectedDg().id;
        var url = ko.baseUrl + "/OrgaChart/Delete/" + deleteId;
        jQuery.ajax({
            type: "DELETE",
            url: url,
            data: {},
            success: function (data) {
                self.closeModal();
                if (data.success === true) {
                    self.allDgs.remove(item => item.id === deleteId);
                    self.selectedDg(self.allDgs[0]);
                    showAlert({ type: "success", message: data.message });
                } else {
                    showAlert({ type: "error", message: data.message });
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                showAlert({ type: "error", message: "HTTP error: " + errorThrown });
            }
        });
    }

    self.showDownloadProgressBar = function () {
        window.clearTimeout(self.downloadProgressTimer);
        self.downloadProgressTimer = window.setTimeout(function () { $("#downloadProgressBar").show(); }, 300);
    }

    self.hideDownloadProgressBar = function () {
        window.clearTimeout(self.downloadProgressTimer);
        $("#downloadProgressBar").hide();
    }

    self.showTreeProgressBar = function () {
        window.clearTimeout(self.treeProgressTimer);
        self.treeProgressTimer = window.setTimeout(function () { $("#treeProgressBar").show(); }, 100);
    }

    self.hideTreeProgressBar = function () {
        window.clearTimeout(self.treeProgressTimer);
        $("#treeProgressBar").hide();
    }

    this.closeModal = function () {
        $(".reveal-modal").foundation("reveal", "close");
    }

    self.init();
}
