using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Database;

namespace Eurolook.ManagementConsole.ViewModels.OrgaChart
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript."),
    ]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    public class JsTreeNode
    {
        public JsTreeNode()
        {
            Children = new List<JsTreeNode>();
            ShowInHeader = true;
        }

        public JsTreeNode(Author author)
            : this()
        {
            Id = author.Id.ToString();
            DisplayName = $"{author.LatinLastName}, {author.LatinFirstName}";
            if (string.IsNullOrWhiteSpace(DisplayName))
            {
                DisplayName = Id;
            }

            LogicalLevel = 4;
            JsFunc = $"ko.viewModel.showAuthor('{Id}');";
            ToolTip = "Click to show the author details.";
        }

        public JsTreeNode(OrgaEntity orgaEntity)
            : this()
        {
            Id = orgaEntity.Id.ToString();
            LogicalLevel = orgaEntity.LogicalLevel;
            OrderIndex = orgaEntity.OrderIndex;
            ShowInHeader = orgaEntity.ShowInHeader;
            Acronym = orgaEntity.Name;

            string isNew = orgaEntity.IsNew ? "* " : "";
            string name = string.IsNullOrWhiteSpace(orgaEntity.Name) ? "" : $"[{orgaEntity.Name}]";
            string function = string.IsNullOrWhiteSpace(orgaEntity.Function) ? "" : $"[{orgaEntity.Function}]";
            string gender = string.IsNullOrWhiteSpace(orgaEntity.Gender) ? "" : $"[{orgaEntity.Gender}]";
            string headerText = orgaEntity.Header?.Deleted == true || string.IsNullOrWhiteSpace(orgaEntity.Header?.Value) ? "" : $" {orgaEntity.Header}";
            string addresses = "";
            string primaryAdrStr = orgaEntity.PrimaryAddress?.ActiveDirectoryReference;
            string secondaryAdrStr = orgaEntity.SecondaryAddress?.ActiveDirectoryReference;
            if (primaryAdrStr != null && secondaryAdrStr != null)
            {
                addresses = $" ({primaryAdrStr}+{secondaryAdrStr})";
            }

            if (primaryAdrStr != null && secondaryAdrStr == null)
            {
                addresses = $" ({primaryAdrStr})";
            }

            DisplayName += $"{isNew}{name}{function}{gender}{headerText}{addresses}";
        }

        public bool ShowInHeader { get; set; }

        public string Id { get; set; }

        public string DisplayName { get; set; }

        public string Acronym { get; set; }

        public List<JsTreeNode> Children { get; set; }

        public int LogicalLevel { get; set; }

        public int OrderIndex { get; set; }

        public string JsFunc { get; set; }
        public string ToolTip { get; set; }

        public static JsTreeNode FromOrgaTree(IOrgaChartDatabase orgaChartDatabase, OrgaEntity orgaEntity, int authorCount = 0)
        {
            var node = new JsTreeNode(orgaEntity);

            // children
            if (orgaEntity.SubEntities != null && orgaEntity.SubEntities.Any())
            {
                node.Children.AddRange(orgaEntity.SubEntities.OrderBy(o => o.OrderIndex)
                                                 .Select(o => FromOrgaTree(orgaChartDatabase, o, authorCount))
                                                 .ToList());
            }

            // authors
            if (authorCount > 0)
            {
                node.Children.AddRange(orgaChartDatabase.GetAuthorsNodes(orgaEntity.Id, authorCount));
            }

            return node;
        }
    }
}
