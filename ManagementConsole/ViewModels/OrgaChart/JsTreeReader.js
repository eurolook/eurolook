function JsTreeReader(baseUrl) {
    var self = this;
    self.baseUrl = baseUrl;
    self.read = function(treeNode) {
      var jsTreeNode = {};
      jsTreeNode.id = treeNode.id;
      jsTreeNode.text = treeNode.displayName;

      // state
      jsTreeNode.state = {};
      jsTreeNode.state.opened = true;
      jsTreeNode.state.disabled = false;
      jsTreeNode.state.selected = false;

        // icons
      switch (treeNode.logicalLevel) {
        case 1:
          // DG
          jsTreeNode.icon = 'fi-home indigo';
          break;
        case 2:
          // Directorate
          jsTreeNode.icon = 'fi-folder blue';
          break;
        case 3:
          // Unit or Function
          jsTreeNode.icon = 'fi-torsos cyan';
          break;
        case 4:
          // Person
          jsTreeNode.icon = 'fi-torso choc';
          break;
      }
    
      // show in header
      if (!treeNode.showInHeader) {
          jsTreeNode.a_attr = { style: "opacity: 0.6", title: "Not shown in header." };
      }
      else if (treeNode.jsFunc != null) {
        jsTreeNode.a_attr = {
          onClick: treeNode.jsFunc,
          title: treeNode.toolTip
        };
      }

      // children
      jsTreeNode.children = [];
      if (treeNode.children) {
          for (var i = 0; i < treeNode.children.length; i++) {
              jsTreeNode.children.push(self.read(treeNode.children[i]));
          }
      }
      return jsTreeNode;
    }
}
