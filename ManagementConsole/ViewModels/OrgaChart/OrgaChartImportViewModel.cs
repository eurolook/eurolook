﻿using System.Collections.Generic;
using Eurolook.ManagementConsole.ViewModels.Languages;

namespace Eurolook.ManagementConsole.ViewModels.OrgaChart
{
    public class OrgaChartImportViewModel
    {
        public List<JsTreeNode> Dgs { get; set; }

        public List<LanguageViewModel> Languages { get; set; }
    }
}
