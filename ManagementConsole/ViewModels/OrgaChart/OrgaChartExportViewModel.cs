using System.Collections.Generic;

namespace Eurolook.ManagementConsole.ViewModels.OrgaChart
{
    public class OrgaChartExportViewModel
    {
        public List<JsTreeNode> dgs { get; set; }
    }
}
