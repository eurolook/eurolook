﻿using System.Collections.Generic;
using Eurolook.ManagementConsole.ViewModels.Languages;

namespace Eurolook.ManagementConsole.ViewModels.OrgaChart
{
    public class OrgaChartIndexViewModel
    {
        public List<LanguageViewModel> Languages { get; set; }

        public List<JsTreeNode> Dgs { get; set; }
    }
}
