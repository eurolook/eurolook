using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Eurolook.Data.Models;

namespace Eurolook.ManagementConsole.ViewModels.OrgaChart
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    public class OrgaEntityViewModel
    {
        public OrgaEntityViewModel()
        {
            // Used by ASP actions
        }

        public OrgaEntityViewModel(OrgaEntity model)
        {
            Id = model.Id;
            SuperEntityId = model.SuperEntityId;
            LogicalLevel = model.LogicalLevel;
            Name = model.Name;
            DisplayName = $"{model.Name}: {model.Header?.Value}";
            SubEntities = new List<OrgaEntityViewModel>();
        }

        public Guid Id { get; set; }

        public Guid? SuperEntityId { get; set; }

        public string Name { get; set; }

        public int LogicalLevel { get; set; }

        public string DisplayName { get; set; }

        public List<OrgaEntityViewModel> SubEntities { get; set; }
    }
}
