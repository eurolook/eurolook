function OrgaChartImportViewModel(dgs, languages) {
    var self = this;

    var notificationTimeout = 0; // don't disappear'

    // Properties

    self.allDgs = ko.observableArray(dgs);
    self.allLanguages = ko.observableArray(languages);
    self.selectedDg = ko.observable();
    self.selectedLanguage = ko.observable();
    self.spreadsheetFileInfo = ko.observable();
    self.treeReader = new JsTreeReader(ko.baseUrl);
    self.validationResult = ko.observable("Validation result");
    self.authorResult = ko.observable("Default");
    self.uploadId = ko.observable();    

    // Events

    self.selectedLanguage.subscribe(function () {
        self.getUploadedOrgaTreeEL10();
    });

    // Functions

    self.init = function () {
        languages.forEach(function (lang) {
            if (lang.name == "EN") {
                self.selectedLanguage(lang);
            }
        });
    }

    self.uploadSpreadsheet = function () {
        var file = self.getExcelFileEL10();        
        if (!file) {
            return;
        }
        
        self.spreadsheetFileFormData = new FormData();
        self.spreadsheetFileFormData.append("file", file);
        self.spreadsheetFileInfo(file.name);
        self.showUploadProgressBar();
        document.getElementById("languageSelection").focus();

        jQuery.ajax({
            type: "POST",
            url: ko.baseUrl + "/OrgaChart/Upload",
            data: self.spreadsheetFileFormData,
            contentType: false,
            processData: false,
            success: function (data) {
                self.hideUploadProgressBar();
                self.clearExcelFile();
                if (data.success == true) {
                    self.uploadId(data.model.uploadId);                    
                    self.getUploadedOrgaTreeEL10();                                        
                } else {
                    showAlert({ type: "error", message: data.message, timeout: notificationTimeout });
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                self.hideUploadProgressBar();
                self.clearExcelFile();
                showAlert({ type: "error", message: "HTTP error: " + errorThrown, timeout: notificationTimeout });
            }
        });
    }    

    self.getUploadedOrgaTreeEL10 = function () {
        if (!self.uploadId() || !self.selectedLanguage()) return;
        self.showUploadProgressBar();

        $("#tree").jstree("destroy").empty();
        self.showTreeProgressBar();
        jQuery.ajax({
            type: "GET",
            url: ko.baseUrl + "/OrgaChart/UploadedOrgaTreeEL10?languageId=" + self.selectedLanguage().id + "&uploadId=" + self.uploadId(),
            success: function (data) {
                self.hideUploadProgressBar(); /* Orga Tree only appears on panel 2, but is creating upon upload, on panel 1, so it requires to be hidden from here */
                self.hideTreeProgressBar();
                if (data.success === true) {
                    var tree = self.treeReader.read(data.model);
                  $("#tree").jstree({ core: { data: [tree] } });
                    self.validationResult("Your file was uploaded successfully. Go to 'Validate' to check the orga chart.");
                    self.authorResult(data.message);
                } else {
                    self.validationResult(data.message);
                    showAlert({ type: "error", message: data.message, timeout: notificationTimeout });
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                self.hideTreeProgressBar();
                self.hideUploadProgressBar(); /* Orga Tree only appears on panel 2, but is creating upon upload, on panel 1, so it requires to be hidden from here */
                showAlert({ type: "error", message: "HTTP error: " + errorThrown, timeout: notificationTimeout });
            }
        });
    }

    self.confirmUpload = function() {
        if (!self.uploadId()) return;

        self.showImportProgressBar();

        var url = ko.baseUrl + "/OrgaChart/ConfirmUpload?id=" + self.uploadId();
        jQuery.ajax({
            type: "POST",
            url: url,
            data: {},
            success: function (data) {
                self.hideImportProgressBar();
                if (data.success == true) {
                    showAlert({ type: "success", message: data.message, timeout: notificationTimeout });
                } else {
                    showAlert({ type: "error", message: data.message, timeout: notificationTimeout });
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                self.hideImportProgressBar();
                showAlert({ type: "error", message: "HTTP error: " + errorThrown, timeout: notificationTimeout });
            }
        });
    }

    self.getExcelFileEL10 = function () {
        return $("#excelFileEL10")[0].files[0];
    }

    self.clearExcelFile = function () {
        $("#excelFile").val("");
        $("#excelFileEL10").val("");
    }

    self.showTreeProgressBar = function () {
        window.clearTimeout(self.treeProgressTimer);
        self.treeProgressTimer = window.setTimeout(function () {
            $("#treeProgressBar").show();
            $("#tree").hide();
        }, 500);
    }

    self.hideTreeProgressBar = function () {
        window.clearTimeout(self.treeProgressTimer);
        $("#treeProgressBar").hide();
        $("#tree").show();
    }

    self.showUploadProgressBar = function () {
        window.clearTimeout(self.uploadProgressTimer);
        self.uploadProgressTimer = window.setTimeout(function () {
            $("#uploadValidationResult").hide();
            $("#uploadProgressBar").show();
        }, 500);
    }

    self.hideUploadProgressBar = function () {
        window.clearTimeout(self.uploadProgressTimer);
        $("#uploadProgressBar").hide();
        $("#uploadValidationResult").show();
    }

    self.showImportProgressBar = function () {
        window.clearTimeout(self.importProgressTimer);
        self.importProgressTimer = window.setTimeout(function () { $("#importProgressBar").show(); }, 500);
    }

    self.hideImportProgressBar = function () {
        window.clearTimeout(self.importProgressTimer);
        $("#importProgressBar").hide();
    }

    self.init();
}
