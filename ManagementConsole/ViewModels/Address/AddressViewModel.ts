export class AddressViewModel {

    id = ko.observable("");
    name = ko.observable("");
    locationTextId = ko.observable();
    footerTextId = ko.observable();
    isDefault = ko.observable();
    isSelected = ko.observable(false);

    constructor(model) {

        if (!model) {
            return;
        }

        this.id(model.id);
        this.name(model.name);
        this.locationTextId(model.locationTextId);
        this.footerTextId(model.footerTextId);
        this.isDefault(model.isDefault);

    }
}
