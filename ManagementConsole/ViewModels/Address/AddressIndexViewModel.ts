import { PaginatedTableViewModel } from "PaginatedTableViewModel"
import {AddressViewModel} from "Address/AddressViewModel"

class AddressIndexViewModel extends PaginatedTableViewModel {

    addresses: KnockoutObservableArray<AddressViewModel> = ko.observableArray([]);
    selectedAddresses: KnockoutObservableArray<AddressViewModel> = ko.observableArray([]);
    newAddressName = ko.observable("");
    deleteModalTitle = ko.observable("");
    restoreModalTitle = ko.observable("");
    searchRequest: JQueryXHR;
    deletingAddress: AddressViewModel;
    restoringAddress: AddressViewModel;
    includeDependencies = ko.observable(false);
    isSelectedAll = ko.pureComputed({
        read: function () {
            return this.selectedAddresses().length === this.addresses().length;
        },
        write: function (value) {
            this.selectedAddresses(value ? this.addresses().slice(0) : []);
        },
        owner: this
    });

    constructor(baseUrl, isReadOnly: string) {
        super(baseUrl, "/Address", isReadOnly);
        if (!this.orderProperty()) {
            this.orderProperty("name");
            this.orderAscending(true);
        }
        this.loadPageData();
        $("#SearchBox").focus();
    }

    loadPageData() {
        if (this.searchRequest && this.searchRequest.readyState < 4) {
            this.searchRequest.abort();
        }
        this.showProgressSpinner();
        this.searchRequest = this.getJson(this.getPageUrl(this.baseUrl() + "/Address"),
            this.loadPageDataSucceeded,
            this);
    }

    loadPageDataSucceeded(result) {
        this.selectedAddresses.removeAll();
        this.addresses.removeAll();
        if (result.success === true) {
            const page = result.model;
            for (let ad of page.data) {
                this.addresses.push(new AddressViewModel(ad));
            }
            this.setPagination(page);
        }
        this.hideProgressSpinner();
    }

    createAddress() {
        this.postJson(`${this.baseUrl()}/Address/Create`,
            ko.toJSON({ name: this.newAddressName() }),
            this.createAddressModelSucceeded,
            this);
    }

    createAddressModelSucceeded(data) {
        this.closeModal();
        if (data.success === true) {
            this.showAlert({ type: "success", message: data.message });
            //this.addresses.push(new AddressViewModel(data.model));
            this.loadPageData();
        } else {
            this.showAlert({ type: "error", message: data.message });
        }
    }

    deleteAddress() {
        if (!this.deletingAddress) return;

        this.deleteJson(`${this.baseUrl()}/Address/Delete/${this.deletingAddress.id()}`,
            ko.toJSON(this.deletingAddress),
            this.deleteAddressSucceeded,
            this);
    }

    deleteAddressSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var deleteId = this.deletingAddress.id();
            this.addresses.remove(item => item.id() === deleteId);
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    restoreAddress() {
        if (!this.restoringAddress) return;
        this.postJson(`${this.baseUrl()}/Address/Restore/${this.restoringAddress.id()}`,
            ko.toJSON(this.restoringAddress),
            this.restoreAddressSucceeded,
            this);
    }

    restoreAddressSucceeded(result){
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var reviveId = this.restoringAddress.id();
            this.addresses.remove(item => item.id() === reviveId);
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    downloadDataPackage() {
        if (this.selectedAddresses().length === 0) {
            this.closeModal();
            this.showAlert({ type: "error", message: "No addresses selected." });
            return;
        }
        this.showProgressBar();
        this.postJson(`${this.baseUrl()}/Address/Export?includeDependencies=${this.includeDependencies()}`,
            ko.toJSON(this.selectedAddresses), this.downloadDataPackageSucceeded, this);
    }

    downloadDataPackageSucceeded(result) {
        if (result.success) {
            window.location.href = `${this.baseUrl()}/DataExchange/Export/Package/${result.model.packageId}`;
        } else {
            this.hideProgressBar();
            this.showAlert({ type: "error", message: result.message });
        }
    }

    showDeleteModal(viewModel: AddressViewModel) {
        this.deletingAddress = viewModel;
        this.deleteModalTitle(viewModel.name());
        $("#DeleteModal").foundation("reveal", "open");
    }

    showRestoreModal(viewModel: AddressViewModel) {
        this.restoringAddress = viewModel;
        this.restoreModalTitle(viewModel.name());
        $("#RestoreModal").foundation("reveal", "open");
    }
}
export = AddressIndexViewModel
