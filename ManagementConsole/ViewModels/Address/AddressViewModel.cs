using System;

namespace Eurolook.ManagementConsole.ViewModels.Address
{
    public class AddressViewModel
    {
        public AddressViewModel()
        {
            // Used by ASP.NET
        }

        public AddressViewModel(Data.Models.Address model)
        {
            Id = model.Id;
            Name = model.Name;
            LocationTextId = model.LocationTextId;
            FooterTextId = model.FooterTextId;
            NameTextId = model.NameTextId;
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public Guid? LocationTextId { get; set; }

        public Guid? FooterTextId { get; set; }

        public Guid? NameTextId { get; set; }
    }
}
