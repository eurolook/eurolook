
import { PageViewModel } from 'PageViewModel'

class AddressEditViewModel extends PageViewModel {

    id: string;
    url: string;
    isSaving = ko.observable(false);
    name = ko.observable('');
    phone = ko.observable('');
    locationTextAlias = ko.observable('');
    footerTextAlias = ko.observable('');
    nameTextAlias = ko.observable('');
    isDefault = ko.observable('');
    activeDirectoryReference = ko.observable('');

    hasNameText = ko.computed(function () {
        // ReSharper disable once SuspiciousThisUsage
        return this.nameTextAlias() != null;
    }, this);

    constructor(baseUrl: string, id: string, isReadOnly: string) {
        super(baseUrl, '/Address', isReadOnly);
        this.id = id;
        this.url = `${baseUrl}/Address/Edit/${id}`;
        this.bindCtrlSave(this.save);
        this.load();
    }

    load() {
        this.showProgressSpinner();
        this.getJson(this.url, this.loadSucceeded, this);

    }

    loadSucceeded(result) {
        if (!result.success) {
            this.showAlert({ type: 'error', message: result.message });
        }
        else {

            this.name(result.model.name);
            this.phone(result.model.phone);
            this.locationTextAlias(result.model.locationTextAlias);
            this.footerTextAlias(result.model.footerTextAlias);
            this.nameTextAlias(result.model.nameTextAlias);
            this.isDefault(result.model.isDefault);
            this.activeDirectoryReference(result.model.activeDirectoryReference);
            this.hideProgressSpinner();
        }
    }


    createNameText() {
        const url = `${this.baseUrl()}/Address/NameText/${this.id}`;
        this.postJson(url, null, this.createNameTextSucceeded, this);
    }

    createNameTextSucceeded(result) {
        if (!result.success) {
            this.showAlert({ type: 'error', message: result.message });
        } else {
            this.nameTextAlias(result.model);
            this.showAlert({ type: 'success', message: result.message });
        }

    }


    save() {
        if (this.isSaving()) return;
        this.isSaving(true);
        this.putJson(this.url, ko.toJSON(this), this.saveSucceeded, this);

    }

    saveSucceeded(result) {
        if (!result.success) {
            this.showAlert({ type: 'error', message: result.message });
        } else {
            this.showAlert({ type: 'success', message: result.message });
        }
        this.isSaving(false);
        this.load();
    }

}
export = AddressEditViewModel
