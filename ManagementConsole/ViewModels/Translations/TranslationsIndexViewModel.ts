import { PaginatedTableViewModel } from "PaginatedTableViewModel"
import { TranslationViewModel } from "Translations/TranslationViewModel"
declare var autosize: any;

class TranslationIndexViewModel extends PaginatedTableViewModel {

    typeFilters = ko.observableArray();
    languageFilters = ko.observableArray();
    translations: KnockoutObservableArray<TranslationViewModel> = ko.observableArray([]);

    selectedTranslations = ko.computed({
        read:() => {
        var result = Array();
        this.translations().forEach(function(item) {
                if (item.isSelected())
                    result.push(item);
            }, this);
        return result;
        },
        owner: this
    });

    selectedLanguageCode = ko.observable("");
    searchRequest: JQueryXHR;
    selectedTypeFilter = ko.observable();
    includeTexts = ko.observable(false);
    aliasSearchText = ko.observable("");
    translationSearchText = ko.observable("");

    isAliasSearchTextboxVisible = ko.computed(function() {
        return (this.selectedTypeFilter() === "alias");
    }, this);

    isTranslationSearchTextboxVisible = ko.computed(function() {
        return (this.selectedTypeFilter() === "translation");
    }, this);

    queryForAlias(aliasText) {
        this.aliasSearchText(aliasText);
        this.loadPageData();
    }

    queryForTranslation(translationText) {
        this.translationSearchText(translationText);
        this.loadPageData();
    }

    isSelectedAll = ko.pureComputed({
        read: function () {
            return this.selectedTranslations().length === this.translations().length;
        },
        write: function (value) {
            this.translations().forEach(function(item) {
                item.isSelected(value);
            })},
        owner: this
    });

    hasSelection = ko.computed(function() {
        return this.selectedTranslations().length > 0;
    }, this);

    constructor(baseUrl, isReadOnly: string) {
        super(baseUrl, "/Translations", isReadOnly);
        this.pageSizes = ko.observableArray([20, 50, 100, 500, 1000]);
        this.loadFilters().then(function() {
            // init the parameter from URL
            if (!this.orderProperty()) {
                this.orderProperty("alias");
                this.orderAscending(true);
            }
            var l = this.getParameterByName("l");
            if (l) {
                this.selectedLanguageCode(l);
            } else {
                this.selectedLanguageCode("EN");
            }
            var t = this.getParameterByName("t");
            if (t) {
                this.selectedTypeFilter(t);
            } else {
                this.selectedTypeFilter("orga");
            }
            var q = this.getParameterByName("q");
            if (!q) {
                q = "";
            }
            if (this.selectedTypeFilter() === "alias") {
                this.aliasSearchText(q);
                $("#aliasSearchTextbox").val(q);
            } else if (this.selectedTypeFilter() === "translation") {
                this.translationSearchText(q);
            }
            
            // load the data
            this.loadPageData().then(function () {
                // subscribe for events
                if (!this.typeFilterSub) {
                    this.typeFilterSub = this.selectedTypeFilter.subscribe(this.loadPageData, this);
                }
                if (!this.languageSub) {
                this.languageSub = this.selectedLanguageCode.subscribe(this.loadPageData, this);
                }
            });
        });
    }

    loadFilters() {
        this.showProgressBar();
        return this.searchRequest = this.getJson(this.getPageUrl(this.baseUrl() + "/Translations/Filters"),
            this.loadFiltersSucceeded,
            this);
    }

    loadFiltersSucceeded(result) {
        this.typeFilters.removeAll();
        this.languageFilters.removeAll();
        if (result.success === true) {
            result.model.types.forEach(function (filter) {
                this.typeFilters.push(filter);
            }, this);
            result.model.languages.forEach(function (filter) {
                this.languageFilters.push(filter);
            }, this);
        }
        this.hideProgressBar();
    }

    deleteTranslations() {
        if (this.selectedTranslations().length === 0) {
            this.closeModal();
            return;
        }
        this.deleteJson(`${this.baseUrl()}/Translations/Delete`,
            ko.toJSON(this.selectedTranslations()),
            this.deleteTranslationsSucceeded,
            this);
    }

    deleteTranslationsSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            this.translations.removeAll(this.selectedTranslations());
        } else {
            this.showAlert({ type: "error", message: result.message });
        }

    }

    loadPageData() {
        // close pending request
        if (this.searchRequest && this.searchRequest.readyState < 4) {
            this.searchRequest.abort();
        }
        // open a new request
        this.showProgressBar();
        if (this.selectedTypeFilter() === "alias") {
            this.searchQuery(this.aliasSearchText());
        }
        else if (this.selectedTypeFilter() === "translation") {
            this.searchQuery(this.translationSearchText());
        }

        var url = this.getPageUrl(this.baseUrl() + "/Translations");
        url += "&l=" + this.selectedLanguageCode();
        url += "&t=" + this.selectedTypeFilter();

        return this.searchRequest = this.getJson(url,
            this.loadPageDataSucceeded,
            this);
    }

    loadPageDataSucceeded(result) {
        this.translations.removeAll();
        if (result.success === true) {
            var page = result.model;
            for (let trans of page.data) {
                this.translations.push(new TranslationViewModel(trans));
            }
            this.setPagination(page);
            this.hideProgressBar();
            autosize(document.querySelectorAll('textarea'));
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    editTranslations() {
        if (this.selectedTranslations().length === 0) {
            return;
        }
        this.putJson(`${this.baseUrl()}/Translations/Edit`,
            ko.toJSON( this.selectedTranslations() ),
            this.editTranslationsSucceeded,
            this);
    }

    editTranslationsSucceeded(result) {
        if (result.success === true) {
            this.showAlert({ type: 'success', message: result.message });
            this.loadPageData();
        } else {
            this.showAlert({ type: 'error', message: result.message });
        }
    }

    downloadDataPackage() {
        if (this.selectedTranslations().length === 0) {
            this.closeModal();
            this.showAlert({ type: "error", message: "No translations selected." });
            return;
        }
        this.showProgressBar();
        this.postJson(`${this.baseUrl()}/Translations/Export?includeTexts=${this.includeTexts()}`,
            ko.toJSON(this.selectedTranslations), this.downloadDataPackageSucceeded, this);
    }

    downloadDataPackageSucceeded(result) {
        if (result.success) {
            window.location.href = `${this.baseUrl()}/DataExchange/Export/Package/${result.model.packageId}`;

        } else {
            this.hideProgressBar();
            this.showAlert({ type: "error", message: result.message });
        }
    }
}
export = TranslationIndexViewModel
