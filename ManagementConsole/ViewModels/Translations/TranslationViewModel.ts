export class TranslationViewModel {

    languageId = ko.observable("");
    languageName = ko.observable("");
    textId = ko.observable("");
    textAlias = ko.observable("");
    translationId = ko.observable("");
    originalValue = ko.observable("");
    value = ko.observable("");
    isSelected = ko.observable(false);

    hasTranslation = ko.pureComputed(function() {
        return (this.translationId() && this.translationId().length > 0);
    }, this);

    placeholderText = ko.pureComputed(function() {
        if (!this.hasTranslation()) {
            return "Type to create a translation ...";
        }
        return "";
    }, this);

    constructor(model) {
        if (!model) return;
        this.languageId(model.languageId);
        this.languageName(model.languageName);
        this.textId(model.textId);
        this.textAlias(model.textAlias);
        this.translationId(model.translationId);
        this.originalValue(model.originalValue);
        this.value(model.value);

        this.value.subscribe(function (newValue) {
            if (newValue != this.originalValue()) {
                this.isSelected(true);
            } else {
                this.isSelected(false);
            }
        }, this);
    }
}
