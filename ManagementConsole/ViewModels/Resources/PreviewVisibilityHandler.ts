export module PreviewVisibilityHandler
{
    export const contentTypes = {
        "image/png": "data:image/png;base64,",
        "image/jpeg": "data:image/jpeg;base64,",
        "image/emf": "", //No Web Preview
        "image/x-emf": "", //No Web Preview
        "image/svg+xml": "data:image/svg+xml;base64,",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet": "", //No Web Preview
        "text/xml": ""
    };

    export function updatePreview(model, file)
    {
        updatePreviewVisibility(model);
        if (model.hasPreview()) {
            const reader = new FileReader();
            reader.onload = e => {
                model.previewData(String(reader.result));
            };
            reader.readAsDataURL(file);
        }
    };

    export function updatePreviewVisibility(model) {
        switch (model.mimeType()) {
        case "image/png":
            model.hasPreview(true);
            break;
        case "image/jpeg":
            model.hasPreview(true);
            break;
        case "image/emf":
            model.hasPreview(false);
            break;
        case "image/x-emf":
            model.hasPreview(false);
            break;
        case "image/svg+xml":
            model.hasPreview(true);
            break;
        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
            model.hasPreview(false);
            break;
        default: // unknown -> no preview
            model.hasPreview(false);
            break;
        }
    }
}
