using System;
using System.Diagnostics.CodeAnalysis;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.ViewModels.Resources
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    public class ResourceViewModel
    {
        [UsedImplicitly]
        public ResourceViewModel()
        {
            // default constructor is used by ASP.NET MVC for creating the model from a HTTP request
        }

        public ResourceViewModel(Resource model)
        {
            Id = model.Id;
            Alias = model.Alias;
            FileName = model.FileName;
            MimeType = model.MimeType;
        }

        public Guid Id { get; set; }

        public string Alias { get; set; }

        public string FileName { get; set; }

        public string MimeType { get; set; }
    }
}
