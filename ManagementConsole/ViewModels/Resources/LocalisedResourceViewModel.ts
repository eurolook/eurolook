import { PreviewVisibilityHandler } from "Resources/PreviewVisibilityHandler";

export class LocalisedResourceViewModel {

    languageId = ko.observable();
    languageName = ko.observable<string>();
    mimeType = ko.observable<string>();
    fileNameDate = ko.observable("");
    rawData = ko.observable("");
    hasData = ko.observable(false);
    alreadyHadData = ko.observable(false);
    uploadedFile: FormData;
    isSelected = ko.observable(false);
    isModified = ko.observable(false);

    hasPreview = ko.observable(false);
    previewData = ko.observable("");

    constructor(model) {
        if (!model) return;

        this.languageId(model.languageId);
        this.languageName(model.language.name);

        if (model.mimeType != null) {
            this.mimeType(model.mimeType);
        }

        if (model.rawData != null) {
            this.fileNameDate(model.fileNameDate);
            this.rawData(model.rawData);

            this.hasData(model.hasData);
            this.isSelected(model.hasData);
        }

        if (this.hasData()) {
            this.alreadyHadData(true);
            PreviewVisibilityHandler.updatePreviewVisibility(this);
            this.previewData(PreviewVisibilityHandler.contentTypes[this.mimeType()] + this.rawData());
        }

        this.isModified(false);
    }

    delete() {
        this.uploadedFile = null;
        this.mimeType("");
        this.fileNameDate("");
        this.hasData(false);
        this.isSelected(false);
        this.hasPreview(false);
        this.previewData("");
        this.isModified(true);
        return;
    }
}

