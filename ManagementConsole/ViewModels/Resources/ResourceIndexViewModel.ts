import { PaginatedTableViewModel } from "PaginatedTableViewModel"
import { ResourceViewModel } from "Resources/ResourceViewModel"

class ResourceIndexViewModel extends PaginatedTableViewModel {

    // ReSharper disable SuspiciousThisUsage
    isSelectedAll = ko.pureComputed({
        read: function () {
            return this.selectedResources().length === this.resources().length;
        },
        write: function (value) {
            this.selectedResources(value ? this.resources().slice(0) : []);
        },
        owner: this
    });
    // ReSharper restore SuspiciousThisUsage

    resources: KnockoutObservableArray<ResourceViewModel> = ko.observableArray([]);
    selectedResources: KnockoutObservableArray<ResourceViewModel> = ko.observableArray([]);

    searchRequest: JQueryXHR;
    newResourceName = ko.observable("");
    deletingResource: ResourceViewModel = null;
    deleteModalTitle = ko.observable("");
    includeDefaultData = ko.observable(true);
    includeLocalisations = ko.observable(true);
    isSystemResource = ko.observable(false);

    constructor(baseUrl, isReadOnly: string) {
        super(baseUrl, "/Resources", isReadOnly);
        if (!this.orderProperty()) {
            this.orderProperty("alias");
            this.orderAscending(true);
        }

        this.loadPageData();
    }

    loadPageData() {
        if (this.searchRequest && this.searchRequest.readyState < 4) {
            this.searchRequest.abort();
        }
        this.showProgressSpinner();
        this.searchRequest = this.getJson(this.getPageUrl(this.baseUrl() + "/Resources"),
            this.loadPageDataSucceeded,
            this);
    }

    loadPageDataSucceeded(result) {
        this.selectedResources.removeAll();
        this.resources.removeAll();
        if (result.success === true) {
            const page = result.model;
            for (let rm of page.data) {
                this.resources.push(new ResourceViewModel(rm));
            }
            this.setPagination(page);
        }
        this.hideProgressSpinner();
    }

    createResource() {
        const viewModel = new ResourceViewModel(null);
        viewModel.alias(this.newResourceName());
        viewModel.isSystemResource(this.isSystemResource());
        this.postJson(`${this.baseUrl()}/Resources/Create`,
            ko.toJSON(viewModel),
            this.createResourceSucceeded,
            this);
    }

    createResourceSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            this.resources.push(new ResourceViewModel(result.model));
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    deleteResource() {
        if (!this.deletingResource) return;
        this.deleteJson(`${this.baseUrl()}/Resources/Delete/${this.deletingResource.id()}`,
            ko.toJSON(this.deletingResource),
            this.deleteResourceSucceeded,
            this);
    }

    deleteResourceSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var deleteId = this.deletingResource.id();
            this.resources.remove(item => item.id() === deleteId);
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    showDeleteModal(viewModel) {
        this.deletingResource = viewModel;
        this.deleteModalTitle(viewModel.name);
        // ReSharper disable once TsResolvedFromInaccessibleModule
        $("#DeleteModal").foundation("reveal", "open");
    }

    downloadDataPackage() {
        if (this.selectedResources().length === 0) {
            this.closeModal();
            return this.showAlert({ type: "error", message: "No Resource selected." });
        }
        this.showProgressBar();
        this.postJson(`${this.baseUrl()}/Resources/Export?includeDefaultData=${this.includeDefaultData()}&includeLocalisations=${this.includeLocalisations()}`,
            ko.toJSON(this.selectedResources),
            this.downloadDataPackageSucceeded,
            this);
    }

    downloadDataPackageSucceeded(result) {
        if (result.success) {
            window.location.href = `${this.baseUrl()}/DataExchange/Export/Package/${result.model.packageId}`;
        } else {
            this.hideProgressBar();
            this.showAlert({ type: "error", message: result.message });
        }
    }
}

export = ResourceIndexViewModel;
