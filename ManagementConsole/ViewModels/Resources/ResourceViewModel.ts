export class ResourceViewModel {

    id = ko.observable("");
    alias = ko.observable("");
    fileName = ko.observable("");
    mimeType = ko.observable("");
    niceName = ko.observable("");

    isSelected = ko.observable(false);
    isDeleted = ko.observable(false);
    isSystemResource = ko.observable(false);

    constructor(model) {
        if (!model) return;

        this.id(model.id);
        this.alias(model.alias);
        this.fileName(model.fileName);
        this.mimeType(model.mimeType);

        this.niceName(this.mimeType() == null ? this.alias() : this.alias() + " <" + this.mimeType() + ">");

        this.isDeleted(model.isDeleted === true);
    }
}
