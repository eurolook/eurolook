import { PageViewModel } from "PageViewModel"
import { LocalisedResourceViewModel } from "Resources/LocalisedResourceViewModel";
import { PreviewVisibilityHandler } from "Resources/PreviewVisibilityHandler";
import Utils = require("Utils")
import { BrickViewModel } from "../Bricks/BrickViewModel";

class ResourceEditViewModel extends PageViewModel {

    id: string;
    url: string;

    alias = ko.observable("");
    fileName = ko.observable("");

    mimeType = ko.observable("");
    rawDataFormData: FormData = null;
    rawData = ko.observable("");
    hasData = ko.observable(false);
    alreadyHadData = ko.observable(false);
    rawDataModified = ko.observable(false);
    isSystemResource = ko.observable(false);

    bricks: KnockoutObservableArray<BrickViewModel> = ko.observableArray([]);

    localisations = ko.observableArray<LocalisedResourceViewModel>();
    languages = ko.observableArray(null);

    hasPreview = ko.observable(false);
    previewData = ko.observable("");

    isSaving = ko.observable(false);

    constructor(baseUrl: string, id: string, isReadOnly: string) {
        super(baseUrl, "/Resources", isReadOnly);
        this.id = id;
        this.url = baseUrl + "/Resources/Edit/" + id;
        this.bindCtrlSave(this.save);
        this.load();
    }

    load() {
        this.showProgressSpinner();
        this.getJson(this.url, this.loadSucceeded, this);
    }

    loadSucceeded(result) {
        this.alias(result.model.alias);
        if (result.model.mimeType != null) {
            this.mimeType(result.model.mimeType);
        }

        if (result.model.rawData != null) {
            this.rawData(result.model.rawData);
            this.hasData(true);
            this.alreadyHadData(true);
            this.fileName(result.model.fileName);
        }

        if (this.hasData()) {
            PreviewVisibilityHandler.updatePreviewVisibility(this);
            this.previewData(PreviewVisibilityHandler.contentTypes[this.mimeType()] + this.rawData());
        }

        this.bricks(result.model.bricks);

        for (let lang of result.model.localisations) {
            this.localisations.push(new LocalisedResourceViewModel(lang));
        }

        this.isSystemResource(result.model.isSystemResource);

        this.hideProgressSpinner();
    }

    save() {
        if (this.isSaving()) return;
        this.isSaving(true);

        const pojo = ko.toJS(this);
        delete pojo.rawData;
        delete pojo.previewData;
        for (let lr of pojo.localisations) {
            delete lr.rawData;
            delete lr.previewData;
        }

        const requests: Array<JQueryXHR> = new Array();
        requests.push(this.postFormData(`${this.baseUrl()}/Resources/Data/${this.id}?isModified=${this.rawDataModified()}`, this.rawDataFormData, this));
        requests.push(this.putJson(this.baseUrl() + "/Resources/Edit/" + this.id, ko.toJSON(pojo), null, this));
        for (let lr of this.localisations()) {
            if (!lr.isSelected() && lr.alreadyHadData()) {
                requests.push(this.postFormData(`${this.baseUrl()}/Resources/LocalisedResource/${this.id}?languageId=${lr.languageId()}`, null, this));
            }
            if (lr.isSelected() && lr.isModified()) {
                requests.push(this.postFormData(`${this.baseUrl()}/Resources/LocalisedResource/${this.id}?languageId=${lr.languageId()}&fileNameDate=${lr.fileNameDate()}`, lr.uploadedFile, this));
            }
        }

        jQuery.when.apply(jQuery, requests).then(this.saveSucceeded).fail(this.saveFailed);
    }

    saveSucceeded(dataResponse, editResponse) {
        const that: ResourceEditViewModel = this[0];
        const dataStatus = dataResponse[0];
        const editStatus = editResponse[0];
        if (dataStatus.success === true &&
            editStatus.success === true) {
            that.showAlert({ type: "success", message: editStatus.message });
        } else {
            // Server side error
            let errorMsg = "";
            if (dataStatus.success === false)
                errorMsg += dataStatus.message;
            if (editStatus.success === false)
                errorMsg += editStatus.message;
            that.showAlert({ type: "error", message: errorMsg });
        }
        that.isSaving(false);
    }

    saveFailed(jqXhr) {
        this.showAlert({ type: "error", message: `HTTP error: ${jqXhr.statusText}` });
        this.isSaving(false);
    }

    onLocalisedResourceFileClick(linkElement: HTMLElement) {
        const prevElement = linkElement.previousElementSibling;
        if (prevElement.nodeName === "INPUT") {
            const inputElement = prevElement as HTMLInputElement;
            inputElement.click();
        }
    }
    
    setLocalisedResourceFile(localisedResource: LocalisedResourceViewModel, file: File) {
        if (!Object.keys(PreviewVisibilityHandler.contentTypes).includes(file.type)) {
            this.showAlert({ type: "error", message: "Error: Invalid File Extension" });
            return;
        }

        localisedResource.uploadedFile = new FormData();
        localisedResource.uploadedFile.append("file", file);
        const fileNameDate = Utils.getDateForFilename(new Date());
        localisedResource.uploadedFile.append("fileName", fileNameDate);

        localisedResource.mimeType(file.type);
        localisedResource.fileNameDate(fileNameDate);
        localisedResource.hasData(true);
        localisedResource.isModified(true);
        localisedResource.isSelected(true);
        PreviewVisibilityHandler.updatePreview(localisedResource, file);
    }

    chooseResourceFile() {
        $("#rawData").click();
    }

    choseResourceFile(file) {        
        this.rawDataFormData = new FormData();
        this.rawDataFormData.append("file", file);

        this.mimeType(file.type);
        const fileName = this.alias() + " " + file.name + " " + Utils.getDateForFilename(new Date());
        this.rawDataFormData.append("fileName", fileName);
        this.fileName(fileName);
        this.hasData(true);
        this.rawDataModified(true);
        PreviewVisibilityHandler.updatePreview(this, file);
    }

    deleteResourceFile() {
        this.rawDataFormData = null;
        this.rawData("");
        this.mimeType("");
        this.fileName("");
        this.hasData(false);
        this.rawDataModified(true);
        this.hasPreview(false);
        this.previewData("");
        return;
    }
}
export = ResourceEditViewModel
