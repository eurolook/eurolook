import { PageViewModel } from "PageViewModel"

class HomeIndexViewModel extends PageViewModel {
    shortcutCategories = ko.observableArray([]);

    constructor(baseUrl, isReadOnly: string) {
        super(baseUrl, "/", isReadOnly);
        this.loadShortcuts();
    }

    loadShortcuts() {
        this.showProgressBar();
        this.getJson(this.baseUrl() + "/Home/Shortcuts",
            this.loadShortcutsSucceeded,
            this);
    }

    loadShortcutsSucceeded(result) {
        this.shortcutCategories.removeAll();
        this.hideProgressBar();
        if (result.success === true) {
            result.model.categories.forEach(category => {
                this.shortcutCategories.push(category);
            }, this);
        }
    }

    convertToUrl(target: string): string {
        if (target.startsWith("/"))
            return target;
        else if (target.includes("://"))
            return target;
        else
            return `/${target}`;
    }
}

export = HomeIndexViewModel;
