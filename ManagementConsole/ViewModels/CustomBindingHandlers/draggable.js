﻿ko.bindingHandlers.draggable = {
    init: function (element) {
        $(element).draggable().draggable({
             revert: true, cursor: 'move', helper: 'clone'
        });
    }
};