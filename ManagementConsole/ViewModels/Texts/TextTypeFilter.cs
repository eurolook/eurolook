﻿namespace Eurolook.ManagementConsole.ViewModels.Texts
{
    public class TextTypeFilter
    {
        public static readonly TextTypeFilter All = new TextTypeFilter
        {
            Id = "all",
            DisplayName = "All texts",
        };

        public static readonly TextTypeFilter Alias = new TextTypeFilter
        {
            Id = "alias",
            DisplayName = "Name is like...",
        };

        public static readonly TextTypeFilter Translation = new TextTypeFilter
        {
            Id = "translation",
            DisplayName = "Translation is like...",
        };

        public static readonly TextTypeFilter Orga = new TextTypeFilter
        {
            Id = "orga",
            DisplayName = "Organigram",
        };

        public static readonly TextTypeFilter Addresses = new TextTypeFilter
        {
            Id = "addresses",
            DisplayName = "Addresses",
        };

        public static readonly TextTypeFilter Functions = new TextTypeFilter
        {
            Id = "functions",
            DisplayName = "Functions",
        };

        public static readonly TextTypeFilter SystemTexts = new TextTypeFilter
        {
            Id = "systemTexts",
            DisplayName = "System Texts",
        };

        public static readonly TextTypeFilter Docs = new TextTypeFilter
        {
            Id = "docs",
            DisplayName = "Any document model",
        };

        public static readonly TextTypeFilter Unused = new TextTypeFilter
        {
            Id = "unused",
            DisplayName = "Unused Texts",
        };

        public string Id { get; set; }

        public string DisplayName { get; set; }
    }
}
