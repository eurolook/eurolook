function TextViewModel(model) {
    var self = this;

    if(model === undefined || model == null) {
        self.alias = ko.observable();
        self.englishTranslation = ko.observable();
        self.textId = ko.observable();
        self.translationCount = ko.observable();
    }
    else {
        self.alias = ko.observable(model.alias);
        self.englishTranslation = ko.observable(model.englishTranslation);
        self.textId = ko.observable(model.textId);
        self.translationCount = ko.observable(model.translationCount);
    }

    self.displayText = ko.computed(function () {
        var charCount = 50;
        var txt;
        var en = self.englishTranslation();
        if (en != null) {
            txt = self.alias() + " - " + en.substring(0, charCount);
            if (en.length > charCount) {
                txt += "...";
            }
        } else {
            txt = self.alias();
        }
        return txt;
    });

    self.isSelected = ko.observable(false);
}