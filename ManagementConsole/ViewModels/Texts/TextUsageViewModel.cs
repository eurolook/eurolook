using System;

namespace Eurolook.ManagementConsole.ViewModels.Texts
{
    public class TextUsageViewModel
    {
        public TextUsageViewModel(string name, Guid id)
        {
            Name = name;
            Id = id;
        }

        public string Name { get; }

        public Guid Id { get; }
    }
}
