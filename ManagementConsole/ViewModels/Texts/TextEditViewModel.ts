import { PageViewModel } from "PageViewModel"

class TextEditViewModel extends PageViewModel {

    textId: string;
    url: string;
    alias = ko.observable('');
    usedInBricks: KnockoutObservableArray<string> = ko.observableArray([]);
    usedInAddresses: KnockoutObservableArray<string> = ko.observableArray([]);
    usedInOrgaEntities: KnockoutObservableArray<string> = ko.observableArray([]);
    usedInPredefinedFunctions: KnockoutObservableArray<string> = ko.observableArray([]);
    isSystemText = ko.observable(false);
    isUnusedText = ko.observable(false);

    isSaving = ko.observable(false);

    constructor(baseUrl: string, id: string, isReadOnly: string) {
        super(baseUrl, '/Texts', isReadOnly);
        this.textId = id;
        this.url = `${baseUrl}/Texts/Edit/${id}`;
        this.load();
    }

    load() {
        this.showProgressSpinner();
        this.getJson(this.url, this.loadSucceeded, this);
    }

    loadSucceeded(result) {
            this.alias(result.model.alias);
            this.usedInAddresses(result.model.usedInAddresses);
            this.usedInBricks(result.model.usedInBricks);
            this.usedInOrgaEntities(result.model.usedInOrgaEntities);
            this.usedInPredefinedFunctions(result.model.usedInPredefinedFunctions);
            this.isSystemText(result.model.isSystemText);
            this.isUnusedText(result.model.isUnusedText);
            this.hideProgressSpinner();
    }

    save() {
        if (this.isSaving()) return;
        this.isSaving(true);

        const pojo = ko.toJS(this);
        pojo.IsSystemText = this.isSystemText;


        $.when(
            this.putJson(this.baseUrl() + "/Texts/Edit/" + this.textId,
                ko.toJSON(pojo),
                null,
                this)
        ).then(this.saveSucceeded).fail(this.saveFailed);
    }

    saveSucceeded(editResponse) {
        const that: TextEditViewModel = this;
        const editStatus = editResponse;
        if (editStatus.success === true) {
            that.showAlert({ type: "success", message: editStatus.message });
        } else {
            // Server side error
            let errorMsg = "";
            if (editStatus.success === false)
                errorMsg += editStatus.message;
            that.showAlert({ type: "error", message: errorMsg });
        }
        that.isSaving(false);
    }

    saveFailed(jqXhr) {
        this.showAlert({ type: "error", message: `HTTP error: ${jqXhr.statusText}` });
        this.isSaving(false);
    }
}
export = TextEditViewModel
