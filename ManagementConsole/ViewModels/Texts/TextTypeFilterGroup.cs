﻿using System.Collections.Generic;

namespace Eurolook.ManagementConsole.ViewModels.Texts
{
    public class TextTypeFilterGroup
    {
        public string Label { get; set; }

        public List<TextTypeFilter> Options { get; set; }
    }
}
