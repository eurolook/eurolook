export class TextViewModel {

    alias = ko.observable("");
    textId = ko.observable();
    englishTranslation = ko.observable("");
    translationCount = ko.observable();
    displayText = ko.observable();

    constructor(model) {
        if (model === undefined || model == null) {
            this.alias = ko.observable();
            this.englishTranslation = ko.observable();
            this.textId = ko.observable();
            this.translationCount = ko.observable();
        } else {
            this.alias = ko.observable(model.alias);
            this.englishTranslation = ko.observable(model.englishTranslation);
            this.textId = ko.observable(model.textId);
            this.translationCount = ko.observable(model.translationCount);
            this.displayText = ko.pureComputed(function (this) {
                var charCount = 50;
                var txt;
                var en = this.englishTranslation();
                if (en != null) {
                    txt = this.alias() + " - " + en.substring(0, charCount);
                    if (en.length > charCount) {
                        txt += "...";
                    }
                } else {
                    txt = this.alias();
                }
                return txt;
            }, this);
        }

    }

}
