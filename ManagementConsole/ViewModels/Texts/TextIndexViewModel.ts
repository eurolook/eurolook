import { PaginatedTableViewModel } from "PaginatedTableViewModel"
import { TextViewModel } from "Texts/TextViewModel"

class TextIndexViewModel extends PaginatedTableViewModel {

    texts: KnockoutObservableArray<TextViewModel> = ko.observableArray([]);
    selectedTexts: KnockoutObservableArray<TextViewModel> = ko.observableArray([]);
    textFilters = ko.observableArray();
    textCategories = ko.observableArray();
    newTextAlias = ko.observable();
    isSystemText = ko.observable(false);
    searchRequest: JQueryXHR;
    selectedTypeFilter = ko.observable();
    includeTranslations = ko.observable(false);

    isSelectedAll = ko.pureComputed({
        read: function () {
            return this.selectedTexts().length === this.texts().length;
        },
        write: function (value) {
            this.selectedTexts(value ? this.texts().slice(0) : []);
        },
        owner: this
    });

    hasSelection = ko.computed(function () {
        return this.selectedTexts().length > 0;
    }, this);

    constructor(baseUrl, isReadOnly: string) {
        super(baseUrl, "/Texts", isReadOnly);
        this.pageSizes = ko.observableArray([20, 50, 100, 500, 1000]);
        this.loadFilters().then(function() {
            this.orderProperty("alias");
            this.orderAscending(true);
            const t = this.getParameterByName("t");
            if (t) {
                this.selectedTypeFilter(t);
            } else {
                this.selectedTypeFilter("orga");
            }
            this.loadPageData().then(function() {
                if (!this.typeFilterSub) {
                    this.typefilterSub = this.selectedTypeFilter.subscribe(this.loadPageData, this);
                }
            });
        });
    }

    loadPageData() {
        if (this.searchRequest && this.searchRequest.readyState < 4) {
            this.searchRequest.abort();
        }
        this.showProgressBar();
        return this.searchRequest = this.getJson(this.getPageUrl(this.baseUrl() + "/Texts"),
            this.loadPageDataSucceeded,
            this);
    }

    loadPageDataSucceeded(result) {
        this.texts.removeAll();
        if (result.success === true) {
            const page = result.model;
            for (let pag of page.data) {
                this.texts.push(new TextViewModel(pag));
            }
            this.setPagination(page);
            this.hideProgressBar();
        } else {
            this.showAlert({ type: "error", message: result.message });
            this.hideProgressBar();
        }
    }
 
    loadFilters() {
        this.showProgressBar();
        return this.searchRequest = this.getJson(this.getPageUrl(this.baseUrl() + "/Texts/Filters"),
            this.loadFiltersSucceeded,
            this);
    }

    loadFiltersSucceeded(result) {
        this.textFilters.removeAll();
        if (result.success === true) {
            result.model.forEach(function(filter) {
                    this.textFilters.push(filter);
                },
                this);
        } else {
            this.showAlert({ type: "error", message: result.message });
            this.hideProgressBar();
        }
        this.hideProgressBar();
    }

    createText() {
        this.postJson(`${this.baseUrl()}/Texts/Create`,
            ko.toJSON(
                {
                    alias: this.newTextAlias(),
                    isSystemText: this.isSystemText()
                }),
            this.createTextModelSucceeded,
            this);
    }

    createTextModelSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            if (this.selectedTypeFilter() === "unused") {
                this.loadPageData();
            } else {
                this.selectedTypeFilter("unused");
            }
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    deleteTexts() {
        if (!this.selectedTexts()) {
            this.closeModal();
            return;
        }
        this.deleteJson(`${this.baseUrl()}/Texts/Delete`,
            ko.toJSON(this.selectedTexts()),
            this.deleteTextsSucceeded,
            this);
    }

    deleteTextsSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.closeModal();
            this.showAlert({ type: "success", message: result.message });
            this.texts.removeAll(this.selectedTexts());
            this.selectedTexts([]);
        } else {
            this.closeModal();
            this.showAlert({ type: "error", message: result.message });
        }
    }

    downloadDataPackage() {
        if (this.selectedTexts().length === 0) {
            this.closeModal();
            this.showAlert({ type: "error", message: "No texts selected." });
            return;
        }
        this.showProgressBar();
        this.postJson(`${this.baseUrl()}/Texts/Export?includeTranslations=${this.includeTranslations()}`,
            ko.toJSON(this.selectedTexts), this.downloadDataPackageSucceeded, this);
    }

    downloadDataPackageSucceeded(result) {
        if (result.success) {
            window.location.href = `${this.baseUrl()}/DataExchange/Export/Package/${result.model.packageId}`;
        } else {
            this.hideProgressBar();
            this.showAlert({ type: "error", message: result.message });
        }
    }

    getPageUrl(baseUrl) {
        return super.getPageUrl(baseUrl) + "&t=" + this.selectedTypeFilter();
    }
}

export = TextIndexViewModel
