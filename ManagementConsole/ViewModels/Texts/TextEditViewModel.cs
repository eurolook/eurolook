﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Eurolook.ManagementConsole.ViewModels.Texts
{
    public class TextEditViewModel
    {
        public TextEditViewModel()
        {
        }

        public TextEditViewModel(
            Guid id,
            string alias,
            IEnumerable<TextUsageViewModel> usedInAddresses,
            IEnumerable<TextUsageViewModel> usedInOrgaEntities,
            IEnumerable<TextUsageViewModel> usedInBricks,
            IEnumerable<TextUsageViewModel> usedInPredefinedFunctions,
            bool isSystemText)
        {
            Alias = alias;
            TextId = id;
            UsedInAddresses = usedInAddresses;
            UsedInOrgaEntities = usedInOrgaEntities;
            UsedInBricks = usedInBricks;
            UsedInPredefinedFunctions = usedInPredefinedFunctions;
            IsSystemText = isSystemText;
            IsUnusedText = !(UsedInAddresses.Any()
                || UsedInOrgaEntities.Any()
                || UsedInBricks.Any()
                || UsedInPredefinedFunctions.Any());
        }

        public string Alias { get; set; }

        public Guid? TextId { get; set; }

        public IEnumerable<TextUsageViewModel> UsedInAddresses { get; set; }

        public IEnumerable<TextUsageViewModel> UsedInOrgaEntities { get; set; }

        public IEnumerable<TextUsageViewModel> UsedInBricks { get; set; }

        public IEnumerable<TextUsageViewModel> UsedInPredefinedFunctions { get; set; }

        public bool IsSystemText { get; set; }

        public bool IsUnusedText { get; set; }
    }
}
