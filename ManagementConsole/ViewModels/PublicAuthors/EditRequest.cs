using System.ComponentModel.DataAnnotations;

namespace Eurolook.ManagementConsole.ViewModels.PublicAuthors
{
    public class EditRequest
    {
        [Required(ErrorMessage = "Your email address is required")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        public string YourEmailAddress { get; set; }

        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        public string OtherEmailAddress { get; set; }
    }
}
