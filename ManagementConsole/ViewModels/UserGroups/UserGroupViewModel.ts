export class UserGroupViewModel {

    isSelected = ko.observable(false);
    id: string = "";
    name: string = "";
    description: string = "";
    uiPositionIndex: number;
    adGroups: string = "";

    constructor(model) {
        if (!model) return;

        this.isSelected(model.isSelected);
        this.id = model.id;
        this.name = model.name;
        this.description = model.description;
        this.uiPositionIndex = model.uiPositionIndex;
        this.adGroups = model.adGroups;
    }
}
