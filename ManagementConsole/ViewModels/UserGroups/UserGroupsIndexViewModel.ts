import { PaginatedTableViewModel } from "PaginatedTableViewModel"
import { UserGroupViewModel } from "UserGroups/UserGroupViewModel"

class UserGroupsIndexViewModel extends PaginatedTableViewModel {

    userGroups: KnockoutObservableArray<UserGroupViewModel> = ko.observableArray([]);
    newUserGroupName = ko.observable("");
    deleteModalTitle = ko.observable("");
    reviveModalTitle = ko.observable("");
    searchRequest: JQueryXHR;
    deletingUserGroup: UserGroupViewModel;
    revivingUserGroup: UserGroupViewModel;

    selectedUserGroups: KnockoutObservableArray<UserGroupViewModel> = ko.observableArray([]);

    // ReSharper disable SuspiciousThisUsage
    isSelectedAll = ko.pureComputed({
        read: function () {
            return this.selectedUserGroups().length > 0 &&
                this.selectedUserGroups().length === this.userGroups().length;
        },
        write: function (value) {
            this.selectedUserGroups(value ? this.userGroups().slice(0) : []);
        },
        owner: this
    });
    // ReSharper enable SuspiciousThisUsage

    constructor(baseUrl, isReadOnly: string) {
        super(baseUrl, "/UserGroups", isReadOnly);
        if (!this.orderProperty()) {
            this.orderProperty("uiPositionIndex");
            this.orderAscending(true);
        }
        this.loadPageData();
        $("#SearchBox").focus();
    }

    loadPageData() {
        if (this.searchRequest && this.searchRequest.readyState < 4) {
            this.searchRequest.abort();
        }
        this.showProgressSpinner();
        this.searchRequest = this.getJson(this.getPageUrl(this.baseUrl() + "/UserGroups"),
            this.loadPageDataSucceeded,
            this);
    }

    loadPageDataSucceeded(result) {
        this.userGroups.removeAll();
        if (result.success === true) {
            const page = result.model;
            for (let userGroup of page.data) {
                this.userGroups.push(new UserGroupViewModel(userGroup));
            }
            this.setPagination(page);
        }
        this.hideProgressSpinner();
    }
    
    createUserGroup() {
        this.postJson(`${this.baseUrl()}/UserGroups/Create`,
            ko.toJSON({ name: this.newUserGroupName() }),
            this.createUserGroupSucceeded,
            this);
    }

    createUserGroupSucceeded(data) {
        this.closeModal();
        if (data.success === true) {
            this.showAlert({ type: "success", message: data.message });
            this.loadPageData();
        } else {
            this.showAlert({ type: "error", message: data.message });
        }
    }

    deleteUserGroup() {
        if (!this.deletingUserGroup) return;
        this.deleteJson(`${this.baseUrl()}/UserGroups/Delete/${this.deletingUserGroup.id}`,
            ko.toJSON(this.deletingUserGroup),
            this.deleteUserGroupSucceeded,
            this);
    }

    deleteUserGroupSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var deleteId = this.deletingUserGroup.id;
            this.userGroups.remove(item => item.id === deleteId);
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    reviveUserGroup() {
        if (!this.revivingUserGroup) return;
        this.postJson(`${this.baseUrl()}/UserGroups/Revive/${this.revivingUserGroup.id}`,
            ko.toJSON(this.revivingUserGroup),
            this.reviveUserGroupSucceeded,
            this);
    }

    reviveUserGroupSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var reviveId = this.revivingUserGroup.id;
            this.userGroups.remove(item => item.id === reviveId);
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    downloadDataPackage() {
        if (this.selectedUserGroups().length === 0) {
            this.closeModal();
            this.showAlert({ type: "error", message: "No user group selected." });
            return;
        }
        this.showProgressBar();
        this.postJson(`${this.apiEndpointUrl()}/Export`,
            ko.toJSON(this.selectedUserGroups),
            this.downloadDataPackageSucceeded,
            this);
    }

    downloadDataPackageSucceeded(result) {
        if (result.success) {
            window.location.href = `${this.baseUrl()}/DataExchange/Export/Package/${result.model.packageId}`;

        } else {
            this.hideProgressBar();
            this.showAlert({ type: "error", message: result.message });
        }
    }

    showDeleteModal(viewModel) {
        this.deletingUserGroup = viewModel;
        this.deleteModalTitle(viewModel.name);
        // ReSharper disable once TsResolvedFromInaccessibleModule
        $("#DeleteModal").foundation("reveal", "open");
    }

    showReviveModal(viewModel) {
        this.revivingUserGroup = viewModel;
        this.reviveModalTitle(viewModel.name);
        // ReSharper disable once TsResolvedFromInaccessibleModule
        $("#ReviveModal").foundation("reveal", "open");
    }
}
export = UserGroupsIndexViewModel;
