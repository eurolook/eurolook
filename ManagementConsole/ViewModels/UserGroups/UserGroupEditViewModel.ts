import { PageViewModel } from "../PageViewModel"

class UserGroupEditViewModel extends PageViewModel {

    id: string;
    url: string;
    name = ko.observable("");
    description = ko.observable("");
    uiPositionIndex = ko.observable(0);
    adGroups = ko.observable("");

    isSaving = ko.observable(false);

    constructor(baseUrl: string, id: string, isReadOnly: string) {
        super(baseUrl, "/UserGroups", isReadOnly);
        this.id = id;
        this.url = this.apiEndpointUrl() + "/Edit/" + id;
        this.bindCtrlSave(this.save);
        this.load();
    }

    load() {
        this.showProgressSpinner();
        this.getJson(this.url, this.loadSucceeded, this);
    }

    loadSucceeded(result) {
        this.name(result.model.name);
        this.description(result.model.description);
        this.uiPositionIndex(result.model.uiPositionIndex);
        this.adGroups(result.model.adGroups);

        this.hideProgressSpinner();
    }

    save() {
        if (this.isSaving()) return;
        this.isSaving(true);
        this.putJson(this.url, ko.toJSON(this), this.saveSucceeded, this);
    }

    saveSucceeded(result) {
        if (!result.success) {
            this.showAlert({ type: "error", message: result.message });
        } else {
            this.showAlert({ type: "success", message: result.message });
        }

        this.isSaving(false);
    }
}

export = UserGroupEditViewModel
