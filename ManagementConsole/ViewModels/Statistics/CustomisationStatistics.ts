import { PageViewModel } from "../PageViewModel"
declare var Chart: typeof import("../../node_modules/chart.js/auto");

class CustomisationStatistics extends PageViewModel {
    membershipChart = null;
    colorSchemeChart = null;
    stylesBoxChart = null;
    defaultBricksChart = null;
    addedDefaultBricksChart = null;
    removedDefaultBricksChart = null;

    updateMembershipChartRequest: JQueryXHR = null;
    updateColorSchemeChartRequest: JQueryXHR = null;
    updateStylesBoxChartRequest: JQueryXHR = null;
    updateDefaultBricksChartRequest: JQueryXHR = null;

    membershipChartProgressTimer: number = null;
    colorSchemeChartProgressTimer: number = null;
    stylesBoxChartProgressTimer: number = null;
    defaultBricksChartProgressTimer: number = null;

    selectedMonthsFilter = ko.observable(3);
    monthsFilters = ko.observableArray([
        {
            displayName: "Last Month (today minus 1 month)",
            value: 2
        },
        {
            displayName: "Last 3 Months (today minus 3 months)",
            value: 3
        },
        {
            displayName: "Last 6 Months (today minus 6 months)",
            value: 4
        },
        {
            displayName: "Last 12 Months (today minus 12 months)",
            value: 5
        },
    ]);

    constructor(baseUrl, isReadOnly: string, chart) {
        super(baseUrl, "/Statistics", isReadOnly);

        Chart = chart;

        this.selectedMonthsFilter.subscribe(function (_) {
            this.updateCharts();
        }.bind(this));

        this.updateCharts();
    }

    async updateCharts() {
        this.showProgressBars();
        this.destroyCharts();

        if (this.updateMembershipChartRequest && this.updateMembershipChartRequest.readyState < 4) {
            this.updateMembershipChartRequest.abort();
        }

        this.updateMembershipChartRequest = this.getJson(
            this.baseUrl() + "/Statistics/cepmembership/" + this.selectedMonthsFilter(),
            (result) => {
                this.membershipChart = this.createPieChart(result, "cepChart");
                this.hideMembershipProgressBar();
            },
            this
        );

        if (this.updateColorSchemeChartRequest && this.updateColorSchemeChartRequest.readyState < 4) {
            this.updateColorSchemeChartRequest.abort();
        }

        this.updateColorSchemeChartRequest = this.getJson(
            this.baseUrl() + "/Statistics/colorscheme/" + this.selectedMonthsFilter(),
            (result) => {
                this.colorSchemeChart = this.createPieChart(result, "colorSchemeChart", [
                    {
                        label: "Users",
                        data: result.map(row => row.y),
                        backgroundColor: result.map(row => this.getBackgroundColor(row.name))
                    }
                ]);
                this.hideColorSchemeProgressBar();
            },
            this
        );

        if (this.updateStylesBoxChartRequest && this.updateStylesBoxChartRequest.readyState < 4) {
            this.updateStylesBoxChartRequest.abort();
        }

        this.updateStylesBoxChartRequest = this.getJson(
            this.baseUrl() + "/Statistics/stylesbox/" + this.selectedMonthsFilter(),
            (result) => {
                this.stylesBoxChart = this.createPieChart(result, "stylesBoxChart");
                this.hideStylesBoxProgressBar();
            },
            this
        );

        if (this.updateDefaultBricksChartRequest && this.updateDefaultBricksChartRequest.readyState < 4) {
            this.updateDefaultBricksChartRequest.abort();
        }

        this.updateDefaultBricksChartRequest = this.getJson(
            this.baseUrl() + "/Statistics/defaultbricksdata/" + this.selectedMonthsFilter(),
            (result) => {
                this.defaultBricksChart = this.createBarChart(result.defaultBricksModifications, "defaultBricksChart");
                this.addedDefaultBricksChart = this.createBarChart(result.addedDefaultBricks, "addedDefaultBricksChart");
                this.removedDefaultBricksChart = this.createBarChart(result.removedDefaultBricks, "removedDefaultBricksChart");
                this.hideDefaultBricksProgressBar();
            },
            this
        );
    }

    showProgressBars() {
        this.showMembershipProgressBar();
        this.showColorSchemeProgressBar();
        this.showStylesBoxProgressBar();
        this.showDefaultBricksProgressBar();
    }

    destroyCharts() {
        if (this.membershipChart != null) {
            this.membershipChart.destroy();
        }
        if (this.colorSchemeChart != null) {
            this.colorSchemeChart.destroy();
        }
        if (this.stylesBoxChart != null) {
            this.stylesBoxChart.destroy();
        }
        if (this.defaultBricksChart != null) {
            this.defaultBricksChart.destroy();
        }
        if (this.addedDefaultBricksChart != null) {
            this.addedDefaultBricksChart.destroy();
        }
        if (this.removedDefaultBricksChart != null) {
            this.removedDefaultBricksChart.destroy();
        }
    }

    showMembershipProgressBar() {
        window.clearTimeout(this.membershipChartProgressTimer);
        this.membershipChartProgressTimer = window.setTimeout(() => {
            $("#cepChartProgressBar").show();
        }, 300);
    }

    hideMembershipProgressBar() {
        window.clearTimeout(this.membershipChartProgressTimer);
        $("#cepChartProgressBar").hide();
    }

    showColorSchemeProgressBar() {
        window.clearTimeout(this.colorSchemeChartProgressTimer);
        this.colorSchemeChartProgressTimer = window.setTimeout(() => {
            $("#colorSchemeChartProgressBar").show();
        }, 300);
    }

    hideColorSchemeProgressBar() {
        window.clearTimeout(this.colorSchemeChartProgressTimer);
        $("#colorSchemeChartProgressBar").hide();
    }

    showStylesBoxProgressBar() {
        window.clearTimeout(this.stylesBoxChartProgressTimer);
        this.stylesBoxChartProgressTimer = window.setTimeout(() => {
            $("#stylesBoxChartProgressBar").show();
        }, 300);
    }

    hideStylesBoxProgressBar() {
        window.clearTimeout(this.stylesBoxChartProgressTimer);
        $("#stylesBoxChartProgressBar").hide();
    }

    showDefaultBricksProgressBar() {
        window.clearTimeout(this.defaultBricksChartProgressTimer);
        this.defaultBricksChartProgressTimer = window.setTimeout(() => {
            $("#defaultBricksChartProgressBar").show();
            $("#addedDefaultBricksChartProgressBar").show();
            $("#removedDefaultBricksChartProgressBar").show();
        }, 300);
    }

    hideDefaultBricksProgressBar() {
        window.clearTimeout(this.defaultBricksChartProgressTimer);
        $("#defaultBricksChartProgressBar").hide();
        $("#addedDefaultBricksChartProgressBar").hide();
        $("#removedDefaultBricksChartProgressBar").hide();
    }

    createPieChart(data, renderTo, datasets = null) {
        const total = data.reduce((acc, row) => acc + row.y, 0);
        if (datasets == null) {
            datasets = [
                {
                    label: "Users",
                    data: data.map(row => row.y),
                }
            ]
        }

        return new Chart.Chart(
            document.getElementById(renderTo) as HTMLCanvasElement,
            {
                type: 'doughnut',
                options: {
                    responsive: true,
                    maintainAspectRatio: true,
                    plugins: {
                        title: {
                            display: true,
                            text: "Source: User Settings"
                        },
                        legend: {
                            display: true,
                            position: "bottom",
                        },
                        tooltip: {
                            callbacks: {
                                title: context => {
                                    const result: string[] = [];

                                    for (var i = 0; i < context.length; i++) {
                                        let label = context[i].label.substring(0, context[i].label.lastIndexOf(':'));

                                        if (context[i].parsed !== null) {
                                            result.push(`${label}: ${(context[i].parsed / total * 100).toFixed(1)}%`);
                                        } else {
                                            result.push(label);
                                        }
                                    }

                                    return result;
                                },
                                label: context => {
                                    let label = context.dataset.label || '';
                                    if (label) {
                                        label += ": ";
                                    }

                                    if (context.parsed !== null) {
                                        label += `${context.parsed} out of ${total}`;
                                    }

                                    return label;
                                }
                            }
                        }
                    }
                },
                data: {
                    labels: data.map(row => `${row.name}: ${row.y}`),
                    datasets: datasets
                }
            });
    }

    createBarChart(data, renderTo) {
        return new Chart.Chart(
            document.getElementById(renderTo) as HTMLCanvasElement,
            {
                type: 'bar',
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    indexAxis: 'y',
                    plugins: {
                        title: {
                            display: true,
                            text: "Source: User Settings"
                        },
                        legend: {
                            display: true,
                            position: "bottom"
                        }
                    }
                },
                data: {
                    labels: data.categories,
                    datasets: data.series.map(s => ({
                        label: s.name,
                        data: s.data
                    }))
                }
            }
        );
    }

    getBackgroundColor(name): string {
        switch (name) {
            case "Default":
                return "#0072c6";
            case "Marine":
                return "#16437a";
            case "Purple":
                return "#910091";
            case "Plum":
                return "#9a263e";
            case "Coffee":
                return "#9e4215";
        }

        return "";
    }
}

export = CustomisationStatistics
