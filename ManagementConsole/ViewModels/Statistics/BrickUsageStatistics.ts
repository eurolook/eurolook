import { PageViewModel } from "../PageViewModel"
declare var Chart: typeof import("../../node_modules/chart.js/auto");

class BrickUsageStatistics extends PageViewModel {
    brickUsageChart = null;
    updateBrickUsageRequest = null;

    brickUsageHistoryChart = null;
    updateBrickUsageHistoryRequest: JQueryXHR = null;

    brickUsageHistoryProgressTimer: number = null;
    brickUsageProgressTimer: number = null;

    options = ko.observableArray(null);
    selectedOptions = ko.observableArray(null);

    monthsFilters = ko.observableArray([
        {
            displayName: "3 Months",
            value: 3
        },
        {
            displayName: "6 Months",
            value: 4
        },
        {
            displayName: "1 Year",
            value: 5
        },
        {
            displayName: "2 Years",
            value: 6
        },
        {
            displayName: "3 Years",
            value: 7
        },
        {
            displayName: "Complete History",
            value: 99
        }
    ]);
    selectedMonthFilter = ko.observable(2);

    timespanFilters = ko.observableArray([
        {
            displayName: "Last Month (today minus 1 month)",
            value: 2
        },
        {
            displayName: "Last 3 Months (today minus 3 months)",
            value: 3
        },
        {
            displayName: "Last 6 Months (today minus 6 months)",
            value: 4
        }
    ]);
    selectedTimespanFilter = ko.observable(3);

    defaultDocumentSelectionFilter = {
        displayName: "All",
        guid: null,
        isCategory: false
    };
    documentTypes = ko.observableArray([this.defaultDocumentSelectionFilter]);

    selectedDocumentTypeHistoryFilter = ko.observable(null);
    selectedDocumentTypeFilter = ko.observable(null);

    constructor(baseUrl, isReadOnly: string, chart) {
        super(baseUrl, "/Statistics", isReadOnly);

        Chart = chart;

        this.selectedOptions.subscribe(function (_) {
            this.updateBrickUsageHistoryChart();
        }.bind(this));

        this.selectedMonthFilter.subscribe(function (_) {
            this.updateBrickUsageHistoryChart();
        }.bind(this));

        this.selectedTimespanFilter.subscribe(function (_) {
            this.updateBrickUsageChart();
        }.bind(this));

        this.selectedDocumentTypeHistoryFilter.subscribe(function (_) {
            this.updateBrickUsageHistoryChart();
        }.bind(this));

        this.selectedDocumentTypeFilter.subscribe(function (_) {
            this.updateBrickUsageChart();
        }.bind(this));

        this.updateBrickUsageChart();

        this.bricksFilterUpdateRequest();

        this.documentTypeFilterUpdateRequest();
    }

    bricksFilterUpdateRequest() {
        this.getJson(
            this.baseUrl() + "/Statistics/bricknamesbycategory/",
            (result) => {
                this.options(result);
            },
            this);
    }

    documentTypeFilterUpdateRequest() {
        this.getJson(
            this.baseUrl() + "/Statistics/documenttypesbycategory/",
            (result) => {
                for (let category of result) {
                    this.documentTypes.push({
                        displayName: category.label,
                        guid: null,
                        isCategory: true
                    });
                    for (let type of category.options) {
                        this.documentTypes.push({
                            displayName: type.name,
                            guid: type.value,
                            isCategory: false
                        });
                    }
                }
            },            
            this);
    }

    updateBrickUsageHistoryChart() {
        this.showBrickUsageHistoryProgressBar();
        this.destroyHistoryChart();

        if (this.updateBrickUsageHistoryRequest && this.updateBrickUsageHistoryRequest.readyState < 4) {
            this.updateBrickUsageHistoryRequest.abort();
        }

        // no need to fire a request if no brick is selected
        if (this.selectedOptions() === null || this.selectedOptions().length === 0) {
            this.setBrickUsageHistoryForNoData();
            this.hideBrickUsageHistoryProgressBar();
            return;
        }

        // open a new request
        this.updateBrickUsageHistoryRequest = this.postJson(
            this.baseUrl() + "/Statistics/brickusageovertime/" + this.selectedMonthFilter(),
            ko.toJSON(
                {
                    bricksToInclude: this.selectedOptions(),
                    documentTypeToInclude: this.selectedDocumentTypeHistoryFilter()
                }
            ),
            (result) => {
                this.clearBrickUsageHistoryForNoData();
                this.setBrickUsageHistoryChart(result);
                this.hideBrickUsageHistoryProgressBar();
            },
            this);
    };

    setBrickUsageHistoryForNoData() {
        document.getElementById("brickUsageHistoryChartNoData").innerHTML = "Select the bricks you want to see the statistics for";
    }

    clearBrickUsageHistoryForNoData() {
        document.getElementById("brickUsageHistoryChartNoData").innerHTML = "";
    }

    setBrickUsageHistoryChart(data) {
        this.brickUsageHistoryChart = new Chart.Chart(
            document.getElementById("brickUsageHistoryChart") as HTMLCanvasElement,
            {
                type: 'line',
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        y: {
                            title: {
                                display: true,
                                text: "Bricks and Tools used"
                            },
                            beginAtZero: true,
                            suggestedMin: 0,
                            ticks: {
                                precision: 0
                            }
                        }
                    },
                    plugins: {
                        title: {
                            display: false,
                        },
                        subtitle: {
                            display: true,
                            text: "Source: User Experience Program"
                        },
                        legend: {
                            display: true,
                            position: "bottom",
                        }
                    }
                },
                data: {
                    labels: data.categories,
                    datasets: data.series.map(s => ({
                        label: s.name,
                        data: s.data
                    }))
                }
            }
        );
    }

    updateBrickUsageChart() {
        this.showBrickUsageProgressBar();
        this.destroyChart();

        if (this.updateBrickUsageRequest && this.updateBrickUsageRequest.readyState < 4) {
            this.updateBrickUsageRequest.abort();
        }

        this.updateBrickUsageRequest = this.getJson(
            this.baseUrl() + "/statistics/brickusagedata/" + this.selectedTimespanFilter() + "?documentTypeToInclude=" + this.selectedDocumentTypeFilter(),
            (result) => {
                this.setBrickUsageChart(result);
                this.hideBrickUsageProgressBar();
            },
            this);
    }

    setBrickUsageChart(data) {
        let container = document.getElementById("brickUsageChartContainer");
        container.style.cssText = `height: ${128 + data.categories.length * 32}px`;
        this.brickUsageChart = new Chart.Chart(
            document.getElementById("brickUsageChart") as HTMLCanvasElement,
            {
                type: 'bar',
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    indexAxis: 'y',
                    scales: {
                        x: {
                            stacked: true,
                            title: {
                                display: true,
                                text: "Insertion Count"
                            },
                            position: "top"
                        },
                        y: {
                            stacked: true
                        }
                    },
                    plugins: {
                        title: {
                            display: false,
                        },
                        subtitle: {
                            display: true,
                            text: "Source: User Experience Program"
                        },
                        legend: {
                            display: true,
                            position: "bottom",
                            onHover(_, legendItem) {
                                const elementList = [];
                                for (let i = 0; i < this.chart.getDatasetMeta(legendItem.datasetIndex).data.length; i++) {
                                    elementList.push({
                                        datasetIndex: legendItem.datasetIndex,
                                        index: i,
                                    });
                                }
                                this.chart.setActiveElements(elementList);
                                this.chart.update();
                            },
                        }
                    },
                },
                data: {
                    labels: data.categories,
                    datasets: data.series.map(s => ({
                        label: s.name,
                        data: s.data,
                        hoverBorderWidth: 2,
                    }))
                }
            }
        );
    }

    showBrickUsageHistoryProgressBar() {
        window.clearTimeout(this.brickUsageHistoryProgressTimer);
        this.brickUsageHistoryProgressTimer = window.setTimeout(() => {
            $("#brickUsageHistoryProgressBar").show();
        }, 300);
    };

    hideBrickUsageHistoryProgressBar () {
        window.clearTimeout(this.brickUsageHistoryProgressTimer);
        $("#brickUsageHistoryProgressBar").hide();
    };

    destroyHistoryChart() {
        if (this.brickUsageHistoryChart) {
            this.brickUsageHistoryChart.destroy();
        }
    }

    showBrickUsageProgressBar() {
        window.clearTimeout(this.brickUsageProgressTimer);
        this.brickUsageProgressTimer = window.setTimeout(() => {
            $("#brickUsageProgressBar").show();
        }, 300);
    }

    hideBrickUsageProgressBar() {
        window.clearTimeout(this.brickUsageProgressTimer);
        $("#brickUsageProgressBar").hide();
    }

    destroyChart() {
        if (this.brickUsageChart) {
            this.brickUsageChart.destroy();
        }
    }
}

export = BrickUsageStatistics
