import { PageViewModel } from "../PageViewModel";
declare var Chart: typeof import("../../node_modules/chart.js/auto");

class ActivityStatistics extends PageViewModel {
    activityDetailsChart = null;
    activityOverviewChart = null;

    activityDetailsRequest: JQueryXHR = null;
    activityOverviewRequest: JQueryXHR = null;

    activityOverviewProgressTimer: number = null;
    activityDetailsProgressTimer: number = null;

    selectedActivityOverviewFilter = ko.observable(3);
    selectedActivityDetailsFilter = ko.observable(0);

    monthsFilters = ko.observableArray([{
            displayName: "3 Months",
            value: 3
        }, {
            displayName: "6 Months",
            value: 4
        }, {
            displayName: "1 Year",
            value: 5
        }, {
            displayName: "2 Years",
            value: 6
        }, {
            displayName: "3 Years",
            value: 7
        }, {
            displayName: "Complete History",
            value: 99
        }]);

    timespanFilters = ko.observableArray([{
            displayName: "Last Week (yesterday minus 7 days)",
            value: 0
        }, {
            displayName: "Last 2 Weeks (yesterday minus 14 days)",
            value: 1
        }, {
            displayName: "Last Month (yesterday minus 1 month)",
            value: 2
        }, {
            displayName: "Last 3 Months (yesterday minus 3 months)",
            value: 3
        }]);


    constructor(baseUrl, isReadOnly: string, chart) {
        super(baseUrl, "/Statistics", isReadOnly);

        Chart = chart;

        this.selectedActivityOverviewFilter.subscribe(function (_) {
            this.updateActivityOverviewChart();
        }.bind(this));

        this.selectedActivityDetailsFilter.subscribe(function (_) {
            this.updateActivityDetailsChart();
        }.bind(this));

        this.updateActivityOverviewChart();
        this.updateActivityDetailsChart();
    }

    updateActivityOverviewChart() {
        this.showActivityOverviewProgressBar();
        this.destroyOverviewChart();

        if (this.activityOverviewRequest && this.activityOverviewRequest.readyState < 4) {
            this.activityOverviewRequest.abort();
        }

        this.activityOverviewRequest = this.getJson(
            this.baseUrl() + "/Statistics/activityoverview/" + this.selectedActivityOverviewFilter(),
            (result) => {
                this.hideActivityOverviewProgressBar();
                this.activityOverviewChart = new Chart.Chart(
                    document.getElementById("activityOverviewChart") as HTMLCanvasElement,
                    {
                        type: 'bar',
                        options: {
                            responsive: true,
                            maintainAspectRatio: false,
                            indexAxis: 'x',
                            scales: {
                                y: {
                                    title: {
                                        display: true,
                                        text: "Count"
                                    },
                                    position: "left"
                                },
                            },
                            plugins: {
                                title: {
                                    display: false,
                                },
                                subtitle: {
                                    display: true,
                                    text: "Source: User Experience Program"
                                },
                                legend: {
                                    display: true,
                                    position: "bottom",
                                }
                            },
                        },
                        data: {
                            labels: result.categories,
                            datasets: result.series.map(s => ({
                                label: s.name,
                                data: s.data,
                            }))
                        }
                    }
                );
            },
            this);
    }

    updateActivityDetailsChart() {
        this.showActivityDetailsProgressBar();
        this.destroyDetailsChart();

        if (this.activityDetailsRequest && this.activityDetailsRequest.readyState < 4) {
            this.activityDetailsRequest.abort();
        }

        this.activityDetailsRequest = this.getJson(
            this.baseUrl() + "/Statistics/activitydetails/" + this.selectedActivityDetailsFilter(),
            (result) => {
                this.hideActivityDetailsProgressBar();
                this.activityDetailsChart = new Chart.Chart(
                    document.getElementById("activityDetailsChart") as HTMLCanvasElement,
                    {
                        type: 'bar',
                        options: {
                            responsive: true,
                            maintainAspectRatio: false,
                            indexAxis: 'x',
                            scales: {
                                y: {
                                    title: {
                                        display: true,
                                        text: "Count"
                                    },
                                    position: "left"
                                },
                            },
                            plugins: {
                                title: {
                                    display: false,
                                },
                                subtitle: {
                                    display: true,
                                    text: "Source: User Experience Program"
                                },
                                legend: {
                                    display: true,
                                    position: "bottom"
                                }
                            },
                        },
                        data: {
                            labels: result.categories,
                            datasets: result.series.map(s => ({
                                label: s.name,
                                data: s.data
                            }))
                        }
                    }
                );
            },
            this);
    }

    showActivityOverviewProgressBar() {
        window.clearTimeout(this.activityOverviewProgressTimer);
        this.activityOverviewProgressTimer = window.setTimeout(() => {
            $("#activityOverviewProgressBar").show();
        }, 300);
    }

    hideActivityOverviewProgressBar() {
        window.clearTimeout(this.activityOverviewProgressTimer);
        $("#activityOverviewProgressBar").hide();
    }

    showActivityDetailsProgressBar() {
        window.clearTimeout(this.activityDetailsProgressTimer);
        this.activityDetailsProgressTimer = window.setTimeout(() => {
            $("#activityDetailsProgressBar").show();
        }, 300);
    }

    hideActivityDetailsProgressBar() {
        window.clearTimeout(this.activityDetailsProgressTimer);
        $("#activityDetailsProgressBar").hide();
    }

    destroyOverviewChart() {
        if (this.activityOverviewChart) {
            this.activityOverviewChart.destroy();
        }
    }

    destroyDetailsChart() { 
        if (this.activityDetailsChart) {
            this.activityDetailsChart.destroy();
        }
    }
}

export = ActivityStatistics
