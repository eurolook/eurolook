import { PageViewModel } from "../PageViewModel"
declare var Chart: typeof import("../../node_modules/chart.js/auto");

class HelpStatistics extends PageViewModel {
    helpPagesChart = null;
    helpPagesChartRequest: JQueryXHR = null;
    helpPagesChartProgressTimer: number = null;

    monthsFilters = ko.observableArray([
        {
            displayName: "Last Month (today minus 1 month)",
            value: 2
        },
        {
            displayName: "Last 3 Months (today minus 3 months)",
            value: 3
        },
        {
            displayName: "Last 6 Months (today minus 6 months)",
            value: 4
        },
        {
            displayName: "Last 12 Months (today minus 12 months)",
            value: 5
        },
        {
            displayName: "All Requests",
            value: 99
        }
    ]);

    selectedMonthsFilter = ko.observable(3);

    constructor(baseUrl, isReadOnly: string, chart) {
        super(baseUrl, "/Statistics", isReadOnly);

        Chart = chart;

        this.selectedMonthsFilter.subscribe(function (_) {
            this.updateHelpPagesChart();
        }.bind(this));

        this.updateHelpPagesChart();
    }

    updateHelpPagesChart() {
        this.showHelpPagesChartProgressBar();
        this.destroyChart();

        if (this.helpPagesChartRequest && this.helpPagesChartRequest.readyState < 4) {
            this.helpPagesChartRequest.abort();
        }

        this.helpPagesChartRequest = this.getJson(
            this.baseUrl() + "/Statistics/gethelprequests/" + this.selectedMonthsFilter(),
            (result) => {
                this.hideHelpPagesChartProgressBar();
                this.helpPagesChart = new Chart.Chart(
                    document.getElementById("helpPagesChart") as HTMLCanvasElement,
                    {
                        type: 'bar',
                        options: {
                            responsive: true,
                            maintainAspectRatio: false,
                            indexAxis: 'y',
                            plugins: {
                                title: {
                                    display: true,
                                    text: "Source: IIS Log Files"
                                },
                                legend: {
                                    display: false
                                }
                            },
                            scales: {
                                x: {
                                    title: {
                                        display: true,
                                        text: "Count",
                                        align: "center"
                                    }
                                }
                            }
                        },
                        data: {
                            labels: result.categories,
                            datasets: result.series.map(s => ({
                                label: s.name,
                                data: s.data
                            }))
                        }
                    }
                );
            },
            this);
    }

    showHelpPagesChartProgressBar() {
        window.clearTimeout(this.helpPagesChartProgressTimer);
        this.helpPagesChartProgressTimer = window.setTimeout(() => {
            $("#helpPagesChartProgressBar").show();
        }, 300);
    }

    hideHelpPagesChartProgressBar() {
        window.clearTimeout(this.helpPagesChartProgressTimer);
        $("#helpPagesChartProgressBar").hide();
    }

    destroyChart() {
        if (this.helpPagesChart) {
            this.helpPagesChart.destroy();
        }
    }
}

export = HelpStatistics
