import { type } from "jquery";
import { PageViewModel } from "../PageViewModel"
declare var Chart: typeof import("../../node_modules/chart.js/auto");

class CustomBricksStatistics extends PageViewModel {
    usageChart = null;
    usageChartRequest: JQueryXHR = null;
    usageChartProgressTimer: number = null;

    countPerUserChart = null;
    countPerUserChartRequest: JQueryXHR = null;
    countPerUserChartProgressTimer: number = null;

    sizePerUserChart = null;
    sizePerUserChartRequest: JQueryXHR = null;
    sizePerUserChartProgressTimer: number = null;

    monthsFilters = ko.observableArray([
            {
                displayName: "Last 3 Months (today minus 3 months)",
                value: 3
            },
            {
                displayName: "Last 6 Months (today minus 6 months)",
                value: 4
            },
            {
                displayName: "Last 12 Months (today minus 12 months)",
                value: 5
            },
            {
                displayName: "Complete History",
                value: 99
            }
        ]);
    selectedMonthsFilter = ko.observable(4);

    constructor(baseUrl, isReadOnly: string, chart) {
        super(baseUrl, "/Statistics", isReadOnly);

        Chart = chart;

        this.selectedMonthsFilter.subscribe(function (_) {
            this.updateCharts();
        }.bind(this));

        this.updateCharts();
    }

    updateCharts() {
        this.updateUsageChart();
        this.updateCountPerUserChart();
        this.updateSizePerUserChart();
    }

    updateUsageChart() {
        this.showUsageChartProgressBar();
        this.destroyUsageChart();

        if (this.usageChartRequest && this.usageChartRequest.readyState < 4) {
            this.usageChartRequest.abort();
        }

        this.usageChartRequest = this.getJson(
            this.baseUrl() + "/Statistics/GetCustomBricksUsage/" + this.selectedMonthsFilter(),
            (result) => {
                this.hideUsageChartProgressBar();
                const total = result.reduce((acc, row) => acc + row.y, 0);

                this.usageChart = new Chart.Chart(
                    document.getElementById("usageChart") as HTMLCanvasElement,
                    {
                        type: 'doughnut',
                        options: {
                            responsive: true,
                            maintainAspectRatio: true,
                            plugins: {
                                title: {
                                    display: true,
                                    text: "Source: User Settings"
                                },
                                legend: {
                                    display: true,
                                    position: "bottom"
                                },
                                tooltip: {
                                    callbacks: {
                                        title: context => {
                                            const result: string[] = [];

                                            for (var i = 0; i < context.length; i++) {
                                                let label = context[i].label.substring(0, context[i].label.lastIndexOf(':'));

                                                if (context[i].parsed !== null) {
                                                    result.push(`${label}: ${(context[i].parsed / total * 100).toFixed(1)}%`);
                                                } else {
                                                    result.push(label);
                                                }
                                            }

                                            return result;
                                        },
                                        label: context => {
                                            let label = context.dataset.label || '';
                                            if (label) {
                                                label += ": ";
                                            }

                                            if (context.parsed !== null) {
                                                label += `${context.parsed} out of ${total}`;
                                            }

                                            return label;
                                        }
                                    }
                                }
                            }
                        },
                        data: {
                            labels: result.map(row => `${row.name}: ${row.y}`),
                            datasets: [
                                {
                                    label: 'Users',
                                    data: result.map(row => row.y)
                                }
                            ]
                        }
                    });
            },
            this);
    }

    updateCountPerUserChart() {
        this.showCountPerUserChartProgressBar();
        this.destroyCountPerUserChart();

        if (this.countPerUserChartRequest && this.countPerUserChartRequest.readyState < 4) {
            this.countPerUserChartRequest.abort();
        }

        this.countPerUserChartRequest = this.getJson(
            this.baseUrl() + "/Statistics/GetCustomBricksCountPerUser/" + this.selectedMonthsFilter(),
            (result) => {
                this.hideCountPerUserChartProgressBar();
                this.countPerUserChart = new Chart.Chart(
                    document.getElementById("countPerUserChart") as HTMLCanvasElement,
                    {
                        type: 'line',
                        options: {
                            responsive: true,
                            maintainAspectRatio: false,
                            scales: {
                                y: {
                                    title: {
                                        display: true,
                                        text: "Users"
                                    },
                                    type: "logarithmic"
                                }
                            },
                            plugins: {
                                title: {
                                    display: false,
                                },
                                subtitle: {
                                    display: true,
                                    text: "Source: User Settings"
                                },
                                legend: {
                                    display: true,
                                    position: "bottom",
                                },
                                filler: {
                                    propagate: false,
                                }
                            }
                        },
                        data: {
                            labels: result.categories,
                            datasets: result.series.map(s => ({
                                label: s.name,
                                data: s.data,
                                fill: true
                            }))
                        }
                    });
            },
            this);

    }

    updateSizePerUserChart() {
        this.showSizePerUserChartProgressBar();
        this.destroySizePerUserChart();

        if (this.sizePerUserChartRequest && this.sizePerUserChartRequest.readyState < 4) {
            this.sizePerUserChartRequest.abort();
        }

        this.sizePerUserChartRequest = this.getJson(
            this.baseUrl() + "/Statistics/GetCustomBricksSizePerUser/" + this.selectedMonthsFilter(),
            (result) => {
                this.hideSizePerUserChartProgressBar();
                this.sizePerUserChart = new Chart.Chart(
                    document.getElementById("sizePerUserChart") as HTMLCanvasElement,
                    {
                        type: 'line',
                        options: {
                            responsive: true,
                            maintainAspectRatio: false,
                            scales: {
                                y: {
                                    title: {
                                        display: true,
                                        text: "Users"
                                    },
                                    ticks: {
                                        precision: 0
                                    }
                                }
                            },
                            plugins: {
                                title: {
                                    display: false,
                                },
                                subtitle: {
                                    display: true,
                                    text: "Source: User Settings"
                                },
                                legend: {
                                    display: true,
                                    position: "bottom",
                                },
                                filler: {
                                    propagate: false,
                                }
                            }
                        },
                        data: {
                            labels: result.categories,
                            datasets: result.series.map(s => ({
                                label: s.name,
                                data: s.data,
                                fill: true
                            }))
                        }
                    });
            },
            this);
    }

    showUsageChartProgressBar() {
        window.clearTimeout(this.usageChartProgressTimer);
        this.usageChartProgressTimer = window.setTimeout(() => {
            $("#initStatusChartProgressBar").show();
        }, 300);
    }

    hideUsageChartProgressBar() {
        window.clearTimeout(this.usageChartProgressTimer);
        $("#initStatusChartProgressBar").hide();
    }

    destroyUsageChart() {
        if (this.usageChart) {
            this.usageChart.destroy();
        }
    }

    showCountPerUserChartProgressBar() {
        window.clearTimeout(this.countPerUserChartProgressTimer);
        this.countPerUserChartProgressTimer = window.setTimeout(() => {
            $("#initStatusChartProgressBar").show();
        }, 300);
    }

    hideCountPerUserChartProgressBar() {
        window.clearTimeout(this.countPerUserChartProgressTimer);
        $("#initStatusChartProgressBar").hide();
    }

    destroyCountPerUserChart() {
        if (this.countPerUserChart) {
            this.countPerUserChart.destroy();
        }
    }

    showSizePerUserChartProgressBar() {
        window.clearTimeout(this.sizePerUserChartProgressTimer);
        this.sizePerUserChartProgressTimer = window.setTimeout(() => {
            $("#initStatusChartProgressBar").show();
        }, 300);
    }

    hideSizePerUserChartProgressBar() {
        window.clearTimeout(this.sizePerUserChartProgressTimer);
        $("#initStatusChartProgressBar").hide();
    }

    destroySizePerUserChart() {
        if (this.sizePerUserChart) {
            this.sizePerUserChart.destroy();
        }
    }
}

export = CustomBricksStatistics
