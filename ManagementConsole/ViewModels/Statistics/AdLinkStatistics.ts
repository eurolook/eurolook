import { PageViewModel } from "../PageViewModel"
declare var Chart: typeof import("../../node_modules/chart.js/auto");

class AdLinkStatistics extends PageViewModel {
    adLinkStatusChart = null;
    updateAdLinkStatusRequest: JQueryXHR = null;
    allDgs = ko.observableArray();
    selectedDg = ko.observable();

    adLinkStatusProgressTimer: number = null;

    constructor(baseUrl, isReadOnly: string, chart) {
        super(baseUrl, "/Statistics", isReadOnly);

        Chart = chart;

        this.selectedDg.subscribe(function (_) {
            this.updateAdLinkStatusChart();
        }.bind(this));

        this.loadDgs();
    }

    loadDgs() {
        this.getJson(
            this.baseUrl() + "/statistics/dglist",
            (result) => {
                this.allDgs(result.dgs);
                //this.selectedDg(this.allDgs()[0]);
            },
            this);
    }

    updateAdLinkStatusChart() {
        this.showAdLinkStatusProgressBar();
        this.destroyChart();

        if (this.updateAdLinkStatusRequest && this.updateAdLinkStatusRequest.readyState < 4) {
            this.updateAdLinkStatusRequest.abort();
        }

        this.updateAdLinkStatusRequest = this.getJson(
            this.baseUrl() + "/statistics/adlinkstatus/" + this.selectedDg().id,
            (result) => {
                this.hideAdLinkStatusProgressBar();
                this.adLinkStatusChart = new Chart.Chart(
                    document.getElementById("adLinkStatusChart") as HTMLCanvasElement,
                    {
                        type: 'bar',
                        options: {
                            responsive: true,
                            maintainAspectRatio: false,
                            indexAxis: 'y',
                            scales: {
                                x: {
                                    stacked: true
                                },
                                y: {
                                    stacked: true
                                }
                            },
                            plugins: {
                                title: {
                                    display: true,
                                    text: "Source: Author Data"
                                },
                                subtitle: {
                                    display: true,
                                    text: "ADLink Properties",
                                    position: "bottom"
                                },
                                legend: {
                                    display: true,
                                    position: "bottom",
                                    onHover(_, legendItem) {
                                        const elementList = [];
                                        for (let i = 0; i < this.chart.getDatasetMeta(legendItem.datasetIndex).data.length; i++) {
                                            elementList.push({
                                                datasetIndex: legendItem.datasetIndex,
                                                index: i,
                                            });
                                        }
                                        this.chart.setActiveElements(elementList);
                                        this.chart.update();
                                    },
                                }
                            }
                        },
                        data: {
                            labels: result.categories,
                            datasets: result.series.map(s => ({
                                label: s.name,
                                data: s.data,
                                hoverBorderWidth: 2,
                            }))
                        }
                    }
                );
            },
            this);
    }

    showAdLinkStatusProgressBar() {
        window.clearTimeout(this.adLinkStatusProgressTimer);
        this.adLinkStatusProgressTimer = window.setTimeout(() => {
            $("#adLinkStatusProgressBar").show();
        }, 300);
    }

    hideAdLinkStatusProgressBar() {
        window.clearTimeout(this.adLinkStatusProgressTimer);
        $("#adLinkStatusProgressBar").hide();
    }

    destroyChart() {
        if (this.adLinkStatusChart) {
            this.adLinkStatusChart.destroy();
        }
    }
}

export = AdLinkStatistics
