import { ActiveDataPoint } from "chart.js";
import { Chart } from "chart.js/dist/core/core.scale";
import { PageViewModel } from "../PageViewModel"
declare var Chart: typeof import("../../node_modules/chart.js/auto");

class DocumentUsageStatistics extends PageViewModel {
    documentCreationHistoryChart = null;
    documentCreationChart = null;
    documentCreationByDgChart = null;
    documentCreationByEntityChart = null;

    updateDocumentCreationHistoryRequest: JQueryXHR= null;
    updateDocumentCreationRequest: JQueryXHR= null;
    updateDocumentCreationByDgRequest: JQueryXHR= null;
    updateDocumentCreationByEntityRequest: JQueryXHR = null;

    documentCreationHistoryProgressTimer: number = null;
    documentCreationProgressTimer: number = null;
    documentCreationByEntityProgressTimer: number = null;
    documentCreationByDgProgressTimer: number = null;

    chartDataLimit = ko.observable(30);

    selectedFilterByDg = ko.observable(2);
    selectedFilterByEntity = ko.observable(2);
    selectedFilterHistory = ko.observable(3);
    selectedFilterByDocument = ko.observable(3);

    monthsFilters = ko.observableArray([{
        displayName: "3 Months",
        value: 3
    }, {
        displayName: "6 Months",
        value: 4
    }, {
        displayName: "1 Year",
        value: 5
    }, {
        displayName: "2 Years",
        value: 6
    }, {
        displayName: "3 Years",
        value: 7
    }, {
        displayName: "Complete History",
        value: 99
    }]);

    
    timespanFilters = ko.observableArray([{
        displayName: "Last Month (today minus 1 month)",
        value: 2
    }, {
        displayName: "Last 3 Months (today minus 3 months)",
        value: 3
    }, {
        displayName: "Last 6 Months (today minus 6 months)",
        value: 4
    }]);
    
    constructor(baseUrl, isReadOnly: string, chart) {
        super(baseUrl, "/Statistics", isReadOnly);

        Chart = chart;

        this.selectedFilterHistory.subscribe(function (_) {
            this.updateDocumentCreationHistoryChart();
        }.bind(this));

        this.selectedFilterByDocument.subscribe(function (_) {
            this.updateDocumentCreationChart();
        }.bind(this));

        this.selectedFilterByDg.subscribe(function (_) {
            this.updateDocumentCreationByDgChart();
        }.bind(this));

        this.selectedFilterByEntity.subscribe(function (_) {
            this.updateDocumentCreationByEntityChart();
        }.bind(this));

        this.updateDocumentCreationHistoryChart();
        this.updateDocumentCreationChart();
        this.updateDocumentCreationByEntityChart();
        this.updateDocumentCreationByDgChart();
    }

    updateDocumentCreationHistoryChart() {
        this.showDocumentCreationHistoryProgressBar();
        this.destroyDocumentCreationHistoryChart();

        if (this.updateDocumentCreationHistoryRequest && this.updateDocumentCreationHistoryRequest.readyState < 4) {
            this.updateDocumentCreationHistoryRequest.abort();
        }

        this.updateDocumentCreationHistoryRequest = this.getJson(
            this.baseUrl() + "/Statistics/documentcreationhistory/" + this.selectedFilterHistory(),
            (result) => {
                this.hideDocumentCreationHistoryProgressBar();

                this.documentCreationHistoryChart = new Chart.Chart(
                    document.getElementById("documentCreationHistoryChart") as HTMLCanvasElement,
                    {
                        type: 'line',
                        options: {
                            responsive: true,
                            maintainAspectRatio: false,
                            scales: {
                                y: {
                                    title: {
                                        display: true,
                                        text: "Documents created"
                                    },
                                    beginAtZero: true,
                                    suggestedMin: 0,
                                    ticks: {
                                        precision: 0
                                    }
                                }
                            },
                            plugins: {
                                title: {
                                    display: false,
                                },
                                subtitle: {
                                    display: true,
                                    text: "Source: User Experience Program"
                                },
                                legend: {
                                    display: true,
                                    position: "bottom",
                                }
                            }
                        },
                        data: {
                            labels: result.categories,
                            datasets: result.series.map(s => ({
                                label: s.name,
                                data: s.data
                            }))
                        }
                    }
                );
            },
            this);
    }

    updateDocumentCreationChart() {
        this.showDocumentCreationProgressBar();
        this.destroyDocumentCreationChart();

        if (this.updateDocumentCreationRequest && this.updateDocumentCreationRequest.readyState < 4) {
            this.updateDocumentCreationRequest.abort();
        }

        this.updateDocumentCreationRequest = this.getJson(
            this.baseUrl() + "/Statistics/documentcreation/" + this.selectedFilterByDocument(),
            (result) => {
                this.hideDocumentCreationProgressBar();
                this.documentCreationChart = this.createBarChart(result, "documentCreationChart");
            },
            this);
    }

    updateDocumentCreationByEntityChart() {
        this.showDocumentCreationByEntityProgressBar();
        this.destroyDocumentCreationByEntityChart();

        if (this.updateDocumentCreationByEntityRequest && this.updateDocumentCreationByEntityRequest.readyState < 4) {
            this.updateDocumentCreationByEntityRequest.abort();
        }

        this.updateDocumentCreationByEntityRequest = this.getJson(
            this.baseUrl() + "/Statistics/documentcreationbyentity/" + this.selectedFilterByEntity() + "?limit=" + this.chartDataLimit(),
            (result) => {
                this.hideDocumentCreationByEntityProgressBar();
                this.documentCreationByEntityChart = this.createBarChart(result, "documentCreationByEntityChart");
            },
            this);        
    }

    updateDocumentCreationByDgChart() {
        this.showDocumentCreationByDgProgressBar();
        this.destroyDocumentCreationByDgChart();

        if (this.updateDocumentCreationByDgRequest && this.updateDocumentCreationByDgRequest.readyState < 4) {
            this.updateDocumentCreationByDgRequest.abort();
        }

        this.updateDocumentCreationByDgRequest = this.getJson(
            this.baseUrl() + "/Statistics/documentcreationbydg/" + this.selectedFilterByDg() + "?limit=" + this.chartDataLimit(),
            (result) => {
                this.hideDocumentCreationByDgProgressBar();
                this.documentCreationByDgChart = this.createBarChart(result, "documentCreationByDgChart");
            },
            this); 
    }

    showDocumentCreationHistoryProgressBar() {
        window.clearTimeout(this.documentCreationHistoryProgressTimer);
        this.documentCreationHistoryProgressTimer = window.setTimeout(() => {
            $("#documentCreationHistoryProgressBar").show();
        }, 300);
    }

    hideDocumentCreationHistoryProgressBar() {
        window.clearTimeout(this.documentCreationHistoryProgressTimer);
        $("#documentCreationHistoryProgressBar").hide();
    }

    destroyDocumentCreationHistoryChart() {
        if (this.documentCreationHistoryChart) {
            this.documentCreationHistoryChart.destroy();
        }
    }

    showDocumentCreationProgressBar() {
        window.clearTimeout(this.documentCreationProgressTimer);
        this.documentCreationProgressTimer = window.setTimeout(() => {
            $("#documentCreationProgressBar").show(); }, 300);
    }

    hideDocumentCreationProgressBar() {
        window.clearTimeout(this.documentCreationProgressTimer);
        $("#documentCreationProgressBar").hide();
    }

    destroyDocumentCreationChart() {
        if (this.documentCreationChart) {
            this.documentCreationChart.destroy();
        }
    }

    showDocumentCreationByEntityProgressBar() {
        window.clearTimeout(this.documentCreationByEntityProgressTimer);
        this.documentCreationByEntityProgressTimer = window.setTimeout(() => {
            $("#documentCreationByEntityProgressBar").show(); }, 300);
    }

    hideDocumentCreationByEntityProgressBar() {
        window.clearTimeout(this.documentCreationByEntityProgressTimer);
        $("#documentCreationByEntityProgressBar").hide();
    }

    destroyDocumentCreationByEntityChart() {
        if (this.documentCreationByEntityChart) {
            this.documentCreationByEntityChart.destroy();
        }
    }

    showDocumentCreationByDgProgressBar() {
        window.clearTimeout(this.documentCreationByDgProgressTimer);
        this.documentCreationByDgProgressTimer = window.setTimeout(() => {
            $("#documentCreationByDgProgressBar").show(); }, 300);
    }

    hideDocumentCreationByDgProgressBar() {
        window.clearTimeout(this.documentCreationByDgProgressTimer);
        $("#documentCreationByDgProgressBar").hide();
    }

    destroyDocumentCreationByDgChart() {
        if (this.documentCreationByDgChart) {
            this.documentCreationByDgChart.destroy();
        }
    }

    createBarChart(result, canvasId) {
        var datasets = result.series.map(s => ({
            label: s.name,
            data: s.data,
            hoverBorderWidth: 2,
        }));

        return new Chart.Chart(
            document.getElementById(canvasId) as HTMLCanvasElement,
            {
                type: 'bar',
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    indexAxis: 'y',
                    scales: {
                        x: {
                            title: {
                                display: true,
                                text: "Creation Count"
                            },
                            position: "top",
                            stacked: true
                        },
                        y: {
                            stacked: true
                        }
                    },
                    plugins: {
                        title: {
                            display: false,
                        },
                        subtitle: {
                            display: true,
                            text: "Source: User Experience Program"
                        },
                        legend: {
                            display: true,
                            position: "bottom",
                            onHover(_, legendItem) {
                                const elementList = [];
                                for (let i = 0; i < this.chart.getDatasetMeta(legendItem.datasetIndex).data.length; i++) {
                                    elementList.push({
                                        datasetIndex: legendItem.datasetIndex,
                                        index: i,
                                    });
                                }
                                this.chart.setActiveElements(elementList);
                                this.chart.update();
                            },
                        },
                        tooltip: {
                            callbacks: {
                                label: function (context) {
                                    let label = context.dataset.label || '';
                                    if (label) {
                                        label += ": ";
                                    }

                                    var total = 0;
                                    for (var i = 0; i < datasets.length; i++) {
                                        total += datasets[i].data[context.dataIndex]
                                    }

                                    if (context.parsed !== null) {
                                        label += `${context.parsed.x} (of ${total})`;
                                    }

                                    return label;
                                },
                            }
                        }
                    },
                },
                data: {
                    labels: result.categories,
                    datasets: datasets
                }
            });
    }
}

export = DocumentUsageStatistics
