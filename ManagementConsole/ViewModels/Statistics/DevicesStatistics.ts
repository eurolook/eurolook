import { PageViewModel } from "../PageViewModel"
declare var Chart: typeof import("../../node_modules/chart.js/auto");

class DeviceStatistics extends PageViewModel {
    initStatusChart = null;
    versionsCountChart = null;
    versionsChart = null;
    deviceOwnersChart = null;

    updateInitStatusChartRequest: JQueryXHR = null;
    updateVersionsChartRequest: JQueryXHR = null;
    updateVersionsCountChartRequest: JQueryXHR = null;
    updateDeviceOwnersChartRequest: JQueryXHR = null;

    initStatusChartProgressTimer: number = null;
    versionsProgressTimer: number = null;
    versionsCountProgressTimer: number = null;
    deviceOwnersProgressTimer: number = null;

    selectedMonthsFilter = ko.observable(3);
    monthsFilters = ko.observableArray([
        {
            displayName: "Last Month (today minus 1 month)",
            value: 2
        },
        {
            displayName: "Last 3 Months (today minus 3 months)",
            value: 3
        },
        {
            displayName: "Last 6 Months (today minus 6 months)",
            value: 4
        },
        {
            displayName: "Last 12 Months (today minus 12 months)",
            value: 5
        },
        {
            displayName: "All devices",
            value: 99
        }
    ]);

    

    selectedDevicesFilter = ko.observable(1);
    devicesFilter = ko.observableArray([
        {
            displayName: "Excluding Terminal Servers",
            value: 1
        },
        {
            displayName: "Only Terminal Servers",
            value: 2
        },
        {
            displayName: "All",
            value: 99
        },
    ]);

    constructor(baseUrl, isReadOnly: string, chart) {
        super(baseUrl, "/Statistics", isReadOnly);

        Chart = chart;
        
        this.selectedMonthsFilter.subscribe(function (_) {
            this.updateCharts();
        }.bind(this));

        this.selectedDevicesFilter.subscribe(function (_) {
            this.updateCharts();
        }.bind(this));

        this.updateCharts();
    }

    async updateCharts() {
        this.showProgressBars();
        this.destroyCharts();

        if (this.updateVersionsCountChartRequest && this.updateVersionsCountChartRequest.readyState < 4) {
            this.updateVersionsCountChartRequest.abort();
        }

        this.updateVersionsCountChartRequest = this.getJson(
            this.baseUrl() + "/Statistics/versionscount/" + this.selectedMonthsFilter() + "?deviceFilter=" + this.selectedDevicesFilter(),
            (result) => {
                this.hideVersionsCountProgressBar();
                this.versionsCountChart = this.createPieChart(result, "versionsCountChart");
            },
            this);

        if (this.updateDeviceOwnersChartRequest && this.updateDeviceOwnersChartRequest.readyState < 4) {
            this.updateDeviceOwnersChartRequest.abort();
        }

        this.updateDeviceOwnersChartRequest = this.getJson(
            this.baseUrl() + "/Statistics/deviceowners/" + this.selectedMonthsFilter() + "?deviceFilter=" + this.selectedDevicesFilter(),
            (result) => {
                this.hideDeviceOwnersProgressBar();
                this.deviceOwnersChart = this.createPieChart(result, "deviceOwnersChart");
            },
            this);

        if (this.updateInitStatusChartRequest && this.updateInitStatusChartRequest.readyState < 4) {
            this.updateInitStatusChartRequest.abort();
        }

        this.updateInitStatusChartRequest = this.getJson(
            this.baseUrl() + "/Statistics/initstatus/" + this.selectedMonthsFilter() + "?deviceFilter=" + this.selectedDevicesFilter(),
            (result) => {
                this.hideInitStatusProgressBar();
                this.initStatusChart = this.createPieChart(result, "initStatusChart");
            },
            this);

        if (this.updateVersionsChartRequest && this.updateVersionsChartRequest.readyState < 4) {
            this.updateVersionsChartRequest.abort();
        }

        this.updateVersionsChartRequest = this.getJson(
            this.baseUrl() + "/Statistics/versionusage/" + this.selectedMonthsFilter() + "?deviceFilter=" + this.selectedDevicesFilter(),
            (result) => {
                this.hideVersionsProgressBar();
                this.versionsChart = new Chart.Chart(
                    document.getElementById("versionsChart") as HTMLCanvasElement,
                    {
                        type: 'bar',
                        options: {
                            responsive: true,
                            maintainAspectRatio: false,
                            indexAxis: 'y',
                            scales: {
                                x: {
                                    stacked: true
                                },
                                y: {
                                    stacked: true
                                }
                            },
                            plugins: {
                                title: {
                                    display: true,
                                    text: "Source: Device Settings"
                                },
                                legend: {
                                    display: true,
                                    position: "bottom"
                                }
                            }
                        },
                        data: {
                            labels: result.categories,
                            datasets: result.series.map(s => ({
                                label: s.name,
                                data: s.data
                            }))
                        }
                    }
                );
            },
            this);
    }

    showProgressBars() {
        this.showVersionsProgressBar();
        this.showVersionsCountProgressBar();
        this.showDeviceOwnersProgressBar();
        this.showInitStatusProgressBar();
    }

    destroyCharts() {
        if (this.versionsChart != null) {
            this.versionsChart.destroy();
        }
        if (this.versionsCountChart) {
            this.versionsCountChart.destroy();
        }
        if (this.deviceOwnersChart) {
            this.deviceOwnersChart.destroy();
        }
        if (this.initStatusChart) {
            this.initStatusChart.destroy();
        }
    }

    showVersionsCountProgressBar() {
        window.clearTimeout(this.versionsCountProgressTimer);
        this.versionsCountProgressTimer = window.setTimeout(() => {
            $("#versionsCountProgressBar").show();
        }, 300);
    }

    hideVersionsCountProgressBar() {
        window.clearTimeout(this.versionsCountProgressTimer);
        $("#versionsCountProgressBar").hide();
    }

    showVersionsProgressBar() {
        window.clearTimeout(this.versionsProgressTimer);
        this.versionsProgressTimer = window.setTimeout(() => {
            $("#versionsProgressBar").show();
        }, 300);
    }

    hideVersionsProgressBar() {
        window.clearTimeout(this.versionsProgressTimer);
        $("#versionsProgressBar").hide();
    }

    showDeviceOwnersProgressBar() {
        window.clearTimeout(this.deviceOwnersProgressTimer);
        this.deviceOwnersProgressTimer = window.setTimeout(() => {
            $("#deviceOwnersProgressBar").show();
        }, 300);
    }

    hideDeviceOwnersProgressBar() {
        window.clearTimeout(this.deviceOwnersProgressTimer);
        $("#deviceOwnersProgressBar").hide();
    }

    showInitStatusProgressBar() {
        window.clearTimeout(this.initStatusChartProgressTimer);
        this.initStatusChartProgressTimer = window.setTimeout(() => {
            $("#initStatusChartProgressBar").show();
        }, 300);
    }

    hideInitStatusProgressBar() {
        window.clearTimeout(this.initStatusChartProgressTimer);
        $("#initStatusChartProgressBar").hide();
    }

    createPieChart(data, renderTo, datasets = null) {
        const total = data.reduce((acc, row) => acc + row.y, 0);
        if (datasets == null) {
            datasets = [
                {
                    label: "Devices",
                    data: data.map(row => row.y),
                }
            ]
        }

        return new Chart.Chart(
            document.getElementById(renderTo) as HTMLCanvasElement,
            {
                type: 'doughnut',
                options: {
                    responsive: true,
                    maintainAspectRatio: true,
                    plugins: {
                        title: {
                            display: true,
                            text: "Source: Device Settings"
                        },
                        legend: {
                            display: true,
                            position: "bottom",
                        },
                        tooltip: {
                            callbacks: {
                                title: context => {
                                    const result: string[] = [];

                                    for (var i = 0; i < context.length; i++) {
                                        let label = context[i].label.substring(0, context[i].label.lastIndexOf(':'));

                                        if (context[i].parsed !== null) {
                                            result.push(`${label}: ${(context[i].parsed / total * 100).toFixed(1)}%`);
                                        } else {
                                            result.push(label);
                                        }
                                    }

                                    return result;
                                },
                                label: context => {
                                    let label = context.dataset.label || '';
                                    if (label) {
                                        label += ": ";
                                    }

                                    if (context.parsed !== null) {
                                        label += `${context.parsed} out of ${total}`;
                                    }

                                    return label;
                                }
                            }
                        }
                    }
                },
                data: {
                    labels: data.map(row => `${row.name}: ${row.y}`),
                    datasets: datasets
                }
            });
    }

}

export = DeviceStatistics
