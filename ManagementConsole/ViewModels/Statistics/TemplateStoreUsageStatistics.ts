import { PageViewModel } from "../PageViewModel"
declare var Chart: typeof import("../../node_modules/chart.js/auto");

class TemplateStoreUsageStatistics extends PageViewModel {
    templateCreationChart = null;
    templateCreationDMChart = null;
    templateCreationDGChart = null;
    
    updateTemplateCreationRequest: JQueryXHR = null;
    updateTemplateCreationDMRequest: JQueryXHR = null;
    updateTemplateCreationDGRequest: JQueryXHR = null;

    templateCreationProgressTimer: number = null;
    templateCreationDMProgressTimer: number = null;
    templateCreationDGProgressTimer: number = null;
    
    chartDataLimit = ko.observable(30); 

    monthsFilters = ko.observableArray([{
        displayName: "3 Months",
        value: 3
    }, {
        displayName: "6 Months",
        value: 4
    }, {
        displayName: "1 Year",
        value: 5
    }, {
        displayName: "2 Years",
        value: 6
    }, {
        displayName: "3 Years",
        value: 7
    }, {
        displayName: "Complete History",
        value: 99
    }]);

    timespanFilters = ko.observableArray([{
        displayName: "Last Month (today minus 1 month)",
        value: 2
    }, {
        displayName: "Last 3 Months (today minus 3 months)",
        value: 3
    }, {
        displayName: "Last 6 Months (today minus 6 months)",
        value: 4
    }, {
        displayName: "Last Year (today minus 12 months)",
        value: 5
    }]);

    selectedFilterByDocument = ko.observable(3);

    selectedFilterByDocumentModel = ko.observable(3);

    selectedFilterByDG = ko.observable(3);

    constructor(baseUrl, isReadOnly: string, chart) {
        super(baseUrl, "/Statistics", isReadOnly);

        Chart = chart;

        this.selectedFilterByDocument.subscribe(function (_) {
            this.updateTemplateCreationChart();
        }.bind(this));

        this.selectedFilterByDocumentModel.subscribe(function (_) {
            this.updateTemplateCreationDMChart();
        }.bind(this));

        this.selectedFilterByDG.subscribe(function (_) {
            this.updateTemplateCreationDGChart();
        }.bind(this));

        this.updateTemplateCreationChart();
        this.updateTemplateCreationDMChart();
        this.updateTemplateCreationDGChart();
    }

    updateTemplateCreationChart() {
        this.showTemplateCreationProgressBar();
        this.destroyTemplateCreationChart();

        if (this.updateTemplateCreationRequest && this.updateTemplateCreationRequest.readyState < 4) {
            this.updateTemplateCreationRequest.abort();
        }

        this.updateTemplateCreationRequest = this.getJson(
            this.baseUrl() + "/Statistics/mostusedtemplates/" + this.selectedFilterByDocument(),
            (result) => {
                this.hideTemplateCreationProgressBar();
                this.templateCreationChart = this.createBarChart(result, "templateCreationChart");
            },
            this);
    }

    updateTemplateCreationDMChart() {
        this.showTemplateCreationDMProgressBar();
        this.destroyTemplateCreationDMChart();

        if (this.updateTemplateCreationDMRequest && this.updateTemplateCreationDMRequest.readyState < 4) {
            this.updateTemplateCreationDMRequest.abort();
        }

        this.updateTemplateCreationDMRequest = this.getJson(
            this.baseUrl() + "/Statistics/templatesbymodel/" + this.selectedFilterByDocumentModel(),
            (result) => {
                this.hideTemplateCreationDMProgressBar();
                this.templateCreationDMChart = this.createBarChart(result, "templateCreationDMChart");
            },
            this);
    }

    updateTemplateCreationDGChart() {
        this.showTemplateCreationDGProgressBar();
        this.destroyTemplateCreationDGChart();

        if (this.updateTemplateCreationDGRequest && this.updateTemplateCreationDGRequest.readyState < 4) {
            this.updateTemplateCreationDGRequest.abort();
        }

        this.updateTemplateCreationDGRequest = this.getJson(
            this.baseUrl() + "/Statistics/templatesbydg/" + this.selectedFilterByDG(),
            (result) => {
                this.hideTemplateCreationDGProgressBar();
                this.templateCreationDGChart = this.createBarChart(result, "templateCreationDGChart");
            },
            this);
    }

    showTemplateCreationProgressBar() {
        window.clearTimeout(this.templateCreationProgressTimer);
        this.templateCreationProgressTimer = window.setTimeout(() => {
            $("#templateCreationProgressBar").show();
        }, 300);
    };

    hideTemplateCreationProgressBar() {
        window.clearTimeout(this.templateCreationProgressTimer);
        $("#templateCreationProgressBar").hide();
    };

    destroyTemplateCreationChart() {
        if (this.templateCreationChart) {
            this.templateCreationChart.destroy();
        }
    }

    showTemplateCreationDMProgressBar() {
        window.clearTimeout(this.templateCreationDMProgressTimer);
        this.templateCreationDMProgressTimer = window.setTimeout(() => {
            $("#templateCreationDMProgressBar").show(); }, 300);
    };

    hideTemplateCreationDMProgressBar() {
        window.clearTimeout(this.templateCreationDMProgressTimer);
        $("#templateCreationDMProgressBar").hide();
    };

    destroyTemplateCreationDMChart() {
        if (this.templateCreationDMChart) {
            this.templateCreationDMChart.destroy();
        }
    }

    showTemplateCreationDGProgressBar() {
        window.clearTimeout(this.templateCreationDGProgressTimer);
        this.templateCreationDGProgressTimer = window.setTimeout(() => {
            $("#templateCreationDGProgressBar").show(); }, 300);
    };

    destroyTemplateCreationDGChart() {
        if (this.templateCreationDGChart) {
            this.templateCreationDGChart.destroy();
        }
    }

    hideTemplateCreationDGProgressBar() {
        window.clearTimeout(this.templateCreationDGProgressTimer);
        $("#templateCreationDGProgressBar").hide();
    };

    createBarChart(result, canvasId) {
        var datasets = result.series.map(s => ({
            label: s.name,
            data: s.data,
            hoverBorderWidth: 2,
        }));

        return new Chart.Chart(
            document.getElementById(canvasId) as HTMLCanvasElement,
            {
                type: 'bar',
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    indexAxis: 'y',
                    scales: {
                        x: {
                            title: {
                                display: true,
                                text: "Creation Count"
                            },
                            position: "top",
                            stacked: true
                        },
                        y: {
                            stacked: true
                        }
                    },
                    plugins: {
                        title: {
                            display: false,
                        },
                        subtitle: {
                            display: true,
                            text: "Source: User Experience Program"
                        },
                        legend: {
                            display: true,
                            position: "bottom",
                            onHover(_, legendItem) {
                                const elementList = [];
                                for (let i = 0; i < this.chart.getDatasetMeta(legendItem.datasetIndex).data.length; i++) {
                                    elementList.push({
                                        datasetIndex: legendItem.datasetIndex,
                                        index: i,
                                    });
                                }
                                this.chart.setActiveElements(elementList);
                                this.chart.update();
                            },
                        },
                        tooltip: {
                            callbacks: {
                                label: function (context) {
                                    let label = context.dataset.label || '';
                                    if (label) {
                                        label += ": ";
                                    }

                                    var total = 0;
                                    for (var i = 0; i < datasets.length; i++) {
                                        total += datasets[i].data[context.dataIndex]
                                    }

                                    if (context.parsed !== null) {
                                        label += `${context.parsed.x} (of ${total})`;
                                    }

                                    return label;
                                },
                            }
                        }
                    },
                },
                data: {
                    labels: result.categories,
                    datasets: datasets,
                }
            });
    }
}

export = TemplateStoreUsageStatistics
