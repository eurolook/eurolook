import { PaginatedTableViewModel } from "PaginatedTableViewModel"
import { BrickViewModel } from "Bricks/BrickViewModel"

class BricksIndexViewModel extends PaginatedTableViewModel {

    bricks: KnockoutObservableArray<BrickViewModel> = ko.observableArray([]);
    selectedBricks: KnockoutObservableArray<BrickViewModel> = ko.observableArray([]);
    searchBricksResults: KnockoutObservableArray<BrickViewModel> = ko.observableArray([]);
    searchBricksToDuplicateResult: KnockoutObservableArray<BrickViewModel> = ko.observableArray([]);
    sendBricks: KnockoutObservableArray<BrickViewModel> = ko.observableArray([]);
    // ReSharper disable SuspiciousThisUsage
    isSelectedAll = ko.pureComputed({
        read: function () {
            return this.selectedBricks().length === this.bricks().length;
        },
        write: function (value) {
            this.selectedBricks(value ? this.bricks().slice(0) : []);
        },
        owner: this
    });
    // ReSharper restore SuspiciousThisUsage
    newBrickName = ko.observable("");
    newBrickNameDuplicate = ko.observable("");
    newBrickValidated = ko.observable(false);
    brickClasses = ko.observableArray([
        { name: "Content Brick", value: "Eurolook.Data.Models.ContentBrick" },
        { name: "Dynamic Brick", value: "Eurolook.Data.Models.DynamicBrick" },
        { name: "Command Brick", value: "Eurolook.Data.Models.CommandBrick" }
    ]);
    newBrickClass = ko.observable("");
    newBrickClassDuplicate = ko.observable("");
    deletingBrick: BrickViewModel = null;
    selectedBrick: KnockoutObservable<BrickViewModel> = ko.observable();
    searchInputDuplicate = ko.observable("");
    deleteModalTitle = ko.observable("");
    searchRequest: JQueryXHR;
    includeGroups = ko.observable(false);
    includeTexts = ko.observable(false);
    includeResources = ko.observable(false);
    includeUserGroups = ko.observable(false);

    validateNewBrick() {
        var result = false;
        if (this.selectedBrick()) {
            result = true;
        }
        this.newBrickValidated(result);
    }
    constructor(baseUrl, isReadOnly: string) {
        super(baseUrl, "/Bricks", isReadOnly);
        this.pageSizes = ko.observableArray([20, 50, 100, 500]);
        if (!this.orderProperty()) {
            this.orderProperty("name");
            this.orderAscending(true);
        }
        this.loadPageData();
        $("#SearchBox").focus();
        this.selectedBrick.subscribe(() => this.validateNewBrick());
        this.validateNewBrick();
    }

    loadPageData() {
        if (this.searchRequest && this.searchRequest.readyState < 4) {
            this.searchRequest.abort();
        }
        this.searchInputDuplicate.subscribe(this.searchInputDuplicateChanged, this);
        this.showProgressSpinner();
        this.searchRequest =  this.getJson(
            this.getPageUrl(this.baseUrl() + "/Bricks"),
            this.loadPageDataSucceeded,
            this);
    }

    loadPageDataSucceeded(result) {
        this.selectedBricks.removeAll();
        this.bricks.removeAll();
        if (result.success === true) {
            const page = result.model;
            for (let brick of page.data) {
                this.bricks.push(new BrickViewModel(brick));
            }
            this.setPagination(page);
        }
        this.hideProgressSpinner();
    }

    loadAllBricks() {
        this.showProgressBar();
        return this.searchRequest = this.getJson(this.getPageUrl(this.baseUrl() + "/Bricks/LoadBricks"),
            this.loadAllBricksSucceeded,
            this);
    }

    loadAllBricksSucceeded(result) {
        if (result.success === true) {
            if (this.searchInputDuplicate() === "") {
                this.searchBricksResults.removeAll();
                result.model.forEach(function(brick) {
                        this.searchBricksResults.push(new BrickViewModel(brick));
                    },
                    this);
            }
        } else {
            this.showAlert({ type: "error", message: result.message });
            this.hideProgressBar();
        }
        this.hideProgressBar();
    }

    createBrick() {
        const viewModel = new BrickViewModel(null);
        viewModel.name(this.newBrickName());
        viewModel.brickClass(this.newBrickClass());
        this.postJson(`${this.baseUrl()}/Bricks/Create`,
            ko.toJSON(viewModel), this.createBrickSucceeded, this);
    }

    createBrickSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            const vm = new BrickViewModel(result.model);
            this.bricks.push(vm);
            window.location.href = this.baseUrl() + "/Bricks/Edit/" + vm.id();
        }
        else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    createDuplicateBrick() {
        const viewModel = new BrickViewModel(null);
        viewModel.name(this.newBrickNameDuplicate());
        for (let bc of this.brickClasses()) {
            if (bc.name.replace(/\s/g, "") === this.selectedBrick().brickClass()) {
                this.newBrickClassDuplicate(bc.value);
            }
        }
        viewModel.brickClass(this.newBrickClassDuplicate());
        this.postJson(`${this.baseUrl()}/Bricks/Create`,
            ko.toJSON(viewModel), this.createDuplicateBrickSucceeded, this);
    }

    createDuplicateBrickSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            const vm = new BrickViewModel(result.model);
            this.bricks.push(vm);
            this.sendBricks.push(vm);
            this.sendBricks.push(this.selectedBrick());
            this.postJson(`${this.baseUrl()}/Bricks/Duplicate`,
                ko.toJSON(this.sendBricks()), this.duplicateBrickSucceeded, this);
        }
        else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    duplicateBrickSucceeded(result) {
        this.searchInputDuplicate(null);
        this.newBrickNameDuplicate(null);
        if (result.success === true) {
            window.location.href = `${this.baseUrl()}/Bricks/Edit/${result.model.id}`;
        }
        else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    deleteBrick() {
        if (!this.deletingBrick) {
            return;
        }
        this.deleteJson(`${this.baseUrl()}/Bricks/Delete/${this.deletingBrick.id()}`,
            ko.toJSON(this.deletingBrick), this.deleteBrickSucceeded, this);
    }

    deleteBrickSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var deleteId = this.deletingBrick.id();
            this.bricks.remove(item => item.id() === deleteId);
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    downloadDataPackage() {
        if (this.selectedBricks().length === 0) {
            this.closeModal();
            this.showAlert({ type: "error", message: "No Brick selected." });
            return;
        }
        this.showProgressBar();
        this.postJson(`${this.baseUrl()}/Bricks/Export?includeGroups=${this.includeGroups()}&includeTexts=${this.includeTexts()}&includeResources=${this.includeResources()}&includeUserGroups=${this.includeUserGroups()}`,
            ko.toJSON(this.selectedBricks), this.downloadDataPackageSucceeded, this);
    }

    downloadDataPackageSucceeded(result) {
        if (result.success) {
            window.location.href = `${this.baseUrl()}/DataExchange/Export/Package/${result.model.packageId}`;

        } else {
            this.hideProgressBar();
            this.showAlert({ type: "error", message: result.message });
        }
    }

    showDeleteModal(viewModel) {
        this.deletingBrick = viewModel;
        this.deleteModalTitle(viewModel.displayName);
        // ReSharper disable once TsResolvedFromInaccessibleModule
        $("#DeleteModal").foundation("reveal", "open");
    }

    searchBricks(searchTerm: string) {
        if (!searchTerm || searchTerm.length <= 0) {
            return;
        }
        if (this.searchRequest && this.searchRequest.readyState < 4) {
            this.searchRequest.abort();
        }
        this.searchBricksResults.removeAll();
        this.searchRequest = this.getJson(`${this.baseUrl()}/Bricks?q=${searchTerm}&p=1&s=500&o=name&asc=true`, this.searchBricksSucceeded, this);
    }

    searchBricksSucceeded(result) {
        if (!result.success) {
            return;
        }
        for (let brick of result.model.data) {
            this.searchBricksResults.push(new BrickViewModel(brick));
        }
    }

    searchInputDuplicateChanged(newValue) {
        var that: BricksIndexViewModel = this;
        window.clearTimeout(this.searchInputTimer);
        this.searchInputTimer = window.setTimeout(() => {
            that.searchBricks(newValue);
        }, 500);
    }
}
export = BricksIndexViewModel
