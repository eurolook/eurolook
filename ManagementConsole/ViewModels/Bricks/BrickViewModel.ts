export class BrickViewModel {

    id = ko.observable(null);
    isSelected = ko.observable(false);
    name = ko.observable("");
    displayName = ko.observable("");
    brickClass = ko.observable("");
    categoryName = ko.observable("");
    uiPositionIndex = ko.observable(0);

    constructor(model) {
        if (!model) {
            return;
        }

        this.id(model.id);
        this.name(model.name);
        this.displayName(model.displayName);
        this.brickClass(model.brickClass.replace("Eurolook.Data.Models.", ""));
        this.uiPositionIndex(model.uiPositionIndex);
        this.categoryName(model.categoryName);
    }
}
