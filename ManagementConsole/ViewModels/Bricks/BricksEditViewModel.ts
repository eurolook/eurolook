import { PageViewModel } from "PageViewModel"
import Utils = require("Utils")
import { TextViewModel } from "Texts/TextViewModel"
import { ResourceViewModel } from "Resources/ResourceViewModel"
import { UserGroupViewModel } from "UserGroups/UserGroupViewModel";
import { BrickGroupViewModel } from "../BrickGroups/BrickGroupViewModel";
declare var monaco: any;

// @ts-ignore
self.MonacoEnvironment = {
    getWorkerUrl: function (moduleId, label) {
        if (label === 'json') {
            return '../../dist/json.worker.bundle.js';
        }
        if (label === 'css' || label === 'scss' || label === 'less') {
            return '../../dist/css.worker.bundle.js';
        }
        if (label === 'html' || label === 'handlebars' || label === 'razor') {
            return '../../dist/html.worker.bundle.js';
        }
        if (label === 'typescript' || label === 'javascript') {
            return '../../dist/ts.worker.bundle.js';
        }
        return '../../dist/editor.worker.bundle.js';
    }
};

class BricksEditViewModel extends PageViewModel {
    id: string;
    url: string;
    categoryId = ko.observable("");
    groupId = ko.observable("");
    name = ko.observable("");
    description = ko.observable("");
    displayName = ko.observable("");
    hotKey = ko.observable("");
    acronym = ko.observable("");
    uiPositionIndex = ko.observable(0);
    isVisible = ko.observable(true);
    isMultiInstance = ko.observable(true);
    colorLuminance = ko.observable(0);
    minVersion = ko.observable("");
    maxVersion = ko.observable("");
    isCommandBrick = ko.observable(false);
    isContentBrick = ko.observable(false);
    isDynamicBrick = ko.observable(false);
    content: string;
    contentBrickActionsClassName = ko.observable("");
    command = ko.observable("");
    commandArgument = ko.observable();
    configuration: string;
    categories = ko.observableArray();
    userGroups: KnockoutObservableArray<UserGroupViewModel> = ko.observableArray();
    selectedCategory = ko.observable();
    groups: KnockoutObservableArray<BrickGroupViewModel> = ko.observableArray();
    selectedGroup = ko.observable();

    iconFileFormData: FormData;
    iconInfo = ko.observable();
    iconImage = ko.observable();
    hasIcon = ko.observable();
    isIconDeleted = ko.observable();

    vectorIconFileFormData: FormData;
    vectorIconInfo = ko.observable();
    vectorIconImage = ko.observable();
    hasVectorIcon = ko.observable();
    isVectorIconDeleted = ko.observable();

    hasCustomUi = ko.observable();
    luminanceValues = [
        { name: "Medium", value: 0 },
        { name: "Bright", value: 1 },
        { name: "Dark", value: 2 },
        { name: "Highlighted", value: 3}
    ];
    brickTexts: KnockoutObservableArray<TextViewModel> = ko.observableArray([]);
    selectedInsertText: KnockoutObservable<TextViewModel> = ko.observable(null);
    newTextAlias = ko.observable("");
    searchTextResults: KnockoutObservableArray<TextViewModel> = ko.observableArray([]);
    searchTimer = null;
    allResources: KnockoutObservableArray<ResourceViewModel> = ko.observableArray([]);
    brickResources: KnockoutObservableArray<ResourceViewModel> = ko.observableArray([]);
    selectedResource: KnockoutObservable<ResourceViewModel> = ko.observable(null);
    newResourceAlias = ko.observable("");
    documentModels: KnockoutObservableArray<string> = ko.observableArray([]);
    searchInput = ko.observable("");
    searchInputTimer: number;
    isSaving = ko.observable(false);
    searchTextsRequests: JQueryXHR;
    allTexts: KnockoutObservableArray<TextViewModel> = ko.observableArray([]);

    brickConfigEditor: any;
    bdlEditor: any;

    containerIsVanishing = /data-container="vanishing"/i;
    containerIsNone = /data-container="none"/i;
    isSingleInstanceWarningVisible = ko.observable(false);
    isSingleInstanceErrorVisible = ko.observable(false);
    singleInstanceError = ko.observable("Single-Instance bricks cannot have data-container=\"none\".");
    singleInstanceWarning = ko.observable("Single-Instance bricks should not have data-container=\"vanishing\".");


    constructor(baseUrl: string, id: string, isReadOnly: string) {
        super(baseUrl, "/Bricks", isReadOnly);
        this.id = id;
        this.url = baseUrl + "/Bricks/Edit/" + id;
        this.searchInput.subscribe(this.searchInputChanged, this);
        this.isMultiInstance.subscribe(this.isMultiInstanceChanged, this);
        this.bindCtrlSave(this.save);
        $("#SearchBox").focus();

        monaco.languages.json.jsonDefaults.setDiagnosticsOptions({
            validate: true,
            allowComments: true,
            schemaValidation: 'error'
        });

        const editorOptions = {
            theme: "vs",
            //renderSideBySide: false,
            readOnly: this.isReadOnlyMode(),
            enableSplitViewResizing: false,
            value: '',
            language: '',
            automaticLayout: true,
            scrollbar: {
                useShadows: false,
                verticalHasArrows: true,
                horizontalHasArrows: true,
                scrollBeyondLastLine: false,
            },
            minimap: {
                enabled: false
            }
        };

        editorOptions.language = 'json';
        this.brickConfigEditor = monaco.editor.create(document.getElementById("configuration-editor"), editorOptions);

        editorOptions.language = 'xml';
        this.bdlEditor = monaco.editor.create(document.getElementById("bdl-editor"), editorOptions);

        this.load();
    }

    load() {
        this.showProgressBar();
        this.getJson(this.url, this.loadSucceeded, this);
    }

    loadSucceeded(result) {
        this.categoryId(result.model.categoryId);
        this.groupId(result.model.groupId);
        this.name(result.model.name);
        this.groupId(result.model.groupId);
        this.description(result.model.description);
        this.displayName(result.model.displayName);
        this.hotKey(result.model.hotKey);
        this.acronym(result.model.acronym);
        this.uiPositionIndex(result.model.uiPositionIndex);
        this.isVisible(result.model.isVisible);
        this.isMultiInstance(result.model.isMultiInstance);
        this.colorLuminance(result.model.colorLuminance);
        this.minVersion(result.model.minVersion);
        this.maxVersion(result.model.maxVersion);
        this.isCommandBrick(result.model.isCommandBrick);
        this.isContentBrick(result.model.isContentBrick);
        this.isDynamicBrick(result.model.isDynamicBrick);
        this.content = result.model.content;
        this.contentBrickActionsClassName(result.model.contentBrickActionsClassName);
        this.command(result.model.command);
        this.commandArgument(result.model.commandArgument);
        this.configuration = result.model.configuration;
        this.categories(result.model.categories);
        for (let userGroup of result.model.userGroups) {
            this.userGroups.push(new UserGroupViewModel(userGroup));
        }

        this.selectedCategory(result.model.selectedCategory);
        this.groups(result.model.groups);
        this.selectedGroup(this.groups().find(g => g.id === result.model.selectedGroupId));

        this.iconInfo(result.model.iconInfo);
        this.iconImage(result.model.iconImage);
        this.hasIcon(result.model.hasIcon);
        this.isIconDeleted(result.model.isIconDeleted);

        this.vectorIconInfo(result.model.vectorIconInfo);
        this.vectorIconImage(result.model.vectorIconImage);
        this.hasVectorIcon(result.model.hasVectorIcon);
        this.isVectorIconDeleted(result.model.isVectorIconDeleted);

        this.hasCustomUi(result.model.hasCustomUi);
        for (let text of result.model.brickTexts) {
            this.brickTexts.push(new TextViewModel(text));
        }
        for (let resource of result.model.allResources) {
            this.allResources.push(new ResourceViewModel(resource));
        }
        for (let resource of result.model.brickResources) {
            this.brickResources.push(new ResourceViewModel(resource));
        }
        this.documentModels(result.model.documentModels);
        this.hideProgressSpinner();
        this.checkSingleInstanceError();
        this.allTexts(result.model.allTexts);

        this.loadTextToEditor(this.brickConfigEditor, this.configuration);
        this.loadTextToEditor(this.bdlEditor, this.content);
        this.bdlEditor.getModel().onDidChangeContent((e): void => {
            this.checkSingleInstanceError();
        });
        
        this.hideProgressBar();
    }
    loadTexts() {
        this.showProgressSpinner();
        for (let text of this.allTexts()) {
            this.searchTextResults.push(new TextViewModel(text));
        }
        this.hideProgressSpinner();
    }

    loadTextToEditor(editor: any, config: string) {
        if (!config) {
            return;
        }

        editor.updateOptions({ readOnly: false });
        editor.getModel().setValue(config);
        editor.getAction('editor.action.formatDocument').run();
        editor.updateOptions({ readOnly: this.isReadOnlyMode() });
    }

    save() {
        if (this.isSaving()) return;
        if (this.isSingleInstanceErrorVisible()) {
            this.showAlert({ type: "error", message: this.singleInstanceError() });
            return;
        }

        this.isSaving(true);

        // Workaround for ko.toJS
        const brickConfigEditorReference = this.brickConfigEditor;
        const bdlEditorReference = this.bdlEditor;
        this.brickConfigEditor = null;
        this.bdlEditor = null;
        const vm = ko.toJS(this);
        this.brickConfigEditor = brickConfigEditorReference;
        this.bdlEditor = bdlEditorReference;

        vm.content = this.getEditorContent(this.bdlEditor);
        vm.configuration = this.getEditorContent(this.brickConfigEditor);
        vm.selectedGroupId = this.selectedGroup() ? this.selectedGroup().id : null;
        delete vm.categories;
        delete vm.groups;
        delete vm.searchTextResults;

        $.when(
            this.postFormData(`${this.baseUrl()}/Bricks/Icon/${this.id}`, this.iconFileFormData, this),
            this.postFormData(`${this.baseUrl()}/Bricks/VectorIcon/${this.id}`, this.vectorIconFileFormData, this),
            this.putJson(`${this.baseUrl()}/Bricks/Edit/${this.id}`,
                ko.toJSON(vm),
                null,
                this)
        ).then(this.saveSucceeded).fail(this.saveFailed);
    }

    saveSucceeded(saveIconResponse, saveVectorIconResponse, saveBrickResponse) {
        const that: BricksEditViewModel = this[0];
        const saveIconResult = saveIconResponse[0];
        const saveVectorIconResult = saveVectorIconResponse[0];
        const saveBrickResult = saveBrickResponse[0];
        if (saveIconResult.success === true && saveVectorIconResult.success === true && saveBrickResult.success === true) {
            if (saveIconResult.model != null) {
                that.iconImage(saveIconResult.model);
            }
            if (saveVectorIconResult.model != null) {
                that.vectorIconImage(saveVectorIconResult.model);
            }
            that.showAlert({ type: "success", message: saveBrickResult.message });
        } else {
            // Server side error
            let errorMsg = "";
            if (saveIconResult.success === false)
                errorMsg += saveIconResult.message;
            if (saveBrickResult.success === false)
                errorMsg += saveBrickResult.message;
            that.showAlert({ type: "error", message: errorMsg });
        }
        that.isSaving(false);
    }

    saveFailed(jqXhr: JQueryXHR, textStatus: string, errorThrown) {
        const that: BricksEditViewModel = this[0];
        that.showAlert({ type: "error", message: `HTTP error: ${jqXhr.statusText}` });
        that.isSaving(false);
    }

    chooseIconFile() {
        $("#iconFile").click();
    }

    choseIconFile(file: File) {
        this.iconFileFormData = new FormData();
        this.iconFileFormData.append("file", file);
        this.iconInfo(file.name);
        this.isIconDeleted(false);
    }

    deleteIcon() {
        this.iconImage(null);
        this.iconInfo(null);
        this.hasIcon(false);
        this.isIconDeleted(true);
    }

    
    chooseVectorIconFile() {
        $("#vectorIconFile").click();
    }

    choseVectorIconFile(file: File) {
        this.vectorIconFileFormData = new FormData();
        this.vectorIconFileFormData.append("file", file);
        this.vectorIconInfo(file.name);
        this.isVectorIconDeleted(false);
    }

    deleteVectorIcon() {
        this.vectorIconImage(null);
        this.vectorIconInfo(null);
        this.hasVectorIcon(false);
        this.isVectorIconDeleted(true);
    }

    insertSelectedText() {
        this.closeModal();
        if (this.isContentBrick) {
            this.insertTextReference(this.selectedInsertText());
        }
        if (!this.brickTexts().some(function (e) {
            // ReSharper disable once SuspiciousThisUsage
            return e.alias() === this.selectedInsertText().alias();
        }, this)) {
            this.brickTexts.push(this.selectedInsertText());
        }
    }

    insertSelectedResource() {
        this.closeModal();
        if (this.isContentBrick && this.selectedResource().mimeType != null) {
            if (this.selectedResource().mimeType().startsWith("image/")) {
                this.insertImageReference(this.selectedResource());
            } else if (this.selectedResource().mimeType() === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
                this.insertExcelSheetReference(this.selectedResource());
            }
        }
        if (!this.brickResources().some(function (e) {
            return e.alias() === this.selectedResource().alias();
        }, this)) {
            this.brickResources.push(this.selectedResource());
        }
    }

    insertReference(xml) {
        this.insertIntoEditor(this.bdlEditor, xml);
    }

    insertTextReference(text) {
        if (this.isContentBrick) {
            const xml = `<span data-type="binding" data-prop="/Texts/${text.alias()}" />`;
            this.insertReference(xml);
        }
    }

    createAndInsertNewText() {
        this.postJson(`${this.baseUrl()}/Texts/Create`,
            ko.toJSON({ Alias: this.newTextAlias }),
            this.createAndInsertNewTextSucceeded,
            this);
    }

    createAndInsertNewTextSucceeded(result) {
        if (result.success === true) {
            const vm = new TextViewModel(result.model);
            this.closeModal();
            this.insertTextReference(vm);
        } else {
            this.closeModal();
            this.showAlert({ type: "error", message: result.message });
        }
    }

    removeText(textVm: TextViewModel) {
        if (this.isContentBrick()) {
            let editorText = this.getEditorContent(this.bdlEditor);
            var matches = [];
            const alias = textVm.alias();
            try {
                const xml = $.parseXML(editorText);
                $(xml).find(`[data-prop='/Texts/${alias}']`).each(function () {
                    // ReSharper disable once SuspiciousThisUsage
                    matches.push(this);
                });
            } catch (err) {
                this.showAlert({ type: "error", message: "Text could not be removed: BDL is invalid." });
                return;
            }
            try {
                for (let e of matches) {
                    const tag = `<${e.tagName} `;
                    const dataprop = `data-prop=(\\"|')/Texts/${Utils.escapeRegExp(alias)}(\\"|')`;
                    let regexp = new RegExp(`\\s*${tag}.*${dataprop}[.^>]*\\s*\\/>`, "i");
                    if (regexp.test(editorText)) {
                        editorText = editorText.replace(regexp, "");
                    } else {
                        regexp = new RegExp(`\\s*${tag}.*${dataprop}.*\\s*>\\s*${Utils.escapeRegExp(e.textContent)}\\s*<\\/${e.tagName}\\s*>`, "i");
                        if (regexp.test(editorText)) {
                            editorText = editorText.replace(regexp, "");
                        } else {
                            regexp = new RegExp(`\\s*${tag}.*${dataprop}.*\\s*>\\s*${Utils.escapeRegExp(e.innerHTML.replace("/>", " />"))}\\s*<\\/${e.tagName}\\s*>`, "i");
                            if (regexp.test(editorText)) {
                                editorText = editorText.replace(regexp, "");
                            } else {
                                throw new Error();
                            }
                        }
                    }
                }
            } catch (err) {
                this.setEditorContent(this.bdlEditor, editorText);
                this.showAlert({ type: "warning", message: `Text ${alias} could not be removed automatically.` });
                return;
            }
            this.setEditorContent(this.bdlEditor, editorText);
        }
        this.brickTexts.remove(textVm);
    }

    removeResource(resourceVm: ResourceViewModel) {
        if (this.isContentBrick()) {
            let editorText = this.getEditorContent(this.bdlEditor);
            var matches = [];
            const alias = resourceVm.alias();
            try {
                const xml = $.parseXML(editorText);
                $(xml).find(`[src='${alias}']`).each(function () {
                    // ReSharper disable once SuspiciousThisUsage
                    matches.push(this);
                });
            } catch (err) {
                this.showAlert({ type: "error", message: "Resource could not be removed: BDL is invalid." });
                return;
            }
            try {
                for (let e of matches) {
                    const tag = `<${e.tagName} `;
                    const src = `src=(\\"|')${Utils.escapeRegExp(alias)}(\\"|')`;
                    let regexp = new RegExp(`\\s*${tag}.*${src}[.^>]*\\s*\\/>`, "i");
                    if (regexp.test(editorText)) {
                        editorText = editorText.replace(regexp, "");
                    } else {
                        regexp = new RegExp(`\\s*${tag}.*${src}.*\\s*>\\s*${Utils.escapeRegExp(e.innerHTML.replace("/>", " />"))}\\s*<\\/${e.tagName}\\s*>`, "i");
                        if (regexp.test(editorText)) {
                            editorText = editorText.replace(regexp, "");
                        } else {
                            throw new Error();
                        }
                    }
                }
            } catch (err) {
                this.setEditorContent(this.bdlEditor, editorText);
                this.showAlert({ type: "warning", message: `Resource ${alias} could not be removed automatically.` });
                return;
            }
            this.setEditorContent(this.bdlEditor, editorText);
        }
        this.brickResources.remove(resourceVm);
    }

    insertImageReference(resource) {
        const xml = `<img src="${resource.alias()}" />`;
        this.insertReference(xml);
    }

    insertExcelSheetReference(resource) {
        const xml = `<embed src="${resource.alias()}" />`;
        this.insertReference(xml);
    }

    createAndInsertNewResource() {
        this.postJson(`${this.baseUrl()}/Resources/Create`,
            ko.toJSON({
                Alias: this.newResourceAlias,
            }),
            this.createAndInsertNewResourceSucceeded,
            this);
    }

    createAndInsertNewResourceSucceeded(result) {
        if (result.success === true) {
            const vm = new ResourceViewModel(result.model);
            this.closeModal();
            if (this.isContentBrick) {
                this.insertImageReference(vm);
            }
            this.brickResources.push(vm);
        } else {
            this.closeModal();
            this.showAlert({ type: "error", message: result.message });
        }
    }

    searchInputChanged(newValue) {
        var that: BricksEditViewModel = this;
        window.clearTimeout(this.searchInputTimer);
        this.searchInputTimer = window.setTimeout(() => {
            that.searchTexts(newValue);
        }, 500);
    }

    searchTexts(searchTerm: string) {
        if (!searchTerm || searchTerm.length <=0) {
            return;
        }
        if (this.searchTextsRequests && this.searchTextsRequests.readyState < 4) {
            this.searchTextsRequests.abort();
        }
        this.searchTextResults.removeAll();
        if (searchTerm.indexOf("*") < 0) {
            // when not otherwise specified, do a prefix search
            searchTerm += "*";
        }
        this.searchTextsRequests = this.getJson(`${this.baseUrl()}/Texts/?p=1&s=1000&t=alias&q=${searchTerm}`, this.searchTextsSucceeded, this);
    }

    searchTextsSucceeded(result) {
        if (!result.success) {
            return;
        }
        for (let textDm of result.model.data) {
            this.searchTextResults.push(new TextViewModel(textDm));
        }
    }

    getEditorContent(editor: any): string {
        return editor.getModel().getValue() as string;
    }

    setEditorContent(editor: any, content: string) {
        editor.getModel().setValue(content);
        editor.getAction('editor.action.formatDocument').run();
    }

    insertIntoEditor(editor: any, content: string) {
        const selection = this.bdlEditor.getSelection();
        const operation = {
            identifier: { major: 1, minor: 1 },
            range: selection,
            text: content,
            forceMoveMarkers: true
        };
        editor.executeEdits("insert-text-reference", [operation]);
        editor.getAction('editor.action.formatDocument').run();
    }

    isMultiInstanceChanged() {
        this.checkSingleInstanceError();
    }

    checkSingleInstanceError() {
        this.isSingleInstanceWarningVisible(false);
        this.isSingleInstanceErrorVisible(false);
        if (this.isMultiInstance()) {
            return;
        }

        const editorContent = this.bdlEditor.getValue();
        if (this.containerIsNone.test(editorContent)) {
            this.isSingleInstanceErrorVisible(true);
        }
        else if (this.containerIsVanishing.test(editorContent)) {
            this.isSingleInstanceWarningVisible(true);
        }
    }
}
export = BricksEditViewModel;
