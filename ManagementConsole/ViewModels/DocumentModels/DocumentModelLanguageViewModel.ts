export class DocumentModelLanguageViewModel {

    isSelected = ko.observable(false);
    languageId = ko.observable();
    languageName = ko.observable<string>();
    templateName = ko.observable<string>();
    templateNiceName = ko.observable<string>();
    hasTemplate = ko.observable(false);
    uploadedFile: FormData;
    isModified = ko.observable(false);

    constructor(model) {
        if (!model) return;

        this.isSelected(model.isSelected);

        this.languageId(model.languageId);
        this.languageName(model.languageName);

        this.templateName(model.templateName);
        this.templateNiceName(this.getTemplateNiceName(model.templateName));
        this.hasTemplate(model.hasTemplate);

        this.isModified(false);
    }

    getTemplateNiceName(name: string) {
        if (name == null) {
            return "";
        }

        let niceName = name;
        if (name.length > 27) {
            niceName = name.substr(0, 10) + "..." + name.substr(-15, 15);
        }
        return `(${niceName})`;
    }

    delete() {
        this.uploadedFile = null;
        this.hasTemplate(false);
        this.templateName("");
        this.templateNiceName("");        
        this.isModified(true);
        return;
    }
}