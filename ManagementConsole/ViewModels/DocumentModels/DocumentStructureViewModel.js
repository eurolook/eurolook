function DocumentStructureViewModel(model) {
    
    var self = this;

    if (model === undefined || model === null) {
        self.structureId = ko.observable("");
        self.brickId = ko.observable("");
        self.name = ko.observable("");
        self.positionIndex = ko.observable(0);
        self.isFirstPage = ko.observable(true);
        self.isOddPages = ko.observable(true);
        self.isEvenPages = ko.observable(true);
        self.autoBrickCreationType = ko.observable(0);
        self.isAutomatable = ko.observable(false);
    }
    else {
        self.structureId = ko.observable(model.structureId);
        self.brickId = ko.observable(model.brickId);
        self.name = ko.observable(model.name);
        self.positionIndex = ko.observable(model.positionIndex);
        self.isFirstPage = ko.observable(model.isFirstPage);
        self.isOddPages = ko.observable(model.isOddPages);
        self.isEvenPages = ko.observable(model.isEvenPages);
        self.autoBrickCreationType = ko.observable(model.autoBrickCreationType);
        self.isAutomatable = ko.observable(model.isAutomatable);
    }

    // a unique key for use within the DOM (this is needed because there is no structureId yet for new document structures
    var uuid = createGuid();

    self.key = function () {
        return uuid;
    };

    // computed property for non-body structures that still use a checkbox
    self.isAuto = ko.computed({
        read: function () {
            var type = self.autoBrickCreationType();
            return type !== 0 ? true : false;
        },
        write: function (newValue) {
            var type = newValue ? 1 : 0;
            self.autoBrickCreationType(type);
        },
        owner: this
    });

    // create a string value that can easily bound to the radio button value
    self.autoBrickCreationTypeValue = ko.computed({
        read: function () {
            var type = self.autoBrickCreationType();
            return getBrickCreationTypeDescription(type);
        },
        write: function (newValue) {
            var type = getBrickCreationTypeFromDescription(newValue);
            self.autoBrickCreationType(type);
        },
        owner: this        
    });
    
    // a color for the * indicator
    self.autoBrickCreationColor = ko.computed(function () {
        var type = self.autoBrickCreationType();
        return type === 0 ? "rgb(128, 128, 128)" : type === 1 ? "#008cba" : type === 2 ? "green" : "red";
    });
    
    // a unique key for each radio button + label, so that the label can be mapped to the radio button
    self.autoBrickCreationTypeKey = function (type) {
        return getBrickCreationTypeDescription(type) + "-" + self.key();
    };

    // close dropdown panel
    self.closeDropDown = function(data, event) {
        var $this = $(event.currentTarget);
        if ($this.hasClass("open")) {
            $this.foundation("dropdown", "close", $this);
        }
        return true;
    };

    // helper function to convert an autoBrickCreationType into a string
    function getBrickCreationTypeDescription(type) {
        return type === 0 ? "creationNone" : type === 1 ? "creationAlways" : type === 2 ? "creationSimpleContent" : "creationRichContent";
    }

    // helper function to get an autoBrickCreationType from a string description
    function getBrickCreationTypeFromDescription(description) {
        return description === "creationNone" ? 0 : description === "creationAlways" ? 1 : description === "creationSimpleContent" ? 2 : 3;
    }
}
