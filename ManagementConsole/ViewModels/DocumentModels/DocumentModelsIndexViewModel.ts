import { PaginatedTableViewModel } from "PaginatedTableViewModel"
import { DocumentModelViewModel } from "DocumentModels/DocumentModelViewModel"

class DocumentModelsIndexViewModel extends PaginatedTableViewModel {

    // ReSharper disable SuspiciousThisUsage
    isSelectedAll = ko.pureComputed({
        read: function () {
            return this.selectedDocumentModels().length === this.documentModels().length;
        },
        write: function (value) {
            this.selectedDocumentModels(value ? this.documentModels().slice(0) : []);
        },
        owner: this
    });
    // ReSharper restore SuspiciousThisUsage
    documentModels: KnockoutObservableArray<DocumentModelViewModel> = ko.observableArray([]);
    selectedDocumentModels: KnockoutObservableArray<DocumentModelViewModel> = ko.observableArray([]);
    searchRequest: JQueryXHR;
    newDocumentModelName = ko.observable("");
    deletingDocumentModel: DocumentModelViewModel = null;
    deleteModalTitle = ko.observable("");
    includeStructures = ko.observable(false);
    includeBricks = ko.observable(false);
    includeDocumentModelMetadataDefinitions = ko.observable(false);
    includeMetadataDefinitions = ko.observable(false);
    includeAuthorRoles = ko.observable(false);
    copyMetadataDefinitionsTargetDocumentModelId = ko.observable();
    copyMetadataDefinitionsSourceDocumentModel = ko.observable(null);
    copyMetadataDefinitionsModalTitle = ko.observable("");

    copyMetadataDefinitionsTargetDocumentModels = ko.pureComputed({
        read: () => this.documentModels()
            .filter(dm => !dm.isClassicDocument()
                && (!this.copyMetadataDefinitionsSourceDocumentModel() || dm.id !== this.copyMetadataDefinitionsSourceDocumentModel().id)),
        owner: this
    });

    constructor(baseUrl, isReadOnly: string) {
        super(baseUrl, "/DocumentModels", isReadOnly);
        if (!this.orderProperty()) {
            this.orderProperty("name");
            this.orderAscending(true);
        }
        this.includeStructures.subscribe(function (newValue) {
            if (newValue === false && this.includeBricks() === true) {
                this.includeBricks(false);
            }
        }, this);
        this.includeBricks.subscribe(function (newValue) {
            if (newValue && this.includeStructures() === false) {
                this.includeStructures(true);
            }
        }, this);
        this.includeDocumentModelMetadataDefinitions.subscribe(function (newValue) {
            if (newValue === false && this.includeMetadataDefinitions() === true) {
                this.includeMetadataDefinitions(false);
            }
        }, this);
        this.includeMetadataDefinitions.subscribe(function (newValue) {
            if (newValue && this.includeDocumentModelMetadataDefinitions() === false) {
                this.includeDocumentModelMetadataDefinitions(true);
            }
        }, this);
        this.includeAuthorRoles.subscribe(function (newValue) {
            if (newValue && this.includeAuthorRoles() === false) {
                this.includeAuthorRoles(true);
            }
        }, this);
        this.loadPageData();
        $("#SearchBox").focus();
    }

    loadPageData() {
        if (this.searchRequest && this.searchRequest.readyState < 4) {
            this.searchRequest.abort();
        }
        this.showProgressSpinner();
        this.searchRequest = this.getJson(this.getPageUrl(this.baseUrl() + "/DocumentModels"),
            this.loadPageDataSucceeded,
            this);
    }

    loadPageDataSucceeded(result) {
        this.selectedDocumentModels.removeAll();
        this.documentModels.removeAll();
        if (result.success === true) {
            const page = result.model;
            for (let dm of page.data) {
                this.documentModels.push(new DocumentModelViewModel(dm));
            }
            this.setPagination(page);
        }
        this.hideProgressSpinner();
    }

    createDocumentModel() {
        const viewModel = new DocumentModelViewModel(null);
        viewModel.name(this.newDocumentModelName());
        this.postJson(`${this.baseUrl()}/DocumentModels/Create`,
            ko.toJSON(viewModel),
            this.createDocumentModelSucceeded,
            this);
    }

    createDocumentModelSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            this.documentModels.push(new DocumentModelViewModel(result.model));
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    deleteDocumentModel() {
        if (!this.deletingDocumentModel) return;
        this.deleteJson(`${this.baseUrl()}/DocumentModels/Delete/${this.deletingDocumentModel.id()}`,
            ko.toJSON(this.deletingDocumentModel),
            this.deleteDocumentModelSucceeded,
            this);
    }

    deleteDocumentModelSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var deleteId = this.deletingDocumentModel.id();
            this.documentModels.remove(item => item.id() === deleteId);
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    copyMetadataDefinitions() {
        if (!this.copyMetadataDefinitionsSourceDocumentModel()) return;
        this.postJson(`${this.baseUrl()}/DocumentModels/MetadataDefinitions/${this.copyMetadataDefinitionsTargetDocumentModelId()}?sourceDocumentModelId=${this.copyMetadataDefinitionsSourceDocumentModel().id()}`,
            null,
            this.copyMetadataDefinitionsSucceeded,
            this);
    }

    copyMetadataDefinitionsSucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    downloadDataPackage() {
        if (this.selectedDocumentModels().length === 0) {
            this.closeModal();
            return this.showAlert({ type: "error", message: "No DocumentModel selected." });
        }
        this.showProgressBar();
        this.postJson(`${this.baseUrl()}/DocumentModels/Export?includeStructures=${this.includeStructures()}&includeBricks=${this.includeBricks()}&includeDocumentModelMetadataDefinitions=${this.includeDocumentModelMetadataDefinitions()}&includeMetadataDefinitions=${this.includeMetadataDefinitions()}&includeAuthorRoles=${this.includeAuthorRoles()}`,
            ko.toJSON(this.selectedDocumentModels),
            this.downloadDataPackageSucceeded,
            this);
    }

    downloadDataPackageSucceeded(result) {
        if (result.success) {
            window.location.href = `${this.baseUrl()}/DataExchange/Export/Package/${result.model.packageId}`;
        } else {
            this.hideProgressBar();
            this.showAlert({ type: "error", message: result.message });
        }
    }

    showDeleteModal(viewModel) {
        this.deletingDocumentModel = viewModel;
        this.deleteModalTitle(viewModel.name);
        // ReSharper disable once TsResolvedFromInaccessibleModule
        $("#DeleteModal").foundation("reveal", "open");
    }

    showCopyMetadataDefinitionModal(viewModel) {
        this.copyMetadataDefinitionsSourceDocumentModel(viewModel);
        this.copyMetadataDefinitionsModalTitle(viewModel.name);
        // ReSharper disable once TsResolvedFromInaccessibleModule
        $("#CopyMetadataModal").foundation("reveal", "open");
    }
}

export = DocumentModelsIndexViewModel;
