define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var DocumentModelAuthorRoleViewModel = /** @class */ (function () {
        function DocumentModelAuthorRoleViewModel(model) {
            this.isSelected = ko.observable(false);
            this.authorRoleId = ko.observable();
            this.authorRoleName = ko.observable();
            if (!model)
                return;
            this.isSelected(model.isSelected);
        }
        return DocumentModelAuthorRoleViewModel;
    }());
    exports.DocumentModelAuthorRoleViewModel = DocumentModelAuthorRoleViewModel;
});
//# sourceMappingURL=DocumentModelAuthorRoleModel.js.map