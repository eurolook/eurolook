﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Eurolook.Data.Models;

namespace Eurolook.ManagementConsole.ViewModels.DocumentModels
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    public class DocumentModelsStructureViewModel
    {
        public DocumentModelsStructureViewModel()
        {
            // called by ASP.NET MVC JSON serializer/deserializer
            HeaderStructures = new List<DocumentStructureViewModel>();
            BeginStructures = new List<DocumentStructureViewModel>();
            BodyStructures = new List<DocumentStructureViewModel>();
            EndStructures = new List<DocumentStructureViewModel>();
            FooterStructures = new List<DocumentStructureViewModel>();
            CursorStructures = new List<DocumentStructureViewModel>();
        }

        public DocumentModelsStructureViewModel(
            IEnumerable<DocumentStructure> header,
            IEnumerable<DocumentStructure> begin,
            IEnumerable<DocumentStructure> body,
            IEnumerable<DocumentStructure> end,
            IEnumerable<DocumentStructure> footer,
            IEnumerable<DocumentStructure> cursor)
            : this()
        {
            foreach (var h in header)
            {
                HeaderStructures.Add(new DocumentStructureViewModel(h));
            }

            foreach (var b in begin)
            {
                BeginStructures.Add(new DocumentStructureViewModel(b));
            }

            foreach (var b in body)
            {
                BodyStructures.Add(new DocumentStructureViewModel(b));
            }

            foreach (var e in end)
            {
                EndStructures.Add(new DocumentStructureViewModel(e));
            }

            foreach (var f in footer)
            {
                FooterStructures.Add(new DocumentStructureViewModel(f));
            }

            foreach (var c in cursor)
            {
                CursorStructures.Add(new DocumentStructureViewModel(c));
            }
        }

        public List<DocumentStructureViewModel> HeaderStructures { get; set; }

        public List<DocumentStructureViewModel> BeginStructures { get; set; }

        public List<DocumentStructureViewModel> BodyStructures { get; set; }

        public List<DocumentStructureViewModel> EndStructures { get; set; }

        public List<DocumentStructureViewModel> FooterStructures { get; set; }

        public List<DocumentStructureViewModel> CursorStructures { get; set; }
    }
}
