function DocumentModelsStructureViewModel(model) {

    this.headerStructures = ko.observableArray();
    this.beginStructures = ko.observableArray();
    this.bodyStructures = ko.observableArray();
    this.endStructures = ko.observableArray();
    this.footerStructures = ko.observableArray();
    this.cursorStructures = ko.observableArray();
    this.brickGroups = ko.observableArray();
    this.isSaving = ko.observable(false);
    this.isReadOnlyMode = ko.observable(false);
    this.filteredByBrickGroups = ko.observable(false);

    var self = this;
    model.headerStructures.forEach(function (item) {
        self.headerStructures.push(new DocumentStructureViewModel(item));
    });
    model.beginStructures.forEach(function (item) {
        self.beginStructures.push(new DocumentStructureViewModel(item));
    });
    model.bodyStructures.forEach(function (item) {
        self.bodyStructures.push(new DocumentStructureViewModel(item));
    });
    model.endStructures.forEach(function (item) {
        self.endStructures.push(new DocumentStructureViewModel(item));
    });
    model.footerStructures.forEach(function (item) {
        self.footerStructures.push(new DocumentStructureViewModel(item));
    });
    model.cursorStructures.forEach(function(item) {
        self.cursorStructures.push(new DocumentStructureViewModel(item));
    });

    this.loadBricksAsync = function(query, callback) {

        // show progress bar
        var progressTimeOut = window.setTimeout(function() {
                $("#brickProgress").show();
            },
            700);

        // load the bricks from server
        var self = this;
        jQuery.ajax({
            url: ko.baseUrl + "/DocumentModels/StructureBricks?text=" + query + "&filteredByBrickGroups=" + self.filteredByBrickGroups(),
            dataType: "json",
            success: function(data) {
                self.brickGroups.removeAll();
                if (!self.filteredByBrickGroups()) {
                    self.brickGroups.push({
                        id: "noBrickGroup",
                        name: undefined,
                        members: ko.observableArray()
                    })
                }
                else {
                    data.forEach(function (brick) {
                        const isBrickGroupAlreadyAdded = self.brickGroups().some(bg => bg.name === brick.groupName);
                        if (!isBrickGroupAlreadyAdded) {
                            self.brickGroups.push({
                                id: brick.groupId,
                                name: brick.groupName,
                                members: ko.observableArray()
                            });
                        }
                    });
                }

                self.brickGroups().forEach(function (brickGroup) {
                    const containedBricks = data.filter(b => b.groupName === brickGroup.name).map(b => ({
                        id: b.id,
                        name: b.name,
                        isContentBrick: b.isContentBrick
                    }));

                    brickGroup.members.push(...containedBricks);
                });

                if (callback) {
                    callback();
                }

                // hide progress bar
                window.clearTimeout(progressTimeOut);
                $("#brickProgress").hide();
            }
        });
    };

    this.save = function() {
        // don't let the user save twice in a row
        if (self.isSaving()) {
            return;
        }
        self.isSaving(true);

        // do not submit bricks
        var json = ko.toJSON(self);
        json.bricks = null;
        jQuery.ajax({
            type: "PUT",
            url: "",
            contentType: "application/json",
            dataType: "json",
            data: ko.toJSON(self),
            success: function(data) {
                if (data.success === true) {
                    showAlert({ type: "success", message: data.message });
                } else {
                    showAlert({ type: "error", message: data.message });
                }
                self.isSaving(false);
            },
            error: function(jqXhr, textStatus, errorThrown) {
                showAlert({ type: "error", message: "HTTP error: " + errorThrown });
                self.isSaving(false);
            }
        });
    };

    this.cancel = function() {
        window.location.reload();
    };

    this.getListByBinding = function(binding) {
        var list = this.headerStructures;
        if (binding.indexOf("begin") !== -1) {
            list = this.beginStructures;
        } else if (binding.indexOf("body") !== -1) {
            list = this.bodyStructures;
        } else if (binding.indexOf("end") !== -1) {
            list = this.endStructures;
        } else if (binding.indexOf("footer") !== -1) {
            list = this.footerStructures;
        } else if (binding.indexOf("cursor") !== -1) {
            list = this.cursorStructures;
        }
        return list;
    };

    this.addStructure = function(brickId, name, binding) {
        var brick = this.getBrickById(brickId);
        if (brick !== null) {
            var list = this.getListByBinding(binding);
            var structure = new DocumentStructureViewModel();
            structure.brickId(brick.id);
            structure.isAutomatable(brick.isContentBrick);
            structure.name(name);
            list.push(structure);
            this.reindex(list);
        }
    };

    this.deleteStructure = function(structure, binding) {
        var list = this.getListByBinding(binding);
        list.remove(structure);
        this.reindex(list);
    };

    this.reindex = function(structuresArray) {
        var pos = 1;
        for (var i = 0; i < structuresArray().length; i++) {
            var structure = structuresArray()[i];
            structure.positionIndex(pos);
            pos += 1;
        }
    };

    this.getBrickById = function(brickId) {
        var self = this;
        for (var i = 0; i < self.brickGroups().length; i++) {
            var brickGroup = self.brickGroups()[i];
            for (var j = 0; j < brickGroup.members().length; j++) {
                var brick = brickGroup.members()[j];
                if (brick.id === brickId) {
                    return brick;
                }
            }
        }

        return null;
    };
}
