using System;
using System.Collections.Generic;

namespace Eurolook.ManagementConsole.ViewModels.DocumentModels.MetadataDefinitions
{
    public class MetadataDefinitionSelectionViewModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string DisplayName { get; set; }

        public bool IsBoundToTermSet { get; set; }

        public List<DefaultValueViewModel> DefaultValues { get; set; }
    }
}
