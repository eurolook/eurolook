using System;

namespace Eurolook.ManagementConsole.ViewModels.DocumentModels.MetadataDefinitions
{
    public class DefaultValueViewModel
    {
        public string Value { get; set; }

        public Guid? SharePointId { get; set; }
    }
}
