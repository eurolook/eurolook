﻿using System;
using System.Collections.Generic;
using Eurolook.Common.Extensions;
using Eurolook.ManagementConsole.Models;

namespace Eurolook.ManagementConsole.ViewModels.DocumentModels.MetadataDefinitions
{
    public class AssociatedMetadataDefinitionViewModel
    {
        public Guid MetadataDefinitionId { get; set; }

        public string DefaultValue { get; set; }

        public string CalculationCommandClassName { get; set; }

        public bool? IsReadOnlyOverride { get; set; }

        public string EditableByUserGroups { get; set; }

        public IEnumerable<UserGroupDataModel> AllUserGroupDataModels { get; set; }

        public bool IsEditableByUserGroup(Guid id)
        {
            return EditableByUserGroups?.ContainsInvariantIgnoreCase(id.ToString()) == true;
        }
    }
}
