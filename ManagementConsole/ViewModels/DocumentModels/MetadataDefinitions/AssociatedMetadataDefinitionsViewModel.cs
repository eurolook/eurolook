﻿using System;
using System.Collections.Generic;

namespace Eurolook.ManagementConsole.ViewModels.DocumentModels.MetadataDefinitions
{
    public class AssociatedMetadataDefinitionsViewModel
    {
        public Guid DocumentModelId { get; set; }

        public IEnumerable<AssociatedMetadataDefinitionViewModel> AssociatedMetadataDefinitions { get; set; }
    }
}
