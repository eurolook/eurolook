function AssociatedMetadataDefinitionsViewModel(model, readOnlyMode) {

    var self = this;
    self.associatedMetadataDefinitions = ko.observableArray();
    self.metadataDefinitions = ko.observableArray();
    self.isSaving = ko.observable(false);
    self.progressBarVisible = ko.observable(false);
    self.searchTerm = ko.observable("");
    self.readOnlyOverrideOptions = ko.observableArray([null, true, false]);
    self.isReadOnlyMode = ko.observable((readOnlyMode === 'true' || readOnlyMode === 'True'));

    self.loadMetadataDefinitionsAsync = function() {
        // close pending request
        if (self.loadRequest && self.loadRequest.readyState < 4) {
            self.loadRequest.abort();
        }
        // open a new request
        self.showProgressBar();
        var url = ko.baseUrl + "/Metadata/MetadataDefinitions/DefinitionsList";
        self.loadRequest = jQuery.ajax({
            type: "GET",
            url: url,
            dataType: "json",
            success: function(result) {
                self.metadataDefinitions.removeAll();
                if (result.success === true) {
                    self.initMetadataDefinitions(result.model);
                    self.initAssociatedMetadataDefinitions();
                }
                self.hideProgressBar();
            },
            error: function(jqXhr, textStatus, errorThrown) {
                self.hideProgressBar();
                showAlert({ type: "error", message: "HTTP error: " + errorThrown });
            }
        });
    };

    self.getMetadataDefinitionById = function (id) {
        for (var i = 0; i < self.metadataDefinitions().length; i++) {
            var metadataDefinition = self.metadataDefinitions()[i];
            if (metadataDefinition.id === id) {
                return metadataDefinition;
            }
        }
        return null;
    };

    self.createAssociatedMetadataDefinition = function (metadataDefinition) {
        var id = metadataDefinition.metadataDefinitionId;

        return {
            isBoundToTermSet: function() {
                return self.getMetadataDefinitionById(id).isBoundToTermSet;
            },
            defaultValues: function() {
                return self.getMetadataDefinitionById(id).defaultValues;
            },
            name: function() {
                return self.getMetadataDefinitionById(id).name;
            },
            displayName: function() {
                return self.getMetadataDefinitionById(id).displayName || self.getMetadataDefinitionById(id).name;
            },
            metadataDefinitionId: id,
            isReadOnlyMode: self.isReadOnlyMode,
            defaultValue: ko.observable(metadataDefinition.defaultValue),
            calculationCommandClassName: ko.observable(metadataDefinition.calculationCommandClassName),
            isReadOnlyOverride: ko.observable(metadataDefinition.isReadOnlyOverride),
            editableByUserGroups: ko.observable(metadataDefinition.editableByUserGroups),
            allUserGroupDataModels: ko.observableArray(metadataDefinition.allUserGroupDataModels),
        };
    };

    self.initMetadataDefinitions = function(metadataDefinitions) {
        metadataDefinitions.forEach(function(metadataDefinition) {
            self.metadataDefinitions.push({
                id: metadataDefinition.id,
                name: metadataDefinition.name,
                displayName: metadataDefinition.displayName || metadataDefinition.name,
                isBoundToTermSet: metadataDefinition.isBoundToTermSet,
                defaultValues: metadataDefinition.defaultValues
            });
        });
    };

    self.initAssociatedMetadataDefinitions = function() {
        self.associatedMetadataDefinitions.removeAll();
        model.associatedMetadataDefinitions.forEach(function(metadataDefinition) {
            self.associatedMetadataDefinitions.push(self.createAssociatedMetadataDefinition(metadataDefinition));
        });
    };

    self.getAssociatedMetadataDefinitionById = function (id) {
        for (var i = 0; i < model.associatedMetadataDefinitions.length; i++) {
            var metadataDefinition = self.metadataDefinitions()[i];
            if (metadataDefinition.id === id) {
                return metadataDefinition;
            }
        }
        return null;
    };

    self.filteredMetadataDefinitions = ko.computed(function () {

        var isNotAssociated = function(metadataDefinition) {
            var results = jQuery.grep(self.associatedMetadataDefinitions(),
                function(item) {
                    return item.metadataDefinitionId === metadataDefinition.id;
                });
            return results.length === 0;
        };

        var nameContainsSearchTerm = function(metadataDefinition) {
            return self.searchTerm() === "" ||
                metadataDefinition.name.toLowerCase().indexOf(self.searchTerm().toLowerCase()) > -1 ||
                metadataDefinition.displayName.toLowerCase().indexOf(self.searchTerm().toLowerCase()) > -1;
        };
        
        return self.metadataDefinitions()
            .filter(isNotAssociated)
            .filter(nameContainsSearchTerm);
    });

    self.save = function() {
        // don't let the user save twice in a row
        if (self.isSaving()) {
            return;
        }
        self.isSaving(true);

        var vm = new Object;
        vm.AssociatedMetadataDefinitions = ko.toJS(self.associatedMetadataDefinitions());
        vm.DocumentModelId = model.documentModelId;

        jQuery.ajax({
            type: "PUT",
            url: ko.baseUrl + "/DocumentModels/MetadataDefinitions",
            contentType: "application/json",
            dataType: "json",
            data: ko.toJSON(vm),
            success: function(data) {
                if (data.success === true) {
                    showAlert({ type: "success", message: data.message });
                } else {
                    showAlert({ type: "error", message: data.message });
                }
                self.isSaving(false);
            },
            error: function(jqXhr, textStatus, errorThrown) {
                showAlert({ type: "error", message: "HTTP error: " + errorThrown });
                self.isSaving(false);
            }
        });
    };

    self.cancel = function() {
        window.location.reload();
    };

    self.addMetadataDefinition = function(metadataDefinitionId) {
        self.associatedMetadataDefinitions.push(
            self.createAssociatedMetadataDefinition({
                metadataDefinitionId: metadataDefinitionId,
                defaultValue: "",
                calculationCommandClassName: "",
                isReadOnlyOverride: null,
                editableByUserGroups: "",
                allUserGroupDataModels: [],
            })
        );
    };

    self.deleteMetadataDefinition = function (metadataDefinition) {
        self.associatedMetadataDefinitions.remove(metadataDefinition);
    };

    self.showProgressBar = function() {
        window.clearTimeout(self.loadingProgressTimer);
        self.loadingProgressTimer = window.setTimeout(function() { self.progressBarVisible(true); }, 500);
    };

    self.hideProgressBar = function() {
        window.clearTimeout(self.loadingProgressTimer);
        self.progressBarVisible(false);
    };
}
