﻿using System;
using System.Diagnostics.CodeAnalysis;
using Eurolook.Data;
using Eurolook.Data.Models;

namespace Eurolook.ManagementConsole.ViewModels.DocumentModels
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    public class DocumentStructureViewModel
    {
        public DocumentStructureViewModel()
        {
            // called by ASP.NET MVC JSON serializer/deserializer
        }

        public DocumentStructureViewModel(DocumentStructure structure)
        {
            StructureId = structure.Id;
            BrickId = structure.BrickId;
            Name = structure.Brick.Name;
            PositionIndex = structure.VerticalPositionIndex;
            if (EnumHelper.IsStory(structure.Position, StoryType.Header)
                || EnumHelper.IsStory(structure.Position, StoryType.Footer))
            {
                IsFirstPage = EnumHelper.IsOccurrence(structure.Position, Occurrence.First);
                IsOddPages = EnumHelper.IsOccurrence(structure.Position, Occurrence.Odd);
                IsEvenPages = EnumHelper.IsOccurrence(structure.Position, Occurrence.Even);
            }

            IsAuto = structure.AutoBrickCreationType == AutoBrickCreationType.Always;
            AutoBrickCreationType = structure.AutoBrickCreationType;
            IsAutomatable = structure.Brick is ContentBrick;
        }

        public Guid? StructureId { get; set; }

        public Guid? BrickId { get; set; }

        public string Name { get; set; }

        public int PositionIndex { get; set; }

        public bool IsFirstPage { get; set; }

        public bool IsOddPages { get; set; }

        public bool IsEvenPages { get; set; }

        public bool IsAuto { get; set; }

        public AutoBrickCreationType AutoBrickCreationType { get; set; }

        public bool IsAutomatable { get; set; }
    }
}
