export class DocumentModelViewModel {

  id = ko.observable("");
  isSelected = ko.observable(false);
  name = ko.observable("");
  isClassicDocument = ko.observable(false);
  isHidden = ko.observable(false);

  constructor(model) {
    if (!model) return;

    this.id(model.id);
    this.name(model.name);
    this.isClassicDocument(model.isClassicDocument === true);
    this.isHidden(model.isHidden === true);
  }
}
