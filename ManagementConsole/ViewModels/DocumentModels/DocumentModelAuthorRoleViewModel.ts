export class DocumentModelAuthorRoleViewModel {

    isSelected = ko.observable(false);
    authorRoleId = ko.observable();
    authorRoleName = ko.observable<string>();
    description = ko.observable<string>();

    constructor(model) {
        if (!model) return;

        this.isSelected(model.isSelected);
        this.authorRoleId(model.authorRoleId);
        this.authorRoleName(model.authorRoleName);
        this.description(model.description);
    }
}
