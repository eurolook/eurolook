import { PageViewModel } from "PageViewModel"
import { DocumentModelLanguageViewModel } from "DocumentModels/DocumentModelLanguageViewModel";
import { DocumentModelAuthorRoleViewModel } from "DocumentModels/DocumentModelAuthorRoleViewModel";
import Utils = require("Utils")

class DocumentModelsEditViewModel extends PageViewModel {

    id: string;
    url: string;
    name = ko.observable("");
    displayName = ko.observable("");
    description = ko.observable("");
    keywords = ko.observable("");
    minVersion = ko.observable("");
    maxVersion = ko.observable("");
    previewInfo = ko.observable("");
    previewImage = ko.observable("");
    previewWithSampleTextInfo = ko.observable("");
    previewWithSampleTextImage = ko.observable("");
    hasPreview = ko.observable(false);
    hasPreviewWithSampleText = ko.observable("");
    templateInfo = ko.observable("");
    hasTemplate = ko.observable(false);
    documentModelLanguages = ko.observableArray<DocumentModelLanguageViewModel>();
    documentModelAuthorRoles = ko.observableArray<DocumentModelAuthorRoleViewModel>();
    isClassicDocument = ko.observable(false);
    isHidden = ko.observable(false);
    createDocumentWizardPages = ko.observable("");
    hideBrickChooser = ko.observable(false);
    categories = ko.observableArray();
    selectedCategoryId = ko.observable("00000000-0000-0000-0000-000000000000");
    uiPositionIndex = ko.observable(0);
    isSaving = ko.observable(false);
    previewFileFormData: FormData = null;
    previewWithSampleTextFileFormData: FormData = null;
    templateFileFormData: FormData = null;
    templateFile = null;

    constructor(baseUrl: string, id: string, isReadOnly: string) {
        super(baseUrl, "/DocumentModels", isReadOnly);
        this.id = id;
        this.url = baseUrl + "/DocumentModels/Edit/" + id;
        this.bindCtrlSave(this.save);
        this.load();
    }

    load() {
        this.showProgressSpinner();
        this.getJson(this.url, this.loadSucceeded, this);
    }

    loadSucceeded(result) {
        this.name(result.model.name);
        this.displayName(result.model.displayName);
        this.description(result.model.description);
        this.keywords(result.model.keywords);
        this.minVersion(result.model.minVersion);
        this.maxVersion(result.model.maxVersion);
        this.previewInfo(result.model.previewInfo);
        this.previewImage(result.model.previewImage);
        this.previewWithSampleTextInfo(result.model.previewWithSampleTextInfo);
        this.previewWithSampleTextImage(result.model.previewWithSampleTextImage);
        this.hasPreview(result.model.hasPreview);
        this.hasPreviewWithSampleText(result.model.hasPreviewWithSampleText);
        this.templateInfo(result.model.templateInfo);
        this.hasTemplate(result.model.hasTemplate);
        this.isClassicDocument(result.model.isClassicDocument);
        this.isHidden(result.model.isHidden);
        this.createDocumentWizardPages(result.model.createDocumentWizardPages);
        this.hideBrickChooser(result.model.hideBrickChooser);
        this.categories(result.model.categories);
        this.selectedCategoryId(result.model.selectedCategoryId);
        this.uiPositionIndex(result.model.uiPositionIndex);
        for (let lang of result.model.documentModelLanguages) {
            this.documentModelLanguages.push(new DocumentModelLanguageViewModel(lang));
        }
        for (let authorRole of result.model.documentModelAuthorRoles) {
            this.documentModelAuthorRoles.push(new DocumentModelAuthorRoleViewModel(authorRole));
        }

        this.hideProgressSpinner();
    }

    save() {
        if (this.isSaving()) return;
        this.isSaving(true);
        const pojo = ko.toJS(this);
        delete pojo.previewImage;
        delete pojo.previewWithSampleTextImage;
        delete pojo.categories;

        const requests: Array<JQueryXHR> = new Array();
        requests.push(this.postFormData(`${this.baseUrl()}/DocumentModels/Template/${this.id}`, this.templateFileFormData, this));
        requests.push(this.postFormData(`${this.baseUrl()}/DocumentModels/Preview/${this.id}`, this.previewFileFormData, this));
        requests.push(this.postFormData(`${this.baseUrl()}/DocumentModels/PreviewWithSampleText/${this.id}`, this.previewWithSampleTextFileFormData, this));
        requests.push(this.putJson(this.baseUrl() + "/DocumentModels/Edit/" + this.id, ko.toJSON(pojo), null, this));
        for (let dml of this.documentModelLanguages()) {
            if (dml.isModified()) {
                requests.push(this.postFormData(`${this.baseUrl()}/DocumentModels/LanguageTemplate/${this.id}?languageId=${dml.languageId()}`, dml.uploadedFile, this));
            }
        }

        jQuery.when.apply(jQuery, requests).then(this.saveSucceeded).fail(this.saveFailed);
    }

    saveSucceeded(templateResponse, previewResponse, previewWithSampleTextResponse, editResponse) {
        const that: DocumentModelsEditViewModel = this[0];
        const templateStatus = templateResponse[0];
        const previewStatus = previewResponse[0];
        const previewWithSampleTextStatus = previewWithSampleTextResponse[0];
        const editStatus = editResponse[0];
        if (templateStatus.success === true &&
            previewStatus.success === true &&
            previewWithSampleTextStatus.success === true &&
            editStatus.success === true) {
            that.previewImage(previewStatus.model);
            that.previewWithSampleTextImage(previewWithSampleTextStatus.model);
            that.showAlert({ type: "success", message: editStatus.message });
        } else {
            // Server side error
            let errorMsg = "";
            if (templateStatus.success === false)
                errorMsg += previewStatus.message;
            if (previewStatus.success === false)
                errorMsg += previewStatus.message;
            if (previewWithSampleTextStatus.success === false)
                errorMsg += previewWithSampleTextStatus.message;
            if (editStatus.success === false)
                errorMsg += editStatus.message;
            that.showAlert({ type: "error", message: errorMsg });
        }
        that.isSaving(false);
    }

    saveFailed(jqXhr) {
        this.showAlert({ type: "error", message: `HTTP error: ${jqXhr.statusText}` });
        this.isSaving(false);
    }

    onLanguageTemplateFileClick(linkElement: HTMLElement) {
        const prevElement = linkElement.previousElementSibling;
        if (prevElement.nodeName === "INPUT") {
            const inputElement = prevElement as HTMLInputElement;
            inputElement.click();
        }
    }

    setLanguageTemplateFile(documentModelLanguage: DocumentModelLanguageViewModel, file: File) {
        documentModelLanguage.templateNiceName(documentModelLanguage.getTemplateNiceName(file.name));
        documentModelLanguage.templateName(file.name);
        documentModelLanguage.uploadedFile = new FormData();
        documentModelLanguage.uploadedFile.append("file", file);
        documentModelLanguage.uploadedFile.append("fileName", file.name);
        documentModelLanguage.hasTemplate(true);
        documentModelLanguage.isModified(true);
    }

    chooseTemplateFile() {
        $("#templateFile").click();
    }

    choseTemplateFile(file) {
        this.templateFileFormData = new FormData();
        this.templateFileFormData.append("file", file);
        const fileName = this.name() + " " + Utils.getDateForFilename(new Date()) + ".dotx";
        this.templateFileFormData.append("fileName", fileName);
        this.templateInfo(fileName);
    }

    choosePreviewFile () {
        $("#previewFile").click();
    }

    chosePreviewFile(file) {
        this.previewFileFormData = new FormData();
        this.previewFileFormData.append("file", file);
        this.previewInfo(file.name);
        const reader = new FileReader();
        reader.onload = e => {
            this.previewImage(String(reader.result));
        };
        reader.readAsDataURL(file);
    }

    choosePreviewWithSampleTextFile() {
        $("#previewWithSampleTextFile").click();
    }

    chosePreviewWithSampleTextFile(file) {
        this.previewWithSampleTextFileFormData = new FormData();
        this.previewWithSampleTextFileFormData.append("file", file);
        this.previewWithSampleTextInfo(file.name);
        const reader = new FileReader();
        reader.onload = e => {
            this.previewWithSampleTextImage(String(reader.result));
        };
        reader.readAsDataURL(file);
    }
}
export = DocumentModelsEditViewModel
