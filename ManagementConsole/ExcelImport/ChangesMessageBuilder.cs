﻿using Eurolook.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eurolook.ManagementConsole.ExcelImport
{
    public enum ChangeMessageType {
        ModifiedAcronym,
        ModifiedLogicalLevel,
        ModifiedLocation,
        MovedDirectorate,
        MovedUnit,
        AddedDirectorate,
        AddedUnit,
        RemovedDirectorate,
        RemovedUnit,
        AddedTranslation,
        ModifiedTranslation,
        InvalidMessage
    };

    public class ChangeMessage
    {
        public ChangeMessageType Type;
        public string Message;
        public int Row;

        public ChangeMessage(int Row)
        {
            this.Row = Row;
        }

        static public ChangeMessage ForModifiedAcronym(string oldOrgaEntityName, string newOrgaEntityName, int Row)
        {
            var Mess = new ChangeMessage(Row);
            Mess.Type = ChangeMessageType.ModifiedAcronym;
            Mess.Message = string.Format("Acronym for entity {0} changed to {1}.", oldOrgaEntityName, newOrgaEntityName);

            return Mess;
        }

        static public ChangeMessage ForModifiedLogicalLevel(int oldLogicalLevel, OrgaEntity orgaEntity, int Row)
        {
            var Mess = new ChangeMessage(Row);
            Mess.Type = ChangeMessageType.ModifiedLogicalLevel;
            Mess.Message = string.Format("Logical level for entity {0} changed from {1} to {2}.", orgaEntity.Name, oldLogicalLevel, orgaEntity.LogicalLevel);

            return Mess;
        }

        static public ChangeMessage ForModifiedLocation(string oldLocation, string newLocation, OrgaEntity orgaEntity, int Row)
        {
            var Mess = new ChangeMessage(Row);
            Mess.Type = ChangeMessageType.ModifiedLocation;
            Mess.Message = string.Format("Location for entity {0} changed from {1} to {2}.", orgaEntity.Name, oldLocation, newLocation);

            return Mess;
        }

        static public ChangeMessage ForMovedDirectorate(string oldDGName, string newDGName, OrgaEntity orgaEntity, int Row)
        {
            var Mess = new ChangeMessage(Row);
            Mess.Type = ChangeMessageType.MovedDirectorate;
            Mess.Message = string.Format("Directorate {0} moved from directorate general {1} to directorate general {2}.", orgaEntity.Name, oldDGName, newDGName);

            return Mess;
        }

        static public ChangeMessage ForMovedUnit(string oldDirectorateName, string newDirectorateName, OrgaEntity orgaEntity, int Row)
        {
            var Mess = new ChangeMessage(Row);
            Mess.Type = ChangeMessageType.MovedUnit;
            Mess.Message = string.Format("Unit {0} moved from directorate {1} to directorate {2}.", orgaEntity.Name, oldDirectorateName, newDirectorateName);

            return Mess;
        }

        static public ChangeMessage ForAddedDirectorate(string newDGName, OrgaEntity orgaEntity, int Row)
        {
            var Mess = new ChangeMessage(Row);
            Mess.Type = ChangeMessageType.AddedDirectorate;
            Mess.Message = string.Format("Directorate {0} added in directorate general {1}.", orgaEntity.Name, newDGName);

            return Mess;
        }

        static public ChangeMessage ForAddedUnit(string newDirectorateName, OrgaEntity orgaEntity, int Row)
        {
            var Mess = new ChangeMessage(Row);
            Mess.Type = ChangeMessageType.AddedUnit;
            Mess.Message = string.Format("Unit {0} added in directorate {1}.", orgaEntity.Name, newDirectorateName);

            return Mess;
        }

        static public ChangeMessage ForRemovedDirectorate(int authorsCount, OrgaEntity orgaEntity, int Row)
        {
            var Mess = new ChangeMessage(Row);
            Mess.Type = ChangeMessageType.RemovedDirectorate;
            Mess.Message = string.Format("Directorate {0} is removed. {1} authors were attached.", orgaEntity.Name, authorsCount);

            return Mess;
        }

        static public ChangeMessage ForRemovedUnit(int authorsCount, OrgaEntity orgaEntity, int Row)
        {
            var Mess = new ChangeMessage(Row);
            Mess.Type = ChangeMessageType.RemovedUnit;
            Mess.Message = string.Format("Unit {0} is removed. {1} authors were attached.", orgaEntity.Name, authorsCount);

            return Mess;
        }

        static public ChangeMessage ForAddedTranslation(Translation[] translations, string[] langName, OrgaEntity orgaEntity, int Row)
        {
            var Mess = new ChangeMessage(Row);
            Mess.Type = ChangeMessageType.AddedTranslation;
            if (translations.Length == 1)
            {
                Mess.Message = string.Format("Name for entity {0} added in {1} : {2}.", orgaEntity.Name, langName[0], translations[0].Value);
               
            }
            else if (translations.Length < 5)
            {
                string langsString = langName[0];
                for (int i = 1; i < translations.Length - 1; i++)
                {
                    langsString += ", " + langName[i];
                }
                langsString += " and " + langName[translations.Length - 1];
                Mess.Message = string.Format("Name for entity {0} added in {1}.", orgaEntity.Name, langsString);
            }
            else
            {
                Mess.Message = string.Format("Name for entity {0} added in {1} languages.", orgaEntity.Name, translations.Length);
            }

            return Mess;
        }

        static public ChangeMessage ForModifiedTranslation(string[] oldTranslations, Translation[] translations, string[] langName, OrgaEntity orgaEntity, int Row)
        {
            var Mess = new ChangeMessage(Row);
            Mess.Type = ChangeMessageType.ModifiedTranslation;
            if (translations.Length == 1)
            {
                Mess.Message = string.Format("Name for entity {0} modified from {1} to {2} in {3}.", orgaEntity.Name, oldTranslations[0], translations[0].Value, langName[0]);

            }
            else if (translations.Length < 5)
            {
                string langsString = langName[0];
                for (int i = 1; i < translations.Length - 1; i++)
                {
                    langsString += ", " + langName[i];
                }
                langsString += " and " + langName[translations.Length - 1];
                Mess.Message = string.Format("Name for entity {0} modified in {1}.", orgaEntity.Name, langsString);
            }
            else
            {
                Mess.Message = string.Format("Name for entity {0} modified in {1} languages.", orgaEntity.Name, translations.Length);
            }

            return Mess;
        }
    }

    public class ChangesMessageBuilder
    {
        public List<ChangeMessage> messages;

        public ChangesMessageBuilder()
        {
            this.messages = new List<ChangeMessage>();
        }

        public void AddModifiedAcronym(string oldOrgaEntityName, string newOrgaEntityName, int Row)
        {
            this.messages.Add(ChangeMessage.ForModifiedAcronym(oldOrgaEntityName, newOrgaEntityName, Row));
        }

        public void AddModifiedLogicalLevel(int oldLogicalLevel, OrgaEntity orgaEntity, int Row)
        {
            this.messages.Add(ChangeMessage.ForModifiedLogicalLevel(oldLogicalLevel, orgaEntity, Row));
        }

        public void AddModifiedLocation(string oldLocation, string newLocation, OrgaEntity orgaEntity, int Row)
        {
            this.messages.Add(ChangeMessage.ForModifiedLocation(oldLocation, newLocation, orgaEntity, Row));
        }

        public void AddMovedDirectorate(string oldDGName, string newDGName, OrgaEntity orgaEntity, int Row)
        {
            this.messages.Add(ChangeMessage.ForMovedDirectorate(oldDGName, newDGName, orgaEntity, Row));
        }

        public void AddMovedUnit(string oldDirectorateName, string newDirectorateName, OrgaEntity orgaEntity, int Row)
        {
            this.messages.Add(ChangeMessage.ForMovedUnit(oldDirectorateName, newDirectorateName, orgaEntity, Row));
        }

        public void AddAddedDirectorate(string newDGName, OrgaEntity orgaEntity, int Row)
        {
            this.messages.Add(ChangeMessage.ForAddedDirectorate(newDGName, orgaEntity, Row));
        }

        public void AddAddedUnit(string newDirectorateName, OrgaEntity orgaEntity, int Row)
        {
            this.messages.Add(ChangeMessage.ForAddedUnit(newDirectorateName, orgaEntity, Row));
        }

        public void AddRemovedOrgaEntity(int authorsCount, OrgaEntity orgaEntity)
        {
            if (orgaEntity.LogicalLevel == 2)
            {
                this.messages.Add(ChangeMessage.ForRemovedDirectorate(authorsCount, orgaEntity, 0));
            }
            if (orgaEntity.LogicalLevel == 3)
            {
                this.messages.Add(ChangeMessage.ForRemovedUnit(authorsCount, orgaEntity, 0));
            }
        }

        public void AddAddedTranslation(Translation[] translations, string[] langName, OrgaEntity orgaEntity, int Row)
        {
            if (translations.Length == 0)
            {
                return;
            }
            this.messages.Add(ChangeMessage.ForAddedTranslation(translations, langName, orgaEntity, Row));
        }

        public void AddModifiedTranslation(string[] oldTranslations, Translation[] translations, string[] langName, OrgaEntity orgaEntity, int Row)
        {
            if (translations.Length == 0)
            {
                return;
            }
            if (translations.Length != oldTranslations.Length)
            {
                return;
            }
            this.messages.Add(ChangeMessage.ForModifiedTranslation(oldTranslations, translations, langName, orgaEntity, Row));
        }

        public string OutputString()
        {
            var output = "### Output start ###\n";

            foreach (ChangeMessage m in messages.OrderBy(o => o.Type).ThenBy(o => o.Row))
            {
                output += string.Format("{0} (Row #{1})\n", m.Message, m.Row);
            }

            return output + "### Output end ###";
        }
    }
}
