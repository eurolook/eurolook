﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Eurolook.Data.Models;

namespace Eurolook.ManagementConsole.ExcelImport
{
    internal static class PersonNameWorkbook
    {
        private const string FirstNameColumn = "D";
        private const string LastNameColumn = "C";
        private const string GroupColumn = "F";
        private const string CountryColumn = "G";

        private static SharedStringItem[] _sharedStringTableValues;

        public static List<PersonName> GetPersonInfo(string excelDocumentName)
        {
            var persons = new List<PersonName>();

            using (var document = SpreadsheetDocument.Open(excelDocumentName, false))
            {
                var workbookPart = document.WorkbookPart;
                var sheet = workbookPart.Workbook.Descendants<Sheet>().FirstOrDefault();
                if (sheet == null)
                {
                    return null;
                }

                var worksheetPart = (WorksheetPart)workbookPart.GetPartById(sheet.Id);
                _sharedStringTableValues = workbookPart.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>().ToArray();

                foreach (var row in worksheetPart.Worksheet.Descendants<Row>())
                {
                    int rowIndex = int.Parse(row.RowIndex.InnerText);
                    if (rowIndex == 1)
                    {
                        continue;
                    }

                    string firstName = GetCellValue(row, rowIndex, FirstNameColumn);
                    string lastName = GetCellValue(row, rowIndex, LastNameColumn);
                    string country = GetCellValue(row, rowIndex, CountryColumn);
                    string group = GetCellValue(row, rowIndex, GroupColumn);

                    if (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName) ||
                        string.IsNullOrEmpty(country))
                    {
                        continue;
                    }

                    if (country.TrimStart().Length >= 2)
                    {
                        country = country.TrimStart().Substring(0, 2);
                    }

                    persons.Add(
                        new PersonName
                        {
                            Country = country,
                            PoliticalGroup = group,
                            FirstName = firstName,
                            LastName = lastName,
                        });
                }
            }

            return persons;
        }

        private static string GetCellValue(OpenXmlElement topElement, int rowIndex, string columnName)
        {
            try
            {
                var cell =
                    topElement.Descendants<Cell>()
                              .FirstOrDefault(
                                  c => c.CellReference == columnName + rowIndex.ToString(CultureInfo.InvariantCulture));
                if (cell == null)
                {
                    return null;
                }

                if (cell.CellValue == null || cell.DataType == null)
                {
                    return null;
                }

                if (cell.DataType.Value != CellValues.SharedString)
                {
                    return null;
                }

                return _sharedStringTableValues[int.Parse(cell.CellValue.InnerText)].InnerText;
            }
            catch
            {
                return null;
            }
        }
    }
}
