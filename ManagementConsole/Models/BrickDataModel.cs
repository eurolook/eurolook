﻿using System;
using System.Diagnostics.CodeAnalysis;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    public class BrickDataModel
    {
        [UsedImplicitly]
        public BrickDataModel()
        {
            // default constructor is used by ASP.NET MVC for creating the model from a HTTP request
        }

        public BrickDataModel(Brick model)
        {
            Id = model.Id;
            Name = model.Name;
            DisplayName = model.DisplayName;
            BrickClass = model.GetType().ToString();
            IsContentBrick = model is ContentBrick;
            UiPositionIndex = model.UiPositionIndex;
            VisibleToUserGroups = model.VisibleToUserGroups;
            if (model.Category != null)
            {
                CategoryName = model.Category.Name;
            }

            if (model.Group != null)
            {
                GroupName = model.Group.Name;
                GroupId = model.GroupId;
            }
        }

        public Guid? Id { get; set; }

        public Guid? GroupId { get; set; }

        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string CategoryName { get; set; }

        public string GroupName { get; set; }

        public string VisibleToUserGroups { get; set; }

        public string BrickClass { get; set; }

        public bool IsContentBrick { get; set; }

        public int UiPositionIndex { get; set; }
    }
}
