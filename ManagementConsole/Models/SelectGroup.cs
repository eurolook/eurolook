using System.Collections.Generic;

namespace Eurolook.ManagementConsole.Models
{
    public class SelectGroup
    {
        public SelectGroup()
        {
            Options = new List<SelectOption>();
        }

        public string Label { get; set; }

        public List<SelectOption> Options { get; set; }
    }
}
