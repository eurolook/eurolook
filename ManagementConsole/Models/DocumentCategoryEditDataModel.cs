using System;

namespace Eurolook.ManagementConsole.Models
{
    public class DocumentCategoryEditDataModel
    {
        public Guid? Id { get; set; }

        public string Name { get; set; }
    }
}
