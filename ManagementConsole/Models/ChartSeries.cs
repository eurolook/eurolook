namespace Eurolook.ManagementConsole.Models
{
    public class ChartSeries : ChartSeries<int>
    {
    }

    public class ChartSeries<T>
    {
        public string Name { get; set; }

        public T[] Data { get; set; }

        public string Color { get; set; }
    }
}
