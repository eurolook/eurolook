using System.Collections.Generic;

namespace Eurolook.ManagementConsole.Models
{
    public class VersionUsageInfo
    {
        public string Version { get; set; }
        public int DeviceCount { get; set; }
        public Dictionary<string, VersionOwnerInfo> DeviceOwners { get; set; }
        public VersionUsageInfo()
        {
            DeviceOwners = new Dictionary<string, VersionOwnerInfo>();
        }
    }
}
