using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript.")]
    public class DeviceDataModel
    {
        [UsedImplicitly]
        public DeviceDataModel()
        {
            // default constructor is used by ASP.NET MVC for creating the model from a HTTP request
        }

        public DeviceDataModel(DeviceSettings deviceSettings, IEnumerable<UserGroup> userGroups, User user = null)
        {
            Id = deviceSettings.Id;
            DeviceName = deviceSettings.DeviceName;
            OperatingSystem = deviceSettings.OperatingSystem;
            OfficeVersion = deviceSettings.OfficeVersion;
            AddinVersion = deviceSettings.AddinVersion;

            Owner = deviceSettings.Owner;

            LastLogin = deviceSettings.LastLogin;
            IsTaskpaneVisible = deviceSettings.IsTaskpaneVisible;
            TaskpaneWidth = deviceSettings.TaskpaneWidth;

            ServerModificationTimeLocal = deviceSettings.ServerModificationTimeUtc.ToLocalTime();
            IsInitialised = deviceSettings.IsInitialised;

            QuickStartGuideShown = deviceSettings.QuickStartGuideShown;

            DotNetRuntimeVersion = deviceSettings.DotNetRuntimeVersion;
            DotNetFrameworkVersion = deviceSettings.DotNetFrameworkVersion;

            InstallRoot = deviceSettings.InstallRoot;
            IsPerMachineInstall = deviceSettings.IsPerMachineInstall;
            IsPerUserInstall = deviceSettings.IsPerUserInstall;

            AddInLoadTimesMilliseconds = deviceSettings.AddInLoadTimesMilliseconds;
            MaxAddInLoadTimeBucketSeconds = deviceSettings.MaxAddInLoadTimeBucketSeconds;

            IsTouchScreen = deviceSettings.IsTouchScreen;
            HasBattery = deviceSettings.HasBattery;
            ScreenWidth = deviceSettings.ScreenWidth;
            ScreenHeight = deviceSettings.ScreenHeight;
            ScreenDpi = deviceSettings.ScreenDpi;
            PhysicallyInstalledSystemMemoryGb = deviceSettings.PhysicallyInstalledSystemMemoryKb / 1024 / 1024;
            ProcessorName = deviceSettings.ProcessorName;

            IsRemoteWipeRequested = deviceSettings.IsRemoteWipeRequested;

            if (user != null)
            {
                User = new UserEditDataModel(user, userGroups);
            }
        }

        public Guid Id { get; set; }

        public string DeviceName { get; set; }

        public string OperatingSystem { get; set; }

        public string AddinVersion { get; set; }

        public string OfficeVersion { get; set; }

        public string Owner { get; set; }

        // Activity
        public DateTime? LastLogin { get; set; }

        public bool? IsTaskpaneVisible { get; set; }

        public int? TaskpaneWidth { get; set; }

        ////public bool ClientModification { get; set; }

        public DateTime ServerModificationTimeLocal { get; set; }

        public bool IsInitialised { get; set; }

        public string QuickStartGuideShown { get; set; }

        ////public string ActivityTrack { get; set; }

        // Deployment
        public string DotNetRuntimeVersion { get; set; }

        public string DotNetFrameworkVersion { get; set; }

        public string InstallRoot { get; set; }

        public bool IsPerUserInstall { get; set; }

        public bool IsPerMachineInstall { get; set; }

        public string AddInLoadTimesMilliseconds { get; set; }

        public float MaxAddInLoadTimeBucketSeconds { get; set; }

        // Hardware related properties
        public bool IsTouchScreen { get; set; }

        public bool HasBattery { get; set; }

        public int ScreenWidth { get; set; }

        public int ScreenHeight { get; set; }

        public double ScreenDpi { get; set; }

        public long PhysicallyInstalledSystemMemoryGb { get; set; }

        public string ProcessorName { get; set; }

        public bool IsRemoteWipeRequested { get; set; }

        public UserEditDataModel User { get; set; }
    }
}
