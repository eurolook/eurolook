using System;

namespace Eurolook.ManagementConsole.Models
{
    public class SelectOption
    {
        public string Name { get; set; }

        public Guid Value { get; set; }
    }
}
