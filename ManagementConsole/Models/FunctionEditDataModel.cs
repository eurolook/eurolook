﻿using System;
using System.Diagnostics.CodeAnalysis;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    public class FunctionEditDataModel
    {
        [UsedImplicitly]
        public FunctionEditDataModel()
        {
            // Used by ASP actions
        }

        public FunctionEditDataModel(JobFunction authorFunction)
        {
            Id = authorFunction.Id;
            Text = authorFunction.Function;
            LanguageId = authorFunction.LanguageId;
        }

        public Guid? Id { get; set; }

        public string Text { get; set; }

        public Guid LanguageId { get; set; }
    }
}
