namespace Eurolook.ManagementConsole.Models
{
    public class MultiSelectOption
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public bool Checked { get; set; } = false;
    }
}
