using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Common.Extensions;

namespace Eurolook.ManagementConsole.Models
{
    public class ActionLogInfoHistory
    {
        public ActionLogInfoHistory(DateTime fromDate)
        {
            Dates = new List<DateTime>();
            Counts = new Dictionary<Guid, HistoryCountEntry>();

            for (var month = fromDate.ToDateType(DateTimeExtensions.DateGroupType.Month);
                 month <= DateTime.UtcNow.ToDateType(DateTimeExtensions.DateGroupType.Month);
                 month = month.AddMonths(1))
            {
                Dates.Add(month);
            }
        }

        public List<DateTime> Dates { get; set; }

        public Dictionary<Guid, HistoryCountEntry> Counts { get; set; }

        public Dictionary<string, HistoryCountEntry> CountsByName
        {
            get => Counts.Values.ToDictionary(entry => entry.Name);
        }

        public string[] Categories => Dates.Select(x => x.ToString("y")).ToArray();
    }
}
