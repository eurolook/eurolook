using System;

namespace Eurolook.ManagementConsole.Models
{
    public class FeedbackInfo
    {
        public DateTime Submitted { get; set; }

        public string User { get; set; }

        public string Rating { get; set; }

        public string Message { get; set; }
    }
}
