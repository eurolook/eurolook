using System;
using System.Diagnostics.CodeAnalysis;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    public class PredefinedFunctionDataModel
    {
        [UsedImplicitly]
        public PredefinedFunctionDataModel()
        {
            // default constructor is used by ASP.NET MVC for creating the model from a HTTP request
        }

        public PredefinedFunctionDataModel(PredefinedFunction model)
        {
            if (model != null)
            {
                Name = model.Name;
                Id = model.Id;
                ShowInSignature = model.ShowInSignature;
                ShowInHeader = model.ShowInHeader;
                IsSuperior = model.IsSuperior;
                FunctionTextId = model.FunctionTextId;
                FunctionFemaleTextId = model.FunctionFemaleTextId;
                FunctionHeaderTextId = model.FunctionHeaderTextId;
                FunctionHeaderFemaleTextId = model.FunctionHeaderFemaleTextId;
            }
        }

        public Guid? Id { get; set; }

        public bool ShowInSignature { get; set; }

        public bool ShowInHeader { get; set; }

        public bool IsSuperior { get; set; }

        public string Name { get; set; }

        public Guid? FunctionTextId { get; set; }

        public Guid? FunctionFemaleTextId { get; set; }

        public Guid? FunctionHeaderTextId { get; set; }

        public Guid? FunctionHeaderFemaleTextId { get; set; }
    }
}
