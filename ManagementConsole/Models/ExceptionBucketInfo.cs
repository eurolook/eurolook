using System;
using System.Collections.Generic;

namespace Eurolook.ManagementConsole.Models
{
    public class ExceptionBucketInfo
    {
        public byte[] ExceptionBucketId { get; set; }
        public string LastMessage { get; set; }
        public int Count { get; set; }
        public string StackTrace { get; set; }
        public DateTime LastOccurred { get; set; }
        public int AffectedUsers { get; set; }
        public IEnumerable<ExceptionEntryInfo> Entries { get; set; }
    }
}
