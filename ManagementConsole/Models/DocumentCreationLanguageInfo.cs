namespace Eurolook.ManagementConsole.Models
{
    public class DocumentCreationLanguageInfo
    {
        public string LanguageName { get; set; }

        public int CreationCount { get; set; }
    }
}
