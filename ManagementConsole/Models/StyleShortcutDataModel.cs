using System;
using Eurolook.Data.Models;

namespace Eurolook.ManagementConsole.Models
{
    public class StyleShortcutDataModel
    {
        public StyleShortcutDataModel()
        {
        }

        public StyleShortcutDataModel(StyleShortcut styleShortcut)
        {
            Id = styleShortcut.Id;
            Shortcut = styleShortcut.Shortcut;
            StyleName = styleShortcut.StyleName;
        }

        public Guid? Id { get; set; }

        public string Shortcut { get; set; }

        public string StyleName { get; set; }
    }
}
