﻿using System;

namespace Eurolook.ManagementConsole.Models
{
    public class ExceptionLogInfo
    {
        public byte[] ExceptionBucketId { get; set; }

        public string LoggerName { get; set; }

        public string ExceptionType { get; set; }

        public string StackTrace { get; set; }

        public DateTime CreatedUtc { get; set; }

        public string Message { get; set; }

        public Guid OriginatorId { get; set; }

        public string Originator { get; set; }

        public string ClientVersion { get; set; }

        public string OfficeVersion { get; set; }
    }
}
