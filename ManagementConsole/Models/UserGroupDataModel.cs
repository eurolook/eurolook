using System;
using Eurolook.Data.Models;

namespace Eurolook.ManagementConsole.Models
{
    public class UserGroupDataModel
    {
        public UserGroupDataModel()
        {
            // default constructor is used by ASP.NET MVC for creating the model from a POST request
        }

        public UserGroupDataModel(UserGroup userGroup)
        {
            Id = userGroup.Id;
            Name = userGroup.Name;
            Description = userGroup.Description;
            UiPositionIndex = userGroup.UiPositionIndex;
            AdGroups = userGroup.AdGroups;
        }

        public bool IsSelected { get; set; }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int UiPositionIndex { get; set; }

        public string AdGroups { get; set; }
    }
}
