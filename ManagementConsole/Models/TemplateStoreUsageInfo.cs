using System;
using System.Collections.Generic;
using System.Linq;

namespace Eurolook.ManagementConsole.Models
{
    public class TemplateStoreUsageInfo
    {
        public string TemplateName { get; set; }

        public int CreationCount { get; set; }
    }
}
