using System;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.Models
{
    public class LocalisedResourceDataModel
    {
        [UsedImplicitly]
        public LocalisedResourceDataModel()
        {
            // default constructor is used by ASP.NET MVC for creating the model from a POST request
        }

        public LocalisedResourceDataModel(LocalisedResource localisedResource)
        {
            HasData = localisedResource != null && localisedResource.RawData != null;
            MimeType = localisedResource?.MimeType;
            FileNameDate = localisedResource?.FileNameDate;
            RawData = localisedResource?.RawData;
        }

        public Guid LanguageId { get; set; }

        public Language Language { get; set; }

        public string MimeType { get; set; }

        public string FileNameDate { get; set; }

        public byte[] RawData { get; set; }

        public bool HasData { get; set; }
    }
}
