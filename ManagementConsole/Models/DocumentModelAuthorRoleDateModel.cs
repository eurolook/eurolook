using System;
using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.Models
{
    public class DocumentModelAuthorRoleDateModel
    {
        [UsedImplicitly]
        public DocumentModelAuthorRoleDateModel()
        {
            // default constructor is used by ASP.NET MVC for creating the model from a POST request
        }

        public bool IsSelected { get; set; }

        public Guid AuthorRoleId { get; set; }

        public string AuthorRoleName { get; set; }

        public string Description { get; set; }
    }
}
