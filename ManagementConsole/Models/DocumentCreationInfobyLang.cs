using System.Collections.Generic;
using System.Linq;

namespace Eurolook.ManagementConsole.Models
{
    public class DocumentCreationInfobyLang
    {
        public DocumentCreationInfobyLang()
        {
            DocumentCreations = new Dictionary<string, DocumentCreationLanguageInfo>();
        }

        public string DocumentModelName { get; set; }

        public Dictionary<string, DocumentCreationLanguageInfo> DocumentCreations { get; set; }

        public int TotalCreationCount
        {
            get { return DocumentCreations.Sum(kvp => kvp.Value.CreationCount); }
        }
    }
}
