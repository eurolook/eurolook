using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.Models
{
    public class PieChartSegment
    {
        [UsedImplicitly]
        public string Name { get; set; }

        [UsedImplicitly]
        public double Y { get; set; }
    }
}
