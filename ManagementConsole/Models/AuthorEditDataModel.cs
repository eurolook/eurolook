using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.ViewModels.OrgaChart;
using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    public class AuthorEditDataModel
    {
        [UsedImplicitly]
        public AuthorEditDataModel()
        {
            // Used by ASP actions
            Workplaces = new List<WorkplaceEditDataModel>();
            JobAssignments = new List<JobAssignmentDataModel>();
            PredefinedFunctions = new List<PredefinedFunctionDataModel>();
            Addresses = new List<Address>();
            Languages = new List<Language>();
            OrgaTree = new List<OrgaEntityViewModel>();
        }

        public AuthorEditDataModel(Author author, IList<Language> languages)
            : this()
        {
            if (author == null)
            {
                return;
            }

            // personal data
            LatinFirstName = author.LatinFirstName;
            LatinLastName = author.LatinLastName;
            GreekFirstName = author.GreekFirstName;
            GreekLastName = author.GreekLastName;
            CyrillicFirstName = author.BulgarianFirstName;
            CyrillicLastName = author.BulgarianLastName;
            Gender = author.Gender;
            Initials = author.Initials;
            Email = author.Email;
            Languages.AddRange(languages);

            // job assignments
            JobAssignments.Add(new JobAssignmentDataModel(author, languages));
            foreach (var jobAssignment in author.JobAssignments)
            {
                var jobVm = new JobAssignmentDataModel(jobAssignment, languages);
                JobAssignments.Add(jobVm);
            }

            // workplaces
            if (author.Workplaces != null)
            {
                foreach (var workplace in author.Workplaces)
                {
                    var workplaceDataModel = new WorkplaceEditDataModel(workplace)
                    {
                        IsPrimaryWorkplace = workplace.Id == author.MainWorkplaceId,
                    };
                    Workplaces.Add(workplaceDataModel);
                }
            }

            // linked accounts
#pragma warning disable CS0618 // Type or member is obsolete

            // Kept for backwards compatibility to be able to view/edit the fields for older clients
            SharePointAccountId = author.SharePointAccountId;
            SharePointLoginName = author.SharePointLoginName;
#pragma warning restore CS0618 // Type or member is obsolete
        }

        public string LatinFirstName { get; set; }

        public string LatinLastName { get; set; }

        public string GreekFirstName { get; set; }

        public string GreekLastName { get; set; }

        public string CyrillicFirstName { get; set; }

        public string CyrillicLastName { get; set; }

        public string Initials { get; set; }

        public string Email { get; set; }

        public string Gender { get; set; }

        public int? SharePointAccountId { get; set; }

        public string SharePointLoginName { get; set; }

        public List<WorkplaceEditDataModel> Workplaces { get; set; }

        public List<JobAssignmentDataModel> JobAssignments { get; set; }

        public List<Language> Languages { get; set; }

        public List<OrgaEntityViewModel> OrgaTree { get; set; }

        public List<PredefinedFunctionDataModel> PredefinedFunctions { get; set; }

        public List<Address> Addresses { get; set; }

        public async Task LoadDataAsync(AuthorsDatabase db)
        {
            await db.LoadOrgaTreeAsync(OrgaTree);

            PredefinedFunctions.Clear();
            PredefinedFunctions.Add(
                new PredefinedFunctionDataModel
                {
                    Id = null,
                    Name = "Custom function...",
                });
            await db.LoadPredefinedFunctions(PredefinedFunctions);

            Addresses.Clear();
            Addresses.AddRange(await db.GetAddressesAsync(true));
        }
    }
}
