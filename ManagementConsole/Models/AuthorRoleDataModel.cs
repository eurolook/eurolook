namespace Eurolook.ManagementConsole.Models
{
    public class AuthorRoleDataModel
    {
        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string Description { get; set; }

        public int UiPositionIndex { get; set; }
    }
}
