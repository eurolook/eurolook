using System.Linq;

namespace Eurolook.ManagementConsole.Models
{
    public class HistoryCountEntry
    {
        public HistoryCountEntry(int capacity)
        {
            Count = new int[capacity];
        }

        public string Name { get; set; }
        public int[] Count { get; }

        public int TotalCount
        {
            get { return Count.Sum(); }
        }
    }
}
