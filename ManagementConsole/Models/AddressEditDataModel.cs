using System;

namespace Eurolook.ManagementConsole.Models
{
    public class AddressEditDataModel
    {
        public Guid? Id { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public string LocationTextAlias { get; set; }

        public string FooterTextAlias { get; set; }

        public string NameTextAlias { get; set; }

        public bool IsDefault { get; set; }

        public string ActiveDirectoryReference { get; set; }
    }
}
