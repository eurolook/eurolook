using System;
using System.Diagnostics.CodeAnalysis;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    public class OrgaEntityDataModel
    {
        [UsedImplicitly]
        public OrgaEntityDataModel()
        {
            // default constructor for ASP.NET MVC
        }

        public OrgaEntityDataModel(OrgaEntity model)
        {
            Id = model.Id;
            SuperEntityId = model.SuperEntityId;
            SuperEntity = model.SuperEntity;
            PrimaryAddressId = model.PrimaryAddressId;
            PrimaryAddress = model.PrimaryAddress;
            SecondaryAddressId = model.SecondaryAddressId;
            SecondaryAddress = model.SecondaryAddress;
            LogicalLevel = model.LogicalLevel;
            Name = model.Name;
            HeaderText = model.HeaderText;
            HeaderTextId = model.HeaderTextId;
            Header = model.Header;
            WebAddress = model.WebAddress;
            OrderIndex = model.OrderIndex;
            IsNew = model.IsNew;
            WasDeleted = model.WasDeleted;
            ShowInHeader = model.ShowInHeader;
            Function = model.Function;
            Gender = model.Gender;
        }

        public Guid? Id { get; set; }

        public Guid? SuperEntityId { get; set; }

        public OrgaEntity SuperEntity { get; set; }

        public Guid? PrimaryAddressId { get; set; }

        public Address PrimaryAddress { get; set; }

        public Guid? SecondaryAddressId { get; set; }

        public Address SecondaryAddress { get; set; }

        public int LogicalLevel { get; set; }

        public string Name { get; set; }

        public Text HeaderText { get; set; }

        public Guid? HeaderTextId { get; set; }

        public Translation Header { get; set; }

        public string WebAddress { get; set; }

        public int OrderIndex { get; set; }

        public bool IsNew { get; set; }

        public bool WasDeleted { get; set; }

        public bool ShowInHeader { get; set; }

        public string Function { get; set; }

        public string Gender { get; set; }
    }
}
