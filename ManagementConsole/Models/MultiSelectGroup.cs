using System.Collections.Generic;

namespace Eurolook.ManagementConsole.Models
{
    public class MultiSelectGroup
    {
        public MultiSelectGroup()
        {
            Options = new List<MultiSelectOption>();
        }

        public string Label { get; set; }
        public List<MultiSelectOption> Options { get; set; }
    }
}
