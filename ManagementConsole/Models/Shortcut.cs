using System;
using System.Collections.Generic;
using System.Linq;

namespace Eurolook.ManagementConsole.Models
{
    public class Shortcut
    {
        public string Name { get; set; }

        public string Target { get; set; }
    }
}
