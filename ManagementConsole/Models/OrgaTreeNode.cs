﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Database;

namespace Eurolook.ManagementConsole.Models
{
    public class OrgaTreeNode
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public List<OrgaTreeNode> Children { get; set; }
        public int LogicalLevel { get; set; }
        public int OrderIndex { get; set; }


        public OrgaTreeNode()
        {
            Children = new List<OrgaTreeNode>();
        }

        public OrgaTreeNode(Author author)
        {
            Id = author.Id.ToString();
            DisplayName = string.Format("{0} {1}", author.LatinLastName, author.LatinFirstName);
            if (string.IsNullOrWhiteSpace(DisplayName))
            {
                DisplayName = Id;
            }
            LogicalLevel = 4;
            Children = new List<OrgaTreeNode>();
        }

        public OrgaTreeNode(OrgaEntity orgaEntity)
        {
            Id = orgaEntity.Id.ToString();
            if (orgaEntity.Name == null || string.IsNullOrWhiteSpace(orgaEntity.Name.Value))
            {
                DisplayName = "[Not Translated]";
            }
            else
            {
                DisplayName = orgaEntity.Name.ToString();
            }
            LogicalLevel = orgaEntity.LogicalLevel;
            Children = new List<OrgaTreeNode>();
            OrderIndex = orgaEntity.OrderIndex;
        }

        public static OrgaTreeNode FromOrgaTree(OrgaEntity orgaEntity, int authorCount = 0)
        {
            var node = new OrgaTreeNode(orgaEntity);

            // children
            if (orgaEntity.SubEntities != null && orgaEntity.SubEntities.Any())
            {
                node.Children.AddRange(orgaEntity.SubEntities.Select(o => FromOrgaTree(o, authorCount)).ToList());
            }

            // authors
            if (authorCount > 0)
            {
                var db = new OrgaChartDatabase();
                node.Children.AddRange(db.GetAuthorsNodes(orgaEntity.Id, authorCount));
            }

            return node;
        }
    }
}
