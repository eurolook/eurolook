using System;

namespace Eurolook.ManagementConsole.Models
{
    public class OrgChartCleanupDataModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Texts { get; set; }
        public int Translations { get; set; }
    }
}
