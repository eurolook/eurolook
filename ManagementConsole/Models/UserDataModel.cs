using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.Models;

namespace Eurolook.ManagementConsole.Models
{
    public class UserDataModel
    {
        public UserDataModel(User user, IEnumerable<UserGroup> userGroups)
        {
            Id = user.Id;
            CreatedDate = user.CreatedTimeUtc.ToLocalTime();
            Login = user.Login;
            DisplayName = user.Self?.LatinLastName == null && user.Self?.LatinFirstName == null
                ? ""
                : $"{user.Self.LatinLastName}, {user.Self.LatinFirstName}";

            Service = user.Self?.Service;
            Email = user.Self?.Email;
            Initials = user.Self?.Initials;

            var userGroupNames = userGroups.Where(ug => user.IsMemberOfUserGroup(ug.Id)).Select(ug => ug.Name);
            UserGroups = string.Join(", ", userGroupNames);

            var osList = user.Settings
                             .DeviceSettings
                             .Select(d => d.OperatingSystem)
                             .Where(o => o != null)
                             .Select(
                                 o => o.Replace("Microsoft ", "")
                                       .Replace(" Enterprise", "")
                                       .Replace(" Pro", "")
                                       .Replace(" Home", ""))
                             .Distinct()
                             .ToArray();

            OperatingSystems = string.Join(", ", osList);

            var versionList = user.Settings
                                  .DeviceSettings
                                  .Select(d => d.AddinVersion)
                                  .Where(a => a != null)
                                  .Select(a => a.Replace("10.0", ""))
                                  .Distinct()
                                  .ToArray();

            AddinVersions = string.Join(", ", versionList);
        }

        public Guid Id { get; }

        public DateTime CreatedDate { get; }

        public string Login { get; }

        public string DisplayName { get; }

        public string Service { get; }

        public string Email { get; }

        public string Initials { get; }

        public string UserGroups { get; }

        public string OperatingSystems { get; }

        public string AddinVersions { get; }
    }
}
