using System;

namespace Eurolook.ManagementConsole.Models
{
    public class ActionLogInfo
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public int HotKeyCount { get; set; }

        public int ClickCount { get; set; }

        public int HandlerCount { get; set; }

        public int RibbonCount { get; set; }

        public int TotalCount
        {
            get { return HotKeyCount + ClickCount + HandlerCount + RibbonCount; }
        }
    }
}
