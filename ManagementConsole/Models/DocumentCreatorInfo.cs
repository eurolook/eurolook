﻿namespace Eurolook.ManagementConsole.Models
{
    public class DocumentCreatorInfo
    {
        public string CreatorName { get; set; }

        public int CreationCount { get; set; }
    }
}
