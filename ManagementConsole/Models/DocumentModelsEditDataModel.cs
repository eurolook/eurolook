﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Eurolook.Common;
using Eurolook.Data.Models;
using Eurolook.Data.Models.AuthorRoles;
using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    public class DocumentModelsEditDataModel
    {
        private string _description;

        [UsedImplicitly]
        public DocumentModelsEditDataModel()
        {
            // default constructor is used by ASP.NET MVC for creating the model from a POST request
        }

        public DocumentModelsEditDataModel(
            DocumentModel model,
            IEnumerable<Language> allLanguages,
            IEnumerable<DocumentCategory> allDocumentCategories,
            IEnumerable<AuthorRole> allAuthorRoles)
        {
            Id = model.Id;
            Name = model.Name;
            DisplayName = model.DisplayName;
            Description = model.Description;
            Keywords = model.Keywords;
            Categories = new List<DocumentCategory>
            {
                new DocumentCategory
                {
                    Id = Guid.Empty,
                    Name = "-",
                },
            };
            Categories.AddRange(allDocumentCategories);
            SelectedCategoryId = model.DocumentCategoryId;

            UIPositionIndex = model.UiPositionIndex;

            MinVersion = model.MinVersion;
            MaxVersion = model.MaxVersion;

            if (model.PreviewImage != null)
            {
                PreviewInfo = model.PreviewImageFileName;
                PreviewImage = Tools.GetBase64Image(model.PreviewImage);
                HasPreview = true;
            }

            if (model.PreviewImageWithSampleText != null)
            {
                PreviewWithSampleTextInfo = model.PreviewImageWithSampleTextFileName;
                PreviewWithSampleTextImage = Tools.GetBase64Image(model.PreviewImageWithSampleText);
                HasPreviewWithSampleText = true;
            }

            if ((model.Template != null) && (model.Template.Length > 0))
            {
                TemplateInfo = model.TemplateFileName;
                HasTemplate = true;
            }

            IsClassicDocument = model.IsClassicDocument ?? false;
            IsHidden = model.IsHidden ?? false;
            CreateDocumentWizardPages = model.CreateDocumentWizardPages;
            HideBrickChooser = model.HideBrickChooser ?? false;

            DocumentModelLanguages = new List<DocumentModelLanguageDataModel>();
            foreach (var lang in allLanguages.OrderBy(l => l.DisplayName))
            {
                var documentModelLanguage = model.DocumentLanguages.FirstOrDefault(x => !x.Deleted && x.DocumentLanguageId == lang.Id);
                DocumentModelLanguages.Add(
                    new DocumentModelLanguageDataModel(documentModelLanguage)
                    {
                        LanguageId = lang.Id,
                        LanguageName = lang.DisplayName,
                    });
            }

            FillDocumentModelAuthorRoles(model, allAuthorRoles);
        }

        private void FillDocumentModelAuthorRoles(DocumentModel model, IEnumerable<AuthorRole> allAuthorRoles)
        {
            bool IsAuthorRoleSelected(AuthorRole ar) => model.AuthorRoles
                                                             ?.Any(x => !x.Deleted && x.AuthorRoleId == ar.Id) == true;

            string GetDescription(AuthorRole ar) =>
                model.AuthorRoles.Where(x => !x.Deleted && x.AuthorRoleId == ar.Id).Select(x => x.Description)
                     .FirstOrDefault();

            DocumentModelAuthorRoles = allAuthorRoles.OrderBy(ar => ar.UiPositionIndex)
                                                     .Select(
                                                         ar => new DocumentModelAuthorRoleDateModel
                                                         {
                                                             AuthorRoleId = ar.Id,
                                                             AuthorRoleName = ar.DisplayName ?? ar.Name,
                                                             IsSelected = IsAuthorRoleSelected(ar),
                                                             Description = GetDescription(ar),
                                                         })
                                                     .ToList();
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string Description
        {
            get { return _description; }
            set { _description = string.IsNullOrWhiteSpace(value) ? null : value; }
        }

        public string Keywords { get; set; }

        public string PreviewInfo { get; set; }

        public string PreviewImage { get; set; }

        public string PreviewWithSampleTextInfo { get; set; }

        public string PreviewWithSampleTextImage { get; set; }

        public bool HasPreview { get; }

        public bool HasPreviewWithSampleText { get; }

        public string TemplateInfo { get; set; }

        public bool HasTemplate { get; }

        public bool IsClassicDocument { get; set; }

        public bool IsHidden { get; set; }

        public string CreateDocumentWizardPages { get; set; }

        public bool HideBrickChooser { get; set; }

        [RegularExpression(@"\d{1,5}\.\d{1,5}\.\d{1,5}\.\d{1,5}", ErrorMessage = "The field MinVersion must be in the format 1.0.0.0")]
        public string MinVersion { get; set; }

        [RegularExpression(@"\d{1,5}\.\d{1,5}\.\d{1,5}\.\d{1,5}", ErrorMessage = "The field MaxVersion must be in the format 1.0.0.0")]
        public string MaxVersion { get; set; }

        public IList<DocumentModelLanguageDataModel> DocumentModelLanguages { get; set; }

        public IList<DocumentModelAuthorRoleDateModel> DocumentModelAuthorRoles { get; set; }

        public List<DocumentCategory> Categories { get; set; }

        public Guid? SelectedCategoryId { get; set; }

        public int UIPositionIndex { get; set; }
    }
}
