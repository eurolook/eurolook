﻿using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.Models
{
    public class VersionOwnerInfo
    {
        [UsedImplicitly]
        public string Name { get; set; }

        public int DeviceCount { get; set; }
    }
}
