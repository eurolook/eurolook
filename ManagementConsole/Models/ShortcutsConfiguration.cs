using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.Database;

namespace Eurolook.ManagementConsole.Models
{
    public class ShortcutsConfiguration : ISystemConfigurationSetting
    {
        public string Key => "ShortcutsConfiguration";

        public List<ShortcutsCategory> Categories { get; set; }
    }
}
