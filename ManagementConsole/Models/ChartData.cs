using System.Collections.Generic;

namespace Eurolook.ManagementConsole.Models
{
    public class ChartData : ChartData<int>
    {
    }

    public class ChartData<T>
    {
        public ChartData()
        {
            Categories = new List<string>();
            Series = new List<ChartSeries<T>>();
        }

        public string Title { get; set; }

        public List<string> Categories { get; set; }

        public List<ChartSeries<T>> Series { get; set; }
    }
}
