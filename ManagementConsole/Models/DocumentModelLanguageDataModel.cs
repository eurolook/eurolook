using System;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.Models
{
    public class DocumentModelLanguageDataModel
    {
        [UsedImplicitly]
        public DocumentModelLanguageDataModel()
        {
            // default constructor is used by ASP.NET MVC for creating the model from a POST request
        }

        public DocumentModelLanguageDataModel(DocumentModelLanguage documentModelLanguage)
        {
            IsSelected = documentModelLanguage != null;
            TemplateName = documentModelLanguage?.TemplateFileName;
            HasTemplate = documentModelLanguage?.Template != null && documentModelLanguage?.Template.Length > 0;
        }

        public bool IsSelected { get; set; }

        public Guid LanguageId { get; set; }

        public string LanguageName { get; set; }

        public string TemplateName { get; set; }

        public bool HasTemplate { get; set; }
    }
}
