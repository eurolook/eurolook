using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript."),
    ]
    public class UserEditDataModel
    {
        [UsedImplicitly]
        public UserEditDataModel()
        {
            DeviceSettings = new List<DeviceDataModel>();
        }

        public UserEditDataModel(User user, IEnumerable<UserGroup> userGroups)
        {
            Id = user.Id;
            Login = user.Login;
            AuthorId = user.SelfId;
            CreatedDate = user.CreatedTimeUtc.ToLocalTime();
            if (user.Settings != null)
            {
                DataSyncInterval = user.Settings.DataSyncIntervalMinutes;
                DataSyncDelay = user.Settings.DataSyncDelayMinutes;
                IsCepEnabled = user.Settings.IsCepEnabled;
                DeviceSettings = user.Settings.DeviceSettings.Select(d => new DeviceDataModel(d, userGroups)).ToList();
                IsInsertNameInline = user.Settings.IsInsertNameInline;
            }

            UserGroups = userGroups
                         .Select(
                             ug => new UserGroupDataModel(ug)
                             {
                                 IsSelected = user.IsMemberOfUserGroup(ug.Id),
                             })
                         .OrderBy(ug => ug.UiPositionIndex)
                         .ToList();
        }

        public Guid Id { get; set; }

        public string Login { get; set; }

        public int DataSyncInterval { get; set; }

        public int DataSyncDelay { get; set; }

        public bool IsCepEnabled { get; set; }

        public bool IsInsertNameInline { get; set; }

        public List<UserGroupDataModel> UserGroups { get; set; }

        public List<DeviceDataModel> DeviceSettings { get; set; }

        public DateTime CreatedDate { get; }

        public Guid AuthorId { get; set; }
    }
}
