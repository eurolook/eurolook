using System;
using System.Collections.Generic;
using System.Linq;

namespace Eurolook.ManagementConsole.Models
{
    public class DocumentCreationInfobyDg
    {
        public DocumentCreationInfobyDg()
        {
            DocumentCreations = new Dictionary<Guid, DocumentModelCreationInfo>();
        }

        public string DgName { get; set; }

        public Dictionary<Guid, DocumentModelCreationInfo> DocumentCreations { get; set; }

        public int TotalCreationCount
        {
            get { return DocumentCreations.Sum(kvp => kvp.Value.CreationCount); }
        }

        public Dictionary<Guid, DocumentModelCreationInfo> AggregatedDocumentCreations(int take = 10)
        {
            if (DocumentCreations.Count <= take)
            {
                return DocumentCreations;
            }

            var ordered = DocumentCreations
                          .OrderByDescending(g => g.Value.CreationCount)
                          .ToList();

            var aggregateCount = new DocumentModelCreationInfo
            {
                DocumentModelName = "others",
                CreationCount = ordered
                                .Skip(take)
                                .Sum(g => g.Value.CreationCount),
            };

            var result = ordered.Take(take).ToDictionary(g => g.Key, g => g.Value);
            result[Guid.NewGuid()] = aggregateCount;

            return result;
        }
    }
}
