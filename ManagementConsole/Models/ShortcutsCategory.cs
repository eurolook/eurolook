using System;
using System.Collections.Generic;
using System.Linq;

namespace Eurolook.ManagementConsole.Models
{
    public class ShortcutsCategory
    {
        public string Name { get; set; }

        public List<Shortcut> Shortcuts { get; set; }
    }
}
