using System;
using System.Collections.Generic;
using Eurolook.Data;

namespace Eurolook.ManagementConsole.Models
{
    public class DataPage<T> : IPaginationInfo
    {
        private uint _totalCount;
        private uint _pageCount;

        public DataPage(uint pageNumber, uint pageSize, bool orderAsc, string orderProperty)
        {
            PageSize = pageSize;
            PageNumber = pageNumber;
            From = PageNumber * PageSize - PageSize;
            To = PageNumber * PageSize - 1;
            Data = new List<T>();
            OrderAscending = orderAsc;
            OrderProperty = orderProperty;
        }

        public uint PageNumber { get; set; }

        public uint From { get; set; }

        public uint To { get; set; }

        public List<T> Data { get; set; }

        public uint PageSize { get; set; }

        public bool OrderAscending { get; }

        public string OrderProperty { get; }

        public uint PageCount
        {
            get { return _pageCount; }
            set
            {
                _pageCount = value;
                if (PageNumber > _pageCount)
                {
                    PageNumber = 1;
                }
            }
        }

        public uint TotalCount
        {
            get { return _totalCount; }
            set
            {
                _totalCount = value;
                PageCount = (uint)Math.Ceiling((double)_totalCount / PageSize);
            }
        }
    }
}
