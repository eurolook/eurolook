﻿using System;
using System.Diagnostics.CodeAnalysis;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    public class TranslationDataModel
    {
        public TranslationDataModel()
        {
            // default constructor is used by ASP.NET MVC for creating the model from a HTTP request
        }

        [UsedImplicitly]
        public TranslationDataModel(Translation translation)
        {
            TranslationId = translation.Id;
            TextId = translation.TextId;
            LanguageId = translation.LanguageId;
            Value = translation.Value;
            OriginalValue = translation.Value;
            if (translation.Text != null)
            {
                TextAlias = translation.Text.Alias;
            }

            if (translation.Language != null)
            {
                LanguageName = translation.Language.Name;
            }
        }

        public Guid? TranslationId { get; set; }

        public Guid TextId { get; set; }

        public Guid LanguageId { get; set; }

        public string LanguageName { get; set; }

        public string TextAlias { get; set; }

        public string Value { get; set; }

        public string OriginalValue { get; set; }

        public bool IsSelected { get; set; }
    }
}
