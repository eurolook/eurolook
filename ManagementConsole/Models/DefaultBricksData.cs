namespace Eurolook.ManagementConsole.Models
{
    public class DefaultBricksData
    {
        public DefaultBricksData()
        {
            DefaultBricksModifications = new ChartData();
            AddedDefaultBricks = new ChartData();
            RemovedDefaultBricks = new ChartData();
        }

        public ChartData DefaultBricksModifications { get; set; }
        public ChartData RemovedDefaultBricks { get; set; }
        public ChartData AddedDefaultBricks { get; set; }
    }
}
