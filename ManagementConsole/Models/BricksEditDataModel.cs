﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Eurolook.Common;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.ViewModels.Resources;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Eurolook.ManagementConsole.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    public class BricksEditDataModel
    {
        public BricksEditDataModel()
        {
            // default constructor is used by ASP.NET MVC for creating the model from a HTTP request
        }

        public BricksEditDataModel(
            Brick brick,
            IEnumerable<BrickCategory> categories,
            IEnumerable<UserGroup> userGroups,
            IEnumerable<BrickGroup> groups,
            IEnumerable<ResourceViewModel> allResources,
            IEnumerable<ResourceViewModel> brickResources,
            IEnumerable<DocumentModel> documentModels,
            List<TextDataModel> brickTexts,
            ModelStateDictionary modelState,
            Text[] allTexts)
        {
            Id = brick.Id;
            Name = brick.Name;
            Description = brick.Description;
            DisplayName = brick.DisplayName;
            HotKey = brick.HotKey;
            Acronym = brick.Acronym;
            UIPositionIndex = brick.UiPositionIndex;
            IsVisible = !brick.IsHidden;
            VisibleToUserGroups = brick.VisibleToUserGroups;
            MinVersion = brick.MinVersion;
            MaxVersion = brick.MaxVersion;
            if (brick.Icon != null)
            {
                IconInfo = $"{Math.Ceiling(brick.Icon.Length / 1024.0)} kb";
                IconImage = Tools.GetBase64Image(brick.Icon);
                HasIcon = true;
            }

            if (brick.VectorIcon != null)
            {
                VectorIconInfo = $"{Math.Ceiling(brick.VectorIcon.Length / 1024.0)} kb";
                VectorIconImage = Tools.GetBase64Image(brick.VectorIcon);
                HasVectorIcon = true;
            }

            IsIconDeleted = false; // is set by the view
            IsVectorIconDeleted = false; // is set by the view
            ColorLuminance = brick.ColorLuminance;
            AllTexts = allTexts;
            Categories = new List<BrickCategory>();
            Categories.AddRange(categories);
            SelectedCategory = brick.CategoryId;

            UserGroups = userGroups
                         .Select(
                             ug => new UserGroupDataModel(ug)
                             {
                                 IsSelected = brick.IsVisibleToUserGroup(ug.Id),
                             })
                         .OrderBy(ug => ug.UiPositionIndex)
                         .ToList();

            Groups = new List<BrickGroup> { };
            Groups.AddRange(groups);
            SelectedGroupId = brick.GroupId ?? Guid.Empty;

            IsCommandBrick = brick is CommandBrick;
            IsContentBrick = brick is ContentBrick;
            IsDynamicBrick = brick is DynamicBrick;
            AllResources = new List<ResourceViewModel>();
            BrickResources = new List<ResourceViewModel>();
            DocumentModels = documentModels;
            BrickTexts = new List<TextDataModel>(brickTexts);

            if (allResources != null)
            {
                AllResources.AddRange(allResources);
            }

            if (brickResources != null)
            {
                BrickResources.AddRange(brickResources);
            }

            if (IsContentBrick)
            {
                var cb = brick as ContentBrick;
                IsMultiInstance = cb.IsMultiInstance;
                ContentBrickActionsClassName = cb.ContentBrickActionsClassName;
                try
                {
                    Content = XmlFormatter.PrettyPrint(cb.RawContent);
                }
                catch (Exception)
                {
                    modelState.AddModelError("Content", "Content is not valid XML.");
                    Content = cb.RawContent;
                }
            }

            if (IsCommandBrick)
            {
                var cb = brick as CommandBrick;
                Command = cb.CommandClass;
                CommandArgument = cb.Argument1;
                Configuration = cb.Configuration;
                HasCustomUi = cb.HasCustomUi;
            }
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string DisplayName { get; set; }

        public string HotKey { get; set; }

        public string Acronym { get; set; }

        public int UIPositionIndex { get; set; }

        public bool IsVisible { get; set; }

        public bool IsMultiInstance { get; set; }

        [RegularExpression(@"\d{1,5}\.\d{1,5}\.\d{1,5}\.\d{1,5}", ErrorMessage = "The field MinVersion must be in the format 1.0.0.0")]
        public string MinVersion { get; set; }

        [RegularExpression(@"\d{1,5}\.\d{1,5}\.\d{1,5}\.\d{1,5}", ErrorMessage = "The field MaxVersion must be in the format 1.0.0.0")]
        public string MaxVersion { get; set; }

        public string ContentBrickActionsClassName { get; set; }

        public string Content { get; set; }

        public string Command { get; set; }

        public string CommandArgument { get; set; }

        public string Configuration { get; set; }

        public List<TextDataModel> BrickTexts { get; set; }

        public bool IsContentBrick { get; set; }

        public bool IsCommandBrick { get; set; }

        public bool IsDynamicBrick { get; set; }

        public string IconInfo { get; set; }

        public string IconImage { get; set; }

        public bool HasIcon { get; }

        public string VectorIconInfo { get; set; }

        public string VectorIconImage { get; set; }

        public bool HasVectorIcon { get; }

        public List<BrickCategory> Categories { get; set; }

        public Guid SelectedCategory { get; set; }

        public List<UserGroupDataModel> UserGroups { get; set; }

        public string VisibleToUserGroups { get; set; }

        public List<BrickGroup> Groups { get; set; }

        public Guid? SelectedGroupId { get; set; }

        public List<ResourceViewModel> AllResources { get; set; }

        public List<ResourceViewModel> BrickResources { get; set; }

        public IEnumerable<DocumentModel> DocumentModels { get; set; }

        public bool IsIconDeleted { get; set; }

        public bool IsVectorIconDeleted { get; set; }

        public bool HasCustomUi { get; set; }

        public Luminance ColorLuminance { get; set; }

        public Text[] AllTexts { get; set; }
    }
}
