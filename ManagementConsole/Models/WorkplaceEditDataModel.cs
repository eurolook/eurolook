using System;
using System.Diagnostics.CodeAnalysis;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    public class WorkplaceEditDataModel
    {
        [UsedImplicitly]
        public WorkplaceEditDataModel()
        {
            // Used by ASP actions
        }

        public WorkplaceEditDataModel(Workplace workplace)
        {
            Id = workplace.Id;
            Office = workplace.Office;
            Phone = workplace.PhoneExtension;
            Fax = workplace.FaxExtension;
            AddressId = workplace.AddressId;
        }

        public Guid Id { get; set; }

        public string Office { get; set; }

        public string Phone { get; set; }

        public string Fax { get; set; }

        public bool IsPrimaryWorkplace { get; set; }

        public Guid AddressId { get; set; }
    }
}
