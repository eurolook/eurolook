﻿using System.Collections.Generic;

namespace Eurolook.ManagementConsole.Models
{
    public class PerformanceLogClientResultModel
    {
        public PerformanceLogClientResultModel(IReadOnlyCollection<string> categories)
        {
            AddInLoaded = new ChartData<decimal>();
            AddInLoaded.Categories.AddRange(categories);

            AddInLoadedDetails = new ChartData<decimal>();
            AddInLoadedDetails.Categories.AddRange(categories);

            CreationDialogLoaded = new ChartData<decimal>();
            CreationDialogLoaded.Categories.AddRange(categories);

            CreationDialogLoadedDetails = new ChartData<decimal>();
            CreationDialogLoadedDetails.Categories.AddRange(categories);

            DataSyncFinished = new ChartData<decimal>();
            DataSyncFinished.Categories.AddRange(categories);

            DataSyncFinishedDetails = new ChartData<decimal>();
            DataSyncFinishedDetails.Categories.AddRange(categories);

            DocumentCreated = new ChartData<decimal>();
            DocumentCreated.Categories.AddRange(categories);

            DocumentCreatedDetails = new ChartData<decimal>();
            DocumentCreatedDetails.Categories.AddRange(categories);
        }

        public ChartData<decimal> AddInLoaded { get; private set; }
        public ChartData<decimal> AddInLoadedDetails { get; private set; }

        public ChartData<decimal> CreationDialogLoaded { get; private set; }
        public ChartData<decimal> CreationDialogLoadedDetails { get; private set; }

        public ChartData<decimal> DataSyncFinished { get; private set; }
        public ChartData<decimal> DataSyncFinishedDetails { get; private set; }

        public ChartData<decimal> DocumentCreated { get; private set; }
        public ChartData<decimal> DocumentCreatedDetails { get; private set; }
    }
}
