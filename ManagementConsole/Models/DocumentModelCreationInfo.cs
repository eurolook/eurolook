﻿namespace Eurolook.ManagementConsole.Models
{
    public class DocumentModelCreationInfo
    {
        public string DocumentModelName { get; set; }

        public int CreationCount { get; set; }
    }
}
