using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    public class JobAssignmentDataModel
    {
        [UsedImplicitly]
        public JobAssignmentDataModel()
        {
            Functions = new List<FunctionEditDataModel>();
            Languages = new List<Language>();
        }

        public JobAssignmentDataModel(JobAssignment jobAssignment, IEnumerable<Language> languages)
            : this()
        {
            OrgaEntityId = jobAssignment.OrgaEntityId;
            Id = jobAssignment.Id;
            IsMainJob = false;
            IsNew = false;
            Service = jobAssignment.Service;
            WebAddress = jobAssignment.WebAddress;
            FunctionalMailbox = jobAssignment.FunctionalMailbox;
            PredefinedFunctionId = jobAssignment.PredefinedFunctionId;
            Functions.AddRange(jobAssignment.Functions.Select(f => new FunctionEditDataModel(f)));
            Languages.AddRange(languages);
        }

        public JobAssignmentDataModel(Author author, IEnumerable<Language> languages)
            : this()
        {
            OrgaEntityId = author.OrgaEntityId;
            IsMainJob = true;
            IsNew = false;
            Service = author.Service;
            WebAddress = author.WebAddress;
            FunctionalMailbox = author.FunctionalMailbox;
            PredefinedFunctionId = author.PredefinedFunctionId;
            Functions.AddRange(author.Functions.Select(f => new FunctionEditDataModel(f)));
            Languages.AddRange(languages);
        }

        public List<Language> Languages { get; set; }

        public Guid? OrgaEntityId { get; set; }

        public string Service { get; set; }

        public string WebAddress { get; set; }

        public string FunctionalMailbox { get; set; }

        public List<FunctionEditDataModel> Functions { get; set; }

        public bool IsMainJob { get; set; }

        public bool IsNew { get; set; }

        public bool IsDeleted { get; set; }

        public Guid Id { get; set; }

        public Guid? PredefinedFunctionId { get; set; }
    }
}
