namespace Eurolook.ManagementConsole.Models
{
    public class DocumentCreationInfoByEntity
    {
        public string EntityName { get; set; }

        public int TotalCreationCount { get; set; }
    }
}
