using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Common.Extensions;

namespace Eurolook.ManagementConsole.Models
{
    public class PerformanceLogInfoByDate
    {
        private readonly bool _daily;

        public PerformanceLogInfoByDate(DateTime fromDate, bool daily = false)
        {
            _daily = daily;
            Entries = new Dictionary<DateTime, List<PerformanceLogInfo>>();
            if (daily)
            {
                for (var day = fromDate.ToDateType(DateTimeExtensions.DateGroupType.Day);
                     day <= DateTime.UtcNow.ToDateType(DateTimeExtensions.DateGroupType.Day);
                     day = day.AddDays(1))
                {
                    Entries.Add(day, new List<PerformanceLogInfo>());
                }
            }
            else
            {
                for (var week = fromDate.ToDateType(DateTimeExtensions.DateGroupType.Week);
                     week <= DateTime.UtcNow.ToDateType(DateTimeExtensions.DateGroupType.Week);
                     week = week.AddDays(7))
                {
                    Entries.Add(week, new List<PerformanceLogInfo>());
                }
            }
        }

        public Dictionary<DateTime, List<PerformanceLogInfo>> Entries { get; }

        public void AddEntry(Version version, string eventName, DateTime date, TimeSpan average)
        {
            if (!_daily)
            {
                date = date.ToDateType(DateTimeExtensions.DateGroupType.Week);
            }

            if (Entries.ContainsKey(date))
            {
                var entry = Entries[date].FirstOrDefault(p => p.Version == version && p.Event == eventName);
                if (entry != null)
                {
                    entry.Averages.Add(average);
                }
                else
                {
                    Entries[date].Add(new PerformanceLogInfo(version, eventName, average));
                }
            }
        }

        public IEnumerable<string> GetCategories()
        {
            return Entries.Keys
                          .Select(d => _daily ? d.ToString("M") : d.ToString("M") + " - " + d.AddDays(6).ToString("M"))
                          .ToList();
        }

        public List<PerformanceLogInfo> GetEventEntriesForVersion(Version version, string eventName)
        {
            return Entries.Keys
                          .Select(
                              date => Entries[date].FirstOrDefault(e => e.Version == version && e.Event == eventName))
                          .ToList();
        }
    }
}
