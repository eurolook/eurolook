﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Eurolook.Common;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Database;

namespace Eurolook.ManagementConsole.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    public class BrickGroupsEditDataModel
    {
        public BrickGroupsEditDataModel()
        {
            // default constructor is used by ASP.NET MVC for creating the model from a HTTP request
        }

        public BrickGroupsEditDataModel(BrickGroup brickGroup, List<DocumentModel> documentModels)
        {
            Id = brickGroup.Id;
            Name = brickGroup.Name;
            Description = brickGroup.Description;
            DisplayName = brickGroup.DisplayName;
            Acronym = brickGroup.Acronym;
            UiPositionIndex = brickGroup.UiPositionIndex;
            ColorLuminance = brickGroup.ColorLuminance;
            IsMultipleChoice = brickGroup.IsMultipleChoice;
            Members = new List<BrickDataModel>();
            foreach (var brick in brickGroup.Bricks.OrderBy(b => b.UiPositionIndex).ThenBy(b => b.DisplayName))
            {
                Members.Add(new BrickDataModel(brick));
            }

            DocumentModels = documentModels;

            if (brickGroup.Icon != null)
            {
                IconInfo = string.Format("{0} kb", Math.Ceiling(brickGroup.Icon.Length / 1024.0));
                IconImage = Tools.GetBase64Image(brickGroup.Icon);
                HasIcon = true;
            }

            IsIconDeleted = false; // is set by the view
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string Description { get; set; }

        public string Acronym { get; set; }

        public int UiPositionIndex { get; set; }

        public bool IsMultipleChoice { get; set; }

        public string IconInfo { get; set; }

        public string IconImage { get; set; }

        public bool HasIcon { get; }

        public bool IsIconDeleted { get; set; }

        public List<BrickDataModel> Members { get; set; }

        public List<DocumentModel> DocumentModels { get; set; }

        public Luminance ColorLuminance { get; set; }
    }
}
