﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Eurolook.Common;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.ExcelExport;
using JetBrains.Annotations;
using Text = Eurolook.Data.Models.Text;

namespace Eurolook.ManagementConsole.Models
{
    public class OrgaChartReader : IDisposable
    {
        private readonly IOrgaChartDatabase _db;
        private readonly ILanguageRepository _languageRepository;
        private readonly Stream _inputStream;
        private readonly ChangesMessageBuilder _output;
        private readonly List<Translation> _translations;
        private readonly Dictionary<Guid, int> _idsRead;

        private OrgaEntity _root;
        private OrgaEntity _dg;
        private OrgaEntity _lastDirectorate;
        private Dictionary<int, Language> _languages;
        private List<Address> _addresses;
        private List<OrgaEntity> _orgaEntities;
        private List<OrgaEntity> _orphans;

        // Cache database to improve performances.
        private SheetData _sheet;
        private SharedStringTable _sharedStringTable;

        public string WebAddress { get; set; }

        // Sven : I'm pretty sure caching Addresses is a good idea, as there will be only a few of them, there is no need to make a database call each time
        // one has to be loaded. I'm less sure about Translation, as the amount of Translations might, at some point, grow so much it's not wise to load them
        // all at the same time.
        public OrgaChartReader(Stream inputStream, IOrgaChartDatabase orgaChartDatabase, ILanguageRepository languageRepository)
        {
            _inputStream = inputStream;
            _db = orgaChartDatabase;
            _languageRepository = languageRepository;
            _idsRead = new Dictionary<Guid, int>();
            _output = new ChangesMessageBuilder();
            _translations = new List<Translation>();
        }

        ~OrgaChartReader()
        {
            Dispose(false);
        }

        public OrgaEntity ReadChart(Guid? languageId = null)
        {
            return ReadChart(out _, languageId);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="output">Output for the validation step to be shown in AdminUI</param>
        /// <param name="languageId">The id of the selected language</param>
        /// <returns>The DG that shall be imported.</returns>
        public OrgaEntity ReadChart(out ChangesMessageBuilder output, Guid? languageId = null)
        {
            _root = _db.GetRoot();
            if (_root == null)
            {
                throw new Exception("There is no root node (LogicalLevel=0) in the database.");
            }

            _addresses = _db.GetAddresses(true);
            _translations.Clear();

            ReadSheet();

            // row 1
            ReadWebAddress();

            // row 2-4
            ParseHeadRow();
            var languages = GetReadingLanguages(languageId);
            ParseDgRow(languages);

            // row 5-n
            for (int i = 5; i <= _sheet.Descendants<Row>().Count(); i++)
            {
                var row = GetRow($"{i}");
                if (row != null)
                {
                    ParseRow(i, row, languages.ToArray());
                }
            }

            _orgaEntities = FlattenTree(_dg);
            _orphans = _db.GetOrphanedOrgaEntities(_orgaEntities);
            foreach (var o in _orphans)
            {
                _output.AddRemovedOrgaEntity(_db.GetAuthorsNodes(o.Id, 10).Count, o);
            }

            output = _output;
            return _dg;
        }

        private void ReadWebAddress()
        {
            WebAddress = GetCellValue(GetCell(GetRow("1"), "A1"));
        }

        private Language[] GetReadingLanguages(Guid? languageId)
        {
            var result = new List<Language>();
            if (languageId == null)
            {
                result.AddRange(_languages.Values);
            }
            else
            {
                result.Add(_languageRepository.GetLanguage(languageId.Value));
            }

            return result.ToArray();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _inputStream?.Close();
            }
        }

        private void ParseHeadRow()
        {
            // allLanguagesCheck will ensure all languages have their column in the spreadsheet.
            var allLanguagesCheck = _languageRepository.GetAllLanguages();
            _languages = new Dictionary<int, Language>();
            var headRow = GetRow("2");
            for (int i = 11; i <= 34; i++)
            {
                string languageCode = GetCellValue(headRow, i);
                if (string.IsNullOrWhiteSpace(languageCode))
                {
                    continue;
                }

                var language = _languageRepository.GetLanguage(languageCode);
                if (language == null)
                {
                    throw new OrgaChartReaderException($"Invalid LANGUAGE: The language '{languageCode}' is not a valid language.");
                }

                allLanguagesCheck.RemoveAll(l => l.Name == languageCode);
                _languages[i] = language;
            }

            if (allLanguagesCheck.Count > 0)
            {
                string missingLanguages = string.Join(", ", allLanguagesCheck.Select(l => l.Name));
                throw new OrgaChartReaderException(string.Format("Invalid LANGUAGE: Column{0} for language{0} '{1}' {2} missing.", allLanguagesCheck.Count == 1 ? "" : "s", missingLanguages, allLanguagesCheck.Count == 1 ? "is" : "are"));
            }
        }

        private void ParseDgRow(Language[] languages)
        {
            var row3 = GetRow("3");
            var row4 = GetRow("4");
            if (!IsChecked(GetCellValue(row3, "A3")) || !IsChecked(GetCellValue(row4, "A4")))
            {
                throw new OrgaChartReaderException("Invalid DG: Row 3 and Row 4 are expected to be DG.");
            }

            _dg = GetOrgaEntity(3, row3, row4, languages, 1);
            _dg.SuperEntity = _root;
            _dg.SuperEntityId = _root.Id;
            _dg.ShowInHeader = !IsChecked(GetCellValue(row3, GetCellName(row3, OrgaChartSheetLayout.ColumnNotADg)));
            if (!string.IsNullOrWhiteSpace(WebAddress))
            {
                _dg.WebAddress = WebAddress;
            }
        }

        private static string GetCellName(Row row, string index)
        {
            return index + row.RowIndex.Value;
        }

        private static string GetCellName(Row row, int index)
        {
            return GetRefName(index) + row.RowIndex.Value;
        }

        private void ParseRow(int rowNo, Row row, Language[] languages)
        {
            bool isDg = IsChecked(GetCellValue(row, GetCellName(row, OrgaChartSheetLayout.ColumnDg)));
            bool isDirectorate = IsChecked(GetCellValue(row, GetCellName(row, OrgaChartSheetLayout.ColumnDir)));
            bool isUnit = IsChecked(GetCellValue(row, GetCellName(row, OrgaChartSheetLayout.ColumnUnit)));
            if (isDg)
            {
                throw new OrgaChartReaderException($"Invalid ROW: Only the first OrgaEntity can be a DG. (Row={rowNo})");
            }

            if (!isDirectorate && !isUnit)
            {
                throw new OrgaChartReaderException($"Invalid LOGICAL LEVEL: OrgaEntity provided with no LogicalLevel. (Row={rowNo})");
            }

            int logicalLevel = isDirectorate ? 2 : 3;

            var entity = GetOrgaEntity(rowNo, row, null, languages, logicalLevel);
            entity.ShowInHeader = !IsChecked(GetCellValue(row, GetCellName(row, OrgaChartSheetLayout.ColumnNotADg)));
            bool isNewOrWasDeleted = entity.IsNew || entity.WasDeleted;
            if (isDirectorate)
            {
                // Directorate
                entity.LogicalLevel = 2;
                entity.SuperEntity = _dg;
                entity.SuperEntityId = _dg.Id;
                _lastDirectorate = entity;
                _dg.SubEntities.Add(entity);
                if (isNewOrWasDeleted)
                {
                    _output.AddAddedDirectorate(_dg.Name, entity, rowNo);
                }
            }
            else
            {
                // Unit or Function
                entity.LogicalLevel = 3;
                if (_lastDirectorate != null)
                {
                    entity.SuperEntity = _lastDirectorate;
                    entity.SuperEntityId = _lastDirectorate.Id;
                    _lastDirectorate.SubEntities.Add(entity);
                }
                else
                {
                    entity.SuperEntity = _dg;
                    entity.SuperEntityId = _dg.Id;
                    _dg.SubEntities.Add(entity);
                }

                if (isNewOrWasDeleted && _lastDirectorate != null)
                {
                    _output.AddAddedUnit(_lastDirectorate.Name, entity, rowNo);
                }
            }
        }

        private OrgaEntity CreateNewEntity(Guid? id = null)
        {
            var orgaEntity = new OrgaEntity { SubEntities = new List<OrgaEntity>() };
            orgaEntity.Init();
            if (id != null)
            {
                orgaEntity.Id = id.Value;
            }

            orgaEntity.HeaderText = new Text();
            orgaEntity.HeaderText.Init();
            orgaEntity.HeaderTextId = orgaEntity.HeaderText.Id;
            return orgaEntity;
        }

        private Tuple<Address, Address> ReadLocations(string locationsStr)
        {
            var locations = locationsStr.Split('+');

            // read the primary address
            string primaryLocation = locations[0].Trim();
            var primaryAddress = _addresses.FirstOrDefault(
                a => a.ActiveDirectoryReference != null && a.ActiveDirectoryReference.Equals(primaryLocation, StringComparison.InvariantCultureIgnoreCase));
            if (primaryAddress == null)
            {
                throw new OrgaChartReaderException($"Invalid LOCATION '{primaryLocation}'.");
            }

            if (locations.Length <= 1)
            {
                return new Tuple<Address, Address>(primaryAddress, null);
            }

            // read the secondary address
            string secondaryLocation = locations[1].Trim();
            var secondaryAddress = _addresses.FirstOrDefault(
                a => a.ActiveDirectoryReference != null && a.ActiveDirectoryReference.Equals(secondaryLocation, StringComparison.InvariantCultureIgnoreCase));
            if (secondaryAddress == null)
            {
                throw new OrgaChartReaderException($"Invalid LOCATION '{secondaryLocation}'.");
            }

            return new Tuple<Address, Address>(primaryAddress, secondaryAddress);
        }

        /// <summary>
        /// Only difference with ReadTranslations is in the fact it checks for modified or added Translations.
        /// Also log the changes.
        /// </summary>
        /// <param name="rowNo">the number of the current row</param>
        /// <param name="row">the current row element.</param>
        /// <param name="row2">if there is more content that does not fit in one cell.</param>
        /// <param name="orgaEntity">the current orga entity</param>
        /// <param name="languages">the languages of the orga chart.</param>
        private void ParseTranslations(int rowNo, Row row, Row row2, OrgaEntity orgaEntity, Language[] languages)
        {
            if (orgaEntity == null)
            {
                throw new ArgumentNullException(nameof(orgaEntity));
            }

            if (orgaEntity.HeaderTextId == null)
            {
                throw new ArgumentNullException(nameof(orgaEntity.HeaderTextId));
            }

            var addedTranslations = new List<string>();
            var addedTranslationsLang = new List<string>();
            var oldTranslations = new List<string>();
            var updatedTranslations = new List<string>();
            var deletedTranslations = new List<string>();
            var updatedTranslationsLang = new List<string>();
            var translations = _db.GetAllTranslations(orgaEntity.HeaderTextId.Value);
            foreach (var language in languages)
            {
                var translation = translations.FirstOrDefault(t => t.LanguageId == language.Id);

                int langColIndex = GetLanguageCol(language);
                string newValue = GetCellValue(row, GetCellName(row, langColIndex))?.Trim();
                if (row2 != null)
                {
                    string secondLine = GetCellValue(row2, GetCellName(row2, langColIndex))?.Trim();
                    if (!string.IsNullOrEmpty(secondLine))
                    {
                        newValue += "<>" + secondLine;
                    }
                }

                bool cellHasValue = !string.IsNullOrWhiteSpace(newValue);
                if (translation == null && cellHasValue)
                {
                    translation = CreateTranslation(orgaEntity, language, newValue);
                    addedTranslations.Add(newValue);
                    addedTranslationsLang.Add(language.Name);
                }
                else if (translation != null)
                {
                    if (!cellHasValue && !translation.Deleted)
                    {
                        DeleteTranslation(translation);
                        deletedTranslations.Add(translation.Value);
                        updatedTranslationsLang.Add(language.Name);
                    }
                    else if (cellHasValue)
                    {
                        if (!string.Equals(translation.Value, newValue))
                        {
                            oldTranslations.Add(translation.Value);
                            UpdateTranslation(translation, newValue);
                            updatedTranslations.Add(translation.Value);
                            updatedTranslationsLang.Add(language.Name);
                        }
                        else if (translation.Deleted)
                        {
                            UndeleteTranslation(translation, newValue);
                            oldTranslations.Add("(deleted)");
                            updatedTranslations.Add(translation.Value);
                            updatedTranslationsLang.Add(language.Name);
                        }
                    }
                }

                if (translation != null)
                {
                    _translations.Add(translation);
                }

                if (languages.Count() == 1 || language.Name.Equals("EN", StringComparison.InvariantCultureIgnoreCase))
                {
                    orgaEntity.Header = translation;
                }
            }

            _output.AddRemovedTranslations(deletedTranslations.ToArray(), updatedTranslationsLang.ToArray(), orgaEntity, rowNo);
            _output.AddAddedTranslation(addedTranslations.ToArray(), addedTranslationsLang.ToArray(), orgaEntity, rowNo);
            _output.AddModifiedTranslation(oldTranslations.ToArray(), updatedTranslations.ToArray(), updatedTranslationsLang.ToArray(), orgaEntity, rowNo);
        }

        private static void UndeleteTranslation(Translation translation, string newValue)
        {
            translation.Value = newValue;
            translation.SetDeletedFlag(false);
        }

        private static void DeleteTranslation(Translation translation)
        {
            translation.SetDeletedFlag();
        }

        private static void UpdateTranslation(Translation translation, string newValue)
        {
            translation.Value = newValue;
            translation.SetDeletedFlag(false);
        }

        private static Translation CreateTranslation(OrgaEntity orgaEntity, Language language, string newValue)
        {
            var translation = new Translation();
            translation.Init();
            translation.TextId = orgaEntity.HeaderTextId.Value;
            translation.LanguageId = language.Id;
            translation.Value = newValue;
            return translation;
        }

        private void ReadSheet()
        {
            using (var spreadsheet = SpreadsheetDocument.Open(_inputStream, false))
            {
                var workbookPart = spreadsheet.WorkbookPart;
                var firstSheet = workbookPart.Workbook.Descendants<Sheet>().First();
                var worksheetPart = (WorksheetPart)workbookPart.GetPartById(firstSheet.Id);
                _sheet = worksheetPart.Worksheet.Elements<SheetData>().First();
                _sharedStringTable = workbookPart.SharedStringTablePart.SharedStringTable;
            }
        }

        private bool IsChecked(string cellValue)
        {
            if (cellValue == null)
            {
                return false;
            }

            cellValue = cellValue.Trim();
            return cellValue.Equals("X", StringComparison.InvariantCultureIgnoreCase);
        }

        private static string GetRefName(int index)
        {
            int dividend = index;
            string columnName = string.Empty;
            while (dividend > 0)
            {
                int modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo) + columnName;
                dividend = (dividend - modulo) / 26;
            }

            return columnName;
        }

        private Row GetRow(string rowIndex)
        {
            return _sheet.Descendants<Row>().FirstOrDefault(r => r.RowIndex == rowIndex);
        }

        private static Cell GetCell(Row row, int index)
        {
            return GetCell(row, GetCellName(row, index));
        }

        private static Cell GetCell(Row row, string cellName)
        {
            return row?.Descendants<Cell>().FirstOrDefault(c => c.CellReference.Value == cellName);
        }

        private string GetCellValue(Row row, string cellName)
        {
            return GetCellValue(GetCell(row, cellName));
        }

        private string GetCellValue(Row row, int index)
        {
            return GetCellValue(GetCell(row, index));
        }

        private string GetCellValue(Cell cell)
        {
            if (cell == null)
            {
                return null;
            }

            var cellValue = cell.CellValue;
            string text = cellValue == null ? cell.InnerText : cellValue.Text;
            if ((cell.DataType != null) && (cell.DataType == CellValues.SharedString))
            {
                text = _sharedStringTable
                                    .Elements<SharedStringItem>().ElementAt(
                                        Convert.ToInt32(cell.CellValue.Text)).InnerText;
            }

            return (text ?? string.Empty).Trim();
        }

        private int GetLanguageCol(Language lang)
        {
            foreach (int index in _languages.Keys)
            {
                if (_languages[index].Id == lang.Id)
                {
                    return index;
                }
            }

            return -1;
        }

        private OrgaEntity LoadEntity(Guid id)
        {
            var orgaEntity = _db.GetOrgaEntityForImport(id);
            if (orgaEntity != null)
            {
                orgaEntity.SubEntities = new List<OrgaEntity>();
                return orgaEntity;
            }

            return null;
        }

        /// <summary>
        /// EL10 version of ReadOrgaEntity. Main difference is in checking Database for existing OrgaEntity and editing it if found.
        /// Also, difference with existing OrgaEntities will be computed.
        /// </summary>
        /// <returns>the read-in orga entity</returns>
        private OrgaEntity GetOrgaEntity(int rowNo, Row row, Row row2, Language[] languages, int logicalLevel)
        {
            string cellName = GetCellName(row, OrgaChartSheetLayout.ColumnLocation);

            bool showInHeader = !IsChecked(GetCellValue(row, GetCellName(row, OrgaChartSheetLayout.ColumnNotADg)));
            string acronymStr = GetCellValue(row, GetCellName(row, OrgaChartSheetLayout.ColumnAcronym));
            if (string.IsNullOrWhiteSpace(acronymStr))
            {
                throw new OrgaChartReaderException($"Invalid ACRONYM. Acronym cannot be empty. (Row={rowNo})");
            }

            acronymStr = acronymStr.Trim();
            string idStr = GetCellValue(row, GetCellName(row, OrgaChartSheetLayout.ColumnKey))?.Trim();

            OrgaEntity orgaEntity;
            if (!string.IsNullOrWhiteSpace(idStr))
            {
                if (Guid.TryParse(idStr, out var id))
                {
                    if (_idsRead.ContainsKey(id))
                    {
                        throw new OrgaChartReaderException(
                            $"Invalid INTERNAL KEY: '{idStr}'. Two entities are provided with same INTERNAL KEY. (Row={rowNo}, Row={_idsRead[id]})\nLeave INTERNAL KEY field empty to create a new entity.");
                    }

                    _idsRead.Add(id, rowNo);
                    orgaEntity = LoadEntity(id) ?? CreateNewEntity(id);
                }
                else
                {
                    throw new OrgaChartReaderException(
                        $"Invalid INTERNAL KEY: '{idStr}'. It is not a valid guid. (Row={rowNo}, Acronym={acronymStr})\nLeave INTERNAL KEY field empty to create a new entity.");
                }
            }
            else
            {
                // DG is a special case. As far as spec goes, admin is not allowed to provide a new DG with the Importer.
                if (logicalLevel == 1)
                {
                    throw new OrgaChartReaderException(
                        $"Invalid DIRECTORATE GENERAL : Cannot find DG with INTERNAL KEY '{idStr}'. (Row={rowNo}, Acronym={acronymStr})\nDIRECTORATE GENERAL can not be added with OrgaChart Import functions.");
                }

                // Otherwise, Directors and Units can be created.
                orgaEntity = CreateNewEntity();
            }

            int.TryParse(GetCellValue(row, GetCellName(row, OrgaChartSheetLayout.ColumnOrderIndex)), out int orderIndex);

            // read locations
            string locationsStr = GetCellValue(row, cellName.Trim());
            if (!IsValidLocationStringFormat(locationsStr))
            {
                throw new OrgaChartReaderException($"Invalid LOCATION '{locationsStr}'. Use something like BRU+LUX. (Row={rowNo - 1})");
            }

            var locations = ReadLocations(locationsStr);

            // read function
            string function = GetCellValue(row, GetCellName(row, OrgaChartSheetLayout.ColumnFunction));
            if (function == "")
            {
                function = null;
            }

            // read gender
            string gender = GetCellValue(row, GetCellName(row, OrgaChartSheetLayout.ColumnGender));
            if (!IsValidGenderString(gender))
            {
                throw new OrgaChartReaderException($"Invalid GENDER '{gender}'. Use m, f or nothing.");
            }

            if (gender == "")
            {
                gender = null;
            }

            if (orgaEntity.IsNew)
            {
                orgaEntity.HeaderText.Alias = CreateHeaderTextAlias(acronymStr);
            }
            else
            {
                if (logicalLevel != orgaEntity.LogicalLevel)
                {
                    _output.AddModifiedLogicalLevel(orgaEntity.LogicalLevel, logicalLevel, orgaEntity, rowNo);
                }

                if (acronymStr != orgaEntity.Name)
                {
                    // the acronymw as changed, update the header text alias
                    orgaEntity.HeaderText.Alias = CreateHeaderTextAlias(acronymStr);
                    _output.AddModifiedAcronym(orgaEntity.Name, acronymStr, rowNo);
                }

                if (orgaEntity.ShowInHeader != showInHeader)
                {
                    _output.AddModifiedShowInHeader(orgaEntity.ShowInHeader, showInHeader, orgaEntity, rowNo);
                }

                var oldPrimaryAddress = _addresses.FirstOrDefault(a => a.Id == orgaEntity.PrimaryAddressId);
                var oldSecondaryAddress = _addresses.FirstOrDefault(a => a.Id == orgaEntity.SecondaryAddressId);
                string oldLocationString = oldPrimaryAddress != null ? oldPrimaryAddress.ActiveDirectoryReference : "";
                oldLocationString += oldSecondaryAddress != null ? "+" + oldSecondaryAddress.ActiveDirectoryReference : "";
                if (oldLocationString != locationsStr)
                {
                    _output.AddModifiedLocation(oldLocationString, locationsStr, orgaEntity, rowNo);
                }

                if (function != orgaEntity.Function)
                {
                    _output.AddModifiedFunction(orgaEntity.Function, function, orgaEntity, rowNo);
                }

                if (gender != orgaEntity.Gender)
                {
                    _output.AddModifiedGender(orgaEntity.Gender, gender, orgaEntity, rowNo);
                }
            }

            orgaEntity.LogicalLevel = logicalLevel;
            orgaEntity.Name = acronymStr;
            orgaEntity.Function = function;
            orgaEntity.Gender = gender;
            orgaEntity.OrderIndex = orderIndex;
            ParseTranslations(rowNo, row, row2, orgaEntity, languages);
            orgaEntity.PrimaryAddress = locations.Item1;
            orgaEntity.PrimaryAddressId = locations.Item1?.Id;
            orgaEntity.SecondaryAddress = locations.Item2;
            orgaEntity.SecondaryAddressId = locations.Item2?.Id;
            orgaEntity.ShowInHeader = showInHeader;
            if (orgaEntity.Deleted)
            {
                orgaEntity.WasDeleted = true;
                orgaEntity.SetDeletedFlag(false);
            }

            return orgaEntity;
        }

        public DataPackage GetImportDataPackage()
        {
            var result = new DataPackage
            {
                Description = "Orga Chart Import",
            };

            // added or updated orga entities, texts and translations
            foreach (var orgaEntity in _orgaEntities)
            {
                CleanReferences(orgaEntity);
                result.OrgaEntities.Add(orgaEntity);
                result.Texts.Add(orgaEntity.HeaderText);
            }

            foreach (var translation in _translations)
            {
                translation.Language = null;
                translation.Text = null;
                result.Translations.Add(translation);
            }

            // deleted orga entities, texts and translations
            foreach (var orphanedEntity in _orphans)
            {
                orphanedEntity.SetDeletedFlag();
                CleanReferences(orphanedEntity);
                result.OrgaEntities.Add(orphanedEntity);
            }

            var textIds = _orphans.Select(o => o.HeaderTextId).ToArray();
            foreach (var text in _db.GetTexts(textIds))
            {
                text.SetDeletedFlag();
                result.Texts.Add(text);
            }

            foreach (var translation in _db.GetTranslations(textIds))
            {
                translation.SetDeletedFlag();
                result.Translations.Add(translation);
            }

            return result;
        }

        private static void CleanReferences(OrgaEntity orgaEntity)
        {
            orgaEntity.SuperEntity = null;
            orgaEntity.Header = null;
            orgaEntity.PrimaryAddress = null;
            orgaEntity.SecondaryAddress = null;
            if (orgaEntity.HeaderText != null)
            {
                orgaEntity.HeaderText.Translations = null;
            }
        }

        [NotNull]
        private static List<OrgaEntity> FlattenTree(OrgaEntity node)
        {
            var result = new List<OrgaEntity>();
            if (node.SubEntities == null || !node.SubEntities.Any())
            {
                result.Add(node);
            }
            else
            {
                foreach (var child in node.SubEntities)
                {
                    result.AddRange(FlattenTree(child));
                }

                result.Add(node);
            }

            return result;
        }

        private string CreateHeaderTextAlias(string acronymStr)
        {
            return $"{Tools.GetCleanXmlName(acronymStr)}_Header_{GetSmallUniqueId()}";
        }

        private bool IsValidLocationStringFormat(string locationsStr)
        {
            if (string.IsNullOrWhiteSpace(locationsStr))
            {
                return false;
            }

            var locations = locationsStr.Split('+');
            if (locations.Length < 1 || locations.Length > 2)
            {
                return false;
            }

            return true;
        }

        private bool IsValidGenderString(string genderStr)
        {
            return string.IsNullOrWhiteSpace(genderStr) ||
                   genderStr.Equals("m", StringComparison.InvariantCultureIgnoreCase) ||
                   genderStr.Equals("f", StringComparison.InvariantCultureIgnoreCase);
        }

        private string GetSmallUniqueId()
        {
            return Regex.Replace(Convert.ToBase64String(Guid.NewGuid().ToByteArray()), "[/+=]", "");
        }
    }
}
