﻿using System;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.Models
{
    public class NotificationsEditDataModel
    {
        public Guid? Id { get; set; }

        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string Content { get; set; }

        public byte[] ThumbnailBytes { get; set; }

        public string ThumbnailInfo { get; set; }

        public string ThumbnailImage { get; set; }

        public bool IsHidden { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        [UsedImplicitly]
        public string FromDateString { get; set; }

        [UsedImplicitly]
        public string ToDateString { get; set; }

        public string MinVersion { get; set; }

        public string MaxVersion { get; set; }

        public NotificationPriority Priority { get; set; }
    }
}
