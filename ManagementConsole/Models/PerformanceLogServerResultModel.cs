using System.Collections.Generic;

namespace Eurolook.ManagementConsole.Models
{
    public class PerformanceLogServerResultModel
    {
        public PerformanceLogServerResultModel(IReadOnlyCollection<string> categories)
        {
            GetInitialization = new ChartData<decimal>();
            GetInitialization.Categories.AddRange(categories);

            GetUpdates = new ChartData<decimal>();
            GetUpdates.Categories.AddRange(categories);

            GetUserProfile = new ChartData<decimal>();
            GetUserProfile.Categories.AddRange(categories);
        }

        public ChartData<decimal> GetInitialization { get; private set; }
        public ChartData<decimal> GetUpdates { get; private set; }
        public ChartData<decimal> GetUserProfile { get; private set; }
    }
}
