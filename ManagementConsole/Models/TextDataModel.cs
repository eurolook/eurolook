﻿using System;
using System.Diagnostics.CodeAnalysis;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    public class TextDataModel
    {
        [UsedImplicitly]
        public TextDataModel()
        {
            // default constructor is used by ASP.NET MVC for creating the model from a HTTP request
        }

        public TextDataModel(Text text)
        {
            TextId = text.Id;
            Alias = text.Alias;
        }

        public string Alias { get; set; }

        public Guid? TextId { get; set; }

        public int TranslationCount { get; set; }

        public bool IsSystemText { get; set; }
    }
}
