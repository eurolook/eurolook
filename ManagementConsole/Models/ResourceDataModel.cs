﻿using System;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.Models
{
    public class ResourceDataModel
    {
        [UsedImplicitly]
        public ResourceDataModel()
        {
            // default constructor is used by ASP.NET MVC for creating the model from a POST request
        }

        public ResourceDataModel(
            Resource resource)
        {
            Id = resource.Id;
            Alias = resource.Alias;
            FileName = resource.FileName;
        }

        public Guid? Id { get; set; }

        public string Alias { get; set; }

        public string FileName { get; set; }

        public string MimeType { get; set; }

        public bool IsSystemResource { get; set; }
    }
}
