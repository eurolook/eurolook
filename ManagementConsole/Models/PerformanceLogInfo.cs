using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Common.Extensions;
using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.Models
{
    public class PerformanceLogInfo
    {
        public PerformanceLogInfo(Version version, string eventName, TimeSpan initialAverage)
        {
            this.Version = version;
            this.Event = eventName;
            this.Averages = new List<TimeSpan>
            {
                initialAverage,
            };
        }

        [UsedImplicitly]
        public PerformanceLogInfo()
        {
            this.Version = Version.Parse("0");
            this.Averages = new List<TimeSpan>()
            {
                new TimeSpan(0),
            };
        }

        public Version Version { get; private set; }
        public string Event { get; private set; }
        public List<TimeSpan> Averages { get; private set; }

        public TimeSpan Fastest25Median { get => GetMedian(this.Averages.OrderBy(t => t).Take((25 * this.Averages.Count / 100) + 1).ToList());  }
        public TimeSpan AllMedian { get => GetMedian(this.Averages); }
        public TimeSpan Slowest15Median { get => GetMedian(this.Averages.OrderByDescending(t => t).Take((15 * this.Averages.Count / 100) + 1).ToList()); }

        private static TimeSpan GetMedian(List<TimeSpan> list)
        {
            var times = list.ToArray();
            if (times.IsNullOrEmpty())
            {
                return TimeSpan.MinValue;
            }

            Array.Sort(times);
            int count = times.Length;

            // count is odd, return the middle element
            if (count % 2 != 0)
            {
                return times[count / 2];
            }

            // count is even, average two middle elements
            var a = times[count / 2 - 1];
            var b = times[count / 2];
            var c = (a.Ticks + b.Ticks) / 2;
            return new TimeSpan(c);
        }
    }
}
