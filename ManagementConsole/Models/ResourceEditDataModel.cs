﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.Models
{
    public class ResourceEditDataModel
    {
        [UsedImplicitly]
        public ResourceEditDataModel()
        {
            // default constructor is used by ASP.NET MVC for creating the model from a POST request
        }

        public ResourceEditDataModel(
            Resource resource,
            List<Brick> bricks,
            IEnumerable<Language> allLanguages,
            bool isSystemResource)
        {
            Id = resource.Id;
            Alias = resource.Alias;
            FileName = resource.FileName;
            MimeType = resource.MimeType;
            RawData = resource.RawData;

            Bricks = new List<BrickDataModel>();

            foreach (var brick in bricks.OrderBy(b => b.Name))
            {
                Bricks.Add(new BrickDataModel(brick));
            }

            Localisations = new List<LocalisedResourceDataModel>();

            foreach (var lang in allLanguages.OrderBy(l => l.DisplayName))
            {
                var localisedResource = resource.Localisations?.FirstOrDefault(x => !x.Deleted && x.LanguageId == lang.Id);
                Localisations.Add(
                    new LocalisedResourceDataModel(localisedResource)
                    {
                        LanguageId = lang.Id,
                        Language = lang,
                    });
            }

            IsSystemResource = isSystemResource;
        }

        public Guid Id { get; set; }

        public string Alias { get; set; }

        public string FileName { get; set; }

        public byte[] RawData { get; set; }

        public string MimeType { get; set; }

        public List<BrickDataModel> Bricks { get; set; }

        public List<LocalisedResourceDataModel> Localisations { get; set; }

        public bool IsSystemResource { get; set; }
    }
}
