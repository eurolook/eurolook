using System;

namespace Eurolook.ManagementConsole.Models
{
    public class PredefinedFunctionEditDataModel
    {
        public Guid? Id { get; set; }

        public string Name { get; set; }

        public string ActiveDirectoryReference { get; set; }

        public bool ShowInSignature { get; set; }

        public bool ShowInHeader { get; set; }

        public bool IsSuperior { get; set; }

        public string TextAlias { get; set; }

        public string TextFemaleAlias { get; set; }

        public string TextHeaderAlias { get; set; }

        public string TextHeaderFemaleAlias { get; set; }
    }
}
