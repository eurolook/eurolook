using System;

namespace Eurolook.ManagementConsole.Models
{
    public class TextCleanupDataModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Translations { get; set; }
    }
}
