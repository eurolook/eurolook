﻿using System;

namespace Eurolook.ManagementConsole.Models
{
    [Serializable]
    public class OrgaChartReaderException : Exception
    {
        public OrgaChartReaderException(string message)
            : base(message)
        {
        }

        public OrgaChartReaderException(Exception inner)
            : base($"Something is wrong with the orga chart file: {inner.Message}", inner)
        {
        }

        public OrgaChartReaderException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
