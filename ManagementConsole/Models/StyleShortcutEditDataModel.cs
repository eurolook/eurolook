using System;

namespace Eurolook.ManagementConsole.Models
{
    public class StyleShortcutEditDataModel
    {
        public Guid? Id { get; set; }

        public string StyleName { get; set; }

        public string Shortcut { get; set; }
    }
}
