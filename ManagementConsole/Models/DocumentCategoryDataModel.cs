using System;
using Eurolook.Data.Models;

namespace Eurolook.ManagementConsole.Models
{
    public class DocumentCategoryDataModel
    {
        public DocumentCategoryDataModel()
        {
        }

        public DocumentCategoryDataModel(DocumentCategory documentCategory)
        {
            Id = documentCategory.Id;
            Name = documentCategory.Name;
        }

        public Guid? Id { get; set; }

        public string Name { get; set; }
    }
}
