﻿using System.Diagnostics.CodeAnalysis;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    public class BrickGroupDataModel
    {
        [UsedImplicitly]
        public BrickGroupDataModel()
        {
            // default constructor is used by ASP.NET MVC for creating the model from a HTTP request
        }

        public BrickGroupDataModel(BrickGroup brickGroup)
        {
            Id = brickGroup.Id.ToString();
            Name = brickGroup.Name;
            Description = brickGroup.Description;
        }

        public string Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
