using System;
using Eurolook.Data.Models;

namespace Eurolook.ManagementConsole.Models
{
    public class PersonNamesDataModel
    {
        public PersonNamesDataModel()
        {
        }

        public PersonNamesDataModel(PersonName personName)
        {
            Id = personName.Id;
            FirstName = personName.FirstName;
            LastName = personName.LastName;
            Country = personName.Country;
            PoliticalGroup = personName.PoliticalGroup;
            Category = personName.Category;
        }

        public Guid? Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Country { get; set; }

        public string PoliticalGroup { get; set; }

        public string Category { get; set; }
    }
}
