using System;

namespace Eurolook.ManagementConsole.Models
{
    public class ExceptionEntryInfo
    {
        public byte[] BucketId { get; set; }
        public Guid OriginatorId { get; set; }

        public string Originator { get; set; }

        public DateTime Occurred { get; set; }

        public string ClientVersion { get; set; }

        public string OfficeVersion { get; set; }
    }
}
