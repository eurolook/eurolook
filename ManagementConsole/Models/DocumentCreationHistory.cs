using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Common.Extensions;

namespace Eurolook.ManagementConsole.Models
{
    public class DocumentCreationHistory
    {
        public DocumentCreationHistory(DateTime fromDate)
        {
            Dates = new List<DateTime>();
            Series = new List<HistoryCountEntry>();

            for (var month = fromDate.ToDateType(DateTimeExtensions.DateGroupType.Month);
                 month <= DateTime.UtcNow.ToDateType(DateTimeExtensions.DateGroupType.Month);
                 month = month.AddMonths(1))
            {
                Dates.Add(month);
            }
        }

        public List<DateTime> Dates { get; set; }
        public List<HistoryCountEntry> Series { get; set; }
        public string[] Categories => Dates.Select(x => x.ToString("y")).ToArray();
    }
}
