﻿using System;

namespace Eurolook.ManagementConsole.Models
{
    public class BrickUsageHistoryRequest
    {
        public string[] BricksToInclude { get; set; }

        public Guid? DocumentTypeToInclude { get; set; }
    }
}
