﻿using System;
using Eurolook.Data.Models;

namespace Eurolook.ManagementConsole.Models
{
    public class NotificationsDataModel
    {
        public NotificationsDataModel()
        {
        }

        public NotificationsDataModel(Notification notification)
        {
            Name = notification.Name;
            Id = notification.Id;
            DisplayName = notification.DisplayName;
            Content = notification.Content;
            ThumbnailBytes = notification.ThumbnailBytes;
            IsHidden = notification.IsHidden;
            FromDate = notification.FromDate;
            ToDate = notification.ToDate;
            MinVersion = notification.MinVersion;
            MaxVersion = notification.MaxVersion;
            Priority = notification.Priority;
            IsHighlighted = notification.Priority == NotificationPriority.High;
        }

        public Guid? Id { get; set; }

        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string Content { get; set; }

        public byte[] ThumbnailBytes { get; set; }

        public bool IsHidden { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public string MinVersion { get; set; }

        public string MaxVersion { get; set; }

        public NotificationPriority Priority { get; set; }

        public bool IsHighlighted { get; set; }
    }
}
