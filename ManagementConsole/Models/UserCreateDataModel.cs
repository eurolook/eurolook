namespace Eurolook.ManagementConsole.Models;

public class UserCreateDataModel
{
    public string UserName { get; set; }

    public bool ForceCreate { get; set; }
}
