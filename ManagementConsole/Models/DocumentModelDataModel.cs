using System;
using System.Diagnostics.CodeAnalysis;
using Eurolook.Data.Models;

namespace Eurolook.ManagementConsole.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    public class DocumentModelDataModel
    {
        public DocumentModelDataModel()
        {
            // default constructor is used by ASP.NET MVC for creating the model from a HTTP request
        }

        public DocumentModelDataModel(DocumentModel model)
        {
            Id = model.Id;
            Name = model.Name;
            IsClassicDocument = model.IsClassicDocument ?? false;
            IsHidden = model.IsHidden ?? false;
        }

        public Guid? Id { get; set; }

        public string Name { get; set; }

        public bool IsSelected { get; set; }

        public bool IsClassicDocument { get; set; }

        public bool IsHidden { get; set; }
    }
}
