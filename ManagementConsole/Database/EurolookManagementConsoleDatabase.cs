﻿using Eurolook.Data;
using Microsoft.Extensions.Configuration;

namespace Eurolook.ManagementConsole.Database
{
    public abstract class EurolookManagementConsoleDatabase : EurolookServerDatabase, IEurolookManagementConsoleDatabase
    {
        protected EurolookManagementConsoleDatabase(IConfiguration configuration)
            : base(configuration.GetConnectionString("ServerDatabaseConnection"))
        {
        }
    }
}
