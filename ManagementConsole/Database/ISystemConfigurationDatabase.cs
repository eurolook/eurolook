﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Controllers;

namespace Eurolook.ManagementConsole.Database
{
    public interface ISystemConfigurationDatabase
    {
        Task<List<SystemConfiguration>> GetSystemConfigurationsAsync();

        Task<SystemConfiguration> CreateSystemConfigurationAsync(SystemConfigurationModel configuration);

        Task UpdateSystemConfigurationAsync(Guid id, string value);

        Task DeleteSystemConfigurationAsync(Guid id);
    }
}
