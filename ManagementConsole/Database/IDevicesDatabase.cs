using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.ManagementConsole.Models;

namespace Eurolook.ManagementConsole.Database
{
    public interface IDevicesDatabase
    {
        Task<IEnumerable<DeviceDataModel>> GetDevices(DataPage<DeviceDataModel> page, string searchTerm, bool deleted);

        int GetDevicesCount(string searchTerm, bool deleted);

        DeviceDataModel GetDeviceForEdit(Guid id);

        void DeleteDevice(Guid id);

        void DeleteDevicePermanently(Guid id);

        void ReviveDevice(Guid id);
    }
}
