using System;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Models;

namespace Eurolook.ManagementConsole.Database
{
    public interface IDocumentCategoryDatabase
    {
        DocumentCategory[] GetDocumentCategories(DataPage<DocumentCategory> page, string searchTerm, bool deleted);

        DocumentCategory CreateDocumentCategory(string name);

        DocumentCategory GetDocumentCategory(Guid id);

        void UpdateDocumentCategory(Guid id, DocumentCategory category);

        void DeleteDocumentCategory(Guid id);

        void ReviveDocumentCategory(Guid id);

        void DeleteDocumentCategoryPermanently(Guid id);

        int GetDocumentCategoryCount(string searchTerm, bool deleted);
    }
}