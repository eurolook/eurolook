using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.ViewModels.Address;

namespace Eurolook.ManagementConsole.Database
{
    public interface IAddressDatabase
    {
        Address[] GetAddresses(DataPage<Address> page, string searchTerm, bool deleted);

        int GetAddressCount(string query, bool deleted);

        Address CreateAddress(AddressViewModel viewModel);

        string CreateNameText(Guid id);

        Task DeleteAddress(Guid id, bool deleted);

        Task RestoreAddress(Guid id, bool deleted);

        AddressEditDataModel GetAddressForEdit(Guid id);

        void UpdateAddress(Guid id, AddressEditDataModel model);

        Task<List<Address>> GetAllAddressesAsync();
    }
}