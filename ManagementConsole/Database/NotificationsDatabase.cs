﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Eurolook.Common;
using Eurolook.Data.Extensions;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Eurolook.ManagementConsole.Database
{
    public class NotificationsDatabase : EurolookManagementConsoleDatabase, INotificationsDatabase
    {
        public NotificationsDatabase(IConfiguration configuration)
            : base(configuration)
        {
        }

        public Notification[] GetNotifications(DataPage<Notification> page, string searchTerm, bool deleted)
        {
            using var context = GetContext();
            return context.Notifications
                          .Where(GetNotificationsQueryPredicate(searchTerm, deleted))
                          .OrderByAndPaginate(page)
                          .ToArray();
        }

        public int GetNotificationsCount(string query, bool deleted)
        {
            using var context = GetContext();
            return context.Notifications.Count(GetNotificationsQueryPredicate(query, deleted));
        }

        public Notification GetNotification(Guid id)
        {
            return GetContext().Notifications.FirstOrDefault(x => x.Id == id);
        }

        public async Task<byte[]> UpdateThumbnail(Guid notificationId, IFormFile file)
        {
            if (file == null)
            {
                return null;
            }

            var context = GetContext();
            var dbNotification = context.Notifications.FirstOrDefault(x => x.Id == notificationId);

            if (dbNotification == null)
            {
                return null;
            }

            var fileBytes = await file.GetBytes();
            dbNotification.ThumbnailBytes = fileBytes;
            dbNotification.UpdateServerModificationTime();
            context.SaveChanges();
            return fileBytes;
        }

        public async Task<Notification> CreateNotification(NotificationsDataModel viewModel)
        {
            await using var context = GetContext();

            var date = DateTime.Now.Date.AddHours(DateTime.Now.Hour);
            var notification = new Notification();
            notification.Init();
            notification.Name = viewModel.Name;
            notification.DisplayName = viewModel.DisplayName;
            notification.Content = viewModel.Content;
            notification.FromDate = date.AddDays(1).ToUniversalTime();
            notification.ToDate = date.ToUniversalTime();
            notification.ThumbnailBytes = viewModel.ThumbnailBytes;
            notification.MinVersion = viewModel.MinVersion;
            notification.MaxVersion = viewModel.MaxVersion;
            notification.Priority = viewModel.Priority;
            notification.IsHighlighted = viewModel.Priority == NotificationPriority.High;
            notification.IsHidden = false;

            var result = context.Notifications.Add(notification).Entity;
            await context.SaveChangesAsync();
            return result;
        }

        public async Task DeleteNotification(Guid id, bool deleted)
        {
            await using var context = GetContext();
            var notification = await context.Notifications.FirstOrDefaultAsync(x => x.Id == id);
            notification.SetDeletedFlag();
            await context.SaveChangesAsync();
        }

        public async Task RestoreNotification(Guid id, bool deleted)
        {
            await using var context = GetContext();
            var notification = await context.Notifications.FirstOrDefaultAsync(x => x.Id == id);
            notification.SetDeletedFlag(deleted);
            await context.SaveChangesAsync();
        }

        public NotificationsEditDataModel GetNotificationsForEdit(Guid id)
        {
            using var context = GetContext();
            var notification = context.Notifications.FirstOrDefault(x => x.Id == id);
            if (notification == null)
            {
                return null;
            }

            return new NotificationsEditDataModel
            {
                Name = notification.Name,
                DisplayName = notification.DisplayName,
                Content = notification.Content,
                ThumbnailBytes = notification.ThumbnailBytes,
                ThumbnailImage = notification.ThumbnailBytes != null ? Tools.GetBase64Image(notification.ThumbnailBytes) : null,
                ThumbnailInfo = notification.ThumbnailBytes != null ? $"{Math.Ceiling(notification.ThumbnailBytes.Length / 1024.0)} kb" : null,
                IsHidden = notification.IsHidden,
                FromDate = notification.FromDate.ToLocalTime(),
                ToDate = notification.ToDate.ToLocalTime(),
                FromDateString = notification.FromDate.ToLocalTime().ToString("g"),
                ToDateString = notification.ToDate.ToLocalTime().ToString("g"),
                MinVersion = notification.MinVersion,
                MaxVersion = notification.MaxVersion,
                Priority = notification.Priority,
            };
        }

        public void UpdateNotification(Guid id, NotificationsEditDataModel model)
        {
            using var context = GetContext();
            var notification = context.Notifications.FirstOrDefault(x => x.Id == id);
            if (notification == null)
            {
                return;
            }

            notification.Name = model.Name;
            notification.DisplayName = model.DisplayName;
            notification.Content = model.Content;
            notification.ThumbnailBytes = model.ThumbnailBytes;
            notification.IsHidden = model.IsHidden;
            notification.FromDate = model.FromDate.ToUniversalTime();
            notification.ToDate = model.ToDate.ToUniversalTime();
            notification.MinVersion = model.MinVersion;
            notification.MaxVersion = model.MaxVersion;
            notification.Priority = model.Priority;
            notification.IsHighlighted = model.Priority == NotificationPriority.High;
            notification.UpdateServerModificationTime();
            context.SaveChanges();
        }

        public async Task<Notification[]> GetAllNotificationsAsync()
        {
            await using var context = GetContext();
            return await context.Notifications.Where(a => !a.Deleted).ToArrayAsync();
        }

        private static Expression<Func<Notification, bool>> GetNotificationsQueryPredicate(string query, bool deleted)
        {
            return (string.IsNullOrWhiteSpace(query) || query == "*")
                ? x => x.Deleted == deleted
                : x => x.Deleted == deleted && x.DisplayName.Contains(query);
        }
    }
}
