using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Models;

namespace Eurolook.ManagementConsole.Database
{
    public interface IStyleShortcutsDatabase
    {
        Task<StyleShortcut> CreateStyleShortcut(StyleShortcutDataModel model);

        int GetStyleShortcutsCount(string searchTerm, bool deleted);

        StyleShortcutDataModel GetStyleShortcutForEdit(Guid id);

        void UpdateStyleShortcut(Guid id, StyleShortcutDataModel model);

        Task DeleteStyleShortcut(Guid id, bool isDeleted);

        IEnumerable<StyleShortcutDataModel> GetStyleShortcuts(DataPage<StyleShortcutDataModel> page, string searchTerm, bool deleted);
    }
}