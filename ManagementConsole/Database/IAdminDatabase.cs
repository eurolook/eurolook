﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.ViewModels.DocumentModels;
using Eurolook.ManagementConsole.ViewModels.Texts;
using Microsoft.AspNetCore.Http;

namespace Eurolook.ManagementConsole.Database
{
    public interface IAdminDatabase
    {
        public List<DocumentModel> GetAllDocumentModels(bool deleted = false);

        public DocumentModel GetDocumentModel(Guid id);

        public DocumentModel GetDocumentModelForEdit(Guid id);

        public IEnumerable<DocumentModel> GetDocumentModels(DataPage<DocumentModel> page, string searchTerm, bool deleted);

        public int GetDocumentModelsCount(string searchTerm, bool deleted);

        public Text[] GetTextsForBrickEdit();

        public DocumentModel GetDocumentModelIncludingMetadataDefinitions(Guid id);

        public List<DocumentModel> GetDocumentModelsByBrick(Guid brickId);

        public DocumentModel CreateDocumentModel(DocumentModelDataModel dataModel);

        public void DeleteDocumentModel(Guid id);

        public Task<byte[]> UpdateDocumentModelPreview(Guid documentModelId, IFormFile file);

        public Task<byte[]> UpdateDocumentModelPreviewWithSampleText(Guid documentModelId, IFormFile file);

        public Task<byte[]> UpdateDocumentModelTemplate(Guid documentModelId, IFormFile file, string fileName);

        public Task UpdateDocumentModelAsync(DocumentModelsEditDataModel model);

        public List<DocumentStructure> GetDocumentStructures(Guid documentModelId, PositionType position);

        public List<DocumentStructure> GetDocumentStructures(Guid documentModelId, StoryType story);

        public void EditDocumentStructures(Guid docModelId, DocumentModelsStructureViewModel viewModel);

        public IEnumerable<BrickGroup> GetBrickGroups(DataPage<BrickGroup> page, string searchTerm, bool deleted);

        public int GetBrickGroupCount(string query, bool deleted);

        public BrickDataModel[] GetBricks(DataPage<BrickDataModel> page, string searchTerm, bool deleted);

        public BrickDataModel[] GetBricks(string query, bool deleted = false);

        public BrickDataModel[] GetBricksByGroupName(string query, bool deleted = false);

        public int GetBrickCount(string query, bool deleted);

        public Brick CreateBrick(BrickDataModel model);

        public IEnumerable<Brick> GetBricksUsingText(Text text);

        public Brick GetBrick(Guid id);

        public void UpdateBrick(Guid id, BricksEditDataModel viewModel);

        public Task<byte[]> UpdateBrickIcon(Guid brickId, IFormFile file);

        public Task<byte[]> UpdateBrickVectorIcon(Guid brickId, IFormFile file);

        public byte[] UpdateBrickIcon(Guid brickId, byte[] fileBytes);

        public void DeleteBrick(Guid id);

        public List<BrickGroup> GetAllBrickGroups(bool deleted);

        public BrickGroup CreateBrickGroup(BrickGroupDataModel model);

        public void DeleteBrickGroup(Guid id);

        public void UpdateBrickGroup(Guid id, BrickGroupsEditDataModel viewModel);

        public List<DocumentModel> GetDocumentModelsByBrickGroup(BrickGroup brickGroup);

        public Task<byte[]> UpdateBrickGroupIcon(Guid brickGroupId, IFormFile file);

        public Text CreateText(TextDataModel viewModel);

        public void DeleteTexts(List<TextDataModel> textDms);

        public List<string> QueryAliases(string q);

        public Text GetText(Guid id);

        public void UpdateText(TextEditViewModel text);

        public IEnumerable<Text> GetTexts(IPaginationInfo paginationInfo, string typeId, bool deleted, string searchQuery = null);

        public int GetTextCount(string typeId, bool deleted, string searchQuery = null);

        public List<Translation> GetTranslations(Guid? textId);

        public int GetTranslationCount(Text text);

        public int GetTranslationCount(Text text, bool deleted);

        public Resource CreateResource(ResourceDataModel model);

        public void DeleteResource(Guid id);

        public Resource GetResource(Guid id);

        public List<Resource> GetResourcesByBrick(Guid brickId);

        public List<Brick> GetBricksForResource(Guid resourceId);

        public LocalisedResource GetLocalisedResource(Guid resourceId, Guid languageId);

        public void UpdateResource(ResourceEditDataModel resource);

        public Task UpdateResourceRawData(Guid resourceId, IFormFile file, string fileName);

        public Task UpdateLocalisedResource(Guid resourceId, Guid languageId, IFormFile file, string fileNameDate);

        public Resource GetResourceForEdit(Guid id);

        public IEnumerable<Resource> GetResources(DataPage<Resource> page, string searchTerm, bool deleted);

        public int GetResourcesCount(string searchTerm, bool deleted);

        public UserFeedback[] GetUserFeedback(DateTime time, DataPage<UserFeedback> page);

        public IEnumerable<FeedbackInfo> GetUserFeedbackDownload(DateTime time);

        public int GetUserFeedbackCount(DateTime time);

        public void DeleteFeedback(List<UserFeedback> userFeedback);

        public List<OrgaEntity> GetAllDgs();

        public List<OrgaEntity> GetAllOrgaEntity();

        public DocumentCategory[] GetAllDocumentCategories();

        public Task UpdateDocumentModelLanguageTemplate(Guid documentModelId, Guid languageId, IFormFile file);

        List<Address> GetAddresses(bool includeSpecialAddressNoAddress = false);

        BrickGroup GetBrickGroupForEdit(Guid id);

        List<Brick> GetAllBricks();

        List<BrickCategory> GetAllBrickCategories();

        List<UserGroup> GetAllUserGroups();

        List<Text> GetAllTexts();

        List<Resource> GetAllResources();

        List<PredefinedFunction> GetPredefinedFunctions();

        List<SystemText> GetSystemTexts();

        List<SystemResource> GetSystemResources();
    }
}