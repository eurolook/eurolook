﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.InkML;
using DocumentFormat.OpenXml.Office2010.Excel;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Controllers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Eurolook.ManagementConsole.Database
{
    public class SystemConfigurationDatabase : EurolookManagementConsoleDatabase, ISystemConfigurationDatabase
    {
        public SystemConfigurationDatabase(IConfiguration configuration)
            : base(configuration)
        {
        }

        public async Task<List<SystemConfiguration>> GetSystemConfigurationsAsync()
        {
            using var context = GetContext();
            return await context.SystemConfigurations.Where(sc => !sc.Deleted)
                .OrderBy(sc => sc.Key)
                .ToListAsync();
        }

        public async Task<SystemConfiguration> CreateSystemConfigurationAsync(SystemConfigurationModel configuration)
        {
            using var context = GetContext();
            var key = configuration.Key;
            var systemConfig = await context.SystemConfigurations.FirstOrDefaultAsync(c => c.Key == key);

            if (systemConfig != null && systemConfig.Deleted)
            {
                systemConfig.SetDeletedFlag(false);
            }
            else if (systemConfig == null)
            {
                systemConfig = new SystemConfiguration();
                systemConfig.Init();
                systemConfig.Key = configuration.Key;
                systemConfig.Value = configuration.Value ?? "";

                context.SystemConfigurations.Add(systemConfig);
            }

            context.SaveChanges();
            return systemConfig;
        }

        public async Task UpdateSystemConfigurationAsync(Guid id, string value)
        {
            using var context = GetContext();
            var existingModel = await context.SystemConfigurations.FirstOrDefaultAsync(c => c.Id == id);

            if (existingModel != null)
            {
                existingModel.SetDeletedFlag(false);
                existingModel.Value = value;
            }

            await context.SaveChangesAsync();
        }

        public async Task DeleteSystemConfigurationAsync(Guid id)
        {
            using var context = GetContext();
            var systemConfig = await context.SystemConfigurations.FirstOrDefaultAsync(c => c.Id == id);
            systemConfig?.SetDeletedFlag(true);
            context.SaveChanges();
        }
    }
}
