using System.Collections.Generic;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Models;

namespace Eurolook.ManagementConsole.Database
{
    public interface IPersonNamesDatabase
    {
        IEnumerable<PersonName> GetPersonNames(DataPage<PersonName> page, string type, string searchText, bool deleted);

        void DeletePersonNames(IEnumerable<PersonName> personNames);

        int Count(string type, string searchText);

        void UpdatePersonNames(IEnumerable<PersonName> personNames);

        PersonName CreatePersonName(PersonName model);

        string GetChanges(IEnumerable<PersonName> newPersonNames, string category);

        string ApplyChanges(IEnumerable<PersonName> newPersonNames, string categoryName);
    }
}