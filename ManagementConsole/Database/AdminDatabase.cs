﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml.Linq;
using Eurolook.Common;
using Eurolook.Common.Log;
using Eurolook.Data;
using Eurolook.Data.Database;
using Eurolook.Data.Extensions;
using Eurolook.Data.Models;
using Eurolook.Data.Models.AuthorRoles;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.ViewModels.DocumentModels;
using Eurolook.ManagementConsole.ViewModels.Texts;
using JetBrains.Annotations;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Eurolook.ManagementConsole.Database
{
    public class AdminDatabase : EurolookManagementConsoleDatabase, IAdminDatabase
    {
        private readonly ILanguageRepository _languageRepository;

        public AdminDatabase(IConfiguration configuration)
            : base(configuration)
        {
            _languageRepository = new LanguageRepository(GetContext);
        }

        // *********************************************************************
        //                                                      DOCUMENT MODELS
        // *********************************************************************
        public List<DocumentModel> GetAllDocumentModels(bool deleted = false)
        {
            var context = GetContext();
            return context.DocumentModels.Where(x => x.Deleted == deleted).Include(dm => dm.Owner).ToList();
        }

        public DocumentModel GetDocumentModel(Guid id)
        {
            return GetContext().DocumentModels.FirstOrDefault(x => x.Id == id);
        }

        public DocumentModel GetDocumentModelForEdit(Guid id)
        {
            return GetContext().DocumentModels
                               .Include(dm => dm.DocumentLanguages)
                               .Include(dm => dm.AuthorRoles)
                               .FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<DocumentModel> GetDocumentModels(DataPage<DocumentModel> page, string searchTerm, bool deleted)
        {
            using (var context = GetContext())
            {
                return context.DocumentModels
                              .Where(GetDocumentModelQueryPredicate(searchTerm, deleted))
                              .OrderByAndPaginate(page)
                              .ToArray();
            }
        }

        public int GetDocumentModelsCount(string searchTerm, bool deleted)
        {
            using (var context = GetContext())
            {
                return context.DocumentModels.Count(GetDocumentModelQueryPredicate(searchTerm, deleted));
            }
        }

        private static Expression<Func<DocumentModel, bool>> GetDocumentModelQueryPredicate(string searchTerm, bool deleted)
        {
            Expression<Func<DocumentModel, bool>> predicate;
            if (string.IsNullOrWhiteSpace(searchTerm) || searchTerm == "*")
            {
                predicate = x => x.Deleted == deleted;
            }
            else
            {
                predicate = x => x.Deleted == deleted && x.Name.Contains(searchTerm);
            }

            return predicate;
        }

        public Text[] GetTextsForBrickEdit()
        {
            var result = new List<Text>();
            using (var context = GetContext())
            {
                result.AddRange(
                    context.BrickTexts
                           .Where(x => !x.Deleted)
                           .Include(x => x.Text)
                           .Select(x => x.Text)
                           .Distinct());
                result.AddRange(
                    context.Addresses
                           .Where(x => !x.Deleted)
                           .Include(x => x.NameText)
                           .Select(x => x.NameText));
                result.AddRange(
                    context.Addresses
                           .Where(x => !x.Deleted)
                           .Include(x => x.FooterText)
                           .Include(x => x.LocationText)
                           .Select(
                               x => new[]
                               {
                                   x.FooterText,
                                   x.LocationText,
                               })
                           .ToArray()
                           .SelectMany(x => x));
                result.AddRange(
                    context.PredefinedFunctions
                           .Where(x => !x.Deleted)
                           .Include(x => x.FunctionFemaleText)
                           .Include(x => x.FunctionHeaderFemaleText)
                           .Select(
                               x => new[]
                               {
                                   x.FunctionFemaleText,
                                   x.FunctionHeaderFemaleText,
                               })
                           .ToArray()
                           .SelectMany(x => x));
                result.AddRange(
                    context.PredefinedFunctions
                           .Where(x => !x.Deleted)
                           .Include(x => x.FunctionText)
                           .Include(x => x.FunctionHeaderText)
                           .Select(
                               x => new[]
                               {
                                   x.FunctionText,
                                   x.FunctionHeaderText,
                               })
                           .ToArray()
                           .SelectMany(x => x));
            }

            return result.Where(x => x != null && x.Alias != null).OrderBy(x => x.Alias).ToArray();
        }

        public DocumentModel GetDocumentModelIncludingMetadataDefinitions(Guid id)
        {
            using (var context = GetContext())
            {
                return context.DocumentModels
                    .AsNoTracking()
                    .Include(dm => dm.DocumentModelMetadataDefinitions).ThenInclude(dmmd => dmmd.MetadataDefinition)
                    .FirstOrDefault(x => x.Id == id);
            }
        }

        public List<DocumentModel> GetDocumentModelsByBrick(Guid brickId)
        {
            var context = GetContext();

            var models = (from dm in context.DocumentModels
                          join ds in context.DocumentStructure on dm.Id equals ds.DocumentModelId
                          where ds.BrickId == brickId && !ds.Deleted && !dm.Deleted
                          select dm).Distinct();

            return models.ToList();
        }

        public List<DocumentModel> GetDocumentModelsByBrickGroup(BrickGroup brickGroup)
        {
            return brickGroup.Bricks
                             .SelectMany(b => GetDocumentModelsByBrick(b.Id))
                             .DistinctBy(dm => dm.Id)
                             .OrderBy(dm => dm.Name)
                             .ToList();
        }

        public DocumentModel CreateDocumentModel(DocumentModelDataModel dataModel)
        {
            var context = GetContext();

            var model = new DocumentModel();
            model.Init();
            model.Name = dataModel.Name;
            model.DisplayName = dataModel.Name;

            var entityEntry = context.DocumentModels.Add(model);
            context.SaveChanges();
            return entityEntry.Entity;
        }

        public void DeleteDocumentModel(Guid id)
        {
            using (var context = GetContext())
            {
                foreach (var documentModelSetting in context.DocumentModelSettings.Where(x => x.DocumentModelId == id))
                {
                    documentModelSetting.SetDeletedFlag();
                }

                foreach (var docModelLang in context.DocumentModelLanguages.Where(x => x.DocumentModelId == id))
                {
                    docModelLang.SetDeletedFlag();
                }

                foreach (var docStructure in context.DocumentStructure.Where(x => x.DocumentModelId == id))
                {
                    docStructure.SetDeletedFlag();
                }

                foreach (var documentModelMetadataDefinition in context.DocumentModelMetadataDefinitions.Where(x => x.DocumentModelId == id))
                {
                    documentModelMetadataDefinition.SetDeletedFlag();
                }

                foreach (var documentModelAuthorRole in context.DocumentModelAuthorRoles.Where(x => x.DocumentModelId == id))
                {
                    documentModelAuthorRole.SetDeletedFlag();
                }

                DeleteUpdatable(context, context.DocumentModels, id);
                context.SaveChanges();
            }
        }

        public async Task<byte[]> UpdateDocumentModelPreview(Guid documentModelId, IFormFile file)
        {
            if (file == null)
            {
                return null;
            }

            return await UpdateHttpPostedFile(
                documentModelId,
                async documentModel =>
                {
                    var fileBytes = await file.GetBytes();
                    documentModel.PreviewImage = fileBytes;
                    documentModel.PreviewImageFileName = file.FileName;
                    return fileBytes;
                });
        }

        public async Task<byte[]> UpdateDocumentModelPreviewWithSampleText(Guid documentModelId, IFormFile file)
        {
            if (file == null)
            {
                return null;
            }

            return await UpdateHttpPostedFile(
                documentModelId,
                async documentModel =>
                {
                    var fileBytes = await file.GetBytes();
                    documentModel.PreviewImageWithSampleText = fileBytes;
                    documentModel.PreviewImageWithSampleTextFileName = file.FileName;
                    return fileBytes;
                });
        }

        public async Task<byte[]> UpdateDocumentModelTemplate(Guid documentModelId, IFormFile file, string fileName)
        {
            if (file == null)
            {
                return null;
            }

            return await UpdateHttpPostedFile(
                documentModelId,
                async documentModel =>
                {
                    var fileBytes = await file.GetBytes();
                    documentModel.Template = fileBytes;
                    documentModel.TemplateFileName = fileName;
                    return fileBytes;
                });
        }

        private async Task<byte[]> UpdateHttpPostedFile(Guid documentModelId, Func<DocumentModel, Task<byte[]>> updateFieldsAction)
        {
            var context = GetContext();
            var dbDocumentModel = context.DocumentModels.FirstOrDefault(x => x.Id == documentModelId);

            if (dbDocumentModel == null)
            {
                return null;
            }

            var fileBytes = await updateFieldsAction(dbDocumentModel);

            dbDocumentModel.UpdateServerModificationTime();
            context.SaveChanges();

            return fileBytes;
        }

        public async Task UpdateDocumentModelAsync(DocumentModelsEditDataModel model)
        {
            var context = GetContext();
            var dbDocumentModel = context.DocumentModels.FirstOrDefault(x => x.Id == model.Id);

            if (dbDocumentModel == null)
            {
                return;
            }

            dbDocumentModel.Name = model.Name;
            dbDocumentModel.DisplayName = model.DisplayName;
            dbDocumentModel.Description = model.Description;
            dbDocumentModel.Keywords = model.Keywords;
            dbDocumentModel.IsClassicDocument = model.IsClassicDocument;
            dbDocumentModel.IsHidden = model.IsHidden;
            dbDocumentModel.CreateDocumentWizardPages = model.CreateDocumentWizardPages;
            dbDocumentModel.HideBrickChooser = model.HideBrickChooser;
            dbDocumentModel.MinVersion = model.MinVersion;
            dbDocumentModel.MaxVersion = model.MaxVersion;
            dbDocumentModel.UiPositionIndex = model.UIPositionIndex;
            if (model.SelectedCategoryId != null && model.SelectedCategoryId != Guid.Empty)
            {
                dbDocumentModel.DocumentCategoryId = model.SelectedCategoryId.Value;
            }
            else
            {
                dbDocumentModel.DocumentCategoryId = null;
            }

            // Check for language selections added by the user
            var currentDocumentLanguageIds = context.DocumentModelLanguages.Where(dml => dml.DocumentModelId == dbDocumentModel.Id).Select(dml => dml.DocumentLanguageId).ToList();
            foreach (var selectedLanguageId in model.DocumentModelLanguages.Where(x => x.IsSelected).Select(x => x.LanguageId))
            {
                if (!currentDocumentLanguageIds.Contains(selectedLanguageId))
                {
                    // it is totally new, create it
                    var dml = new DocumentModelLanguage();
                    dml.Init();
                    dml.DocumentModelId = model.Id;
                    dml.DocumentLanguageId = selectedLanguageId;
                    context.DocumentModelLanguages.Add(dml);
                }
                else
                {
                    // the relation already exists, check if it is deleted?
                    var dml = context.DocumentModelLanguages.FirstOrDefault(x => x.DocumentModelId == dbDocumentModel.Id && x.DocumentLanguageId == selectedLanguageId);
                    if (dml == null || !dml.Deleted)
                    {
                        continue;
                    }

                    // re-enable it
                    dml.Deleted = false;
                    dml.UpdateServerModificationTime();
                }
            }

            // Check for language selections remove by the user
            foreach (var unselectedLanguageId in model.DocumentModelLanguages.Where(x => !x.IsSelected).Select(x => x.LanguageId))
            {
                if (!currentDocumentLanguageIds.Contains(unselectedLanguageId))
                {
                    continue;
                }

                var dml = context.DocumentModelLanguages.FirstOrDefault(x => x.DocumentModelId == dbDocumentModel.Id && x.DocumentLanguageId == unselectedLanguageId);
                if (dml != null)
                {
                    DeleteUpdatable(context, context.DocumentModelLanguages, dml.Id);
                }
            }

            await UpdateAuthorRolesAsync(context, dbDocumentModel, model);

            dbDocumentModel.UpdateServerModificationTime();
            context.SaveChanges();
        }

        private async Task UpdateAuthorRolesAsync(
            EurolookServerContext context,
            DocumentModel dbDocumentModel,
            DocumentModelsEditDataModel model)
        {
            var selectedAuthorRoles = GetSelectedDocumentModelAuthorRoles(model, dbDocumentModel.Id);
            foreach (var documentModelAuthorRole in selectedAuthorRoles)
            {
                await context.DocumentModelAuthorRoles.CreateOrUpdateAsync(
                    documentModelAuthorRole,
                    context,
                    dmar => dmar.DocumentModelId == dbDocumentModel.Id
                            && dmar.AuthorRoleId == documentModelAuthorRole.AuthorRoleId);
            }

            var unselectedAuthorRoles = GetUnselectedDocumentModelAuthorRoles(context, model, dbDocumentModel.Id);
            foreach (var documentModelAuthorRole in unselectedAuthorRoles)
            {
                DeleteUpdatable(context, context.DocumentModelAuthorRoles, documentModelAuthorRole.Id);
            }
        }

        private IEnumerable<DocumentModelAuthorRole> GetSelectedDocumentModelAuthorRoles(
            DocumentModelsEditDataModel model,
            Guid dbDocumentModelId)
        {
            return model.DocumentModelAuthorRoles
                        .Where(x => x.IsSelected)
                        .Select(
                            x => new DocumentModelAuthorRole
                            {
                                AuthorRoleId = x.AuthorRoleId,
                                DocumentModelId = dbDocumentModelId,
                                Description = x.Description,
                            });
        }

        private IEnumerable<DocumentModelAuthorRole> GetUnselectedDocumentModelAuthorRoles(
            EurolookServerContext context,
            DocumentModelsEditDataModel model,
            Guid dbDocumentModelId)
        {
            var currentAuthorRoles = context.DocumentModelAuthorRoles
                                            .Where(dmar => dmar.DocumentModelId == dbDocumentModelId)
                                            .ToList();

            return model.DocumentModelAuthorRoles
                        .Where(x => !x.IsSelected)
                        .Select(x => currentAuthorRoles.FirstOrDefault(ar => ar.AuthorRoleId == x.AuthorRoleId))
                        .Where(dmar => dmar != null);
        }

        public List<DocumentStructure> GetDocumentStructures(Guid documentModelId, PositionType position)
        {
            using (var context = GetContext())
            {
                var query = context.DocumentStructure
                                   .Where(ds => ds.DocumentModelId == documentModelId && position == (PositionType)ds.RawPosition && !ds.Deleted)
                                   .Include(ds => ds.Brick);

                // sort cursor and command bricks as they appear in the Eurolook Task Pane,
                // all other elements are sorted by their position within the document structure
                return position == PositionType.Cursor
                    ? query.OrderBy(ds => ds.Brick.Category.UiPositionIndex).ThenBy(ds => ds.Brick.UiPositionIndex).ThenBy(ds => ds.Brick.DisplayName).ToList()
                    : query.OrderBy(ds => ds.VerticalPositionIndex).ToList();
            }
        }

        public List<DocumentStructure> GetDocumentStructures(Guid documentModelId, StoryType story)
        {
            var context = GetContext();
            var positions = EnumHelper.GetPositionTypes(story);
            return context.DocumentStructure.Where(ds => ds.DocumentModelId == documentModelId && positions.Contains((PositionType)ds.RawPosition) && !ds.Deleted).
                Include(ds => ds.Brick).OrderBy(ds => ds.VerticalPositionIndex).ToList();
        }

        public void EditDocumentStructures(Guid docModelId, DocumentModelsStructureViewModel viewModel)
        {
            var context = GetContext();
            EditDocumentStructures(docModelId, PositionType.Cursor, viewModel.CursorStructures, context);
            EditDocumentStructures(docModelId, StoryType.Header, viewModel.HeaderStructures, context);
            EditDocumentStructures(docModelId, StoryType.Begin, viewModel.BeginStructures, context);
            EditDocumentStructures(docModelId, StoryType.Body, viewModel.BodyStructures, context);
            EditDocumentStructures(docModelId, StoryType.End, viewModel.EndStructures, context);
            EditDocumentStructures(docModelId, StoryType.Footer, viewModel.FooterStructures, context);
            context.SaveChanges();
        }

        private void EditDocumentStructures(Guid docModelId, PositionType position, List<DocumentStructureViewModel> structureVms, EurolookContext context)
        {
            // Add or Update
            foreach (var structureVm in structureVms)
            {
                AddOrUpdateDocumentStructure(context, docModelId, structureVm, StoryType.None, position);
            }

            // Delete
            var oldStructures = GetDocumentStructures(docModelId, position);
            foreach (var structure in oldStructures)
            {
                if (structureVms.Count(vm => vm.BrickId == structure.BrickId) == 0)
                {
                    DeleteUpdatable(context, context.DocumentStructure, structure.Id);
                }
            }
        }

        private void EditDocumentStructures(Guid docModelId, StoryType story, List<DocumentStructureViewModel> structureVms, EurolookContext context)
        {
            // Add or Update
            foreach (var structureVm in structureVms)
            {
                var position = EnumHelper.GetPosition(story, structureVm.IsFirstPage, structureVm.IsOddPages, structureVm.IsEvenPages);
                AddOrUpdateDocumentStructure(context, docModelId, structureVm, story, position);
            }

            // Delete
            var oldStructures = GetDocumentStructures(docModelId, story);
            foreach (var structure in oldStructures)
            {
                if (structureVms.Count(vm => vm.BrickId == structure.BrickId) == 0)
                {
                    DeleteUpdatable(context, context.DocumentStructure, structure.Id);
                }
            }
        }

        private void AddOrUpdateDocumentStructure(EurolookContext context, Guid docModelId, DocumentStructureViewModel structureVm, StoryType story, PositionType position)
        {
            if (structureVm?.BrickId == null)
            {
                throw new ArgumentNullException(nameof(structureVm));
            }

            // Check if a DocumentStructure can be recycled.
            DocumentStructure recyclingStructure = null;
            if (story == StoryType.None)
            {
                recyclingStructure = FindDocumentStructure(context, docModelId, structureVm.BrickId.Value, 1, position);
            }
            else
            {
                var activeStoryStructure = FindActiveDocumentStructure(context, docModelId, structureVm.BrickId.Value, 1, story);
                var bestMatch = FindDocumentStructure(context, docModelId, structureVm.BrickId.Value, 1, position);
                if (activeStoryStructure != null && bestMatch != null && activeStoryStructure.Id != bestMatch.Id)
                {
                    // The user changed the position of the structure.
                    // Delete the active structure and recylce the better match.
                    DeleteUpdatable(context, context.DocumentStructure, activeStoryStructure.Id);
                    recyclingStructure = bestMatch;
                }
                else if (bestMatch != null)
                {
                    recyclingStructure = bestMatch;
                }
                else if (activeStoryStructure != null)
                {
                    recyclingStructure = activeStoryStructure;
                }
            }

            // Recycle the existing structure
            if (recyclingStructure != null)
            {
                bool hasChanged = false;
                if (recyclingStructure.Position != position)
                {
                    recyclingStructure.Position = position;
                    hasChanged = true;
                }

                if (recyclingStructure.AutoBrickCreationType != structureVm.AutoBrickCreationType)
                {
                    recyclingStructure.AutoBrickCreationType = structureVm.AutoBrickCreationType;
                    hasChanged = true;
                }

                if (recyclingStructure.VerticalPositionIndex != structureVm.PositionIndex)
                {
                    recyclingStructure.VerticalPositionIndex = structureVm.PositionIndex;
                    hasChanged = true;
                }

                if (recyclingStructure.Deleted)
                {
                    recyclingStructure.Deleted = false;
                    hasChanged = true;
                }

                if (hasChanged)
                {
                    recyclingStructure.UpdateServerModificationTime();
                }
            }
            else
            {
                // Create a new entry.
                var structure = new DocumentStructure();
                structure.Init();
                structure.DocumentModelId = docModelId;
                structure.BrickId = structureVm.BrickId.Value;
                structure.Position = position;
                structure.VerticalPositionIndex = structureVm.PositionIndex;
                structure.AutoBrickCreationType = structureVm.AutoBrickCreationType;
                structure.SectionId = 1;
                context.DocumentStructure.Add(structure);
            }
        }

        private DocumentStructure FindDocumentStructure(EurolookContext context, Guid docModelId, Guid brickId, int sectionId, PositionType position)
        {
            return context.DocumentStructure.FirstOrDefault(ds => ds.DocumentModelId == docModelId &&
                                                                  ds.BrickId == brickId &&
                                                                  ds.SectionId == sectionId &&
                                                                  ds.RawPosition == (int)position);
        }

        private DocumentStructure FindActiveDocumentStructure(EurolookContext context, Guid docModelId, Guid brickId, int sectionId, StoryType story)
        {
            var positions = EnumHelper.GetPositionTypes(story).Select(p => (int)p).ToList();
            return context.DocumentStructure.FirstOrDefault(ds => ds.DocumentModelId == docModelId &&
                                                                  ds.BrickId == brickId &&
                                                                  ds.SectionId == sectionId &&
                                                                  positions.Contains(ds.RawPosition) &&
                                                                  !ds.Deleted);
        }

        // *********************************************************************
        //                                                          BRICK GROUPS
        // *********************************************************************
        public IEnumerable<BrickGroup> GetBrickGroups(DataPage<BrickGroup> page, string searchTerm, bool deleted)
        {
            using (var context = GetContext())
            {
                return context.BrickGroups
                              .Where(GetBrickGroupQueryPredicate(searchTerm, deleted))
                              .OrderByAndPaginate(page)
                              .ToArray();
            }
        }

        public int GetBrickGroupCount(string query, bool deleted)
        {
            using (var context = GetContext())
            {
                return context.BrickGroups.Count(GetBrickGroupQueryPredicate(query, deleted));
            }
        }

        private static Expression<Func<BrickGroup, bool>> GetBrickGroupQueryPredicate(string query, bool deleted)
        {
            Expression<Func<BrickGroup, bool>> predicate;
            if (string.IsNullOrWhiteSpace(query) || query == "*")
            {
                predicate = b => b.Deleted == deleted;
            }
            else
            {
                predicate = b => b.Deleted == deleted && b.Name.Contains(query);
            }

            return predicate;
        }

        // *********************************************************************
        //                                                               BRICKS
        // *********************************************************************
        public BrickDataModel[] GetBricks(DataPage<BrickDataModel> page, string searchTerm, bool deleted)
        {
            using (var context = GetContext())
            {
                return context.Bricks
                              .Include(b => b.Category)
                              .Where(GetBrickQueryPredicate(searchTerm, deleted))
                              .OrderByAndPaginate(page)
                              .Select(x => new BrickDataModel(x))
                              .ToArray();
            }
        }

        public BrickDataModel[] GetBricks(string query, bool deleted = false)
        {
            using (var context = GetContext())
            {
                return context.Bricks
                              .Where(GetBrickQueryPredicate(query, deleted))
                              .OrderBy(b => b.Name)
                              .Select(x => new BrickDataModel(x))
                              .ToArray();
            }
        }

        public BrickDataModel[] GetBricksByGroupName(string query, bool deleted = false)
        {
            using (var context = GetContext())
            {
                var brickGroupIds = context.BrickGroups
                                         .Where(GetBrickGroupQueryPredicate(query, deleted))
                                         .Select(bg => bg.Id)
                                         .ToArray();

                return context.Bricks
                              .Include(b => b.Group)
                              .Where(b => b.Deleted == deleted && b.Group.Name != null && brickGroupIds.Contains((Guid)b.GroupId))
                              .OrderBy(b => b.Group.Name)
                              .ThenBy(b => b.Name)
                              .Select(x => new BrickDataModel(x))
                              .ToArray();
            }
        }

        public int GetBrickCount(string query, bool deleted)
        {
            using (var context = GetContext())
            {
                return context.Bricks.Count(GetBrickQueryPredicate(query, deleted));
            }
        }

        private static Expression<Func<Brick, bool>> GetBrickQueryPredicate(string query, bool deleted)
        {
            Expression<Func<Brick, bool>> predicate = b => !(b is BuildingBlockBrick);
            if (string.IsNullOrWhiteSpace(query) || query == "*")
            {
                predicate = predicate.And(b => b.Deleted == deleted);
            }
            else
            {
                predicate = predicate.And(b => b.Deleted == deleted && b.Name.Contains(query));
            }

            return predicate;
        }

        public Brick CreateBrick(BrickDataModel model)
        {
            // create the brick object
            var dataDll = Assembly.GetAssembly(typeof(Brick));
            var brickType = dataDll.GetType(model.BrickClass);
            var ctor = brickType.GetConstructor(new Type[0]);
            var brick = ctor.Invoke(null) as Brick;
            brick.Init();
            brick.Name = model.Name;
            brick.DisplayName = model.DisplayName ?? model.Name ?? "";

            var context = GetContext();

            // add a category
            if (brick.CategoryId == Guid.Empty)
            {
                brick.Category = context.BrickCategories.FirstOrDefault();
            }

            // preset content
            if (brickType == typeof(DynamicBrick))
            {
                if (brick is DynamicBrick dynamicBrick)
                {
                    dynamicBrick.RawContent = XDocument.Parse(
                        "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">" +
                        "<xsl:output method=\"xml\" indent=\"yes\" />" +
                        "<xsl:template match=\"/\">" +
                        "<html></html>" +
                        "</xsl:template>" +
                        "</xsl:stylesheet>").ToString();
                }
            }
            else if (brickType == typeof(ContentBrick) && brick is ContentBrick contentBrick)
            {
                contentBrick.RawContent = XDocument.Parse("<html></html>").ToString();
            }

            // save it
            var entityEntry = context.Bricks.Add(brick);
            context.SaveChanges();
            return entityEntry.Entity;
        }

        public IEnumerable<Brick> GetBricksUsingText(Text text)
        {
            using (var context = GetContext())
            {
                return context.BrickTexts
                              .Where(bt => !bt.Deleted && bt.TextId == text.Id)
                              .Select(bt => bt.Brick)
                              .ToList();
            }
        }

        [CanBeNull]
        public new Brick GetBrick(Guid id)
        {
            using (var context = GetContext())
            {
                return context.Bricks.Include(b => b.Texts).ThenInclude(t => t.Text).FirstOrDefault(x => x.Id == id);
            }
        }

        public void UpdateBrick(Guid id, BricksEditDataModel viewModel)
        {
            var context = GetContext();
            var dbBrick = context.Bricks.FirstOrDefault(b => b.Id == id);
            if (dbBrick == null)
            {
                throw new Exception("Could not find brick with id " + id);
            }

            dbBrick.CategoryId = viewModel.SelectedCategory;
            dbBrick.GroupId = viewModel.SelectedGroupId != Guid.Empty ? viewModel.SelectedGroupId : null;

            dbBrick.Name = viewModel.Name;
            dbBrick.Description = viewModel.Description;
            dbBrick.Acronym = viewModel.Acronym;
            dbBrick.DisplayName = viewModel.DisplayName;
            dbBrick.HotKey = viewModel.HotKey;
            dbBrick.UiPositionIndex = viewModel.UIPositionIndex;
            dbBrick.IsHidden = !viewModel.IsVisible;
            var idsOfSelectedUserGroups = viewModel.UserGroups.Where(ug => ug.IsSelected).Select(ug => ug.Id);
            dbBrick.VisibleToUserGroups = string.Join(";", idsOfSelectedUserGroups);
            dbBrick.ColorLuminance = viewModel.ColorLuminance;
            dbBrick.MinVersion = string.IsNullOrWhiteSpace(viewModel.MinVersion) ? null : viewModel.MinVersion;
            dbBrick.MaxVersion = string.IsNullOrWhiteSpace(viewModel.MaxVersion) ? null : viewModel.MaxVersion;
            if (viewModel.IsIconDeleted)
            {
                dbBrick.Icon = null;
            }

            if (dbBrick is ContentBrick dbContentBrick)
            {
                var newXml = new XDocument();
                if (!string.IsNullOrWhiteSpace(viewModel.Content))
                {
                    newXml = XDocument.Parse(viewModel.Content);
                }

                UpdateTexts(context, dbBrick.Id, newXml);
                UpdateResources(context, dbBrick.Id, newXml);
                dbContentBrick.ContentBrickActionsClassName = viewModel.ContentBrickActionsClassName;
                dbContentBrick.RawContent = XmlFormatter.Linearize(viewModel.Content);
                dbContentBrick.IsMultiInstance = viewModel.IsMultiInstance;
            }

            if (dbBrick is CommandBrick dbCommandBrick)
            {
                dbCommandBrick.CommandClass = viewModel.Command;
                dbCommandBrick.Argument1 = viewModel.CommandArgument;
                dbCommandBrick.HasCustomUi = viewModel.HasCustomUi;
                dbCommandBrick.Configuration = viewModel.Configuration;

                UpdateTexts(context, dbBrick.Id, viewModel.BrickTexts);
                var newAliases = viewModel.BrickResources?.Select(br => br.Alias).ToList() ?? new List<string>();
                UpdateResources(context, dbBrick.Id, newAliases);
            }

            dbBrick.UpdateServerModificationTime();
            context.SaveChanges();
        }

        private void UpdateTexts(EurolookContext context, Guid brickId, XDocument brickXml)
        {
            var newAliases = ExtractAliasesFromBdl(brickXml);
            UpdateTexts(context, brickId, newAliases);
        }

        private void UpdateTexts(EurolookServerContext context, Guid brickId, List<TextDataModel> brickTexts)
        {
            var newAliases = (brickTexts ?? Enumerable.Empty<TextDataModel>()).Select(bt => bt.Alias).ToList();
            UpdateTexts(context, brickId, newAliases);
        }

        private void UpdateTexts(EurolookContext context, Guid brickId, List<string> newAliases)
        {
            var now = DateTime.UtcNow;

            foreach (string newAlias in newAliases)
            {
                // the Text must already exist
                var text = context.Texts.FirstOrDefault(x => x.Alias == newAlias);
                if (text != null)
                {
                    // check if there is a deleted BrickTexts reference
                    var brickText = context.BrickTexts.FirstOrDefault(x => x.BrickId == brickId && x.TextId == text.Id);
                    if (brickText == null)
                    {
                        brickText = new BrickText
                        {
                            BrickId = brickId,
                            TextId = text.Id,
                        };
                        brickText.Init();
                        context.BrickTexts.Add(brickText);
                    }
                    else
                    {
                        // there is a deleted reference in the db, reactivate it
                        if (brickText.Deleted)
                        {
                            brickText.Deleted = false;
                            brickText.UpdateServerModificationTime();
                        }
                    }
                }
                else
                {
                    // Text does not exist, don't create one. Throw exception!
                    throw new Exception($"The referenced text '{newAlias}' does not exist. Please create the text first.");
                }
            }

            // remove no longer used texts from the brick
            var brickTextsToDelete = from text in context.Texts
                                     join brickText in context.BrickTexts on text.Id equals brickText.TextId
                                     where !brickText.Deleted && brickText.BrickId == brickId && !newAliases.Contains(text.Alias)
                                     select brickText;
            foreach (var oldAlias in brickTextsToDelete)
            {
                oldAlias.Deleted = true;
                oldAlias.UpdateServerModificationTime();
            }
        }

        private List<string> ExtractAliasesFromBdl(XDocument brickXml)
        {
            const string txtIndicator = "/Texts/";

            var newAliases = brickXml.Descendants("span")
                .Where(x => (string)x.Attribute("data-type") == "binding" && ((string)x.Attribute("data-prop"))?.StartsWith(txtIndicator) == true)
                .Select(x => ((string)x.Attribute("data-prop"))?.Replace(txtIndicator, "")).ToList();

            newAliases.AddRange(brickXml.Descendants("span")
                .Where(x => ((string)x.Attribute("data-type"))?.StartsWith("date", StringComparison.InvariantCulture) == true
                            && ((string)x.Attribute("data-prop"))?.StartsWith(txtIndicator, StringComparison.InvariantCulture) == true)
                .Select(x => ((string)x.Attribute("data-prop"))?.Replace(txtIndicator, "")));

            newAliases.AddRange(brickXml.Descendants("option")
                .Where(x => (string)x.Attribute("data-type") == "binding"
                            && ((string)x.Attribute("data-prop"))?.StartsWith(txtIndicator, StringComparison.InvariantCulture) == true)
                .Select(x => ((string)x.Attribute("data-prop"))?.Replace(txtIndicator, "")));

            newAliases.AddRange(brickXml.Descendants("span")
                .Where(x => (string)x.Attribute("data-type") == "formattedText" && ((string)x.Attribute("data-prop"))?.StartsWith(txtIndicator) == true)
                .Select(x => ((string)x.Attribute("data-prop"))?.Replace(txtIndicator, "")).ToList());

            return newAliases.Distinct().ToList();
        }

        private void UpdateResources(EurolookContext context, Guid brickId, List<string> newAliases)
        {
            foreach (var alias in newAliases)
            {
                // the Resource must already exist
                var resource = context.Resources.FirstOrDefault(x => x.Alias == alias);
                if (resource != null)
                {
                    // the resource exists, check if there is a deleted BrickResources reference
                    var brickResource = context.BrickResources.FirstOrDefault(x => x.BrickId == brickId && x.ResourceId == resource.Id);
                    if (brickResource == null)
                    {
                        brickResource = new BrickResource
                        {
                            BrickId = brickId,
                            ResourceId = resource.Id,
                        };
                        brickResource.Init();
                        context.BrickResources.Add(brickResource);
                    }
                    else
                    {
                        // there is a deleted reference in the db, reactivate it
                        if (brickResource.Deleted)
                        {
                            brickResource.SetDeletedFlag(false);
                        }
                    }
                }
                else
                {
                    // Resource does not exist, don't create one. Throw exception!
                    throw new Exception($"The referenced resource '{alias}' does not exist. Please create the resource first.");
                }
            }

            // remove no longer used resources from the brick
            var brickResourcesToDelete = from resource in context.Resources
                                         join brickResource in context.BrickResources on resource.Id equals brickResource.ResourceId
                                         where brickResource.BrickId == brickId && !newAliases.Contains(resource.Alias)
                                         select brickResource;
            foreach (var oldBrickResource in brickResourcesToDelete)
            {
                oldBrickResource.SetDeletedFlag();
            }
        }

        private void UpdateResources(EurolookContext context, Guid brickId, XDocument brickXml)
        {
            var newImgSrcs = brickXml.Descendants("img").Select(x => x.Attribute("src").Value).Distinct().ToList();
            var newExcelSrcs = brickXml.Descendants("embed").Select(x => x.Attribute("src").Value).Distinct().ToList();

            newImgSrcs.AddRange(newExcelSrcs);

            foreach (string newImgAlias in newImgSrcs)
            {
                // the Resource must already exist
                var resource = context.Resources.FirstOrDefault(x => x.Alias == newImgAlias);
                if (resource != null)
                {
                    // the resource exists, check if there is a deleted BrickResources reference
                    var brickResource = context.BrickResources.FirstOrDefault(x => x.BrickId == brickId && x.ResourceId == resource.Id);
                    if (brickResource == null)
                    {
                        brickResource = new BrickResource
                        {
                            BrickId = brickId,
                            ResourceId = resource.Id,
                        };
                        brickResource.Init();
                        context.BrickResources.Add(brickResource);
                    }
                    else
                    {
                        // there is a deleted reference in the db, reactivate it
                        if (brickResource.Deleted)
                        {
                            brickResource.SetDeletedFlag(false);
                        }
                    }
                }
                else
                {
                    // Resource does not exist, don't create one. Throw exception!
                    throw new Exception($"The referenced resource '{newImgAlias}' does not exist. Please create the resource first.");
                }
            }

            // remove no longer used resources from the brick
            var brickResourcesToDelete = from resource in context.Resources
                                         join brickResource in context.BrickResources on resource.Id equals brickResource.ResourceId
                                         where brickResource.BrickId == brickId && !newImgSrcs.Contains(resource.Alias)
                                         select brickResource;
            foreach (var oldBrickResource in brickResourcesToDelete)
            {
                oldBrickResource.SetDeletedFlag();
            }
        }

        public async Task<byte[]> UpdateBrickIcon(Guid brickId, IFormFile file)
        {
            if (file == null)
            {
                return null;
            }

            var context = GetContext();
            var dbBrick = context.Bricks.FirstOrDefault(x => x.Id == brickId);
            if (dbBrick == null)
            {
                return null;
            }

            var fileBytes = await file.GetBytes();
            dbBrick.Icon = fileBytes;
            dbBrick.UpdateServerModificationTime();
            context.SaveChanges();
            return fileBytes;
        }

        public async Task<byte[]> UpdateBrickVectorIcon(Guid brickId, IFormFile file)
        {
            if (file == null)
            {
                return null;
            }

            var context = GetContext();
            var dbBrick = context.Bricks.FirstOrDefault(x => x.Id == brickId);
            if (dbBrick == null)
            {
                return null;
            }

            var fileBytes = await file.GetBytes();
            dbBrick.VectorIcon = fileBytes;
            dbBrick.UpdateServerModificationTime();
            context.SaveChanges();
            return fileBytes;
        }

        public byte[] UpdateBrickIcon(Guid brickId, byte[] fileBytes)
        {
            if (fileBytes == null)
            {
                return null;
            }

            var context = GetContext();
            var dbBrick = context.Bricks.FirstOrDefault(x => x.Id == brickId);

            if (dbBrick == null)
            {
                return null;
            }

            dbBrick.Icon = fileBytes;
            dbBrick.UpdateServerModificationTime();
            context.SaveChanges();
            return fileBytes;
        }

        public void DeleteBrick(Guid id)
        {
            var context = GetContext();

            // Check if a document structure exists
            var structures = context.DocumentStructure.Where(x => x.BrickId == id && !x.Deleted).Include(x => x.DocumentModel).ToList();
            if (structures.Count == 1)
            {
                throw new Exception($"The document model '{structures.First().DocumentModel.Name}' is still referencing the brick.");
            }

            if (structures.Count > 1)
            {
                throw new Exception(
                    $"There are still {structures.Count} document models referencing the brick: {string.Join(", ", structures.Select(x => x.DocumentModel.Name).ToArray())}.");
            }

            // Delete brick text references
            foreach (var brickText in context.BrickTexts.Where(x => x.BrickId == id))
            {
                DeleteUpdatable(brickText);
            }

            // Delete brick resource references
            foreach (var brickResource in context.BrickResources.Where(x => x.BrickId == id))
            {
                DeleteUpdatable(brickResource);
            }

            // Delete the brick itself
            DeleteUpdatable(context, context.Bricks, id);

            context.SaveChanges();
        }

        // *********************************************************************
        //                                                         BRICK GROUPS
        // *********************************************************************
        public List<BrickGroup> GetAllBrickGroups(bool deleted)
        {
            var context = GetContext();
            return context.BrickGroups.Where(bg => bg.Deleted == deleted).OrderBy(b => b.Name).ToList();
        }

        public BrickGroup CreateBrickGroup(BrickGroupDataModel model)
        {
            var brickGroup = new BrickGroup();
            brickGroup.Init();
            brickGroup.Name = model.Name;

            using (var context = GetContext())
            {
                var entityEntry = context.BrickGroups.Add(brickGroup);
                context.SaveChanges();
                return entityEntry.Entity;
            }
        }

        public void DeleteBrickGroup(Guid id)
        {
            using (var context = GetContext())
            {
                // Check if there is still a brick in this group
                var bricks = context.Bricks.Where(x => x.GroupId == id && !x.Deleted).ToList();
                if (bricks.Count == 1)
                {
                    throw new Exception($"The group still contains the brick '{bricks.First().Name}'.");
                }

                if (bricks.Count > 1)
                {
                    throw new Exception(
                        $"The group still contains {bricks.Count} bricks: {string.Join(", ", bricks.Select(x => x.Name).ToArray())}.");
                }

                // Delete the group
                DeleteUpdatable(context, context.BrickGroups, id);

                context.SaveChanges();
            }
        }

        public void UpdateBrickGroup(Guid id, BrickGroupsEditDataModel viewModel)
        {
            var context = GetContext();
            var dbBrickGroup = context.BrickGroups.FirstOrDefault(b => b.Id == id);
            if (dbBrickGroup == null)
            {
                throw new Exception("Could not find brick group with id " + id);
            }

            dbBrickGroup.Name = viewModel.Name;
            dbBrickGroup.DisplayName = viewModel.DisplayName;
            dbBrickGroup.Description = viewModel.Description;
            dbBrickGroup.Acronym = viewModel.Acronym;
            dbBrickGroup.UiPositionIndex = viewModel.UiPositionIndex;
            dbBrickGroup.ColorLuminance = viewModel.ColorLuminance;
            dbBrickGroup.IsMultipleChoice = viewModel.IsMultipleChoice;
            if (viewModel.IsIconDeleted)
            {
                dbBrickGroup.Icon = null;
            }

            dbBrickGroup.UpdateServerModificationTime();
            context.SaveChanges();
        }

        public async Task<byte[]> UpdateBrickGroupIcon(Guid brickGroupId, IFormFile file)
        {
            if (file == null)
            {
                return null;
            }

            var context = GetContext();
            var dbBrickGroup = context.BrickGroups.FirstOrDefault(x => x.Id == brickGroupId);

            if (dbBrickGroup == null)
            {
                return null;
            }

            var fileBytes = await file.GetBytes();
            dbBrickGroup.Icon = fileBytes;
            dbBrickGroup.UpdateServerModificationTime();
            context.SaveChanges();
            return fileBytes;
        }

        // *********************************************************************
        //                                               TEXTS AND TRANSLATIONS
        // *********************************************************************
        public Text CreateText(TextDataModel viewModel)
        {
            var en = _languageRepository.GetEnglishLanguage();

            var context = GetContext();

            string cleanAlias = Tools.GetCleanXmlName(viewModel.Alias);

            // check if there is a deleted text with the same alias
            var text = context.Texts.FirstOrDefault(t => t.Deleted && t.Alias == cleanAlias);
            if (text != null)
            {
                // recycle
                text.SetDeletedFlag(false);
            }
            else
            {
                text = new Text
                {
                    Alias = cleanAlias,
                };
                text.Init();
                context.Texts.Add(text);
            }

            var enTranslation = context.Translations.FirstOrDefault(t => t.Text.Id == text.Id && t.LanguageId == en.Id);
            if (enTranslation != null)
            {
                // recycle
                enTranslation.SetDeletedFlag(false);
                enTranslation.Value = text.Alias;
            }
            else
            {
                enTranslation = new Translation
                {
                    TextId = text.Id,
                    LanguageId = en.Id,
                    Value = text.Alias,
                };
                enTranslation.Init();
                context.Translations.Add(enTranslation);
            }

            var systemText = context.SystemTexts.FirstOrDefault(t => t.TextId == text.Id);
            if (systemText != null)
            {
                systemText.SetDeletedFlag(viewModel.IsSystemText);
            }
            else if (viewModel.IsSystemText)
            {
                systemText = new SystemText();
                systemText.Init();
                systemText.TextId = text.Id;
                context.SystemTexts.Add(systemText);
            }

            context.SaveChanges();
            return text;
        }

        public void DeleteTexts(List<TextDataModel> textDms)
        {
            var context = GetContext();
            foreach (var textVm in textDms)
            {
                var vm = textVm;

                if (vm.TextId == null)
                {
                    continue;
                }

                // check if a brick references this text
                var brickTexts = context.BrickTexts.Where(bt => !bt.Deleted && bt.TextId == vm.TextId).Include(bt => bt.Brick).ToList();
                if (brickTexts.Count > 0)
                {
                    throw new Exception(
                        $"The bricks {string.Join(", ", brickTexts.Select(bt => bt.Brick.Name).ToArray())} still reference this text. Please remove the text from the bricks first.");
                }

                // check if an orga entity references this text
                var orgaEntities = context.OrgaEntities.Where(oe => !oe.Deleted && (oe.HeaderTextId == textVm.TextId)).ToList();
                if (orgaEntities.Count > 0)
                {
                    var en = _languageRepository.GetEnglishLanguage();
                    foreach (var oe in orgaEntities)
                    {
                        LoadTexts(oe, en, null);
                    }

                    throw new Exception(
                        $"The orga entities {string.Join(", ", orgaEntities.Select(oe => oe.Header?.Value ?? oe.Name).ToArray())} still reference this text. Please remove the text from the orga entities first.");
                }

                // check if an address references this text
                var addresses = context.Addresses.Where(a => !a.Deleted && (a.FooterTextId == textVm.TextId || a.LocationTextId == textVm.TextId)).ToList();
                if (addresses.Count > 0)
                {
                    throw new Exception(
                        $"The addresses {string.Join(", ", addresses.Select(a => a.Name).ToArray())} still reference this text. Please remove the text from the addresses first.");
                }

                // check if it is a system text
                if (context.SystemTexts.AsNoTracking().Any(t => !t.Deleted && t.TextId == textVm.TextId))
                {
                    throw new Exception(
                        "The text is a system text which is used somewhere in the code. Please check if the text is still necessary. \n" +
                        $"Please uncheck the system text check box before deleting the text: {textVm.Alias}");
                }

                // delete translations and the text
                foreach (var translation in context.Translations.Where(t => t.TextId == vm.TextId))
                {
                    DeleteUpdatable(context, context.Translations, translation.Id);
                }

                DeleteUpdatable(context, context.Texts, vm.TextId.Value);
            }

            context.SaveChanges();
        }

        public List<string> QueryAliases(string q)
        {
            var context = GetContext();
            return context.Texts.Where(t => !t.Deleted && t.Alias.Contains(q)).Select(t => t.Alias).ToList();
        }

        public Text GetText(Guid id)
        {
            var context = GetContext();
            return context.Texts.FirstOrDefault(t => t.Id == id);
        }

        public void UpdateText(TextEditViewModel text)
        {
            using (var context = GetContext())
            {
                var dbSystemText = context.SystemTexts.FirstOrDefault(x => x.TextId == text.TextId);

                if (dbSystemText == null && text.IsSystemText)
                {
                    dbSystemText = new SystemText();
                    dbSystemText.Init();
                    dbSystemText.TextId = text.TextId;
                    context.SystemTexts.Add(dbSystemText);
                }

                dbSystemText?.SetDeletedFlag(!text.IsSystemText);
                dbSystemText?.UpdateServerModificationTime();
                context.SaveChanges();
            }
        }

        public IEnumerable<Text> GetTexts(IPaginationInfo paginationInfo, string typeId, bool deleted, string searchQuery = null)
        {
            using (var context = GetContext())
            {
                return GetTextsQuery(context, paginationInfo, typeId, deleted, searchQuery)
                       .Paginate(paginationInfo)
                       .ToArray();
            }
        }

        public IQueryable<Text> GetTextsQuery(
            EurolookServerContext context,
            IPaginationInfo paginationInfo,
            string typeId,
            string searchQuery = null)
        {
            return GetTextsQuery(context, typeId, null, searchQuery).OrderBy(paginationInfo);
        }

        public IQueryable<Text> GetTextsQuery(EurolookServerContext context, IPaginationInfo paginationInfo, string typeId, bool deleted, string searchQuery = null)
        {
            return GetTextsQuery(context, typeId, deleted, searchQuery).OrderBy(paginationInfo);
        }

        public IQueryable<Text> GetTextsQuery(EurolookServerContext context, string typeId, bool? deleted, string searchQuery = null)
        {
            if (string.IsNullOrWhiteSpace(typeId))
            {
                throw new ArgumentNullException(nameof(typeId));
            }

            return context.Texts.AsExpandable().Where(GetTextQueryPredicate(context, typeId, deleted, searchQuery));
        }

        public int GetTextCount(string typeId, bool deleted, string searchQuery = null)
        {
            if (string.IsNullOrWhiteSpace(typeId))
            {
                return 0;
            }

            using (var context = GetContext())
            {
                return context.Texts.AsExpandable()
                    .Where(GetTextQueryPredicate(context, typeId, deleted, searchQuery))
                    .Count();
            }
        }

        protected static Expression<Func<Text, bool>> GetTextQueryPredicate(
            EurolookServerContext context,
            string typeId,
            bool? deleted,
            string searchQuery)
        {
            var textFilter = PredicateBuilder.New<Text>(true);
            if (deleted.HasValue)
            {
                textFilter = textFilter.And(t => t.Deleted == deleted.Value);
            }

            if (typeId.Equals(TextTypeFilter.Orga.Id, StringComparison.InvariantCultureIgnoreCase))
            {
                var orgaTextIds = GetOrgaChartTextIds(context);
                textFilter = textFilter.And(t => orgaTextIds.Contains(t.Id));
            }
            else if (typeId.Equals(TextTypeFilter.Functions.Id, StringComparison.InvariantCultureIgnoreCase))
            {
                var functionTextIds = GetFunctionTextIds(context);
                textFilter = textFilter.And(t => functionTextIds.Contains(t.Id));
            }
            else if (typeId.Equals(TextTypeFilter.Addresses.Id, StringComparison.InvariantCultureIgnoreCase))
            {
                var addressTextIds = GetAddressTextIds(context);
                textFilter = textFilter.And(t => addressTextIds.Contains(t.Id));
            }
            else if (typeId.Equals(TextTypeFilter.SystemTexts.Id, StringComparison.InvariantCultureIgnoreCase))
            {
                var systemTextIds = GetSystemTextIds(context);
                textFilter = textFilter.And(t => systemTextIds.Contains(t.Id));
            }
            else if (typeId.Equals(TextTypeFilter.Docs.Id, StringComparison.InvariantCultureIgnoreCase))
            {
                var docModelTextIds = new List<Guid>();

                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (var docModelText in context.BrickTexts.Where(bt => !bt.Deleted))
                {
                    docModelTextIds.Add(docModelText.TextId);
                }

                textFilter = textFilter.And(t => docModelTextIds.Contains(t.Id));
            }
            else if (typeId.Equals(TextTypeFilter.Alias.Id, StringComparison.InvariantCultureIgnoreCase))
            {
                if (searchQuery == null)
                {
                    searchQuery = "";
                }

                if (searchQuery.EndsWith("*"))
                {
                    string query = searchQuery.Substring(0, searchQuery.IndexOf("*", StringComparison.InvariantCultureIgnoreCase));
                    textFilter = textFilter.And(t => t.Alias.StartsWith(query));
                }
                else if (searchQuery.StartsWith("*"))
                {
                    string query = searchQuery.Substring(1);
                    textFilter = textFilter.And(t => t.Alias.EndsWith(query));
                }
                else
                {
                    textFilter = textFilter.And(t => t.Alias == searchQuery);
                }
            }
            else if (typeId.Equals(TextTypeFilter.Unused.Id, StringComparison.InvariantCultureIgnoreCase))
            {
                var activeTextIds = new List<Guid>();
                activeTextIds.AddRange(GetBrickTextIds(context));
                activeTextIds.AddRange(GetOrgaChartTextIds(context));
                activeTextIds.AddRange(GetAddressTextIds(context));
                activeTextIds.AddRange(GetFunctionTextIds(context));
                activeTextIds.AddRange(GetSystemTextIds(context));
                textFilter = textFilter.And(t => !activeTextIds.Contains(t.Id));
            }
            else if (typeId.StartsWith("doc:"))
            {
                // Filter for specific DocumentModel
                if (Guid.TryParse(
                    typeId.Substring(typeId.IndexOf(":", StringComparison.InvariantCultureIgnoreCase) + 1),
                    out var docModelId))
                {
                    var structureBrickIds =
                        context.DocumentStructure.Where(ds => !ds.Deleted && ds.DocumentModelId == docModelId)
                               .Select(ds => ds.BrickId)
                               .ToList();
                    var textIds =
                        context.BrickTexts.Where(bt => !bt.Deleted && structureBrickIds.Contains(bt.BrickId))
                               .Select(bt => bt.TextId)
                               .ToList();
                    textFilter = textFilter.And(t => textIds.Contains(t.Id));
                }
            }

            return textFilter;
        }

        private static Guid[] GetBrickTextIds(EurolookServerContext context)
        {
            return context.BrickTexts
                          .Where(bt => !bt.Deleted)
                          .Select(bt => bt.TextId).ToArray();
        }

        private static Guid[] GetOrgaChartTextIds(EurolookServerContext context)
        {
            return context.OrgaEntities.Where(oe => !oe.Deleted && oe.HeaderTextId != null)
                          .Select(oe => oe.HeaderTextId.Value).ToArray();
        }

        private static Guid[] GetAddressTextIds(EurolookServerContext context)
        {
            var result = new List<Guid>();
            result.AddRange(context.Addresses
                                   .Where(a => !a.Deleted && a.NameTextId != null)
                                   .Select(a => a.NameTextId.Value));
            result.AddRange(context.Addresses
                                   .Where(a => !a.Deleted && a.LocationTextId != null)
                                   .Select(a => a.LocationTextId.Value));
            result.AddRange(context.Addresses
                                   .Where(a => !a.Deleted && a.FooterTextId != null)
                                   .Select(a => a.FooterTextId.Value));
            return result.ToArray();
        }

        private static Guid[] GetFunctionTextIds(EurolookServerContext context)
        {
            var result = new List<Guid>();
            result.AddRange(context.PredefinedFunctions
                                   .Where(x => !x.Deleted && x.FunctionTextId != null)
                                   .Select(a => a.FunctionTextId.Value));
            result.AddRange(context.PredefinedFunctions
                                   .Where(x => !x.Deleted && x.FunctionFemaleTextId != null)
                                   .Select(a => a.FunctionFemaleTextId.Value));
            result.AddRange(context.PredefinedFunctions
                                   .Where(x => !x.Deleted && x.FunctionHeaderTextId != null)
                                   .Select(a => a.FunctionHeaderTextId.Value));
            result.AddRange(context.PredefinedFunctions
                                   .Where(x => !x.Deleted && x.FunctionHeaderFemaleTextId != null)
                                   .Select(a => a.FunctionHeaderFemaleTextId.Value));
            return result.ToArray();
        }

        private static Guid[] GetSystemTextIds(EurolookServerContext context)
        {
            var result = new List<Guid>();
            result.AddRange(context.SystemTexts
                                   .Where(s => !s.Deleted && s.TextId != null)
                                   .Select(a => a.TextId.Value));
            return result.ToArray();
        }

        public List<Translation> GetTranslations(Guid textId, IEnumerable<Language> languages)
        {
            var languageIds = languages.Select(l => l.Id).ToList();
            var context = GetContext();
            return context.Translations.Include(t => t.Language).Include(t => t.Text).
                Where(t => t.TextId == textId && languageIds.Contains(t.LanguageId) && !t.Deleted).ToList();
        }

        public List<Translation> GetTranslations(Guid? textId)
        {
            var context = GetContext();
            return context.Translations.Include(t => t.Language).Include(t => t.Text).
                           Where(t => t.TextId == textId && !t.Deleted).ToList();
        }

        public List<Translation> GetAllTranslations(Guid? textId)
        {
            var context = GetContext();
            return context.Translations.Include(t => t.Language).Include(t => t.Text).
                           Where(t => t.TextId == textId).ToList();
        }

        public int GetTranslationCount(Text text)
        {
            return GetTranslationCount(text, false);
        }

        public int GetTranslationCount(Text text, bool deleted)
        {
            using (var context = GetContext())
            {
                return context.Translations.Count(t => t.Deleted == deleted && t.TextId == text.Id);
            }
        }

        // *********************************************************************
        //                                                            RESOURCES
        // *********************************************************************
        public Resource CreateResource(ResourceDataModel model)
        {
            var context = GetContext();

            var resource = new Resource();
            resource.Init();
            resource.Alias = model.Alias;
            resource.MimeType = model.MimeType;

            if (model.IsSystemResource)
            {
                var systemResource = new SystemResource();
                systemResource.Init();
                systemResource.ResourceId = resource.Id;
                context.SystemResources.Add(systemResource);
            }

            context.Resources.Add(resource);
            context.SaveChanges();
            return resource;
        }

        public void DeleteResource(Guid id)
        {
            using (var context = GetContext())
            {
                var resource = context.Resources.Where(x => x.Id == id).First();
                resource.SetDeletedFlag();

                foreach (var localisedResource in context.LocalisedResources.Where(lr => lr.ResourceId == id))
                {
                    localisedResource.SetDeletedFlag();
                    DeleteUpdatable(context, context.LocalisedResources, localisedResource.Id);
                }

                var systemResource = context.SystemResources.Where(x => x.ResourceId == id).FirstOrDefault();
                if (systemResource != null)
                {
                    systemResource.SetDeletedFlag();
                    DeleteUpdatable(context, context.SystemResources, systemResource.Id);
                }

                DeleteUpdatable(context, context.Resources, id);
                context.SaveChanges();
            }
        }

        public Resource GetResource(Guid id)
        {
            return GetContext().Resources.FirstOrDefault(x => x.Id == id);
        }

        public List<Resource> GetResourcesByBrick(Guid brickId)
        {
            var context = GetContext();

            var resourcedIds = context.BrickResources.Where(br => br.BrickId == brickId && !br.Deleted).Select(br => br.ResourceId);
            var resources = context.Resources
                .Where(r => resourcedIds.Contains(r.Id))
                .ToList();

            return resources;
        }

        public List<Brick> GetBricksForResource(Guid resourceId)
        {
            var context = GetContext();

            var brickIds = context.BrickResources.Where(br => br.ResourceId == resourceId && !br.Deleted).Select(br => br.BrickId);
            var bricks = context.Bricks.Where(b => brickIds.Contains(b.Id)).ToList();

            return bricks;
        }

        public LocalisedResource GetLocalisedResource(Guid resourceId, Guid languageId)
        {
            return GetContext().LocalisedResources
                               .Include(x => x.Resource)
                               .Include(x => x.Language)
                               .FirstOrDefault(lr => lr.LanguageId == languageId && lr.ResourceId == resourceId);
        }

        public void UpdateResource(ResourceEditDataModel resource)
        {
            var context = GetContext();
            var dbResource = context.Resources.FirstOrDefault(x => x.Id == resource.Id);
            if (dbResource == null)
            {
                return;
            }

            dbResource.Alias = resource.Alias;
            dbResource.MimeType = resource.MimeType;
            dbResource.UpdateServerModificationTime();

            var dbSystemResource = context.SystemResources.FirstOrDefault(r => r.ResourceId == resource.Id);
            if (dbSystemResource == null && resource.IsSystemResource)
            {
                dbSystemResource = new SystemResource();
                dbSystemResource.Init();
                dbSystemResource.ResourceId = resource.Id;
                context.SystemResources.Add(dbSystemResource);
            }

            dbSystemResource?.SetDeletedFlag(!resource.IsSystemResource);
            dbSystemResource?.UpdateServerModificationTime();

            context.SaveChanges();
        }

        public async Task UpdateResourceRawData(Guid resourceId, IFormFile file, string fileName)
        {
            var context = GetContext();
            var dbResource = context.Resources.FirstOrDefault(x => x.Id == resourceId);

            if (dbResource == null)
            {
                return;
            }

            if (file == null)
            {
                dbResource.RawData = null;
                dbResource.MimeType = "";
                dbResource.FileName = "";
            }
            else
            {
                var fileBytes = await file.GetBytes();
                dbResource.RawData = fileBytes;
                dbResource.MimeType = file.ContentType;
                dbResource.FileName = fileName;
            }

            dbResource.UpdateServerModificationTime();
            context.SaveChanges();
        }

        public async Task UpdateLocalisedResource(Guid resourceId, Guid languageId, IFormFile file, string fileNameDate)
        {
            using (var context = GetContext())
            {
                var lr = context.LocalisedResources.FirstOrDefault(
                    x => x.ResourceId == resourceId && x.LanguageId == languageId);

                if (lr == null)
                {
                    // it is totally new, create it
                    lr = new LocalisedResource();
                    lr.Init();
                    lr.ResourceId = resourceId;
                    lr.LanguageId = languageId;
                    lr.FileNameDate = "";
                    context.LocalisedResources.Add(lr);
                }

                if (file == null)
                {
                    // the file is null => file has been deleted.
                    lr.RawData = null;
                    lr.FileNameDate = "";
                    lr.SetDeletedFlag();
                }
                else
                {
                    var fileBytes = await file.GetBytes();
                    lr.RawData = fileBytes;
                    lr.MimeType = file.ContentType;
                    lr.FileNameDate = fileNameDate;
                    lr.SetDeletedFlag(false);
                }

                lr.UpdateServerModificationTime();
                context.SaveChanges();
            }
        }

        public Resource GetResourceForEdit(Guid id)
        {
            return GetContext().Resources
                               .Include(lr => lr.Localisations)
                               .FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Resource> GetResources(DataPage<Resource> page, string searchTerm, bool deleted)
        {
            using (var context = GetContext())
            {
                return context.Resources
                              .Where(GetResourceQueryPredicate(searchTerm, deleted))
                              .OrderByAndPaginate(page)
                              .ToArray();
            }
        }

        public int GetResourcesCount(string searchTerm, bool deleted)
        {
            using (var context = GetContext())
            {
                return context.Resources.Count(GetResourceQueryPredicate(searchTerm, deleted));
            }
        }

        private static Expression<Func<Resource, bool>> GetResourceQueryPredicate(string searchTerm, bool deleted)
        {
            Expression<Func<Resource, bool>> predicate;
            if (string.IsNullOrWhiteSpace(searchTerm) || searchTerm == "*")
            {
                predicate = x => x.Deleted == deleted;
            }
            else
            {
                predicate = x => x.Deleted == deleted && x.Alias.Contains(searchTerm);
            }

            return predicate;
        }

        // *********************************************************************
        //                                                               HELPER
        // *********************************************************************
        protected void DeleteUpdatable<T>(EurolookContext context, DbSet<T> table, Guid id, bool isDeleted = true)
            where T : Updatable
        {
            this.LogDebugFormat("Delete<{0}>(Id={1})", typeof(T).Name, id);
            var dbEntity = SelectFromLocalOrDb(table, id);
            DeleteUpdatable(dbEntity, isDeleted);
        }

        protected void DeleteUpdatable<T>(T entity, bool isDeleted = true)
            where T : Updatable
        {
            entity?.SetDeletedFlag(isDeleted);
        }

        // *********************************************************************
        //                                                        USER FEEDBACK
        // *********************************************************************
        public UserFeedback[] GetUserFeedback(DateTime time, DataPage<UserFeedback> page)
        {
            using (var context = GetContext())
            {
                var query = context.UserFeedbacks.Include(u => u.User).Where(f => f.Submitted > time);
                if (page.OrderProperty.Equals("submitted", StringComparison.InvariantCultureIgnoreCase))
                {
                    query = page.OrderAscending
                        ? query.OrderBy(uf => uf.Submitted)
                        : query.OrderByDescending(uf => uf.Submitted);
                }
                else if (page.OrderProperty.Equals("user", StringComparison.InvariantCultureIgnoreCase))
                {
                    query = page.OrderAscending
                        ? query.OrderBy(uf => uf.User.Login)
                        : query.OrderByDescending(uf => uf.User.Login);
                }
                else if (page.OrderProperty.Equals("rating", StringComparison.InvariantCultureIgnoreCase))
                {
                    query = page.OrderAscending
                        ? query.OrderBy(uf => uf.Rating)
                        : query.OrderByDescending(uf => uf.Rating);
                }
                else if (page.OrderProperty.Equals("message", StringComparison.InvariantCultureIgnoreCase))
                {
                    query = page.OrderAscending
                        ? query.OrderBy(uf => uf.Message)
                        : query.OrderByDescending(uf => uf.Message);
                }

                return query.Paginate(page)
                            .ToArray();
            }
        }

        public IEnumerable<FeedbackInfo> GetUserFeedbackDownload(DateTime time)
        {
            using (var context = GetContext())
            {
                var feedbackinfo = (from f in context.UserFeedbacks
                                    where f.Submitted >= time
                                    orderby f.Submitted descending
                                    select new FeedbackInfo
                                    {
                                        Message = f.Message,
                                        Rating = f.Rating.ToString(),
                                        Submitted = f.Submitted,
                                        User = f.User.Login,
                                    }).ToList();
                return feedbackinfo;
            }
        }

        public int GetUserFeedbackCount(DateTime time)
        {
            using (var context = GetContext())
            {
                return context.UserFeedbacks.Count(f => f.Submitted > time);
            }
        }

        public void DeleteFeedback(List<UserFeedback> userFeedback)
        {
            var context = GetContext();
            context.UserFeedbacks.RemoveRange(userFeedback);
            context.SaveChanges();
        }

        // *********************************************************************
        //                                                               COMMON
        // *********************************************************************
        public List<OrgaEntity> GetAllDgs()
        {
            List<OrgaEntity> result;
            var en = _languageRepository.GetEnglishLanguage();
            using (var context = GetContext())
            {
                result = context.OrgaEntities.Where(o => o.LogicalLevel == 1 && !o.Deleted).ToList();
            }

            foreach (var dg in result)
            {
                LoadTexts(dg, en, null);
            }

            return result.OrderBy(o => o.Name).ToList();
        }

        public List<OrgaEntity> GetAllOrgaEntity()
        {
            List<OrgaEntity> result;
            var en = _languageRepository.GetEnglishLanguage();
            using (var context = GetContext())
            {
                result = context.OrgaEntities.ToList();
            }

            foreach (var orgaEntity in result)
            {
                LoadTexts(orgaEntity, en, null);
            }

            return result.OrderBy(o => o.OrderIndex).ThenBy(o => o.ToString()).ToList();
        }

        public DocumentCategory[] GetAllDocumentCategories()
        {
            using (var context = GetContext())
            {
                return context.DocumentCategories.Where(x => !x.Deleted).ToArray();
            }
        }

        public async Task UpdateDocumentModelLanguageTemplate(Guid documentModelId, Guid languageId, IFormFile file)
        {
            using (var context = GetContext())
            {
                var dml = context.DocumentModelLanguages.FirstOrDefault(
                    x => x.DocumentModelId == documentModelId && x.DocumentLanguageId == languageId);

                if (dml == null)
                {
                    // it is totally new, create it
                    dml = new DocumentModelLanguage();
                    dml.Init();
                    dml.DocumentModelId = documentModelId;
                    dml.DocumentLanguageId = languageId;
                    context.DocumentModelLanguages.Add(dml);
                }

                if (file == null)
                {
                    // the file is null => file has been deleted.
                    dml.Template = null;
                    dml.TemplateFileName = null;
                    dml.SetDeletedFlag(true);
                }
                else
                {
                    var fileBytes = await file.GetBytes();
                    dml.Template = fileBytes;
                    dml.TemplateFileName = file.FileName;
                    dml.SetDeletedFlag(false);
                }

                context.SaveChanges();
            }
        }

        public List<PredefinedFunction> GetPredefinedFunctions()
        {
            using (var context = GetContext())
            {
                return context.PredefinedFunctions.Where(x => !x.Deleted).ToList();
            }
        }

        public List<SystemText> GetSystemTexts()
        {
            using (var context = GetContext())
            {
                return context.SystemTexts.AsNoTracking().Where(x => !x.Deleted).ToList();
            }
        }

        public List<SystemResource> GetSystemResources()
        {
            {
                using (var context = GetContext())
                {
                    return context.SystemResources.AsNoTracking().Where(x => !x.Deleted).ToList();
                }
            }
        }
    }
}
