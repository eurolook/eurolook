﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.Extensions;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Eurolook.ManagementConsole.Database
{
    public class PredefinedFunctionsDatabase : EurolookManagementConsoleDatabase, IPredefinedFunctionsDatabase
    {
        public PredefinedFunctionsDatabase(IConfiguration configuration)
            : base(configuration)
        {
        }

        public IEnumerable<PredefinedFunction> GetPredefinedFunctions(
            DataPage<PredefinedFunction> page,
            string searchTerm,
            bool deleted)
        {
            using (var context = GetContext())
            {
                var query = context.PredefinedFunctions.Where(GetPredefinedFunctionQueryPredicate(searchTerm, deleted));

                return query.OrderByAndPaginate(page)
                            .ToArray();
            }
        }

        public int GetPredefinedFunctionCount(string searchTerm, bool deleted)
        {
            using (var context = GetContext())
            {
                return context.PredefinedFunctions.Count(GetPredefinedFunctionQueryPredicate(searchTerm, deleted));
            }
        }

        public async Task<PredefinedFunctionEditDataModel> GetPredefinedFunctionForEdit(Guid id)
        {
            using (var context = GetContext())
            {
                var predefinedFunction = await context.PredefinedFunctions
                                                      .Include(x => x.FunctionText)
                                                      .Include(x => x.FunctionFemaleText)
                                                      .Include(x => x.FunctionHeaderText)
                                                      .Include(x => x.FunctionHeaderFemaleText)
                                                      .FirstOrDefaultAsync(x => x.Id == id);
                if (predefinedFunction != null)
                {
                    return new PredefinedFunctionEditDataModel
                    {
                        Id = id,
                        Name = predefinedFunction.Name,
                        ShowInSignature = predefinedFunction.ShowInSignature,
                        ShowInHeader = predefinedFunction.ShowInHeader,
                        IsSuperior = predefinedFunction.IsSuperior,
                        ActiveDirectoryReference = predefinedFunction.AdReference,
                        TextAlias = predefinedFunction.FunctionText?.Alias,
                        TextFemaleAlias = predefinedFunction.FunctionFemaleText?.Alias,
                        TextHeaderAlias = predefinedFunction.FunctionHeaderText?.Alias,
                        TextHeaderFemaleAlias = predefinedFunction.FunctionHeaderFemaleText?.Alias,
                    };
                }

                return null;
            }
        }

        public async Task UpdatePredefinedFunction(Guid id, PredefinedFunctionEditDataModel model)
        {
            using (var context = GetContext())
            {
                var predefinedFunction = await context.PredefinedFunctions.FirstOrDefaultAsync(x => x.Id == id);
                if (predefinedFunction != null)
                {
                    predefinedFunction.Name = model.Name;
                    predefinedFunction.ShowInSignature = model.ShowInSignature;
                    predefinedFunction.ShowInHeader = model.ShowInHeader;
                    predefinedFunction.IsSuperior = model.IsSuperior;
                    predefinedFunction.AdReference = model.ActiveDirectoryReference;
                    predefinedFunction.UpdateServerModificationTime();
                    await context.SaveChangesAsync();
                }
            }
        }

        public async Task DeletePredefinedFunction(Guid id, bool deleted)
        {
            using (var context = GetContext())
            {
                var predefinedFunction = await context.PredefinedFunctions.FirstOrDefaultAsync(x => x.Id == id);
                if (predefinedFunction != null)
                {
                    predefinedFunction.SetDeletedFlag(deleted);
                    if (predefinedFunction.FunctionTextId != null)
                    {
                        await DeleteFunctionText(context, predefinedFunction.FunctionTextId.Value, deleted);
                    }

                    if (predefinedFunction.FunctionFemaleTextId != null)
                    {
                        await DeleteFunctionText(context, predefinedFunction.FunctionFemaleTextId.Value, deleted);
                    }

                    if (predefinedFunction.FunctionHeaderTextId != null)
                    {
                        await DeleteFunctionText(context, predefinedFunction.FunctionHeaderTextId.Value, deleted);
                    }

                    if (predefinedFunction.FunctionHeaderFemaleTextId != null)
                    {
                        await DeleteFunctionText(context, predefinedFunction.FunctionHeaderFemaleTextId.Value, deleted);
                    }

                    await context.SaveChangesAsync();
                }
            }
        }

        public async Task<string> CreateFemaleFunction(Guid id)
        {
            using (var context = GetContext())
            {
                var predefinedFunction = await context.PredefinedFunctions.FirstOrDefaultAsync(x => x.Id == id);
                if (predefinedFunction != null)
                {
                    predefinedFunction.FunctionFemaleText = new Text();
                    predefinedFunction.FunctionFemaleText.Init();
                    predefinedFunction.FunctionFemaleText.Alias = $"Function_{GetSmallUniqueId()}";
                    predefinedFunction.UpdateServerModificationTime();
                    await context.SaveChangesAsync();

                    return predefinedFunction.FunctionFemaleText.Alias;
                }

                return null;
            }
        }

        public async Task<PredefinedFunction> CreatePredefinedFunction(PredefinedFunctionDataModel dataModel)
        {
            using (var context = GetContext())
            {
                var predefinedFunction = new PredefinedFunction();
                predefinedFunction.Init();
                predefinedFunction.Name = dataModel.Name;
                predefinedFunction.ShowInSignature = true;

                predefinedFunction.FunctionText = new Text();
                predefinedFunction.FunctionText.Init();
                predefinedFunction.FunctionText.Alias = $"Function_{GetSmallUniqueId()}";

                var entityEntry = context.PredefinedFunctions.Add(predefinedFunction);
                await context.SaveChangesAsync();
                return entityEntry.Entity;
            }
        }

        public async Task<string> CreateHeaderFunction(Guid id)
        {
            using (var context = GetContext())
            {
                var predefinedFunction = await context.PredefinedFunctions.FirstOrDefaultAsync(x => x.Id == id);
                if (predefinedFunction != null)
                {
                    predefinedFunction.FunctionHeaderText = new Text();
                    predefinedFunction.FunctionHeaderText.Init();
                    predefinedFunction.FunctionHeaderText.Alias = $"Function_{GetSmallUniqueId()}";
                    predefinedFunction.UpdateServerModificationTime();
                    await context.SaveChangesAsync();

                    return predefinedFunction.FunctionHeaderText.Alias;
                }

                return null;
            }
        }

        public async Task<string> CreateFemaleHeaderFunction(Guid id)
        {
            using (var context = GetContext())
            {
                var predefinedFunction = await context.PredefinedFunctions.FirstOrDefaultAsync(x => x.Id == id);
                if (predefinedFunction != null)
                {
                    predefinedFunction.FunctionHeaderFemaleText = new Text();
                    predefinedFunction.FunctionHeaderFemaleText.Init();
                    predefinedFunction.FunctionHeaderFemaleText.Alias = $"Function_{GetSmallUniqueId()}";
                    predefinedFunction.UpdateServerModificationTime();
                    await context.SaveChangesAsync();

                    return predefinedFunction.FunctionHeaderFemaleText.Alias;
                }

                return null;
            }
        }

        private static Expression<Func<PredefinedFunction, bool>> GetPredefinedFunctionQueryPredicate(
            string query,
            bool deleted)
        {
            Expression<Func<PredefinedFunction, bool>> predicate;
            if (string.IsNullOrWhiteSpace(query) || query == "*")
            {
                predicate = x => x.Deleted == deleted;
            }
            else
            {
                predicate = x => x.Deleted == deleted && x.Name.Contains(query);
            }

            return predicate;
        }

        private static async Task DeleteFunctionText(EurolookServerContext context, Guid textId, bool deleted)
        {
            var text = await context.Texts.FirstOrDefaultAsync(x => x.Id == textId);
            text?.SetDeletedFlag(deleted);
            foreach (var translation in await context.Translations.Where(x => x.TextId == textId).ToArrayAsync())
            {
                translation.SetDeletedFlag(deleted);
            }
        }

        private static string GetSmallUniqueId()
        {
            return Regex.Replace(Convert.ToBase64String(Guid.NewGuid().ToByteArray()), "[/+=]", "");
        }
    }
}
