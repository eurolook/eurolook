﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Data;
using Eurolook.Data.Constants;
using Eurolook.Data.Database;
using Eurolook.Data.Extensions;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Queries;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Eurolook.ManagementConsole.Database
{
    public class UsersDatabase : EurolookManagementConsoleDatabase, IUsersDatabase
    {
        private const string OrderPropLogin = "Login";
        private const string OrderPropName = "Name";
        private const string OrderPropCreated = "Created";
        private const string OrderPropEmail = "Email";
        private const string OrderPropOrgAcronym = "OrgAcronym";
        private const string OrderPropUserGroups = "UserGroup";

        public UsersDatabase(IConfiguration configuration)
            : base(configuration)
        {
        }

        public UserDataModel[] GetUsers(
            DataPage<UserDataModel> page,
            IEnumerable<UserGroup> userGroups,
            string searchQuery,
            bool deleted)
        {
            using (var context = GetContext())
            {
                var predicate = new UserSearchQuery(deleted).GetPredicate(searchQuery, userGroups);
                var query = context.Users
                                   .Include(u => u.Self)
                                   .Include(u => u.Settings).ThenInclude(x => x.DeviceSettings)
                                   .Where(predicate);
                string orderProperty = page.OrderProperty;
                bool orderAscending = page.OrderAscending;

                if (page.OrderProperty.Equals(OrderPropLogin, StringComparison.InvariantCultureIgnoreCase))
                {
                    query = orderAscending
                        ? query.OrderBy(u => u.Login)
                        : query.OrderByDescending(u => u.Login);
                }
                else if (orderProperty.Equals(OrderPropCreated, StringComparison.InvariantCultureIgnoreCase))
                {
                    query = orderAscending
                        ? query.OrderBy(u => u.CreatedTimeUtc)
                        : query.OrderByDescending(u => u.CreatedTimeUtc);
                }
                else if (orderProperty.Equals(OrderPropName, StringComparison.InvariantCultureIgnoreCase))
                {
                    query = orderAscending
                        ? query.OrderBy(u => u.Self.LatinLastName).ThenBy(u => u.Self.LatinFirstName)
                        : query.OrderByDescending(u => u.Self.LatinLastName)
                               .ThenByDescending(u => u.Self.LatinFirstName);
                }
                else if (orderProperty.Equals(OrderPropOrgAcronym, StringComparison.InvariantCultureIgnoreCase))
                {
                    query = orderAscending
                        ? query.OrderBy(u => u.Self.Service)
                        : query.OrderByDescending(u => u.Self.Service);
                }
                else if (orderProperty.Equals(OrderPropEmail, StringComparison.InvariantCultureIgnoreCase))
                {
                    query = orderAscending
                        ? query.OrderBy(u => u.Self.Email)
                        : query.OrderByDescending(u => u.Self.Email);
                }
                else if (orderProperty.Equals(OrderPropUserGroups, StringComparison.InvariantCultureIgnoreCase))
                {
                    query = orderAscending
                        ? query.OrderBy(u => u.UserGroupIds)
                        : query.OrderByDescending(u => u.UserGroupIds);
                }

                var result = query.Paginate(page)
                                  .ToArray();

                foreach (var user in result)
                {
                    user.Settings.DeviceSettings.RemoveAll(d => d.Deleted);
                }

                return result
                       .ToArray()
                       .Select(u => new UserDataModel(u, userGroups))
                       .ToArray();
            }
        }

        public User GetUserForEdit(Guid id)
        {
            using (var context = GetContext())
            {
                return GetUserForEdit(context, id);
            }
        }

        public int GetUserCount(IEnumerable<UserGroup> userGroups, string searchQuery, bool deleted)
        {
            using (var context = GetContext())
            {
                var predicate = new UserSearchQuery(deleted).GetPredicate(searchQuery, userGroups);
                return context.Users.Count(predicate);
            }
        }

        public User GetUser(string userName)
        {
            using (var context = GetContext())
            {
                return context.Users.First(u => u.Login == userName);
            }
        }

        public Guid GetUserIdByName(string userName)
        {
            using (var context = GetContext())
            {
                // ReSharper disable once SpecifyStringComparison
                return context.Users.FirstOrDefault(u => u.Login.ToLower() == userName.ToLower())?.Id ?? Guid.Empty;
            }
        }

        public void UpdateUserProfile(Guid id, UserEditDataModel model)
        {
            using (var context = GetContext())
            {
                var dbUser = GetUserForUpdate(context, id);
                var now = DateTime.UtcNow;

                // User
                dbUser.Login = model.Login;
                var idsOfSelectedUserGroups = model.UserGroups
                                                   .Where(ug => ug.IsSelected)
                                                   .Select(ug => ug.Id)
                                                   .ToArray();
                dbUser.UserGroupIds = idsOfSelectedUserGroups.Any()
                    ? string.Join(";", idsOfSelectedUserGroups).ToUpperInvariant()
                    : CommonDataConstants.DefaultUserGroupId.ToString();

                dbUser.UpdateServerModificationTime(now);

                // Settings
                dbUser.Settings.DataSyncIntervalMinutes = model.DataSyncInterval;
                dbUser.Settings.DataSyncDelayMinutes = model.DataSyncDelay;
                dbUser.Settings.IsCepEnabled = model.IsCepEnabled;
                dbUser.Settings.UpdateServerModificationTime(now);
                dbUser.Settings.IsInsertNameInline = model.IsInsertNameInline;

                context.SaveChanges();
            }
        }

        public void RequestRemoteWipe(Guid id)
        {
            using (var context = GetContext())
            {
                var settingsId = context.Users.Where(u => u.Id == id).Select(u => u.SettingsId).First();
                foreach (var deviceSetting in context.DeviceSettings.Where(d => d.UserSettingsId == settingsId))
                {
                    deviceSetting.IsRemoteWipeRequested = true;
                }

                context.SaveChanges();
            }
        }

        private User GetUserForEdit(EurolookServerContext context, Guid id)
        {
            var result = GetUserForUpdate(context, id);
            result?.Settings.DeviceSettings.RemoveAll(d => d.Deleted);

            return result;
        }

        private User GetUserForUpdate(EurolookServerContext context, Guid id)
        {
            return context.Users.Include(u => u.Settings)
                          .Include(u => u.Settings).ThenInclude(x => x.DeviceSettings)
                          .FirstOrDefault(u => u.Id == id);
        }
    }
}
