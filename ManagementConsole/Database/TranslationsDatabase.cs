using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using Eurolook.Data;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.ViewModels.Texts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Eurolook.ManagementConsole.Database
{
    public class TranslationsDatabase : AdminDatabase
    {
        private readonly ILanguageRepository _languageRepository;

        public TranslationsDatabase(IConfiguration configuration, ILanguageRepository languageRepository)
            : base(configuration)
        {
            _languageRepository = languageRepository;
        }

        public int GetTranslationViewModelsCount(string typeId, string lang, string searchQuery)
        {
            using var context = GetContext();
            var languages = lang == "*"
                ? _languageRepository.GetAllLanguages()
                : new List<Language> { _languageRepository.GetLanguage(lang) };
            var languageIds = languages.Select(l => l.Id).ToList();

            int textsCount;
            if (typeId.Equals(TextTypeFilter.Translation.Id, StringComparison.InvariantCultureIgnoreCase))
            {
                var translationsQuery = context.Translations.Include(t => t.Text).Where(t =>
                    !t.Deleted && t.Value.Contains(searchQuery)
                               && languageIds.Contains(t.LanguageId));
                textsCount = translationsQuery.Select(t => t.TextId).Distinct().Count();
            }
            else
            {
                textsCount = GetTextsQuery(context, typeId, false, searchQuery).Count();
            }

            if (lang == "*")
            {
                return textsCount * context.Languages.Count();
            }

            return textsCount;
        }

        public List<TranslationDataModel> GetTranslationViewModels(IPaginationInfo page, string typeId, string lang, string searchQuery, bool deleted)
        {
            using var context = GetContext();
            var languages = lang == "*"
                ? _languageRepository.GetAllLanguages()
                : new List<Language> { _languageRepository.GetLanguage(lang) };
            var languageIds = languages.Select(l => l.Id).ToList();

            if (typeId.Equals(TextTypeFilter.Translation.Id, StringComparison.InvariantCultureIgnoreCase))
            {
                var translationsQuery = context.Translations
                                               .Include(t => t.Text)
                                               .Include(t => t.Language)
                                               .Where(t =>
                                                          t.Deleted == deleted &&
                                                          t.Value.Contains(searchQuery) &&
                                                          languageIds.Contains(t.LanguageId));
                translationsQuery = page.OrderAscending ?
                    translationsQuery.OrderBy(t => t.Text.Alias) :
                    translationsQuery.OrderByDescending(t => t.Text.Alias);

                var translations = translationsQuery.ToArray();
                return GetTranslationViewModels(translations);
            }
            else
            {
                var texts = GetTextsQuery(context, page, typeId, searchQuery).ToArray();
                var textIds = texts.Select(t => t.Id).ToArray();

                var translationsQuery = context.Translations
                                          .Include(t => t.Language).Include(t => t.Text)
                                          .Where(t => t.Deleted == deleted && languageIds.Contains(t.LanguageId) && textIds.Contains(t.TextId));
                if (deleted)
                {
                    translationsQuery = page.OrderAscending ?
                        translationsQuery.OrderBy(t => t.Text.Alias).ThenBy(t => t.Language.Name) :
                        translationsQuery.OrderByDescending(t => t.Text.Alias).ThenBy(t => t.Language.Name);

                    var translations = translationsQuery.ToArray();
                    return GetTranslationViewModels(translations);
                }
                else
                {
                    var translations = translationsQuery.AsEnumerable()
                                                        .GroupBy(t => t.TextId)
                                                        .ToDictionary(g => g.Key, g => g.ToArray());

                    return GetTranslationViewModels(languages, texts, translations);
                }
            }
        }

        private static List<TranslationDataModel> GetTranslationViewModels(Translation[] translations)
        {
            return translations.Select(
                                   translation => new TranslationDataModel
                                   {
                                       TextId = translation.Text.Id,
                                       TextAlias = translation.Text.Alias,
                                       LanguageId = translation.Language.Id,
                                       LanguageName = translation.Language.Name,
                                       IsSelected = false,
                                       TranslationId = translation.Id,
                                       Value = translation.Value,
                                       OriginalValue = translation.Value,
                                   })
                               .ToList();
        }

        private static List<TranslationDataModel> GetTranslationViewModels(IEnumerable<Language> languages, IEnumerable<Text> texts, IReadOnlyDictionary<Guid, Translation[]> translationByText)
        {
            var result = new List<TranslationDataModel>();

            var languagesList = languages.OrderBy(l => l.Name).ToList();
            foreach (var text in texts)
            {
                if (!translationByText.TryGetValue(text.Id, out var translations))
                {
                    translations = new Translation[] { };
                }

                foreach (var language in languagesList)
                {
                    var translationDm = new TranslationDataModel
                    {
                        TextId = text.Id,
                        TextAlias = text.Alias,
                        LanguageId = language.Id,
                        LanguageName = language.Name,
                        IsSelected = false,
                    };
                    var translation = translations.FirstOrDefault(t => t.TextId == text.Id && t.LanguageId == language.Id);
                    if (translation != null)
                    {
                        translationDm.TranslationId = translation.Id;
                        translationDm.Value = translation.Value;
                        translationDm.OriginalValue = translation.Value;
                    }

                    if (!text.Deleted || translation != null)
                    {
                        result.Add(translationDm);
                    }
                }
            }

            return result;
        }

        public void UpdateTranslations(List<TranslationDataModel> translationVms)
        {
            var context = GetContext();
            var modificationTime = DateTime.UtcNow;
            foreach (var translationVm in translationVms)
            {
                if (translationVm.IsSelected && translationVm.OriginalValue != translationVm.Value)
                {
                    if (translationVm.TranslationId != null)
                    {
                        // update
                        var dbTranslation = context.Translations.FirstOrDefault(t => t.Id == translationVm.TranslationId.Value);
                        if (dbTranslation != null)
                        {
                            dbTranslation.Value = translationVm.Value;
                            dbTranslation.UpdateServerModificationTime(modificationTime);
                        }
                    }
                    else
                    {
                        var dbTranslation = context.Translations.FirstOrDefault(t => t.TextId == translationVm.TextId && t.LanguageId == translationVm.LanguageId);
                        if (dbTranslation != null)
                        {
                            // recycle
                            dbTranslation.Value = translationVm.Value;
                            DeleteUpdatable(dbTranslation, false);
                        }
                        else
                        {
                            // create
                            var translation = new Translation
                            {
                                TextId = translationVm.TextId,
                                LanguageId = translationVm.LanguageId,
                                Value = translationVm.Value,
                            };
                            translation.Init();
                            context.Translations.Add(translation);
                        }
                    }
                }
            }

            context.SaveChanges();
        }

        public void DeleteTranslations(List<TranslationDataModel> translationDms)
        {
            var context = GetContext();
            foreach (var t in translationDms)
            {
                if (!t.IsSelected || t.TranslationId == null)
                {
                    continue;
                }

                DeleteUpdatable(context, context.Translations, t.TranslationId.Value);
            }

            context.SaveChanges();
        }
    }
}
