using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.ViewModels.OrgaChart;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Eurolook.ManagementConsole.Database
{
    public class AuthorsDatabase : EurolookManagementConsoleDatabase
    {
        private readonly ILanguageRepository _languageRepository;

        public AuthorsDatabase(IConfiguration configuration, ILanguageRepository languageRepository)
            : base(configuration)
        {
            _languageRepository = languageRepository;
        }

        public Author GetAuthorForEdit(Guid id)
        {
            using (var context = GetContext())
            {
                return GetAuthorForEdit(id, context);
            }
        }

        private Author GetAuthorForEdit(Guid id, EurolookServerContext context)
        {
            var author = context.Authors.
                Include(a => a.OrgaEntity).
                Include(a => a.Functions).
                Include(a => a.Workplaces).ThenInclude(wp => wp.Address).
                Include(a => a.JobAssignments).ThenInclude(j => j.OrgaEntity).
                Include(a => a.JobAssignments).ThenInclude(j => j.Functions).
                FirstOrDefault(a => a.Id == id);
            if (author != null)
            {
                author.Workplaces.RemoveWhere(wp => wp.Deleted);
                author.JobAssignments.RemoveWhere(ja => ja.Deleted);
                author.Functions.RemoveWhere(f => f.Deleted);
            }

            return author;
        }

        internal void UpdateAuthor(Guid id, AuthorEditDataModel model)
        {
            using (var context = GetContext())
            {
                var dbAuthor = context.Authors.First(a => a.Id == id);

                // personal data
                dbAuthor.LatinFirstName = model.LatinFirstName;
                dbAuthor.LatinLastName = model.LatinLastName;
                dbAuthor.GreekFirstName = model.GreekFirstName;
                dbAuthor.GreekLastName = model.GreekLastName;
                dbAuthor.BulgarianFirstName = model.CyrillicFirstName;
                dbAuthor.BulgarianLastName = model.CyrillicLastName;
                dbAuthor.Gender = model.Gender;
                dbAuthor.Initials = model.Initials;
                dbAuthor.Email = model.Email;

                // job assignments
                var mainJobVm = model.JobAssignments.First(j => j.IsMainJob);
                UpdateMainJobAssignment(context, dbAuthor, mainJobVm);
                foreach (var jobVm in model.JobAssignments.Where(j => !j.IsMainJob))
                {
                    UpdateJobAssignment(context, dbAuthor.Id, jobVm);
                }

                // workplaces
                foreach (var workplaceVm in model.Workplaces)
                {
                    if (workplaceVm.IsPrimaryWorkplace)
                    {
                        dbAuthor.MainWorkplaceId = workplaceVm.Id;
                    }

                    var dbWp = context.Workplaces.FirstOrDefault(wp => wp.Id == workplaceVm.Id);
                    if (dbWp != null)
                    {
                        dbWp.AddressId = workplaceVm.AddressId;
                        dbWp.Office = workplaceVm.Office;
                        dbWp.PhoneExtension = workplaceVm.Phone;
                        dbWp.FaxExtension = workplaceVm.Fax;
                        dbWp.SetModification(ModificationType.ServerModification);
                    }
                }

                // linked accounts
#pragma warning disable CS0618 // Type or member is obsolete

                // Kept for backwards compatibility to be able to view/edit the fields for older clients
                dbAuthor.SharePointAccountId = model.SharePointAccountId;
                dbAuthor.SharePointLoginName = model.SharePointLoginName;
#pragma warning restore CS0618 // Type or member is obsolete

                context.SaveChanges();
            }
        }

        private static void UpdateJobAssignmentProperties(IJobAssignment jobAssignment, JobAssignmentDataModel jobAssignmentDm)
        {
            jobAssignment.Service = jobAssignmentDm.Service;
            jobAssignment.WebAddress = jobAssignmentDm.WebAddress;
            jobAssignment.FunctionalMailbox = jobAssignmentDm.FunctionalMailbox;
            jobAssignment.OrgaEntityId = jobAssignmentDm.OrgaEntityId;
            jobAssignment.PredefinedFunctionId = jobAssignmentDm.PredefinedFunctionId;
        }

        private static void UpdateMainJobAssignment(EurolookServerContext context, Author dbAuthor, JobAssignmentDataModel mainJobVm)
        {
            UpdateJobAssignmentProperties(dbAuthor, mainJobVm);
            foreach (var functionVm in mainJobVm.Functions)
            {
                if (functionVm.Id == null)
                {
                    continue;
                }

                var dbFunction = context.JobFunctions.FirstOrDefault(af => af.Id == functionVm.Id.Value);
                if (dbFunction != null && dbFunction.Function != functionVm.Text)
                {
                    // update the function text
                    dbFunction.Function = functionVm.Text;
                    dbFunction.SetModification(ModificationType.ServerModification);
                }
                else if (dbFunction == null && functionVm.LanguageId != Guid.Empty && !string.IsNullOrWhiteSpace(functionVm.Text))
                {
                    // save the new function
                    dbFunction = new JobFunction
                    {
                        Id = functionVm.Id.Value,
                        LanguageId = functionVm.LanguageId,
                        Function = functionVm.Text,
                        AuthorId = dbAuthor.Id,
                    };
                    dbFunction.SetModification(ModificationType.ServerModification);
                    context.JobFunctions.Add(dbFunction);
                }
            }

            dbAuthor.SetModification(ModificationType.ServerModification);
        }

        private static void UpdateJobAssignment(EurolookServerContext context, Guid authorId, JobAssignmentDataModel jobVm)
        {
            if (jobVm.IsMainJob)
            {
                return;
            }

            var now = DateTime.UtcNow;
            JobAssignment dbJob;
            if (jobVm.IsNew)
            {
                var newJob = new JobAssignment();
                newJob.Init();
                if (jobVm.Id != Guid.Empty)
                {
                    newJob.Id = jobVm.Id;
                }

                newJob.AuthorId = authorId;
                dbJob = context.JobAssignments.Add(newJob).Entity;
            }
            else
            {
                dbJob = context.JobAssignments.First(j => j.Id == jobVm.Id);
            }

            if (dbJob == null)
            {
                return;
            }

            if (jobVm.IsDeleted)
            {
                dbJob.SetDeletedFlag(now);
                foreach (var dbFunction in context.JobFunctions.Where(f => f.JobAssignmentId == dbJob.Id))
                {
                    dbFunction.SetDeletedFlag(now);
                }
            }
            else
            {
                UpdateJobAssignmentProperties(dbJob, jobVm);
                dbJob.UpdateServerModificationTime(now);
                foreach (var functionVm in jobVm.Functions)
                {
                    if (functionVm.Id == null)
                    {
                        continue;
                    }

                    var dbFunction = context.JobFunctions.FirstOrDefault(af => af.Id == functionVm.Id.Value);
                    if (dbFunction != null && dbFunction.Function != functionVm.Text)
                    {
                        // update the function text
                        dbFunction.Function = functionVm.Text;
                        dbFunction.UpdateServerModificationTime(now);
                    }
                    else if (dbFunction == null && functionVm.LanguageId != Guid.Empty && !string.IsNullOrWhiteSpace(functionVm.Text))
                    {
                        // save the new function
                        dbFunction = new JobFunction
                        {
                            Id = functionVm.Id.Value,
                            LanguageId = functionVm.LanguageId,
                            Function = functionVm.Text,
                            JobAssignmentId = dbJob.Id,
                        };
                        dbFunction.UpdateServerModificationTime(now);
                        context.JobFunctions.Add(dbFunction);
                    }
                }
            }
        }

        public async Task LoadOrgaTreeAsync(List<OrgaEntityViewModel> orgaTree)
        {
            if (orgaTree == null)
            {
                return;
            }

            // load data from db
            var en = _languageRepository.GetEnglishLanguage();
            OrgaEntity[] entities;
            Translation[] translations;
            using (var context = GetContext())
            {
                entities = await context.OrgaEntities.Where(o => !o.Deleted).Include(o => o.HeaderText).ToArrayAsync();
                translations = await context.Translations.Where(t => !t.Deleted && t.LanguageId == en.Id).ToArrayAsync();
            }

            // build the tree
            foreach (var level1Entity in entities.Where(x => x.LogicalLevel == 1).OrderBy(o => o.Name))
            {
                var level1EntityVm = GetViewModel(level1Entity, orgaTree, translations);
                foreach (var level2Entity in entities.Where(x => x.SuperEntityId == level1Entity.Id).OrderBy(o => o.OrderIndex))
                {
                    var level2EntityVm = GetViewModel(level2Entity, level1EntityVm.SubEntities, translations);
                    foreach (var level3Entity in entities.Where(x => x.SuperEntityId == level2Entity.Id).OrderBy(o => o.OrderIndex))
                    {
                        GetViewModel(level3Entity, level2EntityVm.SubEntities, translations);
                    }
                }
            }
        }

        private static OrgaEntityViewModel GetViewModel(OrgaEntity entity, ICollection<OrgaEntityViewModel> orgaTree, IEnumerable<Translation> translations)
        {
            entity.Header = translations?.FirstOrDefault(t => t.TextId == entity.HeaderTextId);
            var entityVm = new OrgaEntityViewModel(entity);
            orgaTree.Add(entityVm);
            return entityVm;
        }

        public async Task LoadPredefinedFunctions(List<PredefinedFunctionDataModel> predefinedFunctions)
        {
            if (predefinedFunctions == null)
            {
                return;
            }

            using (var context = GetContext())
            {
                var models = await context.PredefinedFunctions
                                          .Where(x => !x.Deleted)
                                          .OrderBy(f => f.Name)
                                          .ToListAsync();

                predefinedFunctions.AddRange(models.Select(x => new PredefinedFunctionDataModel(x)));
            }
        }
    }
}
