﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.Extensions;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Eurolook.ManagementConsole.Database
{
    public class StyleShortcutsDatabase : EurolookManagementConsoleDatabase, IStyleShortcutsDatabase
    {
        public StyleShortcutsDatabase(IConfiguration configuration)
            : base(configuration)
        {
        }

        public async Task<StyleShortcut> CreateStyleShortcut(StyleShortcutDataModel model)
        {
            using (var context = GetContext())
            {
                var styleShortcut = new StyleShortcut();
                styleShortcut.Init();
                styleShortcut.StyleName = model.StyleName;
                styleShortcut.Shortcut = model.Shortcut;

                var entityEntry = context.StyleShortcuts.Add(styleShortcut);
                await context.SaveChangesAsync();
                return entityEntry.Entity;
            }
        }

        public IEnumerable<StyleShortcutDataModel> GetStyleShortcuts(DataPage<StyleShortcutDataModel> page, string searchTerm, bool deleted)
        {
            using (var context = GetContext())
            {
                var query = context.StyleShortcuts.Where(GetStyleShortcutsQueryPredicate(searchTerm, deleted));
                return query.OrderByAndPaginate(page).AsEnumerable().Select(x => new StyleShortcutDataModel(x)).ToArray();
            }
        }

        public int GetStyleShortcutsCount(string searchTerm, bool deleted)
        {
            using (var context = GetContext())
            {
                return context.StyleShortcuts.Count(GetStyleShortcutsQueryPredicate(searchTerm, deleted));
            }
        }

        public StyleShortcutDataModel GetStyleShortcutForEdit(Guid id)
        {
            using (var context = GetContext())
            {
                var styleShortcut = context.StyleShortcuts.FirstOrDefault(x => x.Id == id);
                if (styleShortcut != null)
                {
                    return new StyleShortcutDataModel(styleShortcut);
                }

                return null;
            }
        }

        private Expression<Func<StyleShortcut, bool>> GetStyleShortcutsQueryPredicate(string searchTerm, bool deleted)
        {
            Expression<Func<StyleShortcut, bool>> predicate;
            if (string.IsNullOrWhiteSpace(searchTerm) || searchTerm == "*")
            {
                predicate = x => x.Deleted == deleted;
            }
            else if (string.Equals(searchTerm, "shortcut", StringComparison.InvariantCultureIgnoreCase))
            {
                predicate = x => x.Deleted == deleted && x.Shortcut.Contains(searchTerm);
            }
            else
            {
                predicate = x => x.Deleted == deleted && x.StyleName.Contains(searchTerm);
            }

            return predicate;
        }

        public void UpdateStyleShortcut(Guid id, StyleShortcutDataModel model)
        {
            using (var context = GetContext())
            {
                var styleShortcut = context.StyleShortcuts.FirstOrDefault(x => x.Id == id);
                if (styleShortcut != null)
                {
                    styleShortcut.Shortcut = model.Shortcut;
                    styleShortcut.StyleName = model.StyleName;
                    styleShortcut.UpdateServerModificationTime();
                    context.SaveChanges();
                }
            }
        }

        public async Task DeleteStyleShortcut(Guid id, bool isDeleted)
        {
            using (var context = GetContext())
            {
                var styleShortcut = await context.StyleShortcuts.FirstOrDefaultAsync(x => x.Id == id);
                if (styleShortcut != null)
                {
                    styleShortcut.Deleted = isDeleted;
                    await context.SaveChangesAsync();
                }
            }
        }
    }
}
