﻿using System;
using System.Linq;
using Eurolook.Data.Extensions;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Models;
using Microsoft.Extensions.Configuration;

namespace Eurolook.ManagementConsole.Database
{
    public class DocumentCategoryDatabase : EurolookManagementConsoleDatabase, IDocumentCategoryDatabase
    {
        public DocumentCategoryDatabase(IConfiguration configuration)
            : base(configuration)
        {
        }

        public DocumentCategory[] GetDocumentCategories(
            DataPage<DocumentCategory> page,
            string searchTerm,
            bool deleted)
        {
            using (var context = GetContext())
            {
                var query = context.DocumentCategories.Where(x => x.Deleted == deleted);

                return query.OrderByAndPaginate(page)
                            .ToArray();
            }
        }

        public DocumentCategory CreateDocumentCategory(string name)
        {
            using (var context = GetContext())
            {
                var category = new DocumentCategory();
                category.Init();
                category.Name = name;
                var entityEntry = context.DocumentCategories.Add(category);
                context.SaveChanges();
                return entityEntry.Entity;
            }
        }

        public DocumentCategory GetDocumentCategory(Guid id)
        {
            using (var context = GetContext())
            {
                return context.DocumentCategories.FirstOrDefault(dc => dc.Id == id);
            }
        }

        public void UpdateDocumentCategory(Guid id, DocumentCategory category)
        {
            using (var context = GetContext())
            {
                var dbDocumentCategory = context.DocumentCategories.FirstOrDefault(dc => dc.Id == id);
                if (dbDocumentCategory == null)
                {
                    return;
                }

                dbDocumentCategory.Name = category.Name;
                dbDocumentCategory.UpdateServerModificationTime();
                context.SaveChanges();
            }
        }

        public void DeleteDocumentCategory(Guid id)
        {
            using (var context = GetContext())
            {
                var modificationDate = DateTime.UtcNow;
                var dbDocumentCategory = context.DocumentCategories.First(u => u.Id == id);

                var documentModels = context.DocumentModels.Where(x => (x.DocumentCategoryId == id) && !x.Deleted).ToList();
                if (documentModels.Count == 1)
                {
                    throw new Exception(
                        $"The document model '{documentModels.First().Name}' is still referencing the document category.");
                }

                if (documentModels.Count > 1)
                {
                    throw new Exception(
                        $"There are still {documentModels.Count} document models referencing the document category: {string.Join(", ", documentModels.Select(x => x.Name).ToList())}.");
                }

                dbDocumentCategory.SetDeletedFlag(modificationDate);
                context.SaveChanges();
            }
        }

        public void ReviveDocumentCategory(Guid id)
        {
            using (var context = GetContext())
            {
                var dbDocumentCategory = context.DocumentCategories.First(u => u.Id == id);
                dbDocumentCategory.SetDeletedFlag(false);
                context.SaveChanges();
            }
        }

        public void DeleteDocumentCategoryPermanently(Guid id)
        {
            using (var context = GetContext())
            {
                context.DocumentCategories.Remove(context.DocumentCategories.First(x => x.Id == id));
                context.SaveChanges();
            }
        }

        public int GetDocumentCategoryCount(string searchTerm, bool deleted)
        {
            using (var context = GetContext())
            {
                return context.DocumentCategories.Count(x => x.Deleted == deleted);
            }
        }
    }
}
