﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Eurolook.Common;
using Eurolook.Common.Extensions;
using Eurolook.Data;
using Eurolook.Data.ActionLogs;
using Eurolook.Data.Constants;
using Eurolook.Data.Database;
using Eurolook.Data.ExceptionLog;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.ViewModels.Exceptions;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Eurolook.ManagementConsole.Database
{
    public class StatisticsDatabase : EurolookManagementConsoleDatabase
    {
        private const string LabelUnknown = "0 - Unknown";

        private readonly ILanguageRepository _languageRepository;

        public StatisticsDatabase(IConfiguration configuration, ILanguageRepository languageRepository)
            : base(configuration)
        {
            _languageRepository = languageRepository;
        }

        public enum MonthsFilter
        {
            OneMonth = 0,
            ThreeMonths = 1,
            SixMonths = 2,
            TwelveMonths = 3,
        }

        // Returns null if the name of the brick is filtered, e.g. the BuildingBlockBrick
        private static string GetBrickNameFromInsertionInfo(InsertionInfo info, EurolookContext context)
        {
            if (info.BrickName != null)
            {
                return info.BrickName;
            }

            var brick = context.Bricks.FirstOrDefault(b => b.Id == info.BrickId);
            if (brick is BuildingBlockBrick)
            {
                return null;
            }

            var name = brick?.Name;
            if (name == null && info.BrickId == CommonDataConstants.ChangeBrickAuthorActionId)
            {
                name = "Brick Bar: Change Brick Author";
            }

            return name ?? "(Deleted)";
        }

        public int GetUserCount(DateTime? fromDate = null)
        {
            using (var context = GetContext())
            {
                if (fromDate == null)
                {
                    return context.Users.Count(u => !u.Deleted);
                }

                int count = 0;
                foreach (var user in context.Users.Where(u => !u.Deleted).Include(u => u.Settings).ThenInclude(x => x.DeviceSettings))
                {
                    if (user.Settings.DeviceSettings.Any(d => d.LastLogin > fromDate))
                    {
                        count++;
                    }
                }

                return count;
            }
        }

        public int GetCepMemberCount(DateTime? fromDate)
        {
            using (var context = GetContext())
            {
                int count = 0;
                foreach (var user in context.Users.Where(u => !u.Deleted).Include(u => u.Settings).ThenInclude(x => x.DeviceSettings))
                {
                    if (user.Settings.DeviceSettings.Any(d => d.LastLogin > fromDate) && user.Settings.IsCepEnabled)
                    {
                        count++;
                    }
                }

                return count;
            }
        }

        public int GetStylesBoxEnabled(DateTime? fromDate)
        {
            using (var context = GetContext())
            {
                int count = 0;
                foreach (var user in context.Users.Where(u => !u.Deleted).Include(u => u.Settings).ThenInclude(x => x.DeviceSettings))
                {
                    if (user.Settings.DeviceSettings.Any(d => d.LastLogin > fromDate) && !user.Settings.IsStyleBoxDisabled)
                    {
                        count++;
                    }
                }

                return count;
            }
        }

        internal List<PieChartSegment> GetColorSchemeChart(DateTime? fromDate)
        {
            var result = new List<PieChartSegment>();
            using (var context = GetContext())
            {
                var data = new Dictionary<Guid, int>
                {
                    [Guid.Empty] = 0,
                };

                var colorSchemes = context.ColorSchemes.ToList();
                foreach (var colorScheme in colorSchemes)
                {
                    data.Add(colorScheme.Id, 0);
                }

                foreach (var user in context.Users.Where(u => !u.Deleted).Include(u => u.Settings).ThenInclude(x => x.DeviceSettings))
                {
                    if (!user.Settings.DeviceSettings.Any(d => d.LastLogin > fromDate))
                    {
                        continue;
                    }

                    var id = user.Settings.ColorSchemeId ?? Guid.Empty;
                    data[id]++;
                }

                result.Add(
                    new PieChartSegment
                    {
                        Name = "Default",
                        Y = data[Guid.Empty],
                    });
                result.AddRange(
                    colorSchemes.Select(
                        colorScheme => new PieChartSegment
                        {
                            Name = colorScheme.Name,
                            Y = data[colorScheme.Id],
                        }));
            }

            return result;
        }

        public VersionUsageInfo[] GetVersionCounts(DateTime? fromDateUtc, string filter)
        {
            var result = new Dictionary<string, VersionUsageInfo>();
            using (var context = GetContext())
            {
                var deviceSettings = fromDateUtc != null
                    ? context.DeviceSettings.Where(ds => !ds.Deleted && ds.LastLogin >= fromDateUtc).ToArray()
                    : context.DeviceSettings.Where(ds => !ds.Deleted).ToArray();

                var filteringRegex = new Regex(filter);

                foreach (var ds in deviceSettings)
                {
                    if (filteringRegex.IsMatch(ds.DeviceName))
                    {
                        string addinVersion = ds.AddinVersion;
                        if (string.IsNullOrWhiteSpace(addinVersion))
                        {
                            addinVersion = LabelUnknown;
                        }

                        if (!result.ContainsKey(addinVersion))
                        {
                            result[addinVersion] = new VersionUsageInfo
                            {
                                Version = addinVersion,
                            };
                        }

                        var versionInfo = result[addinVersion];
                        versionInfo.DeviceCount += 1;
                    }
                }
            }

            return result.Values.ToArray();
        }

        public VersionUsageInfo[] GetVersionUsageInfo(DateTime? fromDateUtc, string filter)
        {
            var result = new Dictionary<string, VersionUsageInfo>();
            using (var context = GetContext())
            {
                var deviceSettings = fromDateUtc != null
                    ? context.DeviceSettings.Where(ds => !ds.Deleted && ds.LastLogin >= fromDateUtc).ToArray()
                    : context.DeviceSettings.Where(ds => !ds.Deleted).ToArray();

                var filteringRegex = new Regex(filter);

                foreach (var ds in deviceSettings)
                {
                    if (filteringRegex.IsMatch(ds.DeviceName))
                    {
                        string addinVersion = ds.AddinVersion;
                        if (string.IsNullOrWhiteSpace(addinVersion))
                        {
                            addinVersion = LabelUnknown;
                        }

                        string owner = ds.Owner;
                        if (string.IsNullOrWhiteSpace(owner))
                        {
                            owner = LabelUnknown;
                        }

                        if (!result.ContainsKey(addinVersion))
                        {
                            result[addinVersion] = new VersionUsageInfo
                            {
                                Version = addinVersion,
                            };
                        }

                        var versionInfo = result[addinVersion];
                        if (!versionInfo.DeviceOwners.ContainsKey(owner))
                        {
                            versionInfo.DeviceOwners[owner] = new VersionOwnerInfo
                            {
                                Name = owner,
                                DeviceCount = 0,
                            };
                        }

                        var deviceOwner = versionInfo.DeviceOwners[owner];
                        deviceOwner.DeviceCount += 1;
                        versionInfo.DeviceCount += 1;
                    }
                }
            }

            return result.Values.ToArray();
        }

        internal Dictionary<string, int> GetDeviceOwners(DateTime? fromDateUtc, string filter)
        {
            var result = new Dictionary<string, int>();
            using (var context = GetContext())
            {
                var deviceSettings = fromDateUtc != null
                    ? context.DeviceSettings.Where(ds => !ds.Deleted && ds.LastLogin >= fromDateUtc).ToArray()
                    : context.DeviceSettings.Where(ds => !ds.Deleted).ToArray();

                var filteringRegex = new Regex(filter);

                foreach (var ds in deviceSettings)
                {
                    if (filteringRegex.IsMatch(ds.DeviceName))
                    {
                        string owner = ds.Owner;
                        if (string.IsNullOrWhiteSpace(owner))
                        {
                            owner = LabelUnknown;
                        }

                        int counter = 1;
                        if (result.ContainsKey(owner))
                        {
                            counter = result[owner] += 1;
                        }

                        result[owner] = counter;
                    }
                }
            }

            return result;
        }

        public ActionLogInfo[] GetActionLogInfos(DateTime? fromDate, Guid? documentType)
        {
            var infos = new Dictionary<Guid, ActionLogInfo>();
            using (var context = GetContext())
            {
                foreach (
                    var action in context.ActionLogs.Where(a => (a.Action == BrickInsertionLog.ActionId || a.Action == ToolCommandExecutionLog.ActionId) &&
                                                                a.Info != null && a.CreatedUtc >= fromDate &&
                                                                (documentType == null || a.DocumentModel.Id == documentType)).ToArray())
                {
                    var info = JsonConvert.DeserializeObject<InsertionInfo>(action.Info);
                    if (!infos.ContainsKey(info.BrickId))
                    {
                        var brickName = GetBrickNameFromInsertionInfo(info, context);
                        if (brickName != null)
                        {
                            infos[info.BrickId] = new ActionLogInfo
                            {
                                Id = info.BrickId,
                                Name = brickName,
                            };
                        }
                        else
                        {
                            continue;
                        }
                    }

                    var brickInfo = infos[info.BrickId];
                    if (info.Method?.Equals("Click", StringComparison.InvariantCultureIgnoreCase) == true)
                    {
                        brickInfo.ClickCount += 1;
                    }

                    if (info.Method?.Equals("HotKey", StringComparison.InvariantCultureIgnoreCase) == true)
                    {
                        brickInfo.HotKeyCount += 1;
                    }

                    if (info.Method?.Equals("Handler", StringComparison.InvariantCultureIgnoreCase) == true)
                    {
                        brickInfo.HandlerCount += 1;
                    }

                    if (info.Method?.Equals("Ribbon", StringComparison.InvariantCultureIgnoreCase) == true)
                    {
                        brickInfo.RibbonCount += 1;
                    }
                }
            }

            return infos.Values.ToArray();
        }

        public ActionLogInfoHistory GetBrickUsageHistory(DateTime? fromDate, Guid? documentType)
        {
            ActionLogInfoHistory history;
            using (var context = GetContext())
            {
                history = new ActionLogInfoHistory(fromDate ?? context.ActionLogs.Where(a => a.Action == BrickInsertionLog.ActionId).Select(a => a.CreatedUtc).OrderBy(d => d).FirstOrDefault());
                foreach ((var month, int index) in history.Dates.Select((date, index) => (date, index)))
                {
                    foreach (
                        var action in context.ActionLogs.Where(
                            a => (a.Action == BrickInsertionLog.ActionId || a.Action == ToolCommandExecutionLog.ActionId) &&
                                 (documentType == null || a.DocumentModel.Id == documentType) &&
                                 a.Info != null && a.CreatedUtc >= month && a.CreatedUtc < month.AddMonths(1)).ToArray())
                    {
                        var info = JsonConvert.DeserializeObject<InsertionInfo>(action.Info);
                        if (!history.Counts.ContainsKey(info.BrickId))
                        {
                            var brickName = GetBrickNameFromInsertionInfo(info, context);
                            if (brickName != null)
                            {
                                history.Counts[info.BrickId] = new HistoryCountEntry(history.Dates.Count)
                                {
                                    Name = brickName,
                                };
                            }
                            else
                            {
                                continue;
                            }
                        }

                        var historyEntry = history.Counts[info.BrickId];
                        historyEntry.Count[index] += 1;
                    }
                }
            }

            return history;
        }

        public Dictionary<string, List<string>> GetBricksByCategory()
        {
            var result = new Dictionary<string, List<string>>();
            using (var context = GetContext())
            {
                foreach (var brickCategory in context.BrickCategories.Where(bc => !bc.Deleted && bc.Id != CommonDataConstants.CustomBricksCategoryId).OrderBy(bc => bc.UiPositionIndex).ToList())
                {
                    var brickNames = context.Bricks.Where(b => !b.Deleted && b.CategoryId == brickCategory.Id).Select(brick => brick.Name).OrderBy(b => b).ToList();

                    result[brickCategory.Name] = brickNames;
                }
            }

            return result;
        }

        public Dictionary<string, List<DocumentModel>> GetDocumentModelsByCategory()
        {
            var result = new Dictionary<string, List<DocumentModel>>();
            using (var context = GetContext())
            {
                foreach (var documentCategory in context.DocumentCategories.Where(dc => !dc.Deleted && dc.Name != null).OrderBy(dc => dc.Name).ToList())
                {
                    var documentTypes = context.DocumentModels
                                               .Where(dm => !dm.Deleted && dm.DocumentCategoryId == documentCategory.Id)
                                               .OrderBy(dm => dm.UiPositionIndex).ThenBy(dm => dm.Name).ToList();

                    result[documentCategory.Name] = documentTypes;
                }
            }

            return result;
        }

        public async Task<Dictionary<DateTime, int>> GetActiveDevices(DateTime? fromDate)
        {
            var result = new Dictionary<DateTime, int>();
            var thisMonth = DateTime.UtcNow.ToDateType(DateTimeExtensions.DateGroupType.Month);

            using (var context = GetContext())
            {
                var devices = await context.DeviceSettings
                                       .Where(d => !d.Deleted && d.ActivityTrack != null && d.LastLogin >= (fromDate ?? DateTime.MinValue))
                                       .ToArrayAsync();

                foreach (var device in devices)
                {
                    if (string.IsNullOrWhiteSpace(device.ActivityTrack))
                    {
                        continue;
                    }

                    foreach (var activeMonth in device.DeserializeActivityTrack().Where(m => m >= (fromDate ?? DateTime.MinValue)))
                    {
                        if (result.ContainsKey(activeMonth))
                        {
                            result[activeMonth] += 1;
                        }
                        else
                        {
                            result[activeMonth] = 1;
                        }
                    }
                }

                var minDate = fromDate ?? result.Keys.Min(d => d);
                for (var m = minDate;
                     m <= thisMonth;
                     m = m.AddMonths(1))
                {
                    if (!result.ContainsKey(m))
                    {
                        result[m] = 0;
                    }
                }
            }

            return result;
        }

        public DocumentCreationHistory GetDocumentCreationHistory(DateTime? fromDate)
        {
            DocumentCreationHistory result;

            using (var context = GetContext())
            {
                result = new DocumentCreationHistory(fromDate ?? context.ActionLogs.Where(a => a.Action == DocumentCreationLog.ActionId).Select(a => a.CreatedUtc).OrderBy(d => d).FirstOrDefault());
                var sumSeries = new HistoryCountEntry(result.Dates.Count) { Name = "Sum" };
                result.Series.Add(sumSeries);
                var templateStoreSeries = new HistoryCountEntry(result.Dates.Count) { Name = "Template Store Document" };
                result.Series.Add(templateStoreSeries);

                var documentModels = context.DocumentModels.ToArray();
                for (int i = 0; i < result.Dates.Count; i++)
                {
                    var month = result.Dates[i];
                    var nextMonth = i < (result.Dates.Count - 1) ? result.Dates[i + 1] : DateTime.MaxValue;

                    var createActions = context.ActionLogs.Where(
                        a => a.Action == DocumentCreationLog.ActionId
                             && a.CreatedUtc >= month
                             && a.CreatedUtc < nextMonth).ToList();

                    foreach (var documentModel in documentModels)
                    {
                        var series = result.Series.FirstOrDefault(x => x.Name == documentModel.Name);
                        if (series == null)
                        {
                            series = new HistoryCountEntry(result.Dates.Count) { Name = documentModel.Name };
                            result.Series.Add(series);
                        }

                        var count = createActions.Count(x => x.DocumentModelId == documentModel.Id);
                        series.Count[i] = count;
                        sumSeries.Count[i] += count;
                    }

                    var templateStoreCount = context.ActionLogs.Count(
                        a => a.Action == TemplateStoreDocumentCreationLog.ActionId
                             && a.CreatedUtc >= month
                             && a.CreatedUtc < nextMonth);

                    templateStoreSeries.Count[i] = templateStoreCount;
                    sumSeries.Count[i] += templateStoreCount;
                }
            }

            result.Series.RemoveAll(x => x.Count.Sum() == 0);
            result.Series = result.Series.OrderByDescending(x => x.Count.Sum()).ToList();
            return result;
        }

        public DocumentCreationInfobyLang[] GetDocumentCreationInfosByLang(DateTime? fromDate)
        {
            var creationInfos = new Dictionary<Guid, DocumentCreationInfobyLang>();
            using (var context = GetContext())
            {
                foreach (var action in context.ActionLogs.Where(a => a.Action == DocumentCreationLog.ActionId && a.CreatedUtc >= fromDate).ToArray())
                {
                    if (action.DocumentModelId == null)
                    {
                        continue;
                    }

                    if (action.OriginatorId == null)
                    {
                        continue;
                    }

                    if (action.Info == null)
                    {
                        continue;
                    }

                    var dmId = action.DocumentModelId.Value;
                    var locale = JsonConvert.DeserializeObject<DocumentCreationLog>(action.Info).Language;

                    if (!creationInfos.ContainsKey(dmId))
                    {
                        string documentModelName = context.DocumentModels.Where(dm => dm.Id == dmId).Select(dm => dm.Name).FirstOrDefault();
                        creationInfos[dmId] = new DocumentCreationInfobyLang()
                        {
                            DocumentModelName = documentModelName ?? "(deleted)",
                        };
                    }

                    var creationInfo = creationInfos[dmId];
                    if (!creationInfo.DocumentCreations.ContainsKey(locale))
                    {
                        string language = context.Languages.Where(l => l.Locale == locale).Select(l => l.Name).FirstOrDefault();
                        creationInfo.DocumentCreations[locale] = new DocumentCreationLanguageInfo()
                        {
                            LanguageName = language ?? locale,
                        };
                    }

                    creationInfo.DocumentCreations[locale].CreationCount += 1;
                }

                var templateStoreCreationInfo = new DocumentCreationInfobyLang()
                {
                    DocumentModelName = "Template Store Document",
                };
                creationInfos[Guid.NewGuid()] = templateStoreCreationInfo;

                foreach (var action in context.ActionLogs
                                              .Where(
                                                  a => a.Action == TemplateStoreDocumentCreationLog.ActionId
                                                       && a.CreatedUtc >= fromDate).ToArray())
                {
                    if (action.DocumentModelId == null)
                    {
                        continue;
                    }

                    if (action.OriginatorId == null)
                    {
                        continue;
                    }

                    if (action.Info == null)
                    {
                        continue;
                    }

                    var locale = JsonConvert.DeserializeObject<TemplateStoreDocumentCreationLog>(action.Info).Language;
                    if (!templateStoreCreationInfo.DocumentCreations.ContainsKey(locale))
                    {
                        string language = context.Languages.Where(l => l.Locale == locale).Select(l => l.Name).FirstOrDefault();
                        templateStoreCreationInfo.DocumentCreations[locale] = new DocumentCreationLanguageInfo()
                        {
                            LanguageName = language ?? locale,
                        };
                    }

                    templateStoreCreationInfo.DocumentCreations[locale].CreationCount += 1;
                }
            }

            return creationInfos.Values.ToArray();
        }

        public DocumentCreationInfobyDg[] GetDocumentCreationInfosByDg(DateTime? fromDate)
        {
            var creationInfos = new Dictionary<Guid, DocumentCreationInfobyDg>();
            var originatorMap = new Dictionary<Guid, OrgaEntity>();
            using (var context = GetContext())
            {
                var templateStoreGuide = Guid.NewGuid();

                foreach (var action in context.ActionLogs.Where(
                             a => (a.Action == DocumentCreationLog.ActionId
                                   || a.Action == TemplateStoreDocumentCreationLog.ActionId)
                                  && a.CreatedUtc >= fromDate).ToArray())
                {
                    if (action.DocumentModelId == null)
                    {
                        continue;
                    }

                    if (action.OriginatorId == null)
                    {
                        continue;
                    }

                    // find the DG in the DB
                    var originatorId = action.OriginatorId.Value;
                    if (!originatorMap.ContainsKey(originatorId))
                    {
                        originatorMap[originatorId] = GetDgOfEntity(context, originatorId);
                    }

                    var dg = originatorMap[originatorId];
                    var dgId = dg?.Id ?? Guid.Empty;

                    // get or create a record for that originator
                    if (!creationInfos.ContainsKey(dgId))
                    {
                        creationInfos[dgId] = new DocumentCreationInfobyDg
                        {
                            DgName = dg?.Name ?? "(Deleted)",
                        };
                    }

                    var creationInfo = creationInfos[dgId];

                    // get or create a DocumentModelCreationInfo record for that document model for that originator
                    string documentName;
                    Guid documentKey;
                    if (action.Action == DocumentCreationLog.ActionId)
                    {
                        var documentModel = context.DocumentModels.FirstOrDefault(dm => dm.Id == action.DocumentModelId.Value);
                        documentKey = documentModel?.Id ?? Guid.Empty;
                        documentName = documentModel?.Name ?? "(Deleted)";
                    }
                    else if (action.Action == TemplateStoreDocumentCreationLog.ActionId)
                    {
                        documentKey = templateStoreGuide;
                        documentName = "Template Store Document";
                    }
                    else
                    {
                        continue;
                    }

                    if (!creationInfo.DocumentCreations.ContainsKey(documentKey))
                    {
                        creationInfo.DocumentCreations[documentKey] = new DocumentModelCreationInfo
                        {
                            DocumentModelName = documentName,
                        };
                    }

                    var documentModelInfo = creationInfo.DocumentCreations[documentKey];
                    documentModelInfo.CreationCount += 1;
                }
            }

            return creationInfos.Values.ToArray();
        }

        public DocumentCreationInfoByEntity[] GetDocumentCreationInfosByEntity(DateTime? fromDate)
        {
            var creationInfos = new Dictionary<Guid, DocumentCreationInfoByEntity>();
            var originatorMap = new Dictionary<Guid, OrgaEntity>();
            using (var context = GetContext())
            {
                foreach (var action in context.ActionLogs.Where(a => a.Action == DocumentCreationLog.ActionId && a.CreatedUtc >= fromDate).ToArray())
                {
                    if (action.DocumentModelId == null)
                    {
                        continue;
                    }

                    if (action.OriginatorId == null)
                    {
                        continue;
                    }

                    // find the entity in the DB
                    var originatorId = action.OriginatorId.Value;
                    if (!originatorMap.ContainsKey(originatorId))
                    {
                        originatorMap[originatorId] = GetOrgaEntity(context, originatorId);
                    }

                    var orgEntity = originatorMap[originatorId];
                    var orgEntityId = orgEntity?.Id ?? Guid.Empty;

                    // get or create a record for that originator
                    if (!creationInfos.ContainsKey(orgEntityId))
                    {
                        creationInfos[orgEntityId] = new DocumentCreationInfoByEntity
                        {
                            EntityName = orgEntity?.Name ?? "(Deleted)",
                        };
                    }

                    creationInfos[orgEntityId].TotalCreationCount += 1;
                }
            }

            return creationInfos.Values.ToArray();
        }

        private OrgaEntity GetDgOfEntity(EurolookServerContext context, Guid orgaEntityId)
        {
            var orgaEntity = context.OrgaEntities.FirstOrDefault(o => o.Id == orgaEntityId);
            if (orgaEntity == null)
            {
                return null;
            }

            if (orgaEntity.LogicalLevel == 1)
            {
                return orgaEntity;
            }

            if (orgaEntity.SuperEntityId != null)
            {
                return GetDgOfEntity(context, orgaEntity.SuperEntityId.Value);
            }

            throw new Exception("Selected OrgaEntity does not belong to a DG");
        }

        public DocumentCreationInfobyLang[] GetMostUsedTemplateStoreTemplates(DateTime? fromDate)
        {
            var creationInfos = new Dictionary<Guid, DocumentCreationInfobyLang>();
            var creationLogs = new List<TemplateStoreDocumentCreationLog>();

            using (var context = GetContext())
            {
                creationLogs.AddRange(
                    from action in context.ActionLogs.Where(a => a.Action == TemplateStoreDocumentCreationLog.ActionId && a.CreatedUtc >= fromDate)
                                          .ToArray()
                    where action.Info != null
                    select JsonConvert.DeserializeObject<TemplateStoreDocumentCreationLog>(action.Info));

                var top = creationLogs
                          .GroupBy(a => a.TemplateId)
                          .OrderByDescending(g => g.Count())
                          .Take(15)
                          .ToList();

                foreach (var entry in top)
                {
                    if (!creationInfos.ContainsKey(entry.Key))
                    {
                        string name;
                        var template = context.Templates.FirstOrDefault(t => t.Id == entry.Key);
                        if (template != null)
                        {
                            name = template.Name;
                            if (template.Deleted)
                            {
                                name += " (Deleted)";
                            }
                        }
                        else
                        {
                            name = "unknown";
                        }

                        creationInfos[entry.Key] = new DocumentCreationInfobyLang { DocumentModelName = name };
                    }

                    var creationInfo = creationInfos[entry.Key];

                    foreach (var log in entry)
                    {
                        if (!creationInfo.DocumentCreations.ContainsKey(log.Language))
                        {
                            string language = context.Languages.Where(l => l.Locale == log.Language).Select(l => l.Name).FirstOrDefault();
                            creationInfo.DocumentCreations[log.Language] = new DocumentCreationLanguageInfo()
                            {
                                LanguageName = language ?? log.Language,
                            };
                        }

                        creationInfo.DocumentCreations[log.Language].CreationCount += 1;
                    }
                }
            }

            return creationInfos
                   .Values
                   .OrderByDescending(l => l.TotalCreationCount)
                   .ToArray();
        }

        public DocumentCreationInfobyLang[] GetTemplateStoreDocumentsByDocumentModel(DateTime? fromDate)
        {
            var creationInfos = new Dictionary<Guid, DocumentCreationInfobyLang>();

            using (var context = GetContext())
            {
                foreach (var action in context.ActionLogs
                                              .Where(
                                                  a => a.Action == TemplateStoreDocumentCreationLog.ActionId
                                                       && a.CreatedUtc >= fromDate).ToArray())
                {
                    if (action.DocumentModelId == null || action.OriginatorId == null || action.Info == null)
                    {
                        continue;
                    }

                    var baseDM = action.DocumentModelId.Value;
                    var locale = JsonConvert.DeserializeObject<TemplateStoreDocumentCreationLog>(action.Info).Language;

                    if (!creationInfos.ContainsKey(baseDM))
                    {
                        string documentModelName = context.DocumentModels.Where(dm => dm.Id == baseDM).Select(dm => dm.Name).FirstOrDefault();
                        creationInfos[baseDM] = new DocumentCreationInfobyLang()
                        {
                            DocumentModelName = documentModelName ?? "(deleted)",
                        };
                    }

                    var creationInfo = creationInfos[baseDM];
                    if (!creationInfo.DocumentCreations.ContainsKey(locale))
                    {
                        string language = context.Languages.Where(l => l.Locale == locale).Select(l => l.Name).FirstOrDefault();
                        creationInfo.DocumentCreations[locale] = new DocumentCreationLanguageInfo()
                        {
                            LanguageName = language ?? locale,
                        };
                    }

                    creationInfo.DocumentCreations[locale].CreationCount += 1;
                }
            }

            return creationInfos
                   .Values
                   .ToArray();
        }

        public DocumentCreationInfobyDg[] GetTemplateStoreDocumentsByOriginator(DateTime? fromDate)
        {
            var creationInfos = new Dictionary<Guid, DocumentCreationInfobyDg>();
            var originatorMap = new Dictionary<Guid, OrgaEntity>();

            using (var context = GetContext())
            {
                foreach (var action in context.ActionLogs.Where(
                                                  a => a.Action == TemplateStoreDocumentCreationLog.ActionId
                                                       && a.CreatedUtc >= fromDate)
                                              .ToArray())
                {
                    if (!action.DocumentModelId.HasValue || !action.OriginatorId.HasValue || action.Info == null)
                    {
                        continue;
                    }

                    // find the originating DG
                    var originatorId = action.OriginatorId.Value;
                    if (!originatorMap.ContainsKey(originatorId))
                    {
                        originatorMap[originatorId] = GetDgOfEntity(context, originatorId);
                    }

                    var dg = originatorMap[originatorId];
                    var dgId = dg?.Id ?? Guid.Empty;

                    if (!creationInfos.ContainsKey(dgId))
                    {
                        creationInfos[dgId] = new DocumentCreationInfobyDg { DgName = dg?.Name ?? "(Deleted)" };
                    }

                    var logInfo = JsonConvert.DeserializeObject<TemplateStoreDocumentCreationLog>(action.Info);
                    var creationInfo = creationInfos[dgId];
                    var template = context.Templates.FirstOrDefault(t => t.Id == logInfo.TemplateId);
                    var templateId = template?.Id ?? Guid.Empty;
                    string templateName = template?.Name ?? "(Deleted)";

                    if (!creationInfo.DocumentCreations.ContainsKey(templateId))
                    {
                        creationInfo.DocumentCreations[templateId] = new DocumentModelCreationInfo
                        {
                            DocumentModelName = templateName,
                        };
                    }

                    creationInfo.DocumentCreations[templateId].CreationCount += 1;
                }
            }

            return creationInfos.Values.ToArray();
        }

        public async Task<IEnumerable<ExceptionBucketViewModel>> GetExceptionBuckets(DataPage<ExceptionBucketViewModel> dataPage, DateTime? fromDateUtc, List<string> userFilter)
        {
            var bucketViewModels = new List<ExceptionBucketViewModel>();
            using (var context = GetContext())
            {
                // in this method data is only read, never written - so we make the context non-tracking
                context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

                var filteredBucketsQuery = context.ExceptionBuckets
                                       .Where(b => !b.IsIgnored && b.ModifiedUtc >= fromDateUtc)
                                       .Select(b => b.Id);

                // find applicable buckets depending on user filter
                if (!userFilter.IsNullOrEmpty())
                {
                    var ids = filteredBucketsQuery;
                    var users = await FindOriginatorGuids(context, userFilter);

                    filteredBucketsQuery = context.ExceptionLogEntries
                                       .Where(e => e.CreatedUtc >= fromDateUtc && ids.Any(id => e.ExceptionBuckedId == id))
                                       .Select(e => new
                                       {
                                           bucket = e.ExceptionBuckedId,
                                           originator = e.OriginatorId,
                                       })
                                       .Where(p => users.Any(g => p.originator == g))
                                       .Select(p => p.bucket)
                                       .Distinct();
                }

                dataPage.TotalCount = (uint)await filteredBucketsQuery.CountAsync();

                // prepare DB query to count affected users
                var affectedUsersQuery = context.ExceptionLogEntries
                               .Where(e => e.CreatedUtc >= fromDateUtc)
                               .Select(e => new { e.ExceptionBuckedId, e.OriginatorId })
                               .Distinct()
                               .GroupBy(e => e.ExceptionBuckedId)
                               .Select(g => new
                               {
                                   ExceptionBucketId = g.Key,
                                   Count = g.Count(),
                               });

                // prepare DB query to count occurrences
                var occurrencesQuery = context.ExceptionLogEntries
                                              .Where(e => e.CreatedUtc >= fromDateUtc)
                                              .GroupBy(e => e.ExceptionBuckedId)
                                              .Select(g => new
                                              {
                                                  ExceptionBucketId = g.Key,
                                                  Count = g.Count(),
                                              });

                // Sort and filter buckets depending on request parameters
                List<byte[]> bucketIds;                     // Ids of the relevant buckets
                Dictionary<byte[], int> affectedUsers;      // Affected users for the relevant buckets
                Dictionary<byte[], int> occurrences;        // Occurrences for the relevant buckets
                if (string.Equals(dataPage.OrderProperty, "occurred", StringComparison.InvariantCultureIgnoreCase))
                {
                    // Sort by date
                    if (dataPage.OrderAscending)
                    {
                        bucketIds = context.ExceptionBuckets
                                           .Where(e => filteredBucketsQuery.Any(id => e.Id == id))
                                           .OrderBy(b => b.ModifiedUtc)
                                           .Select(e => e.Id)
                                           .Skip((int)dataPage.From)
                                           .Take((int)dataPage.PageSize)
                                           .ToList();
                    }
                    else
                    {
                        bucketIds = context.ExceptionBuckets
                                           .Where(e => filteredBucketsQuery.Any(id => e.Id == id))
                                           .OrderByDescending(b => b.ModifiedUtc)
                                           .Select(e => e.Id)
                                           .Skip((int)dataPage.From)
                                           .Take((int)dataPage.PageSize)
                                           .ToList();
                    }

                    affectedUsers = affectedUsersQuery
                                    .Where(count => bucketIds.Any(id => id == count.ExceptionBucketId))
                                    .ToDictionary(count => count.ExceptionBucketId, count => count.Count, new ByteArrayComparer());

                    occurrences = occurrencesQuery
                                  .Where(count => bucketIds.Any(id => id == count.ExceptionBucketId))
                                  .ToDictionary(count => count.ExceptionBucketId, count => count.Count, new ByteArrayComparer());
                }
                else if (string.Equals(dataPage.OrderProperty, "affectedUsers", StringComparison.InvariantCultureIgnoreCase))
                {
                    // Sort by affected users
                    if (dataPage.OrderAscending)
                    {
                        affectedUsers = affectedUsersQuery
                                      .Where(e => filteredBucketsQuery.Any(id => e.ExceptionBucketId == id))
                                      .OrderBy(e => e.Count)
                                      .Skip((int)dataPage.From)
                                      .Take((int)dataPage.PageSize)
                                      .ToDictionary(
                                          count => count.ExceptionBucketId,
                                          count => count.Count,
                                          new ByteArrayComparer());
                    }
                    else
                    {
                        affectedUsers = affectedUsersQuery
                                        .Where(e => filteredBucketsQuery.Any(id => e.ExceptionBucketId == id))
                                        .OrderByDescending(e => e.Count)
                                        .Skip((int)dataPage.From)
                                        .Take((int)dataPage.PageSize)
                                        .ToDictionary(
                                            count => count.ExceptionBucketId,
                                            count => count.Count,
                                            new ByteArrayComparer());
                    }

                    bucketIds = affectedUsers.Keys.ToList();

                    occurrences = occurrencesQuery
                                    .Where(count => bucketIds.Any(id => id == count.ExceptionBucketId))
                                    .ToDictionary(count => count.ExceptionBucketId, count => count.Count, new ByteArrayComparer());
                }
                else
                {
                    // sort by occurrences
                    if (dataPage.OrderAscending)
                    {
                        occurrences = occurrencesQuery
                                      .Where(e => filteredBucketsQuery.Any(id => e.ExceptionBucketId == id))
                                      .OrderBy(e => e.Count)
                                      .Skip((int)dataPage.From)
                                      .Take((int)dataPage.PageSize)
                                      .ToDictionary(
                                          count => count.ExceptionBucketId,
                                          count => count.Count,
                                          new ByteArrayComparer());
                    }
                    else
                    {
                        occurrences = occurrencesQuery
                                      .Where(e => filteredBucketsQuery.Any(id => e.ExceptionBucketId == id))
                                      .OrderByDescending(e => e.Count)
                                      .Skip((int)dataPage.From)
                                      .Take((int)dataPage.PageSize)
                                      .ToDictionary(
                                          count => count.ExceptionBucketId,
                                          count => count.Count,
                                          new ByteArrayComparer());
                    }

                    bucketIds = occurrences.Keys.ToList();

                    affectedUsers = affectedUsersQuery
                                    .Where(count => bucketIds.Any(id => id == count.ExceptionBucketId))
                                    .ToDictionary(count => count.ExceptionBucketId, count => count.Count, new ByteArrayComparer());
                }

                // Finally fetch the whole exception buckets required
                var buckets = context.ExceptionBuckets.Where(b => bucketIds.Any(id => b.Id == id)).ToDictionary(bucket => bucket.Id, bucket => bucket, new ByteArrayComparer());

                // Create the view models
                foreach (var id in bucketIds)
                {
                    bucketViewModels.Add(
                        new ExceptionBucketViewModel(buckets[id])
                        {
                            Count = occurrences[id],
                            AffectedUsers = affectedUsers[id],
                        });
                }
            }

            return bucketViewModels;
        }

        internal async Task<int> GetExceptionBucketsCount(DateTime? fromDateUtc, List<string> userFilter)
        {
            using (var context = GetContext())
            {
                // in this method data is only read, never written - so we make the context non-tracking
                context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

                var filteredBucketsQuery = context.ExceptionBuckets
                                                  .Where(b => !b.IsIgnored && b.ModifiedUtc >= fromDateUtc)
                                                  .Select(b => b.Id);

                // find applicable buckets depending on user filter
                if (!userFilter.IsNullOrEmpty())
                {
                    var ids = filteredBucketsQuery;
                    var users = await FindOriginatorGuids(context, userFilter);

                    filteredBucketsQuery = context.ExceptionLogEntries
                                                  .Where(e => ids.Any(id => e.ExceptionBuckedId == id))
                                                  .Select(e => new
                                                  {
                                                      bucket = e.ExceptionBuckedId,
                                                      originator = e.OriginatorId,
                                                  })
                                                  .Where(p => users.Any(g => p.originator == g))
                                                  .Select(p => p.bucket)
                                                  .Distinct();
                }

                return await filteredBucketsQuery.CountAsync();
            }
        }

        public ExceptionBucket GetExceptionBucket(string id)
        {
            var bucketId = StringToByteArray(id);
            using (var context = GetContext())
            {
                return context.ExceptionBuckets.FirstOrDefault(b => b.Id == bucketId);
            }
        }

        public List<ExceptionLogDetailViewModel> GetExceptionLogDetails(string id, DataPage<ExceptionLogDetailViewModel> dataPage, DateTime? fromDateUtc)
        {
            if (id.IsNullOrEmpty())
            {
                return new List<ExceptionLogDetailViewModel>();
            }

            using (var context = GetContext())
            {
                // in this method data is only read, never written - so we make the context non-tracking
                context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

                var en = _languageRepository.GetEnglishLanguage();
                var bucketId = StringToByteArray(id);

                var logEntriesQuery = context.ExceptionLogEntries
                                             .Where(e => e.ExceptionBuckedId == bucketId && e.CreatedUtc >= fromDateUtc);
                logEntriesQuery = dataPage.OrderAscending
                    ? logEntriesQuery.OrderBy(l => l.CreatedUtc)
                    : logEntriesQuery.OrderByDescending(l => l.CreatedUtc);

                dataPage.TotalCount = (uint)logEntriesQuery.Count();

                logEntriesQuery = logEntriesQuery.Skip((int)dataPage.From).Take((int)dataPage.PageSize);

                var originatorIds = logEntriesQuery
                                    .Select(e => e.OriginatorId)
                                    .Distinct();

                var users = context.Users
                                   .Where(u => originatorIds.Any(guid => u.Id == guid))
                                   .Select(
                                       u => new
                                       {
                                           id = u.Id,
                                           name = u.Login,
                                       })
                                   .ToDictionary(u => u.id, u => u.name);

                var entities = context.OrgaEntities
                                      .Where(oE => originatorIds.Any(guid => oE.Id == guid))
                                      .ToDictionary(oE => oE.Id, oE => oE);

                var exceptionLogEntries = logEntriesQuery.ToList();

                return exceptionLogEntries.Select(
                    exceptionLogEntry => new ExceptionLogDetailViewModel
                    {
                        Message = exceptionLogEntry.Message,
                        Created = exceptionLogEntry.CreatedUtc.ToLocalTime().ToString(/*CultureInfo.CurrentUICulture*/),
                        Originator = GetOriginator(exceptionLogEntry.OriginatorId, users, entities, en),
                        ClientVersion = exceptionLogEntry.ClientVersion,
                        OfficeVersion = exceptionLogEntry.OfficeVersion,
                    }).ToList();
            }
        }

        public IEnumerable<(string Version, int Count)> GetAffectedOfficeVersions(ExceptionBucket exceptionBucket, DateTime? fromDateUtc)
        {
            IEnumerable<(string Version, int Count)> result;
            using (var context = GetContext())
            {
                var versionList = context.ExceptionLogEntries
                                         .Where(
                                             e => e.ExceptionBuckedId == exceptionBucket.Id
                                                  && e.CreatedUtc >= fromDateUtc)
                                         .GroupBy(l => l.OfficeVersion)
                                         .Select(
                                             g => new
                                             {
                                                 version = g.Key,
                                                 count = g.Count(),
                                             })
                                         .ToList();

                result = versionList.Select(g => (Version: g.version, Count: g.count));
            }

            return result;
        }

        public IEnumerable<(string Version, int Count)> GetAffectedClientVersions(ExceptionBucket exceptionBucket, DateTime? fromDateUtc)
        {
            IEnumerable<(string Version, int Count)> result;
            using (var context = GetContext())
            {
                var versionList = context.ExceptionLogEntries
                                         .Where(
                                             e => e.ExceptionBuckedId == exceptionBucket.Id
                                                  && e.CreatedUtc >= fromDateUtc)
                                         .GroupBy(l => l.ClientVersion)
                                         .Select(
                                             g => new
                                             {
                                                 version = g.Key,
                                                 count = g.Count(),
                                             })
                                         .ToList();

                result = versionList.Select(g => (Version: g.version, Count: g.count));
            }

            return result;
        }

        public async Task<IEnumerable<ExceptionLogInfo>> GetExceptionLogInfoByEntry(DateTime? fromDateUtc)
        {
            await using var context = GetContext();

            // in this method data is only read, never written - so we make the context non-tracking
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            var exceptionLogInfo = (from e in context.ExceptionLogEntries
                                    where e.CreatedUtc >= fromDateUtc
                                    join b in context.ExceptionBuckets on e.ExceptionBuckedId equals b.Id
                                    orderby e.CreatedUtc descending
                                    select new ExceptionLogInfo
                                    {
                                        ExceptionBucketId = b.Id,
                                        LoggerName = b.LoggerName,
                                        ExceptionType = b.ExceptionType,
                                        StackTrace = b.StackTrace == "<no exception info>" ? b.LoggerName + ": " + b.ExceptionType : b.StackTrace,
                                        CreatedUtc = e.CreatedUtc.ToLocalTime(),
                                        Message = e.Message,
                                        OriginatorId = e.OriginatorId,
                                        ClientVersion = e.ClientVersion,
                                        OfficeVersion = e.OfficeVersion,
                                    }).Take(1000000).ToList();

            var en = _languageRepository.GetEnglishLanguage();

            var originatorIds = context.ExceptionLogEntries
                                       .Where(e => e.CreatedUtc >= fromDateUtc)
                                       .Select(e => e.OriginatorId)
                                       .Distinct();

            var users = context.Users
                               .Where(u => originatorIds.Any(id => u.Id == id))
                               .Select(
                                   u => new
                                   {
                                       id = u.Id,
                                       name = u.Login,
                                   })
                               .ToDictionary(u => u.id, u => u.name);

            var entities = context.OrgaEntities
                                  .Where(oE => originatorIds.Any(id => oE.Id == id))
                                  .ToDictionary(oE => oE.Id, oE => oE);

            // fill originator name
            foreach (var logInfoByOriginator in exceptionLogInfo.GroupBy(e => e.OriginatorId))
            {
                var originatorId = logInfoByOriginator.Key;
                string originator = GetOriginator(originatorId, users, entities, en);

                foreach (var logInfo in logInfoByOriginator)
                {
                    logInfo.Originator = originator;
                }
            }

            return exceptionLogInfo;
        }

        public async Task<IEnumerable<ExceptionBucketInfo>> GetExceptionLogInfoByBucket(DateTime? fromDateUtc)
        {
            await using var context = GetContext();

            // in this method data is only read, never written - so we make the context non-tracking
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            var en = _languageRepository.GetEnglishLanguage();

            var affectedUsers = await context.ExceptionLogEntries
                                             .Where(e => e.CreatedUtc >= fromDateUtc)
                                             .Select(e => new { e.ExceptionBuckedId, e.OriginatorId })
                                             .Distinct()
                                             .GroupBy(e => e.ExceptionBuckedId)
                                             .Select(g => new
                                             {
                                                 ExceptionBucketId = g.Key,
                                                 Count = g.Count(),
                                             })
                                             .ToDictionaryAsync(e => e.ExceptionBucketId, e => e.Count, new ByteArrayComparer());

            var occurrences = await context.ExceptionLogEntries
                                           .Where(e => e.CreatedUtc >= fromDateUtc)
                                           .GroupBy(e => e.ExceptionBuckedId)
                                           .Select(g => new
                                           {
                                               ExceptionBucketId = g.Key,
                                               Count = g.Count(),
                                           })
                                           .ToDictionaryAsync(e => e.ExceptionBucketId, e => e.Count, new ByteArrayComparer());

            var relevantBuckets = await context.ExceptionBuckets.Where(b => b.ModifiedUtc >= fromDateUtc && !b.IsIgnored).ToListAsync();
            var exceptionLogInfo = relevantBuckets
                                   .Select(b => new ExceptionBucketInfo
                                   {
                                       ExceptionBucketId = b.Id,
                                       LastMessage = b.LastMessage,
                                       StackTrace = b.StackTrace == "<no exception info>" ? b.LoggerName + ": " + b.ExceptionType : b.StackTrace,
                                       LastOccurred = b.ModifiedUtc.ToLocalTime(),
                                       Count = occurrences.GetValueOrDefault(b.Id),
                                       AffectedUsers = affectedUsers.GetValueOrDefault(b.Id),
                                   })
                                   .ToList();

            var entries = (await context.ExceptionLogEntries
                                        .Where(e => e.CreatedUtc >= fromDateUtc)
                                        .OrderByDescending(e => e.CreatedUtc)
                                        .Take(1000000)
                                        .Select(e => new ExceptionEntryInfo
                                        {
                                            BucketId = e.ExceptionBuckedId,
                                            OriginatorId = e.OriginatorId,
                                            Occurred = e.CreatedUtc.ToLocalTime(),
                                            ClientVersion = e.ClientVersion,
                                            OfficeVersion = e.OfficeVersion,
                                        })
                                        .ToListAsync())
                .ToLookup(e => e.BucketId, e => e, new ByteArrayComparer());

            foreach (var bucketInfo in exceptionLogInfo)
            {
                bucketInfo.Entries = entries[bucketInfo.ExceptionBucketId];
            }

            var originatorIds = context.ExceptionLogEntries
                                       .Where(e => e.CreatedUtc >= fromDateUtc)
                                       .Select(e => e.OriginatorId)
                                       .Distinct();

            var users = context.Users
                               .Where(u => originatorIds.Any(id => u.Id == id))
                               .Select(
                                   u => new
                                   {
                                       id = u.Id,
                                       name = u.Login,
                                   })
                               .ToDictionary(u => u.id, u => u.name);

            var entities = context.OrgaEntities
                                  .Where(oE => originatorIds.Any(id => oE.Id == id))
                                  .ToDictionary(oE => oE.Id, oE => oE);

            // fill originator name
            foreach (var logInfoByOriginator in exceptionLogInfo.SelectMany(b => b.Entries).GroupBy(e => e.OriginatorId))
            {
                var originatorId = logInfoByOriginator.Key;
                string originator = GetOriginator(originatorId, users, entities, en);

                foreach (var logInfo in logInfoByOriginator)
                {
                    logInfo.Originator = originator;
                }
            }

            return exceptionLogInfo.OrderByDescending(e => e.Count);
        }

        private string GetOriginator(Guid id, IDictionary<Guid, string> possibleUsers, IDictionary<Guid, OrgaEntity> possibleEntities, Language language)
        {
            if (possibleUsers.TryGetValue(id, out var login))
            {
                return login;
            }

            if (possibleEntities.TryGetValue(id, out var orgaEntity))
            {
                LoadTexts(orgaEntity, language, null);
                if (orgaEntity.Header != null)
                {
                    return orgaEntity.Header.Value;
                }
            }

            return "Unknown Creator: " + id;
        }

        private static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        [SuppressMessage("Sonar", "S4143:Collection elements should not be replaced unconditionally", Justification = "False positive")]
        internal void LoadAdLinkStatus(ChartSeries linkedSeries, ChartSeries unlinkedSeries, Guid dgId)
        {
            using (var context = GetContext())
            {
                Author[] authors;
                int office, officeUnlinked, phone, phoneUnlinked, address, addressUnlinked;
                if (dgId == Guid.Empty)
                {
                    authors = context.Authors.Where(GetAdLinkFilter()).ToArray();
                    CountWorkplaceProperties(context, authors, out office, out officeUnlinked, out phone, out phoneUnlinked, out address, out addressUnlinked);
                }
                else
                {
                    var subEntityIds = GetSubEntitiyIds(context, dgId);
                    authors = context.Authors.Where(GetAdLinkFilter(subEntityIds)).ToArray();
                    CountWorkplaceProperties(context, authors, out office, out officeUnlinked, out phone, out phoneUnlinked, out address, out addressUnlinked);
                }

                linkedSeries.Data[0] = authors.Count(a => a.Email == a.AdEmail);
                linkedSeries.Data[1] = authors.Count(a => a.Service == a.AdService);
                linkedSeries.Data[2] = address;
                linkedSeries.Data[3] = office;
                linkedSeries.Data[4] = phone;
                unlinkedSeries.Data[0] = authors.Count(a => a.Email != a.AdEmail);
                unlinkedSeries.Data[1] = authors.Count(a => a.Service != a.AdService);
                unlinkedSeries.Data[2] = addressUnlinked;
                unlinkedSeries.Data[3] = officeUnlinked;
                unlinkedSeries.Data[4] = phoneUnlinked;
            }
        }

        private Func<Author, bool> GetAdLinkFilter(Guid[] subEntityIds = null)
        {
            if (subEntityIds != null)
            {
                return a => a.MainWorkplaceId != null && a.OrgaEntityId != null && subEntityIds.Contains(a.OrgaEntityId.Value);
            }

            return a => a.MainWorkplaceId != null;
        }

        private static void CountWorkplaceProperties(
            EurolookContext context,
            [JetBrains.Annotations.NotNull] Author[] authors,
            out int office,
            out int officeUnlinked,
            out int phone,
            out int phoneUnlinked,
            out int address,
            out int addressUnlinked)
        {
            if (authors == null)
            {
                throw new ArgumentNullException(nameof(authors));
            }

            int officeResult = 0, officeUnlinkedResult = 0, phoneResult = 0, phoneUnlinkedResult = 0, addressResult = 0, addressUnlinkedResult = 0;
            foreach (var author in authors)
            {
                var workplace = context.Workplaces.Include(w => w.Address).First(w => w.Id == author.MainWorkplaceId);
                if (workplace.Office == author.AdOffice)
                {
                    officeResult++;
                }
                else
                {
                    officeUnlinkedResult++;
                }

                if (workplace.PhoneExtension == author.AdPhoneExtension)
                {
                    phoneResult++;
                }
                else
                {
                    phoneUnlinkedResult++;
                }

                if (workplace.Address.ActiveDirectoryReference == author.AdAddress)
                {
                    addressResult++;
                }
                else
                {
                    addressUnlinkedResult++;
                }
            }

            office = officeResult;
            officeUnlinked = officeUnlinkedResult;
            phone = phoneResult;
            phoneUnlinked = phoneUnlinkedResult;
            address = addressResult;
            addressUnlinked = addressUnlinkedResult;
        }

        private Guid[] GetSubEntitiyIds(EurolookContext context, Guid dgId)
        {
            var result = new List<Guid> { dgId };
            foreach (var dirId in context.OrgaEntities.Where(o => !o.Deleted && o.SuperEntityId == dgId).Select(o => o.Id).ToArray())
            {
                result.Add(dirId);
                result.AddRange(context.OrgaEntities.Where(o => !o.Deleted && o.SuperEntityId == dirId).Select(o => o.Id));
            }

            return result.ToArray();
        }

        internal int GetDeviceCount(DateTime? fromDateUtc, string filter)
        {
            using (var context = GetContext())
            {
                var filteringRegex = new Regex(filter);

                return fromDateUtc != null
                    ? context.DeviceSettings
                             .Where(ds => !ds.Deleted && ds.LastLogin >= fromDateUtc)
                             .Select(ds => ds.DeviceName)
                             .AsEnumerable()
                             .Count(name => filteringRegex.IsMatch(name))
                    : context.DeviceSettings
                             .Where(ds => !ds.Deleted)
                             .Select(ds => ds.DeviceName)
                             .AsEnumerable()
                             .Count(name => filteringRegex.IsMatch(name));
            }
        }

        internal async Task<DefaultBricksData> GetDefaultBricksChartData(DateTime? fromDateUtc)
        {
            var addedData = new Dictionary<Guid, int>();
            var removedData = new Dictionary<Guid, int>();
            var modifiedData = new Dictionary<string, int>();
            using (var context = GetContext())
            {
                foreach (var user in await context.Users.Where(u => !u.Deleted)
                                                  .Include(u => u.Settings).ThenInclude(x => x.DeviceSettings)
                                                  .Include(u => u.Settings).ThenInclude(x => x.DocumentModelSettings).ThenInclude(x => x.DocumentModel)
                                                  .ToArrayAsync())
                {
                    if (!user.Settings.DeviceSettings.Any(d => d.LastLogin > fromDateUtc))
                    {
                        continue;
                    }

                    foreach (var documentModelSettings in user.Settings.DocumentModelSettings
                                                              .Where(x => !x.Deleted && x.UserBrickChoices != null))
                    {
                        if (!documentModelSettings.UserBrickChoices.StartsWith("[v2]"))
                        {
                            continue;
                        }

                        IncreaseValue(modifiedData, documentModelSettings.DocumentModel.Name);

                        foreach (var choice in documentModelSettings.UserBrickChoices.Replace("[v2]", "").Split(';'))
                        {
                            var keyValue = choice.Split(':');
                            if (keyValue.Length != 2)
                            {
                                continue;
                            }

                            if (int.TryParse(keyValue[1], out int value) && value == 1)
                            {
                                IncreaseValue(addedData, Guid.Parse(keyValue[0]));
                            }
                            else if (value == 0)
                            {
                                IncreaseValue(removedData, Guid.Parse(keyValue[0]));
                            }
                        }
                    }
                }

                return new DefaultBricksData
                {
                    DefaultBricksModifications = CreateDefaultBricksChart(modifiedData),
                    AddedDefaultBricks = await CreateAddedDefaultBricksChart(addedData, context),
                    RemovedDefaultBricks = await CreateRemovedDefaultBricksChart(removedData, context),
                };
            }
        }

        private static ChartData CreateDefaultBricksChart(Dictionary<string, int> data)
        {
            var result = new ChartData { Title = "Number of users who modified the defaults" };
            var filteredData = data.OrderByDescending(x => x.Value).ThenBy(x => x.Key).Take(15).ToArray();
            var countSeries = new ChartSeries
            {
                Name = result.Title,
                Data = new int[filteredData.Length],
            };
            result.Series.Add(countSeries);
            for (var i = 0; i < filteredData.Length; i++)
            {
                var keyValue = filteredData[i];
                result.Categories.Add(keyValue.Key);
                countSeries.Data[i] = keyValue.Value;
            }

            return result;
        }

        private async Task<ChartData> CreateAddedDefaultBricksChart(Dictionary<Guid, int> data, EurolookContext context)
        {
            var result = new ChartData { Title = "Times this brick was added to the defaults" };
            var filteredData = data.OrderByDescending(x => x.Value).ThenBy(x => x.Key).Take(15).ToArray();
            var countSeries = new ChartSeries
            {
                Name = "Count",
                Data = new int[filteredData.Length],
            };
            result.Series.Add(countSeries);
            for (var i = 0; i < filteredData.Length; i++)
            {
                var keyValue = filteredData[i];
                result.Categories.Add(await context.Bricks.Where(y => y.Id == keyValue.Key).Select(y => y.Name).FirstOrDefaultAsync());
                countSeries.Data[i] = keyValue.Value;
            }

            return result;
        }

        private async Task<ChartData> CreateRemovedDefaultBricksChart(Dictionary<Guid, int> data, EurolookContext context)
        {
            var result = new ChartData { Title = "Times this brick was removed from the defaults" };
            var filteredData = data.OrderByDescending(x => x.Value).ThenBy(x => x.Key).Take(15).ToArray();
            var countSeries = new ChartSeries
            {
                Name = "Count",
                Data = new int[filteredData.Length],
            };
            result.Series.Add(countSeries);
            for (var i = 0; i < filteredData.Length; i++)
            {
                var keyValue = filteredData[i];
                result.Categories.Add(await context.Bricks.Where(y => y.Id == keyValue.Key).Select(y => y.Name).FirstOrDefaultAsync());
                countSeries.Data[i] = keyValue.Value;
            }

            return result;
        }

        private static void IncreaseValue<T>(Dictionary<T, int> dictionary, T key)
        {
            if (!dictionary.ContainsKey(key))
            {
                dictionary.Add(key, 0);
            }

            dictionary[key] += 1;
        }

        internal int GetInitialisedCount(DateTime? fromDateUtc, string filter)
        {
            using (var context = GetContext())
            {
                var filteringRegex = new Regex(filter);
                return fromDateUtc != null
                    ? context.DeviceSettings
                             .Where(ds => !ds.Deleted && ds.IsInitialised && ds.LastLogin >= fromDateUtc)
                             .Select(ds => ds.DeviceName)
                             .AsEnumerable()
                             .Count(name => filteringRegex.IsMatch(name))
                    : context.DeviceSettings
                             .Where(ds => !ds.Deleted && ds.IsInitialised)
                             .Select(ds => ds.DeviceName)
                             .AsEnumerable()
                             .Count(name => filteringRegex.IsMatch(name));
            }
        }

        internal int[] GetDeviceCount(DateTime[] dates)
        {
            var result = new int[dates.Length];
            using (var context = GetContext())
            {
                for (int i = 0; i < dates.Length; i++)
                {
                    var date = dates[i];
                    var tracks = context.ActivityTracks.Where(at => at.DateUtc == date).ToArray();
                    result[i] = tracks.Length > 0 ? tracks.Sum(at => at.DeviceCount) : 0;
                }
            }

            return result;
        }

        internal int[] GetSessionCounts(DateTime[] dates)
        {
            var result = new int[dates.Length];
            using (var context = GetContext())
            {
                for (int i = 0; i < dates.Length; i++)
                {
                    var date = dates[i];
                    var tracks = context.ActivityTracks.Where(at => at.DateUtc == date).ToArray();
                    result[i] = tracks.Length > 0 ? tracks.Sum(at => at.SessionCount) : 0;
                }
            }

            return result;
        }

        public int GetCustomBricksUsageCount(DateTime? fromDateUtc)
        {
            using (var context = GetContext())
            {
                int count = 0;
                foreach (var user in context.Users
                                            .Where(u => !u.Deleted)
                                            .Include(u => u.Settings).ThenInclude(x => x.DeviceSettings)
                                            .Include(u => u.Bricks))
                {
                    if (user.Settings.DeviceSettings.Any(d => d.LastLogin > fromDateUtc) && user.Bricks.Count > 0)
                    {
                        count++;
                    }
                }

                return count;
            }
        }

        public Dictionary<int, int> GetCustomBricksCountPerUser(DateTime? fromDateUtc)
        {
            var result = new Dictionary<int, int>();
            using (var context = GetContext())
            {
                foreach (var user in context.Users
                                            .Where(u => !u.Deleted)
                                            .Include(u => u.Settings).ThenInclude(x => x.DeviceSettings)
                                            .Include(u => u.Bricks))
                {
                    if (user.Settings.DeviceSettings.Any(d => d.LastLogin > fromDateUtc))
                    {
                        var key = user.Bricks.Count(x => !x.Deleted);
                        if (!result.ContainsKey(key))
                        {
                            result.Add(key, 0);
                        }

                        result[key] += 1;
                    }
                }
            }

            return result;
        }

        internal Dictionary<int, int> GetCustomBricksSizePerUser(DateTime? fromDateUtc)
        {
            var result = new Dictionary<int, int>();
            using (var context = GetContext())
            {
                foreach (var user in context.Users
                                            .Where(u => !u.Deleted)
                                            .Include(u => u.Settings).ThenInclude(x => x.DeviceSettings)
                                            .Include(u => u.Bricks)
                                            .ToArray())
                {
                    if (user.Settings.DeviceSettings.Any(d => d.LastLogin > fromDateUtc))
                    {
                        foreach (var brick in user.Bricks.Where(x => !x.Deleted))
                        {
                            var brickIdParam = new SqlParameter("brickId", brick.BrickId);
                            var lengthParam = new SqlParameter("length", SqlDbType.Int)
                            {
                                Direction = ParameterDirection.Output,
                            };

                            context.Database.ExecuteSqlRaw(
                                "SELECT @length=DATALENGTH(BuildingBlockBytes) FROM Bricks WHERE Id=@brickId;",
                                lengthParam,
                                brickIdParam);

                            if (lengthParam.Value is int byteLength)
                            {
                                double kiloBytes = byteLength / 1024.0;
                                int key = (int)Math.Round(kiloBytes, 0);
                                if (!result.ContainsKey(key))
                                {
                                    result[key] = 0;
                                }

                                result[key] += 1;
                            }
                        }
                    }
                }
            }

            return result;
        }

        internal static async Task<IEnumerable<Guid>> FindOriginatorGuids(EurolookContext context, List<string> userFilter)
        {
            if (userFilter.IsNullOrEmpty())
            {
                return await context.Users.Select(user => user.Id).ToListAsync();
            }

            return (await context.Users.ToListAsync())
                   .Where(
                       user => userFilter.Any(
                           login => string.Equals(
                               login,
                               user.Login,
                               StringComparison.InvariantCultureIgnoreCase)))
                   .Select(user => user.Id);
        }

        internal static async Task<IEnumerable<ExceptionBucket>> FilterExceptionBucketsByUser(EurolookContext context, List<string> userFilter, DateTime? fromDateUtc)
        {
            var result = new List<ExceptionBucket>();
            var userIds = (await context.Users.ToListAsync())
                          .Where(
                              user => userFilter.Any(
                                  login => string.Equals(
                                      login,
                                      user.Login,
                                      StringComparison.InvariantCultureIgnoreCase)))
                          .Select(user => user.Id);
            foreach (var bucket in await context.ExceptionBuckets.Where(b => b.ModifiedUtc >= fromDateUtc).ToArrayAsync())
            {
                var bucketEntries = await context.ExceptionLogEntries
                                                 .Where(log => log.ExceptionBuckedId == bucket.Id && log.CreatedUtc >= fromDateUtc).ToArrayAsync();
                if (userFilter.IsNullOrEmpty() || bucketEntries.Any(log => userIds.Contains(log.OriginatorId)))
                {
                    result.Add(bucket);
                }
            }

            return result;
        }
    }
}
