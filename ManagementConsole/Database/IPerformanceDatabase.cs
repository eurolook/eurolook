﻿using System;
using System.Collections.Generic;
using Eurolook.ManagementConsole.Models;

namespace Eurolook.ManagementConsole.Database
{
    public interface IPerformanceDatabase
    {
        List<Version> GetVersionsWithServerLogs(int maximum);

        List<Version> GetVersionsWithClientLogs(int maximum);

        PerformanceLogInfoByDate GetServerPerformanceLogInfosByDate(DateTime? fromDate, bool daily = false);

        PerformanceLogInfoByDate GetClientPerformanceLogInfosByDate(DateTime? fromDate, bool daily = false);
    }
}