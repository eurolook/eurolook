using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Models;

namespace Eurolook.ManagementConsole.Database
{
    public interface IPredefinedFunctionsDatabase
    {
        IEnumerable<PredefinedFunction> GetPredefinedFunctions(
            DataPage<PredefinedFunction> page,
            string searchTerm,
            bool deleted);

        int GetPredefinedFunctionCount(string searchTerm, bool deleted);

        Task<PredefinedFunctionEditDataModel> GetPredefinedFunctionForEdit(Guid id);

        Task UpdatePredefinedFunction(Guid id, PredefinedFunctionEditDataModel model);

        Task DeletePredefinedFunction(Guid id, bool deleted);

        Task<string> CreateFemaleFunction(Guid id);

        Task<PredefinedFunction> CreatePredefinedFunction(PredefinedFunctionDataModel dataModel);

        Task<string> CreateHeaderFunction(Guid id);

        Task<string> CreateFemaleHeaderFunction(Guid id);
    }
}