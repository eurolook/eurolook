﻿using System;
using System.Threading.Tasks;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Models;
using Microsoft.AspNetCore.Http;

namespace Eurolook.ManagementConsole.Database
{
    public interface INotificationsDatabase
    {
        Notification[] GetNotifications(DataPage<Notification> page, string searchTerm, bool deleted);

        int GetNotificationsCount(string query, bool deleted);

        Task<Notification> CreateNotification(NotificationsDataModel viewModel);

        Task DeleteNotification(Guid id, bool deleted);

        Task RestoreNotification(Guid id, bool deleted);

        NotificationsEditDataModel GetNotificationsForEdit(Guid id);

        void UpdateNotification(Guid id, NotificationsEditDataModel model);

        Task<Notification[]> GetAllNotificationsAsync();

        Notification GetNotification(Guid id);

        Task<byte[]> UpdateThumbnail(Guid notificationId, IFormFile file);
    }
}