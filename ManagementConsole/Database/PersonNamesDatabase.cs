﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Eurolook.Data;
using Eurolook.Data.Extensions;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Models;
using Microsoft.Extensions.Configuration;

namespace Eurolook.ManagementConsole.Database
{
    public class PersonNamesDatabase : EurolookManagementConsoleDatabase, IPersonNamesDatabase
    {
        public PersonNamesDatabase(IConfiguration configuration)
            : base(configuration)
        {
        }

        public IEnumerable<PersonName> GetPersonNames(DataPage<PersonName> page, string type, string searchText, bool deleted)
        {
            using (var context = GetContext())
            {
                var query =
                    context.PersonNames.Where(
                        pn => (!deleted && !pn.Deleted && (pn.Category == type || type == "All")) || (deleted && pn.Deleted && (pn.Category == type || type == "All")));

                if (!string.IsNullOrWhiteSpace(searchText))
                {
                    query = query.Where(p => p.LastName.Contains(searchText) || p.FirstName.Contains(searchText));
                }

                return query.OrderByAndPaginate(page)
                            .ToArray();
            }
        }

        public void DeletePersonNames(IEnumerable<PersonName> personNames)
        {
            bool modified = false;
            using (var context = GetContext())
            {
                foreach (var p in personNames)
                {
                    var dbPersonName = GetPersonNameForEdit(p.Id, context);
                    if (dbPersonName != null)
                    {
                        dbPersonName.SetDeletedFlag();
                        modified = true;
                    }
                }

                if (modified)
                {
                    context.SaveChanges();
                }
            }
        }

        public int Count(string type, string searchText)
        {
            using (var context = GetContext())
            {
                var personNames =
                    context.PersonNames.Where(
                        pn => (!pn.Deleted && (pn.Category == type || type == "All")) || (type == "Deleted" && pn.Deleted));

                int result = !string.IsNullOrEmpty(searchText)
                    ? personNames.Count(pn => pn.LastName.Contains(searchText))
                    : personNames.Count();

                return result;
            }
        }

        public void UpdatePersonNames(IEnumerable<PersonName> personNames)
        {
            bool modified = false;
            using (var context = GetContext())
            {
                foreach (var p in personNames)
                {
                    var dbPersonName = GetPersonNameForEdit(p.Id, context);
                    modified |= UpdatePersonName(p, dbPersonName);
                }

                if (modified)
                {
                    context.SaveChanges();
                }
            }
        }

        public PersonName CreatePersonName(PersonName model)
        {
            using (var context = GetContext())
            {
                if (
                    context.PersonNames.Any(
                        pn => !pn.Deleted && pn.Category == model.Category && pn.FirstName == model.FirstName
                              && pn.LastName == model.LastName && pn.Country == model.Country))
                {
                    throw new DuplicateNameException("Person name entry already exists");
                }

                var newPerson = new PersonName
                {
                    Category = model.Category,
                    PoliticalGroup = model.PoliticalGroup,
                    LastName = model.LastName,
                    FirstName = model.FirstName,
                    Country = model.Country,
                };
                newPerson.Init(); // set id, modification time and deleted
                context.PersonNames.Add(newPerson);
                context.SaveChanges();
                return newPerson;
            }
        }

        public string GetChanges(IEnumerable<PersonName> newPersonNames, string category)
        {
            var deleted = new StringBuilder("DELETED NAMES\n");
            var modified = new StringBuilder("MODIFIED NAMES\n");
            var added = new StringBuilder("ADDED/RECOVERED NAMES\n");
            var unmodified = new StringBuilder("UNMODIFIED NAMES\n");
            int deletedCount = 0;
            int modifiedCount = 0;
            int addedCount = 0;
            int unmodifiedCount = 0;

            using (var context = GetContext())
            {
                var modifiedPersonNames = context.PersonNames.Where(pn => !pn.Deleted && pn.Category == category).ToList();

                // skip recovered
                modifiedPersonNames.ForEach(pn => pn.Deleted = true);

                foreach (var newPersonName in newPersonNames)
                {
                    var modifiedPerson = MatchPersonName(newPersonName, modifiedPersonNames);
                    if (modifiedPerson != null)
                    {
                        if (AreEqual(newPersonName, modifiedPerson))
                        {
                            unmodified.AppendLine($"\t{modifiedPerson.InsertName}");
                            unmodifiedCount++;
                        }
                        else
                        {
                            modified.AppendLine($"\t{modifiedPerson.InsertName,-50}>>>   {newPersonName.InsertName}");
                            modifiedCount++;
                        }

                        modifiedPersonNames.Remove(modifiedPerson);
                    }
                    else
                    {
                        // new person names (or recovered because deleted was not checked)
                        added.AppendLine($"\t{newPersonName.InsertName}");
                        addedCount++;
                    }
                }

                foreach (var deletedPersonName in modifiedPersonNames)
                {
                    deleted.AppendLine($"\t{deletedPersonName.InsertName}");
                }

                deletedCount = modifiedPersonNames.Count;
            }

            deleted.Insert(0, $"{deletedCount} ");
            added.Insert(0, $"{addedCount} ");
            modified.Insert(0, $"{modifiedCount} ");
            unmodified.Insert(0, $"{unmodifiedCount} ");

            added.AppendLine();
            added.AppendLine(deleted.ToString());
            added.AppendLine();
            added.AppendLine(modified.ToString());
            added.AppendLine();
            added.AppendLine(unmodified.ToString());

            return added.ToString();
        }

        public string ApplyChanges(IEnumerable<PersonName> newPersonNames, string categoryName)
        {
            using (var context = GetContext())
            {
                var categoryPersonList = context.PersonNames.Where(pn => pn.Category == categoryName);
                var activePersons = categoryPersonList.Where(pn => !pn.Deleted);
                var inactivePersons = categoryPersonList.Where(pn => pn.Deleted);

                activePersons.ToList().ForEach(pn => pn.Deleted = true); // deleted person name

                var lookUpActivePersons = activePersons.ToLookup(pn => pn.FullName + ":" + pn.Country);
                var lookUpInactivePersons = inactivePersons.ToLookup(pn => pn.FullName + ":" + pn.Country);
                foreach (var newPersonName in newPersonNames)
                {
                    var modifiedPerson = lookUpActivePersons[newPersonName.FullName + ":" + newPersonName.Country].FirstOrDefault();

                    if (modifiedPerson != null)
                    {
                        // modified person name
                        if (modifiedPerson.PoliticalGroup != newPersonName.PoliticalGroup)
                        {
                            modifiedPerson.PoliticalGroup = newPersonName.PoliticalGroup;
                            modifiedPerson.SetDeletedFlag(false);
                        }
                        else
                        {
                            // unmodified person name
                            modifiedPerson.Deleted = false;
                        }
                    }
                    else
                    {
                        var addedPerson = lookUpInactivePersons[newPersonName.FullName + ":" + newPersonName.Country].FirstOrDefault();
                        if (addedPerson != null)
                        {
                            // recovered person name
                            addedPerson.PoliticalGroup = newPersonName.PoliticalGroup;
                            addedPerson.SetDeletedFlag(false);
                        }
                        else
                        {
                            // new person name
                            var addPerson = new PersonName
                            {
                                Category = newPersonName.Category,
                                PoliticalGroup = newPersonName.PoliticalGroup,
                                LastName = newPersonName.LastName,
                                FirstName = newPersonName.FirstName,
                                Country = newPersonName.Country,
                            };
                            addPerson.Init(); // set id, modification time and deleted
                            context.PersonNames.Add(addPerson);
                        }
                    }
                }

                context.SaveChanges();
            }

            return "Members of Parliament List successfully updated!";
        }

        private static bool AreEqual(PersonName source, PersonName target)
        {
            return source.FirstName == target.FirstName
                && source.LastName == target.LastName
                && source.Country == target.Country
                && source.PoliticalGroup == target.PoliticalGroup
                && source.Category == target.Category;
        }

        private static bool UpdatePersonName(PersonName source, PersonName target)
        {
            if (AreEqual(source, target))
            {
                return false;
            }

            target.FirstName = source.FirstName;
            target.LastName = source.LastName;
            target.Country = source.Country;
            target.PoliticalGroup = source.PoliticalGroup;
            target.Category = source.Category;
            target.UpdateServerModificationTime();
            return true;
        }

        private PersonName GetPersonNameForEdit(Guid id, EurolookServerContext context)
        {
            return context.PersonNames.FirstOrDefault(pn => pn.Id == id);
        }

        private PersonName GetPersonNameForEdit(PersonName p, EurolookServerContext context)
        {
            return
                context.PersonNames.FirstOrDefault(
                    pn => pn.FirstName == p.FirstName && pn.LastName == p.LastName && pn.Country == p.Country);
        }

        private PersonName MatchPersonName(PersonName p, List<PersonName> dbPersonNames)
        {
            return dbPersonNames.FirstOrDefault(pn => (pn.FullName == p.FullName && pn.Country == p.Country) || pn.Id == p.Id);
        }
    }
}
