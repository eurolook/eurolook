﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Data;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.ViewModels.OrgaChart;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Eurolook.ManagementConsole.Database
{
    public class OrgaChartDatabase : AdminDatabase, IOrgaChartDatabase
    {
        private readonly ILanguageRepository _languageRepository;
        private readonly IOrgaEntityRepository _orgaEntityRepository;

        public OrgaChartDatabase(
            IConfiguration configuration,
            ILanguageRepository languageRepository,
            IOrgaEntityRepository orgaEntityRepository)
            : base(configuration)
        {
            _languageRepository = languageRepository;
            _orgaEntityRepository = orgaEntityRepository;
        }

        public OrgaEntity GetOrgaTreeForExcelExport(Guid dgId)
        {
            using (var context = GetContext())
            {
                return GetOrgaTreeForExcelExportRecursive(context, dgId);
            }
        }

        private OrgaEntity GetOrgaTreeForExcelExportRecursive(EurolookServerContext context, Guid id)
        {
            var orgaEntity = context.OrgaEntities.Where(o => o.Id == id).
                                     Include(o => o.SubEntities).
                                     Include(o => o.PrimaryAddress).
                                     Include(o => o.SecondaryAddress).
                                     First();
            orgaEntity.SubEntities.RemoveAll(s => s.Deleted);

            // load children
            foreach (var subEntity in orgaEntity.SubEntities.ToArray())
            {
                var fullyLoadedSubEntity = GetOrgaTreeForExcelExportRecursive(context, subEntity.Id);
                orgaEntity.SubEntities.Remove(subEntity);
                orgaEntity.SubEntities.Add(fullyLoadedSubEntity);
            }

            return orgaEntity;
        }

        public JsTreeNode GetOrgaTreeForView(Guid dgId, Guid languageId, int authorCount = 0)
        {
            using (var context = GetContext())
            {
                return GetOrgaTreeForViewRecursive(context, dgId, languageId, authorCount);
            }
        }

        public OrgaEntity GetRoot()
        {
            using (var context = GetContext())
            {
                return context.OrgaEntities.FirstOrDefault(o => o.LogicalLevel == 0);
            }
        }

        private JsTreeNode GetOrgaTreeForViewRecursive(EurolookServerContext context, Guid id, Guid? languageId, int authorCount)
        {
            var orgaEntity = context.OrgaEntities.Where(o => o.Id == id).
                Include(o => o.SubEntities).
                Include(o => o.PrimaryAddress).
                Include(o => o.SecondaryAddress).
                First();
            orgaEntity.SubEntities.RemoveAll(s => s.Deleted);

            if (languageId != null)
            {
                LoadTexts(context, languageId.Value, orgaEntity, null, false);
            }

            var node = new JsTreeNode(orgaEntity);

            // authors
            if (authorCount > 0)
            {
                node.Children.AddRange(GetAuthorsNodes(orgaEntity.Id, authorCount));
            }

            // load children
            foreach (var subEntity in orgaEntity.SubEntities.OrderBy(o => o.OrderIndex))
            {
                var child = GetOrgaTreeForViewRecursive(context, subEntity.Id, languageId, authorCount);
                node.Children.Add(child);
            }

            return node;
        }

        private List<OrgaEntity> GetFlatTree(EurolookServerContext context, Guid orgaEntityId)
        {
            var result = new List<OrgaEntity> { context.OrgaEntities.Include(o => o.HeaderText).First(o => o.Id == orgaEntityId) };
            foreach (var childId in context.OrgaEntities.Where(o => o.SuperEntityId == orgaEntityId && !o.Deleted).Select(o => o.Id).ToArray())
            {
                result.AddRange(GetFlatTree(context, childId));
            }

            return result;
        }

        public List<JsTreeNode> GetAuthorsNodes(Guid orgaEntityId, int? authorCount)
        {
            var result = new List<JsTreeNode>();
            using (var context = GetContext())
            {
                var allAuthors = context.Authors.Where(a => a.OrgaEntityId == orgaEntityId && !a.Deleted).ToList();
                allAuthors.AddRange(context.JobAssignments.Where(j => !j.Deleted && j.OrgaEntityId == orgaEntityId).Select(j => j.Author));

                int takeCount = allAuthors.Count;
                if (authorCount != null)
                {
                    takeCount = authorCount.Value;
                    if (allAuthors.Count == authorCount.Value + 1)
                    {
                        takeCount += 1;
                    }
                }

                result.AddRange(allAuthors.OrderBy(a => a.LatinLastName).Take(takeCount).Select(author => new JsTreeNode(author)));
                if (allAuthors.Count > takeCount)
                {
                    result.Add(new JsTreeNode
                    {
                        Id = Guid.NewGuid().ToString(),
                        DisplayName = $"... ({allAuthors.Count - takeCount} more, click to show)",
                        JsFunc = $"ko.viewModel.revealAuthors('{orgaEntityId}');",
                        ToolTip = "Click to reveal all authors.",
                        LogicalLevel = 4,
                    });
                }
            }

            return result;
        }

        public OrgaEntity GetOrgaEntityForImport(Guid id)
        {
            using (var context = GetContext())
            {
                return context.OrgaEntities.Include(o => o.HeaderText).FirstOrDefault(o => o.Id == id);
            }
        }

        public int DeleteOrgaChart(Guid rootId)
        {
            IEnumerable<OrgaEntity> tree;
            using (var context = GetContext())
            {
                tree = GetFlatTree(context, rootId);
            }

            var detachedAuthors = new List<string>();
            foreach (var orgaEntity in tree)
            {
                detachedAuthors.AddRange(_orgaEntityRepository.DeleteOrgaEntity(orgaEntity));
            }

            return detachedAuthors.Distinct().Count();
        }

        public List<OrgaEntity> GetOrphanedOrgaEntities([NotNull] List<OrgaEntity> orgaEntities)
        {
            var result = new List<OrgaEntity>();
            var dg = orgaEntities.FirstOrDefault(o => o.LogicalLevel == 1);
            if (dg == null)
            {
                return result;
            }

            using (var context = GetContext())
            {
                var dbOrgaEntities = context.OrgaEntities
                                            .Where(o => !o.Deleted && (o.SuperEntity.Id == dg.Id || o.SuperEntity.SuperEntity.Id == dg.Id))
                                            .OrderBy(x => x.OrderIndex).ToArray();
                foreach (var dbEntity in dbOrgaEntities)
                {
                    if (!orgaEntities.Exists(o => o.Id == dbEntity.Id))
                    {
                        result.Add(dbEntity);
                    }
                }

                return result;
            }
        }

        public OrgaEntity CreateDg(string acronym, Guid primaryAddressId, Guid? secondaryAddressId)
        {
            var en = _languageRepository.GetEnglishLanguage();
            var rootNode = GetRoot();
            using (var context = GetContext())
            {
                var headerText = new Text();
                headerText.Init();
                headerText.Alias = $"{acronym}_Header_{DateTime.Now.Ticks}";
                context.Texts.Add(headerText);

                var headerTranslation = new Translation();
                headerTranslation.Init();
                headerTranslation.TextId = headerText.Id;
                headerTranslation.LanguageId = en.Id;
                headerTranslation.Value = acronym;
                context.Translations.Add(headerTranslation);

                var dg = new OrgaEntity();
                dg.Init();
                dg.SuperEntityId = rootNode.Id;
                dg.LogicalLevel = 1;
                dg.Name = acronym;
                dg.PrimaryAddressId = primaryAddressId;
                dg.ShowInHeader = true;
                if (secondaryAddressId != null)
                {
                    dg.SecondaryAddressId = secondaryAddressId.Value;
                }

                dg.HeaderTextId = headerText.Id;
                context.OrgaEntities.Add(dg);

                context.SaveChanges();
                return dg;
            }
        }

        public Text[] GetTexts(Guid?[] textIds)
        {
            using (var context = GetContext())
            {
                return context.Texts.Where(t => textIds.Contains(t.Id)).ToArray();
            }
        }

        public Translation[] GetTranslations(Guid?[] textIds)
        {
            using (var context = GetContext())
            {
                return context.Translations.Where(t => textIds.Contains(t.TextId)).ToArray();
            }
        }
    }
}
