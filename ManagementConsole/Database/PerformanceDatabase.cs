﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Data;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Models;
using Microsoft.Extensions.Configuration;

namespace Eurolook.ManagementConsole.Database
{
    public class PerformanceDatabase : EurolookManagementConsoleDatabase, IPerformanceDatabase
    {
        public PerformanceDatabase(IConfiguration configuration)
            : base(configuration)
        {
        }

        public List<Version> GetVersionsWithServerLogs(int maximum)
        {
            using var context = GetContext();
            var result = GetServerPerformanceLogs(context)
                         .Select(log => log.Version)
                         .Distinct()
                         .AsEnumerable()
                         .Select(s => Version.TryParse(s, out var version) ? version : new Version())
                         .OrderByDescending(v => v)
                         .Take(maximum)
                         .ToList();

            result.Reverse();

            return result;
        }

        public List<Version> GetVersionsWithClientLogs(int maximum)
        {
            using var context = GetContext();
            var result = GetClientPerformanceLogs(context)
                         .Select(log => log.Version)
                         .Distinct()
                         .AsEnumerable()
                         .Select(s => Version.TryParse(s, out var version) ? version : new Version())
                         .OrderByDescending(v => v)
                         .Take(maximum)
                         .ToList();

            result.Reverse();

            return result;
        }

        public PerformanceLogInfoByDate GetServerPerformanceLogInfosByDate(DateTime? fromDate, bool daily = false)
        {
            using var context = GetContext();
            var date = fromDate ?? GetMinimumDate(context);
            var infos = new PerformanceLogInfoByDate(date, daily);
            foreach (var logEntry in GetServerPerformanceLogs(context).Where(p => p.Date >= date))
            {
                if (!logEntry.Date.HasValue)
                {
                    continue;
                }

                infos.AddEntry(Version.TryParse(logEntry.Version, out var version) ? version : new Version(), logEntry.Event, logEntry.Date.Value, logEntry.Average);
            }

            return infos;
        }

        public PerformanceLogInfoByDate GetClientPerformanceLogInfosByDate(DateTime? fromDate, bool daily = false)
        {
            using var context = GetContext();
            var date = fromDate ?? GetMinimumDate(context);
            var infos = new PerformanceLogInfoByDate(date, daily);
            foreach (var logEntry in GetClientPerformanceLogs(context).Where(p => p.Date >= date))
            {
                if (!logEntry.Date.HasValue)
                {
                    continue;
                }

                infos.AddEntry(Version.TryParse(logEntry.Version, out var version) ? version : new Version(), logEntry.Event, logEntry.Date.Value, logEntry.Average);
            }

            return infos;
        }

        private static IQueryable<PerformanceLog> GetServerPerformanceLogs(EurolookContext context)
        {
            return context.PerformanceLogs.Where(
                log => log.Event == nameof(PerformanceLogEvent.ServerGetUpdates)
                       || log.Event == nameof(PerformanceLogEvent.ServerGetUserProfile)
                       || log.Event == nameof(PerformanceLogEvent.ServerGetInitialization));
        }

        private static IQueryable<PerformanceLog> GetClientPerformanceLogs(EurolookContext context)
        {
            return context.PerformanceLogs.Where(
                log => log.Event == nameof(PerformanceLogEvent.AddInLoaded)
                       || log.Event == nameof(PerformanceLogEvent.CreationDialogLoaded)
                       || log.Event == nameof(PerformanceLogEvent.DataSyncFinished)
                       || log.Event == nameof(PerformanceLogEvent.DocumentCreated));
        }

        private static DateTime GetMinimumDate(EurolookContext context)
        {
            return context.PerformanceLogs.Where(log => log.Date.HasValue)
                          .Select(log => log.Date.Value)
                          .OrderBy(date => date)
                          .FirstOrDefault();
        }
    }
}
