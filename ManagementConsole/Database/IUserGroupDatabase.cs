using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Models;

namespace Eurolook.ManagementConsole.Database
{
    public interface IUserGroupDatabase
    {
        Task<IEnumerable<UserGroup>> GetUserGroupsAsync(
            DataPage<UserGroup> page,
            string searchTerm,
            bool deleted);

        Task<uint> GetUserGroupCountAsync(string searchTerm, bool deleted);

        Task<UserGroup> CreateUserGroupAsync(string name);

        Task<UserGroup> GetUserGroupAsync(Guid id);

        Task UpdateUserGroupAsync(Guid id, UserGroup userGroup);

        Task DeleteUserGroupAsync(Guid id);

        Task<UserGroup> ReviveUserGroupAsync(Guid id);
    }
}
