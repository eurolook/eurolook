using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.Extensions;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Eurolook.ManagementConsole.Database
{
    public class DevicesDatabase : EurolookManagementConsoleDatabase, IDevicesDatabase
    {
        public DevicesDatabase(IConfiguration configuration)
            : base(configuration)
        {
        }

        public async Task<IEnumerable<DeviceDataModel>> GetDevices(DataPage<DeviceDataModel> page, string searchTerm, bool deleted)
        {
            using (var context = GetContext())
            {
                var userGroups = await context.UserGroups.AsNoTracking().Where(ug => !ug.Deleted).ToArrayAsync();
                var filteredQuery = GetFilteredDevicesQuery(context, searchTerm, deleted);
                var filteredDevices = await filteredQuery.OrderByAndPaginate(page).ToArrayAsync();
                return filteredDevices.Select(x => new DeviceDataModel(x.DeviceSettings, userGroups, x.User)).ToList();
            }
        }

        public int GetDevicesCount(string searchTerm, bool deleted)
        {
            using (var context = GetContext())
            {
                return GetFilteredDevicesQuery(context, searchTerm, deleted).Count();
            }
        }

        public DeviceDataModel GetDeviceForEdit(Guid id)
        {
            using (var context = GetContext())
            {
                var userGroups = context.UserGroups.AsNoTracking().Where(ug => !ug.Deleted).ToArray();

                var result = (from deviceSettings in context.DeviceSettings
                              where deviceSettings.Id == id
                              join userSettings in context.UserSettings on deviceSettings.UserSettingsId equals
                                  userSettings.Id
                              join user in context.Users on userSettings.Id equals user.SettingsId
                              select new { deviceSettings, user }).FirstOrDefault();

                return result != null ? new DeviceDataModel(result.deviceSettings, userGroups, result.user) : null;
            }
        }

        public void DeleteDevice(Guid id)
        {
            SetDeletedFlag(id);
        }

        public void DeleteDevicePermanently(Guid id)
        {
            using (var context = GetContext())
            {
                context.DeviceSettings.Remove(context.DeviceSettings.First(d => d.Id == id));
                context.SaveChanges();
            }
        }

        public void ReviveDevice(Guid id)
        {
            SetDeletedFlag(id, false);
        }

        private static IQueryable<DevicesResult> GetFilteredDevicesQuery(
            EurolookContext context,
            string searchTerm,
            bool deleted)
        {
            var queryable = from deviceSettings in context.DeviceSettings
                            join user in context.Users on deviceSettings.UserSettingsId equals user.SettingsId
                            select new DevicesResult
                            {
                                Id = deviceSettings.Id,
                                DeviceName = deviceSettings.DeviceName,
                                Login = user.Login,
                                EurolookVersion = deviceSettings.AddinVersion,
                                OfficeVersion = deviceSettings.OfficeVersion,
                                DotNetFullFrameworkReleaseVersion = deviceSettings.DotNetFullFrameworkReleaseVersion,
                                PhysicallyInstalledSystemMemoryKb = deviceSettings.PhysicallyInstalledSystemMemoryKb,
                                LastLogin = deviceSettings.LastLogin,
                                LoadTime = deviceSettings.MaxAddInLoadTimeBucketSeconds,

                                DeviceSettings = deviceSettings,
                                User = user,
                            };

            Expression<Func<DevicesResult, bool>> predicate;
            if (string.IsNullOrWhiteSpace(searchTerm) || searchTerm == "*")
            {
                predicate = d => d.DeviceSettings.Deleted == deleted;
            }
            else
            {
                predicate = d => d.DeviceSettings.Deleted == deleted &&
                                 (d.DeviceSettings.DeviceName.Contains(searchTerm)
                                  || d.DeviceSettings.AddinVersion.Contains(searchTerm)
                                  || d.User.Login.Contains(searchTerm));
            }

            var filteredQuery = queryable.Where(predicate);
            return filteredQuery;
        }

        private void SetDeletedFlag(Guid id, bool flag = true)
        {
            using (var context = GetContext())
            {
                var result = context.DeviceSettings.First(deviceSettings => deviceSettings.Id == id);
                result.SetDeletedFlag(flag);

                context.SaveChanges();
            }
        }

        private class DevicesResult : Identifiable
        {
            public string DeviceName { get; set; }

            public string Login { get; set; }

            public string EurolookVersion { get; set; }

            public string OfficeVersion { get; set; }

            public int? DotNetFullFrameworkReleaseVersion { get; set; }

            public long PhysicallyInstalledSystemMemoryKb { get; set; }

            public DateTime? LastLogin { get; set; }

            public float LoadTime { get; set; }

            public User User { get; set; }

            public DeviceSettings DeviceSettings { get; set; }
        }
    }
}
