using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data.Models.AuthorRoles;
using Eurolook.ManagementConsole.Models;

namespace Eurolook.ManagementConsole.Database
{
    public interface IAuthorRolesDatabase
    {
        Task<AuthorRole> GetAuthorRoleAsync(Guid id);

        Task<IEnumerable<AuthorRole>> GetAllAuthorRolesAsync();

        Task<IEnumerable<AuthorRole>> GetAuthorRolesAsync(
            DataPage<AuthorRole> page,
            string searchTerm,
            bool deleted);

        Task<uint> GetAuthorRolesCountAsync(string searchTerm, bool deleted);

        Task<AuthorRole> CreateAuthorRoleAsync(AuthorRole model);

        Task<AuthorRole> UpdateAuthorRoleAsync(Guid id, AuthorRole authorRole);

        Task DeleteAuthorRoleAsync(Guid id);

        Task<AuthorRole> ReviveAuthorRoleAsync(Guid id);
    }
}
