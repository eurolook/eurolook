using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.Extensions;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Eurolook.ManagementConsole.Database
{
    public class UserGroupDatabase : EurolookManagementConsoleDatabase, IUserGroupDatabase
    {
        public UserGroupDatabase(IConfiguration configuration)
            : base(configuration)
        {
        }

        public async Task<IEnumerable<UserGroup>> GetUserGroupsAsync(DataPage<UserGroup> page, string searchTerm, bool deleted)
        {
            using (var context = GetContext())
            {
                var query = context.UserGroups.Where(
                    x => (searchTerm == null || x.Name.Contains(searchTerm))
                         && x.Deleted == deleted);

                return await query.OrderByAndPaginate(page)
                                  .ToListAsync();
            }
        }

        public async Task<uint> GetUserGroupCountAsync(string searchTerm, bool deleted)
        {
            using (var context = GetContext())
            {
                return (uint)await context.UserGroups.CountAsync(x => (searchTerm == null || x.Name.Contains(searchTerm)) && x.Deleted == deleted);
            }
        }

        public async Task<UserGroup> CreateUserGroupAsync(string name)
        {
            using (var context = GetContext())
            {
                var userGroup = new UserGroup();
                userGroup.Init();
                userGroup.Name = name;
                var entityEntry = context.UserGroups.Add(userGroup);
                await context.SaveChangesAsync();
                return entityEntry.Entity;
            }
        }

        public async Task<UserGroup> GetUserGroupAsync(Guid id)
        {
            using (var context = GetContext())
            {
                return await context.UserGroups.FirstOrDefaultAsync(u => u.Id == id);
            }
        }

        public async Task UpdateUserGroupAsync(Guid id, UserGroup userGroup)
        {
            using (var context = GetContext())
            {
                var existingUserGroup = await context.UserGroups.FirstOrDefaultAsync(u => u.Id == id);
                if (existingUserGroup == null)
                {
                    return;
                }

                existingUserGroup.UpdateServerModificationTime();
                existingUserGroup.Description = userGroup.Description;
                existingUserGroup.Name = userGroup.Name;
                existingUserGroup.UiPositionIndex = userGroup.UiPositionIndex;
                existingUserGroup.AdGroups = userGroup.AdGroups;
                await context.SaveChangesAsync();
            }
        }

        public async Task DeleteUserGroupAsync(Guid id)
        {
            using (var context = GetContext())
            {
                var userGroup = await context.UserGroups.FirstAsync(c => c.Id == id);
                await ValidateUserGroupCanBeDeleted(context, userGroup);

                userGroup.SetDeletedFlag();
                await context.SaveChangesAsync();
            }
        }

        public async Task<UserGroup> ReviveUserGroupAsync(Guid id)
        {
            using (var context = GetContext())
            {
                var userGroup = await context.UserGroups.FirstAsync(u => u.Id == id);
                userGroup.SetDeletedFlag(false);
                await context.SaveChangesAsync();
                return userGroup;
            }
        }

        private async Task ValidateUserGroupCanBeDeleted(EurolookServerContext context, UserGroup userGroup)
        {
            string exceptionMessage = "";
            if (userGroup == null)
            {
                throw new InvalidEnumArgumentException("User group is null");
            }

            var users = await context.Users
                                     .Where(u => u.UserGroupIds.Contains(userGroup.Id.ToString()) && !u.Deleted)
                                     .ToListAsync();
            if (users.Any())
            {
                exceptionMessage = $"The user group '{userGroup.Name}' is still referenced by one or more users.";
            }

            var bricks = await context.Bricks
                                      .Where(b => b.VisibleToUserGroups.Contains(userGroup.Id.ToString()) && !b.Deleted)
                                      .ToListAsync();
            if (bricks.Any())
            {
                exceptionMessage += $"The user group '{userGroup.Name}' is still referenced by one or more bricks.";
            }

            if (!string.IsNullOrEmpty(exceptionMessage))
            {
                throw new ConstraintException(exceptionMessage);
            }
        }
    }
}
