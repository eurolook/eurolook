﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Models;

namespace Eurolook.ManagementConsole.Database
{
    public interface IUsersDatabase
    {
        UserDataModel[] GetUsers(
            DataPage<UserDataModel> page,
            IEnumerable<UserGroup> userGroups,
            string searchQuery,
            bool deleted);

        User GetUserForEdit(Guid id);

        bool UserExists(string login);

        User AddUserProfile(User user);

        Author UpdateAuthor(Author author, ModificationType modificationType);

        Author GetAuthorByUserName(string userName, bool includeDeleted = true);

        Author GetAuthorByUserId(Guid userId, bool includeDeleted = true);

        UserSettings UpdateUserSettings(UserSettings settings, ModificationType modificationType);

        List<Address> GetAddresses(bool includeSpecialAddressNoAddress = false);

        Task<List<Address>> GetAddressesAsync(bool includeSpecialAddressNoAddress = false);

        Address GetAddress(Guid? id);

        OrgaEntity GetOrgaEntity(Guid id);

        OrgaEntity GetOrgaEntity(EurolookContext context, Guid id);

        List<UserGroup> GetAllUserGroups();

        /// <summary>
        /// Creates or revives a user profile.
        /// </summary>
        /// <param name="login">The user's Windows login.</param>
        /// <param name="email">The user's email address.</param>
        /// <returns>Returns true when the profile was created or revived, false when it already exists.</returns>
        bool CreateOrReviveUserProfile(string login, string email = null);

        bool DeleteUserProfile(string userName);

        void DeleteUserProfile(Guid id);

        bool DeleteUserProfilePermanently(string userName);

        void DeleteUserProfilePermanently(Guid id);

        void ReviveUserProfile(Guid id);

        void ReviveUserProfile(EurolookServerContext context, Guid id);

        string FindUserName(string fullLoginName);

        bool TryMigrateUserName(string userName, string newUserName);

        int GetUserCount(IEnumerable<UserGroup> userGroups, string searchQuery, bool deleted);

        Guid GetUserIdByName(string userName);

        User GetUser(string userName);

        void UpdateUserProfile(Guid id, UserEditDataModel model);

        void RequestRemoteWipe(Guid id);
    }
}
