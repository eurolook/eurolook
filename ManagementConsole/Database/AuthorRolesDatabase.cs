using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Data.Extensions;
using Eurolook.Data.Models.AuthorRoles;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Eurolook.ManagementConsole.Database
{
    public class AuthorRolesDatabase : EurolookManagementConsoleDatabase, IAuthorRolesDatabase
    {
        public AuthorRolesDatabase(IConfiguration configuration)
            : base(configuration)
        {
        }

        public async Task<AuthorRole> GetAuthorRoleAsync(Guid id)
        {
            using (var context = GetContext())
            {
                return await context.AuthorRoles.FirstOrDefaultAsync(ar => ar.Id == id);
            }
        }

        public async Task<IEnumerable<AuthorRole>> GetAllAuthorRolesAsync()
        {
            using (var context = GetContext())
            {
                return await context.AuthorRoles
                                    .AsNoTracking()
                                    .Where(l => !l.Deleted)
                                    .OrderBy(dl => dl.UiPositionIndex)
                                    .ToListAsync();
            }
        }

        public async Task<IEnumerable<AuthorRole>> GetAuthorRolesAsync(
            DataPage<AuthorRole> page,
            string searchTerm,
            bool deleted)
        {
            using (var context = GetContext())
            {
                var query = context.AuthorRoles.Where(ar => (searchTerm == null || ar.Name.Contains(searchTerm)) && ar.Deleted == deleted);
                return await query.OrderByAndPaginate(page)
                                  .ToArrayAsync();
            }
        }

        public async Task<uint> GetAuthorRolesCountAsync(string searchTerm, bool deleted)
        {
            using (var context = GetContext())
            {
                return (uint)await context.AuthorRoles.CountAsync(x => x.Deleted == deleted);
            }
        }

        public async Task<AuthorRole> CreateAuthorRoleAsync(AuthorRole model)
        {
            using (var context = GetContext())
            {
                return await context.AuthorRoles.CreateOrUpdateAsync(
                    model,
                    context,
                    ar => ar.Deleted && ar.Name == model.Name);
            }
        }

        public async Task<AuthorRole> UpdateAuthorRoleAsync(Guid id, AuthorRole authorRole)
        {
            using (var context = GetContext())
            {
                var existingAuthorRole = await GetAuthorRoleAsync(id);
                if (existingAuthorRole == null)
                {
                    return null;
                }

                authorRole.Id = id;
                authorRole.UpdateServerModificationTime();
                context.AuthorRoles.AddOrUpdate(authorRole);
                await context.SaveChangesAsync();
                return authorRole;
            }
        }

        public async Task DeleteAuthorRoleAsync(Guid id)
        {
            using (var context = GetContext())
            {
                var documentModelAuthorRoles =
                    context.DocumentModelAuthorRoles.Where(x => x.AuthorRoleId == id && !x.Deleted)
                           .Include(x => x.DocumentModel)
                           .ToList();

                if (documentModelAuthorRoles.Count > 0)
                {
                    throw new ConstraintException(
                        $"There are still {documentModelAuthorRoles.Count} document models using this author role: " +
                        $"{string.Join(", ", documentModelAuthorRoles.Select(x => x.DocumentModel.Name).ToArray())}.");
                }

                var authorRole = await context.AuthorRoles.FirstOrDefaultAsync(ar => ar.Id == id);
                authorRole?.SetDeletedFlag();
                await context.SaveChangesAsync();
            }
        }

        public async Task<AuthorRole> ReviveAuthorRoleAsync(Guid id)
        {
            using (var context = GetContext())
            {
                var authorRole = await context.AuthorRoles.FirstAsync(c => c.Id == id);
                authorRole.SetDeletedFlag(false);

                await context.SaveChangesAsync();
                return authorRole;
            }
        }
    }
}
