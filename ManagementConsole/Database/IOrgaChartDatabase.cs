﻿using System;
using System.Collections.Generic;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.ViewModels.OrgaChart;
using JetBrains.Annotations;

namespace Eurolook.ManagementConsole.Database
{
    public interface IOrgaChartDatabase
    {
        JsTreeNode GetOrgaTreeForView(Guid dgId, Guid languageId, int authorCount = 0);

        OrgaEntity GetRoot();

        List<JsTreeNode> GetAuthorsNodes(Guid orgaEntityId, int? authorCount);

        OrgaEntity GetOrgaEntityForImport(Guid id);

        int DeleteOrgaChart(Guid rootId);

        List<OrgaEntity> GetOrphanedOrgaEntities([NotNull] List<OrgaEntity> orgaEntities);

        OrgaEntity CreateDg(string acronym, Guid primaryAddressId, Guid? secondaryAddressId);

        Text[] GetTexts(Guid?[] textIds);

        Translation[] GetTranslations(Guid?[] textIds);

        List<OrgaEntity> GetAllDgs();

        List<Address> GetAddresses(bool includeSpecialAddressNoAddress = false);

        List<Translation> GetAllTranslations(Guid? textId);
    }
}