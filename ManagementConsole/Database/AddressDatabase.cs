﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.Extensions;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.ViewModels.Address;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Eurolook.ManagementConsole.Database
{
    public class AddressDatabase : EurolookManagementConsoleDatabase, IAddressDatabase
    {
        public AddressDatabase(IConfiguration configuration)
            : base(configuration)
        {
        }

        public Address[] GetAddresses(DataPage<Address> page, string searchTerm, bool deleted)
        {
            using (var context = GetContext())
            {
                return context.Addresses
                              .Where(GetAddressQueryPredicate(searchTerm, deleted))
                              .OrderByAndPaginate(page)
                              .ToArray();
            }
        }

        public int GetAddressCount(string query, bool deleted)
        {
            using (var context = GetContext())
            {
                return context.Addresses.Count(GetAddressQueryPredicate(query, deleted));
            }
        }

        private static Expression<Func<Address, bool>> GetAddressQueryPredicate(string query, bool deleted)
        {
            Expression<Func<Address, bool>> predicate;
            if (string.IsNullOrWhiteSpace(query) || query == "*")
            {
                predicate = x => x.Deleted == deleted;
            }
            else
            {
                predicate = x => x.Deleted == deleted && x.Name.Contains(query);
            }

            return predicate;
        }

        public Address CreateAddress(AddressViewModel viewModel)
        {
            using (var context = GetContext())
            {
                if (context.Addresses.Any(x => x.Name == viewModel.Name))
                {
                    throw new Exception($"There is already an address named '{viewModel.Name}'.");
                }

                var address = new Address();
                address.Init();
                address.Name = viewModel.Name;

                string aliasLocation = "AddressLocation" + viewModel.Name;
                int aliasLocationNo = context.Texts.Count(t => t.Alias == aliasLocation) + 1;
                aliasLocation += aliasLocationNo;

                string aliasFooter = "AddressFooter" + viewModel.Name;
                int aliasFooterNo = context.Texts.Count(t => t.Alias == aliasFooter) + 1;
                aliasFooter += aliasFooterNo;

                address.LocationText = new Text();
                address.LocationText.Init();
                address.LocationText.Alias = aliasLocation;

                address.FooterText = new Text();
                address.FooterText.Init();
                address.FooterText.Alias = aliasFooter;

                context.Addresses.Add(address);
                context.SaveChanges();
                return address;
            }
        }

        public string CreateNameText(Guid id)
        {
            using (var context = GetContext())
            {
                var address = context.Addresses.First(x => x.Id == id);
                if (address.NameTextId == null)
                {
                    address.NameText = new Text { Alias = $"AddressName{address.Name}" };
                    address.NameText.Init();
                    address.UpdateServerModificationTime(address.NameText.ServerModificationTimeUtc);
                    context.Texts.Add(address.NameText);
                    context.SaveChanges();
                    return address.NameText.Alias;
                }

                return null;
            }
        }

        public async Task DeleteAddress(Guid id, bool deleted)
        {
            using (var context = GetContext())
            {
                var address = await context.Addresses.FirstOrDefaultAsync(x => x.Id == id);
                address.SetDeletedFlag();
                await context.SaveChangesAsync();
            }
        }

        public async Task RestoreAddress(Guid id, bool deleted)
        {
            using (var context = GetContext())
            {
                var address = await context.Addresses.FirstOrDefaultAsync(x => x.Id == id);
                address.SetDeletedFlag(deleted);
                await context.SaveChangesAsync();
            }
        }

        public AddressEditDataModel GetAddressForEdit(Guid id)
        {
            using (var context = GetContext())
            {
                var address = context.Addresses.Where(x => x.Id == id)
                               .Include(x => x.FooterText)
                               .Include(x => x.LocationText)
                               .Include(x => x.NameText)
                               .FirstOrDefault();
                if (address != null)
                {
                    return new AddressEditDataModel
                    {
                        Name = address.Name,
                        Phone = address.PhoneNumberPrefix,
                        LocationTextAlias = address.LocationText.Alias,
                        FooterTextAlias = address.FooterText.Alias,
                        NameTextAlias = address.NameText?.Alias,
                        IsDefault = address.IsDefault ?? false,
                        ActiveDirectoryReference = address.ActiveDirectoryReference,
                    };
                }

                return null;
            }
        }

        public void UpdateAddress(Guid id, AddressEditDataModel model)
        {
            using (var context = GetContext())
            {
                var address = context.Addresses.FirstOrDefault(x => x.Id == id);
                if (address != null)
                {
                    address.Name = model.Name;
                    address.PhoneNumberPrefix = model.Phone;
                    address.UpdateServerModificationTime();
                    address.IsDefault = model.IsDefault;
                    address.ActiveDirectoryReference = model.ActiveDirectoryReference;
                    context.SaveChanges();
                }
            }
        }

        public async Task<List<Address>> GetAllAddressesAsync()
        {
            return await GetAddressesAsync(true);
        }
    }
}
