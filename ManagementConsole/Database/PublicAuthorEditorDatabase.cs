using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.Data.PublicAuthorEditor;
using Eurolook.ManagementConsole.ViewModels.PublicAuthors;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;

namespace Eurolook.ManagementConsole.Database
{
    [AllowAnonymous]
    public class PublicAuthorEditorDatabase : EurolookManagementConsoleDatabase
    {
        public PublicAuthorEditorDatabase(IConfiguration configuration)
            : base(configuration)
        {
        }

        public AuthorEditRequest CreateRequest(EditRequest request, string userName)
        {
            AuthorEditRequest result;
            using (var context = GetContext())
            {
                var originator = context.Authors.FirstOrDefault(x => x.Email == request.YourEmailAddress);
                if (originator == null)
                {
                    return null;
                }

                var addressee = GetAuthorByUserName(userName);
                var newRequest = new AuthorEditRequest
                {
                    CreatedUtc = DateTime.UtcNow,
                    OriginatorEmail = originator.Email,
                    OriginatorName = !string.IsNullOrWhiteSpace(originator.LatinFullName) ? originator.LatinFullName : originator.Email,
                    AddresseeEmail = request.OtherEmailAddress,
                    AddresseeName = !string.IsNullOrWhiteSpace(addressee?.LatinFullName) ? addressee.LatinFullName : request.OtherEmailAddress,
                    Token = CreateToken(),
                    IsApproved = false,
                };
                result = context.AuthorEditRequests.Add(newRequest).Entity;
                context.SaveChanges();
            }

            return result;
        }

        public AuthorEditRequest GetEditRequest(string token)
        {
            using (var context = GetContext())
            {
                return context.AuthorEditRequests.FirstOrDefault(x => x.Token == token);
            }
        }

        private string CreateToken()
        {
            using (var hash = SHA256.Create())
            {
                return string.Concat(hash
                  .ComputeHash(Encoding.UTF8.GetBytes(Guid.NewGuid().ToString()))
                  .Select(item => item.ToString("x2")));
            }
        }

        public AuthorEditRequest ApproveRequest(string token)
        {
            using (var context = GetContext())
            {
                var request = context.AuthorEditRequests.FirstOrDefault(x => x.Token == token);
                if (request != null)
                {
                    request.IsApproved = true;
                    request.ExpiredUtc = DateTime.UtcNow.AddDays(request.ExpirationDays);
                    context.SaveChanges();
                    return request;
                }

                return null;
            }
        }

        public void AddAuthorToUser(string userName, Author author)
        {
            using (var context = GetContext())
            {
                var user = context.Users.FirstOrDefault(u => u.Login == userName);
                if (user == null)
                {
                    this.LogError($"Could not find user {userName}.");
                    return;
                }

                // get a UserAuthors relation that is not deleted
                var userAuthor = context.UserAuthors.FirstOrDefault(ua => ua.UserId == user.Id && ua.AuthorId == author.Id && !ua.Deleted);
                if (userAuthor == null)
                {
                    // there is no relation, check if there is a deleted relation and undelete it
                    userAuthor = context.UserAuthors.FirstOrDefault(ua => ua.UserId == user.Id && ua.AuthorId == author.Id && ua.Deleted);
                    if (userAuthor != null)
                    {
                        userAuthor.SetDeletedFlag(false);
                        this.LogDebug(
                            $"Undeleted UserAuthor relation ({userAuthor.Id}) for user {user.Login} and author {author.LatinFullName}.");
                    }
                }

                // there is no relation at all, create a new one
                if (userAuthor == null)
                {
                    userAuthor = new UserAuthor();
                    userAuthor.Init();
                    userAuthor.UserId = user.Id;
                    userAuthor.AuthorId = author.Id;
                    context.UserAuthors.Add(userAuthor);
                    this.LogDebug($"Created a new  UserAuthor relation ({userAuthor.Id}) for user {user.Login} and author {author.LatinFullName}.");
                }

                context.SaveChanges();
            }
        }
    }
}
