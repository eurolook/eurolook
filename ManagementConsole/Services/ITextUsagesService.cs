﻿using System.Collections.Generic;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.ViewModels.Texts;

namespace Eurolook.ManagementConsole.Services
{
    public interface ITextUsagesService
    {
        IEnumerable<TextUsageViewModel> GetAdressUsages(Text text);

        IEnumerable<TextUsageViewModel> GetOrgaEntityUsages(Text text);

        IEnumerable<TextUsageViewModel> GetBrickUsages(Text text);

        IEnumerable<TextUsageViewModel> GetPredefinedFunctionUsages(Text text);

        bool IsSystemText(Text text);
    }
}
