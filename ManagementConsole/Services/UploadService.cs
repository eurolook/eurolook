using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace Eurolook.ManagementConsole.Services
{
    public class UploadService
    {
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly string _uploadFolder;

        public UploadService(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
            _uploadFolder = GetUploadFolderName();
        }

        public async Task<Guid> UploadFile(IFormFile file)
        {
            var uploadId = Guid.NewGuid();
            var filePath = GetUploadFileName(uploadId);
            using (Stream fileStream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }

            return uploadId;
        }

        public string GetUploadFileName(Guid id)
        {
            return $"{_uploadFolder}/{id}";
        }

        internal void TryDeleteFile(Guid uploadId)
        {
            try
            {
                DeleteFile(uploadId);
            }
            catch (Exception)
            {
                // ignored
            }
        }

        internal void DeleteFile(Guid uploadId)
        {
            string filePath = GetUploadFileName(uploadId);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }

        private string GetUploadFolderName()
        {
            string folderName = Path.Combine(_webHostEnvironment.ContentRootPath, "App_Data", "Uploads");
            if (!Directory.Exists(folderName))
            {
                Directory.CreateDirectory(folderName);
            }

            return folderName;
        }
    }
}
