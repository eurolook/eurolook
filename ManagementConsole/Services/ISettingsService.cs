﻿namespace Eurolook.ManagementConsole.Services
{
    public interface ISettingsService
    {
        string BaseUrl { get; }

        string SmtpServer { get; }

        string FromAddress { get; }

        string IisLogFilesFolder { get; }

        string TerminalServerPrefixes { get; }

        bool IsReadOnlyMode { get; }

        bool EnabledExceptionLogMailer { get; }

        string ExceptionLogMailTo { get; }
    }
}
