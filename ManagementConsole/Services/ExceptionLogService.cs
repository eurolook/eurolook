﻿using System;
using System.Threading.Tasks;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.ExcelExport;

namespace Eurolook.ManagementConsole.Services
{
    public class ExceptionLogService
    {
        private readonly StatisticsDatabase _statisticsDatabase;

        public ExceptionLogService(StatisticsDatabase statisticsDatabase)
        {
            _statisticsDatabase = statisticsDatabase;
        }

        public async Task<byte[]> GetExceptionLogByBucket(DateTime fromDateUtc)
        {
            var exceptionLogInfo = await _statisticsDatabase.GetExceptionLogInfoByBucket(fromDateUtc);
            var excelExporter = new ExcelExporter();
            return excelExporter.ExportExceptionLog(exceptionLogInfo);
        }

        public async Task<byte[]> GetExceptionLogByEntry(DateTime fromDateUtc)
        {
            var exceptionLogInfo = await _statisticsDatabase.GetExceptionLogInfoByEntry(fromDateUtc);
            var excelExporter = new ExcelExporter();
            return excelExporter.ExportExceptionLog(exceptionLogInfo);
        }
    }
}
