﻿using System.Configuration;
using Eurolook.ActiveDirectoryLink;
using Microsoft.Extensions.Configuration;

namespace Eurolook.ManagementConsole.Services
{
    public class SettingsService : ISettingsService, IActiveDirectoryConfigurationProvider
    {
        private readonly IConfiguration _configuration;

        public SettingsService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string BaseUrl => _configuration["BaseUrl"];

        public string SmtpServer => _configuration["SmtpServer"];

        public string FromAddress => _configuration["FromAddress"];

        public string IisLogFilesFolder => _configuration["IISLogFilesFolder"];

        public string TerminalServerPrefixes => _configuration["TerminalServerPrefixes"];

        public bool IsReadOnlyMode => bool.Parse(_configuration["IsReadOnlyMode"]);

        public bool EnabledExceptionLogMailer => bool.Parse(_configuration["EnabledExceptionLogMailer"]);

        public string ExceptionLogMailTo => _configuration["ExceptionLogMailTo"];

        public string LdapServer => _configuration["LDAPServer"];

        public string[] LdapSearchBases => _configuration.GetSection("LDAPSearchBases").Get<string[]>();

        public string LdapAuthenticationType => _configuration["LDAPAuthenticationType"];

        public int ActiveDirectoryPageSize => int.Parse(_configuration["ActiveDirectoryPageSize"]);

        public string ActiveDirectorySearchFilter => _configuration["ActiveDirectorySearchFilter"];

        public string RetiredUsersSearchFilter => _configuration["RetiredUsersSearchFilter"];

        public string AgencyNames => _configuration["AgencyNames"];

        public string DefaultDomain => _configuration["DefaultDomain"];

        public string ServerDatabaseConnection => _configuration.GetConnectionString("ServerDatabaseConnection");
    }
}
