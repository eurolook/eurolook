using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Eurolook.Common.Extensions;

namespace Eurolook.ManagementConsole.Services
{
    public class IssLogFileParserService
    {
        public static IList<IisLogEntry> ParseLogFiles(string directory, DateTime fromDateUtc, string filter)
        {
            var entries = new List<IisLogEntry>();
            var directories = Directory.GetDirectories(directory);
            Parallel.ForEach(
                directories,
                subDirectory =>
                {
                    ReadDirectory(subDirectory, entries, fromDateUtc, filter);
                });

            return entries;
        }

        private static void ReadDirectory(string directory, List<IisLogEntry> entries, DateTime fromDateUtc, string filter)
        {
            var files = Directory.GetFiles(directory);
            Parallel.ForEach(
                files,
                file =>
                {
                    ReadFile(file, entries, fromDateUtc, filter);
                });
        }

        private static void ReadFile(string file, List<IisLogEntry> entries, DateTime fromDateUtc, string filter)
        {
            var modification = File.GetLastWriteTime(file);
            if (modification < fromDateUtc)
            {
                return;
            }

            using (var fileStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (var reader = new StreamReader(fileStream))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var entry = ParseLine(line);
                        if (entry?.Url != null && entry.Url.Like(filter) && entry.Date > fromDateUtc)
                        {
                            entries.Add(entry);
                        }
                    }
                }
            }
        }

        private static IisLogEntry ParseLine(string line)
        {
            if (line.StartsWith("#"))
            {
                return null;
            }

            var parts = line.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            return new IisLogEntry(parts);
        }
    }

    public class IisLogEntry
    {
        public DateTime Date { get; set; }
        public string Url { get; set; }
        public IisLogEntry(string[] entry)
        {
            var datePart = $"{entry[0]} {entry[1]}";
            if (DateTime.TryParse(datePart, out var parsedDate))
            {
                Date = parsedDate;
                Url = entry[4];
            }
        }
    }
}
