using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Eurolook.Data.Database;
using Eurolook.DocumentProcessing.BrickDefinitionLanguage.FormattedText;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Models;

namespace Eurolook.ManagementConsole.Services
{
    public class TranslationValidationService : ITranslationValidationService
    {
        private readonly IAdminDatabase _adminDatabase;
        private readonly ILanguageRepository _languageRepository;

        public TranslationValidationService(IAdminDatabase adminDatabase, ILanguageRepository languageRepository)
        {
            _adminDatabase = adminDatabase;
            _languageRepository = languageRepository;
        }

        public string ValidateTranslations(List<TranslationDataModel> translations)
        {
            var message = "";
            foreach (var translation in translations)
            {
                var translationTextAlias = _adminDatabase.GetText(translation.TextId).Alias;
                var translationLanguageName = _languageRepository.GetLanguage(translation.LanguageId).Name;
                if (translation.Value.Count(f => f == '{') != translation.Value.Count(f => f == '}'))
                {
                    message +=
                        $"The {translationLanguageName} translation of the Text {translationTextAlias} is missing an opening or closing bracket. {Environment.NewLine}";
                }

                var formattingCode = Regex.Matches(translation.Value, @"{.+?}");

                foreach (var token in formattingCode)
                {
                    var tokenHandler =
                        new FormattedText()._tokenHandlers.FirstOrDefault(t => t.HandlesToken(token.ToString()));

                    if (tokenHandler == null)
                    {
                        break;
                    }

                    if (tokenHandler.HandlesToken("{field:"))
                    {
                        if (!Regex.IsMatch(translation.Value, @"{field:HYPERLINK \s*"".+?""\s*\|\s*(.|\n)*\s*}")
                            && !Regex.IsMatch(translation.Value, @"{field:\s*(.|\n)*\s*}"))
                        {
                            message +=
                                $"The code {token} in the {translationLanguageName} translation of the Text {translationTextAlias} is incorrect. ";
                        }
                    }

                    if (tokenHandler.HandlesToken("{style:"))
                    {
                        if (!Regex.IsMatch(token.ToString(), @"{style:\s*(.|\n)*\s*\|\s*(.|\n)*\s*}"))
                        {
                            message +=
                                $"The code {token} in the {translationLanguageName} translation of the Text {translationTextAlias} is incorrect. ";
                        }
                    }

                    if (tokenHandler.HandlesToken("{date-static:"))
                    {
                        var inputDateFormat = token.ToString().Substring(token.ToString().LastIndexOf('|') + 1)
                                                   .TrimEnd('}');

                        if (inputDateFormat == "long" || inputDateFormat == "short")
                        {
                            message +=
                                $"The date format 'long' or 'short' in the {translationLanguageName} translation of the Text {translationTextAlias} are not supported. ";
                        }

                        if (!Regex.IsMatch(token.ToString(), @"{date-static:\s*(.|\n)*\s*\|\s*(.|\n)*\s*}"))
                        {
                            message +=
                                $"The code {token} in the {translationLanguageName} translation of the Text {translationTextAlias} is incorrect.";
                        }
                    }

                    if (tokenHandler.HandlesToken("{metadata:"))
                    {
                        if (!Regex.IsMatch(token.ToString(), @"{metadata:\s*(.|\n)*\s*\|\s*(.|\n)*\s*}"))
                        {
                            message +=
                                $"The code {token} in the {translationLanguageName} translation of the Text {translationTextAlias} is incorrect.";
                        }
                    }

                    if (tokenHandler.HandlesToken("{date-bound:"))
                    {
                        if (!Regex.IsMatch(
                                token.ToString(),
                                @"{date-bound:\s*(.|\n)*\s*\|\s*(.|\n)*\s*\|\s*(.|\n)*\s*}"))
                        {
                            message +=
                                $"The code {token} in the {translationLanguageName} translation of the Text {translationTextAlias} is incorrect. ";
                        }
                    }
                }
            }

            if (message != "")
            {
                return
                    $"Be aware that the following translations are not valid for formattedText: {Environment.NewLine}{message}";
            }

            return message;
        }
    }
}
