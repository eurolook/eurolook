using System.Collections.Generic;
using Eurolook.ManagementConsole.Models;

namespace Eurolook.ManagementConsole.Services
{
    public interface ITranslationValidationService
    {
        string ValidateTranslations(List<TranslationDataModel> translations);
    }
}
