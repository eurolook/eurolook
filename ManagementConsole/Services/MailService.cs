﻿using System;
using System.IO;
using System.Net.Mail;
using Eurolook.Data.PublicAuthorEditor;
using Microsoft.Extensions.Hosting;

namespace Eurolook.ManagementConsole.Services
{
    public class MailService
    {
        private readonly ISettingsService _settingsService;
        private readonly IHostEnvironment _hostingEnvironment;

        public MailService(ISettingsService settingsService, IHostEnvironment hostingEnvironment)
        {
            _settingsService = settingsService;
            _hostingEnvironment = hostingEnvironment;
        }

        public void SendAuthorEditRequest(AuthorEditRequest request)
        {
            var mailMessage = new MailMessage();
            mailMessage.To.Add(request.AddresseeEmail.Trim());
            mailMessage.From = new MailAddress(string.IsNullOrWhiteSpace(_settingsService.FromAddress) ? request.OriginatorEmail.Trim() : _settingsService.FromAddress);
            mailMessage.Subject = "Eurolook 10 - Request to edit your author data";
            mailMessage.Body =
                $"Hello {request.AddresseeName},\n\n" +
                $"{request.OriginatorName} ({request.OriginatorEmail}) requests permission to edit your Eurolook author data. To grant permission, click on the confirmation link below:\n\n" +
                $"{_settingsService.BaseUrl}/PublicAuthors/Approve/{request.Token}\n\n" +
                $"{request.OriginatorName} will be able to edit your author data for {request.ExpirationDays} days.\n\n" +
                "Best regards,\n" +
                "Your Eurolook Team";

            var smtpClient = new SmtpClient(_settingsService.SmtpServer);
            smtpClient.Send(mailMessage);
        }

        public void SendExceptionLogMail(byte[] exceptionLogBytes, DateTime fromDate)
        {
            var mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(_settingsService.FromAddress);
            foreach (var recipient in _settingsService.ExceptionLogMailTo
                .Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries))
            {
                var address = recipient.Trim();
                if (!string.IsNullOrEmpty(address))
                {
                    mailMessage.To.Add(address);
                }
            }

            mailMessage.Subject = $"Eurolook 10 - Exception Logs [{_hostingEnvironment.EnvironmentName}] [{Environment.MachineName}]";
            mailMessage.Body =
                "Hello,\n\n" +
                $"Attached please find the exceptions that occured between {fromDate} (UTC) and now.\n\n" +
                "Best regards,\n" +
                "The Eurolook Team";
            mailMessage.Attachments.Add(
                new Attachment(
                    new MemoryStream(exceptionLogBytes),
                    "ExceptionLog.xlsx",
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));

            var smtpClient = new SmtpClient(_settingsService.SmtpServer);
            smtpClient.Send(mailMessage);
        }

        public void SendAuthorEditApproval(AuthorEditRequest request)
        {
            var mailMessage = new MailMessage();
            mailMessage.To.Add(request.OriginatorEmail.Trim());
            mailMessage.From = new MailAddress(string.IsNullOrWhiteSpace(_settingsService.FromAddress) ? request.AddresseeEmail.Trim() : _settingsService.FromAddress);
            mailMessage.Subject = "Eurolook 10 - Confirmation for editing author data";
            mailMessage.Body =
                $"Hello {request.OriginatorName},\n\n" +
                $"{request.AddresseeName} ({request.AddresseeEmail}) confirmed your request to edit his/her author data. Click on the link below to access the author editor:\n\n" +
                $"{_settingsService.BaseUrl}/PublicAuthors/Edit/{request.Token}\n\n" +
                $"This link is valid until {request.ExpiredDate}.\n\n" +
                "Best regards,\n" +
                "Your Eurolook Team";

            var smtpClient = new SmtpClient(_settingsService.SmtpServer);
            smtpClient.Send(mailMessage);
        }
    }
}
