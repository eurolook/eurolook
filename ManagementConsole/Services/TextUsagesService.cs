﻿using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.ViewModels.Texts;

namespace Eurolook.ManagementConsole.Services
{
    public class TextUsagesService : ITextUsagesService
    {
        private readonly IAdminDatabase _adminDatabase;

        public TextUsagesService(IAdminDatabase adminDatabase)
        {
            _adminDatabase = adminDatabase;
        }

        public IEnumerable<TextUsageViewModel> GetAdressUsages(Text text)
        {
            return _adminDatabase.GetAddresses(true)
                                 .Where(a => a.FooterTextId == text.Id || a.LocationTextId == text.Id || a.NameTextId == text.Id)
                                 .Select(a => new TextUsageViewModel(a.Name, a.Id));
        }

        public IEnumerable<TextUsageViewModel> GetOrgaEntityUsages(Text text)
        {
            return _adminDatabase.GetAllOrgaEntity()
                                 .Where(oe => oe.HeaderTextId == text.Id)
                                 .Select(a => new TextUsageViewModel(a.Name, a.Id));
        }

        public IEnumerable<TextUsageViewModel> GetBrickUsages(Text text)
        {
            return _adminDatabase.GetBricksUsingText(text)
                                 .Select(a => new TextUsageViewModel(a.Name, a.Id));
        }

        public IEnumerable<TextUsageViewModel> GetPredefinedFunctionUsages(Text text)
        {
            return _adminDatabase.GetPredefinedFunctions()
                                 .Where(f => f.FunctionTextId == text.Id || f.FunctionHeaderTextId == text.Id || f.FunctionFemaleTextId == text.Id || f.FunctionHeaderFemaleTextId == text.Id)
                                 .Select(a => new TextUsageViewModel(a.Name, a.Id));
        }

        public bool IsSystemText(Text text)
        {
            return _adminDatabase.GetSystemTexts().Any(t => t.TextId == text.Id);
        }
    }
}
