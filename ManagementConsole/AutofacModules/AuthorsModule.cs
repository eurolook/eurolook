using Autofac;
using Eurolook.ManagementConsole.Database;

namespace Eurolook.ManagementConsole.AutofacModules
{
    public class AuthorsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AuthorsDatabase>()
                   .AsSelf();
        }
    }
}
