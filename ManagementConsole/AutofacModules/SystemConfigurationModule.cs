using Autofac;
using Eurolook.ManagementConsole.Database;

namespace Eurolook.ManagementConsole.AutofacModules
{
    public class SystemConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SystemConfigurationDatabase>()
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();
        }
    }
}
