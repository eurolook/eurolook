using Autofac;
using Eurolook.ManagementConsole.Database;

namespace Eurolook.ManagementConsole.AutofacModules
{
    public class UserGroupsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UserGroupDatabase>()
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();
        }
    }
}
