using Autofac;
using Eurolook.ManagementConsole.Database;

namespace Eurolook.ManagementConsole.AutofacModules
{
    public class DevicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DevicesDatabase>()
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();
        }
    }
}
