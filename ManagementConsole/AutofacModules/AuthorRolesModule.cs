using Autofac;
using Eurolook.ManagementConsole.Database;

namespace Eurolook.ManagementConsole.AutofacModules
{
    public class AuthorRolesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AuthorRolesDatabase>()
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();
        }
    }
}
