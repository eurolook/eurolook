using Autofac;
using Eurolook.ManagementConsole.Services;

namespace Eurolook.ManagementConsole.AutofacModules
{
    public class TranslationsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TranslationValidationService>()
                   .AsImplementedInterfaces()
                   .SingleInstance();
        }
    }
}
