using Autofac;
using Eurolook.Data.TermStore;
using Eurolook.ManagementConsole.Areas.Metadata.Database;
using Eurolook.ManagementConsole.Areas.Metadata.TermStore;
using Eurolook.ManagementConsole.Mapping;

namespace Eurolook.ManagementConsole.AutofacModules
{
    public class MetadataModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TermStoreXmlParser>()
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();

            builder.RegisterType<TermStoreXmlValidator>()
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();

            builder.RegisterType<TermStoreServerDatabase>()
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();

            builder.RegisterType<MetadataCategoryDatabase>()
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();

            builder.RegisterType<MetadataDatabase>()
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();

            builder.RegisterType<TermStoreCache>()
                   .AsImplementedInterfaces()
                   .SingleInstance();

            builder.RegisterType<MetadataDefinitionMapper>()
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();
        }
    }
}
