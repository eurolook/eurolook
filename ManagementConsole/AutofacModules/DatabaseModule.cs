using System.Configuration;
using Autofac;
using Eurolook.Data;
using Eurolook.Data.Database;
using Eurolook.ManagementConsole.Database;

namespace Eurolook.ManagementConsole.AutofacModules
{
    public class DatabaseModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<LanguageRepository>()
                   .AsImplementedInterfaces()
                   .SingleInstance();

            builder.RegisterType<SystemConfigurationRepository>()
                   .AsImplementedInterfaces()
                   .SingleInstance();

            builder.RegisterType<OrgaChartDatabase>()
                   .AsSelf()
                   .SingleInstance();

            builder.RegisterType<StatisticsDatabase>()
                   .AsSelf()
                   .SingleInstance();

            builder.RegisterType<TranslationsDatabase>()
                   .AsSelf()
                   .SingleInstance();
        }
    }
}
