const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    mode: 'development',
    // Path to the 'primary' script of your project, that references all other built resources
    entry: {
        'import': 'ImportViewModel.js',
        'export': 'ExportViewModel.js',
        'data.package': 'DataPackageViewModel.js',
        'snapshots': 'SnapshotsViewModel.js',
        'cleanup': 'CleanupViewModel.js',
        'system.configuration': 'SystemConfiguration/App.tsx',
        'editor.worker': 'monaco-editor/esm/vs/editor/editor.worker.js',
        'json.worker': 'monaco-editor/esm/vs/language/json/json.worker',
        'css.worker': 'monaco-editor/esm/vs/language/css/css.worker',
        'html.worker': 'monaco-editor/esm/vs/language/html/html.worker',
        'ts.worker': 'monaco-editor/esm/vs/language/typescript/ts.worker'
    },
    output: {
        // Output directory and file name
        path: path.resolve(__dirname, 'wwwroot/dist'),
        filename: '[name].bundle.js',
        globalObject: 'self'
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.(ts|tsx)$/,
                resolve: {
                    extensions: ['.ts', '.tsx']
                },
                loader: 'ts-loader'
            },
            {
                test: /\.ttf$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: 'fonts/[name].[ext]',
                        publicPath: '/dist/'
                    }
                }]
            }
        ]
    },
    resolve: {
        modules: [
            path.resolve('./Scripts'),
            path.resolve('./ViewModels'),
            path.resolve('./Areas/DataExchange/ViewModels'),
            path.resolve('./Metadata/ViewModels'),
            path.resolve('./Views/'),
            'node_modules'
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new CopyWebpackPlugin({
            patterns: [
                {
                    context: 'node_modules/jquery/dist', 
                    from: "**/*",
                    to: "jquery/"
                },
                {
                    context: 'node_modules/jqueryui',
                    from: "**/*{.js,.css,.ts,.png,.jpg,.map}",
                    to: "jqueryui/"
                },
                {
                    context: 'node_modules/jquery-datetimepicker/build',
                    from: "**/*{.js,.css,.ts,.png,.jpg,.map}",
                    to: "jquery-datetimepicker/"
                },
                {
                    context: 'node_modules/knockout/build/output',
                    from: "**/*{.js,.css,.ts,.png,.jpg,.map}",
                    to: "knockout/"
                },
                {
                    context: 'node_modules/chart.js/dist',
                    from: "**/*.umd.js",
                    to: "chart.js/"
                },
            ]
        })
    ],
    externals: {
        jquery: 'jQuery'
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                monacoCommon: {
                    test: /[\\/]node_modules[\\/]monaco\-editor/,
                    name: 'monaco-editor-common',
                    chunks: 'async'
                }
            }
        }
    }
};
