﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Areas.DataExchange;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Controllers
{
    public class SystemConfigurationModel
    {
        public SystemConfigurationModel()
        {
        }

        public SystemConfigurationModel(SystemConfiguration configuration)
        {
            Id = configuration.Id;
            Key = configuration.Key;
            Value = configuration.Value;
        }

        public Guid? Id { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }
    }

    [Authorize(Policy = "OrgaAdmin")]
    public class SystemConfigurationController : EurolookController
    {
        private readonly ISystemConfigurationDatabase _systemConfigurationDatabase;
        private readonly ExportService _exportService;

        public SystemConfigurationController(
            ISettingsService settingsService,
            ISystemConfigurationDatabase systemConfigurationDatabase,
            ExportService exportService)
            : base(settingsService)
        {
            _systemConfigurationDatabase = systemConfigurationDatabase;
            _exportService = exportService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> List()
        {
            var configurations = await _systemConfigurationDatabase.GetSystemConfigurationsAsync();
            return new JsonResult(configurations.Select(c => new SystemConfigurationModel(c)));
        }

        [HttpPut]
        public async Task Update([FromBody] SystemConfigurationModel[] configurations)
        {
            foreach (var configuration in configurations)
            {
                await _systemConfigurationDatabase.UpdateSystemConfigurationAsync(
                    (Guid)configuration.Id,
                    configuration.Value);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] SystemConfigurationModel configuration)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                var systemConfiguration = await _systemConfigurationDatabase.CreateSystemConfigurationAsync(configuration);
                return JsonResult("The system configuration has been created.", new SystemConfigurationModel(systemConfiguration));
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The system configuration could not be created. {ex.GetInnerException().Message}");
            }
        }

        [HttpPost]
        public async Task<ActionResult> Export(
            [FromBody] List<SystemConfigurationModel> systemConfigurationModels)
        {
            try
            {
                var exportPackage = await _exportService.ExportSystemConfiguration(systemConfigurationModels);
                return JsonResult(new { PackageId = exportPackage.Id });
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The data package could not be created. {ex.GetInnerException().Message}");
            }
        }

        // DELETE: /SystemConfiguration/Delete/00000000-0000-0000-0000-000000000000
        [HttpDelete]
        public ActionResult Delete(Guid id)
        {
            try
            {
                _systemConfigurationDatabase.DeleteSystemConfigurationAsync(id);
                return JsonResult("The system configuration has been deleted.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The system configuration could not be deleted. {ex.GetInnerException().Message}");
            }
        }
    }
}
