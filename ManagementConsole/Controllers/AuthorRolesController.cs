using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Eurolook.Common.Log;
using Eurolook.Data.Models.AuthorRoles;
using Eurolook.ManagementConsole.Areas.DataExchange;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Controllers
{
    public class AuthorRolesController : EurolookController
    {
        private readonly IAuthorRolesDatabase _database;
        private readonly IMapper _mapper;
        private readonly ExportService _exportService;

        public AuthorRolesController(
            ISettingsService settingsService,
            IAuthorRolesDatabase database,
            IMapper mapper,
            ExportService exportService)
            : base(settingsService)
        {
            _database = database;
            _mapper = mapper;
            _exportService = exportService;
        }

        [HttpGet]
        public async Task<ActionResult> Index(
            string q = null,
            uint p = 1,
            uint s = 20,
            string o = "Name",
            bool asc = true,
            bool deleted = false)
        {
            if (!Request.IsAjaxRequest())
            {
                return View();
            }

            try
            {
                var page = new DataPage<AuthorRole>(p, s, asc, o);
                page.Data.AddRange(await _database.GetAuthorRolesAsync(page, q, deleted));
                page.TotalCount = await _database.GetAuthorRolesCountAsync(q, deleted);
                return JsonResult(page);
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"Author roles could not be loaded. {ex.GetInnerException().Message}");
            }
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] AuthorRoleDataModel dataModel)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                var model = _mapper.Map<AuthorRole>(dataModel);
                model = await _database.CreateAuthorRoleAsync(model);
                return JsonResult("Successfully created author role", model);
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"Author role could not be created. {ex.GetInnerException().Message}");
            }
        }

        [HttpGet]
        public async Task<ActionResult> Edit(Guid id)
        {
            if (!Request.IsAjaxRequest())
            {
                return View(id);
            }

            var authorRole = await _database.GetAuthorRoleAsync(id);
            if (authorRole == null)
            {
                return NotFound();
            }

            var authorRoleDataModel = _mapper.Map<AuthorRoleDataModel>(authorRole);
            return JsonResult(authorRoleDataModel);
        }

        [HttpPut]
        public async Task<ActionResult> Edit(Guid id, [FromBody] AuthorRoleDataModel authorRoleDataModel)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                var authorRole = _mapper.Map<AuthorRole>(authorRoleDataModel);
                await _database.UpdateAuthorRoleAsync(id, authorRole);
                return JsonResult("The author role has been updated successfully!");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The author role could not be updated. {ex.GetInnerException().Message}");
            }
        }

        [HttpDelete]
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                await _database.DeleteAuthorRoleAsync(id);
                return JsonResult("The author role has been deleted.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The author role could not be deleted. {ex.GetInnerException().Message}");
            }
        }

        [HttpPost]
        public async Task<JsonResult> Revive(Guid id)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                await _database.ReviveAuthorRoleAsync(id);
                return JsonResult("The author role has been revived.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The author role could not be revived. {ex.GetInnerException().Message}");
            }
        }

        [HttpPost]
        public async Task<ActionResult> Export([FromBody] List<AuthorRole> authorRoles)
        {
            try
            {
                var exportPackage = await _exportService.ExportAuthorRoles(authorRoles);
                return JsonResult(new { PackageId = exportPackage.Id });
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The data package could not be created. {ex.GetInnerException().Message}");
            }
        }
    }
}
