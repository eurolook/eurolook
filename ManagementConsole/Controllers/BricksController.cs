﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Common;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.DocumentProcessing.BrickDefinitionLanguage;
using Eurolook.DocumentProcessing.BrickDefinitionLanguage.FormattedText;
using Eurolook.ManagementConsole.Areas.DataExchange;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Eurolook.ManagementConsole.ViewModels.Resources;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Controllers
{
    public class BricksController : EurolookController
    {
        private readonly IAdminDatabase _adminDatabase;
        private readonly ExportService _exportService;
        private readonly ITranslationValidationService _translationValidationService;

        public BricksController(
            ISettingsService settingsService,
            IAdminDatabase adminDatabase,
            ExportService exportService,
            ITranslationValidationService translationValidationService)
            : base(settingsService)
        {
            _adminDatabase = adminDatabase;
            _exportService = exportService;
            _translationValidationService = translationValidationService;
        }

        // GET: /Bricks/
        // GET: /Bricks/?q=XYZ&p=1
        public ActionResult Index(
            string q = null,
            uint p = 1,
            uint s = 20,
            string o = "alias",
            bool asc = true,
            bool deleted = false)
        {
            if (Request.IsAjaxRequest())
            {
                var page = new DataPage<BrickDataModel>(p, s, asc, o);
                page.Data.AddRange(_adminDatabase.GetBricks(page, q, deleted));
                page.TotalCount = (uint)_adminDatabase.GetBrickCount(q, deleted);
                return JsonResult(page);
            }

            return View();
        }

        // POST: /Bricks/Create
        [HttpPost]
        public ActionResult Create([FromBody] BrickDataModel model)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                var brick = _adminDatabase.CreateBrick(model);
                return JsonResult("The brick has been created.", new BrickDataModel(brick));
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The brick could not be created. {ex.GetInnerException().Message}");
            }
        }

        // DELETE: /Bricks/Delete/00000000-0000-0000-0000-000000000000
        [HttpDelete]
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                _adminDatabase.DeleteBrick(id);
                return JsonResult("The brick has been deleted successfully!");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The brick could not be deleted. {ex.GetInnerException().Message}");
            }
        }

        // GET: /Bricks/LoadBricks
        public ActionResult LoadBricks()
        {
            var brickCount = (uint)_adminDatabase.GetBrickCount(null, false);
            var allBricks = _adminDatabase.GetBricks(null);
            if (brickCount == allBricks.Length)
            {
                return JsonResult(allBricks);
            }

            return JsonError("Could not load all Bricks");
        }

        // POST: /Bricks/Duplicate/00000000-0000-0000-0000-00000000000
        [HttpPost]
        public ActionResult Duplicate([FromBody] List<BrickDataModel> sendBricks)
        {
            try
            {
                var brick = _adminDatabase.GetBrick((Guid)sendBricks[0].Id);
                var selectedBrick = _adminDatabase.GetBrick((Guid)sendBricks[1].Id);
                if (brick != null && selectedBrick != null)
                {
                    var categories = _adminDatabase.GetAllBrickCategories();
                    var userGroups = _adminDatabase.GetAllUserGroups();
                    var allBrickGroups = _adminDatabase.GetAllBrickGroups(false);
                    var allResources = _adminDatabase
                                       .GetAllResources().Select(resource => new ResourceViewModel(resource))
                                       .ToList();
                    var brickResources = _adminDatabase
                                         .GetResourcesByBrick(brick.Id)
                                         .Select(resource => new ResourceViewModel(resource))
                                         .OrderBy(r => r.Alias).ToList();
                    var documentModels = _adminDatabase.GetDocumentModelsByBrick(selectedBrick.Id);
                    var allTexts = _adminDatabase.GetTextsForBrickEdit();
                    foreach (var documentModel in documentModels)
                    {
                        // clean large objects from response to reduce response size
                        documentModel.PreviewImage = null;
                        documentModel.PreviewImageWithSampleText = null;
                        documentModel.Template = null;
                    }

                    var brickTexts = selectedBrick.Texts
                                                  .Where(t => !t.Deleted)
                                                  .Select(text => new TextDataModel(text.Text))
                                                  .ToList();
                    var viewModel = new BricksEditDataModel(
                        brick,
                        categories,
                        userGroups,
                        allBrickGroups,
                        allResources,
                        brickResources,
                        documentModels,
                        brickTexts,
                        ModelState,
                        allTexts);

                    var viewModelDuplicate = new BricksEditDataModel(
                        selectedBrick,
                        categories,
                        userGroups,
                        allBrickGroups,
                        allResources,
                        brickResources,
                        documentModels,
                        brickTexts,
                        ModelState,
                        allTexts);

                    var id = viewModel.Id;
                    var name = viewModel.Name;
                    viewModel = viewModelDuplicate;
                    viewModel.Id = id;
                    viewModel.Name = name;
                    _adminDatabase.UpdateBrick(brick.Id, viewModel);
                    _adminDatabase.UpdateBrickIcon(brick.Id, selectedBrick.Icon);
                    return JsonResult(viewModel);
                }

                return JsonError("Could not duplicate brick");
            }
            catch (InvalidOperationException ex)
            {
                // only log a warning in case the brick is not valid
                this.LogWarnFormat("PUT /Bricks/Edit/{1} failed\n{0}", ex, sendBricks[0].Id);
                return JsonError($"The brick could not be duplicated. {ex.GetInnerException().Message}");
            }
        }

        // GET: /Bricks/Edit/00000000-0000-0000-0000-000000000000
        public ActionResult Edit(Guid id)
        {
            if (Request.IsAjaxRequest())
            {
                var brick = _adminDatabase.GetBrick(id);
                var allTexts = _adminDatabase.GetTextsForBrickEdit();
                if (brick != null)
                {
                    var categories = _adminDatabase.GetAllBrickCategories();
                    var userGroups = _adminDatabase.GetAllUserGroups();
                    var allBrickGroups = _adminDatabase.GetAllBrickGroups(false);
                    var allResources = _adminDatabase.GetAllResources()
                                                     .Select(resource => new ResourceViewModel(resource)).ToList();
                    var brickResources = _adminDatabase
                                         .GetResourcesByBrick(id).Select(resource => new ResourceViewModel(resource))
                                         .OrderBy(r => r.Alias).ToList();
                    var documentModels = _adminDatabase.GetDocumentModelsByBrick(id).OrderBy(dm => dm.Name).ToList();
                    foreach (var documentModel in documentModels)
                    {
                        // clean large objects from response to reduce response size
                        documentModel.PreviewImage = null;
                        documentModel.PreviewImageWithSampleText = null;
                        documentModel.Template = null;
                    }

                    var brickTexts = brick.Texts
                                          .Where(t => !t.Deleted)
                                          .Select(text => new TextDataModel(text.Text))
                                          .OrderBy(t => t.Alias)
                                          .ToList();

                    var viewModel = new BricksEditDataModel(
                        brick,
                        categories,
                        userGroups,
                        allBrickGroups,
                        allResources,
                        brickResources,
                        documentModels,
                        brickTexts,
                        ModelState,
                        allTexts);
                    return JsonResult(viewModel);
                }

                return JsonError($"Could not find brick with id {id}");
            }

            return View(id);
        }

        // PUT: /Bricks/Edit/00000000-0000-0000-0000-000000000000
        [HttpPut]
        public ActionResult Edit(Guid id, [FromBody] BricksEditDataModel model)
        {
            var message = "";
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                if (!ModelState.IsValid)
                {
                    return JsonError($"The brick could not be updated. {ModelState.GetErrorMessages()}");
                }

                if (model.IsContentBrick && !model.IsDynamicBrick)
                {
                    var bdl = BdlDocument.Parse(model.Content);
                    var formattedTextElements = bdl.FindContentOfType<FormattedText>();
                    message = ValidateFormattedText(formattedTextElements, model);
                }

                _adminDatabase.UpdateBrick(id, model);
                if (message != "")
                {
                    message = char.ToLower(message[0]) + message.Substring(1);
                    return JsonResult("The brick has been updated successfully, but " + message);
                }

                return JsonResult("The brick has been updated successfully!");
            }
            catch (InvalidBdlDocumentException ex)
            {
                // only log a warning in case the brick is not valid
                this.LogWarnFormat("PUT /Bricks/Edit/{1} failed\n{0}", ex, id);
                return JsonError($"The brick could not be updated. {ex.GetInnerException().Message}");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The brick could not be updated. {ex.GetInnerException().Message}");
            }
        }

        // GET: /Bricks/Icon/00000000-0000-0000-0000-000000000000
        public ActionResult Icon(Guid id)
        {
            var brick = _adminDatabase.GetBrick(id);

            if (brick?.Icon == null || brick.Icon.Length == 0)
            {
                return NotFound();
            }

            return File(brick.Icon, "application/octet-stream", brick.Name + ".png");
        }

        // POST: /Bricks/Icon/00000000-0000-0000-0000-000000000000
        [HttpPost]
        public async Task<ActionResult> Icon(Guid id, [FromForm] IFormFile file)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                var bytes = await _adminDatabase.UpdateBrickIcon(id, file);
                return JsonResult(null, bytes != null ? Tools.GetBase64Image(bytes) : null);
            }
            catch (Exception ex)
            {
                return HandleError(ex, ex.GetInnerException().Message);
            }
        }

       // GET: /Bricks/VectorIcon/00000000-0000-0000-0000-000000000000
        public ActionResult VectorIcon(Guid id)
        {
            var brick = _adminDatabase.GetBrick(id);

            if (brick?.VectorIcon == null || brick.VectorIcon.Length == 0)
            {
                return NotFound();
            }

            return File(brick.VectorIcon, "application/octet-stream", brick.Name + ".svg");
        }

        // POST: /Bricks/VectorIcon/00000000-0000-0000-0000-000000000000
        [HttpPost]
        public async Task<ActionResult> VectorIcon(Guid id, [FromForm] IFormFile file)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                var bytes = await _adminDatabase.UpdateBrickVectorIcon(id, file);
                return JsonResult(null, bytes != null ? Tools.GetBase64Image(bytes) : null);
            }
            catch (Exception ex)
            {
                return HandleError(ex, ex.GetInnerException().Message);
            }
        }

        // POST: /Bricks/Export
        [HttpPost]
        public async Task<ActionResult> Export(
            [FromBody] List<BrickDataModel> brickVms,
            [FromQuery] bool includeGroups,
            [FromQuery] bool includeTexts,
            [FromQuery] bool includeResources,
            [FromQuery] bool includeUserGroups)
        {
            try
            {
                var exportPackage = await _exportService.ExportBricks(
                    brickVms,
                    includeGroups,
                    includeTexts,
                    includeResources,
                    includeUserGroups);
                return JsonResult(new { PackageId = exportPackage.Id });
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The data package could not be created. {ex.GetInnerException().Message}");
            }
        }

        private string ValidateFormattedText(FormattedText[] formattedTextElements, BricksEditDataModel model)
        {
            var message = "";

            foreach (var formattedTextElement in formattedTextElements)
            {
                try
                {
                    var formattedTextDataModel = model.BrickTexts
                                                      .Where(
                                                          t => t.Alias == formattedTextElement.BoundText.Remove(0, 7))
                                                      .ToList();
                    var translations = _adminDatabase.GetTranslations(formattedTextDataModel[0].TextId)
                                                     .FindAll(t => t.Value != null);

                    var tl = new List<TranslationDataModel>();
                    foreach (var t in translations)
                    {
                        tl.Add(
                            new TranslationDataModel(new Translation())
                            {
                                TextId = (Guid)formattedTextDataModel[0].TextId,
                                TextAlias = formattedTextDataModel[0].Alias,
                                Value = t.Value,
                                LanguageId = t.LanguageId,
                                LanguageName = t.Language.Name,
                            });
                    }

                    message += _translationValidationService.ValidateTranslations(tl);
                }
                catch (Exception e)
                {
                    this.LogError(e);
                }
            }

            return message;
        }
    }
}
