using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Areas.DataExchange;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Controllers
{
    public class DocumentCategoryController : EurolookController
    {
        private readonly IDocumentCategoryDatabase _documentCategoryDatabase;
        private readonly ExportService _exportService;

        public DocumentCategoryController(
            ISettingsService settingsService,
            IDocumentCategoryDatabase documentCategoryDatabase,
            ExportService exportService)
            : base(settingsService)
        {
            _documentCategoryDatabase = documentCategoryDatabase;
            _exportService = exportService;
        }

        // GET: DocumentCategory
        public ActionResult Index(
            string q = null,
            uint p = 1,
            uint s = 20,
            string o = "Name",
            bool asc = true,
            bool deleted = false)
        {
            if (Request.IsAjaxRequest())
            {
                var page = new DataPage<DocumentCategory>(p, s, asc, o);
                page.Data.AddRange(_documentCategoryDatabase.GetDocumentCategories(page, q, deleted));
                page.TotalCount = (uint)_documentCategoryDatabase.GetDocumentCategoryCount(q, deleted);
                return JsonResult(page);
            }

            return View();
        }

        // POST: /DocumentCategory/Create
        [HttpPost]
        public ActionResult Create([FromBody] DocumentCategory model)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                var docCategory = _documentCategoryDatabase.CreateDocumentCategory(model.Name);
                return JsonResult("The document category has been created.", docCategory);
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The document category could not be created. {ex.GetInnerException().Message}");
            }
        }

        // GET: /DocumentCategory/Edit/00000000-0000-0000-0000-000000000000
        public ActionResult Edit(Guid id)
        {
            if (Request.IsAjaxRequest())
            {
                var docCategory = _documentCategoryDatabase.GetDocumentCategory(id);
                if (docCategory != null)
                {
                    return JsonResult(docCategory);
                }

                return NotFound();
            }

            return View(id);
        }

        // PUT: /DocumentCategory/Edit/00000000-0000-0000-0000-000000000000
        [HttpPut]
        public ActionResult Edit(Guid id, [FromBody] DocumentCategory category)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                _documentCategoryDatabase.UpdateDocumentCategory(id, category);
                return JsonResult("The document category has been updated successfully!");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The document category could not be updated. {ex.GetInnerException().Message}");
            }
        }

        // DELETE: /DocumentCategory/Delete/00000000-0000-0000-0000-000000000000
        [HttpDelete]
        public JsonResult Delete(Guid id)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                _documentCategoryDatabase.DeleteDocumentCategory(id);
                return JsonResult("The document category has been deleted.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The document category could not be deleted. {ex.GetInnerException().Message}");
            }
        }

        // DELETE: /DocumentCategory/DeletePermanently/00000000-0000-0000-0000-000000000000
        [HttpDelete]
        public JsonResult DeletePermanently(Guid id)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                _documentCategoryDatabase.DeleteDocumentCategoryPermanently(id);
                return JsonResult("The document category has been deleted permanently.");
            }
            catch (Exception ex)
            {
                return HandleError(
                    ex,
                    $"The document category could not be deleted permanently. {ex.GetInnerException().Message}");
            }
        }

        // POST: /DocumentCategory/Revive/00000000-0000-0000-0000-000000000000
        [HttpPost]
        public JsonResult Revive(Guid id)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                _documentCategoryDatabase.ReviveDocumentCategory(id);
                return JsonResult("The document category has been revived.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The document category could not be revived. {ex.GetInnerException().Message}");
            }
        }

        [HttpPost]
        public async Task<ActionResult> Export([FromBody] List<DocumentCategoryDataModel> documentCategories)
        {
            try
            {
                var exportPackage = await _exportService.ExportDocumentCategory(documentCategories);
                return JsonResult(new { PackageId = exportPackage.Id });
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The data package could not be created. {ex.GetInnerException().Message}");
            }
        }
    }
}
