﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Common.Extensions;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Controllers
{
    public class PerformanceController : EurolookController
    {
        private const int MaximumVersions = 4;

        private static readonly string[] BrightColors =
        {
            "#6586cd", // blue
            "#fec05d", // yellow
            "#de687e", // red
            "#71c171", // green
        };

        private static readonly string[] NormalColors =
        {
            "#32539a", // blue
            "#fca311", // yellow
            "#a4243b", // red
            "#3e8e3e", // green
        };

        private readonly IPerformanceDatabase _performanceDatabase;

        public PerformanceController(ISettingsService settingsService, IPerformanceDatabase performanceDatabase)
            : base(settingsService)
        {
            _performanceDatabase = performanceDatabase;
        }

        private enum DateFilter
        {
            LastWeek = 0,
            Last2Weeks = 1,
            LastMonth = 2,
            Last3Months = 3,
            Last6Months = 4,
            Last12Months = 5,
            NoFilter = 99,
        }

        public ActionResult Index()
        {
            return RedirectToAction("Servers");
        }

        public ActionResult Servers()
        {
            return View();
        }

        public ActionResult Clients()
        {
            return View();
        }

        [HttpGet]
        [ActionName("clientstatistics")]
        public JsonResult GetClientSideStatistics(int id)
        {
            var fromDate = GetStartDateUtc((DateFilter)id);
            var versions = _performanceDatabase.GetVersionsWithClientLogs(MaximumVersions);
            var performanceInfos = _performanceDatabase.GetClientPerformanceLogInfosByDate(fromDate);
            var colorMap = versions
                           .Select(
                               (version, index) => (
                                   version,
                                   normal: NormalColors.GetCircular(index),
                                   bright: BrightColors.GetCircular(index)))
                           .ToDictionary(p => p.version, p => p);
            var result = new PerformanceLogClientResultModel(performanceInfos.GetCategories().ToList());

            foreach (var version in versions)
            {
                var logs = performanceInfos.GetEventEntriesForVersion(version, nameof(PerformanceLogEvent.AddInLoaded));
                AddEntriesToData(result.AddInLoaded, logs, version, colorMap[version].normal);
                AddEntriesToDetailedData(
                    result.AddInLoadedDetails,
                    logs,
                    version,
                    colorMap[version].bright,
                    colorMap[version].normal);

                logs = performanceInfos.GetEventEntriesForVersion(
                    version,
                    nameof(PerformanceLogEvent.CreationDialogLoaded));
                AddEntriesToData(result.CreationDialogLoaded, logs, version, colorMap[version].normal);
                AddEntriesToDetailedData(
                    result.CreationDialogLoadedDetails,
                    logs,
                    version,
                    colorMap[version].bright,
                    colorMap[version].normal);

                logs = performanceInfos.GetEventEntriesForVersion(
                    version,
                    nameof(PerformanceLogEvent.DataSyncFinished));
                AddEntriesToData(result.DataSyncFinished, logs, version, colorMap[version].normal);
                AddEntriesToDetailedData(
                    result.DataSyncFinishedDetails,
                    logs,
                    version,
                    colorMap[version].bright,
                    colorMap[version].normal);

                logs = performanceInfos.GetEventEntriesForVersion(version, nameof(PerformanceLogEvent.DocumentCreated));
                AddEntriesToData(result.DocumentCreated, logs, version, colorMap[version].normal);
                AddEntriesToDetailedData(
                    result.DocumentCreatedDetails,
                    logs,
                    version,
                    colorMap[version].bright,
                    colorMap[version].normal);
            }

            return new JsonResult(result);
        }

        [HttpGet]
        [ActionName("serverstatistics")]
        public JsonResult GetServerSideStatistics(int id)
        {
            var fromDate = GetStartDateUtc((DateFilter)id);
            var versions = _performanceDatabase.GetVersionsWithServerLogs(MaximumVersions);
            var performanceInfos =
                _performanceDatabase.GetServerPerformanceLogInfosByDate(fromDate, id <= (int)DateFilter.LastMonth);
            var colorMap = versions
                           .Select(
                               (version, index) => (
                                   version,
                                   normal: NormalColors.GetCircular(index),
                                   bright: NormalColors.GetCircular(index),
                                   dark: NormalColors.GetCircular(index)))
                           .ToDictionary(p => p.version, p => p);
            var result = new PerformanceLogServerResultModel(performanceInfos.GetCategories().ToList());

            foreach (var version in versions)
            {
                var logs = performanceInfos.GetEventEntriesForVersion(
                    version,
                    nameof(PerformanceLogEvent.ServerGetInitialization));
                AddEntriesToData(result.GetInitialization, logs, version, colorMap[version].normal);

                logs = performanceInfos.GetEventEntriesForVersion(
                    version,
                    nameof(PerformanceLogEvent.ServerGetUpdates));
                AddEntriesToData(result.GetUpdates, logs, version, colorMap[version].normal);

                logs = performanceInfos.GetEventEntriesForVersion(
                    version,
                    nameof(PerformanceLogEvent.ServerGetUserProfile));
                AddEntriesToData(result.GetUserProfile, logs, version, colorMap[version].normal);
            }

            return new JsonResult(result);
        }

        private static void AddEntriesToData(
            ChartData<decimal> data,
            List<PerformanceLogInfo> logs,
            Version version,
            string color)
        {
            var medians = logs.Select(performanceLog => performanceLog?.AllMedian ?? new TimeSpan(0))
                              .ToList();

            data.Series.Add(
                new ChartSeries<decimal>
                {
                    Name = version.ToString(),
                    Color = color,
                    Data = medians.Select(t => decimal.Round((decimal)t.TotalSeconds, 2)).ToArray(),
                });
        }

        private static void AddEntriesToDetailedData(
            ChartData<decimal> detailedData,
            IReadOnlyCollection<PerformanceLogInfo> logs,
            Version version,
            string fastColor,
            string slowColor)
        {
            var fastMedians = logs.Select(performanceLog => performanceLog?.Fastest25Median ?? new TimeSpan(0))
                                  .ToList();
            var slowestMedians = logs.Select(performanceLog => performanceLog?.Slowest15Median ?? new TimeSpan(0))
                                     .ToList();

            detailedData.Series.Add(
                new ChartSeries<decimal>
                {
                    Name = version + " - fastest 25%",
                    Color = fastColor,
                    Data = fastMedians.Select(t => decimal.Round((decimal)t.TotalSeconds, 2)).ToArray(),
                });
            detailedData.Series.Add(
                new ChartSeries<decimal>
                {
                    Name = version + " - slowest 15%",
                    Color = slowColor,
                    Data = slowestMedians.Select(t => decimal.Round((decimal)t.TotalSeconds, 2)).ToArray(),
                });
        }

        private static DateTime? GetStartDateUtc(DateFilter filter)
        {
            var utcNow = DateTime.UtcNow.Date;
            switch (filter)
            {
                case DateFilter.LastWeek:
                    return utcNow.Subtract(new TimeSpan(7, 0, 0, 0));
                case DateFilter.Last2Weeks:
                    return utcNow.Subtract(new TimeSpan(14, 0, 0, 0));
                case DateFilter.LastMonth:
                    return utcNow.AddMonths(-1);
                case DateFilter.Last3Months:
                    return utcNow.AddMonths(-3);
                case DateFilter.Last6Months:
                    return utcNow.AddMonths(-6);
                case DateFilter.Last12Months:
                    return utcNow.AddMonths(-12);
                case DateFilter.NoFilter:
                    return null;
                default:
                    return utcNow;
            }
        }
    }
}
