﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Areas.DataExchange;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.ExcelExport;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Eurolook.ManagementConsole.ViewModels.Languages;
using Eurolook.ManagementConsole.ViewModels.OrgaChart;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Controllers
{
    [Authorize(Policy = "OrgaAdmin")]
    public class OrgaChartController : EurolookController
    {
        private readonly ILanguageRepository _languageRepository;
        private readonly ExcelOrgaChartExporter _excelOrgaChartExporter;
        private readonly IOrgaChartDatabase _orgaChartDatabase;
        private readonly ExportService _exportService;
        private readonly UploadService _uploadService;
        private readonly ImportService _importService;

        public OrgaChartController(
            ISettingsService settingsService,
            ILanguageRepository languageRepository,
            ExcelOrgaChartExporter excelOrgaChartExporter,
            IOrgaChartDatabase orgaChartDatabase,
            UploadService uploadService,
            ImportService importService,
            ExportService exportService)
            : base(settingsService)
        {
            _languageRepository = languageRepository;
            _excelOrgaChartExporter = excelOrgaChartExporter;
            _orgaChartDatabase = orgaChartDatabase;
            _uploadService = uploadService;
            _importService = importService;
            _exportService = exportService;
        }

        // GET: OrgaChart
        public ViewResult Index()
        {
            var allDgs = _orgaChartDatabase.GetAllDgs();
            var allLanguages = _languageRepository.GetAllLanguages();

            return View(
                new OrgaChartIndexViewModel
                {
                    Languages = allLanguages.Select(l => new LanguageViewModel(l)).OrderBy(vm => vm.DisplayName).ToList(),
                    Dgs = allDgs.Select(dg => new JsTreeNode(dg)).OrderBy(x => x.DisplayName).ToList(),
                });
        }

        // GET: OrgaChart/Import
        public ViewResult Import()
        {
            var allDgs = _orgaChartDatabase.GetAllDgs();
            var allLanguages = _languageRepository.GetAllLanguages();

            return View(
                new OrgaChartImportViewModel
                {
                    Languages = allLanguages.Select(l => new LanguageViewModel(l)).OrderBy(vm => vm.DisplayName).ToList(),
                    Dgs = allDgs.Select(dg => new JsTreeNode(dg)).ToList(),
                });
        }

        // GET: OrgaChart/OrgaTree?dgId=<OrgaEntityId>&languageId=<LanguageId>
        public JsonResult OrgaTree([FromQuery] Guid dgId, [FromQuery] Guid languageId)
        {
            try
            {
                var orgaTree = _orgaChartDatabase.GetOrgaTreeForView(dgId, languageId, 2);
                return JsonResult(orgaTree);
            }
            catch (Exception ex)
            {
                return HandleError(ex, ex.Message);
            }
        }

        // GET: RevealAuthors/<OrgaEntityId>
        public JsonResult RevealAuthors(Guid id)
        {
            try
            {
                var nodes = _orgaChartDatabase.GetAuthorsNodes(id, null);
                return JsonResult(nodes);
            }
            catch (Exception ex)
            {
                return HandleError(ex, ex.Message);
            }
        }

        // DELETE: Delete/<OrgaEntityId>
        public JsonResult Delete(Guid id)
        {
            try
            {
                var detachedAuthorCount = _orgaChartDatabase.DeleteOrgaChart(id);
                return JsonResult($"The orga chart has been deleted successfully. {detachedAuthorCount} authors have been detached.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, ex.Message);
            }
        }

        // POST: /OrgaChart/Upload
        [HttpPost]
        [DisableRequestSizeLimit]
        [SuppressMessage("csharpsquid", "S5693:Make sure the content length limit is safe here.", Justification = "API endpoint is internal only.")]
        public async Task<ActionResult> Upload([FromForm] IFormFile file)
        {
            try
            {
                if (file.ContentType != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    return JsonError("File is no Excel file (.xlsx)!");
                }

                return JsonResult(
                    new
                    {
                        UploadId = await _uploadService.UploadFile(file),
                    });
            }
            catch (Exception ex)
            {
                return HandleError(ex, ex.Message);
            }
        }

        // GET: OrgaChart/UploadedOrgaTreeEL10?languageId=<LanguageId>&uploadId=<UploadId>
        public JsonResult UploadedOrgaTreeEL10([FromQuery] Guid languageId, [FromQuery] Guid uploadId)
        {
            try
            {
                // read the excel file
                OrgaEntity rootNode;
                ChangesMessageBuilder messageBuilder;
                using (var reader = new OrgaChartReader(System.IO.File.OpenRead(_uploadService.GetUploadFileName(uploadId)), _orgaChartDatabase, _languageRepository))
                {
                    rootNode = reader.ReadChart(out messageBuilder, languageId);
                }

                var tree = JsTreeNode.FromOrgaTree(_orgaChartDatabase, rootNode, 2);
                return JsonResult(messageBuilder.OutputString(), tree);
            }
            catch (Exception ex)
            {
                _uploadService.TryDeleteFile(uploadId);
                return HandleError(ex, ex.Message);
            }
        }

        // POST: OrgaChart/ConfirmUpload/<UploadId>
        [HttpPost]
        public async Task<ActionResult> ConfirmUpload(Guid id)
        {
            try
            {
                OrgaChartReader reader;
                using (reader = new OrgaChartReader(System.IO.File.OpenRead(_uploadService.GetUploadFileName(id)), _orgaChartDatabase, _languageRepository))
                {
                    reader.ReadChart();
                }

                var dataPackage = reader.GetImportDataPackage();
                var diffPackage = await _importService.CreateOrgaUploadDiffPackage(dataPackage);
                await _importService.ImportDiffPackage(diffPackage);
                return JsonResult("The orga chart was imported, a snapshot has been created.");
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                }

                return HandleError(ex, $"The orga chart could not be imported. {ex.GetInnerException().Message}");
            }
        }

        // GET: /OrgaChart/Export
        [HttpGet]
        public ActionResult Export([FromQuery] Guid dgId, [FromQuery] Guid languageId)
        {
            try
            {
                var binaryContent = _excelOrgaChartExporter.CreateExport(dgId, languageId, out string dgName);
                if (binaryContent == null || binaryContent.Length == 0)
                {
                    return NotFound();
                }

                return File(
                    binaryContent,
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    dgName + "_" + DateTime.Now + ".xlsx");
            }
            catch (Exception ex)
            {
                this.LogError("GET /OrgaChart/Export failed", ex);
                return StatusCode(500);
            }
        }

        // GET: /OrgaChart/ExportDataPackage
        [HttpGet]
        public async Task<ActionResult> ExportDataPackage(Guid id)
        {
            try
            {
                var exportPackage = await _exportService.ExportOrgaChart(id);
                return JsonResult(new { PackageId = exportPackage.Id });
            }
            catch (Exception e)
            {
                return HandleError(e, $"The data package could not be created. {e.GetInnerException().Message}");
            }
        }

        // GET: /OrgaChart/ExportAllOrgaChartsAsPackage
        [HttpGet]
        public async Task<ActionResult> ExportAllOrgaChartsAsPackage()
        {
            try
            {
                var exportPackage = await _exportService.ExportAllOrgaCharts();
                return JsonResult(new { PackageId = exportPackage.Id });
            }
            catch (Exception e)
            {
                return HandleError(e, $"The data package could not be created. {e.GetInnerException().Message}");
            }
        }

        [HttpPost]
        public ActionResult CreateDg(string acronym, Guid primaryAddressId, Guid? secondaryAddressId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(acronym))
                {
                    return JsonError("The acronym cannot be empty.");
                }

                var dg = _orgaChartDatabase.CreateDg(acronym, primaryAddressId, secondaryAddressId);
                return JsonResult("The DG has been created.", new JsTreeNode(dg));
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The DG could not be created. {ex.GetInnerException().Message}");
            }
        }
    }
}
