using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.ManagementConsole.Areas.DataExchange;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Controllers
{
    public class StyleShortcutsController : EurolookController
    {
        private readonly IStyleShortcutsDatabase _styleShortcutsDatabase;
        private readonly ExportService _exportService;

        public StyleShortcutsController(
            ISettingsService settingsService,
            IStyleShortcutsDatabase styleShortcutsDatabase,
            ExportService exportService)
            : base(settingsService)
        {
            _styleShortcutsDatabase = styleShortcutsDatabase;
            _exportService = exportService;
        }

        public ActionResult Index(
            string q = null,
            uint p = 1,
            uint s = 20,
            string o = "shortcut",
            bool asc = true,
            bool deleted = false)
        {
            if (Request.IsAjaxRequest())
            {
                var page = new DataPage<StyleShortcutDataModel>(p, s, asc, o);
                page.Data.AddRange(_styleShortcutsDatabase.GetStyleShortcuts(page, q, deleted));
                page.TotalCount = (uint)_styleShortcutsDatabase.GetStyleShortcutsCount(q, deleted);
                return JsonResult(page);
            }

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] StyleShortcutDataModel model)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                var styleShortcut = await _styleShortcutsDatabase.CreateStyleShortcut(model);
                return JsonResult("The style shortcut has been created.", new StyleShortcutDataModel(styleShortcut));
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The style shortcut could not be created. {ex.GetInnerException().Message}");
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (Request.IsAjaxRequest())
            {
                var model = _styleShortcutsDatabase.GetStyleShortcutForEdit(id);
                if (model != null)
                {
                    return JsonResult(model);
                }

                return JsonError($"Could not find style shortcut with id {id}");
            }

            ViewBag.Id = id;
            return View();
        }

        [HttpPut]
        public JsonResult Edit(Guid id, [FromBody] StyleShortcutDataModel model)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                _styleShortcutsDatabase.UpdateStyleShortcut(id, model);
                return JsonResult("The style shortcut has been updated.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The style shortcut could not be updated. {ex.GetInnerException().Message}");
            }
        }

        [HttpDelete]
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                await _styleShortcutsDatabase.DeleteStyleShortcut(id, true);
                return JsonResult("The style shortcut has been deleted.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The style shortcut could not be deleted. {ex.GetInnerException().Message}");
            }
        }

        [HttpPost]
        public async Task<JsonResult> Revive(Guid id)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                await _styleShortcutsDatabase.DeleteStyleShortcut(id, false);
                return JsonResult("The style shortcut has been revived.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The style shortcut could not be revived. {ex.GetInnerException().Message}");
            }
        }

        [HttpPost]
        public async Task<ActionResult> Export([FromBody] List<StyleShortcutDataModel> styleShortcuts)
        {
            try
            {
                var exportPackage = await _exportService.ExportStylesShortcuts(styleShortcuts);
                return JsonResult(new { PackageId = exportPackage.Id });
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The data package could not be created. {ex.GetInnerException().Message}");
            }
        }
    }
}
