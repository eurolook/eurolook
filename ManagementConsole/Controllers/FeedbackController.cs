using System;
using System.Collections.Generic;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.ExcelExport;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Controllers
{
    public class FeedbackController : EurolookController
    {
        private readonly IAdminDatabase _adminDatabase;

        public FeedbackController(ISettingsService settingsService, IAdminDatabase adminDatabase)
            : base(settingsService)
        {
            _adminDatabase = adminDatabase;
        }

        private enum DateFilter
        {
            LastWeek = 0,
            Last2Weeks = 1,
            LastMonth = 2,
            Last3Months = 3,
            Last6Months = 4,
            All = 100,
        }

        public ActionResult Index(int? filter, uint p = 1, uint s = 20, string o = "submitted", bool asc = true)
        {
            if (Request.IsAjaxRequest())
            {
                var page = new DataPage<UserFeedback>(p, s, asc, o);
                int dateRangeId = filter ?? 1;
                var fromDateUtc = GetStartDateUtc((DateFilter)dateRangeId);
                page.Data.AddRange(_adminDatabase.GetUserFeedback(fromDateUtc, page));
                page.TotalCount = (uint)_adminDatabase.GetUserFeedbackCount(fromDateUtc);
                return JsonResult(page);
            }

            return View();
        }

        // GET: /Feedback/FeedbackDownload?filter=
        public ActionResult FeedbackDownload(int? filter)
        {
            int dateRangeId = filter ?? 1;
            var fromDateUtc = GetStartDateUtc((DateFilter)filter);
            var feedbackinfo = _adminDatabase.GetUserFeedbackDownload(fromDateUtc);

            var excelExporter = new ExcelExporter();
            var package = excelExporter.ExportFeedback(feedbackinfo);
            string fileName = $"Feedback-{DateTime.Now:yyyy-MM-dd--HH-mm}.xlsx";
            return File(package, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }

        // DELETE: /Feedback/Delete
        [HttpDelete]
        public ActionResult Delete([FromBody] List<UserFeedback> userFeedback)
        {
            try
            {
                _adminDatabase.DeleteFeedback(userFeedback);
                return JsonResult("Selected feedback been deleted successfully!");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"Selected feedback could not be deleted. {ex.GetInnerException().Message}");
            }
        }

        private static DateTime GetStartDateUtc(DateFilter filter)
        {
            var utcNow = DateTime.UtcNow.Date;
            switch (filter)
            {
                case DateFilter.LastWeek:
                    return utcNow.Subtract(new TimeSpan(7, 0, 0, 0));
                case DateFilter.Last2Weeks:
                    return utcNow.Subtract(new TimeSpan(14, 0, 0, 0));
                case DateFilter.LastMonth:
                    return utcNow.AddMonths(-1);
                case DateFilter.Last3Months:
                    return utcNow.AddMonths(-3);
                case DateFilter.Last6Months:
                    return utcNow.AddMonths(-6);
                case DateFilter.All:
                    return DateTime.MinValue;
                default:
                    return utcNow;
            }
        }
    }
}
