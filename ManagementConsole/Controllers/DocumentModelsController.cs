﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Eurolook.Common;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Areas.DataExchange;
using Eurolook.ManagementConsole.Areas.Metadata.Database;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Eurolook.ManagementConsole.ViewModels.DocumentModels;
using Eurolook.ManagementConsole.ViewModels.DocumentModels.MetadataDefinitions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Controllers
{
    [SuppressMessage("csharpsquid", "S5693:Make sure the content length limit is safe here.", Justification = "API endpoint is internal only.")]
    public class DocumentModelsController : EurolookController
    {
        private readonly IAdminDatabase _adminDatabase;
        private readonly IMetadataDatabase _metadataDatabase;
        private readonly ILanguageRepository _languageRepository;
        private readonly IAuthorRolesDatabase _authorRolesDatabase;
        private readonly IMapper _mapper;
        private readonly ExportService _exportService;

        public DocumentModelsController(
            ISettingsService settingsService,
            IAdminDatabase adminDatabase,
            IMetadataDatabase metadataDatabase,
            ILanguageRepository languageRepository,
            IAuthorRolesDatabase authorRolesDatabase,
            IMapper mapper,
            ExportService exportService)
            : base(settingsService)
        {
            _adminDatabase = adminDatabase;
            _metadataDatabase = metadataDatabase;
            _languageRepository = languageRepository;
            _authorRolesDatabase = authorRolesDatabase;
            _mapper = mapper;
            _exportService = exportService;
        }

        // GET: /DocumentModels/
        public ActionResult Index(
            string q = null,
            uint p = 1,
            uint s = 20,
            string o = "alias",
            bool asc = true,
            bool deleted = false)
        {
            if (Request.IsAjaxRequest())
            {
                var page = new DataPage<DocumentModel>(p, s, asc, o);
                page.Data.AddRange(_adminDatabase.GetDocumentModels(page, q, deleted));
                page.TotalCount = (uint)_adminDatabase.GetDocumentModelsCount(q, deleted);
                return JsonResult(page);
            }

            return View();
        }

        // POST: /DocumentModels/Create
        [HttpPost]
        public ActionResult Create([FromBody] DocumentModelDataModel dataModel)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                var docModel = _adminDatabase.CreateDocumentModel(dataModel);
                return JsonResult("The document model has been created.", new DocumentModelDataModel(docModel));
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The document model could not be created. {ex.GetInnerException().Message}");
            }
        }

        // DELETE: /DocumentModels/Delete/00000000-0000-0000-0000-000000000000
        [HttpDelete]
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                _adminDatabase.DeleteDocumentModel(id);
                return JsonResult("The document model has been deleted.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The document model could not be deleted. {ex.GetInnerException().Message}");
            }
        }

        // GET: /DocumentModels/Edit/00000000-0000-0000-0000-000000000000
        public async Task<ActionResult> Edit(Guid id)
        {
            if (Request.IsAjaxRequest())
            {
                var viewModel = new DocumentModelsEditDataModel(
                    _adminDatabase.GetDocumentModelForEdit(id),
                    _languageRepository.GetAllLanguages(),
                    _adminDatabase.GetAllDocumentCategories(),
                    await _authorRolesDatabase.GetAllAuthorRolesAsync());

                return JsonResult(viewModel);
            }

            return View(id);
        }

        // PUT: /DocumentModels/Edit/00000000-0000-0000-0000-000000000000
        [HttpPut]
        public async Task<ActionResult> Edit(Guid id, [FromBody] DocumentModelsEditDataModel model)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                if (!ModelState.IsValid)
                {
                    return JsonError("The document model could not be updated. Please check the errors below.");
                }

                await _adminDatabase.UpdateDocumentModelAsync(model);
                return JsonResult("The document model has been updated successfully!");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The document model could not be updated. {ex.GetInnerException().Message}");
            }
        }

        // GET: /DocumentModels/Template/00000000-0000-0000-0000-000000000000
        public ActionResult Template(Guid id)
        {
            var model = _adminDatabase.GetDocumentModel(id);

            if ((model.Template == null) || (model.Template.Length == 0))
            {
                return NotFound();
            }

            return File(model.Template, "application/octet-stream", model.TemplateFileName);
        }

        // POST: /DocumentModels/Template/00000000-0000-0000-0000-000000000000
        [HttpPost]
        [DisableRequestSizeLimit]
        public async Task<ActionResult> Template(Guid id, IFormFile file, string fileName)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                await _adminDatabase.UpdateDocumentModelTemplate(id, file, fileName);
                return JsonResult(null);
            }
            catch (Exception ex)
            {
                return HandleError(ex, ex.Message);
            }
        }

        // POST: /DocumentModels/LanguageTemplate/00000000-0000-0000-0000-000000000000
        [HttpPost]
        [DisableRequestSizeLimit]
        public async Task<ActionResult> LanguageTemplate(Guid id, [FromQuery] Guid languageId, [FromForm] IFormFile file)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                await _adminDatabase.UpdateDocumentModelLanguageTemplate(id, languageId, file);
                return JsonResult(null);
            }
            catch (Exception ex)
            {
                return new JsonResult(
                    HttpStatusCode.InternalServerError,
                    new
                    {
                        Success = false,
                        ex.Message,
                    });
            }
        }

        // GET: /DocumentModels/LanguageTemplate/00000000-0000-0000-0000-000000000000?languageId=00000000-0000-0000-0000-000000000000
        public ActionResult LanguageTemplate(Guid id, [FromQuery] Guid languageId)
        {
            var documentModelLanguage = _languageRepository.GetDocumentModelLanguage(id, languageId);

            if ((documentModelLanguage.Template == null) || (documentModelLanguage.Template.Length == 0))
            {
                return NotFound();
            }

            return File(
                documentModelLanguage.Template,
                "application/octet-stream",
                documentModelLanguage.TemplateFileName);
        }

        // GET: /DocumentModels/Preview/00000000-0000-0000-0000-000000000000
        public ActionResult Preview(Guid id)
        {
            var model = _adminDatabase.GetDocumentModel(id);

            if ((model.Template == null) || (model.Template.Length == 0))
            {
                return NotFound();
            }

            return File(model.PreviewImage, "image/*", model.PreviewImageFileName);
        }

        // POST: /DocumentModels/Preview/00000000-0000-0000-0000-000000000000
        [HttpPost]
        [DisableRequestSizeLimit]
        public async Task<ActionResult> Preview(Guid id, [FromForm] IFormFile file)
        {
            if (SettingsService.IsReadOnlyMode)
            {
                return JsonError(ReadOnlyModeMessage);
            }

            return await PostPreviewActionResult(
                async () => await _adminDatabase.UpdateDocumentModelPreview(id, file));
        }

        // GET: /DocumentModels/PreviewWithSampleText/00000000-0000-0000-0000-000000000000
        public ActionResult PreviewWithSampleText(Guid id)
        {
            var model = _adminDatabase.GetDocumentModel(id);

            if ((model.Template == null) || (model.Template.Length == 0))
            {
                return NotFound();
            }

            return File(model.PreviewImageWithSampleText, "image/*", model.PreviewImageWithSampleTextFileName);
        }

        // POST: /DocumentModels/PreviewWithSampleText/00000000-0000-0000-0000-000000000000
        [HttpPost]
        [DisableRequestSizeLimit]
        public async Task<ActionResult> PreviewWithSampleText(Guid id, [FromForm] IFormFile file)
        {
            if (SettingsService.IsReadOnlyMode)
            {
                return JsonError(ReadOnlyModeMessage);
            }

            return await PostPreviewActionResult(
                async () => await _adminDatabase.UpdateDocumentModelPreviewWithSampleText(id, file));
        }

        // GET: /DocumentModels/Structure/00000000-0000-0000-0000-000000000000
        public ActionResult Structure(Guid id)
        {
            var docModel = _adminDatabase.GetDocumentModel(id);
            var viewModel = new DocumentModelsStructureViewModel(
                _adminDatabase.GetDocumentStructures(id, StoryType.Header),
                _adminDatabase.GetDocumentStructures(id, PositionType.Begin),
                _adminDatabase.GetDocumentStructures(id, PositionType.Body),
                _adminDatabase.GetDocumentStructures(id, PositionType.End),
                _adminDatabase.GetDocumentStructures(id, StoryType.Footer),
                _adminDatabase.GetDocumentStructures(id, PositionType.Cursor));
            ViewBag.DocumentModelName = docModel.DisplayName;
            return View(viewModel);
        }

        // PUT: /DocumentModels/Structure/00000000-0000-0000-0000-000000000000
        [HttpPut]
        public ActionResult Structure(Guid id, [FromBody] DocumentModelsStructureViewModel model)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                _adminDatabase.EditDocumentStructures(id, model);
                return JsonResult("Structure has been saved.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, ex.Message);
            }
        }

        // GET: /DocumentModels/StructureBricks?text=<Text>&filteredByBrickGroups=<FilteredByBrickGroups>
        public ActionResult StructureBricks([FromQuery] string text, [FromQuery] bool filteredByBrickGroups)
        {
            if (filteredByBrickGroups)
            {
                return new JsonResult(_adminDatabase.GetBricksByGroupName(text));
            }

            return new JsonResult(_adminDatabase.GetBricks(text));
        }

        public ActionResult MetadataDefinitions(Guid id)
        {
            try
            {
                var docModel = _adminDatabase.GetDocumentModelIncludingMetadataDefinitions(id);
                var associatedMetadataDefinitions = docModel.DocumentModelMetadataDefinitions?.Where(md => !md.Deleted)
                                                            .OrderBy(md => md.MetadataDefinition.Position)
                                                            .Select(_mapper.Map<AssociatedMetadataDefinitionViewModel>);

                var viewModel = new AssociatedMetadataDefinitionsViewModel
                {
                    DocumentModelId = id,
                    AssociatedMetadataDefinitions = associatedMetadataDefinitions ?? new List<AssociatedMetadataDefinitionViewModel>(),
                };

                UpdateUserGroupDataModels(viewModel);

                ViewBag.DocumentModelName = docModel.DisplayName;
                return View(viewModel);
            }
            catch (Exception ex)
            {
                return HandleError(ex, ex.Message);
            }
        }

        [HttpPut]
        public async Task<ActionResult> MetadataDefinitions([FromBody] AssociatedMetadataDefinitionsViewModel model)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                model.AssociatedMetadataDefinitions ??= new List<AssociatedMetadataDefinitionViewModel>();
                await _metadataDatabase.EditDocumentModelMetadataDefinitionsAsync(model);
                return JsonResult("Metadata definitions have been saved.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, ex.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult> MetadataDefinitions(Guid id, [FromQuery] Guid sourceDocumentModelId)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                if (id == sourceDocumentModelId)
                {
                    return JsonError("Source and target document model identical. Abort");
                }

                await _metadataDatabase.CopyDocumentModelMetadataDefinitionsAsync(sourceDocumentModelId, id);
                return JsonResult("Metadata definitions have been copied.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, ex.Message);
            }
        }

        // POST: /DocumentModels/Export
        [HttpPost]
        public async Task<ActionResult> Export(
            [FromBody] List<DocumentModelDataModel> documentModelVms,
            [FromQuery] bool includeStructures,
            [FromQuery] bool includeBricks,
            [FromQuery] bool includeDocumentModelMetadataDefinitions,
            [FromQuery] bool includeMetadataDefinitions,
            [FromQuery] bool includeAuthorRoles)
        {
            try
            {
                var exportPackage = await _exportService.ExportDocumentModels(
                    documentModelVms,
                    includeStructures,
                    includeBricks,
                    includeDocumentModelMetadataDefinitions,
                    includeMetadataDefinitions,
                    includeAuthorRoles);

                return JsonResult(new { PackageId = exportPackage.Id });
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The data package could not be created. {ex.GetInnerException().Message}");
            }
        }

        private async Task<ActionResult> PostPreviewActionResult(Func<Task<byte[]>> dbUpdateFunc)
        {
            try
            {
                var bytes = await dbUpdateFunc();
                return JsonResult(null, Tools.GetBase64Image(bytes));
            }
            catch (Exception ex)
            {
                return HandleError(ex, ex.Message);
            }
        }

        private void UpdateUserGroupDataModels(AssociatedMetadataDefinitionsViewModel viewModel)
        {
            var userGroups = _adminDatabase.GetAllUserGroups();
            if (userGroups == null)
            {
                return;
            }

            var updatedMetadataDefinitions = new List<AssociatedMetadataDefinitionViewModel>();
            foreach (var definition in viewModel.AssociatedMetadataDefinitions)
            {
                definition.AllUserGroupDataModels =
                    userGroups.Select(
                                  ug => new UserGroupDataModel(ug)
                                  {
                                      IsSelected = definition.IsEditableByUserGroup(ug.Id),
                                  })
                              .OrderBy(ug => ug.UiPositionIndex)
                              .ToList();

                updatedMetadataDefinitions.Add(definition);
            }

            viewModel.AssociatedMetadataDefinitions = updatedMetadataDefinitions;
        }
    }
}
