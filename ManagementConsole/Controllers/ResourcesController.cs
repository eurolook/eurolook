﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Areas.DataExchange;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Controllers
{
    public class ResourcesController : EurolookController
    {
        private readonly IAdminDatabase _adminDatabase;
        private readonly ILanguageRepository _languageRepository;
        private readonly ExportService _exportService;

        public ResourcesController(
            ISettingsService settingsService,
            IAdminDatabase adminDatabase,
            ILanguageRepository languageRepository,
            ExportService exportService)
            : base(settingsService)
        {
            _adminDatabase = adminDatabase;
            _languageRepository = languageRepository;
            _exportService = exportService;
        }

        // GET: /Resources/
        public ActionResult Index(
            string q = null,
            uint p = 1,
            uint s = 20,
            string o = "alias",
            bool asc = true,
            bool deleted = false)
        {
            if (Request.IsAjaxRequest())
            {
                var page = new DataPage<Resource>(p, s, asc, o);
                page.Data.AddRange(_adminDatabase.GetResources(page, q, deleted));
                page.TotalCount = (uint)_adminDatabase.GetResourcesCount(q, deleted);
                return JsonResult(page);
            }

            return View();
        }

        // POST: /Resources/Create
        [HttpPost]
        public ActionResult Create([FromBody] ResourceDataModel resource)
        {
            try
            {
                var newResource = _adminDatabase.CreateResource(resource);
                return JsonResult("The resource has been created.", newResource);
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The resource could not be created. {ex.GetInnerException().Message}");
            }
        }

        // DELETE: /Resources/Delete/00000000-0000-0000-0000-000000000000
        [HttpDelete]
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                _adminDatabase.DeleteResource(id);
                return JsonResult("The resource has been deleted.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The resource could not be deleted. {ex.GetInnerException().Message}");
            }
        }

        // GET: /DocumentModels/Edit/00000000-0000-0000-0000-000000000000
        public ActionResult Edit(Guid id)
        {
            if (Request.IsAjaxRequest())
            {
                var isSystemResource = _adminDatabase.GetSystemResources().Where(r => r.ResourceId == id).Any();
                var viewModel = new ResourceEditDataModel(
                    _adminDatabase.GetResourceForEdit(id),
                    _adminDatabase.GetBricksForResource(id),
                    _languageRepository.GetAllLanguages(),
                    isSystemResource);

                return JsonResult(viewModel);
            }

            return View(id);
        }

        // PUT: /Resources/Edit/00000000-0000-0000-0000-000000000000
        [HttpPut]
        public ActionResult Edit(Guid id, [FromBody] ResourceEditDataModel resource)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                if (!ModelState.IsValid)
                {
                    return JsonError("The resource could not be updated. Please check the errors below.");
                }

                _adminDatabase.UpdateResource(resource);
                return JsonResult("The resource has been updated successfully!");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The resource could not be updated. {ex.GetInnerException().Message}");
            }
        }

        // GET: /Resources/Data/00000000-0000-0000-0000-000000000000
        public ActionResult Data(Guid id)
        {
            var resource = _adminDatabase.GetResource(id);

            if (resource.RawData == null)
            {
                return NotFound();
            }

            return File(resource.RawData, resource.MimeType, resource.GetFileName());
        }

        // POST: /Resources/Data/00000000-0000-0000-0000-000000000000?isModified=b
        [HttpPost]
        [DisableRequestSizeLimit]
        [SuppressMessage("csharpsquid", "S5693:Make sure the content length limit is safe here.", Justification = "API endpoint is internal only.")]
        public async Task<ActionResult> Data(Guid id, [FromQuery] bool isModified, IFormFile file, string fileName)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                if (isModified)
                {
                    await _adminDatabase.UpdateResourceRawData(id, file, fileName);
                }

                return JsonResult(null);
            }
            catch (Exception ex)
            {
                return HandleError(ex, ex.Message);
            }
        }

        // POST: /Resources/LocalisedResource/00000000-0000-0000-0000-000000000000
        [HttpPost]
        public async Task<ActionResult> LocalisedResource(
            Guid id,
            [FromQuery] Guid languageId,
            [FromForm] IFormFile file,
            [FromQuery] string fileNameDate)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                await _adminDatabase.UpdateLocalisedResource(id, languageId, file, fileNameDate);
                return JsonResult(null);
            }
            catch (Exception ex)
            {
                return new JsonResult(
                    HttpStatusCode.InternalServerError,
                    new
                    {
                        Success = false,
                        ex.Message,
                    });
            }
        }

        // GET: /Resources/LocalisedResource/00000000-0000-0000-0000-000000000000?languageId=00000000-0000-0000-0000-000000000000
        public ActionResult LocalisedResource(Guid id, [FromQuery] Guid languageId)
        {
            var lr = _adminDatabase.GetLocalisedResource(id, languageId);
            if (lr?.RawData == null)
            {
                return NotFound();
            }

            return File(lr.RawData, lr.MimeType, lr.GetFileName());
        }

        // POST: /Resources/Export
        [HttpPost]
        public async Task<ActionResult> Export(
            [FromBody] List<ResourceDataModel> resources,
            [FromQuery] bool includeDefaultData,
            [FromQuery] bool includeLocalisations)
        {
            try
            {
                var exportPackage = await _exportService.ExportResources(
                    resources,
                    includeDefaultData,
                    includeLocalisations);

                return JsonResult(new { PackageId = exportPackage.Id });
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The data package could not be created. {ex.GetInnerException().Message}");
            }
        }
    }
}
