using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Areas.DataExchange;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Eurolook.ManagementConsole.ViewModels.Address;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Controllers
{
    public class AddressController : EurolookController
    {
        private readonly IAddressDatabase _addressDatabase;
        private readonly ExportService _exportService;

        public AddressController(
            ISettingsService settingsService,
            IAddressDatabase addressDatabase,
            ExportService exportService)
            : base(settingsService)
        {
            _addressDatabase = addressDatabase;
            _exportService = exportService;
        }

        // GET: /Address/
        // GET: /Address/?q=XYZ&p=1
        public ActionResult Index(
            string q = null,
            uint p = 1,
            uint s = 20,
            string o = "name",
            bool asc = true,
            bool deleted = false)
        {
            if (Request.IsAjaxRequest())
            {
                var page = new DataPage<Address>(p, s, asc, o);
                page.Data.AddRange(_addressDatabase.GetAddresses(page, q, deleted));
                page.TotalCount = (uint)_addressDatabase.GetAddressCount(q, deleted);
                return JsonResult(page);
            }

            return View();
        }

        public async Task<JsonResult> All()
        {
            var addresses = await _addressDatabase.GetAllAddressesAsync();
            return JsonResult(addresses);
        }

        [HttpDelete]
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                await _addressDatabase.DeleteAddress(id, true);
                return JsonResult("The address has been deleted.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The address could not be deleted. {ex.GetInnerException().Message}");
            }
        }

        // POST: /Address/Create
        [HttpPost]
        public ActionResult Create([FromBody] AddressViewModel model)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                var address = _addressDatabase.CreateAddress(model);
                return JsonResult("The address has been created.", new AddressViewModel(address));
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The address could not be created. {ex.GetInnerException().Message}");
            }
        }

        [HttpPost]
        [ActionName("NameText")]
        public JsonResult CreateNameText(Guid id)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                string alias = _addressDatabase.CreateNameText(id);
                return JsonResult("The name text has been created.", alias);
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The name text could not be created. {ex.GetInnerException().Message}");
            }
        }

        // GET: /Address/Edit/00000000-0000-0000-0000-000000000000
        public ActionResult Edit(Guid id)
        {
            if (Request.IsAjaxRequest())
            {
                var addressVm = _addressDatabase.GetAddressForEdit(id);
                if (addressVm != null)
                {
                    return JsonResult(addressVm);
                }

                return JsonError($"Could not find address with id {id}");
            }

            ViewBag.Id = id;
            return View(id);
        }

        // PUT: /Address/Edit/00000000-0000-0000-0000-000000000000
        [HttpPut]
        public JsonResult Edit(Guid id, [FromBody] AddressEditDataModel model)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                _addressDatabase.UpdateAddress(id, model);
                return JsonResult("The address has been updated.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The address could not be updated. {ex.GetInnerException().Message}");
            }
        }

        // POST: /Address/Export
        [HttpPost]
        public async Task<ActionResult> Export(
            [FromBody] List<AddressViewModel> addressVms,
            [FromQuery] bool includeDependencies)
        {
            try
            {
                var exportPackage = await _exportService.ExportAddresses(addressVms, includeDependencies);
                return JsonResult(new { PackageId = exportPackage.Id });
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The data package could not be created. {ex.GetInnerException().Message}");
            }
        }

        [HttpPost]
        public async Task<JsonResult> Restore(Guid id)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                await _addressDatabase.RestoreAddress(id, false);
                return JsonResult("The address has been revived.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The address could not be revived. {ex.GetInnerException().Message}");
            }
        }
    }
}
