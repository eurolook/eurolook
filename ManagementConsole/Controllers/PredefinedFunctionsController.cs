using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Areas.DataExchange;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Controllers
{
    public class PredefinedFunctionsController : EurolookController
    {
        private readonly IPredefinedFunctionsDatabase _predefinedFunctionsDatabase;
        private readonly ExportService _exportService;

        public PredefinedFunctionsController(
            ISettingsService settingsService,
            IPredefinedFunctionsDatabase predefinedFunctionsDatabase,
            ExportService exportService)
            : base(settingsService)
        {
            _predefinedFunctionsDatabase = predefinedFunctionsDatabase;
            _exportService = exportService;
        }

        // GET: /PredefinedFunctions/
        // GET: /PredefinedFunctions/?q=XYZ&p=1
        public ActionResult Index(
            string q = null,
            uint p = 1,
            uint s = 20,
            string o = "name",
            bool asc = true,
            bool deleted = false)
        {
            if (Request.IsAjaxRequest())
            {
                var page = new DataPage<PredefinedFunction>(p, s, asc, o);
                page.Data.AddRange(_predefinedFunctionsDatabase.GetPredefinedFunctions(page, q, deleted));
                page.TotalCount = (uint)_predefinedFunctionsDatabase.GetPredefinedFunctionCount(q, deleted);
                return JsonResult(page);
            }

            return View();
        }

        // POST: /PredefinedFunctions/Create
        [HttpPost]
        public async Task<ActionResult> Create([FromBody] PredefinedFunctionDataModel model)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                var predefinedFunction = await _predefinedFunctionsDatabase.CreatePredefinedFunction(model);
                return JsonResult(
                    "The predefined function has been created.",
                    new PredefinedFunctionDataModel(predefinedFunction));
            }
            catch (Exception ex)
            {
                return HandleError(
                    ex,
                    $"The predefined function could not be created. {ex.GetInnerException().Message}");
            }
        }

        // GET: /PredefinedFunctions/Edit/00000000-0000-0000-0000-000000000000
        public async Task<ActionResult> Edit(Guid id)
        {
            if (Request.IsAjaxRequest())
            {
                var functionEditModel = await _predefinedFunctionsDatabase.GetPredefinedFunctionForEdit(id);
                return functionEditModel != null
                    ? JsonResult(functionEditModel)
                    : JsonError($"Could not find predefined function with id {id}");
            }

            return View(id);
        }

        // PUT: /PredefinedFunctions/Edit/00000000-0000-0000-0000-000000000000
        [HttpPut]
        public async Task<JsonResult> Edit(Guid id, [FromBody] PredefinedFunctionEditDataModel model)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                await _predefinedFunctionsDatabase.UpdatePredefinedFunction(id, model);
                return JsonResult("The predefined function has been updated.");
            }
            catch (Exception ex)
            {
                return HandleError(
                    ex,
                    $"The predefined function could not be updated. {ex.GetInnerException().Message}");
            }
        }

        [HttpPost]
        public async Task<JsonResult> FemaleFunction(Guid id)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                string textAlias = await _predefinedFunctionsDatabase.CreateFemaleFunction(id);
                return JsonResult("The female function has been created.", textAlias);
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The female function could not be created. {ex.GetInnerException().Message}");
            }
        }

        [HttpPost]
        public async Task<JsonResult> HeaderFunction(Guid id)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                string textAlias = await _predefinedFunctionsDatabase.CreateHeaderFunction(id);
                return JsonResult("The header function has been created.", textAlias);
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The header function could not be created. {ex.GetInnerException().Message}");
            }
        }

        [HttpPost]
        public async Task<JsonResult> HeaderFemaleFunction(Guid id)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                string textAlias = await _predefinedFunctionsDatabase.CreateFemaleHeaderFunction(id);
                return JsonResult("The female header function has been created.", textAlias);
            }
            catch (Exception ex)
            {
                return HandleError(
                    ex,
                    $"The female header function could not be created. {ex.GetInnerException().Message}");
            }
        }

        // DELETE: /PredefinedFunctions/Delete/00000000-0000-0000-0000-000000000000
        [HttpDelete]
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                await _predefinedFunctionsDatabase.DeletePredefinedFunction(id, true);
                return JsonResult("The predefined function has been deleted (including text and translations).");
            }
            catch (Exception ex)
            {
                return HandleError(
                    ex,
                    $"The predefined function could not be deleted. {ex.GetInnerException().Message}");
            }
        }

        // POST: /PredefinedFunctions/Revive/00000000-0000-0000-0000-000000000000
        [HttpPost]
        public async Task<JsonResult> Revive(Guid id)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                await _predefinedFunctionsDatabase.DeletePredefinedFunction(id, false);
                return JsonResult("The predefined function has been revived (including text and translations).");
            }
            catch (Exception ex)
            {
                return HandleError(
                    ex,
                    $"The predefined function could not be revived. {ex.GetInnerException().Message}");
            }
        }

        // POST: /PredefinedFunctions/Export
        [HttpPost]
        public async Task<ActionResult> Export(
            [FromBody] List<PredefinedFunctionDataModel> predefinedFunctions,
            [FromQuery] bool includeDependencies)
        {
            try
            {
                var exportPackage = await _exportService.ExportPredefinedFunctions(
                    predefinedFunctions,
                    includeDependencies);
                return JsonResult(new { PackageId = exportPackage.Id });
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The data package could not be created. {ex.GetInnerException().Message}");
            }
        }
    }
}
