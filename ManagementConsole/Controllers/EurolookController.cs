using System;
using System.Text.Json;
using Eurolook.Common.Log;
using Eurolook.ManagementConsole.Services;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Controllers
{
    [ResponseCache(NoStore = true, Duration = 0)]
    public abstract class EurolookController : Controller, ICanLog
    {
        protected EurolookController(ISettingsService settingsService)
        {
            SettingsService = settingsService;
        }

        public ISettingsService SettingsService { get; }

        public string ReadOnlyModeMessage => "This operation cannot be executed in read-only mode.";

        public override ViewResult View()
        {
            ViewBag.Baseurl = SettingsService.BaseUrl;
            ViewBag.IsReadOnlyMode = SettingsService.IsReadOnlyMode;
            return base.View();
        }

        public override ViewResult View(string viewName)
        {
            ViewBag.Baseurl = SettingsService.BaseUrl;
            ViewBag.IsReadOnlyMode = SettingsService.IsReadOnlyMode;
            return base.View(viewName);
        }

        public override ViewResult View(object model)
        {
            ViewBag.Baseurl = SettingsService.BaseUrl;
            ViewBag.IsReadOnlyMode = SettingsService.IsReadOnlyMode;
            return base.View(model);
        }

        public override ViewResult View(string viewName, object model)
        {
            ViewBag.Baseurl = SettingsService.BaseUrl;
            ViewBag.IsReadOnlyMode = SettingsService.IsReadOnlyMode;
            return base.View(viewName, model);
        }

        public override FileContentResult File(byte[] fileContents, string contentType, string fileDownloadName)
        {
            Response.Headers.CacheControl = "private, max-age=0";
            return base.File(fileContents, contentType, fileDownloadName);
        }

        protected JsonResult JsonResult(object model)
        {
            Response.Headers.CacheControl = "private, max-age=0";
            return new JsonResult(
                new
                {
                    Success = true,
                    Model = model,
                });
        }

        protected JsonResult JsonResult(string message)
        {
            Response.Headers.CacheControl = "private, max-age=0";
            return new JsonResult(
                new
                {
                    Success = true,
                    Message = message,
                });
        }

        protected JsonResult JsonResult(string message, object model)
        {
            Response.Headers.CacheControl = "private, max-age=0";
            return new JsonResult(
                new
                {
                    Success = true,
                    Message = message,
                    Model = model,
                });
        }

        protected JsonResult JsonError(string message)
        {
            Response.Headers.CacheControl = "private, max-age=0";
            return new JsonResult(
                new
                {
                    Success = false,
                    Message = message,
                });
        }

        protected JsonResult HandleError(Exception ex, string failureMessage)
        {
            this.LogError($"{Request.Method} {Request.GetDisplayUrl()} failed", ex);
            return JsonError(failureMessage);
        }
    }
}
