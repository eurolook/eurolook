﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Common.Extensions;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Eurolook.ManagementConsole.ViewModels.OrgaChart;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Controllers
{
    public class StatisticsController : EurolookController
    {
        private readonly IAdminDatabase _adminDatabase;
        private readonly StatisticsDatabase _statisticsDatabase;

        public StatisticsController(
            ISettingsService settingsService,
            IAdminDatabase adminDatabase,
            StatisticsDatabase statisticsDatabase)
            : base(settingsService)
        {
            _adminDatabase = adminDatabase;
            _statisticsDatabase = statisticsDatabase;
        }

        private enum DateFilter
        {
            LastWeek = 0,
            Last2Weeks = 1,
            LastMonth = 2,
            Last3Months = 3,
            Last6Months = 4,
            Last12Months = 5,
            Last2Years = 6,
            Last3Years = 7,
            NoFilter = 99,
        }

        private enum DeviceFilter
        {
            ExcludingTerminalServer = 1,
            OnlyTerminalServer = 2,
            AllDevices = 99,
        }

        // GET: Statistics
        public ActionResult Index()
        {
            return RedirectToAction("Devices");
        }

        // GET: Statistics/Devices
        public ActionResult Devices()
        {
            return View();
        }

        // GET: Statistics/DocumentUsage
        public ActionResult DocumentUsage()
        {
            return View();
        }

        // GET: Statistics/TemplateStoreUsage
        public ActionResult TemplateStoreUsage()
        {
            return View();
        }

        // GET: Statistics/BrickUsage
        public ActionResult BrickUsage()
        {
            return View();
        }

        // GET: Statistics/Customisation
        public ActionResult Customisation()
        {
            return View();
        }

        // GET: Statistics/AdLink
        public ActionResult AdLink()
        {
            return View();
        }

        // GET: Statistics/Activity
        public ActionResult Activity()
        {
            return View();
        }

        // GET: Statistics/Help
        public ActionResult Help()
        {
            return View();
        }

        // GET: Statistics/CustomBricks
        public ActionResult CustomBricks()
        {
            return View();
        }

        public JsonResult DgList()
        {
            var dgs = _adminDatabase.GetAllDgs().Select(o => new OrgaEntityViewModel(o)).ToList();
            dgs.Insert(
                0,
                new OrgaEntityViewModel
                {
                    DisplayName = "All DGs",
                    Id = Guid.Empty,
                });
            return new JsonResult(new { dgs });
        }

        [HttpGet]
        [ActionName("cepmembership")]
        public JsonResult GetCepMembershipStatistics(int id)
        {
            var fromDate = GetStartDateUtc((DateFilter)id);
            int totalUserCount = _statisticsDatabase.GetUserCount(fromDate);
            int cepMemberCount = _statisticsDatabase.GetCepMemberCount(fromDate);
            var data = new[]
            {
                new PieChartSegment
                {
                    Name = "Enabled",
                    Y = cepMemberCount,
                },
                new PieChartSegment
                {
                    Name = "Disabled",
                    Y = totalUserCount - cepMemberCount,
                },
            };
            return new JsonResult(data);
        }

        [HttpGet]
        [ActionName("stylesbox")]
        public JsonResult GetStylesboxStatistics(int id)
        {
            var fromDate = GetStartDateUtc((DateFilter)id);
            int totalUserCount = _statisticsDatabase.GetUserCount(fromDate);
            int stylesBoxCount = _statisticsDatabase.GetStylesBoxEnabled(fromDate);
            var data = new[]
            {
                new PieChartSegment
                {
                    Name = "Enabled (Default)",
                    Y = stylesBoxCount,
                },
                new PieChartSegment
                {
                    Name = "Disabled",
                    Y = totalUserCount - stylesBoxCount,
                },
            };
            return new JsonResult(data);
        }

        [HttpGet]
        [ActionName("colorscheme")]
        public JsonResult GetColorSchemeStatistics(int id)
        {
            var fromDate = GetStartDateUtc((DateFilter)id);
            return new JsonResult(_statisticsDatabase.GetColorSchemeChart(fromDate));
        }

        [HttpGet]
        [ActionName("adlinkstatus")]
        public JsonResult GetAdLinkStatus(Guid id)
        {
            var data = new ChartData();
            data.Categories.AddRange(
                new[] { "Email", "Org.Acronym", "Workplace", "Office", "Phone" });
            var linkedSeries = new ChartSeries
            {
                Name = "Linked",
                Data = new int[5],
            };
            data.Series.Add(linkedSeries);
            var unlinkedSeries = new ChartSeries
            {
                Name = "Unlinked",
                Data = new int[5],
            };
            data.Series.Add(unlinkedSeries);

            _statisticsDatabase.LoadAdLinkStatus(linkedSeries, unlinkedSeries, id);

            return new JsonResult(data);
        }

        [HttpGet]
        [ActionName("versionusage")]
        public JsonResult GetVersionUsage(int id, int? deviceFilter)
        {
            var fromDateUtc = GetStartDateUtc((DateFilter)id);
            var filterRegex = GetDeviceFilterRegex((DeviceFilter)(deviceFilter ?? 99));
            var versionInfos = _statisticsDatabase.GetVersionUsageInfo(fromDateUtc, filterRegex)
                                                  .OrderByDescending(di => di.Version).ToArray();

            var data = new ChartData();
            data.Categories.AddRange(versionInfos.Select(vi => vi.Version));

            var seriesEntries = new Dictionary<string, int[]>();
            for (int i = 0; i < versionInfos.Length; i++)
            {
                var vi = versionInfos[i];
                foreach (string ownerName in vi.DeviceOwners.Keys)
                {
                    if (!seriesEntries.ContainsKey(ownerName))
                    {
                        seriesEntries[ownerName] = new int[versionInfos.Length];
                    }

                    var owner = vi.DeviceOwners[ownerName];
                    seriesEntries[ownerName][i] = owner.DeviceCount;
                }
            }

            foreach (var seriesEntry in seriesEntries)
            {
                data.Series.Add(
                    new ChartSeries
                    {
                        Name = seriesEntry.Key,
                        Data = seriesEntry.Value,
                    });
            }

            return new JsonResult(data);
        }

        [HttpGet]
        [ActionName("versionscount")]
        public JsonResult GetVersionsCount(int id, int? deviceFilter)
        {
            var fromDateUtc = GetStartDateUtc((DateFilter)id);
            var filterRegex = GetDeviceFilterRegex((DeviceFilter)(deviceFilter ?? 99));
            var versionCounts = _statisticsDatabase.GetVersionCounts(fromDateUtc, filterRegex)
                                                   .OrderByDescending(di => di.Version).ToArray();
            var data = versionCounts.Select(
                                        versionInfo => new PieChartSegment
                                        {
                                            Name = versionInfo.Version,
                                            Y = versionInfo.DeviceCount,
                                        })
                                    .ToList();

            return new JsonResult(data);
        }

        [HttpGet]
        [ActionName("deviceowners")]
        public JsonResult GetDeviceOwners(int id, int? deviceFilter)
        {
            var fromDateUtc = GetStartDateUtc((DateFilter)id);
            var filterRegex = GetDeviceFilterRegex((DeviceFilter)(deviceFilter ?? 99));
            var deviceOwners = _statisticsDatabase.GetDeviceOwners(fromDateUtc, filterRegex)
                                                  .OrderByDescending(x => x.Value).ToArray();

            var data = deviceOwners.Select(
                                       entry => new PieChartSegment
                                       {
                                           Name = entry.Key,
                                           Y = entry.Value,
                                       })
                                   .ToList();

            return new JsonResult(data);
        }

        [HttpGet]
        [ActionName("brickusagedata")]
        [SuppressMessage(
            "Sonar",
            "S4143:Collection elements should not be replaced unconditionally",
            Justification = "False positive")]
        public JsonResult GetBrickUsageData(int id, Guid? documentTypeToInclude)
        {
            var fromDate = GetStartDateUtc((DateFilter)id);
            var actionLogInfos = _statisticsDatabase.GetActionLogInfos(fromDate, documentTypeToInclude)
                                                    .OrderByDescending(bui => bui.TotalCount).ToArray();

            var data = new ChartData();
            data.Categories.AddRange(actionLogInfos.Select(x => x.Name));
            var clickSeries = new ChartSeries { Name = "Click" };
            var hotKeySeries = new ChartSeries { Name = "Hotkey" };
            var handlerSeries = new ChartSeries { Name = "Handler" };
            var ribbonSeries = new ChartSeries { Name = "Ribbon" };
            clickSeries.Data = new int[actionLogInfos.Length];
            hotKeySeries.Data = new int[actionLogInfos.Length];
            handlerSeries.Data = new int[actionLogInfos.Length];
            ribbonSeries.Data = new int[actionLogInfos.Length];
            for (int i = 0; i < actionLogInfos.Length; i++)
            {
                var action = actionLogInfos[i];
                clickSeries.Data[i] = action.ClickCount;
                hotKeySeries.Data[i] = action.HotKeyCount;
                handlerSeries.Data[i] = action.HandlerCount;
                ribbonSeries.Data[i] = action.RibbonCount;
            }

            data.Series.Add(hotKeySeries);
            data.Series.Add(clickSeries);
            data.Series.Add(handlerSeries);
            data.Series.Add(ribbonSeries);
            return new JsonResult(data);
        }

        [HttpPost]
        [ActionName("brickusageovertime")]
        public JsonResult GetBrickUsageOverTime(
            int id,
            [FromBody] BrickUsageHistoryRequest request)
        {
            var fromDate = GetTimeSpanStartDateUtc((DateFilter)id);
            var brickHistory = _statisticsDatabase.GetBrickUsageHistory(fromDate, request.DocumentTypeToInclude);
            var data = new ChartData();

            if (request.BricksToInclude == null)
            {
                return new JsonResult(data);
            }

            data.Categories.AddRange(brickHistory.Categories);

            var counts = brickHistory.Counts.Values.Where(e => request.BricksToInclude.Contains(e.Name))
                                     .OrderByDescending(v => v.TotalCount);
            foreach (var count in counts)
            {
                data.Series.Add(
                    new ChartSeries
                    {
                        Name = count.Name,
                        Data = count.Count,
                    });
            }

            var bricksWithoutCount =
                request.BricksToInclude.Where(b => !brickHistory.Counts.Values.Select(e => e.Name).Contains(b));
            foreach (string brick in bricksWithoutCount)
            {
                data.Series.Add(
                    new ChartSeries
                    {
                        Name = brick,
                        Data = new int[brickHistory.Dates.Count],
                    });
            }

            return new JsonResult(data);
        }

        [HttpGet]
        [ActionName("bricknamesbycategory")]
        public JsonResult GetBrickNamesInLog()
        {
            var bricks = _statisticsDatabase.GetBricksByCategory();
            var fromDate = GetTimeSpanStartDateUtc(DateFilter.Last3Months);
            var actionLogInfos = _statisticsDatabase.GetActionLogInfos(fromDate, null);

            var result = new List<MultiSelectGroup>();
            foreach (string category in bricks.Keys)
            {
                result.Add(
                    new MultiSelectGroup
                    {
                        Label = category,
                        Options = bricks[category].ConvertAll(
                            brick => new MultiSelectOption
                            {
                                Checked = false,
                                Name = brick,
                                Value = brick,
                            }),
                    });
            }

            var allBricksIds = _adminDatabase.GetAllBricks().Select(b => b.Id);
            var nonBrickActionLogEntryNames = actionLogInfos.Where(i => !allBricksIds.Contains(i.Id) && i.Name != "(Deleted)");
            if (nonBrickActionLogEntryNames.Any())
            {
                result.Add(
                    new MultiSelectGroup
                    {
                        Label = "Non-brick actions (logged in the last 3 months)",
                        Options = nonBrickActionLogEntryNames.Select(
                            info => new MultiSelectOption
                            {
                                Checked = false,
                                Name = info.Name,
                                Value = info.Name,
                            }).ToList(),
                    });
            }

            return new JsonResult(result);
        }

        [HttpGet]
        [ActionName("documenttypesbycategory")]
        public JsonResult GetDocumentTypesInLog()
        {
            var documentTypes = _statisticsDatabase.GetDocumentModelsByCategory();

            var result = new List<SelectGroup>();
            foreach (string category in documentTypes.Keys)
            {
                result.Add(
                    new SelectGroup
                    {
                        Label = category,
                        Options = documentTypes[category].ConvertAll(
                            type => new SelectOption
                            {
                                Name = type.Name,
                                Value = type.Id,
                            }),
                    });
            }

            return new JsonResult(result);
        }

        [HttpGet]
        [ActionName("initstatus")]
        public JsonResult GetInitStatus(int id, int? deviceFilter)
        {
            var fromDateUtc = GetStartDateUtc((DateFilter)id);
            var filterRegex = GetDeviceFilterRegex((DeviceFilter)(deviceFilter ?? 99));

            int totalCount = _statisticsDatabase.GetDeviceCount(fromDateUtc, filterRegex);
            int initCount = _statisticsDatabase.GetInitialisedCount(fromDateUtc, filterRegex);
            var data = new[]
            {
                new PieChartSegment
                {
                    Name = "Initialised",
                    Y = initCount,
                },
                new PieChartSegment
                {
                    Name = "Not Initialised",
                    Y = totalCount - initCount,
                },
            };
            return new JsonResult(data);
        }

        [HttpGet]
        [ActionName("activityoverview")]
        public async Task<JsonResult> GetActivityOverview(int id)
        {
            var fromDate = GetTimeSpanStartDateUtc((DateFilter)id);
            var activeDeviceData = await _statisticsDatabase.GetActiveDevices(fromDate);

            var chartData = new ChartData();
            var deviceCounts = new List<int>();
            foreach (var month in activeDeviceData.Keys.OrderBy(d => d))
            {
                chartData.Categories.Add(month.ToString("y"));
                deviceCounts.Add(activeDeviceData[month]);
            }

            chartData.Series.Add(
                new ChartSeries
                {
                    Name = "Active* Devices",
                    Data = deviceCounts.ToArray(),
                });
            return new JsonResult(chartData);
        }

        [HttpGet]
        [ActionName("activitydetails")]
        public JsonResult GetActivityDetails(int id)
        {
            // because we cannot show today we need to remove it and show one more day in the past
            var days = new DateTime[0];
            var fromDate = GetStartDateUtc((DateFilter)id);
            if (fromDate != null)
            {
                var date = fromDate.Value.Subtract(new TimeSpan(1, 0, 0, 0)); // add a day in the past
                var dayList = GetLastDays(date).ToList();
                dayList.RemoveAt(0); // remove today
                dayList.Reverse();
                days = dayList.ToArray();
            }

            var deviceCounts = _statisticsDatabase.GetDeviceCount(days);
            var sessionCounts = _statisticsDatabase.GetSessionCounts(days);

            var data = new ChartData();
            data.Categories.AddRange(days.Select(d => d.ToShortDateString()));
            data.Series.Add(
                new ChartSeries
                {
                    Name = "Active* Devices",
                    Data = deviceCounts,
                });
            data.Series.Add(
                new ChartSeries
                {
                    Name = "Active* Word Sessions",
                    Data = sessionCounts,
                });
            return new JsonResult(data);
        }

        [HttpGet]
        [ActionName("documentcreationhistory")]
        public JsonResult GetDocumentCreationHistory(int id)
        {
            var fromDate = GetTimeSpanStartDateUtc((DateFilter)id);
            var creationHistory = _statisticsDatabase.GetDocumentCreationHistory(fromDate);
            var data = new ChartData();
            data.Categories.AddRange(creationHistory.Categories);
            foreach (var series in creationHistory.Series)
            {
                data.Series.Add(
                    new ChartSeries
                    {
                        Name = series.Name,
                        Data = series.Count,
                    });
            }

            return new JsonResult(data);
        }

        [HttpGet]
        [ActionName("documentcreation")]
        public JsonResult GetDocumentCreationData(int id)
        {
            var fromDate = GetStartDateUtc((DateFilter)id);
            var creationInfos = _statisticsDatabase.GetDocumentCreationInfosByLang(fromDate)
                                                   .OrderByDescending(dci => dci.TotalCreationCount).ToArray();

            var data = new ChartData();
            data.Categories.AddRange(creationInfos.Select(ci => ci.DocumentModelName));
            var seriesEntries = new Dictionary<string, int[]>();
            for (int i = 0; i < creationInfos.Length; i++)
            {
                var dci = creationInfos[i];
                foreach (var language in dci.DocumentCreations.Values)
                {
                    if (!seriesEntries.ContainsKey(language.LanguageName))
                    {
                        seriesEntries[language.LanguageName] = new int[creationInfos.Length];
                    }

                    seriesEntries[language.LanguageName][i] = language.CreationCount;
                }
            }

            foreach (var seriesEntry in seriesEntries)
            {
                data.Series.Add(
                    new ChartSeries
                    {
                        Name = seriesEntry.Key,
                        Data = seriesEntry.Value.ToArray(),
                    });
            }

            return new JsonResult(data);
        }

        [HttpGet]
        [ActionName("documentcreationbyentity")]
        public JsonResult GetDocumentCreationDataByEntity(int id, int limit)
        {
            var fromDate = GetStartDateUtc((DateFilter)id);
            var creationInfos = _statisticsDatabase.GetDocumentCreationInfosByEntity(fromDate)
                                                   .OrderByDescending(dci => dci.TotalCreationCount).Take(limit)
                                                   .ToArray();

            var data = new ChartData();
            data.Categories.AddRange(creationInfos.Select(ci => ci.EntityName));
            data.Series.Add(
                new ChartSeries
                {
                    Name = "Total count",
                    Data = creationInfos.Select(d => d.TotalCreationCount).ToArray(),
                });

            return new JsonResult(data);
        }

        [HttpGet]
        [ActionName("documentcreationbydg")]
        public JsonResult GetDocumentCreationDataByDg(int id, int limit)
        {
            var fromDate = GetStartDateUtc((DateFilter)id);
            var creationInfos = _statisticsDatabase.GetDocumentCreationInfosByDg(fromDate)
                                                   .OrderByDescending(dci => dci.TotalCreationCount).Take(limit)
                                                   .ToArray();

            var data = new ChartData();
            data.Categories.AddRange(creationInfos.Select(ci => ci.DgName));
            var seriesEntries = new Dictionary<string, int[]>();
            for (int i = 0; i < creationInfos.Length; i++)
            {
                var dci = creationInfos[i];
                foreach (var creator in dci.DocumentCreations.Values)
                {
                    if (!seriesEntries.ContainsKey(creator.DocumentModelName))
                    {
                        seriesEntries[creator.DocumentModelName] = new int[creationInfos.Length];
                    }

                    seriesEntries[creator.DocumentModelName][i] = creator.CreationCount;
                }
            }

            foreach (var seriesEntry in seriesEntries)
            {
                data.Series.Add(
                    new ChartSeries
                    {
                        Name = seriesEntry.Key,
                        Data = seriesEntry.Value.ToArray(),
                    });
            }

            return new JsonResult(data);
        }

        [HttpGet]
        [ActionName("mostusedtemplates")]
        public JsonResult GetMostUsedTemplates(int id)
        {
            var fromDate = GetStartDateUtc((DateFilter)id);
            var usageInfo = _statisticsDatabase.GetMostUsedTemplateStoreTemplates(fromDate);

            var data = new ChartData();
            data.Categories.AddRange(usageInfo.Select(ci => ci.DocumentModelName));
            var seriesEntries = new Dictionary<string, int[]>();
            for (int i = 0; i < usageInfo.Length; i++)
            {
                var dci = usageInfo[i];
                foreach (var language in dci.DocumentCreations.Values)
                {
                    if (!seriesEntries.ContainsKey(language.LanguageName))
                    {
                        seriesEntries[language.LanguageName] = new int[usageInfo.Length];
                    }

                    seriesEntries[language.LanguageName][i] = language.CreationCount;
                }
            }

            foreach (var seriesEntry in seriesEntries)
            {
                data.Series.Add(
                    new ChartSeries
                    {
                        Name = seriesEntry.Key,
                        Data = seriesEntry.Value.ToArray(),
                    });
            }

            return new JsonResult(data);
        }

        [HttpGet]
        [ActionName("templatesbymodel")]
        public JsonResult GetTemplatesByDocumentModel(int id)
        {
            var fromDate = GetStartDateUtc((DateFilter)id);
            var usageInfo = _statisticsDatabase.GetTemplateStoreDocumentsByDocumentModel(fromDate);

            var data = new ChartData();
            data.Categories.AddRange(usageInfo.Select(ci => ci.DocumentModelName));
            var seriesEntries = new Dictionary<string, int[]>();
            for (int i = 0; i < usageInfo.Length; i++)
            {
                var dci = usageInfo[i];
                foreach (var language in dci.DocumentCreations.Values)
                {
                    if (!seriesEntries.ContainsKey(language.LanguageName))
                    {
                        seriesEntries[language.LanguageName] = new int[usageInfo.Length];
                    }

                    seriesEntries[language.LanguageName][i] = language.CreationCount;
                }
            }

            foreach (var seriesEntry in seriesEntries)
            {
                data.Series.Add(
                    new ChartSeries
                    {
                        Name = seriesEntry.Key,
                        Data = seriesEntry.Value.ToArray(),
                    });
            }

            return new JsonResult(data);
        }

        [HttpGet]
        [ActionName("templatesbydg")]
        public JsonResult GetTemplatesByDG(int id)
        {
            var fromDate = GetStartDateUtc((DateFilter)id);
            var creationInfos = _statisticsDatabase.GetTemplateStoreDocumentsByOriginator(fromDate);

            var data = new ChartData();
            data.Categories.AddRange(creationInfos.Select(ci => ci.DgName));
            var seriesEntries = new Dictionary<string, int[]>();
            for (int i = 0; i < creationInfos.Length; i++)
            {
                var dci = creationInfos[i];
                foreach (var creator in dci.DocumentCreations.Values)
                {
                    if (!seriesEntries.ContainsKey(creator.DocumentModelName))
                    {
                        seriesEntries[creator.DocumentModelName] = new int[creationInfos.Length];
                    }

                    seriesEntries[creator.DocumentModelName][i] = creator.CreationCount;
                }
            }

            foreach (var seriesEntry in seriesEntries)
            {
                data.Series.Add(
                    new ChartSeries
                    {
                        Name = seriesEntry.Key,
                        Data = seriesEntry.Value.ToArray(),
                    });
            }

            return new JsonResult(data);
        }

        [HttpGet]
        [ActionName("defaultbricksdata")]
        public async Task<JsonResult> GetDefaultBricksData(int id)
        {
            var fromDateUtc = GetStartDateUtc((DateFilter)id);
            return new JsonResult(await _statisticsDatabase.GetDefaultBricksChartData(fromDateUtc));
        }

        public JsonResult GetHelpRequests(int id)
        {
            var fromDateUtc = GetStartDateUtc((DateFilter)id);
            if (fromDateUtc == null)
            {
                return null;
            }

            var entries = IssLogFileParserService.ParseLogFiles(
                SettingsService.IisLogFilesFolder,
                fromDateUtc.Value,
                "/Help/%.htm");
            var groupedEntries = entries.GroupBy(x => x.Url)
                                        .OrderByDescending(x => x.Count())
                                        .Where(
                                            x => !x.Key.EndsWith(
                                                     "Home.htm",
                                                     StringComparison.InvariantCultureIgnoreCase)
                                                 && !x.Key.EndsWith(
                                                     "Default.htm",
                                                     StringComparison.InvariantCultureIgnoreCase))
                                        .Take(40)
                                        .ToArray();

            var data = new ChartData();
            data.Categories.AddRange(groupedEntries.Select(x => x.Key));

            data.Series.Add(
                new ChartSeries
                {
                    Name = "Request Count",
                    Data = groupedEntries.Select(x => x.Count()).ToArray(),
                });

            return new JsonResult(data);
        }

        public JsonResult GetCustomBricksUsage(int id)
        {
            var fromDateUtc = GetStartDateUtc((DateFilter)id);

            int totalUserCount = _statisticsDatabase.GetUserCount(fromDateUtc);
            int usageCount = _statisticsDatabase.GetCustomBricksUsageCount(fromDateUtc);
            var data = new[]
            {
                new PieChartSegment
                {
                    Name = "Using Custom Bricks",
                    Y = usageCount,
                },
                new PieChartSegment
                {
                    Name = "Not using Custom Bricks",
                    Y = totalUserCount - usageCount,
                },
            };

            return new JsonResult(data);
        }

        public JsonResult GetCustomBricksCountPerUser(int id)
        {
            var fromDateUtc = GetStartDateUtc((DateFilter)id);

            var data = _statisticsDatabase.GetCustomBricksCountPerUser(fromDateUtc);
            var orderedData = new Dictionary<int, int>();
            foreach (int key in data.OrderBy(x => x.Key).Select(x => x.Key))
            {
                orderedData[key] = data[key];
            }

            var chartData = new ChartData();
            chartData.Categories.AddRange(orderedData.Keys.Select(x => x.ToString()));
            chartData.Series.Add(
                new ChartSeries
                {
                    Name = "Custom Bricks Count",
                    Data = orderedData.Values.ToArray(),
                });

            return new JsonResult(chartData);
        }

        public JsonResult GetCustomBricksSizePerUser(int id)
        {
            var fromDateUtc = GetStartDateUtc((DateFilter)id);

            var data = _statisticsDatabase.GetCustomBricksSizePerUser(fromDateUtc);
            var orderedData = new Dictionary<int, int>();
            foreach (int key in data.OrderBy(x => x.Key).Select(x => x.Key))
            {
                orderedData[key] = data[key];
            }

            var chartData = new ChartData();
            chartData.Categories.AddRange(orderedData.Keys.Select(x => x.ToString()));
            chartData.Series.Add(
                new ChartSeries
                {
                    Name = "Custom Brick Size (kb)",
                    Data = orderedData.Values.ToArray(),
                });

            return new JsonResult(chartData);
        }

        private static DateTime[] GetLastDays(DateTime fromDateUtc)
        {
            var date = DateTime.UtcNow.Date;
            var result = new List<DateTime>();
            while (date != fromDateUtc)
            {
                result.Add(date);
                date = date.AddDays(-1);
            }

            return result.ToArray();
        }

        private static DateTime? GetStartDateUtc(DateFilter filter)
        {
            var utcNow = DateTime.UtcNow.Date;
            switch (filter)
            {
                case DateFilter.LastWeek:
                    return utcNow.Subtract(new TimeSpan(7, 0, 0, 0));
                case DateFilter.Last2Weeks:
                    return utcNow.Subtract(new TimeSpan(14, 0, 0, 0));
                case DateFilter.LastMonth:
                    return utcNow.AddMonths(-1);
                case DateFilter.Last3Months:
                    return utcNow.AddMonths(-3);
                case DateFilter.Last6Months:
                    return utcNow.AddMonths(-6);
                case DateFilter.Last12Months:
                    return utcNow.AddMonths(-12);
                case DateFilter.Last2Years:
                    return utcNow.AddMonths(-24);
                case DateFilter.Last3Years:
                    return utcNow.AddMonths(-36);
                case DateFilter.NoFilter:
                    return DateTime.MinValue;
                default:
                    return utcNow;
            }
        }

        private static DateTime? GetTimeSpanStartDateUtc(DateFilter filter)
        {
            var fromDate = DateTime.UtcNow.ToDateType(DateTimeExtensions.DateGroupType.Month);
            switch (filter)
            {
                case DateFilter.LastWeek:
                case DateFilter.Last2Weeks:
                case DateFilter.LastMonth:
                    return fromDate;
                case DateFilter.Last3Months:
                    return fromDate.AddMonths(-2);
                case DateFilter.Last6Months:
                    return fromDate.AddMonths(-5);
                case DateFilter.Last12Months:
                    return fromDate.AddMonths(-11);
                case DateFilter.Last2Years:
                    return fromDate.AddMonths(-23);
                case DateFilter.Last3Years:
                    return fromDate.AddMonths(-35);
                case DateFilter.NoFilter:
                    return null;
                default:
                    return fromDate;
            }
        }

        private string GetDeviceFilterRegex(DeviceFilter filter)
        {
            var prefixes = SettingsService.TerminalServerPrefixes;
            switch (filter)
            {
                case DeviceFilter.ExcludingTerminalServer:
                    return string.IsNullOrEmpty(prefixes) ? @"^.*$" : @"^((?!" + prefixes + @")).*$";
                case DeviceFilter.OnlyTerminalServer:
                    return string.IsNullOrEmpty(prefixes) ? @"^.*$" : @"^((" + prefixes + @")).*$";
                case DeviceFilter.AllDevices:
                    return @"^.*$";
                default:
                    return @"^.*$";
            }
        }
    }
}
