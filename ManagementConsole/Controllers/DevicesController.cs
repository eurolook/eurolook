using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Controllers
{
    public class DevicesController : EurolookController
    {
        private readonly IDevicesDatabase _devicesDatabase;

        public DevicesController(ISettingsService settingsService, IDevicesDatabase devicesDatabase)
            : base(settingsService)
        {
            _devicesDatabase = devicesDatabase;
        }

        // GET: /Devices
        public async Task<ActionResult> Index(
            string q = null,
            uint p = 1,
            uint s = 20,
            string o = "Created",
            bool asc = true,
            bool deleted = false)
        {
            if (Request.IsAjaxRequest())
            {
                var page = new DataPage<DeviceDataModel>(p, s, asc, o);
                page.Data.AddRange(await _devicesDatabase.GetDevices(page, q, deleted));
                page.TotalCount = (uint)_devicesDatabase.GetDevicesCount(q, deleted);
                return JsonResult(page);
            }

            return View();
        }

        // GET: /Devices/Edit/00000000-0000-0000-0000-000000000000
        public ActionResult Edit(Guid id)
        {
            if (Request.IsAjaxRequest())
            {
                var device = _devicesDatabase.GetDeviceForEdit(id);
                if (device != null)
                {
                    return JsonResult(device);
                }

                return JsonError($"Could not find device with id {id}");
            }

            return View(id);
        }

        // DELETE: /Devices/Delete/
        [HttpDelete]
        public JsonResult Delete([FromBody] List<DeviceDataModel> devices)
        {
            try
            {
                foreach (var deviceDataModel in devices)
                {
                    _devicesDatabase.DeleteDevice(deviceDataModel.Id);
                }

                if (devices.Count == 1)
                {
                    var device = devices.First();
                    return JsonResult($"The device profile for '{device.DeviceName}' has been deleted.");
                }

                return JsonResult("The selected devices have been deleted.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The device could not be deleted. {ex.GetInnerException().Message}");
            }
        }

        // DELETE: /Devices/DeletePermanently/
        [HttpDelete]
        public JsonResult DeletePermanently([FromBody] List<DeviceDataModel> devices)
        {
            try
            {
                foreach (var deviceDataModel in devices)
                {
                    _devicesDatabase.DeleteDevicePermanently(deviceDataModel.Id);
                }

                if (devices.Count == 1)
                {
                    var device = devices.First();
                    return JsonResult($"The device profile for '{device.DeviceName}' has been deleted permanently.");
                }

                return JsonResult("The selected devices have been deleted permanently.");
            }
            catch (Exception ex)
            {
                var realEx = ex.GetInnerException();
                string msg = realEx.Message;
                return HandleError(ex, $"The device could not be deleted. {msg}");
            }
        }

        // POST: /Devices/Revive/
        [HttpPost]
        public JsonResult Revive([FromBody] List<DeviceDataModel> devices)
        {
            try
            {
                foreach (var deviceDataModel in devices)
                {
                    _devicesDatabase.ReviveDevice(deviceDataModel.Id);
                }

                if (devices.Count == 1)
                {
                    var device = devices.First();
                    return JsonResult($"The device profile for '{device.DeviceName}' has been restored.");
                }

                return JsonResult("The selected devices have been restored.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The device could not be revived. {ex.GetInnerException().Message}");
            }
        }
    }
}
