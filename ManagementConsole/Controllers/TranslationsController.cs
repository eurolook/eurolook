using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.Data.Database;
using Eurolook.ManagementConsole.Areas.DataExchange;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Eurolook.ManagementConsole.ViewModels.Languages;
using Eurolook.ManagementConsole.ViewModels.Texts;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Controllers
{
    public class TranslationsController : EurolookController
    {
        private readonly ExportService _exportService;
        private readonly ITranslationValidationService _translationValidationService;
        private readonly ILanguageRepository _languageRepository;
        private readonly TranslationsDatabase _translationsDatabase;

        public TranslationsController(
            ISettingsService settingsService,
            ExportService exportService,
            ITranslationValidationService translationValidationService,
            ILanguageRepository languageRepository,
            TranslationsDatabase translationsDatabase)
            : base(settingsService)
        {
            _exportService = exportService;
            _translationValidationService = translationValidationService;
            _languageRepository = languageRepository;
            _translationsDatabase = translationsDatabase;
        }

        // GET: /Translations/
        // GET: /Translations/?l=XYZ&type=ABC
        public ActionResult Index(
            uint p = 1,
            uint s = 20,
            string t = "orga",
            string l = "EN",
            string o = "alias",
            bool asc = true,
            bool deleted = false,
            string q = null)
        {
            if (Request.IsAjaxRequest())
            {
                var page = new DataPage<TranslationDataModel>(p, s, asc, o);
                var data = _translationsDatabase.GetTranslationViewModels(page, t, l, q, deleted);
                page.Data.AddRange(data.Skip((int)page.From).Take((int)page.PageSize));
                page.TotalCount = (uint)data.Count;
                return JsonResult(page);
            }

            return View();
        }

        // GET: /Translations/Filters
        public ActionResult Filters()
        {
            // languages filters
            var languageFilters = _languageRepository.GetAllLanguages().OrderBy(l => l.DisplayName)
                                                     .Select(lang => new LanguageViewModel(lang))
                                                     .ToList();
            languageFilters.Add(
                new LanguageViewModel
                {
                    Id = Guid.Empty,
                    DisplayName = "All",
                    Name = "*",
                });

            // text filters
            var typeFilters = new List<TextTypeFilterGroup>
            {
                new TextTypeFilterGroup
                {
                    Label = "Search",
                    Options = new List<TextTypeFilter>
                    {
                        TextTypeFilter.Alias,
                        TextTypeFilter.Translation,
                    },
                },
                new TextTypeFilterGroup
                {
                    Label = "General Filters",
                    Options = new List<TextTypeFilter>
                    {
                        TextTypeFilter.All,
                        TextTypeFilter.Orga,
                    },
                },
            };
            var docModelGroup = new TextTypeFilterGroup
            {
                Label = "Document Model Filters",
                Options = new List<TextTypeFilter> { TextTypeFilter.Docs },
            };
            foreach (var doc in _translationsDatabase.GetAllDocumentModels().OrderBy(dm => dm.DisplayName))
            {
                var docFilter = new TextTypeFilter
                {
                    Id = "doc:" + doc.Id,
                    DisplayName = doc.DisplayName,
                };
                docModelGroup.Options.Add(docFilter);
            }

            typeFilters.Add(docModelGroup);
            return JsonResult(
                new
                {
                    Languages = languageFilters,
                    Types = typeFilters,
                });
        }

        // PUT: /Translations/Edit
        [HttpPut]
        public ActionResult Edit([FromBody] List<TranslationDataModel> translationDms)
        {
            try
            {
                var message = _translationValidationService.ValidateTranslations(translationDms);
                _translationsDatabase.UpdateTranslations(translationDms);
                if (message != "")
                {
                    message = char.ToLower(message[0]) + message.Substring(1);
                    return JsonResult("The translations have been updated successfully, but " + message);
                }

                return JsonResult("The translations have been updated successfully!");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The translations could not be edited. {ex.GetInnerException().Message}");
            }
        }

        // DELETE: /Translations/Delete
        [HttpDelete]
        public ActionResult Delete([FromBody] List<TranslationDataModel> translations)
        {
            try
            {
                _translationsDatabase.DeleteTranslations(translations);
                return JsonResult("The translations have been deleted successfully!");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The translations could not be deleted. {ex.GetInnerException().Message}");
            }
        }

        // POST: /Translations/Export
        [HttpPost]
        public async Task<ActionResult> Export(
            [FromQuery] bool includeTexts,
            [FromBody] List<TranslationDataModel> translations)
        {
            try
            {
                var exportPackage = await _exportService.ExportTranslations(translations, includeTexts);
                return JsonResult(new { PackageId = exportPackage.Id });
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The data package could not be created. {ex.GetInnerException().Message}");
            }
        }

        // GET: /Translations/Download/00000000-0000-0000-0000-000000000000
        public ActionResult Download(Guid id)
        {
            string filePath = Path.Combine(GetTempPath("Downloads"), $"{id}.csv");
            string content = System.IO.File.ReadAllText(filePath);

            var data = Encoding.UTF8.GetBytes(content);
            var result = Encoding.UTF8.GetPreamble().Concat(data).ToArray();
            return File(result, "text/csv", "TranslationExport.csv");
        }

        private string GetTempPath(string subfolder)
        {
            string path = Path.Combine(Path.GetTempPath(), subfolder);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            return path;
        }
    }
}
