﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.ManagementConsole.Areas.DataExchange;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Eurolook.ManagementConsole.ViewModels.Texts;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Controllers
{
    public class TextsController : EurolookController
    {
        private readonly IAdminDatabase _adminDatabase;
        private readonly ITextUsagesService _textUsageService;
        private readonly ExportService _exportService;

        public TextsController(
            ISettingsService settingsService,
            IAdminDatabase adminDatabase,
            ITextUsagesService textUsageService,
            ExportService exportService)
            : base(settingsService)
        {
            _adminDatabase = adminDatabase;
            _textUsageService = textUsageService;
            _exportService = exportService;
        }

        // GET: /Texts/
        // GET: /Texts/?p=1&t=ABC
        public ActionResult Index(
            uint p = 1,
            uint s = 20,
            string t = null,
            string o = "alias",
            bool asc = true,
            bool deleted = false,
            string q = null)
        {
            if (Request.IsAjaxRequest())
            {
                var page = new DataPage<TextDataModel>(p, s, asc, o)
                {
                    TotalCount = (uint)_adminDatabase.GetTextCount(t, deleted, q),
                };
                foreach (var text in _adminDatabase.GetTexts(page, t, deleted, q))
                {
                    var textDm = new TextDataModel(text)
                    {
                        TranslationCount = _adminDatabase.GetTranslationCount(text, deleted),
                    };
                    page.Data.Add(textDm);
                }

                return JsonResult(page);
            }

            return View();
        }

        public ActionResult Edit(Guid id)
        {
            if (Request.IsAjaxRequest())
            {
                var text = _adminDatabase.GetText(id);
                if (text != null)
                {
                    var usedInBricks = _textUsageService.GetBrickUsages(text);
                    var usedInOrgaEntities = _textUsageService.GetOrgaEntityUsages(text);
                    var usedInAddresses = _textUsageService.GetAdressUsages(text);
                    var usedInPredefinedFunctions = _textUsageService.GetPredefinedFunctionUsages(text);
                    var isSystemText = _textUsageService.IsSystemText(text);

                    var textEditViewModel = new TextEditViewModel(
                        id,
                        text.Alias,
                        usedInAddresses,
                        usedInOrgaEntities,
                        usedInBricks,
                        usedInPredefinedFunctions,
                        isSystemText);

                    return JsonResult(textEditViewModel);
                }

                return JsonError($"Could not find text with id {id}");
            }

            return View(id);
        }

        // PUT: /Texts/Edit/00000000-0000-0000-0000-000000000000
        [HttpPut]
        public ActionResult Edit(Guid id, [FromBody] TextEditViewModel text)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                if (!ModelState.IsValid)
                {
                    return JsonError("The test could not be updated. Please check the errors below.");
                }

                _adminDatabase.UpdateText(text);
                return JsonResult("The text has been updated successfully!");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The text could not be updated. {ex.GetInnerException().Message}");
            }
        }

        // GET: /Texts/Filters
        public ActionResult Filters()
        {
            var filters = new List<TextTypeFilterGroup>
            {
                new TextTypeFilterGroup
                {
                    Label = "General Filters",
                    Options = new List<TextTypeFilter>
                    {
                        TextTypeFilter.All,
                        TextTypeFilter.Orga,
                        TextTypeFilter.Functions,
                        TextTypeFilter.Addresses,
                        TextTypeFilter.SystemTexts,
                        TextTypeFilter.Unused,
                    },
                },
            };
            var docModelGroup = new TextTypeFilterGroup
            {
                Label = "Document Model Filters",
                Options = new List<TextTypeFilter> { TextTypeFilter.Docs },
            };
            foreach (var doc in _adminDatabase.GetAllDocumentModels().OrderBy(dm => dm.DisplayName))
            {
                var docFilter = new TextTypeFilter
                {
                    Id = "doc:" + doc.Id,
                    DisplayName = doc.DisplayName,
                };
                docModelGroup.Options.Add(docFilter);
            }

            filters.Add(docModelGroup);
            return JsonResult(filters);
        }

        // POST: /Texts/Create
        [HttpPost]
        public ActionResult Create([FromBody] TextDataModel text)
        {
            try
            {
                var newText = _adminDatabase.CreateText(text);
                return JsonResult("The text has been created.", new TextDataModel(newText));
            }
            catch (Exception ex)
            {
                var logEx = ex;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    logEx = ex.InnerException.InnerException;
                }

                string message = logEx.Message;
                if (message.Contains("inner exception"))
                {
                    message = ex.InnerException?.Message ?? message;
                }

                if (message.Contains("duplicate key"))
                {
                    message = "Text already exists.";
                }

                return HandleError(ex, $"The text could not be created. {message}");
            }
        }

        [HttpGet]
        public ActionResult Alias(string q)
        {
            var result = new List<string>();
            if (q.Length > 2)
            {
                result = _adminDatabase.QueryAliases(q);
            }

            return new JsonResult(result);
        }

        // DELETE: /Texts/Delete
        [HttpDelete]
        public ActionResult Delete([FromBody] List<TextDataModel> textDms)
        {
            try
            {
                _adminDatabase.DeleteTexts(textDms);
                return JsonResult("The texts have been deleted successfully!");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The texts could not be deleted. {ex.GetInnerException().Message}");
            }
        }

        // POST: /Texts/Export
        [HttpPost]
        public async Task<ActionResult> Export(
            [FromBody] List<TextDataModel> textDms,
            [FromQuery] bool includeTranslations)
        {
            try
            {
                var exportPackage = await _exportService.ExportTexts(textDms, includeTranslations);
                return JsonResult(new { PackageId = exportPackage.Id });
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The data package could not be created. {ex.GetInnerException().Message}");
            }
        }
    }
}
