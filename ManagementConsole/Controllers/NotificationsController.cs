﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Areas.DataExchange;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Controllers
{
    public class NotificationsController : EurolookController
    {
        private readonly INotificationsDatabase _notificationsDatabase;
        private readonly ExportService _exportService;

        // GET: Notifications
        public NotificationsController(
            ISettingsService settingsService,
            INotificationsDatabase notificationsDatabase,
            ExportService exportService)
            : base(settingsService)
        {
            _notificationsDatabase = notificationsDatabase;
            _exportService = exportService;
        }

        public ActionResult Index(
            string q = null,
            uint p = 1,
            uint s = 20,
            string o = "notification",
            bool asc = true,
            bool deleted = false)
        {
            if (Request.IsAjaxRequest())
            {
                var page = new DataPage<Notification>(p, s, asc, o);
                page.Data.AddRange(_notificationsDatabase.GetNotifications(page, q, deleted));
                page.TotalCount = (uint)_notificationsDatabase.GetNotificationsCount(q, deleted);
                return JsonResult(page);
            }

            return View();
        }

        public async Task<JsonResult> All()
        {
            var notification = await _notificationsDatabase.GetAllNotificationsAsync();
            return JsonResult(notification);
        }

        // GET: /Notifications/Edit/00000000-0000-0000-0000-000000000000
        public ActionResult Edit(Guid id)
        {
            if (Request.IsAjaxRequest())
            {
                var notificationVm = _notificationsDatabase.GetNotificationsForEdit(id);
                if (notificationVm != null)
                {
                    return JsonResult(notificationVm);
                }

                return JsonError($"Could not find notification with id {id}");
            }

            ViewBag.Id = id;
            return View(id);
        }

        // PUT: /Address/Edit/00000000-0000-0000-0000-000000000000
        [HttpPut]
        public JsonResult Edit(Guid id, [FromBody] NotificationsEditDataModel model)
        {
            try
            {
                if (DateTime.TryParse(model.FromDateString, out var fromDate))
                {
                    model.FromDate = fromDate;
                }
                else
                {
                    JsonError($"From date {model.FromDateString} is invalid.");
                }

                if (DateTime.TryParse(model.ToDateString, out var toDate))
                {
                    model.ToDate = toDate;
                }
                else
                {
                    JsonError($"To date {model.ToDateString} is invalid.");
                }

                _notificationsDatabase.UpdateNotification(id, model);

                return JsonResult("The notification has been updated.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The notification could not be updated. {ex.GetInnerException().Message}");
            }
        }

        [HttpDelete]
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                await _notificationsDatabase.DeleteNotification(id, true);
                return JsonResult("The notification has been deleted.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The notification could not be deleted. {ex.GetInnerException().Message}");
            }
        }

        // POST: /Notifications/Create
        [HttpPost]
        public async Task<ActionResult> Create([FromBody] NotificationsDataModel model)
        {
            try
            {
                var notification = await _notificationsDatabase.CreateNotification(model);
                return JsonResult("The notification has been created.", new NotificationsDataModel(notification));
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The notification could not be created. {ex.GetInnerException().Message}");
            }
        }

        // POST: /Notifications/Export
        [HttpPost]
        public async Task<ActionResult> Export([FromBody] List<NotificationsDataModel> notificationModels)
        {
            try
            {
                var exportPackage = await _exportService.ExportNotification(notificationModels);
                return JsonResult(new { PackageId = exportPackage.Id });
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The data package could not be created. {ex.GetInnerException().Message}");
            }
        }

        [HttpPost]
        public async Task<JsonResult> Revive(Guid id)
        {
            try
            {
                await _notificationsDatabase.RestoreNotification(id, false);
                return JsonResult("The notification has been revived.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The notification could not be revived. {ex.GetInnerException().Message}");
            }
        }

        // GET: /Notifications/Thumbnail/00000000-0000-0000-0000-000000000000
        public ActionResult Thumbnail(Guid id)
        {
            var notification = _notificationsDatabase.GetNotification(id);

            if (notification?.ThumbnailBytes == null || notification.ThumbnailBytes.Length == 0)
            {
                return NotFound();
            }

            return File(notification.ThumbnailBytes, "application/octet-stream", notification.DisplayName + ".png");
        }

        // POST: /Notifications/Thumbnail/00000000-0000-0000-0000-000000000000
        [HttpPost]
        public async Task<ActionResult> Thumbnail(Guid id, [FromForm] IFormFile file)
        {
            try
            {
                await _notificationsDatabase.UpdateThumbnail(id, file);
                return JsonResult(null);
            }
            catch (Exception ex)
            {
                return HandleError(ex, ex.Message);
            }
        }
    }
}
