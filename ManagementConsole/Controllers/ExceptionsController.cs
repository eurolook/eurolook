﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Eurolook.ManagementConsole.ViewModels.Exceptions;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Controllers
{
    public class ExceptionsController : EurolookController
    {
        private readonly StatisticsDatabase _statisticsDatabase;
        private readonly ExceptionLogService _exceptionLogService;

        public ExceptionsController(
            ISettingsService settingsService,
            StatisticsDatabase statisticsDatabase,
            ExceptionLogService exceptionLogService)
            : base(settingsService)
        {
            _statisticsDatabase = statisticsDatabase;
            _exceptionLogService = exceptionLogService;
        }

        private enum DateFilter
        {
            LastWeek = 0,
            Last2Weeks = 1,
            LastMonth = 2,
            Last3Months = 3,
            Last6Months = 4,
            Last12Months = 5,
            Last2Years = 6,
            Last3Years = 7,
            NoFilter = 99,
        }

        // GET: Exceptions
        public ActionResult Index()
        {
            return View();
        }

        // GET: /Exceptions/ExceptionLogDownload?filter=
        public async Task<ActionResult> ExceptionLogDownload(int? filter)
        {
            int dateRangeId = filter ?? 1;
            var fromDateUtc = GetStartDateUtc((DateFilter)dateRangeId);
            var package = await _exceptionLogService.GetExceptionLogByBucket(fromDateUtc ?? DateTime.UtcNow.AddDays(-7));
            string fileName = $"ExceptionLog-{DateTime.Now:yyyy-MM-dd--HH-mm}.xlsx";
            return File(package, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }

        // GET: /Exceptions/ExceptionLogRawDownload?filter=
        public async Task<ActionResult> ExceptionLogRawDownload(int? filter)
        {
            int dateRangeId = filter ?? 1;
            var fromDateUtc = GetStartDateUtc((DateFilter)dateRangeId);
            var package = await _exceptionLogService.GetExceptionLogByEntry(fromDateUtc ?? DateTime.UtcNow.AddDays(-7));
            string fileName = $"ExceptionLogRaw-{DateTime.Now:yyyy-MM-dd--HH-mm}.xlsx";
            return File(package, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }

        // GET: /Exceptions/ExceptionLogDetails/2ef45...
        public ActionResult ExceptionLogDetails(string id, int? filter)
        {
            if (id == null)
            {
                return View("ExceptionLog");
            }

            int dateRangeId = filter ?? 1;
            var fromDateUtc = GetStartDateUtc((DateFilter)dateRangeId);

            var exceptionBucket = _statisticsDatabase.GetExceptionBucket(id);

            var affectedOfficeVersions = _statisticsDatabase.GetAffectedOfficeVersions(exceptionBucket, fromDateUtc);
            var affectedClientVersions = _statisticsDatabase.GetAffectedClientVersions(exceptionBucket, fromDateUtc);

            var model = new ExceptionLogDetailsViewModel(
                exceptionBucket,
                affectedOfficeVersions,
                affectedClientVersions);

            return View("ExceptionLogDetails", model);
        }

        // GET: /Exceptions/ExceptionLogDetailsEntries/2ef45
        public JsonResult ExceptionLogDetailsEntries(
            [FromRoute] string id,
            int? filter,
            uint p = 1,
            uint s = 20,
            string o = "date",
            bool asc = false)
        {
            var fromDateUtc = GetStartDateUtc((DateFilter)(filter ?? 1));
            var page = new DataPage<ExceptionLogDetailViewModel>(p, s, asc, o);

            var logs = _statisticsDatabase.GetExceptionLogDetails(id, page, fromDateUtc);
            foreach (var log in logs)
            {
                log.Originator = log.Originator.Contains("Unknown") ? "Deleted User" : log.Originator;
            }

            page.Data.AddRange(logs);
            return JsonResult(page);
        }

        [HttpGet]
        [ActionName("exceptionloginfo")]
        public async Task<JsonResult> GetExceptionLogInfo(
            int id,
            uint p = 1,
            uint s = 20,
            string o = "count",
            bool asc = false,
            string users = "")
        {
            var fromDateUtc = GetStartDateUtc((DateFilter)id);
            var page = new DataPage<ExceptionBucketViewModel>(p, s, asc, o);
            users = (users ?? "").Replace(" ", string.Empty);
            var userFilter = users == string.Empty ? new List<string>() : users.Split(';').ToList();

            page.Data.AddRange(await _statisticsDatabase.GetExceptionBuckets(page, fromDateUtc, userFilter));
            return JsonResult(page);
        }

        private static DateTime? GetStartDateUtc(DateFilter filter)
        {
            var utcNow = DateTime.UtcNow.Date;
            switch (filter)
            {
                case DateFilter.LastWeek:
                    return utcNow.Subtract(new TimeSpan(7, 0, 0, 0));
                case DateFilter.Last2Weeks:
                    return utcNow.Subtract(new TimeSpan(14, 0, 0, 0));
                case DateFilter.LastMonth:
                    return utcNow.AddMonths(-1);
                case DateFilter.Last3Months:
                    return utcNow.AddMonths(-3);
                case DateFilter.Last6Months:
                    return utcNow.AddMonths(-6);
                case DateFilter.Last12Months:
                    return utcNow.AddMonths(-12);
                case DateFilter.Last2Years:
                    return utcNow.AddMonths(-24);
                case DateFilter.Last3Years:
                    return utcNow.AddMonths(-36);
                case DateFilter.NoFilter:
                    return DateTime.MinValue;
                default:
                    return utcNow;
            }
        }
    }
}
