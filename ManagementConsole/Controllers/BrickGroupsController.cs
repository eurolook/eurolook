﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Common;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Controllers
{
    public class BrickGroupsController : EurolookController
    {
        private readonly IAdminDatabase _adminDatabase;

        public BrickGroupsController(ISettingsService settingsService, IAdminDatabase adminDatabase)
            : base(settingsService)
        {
            _adminDatabase = adminDatabase;
        }

        // GET: /BrickGroups/
        // GET: /BrickGroups/?q=XYZ&p=1
        public ActionResult Index(
            string q = null,
            uint p = 1,
            uint s = 20,
            string o = "alias",
            bool asc = true,
            bool deleted = false)
        {
            if (Request.IsAjaxRequest())
            {
                var page = new DataPage<BrickGroup>(p, s, asc, o);
                page.Data.AddRange(_adminDatabase.GetBrickGroups(page, q, deleted));
                page.TotalCount = (uint)_adminDatabase.GetBrickGroupCount(q, deleted);
                return JsonResult(page);
            }

            return View();
        }

        // POST: /BrickGroups/Create
        [HttpPost]
        public ActionResult Create([FromBody] BrickGroupDataModel model)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                var brickGroup = _adminDatabase.CreateBrickGroup(model);
                return JsonResult("The brick group has been created.", new BrickGroupDataModel(brickGroup));
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The brick group could not be created. {ex.GetInnerException().Message}");
            }
        }

        // GET: /Bricks/Edit/00000000-0000-0000-0000-000000000000
        public ActionResult Edit(Guid id)
        {
            if (Request.IsAjaxRequest())
            {
                var brickGroup = _adminDatabase.GetBrickGroupForEdit(id);
                if (brickGroup != null)
                {
                    var documentModels = _adminDatabase.GetDocumentModelsByBrickGroup(brickGroup);

                    return JsonResult(new BrickGroupsEditDataModel(brickGroup, documentModels));
                }

                return JsonError($"Could not find brick group with id {id}");
            }

            return View(id);
        }

        // PUT: /BrickGroups/Edit/00000000-0000-0000-0000-000000000000
        [HttpPut]
        public ActionResult Edit(Guid id, [FromBody] BrickGroupsEditDataModel model)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                if (!ModelState.IsValid)
                {
                    return JsonError("The brick group could not be updated. Please check the errors below.");
                }

                _adminDatabase.UpdateBrickGroup(id, model);
                return JsonResult("The brick group has been updated successfully!");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The brick group could not be updated. {ex.GetInnerException().Message}");
            }
        }

        // GET: /BrickGroups/Icon/00000000-0000-0000-0000-000000000000
        public ActionResult Icon(Guid id)
        {
            var brickGroup = _adminDatabase.GetBrickGroupForEdit(id);

            if (brickGroup == null || brickGroup.Icon == null || brickGroup.Icon.Length == 0)
            {
                return NotFound();
            }

            return File(brickGroup.Icon, "application/octet-stream", brickGroup.Name + ".png");
        }

        // POST: /BrickGroups/Icon/00000000-0000-0000-0000-000000000000
        [HttpPost]
        public async Task<ActionResult> Icon(Guid id, [FromForm] IFormFile file)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                var bytes = await _adminDatabase.UpdateBrickGroupIcon(id, file);
                return JsonResult(null, bytes != null ? Tools.GetBase64Image(bytes) : null);
            }
            catch (Exception ex)
            {
                return HandleError(ex, ex.Message);
            }
        }

        // DELETE: /BrickGroups/Delete/00000000-0000-0000-0000-000000000000
        [HttpDelete]
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (SettingsService.IsReadOnlyMode)
                {
                    return JsonError(ReadOnlyModeMessage);
                }

                _adminDatabase.DeleteBrickGroup(id);
                return JsonResult("The brick group has been deleted successfully!");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The brick group could not be deleted. {ex.GetInnerException().Message}");
            }
        }
    }
}
