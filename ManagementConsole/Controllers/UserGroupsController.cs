using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Eurolook.Common.Log;
using Eurolook.Data.Constants;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Areas.DataExchange;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Controllers
{
    public class UserGroupsController : EurolookController
    {
        private readonly IUserGroupDatabase _db;
        private readonly ExportService _exportService;
        private readonly IMapper _mapper;

        public UserGroupsController(
            ISettingsService settingsService,
            IUserGroupDatabase userGroupDatabase,
            ExportService exportService,
            IMapper mapper)
            : base(settingsService)
        {
            _db = userGroupDatabase;
            _exportService = exportService;
            _mapper = mapper;
        }

        // GET: UserGroups
        public async Task<ActionResult> Index(
            string q = null,
            uint p = 1,
            uint s = 20,
            string o = "Name",
            bool asc = true,
            bool deleted = false)
        {
            if (Request.IsAjaxRequest())
            {
                var page = new DataPage<UserGroup>(p, s, asc, o);
                page.Data.AddRange(await _db.GetUserGroupsAsync(page, q, deleted));
                page.TotalCount = await _db.GetUserGroupCountAsync(q, deleted);
                return JsonResult(page);
            }

            return View();
        }

        // GET: /Users/Edit/00000000-0000-0000-0000-000000000000
        public async Task<ActionResult> Edit(Guid id)
        {
            if (Request.IsAjaxRequest())
            {
                var userGroup = await _db.GetUserGroupAsync(id);
                if (userGroup != null)
                {
                    var viewModel = new UserGroupDataModel(userGroup);
                    return JsonResult(viewModel);
                }

                return JsonError($"Could not find user with id {id}");
            }

            return View(id);
        }

        // POST: UserGroups/Create
        [HttpPost]
        public async Task<ActionResult> Create([FromBody] UserGroupDataModel model)
        {
            try
            {
                var userGroup = await _db.CreateUserGroupAsync(model.Name);
                return JsonResult("The user group has been created.", userGroup);
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The user group could not be created. {ex.GetInnerException().Message}");
            }
        }

        // PUT: /UserGroups/Edit/00000000-0000-0000-0000-000000000000
        [HttpPut]
        public async Task<ActionResult> Edit(Guid id, [FromBody] UserGroupDataModel editDataModel)
        {
            try
            {
                var userGroup = _mapper.Map<UserGroup>(editDataModel);
                await _db.UpdateUserGroupAsync(id, userGroup);
                return JsonResult("The user group has been updated successfully!");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The user group could not be updated. {ex.GetInnerException().Message}");
            }
        }

        // DELETE: /UserGroups/Delete/00000000-0000-0000-0000-000000000000
        [HttpDelete]
        public async Task<ActionResult> Delete(Guid id)
        {
            try
            {
                var protectedUserGroups = new[]
                {
                    CommonDataConstants.AdministratorsUserGroupId,
                    CommonDataConstants.ContributorsUserGroupId,
                    CommonDataConstants.DefaultUserGroupId,
                };

                if (protectedUserGroups.Contains(id))
                {
                    return JsonError("The user group cannot be deleted because it is protected.");
                }

                await _db.DeleteUserGroupAsync(id);
                return JsonResult("The user group has been deleted.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The user group could not be deleted. {ex.GetInnerException().Message}");
            }
        }

        // POST: /UserGroups/Revive/00000000-0000-0000-0000-000000000000
        [HttpPost]
        public async Task<JsonResult> Revive(Guid id)
        {
            try
            {
                await _db.ReviveUserGroupAsync(id);
                return JsonResult("The user group has been revived.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The user group could not be revived. {ex.GetInnerException().Message}");
            }
        }

        // POST: /UserGroups/Export
        [HttpPost]
        public async Task<ActionResult> Export([FromBody] List<UserGroup> userGroups)
        {
            try
            {
                var exportPackage = await _exportService.ExportUserGroups(userGroups);
                return JsonResult(new { PackageId = exportPackage.Id });
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The data package could not be created. {ex.GetInnerException().Message}");
            }
        }
    }
}
