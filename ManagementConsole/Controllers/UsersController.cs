﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.ActiveDirectoryLink;
using Eurolook.ActiveDirectoryLink.TemplateOwnership;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Eurolook.ManagementConsole.Controllers
{
    public class UsersController : EurolookController
    {
        private readonly IUsersDatabase _usersDatabase;
        private readonly IActiveDirectorySearcher _activeDirectorySearcher;
        private readonly ICleaner _cleaner;
        private readonly IActiveDirectoryLinker _activeDirectoryLinker;
        private readonly ITemplateOwnershipHandler _templateOwnershipHandler;

        public UsersController(
            ISettingsService settingsService,
            IConfiguration configuration,
            IUsersDatabase usersDatabase,
            Func<string, IActiveDirectoryLinker> activeDirectoryLinkerCreateFunc,
            Func<IActiveDirectorySearcher> activeDirectorySearcherFunc,
            Func<ICleaner> cleanerFunc,
            Func<string, ITemplateOwnershipHandler> templateOwnershipHandlerCreateFunc)
            : base(settingsService)
        {
            _usersDatabase = usersDatabase;
            _activeDirectorySearcher = activeDirectorySearcherFunc();
            _cleaner = cleanerFunc();
            var connectionString = configuration.GetConnectionString("ServerDatabaseConnection");
            _activeDirectoryLinker = activeDirectoryLinkerCreateFunc(connectionString);
            _templateOwnershipHandler = templateOwnershipHandlerCreateFunc(connectionString);
        }

        // GET: Users
        public ActionResult Index(
            string q = null,
            uint p = 1,
            uint s = 20,
            string o = "Created",
            bool asc = true,
            bool deleted = false)
        {
            if (Request.IsAjaxRequest())
            {
                var page = new DataPage<UserDataModel>(p, s, asc, o);
                var userGroups = _usersDatabase.GetAllUserGroups();
                page.Data.AddRange(_usersDatabase.GetUsers(page, userGroups, q, deleted));
                page.TotalCount = (uint)_usersDatabase.GetUserCount(userGroups, q, deleted);
                return JsonResult(page);
            }

            return View();
        }

        // GET: /Users/Edit/00000000-0000-0000-0000-000000000000
        public ActionResult Edit(Guid id)
        {
            if (Request.IsAjaxRequest())
            {
                var user = _usersDatabase.GetUserForEdit(id);
                var userGroups = _usersDatabase.GetAllUserGroups();
                if (user != null)
                {
                    var viewModel = new UserEditDataModel(user, userGroups);
                    return JsonResult(viewModel);
                }

                return JsonError($"Could not find user with id {id}");
            }

            return View(id);
        }

        // GET: /Users/Edit/Login
        public ActionResult ByLogin(string login)
        {
            return RedirectToAction("Edit", new { id = _usersDatabase.GetUserIdByName(login) });
        }

        // POST: Users/Create
        [HttpPost]
        public JsonResult Create([FromBody] UserCreateDataModel model)
        {
            try
            {
                if (_activeDirectoryLinker.CreateAndLinkUser(model.UserName, forceCreate: model.ForceCreate))
                {
                    var user = _usersDatabase.GetUser(model.UserName);
                    return JsonResult("The user has been created.", user);
                }

                return JsonError("The user could not be created. The ADLink failed to create or link the user.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The user could not be created. {ex.GetInnerException().Message}");
            }
        }

        [HttpPost]
        public JsonResult LinkAll()
        {
            try
            {
                Task.Run(() =>
                {
                    this.LogInfo("Start Linking users...");
                    _activeDirectoryLinker.PrepareLinking();

                    var userNames = _activeDirectorySearcher.RetrieveAllUsers().ToArray();
                    foreach (var userName in userNames)
                    {
                        _activeDirectoryLinker.CreateAndLinkUser(userName, out var _);
                    }
                    this.LogInfo("Finished linking users.");
                }).FireAndForgetSafeAsync(this.LogError);

                return JsonResult("Linking has been started.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The users could not be linked. {ex.GetInnerException().Message}");
            }
        }

        [HttpPost]
        public JsonResult CleanUpAll()
        {
            try
            {
                Task.Run(() =>
                {
                    this.LogInfo("Starting cleanup of retired users...");
                    var userNames = _activeDirectorySearcher.RetrieveRetiredUsers().ToArray();
                    foreach (string userName in userNames)
                    {
                        _cleaner.CleanUser(userName, true);
                    }
                    this.LogInfo("Finished cleanup of retired users.");
                }).FireAndForgetSafeAsync(this.LogError);

                return JsonResult("Cleanup has been started.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The users could not be cleaned-up. {ex.GetInnerException().Message}");
            }
        }

        // PUT: /Users/Edit/00000000-0000-0000-0000-000000000000
        [HttpPut]
        public JsonResult Edit(Guid id, [FromBody] UserEditDataModel model)
        {
            try
            {
                _usersDatabase.UpdateUserProfile(id, model);
                return JsonResult("The user has been updated.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The user could not be updated. {ex.GetInnerException().Message}");
            }
        }

        // DELETE: /Users/Delete/00000000-0000-0000-0000-000000000000
        [HttpDelete]
        public JsonResult Delete(Guid id)
        {
            try
            {
                _templateOwnershipHandler.RemoveUserAsTemplateOwner(id);
                _usersDatabase.DeleteUserProfile(id);
                return JsonResult("The user has been deleted.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The user could not be deleted. {ex.GetInnerException().Message}");
            }
        }

        // DELETE: /Users/DeletePermanently/00000000-0000-0000-0000-000000000000
        [HttpDelete]
        public JsonResult DeletePermanently(Guid id)
        {
            try
            {
                _usersDatabase.DeleteUserProfilePermanently(id);
                return JsonResult("The user has been deleted permanently.");
            }
            catch (Exception ex)
            {
                var realEx = ex.GetInnerException();
                return HandleError(ex, $"The user could not be deleted. {realEx.Message}");
            }
        }

        // POST: /Users/Revive/00000000-0000-0000-0000-000000000000
        [HttpPost]
        public JsonResult Revive(Guid id)
        {
            try
            {
                _usersDatabase.ReviveUserProfile(id);
                return JsonResult("The user has been revived.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The user could not be revived. {ex.GetInnerException().Message}");
            }
        }

        [HttpPost]
        public JsonResult RemoteWipe(Guid id)
        {
            try
            {
                _usersDatabase.RequestRemoteWipe(id);
                return JsonResult("Remote wipe has been requested.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"Remote wipe could not be requested. {ex.GetInnerException().Message}");
            }
        }
    }
}
