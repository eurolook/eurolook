using System.Threading.Tasks;
using Eurolook.Data.Database;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Microsoft.AspNetCore.Authorization;

namespace Eurolook.ManagementConsole.Controllers
{
    [Authorize(Policy = "OrgaAdmin")]
    public class HomeController : EurolookController
    {
        private readonly ISystemConfigurationRepository _configurationRepository;

        public HomeController(ISettingsService settingsService, ISystemConfigurationRepository configurationRepository)
            : base(settingsService)
        {
            _configurationRepository = configurationRepository;
        }

        public Microsoft.AspNetCore.Mvc.ActionResult Index()
        {
            return View();
        }

        public async Task<Microsoft.AspNetCore.Mvc.ActionResult> Shortcuts()
        {
            var config = await _configurationRepository.GetConfigurationAsync<ShortcutsConfiguration>();
            return JsonResult(new { categories = config.Categories });
        }
    }
}
