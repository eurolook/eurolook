﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Areas.DataExchange;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.ExcelImport;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Eurolook.ManagementConsole.ViewModels.PersonNames;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Controllers
{
    [Authorize(Policy = "OrgaAdmin")]
    public class PersonNamesController : EurolookController
    {
        private readonly IPersonNamesDatabase _personNamesDatabase;
        private readonly UploadService _uploadService;
        private readonly ExportService _exportService;

        public PersonNamesController(
            ISettingsService settingsService,
            IPersonNamesDatabase personNamesDatabase,
            UploadService uploadService,
            ExportService exportService)
            : base(settingsService)
        {
            _personNamesDatabase = personNamesDatabase;
            _uploadService = uploadService;
            _exportService = exportService;
        }

        // GET: /PersonNames/
        // GET: /PersonNames/?&category=Commissioners&searchText=searchText
        public ActionResult Index(
            uint p = 1,
            uint s = 20,
            string o = "lastname",
            bool asc = true,
            string category = "All",
            string searchText = null,
            bool deleted = false)
        {
            if (Request.IsAjaxRequest())
            {
                var page = new DataPage<PersonName>(p, s, asc, o);
                page.Data.AddRange(_personNamesDatabase.GetPersonNames(page, category, searchText, deleted));
                page.TotalCount = (uint)_personNamesDatabase.Count(category, searchText);
                return JsonResult(page);
            }

            return View();
        }

        // GET: /PersonNames/Filters
        public ActionResult Filters()
        {
            // filters
            var personNameFilters = new List<PersonNameFilter>
            {
                PersonNameFilter.All,
                PersonNameFilter.Commissioners,
                PersonNameFilter.MeP,
            };

            return JsonResult(new { categories = personNameFilters });
        }

        // GET: /PersonNames/Categories
        public ActionResult Categories()
        {
            // filters
            var personNameFilters = new List<PersonNameFilter>
            {
                PersonNameFilter.Commissioners,
                PersonNameFilter.MeP,
            };

            return JsonResult(new { categories = personNameFilters });
        }

        ////
        //// PUT: /PersonNames/Edit
        [HttpPut]
        public ActionResult Edit([FromBody] List<PersonName> personNames)
        {
            try
            {
                _personNamesDatabase.UpdatePersonNames(personNames);
                return JsonResult("The person names have been edited successfully!");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The person name could not be edited. {ex.GetInnerException().Message}");
            }
        }

        ////
        //// DELETE: /PersonNames/Delete
        [HttpDelete]
        public ActionResult Delete([FromBody] List<PersonName> personNames)
        {
            try
            {
                _personNamesDatabase.DeletePersonNames(personNames);
                return JsonResult("Selected person names have been deleted successfully!");
            }
            catch (Exception ex)
            {
                return HandleError(
                    ex,
                    $"The selected person names could not be deleted. {ex.GetInnerException().Message}");
            }
        }

        // POST: /PersonNames/Create
        [HttpPost]
        public ActionResult Create([FromBody] PersonName model)
        {
            try
            {
                var dbModel = _personNamesDatabase.CreatePersonName(model);
                return JsonResult("New person name entry successfully created.", dbModel);
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"New person name not created. {ex.GetInnerException().Message}");
            }
        }

        // GET: /PersonNames/Import
        public ViewResult Import()
        {
            return View();
        }

        // POST: /PersonNames/Upload
        [HttpPost]
        [DisableRequestSizeLimit]
        [SuppressMessage("csharpsquid", "S5693:Make sure the content length limit is safe here.", Justification = "API endpoint is internal only.")]
        public async Task<ActionResult> Upload([FromForm] IFormFile file)
        {
            try
            {
                if (file.ContentType != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    return JsonError($"File {file.FileName} is not an Excel file (.xlsx)!");
                }

                var uploadedFileId = await _uploadService.UploadFile(file);
                return JsonResult(new { UploadId = uploadedFileId });
            }
            catch (Exception ex)
            {
                return HandleError(ex, ex.GetInnerException().Message);
            }
        }

        // GET: PersonNames/GetChanges?uploadId=<UploadId>
        public JsonResult GetChanges([FromQuery] Guid uploadId)
        {
            string message;
            try
            {
                string filename = _uploadService.GetUploadFileName(uploadId);
                var newPersonNames = PersonNameWorkbook.GetPersonInfo(filename);
                newPersonNames.ForEach(pn => pn.Category = PersonName.MepCategoryName);

                message = _personNamesDatabase.GetChanges(newPersonNames, PersonName.MepCategoryName);
                return JsonResult(message);
            }
            catch (Exception ex)
            {
                _uploadService.TryDeleteFile(uploadId);
                message =
                    $"Reading the excel file and creating Person Name changes failed\n{ex.GetInnerException().Message}";
                return HandleError(ex, message);
            }
        }

        // GET: PErsonNames/ApplyChanges?uploadId=<uploadId>
        public JsonResult ApplyChanges([FromQuery] Guid uploadId)
        {
            string message;
            try
            {
                string filename = _uploadService.GetUploadFileName(uploadId);
                var newPersonNames = PersonNameWorkbook.GetPersonInfo(filename);
                newPersonNames.ForEach(pn => pn.Category = PersonName.MepCategoryName);

                message = _personNamesDatabase.ApplyChanges(newPersonNames, PersonName.MepCategoryName);
                return JsonResult(message);
            }
            catch (Exception ex)
            {
                _uploadService.TryDeleteFile(uploadId);
                message = $"Reading the excel file or updating the database failed\n{ex.GetInnerException().Message}";
                return HandleError(ex, message);
            }
        }

        // GET: /PersonNames/Export
        [HttpPost]
        public async Task<ActionResult> Export(
            [FromBody] List<PersonNamesDataModel> personNamesVms,
            [FromQuery] bool includeDependencies)
        {
            try
            {
                var exportPackage = await _exportService.ExportPersonNames(personNamesVms);
                return JsonResult(new { PackageId = exportPackage.Id });
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The data package could not be created. {ex.GetInnerException().Message}");
            }
        }
    }
}
