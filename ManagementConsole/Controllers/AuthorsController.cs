using System;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.Data.Database;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Controllers
{
    public class AuthorsController : EurolookController
    {
        private readonly ILanguageRepository _languageRepository;
        private readonly AuthorsDatabase _authorsDatabase;

        public AuthorsController(
            ISettingsService settingsService,
            ILanguageRepository languageRepository,
            AuthorsDatabase authorsDatabase)
            : base(settingsService)
        {
            _languageRepository = languageRepository;
            _authorsDatabase = authorsDatabase;
        }

        // GET: /Authors/Edit/00000000-0000-0000-0000-000000000000
        public async Task<ActionResult> Edit(Guid id)
        {
            if (Request.IsAjaxRequest())
            {
                var author = _authorsDatabase.GetAuthorForEdit(id);
                if (author != null)
                {
                    var languages = _languageRepository.GetAllLanguages();
                    var model = new AuthorEditDataModel(author, languages);
                    await model.LoadDataAsync(_authorsDatabase);
                    return JsonResult(model);
                }

                return JsonError($"Could not find author with id {id}");
            }

            ViewBag.Layout = "~/Views/Shared/_Layout.cshtml";
            ViewBag.JobAssignmentStyle = "medium-12 large-6 columns";
            ViewBag.SaveButtonStyle = "button small";
            return View(id);
        }

        // PUT: /Authors/Edit/00000000-0000-0000-0000-000000000000
        [HttpPut]
        public JsonResult Edit(Guid id, [FromBody] AuthorEditDataModel model)
        {
            try
            {
                _authorsDatabase.UpdateAuthor(id, model);
                return JsonResult("The author has been updated.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The author could not be updated. {ex.GetInnerException().Message}");
            }
        }
    }
}
