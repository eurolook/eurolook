using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Eurolook.Common.Extensions;
using Eurolook.Data.Models;
using LinqKit;

namespace Eurolook.ManagementConsole.Queries
{
    public class UserSearchQuery
    {
        private const string PropLogin = "Login";
        private const string PropName = "Name";
        private const string PropEmail = "Email";
        private const string PropOrgAcronym = "OrgAcronym";
        private const string PropUserGroups = "UserGroup";

        private readonly ExpressionStarter<User> _predicate;

        public UserSearchQuery(bool deleted)
        {
            _predicate = PredicateBuilder.New<User>(u => u.Deleted == deleted);
        }

        public Expression<Func<User, bool>> GetPredicate(string searchQuery, IEnumerable<UserGroup> userGroups = null)
        {
            if (string.IsNullOrWhiteSpace(searchQuery) || (searchQuery == "*"))
            {
                return _predicate;
            }

            var queries = searchQuery.Contains(" and ", StringComparison.InvariantCultureIgnoreCase)
                ? searchQuery.Split(new[] { " AND ", " and ", " And " }, StringSplitOptions.RemoveEmptyEntries)
                : (searchQuery.Contains("UserGroup", StringComparison.InvariantCultureIgnoreCase)
                    ? new[] { searchQuery }
                    : searchQuery.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries));

            foreach (string query in queries)
            {
                if (query.Contains("="))
                {
                    var instructionKeyValue = query.Split('=');
                    if (instructionKeyValue.Length == 2)
                    {
                        string property = instructionKeyValue[0].Trim();
                        string value = instructionKeyValue[1].Trim();

                        _predicate.And(
                            value.EndsWith("*")
                                ? GetInfixSearchExpression(property, value)
                                : GetDefaultSearchExpression(property, value, userGroups));
                    }
                }
                else
                {
                    _predicate.And(GetDefaultSearchExpression(null, query, userGroups));
                }
            }

            return _predicate;
        }

        private static Expression<Func<User, bool>> GetDefaultSearchExpression(
            string property,
            string value,
            IEnumerable<UserGroup> userGroups = null)
        {
            Expression<Func<User, bool>> defaultExpression = u => u.Login.Contains(value)
                                                                  || u.Self.LatinLastName.Contains(value)
                                                                  || u.Self.LatinFirstName.Contains(value)
                                                                  || u.Self.Email.Contains(value);

            if (string.IsNullOrWhiteSpace(property))
            {
                return defaultExpression;
            }

            if (property.Equals(PropLogin, StringComparison.InvariantCultureIgnoreCase))
            {
                return u => u.Login.Contains(value);
            }

            if (property.Equals(PropName, StringComparison.InvariantCultureIgnoreCase))
            {
                return u => u.Self.LatinLastName.Contains(value) || u.Self.LatinFirstName.Contains(value);
            }

            if (property.Equals(PropEmail, StringComparison.InvariantCultureIgnoreCase))
            {
                return u => u.Self.Email.Contains(value);
            }

            if (property.Equals(PropOrgAcronym, StringComparison.InvariantCultureIgnoreCase))
            {
                return u => u.Self.Service.Contains(value);
            }

            if (property.Equals(PropUserGroups, StringComparison.InvariantCultureIgnoreCase))
            {
                try
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        var userGroupFromName = userGroups?.FirstOrDefault(x => x.Name == value);
                        if (userGroupFromName != null)
                        {
                            return u => u.UserGroupIds.Contains(userGroupFromName.Id.ToString());
                        }

                        return u => false;
                    }
                }
                catch (NullReferenceException)
                {
                    return u => false;
                }
                catch (ArgumentNullException)
                {
                    return u => false;
                }
            }

            return defaultExpression;
        }

        private static Expression<Func<User, bool>> GetInfixSearchExpression(string property, string value)
        {
            return GetDefaultSearchExpression(property, value);

            // Entity Framework doesn't support StartsWith in Linq queries

            ////Expression<Func<User, bool>> defaultExpression = u => u != null;
            ////if (string.IsNullOrWhiteSpace(property))
            ////{
            ////    return defaultExpression;
            ////}

            ////string searchValue = value.Remove(value.LastIndexOf("*", StringComparison.Ordinal));
            ////if (property.Equals(PropLogin, StringComparison.InvariantCultureIgnoreCase))
            ////{
            ////    return u => u.Login.StartsWith(searchValue, StringComparison.InvariantCultureIgnoreCase);
            ////}

            ////if (property.Equals(PropName, StringComparison.InvariantCultureIgnoreCase))
            ////{
            ////    return u => u.Self.LatinLastName.StartsWith(searchValue, StringComparison.InvariantCultureIgnoreCase)
            ////                || u.Self.LatinFirstName.StartsWith(searchValue, StringComparison.InvariantCultureIgnoreCase);
            ////}

            ////if (property.Equals(PropEmail, StringComparison.InvariantCultureIgnoreCase))
            ////{
            ////    return u => u.Self.Email.StartsWith(searchValue, StringComparison.InvariantCultureIgnoreCase);
            ////}

            ////if (property.Equals(PropOrgAcronym, StringComparison.InvariantCultureIgnoreCase))
            ////{
            ////    return u => u.Self.Service.StartsWith(searchValue, StringComparison.InvariantCultureIgnoreCase);
            ////}

            ////return defaultExpression;
        }
    }
}
