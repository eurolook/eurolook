﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Eurolook.ManagementConsole.Services;
using Eurolook.Web.Common.ScheduledTask;
namespace Eurolook.ManagementConsole.ScheduledTasks
{
    public class ExceptionLogMailTask : AbstractScheduledTask
    {
        private readonly ISettingsService _settingsService;
        private readonly MailService _mailService;
        private readonly ExceptionLogService _exceptionLogService;

        public ExceptionLogMailTask(
            ISettingsService settingsService,
            MailService mailService,
            ExceptionLogService exceptionLogService)
            : base(nameof(ExceptionLogMailTask))
        {
            _settingsService = settingsService;
            _mailService = mailService;
            _exceptionLogService = exceptionLogService;
        }

        protected override void SetConfigurationValues()
        {
            Hour = 5;
            Minute = 30;
            DayOfWeek = System.DayOfWeek.Friday;
            IsActive = _settingsService.EnabledExceptionLogMailer;
        }

        protected override async Task RunTask()
        {
            var fromDateUtc = DateTime.UtcNow.AddDays(-1 * 7);
            fromDateUtc = fromDateUtc.AddMinutes(-1 * fromDateUtc.Minute);
            fromDateUtc = fromDateUtc.AddSeconds(-1 * fromDateUtc.Second);
            var exceptionLogBytes = await _exceptionLogService.GetExceptionLogByBucket(fromDateUtc);
            _mailService.SendExceptionLogMail(exceptionLogBytes, fromDateUtc);
        }
    }
}
