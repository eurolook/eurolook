using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Database;
using Microsoft.AspNetCore.Hosting;

namespace Eurolook.ManagementConsole.ExcelExport
{
    public class ExcelOrgaChartExporter
    {
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly ILanguageRepository _languageRepository;
        private readonly OrgaChartDatabase _orgaChartDatabase;
        private readonly string _orgaChartSheetName = "Eurolook Structure Database";

        public ExcelOrgaChartExporter(
            IWebHostEnvironment webHostEnvironment,
            ILanguageRepository languageRepository,
            OrgaChartDatabase orgaChartDatabase)
        {
            _webHostEnvironment = webHostEnvironment;
            _languageRepository = languageRepository;
            _orgaChartDatabase = orgaChartDatabase;
        }

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times", Justification = "Reviewed")]
        public byte[] CreateExport(Guid dgId, Guid languageId, out string dgName)
        {
            using (var memoryStream = new MemoryStream())
            {
                var templateFile = Path.Combine(_webHostEnvironment.ContentRootPath, "OrgaChartExport.xlsx");
                using (var fileStream = new FileStream(templateFile, FileMode.Open, FileAccess.Read))
                {
                    fileStream.CopyTo(memoryStream);
                }

                memoryStream.Seek(0, SeekOrigin.Begin);
                using (var spreadsheetDocument = SpreadsheetDocument.Open(memoryStream, true))
                {
                    var rootNode = _orgaChartDatabase.GetOrgaTreeForExcelExport(dgId);
                    dgName = rootNode.Name;
                    int row = 2; // data will first be written on row 3
                    RecursiveWrite(_orgaChartDatabase, spreadsheetDocument, ref row, rootNode);
                }

                return memoryStream.ToArray();
            }
        }

        private void RecursiveWrite(
            OrgaChartDatabase db,
            SpreadsheetDocument spreadsheetDocument,
            ref int row,
            OrgaEntity orgaEntity)
        {
            // Go to next row
            row++;
            // Mark Cell according to hierarchy level.
            if (orgaEntity.LogicalLevel == 1)
            {
                SpreadSheetTools.Insert("X", row, OrgaChartSheetLayout.ColumnDg, _orgaChartSheetName, spreadsheetDocument);
                // DG's particular case
                SpreadSheetTools.Insert("X", row + 1, OrgaChartSheetLayout.ColumnDg, _orgaChartSheetName, spreadsheetDocument);
            }
            else if (orgaEntity.LogicalLevel == 2)
            {
                SpreadSheetTools.Insert("X", row, OrgaChartSheetLayout.ColumnDir, _orgaChartSheetName, spreadsheetDocument);
            }
            else if (orgaEntity.LogicalLevel == 3)
            {
                SpreadSheetTools.Insert("X", row, OrgaChartSheetLayout.ColumnUnit, _orgaChartSheetName, spreadsheetDocument);
            }

            // order index
            SpreadSheetTools.Insert(orgaEntity.OrderIndex.ToString(), row, OrgaChartSheetLayout.ColumnOrderIndex, _orgaChartSheetName, spreadsheetDocument);

            // Build location value (LUX, BRU, BRU+LUX, etc.)
            string locationString = "";
            locationString += orgaEntity.PrimaryAddress != null ? "" + orgaEntity.PrimaryAddress.ActiveDirectoryReference : "";
            locationString += orgaEntity.SecondaryAddress != null ? "+" + orgaEntity.SecondaryAddress.ActiveDirectoryReference : "";
            SpreadSheetTools.Insert(locationString, row, OrgaChartSheetLayout.ColumnLocation, _orgaChartSheetName, spreadsheetDocument);
            if (!orgaEntity.ShowInHeader)
            {
                SpreadSheetTools.Insert("X", row, OrgaChartSheetLayout.ColumnNotADg, _orgaChartSheetName, spreadsheetDocument);
            }

            // Internal Key
            SpreadSheetTools.Insert(orgaEntity.Id.ToString(), row, OrgaChartSheetLayout.ColumnKey, _orgaChartSheetName, spreadsheetDocument);
            if (orgaEntity.LogicalLevel == 1)
            {
                SpreadSheetTools.Insert(orgaEntity.Id.ToString(), row + 1, OrgaChartSheetLayout.ColumnKey, _orgaChartSheetName, spreadsheetDocument);
            }

            // Acronym
            SpreadSheetTools.Insert(orgaEntity.Name, row, OrgaChartSheetLayout.ColumnAcronym, _orgaChartSheetName, spreadsheetDocument);

            // Function
            SpreadSheetTools.Insert(orgaEntity.Function, row, OrgaChartSheetLayout.ColumnFunction, _orgaChartSheetName, spreadsheetDocument);

            // Gender
            SpreadSheetTools.Insert(orgaEntity.Gender, row, OrgaChartSheetLayout.ColumnGender, _orgaChartSheetName, spreadsheetDocument);

            // Languages
            var allLanguages = _languageRepository.GetAllLanguages();
            var translations = db.GetTranslations(orgaEntity.HeaderTextId.GetValueOrDefault(), allLanguages);
            foreach (var lang in allLanguages)
            {
                string columnIndex = SpreadSheetTools.GetFirstColumnIndexWithValueOnRow(
                    lang.Name,
                    2,
                    _orgaChartSheetName,
                    spreadsheetDocument);
                var translation = translations.FirstOrDefault(t => t.LanguageId == lang.Id);

                // DG's particular case
                if (orgaEntity.LogicalLevel == 1 && translation != null)
                {
                    string line1 = "";
                    string line2 = "";
                    if (translation.Value != null && translation.Value.Contains("<>"))
                    {
                        var translationLines = translation.Value.Split(new[] { "<>" }, StringSplitOptions.None);
                        line1 = translationLines[0];
                        line2 = translationLines[1];
                    }
                    else if (translation.Value != null)
                    {
                        line1 = translation.Value;
                    }

                    SpreadSheetTools.Insert(line1, row, columnIndex, _orgaChartSheetName, spreadsheetDocument);
                    SpreadSheetTools.Insert(line2, row + 1, columnIndex, _orgaChartSheetName, spreadsheetDocument);
                }
                else if (translation == null || string.IsNullOrWhiteSpace(translation.Value))
                {
                    SpreadSheetTools.Insert("", row, columnIndex, _orgaChartSheetName, spreadsheetDocument);
                }
                else
                {
                    SpreadSheetTools.Insert(translation.Value, row, columnIndex, _orgaChartSheetName, spreadsheetDocument);
                }
            }

            // DG's particular case
            if (orgaEntity.LogicalLevel == 1)
            {
                row++;
            }

            if (orgaEntity.LogicalLevel < 3)
            {
                var children = orgaEntity.SubEntities.ToList();
                foreach (var child in children.Where(x => x.LogicalLevel == 3)
                                              .OrderBy(x => x.OrderIndex)
                                              .ThenBy(x => x.Name))
                {
                    RecursiveWrite(db, spreadsheetDocument, ref row, child);
                    children.Remove(child);
                }

                foreach (var child in children
                                      .OrderBy(x => x.OrderIndex)
                                      .ThenBy(x => x.Name))
                {
                    RecursiveWrite(db, spreadsheetDocument, ref row, child);
                }
            }
        }
    }
}
