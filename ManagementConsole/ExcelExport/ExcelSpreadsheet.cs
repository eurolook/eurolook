using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Eurolook.Common.Extensions;
using A = DocumentFormat.OpenXml.Drawing;

namespace Eurolook.ManagementConsole.ExcelExport
{
    /// <summary>
    /// A class that can create an OpenXML Excel document based on the content of an <see cref="IEnumerable{T}"/>
    /// This class is based on the code generated by the OpenXML Document Reflector included in the OpenXML SDK.
    /// </summary>
    /// <typeparam name="T">The type of object whose properties are goint to be exported into the Excel spreadsheet.</typeparam>
    [SuppressMessage("SonarQube", "S3220:Method calls should not resolve ambiguously to overloads with params", Justification = "Reviewed")]
    [SuppressMessage("ReSharper", "PossiblyMistakenUseOfParamsMethod", Justification = "Reviewed")]
    public class ExcelSpreadsheet<T>
    {
        private readonly Dictionary<string, int> _sharedStringLookupTable = new Dictionary<string, int>(8000);

        public ExcelSpreadsheet()
        {
            FirstTableRow = 1;
        }

        public IList<ExportColumnSpec<T>> Columns { get; set; }

        public IEnumerable<T> Items { get; set; }
        public Func<T, byte> OutlineLevelCalculator { get; set; }

        public string WorksheetName { get; set; }

        public string WorksheetTitle { get; set; }

        public string MetaDataCreator { get; set; }

        public string MetaDataTitle { get; set; }

        public int FirstTableRow { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times", Justification = "Reviewed")]
        public byte[] CreatePackage()
        {
            using (var ms = new MemoryStream())
            {
                using (var package = SpreadsheetDocument.Create(ms, SpreadsheetDocumentType.Workbook, true))
                {
                    CreateParts(package);
                }

                return ms.ToArray();
            }
        }

        private void CreateParts(SpreadsheetDocument document)
        {
            var workbookPart = document.AddWorkbookPart();
            var worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
            var workbookStylesPart = workbookPart.AddNewPart<WorkbookStylesPart>();
            var themePart = workbookPart.AddNewPart<ThemePart>();

            var tableDefinitionPart = worksheetPart.AddNewPart<TableDefinitionPart>();

            // Create Workbook Part
            GenerateWorkbookPartContent(workbookPart, workbookPart.GetIdOfPart(worksheetPart));

            // Create Worksheet Part
            GenerateWorksheetPartContent(worksheetPart, worksheetPart.GetIdOfPart(tableDefinitionPart));

            // Create Shared String Table Part if necessary
            if (_sharedStringLookupTable.Count > 0)
            {
                var sharedStringTablePart = workbookPart.AddNewPart<SharedStringTablePart>();
                GenerateSharedStringTablePart(sharedStringTablePart);
            }

            GenerateTableDefinitionPartContent(tableDefinitionPart);

            GenerateWorkbookStylesPartContent(workbookStylesPart);

            GenerateThemePartContent(themePart);

            SetPackageProperties(document);

            document.Close();
        }

        private void GenerateWorkbookPartContent(WorkbookPart workbookPart, StringValue worksheetPartId)
        {
            var writer = OpenXmlWriter.Create(workbookPart);

            writer.WriteStartElement(new Workbook());
            writer.WriteStartElement(new Sheets());

            writer.WriteElement(new Sheet()
            {
                Name = WorksheetName,
                SheetId = 1,
                Id = worksheetPartId,
            });

            writer.WriteEndElement(); // end of sheets
            writer.WriteEndElement(); // end of workbook

            writer.Close();
        }

        private void GenerateWorkbookStylesPartContent(WorkbookStylesPart workbookStylesPart)
        {
            var stylesheet = new Stylesheet { MCAttributes = new MarkupCompatibilityAttributes { Ignorable = "x14ac" } };
            stylesheet.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
            stylesheet.AddNamespaceDeclaration("x14ac", "http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac");

            var numberingFormats = new NumberingFormats()
            {
                Count = 1U,
            };
            var numberingFormat = new NumberingFormat()
            {
                NumberFormatId = 164U,
                FormatCode = "dd/mm/yyyy\\ hh:mm:ss",
            };
            numberingFormats.Append(numberingFormat);

            var fonts = new Fonts(
                new Font(
                    new FontSize { Val = 11D },
                    new Color { Theme = 1U },
                    new FontName { Val = "Calibri" },
                    new FontFamilyNumbering { Val = 2 },
                    new FontScheme { Val = FontSchemeValues.Minor }),
                new Font(
                    new FontSize { Val = 18D },
                    new Color { Theme = 4U },
                    new FontName { Val = "Calibri" },
                    new FontFamilyNumbering { Val = 2 },
                    new FontScheme { Val = FontSchemeValues.Major }),
                new Font(
                    new FontSize { Val = 11D },
                    new Color { Theme = 1U },
                    new FontName { Val = "Consolas" },
                    new FontFamilyNumbering { Val = 3 }))
            { Count = 3U, KnownFonts = true };

            var fills = new Fills(
                new Fill(new PatternFill { PatternType = PatternValues.None }),
                new Fill(new PatternFill { PatternType = PatternValues.Gray125 }))
            { Count = 2U };

            var borders = new Borders(
                new Border(
                    new LeftBorder(),
                    new RightBorder(),
                    new TopBorder(),
                    new BottomBorder(),
                    new DiagonalBorder()))
            { Count = 1U };

            var cellStyleFormats = new CellStyleFormats(
                new CellFormat
                {
                    NumberFormatId = 0U,
                    FontId = 0U,
                    FillId = 0U,
                    BorderId = 0U,
                    Alignment = new Alignment { Vertical = VerticalAlignmentValues.Top, WrapText = true },
                    ApplyAlignment = true,
                },
                new CellFormat
                {
                    NumberFormatId = 0U,
                    FontId = 1U,
                    FillId = 0U,
                    BorderId = 0U,
                    Alignment = new Alignment { Vertical = VerticalAlignmentValues.Top, WrapText = true },
                    ApplyNumberFormat = false,
                    ApplyFill = false,
                    ApplyBorder = false,
                    ApplyAlignment = true,
                    ApplyProtection = false,
                },
                new CellFormat
                {
                    NumberFormatId = 49U,
                    FontId = 2U,
                    FillId = 0U,
                    BorderId = 0U,
                    Alignment = new Alignment { Vertical = VerticalAlignmentValues.Top, WrapText = true },
                    ApplyNumberFormat = false,
                    ApplyFill = false,
                    ApplyBorder = false,
                    ApplyAlignment = true,
                    ApplyProtection = false,
                })
            { Count = 3U };

            var cellFormats = new CellFormats(
                    new CellFormat()
                    {
                        NumberFormatId = 0U,
                        FontId = 0U,
                        FillId = 0U,
                        BorderId = 0U,
                        FormatId = 0U,
                        Alignment = new Alignment() { Vertical = VerticalAlignmentValues.Top, WrapText = true },
                        ApplyAlignment = true,
                    },
                    new CellFormat()
                    {
                        NumberFormatId = 0U,
                        FontId = 1U,
                        FillId = 0U,
                        BorderId = 0U,
                        FormatId = 1U,
                        ApplyBorder = true,
                        ApplyAlignment = true,
                    },
                    new CellFormat()
                    {
                        NumberFormatId = 164U,
                        FontId = 0U,
                        FillId = 0U,
                        BorderId = 0U,
                        FormatId = 0U,
                        Alignment = new Alignment() { Vertical = VerticalAlignmentValues.Top, WrapText = true },
                        ApplyNumberFormat = true,
                        ApplyAlignment = true,
                    })
            { Count = 8U };

            var cellStyles = new CellStyles(
                new CellStyle { Name = "Normal", FormatId = 0U, BuiltinId = 0U },
                new CellStyle { Name = "Title", FormatId = 1U, BuiltinId = 15U, CustomBuiltin = true },
                new CellStyle { Name = "Monospace", FormatId = 2U })
            { Count = 3U };

            var differentialFormats = new DifferentialFormats(
                    new DifferentialFormat(
                        new NumberingFormat
                        {
                            NumberFormatId = 164U,
                            FormatCode = "dd/mm/yyyy\\ hh:mm:ss",
                        }))
            { Count = 1U };

            var tableStyles = new TableStyles
            {
                Count = 0U,
                DefaultTableStyle = "TableStyleMedium2",
                DefaultPivotStyle = "PivotStyleLight16",
            };

            stylesheet.Append(numberingFormats);
            stylesheet.Append(fonts);
            stylesheet.Append(fills);
            stylesheet.Append(borders);
            stylesheet.Append(cellStyleFormats);
            stylesheet.Append(cellFormats);
            stylesheet.Append(cellStyles);
            stylesheet.Append(differentialFormats);
            stylesheet.Append(tableStyles);

            workbookStylesPart.Stylesheet = stylesheet;
        }

        private void GenerateSharedStringTablePart(SharedStringTablePart sharedStringTablePart)
        {
            var writer = OpenXmlWriter.Create(sharedStringTablePart);
            writer.WriteStartElement(new SharedStringTable());

            foreach (var sharedString in _sharedStringLookupTable)
            {
                writer.WriteStartElement(new SharedStringItem());
                writer.WriteElement(new Text(sharedString.Key));
                writer.WriteEndElement(); // of SharedStringItem
            }

            writer.WriteEndElement(); // of SharedStringTable
            writer.Close();
        }

        private void GenerateThemePartContent(ThemePart themePart)
        {
            var theme = new A.Theme { Name = "Office Theme" };
            theme.AddNamespaceDeclaration("a", "http://schemas.openxmlformats.org/drawingml/2006/main");

            var colorScheme = new A.ColorScheme(
                new A.Dark1Color(new A.SystemColor { Val = A.SystemColorValues.WindowText, LastColor = "000000" }),
                new A.Light1Color(new A.SystemColor { Val = A.SystemColorValues.Window, LastColor = "FFFFFF" }),
                new A.Dark2Color(new A.RgbColorModelHex { Val = "44546A" }),
                new A.Light2Color(new A.RgbColorModelHex { Val = "E7E6E6" }),
                new A.Accent1Color(new A.RgbColorModelHex { Val = "0072C6" }),
                new A.Accent2Color(new A.RgbColorModelHex { Val = "3399FF" }),
                new A.Accent3Color(new A.RgbColorModelHex { Val = "A5A5A5" }),
                new A.Accent4Color(new A.RgbColorModelHex { Val = "145B9B" }),
                new A.Accent5Color(new A.RgbColorModelHex { Val = "0091A8" }),
                new A.Accent6Color(new A.RgbColorModelHex { Val = "3D9579" }),
                new A.Hyperlink(new A.RgbColorModelHex { Val = "0563C1" }),
                new A.FollowedHyperlinkColor(new A.RgbColorModelHex { Val = "954F72" }))
            { Name = "Eurolook" };

            var fontScheme = new A.FontScheme(
                new A.MajorFont(
                    new A.LatinFont { Typeface = "Calibri Light", Panose = "020F0302020204030204" },
                    new A.EastAsianFont { Typeface = "" },
                    new A.ComplexScriptFont { Typeface = "" }),
                new A.MinorFont(
                    new A.LatinFont { Typeface = "Calibri", Panose = "020F0502020204030204" },
                    new A.EastAsianFont { Typeface = "" },
                    new A.ComplexScriptFont { Typeface = "" }))
            { Name = "Office" };

            var formatScheme = new A.FormatScheme(
                new A.FillStyleList(
                    new A.SolidFill(new A.SchemeColor { Val = A.SchemeColorValues.PhColor }),
                    new A.GradientFill(
                        new A.GradientStopList(
                            new A.GradientStop(
                                new A.SchemeColor(
                                    new A.LuminanceModulation { Val = 110000 },
                                    new A.SaturationModulation { Val = 105000 },
                                    new A.Tint { Val = 67000 })
                                { Val = A.SchemeColorValues.PhColor })
                            { Position = 0 },
                            new A.GradientStop(
                                new A.SchemeColor(
                                    new A.LuminanceModulation { Val = 105000 },
                                    new A.SaturationModulation { Val = 103000 },
                                    new A.Tint { Val = 73000 })
                                { Val = A.SchemeColorValues.PhColor })
                            { Position = 50000 },
                            new A.GradientStop(
                                new A.SchemeColor(
                                    new A.LuminanceModulation { Val = 105000 },
                                    new A.SaturationModulation { Val = 109000 },
                                    new A.Tint { Val = 81000 })
                                { Val = A.SchemeColorValues.PhColor })
                            { Position = 100000 }),
                        new A.LinearGradientFill { Angle = 5400000, Scaled = false })
                    { RotateWithShape = true },
                    new A.GradientFill(
                        new A.GradientStopList(
                            new A.GradientStop(
                                new A.SchemeColor(
                                    new A.SaturationModulation { Val = 103000 },
                                    new A.LuminanceModulation { Val = 102000 },
                                    new A.Tint { Val = 94000 })
                                { Val = A.SchemeColorValues.PhColor })
                            { Position = 0 },
                            new A.GradientStop(
                                new A.SchemeColor(
                                    new A.SaturationModulation { Val = 110000 },
                                    new A.LuminanceModulation { Val = 100000 },
                                    new A.Shade { Val = 100000 })
                                { Val = A.SchemeColorValues.PhColor })
                            { Position = 50000 },
                            new A.GradientStop(
                                new A.SchemeColor(
                                    new A.LuminanceModulation { Val = 99000 },
                                    new A.SaturationModulation { Val = 120000 },
                                    new A.Shade { Val = 78000 })
                                { Val = A.SchemeColorValues.PhColor })
                            { Position = 100000 }),
                        new A.LinearGradientFill { Angle = 5400000, Scaled = false })
                    { RotateWithShape = true }),
                new A.LineStyleList(
                    new A.Outline(
                        new A.SolidFill(new A.SchemeColor { Val = A.SchemeColorValues.PhColor }),
                        new A.PresetDash { Val = A.PresetLineDashValues.Solid },
                        new A.Miter { Limit = 800000 })
                    {
                        Width = 6350,
                        CapType = A.LineCapValues.Flat,
                        CompoundLineType = A.CompoundLineValues.Single,
                        Alignment = A.PenAlignmentValues.Center,
                    },
                    new A.Outline(
                        new A.SolidFill(new A.SchemeColor { Val = A.SchemeColorValues.PhColor }),
                        new A.PresetDash { Val = A.PresetLineDashValues.Solid },
                        new A.Miter { Limit = 800000 })
                    {
                        Width = 12700,
                        CapType = A.LineCapValues.Flat,
                        CompoundLineType = A.CompoundLineValues.Single,
                        Alignment = A.PenAlignmentValues.Center,
                    },
                    new A.Outline(
                        new A.SolidFill(new A.SchemeColor { Val = A.SchemeColorValues.PhColor }),
                        new A.PresetDash { Val = A.PresetLineDashValues.Solid },
                        new A.Miter { Limit = 800000 })
                    {
                        Width = 19050,
                        CapType = A.LineCapValues.Flat,
                        CompoundLineType = A.CompoundLineValues.Single,
                        Alignment = A.PenAlignmentValues.Center,
                    }),
                new A.EffectStyleList(
                    new A.EffectStyle(new A.EffectList()),
                    new A.EffectStyle(new A.EffectList()),
                    new A.EffectStyle(
                        new A.EffectList(
                            new A.OuterShadow(new A.RgbColorModelHex(new A.Alpha { Val = 63000 }) { Val = "000000" })
                            {
                                BlurRadius = 57150L,
                                Distance = 19050L,
                                Direction = 5400000,
                                Alignment = A.RectangleAlignmentValues.Center,
                                RotateWithShape = false,
                            }))),
                new A.BackgroundFillStyleList(
                    new A.SolidFill(new A.SchemeColor { Val = A.SchemeColorValues.PhColor }),
                    new A.SolidFill(
                        new A.SchemeColor(
                            new A.Tint { Val = 95000 },
                            new A.SaturationModulation { Val = 170000 })
                        { Val = A.SchemeColorValues.PhColor }),
                    new A.GradientFill(
                        new A.GradientStopList(
                            new A.GradientStop(
                                new A.SchemeColor(
                                    new A.Tint { Val = 93000 },
                                    new A.SaturationModulation { Val = 150000 },
                                    new A.Shade { Val = 98000 },
                                    new A.LuminanceModulation { Val = 102000 })
                                { Val = A.SchemeColorValues.PhColor })
                            { Position = 0 },
                            new A.GradientStop(
                                new A.SchemeColor(
                                    new A.Tint { Val = 98000 },
                                    new A.SaturationModulation { Val = 130000 },
                                    new A.Shade { Val = 90000 },
                                    new A.LuminanceModulation { Val = 103000 })
                                { Val = A.SchemeColorValues.PhColor })
                            { Position = 50000 },
                            new A.GradientStop(
                                new A.SchemeColor(
                                    new A.Shade { Val = 63000 },
                                    new A.SaturationModulation { Val = 120000 })
                                { Val = A.SchemeColorValues.PhColor })
                            { Position = 100000 }),
                        new A.LinearGradientFill { Angle = 5400000, Scaled = false })
                    { RotateWithShape = true }))
            { Name = "Office" };

            theme.Append(
                new A.ThemeElements(
                    colorScheme,
                    fontScheme,
                    formatScheme));

            themePart.Theme = theme;
        }

        private void GenerateWorksheetPartContent(WorksheetPart worksheetPart, string tableDefinitionPartId)
        {
            var writer = OpenXmlWriter.Create(worksheetPart);
            writer.WriteStartElement(new Worksheet());

            // Sheet Properties
            writer.WriteStartElement(new SheetProperties());
            writer.WriteElement(new OutlineProperties
            {
                ApplyStyles = true,
                SummaryBelow = false,
                SummaryRight = false,
            });
            writer.WriteEndElement(); // end of SheetProperties

            // Sheet Dimension
            writer.WriteElement(new SheetDimension
            {
                Reference =
                    $"A1:{ComputeColumnName(Columns.Count)}{(Items.Count() + FirstTableRow).ToString(CultureInfo.InvariantCulture)}",
            });

            // Sheet Format Properties
            writer.WriteElement(new SheetFormatProperties
            {
                DefaultRowHeight = 15D,
            });

            // Columns
            writer.WriteStartElement(new Columns());
            for (int i = 0; i < Columns.Count; i++)
            {
                writer.WriteElement(new Column { Min = (uint)(i + 1), Max = (uint)(i + 1), Width = Columns[i].Width, CustomWidth = true });
            }

            writer.WriteEndElement(); // end of Columns

            // Sheet Data
            writer.WriteStartElement(new SheetData());

            // Worksheet Title
            if (!string.IsNullOrEmpty(WorksheetTitle))
            {
                writer.WriteStartElement(new Row(), new List<OpenXmlAttribute>
                {
                    // this is the row index
                    new OpenXmlAttribute("r", null, "1"),
                    new OpenXmlAttribute("spans", null, "1:" + Columns.Count),
                });

                WriteCell(writer, 1, 1, InsertSharedStringItem(WorksheetTitle), CellType.SharedString, 1U);

                writer.WriteEndElement(); // of Row
            }

            // Worksheet Header
            writer.WriteStartElement(new Row(), new List<OpenXmlAttribute>
            {
                // this is the row index
                new OpenXmlAttribute("r", null, FirstTableRow.ToString()),
                new OpenXmlAttribute("spans", null, "1:" + Columns.Count),
            });

            for (int col = 0; col < Columns.Count; ++col)
            {
                WriteCell(writer, FirstTableRow, col + 1, InsertSharedStringItem(Columns[col].Header), CellType.SharedString);
            }

            writer.WriteEndElement(); // of Row

            // Worksheet Data
            int row = FirstTableRow + 1;
            foreach (var item in Items)
            {
                writer.WriteStartElement(new Row(), new List<OpenXmlAttribute>
                {
                    new OpenXmlAttribute("r", null, row.ToString()),
                    new OpenXmlAttribute("spans", null, "1:" + Columns.Count),
                    new OpenXmlAttribute("outlineLevel", null, OutlineLevelCalculator(item).ToString()),
                    new OpenXmlAttribute("hidden", null, OutlineLevelCalculator(item) > 0 ? "1" : "0"),
                });

                int col = 1;
                foreach (var cell in Columns.Select(c => c.CellFormatter))
                {
                    var (cellContent, cellType) = cell(item);

                    string cellText;
                    if (cellType == CellType.DateTime && cellContent is DateTime dateTime)
                    {
                        cellText = dateTime.ToOADate().ToString(CultureInfo.InvariantCulture);
                    }
                    else if (cellContent != null)
                    {
                        cellText = cellContent.ToString();
                    }
                    else
                    {
                        cellText = string.Empty;
                        cellType = CellType.SharedString;
                    }

                    if (cellType == CellType.SharedString)
                    {
                        cellText = InsertSharedStringItem(cellText);
                    }

                    WriteCell(writer, row, col, cellText, cellType);
                    ++col;
                }

                writer.WriteEndElement(); // of Row
                ++row;
            }

            writer.WriteEndElement(); // end of SheetData

            writer.WriteStartElement(new TableParts(), new List<OpenXmlAttribute>
            {
                new OpenXmlAttribute("count", null, "1"),
            });
            writer.WriteElement(new TablePart
            {
                Id = tableDefinitionPartId,
            });
            writer.WriteEndElement(); // end of TableParts

            writer.WriteEndElement(); // end of Worksheet
            writer.Close();
        }

        private void GenerateTableDefinitionPartContent(TableDefinitionPart tableDefinitionPart)
        {
            string tableRange = $"A{FirstTableRow}:{ComputeColumnName(Columns.Count)}{Items.Count() + FirstTableRow}";
            var table1 = new Table { Id = 1U, Name = "Table1", DisplayName = "Table1", Reference = tableRange, TotalsRowShown = false };

            table1.Append(new AutoFilter { Reference = tableRange });

            var tableColumns = new TableColumns { Count = (uint)Columns.Count };
            for (uint i = 0; i < Columns.Count; i++)
            {
                var tableColumn = new TableColumn
                {
                    Id = (UInt32Value)i + 1,
                    Name = Columns[(int)i].Header,
                };

                if (Columns[(int)i].CellType == CellType.DateTime)
                {
                    tableColumn.DataFormatId = 0;
                }

                tableColumns.Append(tableColumn);
            }

            table1.Append(tableColumns);

            table1.Append(
                new TableStyleInfo
                {
                    Name = "TableStyleLight17",
                    ShowFirstColumn = false,
                    ShowLastColumn = false,
                    ShowRowStripes = true,
                    ShowColumnStripes = false,
                });

            tableDefinitionPart.Table = table1;
        }

        private void SetPackageProperties(OpenXmlPackage package)
        {
            package.PackageProperties.Creator = MetaDataCreator;
            package.PackageProperties.Title = MetaDataTitle;

            var timeStamp = DateTime.Now;
            package.PackageProperties.Created = timeStamp;
            package.PackageProperties.Modified = timeStamp;
        }

        private static void WriteCell(OpenXmlWriter writer, int row, int col, string text, CellType dataType, uint styleIndex = 0U)
        {
            string type = string.Empty;
            switch (dataType)
            {
                case CellType.SharedString:
                    type = "s";
                    break;
                case CellType.InlineString:
                    type = "inlineStr";
                    break;
                case CellType.Number:
                    type = "n";
                    break;
                case CellType.Date:
                    type = "d";
                    break;
                case CellType.DateTime:
                    styleIndex = 2U;
                    break;
                case CellType.Boolean:
                    type = "b";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(dataType), dataType, null);
            }

            var attributes = new List<OpenXmlAttribute>
            {
                new OpenXmlAttribute("r", null, ComputeColumnName(col) + row),
                new OpenXmlAttribute("s", null, styleIndex.ToString()),
            };

            if (!type.IsNullOrEmpty())
            {
                attributes.Add(new OpenXmlAttribute("t", null, type));
            }

            writer.WriteStartElement(new Cell(), attributes);
            writer.WriteStartElement(new CellValue());
            writer.WriteString(text);
            writer.WriteEndElement(); // of CellValue
            writer.WriteEndElement(); // of Cell
        }

        private string GetCleanCellText(string text)
        {
            return Regex.Replace(text, @"([\u0000-\u0008]|[\u000b-\u000c]|[\u000e-\u001F])+", string.Empty);
        }

        private string InsertSharedStringItem(string text)
        {
            string safeText = GetCleanCellText(text ?? string.Empty);

            if (_sharedStringLookupTable.ContainsKey(safeText))
            {
                return _sharedStringLookupTable[safeText].ToString(CultureInfo.InvariantCulture);
            }

            int newIndex = _sharedStringLookupTable.Keys.Count;
            _sharedStringLookupTable.Add(safeText, newIndex);

            return newIndex.ToString(CultureInfo.InvariantCulture);
        }

        private static string ComputeColumnName(int columnIndex)
        {
            int dividend = columnIndex;
            string columnName = string.Empty;

            while (dividend > 0)
            {
                int modifier = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modifier).ToString(CultureInfo.InvariantCulture) + columnName;
                dividend = (dividend - modifier) / 26;
            }

            return columnName;
        }
    }
}
