namespace Eurolook.ManagementConsole.ExcelExport
{
    public class OrgaChartSheetLayout
    {
        public const string ColumnDg = "A";
        public const string ColumnDir = "B";
        public const string ColumnUnit = "C";
        public const string ColumnOrderIndex = "D";
        public const string ColumnLocation = "E";
        public const string ColumnNotADg = "F";
        public const string ColumnAcronym = "G";
        public const string ColumnFunction = "H";
        public const string ColumnGender = "I";
        public const string ColumnKey = "J";
    }
}
