﻿using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.Models;

namespace Eurolook.ManagementConsole.ExcelExport
{
    /// <summary>
    /// Use this class to create informative log to the user when modifying several parts of the OrgaChart at the same
    /// time
    /// (ie : when importing a new Excel File)
    /// </summary>
    public class ChangeMessage
    {
        public ChangeMessage(int row)
        {
            Row = row;
        }

        public ChangeMessageType Type { get; set; }

        public string Message { get; set; }

        public int Row { get; }

        public static ChangeMessage ForModifiedAcronym(string oldOrgaEntityName, string newOrgaEntityName, int row)
        {
            return new ChangeMessage(row)
            {
                Type = ChangeMessageType.ModifiedAcronym,
                Message = $"Acronym for entity {oldOrgaEntityName} changed to '{newOrgaEntityName}'.",
            };
        }

        public static ChangeMessage ForModifiedLogicalLevel(int oldLogicalLevel, int newLogicalLevel, OrgaEntity orgaEntity, int row)
        {
            return new ChangeMessage(row)
            {
                Type = ChangeMessageType.ModifiedLogicalLevel,
                Message = $"Level of entity '{orgaEntity.Name}' changed from '{GetLogicalLevelName(oldLogicalLevel)}' to '{GetLogicalLevelName(newLogicalLevel)}'.",
            };
        }

        public static ChangeMessage ForModifiedShowInHeader(bool oldSetStatus, bool newShowInHeaderStatus, OrgaEntity orgaEntity, int row)
        {
            return new ChangeMessage(row)
            {
                Type = ChangeMessageType.ModifiedShowInHeader,
                Message = $"Not a DG field for entity '{orgaEntity.Name}' changed from '{!oldSetStatus}' to '{!newShowInHeaderStatus}'.",
            };
        }

        public static ChangeMessage ForModifiedLocation(string oldLocation, string newLocation, OrgaEntity orgaEntity, int row)
        {
            return new ChangeMessage(row)
            {
                Type = ChangeMessageType.ModifiedLocation,
                Message = $"Location for entity '{orgaEntity.Name}' changed from '{oldLocation}' to '{newLocation}'.",
            };
        }

        public static ChangeMessage ForModifiedFunction(string oldFunction, string newFunction, OrgaEntity orgaEntity, int row)
        {
            return new ChangeMessage(row)
            {
                Type = ChangeMessageType.ModifiedLocation,
                Message = $"Function of entity '{orgaEntity.Name}' changed from '{oldFunction}' to '{newFunction}'.",
            };
        }

        public static ChangeMessage ForModifiedGender(string oldGender, string newGender, OrgaEntity orgaEntity, int row)
        {
            return new ChangeMessage(row)
            {
                Type = ChangeMessageType.ModifiedLocation,
                Message = $"Gender of entity '{orgaEntity.Name}' changed from '{oldGender}' to '{newGender}'.",
            };
        }

        public static ChangeMessage ForMovedDirectorate(string oldDgName, string newDgName, OrgaEntity orgaEntity, int row)
        {
            return new ChangeMessage(row)
            {
                Type = ChangeMessageType.MovedDirectorate,
                Message = $"Directorate '{orgaEntity.Name}' moved from directorate general '{oldDgName}' to directorate general '{newDgName}'.",
            };
        }

        public static ChangeMessage ForMovedUnit(string oldDirectorateName, string newDirectorateName, OrgaEntity orgaEntity, int row)
        {
            return new ChangeMessage(row)
            {
                Type = ChangeMessageType.MovedUnit,
                Message = $"Unit '{orgaEntity.Name}' moved from directorate '{oldDirectorateName}' to directorate '{newDirectorateName}'.",
            };
        }

        public static ChangeMessage ForAddedDirectorate(string newDgName, OrgaEntity orgaEntity, int row)
        {
            var mess = new ChangeMessage(row)
            {
                Type = ChangeMessageType.AddedDirectorate,
                Message = $"Directorate '{orgaEntity.Name}' added in directorate general '{newDgName}'.",
            };

            return mess;
        }

        public static ChangeMessage ForAddedUnit(string newDirectorateName, OrgaEntity orgaEntity, int row)
        {
            var mess = new ChangeMessage(row)
            {
                Type = ChangeMessageType.AddedUnit,
                Message = $"Unit '{orgaEntity.Name}' added in directorate '{newDirectorateName}'.",
            };

            return mess;
        }

        public static ChangeMessage ForRemovedDirectorate(int authorsCount, OrgaEntity orgaEntity, int row)
        {
            var mess = new ChangeMessage(row)
            {
                Type = ChangeMessageType.RemovedDirectorate,
                Message = $"Directorate '{orgaEntity.Name}' is removed. {authorsCount} authors were attached.",
            };

            return mess;
        }

        public static ChangeMessage ForRemovedUnit(int authorsCount, OrgaEntity orgaEntity, int row)
        {
            var mess = new ChangeMessage(row)
            {
                Type = ChangeMessageType.RemovedUnit,
                Message = $"Unit '{orgaEntity.Name}' is removed. {authorsCount} authors were attached.",
            };

            return mess;
        }

        public static ChangeMessage ForAddedTranslation(string[] translations, string[] langName, OrgaEntity orgaEntity, int row)
        {
            var mess = new ChangeMessage(row);
            mess.Type = ChangeMessageType.AddedTranslation;
            if (translations.Length == 1)
            {
                mess.Message = $"Name for entity '{orgaEntity.Name}' added in {langName[0]} : {translations[0]}.";
            }
            else if (translations.Length < 5)
            {
                string langsString = langName[0];
                for (int i = 1; i < translations.Length - 1; i++)
                {
                    langsString += ", " + langName[i];
                }

                langsString += " and " + langName[translations.Length - 1];
                mess.Message = $"Name for entity '{orgaEntity.Name}' added in {langsString}.";
            }
            else
            {
                mess.Message = $"Name for entity '{orgaEntity.Name}' added in {translations.Length} languages.";
            }

            return mess;
        }

        public static ChangeMessage ForModifiedTranslation(
            string[] oldTranslations,
            string[] translations,
            string[] langName,
            OrgaEntity orgaEntity,
            int row)
        {
            var mess = new ChangeMessage(row);
            mess.Type = ChangeMessageType.ModifiedTranslation;
            if (translations.Length == 1)
            {
                mess.Message =
                    $"Name for entity '{orgaEntity.Name}' modified from '{oldTranslations[0]}' to '{translations[0]}' in {langName[0]}.";
            }
            else if (translations.Length < 5)
            {
                string langsString = langName[0];
                for (int i = 1; i < translations.Length - 1; i++)
                {
                    langsString += ", " + langName[i];
                }

                langsString += " and " + langName[translations.Length - 1];
                mess.Message = $"Name for entity '{orgaEntity.Name}' modified in {langsString}.";
            }
            else
            {
                mess.Message = $"Name for entity '{orgaEntity.Name}' modified in {translations.Length} languages.";
            }

            return mess;
        }

        public static ChangeMessage ForDeletedTranslations(string[] deletedTranslations, string[] langName, OrgaEntity orgaEntity, int row)
        {
            var mess = new ChangeMessage(row) { Type = ChangeMessageType.DeletedTranslations };

            for (int i = 0; i < deletedTranslations.Length; i++)
            {
                mess.Message = $"Translation for entity ''{orgaEntity.Name}'' deleted for language {langName[i]}";
            }

            return mess;
        }

        private static string GetLogicalLevelName(int logicalLevel)
        {
            switch (logicalLevel)
            {
                case 1:
                    return "DG";
                case 2:
                    return "Directorate";
                case 3:
                    return "Unit/Function";
                default:
                    return "Unknown";
            }
        }
    }

    public class ChangesMessageBuilder
    {
        public ChangesMessageBuilder()
        {
            Messages = new List<ChangeMessage>();
        }

        public List<ChangeMessage> Messages { get; }

        public string FreeTextMessage { get; set; }

        public void AddModifiedAcronym(string oldOrgaEntityName, string newOrgaEntityName, int row)
        {
            Messages.Add(ChangeMessage.ForModifiedAcronym(oldOrgaEntityName, newOrgaEntityName, row));
        }

        public void AddModifiedLogicalLevel(int oldLogicalLevel, int newLogicalLevel, OrgaEntity orgaEntity, int row)
        {
            Messages.Add(ChangeMessage.ForModifiedLogicalLevel(oldLogicalLevel, newLogicalLevel, orgaEntity, row));
        }

        public void AddModifiedLocation(string oldLocation, string newLocation, OrgaEntity orgaEntity, int row)
        {
            Messages.Add(ChangeMessage.ForModifiedLocation(oldLocation, newLocation, orgaEntity, row));
        }

        public void AddModifiedShowInHeader(bool oldShowInHeaderStatus, bool newShowInHeaderStatus, OrgaEntity orgaEntity, int row)
        {
            Messages.Add(ChangeMessage.ForModifiedShowInHeader(oldShowInHeaderStatus, newShowInHeaderStatus, orgaEntity, row));
        }

        public void AddModifiedFunction(string oldFunction, string newFunction, OrgaEntity orgaEntity, int row)
        {
            Messages.Add(ChangeMessage.ForModifiedFunction(oldFunction, newFunction, orgaEntity, row));
        }

        public void AddModifiedGender(string oldGender, string newGender, OrgaEntity orgaEntity, int row)
        {
            Messages.Add(ChangeMessage.ForModifiedGender(oldGender, newGender, orgaEntity, row));
        }

        public void AddMovedDirectorate(string oldDgName, string newDgName, OrgaEntity orgaEntity, int row)
        {
            Messages.Add(ChangeMessage.ForMovedDirectorate(oldDgName, newDgName, orgaEntity, row));
        }

        public void AddMovedUnit(string oldDirectorateName, string newDirectorateName, OrgaEntity orgaEntity, int row)
        {
            Messages.Add(ChangeMessage.ForMovedUnit(oldDirectorateName, newDirectorateName, orgaEntity, row));
        }

        public void AddAddedDirectorate(string newDgName, OrgaEntity orgaEntity, int row)
        {
            Messages.Add(ChangeMessage.ForAddedDirectorate(newDgName, orgaEntity, row));
        }

        public void AddAddedUnit(string newDirectorateName, OrgaEntity orgaEntity, int row)
        {
            Messages.Add(ChangeMessage.ForAddedUnit(newDirectorateName, orgaEntity, row));
        }

        public void AddRemovedOrgaEntity(int authorsCount, OrgaEntity orgaEntity)
        {
            if (orgaEntity.LogicalLevel == 2)
            {
                Messages.Add(ChangeMessage.ForRemovedDirectorate(authorsCount, orgaEntity, 0));
            }

            if (orgaEntity.LogicalLevel == 3)
            {
                Messages.Add(ChangeMessage.ForRemovedUnit(authorsCount, orgaEntity, 0));
            }
        }

        public void AddAddedTranslation(string[] translations, string[] langName, OrgaEntity orgaEntity, int row)
        {
            if (translations.Length == 0)
            {
                return;
            }

            Messages.Add(ChangeMessage.ForAddedTranslation(translations, langName, orgaEntity, row));
        }

        public void AddModifiedTranslation(
            string[] oldTranslations,
            string[] translations,
            string[] langName,
            OrgaEntity orgaEntity,
            int row)
        {
            if (translations.Length == 0)
            {
                return;
            }

            if (translations.Length != oldTranslations.Length)
            {
                return;
            }

            Messages.Add(ChangeMessage.ForModifiedTranslation(oldTranslations, translations, langName, orgaEntity, row));
        }

        public void AddRemovedTranslations(string[] translations, string[] langName, OrgaEntity orgaEntity, int row)
        {
            if (translations.Length == 0)
            {
                return;
            }

            Messages.Add(ChangeMessage.ForDeletedTranslations(translations, langName, orgaEntity, row));
        }

        public string OutputString()
        {
            if (Messages.Count == 0)
            {
                return "No changes have been made.";
            }

            string output = "### Output start ###\n";
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var changeMessage in Messages.OrderBy(o => o.Type).ThenBy(o => o.Row))
            {
                output += $"{changeMessage.Message} (Row #{changeMessage.Row})\n";
            }

            output += "### Output end ###\n";
            output += FreeTextMessage;
            return output;
        }
    }
}
