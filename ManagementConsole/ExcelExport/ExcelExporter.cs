﻿using System.Collections.Generic;
using System.Linq;
using Eurolook.Common.Extensions;
using Eurolook.ManagementConsole.Models;

namespace Eurolook.ManagementConsole.ExcelExport
{
    public class ExcelExporter
    {
        public byte[] ExportExceptionLog(IEnumerable<ExceptionBucketInfo> items)
        {
            var rowData = new List<ExceptionLogRow>();
            if (items.IsNullOrEmpty())
            {
                // Fix corrupted Excel sheet
                rowData.Add(new ExceptionLogRow { OutlineLevel = 0 });
            }
            else
            {
                foreach (var item in items)
                {
                    rowData.Add(
                        new ExceptionLogRow
                        {
                            OutlineLevel = 0,
                            Id = item.ExceptionBucketId.ToHexString(),
                            Message = item.LastMessage,
                            Count = item.Count,
                            StackTrace = item.StackTrace,
                            LastOccurrence = item.LastOccurred,
                            AffectedUsers = item.AffectedUsers,
                        });

                    rowData.AddRange(
                        item.Entries.Select(
                            e => new ExceptionLogRow
                            {
                                OutlineLevel = 1,
                                Originator = e.Originator,
                                Occurred = e.Occurred,
                                OfficeVersion = e.OfficeVersion,
                                ClientVersion = e.ClientVersion,
                            }));
                }
            }

            var exporter = new ExcelSpreadsheet<ExceptionLogRow>
            {
                Columns = new[]
                {
                    new ExportColumnSpec<ExceptionLogRow>("Id", e => e.Id?.Substring(0, 7) ?? string.Empty, 18),
                    new ExportColumnSpec<ExceptionLogRow>("Message", e => e.Message, 100),
                    new ExportColumnSpec<ExceptionLogRow>("Count", e => e.Count, 18, CellType.Number),
                    new ExportColumnSpec<ExceptionLogRow>("StackTrace", e => e.StackTrace, 100),
                    new ExportColumnSpec<ExceptionLogRow>("Last Occurred", e => e.LastOccurrence, 24, CellType.DateTime),
                    new ExportColumnSpec<ExceptionLogRow>(
                        "Affected Users",
                        e => e.AffectedUsers,
                        24,
                        CellType.Number),
                    new ExportColumnSpec<ExceptionLogRow>("Originator", e => e.Originator, 24),
                    new ExportColumnSpec<ExceptionLogRow>("Occurred", e => e.Occurred, 24, CellType.DateTime),
                    new ExportColumnSpec<ExceptionLogRow>("Client Version", e => e.ClientVersion, 18),
                    new ExportColumnSpec<ExceptionLogRow>("Office Version", e => e.OfficeVersion, 18),
                },
                MetaDataCreator = "Eurolook",
                MetaDataTitle = "Exception Log",
                WorksheetName = "Exception Log",
                WorksheetTitle = "Eurolook Exception Log",
                FirstTableRow = 3,
                OutlineLevelCalculator = i => i.OutlineLevel,
                Items = rowData,
            };
            return exporter.CreatePackage();
        }

        public byte[] ExportExceptionLog(IEnumerable<ExceptionLogInfo> items)
        {
            var exporter = new ExcelSpreadsheet<ExceptionLogInfo>()
            {
                Columns = new[]
                {
                    new ExportColumnSpec<ExceptionLogInfo>("Id", e => e.ExceptionBucketId.ToHexString(), 44),
                    new ExportColumnSpec<ExceptionLogInfo>("Occurred (UTC)", e => e.CreatedUtc, 18),
                    new ExportColumnSpec<ExceptionLogInfo>("Class", e => e.LoggerName, 60),
                    new ExportColumnSpec<ExceptionLogInfo>("Message", e => e.Message, 100),
                    new ExportColumnSpec<ExceptionLogInfo>("Exception Type", e => e.ExceptionType, 50),
                    new ExportColumnSpec<ExceptionLogInfo>("Stacktrace", e => e.StackTrace, 100),
                    new ExportColumnSpec<ExceptionLogInfo>("Originator Id", e => e.OriginatorId, 44),
                    new ExportColumnSpec<ExceptionLogInfo>("Originator", e => e.Originator, 75),
                    new ExportColumnSpec<ExceptionLogInfo>("Client Version", e => e.ClientVersion, 18),
                    new ExportColumnSpec<ExceptionLogInfo>("Office Version", e => e.OfficeVersion, 18),
                },
                MetaDataCreator = "Eurolook",
                MetaDataTitle = "Exception Log",
                WorksheetName = "Exception Log",
                WorksheetTitle = "Eurolook Exception Log",
                FirstTableRow = 3,
                Items = items,
                OutlineLevelCalculator = i => 0,
            };

            return exporter.CreatePackage();
        }

        public byte[] ExportFeedback(IEnumerable<FeedbackInfo> items)
        {
            var exporter = new ExcelSpreadsheet<FeedbackInfo>
            {
                Columns = new[]
                {
                    new ExportColumnSpec<FeedbackInfo>("Submitted", f => f.Submitted, 44),
                    new ExportColumnSpec<FeedbackInfo>("User", f => f.User, 44),
                    new ExportColumnSpec<FeedbackInfo>("Rating", f => f.Rating, 44),
                    new ExportColumnSpec<FeedbackInfo>("Message", f => f.Message, 100),
                },
                MetaDataCreator = "Eurolook",
                MetaDataTitle = "Feedback",
                WorksheetName = "Feedback",
                WorksheetTitle = "Eurolook Feedback",
                FirstTableRow = 3,
                OutlineLevelCalculator = i => 0,
                Items = items,
            };

            return exporter.CreatePackage();
        }
    }
}
