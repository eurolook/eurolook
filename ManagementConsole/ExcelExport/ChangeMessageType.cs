namespace Eurolook.ManagementConsole.ExcelExport
{
    public enum ChangeMessageType
    {
        ModifiedAcronym,
        ModifiedLogicalLevel,
        ModifiedLocation,
        ModifiedShowInHeader,
        MovedDirectorate,
        MovedUnit,
        AddedDirectorate,
        AddedUnit,
        RemovedDirectorate,
        RemovedUnit,
        AddedTranslation,
        ModifiedTranslation,
        InvalidMessage,
        DeletedTranslations,
    }
}
