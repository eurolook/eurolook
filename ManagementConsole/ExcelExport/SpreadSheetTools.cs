using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace Eurolook.ManagementConsole.ExcelExport
{
    public static class SpreadSheetTools
    {
        #region SharedString

        /// <summary>
        /// Return document's SharedStringTable as a string array. If CellType is SHAREDSTRING, CellValue is an index and
        /// what you expect to be in the Cell is SharedStringTable[CellValue]. See MS document for more info.
        /// </summary>
        /// <param name="spreadSheet"></param>
        /// <returns></returns>
        public static string[] GetSharedStringTable(SpreadsheetDocument spreadSheet)
        {
            var sharedStringTable = spreadSheet.WorkbookPart.SharedStringTablePart.SharedStringTable;
            var sharedStringItemList = sharedStringTable.Descendants<SharedStringItem>().ToList();
            var sharedStringArray = new string[sharedStringItemList.Count];
            for (int i = 0; i < sharedStringArray.Length; i++)
            {
                sharedStringArray[i] = sharedStringItemList[i].FirstChild.InnerText;
            }

            return sharedStringArray;
        }

        #endregion

        [Serializable]
        public class InvalidColumnRefException : Exception
        {
            public InvalidColumnRefException()
            {
            }

            public InvalidColumnRefException(string message)
                : base(message)
            {
            }

            public InvalidColumnRefException(string message, Exception inner)
                : base(message, inner)
            {
            }

            protected InvalidColumnRefException(
                SerializationInfo info,
                StreamingContext context)
                : base(info, context)
            {
            }
        }

        [Serializable]
        public class InvalidRowIndexException : Exception
        {
            public InvalidRowIndexException()
            {
            }

            public InvalidRowIndexException(string message)
                : base(message)
            {
            }

            public InvalidRowIndexException(string message, Exception inner)
                : base(message, inner)
            {
            }

            protected InvalidRowIndexException(
                SerializationInfo info,
                StreamingContext context)
                : base(info, context)
            {
            }
        }

        /// <summary>
        /// Dictionary of SheetData accessed by their names. Caching SheetData in code avoid repeating XML access in the
        /// document.
        /// </summary>
        /// <param name="spreadSheet"></param>
        /// <returns></returns>
        #region SheetData
        internal static Dictionary<string, SheetData> GetSheetDatas(SpreadsheetDocument spreadSheet)
        {
            var sheetDatas = new Dictionary<string, SheetData>();
            var sheets = spreadSheet.WorkbookPart.Workbook.Sheets;
            var sheetList = sheets.Descendants<Sheet>().ToList();

            foreach (var sheet in sheetList)
            {
                var worksheetPart = spreadSheet.WorkbookPart.GetPartById(sheet.Id) as WorksheetPart;
                if (worksheetPart != null)
                {
                    var sheetData = worksheetPart.Worksheet.Elements<SheetData>().FirstOrDefault();
                    if (sheetData != null)
                    {
                        sheetDatas.Add(sheet.Name.Value, sheetData);
                    }
                }
            }

            return sheetDatas;
        }

        /// <summary>
        /// Using GetSheetDatas might be wiser if you want the only or the first sheet of a document, or to do reading job
        /// in the document, as this one return null if no existing sheetName is provided.
        /// </summary>
        /// <param name="sheetName"></param>
        /// <param name="spreadSheet"></param>
        /// <returns></returns>
        private static SheetData GetSheetData(string sheetName, SpreadsheetDocument spreadSheet)
        {
            var sheets = spreadSheet.WorkbookPart.Workbook.Sheets;
            var sheet = sheets.Descendants<Sheet>().FirstOrDefault(s => s.Name == sheetName);
            if (sheet == null)
            {
                return null;
            }

            var workSheetPart = spreadSheet.WorkbookPart.GetPartById(sheet.Id) as WorksheetPart;
            if (workSheetPart == null)
            {
                return null;
            }

            var sheetData = workSheetPart.Worksheet.Elements<SheetData>().FirstOrDefault();
            return sheetData;
        }

        #endregion

        #region Row

        private static Row GetRow(string sheetName, int rowIndex, SpreadsheetDocument spreadSheet)
        {
            return GetRow(rowIndex, GetSheetData(sheetName, spreadSheet));
        }

        private static Row GetRow(int rowIndex, SheetData sheetData)
        {
            if (sheetData == null)
            {
                return null;
            }

            return sheetData.Descendants<Row>().FirstOrDefault(r => r.RowIndex == Convert.ToUInt32(rowIndex));
        }

        private static Row AddRow(int rowIndex, SheetData sheetData)
        {
            var row = new Row();
            row.RowIndex = Convert.ToUInt32(rowIndex);
            if (sheetData.Descendants<Row>().Any())
            {
                var nextRow = sheetData.Descendants<Row>().First();
                while (nextRow.RowIndex < rowIndex)
                {
                    nextRow = nextRow.NextSibling<Row>();
                    if (nextRow == null)
                    {
                        sheetData.AppendChild(row);
                        return row;
                    }
                }

                nextRow.InsertBeforeSelf(row);
                return row;
            }

            sheetData.AppendChild(row);
            return row;
        }

        private static Row GetOrCreateRow(int rowIndex, SheetData sheetData)
        {
            var row = GetRow(rowIndex, sheetData) ?? AddRow(rowIndex, sheetData);

            return row;
        }

        #endregion

        #region Cell

        private static Cell GetCell(string sheetName, int rowIndex, string columnReference, SpreadsheetDocument spreadSheet)
        {
            return GetCell(GetRow(rowIndex, GetSheetData(sheetName, spreadSheet)), columnReference);
        }

        private static Cell GetCell(int rowIndex, string columnReference, SheetData sheetData)
        {
            return GetCell(GetRow(rowIndex, sheetData), columnReference);
        }

        private static Cell GetCell(Row row, string columnReference)
        {
            if (!IsValid(columnReference))
            {
                throw new InvalidColumnRefException("Column Ref [" + columnReference + "] is not valid.");
            }

            if (row == null)
            {
                return null;
            }

            string cellReference = columnReference + row.RowIndex;

            return row.Descendants<Cell>().FirstOrDefault(c => c.CellReference == cellReference);
        }

        private static Cell AddCell(string columnReference, Row row)
        {
            var cell = new Cell();
            cell.CellReference = columnReference + row.RowIndex;
            if (row.Descendants<Cell>().Any())
            {
                var nextCell = row.Descendants<Cell>().First();
                // Get the proper cell position (most spreadsheet readers will reject unsorted row)
                while (IsRightTo(cell, nextCell))
                {
                    nextCell = nextCell.NextSibling<Cell>();
                    if (nextCell == null)
                    {
                        row.AppendChild(cell);
                        return cell;
                    }
                }

                nextCell.InsertBeforeSelf(cell);
                return cell;
            }

            row.AppendChild(cell);
            return cell;
        }

        private static Cell GetOrCreateCell(string sheetName, int rowIndex, string columnReference, SpreadsheetDocument spreadSheet)
        {
            return GetOrCreateCell(GetOrCreateRow(rowIndex, GetSheetData(sheetName, spreadSheet)), columnReference);
        }

        private static Cell GetOrCreateCell(int rowIndex, string columnReference, SheetData sheetData)
        {
            return GetOrCreateCell(GetOrCreateRow(rowIndex, sheetData), columnReference);
        }

        private static Cell GetOrCreateCell(Row row, string columnReference)
        {
            var cell = GetCell(row, columnReference) ?? AddCell(columnReference, row);
            return cell;
        }

        private static List<Cell> GetCellsInRow(Row row)
        {
            var cells = new List<Cell>();

            if (row != null)
            {
                cells = row.Descendants<Cell>().ToList();
            }

            return cells;
        }

        private static List<Cell> GetCellsWithValueOnRow(string value, Row row, SpreadsheetDocument spreadSheet)
        {
            return GetCellsInRow(row).FindAll(c => ReadCell(c, spreadSheet) == value);
        }

        public static List<Cell> GetCellsInRow(int rowIndex, SheetData sheetData)
        {
            return GetCellsInRow(GetRow(rowIndex, sheetData));
        }

        public static List<Cell> GetCellsWithValueOnRow(string value, int rowIndex, string sheetName, SpreadsheetDocument spreadSheet)
        {
            return GetCellsWithValueOnRow(value, GetRow(sheetName, rowIndex, spreadSheet), spreadSheet);
        }

        public static Cell GetFirstCellWithValueOnRow(string value, int rowIndex, string sheetName, SpreadsheetDocument spreadSheet)
        {
            return GetCellsWithValueOnRow(value, rowIndex, sheetName, spreadSheet).FirstOrDefault();
        }

        public static string GetFirstColumnIndexWithValueOnRow(
            string value,
            int rowIndex,
            string sheetName,
            SpreadsheetDocument spreadSheet)
        {
            var cell = GetFirstCellWithValueOnRow(value, rowIndex, sheetName, spreadSheet);
            string columnIndex = "";
            if (cell != null)
            {
                columnIndex = GetColumnRef(cell);
            }

            return columnIndex;
        }

        #endregion

        #region Read

        /// <summary>
        /// Do full search on the SpreadSheet to reach the Cell.
        /// </summary>
        /// <param name="sheetName"></param>
        /// <param name="rowIndex"></param>
        /// <param name="columnReference"></param>
        /// <param name="spreadSheet"></param>
        /// <returns></returns>
        public static string ReadCell(string sheetName, int rowIndex, string columnReference, SpreadsheetDocument spreadSheet)
        {
            var cell = GetCell(sheetName, rowIndex, columnReference, spreadSheet);
            return ReadCell(cell, spreadSheet);
        }

        public static string ReadCell(Cell cell, SpreadsheetDocument spreadSheet)
        {
            if (cell == null)
            {
                return "";
            }

            if (cell.DataType == CellValues.SharedString)
            {
                var sharedString = GetSharedStringTable(spreadSheet);
                if (int.TryParse(cell.CellValue.InnerText, out int sharedStringIndex))
                {
                    return sharedString[sharedStringIndex];
                }

                return "";
            }

            if (cell.DataType == CellValues.String)
            {
                return cell.CellValue.InnerText;
            }

            return "";
        }

        #endregion

        #region Write

        /// <summary>
        /// Will overwrite any existing content in the Cell.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="rowIndex"></param>
        /// <param name="columnReference"></param>
        /// <param name="sheetName"></param>
        /// <param name="spreadSheet"></param>
        /// <param name="asSharedString"></param>
        /// <returns></returns>
        public static int Insert(
            string value,
            int rowIndex,
            string columnReference,
            string sheetName,
            SpreadsheetDocument spreadSheet,
            bool asSharedString = false)
        {
            var cell = GetOrCreateCell(sheetName, rowIndex, columnReference, spreadSheet);

            if (cell == null)
            {
                return -1;
            }

            Insert(value, cell);

            return 1;
        }

        /// <summary>
        /// Will overwrite any existing content in the Cell.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="rowIndex"></param>
        /// <param name="columnReference"></param>
        /// <param name="sheetData"></param>
        /// <param name="asSharedString"></param>
        /// <returns></returns>
        public static int Insert(string value, int rowIndex, string columnReference, SheetData sheetData, bool asSharedString = false)
        {
            var cell = GetOrCreateCell(rowIndex, columnReference, sheetData);

            if (cell == null)
            {
                return -1;
            }

            Insert(value, cell);

            return 1;
        }

        /// <summary>
        /// Will overwrite any existing content in the Cell.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="cell"></param>
        /// <returns></returns>
        public static int Insert(string value, Cell cell)
        {
            cell.CellValue = new CellValue(value);
            cell.DataType = CellValues.String;

            return 1;
        }

        #endregion

        #region ColumnOperations

        public static string GetColumnRef(Cell cell)
        {
            string cellReference = cell.CellReference.Value;
            string columnRef = "";

            for (int i = 0; i < cellReference.Length; i++)
            {
                if (char.IsLetter(cellReference[i]))
                {
                    columnRef += cellReference[i];
                }
            }

            return columnRef;
        }

        public static string GetColumnRef(int columnIndex)
        {
            string columnRef = "";
            int sum = columnIndex;
            do
            {
                columnRef = (char)(columnIndex % 26 + 'A') + columnRef;
                sum = sum - sum % 26;
                sum = sum / 26;
            }
            while (sum > 0);

            return columnRef;
        }

        private static bool IsRightTo(Cell cellRight, Cell cellLeft)
        {
            if (cellRight == null)
            {
                return false;
            }

            if (cellLeft == null)
            {
                return false;
            }

            return IsRightTo(GetColumnRef(cellRight), GetColumnRef(cellLeft));
        }

        private static bool IsRightTo(string columnRefRight, string columnRefLeft)
        {
            // ColumnRefXInt is NOT the rank of the column from the left of the sheet. Theese values have no absolute signification and should only be compared
            // column string references.
            int columnRefRightInt = 0;
            int columnRefLeftInt = 0;

            for (int i = 0; i < columnRefRight.Length; i++)
            {
                columnRefRightInt += columnRefRight[columnRefRight.Length - (i + 1)] * (int)Math.Floor(Math.Pow(100, i));
            }

            for (int i = 0; i < columnRefLeft.Length; i++)
            {
                columnRefLeftInt += columnRefLeft[columnRefLeft.Length - (i + 1)] * (int)Math.Floor(Math.Pow(100, i));
            }

            return columnRefRightInt > columnRefLeftInt;
        }

        #endregion

        #region ValidityTest

        internal static bool IsValid(string columnReference)
        {
            var columnReferenceCheck = new Regex(@"^[A-Z]+$");
            // XFE is the last column on the right of an Excel sheet.
            return columnReferenceCheck.IsMatch(columnReference) && IsRightTo("XFE", columnReference);
        }

        internal static bool IsValid(int rowIndex)
        {
            // 1048575 is the last row index of an Excel sheet.
            return (rowIndex > 0) && (rowIndex < 1048576);
        }

        #endregion
    }
}
