using System;

namespace Eurolook.ManagementConsole.ExcelExport
{
    public class ExceptionLogRow
    {
        public byte OutlineLevel { get; set; }
        public string Id { get; set; }
        public string Message { get; set; }
        public int? Count { get; set; }
        public string StackTrace { get; set; }
        public DateTime? LastOccurrence { get; set; }
        public int? AffectedUsers { get; set; }
        public string Originator { get; set; }
        public DateTime? Occurred { get; set; }
        public string OfficeVersion { get; set; }
        public string ClientVersion { get; set; }
    }
}
