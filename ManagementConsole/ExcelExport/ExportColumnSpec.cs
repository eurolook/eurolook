using System;

namespace Eurolook.ManagementConsole.ExcelExport
{
    public enum CellType
    {
        SharedString,
        InlineString,
        Number,
        Boolean,
        Date,
        DateTime,
    }

    public class ExportColumnSpec<T>
    {
        public ExportColumnSpec(string header, Func<T, object> cellFormatter, double width = 15.0D, CellType cellType = CellType.SharedString)
        {
            Header = header;
            CellType = cellType;
            Width = width;

            CellFormatter = c => (cellFormatter(c), cellType);
        }

        public string Header { get; }
        public CellType CellType { get;  }
        public double Width { get; }

        public Func<T, (object, CellType)> CellFormatter { get; }
    }
}
