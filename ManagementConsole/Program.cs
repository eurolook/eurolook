﻿// This application entry point is based on ASP.NET Core new project templates and is included
// as a starting point for app host configuration.
// This file may need updated according to the specific scenario of the application being upgraded.
// For more information on ASP.NET Core hosting, see https://docs.microsoft.com/aspnet/core/fundamentals/host/web-host

using System;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Eurolook.ActiveDirectoryLink.AutofacModules;
using Eurolook.Common;
using Eurolook.Common.Extensions;
using Eurolook.Data;
using Eurolook.Data.Database;
using Eurolook.ManagementConsole.Areas.DataExchange;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.ExcelExport;
using Eurolook.ManagementConsole.ScheduledTasks;
using Eurolook.ManagementConsole.Services;
using Eurolook.Web.Common;
using Eurolook.Web.Common.ScheduledTask;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Server.IISIntegration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Serilog;

try
{
    var builder = WebApplication.CreateBuilder(
        new WebApplicationOptions
        {
            Args = args,
            ContentRootPath = Directory.GetCurrentDirectory(),
        });

    builder.AddDefaultConfigurationProviders();
    builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory());
    builder.Host.UseSerilog();

    builder.Services.AddControllers()
           .AddJsonOptions(
               options =>
               {
                   options.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
                   options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                   options.JsonSerializerOptions.AllowTrailingCommas = true;
                   options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                   options.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.Never;
                   options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
               });

    builder.Services.AddAuthentication(IISDefaults.AuthenticationScheme);

    builder.Services.AddAuthorization(
        options =>
        {
            var admins = builder.Configuration["Roles:Administrator"].Split(
                ',',
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            var orgaAdmins = builder.Configuration["Roles:OrgaAdmin"].Split(
                ',',
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            // Configure the default policy
            options.DefaultPolicy = new AuthorizationPolicyBuilder()
                                    .RequireAuthenticatedUser()
                                    .RequireRole(admins)
                                    .Build();

            options.AddPolicy("OrgaAdmin", policyBuilder => policyBuilder.RequireRole(orgaAdmins.Union(admins)));

            options.FallbackPolicy = options.DefaultPolicy;
        });

    builder.Services.AddHealthChecks()
           .AddSqlServer(builder.Configuration.GetConnectionString("ServerDatabaseConnection"))
           .AddSystemInfo(builder);

    builder.Services
           .AddControllersWithViews()
           .AddNewtonsoftJson(
               options =>
               {
                   //options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                   //options.SerializerSettings.DateFormatString = "dd.MM.yyyy HH:mm";
                   options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                   options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
               });
    builder.Services.AddRazorPages();

    // Register the Swagger generator, defining 1 or more Swagger documents
    builder.Services.AddSwaggerGen(
        c =>
        {
            c.SwaggerDoc(
                "v2",
                new OpenApiInfo
                {
                    Version = "v1",
                    Title = "AdminUI API",
                    Description = "The API of the AdminUI backend.",
                });

            // Set the comments path for the Swagger JSON and UI.
            ////var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            ////var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
            ////c.IncludeXmlComments(xmlPath);
        });

    builder.Host.ConfigureContainer<ContainerBuilder>(
        containerBuilder =>
        {
            // register services
            containerBuilder.RegisterType<MailService>();
            containerBuilder.RegisterType<UploadService>().SingleInstance();
            containerBuilder.RegisterType<ImportService>().SingleInstance();
            containerBuilder.RegisterType<ExportService>().SingleInstance();
            containerBuilder.RegisterType<ExcelOrgaChartExporter>().AsSelf();
            containerBuilder.RegisterType<ExceptionLogService>().AsSelf();
            containerBuilder.RegisterType<TextUsagesService>()
                            .AsImplementedInterfaces()
                            .InstancePerLifetimeScope();
            containerBuilder.RegisterInstance(new ApplicationInfo(builder.Environment.EnvironmentName)).SingleInstance();

            var assemblyType = typeof(Program).Assembly;
            containerBuilder.RegisterAssemblyModules(assemblyType);
            containerBuilder.RegisterModule(new ActiveDirectoryLinkModule());

            containerBuilder.RegisterType<OrgaEntityRepository>().AsImplementedInterfaces();
            containerBuilder.Register(_ => new EurolookServerContext(builder.Configuration.GetConnectionString("ServerDatabaseConnection")))
                            .As<EurolookContext>()
                            .As<EurolookServerContext>()
                            .ExternallyOwned();

            containerBuilder.RegisterAssemblyTypes(typeof(EurolookManagementConsoleDatabase).Assembly)
                            .Where(t => t.GetInterfaces().Contains(typeof(IEurolookManagementConsoleDatabase)) && !t.IsAbstract)
                            .AsImplementedInterfaces();

            var pluginLoader = new PluginLoader(AppContext.BaseDirectory, builder.Configuration.GetSection("Plugins").Get<string[]>());
            pluginLoader.RegisterSingleMandatoryPluginModule<IActiveDirectoryLinkModule>(containerBuilder);

            containerBuilder.RegisterType<SettingsService>().AsImplementedInterfaces();
            containerBuilder.RegisterType<ExceptionLogMailTask>().AsImplementedInterfaces().SingleInstance();
            if (builder.Configuration["FeatureManagement:KeepAliveTask"].ToBool())
            {
                containerBuilder.RegisterType<KeepAliveTask>().AsImplementedInterfaces().SingleInstance();
            }
        });

    var app = builder.Build();
    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v2/swagger.json", "Admin UI API"); });
        // app.UseDeveloperExceptionPage();
    }
    else
    {
        app.UseExceptionHandler("/Error");
        app.UseHsts();
    }

    app.CreateAndSetupLogger(typeof(Program).Assembly);

    app.UseRouting();
    app.UseAuthentication();
    app.UseAuthorization();
    app.UseStaticFiles();
    app.UseStaticFiles(
        new StaticFileOptions
        {
            FileProvider = new PhysicalFileProvider(Path.Combine(AppContext.BaseDirectory, "ViewModels")),
            RequestPath = new PathString("/ViewModels"),
        });
    app.UseStaticFiles(
        new StaticFileOptions
        {
            FileProvider = new PhysicalFileProvider(Path.Combine(AppContext.BaseDirectory, "Areas")),
            RequestPath = new PathString("/Areas"),
        });

    app.MapAreaControllerRoute(
        "DataExchange_default",
        "DataExchange",
        "DataExchange/{controller=Export}/{action=Index}/{id?}");

    app.MapAreaControllerRoute(
        "Metadata_default",
        "Metadata",
        "Metadata/{controller=MetadataDefinitions}/{action=Index}/{id?}");

    app.MapControllerRoute(
        "default",
        "{controller=Home}/{action=Index}/{id?}");

    app.UseEndpoints(endpoints =>
    {
        endpoints.MapHealthChecksWithAnonymousAccess("/health");
    });

    app.MapRazorPages();

    new AdminDatabase(builder.Configuration).Migrate();

    app.StartRegisteredScheduledTasks();
    app.Run();
}
catch (Exception ex)
{
    Log.Fatal(ex, "Host terminated unexpectedly");
}
finally
{
    Log.Information("Shutdown complete");
    Log.CloseAndFlush();
}
