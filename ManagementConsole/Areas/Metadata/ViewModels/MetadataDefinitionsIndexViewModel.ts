import { MetadataDefinitionViewModel } from "./MetadataDefinitionViewModel";
import { MetadataDefinitionTypesViewModel } from "./MetadataDefinitionTypesViewModel";
import { PaginatedTableViewModel } from "../../../ViewModels/PaginatedTableViewModel";

class MetadataDefinitionsIndexViewModel extends PaginatedTableViewModel {

  metadataDefinitions: KnockoutObservableArray<MetadataDefinitionViewModel> = ko.observableArray([]);

  newMetadataDefinitionName = ko.observable();

  metadataTypes = new MetadataDefinitionTypesViewModel().metadataTypes;
  newMetadataType = ko.observable(null);

  deleteModalTitle = ko.observable("");
  reviveModalTitle = ko.observable("");
  searchRequest: JQueryXHR;
  deletingMetadataDefinition: MetadataDefinitionViewModel;
  revivingMetadataDefinition: MetadataDefinitionViewModel;
  selectedMetadataDefinitions: KnockoutObservableArray<MetadataDefinitionViewModel> = ko.observableArray([]);

  // ReSharper disable SuspiciousThisUsage
  isSelectedAll = ko.pureComputed({
    read: function () {
      return this.selectedMetadataDefinitions().length > 0
        && this.selectedMetadataDefinitions().length === this.metadataDefinitions().length;
    },
    write: function (value) {
      this.selectedMetadataDefinitions(value ? this.metadataDefinitions().slice(0) : []);
    },
    owner: this
  });
  // ReSharper enable SuspiciousThisUsage
  includeMetadataCategories = ko.observable(false);

  constructor(baseUrl, isReadOnly: string) {
    super(baseUrl, "/Metadata/MetadataDefinitions", isReadOnly);

    if (!this.orderProperty()) {
      this.orderProperty("displayName");
      this.orderAscending(true);
    }
    this.loadPageData();
    $("#SearchBox").focus();
  }

  loadPageData() {
    if (this.searchRequest && this.searchRequest.readyState < 4) {
      this.searchRequest.abort();
    }
    this.showProgressSpinner();
    this.searchRequest = this.getJson(this.getPageUrl(this.apiEndpointUrl()),
      this.loadPageDataSucceeded,
      this);
  }

    loadPageDataSucceeded(result) {
        this.selectedMetadataDefinitions.removeAll();
    this.metadataDefinitions.removeAll();
    if (result.success === true) {
      const page = result.model;
      for (let metadataDefinition of page.data) {
        this.metadataDefinitions.push(new MetadataDefinitionViewModel(metadataDefinition));
      }
      this.setPagination(page);
    }
    this.hideProgressSpinner();
  }

  createMetadataDefinition() {
    this.postJson(`${this.apiEndpointUrl()}/Create`,
      ko.toJSON({ name: this.newMetadataDefinitionName(), metadataType: this.newMetadataType }),
      this.createMetadataDefinitionSucceeded,
      this);
  }

  createMetadataDefinitionSucceeded(data) {
    this.closeModal();
    if (data.success === true) {
      this.showAlert({ type: "success", message: data.message });
      this.loadPageData();
    } else {
      this.showAlert({ type: "error", message: data.message });
    }
  }

  downloadDataPackage() {
    if (this.selectedMetadataDefinitions().length === 0) {
      this.closeModal();
      this.showAlert({ type: "error", message: "No metadata definition selected." });
      return;
    }
    this.showProgressBar();
    this.postJson(`${this.apiEndpointUrl()}/Export?includeMetadataCategories=${this.includeMetadataCategories()}`,
      ko.toJSON(this.selectedMetadataDefinitions), this.downloadDataPackageSucceeded, this);
  }

  downloadDataPackageSucceeded(result) {
    if (result.success) {
      window.location.href = `${this.baseUrl()}/DataExchange/Export/Package/${result.model.packageId}`;

    } else {
      this.hideProgressBar();
      this.showAlert({ type: "error", message: result.message });
    }
  }

  deleteMetadataDefinition() {
    if (!this.deletingMetadataDefinition) return;
    this.deleteJson(`${this.apiEndpointUrl()}/Delete/${this.deletingMetadataDefinition.id()}`,
      ko.toJSON(this.deletingMetadataDefinition),
      this.deleteMetadataDefinitionSucceeded,
      this);
  }

  deleteMetadataDefinitionSucceeded(result) {
    this.closeModal();
    if (result.success === true) {
      this.showAlert({ type: "success", message: result.message });
      var deleteId = this.deletingMetadataDefinition.id();
      this.metadataDefinitions.remove(item => item.id() === deleteId);
    } else {
      this.showAlert({ type: "error", message: result.message });
    }
  }

  reviveMetadataDefinition() {
    if (!this.revivingMetadataDefinition) return;
    this.postJson(`${this.apiEndpointUrl()}/Revive/${this.revivingMetadataDefinition.id()}`,
      null,
      this.reviveMetadataDefinitionSucceeded,
      this);
  }

  reviveMetadataDefinitionSucceeded(result) {
    this.closeModal();
    if (result.success === true) {
      this.showAlert({ type: "success", message: result.message });
      var reviveId = this.revivingMetadataDefinition.id();
      this.metadataDefinitions.remove(item => item.id() === reviveId);
    } else {
      this.showAlert({ type: "error", message: result.message });
    }
  }

  showDeleteModal(viewModel: MetadataDefinitionViewModel) {
    this.deletingMetadataDefinition = viewModel;
    this.deleteModalTitle(viewModel.name());
    $("#DeleteModal").foundation("reveal", "open");
  }

  showReviveModal(viewModel: MetadataDefinitionViewModel) {
    this.revivingMetadataDefinition = viewModel;
    this.reviveModalTitle(viewModel.name());
    $("#ReviveModal").foundation("reveal", "open");
  }
}
export = MetadataDefinitionsIndexViewModel
