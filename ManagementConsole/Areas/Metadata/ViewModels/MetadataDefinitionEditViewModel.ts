import { MetadataDefinitionTypesViewModel } from "./MetadataDefinitionTypesViewModel";
import { MetadataCategoryViewModel } from "./MetadataCategoryViewModel";
import { TermSetSelectionViewModel } from "./TermSetSelectionViewModel";
import { UserGroupViewModel } from "UserGroups/UserGroupViewModel";
import { PageViewModel } from "../../../ViewModels/PageViewModel"

class MetadataDefinitionEditViewModel extends PageViewModel {

    id: string;
    url: string;
    name = ko.observable("");
    description = ko.observable("");
    displayName = ko.observable("");
    position = ko.observable(0);
    isReadOnly = ko.observable(false);
    editableByUserGroups = ko.observable("");
    allUserGroupDataModels: KnockoutObservableArray<UserGroupViewModel> = ko.observableArray();
    isHidden = ko.observable(false);
    isMandatory = ko.observable(false);
    minLength = ko.observable(0);
    maxLength = ko.observable(0);
    metadataType = ko.observable("");
    metadataCategories: KnockoutObservableArray<MetadataCategoryViewModel> = ko.observableArray([]);
    metadataCategoryId = ko.observable("");

    metadataTypes = ko.observableArray(new MetadataDefinitionTypesViewModel().metadataTypes);

    termSetId = ko.observable("");
    termSets: KnockoutObservableArray<TermSetSelectionViewModel> = ko.observableArray([]);

    termSetsLoaded = ko.observable(false);

    propertiesNotMapped = new Set(["metadataCategories", "termSets", "termSetsLoaded", "metadataCategories", "metadataTypes", "isSaving",
        "selectableMetadataTypes", "isBoundToTermSet", "globalProgressTimer", "apiEndpointUrl", "baseUrl", "url", "propertiesNotMapped"]);

    isSaving = ko.observable(false);

    constructor(baseUrl: string, id: string, isReadOnly: string) {
        super(baseUrl, "/Metadata/MetadataDefinitions", isReadOnly);
        this.id = id;
        this.url = this.apiEndpointUrl() + "/Edit/" + id;
        this.bindCtrlSave(this.save);
        this.loadTermSets();
    }

    load() {
        this.showProgressSpinner();
        this.getJson(this.url, this.loadSucceeded, this);
    }

    loadSucceeded(result) {
        this.name(result.model.name);
        this.description(result.model.description);
        this.displayName(result.model.displayName);

        this.position(result.model.position);
        this.isReadOnly(result.model.isReadOnly);
        this.editableByUserGroups(result.model.editableByUserGroups)
        for (let userGroup of result.model.allUserGroupDataModels) {
            this.allUserGroupDataModels.push(new UserGroupViewModel(userGroup));
        }
        this.isHidden(result.model.isHidden);
        this.isMandatory(result.model.isMandatory);
        this.minLength(result.model.minLength);
        this.maxLength(result.model.maxLength);
        this.metadataType(result.model.metadataType);
        this.termSetId(result.model.termSetId);

        this.metadataCategories(result.model.metadataCategories);
        this.metadataCategoryId(result.model.metadataCategoryId);

        this.hideProgressSpinner();
    }

    loadTermSets() {
        this.showProgressBar();
        this.getJson(this.apiEndpointUrl() + "/TermSets", this.loadTermSetsSucceeded, this);
    }

    loadTermSetsSucceeded(result) {
        this.termSets.removeAll();
        this.termSetsLoaded(true);

        for (let termset of result.model) {
            this.termSets.push(new TermSetSelectionViewModel(termset));
        }

        this.load();

        this.hideProgressBar();
    }

    isBoundToTermSet = ko.pureComputed(
        () => this.metadataType() === undefined ? false : this.metadataTypes()[this.metadataType()].boundToTermStore);

    selectableMetadataTypes = ko.pureComputed(() => {
        var isNotBoundToTermStore = metadataType => !metadataType.boundToTermStore;

        return (this.termSets().length > 0) ? this.metadataTypes : this.metadataTypes().filter(isNotBoundToTermStore);
    });

    save() {
        if (this.isSaving()) return;
        this.isSaving(true);
        
        if (!this.isBoundToTermSet()) {
            this.termSetId = null;
        }

        // strip superfluous properties
        const vm = ko.toJSON(this,
            (key, value) => {
                if (this.propertiesNotMapped.has(key)){
                    return;
                } else {
                    return value;
                }
            });

        this.putJson(this.url, vm, this.saveSucceeded, this);
    }

    saveSucceeded(result) {
        if (!result.success) {
            this.showAlert({ type: "error", message: result.message });
        } else {
            this.showAlert({ type: "success", message: result.message });
        }

        this.isSaving(false);
    }

    showProgressBar(targetElementId?: string) {
        window.clearTimeout(this.globalProgressTimer);
        this.globalProgressTimer = window.setTimeout(() => {
            let spinner = document.getElementById("actionProgressBar");
            if (targetElementId != null) {
                const target = document.getElementById(targetElementId);
                spinner = target.appendChild(spinner);
            }
            if (spinner) {
                spinner.style.display = "block";
            }
        }, 600);
    }

    hideProgressBar() {
        window.clearTimeout(this.globalProgressTimer);
        const spinner = document.getElementById("actionProgressBar");
        if (spinner && spinner.style.display !== "none") {
            spinner.style.display = "none";
        }
    }
}
export = MetadataDefinitionEditViewModel
