export class MetadataDefinitionTypesViewModel {

  metadataTypes = [
    { name: "Text", value: 0, boundToTermStore: false },
    { name: "Long Text", value: 1, boundToTermStore: false },
    { name: "Numeric", value: 2, boundToTermStore: false },
    { name: "Boolean", value: 3, boundToTermStore: false },
    { name: "Date", value: 4, boundToTermStore: false },
    { name: "Time", value: 5, boundToTermStore: false },
    { name: "Date and Time", value: 6, boundToTermStore: false },
    { name: "Single Bounded Term", value: 7, boundToTermStore: true },
    { name: "Multiple Bounded Terms", value: 8, boundToTermStore: true },
    { name: "Single User", value: 9, boundToTermStore: false },
    { name: "Multiple Users", value: 10, boundToTermStore: false }
  ];
}
