export class TermSetSelectionViewModel {

    sharePointId = ko.observable("");
    name = ko.observable("");

    constructor(model) {
        if (!model) return;

        this.sharePointId(model.sharePointId);
        this.name(model.name);
    }
}