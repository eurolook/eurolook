export class MetadataDefinitionViewModel {

    id = ko.observable("");
    name = ko.observable("");
    displayName = ko.observable("");
    metadataType = ko.observable("");
    position = ko.observable(0);

    isSelected = ko.observable(false);

    constructor(model) {
        if (!model) return;

        this.id = ko.observable(model.id);
        this.name = ko.observable(model.name);
        this.displayName = ko.observable(model.displayName);
        this.metadataType = ko.observable(model.metadataType);
        this.position = ko.observable(model.position);
    }
}