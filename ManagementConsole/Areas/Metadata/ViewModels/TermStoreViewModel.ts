import { PageViewModel } from "../../../ViewModels/PageViewModel"

class TermStoreViewModel extends PageViewModel {
    termStoreModificationTime = ko.observable("");
    exportEndpointUrl = ko.observable("");
    progressBarVisible = ko.observable(false);
    fileFormData = new FormData();

    constructor(baseUrl: string, isReadOnly: string) {
        super(baseUrl, "/Metadata/TermStore", isReadOnly);

        this.exportEndpointUrl(this.apiEndpointUrl() + '/Export');
        this.load();
    }

    load() {
        this.showProgressSpinner();
        this.getJson(this.apiEndpointUrl(), this.loadSucceeded, this);
    }

    loadSucceeded(result) {
        this.termStoreModificationTime(result.message);
        this.hideProgressSpinner();
    }

    importTermStoreFile(file: File) {
        this.showActionProgressBar();
        this.fileFormData = new FormData();
        this.fileFormData.append("file", file);

        this.postFormDataWithCallback(this.apiEndpointUrl() + "/Import", this.fileFormData, this.importTermStoreFileSucceeded, this.importTermStoreFileFailed, this);
    }

    importTermStoreFileSucceeded(result) {
        this.hideActionProgressBar();
        if (result.success) {
            this.showAlert({ type: "success", message: "Term Store file successfully imported." });
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    importTermStoreFileFailed(result) {
        this.hideActionProgressBar();
        this.showAlert({ type: "error", message: "HTTP error: " + result.errorThrown });
    }

    showActionProgressBar(targetElementId?: string) {
        window.clearTimeout(this.globalProgressTimer);
        this.globalProgressTimer = window.setTimeout(() => {
            let spinner = document.getElementById("actionProgressBar");
            if (targetElementId != null) {
                const target = document.getElementById(targetElementId);
                spinner = target.appendChild(spinner);
            }
            if (spinner) {
                spinner.style.display = "block";
            }
        }, 600);
    }

    hideActionProgressBar() {
        window.clearTimeout(this.globalProgressTimer);
        const spinner = document.getElementById("actionProgressBar");
        if (spinner && spinner.style.display !== "none") {
            spinner.style.display = "none";
        }
    }
}
export = TermStoreViewModel
