import { MetadataCategoryViewModel } from "./MetadataCategoryViewModel";
import { PaginatedTableViewModel } from "../../../ViewModels/PaginatedTableViewModel";

class MetadataCategoryIndexViewModel extends PaginatedTableViewModel {
    
    metadataCategories: KnockoutObservableArray<MetadataCategoryViewModel> = ko.observableArray([]);

    newMetadataCategoryName = ko.observable("");

    deleteModalTitle = ko.observable("");
    reviveModalTitle = ko.observable("");
    searchRequest: JQueryXHR;
    deletingMetadataCategory: MetadataCategoryViewModel;
    revivingMetadataCategory: MetadataCategoryViewModel;
    selectedMetadataCategories: KnockoutObservableArray<MetadataCategoryViewModel> = ko.observableArray([]);

    // ReSharper disable SuspiciousThisUsage
    isSelectedAll = ko.pureComputed({
        read: function () {
            return this.selectedMetadataCategories().length > 0
                && this.selectedMetadataCategories().length === this.metadataCategories().length;
        },
        write: function (value) {
            this.selectedMetadataCategories(value ? this.metadataCategories().slice(0) : []);
        },
        owner: this
    });
    // ReSharper enable SuspiciousThisUsage
    includeDependencies = ko.observable(false);

    constructor(baseUrl, isReadOnly: string) {
        super(baseUrl, "/Metadata/MetadataCategories", isReadOnly);

        if (!this.orderProperty()) {
            this.orderProperty("name");
            this.orderAscending(true);
        }
        this.loadPageData();
        $("#SearchBox").focus();
    }

    loadPageData() {
        if (this.searchRequest && this.searchRequest.readyState < 4) {
            this.searchRequest.abort();
        }
        this.showProgressSpinner();
        this.searchRequest = this.getJson(this.getPageUrl(this.apiEndpointUrl()),
            this.loadPageDataSucceeded,
            this);
    }

    loadPageDataSucceeded(result) {
        this.selectedMetadataCategories.removeAll();
        this.metadataCategories.removeAll();
        if (result.success === true) {
            const page = result.model;
            for (let metadataCategory of page.data) {
                this.metadataCategories.push(new MetadataCategoryViewModel(metadataCategory));
            }
            this.setPagination(page);
        }
        this.hideProgressSpinner();
    }

    createMetadataCategory() {
        this.postJson(`${this.apiEndpointUrl()}/Create`,
            ko.toJSON({ name: this.newMetadataCategoryName() }),
            this.createMetadataCategorySucceeded,
            this);
    }

    createMetadataCategorySucceeded(data) {
        this.closeModal();
        if (data.success === true) {
            this.showAlert({ type: "success", message: data.message });
            this.loadPageData();
        } else {
            this.showAlert({ type: "error", message: data.message });
        }
    }

    downloadDataPackage() {
        if (this.selectedMetadataCategories().length === 0) {
            this.closeModal();
            this.showAlert({ type: "error", message: "No metadata category selected." });
            return;
        }
        this.showProgressBar();
        this.postJson(`${this.apiEndpointUrl()}/Export?includeDependencies=${this.includeDependencies()}`,
            ko.toJSON(this.selectedMetadataCategories), this.downloadDataPackageSucceeded, this);
    }

    downloadDataPackageSucceeded(result) {
        if (result.success) {
            window.location.href = `${this.baseUrl()}/DataExchange/Export/Package/${result.model.packageId}`;

        } else {
            this.hideProgressBar();
            this.showAlert({ type: "error", message: result.message });
        }
    }

    deleteMetadataCategory() {
        if (!this.deletingMetadataCategory) return;
        this.deleteJson(`${this.apiEndpointUrl()}/Delete/${this.deletingMetadataCategory.id()}`,
            ko.toJSON(this.deletingMetadataCategory),
            this.deleteMetadataCategorySucceeded,
            this);
    }

    deleteMetadataCategorySucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var deleteId = this.deletingMetadataCategory.id();
            this.metadataCategories.remove(item => item.id() === deleteId);
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    reviveMetadataCategory() {
        if (!this.revivingMetadataCategory) return;
        this.postJson(`${this.apiEndpointUrl()}/Revive/${this.revivingMetadataCategory.id()}`,
            null, //ko.toJSON(this.revivingMetadataCategory),
            this.reviveMetadataCategorySucceeded,
            this);
    }

    reviveMetadataCategorySucceeded(result) {
        this.closeModal();
        if (result.success === true) {
            this.showAlert({ type: "success", message: result.message });
            var reviveId = this.revivingMetadataCategory.id();
            this.metadataCategories.remove(item => item.id() === reviveId);
        } else {
            this.showAlert({ type: "error", message: result.message });
        }
    }

    showDeleteModal(viewModel: MetadataCategoryViewModel) {
        this.deletingMetadataCategory = viewModel;
        this.deleteModalTitle(viewModel.name());
        $("#DeleteModal").foundation("reveal", "open");
    }

    showReviveModal(viewModel: MetadataCategoryViewModel) {
        this.revivingMetadataCategory = viewModel;
        this.reviveModalTitle(viewModel.name());
        $("#ReviveModal").foundation("reveal", "open");
    }
}
export = MetadataCategoryIndexViewModel
