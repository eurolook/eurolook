﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.Data.TermStore;
using Eurolook.ManagementConsole.Areas.Metadata.TermStore;
using Eurolook.ManagementConsole.Controllers;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Areas.Metadata.Controllers
{
    [SuppressMessage("csharpsquid", "S5693:Make sure the content length limit is safe here.", Justification = "API endpoint is internal only.")]
    public class TermStoreController : EurolookController
    {
        private readonly ITermStoreXmlValidator _termStoreXmlValidator;
        private readonly ITermStoreDatabase _termStoreDatabase;

        public TermStoreController(
            ISettingsService settingsService,
            ITermStoreXmlValidator termStoreXmlValidator,
            ITermStoreDatabase termStoreDatabase)
            : base(settingsService)
        {
            _termStoreXmlValidator = termStoreXmlValidator;
            _termStoreDatabase = termStoreDatabase;
        }

        public async Task<ActionResult> Index()
        {
            if (Request.IsAjaxRequest())
            {
                var timeStamp = await _termStoreDatabase.GetTermStoreModificationTimeUtc();
                if (timeStamp == default)
                {
                    return JsonResult("No termstore data has been imported yet.");
                }

                return JsonResult(timeStamp.ToLocalTime().ToString("dd MMMM yyyy, H:mm:ss", new CultureInfo("en-us")));
            }

            return View();
        }

        // GET: Export/
        public async Task<ActionResult> Export()
        {
            try
            {
                var termStoreImport = await _termStoreDatabase.GetTermStoreImport();
                var timeStamp = await _termStoreDatabase.GetTermStoreModificationTimeUtc();
                using (var ms = new MemoryStream(termStoreImport.TermStoreXmlFile))
                {
                    return File(
                        ms.ToArray(),
                        "text/xml",
                        $"TermStoreExport_{timeStamp.ToLocalTime():yyyy-MM-dd-HHmm}.xml");
                }
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The XML file could not be exported. {ex.GetInnerException().Message}");
            }
        }

        [HttpPost]
        [DisableRequestSizeLimit]
        [RequestFormLimits(MultipartBodyLengthLimit = int.MaxValue, ValueLengthLimit = int.MaxValue)]
        public async Task<ActionResult> Import([FromForm] IFormFile file)
        {
            try
            {
                await ImportTermStoreFile(file);
                return JsonResult(null);
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The XML file could not be imported. {ex.GetInnerException().Message}");
            }
        }

        private async Task ImportTermStoreFile(IFormFile file)
        {
            using (var stream = file.OpenReadStream())
            {
                await _termStoreXmlValidator.Validate(stream);
                await _termStoreDatabase.UpdateTermStore(stream);
            }
        }
    }
}
