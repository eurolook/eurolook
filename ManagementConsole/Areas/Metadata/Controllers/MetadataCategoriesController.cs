using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Eurolook.Common.Log;
using Eurolook.Data.Models.Metadata;
using Eurolook.ManagementConsole.Areas.DataExchange;
using Eurolook.ManagementConsole.Areas.Metadata.Database;
using Eurolook.ManagementConsole.Areas.Metadata.Models;
using Eurolook.ManagementConsole.Controllers;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Areas.Metadata.Controllers
{
    public class MetadataCategoriesController : EurolookController
    {
        private readonly IMetadataCategoryDatabase _metadataCategoryDatabase;
        private readonly ExportService _exportService;
        private readonly IMapper _mapper;

        public MetadataCategoriesController(
            ISettingsService settingsService,
            IMetadataCategoryDatabase metadataCategoryDatabase,
            ExportService exportService,
            IMapper mapper)
            : base(settingsService)
        {
            _metadataCategoryDatabase = metadataCategoryDatabase;
            _exportService = exportService;
            _mapper = mapper;
        }

        // GET: MetadataCategories
        public async Task<ActionResult> Index(
            string q = null,
            uint p = 1,
            uint s = 20,
            string o = "Name",
            bool asc = true,
            bool deleted = false)
        {
            if (!Request.IsAjaxRequest())
            {
                return View();
            }

            try
            {
                var page = new DataPage<MetadataCategory>(p, s, asc, o);
                page.Data.AddRange(await _metadataCategoryDatabase.GetMetadataCategoriesAsync(page, q, deleted));
                page.TotalCount = await _metadataCategoryDatabase.GetMetadataCategoryCountAsync(q, deleted);
                return JsonResult(page);
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"Metadata categories could not be loaded. {ex.GetInnerException().Message}");
            }
        }

        // POST: /MetadataCategories/Create
        [HttpPost]
        public async Task<ActionResult> Create([FromBody] MetadataCategoryEditDataModel model)
        {
            try
            {
                var metadataCategory = await _metadataCategoryDatabase.CreateMetadataCategoryAsync(model.Name);
                return JsonResult("The metadata category has been created.", metadataCategory);
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The metadata category could not be created. {ex.GetInnerException().Message}");
            }
        }

        // GET: /MetadataCategories/Edit/00000000-0000-0000-0000-000000000000
        public async Task<ActionResult> Edit(Guid id)
        {
            if (!Request.IsAjaxRequest())
            {
                return View(id);
            }

            var metadataCategory = await _metadataCategoryDatabase.GetMetadataCategoryAsync(id);
            if (metadataCategory != null)
            {
                var metadataCategoryEditDataModel = _mapper.Map<MetadataCategoryEditDataModel>(metadataCategory);
                return JsonResult(metadataCategoryEditDataModel);
            }

            return NotFound();
        }

        // PUT: /MetadataCategories/Edit/00000000-0000-0000-0000-000000000000
        [HttpPut]
        public async Task<ActionResult> Edit(Guid id, [FromBody] MetadataCategoryEditDataModel metadataCategoryEditDataModel)
        {
            try
            {
                var metadataCategory = _mapper.Map<MetadataCategory>(metadataCategoryEditDataModel);
                await _metadataCategoryDatabase.UpdateMetadataCategoryAsync(id, metadataCategory);
                return JsonResult("The metadata category has been updated successfully!");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The metadata category could not be updated. {ex.GetInnerException().Message}");
            }
        }

        // DELETE: /MetadataCategories/Delete/00000000-0000-0000-0000-000000000000
        [HttpDelete]
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                await _metadataCategoryDatabase.DeleteMetadataCategoryAsync(id);
                return JsonResult("The metadata category has been deleted.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The metadata category could not be deleted. {ex.GetInnerException().Message}");
            }
        }

        // POST: /MetadataCategories/Revive/00000000-0000-0000-0000-000000000000
        [HttpPost]
        public async Task<JsonResult> Revive(Guid id)
        {
            try
            {
                await _metadataCategoryDatabase.ReviveMetadataCategoryAsync(id);
                return JsonResult("The metadata category has been revived.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The metadata category could not be revived. {ex.GetInnerException().Message}");
            }
        }

        // POST: /MetadataCategories/Export
        [HttpPost]
        public async Task<ActionResult> Export([FromBody] List<MetadataCategory> metadataCategories)
        {
            try
            {
                var exportPackage = await _exportService.ExportMetadataCategories(metadataCategories);
                return JsonResult(new { PackageId = exportPackage.Id });
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The data package could not be created. {ex.GetInnerException().Message}");
            }
        }
    }
}
