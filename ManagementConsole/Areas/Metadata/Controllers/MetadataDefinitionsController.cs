﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Eurolook.Common.Log;
using Eurolook.Data.Models.Metadata;
using Eurolook.Data.TermStore;
using Eurolook.ManagementConsole.Areas.DataExchange;
using Eurolook.ManagementConsole.Areas.Metadata.Database;
using Eurolook.ManagementConsole.Areas.Metadata.Models;
using Eurolook.ManagementConsole.Controllers;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Mapping;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Areas.Metadata.Controllers
{
    public class MetadataDefinitionsController : EurolookController
    {
        private readonly IAdminDatabase _adminDatabase;
        private readonly IMetadataDatabase _metadataDatabase;
        private readonly IMetadataCategoryDatabase _metadataCategoryDatabase;
        private readonly ITermStoreCache _termStoreCache;
        private readonly IMetadataDefinitionMapper _metadataDefinitionMapper;
        private readonly ExportService _exportService;
        private readonly IMapper _mapper;

        public MetadataDefinitionsController(
            ISettingsService settingsService,
            IAdminDatabase adminDatabase,
            IMetadataDatabase metadataDatabase,
            IMetadataCategoryDatabase metadataCategoryDatabase,
            ITermStoreCache termStoreCache,
            IMetadataDefinitionMapper metadataDefinitionMapper,
            ExportService exportService,
            IMapper mapper)
            : base(settingsService)
        {
            _adminDatabase = adminDatabase;
            _metadataDatabase = metadataDatabase;
            _metadataCategoryDatabase = metadataCategoryDatabase;
            _termStoreCache = termStoreCache;
            _metadataDefinitionMapper = metadataDefinitionMapper;
            _exportService = exportService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult> Index(
            string q = null,
            uint p = 1,
            uint s = 50,
            string o = "position",
            bool asc = true,
            bool deleted = false)
        {
            if (!Request.IsAjaxRequest())
            {
                return View();
            }

            try
            {
                return await GetDefinitionsPage(q, p, o, s, asc, deleted);
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"Metadata definitions could not be loaded. {ex.GetInnerException().Message}");
            }
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] MetadataDefinitionViewModel viewModel)
        {
            try
            {
                var model = _mapper.Map<MetadataDefinition>(viewModel);
                model = await _metadataDatabase.CreateMetadataDefinitionAsync(model);
                return JsonResult("Successfully created metadata definition", model);
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"Metadata definition could not be created. {ex.GetInnerException().Message}");
            }
        }

        [HttpGet]
        public async Task<ActionResult> Edit(Guid id)
        {
            if (!Request.IsAjaxRequest())
            {
                return View(id);
            }

            var metadataDefinition = await _metadataDatabase.GetMetadataDefinitionAsync(id);
            if (metadataDefinition == null)
            {
                return NotFound();
            }

            var metadataCategories = await _metadataCategoryDatabase.GetMetadataCategoriesAsync();
            var metadataDefinitionEditViewModel = new MetadataDefinitionEditViewModel(metadataCategories)
            {
                AllUserGroupDataModels = _adminDatabase
                                         .GetAllUserGroups()?.Select(
                                             ug => new UserGroupDataModel(ug)
                                             {
                                                 IsSelected = metadataDefinition.IsEditableByUserGroup(ug.Id),
                                             })
                                         .OrderBy(ug => ug.UiPositionIndex)
                                         .ToList(),
            };

            _mapper.Map(metadataDefinition, metadataDefinitionEditViewModel);
            return JsonResult(metadataDefinitionEditViewModel);
        }

        [HttpPut]
        public async Task<ActionResult> Edit(Guid id, [FromBody] MetadataDefinitionEditViewModel viewModel)
        {
            try
            {
                var model = _mapper.Map<MetadataDefinition>(viewModel);
                if (viewModel.AllUserGroupDataModels != null)
                {
                    var idsOfSelectedUserGroups = viewModel.AllUserGroupDataModels.Where(ug => ug.IsSelected).Select(ug => ug.Id);
                    model.EditableByUserGroups = string.Join(";", idsOfSelectedUserGroups);
                }

                await _metadataDatabase.EditMetadataDefinitionAsync(model);
                return JsonResult("The metadata category has been updated successfully!");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"Metadata definition could not be edited. {ex.GetInnerException().Message}");
            }
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(Guid id)
        {
            try
            {
                await _metadataDatabase.DeleteMetadataDefinitionAsync(id);
                return JsonResult("The metadata definition has been deleted successfully!");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"Metadata definition could not be deleted. {ex.GetInnerException().Message}");
            }
        }

        // POST: /MetadataDefinitions/Revive/00000000-0000-0000-0000-000000000000
        [HttpPost]
        public async Task<JsonResult> Revive(Guid id)
        {
            try
            {
                await _metadataDatabase.ReviveMetadataDefinitionAsync(id);
                return JsonResult("The metadata definition has been revived.");
            }
            catch (Exception ex)
            {
                return HandleError(
                    ex,
                    $"The metadata definition could not be revived. {ex.GetInnerException().Message}");
            }
        }

        // POST: /MetadataCategories/Export
        [HttpPost]
        public async Task<ActionResult> Export(
            [FromBody] List<MetadataDefinition> metadataDefinitions,
            [FromQuery] bool includeMetadataCategories)
        {
            try
            {
                var exportPackage = await _exportService.ExportMetadataDefinitions(
                    metadataDefinitions,
                    includeMetadataCategories);
                return JsonResult(new { PackageId = exportPackage.Id });
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The data package could not be created. {ex.GetInnerException().Message}");
            }
        }

        [HttpGet]
        public async Task<ActionResult> DefinitionsList()
        {
            try
            {
                var metadataDefinitions = await _metadataDatabase.GetAllMetadataDefinitionsAsync("");
                var termStore = await _termStoreCache.GetTermStoreAsync();
                var metadataDefinitionSelectionViewModels = metadataDefinitions
                                                            .OrderBy(md => md.Name)
                                                            .Select(
                                                                md => _metadataDefinitionMapper
                                                                    .ToMetadataDefinitionSelectionViewModel(
                                                                        termStore,
                                                                        md));

                return JsonResult(metadataDefinitionSelectionViewModels);
            }
            catch (Exception ex)
            {
                return HandleError(
                    ex,
                    $"Metadata definitions for selection could not be loaded. {ex.GetInnerException().Message}");
            }
        }

        [HttpGet]
        public async Task<ActionResult> TermSets()
        {
            try
            {
                var termStore = await _termStoreCache.GetTermStoreAsync();
                var model = termStore?.TermSets.Select(t => _mapper.Map<TermSetSelectionViewModel>(t));
                return JsonResult(model ?? new List<TermSetSelectionViewModel>());
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"TermSets could not be loaded. {ex.GetInnerException().Message}");
            }
        }

        private async Task<ActionResult> GetDefinitionsPage(
            string query,
            uint pageNumber,
            string orderProperty,
            uint size,
            bool sortAscending,
            bool deleted)
        {
            var page = new DataPage<MetadataDefinition>(pageNumber, size, sortAscending, orderProperty);
            page.Data.AddRange(await _metadataDatabase.GetAllMetadataDefinitionsAsync(page, query, deleted));
            page.TotalCount = await _metadataDatabase.GetMetadataDefinitionCountAsync(query, deleted);

            return JsonResult(page);
        }
    }
}
