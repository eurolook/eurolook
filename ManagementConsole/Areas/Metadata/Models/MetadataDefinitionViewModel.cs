using System;
using System.Diagnostics.CodeAnalysis;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.ManagementConsole.Areas.Metadata.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Member is accessed from JavaScript.")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is accessed from JavaScript.")]
    public class MetadataDefinitionViewModel
    {
        public Guid? Id { get; set; }

        public string Name { get; set; }

        public string DisplayName { get; set; }

        public MetadataType MetadataType { get; set; }

        public MetadataCategory MetadataCategory { get; set; }
    }
}
