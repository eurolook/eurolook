using System;
using System.Collections.Generic;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.ManagementConsole.Areas.Metadata.Models
{
    public class MetadataCategoryEditDataModel
    {
        public MetadataCategoryEditDataModel()
        {
            // default constructor is used by ASP.NET MVC for creating the model from a HTTP request
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string DisplayName { get; set; }

        public int UiPositionIndex { get; set; }

        public HashSet<MetadataDefinition> MetadataDefinitions { get; set; }
    }
}
