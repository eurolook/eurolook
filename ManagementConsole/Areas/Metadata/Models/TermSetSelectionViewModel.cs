using System;

namespace Eurolook.ManagementConsole.Areas.Metadata.Models
{
    public class TermSetSelectionViewModel
    {
        public Guid SharePointId { get; set; }

        public string Name { get; set; }
    }
}
