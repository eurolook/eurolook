﻿using System;
using System.Collections.Generic;
using Eurolook.Data.Models.Metadata;
using Eurolook.ManagementConsole.Models;

namespace Eurolook.ManagementConsole.Areas.Metadata.Models
{
    public class MetadataDefinitionEditViewModel
    {
        public MetadataDefinitionEditViewModel()
        {
        }

        public MetadataDefinitionEditViewModel(IEnumerable<MetadataCategory> categories)
        {
            MetadataCategories.AddRange(categories);
        }

        public string Id { get; set; }

        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string Description { get; set; }

        public int? MinLength { get; set; }

        public int? MaxLength { get; set; }

        public List<MetadataCategory> MetadataCategories { get; } = new List<MetadataCategory>();

        public Guid MetadataCategoryId { get; set; }

        public MetadataCategory MetadataCategory { get; set; }

        public int Position { get; set; }

        public bool IsReadOnly { get; set; }

        public string EditableByUserGroups { get; set; }

        public List<UserGroupDataModel> AllUserGroupDataModels { get; set; }

        public bool IsHidden { get; set; }

        public bool IsMandatory { get; set; }

        public MetadataType MetadataType { get; set; }

        public Guid? TermSetId { get; set; }
    }
}
