using System.IO;
using System.Threading.Tasks;

namespace Eurolook.ManagementConsole.Areas.Metadata.TermStore
{
    public interface ITermStoreXmlValidator
    {
        Task Validate(Stream stream);
    }
}
