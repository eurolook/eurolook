using System;
using System.Runtime.Serialization;

namespace Eurolook.ManagementConsole.Areas.Metadata.TermStore
{
    [Serializable]
    public class InputFileTooLongException : Exception
    {
        public InputFileTooLongException()
        {
        }

        public InputFileTooLongException(string message)
            : base(message)
        {
        }

        public InputFileTooLongException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected InputFileTooLongException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}
