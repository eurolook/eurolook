using System.IO;
using System.Threading.Tasks;
using System.Xml.Linq;
using Eurolook.Data.TermStore;

namespace Eurolook.ManagementConsole.Areas.Metadata.TermStore
{
    public class TermStoreXmlValidator : ITermStoreXmlValidator
    {
        private readonly ITermStoreXmlParser _termStoreXmlParser;

        public TermStoreXmlValidator(ITermStoreXmlParser termStoreXmlParser)
        {
            _termStoreXmlParser = termStoreXmlParser;
        }

        public async Task Validate(Stream stream)
        {
            int twentyMegabytes = 1024 * 1024 * 20;
            if (stream.Length > twentyMegabytes)
            {
                throw new InputFileTooLongException();
            }

            var document = await Task.Factory.StartNew(() => XDocument.Load(stream));
            await Task.Factory.StartNew(() => _termStoreXmlParser.Parse(document));
            stream.Position = 0;
        }
    }
}
