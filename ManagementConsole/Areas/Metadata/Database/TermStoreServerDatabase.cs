using Eurolook.Data;
using Eurolook.Data.TermStore;
using Microsoft.Extensions.Configuration;

namespace Eurolook.ManagementConsole.Areas.Metadata.Database
{
    public class TermStoreServerDatabase : AbstractTermStoreDatabase<EurolookServerContext>
    {
        private readonly string _connectionString;

        public TermStoreServerDatabase(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("ServerDatabaseConnection");
        }

        protected override EurolookServerContext GetContext()
        {
            return new EurolookServerContext(_connectionString);
        }
    }
}
