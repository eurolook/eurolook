﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Eurolook.Common.Extensions;
using Eurolook.Data;
using Eurolook.Data.Extensions;
using Eurolook.Data.Models.Metadata;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Extensions;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.ViewModels.DocumentModels.MetadataDefinitions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Eurolook.ManagementConsole.Areas.Metadata.Database
{
    public class MetadataDatabase : EurolookManagementConsoleDatabase, IMetadataDatabase
    {
        private readonly IMapper _mapper;

        public MetadataDatabase(IConfiguration configuration, IMapper mapper)
            : base(configuration)
        {
            _mapper = mapper;
        }

        public async Task<MetadataDefinition> GetMetadataDefinitionAsync(Guid id)
        {
            using (var context = GetContext())
            {
                return await context.MetadataDefinitions.FirstOrDefaultAsync(md => md.Id == id);
            }
        }

        public async Task<List<MetadataDefinition>> GetAllMetadataDefinitionsAsync(string searchTerm, bool deleted = false)
        {
            using (var context = GetContext())
            {
                return await context.MetadataDefinitions
                                    .Where(md => (searchTerm == null || md.Name.Contains(searchTerm)) && md.Deleted == deleted)
                                    .AsNoTracking()
                                    .ToListAsync();
            }
        }

        public async Task<List<MetadataDefinition>> GetAllMetadataDefinitionsAsync(
            DataPage<MetadataDefinition> page,
            string searchTerm,
            bool deleted)
        {
            using (var context = GetContext())
            {
                var query =
                    context.MetadataDefinitions.Where(
                        md => (searchTerm == null || md.Name.Contains(searchTerm)) && md.Deleted == deleted);

                return await query.OrderByAndPaginate(page)
                                  .AsNoTracking()
                                  .ToListAsync();
            }
        }

        public async Task<uint> GetMetadataDefinitionCountAsync(string searchTerm, bool deleted)
        {
            using (var context = GetContext())
            {
                return (uint)await context.MetadataDefinitions.CountAsync(md => (searchTerm == null || md.Name.Contains(searchTerm)) && md.Deleted == deleted);
            }
        }

        public async Task<MetadataDefinition> CreateMetadataDefinitionAsync(MetadataDefinition model)
        {
            using (var context = GetContext())
            {
                return await context.MetadataDefinitions.CreateOrUpdateAsync(
                    model,
                    context,
                    md => md.Deleted && md.Name == model.Name);
            }
        }

        public async Task<MetadataDefinition> EditMetadataDefinitionAsync(MetadataDefinition model)
        {
            using (var context = GetContext())
            {
                // rename existing deleted definitions with the same name to avoid that the unique name constraint gets violated
                var existingDefinition = await context.MetadataDefinitions.FirstOrDefaultAsync(md => md.Deleted && md.Name == model.Name);
                if (existingDefinition != null)
                {
                    existingDefinition.Name = existingDefinition.Name + "_" + DateTime.UtcNow.ToString("s");
                    existingDefinition.UpdateServerModificationTime();
                    await context.SaveChangesAsync();
                }

                model.UpdateServerModificationTime();
                context.Entry(model).State = EntityState.Modified;
                await context.SaveChangesAsync();
                return model;
            }
        }

        public async Task DeleteMetadataDefinitionAsync(Guid id)
        {
            using (var context = GetContext())
            {
                // Check if a document structure exists
                var documentModelMetadataDefinitions =
                    context.DocumentModelMetadataDefinitions.Where(x => x.MetadataDefinitionId == id && !x.Deleted)
                           .Include(x => x.DocumentModel)
                           .ToList();
                if (documentModelMetadataDefinitions.Count == 1)
                {
                    throw new ConstraintException(
                        $"The document model '{documentModelMetadataDefinitions.First().DocumentModel.Name}' " +
                        "is still using this metadata definition.");
                }

                if (documentModelMetadataDefinitions.Count > 1)
                {
                    throw new ConstraintException(
                        $"There are still {documentModelMetadataDefinitions.Count} document models using this metadata definition: " +
                        $"{string.Join(", ", documentModelMetadataDefinitions.Select(x => x.DocumentModel.Name).ToArray())}.");
                }

                var metadataDefinition = await context.MetadataDefinitions.FirstOrDefaultAsync(md => md.Id == id);
                metadataDefinition?.SetDeletedFlag();
                await context.SaveChangesAsync();
            }
        }

        public async Task<MetadataDefinition> ReviveMetadataDefinitionAsync(Guid id)
        {
            using (var context = GetContext())
            {
                var metadataDefinition = await context.MetadataDefinitions.Include(md => md.MetadataCategory).FirstAsync(c => c.Id == id);
                metadataDefinition.SetDeletedFlag(false);
                metadataDefinition.MetadataCategory?.SetDeletedFlag(false);

                await context.SaveChangesAsync();
                return metadataDefinition;
            }
        }

        public async Task EditDocumentModelMetadataDefinitionsAsync(AssociatedMetadataDefinitionsViewModel viewModel)
        {
            using (var context = GetContext())
            {
                var existingAssociations = GetExistingDocumentModelMetadataDefinitions(context, viewModel.DocumentModelId);

                SetDeletedFlagForRemovedAssociations(
                    existingAssociations,
                    viewModel.AssociatedMetadataDefinitions.Select(amd => amd.MetadataDefinitionId));
                UpsertMetadataDefinitions(viewModel, context);

                await context.SaveChangesAsync();
            }
        }

        public async Task CopyDocumentModelMetadataDefinitionsAsync(Guid sourceDocumentModelId, Guid targetDocumentModelId)
        {
            using (var context = GetContext())
            {
                var existingAssociationsOnTarget = GetExistingDocumentModelMetadataDefinitions(context, targetDocumentModelId);
                var existingAssociationsOnSource = GetExistingDocumentModelMetadataDefinitions(context, sourceDocumentModelId);

                SetDeletedFlagForRemovedAssociations(
                    existingAssociationsOnTarget,
                    existingAssociationsOnSource.Select(dmmd => dmmd.MetadataDefinitionId));

                var viewModel = new AssociatedMetadataDefinitionsViewModel
                {
                    AssociatedMetadataDefinitions =
                        existingAssociationsOnSource.Select(a => _mapper.Map<AssociatedMetadataDefinitionViewModel>(a)),
                    DocumentModelId = targetDocumentModelId,
                };

                UpsertMetadataDefinitions(viewModel, context, true);

                await context.SaveChangesAsync();
            }
        }

        private List<DocumentModelMetadataDefinition> GetExistingDocumentModelMetadataDefinitions(
            EurolookServerContext context,
            Guid documentModelId)
        {
            return context.DocumentModelMetadataDefinitions
                          .Where(md => md.DocumentModelId == documentModelId && !md.Deleted)
                          .ToList();
        }

        private void SetDeletedFlagForRemovedAssociations(
            List<DocumentModelMetadataDefinition> existingAssociations,
            IEnumerable<Guid> newMetadataDefinitionIds)
        {
            existingAssociations
                .Where(md => newMetadataDefinitionIds.All(id => id != md.MetadataDefinitionId))
                .ForEach(md => md.SetDeletedFlag());
        }

        private void UpsertMetadataDefinitions(
            AssociatedMetadataDefinitionsViewModel viewModel,
            EurolookServerContext context,
            bool keepValuesOfExistingAssociation = false)
        {
            var existingAssociations = context.DocumentModelMetadataDefinitions
                                              .Where(md => md.DocumentModelId == viewModel.DocumentModelId)
                                              .ToList();

            foreach (var associatedMetadataDefinition in viewModel.AssociatedMetadataDefinitions)
            {
                var existingAssociation =
                    existingAssociations.FirstOrDefault(md => md.MetadataDefinitionId == associatedMetadataDefinition.MetadataDefinitionId);

                if (existingAssociation == null)
                {
                    var documentModelMetadataDefinition = _mapper.Map<DocumentModelMetadataDefinition>(associatedMetadataDefinition);
                    documentModelMetadataDefinition.Init();
                    documentModelMetadataDefinition.DocumentModelId = viewModel.DocumentModelId;
                    context.DocumentModelMetadataDefinitions.Add(documentModelMetadataDefinition);

                    if (associatedMetadataDefinition?.AllUserGroupDataModels == null)
                    {
                        continue;
                    }

                    var idsOfSelectedUserGroups = associatedMetadataDefinition.AllUserGroupDataModels.Where(ug => ug.IsSelected).Select(ug => ug.Id);
                    documentModelMetadataDefinition.EditableByUserGroups = string.Join(";", idsOfSelectedUserGroups);
                }
                else
                {
                    existingAssociation.SetDeletedFlag(false);

                    if (keepValuesOfExistingAssociation)
                    {
                        return;
                    }

                    existingAssociation.DefaultValue = associatedMetadataDefinition.DefaultValue;
                    existingAssociation.CalculationCommandClassName = associatedMetadataDefinition.CalculationCommandClassName;
                    existingAssociation.IsReadOnlyOverride = associatedMetadataDefinition.IsReadOnlyOverride;

                    if (associatedMetadataDefinition?.AllUserGroupDataModels == null)
                    {
                        continue;
                    }

                    var idsOfSelectedUserGroups = associatedMetadataDefinition.AllUserGroupDataModels.Where(ug => ug.IsSelected).Select(ug => ug.Id);
                    existingAssociation.EditableByUserGroups = string.Join(";", idsOfSelectedUserGroups);
                }
            }
        }
    }
}
