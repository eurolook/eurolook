using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data.Models.Metadata;
using Eurolook.ManagementConsole.Models;

namespace Eurolook.ManagementConsole.Areas.Metadata.Database
{
    public interface IMetadataCategoryDatabase
    {
        Task<IEnumerable<MetadataCategory>> GetMetadataCategoriesAsync();

        Task<IEnumerable<MetadataCategory>> GetMetadataCategoriesAsync(
            DataPage<MetadataCategory> page,
            string searchTerm,
            bool deleted);

        Task<uint> GetMetadataCategoryCountAsync(string searchTerm, bool deleted);

        Task<MetadataCategory> CreateMetadataCategoryAsync(string name);

        Task<MetadataCategory> GetMetadataCategoryAsync(Guid id);

        Task<MetadataCategory> UpdateMetadataCategoryAsync(Guid id, MetadataCategory category);

        Task DeleteMetadataCategoryAsync(Guid id);

        Task<MetadataCategory> ReviveMetadataCategoryAsync(Guid id);
    }
}
