using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Data.Extensions;
using Eurolook.Data.Models.Metadata;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Eurolook.ManagementConsole.Areas.Metadata.Database
{
    public class MetadataCategoryDatabase : EurolookManagementConsoleDatabase, IMetadataCategoryDatabase
    {
        public MetadataCategoryDatabase(IConfiguration configuration)
            : base(configuration)
        {
        }

        public async Task<IEnumerable<MetadataCategory>> GetMetadataCategoriesAsync()
        {
            using (var context = GetContext())
            {
                return await context.MetadataCategories.OrderBy(c => c.Name).ToListAsync();
            }
        }

        public async Task<IEnumerable<MetadataCategory>> GetMetadataCategoriesAsync(
            DataPage<MetadataCategory> page,
            string searchTerm,
            bool deleted)
        {
            using (var context = GetContext())
            {
                var query =
                    context.MetadataCategories.Where(
                        x => (searchTerm == null || x.Name.Contains(searchTerm)) && x.Deleted == deleted);

                return await query.OrderByAndPaginate(page)
                                  .ToListAsync();
            }
        }

        public async Task<uint> GetMetadataCategoryCountAsync(string searchTerm, bool deleted)
        {
            using (var context = GetContext())
            {
                return (uint)await context.MetadataCategories.CountAsync(x => (searchTerm == null || x.Name.Contains(searchTerm)) && x.Deleted == deleted);
            }
        }

        public async Task<MetadataCategory> CreateMetadataCategoryAsync(string name)
        {
            using (var context = GetContext())
            {
                var metadataCategory = new MetadataCategory();
                metadataCategory.Init();
                metadataCategory.Name = name;
                var entityEntry = context.MetadataCategories.Add(metadataCategory);
                await context.SaveChangesAsync();
                return entityEntry.Entity;
            }
        }

        public async Task<MetadataCategory> GetMetadataCategoryAsync(Guid id)
        {
            using (var context = GetContext())
            {
                return await context.MetadataCategories.Include(c => c.MetadataDefinitions).FirstOrDefaultAsync(c => c.Id == id);
            }
        }

        public async Task<MetadataCategory> UpdateMetadataCategoryAsync(Guid id, MetadataCategory category)
        {
            using (var context = GetContext())
            {
                var metadataCategory = await context.MetadataCategories.AsNoTracking().FirstOrDefaultAsync(c => c.Id == id);
                if (metadataCategory == null)
                {
                    return null;
                }

                category.UpdateServerModificationTime();
                context.MetadataCategories.AddOrUpdate(category);
                await context.SaveChangesAsync();
                return category;
            }
        }

        public async Task DeleteMetadataCategoryAsync(Guid id)
        {
            using (var context = GetContext())
            {
                var metadataCategory = await context.MetadataCategories.FirstAsync(c => c.Id == id);

                var metadataDefinitions =
                    await context.MetadataDefinitions.Where(c => c.MetadataCategoryId == id && !c.Deleted).ToListAsync();
                if (metadataDefinitions.Any())
                {
                    string exceptionMessage = metadataDefinitions.Count == 1
                        ? $"The metadata definition '{metadataDefinitions.First().Name}' is still referencing the metadata category."
                        : $"There are still {metadataDefinitions.Count} metadata definitions referencing the metadata category: {string.Join(", ", metadataDefinitions.Select(x => x.Name).ToList())}.";

                    throw new ConstraintException(exceptionMessage);
                }

                metadataCategory.SetDeletedFlag();
                await context.SaveChangesAsync();
            }
        }

        public async Task<MetadataCategory> ReviveMetadataCategoryAsync(Guid id)
        {
            using (var context = GetContext())
            {
                var metadataCategory = await context.MetadataCategories.FirstAsync(c => c.Id == id);
                metadataCategory.SetDeletedFlag(false);
                await context.SaveChangesAsync();
                return metadataCategory;
            }
        }
    }
}
