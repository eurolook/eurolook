using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data.Models.Metadata;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.ViewModels.DocumentModels.MetadataDefinitions;

namespace Eurolook.ManagementConsole.Areas.Metadata.Database
{
    public interface IMetadataDatabase
    {
        Task<MetadataDefinition> GetMetadataDefinitionAsync(Guid id);

        Task<List<MetadataDefinition>> GetAllMetadataDefinitionsAsync(string searchTerm, bool deleted = false);

        Task<List<MetadataDefinition>> GetAllMetadataDefinitionsAsync(DataPage<MetadataDefinition> page, string searchTerm, bool deleted);

        Task<uint> GetMetadataDefinitionCountAsync(string searchTerm, bool deleted);

        Task<MetadataDefinition> CreateMetadataDefinitionAsync(MetadataDefinition model);

        Task<MetadataDefinition> EditMetadataDefinitionAsync(MetadataDefinition model);

        Task DeleteMetadataDefinitionAsync(Guid id);

        Task<MetadataDefinition> ReviveMetadataDefinitionAsync(Guid id);

        Task EditDocumentModelMetadataDefinitionsAsync(AssociatedMetadataDefinitionsViewModel viewModel);

        Task CopyDocumentModelMetadataDefinitionsAsync(Guid sourceDocumentModelId, Guid targetDocumentModelId);
    }
}
