import ko from 'knockout';

export default function DataPackageViewModel(model) {

    var self = this;
    self.id = ko.observable();
    self.description = ko.observable();
    self.created = ko.observable();
    self.report = ko.observable();

    self.setModel = function (newModel) {
        self.id(newModel.id);
        self.description(newModel.description);
        self.created(new Date(newModel.created).toLocaleString());
        self.report(newModel.report);
    }

    if (model) {
        self.setModel(model);
    }
}

window.initKnockout = function (baseUrl, model) {
    ko.baseUrl = baseUrl;
    ko.viewModel = new DataPackageViewModel(model);
    ko.applyBindings(ko.viewModel);
};
