import * as monaco from 'monaco-editor';
import ko from 'knockout';
import DataPackageViewModel from './DataPackageViewModel.js';

self.MonacoEnvironment = {
    getWorkerUrl: function(moduleId, label) {
        if (label === 'json') {
            return '../../dist/json.worker.bundle.js';
        }
        if (label === 'css' || label === 'scss' || label === 'less') {
            return '../../dist/css.worker.bundle.js';
        }
        if (label === 'html' || label === 'handlebars' || label === 'razor') {
            return '../../dist/html.worker.bundle.js';
        }
        if (label === 'typescript' || label === 'javascript') {
            return '../../dist/ts.worker.bundle.js';
        }
        return '../../dist/editor.worker.bundle.js';
    }
};

export default function ImportViewModel() {
    var self = this;

    self.importProgressTimer = null;
    self.fileFormData = null;
    self.isSelectAll = ko.observable(false);

    self.uploadId = ko.observable();
    self.uploadFileName = ko.observable();
    self.uploadDescription = ko.observable();
    self.uploadCreated = ko.observable();
    self.diff = new DataPackageViewModel();
    self.comparisonResult = ko.observableArray();

    self.isStep1 = ko.computed(function() {
        return self.uploadId() ? false : true;
    });
    self.isStep2 = ko.computed(function() {
        return self.uploadId() ? true : false;
    });

    self.isCentralData = ko.computed(function (file) {
        return self.uploadDescription() === "Export of all Eurolook data";
    });
    self.selectDataPackageFile = function() {
        document.querySelector("#dataPackageFile").click();
    };

    self.dataPackageFileSelected = function(file) {
        self.showActionProgressBar();
        self.fileFormData = new FormData();
        self.fileFormData.append("file", file);
        self.comparisonResult([]);
        fetch(ko.baseUrl + "/DataExchange/Import/UploadDataPackage/",
                {
                    method: "POST",
                    body: self.fileFormData
                    //contentType: false,
                    //processData: false
                })
            .then(response => response.json())
            .then(data => {
                self.hideActionProgressBar();
                if (data.success) {
                    self.uploadId(data.model.uploadId);
                    self.uploadFileName(data.model.uploadFileName);
                    self.uploadDescription(data.model.uploadDescription);
                    self.uploadCreated(new Date(data.model.uploadCreated).toLocaleString());
                    self.diff.setModel(data.model.diff);
                    self.calculateDifferences();
                } else {
                    showAlert({ type: "error", message: data.message });
                }
            }).catch(error => {
                self.hideActionProgressBar();
                showAlert({ type: "error", message: "HTTP error: " + error });
            });
    };

    self.confirmImportDataPackage = function() {

        if (self.isCentralData()) {
            $('#ImportCentralDataModal').foundation('reveal', 'open');
        } else {
            self.importDataPackage();
        }
    }
    self.importDataPackage = function() {
        if (!self.diff) {
            return;
        }
        self.showActionProgressBar();
        fetch(ko.baseUrl + "/DataExchange/Import/ImportDataPackage/" + self.diff.id(),
                {
                    method: "POST",
                })
            .then(response => response.json())
            .then(data => {
                self.hideActionProgressBar();
                if (data.success) {
                    showAlert({ type: "success", message: data.message });
                } else {
                    showAlert({ type: "error", message: data.message });
                }
            }).catch(error => {
                self.hideActionProgressBar();
                showAlert({ type: "error", message: "HTTP error: " + error });
            });
        self.closeModal();
    };

    self.calculateDifferences = function() {

        if (!self.diff || !self.diff.id()) {
            return;
        }

        self.showActionProgressBar("#comparisonActionProgressBar");

        fetch(ko.baseUrl + "/DataExchange/Import/CalculateDifferences/" + self.diff.id())
            .then(response => response.json())
            .then(data => {
                if (data.success) {
                    self.comparisonResult(data.model.comparison);
                    for (let changedEntity of data.model.comparison.changed) {
                        
                        let editorElementXml = document.querySelector("#editor_xml_" + changedEntity.id);
                        if (editorElementXml) {
                            let rawContentProperty =
                                changedEntity.propertyDifferences.find(x => x.propertyName === 'RawContent');

                            self.setupEditor(editorElementXml, rawContentProperty, 'xml');
                        }

                        let editorElementJson = document.querySelector("#editor_json_" + changedEntity.id);
                        if (editorElementJson) {
                            let configurationProperty =
                                changedEntity.propertyDifferences.find(x => x.propertyName === 'Configuration');
                            self.setupEditor(editorElementJson, configurationProperty, 'json');
                        }
                    }
                    self.hideActionProgressBar("#comparisonActionProgressBar");

                } else {
                    showAlert({ type: "error", message: data.message });
                    self.hideActionProgressBar("#comparisonActionProgressBar");
                }
            }).catch(error => {
                self.hideActionProgressBar("#comparisonActionProgressBar");
                showAlert({ type: "error", message: "HTTP error: " + error });
            });
    };

    self.setupEditor = function(editorElement, diffProperty, editorLanguage) {
        var editorOptions = {
            theme: "vs",
            //renderSideBySide: false,
            readOnly: true,
            enableSplitViewResizing: false,
            //overviewRulerLanes: 0,
            //fixedOverflowWidgets: true,
            scrollbar: {
                useShadows: false,
                verticalHasArrows: true,
                horizontalHasArrows: true,
                scrollBeyondLastLine: false,
                // set scrollbar hidden
                vertical: 'hidden',
                //horizontal: 'hidden',

                verticalScrollbarSize: 0,
                horizontalScrollbarSize: 17
            },
            minimap: {
                enabled: false
            }
        };

        let numberOfLines = Math.max(diffProperty.valueA.split("\n").length, diffProperty.valueB.split("\n").length);
        editorElement.style.height = numberOfLines * 20 + "px";

        let diffEditor = monaco.editor.createDiffEditor(editorElement, editorOptions);
        diffEditor.setModel({
            original: monaco.editor.createModel(diffProperty.valueA, editorLanguage),
            modified: monaco.editor.createModel(diffProperty.valueB, editorLanguage)
        });
    };
    self.showActionProgressBar = function(id = "#actionProgressBar") {
        window.clearTimeout(self.importProgressTimer);
        self.importProgressTimer =
            window.setTimeout(function() { document.querySelector(id).style.display = "block"; }, 300);
    };

    self.hideActionProgressBar = function(id = "#actionProgressBar") {
        window.clearTimeout(self.importProgressTimer);
        document.querySelector(id).style.display = "none";
    };

    this.closeModal = function () {
        $("#ImportCentralDataModal").foundation("reveal", "close");
    }
}



window.initKnockout = function (baseUrl) {
    ko.baseUrl = baseUrl;
    ko.viewModel = new ImportViewModel();
    ko.applyBindings(ko.viewModel);
};
