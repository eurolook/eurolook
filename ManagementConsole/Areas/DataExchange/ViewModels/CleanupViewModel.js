import ko from "knockout";

export default function CleanupViewModel() {
    var self = this;
    self.cleanupOrgChartsRequest = null;
    self.cleanupDevicesRequest = null;
    self.cleanupTextsRequest = null;

    self.devicesProgressTimer = null;
    self.orgChartsProgressTimer = null;
    self.textsProgressTimer = null;

    const date = new Date();
    date.setFullYear(date.getFullYear() - 1);
    self.beforeDate = ko.observable(date.toLocaleDateString());

    self.orgCharts = ko.observableArray(null);
    self.selectedOrgCharts = ko.observableArray(null);
    self.selectAllOrgCharts = ko.pureComputed({
        read: function () {
            return this.selectedOrgCharts().length === this.orgCharts().length;
        },
        write: function (value) {
            this.selectedOrgCharts(value ? this.orgCharts().slice(0) : []);
        },
        owner: this
    });

    self.devices = ko.observable(0);

    self.texts = ko.observableArray(null);
    self.selectedTexts = ko.observableArray(null);
    self.selectAllTexts = ko.pureComputed({
        read: function () {
            return this.selectedTexts().length === this.texts().length;
        },
        write: function (value) {
            this.selectedTexts(value ? this.texts().slice(0) : []);
        },
        owner: this
    });

    self.getOrgCharts = function () {
        self.showOrgChartsProgressBar();
        jQuery.ajax({
            type: "GET",
            url: ko.baseUrl + "/DataExchange/Cleanup/GetOrgaEntities",
            contentType: "application/json",
            dataType: "json",
            success: function(data) {
                self.orgCharts(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {

                showAlert({ type: "error", message: "HTTP error: " + errorThrown });
            },
            complete: function() {
                self.hideOrgChartsProgressBar();
            }
        });
    }

    self.getDevices = function () {
        self.showDevicesProgressBar();
        jQuery.ajax({
            type: "GET",
            url: ko.baseUrl + "/DataExchange/Cleanup/GetDevices",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                self.devices(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {

                showAlert({ type: "error", message: "HTTP error: " + errorThrown });
            },
            complete: function () {
                self.hideDevicesProgressBar();
            }
        });
    }

    self.getTexts = function () {
        self.showTextsProgressBar();
        jQuery.ajax({
            type: "GET",
            url: ko.baseUrl + "/DataExchange/Cleanup/GetTexts",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                self.texts(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {

                showAlert({ type: "error", message: "HTTP error: " + errorThrown });
            },
            complete: function () {
                self.hideTextsProgressBar();
            }
        });
    }

    self.cleanupOrgCharts = function () {
        if (self.cleanupOrgChartsRequest != null) {
            self.cleanupOrgChartsRequest.abort();
        }
        self.closeModal();
        self.showOrgChartsProgressBar();
        self.cleanupAllRequest = jQuery.ajax({
            type: "POST",
            url: ko.baseUrl + "/DataExchange/Cleanup/DeleteOrgaEntities",
            dataType: "json",
            data: { "entitiesToDelete": self.selectedOrgCharts().map(t => t.id) },
            success: function (data) {
                if (data.success) {
                    showAlert({ type: "success", message: data.message });
                } else {
                    showAlert({ type: "error", message: data.message });
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                showAlert({ type: "error", message: "HTTP error: " + errorThrown });
            },
            complete: function() {
                self.hideOrgChartsProgressBar();
                self.getOrgCharts();
            }
        });
    }

    self.cleanupDevices = function() {
        if (self.cleanupDevicesRequest != null) {
            self.cleanupDevicesRequest.abort();
        }
        self.closeModal();
        self.showDevicesProgressBar();
        self.cleanupDevicesRequest = jQuery.ajax({
            type: "DELETE",
            url: ko.baseUrl + "/DataExchange/Cleanup/DeleteDevices",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    showAlert({ type: "success", message: data.message });
                } else {
                    showAlert({ type: "error", message: data.message });
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                showAlert({ type: "error", message: "HTTP error: " + errorThrown });
            },
            complete: function () {
                self.hideDevicesProgressBar();
                self.getDevices();
            }
        });
        
    }

    self.cleanupTexts = function() {
        if (self.cleanupOrgChartsRequest != null) {
            self.cleanupOrgChartsRequest.abort();
        }
        self.closeModal();
        self.showTextsProgressBar();
        self.cleanupAllRequest = jQuery.ajax({
            type: "POST",
            url: ko.baseUrl + "/DataExchange/Cleanup/DeleteTexts",
            dataType: "json",
            data: { "textsToDelete": self.selectedTexts().map(t => t.id) },
            success: function (data) {
                if (data.success) {
                    showAlert({ type: "success", message: data.message });
                } else {
                    showAlert({ type: "error", message: data.message });
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                showAlert({ type: "error", message: "HTTP error: " + errorThrown });
            },
            complete: function () {
                self.hideTextsProgressBar();
                self.getTexts();
            }
        });
    }

    self.showDevicesProgressBar = function () {
        window.clearTimeout(self.devicesProgressTimer);
        self.devicesProgressTimer = window.setTimeout(function () { $("#devicesProgressBar").show(); }, 300);
    }

    self.hideDevicesProgressBar = function () {
        window.clearTimeout(self.devicesProgressTimer);
        $("#devicesProgressBar").hide();
    }

    self.showTextsProgressBar = function () {
        window.clearTimeout(self.textsProgressTimer);
        self.textsProgressTimer = window.setTimeout(function () { $("#textsProgressBar").show(); }, 300);
    }

    self.hideTextsProgressBar = function () {
        window.clearTimeout(self.textsProgressTimer);
        $("#textsProgressBar").hide();
    }

    self.showOrgChartsProgressBar = function () {
        window.clearTimeout(self.orgChartsProgressTimer);
        self.orgChartsProgressTimer = window.setTimeout(function () { $("#cleanupOrgChartsProgressBar").show(); }, 300);
    }

    self.hideOrgChartsProgressBar = function () {
        window.clearTimeout(self.orgChartsProgressTimer);
        $("#cleanupOrgChartsProgressBar").hide();
    }
    
    this.getOrgCharts();
    this.getDevices();
    this.getTexts();

    this.closeModal = function () {
        $(".reveal-modal").foundation("reveal", "close");
    }
}

window.initKnockout = function (baseUrl) {
    ko.baseUrl = baseUrl;
    ko.viewModel = new CleanupViewModel();
    ko.applyBindings(ko.viewModel);
};
