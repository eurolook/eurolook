import ko from 'knockout';

export default function ExportViewModel() {
    var self = this;
    self.documentModels = ko.observableArray(null);
    self.selectedDocumentModels = ko.observableArray(null);

    self.includeCharacterMappings = ko.observable(true);
    self.includeColorSchemes = ko.observable(true);
    self.includeStyleShortcuts = ko.observable(true);
    self.includeUserGroups = ko.observable(true);
    self.includeSystemConfigurations = ko.observable(false);
    self.includeAddresses = ko.observable(true);
    self.includePredefinedFunctions = ko.observable(true);
    self.includeSystemTextsAndResources = ko.observable(true);

    self.exportAll = function () {
        self.showExportProgressBar();
        jQuery.ajax({
            type: "POST",
            url: ko.baseUrl + "/DataExchange/Export/All",
            dataType: "json",
            data: {
                "documentModelIds": self.selectedDocumentModels().map(t => t.id),
                "includeCharacterMappings": self.includeCharacterMappings(),
                "includeColorSchemes": self.includeColorSchemes(),
                "includeStyleShortcuts": self.includeStyleShortcuts(),
                "includeUserGroups": self.includeUserGroups,
                "includeSystemConfigurations": self.includeSystemConfigurations,
                "includeAddresses": self.includeAddresses(),
                "includePredefinedFunctions": self.includePredefinedFunctions(),
                "includeSystemTextsAndResources": self.includeSystemTextsAndResources()
            },
            success: function (data) {
                if (data.success) {
                    window.location = ko.baseUrl + "/DataExchange/Export/Package/" + data.model.packageId;

                } else {
                    showAlert({ type: "error", message: data.message });
                    self.hideExportProgressBar();
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                self.hideExportProgressBar();
                showAlert({ type: "error", message: "HTTP error: " + errorThrown });
            }
        });
    }

    self.getDocumentModels = function () {
        jQuery.ajax({
            type: "GET",
            url: ko.baseUrl + "/DataExchange/Export/LoadAllDocumentModelCentralExportDataModel",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                self.documentModels(data);
                self.selectedDocumentModels(data);
            },
            error: function(jqXhr, textStatus, errorThrown) {
                showAlert({ type: "error", message: "HTTP error: " + errorThrown });
            }
        });
    }

    self.showExportProgressBar = function () {
        window.clearTimeout(self.exportProgressTimer);
        self.importProgressTimer = window.setTimeout(function () { $("#exportProgressBar").show(); }, 300);
    }

    self.hideExportProgressBar = function () {
        window.clearTimeout(self.exportProgressTimer);
        $("#exportProgressBar").hide();
    }

    this.getDocumentModels();
}

window.initKnockout = function(baseUrl) {
    ko.baseUrl = baseUrl;
    ko.viewModel = new ExportViewModel();
    ko.applyBindings(ko.viewModel);
};

