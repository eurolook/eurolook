import ko from 'knockout';
import DataPackageViewModel from './DataPackageViewModel.js';

export default function SnapshotsViewModel(model) {

    var self = this;
    self.snapshots = ko.observableArray();
    self.restoreProgressTimer = null;

    if (model) {
        model.forEach(function(item) {
            self.snapshots.push(new DataPackageViewModel(item));
        });
    }

    self.restoreSnapshot = function (snapshot) {
        self.showRestoreProgressBar();
        jQuery.ajax({
            type: "POST",
            url: ko.baseUrl + "/DataExchange/Snapshots/Restore/" + snapshot.id(),
            success: function (data) {
                if (data.success) {
                    showAlert({ type: "success", message: data.message });
                } else {
                    showAlert({ type: "error", message: data.message });
                }
                self.hideRestoreProgressBar();
            }
        });
    }

    self.showRestoreProgressBar = function () {
        window.clearTimeout(self.restoreProgressTimer);
        self.restoreProgressTimer = window.setTimeout(function () { $("#restoreProgressBar").show(); }, 300);
    }

    self.hideRestoreProgressBar = function () {
        window.clearTimeout(self.restoreProgressTimer);
        $("#restoreProgressBar").hide();
    }
}

window.initKnockout = function (baseUrl, model) {
    ko.baseUrl = baseUrl;
    ko.viewModel = new SnapshotsViewModel(model);
    ko.applyBindings(ko.viewModel);
};
