﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Areas.DataExchange.Database;

namespace Eurolook.ManagementConsole.Areas.DataExchange
{
    public class DataPackageReport
    {
        private readonly IImportExportDatabase _importExportDatabase;
        private readonly DataPackage _dataPackage;
        private readonly StringBuilder _report;

        public DataPackageReport(
            IImportExportDatabase importExportDatabase,
            DataPackage dataPackage)
        {
            _importExportDatabase = importExportDatabase;
            _dataPackage = dataPackage;
            _report = new StringBuilder();
        }

        public async Task<string> CreateReportAsync()
        {
            _report.Clear();

            var mergedBricks = new List<Brick>();
            mergedBricks.AddRange(_dataPackage.ContentBricks);
            mergedBricks.AddRange(_dataPackage.CommandBricks);
            mergedBricks.AddRange(_dataPackage.DynamicBricks);

            var bricks = (await _importExportDatabase.GetAllBricksAsync()).ToDictionary(b => b.Id, b => b);
            foreach (var dataPackageBrick in mergedBricks)
            {
                bricks[dataPackageBrick.Id] = dataPackageBrick;
            }

            var texts = (await _importExportDatabase.GetAllTextsAsync()).ToDictionary(t => t.Id, t => t);
            foreach (var dataPackageText in _dataPackage.Texts)
            {
                texts[dataPackageText.Id] = dataPackageText;
            }

            var languages = (await _importExportDatabase.LanguageRepository.GetAllLanguagesAsync()).ToDictionary(l => l.Id, l => l);
            foreach (var dataPackageLanguage in _dataPackage.Languages)
            {
                languages[dataPackageLanguage.Id] = dataPackageLanguage;
            }

            var resources = (await _importExportDatabase.GetAllResourcesAsync()).ToDictionary(r => r.Id, r => r);
            foreach (var dataPackageResource in _dataPackage.Resources)
            {
                resources[dataPackageResource.Id] = dataPackageResource;
            }

            AppendDataSet("Document Category", _dataPackage.DocumentCategories, d => d.Name);
            AppendDataSet("Document Models", _dataPackage.DocumentModels, dm => dm.Name);
            AppendDataSet("Document Model Language", _dataPackage.DocumentModelLanguages);
            AppendDataSet("Document Structure", _dataPackage.DocumentStructures);
            AppendDataSet("Document Model Metadata Definitions", _dataPackage.DocumentModelMetadataDefinitions);
            AppendDataSet("Document Model Author Roles", _dataPackage.DocumentModelAuthorRoles);

            AppendDataSet("Bricks", mergedBricks, e => e.Name);
            AppendDataSet("Brick Groups", _dataPackage.BrickGroups, e => e.Name);
            AppendDataSet("Brick Categories", _dataPackage.BrickCategories, e => e.Name);
            AppendDataSet("User Groups", _dataPackage.UserGroups, e => e.Name);
            AppendDataSet("System Configurations", _dataPackage.SystemConfigurations, e => e.Key);
            AppendDataSet("Brick Texts", _dataPackage.BrickTexts, e => GetBrickTextDescription(e, bricks, texts));
            AppendDataSet("Texts", _dataPackage.Texts, e => e.Alias);
            AppendDataSet("Translations", _dataPackage.Translations, e => GetTranslationDescription(e, texts, languages));

            AppendDataSet("Resources", _dataPackage.Resources, e => e.Alias);
            AppendDataSet("Localised Resources", _dataPackage.LocalisedResources, e => GetLocaliseResourceDescription(e, resources, languages));

            AppendDataSet("OrgaEntities", _dataPackage.OrgaEntities, e => e.Name);

            AppendDataSet("Languages", _dataPackage.Languages, e => e.Name);
            AppendDataSet("Color Schemes", _dataPackage.ColorSchemes, e => e.Name);
            AppendDataSet("Character Mappings", _dataPackage.CharacterMappings);
            AppendDataSet("Style Shortcuts", _dataPackage.StyleShortcuts);

            AppendDataSet("PersonNames", _dataPackage.PersonNames);
            AppendDataSet("Predefined Functions", _dataPackage.PredefinedFunctions, e => e.Name);
            AppendDataSet("System Texts", _dataPackage.SystemTexts, e => GetSystemTextDescription(e, texts));
            AppendDataSet("System Resources", _dataPackage.SystemResources, e => GetSystemResourceDescription(e, resources));

            AppendDataSet("Addresses", _dataPackage.Addresses, e => e.Name);

            AppendDataSet("Authors", _dataPackage.Authors);
            AppendDataSet("JobAssignments", _dataPackage.JobAssignments);

            AppendDataSet("Metadata Categories", _dataPackage.MetadataCategories, e => e.Name);
            AppendDataSet("Metadata Definitions", _dataPackage.MetadataDefinitions, e => e.Name);
            AppendDataSet("Author Roles", _dataPackage.AuthorRoles, e => e.Name);
            AppendDataSet("Term Store Import", _dataPackage.TermStoreImports);

            AppendDataSet("Notifications", _dataPackage.Notifications, e => e.Name);

            return _report.ToString();
        }

        private static string GetBrickTextDescription(BrickText brickText, IDictionary<Guid, Brick> bricks, IDictionary<Guid, Text> texts)
        {
            string brickName = bricks.ContainsKey(brickText.BrickId) ? bricks[brickText.BrickId].Name : brickText.BrickId.ToString();
            string textAlias = texts.ContainsKey(brickText.TextId) ? texts[brickText.TextId].Alias : brickText.TextId.ToString();

            return $"Text '{textAlias}' used by brick '{brickName}'";
        }

        private static string GetTranslationDescription(
            Translation translation,
            IDictionary<Guid, Text> texts,
            IDictionary<Guid, Language> languages)
        {
            string textAlias = texts.ContainsKey(translation.TextId) ? texts[translation.TextId].Alias : translation.TextId.ToString();
            string languageName = languages.ContainsKey(translation.LanguageId)
                ? languages[translation.LanguageId].Name
                : translation.LanguageId.ToString();

            return $"Text '{textAlias}' in {languageName}";
        }

        private string GetLocaliseResourceDescription(
            LocalisedResource localisedResource,
            Dictionary<Guid, Resource> resources,
            Dictionary<Guid, Language> languages)
        {
            string resourceAlias = resources.ContainsKey(localisedResource.ResourceId)
                ? resources[localisedResource.ResourceId].Alias
                : localisedResource.ResourceId.ToString();
            string languageName = languages.ContainsKey(localisedResource.LanguageId)
                ? languages[localisedResource.LanguageId].Name
                : localisedResource.LanguageId.ToString();

            return $"Resource '{resourceAlias}' in {languageName}";
        }

        private string GetSystemTextDescription(
            SystemText systemText,
            Dictionary<Guid, Text> texts)
        {
            string textAlias = texts.ContainsKey((Guid)systemText.TextId) ? texts[(Guid)systemText.TextId].Alias : systemText.TextId.ToString();

            return $"Text '{textAlias}'";
        }

        private string GetSystemResourceDescription(
            SystemResource systemResource,
            Dictionary<Guid, Resource> resources)
        {
            string resourceAlias = resources.ContainsKey((Guid)systemResource.ResourceId) ? resources[(Guid)systemResource.ResourceId].Alias : systemResource.ResourceId.ToString();

            return $"Resource '{resourceAlias}'";
        }

        private void AppendDataSet<T>(string name, IReadOnlyCollection<T> entities, Func<T, string> itemToStringFunc = null)
        {
            string optionalColon = entities.Any() && itemToStringFunc != null ? ":" : "";
            _report.AppendLine($"\n{entities.Count} {name}{optionalColon}");
            if (itemToStringFunc != null)
            {
                var itemDescriptions = entities.Select(itemToStringFunc).OrderBy(d => d).ToList();
                foreach (string itemDescription in itemDescriptions)
                {
                    _report.AppendLine($"\t– {itemDescription}");
                }
            }
        }
    }
}
