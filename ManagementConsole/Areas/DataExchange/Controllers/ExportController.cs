﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.ManagementConsole.Areas.DataExchange.Database;
using Eurolook.ManagementConsole.Controllers;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Eurolook.Web.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Areas.DataExchange.Controllers
{
    [Authorize(Policy = "OrgaAdmin")]
    public class ExportController : EurolookController
    {
        private const string FileNotFoundError = "Could not find Export file on server.";
        private readonly IAdminDatabase _adminDatabase;
        private readonly IImportExportDatabase _importExportDatabase;
        private readonly ExportService _exportService;
        private readonly ApplicationInfo _applicationInfo;

        public ExportController(
            ISettingsService settingsService,
            IAdminDatabase adminDatabase,
            IImportExportDatabase importExportDatabase,
            ExportService exportService,
            ApplicationInfo applicationInfo)
            : base(settingsService)
        {
            _adminDatabase = adminDatabase;
            _importExportDatabase = importExportDatabase;
            _exportService = exportService;
            _applicationInfo = applicationInfo;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> All(
            [FromForm] List<Guid> documentModelIds,
            [FromForm] bool includeCharacterMappings,
            [FromForm] bool includeColorSchemes,
            [FromForm] bool includeStyleShortcuts,
            [FromForm] bool includeUserGroups,
            [FromForm] bool includeAddresses,
            [FromForm] bool includePredefinedFunctions,
            [FromForm] bool includeSystemTextsAndResources)
        {
            try
            {
                var documentModels = new List<DocumentModelDataModel>();
                if (documentModelIds != null)
                {
                    foreach (var id in documentModelIds)
                    {
                        documentModels.Add(new DocumentModelDataModel()
                        {
                            Id = id,
                        });
                    }
                }

                var exportPackage = await _exportService.CreateAllDataExportPackage(
                    documentModels,
                    includeCharacterMappings,
                    includeColorSchemes,
                    includeStyleShortcuts,
                    includeUserGroups,
                    includeAddresses,
                    includePredefinedFunctions,
                    includeSystemTextsAndResources);
                return JsonResult(new { PackageId = exportPackage.Id });
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The data package could not be created. {ex.GetInnerException().Message}");
            }
        }

        // GET: Export/Package/<id>
        public async Task<ActionResult> Package(Guid id)
        {
            try
            {
                var exportPackage = await _exportService.GetExportPackageFromDiskAsync(id);
                return View(exportPackage);
            }
            catch (FileNotFoundException)
            {
                return NotFound(FileNotFoundError);
            }
        }

        // GET: Export/Download/<id>
        public async Task<ActionResult> Download(Guid id)
        {
            try
            {
                var exportPackage = await _exportService.GetExportPackageFromDiskAsync(id);
                using (var ms = new MemoryStream())
                {
                    exportPackage.SaveTo(ms);
                    return File(ms.ToArray(), "text/xml", $"Export_{_applicationInfo.EnvironmentName}_{exportPackage.Created:yyyyMMddHHmm}.xml");
                }
            }
            catch (FileNotFoundException)
            {
                return NotFound(FileNotFoundError);
            }
        }

        [HttpGet]
        public ActionResult LoadBricksViewModel()
        {
            try
            {
                var bricksvms = new List<BrickDataModel>();
                foreach (var b in _adminDatabase.GetAllBricks())
                {
                    bricksvms.Add(new BrickDataModel(b));
                }

                bricksvms = bricksvms.OrderBy(b => b.DisplayName).ToList();
                return JsonResult(bricksvms.ToArray());
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The bricksviewmodels could not be loaded. {ex.GetInnerException().Message}");
            }
        }

        [HttpGet]
        public ActionResult LoadAllDocumentModelViewModel()
        {
            try
            {
                var documentModelsvms = new List<DocumentModelDataModel>();
                foreach (var documentModel in _adminDatabase.GetAllDocumentModels().OrderBy(dm => dm.Name))
                {
                    documentModelsvms.Add(new DocumentModelDataModel(documentModel));
                }

                return JsonResult(documentModelsvms.OrderBy(d => d.Name).ToArray());
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The documentModelViewModels could not be loaded. {ex.GetInnerException().Message}");
            }
        }

        [HttpGet]
        public async Task<JsonResult> LoadAllDocumentModelCentralExportDataModel()
        {
            try
            {
                var dms = await _importExportDatabase.GetDocumentModels();
                var result = dms.Select(
                    x => new DocumentModelDataModel()
                    {
                        Id = x.Id,
                        Name = x.Name,
                    }).ToList();

                return new JsonResult(result.OrderBy(d => d.Name));
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"Search for Document Model failed. {ex.GetInnerException().Message}");
            }
        }

        [HttpGet]
        public ActionResult LoadAllTextViewModel()
        {
            try
            {
                var texts = new List<TextDataModel>();
                foreach (var text in _adminDatabase.GetAllTexts())
                {
                    texts.Add(
                        new TextDataModel(text)
                        {
                            TranslationCount = _adminDatabase.GetTranslationCount(text),
                        });
                }

                return JsonResult(texts.OrderBy(d => d.Alias).ToArray());
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The documentModelViewModels could not be loaded. {ex.GetInnerException().Message}");
            }
        }
    }
}
