using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Controllers;
using Eurolook.ManagementConsole.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Areas.DataExchange.Controllers
{
    [Authorize(Policy = "OrgaAdmin")]
    public class SnapshotsController : EurolookController
    {
        private readonly ImportService _importService;

        public SnapshotsController(ISettingsService settingsService, ImportService importService)
            : base(settingsService)
        {
            _importService = importService;
        }

        public ActionResult Index()
        {
            var viewModel = new List<DataPackage>();
            foreach (string fileName in Directory.GetFiles(_importService.SnapshotsFolder))
            {
                viewModel.Add(DataPackage.FromFile(fileName, true));
            }

            return View(viewModel.OrderByDescending(dp => dp.Created));
        }

        [HttpPost]
        public async Task<ActionResult> Restore(Guid id)
        {
            try
            {
                await _importService.RestoreSnapshot(id);

                return JsonResult("The snapshot has been restored.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"Could not restore the snapshot. {ex.GetInnerException().Message}");
            }
        }
    }
}
