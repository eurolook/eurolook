using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.ManagementConsole.Areas.DataExchange.Database;
using Eurolook.ManagementConsole.Controllers;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Areas.DataExchange.Controllers
{
    [Authorize(Policy = "OrgaAdmin")]
    public class CleanupController : EurolookController
    {
        private readonly IImportExportDatabase _importExportDatabase;

        public CleanupController(ISettingsService settingsService, IImportExportDatabase importExportDatabase)
            : base(settingsService)
        {
            _importExportDatabase = importExportDatabase;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<JsonResult> GetOrgaEntities()
        {
            try
            {
                var entities = await _importExportDatabase.GetOldOrgaEntities();
                var result = entities.Select(
                    x => new OrgChartCleanupDataModel
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Texts = x.HeaderTextId.HasValue ? 1 : 0,
                        Translations = x.HeaderText?.Translations?.Count ?? 0,
                    }).ToList();

                return new JsonResult(result);
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"Search for Cleanup data failed. {ex.GetInnerException().Message}");
            }
        }

        [HttpPost]
        public async Task<JsonResult> DeleteOrgaEntities([FromForm] List<Guid> entitiesToDelete)
        {
            try
            {
                await _importExportDatabase.CleanupOrgaEntities(entitiesToDelete ?? new List<Guid>());
                return JsonResult("OrgaCharts successfully cleaned.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"Cleanup failed. {ex.GetInnerException().Message}");
            }
        }

        [HttpGet]
        public async Task<JsonResult> GetDevices()
        {
            try
            {
                int devices = await _importExportDatabase.GetDevicesCount();
                return new JsonResult(devices);
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"Search for Cleanup data failed. {ex.GetInnerException().Message}");
            }
        }

        [HttpDelete]
        public async Task<JsonResult> DeleteDevices()
        {
            try
            {
                await _importExportDatabase.CleanupDevices();
                return JsonResult("Devices successfully cleaned.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"Cleanup failed. {ex.GetInnerException().Message}");
            }
        }

        [HttpGet]
        public async Task<JsonResult> GetTexts()
        {
            try
            {
                var texts = await _importExportDatabase.GetOrphanedTexts();
                var result = texts.Select(
                    x => new TextCleanupDataModel
                    {
                        Id = x.Id,
                        Name = x.Alias,
                        Translations = x.Translations.Count,
                    }).ToList();

                return new JsonResult(result);
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"Search for Cleanup data failed. {ex.GetInnerException().Message}");
            }
        }

        [HttpPost]
        public async Task<JsonResult> DeleteTexts([FromForm] List<Guid> textsToDelete)
        {
            try
            {
                await _importExportDatabase.CleanupTexts(textsToDelete ?? new List<Guid>());
                return JsonResult("Texts successfully cleaned.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"Cleanup failed. {ex.GetInnerException().Message}");
            }
        }
    }
}
