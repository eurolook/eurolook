﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Controllers;
using Eurolook.ManagementConsole.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.ManagementConsole.Areas.DataExchange.Controllers
{
    [Authorize(Policy = "OrgaAdmin")]
    [SuppressMessage("csharpsquid", "S5693:Make sure the content length limit is safe here.", Justification = "API endpoint is internal only.")]
    public class ImportController : EurolookController
    {
        private readonly ImportService _importService;
        private readonly UploadService _uploadService;

        public ImportController(
            ISettingsService settingsService,
            UploadService uploadService,
            ImportService importService)
            : base(settingsService)
        {
            _uploadService = uploadService;
            _importService = importService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [DisableRequestSizeLimit]
        [RequestFormLimits(MultipartBodyLengthLimit = int.MaxValue, ValueLengthLimit = int.MaxValue)]
        public async Task<ActionResult> UploadDataPackage(IFormFile file)
        {
            try
            {
                var uploadedFileId = await _uploadService.UploadFile(file);
                var uploadedPackage = DataPackage.FromFile(_uploadService.GetUploadFileName(uploadedFileId));
                var diffPackage = await _importService.CreateDiffPackage(uploadedPackage);

                return JsonResult(
                    new
                    {
                        UploadId = uploadedFileId,
                        UploadFileName = file.FileName,
                        UploadDescription = uploadedPackage.Description,
                        UploadCreated = uploadedPackage.Created,
                        Diff = diffPackage,
                    });
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The data package could not be uploaded. {ex.GetInnerException().Message}");
            }
        }

        public async Task<ActionResult> CalculateDifferences(Guid id)
        {
            try
            {
                var diffPackage = DataPackage.FromFile(_importService.GetDiffFileName(id));
                var comparison = await _importService.CreateComparison(diffPackage);
                return JsonResult(
                    new
                    {
                        UploadId = diffPackage.Id,
                        UploadDescription = diffPackage.Description,
                        UploadCreated = diffPackage.Created,
                        Comparison = comparison,
                    });
            }
            catch (FileNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<ActionResult> ImportDataPackage(Guid id)
        {
            try
            {
                await _importService.ImportDiffPackage(id);
                return JsonResult("The data package was imported, a snapshot has been created.");
            }
            catch (Exception ex)
            {
                return HandleError(ex, $"The data package could not be imported. {ex.GetInnerException().Message}");
            }
        }
    }
}
