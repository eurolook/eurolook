﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Eurolook.Data.Models;
using Eurolook.Data.Models.AuthorRoles;
using Eurolook.Data.Models.Metadata;
using Eurolook.ManagementConsole.Areas.DataExchange.Database;
using Eurolook.ManagementConsole.Controllers;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.ViewModels.Address;
using Microsoft.AspNetCore.Hosting;

namespace Eurolook.ManagementConsole.Areas.DataExchange
{
    public class ExportService
    {
        private readonly IImportExportDatabase _importExportDatabase;

        public ExportService(IWebHostEnvironment webHostEnvironment, IImportExportDatabase importExportDatabase)
        {
            _importExportDatabase = importExportDatabase;
            ExportsFolder = Path.Combine(webHostEnvironment.ContentRootPath, "App_Data", "Exports");
        }

        public string ExportsFolder { get; }

        public async Task<DataPackage> ExportAddresses(List<AddressViewModel> addressVms, bool includeDependencies)
        {
            var dataPackage =
                await _importExportDatabase.GetAddressesAsPackageAsync(addressVms, includeDependencies);
            dataPackage.Description = "Export of Addresses";
            dataPackage.SaveAs(GetExportFileName(dataPackage.Id));
            return dataPackage;
        }

        public async Task<DataPackage> ExportPredefinedFunctions(
            List<PredefinedFunctionDataModel> predefinedFunctions,
            bool includeDependencies)
        {
            var dataPackage =
                await _importExportDatabase.GetPredefinedFunctionsAsPackageAsync(
                    predefinedFunctions,
                    includeDependencies);
            dataPackage.Description = "Export of PredefinedFunctions";
            dataPackage.SaveAs(GetExportFileName(dataPackage.Id));
            return dataPackage;
        }

        public async Task<DataPackage> ExportMetadataCategories(List<MetadataCategory> metadataCategories)
        {
            var dataPackage =
                await _importExportDatabase.GetMetadataCategoriesAsPackageAsync(metadataCategories);
            dataPackage.Description = "Export of Metadata Categories";
            dataPackage.SaveAs(GetExportFileName(dataPackage.Id));
            return dataPackage;
        }

        public async Task<DataPackage> ExportMetadataDefinitions(
            List<MetadataDefinition> metadataDefinitions,
            bool includeMetadataCategories)
        {
            var dataPackage =
                await _importExportDatabase.GetMetadataDefinitionsAsPackageAsync(
                    metadataDefinitions,
                    includeMetadataCategories);
            dataPackage.Description = "Export of Metadata Definitions";
            dataPackage.SaveAs(GetExportFileName(dataPackage.Id));
            return dataPackage;
        }

        public async Task<DataPackage> ExportAuthorRoles(List<AuthorRole> authorRoles)
        {
            var dataPackage = await _importExportDatabase.GetAuthorRolesAsPackageAsync(authorRoles);
            dataPackage.Description = "Export of Author Roles";
            dataPackage.SaveAs(GetExportFileName(dataPackage.Id));
            return dataPackage;
        }

        public async Task<DataPackage> ExportUserGroups(List<UserGroup> userGroups)
        {
            var dataPackage = await _importExportDatabase.GetUserGroupsAsPackageAsync(userGroups);
            dataPackage.Description = "Export of User Groups";
            dataPackage.SaveAs(GetExportFileName(dataPackage.Id));
            return dataPackage;
        }

        internal async Task<DataPackage> ExportPersonNames(List<PersonNamesDataModel> personName)
        {
            var dataPackage = await _importExportDatabase.GetPersonNamesAsPackageAsync(personName);
            dataPackage.Description = "Export of PersonNames";
            dataPackage.SaveAs(GetExportFileName(dataPackage.Id));
            return dataPackage;
        }

        internal async Task<DataPackage> ExportDocumentCategory(List<DocumentCategoryDataModel> documentCategory)
        {
            var dataPackage = await _importExportDatabase.GetDocumentCategoryAsPackageAsync(documentCategory);
            dataPackage.Description = "Export of DocumentCategories";
            dataPackage.SaveAs(GetExportFileName(dataPackage.Id));
            return dataPackage;
        }

        internal async Task<DataPackage> CreateAllDataExportPackage(
            List<DocumentModelDataModel> documentModelVms,
            bool includeCharacterMappings,
            bool includeColorSchemes,
            bool includeStyleShortcuts,
            bool includeUserGroups,
            bool includeAddresses,
            bool includePredefinedFunctions,
            bool includeSystemTextsAndResources)
        {
            DataPackage dataPackage;
            if (documentModelVms.Count != 0)
            {
                dataPackage = await _importExportDatabase.GetDocumentModelsAsPackageAsync(
                    documentModelVms,
                    true,
                    true,
                    true,
                    true,
                    true);
            }
            else
            {
                dataPackage = new DataPackage();
            }

            if (includeCharacterMappings)
            {
                await _importExportDatabase.LoadCharacterMappingsForPackage(dataPackage);
            }

            if (includeColorSchemes)
            {
                await _importExportDatabase.LoadColorSchemesForPackage(dataPackage);
            }

            if (includeStyleShortcuts)
            {
                await _importExportDatabase.LoadStyleShortcutsForPackage(dataPackage);
            }

            if (includeUserGroups)
            {
                await _importExportDatabase.LoadUserGroupsForPackage(dataPackage);
            }

            if (includeAddresses)
            {
                await _importExportDatabase.LoadAddressesForPackage(dataPackage);
            }

            if (includePredefinedFunctions)
            {
                await _importExportDatabase.LoadPredefinedFunctionsForPackage(dataPackage);
            }

            if (includeSystemTextsAndResources)
            {
                await _importExportDatabase.LoadSystemTextsAndResourcesForPackage(dataPackage);
            }

            dataPackage.SaveAs(GetExportFileName(dataPackage.Id));
            return dataPackage;
        }

        internal async Task<DataPackage> ExportDocumentModels(
            List<DocumentModelDataModel> documentModelVms,
            bool includeDocumentStructure,
            bool includeBricks,
            bool includeDocumentModelMetadataDefinitions,
            bool includeMetadataDefinitions,
            bool includeAuthorRoles)
        {
            var dataPackage = await _importExportDatabase.GetDocumentModelsAsPackageAsync(
                documentModelVms,
                includeDocumentStructure,
                includeBricks,
                includeDocumentModelMetadataDefinitions,
                includeMetadataDefinitions,
                includeAuthorRoles);

            dataPackage.Description = "Export of Document Data";
            dataPackage.SaveAs(GetExportFileName(dataPackage.Id));
            return dataPackage;
        }

        internal async Task<DataPackage> ExportStylesShortcuts(List<StyleShortcutDataModel> styleShortcuts)
        {
            var dataPackage = await _importExportDatabase.GetStyleShortcutsAsPackageAsync(styleShortcuts);
            dataPackage.Description = "Export of StyleShortcuts";
            dataPackage.SaveAs(GetExportFileName(dataPackage.Id));
            return dataPackage;
        }

        internal async Task<DataPackage> ExportBricks(
            List<BrickDataModel> brickVms,
            bool includeGroups,
            bool includeTexts,
            bool includeResources,
            bool includeUserGroups)
        {
            var dataPackage = await _importExportDatabase.GetBricksAsPackageAsync(
                brickVms,
                includeGroups,
                includeTexts,
                includeResources,
                includeUserGroups);
            dataPackage.Description = "Export of Bricks";
            dataPackage.SaveAs(GetExportFileName(dataPackage.Id));
            return dataPackage;
        }

        internal async Task<DataPackage> ExportSystemConfiguration(List<SystemConfigurationModel> systemConfigurationModels)
        {
            var dataPackage = await _importExportDatabase.GetSystemConfigurationAsPackageAsync(systemConfigurationModels);
            dataPackage.Description = "Export of System Configuration";
            dataPackage.SaveAs(GetExportFileName(dataPackage.Id));
            return dataPackage;
        }

        internal async Task<DataPackage> ExportTexts(List<TextDataModel> textDms, bool includeTranslations)
        {
            var dataPackage = await _importExportDatabase.GetTextsAsPackageAsync(textDms, includeTranslations);
            dataPackage.Description = "Export of Texts";
            dataPackage.SaveAs(GetExportFileName(dataPackage.Id));
            return dataPackage;
        }

        internal async Task<DataPackage> ExportTranslations(List<TranslationDataModel> translations, bool includeTexts)
        {
            var dataPackage =
                await _importExportDatabase.GetTranslationsAsPackageAsync(translations, includeTexts);
            dataPackage.Description = "Export of Translations";
            dataPackage.SaveAs(GetExportFileName(dataPackage.Id));
            return dataPackage;
        }

        internal async Task<DataPackage> ExportResources(
            List<ResourceDataModel> resources,
            bool includeDefaultData,
            bool includeLocalisations)
        {
            var dataPackage = await _importExportDatabase.GetResourcesAsPackageAsync(
                resources,
                includeDefaultData,
                includeLocalisations);

            dataPackage.Description = "Export of Resource Data";
            dataPackage.SaveAs(GetExportFileName(dataPackage.Id));
            return dataPackage;
        }

        internal async Task<DataPackage> ExportNotification(List<NotificationsDataModel> notifications)
        {
            var dataPackage = await _importExportDatabase.GetNotificationsAsPackageAsync(notifications);
            dataPackage.Description = "Export of Notifications";
            dataPackage.SaveAs(GetExportFileName(dataPackage.Id));
            return dataPackage;
        }

        internal async Task<DataPackage> ExportAllOrgaCharts()
        {
            var dataPackage = await _importExportDatabase.GetOrgaChartsDataPackageAsync();
            dataPackage.Description = "Export of all orga charts";
            dataPackage.SaveAs(GetExportFileName(dataPackage.Id));
            return dataPackage;
        }

        internal async Task<DataPackage> ExportOrgaChart(Guid dgId)
        {
            var dataPackage = await _importExportDatabase.GetOrgaChartDataPackageAsync(dgId);
            dataPackage.Description = "Export of an orga chart.";
            dataPackage.SaveAs(GetExportFileName(dataPackage.Id));
            return dataPackage;
        }

        internal async Task<DataPackage> GetExportPackageFromDiskAsync(Guid id)
        {
            var package = DataPackage.FromFile(GetExportFileName(id));
            package.Report = await new DataPackageReport(_importExportDatabase, package).CreateReportAsync();
            return package;
        }

        private string GetExportFileName(Guid id)
        {
            if (!Directory.Exists(ExportsFolder))
            {
                Directory.CreateDirectory(ExportsFolder);
            }

            return $"{ExportsFolder}/{id}_Export.xml";
        }
    }
}
