using System;
using System.IO;
using System.Threading.Tasks;
using Eurolook.Data.DataPackageComparison;
using Eurolook.Data.Models;
using Eurolook.ManagementConsole.Areas.DataExchange.Database;
using Microsoft.AspNetCore.Hosting;

namespace Eurolook.ManagementConsole.Areas.DataExchange
{
    public class ImportService
    {
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IImportExportDatabase _importExportDatabase;

        public ImportService(IWebHostEnvironment webHostEnvironment, IImportExportDatabase importExportDatabase)
        {
            _webHostEnvironment = webHostEnvironment;
            _importExportDatabase = importExportDatabase;
            DiffsFolder = GetDiffsFolderName();
            SnapshotsFolder = GetSnapshotsFolderName();
        }

        public string DiffsFolder { get; }

        public string SnapshotsFolder { get; }

        public async Task<DataPackage> CreateDiffPackage(DataPackage basePackage)
        {
            var allDataPackage = await _importExportDatabase.GetAllDataAsPackageAsync();
            var diffPackage = allDataPackage.Diff(basePackage);
            diffPackage.Description = $"Diff for {basePackage.Description}";
            diffPackage.Report = await new DataPackageReport(_importExportDatabase, diffPackage).CreateReportAsync();
            diffPackage.SaveAs(GetDiffFileName(diffPackage.Id));
            return diffPackage;
        }

        public async Task<ComparisonResult> CreateComparison(DataPackage newPackage)
        {
            var allDataPackage = await _importExportDatabase.GetAllDataAsPackageAsync();
            return new DataPackageComparer().Compare(allDataPackage, newPackage);
        }

        public async Task<DataPackage> CreateOrgaUploadDiffPackage(DataPackage basePackage)
        {
            var allDataPackage = await _importExportDatabase.GetOrgaChartsDataPackageAsync();
            var diffPackage = allDataPackage.DiffOrgaUploadPackage(basePackage);
            diffPackage.Description = $"Diff for {basePackage.Description}";
            diffPackage.Report = await new DataPackageReport(_importExportDatabase, diffPackage).CreateReportAsync();
            diffPackage.SaveAs(GetDiffFileName(diffPackage.Id));
            return diffPackage;
        }

        internal async Task ImportDiffPackage(Guid id)
        {
            var diffPackage = DataPackage.FromFile(GetDiffFileName(id));
            await ImportDiffPackage(diffPackage);
        }

        internal async Task ImportDiffPackage(DataPackage diffPackage)
        {
            var snapshot = await _importExportDatabase.ImportDiffPackageAsync(diffPackage);
            snapshot.Report = await new DataPackageReport(_importExportDatabase, snapshot).CreateReportAsync();
            string snapshotFileName = GetSnapshotFileName(snapshot.Id);
            snapshot.SaveAs(snapshotFileName);
        }

        internal async Task RestoreSnapshot(Guid snapshotPackageId)
        {
            var snapshotPackage = DataPackage.FromFile(GetSnapshotFileName(snapshotPackageId));
            await RestoreSnapshot(snapshotPackage);
        }

        internal async Task RestoreSnapshot(DataPackage snapshotPackage)
        {
            await _importExportDatabase.RestoreSnapshotAsync(snapshotPackage);
        }

        public string GetDiffFileName(Guid id)
        {
            return $"{DiffsFolder}/{id}_Diff.xml";
        }

        private string GetSnapshotFileName(Guid id)
        {
            return $"{SnapshotsFolder}/{id}_Snapshot.xml";
        }

        private string GetDiffsFolderName()
        {
            string folderName = Path.Combine(_webHostEnvironment.ContentRootPath, "App_Data", "Diffs");
            if (!Directory.Exists(folderName))
            {
                Directory.CreateDirectory(folderName);
            }

            return folderName;
        }

        private string GetSnapshotsFolderName()
        {
            string folderName = Path.Combine(_webHostEnvironment.ContentRootPath, "App_Data", "Snapshots");
            if (!Directory.Exists(folderName))
            {
                Directory.CreateDirectory(folderName);
            }

            return folderName;
        }
    }
}
