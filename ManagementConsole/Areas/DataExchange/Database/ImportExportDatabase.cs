﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using EFCore.BulkExtensions;
using Eurolook.Common.Extensions;
using Eurolook.Data;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.Data.Models.AuthorRoles;
using Eurolook.Data.Models.Metadata;
using Eurolook.ManagementConsole.Controllers;
using Eurolook.ManagementConsole.Database;
using Eurolook.ManagementConsole.Models;
using Eurolook.ManagementConsole.ViewModels.Address;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NetTopologySuite.IO;

namespace Eurolook.ManagementConsole.Areas.DataExchange.Database
{
    public interface IImportExportDatabase
    {
        ILanguageRepository LanguageRepository { get; }

        Task<DataPackage> GetAllDataAsPackageAsync();

        Task<DataPackage> GetOrgaChartDataPackageAsync(Guid dgId);

        Task<DataPackage> GetOrgaChartsDataPackageAsync();

        /// <summary>
        /// Creates a DataPackage containing all Eurolook data but not the
        /// Orga Chart (OrgaEntities, Texts and Translations) and not the PersonNames.
        /// </summary>
        Task<DataPackage> GetExportPackageAsync();

        Task<DataPackage> GetDocumentModelsAsPackageAsync(
            List<DocumentModelDataModel> documentModelVms,
            bool includeDocumentStructure,
            bool includeBricks,
            bool includeDocumentModelMetadataDefinitions,
            bool includeMetadataDefinitions,
            bool includeAuthorRoles);

        Task<DataPackage> GetBricksAsPackageAsync(List<BrickDataModel> brickVms, bool includeGroups, bool includeTexts, bool includeResources, bool includeUserGroups);

        Task<DataPackage> GetPersonNamesAsPackageAsync(List<PersonNamesDataModel> personName);

        Task<DataPackage> GetDocumentCategoryAsPackageAsync(List<DocumentCategoryDataModel> documentCategories);

        Task<DataPackage> GetStyleShortcutsAsPackageAsync(List<StyleShortcutDataModel> styleShortcuts);

        Task<DataPackage> GetSystemConfigurationAsPackageAsync(List<SystemConfigurationModel> systemConfigurationModels);

        Task<DataPackage> GetTextsAsPackageAsync(List<TextDataModel> textDms, bool includeTranslations);

        Task<DataPackage> GetTranslationsAsPackageAsync(List<TranslationDataModel> translations, bool includeTexts);

        Task<DataPackage> GetResourcesAsPackageAsync(
            List<ResourceDataModel> resources,
            bool includeDefaultData,
            bool includeLocalisations);

        Task<DataPackage> GetAddressesAsPackageAsync(List<AddressViewModel> addressVms, bool includeDependencies);

        Task<DataPackage> GetPredefinedFunctionsAsPackageAsync(List<PredefinedFunctionDataModel> predefinedFunctions, bool includeDependencies);

        Task<DataPackage> GetMetadataCategoriesAsPackageAsync(List<MetadataCategory> metadataCategories);

        Task<DataPackage> GetMetadataDefinitionsAsPackageAsync(List<MetadataDefinition> metadataDefinitions, bool includeMetadataCategories);

        Task<DataPackage> GetAuthorRolesAsPackageAsync(List<AuthorRole> authorRoles);

        Task<DataPackage> GetUserGroupsAsPackageAsync(List<UserGroup> userGroups);

        Task<IEnumerable<DocumentModel>> GetDocumentModels();

        Task<DataPackage> ImportDiffPackageAsync(DataPackage diffPackage);

        Task RestoreSnapshotAsync(DataPackage snapshot);

        Task<IEnumerable<OrgaEntity>> GetOldOrgaEntities();

        Task<IEnumerable<Text>> GetOrphanedTexts();

        Task<int> GetDevicesCount();

        Task CleanupDevices();

        Task CleanupTexts(List<Guid> textsToDelete);

        Task CleanupOrgaEntities(List<Guid> entitiesToDelete);

        Task LoadCharacterMappingsForPackage(DataPackage dataPackage);

        Task LoadColorSchemesForPackage(DataPackage dataPackage);

        Task LoadStyleShortcutsForPackage(DataPackage dataPackage);

        Task LoadUserGroupsForPackage(DataPackage dataPackage);

        Task LoadAddressesForPackage(DataPackage dataPackage);

        Task LoadPredefinedFunctionsForPackage(DataPackage dataPackage);

        Task LoadSystemTextsAndResourcesForPackage(DataPackage dataPackage);

        Task<List<Text>> GetAllTextsAsync();

        Task<List<Resource>> GetAllResourcesAsync();

        Task<List<Brick>> GetAllBricksAsync();

        Task<DataPackage> GetNotificationsAsPackageAsync(List<NotificationsDataModel> notifications);
    }

    public class ImportExportDatabase : AdminDatabase, IImportExportDatabase
    {
        public ILanguageRepository LanguageRepository { get; }

        public ImportExportDatabase(IConfiguration configuration)
            : base(configuration)
        {
            LanguageRepository = new LanguageRepository(GetContext);
        }

        public Task<DataPackage> GetAllDataAsPackageAsync()
        {
            return Task.Factory.StartNew(() =>
            {
                using (var context = GetContext())
                {
                    var result = new DataPackage
                    {
                        Addresses = context.Addresses.ToList(),
                        BrickCategories = context.BrickCategories.ToList(),
                        BrickGroups = context.BrickGroups.ToList(),
                        BrickResources = context.BrickResources.ToList(),
                        BrickTexts = context.BrickTexts.ToList(),
                        CharacterMappings = context.CharacterMappings.ToList(),
                        CommandBricks = context.Bricks.OfType<CommandBrick>().ToList(),
                        ContentBricks = context.Bricks.OfType<ContentBrick>().Where(b => !(b is DynamicBrick)).ToList(),
                        ColorSchemes = context.ColorSchemes.ToList(),
                        DocumentCategories = context.DocumentCategories.AsNoTracking().ToList(),
                        DocumentModels = context.DocumentModels.ToList(),
                        DocumentModelLanguages = context.DocumentModelLanguages.ToList(),
                        DocumentModelMetadataDefinitions = context.DocumentModelMetadataDefinitions.ToList(),
                        DocumentModelAuthorRoles = context.DocumentModelAuthorRoles.ToList(),
                        DocumentStructures = context.DocumentStructure.ToList(),
                        DynamicBricks = context.Bricks.OfType<DynamicBrick>().ToList(),
                        Languages = context.Languages.ToList(),
                        LocalisedResources = context.LocalisedResources.ToList(),
                        MetadataCategories = context.MetadataCategories.ToList(),
                        MetadataDefinitions = context.MetadataDefinitions.ToList(),
                        AuthorRoles = context.AuthorRoles.ToList(),
                        OrgaEntities = context.OrgaEntities.ToList(),
                        PersonNames = context.PersonNames.ToList(),
                        PredefinedFunctions = context.PredefinedFunctions.ToList(),
                        Resources = context.Resources.ToList(),
                        SystemTexts = context.SystemTexts.ToList(),
                        SystemResources = context.SystemResources.ToList(),
                        StyleShortcuts = context.StyleShortcuts.ToList(),
                        Texts = context.Texts.ToList(),
                        Translations = context.Translations.ToList(),
                        UserGroups = context.UserGroups.ToList(),
                        SystemConfigurations = context.SystemConfigurations.ToList(),
                        Notifications = context.Notifications.ToList(),
                    };

                    // clean self-references
                    foreach (var entity in result.OrgaEntities)
                    {
                        entity.SuperEntity = null;
                        entity.SubEntities = null;
                    }

                    // Remove AD Groups
                    result.UserGroups.ForEach(g => g.AdGroups = null);

                    return result;
                }
            });
        }

        public async Task<DataPackage> GetOrgaChartDataPackageAsync(Guid dgId)
        {
            using (var context = GetContext())
            {
                var orgaEntities = await GetOrgaEntityList(context, dgId);
                return await OrgaChartDataPackageAsync(context, orgaEntities);
            }
        }

        public async Task<DataPackage> GetOrgaChartsDataPackageAsync()
        {
            using (var context = GetContext())
            {
                var rootNode = context.OrgaEntities.FirstOrDefault(x => x.LogicalLevel == 0);
                if (rootNode != null)
                {
                    var orgaEntities = await GetOrgaEntityList(context, rootNode.Id);
                    return await OrgaChartDataPackageAsync(context, orgaEntities);
                }

                return new DataPackage();
            }
        }

        private async Task<List<OrgaEntity>> GetOrgaEntityList(EurolookServerContext context, Guid orgaEntityId)
        {
            var result = new List<OrgaEntity>
            {
                await context.OrgaEntities.FirstAsync(o => o.Id == orgaEntityId),
            };
            foreach (var childId in await context.OrgaEntities.Where(o => o.SuperEntityId == orgaEntityId).Select(o => o.Id).ToArrayAsync())
            {
                result.AddRange(await GetOrgaEntityList(context, childId));
            }

            return result;
        }

        private static async Task<DataPackage> OrgaChartDataPackageAsync(EurolookServerContext context, List<OrgaEntity> orgaEntities)
        {
            foreach (var entity in orgaEntities)
            {
                entity.SuperEntity = null;
                entity.SubEntities = null;
            }

            // addresses
            var addressIds = orgaEntities.Select(x => x.PrimaryAddressId).ToList();
            addressIds.AddRange(orgaEntities.Select(x => x.SecondaryAddressId));
            var addresses = await context.Addresses.Where(x => addressIds.Contains(x.Id)).ToListAsync();

            // texts and translations
            var textIds = orgaEntities.Select(x => x.HeaderTextId).ToList();
            textIds.AddRange(addresses.Select(x => x.NameTextId));
            textIds.AddRange(addresses.Select(x => x.LocationTextId));
            textIds.AddRange(addresses.Select(x => x.FooterTextId));
            var texts = await context.Texts.Where(x => textIds.Contains(x.Id)).ToListAsync();
            var translations = await context.Translations.Where(x => textIds.Contains(x.TextId)).ToListAsync();

            return new DataPackage
            {
                Addresses = addresses,
                OrgaEntities = orgaEntities,
                Texts = texts,
                Translations = translations,
            };
        }

        /// <summary>
        /// Creates a DataPackage containing all Eurolook data but not the
        /// Orga Chart (OrgaEntities, Texts and Translations), the PersonNames and the AD Groups for the user groups.
        /// </summary>
        public async Task<DataPackage> GetExportPackageAsync()
        {
            using (var context = GetContext())
            {
                ////context.Database.SetCommandTimeout(5 * 60);

                var orgaHeaderTextIds = await context.OrgaEntities.AsNoTracking().Where(o => o.HeaderTextId != null).Select(o => o.HeaderTextId.Value).ToArrayAsync();

                // get the texts, but not the OrgaEntity HeaderTexts
                var textsWithoutOrgaChart = await context.Texts.AsNoTracking().Where(t => !orgaHeaderTextIds.Contains(t.Id)).ToListAsync();

                // get the translations, but not the OrgaEntity HeaderText Translations
                var allTranslations = await context.Translations.AsNoTracking().ToListAsync();
                var orgaHeaderTranslationIds = allTranslations.Where(t => orgaHeaderTextIds.Contains(t.TextId)).Select(t => t.Id).ToList();
                var translationsWithoutOrgaChart = allTranslations.Where(t => !orgaHeaderTranslationIds.Contains(t.Id)).ToList();

                // get the user groups, but not the AD groups
                var userGroups = await context.UserGroups.AsNoTracking().ToListAsync();
                userGroups.ForEach(g => g.AdGroups = null);

                // get other data
                return new DataPackage
                {
                    Addresses = await context.Addresses.AsNoTracking().ToListAsync(),
                    BrickCategories = await context.BrickCategories.AsNoTracking().ToListAsync(),
                    BrickGroups = await context.BrickGroups.AsNoTracking().ToListAsync(),
                    BrickResources = await context.BrickResources.AsNoTracking().ToListAsync(),
                    BrickTexts = await context.BrickTexts.AsNoTracking().ToListAsync(),
                    CharacterMappings = await context.CharacterMappings.AsNoTracking().ToListAsync(),
                    CommandBricks = await context.Bricks.AsNoTracking().OfType<CommandBrick>().ToListAsync(),
                    ContentBricks = await context.Bricks.AsNoTracking().OfType<ContentBrick>().Where(b => !(b is DynamicBrick)).ToListAsync(),
                    ColorSchemes = await context.ColorSchemes.AsNoTracking().ToListAsync(),
                    DocumentCategories = await context.DocumentCategories.AsNoTracking().ToListAsync(),
                    DocumentModels = await context.DocumentModels.AsNoTracking().ToListAsync(),
                    DocumentModelLanguages = await context.DocumentModelLanguages.AsNoTracking().ToListAsync(),
                    DocumentModelMetadataDefinitions = await context.DocumentModelMetadataDefinitions.AsNoTracking().ToListAsync(),
                    DocumentModelAuthorRoles = await context.DocumentModelAuthorRoles.AsNoTracking().ToListAsync(),
                    DocumentStructures = await context.DocumentStructure.AsNoTracking().ToListAsync(),
                    DynamicBricks = await context.Bricks.AsNoTracking().OfType<DynamicBrick>().ToListAsync(),
                    Languages = await context.Languages.AsNoTracking().ToListAsync(),
                    LocalisedResources = await context.LocalisedResources.AsNoTracking().ToListAsync(),
                    MetadataCategories = await context.MetadataCategories.AsNoTracking().ToListAsync(),
                    MetadataDefinitions = await context.MetadataDefinitions.AsNoTracking().ToListAsync(),
                    AuthorRoles = await context.AuthorRoles.AsNoTracking().ToListAsync(),
                    Resources = await context.Resources.AsNoTracking().ToListAsync(),
                    StyleShortcuts = await context.StyleShortcuts.AsNoTracking().ToListAsync(),
                    PredefinedFunctions = await context.PredefinedFunctions.AsNoTracking().ToListAsync(),
                    Texts = textsWithoutOrgaChart,
                    Translations = translationsWithoutOrgaChart,
                    UserGroups = userGroups,
                    SystemConfigurations = await context.SystemConfigurations.AsNoTracking().ToListAsync(),
                    SystemTexts = await context.SystemTexts.AsNoTracking().ToListAsync(),
                    SystemResources = await context.SystemResources.AsNoTracking().ToListAsync(),
                };
            }
        }

        public Task<DataPackage> GetDocumentModelsAsPackageAsync(
            List<DocumentModelDataModel> documentModelVms,
            bool includeDocumentStructure,
            bool includeBricks,
            bool includeDocumentModelMetadataDefinitions,
            bool includeMetadataDefinitions,
            bool includeAuthorRoles)
        {
            return Task.Factory.StartNew(() =>
            {
                var documentModelIds = documentModelVms.Select(vm => vm.Id).ToList();
                using (var context = GetContext())
                {
                    var result = new DataPackage
                    {
                        DocumentModels = context.DocumentModels.Include(dm => dm.DocumentCategory).Where(ds => documentModelIds.Contains(ds.Id)).ToList(),
                        DocumentModelLanguages = context.DocumentModelLanguages.Where(dml => documentModelIds.Contains(dml.DocumentModelId)).ToList(),
                    };

                    foreach (var documentModel in result.DocumentModels.Where(dm => dm.DocumentCategory != null))
                    {
                        if (result.DocumentCategories.All(dc => dc.Id != documentModel.DocumentCategory.Id))
                        {
                            result.DocumentCategories.Add(documentModel.DocumentCategory);
                        }

                        documentModel.DocumentCategory = null;
                    }

                    if (includeDocumentStructure)
                    {
                        result.DocumentStructures = context.DocumentStructure.Where(ds => documentModelIds.Contains(ds.DocumentModelId)).ToList();
                        if (includeBricks)
                        {
                            var documentStructuresBrickIds = result.DocumentStructures.Select(ds => ds.BrickId).ToList();
                            var brickIds = context.Bricks.Where(b => documentStructuresBrickIds.Contains(b.Id)).Select(b => b.Id).ToList();
                            ExportBricks(brickIds, context, result, true, true, true, true);
                        }
                    }

                    if (includeDocumentModelMetadataDefinitions)
                    {
                        result.DocumentModelMetadataDefinitions = context.DocumentModelMetadataDefinitions
                                                                         .Where(dmmd => documentModelIds.Contains(dmmd.DocumentModelId))
                                                                         .ToList();

                        if (includeMetadataDefinitions)
                        {
                            var metadataDefinitionIds = result.DocumentModelMetadataDefinitions
                                                              .Select(dmmd => dmmd.MetadataDefinitionId).Distinct()
                                                              .ToList();

                            result.MetadataDefinitions = context.MetadataDefinitions
                                                                .Where(md => metadataDefinitionIds.Contains(md.Id))
                                                                .ToList();
                        }
                    }

                    if (includeAuthorRoles)
                    {
                        result.DocumentModelAuthorRoles =
                            context.DocumentModelAuthorRoles
                                   .Where(dmar => documentModelIds.Contains(dmar.DocumentModelId))
                                   .ToList();

                        var authorRoleIds = result.DocumentModelAuthorRoles
                                                  .Select(dmar => dmar.AuthorRoleId)
                                                  .Distinct()
                                                  .ToList();

                        result.AuthorRoles = context.AuthorRoles
                                                    .Where(ar => authorRoleIds.Contains(ar.Id))
                                                    .ToList();
                    }

                    result.Languages.AddRange(GetLanguagesForPackages(result, context));
                    return result;
                }
            });
        }

        public Task<DataPackage> GetBricksAsPackageAsync(List<BrickDataModel> brickVms, bool includeGroups, bool includeTexts, bool includeResources, bool includeUserGroups)
        {
            return Task.Factory.StartNew(() =>
            {
                var brickIds = brickVms.Where(vm => vm.Id != null).Select(vm => vm.Id.Value).ToList();
                using (var context = GetContext())
                {
                    var result = new DataPackage();
                    ExportBricks(brickIds, context, result, includeGroups, includeTexts, includeResources, includeUserGroups);
                    result.Languages.AddRange(GetLanguagesForPackages(result, context));
                    return result;
                }
            });
        }

        public async Task<DataPackage> GetPersonNamesAsPackageAsync(List<PersonNamesDataModel> personName)
        {
            var ids = personName.Where(vm => vm.Id != null).Select(vm => vm.Id.Value).ToList();
            using (var context = GetContext())
            {
                var result = new DataPackage();
                result.PersonNames.AddRange(await context.PersonNames.Where(x => ids.Contains(x.Id)).ToArrayAsync());
                return result;
            }
        }

        public async Task<DataPackage> GetDocumentCategoryAsPackageAsync(List<DocumentCategoryDataModel> documentCategories)
        {
            var ids = documentCategories.Where(vm => vm.Id != null).Select(vm => vm.Id.Value).ToList();
            using (var context = GetContext())
            {
                var result = new DataPackage();
                result.DocumentCategories.AddRange(await context.DocumentCategories.Where(x => ids.Contains(x.Id)).ToArrayAsync());
                return result;
            }
        }

        public async Task<DataPackage> GetStyleShortcutsAsPackageAsync(List<StyleShortcutDataModel> styleShortcuts)
        {
            var ids = styleShortcuts.Where(vm => vm.Id != null).Select(vm => vm.Id.Value).ToList();
            using (var context = GetContext())
            {
                var result = new DataPackage();
                result.StyleShortcuts.AddRange(await context.StyleShortcuts.Where(x => ids.Contains(x.Id)).ToArrayAsync());
                return result;
            }
        }

        public Task<DataPackage> GetSystemConfigurationAsPackageAsync(List<SystemConfigurationModel> systemConfigurationModels)
        {
            return Task.Factory.StartNew(() =>
            {
                var systemConfigIds = systemConfigurationModels.Where(vm => vm.Id != null).Select(sc => sc.Id).ToArray();
                using (var context = GetContext())
                {
                    var result = new DataPackage
                    {
                        SystemConfigurations = context.SystemConfigurations.Where(sc => systemConfigIds.Contains(sc.Id)).ToList(),
                    };

                    return result;
                }
            });
        }

        public Task<DataPackage> GetTextsAsPackageAsync(List<TextDataModel> textDms, bool includeTranslations)
        {
            return Task.Factory.StartNew(() =>
            {
                var textIds = textDms.Where(vm => vm.TextId != null).Select(vm => vm.TextId.Value).ToArray();
                using (var context = GetContext())
                {
                    var result = new DataPackage
                    {
                        Texts = context.Texts.Where(t => textIds.Contains(t.Id)).ToList(),
                    };
                    if (includeTranslations)
                    {
                        result.Translations = context.Translations.Where(t => textIds.Contains(t.TextId)).ToList();
                        result.Languages.AddRange(GetLanguagesForPackages(result, context));
                    }

                    return result;
                }
            });
        }

        public async Task<DataPackage> GetTranslationsAsPackageAsync(List<TranslationDataModel> translations, bool includeTexts)
        {
            var ids = translations.Select(t => t.TranslationId).ToArray();
            var result = new DataPackage();
            using (var context = GetContext())
            {
                var query = context.Translations.Where(t => ids.Contains(t.Id));
                if (includeTexts)
                {
                    query = query.Include(t => t.Text);
                }

                var dbTranslations = await query.ToListAsync();
                result.Translations.AddRange(dbTranslations.Select(ShallowClone.Clone));

                var languageIds = dbTranslations.Select(t => t.LanguageId).Distinct();
                var dbLanguages = await context.Languages.Where(l => languageIds.Contains(l.Id)).ToListAsync();
                result.Languages.AddRange(dbLanguages.Select(ShallowClone.Clone));

                if (includeTexts)
                {
                    result.Texts.AddRange(dbTranslations.Select(t => t.Text).Select(ShallowClone.Clone));
                }

                return result;
            }
        }

        public Task<DataPackage> GetResourcesAsPackageAsync(
            List<ResourceDataModel> resources,
            bool includeDefaultData,
            bool includeLocalisations)
        {
            return Task.Factory.StartNew(() =>
            {
                var resourceIds = resources.Select(vm => vm.Id).ToList();
                using (var context = GetContext())
                {
                    var result = new DataPackage
                    {
                        Resources = context.Resources.Where(ds => resourceIds.Contains(ds.Id)).ToList(),
                    };

                    if (!includeDefaultData)
                    {
                        foreach (var r in result.Resources)
                        {
                            r.RawData = null;
                        }
                    }

                    if (includeLocalisations)
                    {
                        result.LocalisedResources = context.LocalisedResources.Include(lr => lr.Resource)
                                                           .Where(lr => resourceIds.Contains(lr.ResourceId)).ToList();
                    }

                    result.Languages.AddRange(GetLanguagesForPackages(result, context));
                    return result;
                }
            });
        }

        public async Task<DataPackage> GetNotificationsAsPackageAsync(List<NotificationsDataModel> notificationsDMs)
        {
            var notificationIds = notificationsDMs.Where(m => m.Id.HasValue)
                                                  .Select(m => m.Id.Value);

            await using var context = GetContext();
            var result = new DataPackage()
            {
                Notifications = await context.Notifications.Where(n => notificationIds.Contains(n.Id)).ToListAsync()
            };

            return result;
        }

        public async Task<DataPackage> GetAddressesAsPackageAsync(List<AddressViewModel> addressVms, bool includeDependencies)
        {
            var addressIds = addressVms.Select(vm => vm.Id).ToArray();
            using (var context = GetContext())
            {
                var result = new DataPackage
                {
                    Addresses = await context.Addresses.Where(t => addressIds.Contains(t.Id)).ToListAsync(),
                };
                if (!includeDependencies)
                {
                    return result;
                }

                var textIds = result.Addresses.Where(x => x.LocationTextId != null).Select(x => x.LocationTextId).ToList();
                textIds.AddRange(result.Addresses.Where(x => x.FooterTextId != null).Select(x => x.FooterTextId));
                textIds.AddRange(result.Addresses.Where(x => x.NameTextId != null).Select(x => x.NameTextId));
                result.Texts = await context.Texts.Where(t => textIds.Contains(t.Id)).ToListAsync();
                result.Translations = await context.Translations.Where(t => textIds.Contains(t.TextId)).ToListAsync();
                result.Languages.AddRange(GetLanguagesForPackages(result, context));
                return result;
            }
        }

        public async Task<DataPackage> GetPredefinedFunctionsAsPackageAsync(List<PredefinedFunctionDataModel> predefinedFunctions, bool includeDependencies)
        {
            using (var context = GetContext())
            {
                var predefinedFunctionIds = predefinedFunctions.Select(x => x.Id).ToArray();
                var result = new DataPackage
                {
                    PredefinedFunctions = await context.PredefinedFunctions.Where(x => predefinedFunctionIds.Contains(x.Id)).ToListAsync(),
                };
                if (!includeDependencies)
                {
                    return result;
                }

                var textIds = predefinedFunctions.Where(x => x.FunctionTextId != null).Select(x => x.FunctionTextId).ToList();
                textIds.AddRange(predefinedFunctions.Where(x => x.FunctionFemaleTextId != null).Select(x => x.FunctionFemaleTextId));
                textIds.AddRange(predefinedFunctions.Where(x => x.FunctionHeaderTextId != null).Select(x => x.FunctionHeaderTextId));
                textIds.AddRange(predefinedFunctions.Where(x => x.FunctionHeaderFemaleTextId != null).Select(x => x.FunctionHeaderFemaleTextId));
                result.Texts = await context.Texts.Where(t => textIds.Contains(t.Id)).ToListAsync();
                result.Translations = await context.Translations.Where(t => textIds.Contains(t.TextId)).ToListAsync();
                result.Languages.AddRange(GetLanguagesForPackages(result, context));
                return result;
            }
        }

        public async Task<DataPackage> GetMetadataCategoriesAsPackageAsync(List<MetadataCategory> metadataCategories)
        {
            using (var context = GetContext())
            {
                var metadataCategoryIds = metadataCategories.Select(x => x.Id).ToArray();
                var result = new DataPackage
                {
                    MetadataCategories = await context.MetadataCategories.Where(x => metadataCategoryIds.Contains(x.Id)).ToListAsync(),
                };

                return result;
            }
        }

        public async Task<DataPackage> GetMetadataDefinitionsAsPackageAsync(List<MetadataDefinition> metadataDefinitions, bool includeMetadataCategories)
        {
            using (var context = GetContext())
            {
                var metadataDefinitionIds = metadataDefinitions.Select(x => x.Id).ToArray();
                var result = new DataPackage
                {
                    MetadataDefinitions = await context.MetadataDefinitions.Where(x => metadataDefinitionIds.Contains(x.Id)).ToListAsync(),
                };

                if (!includeMetadataCategories)
                {
                    return result;
                }

                var metadataCategoryIds = result.MetadataDefinitions
                                                .Where(md => md.MetadataCategoryId != null)
                                                .Select(md => md.MetadataCategoryId)
                                                .Distinct();
                result.MetadataCategories = await context.MetadataCategories.Where(mc => metadataCategoryIds.Contains(mc.Id)).ToListAsync();
                return result;
            }
        }

        public async Task<DataPackage> GetAuthorRolesAsPackageAsync(List<AuthorRole> authorRoles)
        {
            using (var context = GetContext())
            {
                var authorRoleIds = authorRoles.Select(x => x.Id).ToArray();
                var result = new DataPackage
                {
                    AuthorRoles = await context.AuthorRoles
                                               .Where(x => authorRoleIds.Contains(x.Id))
                                               .ToListAsync(),
                };

                return result;
            }
        }

        public async Task<DataPackage> GetUserGroupsAsPackageAsync(List<UserGroup> userGroups)
        {
            using (var context = GetContext())
            {
                var userGroupIds = userGroups.Select(x => x.Id).ToArray();
                var result = new DataPackage
                {
                    UserGroups = await context.UserGroups
                                              .Where(x => userGroupIds.Contains(x.Id))
                                              .ToListAsync(),
                };

                // Remove AD Groups
                result.UserGroups.ForEach(g => g.AdGroups = null);

                return result;
            }
        }

        // *********************************************************************
        //                                                               EXPORT
        // *********************************************************************
        public void ExportBricks(List<Guid> brickIds, EurolookServerContext context, DataPackage package, bool includeGroups, bool includeTexts, bool includeResources, bool includeUserGroups)
        {
            package.CommandBricks.AddRange(context.Bricks.OfType<CommandBrick>().Where(b => brickIds.Contains(b.Id)).OrderBy(b => b.Name));
            package.ContentBricks = context.Bricks.OfType<ContentBrick>().Where(b => !(b is DynamicBrick) && brickIds.Contains(b.Id)).ToList();
            package.DynamicBricks = context.Bricks.OfType<DynamicBrick>().Where(b => brickIds.Contains(b.Id)).ToList();
            if (includeGroups)
            {
                package.BrickGroups.AddRange(GetBrickGroupsForPackage(package, context));
                package.BrickCategories.AddRange(GetBrickCategoriesForPackage(package, context));
            }

            if (includeTexts)
            {
                package.BrickTexts.AddRange(context.BrickTexts.Where(bt => brickIds.Contains(bt.BrickId)));

                var textIds = package.BrickTexts.Select(bt => bt.TextId).ToArray();
                package.Texts = context.Texts.Where(t => textIds.Contains(t.Id)).ToList();
                package.Translations = context.Translations.Where(t => textIds.Contains(t.TextId)).ToList();
            }

            if (includeResources)
            {
                package.BrickResources.AddRange(context.BrickResources.Where(br => brickIds.Contains(br.BrickId)));
                package.Resources.AddRange(GetResourcesForPackage(package, context).OrderBy(r => r.Alias));
                package.LocalisedResources.AddRange(GetLocalisedResourcesForPackage(package, context));
            }

            if (includeUserGroups)
            {
                var userGroupIdsList = package.CommandBricks.Select(b => b.VisibleToUserGroups)
                                              .Concat(package.ContentBricks.Select(b => b.VisibleToUserGroups))
                                              .Concat(package.DynamicBricks.Select(b => b.VisibleToUserGroups));

                string userGroupIds = string.Join(";", userGroupIdsList);
                package.UserGroups.AddRange(context.UserGroups.Where(u => userGroupIds.Contains(u.Id.ToString())));

                // Remove AD Groups
                package.UserGroups.ForEach(g => g.AdGroups = null);
            }
        }

        private IEnumerable<LocalisedResource> GetLocalisedResourcesForPackage(DataPackage result, EurolookServerContext context)
        {
            var resourceIds = result.Resources.Select(r => r.Id).ToArray();
            return context.LocalisedResources.Where(lr => resourceIds.Contains(lr.ResourceId)).ToArray();
        }

        private IEnumerable<Resource> GetResourcesForPackage(DataPackage result, EurolookServerContext context)
        {
            var resourceIds = result.BrickResources.Select(br => br.ResourceId).ToArray();
            return context.Resources.Where(r => resourceIds.Contains(r.Id)).ToArray();
        }

        private IEnumerable<Language> GetLanguagesForPackages(DataPackage result, EurolookServerContext context)
        {
            var textIds = result.Texts.Select(t => t.Id).ToArray();
            var languageIds = result.Translations.Where(t => textIds.Contains(t.TextId)).Select(t => t.LanguageId).ToList();

            var resourceIds = result.BrickResources.Select(br => br.ResourceId).ToArray();
            var resourceLanguageIds = result.LocalisedResources.Where(lr => resourceIds.Contains(lr.ResourceId)).Select(lr => lr.LanguageId).ToArray();
            languageIds.AddRange(resourceLanguageIds);

            return context.Languages.Where(l => languageIds.Contains(l.Id)).ToArray();
        }

        private static IEnumerable<BrickGroup> GetBrickGroupsForPackage(DataPackage result, EurolookServerContext context)
        {
            var groupIds = result.CommandBricks.Where(b => b.GroupId != null).Select(b => b.GroupId).ToList();
            groupIds.AddRange(result.ContentBricks.Where(b => b.GroupId != null).Select(b => b.GroupId));
            groupIds.AddRange(result.DynamicBricks.Where(b => b.GroupId != null).Select(b => b.GroupId));
            var distinctGroupIds = groupIds.Distinct();
            return context.BrickGroups.Where(g => distinctGroupIds.Contains(g.Id)).ToArray();
        }

        private IEnumerable<BrickCategory> GetBrickCategoriesForPackage(DataPackage result, EurolookServerContext context)
        {
            var categoryIds = result.CommandBricks.Select(b => b.CategoryId).ToList();
            categoryIds.AddRange(result.ContentBricks.Select(b => b.CategoryId));
            categoryIds.AddRange(result.DynamicBricks.Select(b => b.CategoryId));
            var distinctCategoryIds = categoryIds.Distinct();
            return context.BrickCategories.Where(c => distinctCategoryIds.Contains(c.Id)).ToArray();
        }

        public async Task<IEnumerable<DocumentModel>> GetDocumentModels()
        {
            using (var context = GetContext())
            {
                return await GetDocumentModels(context).ToListAsync();
            }
        }

        private static IQueryable<DocumentModel> GetDocumentModels(EurolookContext context)
        {
            return context.DocumentModels.Where(t => !t.Deleted);
        }

        // *********************************************************************
        //                                                               IMPORT
        // *********************************************************************
        public async Task<DataPackage> ImportDiffPackageAsync(DataPackage diffPackage)
        {
            // 1. create the snapshot
            var snapshot = await CreateSnapshotAsync(diffPackage);

            // 2. Update the modification flags
            // Add 30 Minutes to ensure that everybody gets the changes,
            // including the users that synchronized while this transaction is running.
            diffPackage.SetModificationTime(DateTime.UtcNow.AddMinutes(30));

            // 3. apply the diff to the database
            using (var context = GetContext())
            {
                AddOrUpdate(context.Authors, diffPackage.Authors.ToArray());
                AddOrUpdate(context.JobAssignments, diffPackage.JobAssignments.ToArray());
                AddOrUpdateBasicEurolookData(context, diffPackage);
                await context.SaveChangesAsync();
                await UpdateDiscriminator(context, diffPackage.CommandBricks.ToArray());
                await UpdateDiscriminator(context, diffPackage.DynamicBricks.ToArray());
                await UpdateDiscriminator(context, diffPackage.ContentBricks.ToArray());
            }

            return snapshot;
        }

        public Task<DataPackage> CreateSnapshotAsync(DataPackage diffPackage)
        {
            return Task.Factory.StartNew(() =>
            {
                using (var context = GetContext())
                {
                    var result = new DataPackage
                    {
                        Description = $"Automatic snapshot for '{diffPackage.Description}'",
                    };

                    result.Authors.AddRange(GetEntities(context.Authors, diffPackage.Authors));
                    result.Addresses.AddRange(GetEntities(context.Addresses, diffPackage.Addresses));
                    result.BrickCategories.AddRange(GetEntities(context.BrickCategories, diffPackage.BrickCategories));
                    result.BrickGroups.AddRange(GetEntities(context.BrickGroups, diffPackage.BrickGroups));
                    result.BrickResources.AddRange(GetEntities(context.BrickResources, diffPackage.BrickResources));
                    result.BrickTexts.AddRange(GetEntities(context.BrickTexts, diffPackage.BrickTexts));
                    result.CharacterMappings.AddRange(GetEntities(context.CharacterMappings, diffPackage.CharacterMappings));
                    result.CommandBricks.AddRange(GetEntities(context.Bricks.OfType<CommandBrick>(), diffPackage.CommandBricks));
                    result.ContentBricks.AddRange(GetEntities(context.Bricks.OfType<ContentBrick>(), diffPackage.ContentBricks));
                    result.ColorSchemes.AddRange(GetEntities(context.ColorSchemes, diffPackage.ColorSchemes));
                    result.DocumentCategories.AddRange(GetEntities(context.DocumentCategories, diffPackage.DocumentCategories));
                    result.DocumentModels.AddRange(GetEntities(context.DocumentModels, diffPackage.DocumentModels));
                    result.DocumentModelLanguages.AddRange(GetEntities(context.DocumentModelLanguages, diffPackage.DocumentModelLanguages));
                    result.DocumentModelMetadataDefinitions.AddRange(GetEntities(context.DocumentModelMetadataDefinitions, diffPackage.DocumentModelMetadataDefinitions));
                    result.DocumentModelAuthorRoles.AddRange(GetEntities(context.DocumentModelAuthorRoles, diffPackage.DocumentModelAuthorRoles));
                    result.DocumentStructures.AddRange(GetEntities(context.DocumentStructure, diffPackage.DocumentStructures));
                    result.DynamicBricks.AddRange(GetEntities(context.Bricks.OfType<DynamicBrick>(), diffPackage.DynamicBricks));
                    result.JobAssignments.AddRange(GetEntities(context.JobAssignments, diffPackage.JobAssignments));
                    result.Languages.AddRange(GetEntities(context.Languages, diffPackage.Languages));
                    result.LocalisedResources.AddRange(GetEntities(context.LocalisedResources, diffPackage.LocalisedResources));
                    result.MetadataCategories.AddRange(GetEntities(context.MetadataCategories, diffPackage.MetadataCategories));
                    result.MetadataDefinitions.AddRange(GetEntities(context.MetadataDefinitions, diffPackage.MetadataDefinitions));
                    result.AuthorRoles.AddRange(GetEntities(context.AuthorRoles, diffPackage.AuthorRoles));
                    result.OrgaEntities.AddRange(GetEntities(context.OrgaEntities, diffPackage.OrgaEntities));
                    result.PersonNames.AddRange(GetEntities(context.PersonNames, diffPackage.PersonNames));
                    result.Resources.AddRange(GetEntities(context.Resources, diffPackage.Resources));
                    result.StyleShortcuts.AddRange(GetEntities(context.StyleShortcuts, diffPackage.StyleShortcuts));
                    result.PredefinedFunctions.AddRange(GetEntities(context.PredefinedFunctions, diffPackage.PredefinedFunctions));
                    result.Texts.AddRange(GetEntities(context.Texts, diffPackage.Texts));
                    result.Translations.AddRange(GetEntities(context.Translations, diffPackage.Translations));
                    result.SystemTexts.AddRange(GetEntities(context.SystemTexts, diffPackage.SystemTexts));
                    result.SystemResources.AddRange(GetEntities(context.SystemResources, diffPackage.SystemResources));
                    result.UserGroups.AddRange(GetEntities(context.UserGroups, diffPackage.UserGroups));
                    return result;
                }
            });
        }

        private IEnumerable<T> GetEntities<T>(IQueryable<T> table, IEnumerable<T> index)
            where T : Identifiable
        {
            var result = new List<T>();
            foreach (var entity in index)
            {
                result.AddRange(table.Where(e => e.Id == entity.Id).ToArray());
            }

            return result;
        }

        public async Task RestoreSnapshotAsync(DataPackage snapshot)
        {
            snapshot.SetModificationTime(DateTime.UtcNow);
            using (var context = GetContext())
            {
                AddOrUpdateBasicEurolookData(context, snapshot);
                await context.SaveChangesAsync();
            }
        }

        // *********************************************************************
        //                                                              CLEANUP
        // *********************************************************************
        public async Task<IEnumerable<OrgaEntity>> GetOldOrgaEntities()
        {
            using (var context = GetContext())
            {
                return await GetDeletableOrgaEntities(context);
            }
        }

        public async Task<IEnumerable<Text>> GetOrphanedTexts()
        {
            using (var context = GetContext())
            {
                return await GetDeletableTexts(context).ToListAsync();
            }
        }

        public async Task<int> GetDevicesCount()
        {
            await using var context = GetContext();
            var maxDate = DateTime.UtcNow.AddMonths(-12).ToDateType(DateTimeExtensions.DateGroupType.Day);
            return await context.DeviceSettings.Where(d => d.ServerModificationTimeUtc < maxDate).CountAsync();
        }

        public async Task CleanupDevices()
        {
            await using var context = GetContext();
            var maxDate = DateTime.UtcNow.AddMonths(-12).ToDateType(DateTimeExtensions.DateGroupType.Day);

            // Performance Logs with references to the deletable devices have to be updated
            await context.PerformanceLogs
                         .Include(nameof(PerformanceLog.DeviceSettings))
                         .Where(p => p.DeviceSettings.ServerModificationTimeUtc < maxDate)
                         .BatchUpdateAsync(
                             new PerformanceLog { DeviceSettingsId = null },
                             new List<string> { nameof(PerformanceLog.DeviceSettingsId) });

            // Batch delete old devices without loading them all to memory
            await context.DeviceSettings.Where(d => d.ServerModificationTimeUtc < maxDate).BatchDeleteAsync();
            await context.SaveChangesAsync();
        }

        public async Task CleanupTexts(List<Guid> textsToDelete)
        {
            using (var context = GetContext())
            {
                var oldTexts = (await GetDeletableTexts(context).ToListAsync())
                               .Where(text => textsToDelete.Contains(text.Id))
                               .ToList();
                var oldTranslations = oldTexts.SelectMany(text => text.Translations);
                context.Translations.RemoveRange(oldTranslations);
                context.Texts.RemoveRange(oldTexts);
                await context.SaveChangesAsync();
            }
        }

        public async Task CleanupOrgaEntities(List<Guid> entitiesToDelete)
        {
            using (var context = GetContext())
            {
                var oldEntities = (await GetDeletableOrgaEntities(context))
                                    .Where(orgaChart => entitiesToDelete.Contains(orgaChart.Id))
                                    .ToList();
                var oldTexts = oldEntities.Where(e => e.HeaderTextId.HasValue).Select(orgaChart => orgaChart.HeaderText);
                var oldTranslations = oldEntities.Where(e => e.HeaderTextId.HasValue).SelectMany(orgaChart => orgaChart.HeaderText.Translations);

                var entityIds = oldEntities.Select(entity => entity.Id).ToList();

                // Activity tracks with references to the deletable entities have to be updated
                var activityTracks = context.ActivityTracks.Where(aT => entityIds.Contains(aT.OrgaEntityId));

                // Authors may reference the deletable entities
                var authors = context.Authors.Where(a => a.OrgaEntityId.HasValue && entityIds.Contains(a.OrgaEntityId.Value));
                foreach (var author in authors)
                {
                    author.OrgaEntityId = null;
                    author.AdOrgaEntityId = null;
                }

                // Job Assignments may reference the deletable entities
                var jobAssignments = context.JobAssignments.Where(jA => jA.OrgaEntityId.HasValue && entityIds.Contains(jA.OrgaEntityId.Value));
                foreach (var jobAssignment in jobAssignments)
                {
                    jobAssignment.OrgaEntityId = null;
                }

                context.ActivityTracks.RemoveRange(activityTracks);
                context.JobAssignments.UpdateRange(jobAssignments);
                context.Translations.RemoveRange(oldTranslations);
                context.Texts.RemoveRange(oldTexts);
                context.OrgaEntities.RemoveRange(oldEntities);
                await context.SaveChangesAsync();
            }
        }

        private static async Task<IEnumerable<OrgaEntity>> GetDeletableOrgaEntities(EurolookContext context)
        {
            var maxDate = DateTime.UtcNow.AddMonths(-12).ToDateType(DateTimeExtensions.DateGroupType.Day);
            var entities = context.OrgaEntities.Where(e => e.Deleted && e.ServerModificationTimeUtc < maxDate);
            var withText = await entities.Include(e => e.HeaderText)
                                           .ThenInclude(t => t.Translations)
                                           .Where(e => e.HeaderText.ServerModificationTimeUtc < maxDate
                                                       && e.HeaderText.Translations.All(t => t.ServerModificationTimeUtc < maxDate))
                                           .ToListAsync();
            var result = await entities.Where(e => !e.HeaderTextId.HasValue).ToListAsync();
            result.AddRange(withText);

            return result.OrderBy(e => e.Name);
        }

        private static IQueryable<Text> GetDeletableTexts(EurolookContext context)
        {
            var maxDate = DateTime.UtcNow.AddMonths(-12).ToDateType(DateTimeExtensions.DateGroupType.Day);
            return context.Texts.Where(t => t.Deleted && t.ServerModificationTimeUtc < maxDate)
                          .Where(text => !context.BrickTexts.Any(bT => bT.TextId == text.Id)
                                            && !context.OrgaEntities.Any(oE => oE.HeaderTextId == text.Id)
                                            && !context.PredefinedFunctions.Any(pF => pF.FunctionTextId == text.Id || pF.FunctionFemaleTextId == text.Id || pF.FunctionHeaderTextId == text.Id || pF.FunctionHeaderFemaleTextId == text.Id)
                                            && !context.Addresses.Any(a => a.NameTextId == text.Id || a.LocationTextId == text.Id || a.FooterTextId == text.Id)
                                            && !context.SystemTexts.Any(t => t.TextId == text.Id))
                          .Include(text => text.Translations)
                          .Where(text => text.Translations.All(t => t.Deleted && t.ServerModificationTimeUtc < maxDate));
        }

        public async Task LoadCharacterMappingsForPackage(DataPackage dataPackage)
        {
            using (var context = GetContext())
            {
                dataPackage.CharacterMappings = await context.CharacterMappings.ToListAsync();
            }
        }

        public async Task LoadColorSchemesForPackage(DataPackage dataPackage)
        {
            using (var context = GetContext())
            {
                dataPackage.ColorSchemes = await context.ColorSchemes.ToListAsync();
            }
        }

        public async Task LoadStyleShortcutsForPackage(DataPackage dataPackage)
        {
            using (var context = GetContext())
            {
                dataPackage.StyleShortcuts = await context.StyleShortcuts.ToListAsync();
            }
        }

        public async Task LoadUserGroupsForPackage(DataPackage dataPackage)
        {
            using (var context = GetContext())
            {
                dataPackage.UserGroups = await context.UserGroups.ToListAsync();
                dataPackage.UserGroups.ForEach(g => g.AdGroups = null);
            }
        }

        public async Task LoadAddressesForPackage(DataPackage dataPackage)
        {
            using (var context = GetContext())
            {
                dataPackage.Addresses = await context.Addresses.ToListAsync();
                var textIds = dataPackage.Addresses.Where(x => x.NameTextId.HasValue).Select(x => x.NameTextId).ToList();
                textIds.AddRange(dataPackage.Addresses.Where(x => x.LocationTextId.HasValue).Select(x => x.LocationTextId));
                textIds.AddRange(dataPackage.Addresses.Where(x => x.FooterTextId.HasValue).Select(x => x.FooterTextId));
                AddIfNotContains(context.Texts.Where(x => textIds.Contains(x.Id)), dataPackage.Texts);
                AddIfNotContains(context.Translations.Where(x => textIds.Contains(x.TextId)), dataPackage.Translations);
            }
        }

        private static void AddIfNotContains<T>(IQueryable<T> source, List<T> target)
            where T : Identifiable
        {
            foreach (var item in source)
            {
                if (target.All(x => x.Id != item.Id))
                {
                    target.Add(item);
                }
            }
        }

        public async Task LoadPredefinedFunctionsForPackage(DataPackage dataPackage)
        {
            using (var context = GetContext())
            {
                dataPackage.PredefinedFunctions = await context.PredefinedFunctions.ToListAsync();
                var textIds = dataPackage.PredefinedFunctions.Where(x => x.FunctionTextId.HasValue).Select(x => x.FunctionTextId).ToList();
                textIds.AddRange(dataPackage.PredefinedFunctions.Where(x => x.FunctionFemaleTextId.HasValue).Select(x => x.FunctionFemaleTextId));
                textIds.AddRange(dataPackage.PredefinedFunctions.Where(x => x.FunctionHeaderTextId.HasValue).Select(x => x.FunctionHeaderTextId));
                textIds.AddRange(dataPackage.PredefinedFunctions.Where(x => x.FunctionHeaderFemaleTextId.HasValue).Select(x => x.FunctionHeaderFemaleTextId));
                AddIfNotContains(context.Texts.Where(x => textIds.Contains(x.Id)), dataPackage.Texts);
                AddIfNotContains(context.Translations.Where(x => textIds.Contains(x.TextId)), dataPackage.Translations);
            }
        }

        public async Task LoadSystemTextsAndResourcesForPackage(DataPackage dataPackage)
        {
            using (var context = GetContext())
            {
                dataPackage.SystemTexts = await context.SystemTexts.ToListAsync();
                var textIds = dataPackage.SystemTexts.Where(x => x.TextId.HasValue).Select(x => x.TextId).ToList();
                AddIfNotContains(context.Texts.Where(x => textIds.Contains(x.Id)), dataPackage.Texts);
                AddIfNotContains(context.Translations.Where(x => textIds.Contains(x.TextId)), dataPackage.Translations);

                dataPackage.SystemResources = await context.SystemResources.ToListAsync();
                var resourceIds = dataPackage.SystemResources.Where(x => x.ResourceId.HasValue).Select(x => x.ResourceId).ToList();
                AddIfNotContains(context.Resources.Where(x => resourceIds.Contains(x.Id)), dataPackage.Resources);
                AddIfNotContains(context.LocalisedResources.Where(x => resourceIds.Contains(x.ResourceId)), dataPackage.LocalisedResources);
            }
        }
    }
}
