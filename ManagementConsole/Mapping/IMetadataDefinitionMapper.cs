﻿using Eurolook.Data.Models.Metadata;
using Eurolook.Data.Models.SharePointTermStore;
using Eurolook.ManagementConsole.ViewModels.DocumentModels.MetadataDefinitions;

namespace Eurolook.ManagementConsole.Mapping
{
    public interface IMetadataDefinitionMapper
    {
        MetadataDefinitionSelectionViewModel ToMetadataDefinitionSelectionViewModel(
            TermStore termStore,
            MetadataDefinition metadataDefinition,
            int languageCode = 1033);
    }
}
