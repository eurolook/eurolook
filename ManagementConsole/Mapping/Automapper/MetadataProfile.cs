using AutoMapper;
using Eurolook.Data.Models.Metadata;
using Eurolook.Data.Models.SharePointTermStore;
using Eurolook.ManagementConsole.Areas.Metadata.Models;
using Eurolook.ManagementConsole.ViewModels.DocumentModels.MetadataDefinitions;

namespace Eurolook.ManagementConsole.Mapping.Automapper
{
    public class MetadataProfile : Profile
    {
        public MetadataProfile()
        {
            CreateMap<MetadataCategoryEditDataModel, MetadataCategory>();
            CreateMap<MetadataCategory, MetadataCategoryEditDataModel>();
            CreateMap<MetadataDefinitionViewModel, MetadataDefinition>();
            CreateMap<MetadataDefinitionEditViewModel, MetadataDefinition>().ReverseMap();
            CreateMap<TermSet, TermSetSelectionViewModel>();
            CreateMap<MetadataDefinition, MetadataDefinitionSelectionViewModel>();

            CreateMap<DocumentModelMetadataDefinition, AssociatedMetadataDefinitionViewModel>().ReverseMap();
        }
    }
}
