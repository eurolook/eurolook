using AutoMapper;
using Eurolook.Data.Models.AuthorRoles;
using Eurolook.ManagementConsole.Models;

namespace Eurolook.ManagementConsole.Mapping.Automapper
{
    public class AuthorRolesProfile : Profile
    {
        public AuthorRolesProfile()
        {
            CreateMap<AuthorRole, AuthorRoleDataModel>().ReverseMap();
        }
    }
}
