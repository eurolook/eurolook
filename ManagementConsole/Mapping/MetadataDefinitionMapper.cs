using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Eurolook.Data.Models.Metadata;
using Eurolook.Data.Models.SharePointTermStore;
using Eurolook.ManagementConsole.ViewModels.DocumentModels.MetadataDefinitions;

namespace Eurolook.ManagementConsole.Mapping
{
    public class MetadataDefinitionMapper : IMetadataDefinitionMapper
    {
        private readonly IMapper _mapper;

        public MetadataDefinitionMapper(IMapper mapper)
        {
            _mapper = mapper;
        }

        public MetadataDefinitionSelectionViewModel ToMetadataDefinitionSelectionViewModel(
            TermStore termStore,
            MetadataDefinition metadataDefinition,
            int languageCode = 1033)
        {
            var metadataDefinitionSelectionViewModel = _mapper.Map<MetadataDefinitionSelectionViewModel>(metadataDefinition);

            if (metadataDefinition.IsBoundToTermSet())
            {
                metadataDefinitionSelectionViewModel.DefaultValues = GetDefaultValues(
                    termStore,
                    metadataDefinition.TermSetId.Value,
                    languageCode);
            }

            return metadataDefinitionSelectionViewModel;
        }

        private List<DefaultValueViewModel> GetDefaultValues(
            TermStore termStore,
            Guid termSetId,
            int languageCode)
        {
            Func<TermLabel, bool> isDefaultForLanguage = l => l.IsDefaultForLanguage && (l.LanguageId == languageCode);

            return termStore?.GetTermsFlat(termSetId)
                            .Select(
                                t =>
                                    new DefaultValueViewModel
                                    {
                                        SharePointId = t.SharePointId,
                                        Value = t.Labels.FirstOrDefault(isDefaultForLanguage)?.Value,
                                    })
                            .OrderBy(dv => dv.Value)
                            .ToList()
                   ?? new List<DefaultValueViewModel>();
        }
    }
}
