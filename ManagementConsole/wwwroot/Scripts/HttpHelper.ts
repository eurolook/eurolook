function getFetchParams(method: string): RequestInit {
    return {
        method: method,
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        headers: { "Access-Control-Allow-Credential": "true" },
        referrerPolicy: 'no-referrer', // no-referrer, *client
        credentials: 'include',
    }
}

function getParamsForJsonBody(method: string, body?: any): RequestInit {
    const params = getFetchParams(method);
    
    if (body) {
        const newHeaders = new Headers(params.headers);
        newHeaders.append('Content-Type', 'application/json');
        params.headers = newHeaders;
        params.body = JSON.stringify(body);
    }

    return params;
}

function getParamsForFormBody(method: string, body: any): RequestInit {
    const params = getFetchParams(method);
    params.body = body;
    
    return params;
}

async function handleErrors(response: Response) {
    if (!response.ok) {
        const responseMessage = await response.text();
        const errorMessage = `${response.statusText}${responseMessage ? ': ' + responseMessage : ''}`;
        console.error(errorMessage);
        throw Error(errorMessage);
    }
    return response;
}


export function get(url: string): Promise<Response> {
    return fetch(url, getFetchParams('GET')).then(handleErrors);
}

export function put(url: string, body?: any): Promise<Response> {
    return fetch(url, getParamsForJsonBody('PUT', body)).then(handleErrors);
}

export function post(url: string, body?: any): Promise<Response> {
    return fetch(url, getParamsForJsonBody('POST', body)).then(handleErrors);
}

export function postForm(url: string, body: any): Promise<Response> {
    return fetch(url, getParamsForFormBody('POST', body)).then(handleErrors);
}

export function del(url: string): Promise<Response> {
    return fetch(url, getFetchParams('DELETE')).then(handleErrors);
}

