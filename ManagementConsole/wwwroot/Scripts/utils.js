function hasParameters() {
    return location.search.length > 0;
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    var result = results === null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
    if (!result || result === "undefined") {
        return undefined;
    } else {
        return result;
    }
}

function getBaseUrl() {
    if (!window.location.origin) {
        return window.location.protocol + "//" + window.location.host;
    }
    else {
        return window.location.origin;
    }
}

function showAlert(props) {
    var timeout = 15000;
    if (Number.isInteger(props.timeout)) {
        timeout = props.timeout;
    }
    var alert = '<div data-alert class="alert-box ' + props.type + '" style="position:fixed; margin-top:-49px; margin-left: 280px; z-index:100;">' +
                '<span style="margin-right:1em;">' + props.message + '</span>' +
                '<a href="#" class="close">&times;</a>' +
                '</div>';

    var parent = 'section[role="main"]';
    $(parent).children('.alert-box').remove();
    $(parent).prepend(alert).foundation();

    // fix dropdown panels after inserting the alert box
    $(document).foundation('dropdown', 'off');
    $(document).foundation('dropdown');

    if (timeout > 0) {
        setTimeout(function () {
            $('.alert-box.' + props.type + ' .close').click();
        }, timeout);
    }
}

function getDateForFilename(date) {
    var month = parseInt(date.getMonth()) + 1;
    return "" +
        date.getFullYear() + "-" +
        month + "-" +
        date.getDate() + "--" +
        date.getHours() + "-" +
        date.getMinutes();
}

function createGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8); // NOSONAR
        return v.toString(16);
    });
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function bindCtrlSave(saveFunction) {
    $(window).bind('keydown', function (event) {
        if (event.ctrlKey || event.metaKey) {
            switch (String.fromCharCode(event.which).toLowerCase()) {
                case 's':
                    event.preventDefault();
                    var activeElement = document.activeElement;
                    activeElement.blur();
                    window.setTimeout(function() {
                        saveFunction();
                        activeElement.focus();
                    }, 300);
                    break;
            }
        }
    });
}