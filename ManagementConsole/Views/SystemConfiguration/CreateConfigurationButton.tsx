import * as React from 'react'
import { post } from '../../wwwroot/Scripts/HttpHelper';

import { toast } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';
import './modal.css';
import { ISystemConfiguration } from './Types';

export const CreateButton = ( { baseUrl, isReadOnlyMode, configurations, loadConfigurations } ) => {

    const createSystemConfiguration = async (configName) => {
        if (configurations.some(c => c.key == configName)) {
            toast.warning(`System configuration ${configName} already exists.`);
        }
        else {
            var config = {} as ISystemConfiguration;
            config.key = configName;
            try {
                const res = await post(`${baseUrl}/SystemConfiguration/Create`, config);
                const data = await res.json();
                toast.success(`Created system configuration: ${data.model.key}`);
                loadConfigurations();
            } catch (e) {
                toast.error(`Failed to create system configuration. ${e.message}`);
            }
        }
    }

    const [isOpen, setIsOpen] = React.useState(false);
    const [input, setInput] = React.useState("");

    const toggle = () => {
        setIsOpen(!isOpen);
    }

    const createAction = async () => {
        await createSystemConfiguration(input);
        toggle();
    }

    const Modal = ( ) => {
        const showHideClassName = isOpen ? "modal display-block" : "modal display-none";

        return (
            <div className={showHideClassName}>
                <section className="modal-main">
                    <div id="CreateModal">
                        <h3>New System Configuration</h3>
                        <input type="text"
                            key="inputNewSystemConfig"
                            placeholder="Name of the System Configuration"
                            defaultValue={input}
                            onChange={e => setInput(e.target.value)} />

                        <div className="button-bar">
                            <button className="button small" disabled={!input} onClick={createAction}>Submit</button>
                            <a onClick={toggle} className="button small secondary">Cancel</a>
                        </div>
                    </div>
                </section>
            </div>
        );
    };

    return (
        <React.Fragment>
            <button className="secondary inline icon-left"
                onClick={toggle}
                disabled={isReadOnlyMode}
                style={{ marginLeft: 4 }}>
                <i className="fi-plus" aria-hidden="true"></i>&nbsp;New System Configuration</button>
            { Modal() }
        </React.Fragment>
    );
};
