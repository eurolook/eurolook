import * as React from 'react'
import { del } from '../../wwwroot/Scripts/HttpHelper';

import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './modal.css';

export const DeleteButton = ({ baseUrl, id, configName, loadConfigurations }) => {

    const [isOpen, setIsOpen] = React.useState(false);

    const deleteSystemConfig = async (id, baseUrl) => {
        try {
            await del(`${baseUrl}/SystemConfiguration/Delete/${id}`);
            toast.success("Successfully deleted system configuration.");
            loadConfigurations();
        } catch (e) {
            toast.error(`Failed to delete system configuration. ${e.message}`);
        }
    }

    const openModal = () => {
        setIsOpen(true);
    }

    const closeModal = () => {
        setIsOpen(false);
    }

    const deleteAction = async () => {
        await deleteSystemConfig(id, baseUrl);
        closeModal();
    }

    const Modal = () => {
        const showHideClassName = isOpen ? "modal display-block" : "modal display-none";

        return (
            <div className={showHideClassName}>
                <section className="modal-main">
                    <div id="DeleteModal">
                        <h3>Delete <span>{configName}</span></h3>

                        <p>Do you really want to delete the system configuration?</p>

                        <div className="button-bar">
                            <input type="submit" value="Delete" className="button small" onClick={deleteAction} />
                            <a onClick={closeModal} className="button small secondary">Cancel</a>
                        </div>
                    </div>
                </section>
            </div>
        );
    };

    return (
        <React.Fragment>
            <a onClick={openModal}><i className="fi-x" aria-hidden="true"></i> Delete</a>
            <Modal/>
        </React.Fragment>
    );
};
