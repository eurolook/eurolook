import * as React from 'react'
import * as ReactDOM from 'react-dom';
import { ConfigurationEntry } from './ConfigurationEntry';
import { get, post, put } from '../../wwwroot/Scripts/HttpHelper';
import { ISystemConfiguration } from './Types'
import { CreateButton } from './CreateConfigurationButton'

import { toast, ToastContainer, Slide } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

const App = (props: {baseUrl :string, isReadOnlyMode: string}) => {

    const { baseUrl, isReadOnlyMode } = props;
    const isReadOnly = isReadOnlyMode.toLowerCase() === "true";

    const [isLoading, setLoading] = React.useState(true);
    const [configurations, setConfigurations] = React.useState<ISystemConfiguration[]>([]);
    const [checked, setChecked] = React.useState(true);

    const fetchData = async () => {
        setLoading(true);
        const response = await get(`${baseUrl}/SystemConfiguration/List`);
        const configurations = await (response.json() as Promise<ISystemConfiguration[]>);
        setConfigurations(configurations);
        setLoading(false);
    }

    React.useEffect(() => {
        fetchData();
    }, []);

    const handleChange = (configuration: ISystemConfiguration) => {
        const index = configurations.findIndex(c => c.key === configuration.key);
        const newConfigurations = [...configurations.filter((c, i) => i < index), configuration, ...configurations.filter((c, i) => i > index)];
        setConfigurations(newConfigurations);
    }

    const changeSelection = () => {
        setChecked(!checked);
        const newConfigurations = configurations;
        newConfigurations.forEach(conf => conf.isExcludedFromExport = checked);
        setConfigurations(newConfigurations);
    }

    const isAnyConfigurationSelected = configurations.find(conf => !conf.isExcludedFromExport) == null;
   
    const exportConfigurations = async () => {
        try {
            await post(`${baseUrl}/SystemConfiguration/Export`, configurations.filter(conf => !conf.isExcludedFromExport))
                .then(res => res.json())
                .then(data => {
                    window.location.href = `${baseUrl}/DataExchange/Export/Package/${data.model.packageId}`;
                });
        } catch (e) {
            toast.error(`Failed to export system configuration. ${e.message}`);
        }
    }

    const exportButton =
        <button className="secondary inline icon-left"
                disabled={isAnyConfigurationSelected}
                onClick={exportConfigurations}
                style={{ marginLeft: 4 }}>
            <i className="fi-download" aria-hidden="true"></i>&nbsp;Export System Configuration
        </button>;


    const buttonGroup = <div className="row ela-row">
                    <div className="columns">
                        <div className="button-bar">
                            <ul className="button-group">
                                <li>
                                    <span className="button secondary inline">
                                        <input type="checkbox" checked={checked} onChange={changeSelection} />
                                    </span>
                                </li>
                                <li>
                                    {exportButton}
                                </li>
                                <li>
                                    <CreateButton baseUrl={baseUrl} isReadOnlyMode={isReadOnly} configurations={configurations} loadConfigurations={fetchData} />
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

    const saveChanges = async () => {
        try {
            await put(`${baseUrl}/SystemConfiguration/Update`, configurations);
            toast.success("Successfully saved system configuration.");
        } catch (e) {
            toast.error(`Failed to save system configuration. ${e.message}`);
        }
    }

    const saveButton = <div className="row ela-row">
                           <div className="medium-2 large-2 columns">&nbsp;</div>
                           <div className="medium-10 large-10 column" style={{ paddingLeft: 0 }}>
                               <div className="button-bar left">
                                   <a className="button small" onClick={saveChanges}>Save</a>
                               </div>
                           </div>
    </div>;

    return (
        <div id="root">
            <div className="row ela-row ela-content-header">
                <div className="columns">
                    <h2>System Configuration</h2>
                </div>
            </div>
            { configurations.length > 0 && buttonGroup }
            {configurations.length > 0 &&
                configurations.map(
                    c => <ConfigurationEntry key={c.key} configuration={c} handleChange={handleChange} baseUrl={baseUrl
} loadConfigurations={fetchData}/>) }
            { configurations.length > 0 && saveButton }
            { !isLoading &&
                configurations.length === 0 &&
                <div className="row ela-row">
                    <div className="medium-3 large-3 columns">No system configurations found.</div>
                </div>
            }

            <ToastContainer
                toastClassName='toast-container'
                bodyClassName='toast-container-body'
                autoClose={5000}
                hideProgressBar
                newestOnTop={false}
                transition={Slide}
                closeOnClick
                rtl={false}
                draggable={false}
                pauseOnHover
                style={{ width: "90%" }} />

        </div>
    );
}

const root = document.getElementById('root');
const baseUrl = root.getAttribute('data-baseUrl');
const isReadOnlyMode = root.getAttribute('data-isReadOnlyMode');
ReactDOM.render(<App baseUrl={baseUrl} isReadOnlyMode={isReadOnlyMode} />, root);
