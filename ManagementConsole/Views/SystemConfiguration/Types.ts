export type ISystemConfiguration = {
    id: string,
    key: string,
    value: string,
    isExcludedFromExport: boolean,
};
