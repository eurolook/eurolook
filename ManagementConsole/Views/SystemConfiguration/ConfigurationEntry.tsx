import * as React from 'react';

import * as monaco from "monaco-editor";
import Editor, { loader } from "@monaco-editor/react";
loader.config({ monaco });
import { ISystemConfiguration } from './Types'

import { DeleteButton } from './DeleteConfigurationButton'


// @ts-ignore
self.MonacoEnvironment = {
    getWorkerUrl: function (moduleId, label) {
        if (label === 'json') {
            return '../../dist/json.worker.bundle.js';
        }
        return '../../dist/editor.worker.bundle.js';
    }
};

export const ConfigurationEntry = (props: { configuration: ISystemConfiguration, handleChange: ((newEntry: ISystemConfiguration) => void), baseUrl: String, loadConfigurations: () => void }) => {

    const { configuration, handleChange, baseUrl, loadConfigurations } = props;

    const onChange = (newValue: string) => {
        handleChange({ ...configuration, value: newValue });
    }

    const onCheckboxChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        handleChange({ ...configuration, isExcludedFromExport: !event.target.checked});
    }

    monaco.languages.json.jsonDefaults.setDiagnosticsOptions({
        validate: true,
        allowComments: true,
        schemaValidation: 'error',
    });

    const editorOptions = {
        theme: "vs",
        enableSplitViewResizing: false,
        value: configuration.value,
        language: 'json',

        automaticLayout: true,
        scrollBeyondLastLine: false,
        scrollbar: {
            useShadows: false,
            verticalHasArrows: true,
            horizontalHasArrows: true,
        },
        minimap: {
            enabled: false
        },
        overviewRulerBorder: false
    };

    const numberOfLines = configuration.value.split("\n").length;

    return (
        <div className="row ela-row">
            <div className="medium-2 large-2 columns" style={{ paddingRight: 0 }}>
                <h5>{configuration.key}</h5>
                <input type="checkbox" id={`input-${configuration.id}`} checked={!configuration.isExcludedFromExport} onChange={onCheckboxChange} />
                <label htmlFor={`input-${configuration.id}`}>Included in Export</label>
                <DeleteButton baseUrl={baseUrl} id={configuration.id} configName={configuration.key} loadConfigurations={loadConfigurations} />
            </div>
            <div className="monacoEditor editorResize vertical medium-10 large-10 columns" style={{ paddingLeft: 0, height: `${numberOfLines * 20}px`, minHeight: `${numberOfLines*20}px` }}>
                <Editor
                    defaultLanguage="json"
                    defaultValue={configuration.value}
                    options={editorOptions}
                    onChange={onChange} />
            </div>
            <hr
                style={{
                    color: '#008CBA',
                    height: '3px',
                }}
            />
        </div>
    );
};
