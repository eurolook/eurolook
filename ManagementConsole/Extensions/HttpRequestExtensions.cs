﻿using System;
using Microsoft.AspNetCore.Http;

namespace Eurolook.ManagementConsole.Extensions;

public static class HttpRequestExtensions
{
    public static bool IsAjaxRequest(this HttpRequest request)
    {
        if (request == null)
        {
            throw new ArgumentNullException(nameof(request));
        }

        if (request.Headers != null)
        {
            return !string.IsNullOrEmpty(request.Headers["X-Requested-With"]) &&
                   string.Equals(
                       request.Headers["X-Requested-With"],
                       "XmlHttpRequest",
                       StringComparison.OrdinalIgnoreCase);
        }

        return false;
    }
}
