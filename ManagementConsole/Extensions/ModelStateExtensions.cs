using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Eurolook.ManagementConsole.Extensions
{
    public static class ModelStateExtensions
    {
        public static string GetErrorMessages(this ModelStateDictionary modelState)
        {
            var message = new StringBuilder();
            foreach (var errProp in modelState.Select(m => m.Value.Errors).Where(e => e.Count > 0))
            {
                foreach (string errMessage in errProp.Select(e => e.ErrorMessage))
                {
                    message.AppendLine($"{errMessage} ");
                }
            }

            return message.ToString();
        }
    }
}
