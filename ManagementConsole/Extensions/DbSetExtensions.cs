using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Eurolook.ManagementConsole.Extensions
{
    public static class DbSetExtensions
    {
        public static async Task<T> CreateOrUpdateAsync<T>(this DbSet<T> dbSet, T model, EurolookServerContext context, Expression<Func<T, bool>> existingPredicate)
            where T : Updatable
        {
            var existingModel = await dbSet.FirstOrDefaultAsync(existingPredicate);
            if (existingModel == null)
            {
                model.Init();
                model = dbSet.Add(model).Entity;
            }
            else
            {
                model.Id = existingModel.Id;
                context.Entry(existingModel).CurrentValues.SetValues(model);
                existingModel.SetDeletedFlag(false);
            }

            await context.SaveChangesAsync();
            return model;
        }
    }
}
