﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Eurolook.ManagementConsole.Extensions
{
    public static class RazorExtensions
    {
        public static string ToJson(this object obj, bool prettyPrint = false)
        {
            var serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                Formatting = prettyPrint ? Formatting.Indented : Formatting.None,
            };
            return JsonConvert.SerializeObject(obj, serializerSettings);
        }
    }
}
