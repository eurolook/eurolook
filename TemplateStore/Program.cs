﻿using System;
using System.Linq;
using Eurolook.Common.Extensions;
using Eurolook.Data;
using Eurolook.Data.Database;
using Eurolook.Data.TemplateStore;
using Eurolook.DocumentProcessing;
using Eurolook.Web.Common;
using Eurolook.Web.Common.EuLogin;
using Eurolook.Web.Common.ScheduledTask;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Server.IISIntegration;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.FeatureManagement;
using Serilog;
using TemplateStore;
using TemplateStore.Auth;
using TemplateStore.Authorization;
using TemplateStore.Preview;
using TemplateStore.Templates;
using TemplateStore.Users;

try
{
    var builder = WebApplication.CreateBuilder(args);
    builder.AddDefaultConfigurationProviders();
    builder.Host.UseSerilog();

    builder.AddMigrationCommandLineOption();

    // Add services to the container.

    builder.Services.AddFeatureManagement();

    builder.Services.AddControllers();
    builder.Services.AddResponseCaching();
    string serverDatabaseConnectionString = builder.Configuration.GetConnectionString("ServerDatabaseConnection");

    builder.Services.AddScoped<ITemplateDatabase, TemplateDatabase>();
    builder.Services.AddScoped<IUserRepository, UserRepository>();
    builder.Services.AddScoped<IOrgaEntitiesDatabase, OrgaEntitiesDatabase>();
    builder.Services.AddScoped<Func<EurolookContext>>(_ => () => new EurolookServerContext(serverDatabaseConnectionString, ignoreDeletedFlags: true));
    builder.Services.AddScoped<IUserGroupRepository, UserGroupRepository>();
    builder.Services.AddScoped<IClaimsTransformation, ClaimsTransformation>();
    builder.Services.AddScoped<IAuthorizationHandler, TemplateAuthorizationHandler>();
    builder.Services.AddScoped<IUserRoleService, UserRoleService>();
    builder.Services.AddScoped<IAuthorManager, AuthorManager>();
    builder.Services.AddScoped<ITemplateAnalyzer, TemplateAnalyzer>();
    builder.Services.AddScoped<IEurolookUserService, EurolookUserService>();
    builder.Services.AddScoped<ISharedTemplateRepository, SharedTemplateRepository>();
    builder.Services.AddScoped<IRecentTemplateRepository, RecentTemplateRepository>();
    builder.Services.AddScoped<ISharedTemplateService, SharedTemplateService>();
    builder.Services.AddScoped<ITemplateService, TemplateService>();
    builder.Services.AddHttpClient<IPreviewService, PreviewService>();

    bool useEuLogin = builder.Configuration["FeatureManagement:EuLogin"].ToBool();

    builder.Services.AddAuthentication(IISDefaults.AuthenticationScheme);

    // FIXME: Move back to one authentication scheme once clients have been installed: EULogin (CAS) or Windows
    if (useEuLogin)
    {
        builder.AddEuLoginAuthentication();
    }

    // FIXME: Move back to one authentication scheme once clients have been installed: EULogin (CAS) or Windows
    builder.Services.AddAuthorization(options =>
    {
        // Configure the default policy
        options.DefaultPolicy = new AuthorizationPolicyBuilder(useEuLogin ? EuLoginAuthentication.AuthenticationScheme : IISDefaults.AuthenticationScheme)
                                .RequireAuthenticatedUser()
                                .RequireClaim(AccessClaim.Type, AccessClaim.Admin, AccessClaim.User)
                                .Build();

        options.AddPolicy(IISDefaults.AuthenticationScheme, new AuthorizationPolicyBuilder(IISDefaults.AuthenticationScheme)
                                .RequireAuthenticatedUser()
                                .RequireClaim(AccessClaim.Type, AccessClaim.Admin, AccessClaim.User)
                                .Build());

        options.FallbackPolicy = options.DefaultPolicy;
    });

    builder.Services.AddHealthChecks()
           .AddSqlServer(builder.Configuration.GetConnectionString("ServerDatabaseConnection"))
           .AddSystemInfo(builder);

    // In production, the React files will be served from this directory
    builder.Services.AddSpaStaticFiles(config =>
    {
        config.RootPath = "ClientApp/build";
    });

    if (builder.Configuration["FeatureManagement:KeepAliveTask"].ToBool())
    {
        builder.Services.AddSingleton<IScheduledTask, KeepAliveTask>();
    }

    var app = builder.Build();
    app.UseHttpsRedirection();

    if (app.Environment.IsLocalDevelopment())
    {
        // app.UseDeveloperExceptionPage();
    }
    else
    {
        app.UseExceptionHandler("/Error");
        // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
        app.UseHsts();
    }

    app.CreateAndSetupLogger(typeof(Program).Assembly);

    app.UseStaticFiles();
    app.UseSpaStaticFiles(new StaticFileOptions
    {
        OnPrepareResponse = ctx =>
        {
            if (ctx.Context.Request.Path.StartsWithSegments("/static/media"))
            {
                ctx.Context.Response.Headers.Append("Cache-Control", $"public, max-age={CacheSettings.OneMonthDuration}");
            }
        }
    });

    app.UseRouting();
    app.UseResponseCaching();
    app.UseAuthentication();
    app.UseAuthorization();

    app.UseEndpoints(endpoints =>
    {
        endpoints.MapDefaultControllerRoute();
        endpoints.MapHealthChecksWithAnonymousAccess("/health");
    });

    app.UseSpa(spa =>
    {
        spa.Options.SourcePath = "ClientApp";

        if (app.Environment.IsLocalDevelopment())
        {
            /*** NOTE: If behind proxy, add localhost to the NO_PROXY environment variable for this to work ***/
            spa.UseReactDevelopmentServer(npmScript: "start");
        }
    });

    app.StartRegisteredScheduledTasks();
    app.Run();
}
catch (Exception ex)
{
    Log.Fatal(ex, "Host terminated unexpectedly");
}
finally
{
    Log.Information("Shutdown complete");
    Log.CloseAndFlush();
}
