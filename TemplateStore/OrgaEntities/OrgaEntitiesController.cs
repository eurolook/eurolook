﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Eurolook.Web.Common;
using TemplateStore.Users;
using Eurolook.Data;
using Eurolook.Data.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace TemplateStore.Metadata
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrgaEntitiesController : ControllerBase
    {
        private readonly IOrgaEntitiesDatabase _orgaEntitiesDatabase;

        public OrgaEntitiesController(IOrgaEntitiesDatabase orgaEntitiesDatabase)
        {
            _orgaEntitiesDatabase = orgaEntitiesDatabase;
        }

        public async Task<ActionResult<List<OrgaEntityDataModel>>> GetOrgaEntities([FromQuery(Name = "q")] string query)
        {
            var paginationInfo = new PaginationInfo()
            {
                PageSize = 15
            };

            var orgaEntities = await _orgaEntitiesDatabase.GetOrgaEntities(query, paginationInfo);
            return orgaEntities.Select(o => new OrgaEntityDataModel(o)).ToList();
        }
    }
}
