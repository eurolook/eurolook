﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.Extensions;
using Eurolook.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace TemplateStore.Users
{
    public interface IOrgaEntitiesDatabase
    {
        Task<List<OrgaEntity>> GetOrgaEntities(string q, PaginationInfo paginationInfo);

        Task<OrgaEntity[]> GetOrgaEntitiesIncludingDeleted(Guid[] entityIds);
    }

    public class OrgaEntitiesDatabase : IOrgaEntitiesDatabase
    {
        public Func<EurolookContext> GetContext { get; }

        public OrgaEntitiesDatabase(Func<EurolookContext> eurolookContextFunc)
        {
            GetContext = eurolookContextFunc;
        }

        public async Task<List<OrgaEntity>> GetOrgaEntities(string query, PaginationInfo paginationInfo)
        {
            if (string.IsNullOrWhiteSpace(query))
            {
                return new List<OrgaEntity>();
            }

            await using var context = GetContext();
            return await context.OrgaEntities
                .Where(o => o.Function == null && o.Name.StartsWith(query))
                .OrderBy(o => o.LogicalLevel)
                .ThenBy(o => o.Name)
                .Paginate(paginationInfo)
                .ToListAsync();
        }

        public async Task<OrgaEntity[]> GetOrgaEntitiesIncludingDeleted(Guid[] entityIds)
        {
            await using var context = GetContext();
            return await context.OrgaEntities
                .IgnoreQueryFilters()
                .Where(o => entityIds.Contains(o.Id))
                .ToArrayAsync();
        }
    }
}
