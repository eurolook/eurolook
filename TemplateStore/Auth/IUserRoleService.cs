﻿using System.Security.Claims;
using System.Threading.Tasks;
using Eurolook.Data.Models.TemplateStore;

namespace TemplateStore.Auth
{
    public interface IUserRoleService
    {
        bool IsAdmin(ClaimsPrincipal user);

        Task<bool> IsAdminOrOwner(ClaimsPrincipal user, Template template);
    }
}
