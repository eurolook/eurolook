﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Eurolook.Data.Models.TemplateStore;
using TemplateStore.Auth;
using TemplateStore.Users;

namespace TemplateStore.Authorization
{
    public class UserRoleService : IUserRoleService
    {
        private readonly IUserRepository _userRepository;

        public UserRoleService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<bool> IsAdminOrOwner(ClaimsPrincipal user, Template template)
        {
            return IsAdmin(user) || await IsOwner(user, template);
        }

        public bool IsAdmin(ClaimsPrincipal user)
        {
            return user.HasClaim(AccessClaim.Type, AccessClaim.Admin);
        }

        private async Task<bool> IsOwner(ClaimsPrincipal user, Template template)
        {
            string identity = user.Identity.Name ?? "";
            var elUser = await _userRepository.GetUser(identity);
            return elUser != null && template.Owners.Select(o => o.UserId).Contains(elUser.Id);
        }
    }
}
