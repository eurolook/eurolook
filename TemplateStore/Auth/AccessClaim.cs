﻿namespace TemplateStore.Auth
{
    public static class AccessClaim
    {
        public static string Type = "Access";
        public static string Admin = "Admin";
        public static string User = "User";
    }
}
