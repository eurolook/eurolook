﻿using System.Threading.Tasks;
using Eurolook.Data.Models.TemplateStore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace TemplateStore.Auth
{
    public class TemplateAuthorizationHandler : AuthorizationHandler<OperationAuthorizationRequirement, Template>
    {
        private readonly IUserRoleService _userRoleService;

        public TemplateAuthorizationHandler(IUserRoleService userRoleService)
        {
            _userRoleService = userRoleService;
        }

        protected override async Task HandleRequirementAsync(
            AuthorizationHandlerContext context,
            OperationAuthorizationRequirement operation,
            Template template)
        {
            if (operation.Name == TemplateOperations.Create.Name)
            {
                context.Succeed(operation);
            }

            if (template.Deleted)
            {
                context.Fail();
                return;
            }

            if (operation.Name == TemplateOperations.Read.Name)
            {
                if (template.IsPublic)
                {
                    context.Succeed(operation);
                }
                else
                {
                    if (await _userRoleService.IsAdminOrOwner(context.User, template))
                    {
                        context.Succeed(operation);
                    }
                }
            }

            if (operation.Name == TemplateOperations.Update.Name)
            {
                if (await _userRoleService.IsAdminOrOwner(context.User, template))
                {
                    context.Succeed(operation);
                }

            }

            if (operation.Name == TemplateOperations.Delete.Name)
            {
                if (await _userRoleService.IsAdminOrOwner(context.User, template))
                {
                    context.Succeed(operation);
                }
            }
        }
    }
}
