﻿using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Eurolook.Data.Constants;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.Web.Common.EuLogin;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using Microsoft.FeatureManagement;
using Serilog;
using TemplateStore.Users;

namespace TemplateStore.Auth
{
    public class ClaimsTransformation : IClaimsTransformation
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserGroupRepository _userGroupRepository;
        private readonly IFeatureManager _featureManager;
        private readonly EuLoginOptions _euLoginOptions;

        public ClaimsTransformation(
            IUserRepository userRepository,
            IUserGroupRepository userGroupRepository,
            IFeatureManager featureManager,
            IOptionsSnapshot<EuLoginOptions> euLoginOptions)
        {
            _userRepository = userRepository;
            _userGroupRepository = userGroupRepository;
            _featureManager = featureManager;
            _euLoginOptions = euLoginOptions.Value;
        }

        public async Task<ClaimsPrincipal> TransformAsync(ClaimsPrincipal principal)
        {
            var identity = (ClaimsIdentity)principal.Identity;
            var claimsIdentity = new ClaimsIdentity(
                identity.Claims,
                identity.AuthenticationType,
                identity.NameClaimType,
                identity.RoleClaimType);

            User elUser = await GetElUserFromIdentity(identity);
            if (elUser == null)
            {
                return new ClaimsPrincipal();
            }

            claimsIdentity.AddClaim(new Claim(ClaimTypes.Name, elUser.Login));

            var accessClaim = await CreateAccessClaim(elUser, principal);

            if (!principal.HasClaim(accessClaim.Type, accessClaim.Value))
            {
                claimsIdentity.AddClaim(accessClaim);
            }

            return new ClaimsPrincipal(claimsIdentity);
        }

        private async Task<User> GetElUserFromIdentity(ClaimsIdentity identity)
        {
            Log.Debug($"Authentication type: {identity.AuthenticationType ?? "<null>"}");

            if (identity.AuthenticationType == "Negotiate")
            {
                var login = identity.Name;
                Log.Debug($"Try to get user {login} from database");
                return await _userRepository.GetUser(login);
            }

            if (identity.AuthenticationType == EuLoginAuthentication.AuthenticationScheme)
            {
                var nameIdentifier = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;
                var loginIdentifierPrefixes = _euLoginOptions.LoginIdentifierPrefixes ?? new string[] { "" };

                User elUser = null;
                foreach (var nameIdentifierPrefix in loginIdentifierPrefixes)
                {
                    var login = nameIdentifierPrefix + nameIdentifier;
                    Log.Debug($"Try to get user {login} from database");
                    elUser = await _userRepository.GetUser(login);
                    if (elUser != null)
                    {
                        return elUser;
                    }
                }

                if (elUser == null && await _featureManager.IsEnabledAsync("EuLoginUserMappingViaEmail"))
                {
                    var email = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    Log.Debug($"Try to get user via email {email} from database");
                    return await _userRepository.GetUserByEmail(email);
                }

                return null;
            }

            Log.Error($"Authentication type {identity.AuthenticationType ?? "<null>"} is not supported.");
            return null;
        }

        private async Task<Claim> CreateAccessClaim(User elUser, IPrincipal principal)
        {
            if (await _userGroupRepository.IsMemberOfUserGroupOrMappedAdGroupAsync(elUser, CommonDataConstants.AdministratorsUserGroupId, principal))
            {
                return new Claim(AccessClaim.Type, AccessClaim.Admin);
            }

            return new Claim(AccessClaim.Type, AccessClaim.User);
        }
    }
}
