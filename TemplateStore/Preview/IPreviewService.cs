﻿using System;

namespace TemplateStore.Preview
{
    public interface IPreviewService
    {
        public void ConvertTemplateFileToPdf(Guid templateId, byte[] templateFile, string language);

        public void ExtractPngImagesFromPdf(Guid templateId, string fileUrl, string language);

        public void StoreLargePngImagesFromZip(Guid templateId, string fileUrl, string language);

        public void StoreSmallPngImagesFromZip(Guid templateId, string fileUrl, string language);
    }
}
