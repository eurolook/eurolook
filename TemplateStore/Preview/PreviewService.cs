﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Microsoft.Extensions.Configuration;
using Serilog;
using TemplateStore.Templates;

namespace TemplateStore.Preview
{
    public class PreviewService : IPreviewService, ICanLog
    {
        private readonly HttpClient _httpClient;
        private readonly ITemplateDatabase _database;

        private readonly string _codeUsername;
        private readonly string _codePassword;
        private readonly string _codeImageResolutionLarge;
        private readonly string _codeImageResolutionSmall;
        private readonly string _callbackBaseUrl;
        private readonly string _pdfWebServiceUrl;
        private readonly string _odefWebServiceUrl;
        private const string LogPrefix = "PreviewService: ";

        public PreviewService(IConfiguration configuration, HttpClient httpClient, ITemplateDatabase templateDatabase)
        {
            var codeSection = configuration.GetSection("CoDe");

            _callbackBaseUrl = configuration["TemplateStoreUrl"];
            _codeUsername = codeSection["Username"];
            _codePassword = codeSection["Password"];
            _codeImageResolutionLarge = codeSection["ImageResolutionLarge"];
            _codeImageResolutionSmall = codeSection["ImageResolutionSmall"];
            _pdfWebServiceUrl = codeSection["PdfWebserviceUrl"];
            _odefWebServiceUrl = codeSection["OdefWebserviceUrl"];
            _httpClient = httpClient;
            _database = templateDatabase;
        }

        public async void ConvertTemplateFileToPdf(Guid templateId, byte[] templateFile, string language)
        {
            Log.Information($"{LogPrefix}Starting preview generation for template with id: {templateId}: DOCX -> PDF");
            var codeParameters = CreateCallbackUrl("pdf", templateId, language).CreateList();
            var form = CreateMultipartFormDataContent(templateFile, "template.docx", codeParameters);
            await _database.SetPreviewInitiated(templateId);
            await PostRequest(_pdfWebServiceUrl, form);
        }

        public async void ExtractPngImagesFromPdf(Guid templateId, string fileUrl, string language)
        {
            Log.Information($"{LogPrefix}Continue preview generation for template with id: {templateId}: PDF -> PNG");
            var pdfBytes = await _httpClient.GetByteArrayAsync($"{_pdfWebServiceUrl}/{fileUrl}");

            var codeParametersLarge = CreateCallbackUrl("odef/large", templateId, language).CreateList();
            codeParametersLarge.Add("PNG");
            codeParametersLarge.Add($"Resolution={_codeImageResolutionLarge}");
            var form = CreateMultipartFormDataContent(pdfBytes, "template.pdf", codeParametersLarge);
            await PostRequest(_odefWebServiceUrl, form);

            var codeParametersSmall = CreateCallbackUrl("odef/small", templateId, language).CreateList();
            codeParametersSmall.Add("PNG");
            codeParametersSmall.Add($"Resolution={_codeImageResolutionSmall}");
            form = CreateMultipartFormDataContent(pdfBytes, "template.pdf", codeParametersSmall);
            await PostRequest(_odefWebServiceUrl, form);
        }

        public async void StoreLargePngImagesFromZip(Guid templateId, string fileUrl, string language)
        {
            Log.Information($"{LogPrefix}Saving large preview images for template with id: {templateId}; language: {language}");
            try
            {
                var zipBytes = await _httpClient.GetByteArrayAsync($"{_odefWebServiceUrl}/{fileUrl}");
                var (image1Bytes, image2Bytes) = await ExtractImagesFromZip(zipBytes);
                await _database.UpdateLargePreviewImages(templateId, image1Bytes, image2Bytes, language);
            } catch (Exception ex)
            {
                Log.Error(ex, "Large preview images will not be updated.");
            }
        }

        public async void StoreSmallPngImagesFromZip(Guid templateId, string fileUrl, string language)
        {
            Log.Information($"{LogPrefix}Saving small preview images for template with id: {templateId}; language: {language}");
            try
            {
                var zipBytes = await _httpClient.GetByteArrayAsync($"{_odefWebServiceUrl}/{fileUrl}");
                var (image1Bytes, image2Bytes) = await ExtractImagesFromZip(zipBytes);
                await _database.UpdateSmallPreviewImages(templateId, image1Bytes, image2Bytes, language);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Small preview images will not be updated.");
            }
        }

        private async Task<(byte[], byte[])> ExtractImagesFromZip(byte[] zipBytes)
        {
            Log.Information($"{LogPrefix}Zip File Bytes: {zipBytes?.Length}");
            await using var ms = new MemoryStream(zipBytes, false);
            using var archive = new ZipArchive(ms);
            var image1Bytes = GetFileFromArchive(archive, "img_00001.png");
            var image2Bytes = GetFileFromArchive(archive, "img_00002.png");
            Log.Information($"{LogPrefix}Page1 Bytes: {image1Bytes?.Length}; Page2 Bytes: {image2Bytes?.Length}");

            if (image1Bytes == null || image1Bytes.Length == 0)
            {
                throw new Exception("Image 1 is null or empty.");
            }

            return (image1Bytes, image2Bytes);
        }

        private string CreateCallbackUrl(string serviceType, Guid templateId, string language)
        {
            return $"callbackAddress={_callbackBaseUrl}/api/Preview/callback/{serviceType}/{templateId}/{language}";
        }

        private MultipartFormDataContent CreateMultipartFormDataContent(byte[] fileBytes, string fileName, IEnumerable<string> codeParameters)
        {
            var form = new MultipartFormDataContent();
            form.Add(new StringContent(_codeUsername), "Username");
            form.Add(new StringContent(_codePassword), "Password");

            foreach (var (codeParameter, index) in codeParameters.Select((item, i) => (item, i + 1)))
            {
                form.Add(new StringContent(codeParameter), $"Parameter{index}");
                Log.Information($"Parameter{index}={codeParameter}");
            }

            form.Add(new ByteArrayContent(fileBytes), "FileUpload1", fileName);

            return form;
        }

        private async Task PostRequest(string serviceUrl, MultipartFormDataContent form)
        {
            string createJobServiceUrl = $"{serviceUrl}/CreateJobEx.aspx";

            for (int retryCount = 0; retryCount < 3; retryCount++)
            {
                Log.Information($"{LogPrefix}{(retryCount > 0 ? "RETRY: " : "")}Calling POST {createJobServiceUrl}");
                var response = await _httpClient.PostAsync(createJobServiceUrl, form);
                Log.Information($"{LogPrefix}Response Status Code: {response.StatusCode}");
                if (response.IsSuccessStatusCode)
                {
                    break;
                }

                Log.Error($"{LogPrefix}Call to CoDe webservice failed: {serviceUrl}\n{response.ReasonPhrase}");
                Thread.Sleep(5000 * retryCount);
            }
        }

        private static byte[] GetFileFromArchive(ZipArchive archive, string fileName)
        {
            return archive
                   .Entries
                   .FirstOrDefault(e => e.FullName.Equals(fileName, StringComparison.InvariantCultureIgnoreCase))
                   ?.Open()
                   .ToByteArray() ?? Array.Empty<byte>();
        }
    }
}
