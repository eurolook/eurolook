﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace TemplateStore.Preview
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class PreviewController : ControllerBase
    {
        private readonly XNamespace _codeNs = "http://ec.europa.eu/digit/code/v5.0";
        private readonly IPreviewService _previewService;

        private readonly ContentResult _callbackResult = new()
        {
            StatusCode = 200,
            ContentType = "text/xml",
            Content =
                "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n<soap:Body>\r\n<CoDeCallbackResponse xmlns=\"http://ec.europa.eu/digit/code/v5.0\" />\r\n</soap:Body>\r\n</soap:Envelope>"
        };

        public PreviewController(IPreviewService previewService)
        {
            _previewService = previewService;
        }

        [HttpPost("callback/pdf/{templateId}/{language}")]
        [AllowAnonymous]
        public async Task<ContentResult> PdfCodeCallback(Guid templateId, string language)
        {
            Log.Information($"Callback from PDF CoDe webservice for template with id: {templateId}; language: {language}");
            string linkToFile = await ExtractLinkFromBody(Request.Body);
            _previewService.ExtractPngImagesFromPdf(templateId, linkToFile, language);
            return _callbackResult;
        }

        [HttpPost("callback/odef/large/{templateId}/{language}")]
        [AllowAnonymous]
        public async Task<ContentResult> OdefCodeCallback(Guid templateId, string language)
        {
            Log.Information($"Callback from ODEF CoDe webservice for template with id: {templateId}; language: {language}; large image");
            string linkToFile = await ExtractLinkFromBody(Request.Body);
            _previewService.StoreLargePngImagesFromZip(templateId, linkToFile, language);
            return _callbackResult;
        }

        [HttpPost("callback/odef/small/{templateId}/{language}")]
        [AllowAnonymous]
        public async Task<ContentResult> OdefCodeCallbackSmallImage(Guid templateId, string language)
        {
            Log.Information($"Callback from ODEF CoDe webservice for template with id: {templateId}; language: {language}; small image");
            string linkToFile = await ExtractLinkFromBody(Request.Body);
            _previewService.StoreSmallPngImagesFromZip(templateId, linkToFile, language);
            return _callbackResult;
        }

        private async Task<string> ExtractLinkFromBody(Stream body)
        {
            using var streamReader = new StreamReader(body, Encoding.UTF8);

            var xml = XElement.Parse(await streamReader.ReadToEndAsync());
            string linkToFile = xml?.Descendants(_codeNs + "LinkToFile").FirstOrDefault()?.Value ?? "";
            Log.Information($"Link to converted file extracted from body: {linkToFile}");

            return linkToFile.StartsWith("FileDownload.aspx?")
                ? linkToFile
                : throw new Exception("Link does not have expected format. Aborting due to security reasons.");
        }
    }
}
