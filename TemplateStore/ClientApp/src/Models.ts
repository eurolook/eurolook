export interface IIdentifiable {
    id: string;
}

export interface INamedIdentifiable extends IIdentifiable {
    name: string;
}

export interface User extends IIdentifiable {
    login: string,
    displayName: string;
    email: string;
    orgaName: string;
    isAdmin: boolean;
}

export interface UserDetails extends User {
    orgaEntities: OrgaEntity[];
    userTemplates: Set<string>
    isImpersonated: boolean;
    authenticatedUser: User;
}

export interface TemplateOwner extends User {
    isMain: boolean;
}

export interface NewTemplate {
    name: string;
    description: string;
    tags: string;
}

export interface TemplateSummary extends NewTemplate {
    id: string;
    baseDocumentModelId: string;
    baseDocumentModelName: string;
    languages: string[];
    previewPage1Base64: string;
    previewPage2Base64: string;
    thumbnailBase64: string;
    owners: TemplateOwner[];
    publications: OrgaEntity[];
    lastUpdate: string;
    hasSigner: boolean;
    isSignerFixed: boolean;
    isFeatured: boolean;
    isPublic: boolean;
}

export interface TemplateDetails extends TemplateSummary {
    isEditable: boolean;    
    userCount: number;
    draft: TemplateDetailsDraft;
}

export interface TemplatePreviewImages {
    previewPage1Base64: string;
    previewPage2Base64: string;
}

export interface OrgaEntity extends INamedIdentifiable {
    logicalLevel: number;
}

export interface BaseDocument extends INamedIdentifiable {
}

export interface PublicationFilter {
    orgaEntityId: string,
    hierarchy: boolean;
}


// Has to be kept in sync with respective enum in server side code
export enum OrderByPropertyEnum {
    templateName,
    eurolookTemplate,
    lastModified
}

export interface PaginationInfo {
    orderByProperty: OrderByPropertyEnum,
    orderAsc: boolean
}

export interface DisplayPreferences {
    isInitialized: boolean,
    viewSettings: KeyedViewSetting[],
    quickFilters: QuickFilter[]
}

export interface KeyedViewSetting {
    viewId: string,
    viewSetting: UserViewSetting
};

export interface UserViewSetting extends PaginationInfo {
    tileView: boolean;    
}

export interface QuickFilter extends INamedIdentifiable {
}

export interface SimpleSearchParameters {
    query: string;
    languageFilter: string;
    baseDocumentFilter: string;
    paginationInfo: PaginationInfo;
}

export interface SearchParameters extends SimpleSearchParameters  {
    orgaEntityIdFilter: string;
    hierarchyFilter: boolean;
}

export type UploadedFile = {
    file: File,
    language: string,
}

export interface TemplateDetailsDraft {
    name: string,
    description: string,
    tags: string,
    thumbnailBlob: Blob | null,
    deleteThumbnail: boolean,
    isSignerFixed: boolean;
    isFeatured: boolean;
}

export interface IStorefrontSection {
    name: string,
    templates: TemplateSummary[];
    templateFilter: string;
}

export interface IStorefront {
    sections: IStorefrontSection[];
}

export const unloadedTemplateDetails : TemplateDetails = {
    id: '',
    name: '',
    description: '',
    baseDocumentModelId: '',
    baseDocumentModelName: '',
    languages: [],
    previewPage1Base64: '',
    previewPage2Base64: '',
    thumbnailBase64: '',
    lastUpdate: '',
    tags: '',
    isPublic: false,
    isEditable: false,
    hasSigner: false,
    isSignerFixed: false,
    isFeatured: false,
    publications: [],
    owners: [],
    userCount: 0,
    draft: {
        name: '',
        description: '',
        tags: '',
        thumbnailBlob: null,
        deleteThumbnail: false,
        isSignerFixed: false,
        isFeatured: false,
    }
}

export interface Language extends INamedIdentifiable {
    displayName: string;
}

export interface ApplicationInfo {
    version: string;
    buildTime: string;
    commitHash: string;
    environmentName: string;
}

