import { TemplateSummary } from 'Models';
import React from 'react';
import styled from 'styled-components';

const StyledSpan = styled.span`
    overflow: hidden;
    whitespace: nowrap;
    text-trimming: ellipsis;
    max-width: 120px;
`;

export class TemplateDetailsFormatter  {
    private template: TemplateSummary;

    constructor(template: TemplateSummary) {
        this.template = template;
    }

    getLastUpdateString() {
        let lastUpdateString = '-';
        if (this.template.lastUpdate) {
            const year = this.template.lastUpdate.substring(0, 4);
            const month = this.template.lastUpdate.substring(5, 7);
            const day = this.template.lastUpdate.substring(8, 10);
            const hour = this.template.lastUpdate.substring(11, 13);
            const minute = this.template.lastUpdate.substring(14, 16);
            lastUpdateString = `${day}.${month}.${year} - ${hour}:${minute}`;
        }
        return lastUpdateString;
    }

    getLastUpdateDateOnlyString() {
        let lastUpdateString = '-';
        if (this.template.lastUpdate) {
            const year = this.template.lastUpdate.substring(0, 4);
            const month = this.template.lastUpdate.substring(5, 7);
            const day = this.template.lastUpdate.substring(8, 10);
            lastUpdateString = `${day}.${month}.${year}`;
        }
        return lastUpdateString;
    }

    getPublicationSummary() {

        if (this.template.publications.length === 0) {
            return 'Not published';
        }

        if (this.template.publications.length === 1) {
            return `${this.template.publications[0].name}`;
        }

        return <>
            <StyledSpan id="publicationsCount">{this.template.publications.map(p => p.name).join(", ")}</StyledSpan>
        </>;
    }

    getLanguagesString() {
        const languages = this.template.languages;
        return (languages.length < 5)
            ? languages.join(', ')
            : languages.filter((t, i) => i < 3).join(', ') + ` + ${languages.length - 3} others`;
    }

    getMainOwner() {
        return this.template.owners.find(o => o.isMain);
    }
}
