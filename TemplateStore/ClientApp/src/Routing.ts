import { generatePath } from "react-router-dom";

export const RoutingPaths = {
    storefront: '/Storefront',
    search: '/Search',
    myTemplates: '/MyTemplates',
    integratedMode: '/IntegratedMode',
    quickFilters: '/QuickFilters',
    
    templateFilterPagePattern: '/TemplateFilter/:filter',
    templateDetailsPagePattern: '/Templates/:id',
    templateEditingPagePattern: `/Templates/:id/edit`,
    templateFilesPagePattern: `/Templates/:id/files`,
    templatePublicationsPagePattern: `/Templates/:id/publications`,
    templateOwnersPagePattern: `/Templates/:id/owners`,

    getTemplateFilterPage(filter: string) {
        return generatePath(RoutingPaths.templateFilterPagePattern, { filter });
    },
    
    getTemplateDetailsPage(id: string) {
        return generatePath(RoutingPaths.templateDetailsPagePattern, { id });
    },

    getTemplateEditingPage(id: string) {
        return generatePath(RoutingPaths.templateEditingPagePattern, { id });
    },

    getTemplateFilesPage(id: string) {
        return generatePath(RoutingPaths.templateFilesPagePattern, { id });
    },

    getTemplatePublicationsPage(id: string) {
        return generatePath(RoutingPaths.templatePublicationsPagePattern, { id });
    },

    getTemplateOwnersPage(id: string) {
        return generatePath(RoutingPaths.templateOwnersPagePattern, { id });
    }
}
