import JSZip from 'jszip';

const invalidMessage = 'The file is not a valid Eurolook 10 document. Only recent Eurolook 10 documents are supported.';
const outdatedMessage = 'The file was created from an outdated Eurolook version and needs to be recreated.';
const incompatibleMessage = 'The file was created from an incompatible Eurolook version.';
const trackChangesMessage = 'Documents with tracked changes cannot be uploaded to the Template Store. Resolve all pending changes and deactivate the Track Changes feature.';

const changeDescendants = [
    "ins", "del", "delInstrText", "delText",
    "cellDel", "cellIns", "cellMerge",
    "customXmlDelRangeEnd", "customXmlDelRangeStart", "customXmlInsRangeEnd", "customXmlInsRangeStart",
    "customXmlMoveFromRangeStart", "customXmlMoveFromRangeEnd", "customXmlMoveToRangeStart", "customXmlMoveToRangeEnd",
    "moveFrom", "moveFromRangeEnd", "moveFromRangeStart", "moveTo", "moveToRangeEnd", "moveToRangeStart",
    "numberingChange", "rPrChange", "pPrChange", "sectPrChange", "tcPrChange", "trPrChange",
    "tblGridChange", "tblPrChange", "tblPrExChange"];

export interface EurolookCustomProperties {
    documentModelId: string,
    documentModelName: string,
    languageName: string,
}


export class DocxAnalyzer {

    public async validateDocxFile(file: File): Promise<EurolookCustomProperties> {
        const customXmlFiles = await this.getXmlFiles(file, "customXml");
        const wordXmlFiles = await this.getXmlFiles(file, "word");
        const rootElement = await this.getEurolookProperties(customXmlFiles);
        await this.validateEurolookProperties(rootElement);
        await this.validateAuthorXml(customXmlFiles);
        await this.validateTrackChangesDeactivated(wordXmlFiles);
        await this.validateNoUnresolvedTrackChanges(wordXmlFiles);
        return this.getEurolookCustomProperties(rootElement);
    }

    private async getXmlFiles(file: File, folderName:string): Promise<JSZip.JSZipObject[]> {
        let zip: JSZip;
        try {
            zip = await JSZip.loadAsync(file);
        } catch (ex) {
            console.error(ex);
            throw new Error(invalidMessage);
        }

        const folder = zip.folder(folderName);

        if (folder == null) {
            console.error("Folder '" + folderName + "' not found.");
            throw new Error(invalidMessage);
        }

        return folder.filter(() => true);
    }

    private async validateEurolookProperties(rootElement: Element) {
        const productCustomizationElement = rootElement.getElementsByTagName("ProductCustomizationId")[0];

        if (!productCustomizationElement) {
            console.error("ProductCustomizationId not available.");
            throw new Error(outdatedMessage);
        }

        if (rootElement.getElementsByTagName("ProductCustomizationId")[0]?.innerHTML !== "EC") {
            console.error("ProductCustomizationId mismatch.");
            throw new Error(incompatibleMessage);
        }
    }

    private getEurolookCustomProperties(rootElement: Element) {

        const documentModelElement = rootElement.getElementsByTagName("DocumentModel")[0];

        const documentModelId = documentModelElement.getElementsByTagName("Id")[0].innerHTML.substring(0, 36);
        const documentModelName = documentModelElement.getElementsByTagName("Name")[0].innerHTML.substring(0, 60);
        const languageName = rootElement.getElementsByTagName("Created")[0].getElementsByTagName("Language")[0].innerHTML.substring(0, 2);

        return {
            documentModelId: documentModelId,
            documentModelName: documentModelName,
            languageName: languageName,
        }

    }

    private async validateAuthorXml(customXmlFiles: JSZip.JSZipObject[]) {
        let authorXmlFilesCount = 0;

        for (const customXmlFile of customXmlFiles) {
            const xmlDocument = await this.parseXmlFromJSZipObject(customXmlFile);
            const authorTags = xmlDocument.getElementsByTagName("Author");
            if (authorTags.length === 1) {
                authorXmlFilesCount++;
                if (!authorTags[0].getAttribute("AuthorRoleId") && !authorTags[0].getAttribute("OriginalAuthorRoleId")) {
                    console.error("Author roles not available.");
                    throw new Error(outdatedMessage);
                }
            }
        }

        if (authorXmlFilesCount === 0) {
            console.error("No Author XML file found.");
            throw new Error(invalidMessage);
        }
    }

    private async validateTrackChangesDeactivated(wordXmlFiles: JSZip.JSZipObject[]) {
        const file = wordXmlFiles.filter(x => x.name === 'word/settings.xml')[0];
        const xmlDocument = await this.parseXmlFromJSZipObject(file);

        if (xmlDocument.getElementsByTagName("w:trackRevisions").length > 0) {
            console.error("Track changes activated.");
            throw new Error(trackChangesMessage);
        }
    }

    private async validateNoUnresolvedTrackChanges(wordXmlFiles: JSZip.JSZipObject[]) {
        for (const wordXmlFile of wordXmlFiles) {
            const xmlDocument = await this.parseXmlFromJSZipObject(wordXmlFile);

            changeDescendants.forEach((filter) => {
                if (xmlDocument.getElementsByTagName(("w:" + filter)).length > 0) {
                    console.error("Unresolved track changes detected.");
                    throw new Error(trackChangesMessage);
                }
            });
        }
    }

    private async getEurolookProperties(customXmlFiles: JSZip.JSZipObject[]): Promise<Element> {
        for (const customXmlFile of customXmlFiles) {
            const xmlDocument = await this.parseXmlFromJSZipObject(customXmlFile);
            const elPropertiesTags = xmlDocument.getElementsByTagName("EurolookProperties");
            if (elPropertiesTags.length === 1) {
                return elPropertiesTags[0];
            }
        }

        console.error("EurolookProperties not found.");
        throw new Error(invalidMessage);
    }

    private async parseXmlFromJSZipObject(xmlFileInZip: JSZip.JSZipObject): Promise<XMLDocument>{
        const content = await xmlFileInZip.async("string");
        const parser = new DOMParser();
        // Chrome + Edge have issues parsing xml file starting with a BOM
        const cleanedContent = content.startsWith('\ufeff') ? content.slice(1) : content;
        return parser.parseFromString(cleanedContent, "text/xml");
    }
}
