import { UserDetails, IStorefront, TemplateSummary, NewTemplate, SearchParameters, Language, BaseDocument, TemplateDetails, TemplateOwner, ApplicationInfo, TemplatePreviewImages, SimpleSearchParameters, OrgaEntity, DisplayPreferences} from 'Models';
import { get, putForm, postForm, post, del, put } from 'HttpHelper';


function createUrl(url: string, impersonatedUser: string) {

    if (!impersonatedUser) {
        return url;
    }

    return url.indexOf('?') < 0
        ? `${url}?impersonate=${impersonatedUser}`
        : `${url}&impersonate=${impersonatedUser}`;
}

let impersonatedUser = '';

export function setImpersonatedUser(login: string) {
    impersonatedUser = login;
}

export async function getUserInfo(): Promise<UserDetails> {
    const response = await get(createUrl(`api/Users/My`, impersonatedUser));
    return await response.json();
}

export async function getStorefront(): Promise<IStorefront> {
    const response = await get(createUrl(`api/templates/storefront`, impersonatedUser));
    return await response.json();
}

export async function search(parameters: SearchParameters): Promise<TemplateSummary[]> {
    const query = `api/Templates?q=${parameters.query}` +
        `&l=${parameters.languageFilter}` +
        `&b=${parameters.baseDocumentFilter}` +
        `&o=${ parameters.orgaEntityIdFilter }` +
        `&h=${ parameters.hierarchyFilter }` +
        `&asc=${parameters.paginationInfo.orderAsc}` +
        `&orderBy=${parameters.paginationInfo.orderByProperty}`;
   
    const response = await get(createUrl(query, impersonatedUser));
    return await response.json();
}

export async function getLanguages(): Promise<Language[]> {
    const response = await get(createUrl(`api/Templates/Languages`, impersonatedUser));
    return await response.json();
}

export async function getBaseDocumentFilters(): Promise<BaseDocument[]> {
    const response = await get(createUrl(`api/Templates/BaseDocumentFilters`, impersonatedUser));
    return await response.json();
}

export async function getMyTemplates(parameters: SimpleSearchParameters): Promise<TemplateSummary[]> {
    const query = `api/templates/my?q=${parameters.query}` +
        `&l=${parameters.languageFilter}` +
        `&b=${parameters.baseDocumentFilter}` +
        `&asc=${parameters.paginationInfo.orderAsc}` +
        `&orderBy=${parameters.paginationInfo.orderByProperty}`;

    const response = await get(createUrl(query, impersonatedUser));
    return await response.json();
}

export async function saveTemplateDetails(template: TemplateDetails) {
    await put(createUrl(`api/Templates/${template.id}`, impersonatedUser), template.draft);

    if (template.draft.thumbnailBlob) {
        const formData = new FormData();
        formData.append('file', template.draft.thumbnailBlob);
        await putForm(createUrl(`api/Templates/${template.id}/thumbnail`, impersonatedUser), formData);
    }

    if (template.draft.deleteThumbnail) {
        await del(createUrl(`api/Templates/${template.id}/thumbnail`, impersonatedUser));
    }
}

export async function uploadTemplateFile(file: File, templateId : string) {
    const formData = new FormData();
    formData.append('file', file);
    await postForm(createUrl(`api/Templates/${templateId}/files`, impersonatedUser), formData);
}

export async function createPreviewImages(templateId: string, language: string) {
    await post(createUrl(`api/Templates/${templateId}/images/${language}`, impersonatedUser));
}

export async function getTemplateDetails(templateId: string): Promise<TemplateDetails> {
    const response = await get(createUrl(`api/Templates/${templateId}`, impersonatedUser));
    return await response.json();
}

export async function getTemplateImages(templateId: string, small: boolean = false): Promise<TemplatePreviewImages> {
    const response = await get(createUrl(`api/Templates/${templateId}/images?small=${small}`, impersonatedUser));
    return await response.json();
}

export async function deleteTemplate(templateId: string) {
    await del(createUrl(`api/Templates/${templateId}`, impersonatedUser));
}

export async function deleteTemplateFile(templateId: string, languageName: string) {
    await del(createUrl(`api/Templates/${templateId}/files/${languageName}`, impersonatedUser));
}

export async function getTemplateFile(templateId: string, languageName: string): Promise<Blob> {
    const response = await get(createUrl(`api/templates/${templateId}/files/${languageName}`, impersonatedUser));
    return await response.blob();
}

export async function getTemplateXmlExport(templateId: string): Promise<Blob> {
    const response = await get(createUrl(`api/templates/${templateId}/export`, impersonatedUser));
    return await response.blob();
}

export async function searchUsers(query: string): Promise<TemplateOwner[]> {
    const response = await get(createUrl(`api/users/search?q=${query}&pageSize=15`, impersonatedUser));
    return await response.json();
}

export async function searchOrgaEntities(query: string): Promise<OrgaEntity[]> {
    const response = await get(createUrl(`api/OrgaEntities?q=${query}`, impersonatedUser));
    return await response.json();
}

export async function saveAsUserTemplate(templateId: string) {
    await put(createUrl(`api/users/templates/${templateId}`, impersonatedUser));    
}

export async function updatePublications(templateId: string, orgaEntityIds: string[]) {
    await put(createUrl(`api/Templates/${templateId}/Publications`, impersonatedUser), orgaEntityIds);
}

export async function updateOwners(templateId: string, owners: TemplateOwner[]) {
    await put(createUrl(`api/Templates/${templateId}/owners`, impersonatedUser), owners);
}

export async function createTemplate(template: NewTemplate): Promise<string> {
    const response = await post(createUrl(`api/Templates`, impersonatedUser), template);
    return await response.json();
}

export async function getApplicationInfo(): Promise<ApplicationInfo> {
    const response = await get(createUrl(`api/Metadata/ApplicationInfo`, impersonatedUser));
    return await response.json();
}

export async function getHelpBaseUrl(): Promise<string> {
    const response = await get(createUrl(`api/Metadata/HelpBaseUrl`, impersonatedUser));
    return await response.json();
}

export async function getDisplayPreferences(): Promise<DisplayPreferences> {
    const response = await get(createUrl(`api/DisplayPreferences`, impersonatedUser));
    return await response.json();
}

export async function putDisplayPreferences(displayPreferences: DisplayPreferences) {
    await put(createUrl(`api/DisplayPreferences`, impersonatedUser), displayPreferences);
}

