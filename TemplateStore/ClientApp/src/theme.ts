import styled, { css, ThemedStyledProps } from 'styled-components/macro';
import { Input, Button, Label, Badge, Spinner } from 'reactstrap';
import { lighten } from 'polished';
import { Link } from 'react-router-dom';

const sidebarWidth = 200;
const pageMargin = 74;
const tileWidth = 205;
const tileWidthSmall= tileWidth * 2 / 3 - 6;
const tileGap = 16;
const pageWidthS = 2 * tileWidth + 1 * tileGap;
const pageWidthM = 3 * tileWidth + 2 * tileGap;
const pageWidthL = 4 * tileWidth + 3 * tileGap;
const pageWidthXl = 6 * tileWidth + 5 * tileGap;
const breakpointS = sidebarWidth + pageMargin + pageWidthS;
const breakpointM = sidebarWidth + pageMargin + pageWidthM;
const breakpointL = sidebarWidth + pageMargin + pageWidthL;
const breakpointXl = sidebarWidth + pageMargin + pageWidthXl;

const primaryColor = "#0072C6";
const primaryColorAccent = lighten(0.1, primaryColor);
const primaryBorderColor = "#80B9E3";
const primaryBoxShadow = lighten(0.5, primaryColor);

const secondaryColor = "#f0f0f0";
const secondaryColorAccent = lighten(0.04, secondaryColor);
const secondaryBorderColor = "#acacac";
const secondaryBoxShadow = lighten(0.15, secondaryColor);

const highlightColor = "#de6b39";
const highlightColorAccent = lighten(0.1, highlightColor);
const highlightBorderColor = "#de6b39";
const highlightBoxShadow = lighten(0.5, highlightColor);

const sidebarColor = "#00819D";
const sidebarColorAccent = lighten(0.04, sidebarColor);

const propertiesBoxColor = "#f9f9f9";

const warningColor = "#664d03"
const alertColor = "#842029";

export const Theme = {
    ...{ pageMargin },
    ...{ tileWidth },
    ...{ tileWidthSmall },
    ...{ tileGap },
    ...{ pageWidthS },
    ...{ pageWidthM },
    ...{ pageWidthL },
    ...{ pageWidthXl },
    ...{ breakpointS },
    ...{ breakpointM },
    ...{ breakpointL },
    ...{ breakpointXl },
    ...{ primaryColor },
    ...{ primaryColorAccent },
    ...{ primaryBorderColor },
    ...{ primaryBoxShadow },
    ...{ secondaryColor },
    ...{ secondaryColorAccent },
    ...{ secondaryBorderColor },
    ...{ secondaryBoxShadow },
    ...{ highlightColor },
    ...{ highlightColorAccent },
    ...{ highlightBorderColor },
    ...{ highlightBoxShadow },
    ...{ sidebarColor },
    ...{ sidebarColorAccent },
    ...{ propertiesBoxColor },
    ...{ warningColor },
    ...{ alertColor },
};


interface MediaQueryProps {
    readonly $small?: boolean;
};

type P = ThemedStyledProps<MediaQueryProps, any>;

export const MediaQueries = css`
  @media (min-width: ${(props: P) => props.theme.breakpointS}px) {
    max-width: ${(props: P) => props.theme.pageWidthS}px;
  }

  @media (min-width: ${(props: P) => props.theme.breakpointM}px) {
    max-width: ${(props: P) => props.$small ? props.theme.pageWidthM : props.theme.pageWidthM}px;
  }

  @media (min-width: ${(props: P) => props.theme.breakpointL}px) {
    max-width: ${(props: P) => props.$small ? props.theme.pageWidthL : props.theme.pageWidthL}px;
  }

  @media (min-width: ${(props: P) => props.theme.breakpointXl}px) {
    max-width: ${(props: P) => props.$small ? props.theme.pageWidthM : props.theme.pageWidthXl}px;
  }
`;

export const ListViewMediaQueries = css`
  @media (min-width: ${(props: P) => props.theme.breakpointL}px) {
    max-width: ${(props: P) => props.$small ? props.theme.pageWidthXl : props.theme.pageWidthXl}px;
  }

  @media (min-width: ${(props: P) => props.theme.breakpointXl}px) {
    max-width: ${(props: P) => props.$small ? props.theme.pageWidthM : props.theme.pageWidthXl}px;
  }
`

export const StyledInput = styled(Input)`
  border-radius: 0;
  height: ${(props) => props.type === 'textarea' ? 100: 30}px;
  border-color: ${(props) => props.theme.borderColor};
  box-shadow: none;
  margin-top: 0;
  &:focus, &:hover {
    box-shadow: none;
    border-color: ${props => props.theme.primaryBorderColor};
  }
`;

interface IButtonStyleProps {
    readonly color?: string;
};

type ButtonStyleProps = ThemedStyledProps<IButtonStyleProps, any>;

export const ButtonStyle = css`
    color: ${(props: ButtonStyleProps) => props.color === "primary" ? "#fff" : props.color === "highlight" ? "#fff" : "#000"};
    height: 30px;
    outline: none;
    box-shadow: none; 
    border-radius: 0;
    border-width: 1px;
    border-style: solid;
    background-color: ${(props: ButtonStyleProps) => props.color === "primary" ? props.theme.primaryColor : props.color === "highlight" ? props.theme.highlightColor : props.theme.secondaryColor};
    border-color: ${(props: ButtonStyleProps) => props.color === "primary" ? props.theme.primaryBorderColor : props.color === "highlight" ? props.theme.highlightBorderColor : props.theme.secondaryBorderColor};
    padding: 0.375rem 0.75rem;
    min-width: 100px;
    text-align: center;

    &:focus, &:hover {
        border-radius: 0;
        box-shadow: none;
    }

    &:disabled {
        &&& {
            color: ${(props: ButtonStyleProps) => props.color === "primary" || props.color === "disabled" ? props.theme.secondaryColor : props.theme.secondaryBorderColor};
            background-color: ${(props: ButtonStyleProps) => props.color === "primary" ? props.theme.primaryColor : props.color === "highlight" ? props.theme.highlightColor : props.theme.secondaryColor};
            border-color: ${(props: ButtonStyleProps) => props.color === "primary" ? props.theme.primaryBorderColor : props.color === "highlight" ? props.theme.highlightBorderColor : props.theme.secondaryBorderColor};
            opacity: ${(props: ButtonStyleProps) => props.color === "primary" ? 0.65 : 1};
        }
    }

    &:not(:disabled):not(.disabled) {
        cursor: pointer;
    }

    &:focus,&:not(:disabled):not(.disabled):active {
        color: ${(props: ButtonStyleProps) => props.color === "primary" ? "#fff" : props.color === "highlight" ? "#fff" : "#000"};
        background-color: ${(props: ButtonStyleProps) => props.color === "primary" ? props.theme.primaryColor : props.color === "highlight" ? props.theme.highlightColor : props.theme.secondaryColor};
        border-color: ${(props: ButtonStyleProps) => props.color === "primary" ? props.theme.primaryBorderColor : props.color === "highlight" ? props.theme.highlightBorderColor : props.theme.secondaryBorderColor};
    }

    &:hover {
        color: ${(props: ButtonStyleProps) => props.color === "primary" ? "#fff" : props.color === "highlight" ? "#fff" : "#000"};
        background-color: ${(props: ButtonStyleProps) => props.color === "primary" ? props.theme.primaryColorAccent : props.color === "highlight" ? props.theme.highlightColorAccent : props.theme.secondaryColorAccent};
        border-color: ${(props: ButtonStyleProps) => props.color === "primary" ? props.theme.primaryBorderColor : props.color === "highlight" ? props.theme.highlightBorderColor : props.theme.secondaryBorderColor};
        transition: .5s ease;
    }
 
  svg {
    margin-right: 4px;
    vertical-align: top;

    path {
      fill: ${(props: ButtonStyleProps) => props.theme.primaryColor};
    }
  }
`;

export const StyledButton = styled(Button)`${ButtonStyle}`;

export const StyledIconButton = styled(StyledButton)`
    min-width: 0;
`;

export const TransparentButtonStyle = css`
    ${ButtonStyle}
    color: ${(props) => props.theme.primaryColor};
    background: transparent;
    border: 0;
    min-width: 0px;

    &:focus,&:not(:disabled):not(.disabled):active {
        color: ${(props) => props.theme.primaryColor};
        background-color: transparent;
    }

    &:hover {
        color: ${(props) => props.theme.primaryColor};
    }

    &:disabled {
        color: ${(props: ButtonStyleProps) => props.theme.secondaryBorderColor};
        background-color: transparent;

        svg {
            path {
                fill: ${(props: ButtonStyleProps) => props.theme.secondaryBorderColor};
            }
        }
    }
`;

export const TransparentButton = styled(StyledButton)`${TransparentButtonStyle}`;

export const NavButtonStyle = css`
    color: #fff;
    padding: 0 0 0 20px;
    height: 35px;
    line-height: 35px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    background-color: ${(props) => props.theme.sidebarColor};
    border: 0;    
    text-align: left;

    &:hover, &:not(:disabled):not(.disabled):active {
        color: #fff;
        background-color: ${(props) => props.theme.sidebarColorAccent};
        transition: none;
    }

    &:focus {
        color: #fff;
        background-color: ${(props) => props.theme.sidebarColor};
    }
`;

export const NavButton = styled(StyledButton)`${NavButtonStyle}`;

export const ItemsContainerAction = styled.div`
    display: grid;
    div, button {
        color: ${(props) => props.theme.primaryColor};
        text-align: left;

        &:hover, &:focus, &:not(:disabled):not(.disabled):active {
            color: ${(props) => props.theme.primaryColor};
        }
    }
`;

export const ButtonGroup = styled.div`
    justify-content: ${(props: { leftAligned?: boolean }) => props.leftAligned ? "flex-start" : "flex-end"};
    display: flex;
    flex-wrap: wrap;
    gap: 10px;
`;

export const StyledLabel = styled(Label)`
    margin-bottom: 4px;
    margin-top: 10px;
`;

export const LabelWithoutTopMargin = styled(StyledLabel)`
    margin-top: 0;
`;

export const Box = styled.div`
    height: 30px;
    line-height: 30px;
    padding-left: 8px;
    background: ${(props) => props.theme.secondaryColor};
    border: 1px solid ${(props) => props.theme.secondaryBorderColor};
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
`;

export const TilePreview = styled.div`   
    background-color: #FFF;
    height: 100%;
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    color: ${(props) => props.theme.secondaryBorderColor};

    img {
        width: 100%;
        height: 100%;
        object-fit: cover;
        object-position: top;
    }
`;

export const PropertiesBox = styled.div`
    display: flex;
    flex-wrap: wrap;
    padding: 8px 12px;
    background-color: ${(props) => props.theme.propertiesBoxColor};    
`;

export const StyledLink = styled(Link)`
    text-decoration: none;
    color: ${(props) => props.theme.primaryColor};
`;

export const StyledA = styled.a`
    text-decoration: none;
    color: ${(props) => props.theme.primaryColor};    
`;

export const ButtonInAlertCss = css`
    height: 20px;
    svg {
        height: 20px;
    }
    vertical-align: bottom;
    padding: 0 4px 0 4px;
`;

export const FlexDiv = styled.div`
    display: flex;
    min-width: 0;
    overflow: hidden;
    column-gap: 4px;
`;

export const SpacerDiv = styled.div`
    flex-grow: 1;
`;

export const StyledBadge = styled(Badge)`
    margin-right: 4px;
    font-size: 10px;
    font-style: normal;
    font-weight: normal;
    color: #00819D !important;
    background-color: #CFE7ED !important;
    max-width: 100px;
    overflow: hidden;
    text-overflow: ellipsis;
`

export const LargeSpinner = styled(Spinner)`
    --bs-spinner-width: 3rem;
    --bs-spinner-height: 3rem;
`;

interface IIconContainerProps {
    addRightSpace?: boolean|number;
    highlightOnHover?: boolean;
    iconHeight?: number;
    strokeColor?: string;
}

type IconContainerProps = ThemedStyledProps<IIconContainerProps, any>;

export const IconContainer = styled.span<IconContainerProps>`
    display: flex;
    align-items: center;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    height: 100%;
     svg {
        height: ${(props) => props.iconHeight ? props.iconHeight : 24}px;
        stroke: ${(props) => props.strokeColor ? props.strokeColor : 'currentColor'};
        margin-right: ${(props) => props.addRightSpace ? (typeof props.addRightSpace === 'number' ? props.addRightSpace : 8) : 0}px;
        flex-shrink: 0;

        path {
            fill:none !important;
        }

        ${props => {
            if (props.highlightOnHover) {
                return `&:hover, &:not(:disabled):not(.disabled):active {
                        stroke: ${props.theme.primaryColor};
                    }`;
            }
            else return ``;
        }}

        ${props => {
            if (props.onClick) {
                return `cursor: pointer`;
            }
            else return ``;
        }}
    };
`

export const NoOverflowSpan = styled.span`
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
`;

export const NoOverflowDiv = styled.div`
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
`;

export const Rect = styled.div`
    padding: 10px;
    background: #fff;
    border: 1px solid ${(props) => props.theme.secondaryBorderColor};
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    display: flex;
    flex-direction: column;
`;


