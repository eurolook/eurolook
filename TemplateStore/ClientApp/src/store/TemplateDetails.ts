import { Action, Reducer } from 'redux';
import { AppThunkAction } from '.';
import * as api from 'api';
import { toast } from 'react-toastify';
import { TemplateDetails, TemplateDetailsDraft,  } from 'Models';
import produce from 'immer';


// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface TemplateDetailsState {
    isLoading: boolean;
    templates: TemplateDetails[];
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.

export enum TemplateDetailsActionTypes {
    Request = "REQUEST_TEMPLATE_DETAILS",
    Receive = "RECEIVE_TEMPLATE_DETAILS",
    Edit = "EDIT_TEMPLATE_DETAILS",
    Refresh = "REFRESH_TEMPLATE_DETAILS",
    Save = "SAVE_TEMPLATE_DETAILS",
};

interface RequestTemplateDetailsAction {
    type: TemplateDetailsActionTypes.Request;
}

interface ReceiveTemplateDetailsAction {
    type: TemplateDetailsActionTypes.Receive;
    template: TemplateDetails;
}

interface EditTemplateDetailsAction {
    type: TemplateDetailsActionTypes.Edit;
    templateDraft: TemplateDetailsDraft;
    templateId: string;
}

interface RefreshTemplateDetailsAction {
    type: TemplateDetailsActionTypes.Refresh;
    template: TemplateDetails;
}

interface SaveTemplateDetailsAction {
    type: TemplateDetailsActionTypes.Save;
    template: TemplateDetails;
}


// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
type KnownAction = RequestTemplateDetailsAction | ReceiveTemplateDetailsAction | SaveTemplateDetailsAction | EditTemplateDetailsAction | RefreshTemplateDetailsAction;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    requestTemplateDetails: (id: string): AppThunkAction<KnownAction> => async (dispatch, getState) => {
        // Only load data if it's something we don't already have (and are not already loading)
        const appState = getState();
        if (appState && appState.templateDetails) {
            dispatch({ type: TemplateDetailsActionTypes.Request });
            const data = await api.getTemplateDetails(id);
            return dispatch({ type: TemplateDetailsActionTypes.Receive, template: data });
        }
    },

    editTemplateDetails: (templateId: string, templateDraft: TemplateDetailsDraft): AppThunkAction<KnownAction> => (dispatch, getState) => {
        const appState = getState();
        if (appState && appState.templateDetails) {
            dispatch({
                type: TemplateDetailsActionTypes.Edit,
                templateDraft: templateDraft,
                templateId: templateId,
            });
        }
    },

    refreshTemplateDetails: (templateId: string): AppThunkAction<KnownAction> => async (dispatch, getState) => {
        dispatch({ type: TemplateDetailsActionTypes.Request });
        const data = await api.getTemplateDetails(templateId);
        dispatch({ type: TemplateDetailsActionTypes.Refresh, template: data });
    },

    saveTemplateDetails: (template: TemplateDetails): AppThunkAction<KnownAction> => async (dispatch, getState) => {

        const appState = getState();
        if (appState && appState.templateDetails) {

            try {
                await api.saveTemplateDetails(template);
            }
            catch (e) {
                console.error(e);
                toast.error(`An error occured while saving the template`);
            }

            actionCreators.requestTemplateDetails(template.id)(dispatch, getState);
        };
    }
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

const unloadedState: TemplateDetailsState = { templates: [], isLoading: false };

export const reducer: Reducer<TemplateDetailsState> =
    (state: TemplateDetailsState | undefined, incomingAction: Action): TemplateDetailsState => {
        if (state === undefined) {
            return unloadedState;
        }

        const action = incomingAction as KnownAction;
        switch (action.type) {
        case TemplateDetailsActionTypes.Request:
        {
            return {
                templates: state.templates,
                isLoading: true
            };
        }
        case TemplateDetailsActionTypes.Receive:
        {
            const template = action.template;

            template.draft = {
                name: template.name,
                description: template.description,
                tags: template.tags,
                thumbnailBlob: null,
                deleteThumbnail: false,
                isSignerFixed: template.isSignerFixed,
                isFeatured: template.isFeatured
            }

            return {
                templates: [...state.templates.filter(t => t.id !== template.id), template],
                isLoading: false,
            };
        }
        case TemplateDetailsActionTypes.Edit:
        {
            const newState = produce(state,
                (draftState: TemplateDetailsState) => {
                    const template = draftState.templates.find(t => t.id === action.templateId);
                    if (template) {
                        template.draft = action.templateDraft;
                    }
                });
            return newState;
        }
        case TemplateDetailsActionTypes.Refresh:
        {
            const template = action.template;
            const existingTemplate = state.templates.find(t => t.id === action.template.id);
            if (!existingTemplate) {
                return state;
            }
            template.draft = existingTemplate.draft;

            return {
                templates: [...state.templates.filter(t => t.id !== template.id), template],
                isLoading: false,
            };
        }

        default:
            return state;
        }
    };
