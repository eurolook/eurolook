import * as Storefront from './Storefront';
import * as Search from './Search';
import * as TemplateDetails from './TemplateDetails';
import * as General from './General';

// The top-level state object
export interface ApplicationState {
    search: Search.SearchState;
    storefront: Storefront.StorefrontState;
    templateDetails: TemplateDetails.TemplateDetailsState;
    general: General.GeneralState;
}

// Whenever an action is dispatched, Redux will update each top-level application state property using
// the reducer with the matching name. It's important that the names match exactly, and that the reducer
// acts on the corresponding ApplicationState property type.
export const reducers = {
    storefront: Storefront.reducer,
    search: Search.reducer,
    templateDetails: TemplateDetails.reducer,
    general: General.reducer,
};

// This type can be used as a hint on action creators so that its 'dispatch' and 'getState' params are
// correctly typed to match your store.
export interface AppThunkAction<TAction> {
    (dispatch: (action: TAction) => void, getState: () => ApplicationState): void;
}
