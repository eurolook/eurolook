import { Action, Reducer } from 'redux';
import { AppThunkAction } from '.';
import * as api from 'api';
import { IStorefront } from 'Models';


// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface StorefrontState extends IStorefront{
    isLoading: boolean;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.

export enum StorefrontActionTypes {
    Request = "REQUEST_STOREFRONT_TEMPLATES",
    Receive = "RECEIVE_STOREFRONT_TEMPLATES",
};

interface RequestStorefrontTemplatesAction {
    type: StorefrontActionTypes.Request;
}

interface ReceiveStorefrontTemplatesAction {
    type: StorefrontActionTypes.Receive;
    storefront: IStorefront;
}

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
type KnownAction = RequestStorefrontTemplatesAction | ReceiveStorefrontTemplatesAction;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    requestStorefrontTemplates: (): AppThunkAction<KnownAction> => async (dispatch, getState) => {
        // Only load data if it's something we don't already have (and are not already loading)
        const appState = getState();
        if (appState && appState.storefront) {
            dispatch({ type: StorefrontActionTypes.Request });
            const data = await api.getStorefront();
            dispatch({ type: StorefrontActionTypes.Receive, storefront: data, });
        }
    }
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

const unloadedState: StorefrontState = { sections: [], isLoading: false };

export const reducer: Reducer<StorefrontState> =
    (state: StorefrontState | undefined, incomingAction: Action): StorefrontState => {
        if (state === undefined) {
            return unloadedState;
        }

        const action = incomingAction as KnownAction;
        switch (action.type) {
        case StorefrontActionTypes.Request:
            return {
                ...state,
                isLoading: true
            };
        case StorefrontActionTypes.Receive:
            return {
                ...action.storefront,
                isLoading: false
            };
        default:
            return state;
        }
    };
