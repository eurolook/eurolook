import { Action, Reducer } from 'redux';
import { AppThunkAction } from '.';
import * as api from 'api';

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface GeneralState {
    isIntegratedMode: boolean;
    impersonatedUser: string;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.

export enum GeneralActionTypes {
    SetIntegratedMode = "SET_INTEGRATED_MODE",
    SetImpersonatedUser = "SET_IMPERSONATED_USER",
};

interface SetIntegratedModeAction {
    type: GeneralActionTypes.SetIntegratedMode;
}

interface SetImpersonatedUserAction {
    type: GeneralActionTypes.SetImpersonatedUser;
    login: string;
}

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
type KnownAction = SetIntegratedModeAction | SetImpersonatedUserAction;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    setIntegratedMode: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: GeneralActionTypes.SetIntegratedMode });
    },

    setImpersonatedUser: (login: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({
            type: GeneralActionTypes.SetImpersonatedUser,
            login: login
        });

        api.setImpersonatedUser(login);
    }
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

const unloadedState: GeneralState = {
    isIntegratedMode: false,
    impersonatedUser: '',
};

export const reducer: Reducer<GeneralState> =
    (state: GeneralState | undefined, incomingAction: Action): GeneralState => {
        if (state === undefined) {
            return unloadedState;
        }

        const action = incomingAction as KnownAction;
        switch (action.type) {
            case GeneralActionTypes.SetIntegratedMode:
                return {
                    ...state,
                    isIntegratedMode: true,
                };

            case GeneralActionTypes.SetImpersonatedUser:
                return {
                    ...state,
                    impersonatedUser: action.login,
                };

            default:
            return state;
        }
    };
