import { Action, Reducer } from 'redux';
import { AppThunkAction } from '.';
import * as api from 'api';
import { TemplateSummary, Language, BaseDocument, SearchParameters, PublicationFilter, OrderByPropertyEnum, UserViewSetting, SimpleSearchParameters } from 'Models';

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface SearchState {
    isLoading: boolean;
    refetchTrigger: number;
    results: TemplateSummary[];
    languages: Language[];
    baseDocuments: BaseDocument[];
    query: string;
    languageFilter: Language | undefined;
    publicationFilter: PublicationFilter;
    baseDocumentFilter: BaseDocument | undefined;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.

export enum SearchActionTypes {
    SetSearchQuery = "SET_SEARCH_QUERY",
    Search = "SEARCH_TEMPLATES",
    ReceiveResults = "RECEIVE_SEARCH_RESULTS",
    ResetSearch = "RESET_SEARCH",
    FetchLanguages = "FETCH_LANGUAGES",
    ReceiveLanguages = "RECEIVE_LANGUAGES",
    FetchBaseDocuments = "FETCH_BASE_DOCUMENTS",
    ReceiveBaseDocuments = "RECEIVE_BASE_DOCUMENTS",
    SetLanguageFilter = "SET_LANGUAGE_FILTER",
    SetPublicationFilter = "SET_PUBLICATION_FILTER",
    SetBaseDocumentFilter = "SET_BASE_DOCUMENT_FILTER",
};

interface SetSearchQueryAction {
    type: SearchActionTypes.SetSearchQuery;
    query: string;
}

interface SearchTemplatesAction {
    type: SearchActionTypes.Search;
}

interface ReceiveSearchResultsAction {
    type: SearchActionTypes.ReceiveResults;
    results: TemplateSummary[];
}

interface ResetSearchAction {
    type: SearchActionTypes.ResetSearch;
}

interface SetLanguageFilterAction {
    type: SearchActionTypes.SetLanguageFilter;
    id: string;
}

interface SetPublicationFilterAction {
    type: SearchActionTypes.SetPublicationFilter;
    filter: PublicationFilter;
}

interface SetBaseDocumentFilterAction {
    type: SearchActionTypes.SetBaseDocumentFilter;
    id: string;
}

interface FetchLanguagesAction {
    type: SearchActionTypes.FetchLanguages;
}

interface ReceiveLanguagesAction {
    type: SearchActionTypes.ReceiveLanguages;
    languages: Language[];
}

interface FetchBaseDocumentsAction {
    type: SearchActionTypes.FetchBaseDocuments;
}

interface ReceiveBaseDocumentsAction {
    type: SearchActionTypes.ReceiveBaseDocuments;
    baseDocuments: BaseDocument[];
}


// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
type KnownAction = SearchTemplatesAction | ReceiveSearchResultsAction |
    SetSearchQueryAction | SetLanguageFilterAction | SetPublicationFilterAction | SetBaseDocumentFilterAction| ResetSearchAction |
    FetchLanguagesAction | ReceiveLanguagesAction | FetchBaseDocumentsAction | ReceiveBaseDocumentsAction

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    searchTemplates: (viewSetting: UserViewSetting): AppThunkAction<KnownAction> => async (dispatch, getState) => {
        // Only load data if it's something we don't already have (and are not already loading)
        const appState = getState();
        if (appState && appState.search) {
            const searchParameters : SearchParameters = {
                query: appState.search.query,
                languageFilter: appState.search.languageFilter?.id ?? "",
                baseDocumentFilter: appState.search.baseDocumentFilter?.id ?? "",
                orgaEntityIdFilter: appState.search.publicationFilter.orgaEntityId ?? "",
                hierarchyFilter: appState.search.publicationFilter.hierarchy,
                paginationInfo: {
                    orderAsc: viewSetting?.orderAsc ?? true,
                    orderByProperty: viewSetting?.orderByProperty ?? OrderByPropertyEnum.templateName
                }
            }
            
            dispatch({ type: SearchActionTypes.Search });
            const data = await api.search(searchParameters);
            dispatch({ type: SearchActionTypes.ReceiveResults, results: data });
        }
    },

    fetchMyTemplates: (viewSetting: UserViewSetting): AppThunkAction<KnownAction> => async (dispatch, getState) => {
        // Only load data if it's something we don't already have (and are not already loading)
        const appState = getState();
        if (appState && appState.search) {
            const searchParameters: SimpleSearchParameters = {
                query: appState.search.query,
                languageFilter: appState.search.languageFilter?.id ?? "",
                baseDocumentFilter: appState.search.baseDocumentFilter?.id ?? "",
                paginationInfo: {
                    orderAsc: viewSetting?.orderAsc ?? true,
                    orderByProperty: viewSetting?.orderByProperty ?? OrderByPropertyEnum.templateName
                }
            }

            dispatch({ type: SearchActionTypes.Search });
            const data = await api.getMyTemplates(searchParameters);
            dispatch({ type: SearchActionTypes.ReceiveResults, results: data });
        }
    },

    setSearchQuery: (query: string): AppThunkAction<KnownAction> => (dispatch) => {
        dispatch({ type: SearchActionTypes.SetSearchQuery, query: query });
    },

    fetchLanguages: (): AppThunkAction<KnownAction> => async (dispatch, getState) => {
        const appState = getState();
        if (appState && appState.search) {
            dispatch({ type: SearchActionTypes.FetchLanguages });
            const data = await api.getLanguages();
            dispatch({ type: SearchActionTypes.ReceiveLanguages, languages: data });
        }
    },

    fetchBaseDocuments: (): AppThunkAction<KnownAction> => async (dispatch, getState) => {
        const appState = getState();
        if (appState && appState.search) {
            dispatch({ type: SearchActionTypes.FetchBaseDocuments });
            const data = await api.getBaseDocumentFilters();
            dispatch({ type: SearchActionTypes.ReceiveBaseDocuments, baseDocuments: data });
        }
    },

    setLanguageFilter: (languageId: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: SearchActionTypes.SetLanguageFilter, id: languageId });
    },

    setPublicationFilter: (filter: PublicationFilter): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: SearchActionTypes.SetPublicationFilter, filter: filter});
    },

    setBaseDocumentFilter: (baseDocumentId: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: SearchActionTypes.SetBaseDocumentFilter, id: baseDocumentId });
    },

    resetSearch: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: SearchActionTypes.ResetSearch });
    },
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

export const unloadedSearchState: SearchState = {
    isLoading: false,
    refetchTrigger: 0,
    query: "",
    results: [],
    languages: [],
    baseDocuments: [],
    languageFilter: undefined,
    publicationFilter: {
        orgaEntityId: '',
        hierarchy: true
    },
    baseDocumentFilter: undefined,
};

export const reducer: Reducer<SearchState> =
    (state: SearchState | undefined, incomingAction: Action): SearchState => {
        if (state === undefined) {
            return unloadedSearchState;
        }

        const action = incomingAction as KnownAction;
        switch (action.type) {
        case SearchActionTypes.SetSearchQuery:
            return {
                ...state,
                query: action.query
            };
        case SearchActionTypes.Search:
            return {
                ...state,
                isLoading: true
            };
        case SearchActionTypes.ReceiveResults:
            return {
                ...state,
                results: action.results,
                isLoading: false
            };
        case SearchActionTypes.SetLanguageFilter:
            return {
                ...state,
                languageFilter: state.languages.find(l => l.id === action.id)
            };
        case SearchActionTypes.SetPublicationFilter:
            return {
                ...state,
                publicationFilter: action.filter
            };
        case SearchActionTypes.SetBaseDocumentFilter:
            return {
                ...state,
                baseDocumentFilter: state.baseDocuments.find(bd => bd.id === action.id)
            };

        case SearchActionTypes.ResetSearch:
            return {
                ...unloadedSearchState,
                baseDocuments: state.baseDocuments,
                languages: state.languages,
                isLoading: state.isLoading,
                refetchTrigger: state.refetchTrigger + 1
            };

        case SearchActionTypes.ReceiveLanguages:
            return {
                ...state,
                languages: action.languages,
                };

        case SearchActionTypes.ReceiveBaseDocuments:
            return {
                ...state,
                baseDocuments: action.baseDocuments.sort((a, b) => a.name.localeCompare(b.name))
            };

        default:
            return state;
        }
    };
