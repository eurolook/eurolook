import * as React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Layout }  from 'components/Layout';
import { Storefront } from 'components/Storefront';
import { IntegratedMode } from 'components/IntegratedMode';
import { TemplateSearchPage } from 'components/search/TemplateSearchPage';
import { TemplateFilter } from 'components/search/TemplateFilterPages';
import { MyTemplates }  from 'components/MyTemplates';
import { TemplateDetailsPage } from 'components/template/TemplateDetailsPage';
import { TemplateEditingPage } from 'components/template/TemplateEditingPage';
import { ManageTemplateFilesPage } from 'components/template/files/ManageTemplateFilesPage';
import { RecoilRoot} from 'recoil';
import { toast, ToastContainer, Flip } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { RoutingPaths } from 'Routing';

import './custom.scss'
import { useEffect} from 'react';
import { useDispatch } from 'react-redux';
import { useDisplayPreferencesFetch } from './DisplayPreferences';
import { actionCreators as searchActions } from 'store/Search';
import { QuickFilters } from './components/QuickFilters';
import { ManagePublicationsPage } from './components/template/publication/ManagePublicationsPage';
import { ManageOwnersPage } from './components/template/owners/ManageOwnersPage';

toast.configure();

export function AppInitialization() {
    const dispatch = useDispatch();
    const { fetchDisplayPreferences } = useDisplayPreferencesFetch();

    useEffect(() => {
        async function fetchMetadata() {
            dispatch(searchActions.fetchLanguages());
            dispatch(searchActions.fetchBaseDocuments());
            fetchDisplayPreferences();
        }

        fetchMetadata();
    }, [dispatch, fetchDisplayPreferences]);

    return <></>
}

export function App(props: { basename: string }) {
    return (
        <RecoilRoot>
            <BrowserRouter basename={props.basename}>
                <Layout>                    
                    <Routes>
                        <Route path="/" element={<Storefront />} />
                        <Route path={RoutingPaths.storefront} element={<Storefront />} />
                        <Route path={RoutingPaths.integratedMode} element={<IntegratedMode />} />
                        <Route path={RoutingPaths.search} element={<TemplateSearchPage />} />
                        <Route path={RoutingPaths.templateFilterPagePattern} element={<TemplateFilter />} />
                        <Route path={RoutingPaths.myTemplates} element={<MyTemplates />} />
                        <Route path={RoutingPaths.quickFilters} element={<QuickFilters />} />
                        <Route path={RoutingPaths.templateDetailsPagePattern} element={<TemplateDetailsPage />} />
                        <Route path={RoutingPaths.templateEditingPagePattern} element={<TemplateEditingPage />} />
                        <Route path={RoutingPaths.templateFilesPagePattern} element={<ManageTemplateFilesPage />} />
                        <Route path={RoutingPaths.templatePublicationsPagePattern} element={<ManagePublicationsPage />} />
                        <Route path={RoutingPaths.templateOwnersPagePattern} element={<ManageOwnersPage />} />
                    </Routes>
                </Layout>
            </BrowserRouter>

            <ToastContainer
                toastClassName='toast-container'
                bodyClassName='toast-container-body'
                autoClose={5000}
                hideProgressBar
                newestOnTop={false}
                transition={Flip}
                closeOnClick
                rtl={false}
                draggable={false}
                pauseOnHover
                position="top-center" />
            <AppInitialization />
        </RecoilRoot>
    );
}
