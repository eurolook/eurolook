import React, { ReactNode } from 'react';
import styled from 'styled-components';
import { Box, FlexDiv, StyledInput } from 'theme';

const StyledBox = styled(Box)`
    margin-bottom: 4px;
    padding-right: 12px;
    display flex;
    justify-content: space-between;    
    &:hover {
        background-color: ${props => props.theme.secondaryColorAccent};
        transition: .5s ease;
    }
  cursor: pointer;
`;

const CenteredFlexDiv = styled(FlexDiv)`
    align-items: center;
`

const CheckboxInput = styled(StyledInput)`
    align-self: center;
`;

export function CheckboxLine(props: { checked: boolean; toggle: () => void; children: ReactNode; }) {

    const { checked, toggle, children } = props;

    return <StyledBox onClick={toggle}>
        <CenteredFlexDiv>
            {children}
        </CenteredFlexDiv>
        <CheckboxInput type="checkbox" checked={checked} onChange={toggle} />
    </StyledBox>;
}
