import React, { useEffect, useState } from 'react';
import { ModalTriggerButton } from 'components/modals/ModalTriggerButton';
import { PageTitle, Page, Header, SubPageContainer, PageLeft, PageAlert} from 'components/page/Page';
import { closestCenter, DndContext, PointerSensor, useSensor, useSensors } from '@dnd-kit/core';
import { arrayMove, SortableContext, verticalListSortingStrategy } from '@dnd-kit/sortable';
import { OrgaEntity, QuickFilter } from 'Models';
import { useSortable } from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';
import { useRecoilValue } from 'recoil';
import { userInfoAtom } from './sidebar/UserInfo';
import { OrgaEntitySearchModal } from './modals/OrgaEntitySearchModal';
import { Box, ButtonGroup, IconContainer, ItemsContainerAction, Rect, SpacerDiv, StyledButton } from '../theme';
import styled from 'styled-components';
import { TrashIcon, BuildingOffice2Icon, PlusIcon } from '@heroicons/react/24/outline';
import { useNavigate } from 'react-router-dom';
import { useQuickFilters } from '../DisplayPreferences';
import { WaitForData } from './WaitForData';

const StyledButtonGroup = styled(ButtonGroup)`
  margin-top: 20px;
  margin-bottom: 20px;
`;

const StyledBox = styled(Box)`
  margin-bottom: 4px;
  display flex;
    justify-content: space-between;
    padding-right: 4px;
`;

const StyledItemContainerAction = styled(ItemsContainerAction)`
  margin-top: 10px;
`;

function SortableItem(props: { quickFilter: QuickFilter, deleteCallback: () => void }) {

    const { quickFilter, deleteCallback } = props;

    const {
        attributes,
        listeners,
        setNodeRef,
        transform,
        transition,
    } = useSortable({ id: quickFilter.id });

    const style = {
        transform: CSS.Transform.toString(transform),
        transition,
    };

    return (
        <StyledBox ref={setNodeRef} style={style} {...attributes} {...listeners}>
            <div>
                <IconContainer addRightSpace><BuildingOffice2Icon />{`${quickFilter.name}`}</IconContainer>
            </div>
            <div>
                <IconContainer onClick={deleteCallback} highlightOnHover><TrashIcon /></IconContainer>
            </div>
        </StyledBox>
    );
}


export function QuickFilters() {
    const userInfo = useRecoilValue(userInfoAtom);

    const navigate = useNavigate();

    const sensors = useSensors(
        useSensor(PointerSensor, {
            activationConstraint: {
                distance: 8,
            },
        })
    )

    const { quickFilters, setQuickFilters, isInitialized } = useQuickFilters();
    const userHierarchyQuickFilters = userInfo.orgaEntities.map<QuickFilter>(o => ({ id: o.id, name: o.name }));
    const [items, setItems] = useState<QuickFilter[]>([]);

    useEffect(() => {
        setItems(quickFilters);
    }, [isInitialized, quickFilters]);

    function handleDragEnd(event: any) {
        const { active, over } = event;
        if (active.id !== over.id) {
            const oldIndex = items.findIndex(i => i.id === active.id);
            const newIndex = items.findIndex(i => i.id === over.id);           
            setItems(arrayMove(items, oldIndex, newIndex));
        }
    }

    function addQuickFilter(e: OrgaEntity) {
        setItems([...items, {id: e.id, name: e.name} ]);
    }

    const isValid = (items.length <= 10);
    const infoMessage = items.length > 0 ? "Use drag and drop to rearrange custom filters" : "No custom filters configured";

    return (
        <Page>
            <SubPageContainer>
                <PageLeft>
                    <WaitForData dataAvailable={isInitialized}>
                        <Header $tileView={false}>
                            <PageTitle>Manage Custom Filters</PageTitle>
                        </Header>
                        <Rect>
                            <PageAlert color="info" $tileView={false}>{infoMessage}</PageAlert>
                            <DndContext
                                sensors={sensors}
                                collisionDetection={closestCenter}
                                onDragEnd={handleDragEnd}>
                                <SortableContext
                                    items={items}
                                    strategy={verticalListSortingStrategy}>
                                    {
                                        items.map(qf => <SortableItem quickFilter={qf} deleteCallback={() => setItems([...items.filter(i => i.id !== qf.id)])} key={qf.id} />)
                                    }
                                </SortableContext>
                            </DndContext>
                            <SpacerDiv />
                            <StyledItemContainerAction>
                                <ModalTriggerButton
                                    color="secondary"
                                    modal={
                                        {
                                            component: OrgaEntitySearchModal,
                                            props: {
                                                title: "Add Custom Filter",
                                                confirmAction: addQuickFilter,
                                                isSearchResultDisabled: (o: OrgaEntity) => [...userHierarchyQuickFilters, ...items].some(i => i.id === o.id),
                                            }
                                        }}>
                                    <IconContainer addRightSpace><PlusIcon />Add Custom Filter</IconContainer>
                                </ModalTriggerButton>
                            </StyledItemContainerAction>
                        </Rect>
                        {!isValid && <PageAlert color="danger" $tileView={false}>{`The maximum number of custom filters (10) exceeded`}</PageAlert>}
                        <StyledButtonGroup>
                            <StyledButton color="primary" onClick={() => { setQuickFilters(items); navigate(-1); }} disabled={!isValid}>Save</StyledButton>
                            <StyledButton color="secondary" onClick={() => navigate(-1)}>Cancel</StyledButton>
                        </StyledButtonGroup>
                    </WaitForData>
                </PageLeft>
            </SubPageContainer>
        </Page>
    );
};
