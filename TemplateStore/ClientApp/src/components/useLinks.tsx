import { RoutingPaths } from "../Routing";

export function useLinks() {

    const host = `${window.location.host}`;

    function getEl10LinkToTemplate(templateId: string) {
        return `el10://templatedownload/${templateId}/${host}`;
    }

    function getHttpLinkToTemplateDetailsPage(templateId: string) {
        return `${window.location.protocol}//${window.location.host}${process.env.REACT_APP_BASE_PATH?.slice(0, -1)}${RoutingPaths.getTemplateDetailsPage(templateId)}`;
    }

    return { getEl10LinkToTemplate, getHttpLinkToTemplateDetailsPage };
}
