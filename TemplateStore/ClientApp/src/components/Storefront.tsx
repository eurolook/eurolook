import React, { useCallback, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { SearchBar } from 'components/search/SearchBar';
import { TemplateTileGrid } from 'components/templatesView/TemplateTileGrid';
import { ApplicationState } from 'store';
import { actionCreators as storeFrontActions } from 'store/Storefront';
import { RoutingPaths } from 'Routing';
import { LinkButton } from 'components/LinkButtons';
import { Page, Header } from 'components/page/Page';
import { WaitForData } from './WaitForData';
import { useNavigate } from 'react-router-dom';
import { actionCreators as searchActions } from 'store/Search';
import { useViewSettings } from '../DisplayPreferences';

export function Storefront() {

    const state = useSelector((state: ApplicationState) => state.storefront);
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const { viewSetting } = useViewSettings("storefront");

    useEffect(() => {
            dispatch(storeFrontActions.requestStorefrontTemplates());
        }, [dispatch]);

    const search = useCallback(async () => {
        await dispatch(searchActions.searchTemplates(viewSetting));
        navigate(RoutingPaths.search);
    }, [dispatch, navigate, viewSetting]);

    return (
        <React.Fragment>
            <div className="banner">
                <SearchBar tileView={true} searchCallback={search} />
            </div>
            <Page>
                <WaitForData dataAvailable={!state.isLoading} >
                    {state.sections.map((s, i) => (
                        <React.Fragment key={s.name}>
                            <Header $tileView={true}>
                                <span className="page-title">
                                    {<span>{s.name}</span>}
                                    {s.templateFilter && <span>&nbsp;&nbsp;<LinkButton to={RoutingPaths.getTemplateFilterPage(s.templateFilter)}>More...</LinkButton></span>}
                                </span>
                            </Header>
                            <TemplateTileGrid templates={s.templates} emptyMessage="No templates available." smallTiles={i !== 0} />
                        </React.Fragment>
                    ))}
                </WaitForData>
            </Page>
        </React.Fragment>
    );
}
