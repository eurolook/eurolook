import React, { useCallback, useEffect } from 'react';
import { SearchBar } from './SearchBar';
import { useDispatch, useSelector } from 'react-redux';
import { ApplicationState } from 'store';
import { SearchState, } from 'store/Search';
import { PublicationFilter } from './filters/PublicationFilter';
import { LanguageFilter } from './filters/LanguageFilter';
import { BaseDocumentFilter } from './filters/BaseDocumentFilter';
import { Page, Header, PageActions } from 'components/page/Page';
import { ViewDropdown,SortingDropdown } from '../templatesView/ViewDropdowns';
import { TemplatesResultsView } from './TemplatesResultsView';
import { actionCreators as searchActions } from 'store/Search';
import { UserViewSetting } from '../../Models';
import { useViewSettings } from '../../DisplayPreferences';

export const TemplateSearchPage = () => {
    const viewId = "search";
    const searchState: SearchState = useSelector((state: ApplicationState) => state.search);
    const { viewSetting, isInitialized } = useViewSettings(viewId);
    const tileView = viewSetting.tileView;
    const noResultsMessage = "Your search did not match any templates.";
    const dispatch = useDispatch();

    const search = useCallback(async (newViewSetting?: UserViewSetting) => {
        if (isInitialized) {
            await dispatch(searchActions.searchTemplates(newViewSetting ?? viewSetting));
        }
    }, [dispatch, viewSetting, isInitialized] );

    useEffect(() => {
        search();                
    }, [search, searchState.baseDocumentFilter, searchState.languageFilter, searchState.publicationFilter, searchState.refetchTrigger]);

    return (
        <React.Fragment>
            <div className="banner">
                <SearchBar tileView={viewSetting.tileView} searchCallback={search} />
            </div>
            <Page>
                <Header $tileView={tileView}>
                    <span className="page-title">Search Results</span>
                    <PageActions $tileView={tileView}>
                        <PublicationFilter />
                        <LanguageFilter />
                        <BaseDocumentFilter />
                        {tileView && <SortingDropdown viewId={viewId} />}
                        <ViewDropdown viewId={viewId} />
                    </PageActions>
                </Header>
                <TemplatesResultsView templates={searchState.results} viewId={viewId} emptyMessage={noResultsMessage} isLoading={!isInitialized || searchState.isLoading} />
            </Page>
        </React.Fragment>
    );
}
