import React from 'react';
import { TemplateTileGrid } from 'components/templatesView/TemplateTileGrid';
import { PageAlert } from 'components/page/Page';
import { TemplateListView } from '../templatesView/TemplateListView';
import { TemplateSummary } from '../../Models';
import { WaitForData } from '../WaitForData';
import { useViewSettings } from '../../DisplayPreferences';


export function TemplatesResultsView(props: { templates: TemplateSummary[]; viewId: string; emptyMessage: string; isLoading: boolean }) {

    const { templates, viewId, isLoading, emptyMessage } = props;
    const { viewSetting } = useViewSettings(viewId);
    const tileView = viewSetting.tileView;
    const templatesToShow = templates.slice(0, 50);

    return (
        <WaitForData dataAvailable={!isLoading}>
            {templates.length > 50 &&
                <PageAlert color="warning" $tileView={tileView}>{`First 50 results are shown. Use a filter or refine your search to limit the results.`}</PageAlert>}
            {!tileView && <TemplateListView templates={templatesToShow} emptyMessage={emptyMessage} viewId={viewId} />}
            {tileView && <TemplateTileGrid templates={templatesToShow} emptyMessage={emptyMessage} />}
        </WaitForData>
    );
}
