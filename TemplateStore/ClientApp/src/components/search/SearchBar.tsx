import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { InputGroup, Form } from 'reactstrap';
import { ApplicationState } from 'store';
import { actionCreators as searchActions } from 'store/Search';
import { MagnifyingGlassIcon } from '@heroicons/react/24/outline';
import styled from 'styled-components'
import { MediaQueries, StyledInput, StyledIconButton, ListViewMediaQueries, IconContainer } from 'theme';

const StyledInputGroup = styled(InputGroup)`
    max-width: ${props => props.theme.pageWidthXl}px;
    min-width: ${props => props.theme.tileWidth}px;    

    ${(props: { $tileView: boolean }) => props.$tileView && MediaQueries }
    ${(props: { $tileView: boolean }) => !props.$tileView && ListViewMediaQueries }
`;

const StyledForm = styled(Form)`
    display: flex;
    flex-grow: 1;
`;

export function SearchBar(props: { small?: boolean, tileView: boolean, searchCallback: () => void }) {

    const { small, tileView, searchCallback } = props;
    const dispatch = useDispatch();
    const searchState = useSelector((state: ApplicationState) => state.search);

    async function handleChange(e: React.FormEvent<EventTarget>) {
        const value = (e.target as HTMLInputElement).value;
        await dispatch(searchActions.setSearchQuery(value));
    }

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        searchCallback();
    }

    return (
        <StyledInputGroup $small={small} $tileView={tileView}>
            <StyledForm onSubmit={handleSubmit}>
                <StyledInput placeholder="Search" onChange={handleChange} value={searchState.query} id="searchBox" autoFocus />
                <StyledIconButton type="submit">
                    <IconContainer highlightOnHover><MagnifyingGlassIcon /></IconContainer>
                </StyledIconButton>
            </StyledForm>
        </StyledInputGroup>
    );
}
