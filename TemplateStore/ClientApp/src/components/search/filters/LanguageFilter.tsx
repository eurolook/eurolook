import * as React from 'react';
import { CustomDropdown } from 'components/CustomDropdown';
import { Language } from 'Models';
import { useSelector, useDispatch } from 'react-redux';
import { ApplicationState } from 'store';
import { SearchState } from 'store/Search';
import { actionCreators as searchActions } from 'store/Search';

export const LanguageFilter = () => {
    const searchState: SearchState = useSelector((state: ApplicationState) => state.search);
    const dispatch = useDispatch();

    const dropdownItems = searchState.languages.map((l: Language) => (
        {
            key: l.id,
            display: l.displayName,
            onClick: () => { dispatch(searchActions.setLanguageFilter(l.id)) },
        }));

    dropdownItems.unshift({
        key: "Any Language",
        display: "Any Language",
        onClick: () => {
            dispatch(searchActions.setLanguageFilter(""));
        }
    });

    const dropdownTitle = searchState?.languageFilter?.displayName ?? dropdownItems[0].display;
   
    return (
        <CustomDropdown dropdownTitle={dropdownTitle}>{dropdownItems}</CustomDropdown>
    );
}
