import * as React from 'react';
import { CustomDropdown } from 'components/CustomDropdown';

import { useSelector, useDispatch } from 'react-redux';
import { ApplicationState } from 'store';
import { SearchState } from 'store/Search';
import { actionCreators as searchActions } from 'store/Search';
import { useRecoilValue } from 'recoil';
import { userInfoAtom } from '../../sidebar/UserInfo';
import { useQuickFilters } from '../../../DisplayPreferences';

export const PublicationFilter = () => {
    const searchState: SearchState = useSelector((state: ApplicationState) => state.search);
    const dispatch = useDispatch();
    const userInfo = useRecoilValue(userInfoAtom);
    const { quickFilters } = useQuickFilters();

    const filters = [...userInfo.orgaEntities, ...quickFilters];

    const publicationDropDownItems = filters.map(f => {
        return {
            key: f.id,
            display: `${f.name} Templates`,
            onClick: () => {
                dispatch(searchActions.setPublicationFilter(
                    {
                        orgaEntityId: f.id,
                        hierarchy: false,
                    }));
            },
        }
    });

    if (userInfo.isAdmin) {
        publicationDropDownItems.push({
            key: '',
            display: 'Entire Store',
            onClick: () => {
                dispatch(searchActions.setPublicationFilter({
                    orgaEntityId: '',
                    hierarchy: false
                }));
            }
        });
    }

    publicationDropDownItems.unshift({
        key: 'hierarchy',
        display: 'Published for you',
        onClick: () => {
            dispatch(searchActions.setPublicationFilter({
                orgaEntityId: '',
                hierarchy: true
            }));
        }
    });

    const searchKey = (!searchState || searchState.publicationFilter.hierarchy)
        ? 'hierarchy'
        : searchState.publicationFilter.orgaEntityId;

    const publicationDropdownTitle = publicationDropDownItems.find(i => i.key === searchKey)?.display ?? publicationDropDownItems[0].display;
   
    return (
        <CustomDropdown dropdownTitle={publicationDropdownTitle}>{publicationDropDownItems}</CustomDropdown>
    );
}

