import * as React from 'react';
import { CustomDropdown } from 'components/CustomDropdown';
import { BaseDocument } from 'Models';
import { useSelector, useDispatch } from 'react-redux';
import { ApplicationState } from 'store';
import { SearchState } from 'store/Search';
import { actionCreators as searchActions } from 'store/Search';
import { NoOverflowSpan } from '../../../theme';

export const BaseDocumentFilter = () => {
    const searchState: SearchState = useSelector((state: ApplicationState) => state.search);
    const dispatch = useDispatch();    

    let dropdownItems = searchState.baseDocuments.map((bd: BaseDocument) => (
        {
            key: bd.id,
            display: bd.name,
            onClick: () => { dispatch(searchActions.setBaseDocumentFilter(bd.id)) },
        }));

    dropdownItems.unshift({
        key: "Any Eurolook Template",
        display: "Any Eurolook Template",
        onClick: () => {
            dispatch(searchActions.setBaseDocumentFilter(""));
        }
    });

    const dropdownTitle = <NoOverflowSpan>{searchState?.baseDocumentFilter?.name ?? dropdownItems[0].display}</NoOverflowSpan>;
   
    return (
        <CustomDropdown dropdownTitle={dropdownTitle}>{dropdownItems}</CustomDropdown>
    );
}
