import React, { useCallback, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { ApplicationState } from 'store';
import { actionCreators as searchActions } from 'store/Search';
import { LanguageFilter } from './filters/LanguageFilter';
import { BaseDocumentFilter } from './filters/BaseDocumentFilter';
import { TemplateSummary, PublicationFilter, UserViewSetting } from 'Models';
import { useParams } from "react-router-dom";
import { Page, Header, PageActions, PageTitle } from 'components/page/Page';
import { ViewDropdown, SortingDropdown } from '../templatesView/ViewDropdowns';
import { TemplatesResultsView } from './TemplatesResultsView';
import { userInfoAtom } from '../sidebar/UserInfo';
import { useRecoilValue } from 'recoil';
import { SearchBar } from './SearchBar';
import { useQuickFilters, useViewSettings } from '../../DisplayPreferences';

const TemplateFilterPageRender = (props: {
    pageTitle: string,
    viewId: string,
    templates: TemplateSummary[],
    searchCallback: (viewSetting?: UserViewSetting) => void,
    isLoading: boolean
}) => {

    const { pageTitle, viewId, templates, searchCallback, isLoading } = props;
    const { viewSetting } = useViewSettings(viewId);
    const tileView = viewSetting.tileView;

    return (
        <React.Fragment>
            <Page>
                <Header $tileView={tileView}>
                    <PageTitle>{pageTitle}</PageTitle>
                </Header>
                <PageActions $tileView={tileView}>
                    <SearchBar small={true} tileView={tileView} searchCallback={searchCallback} />
                    <PageActions $tileView={tileView}>
                        <LanguageFilter />
                        <BaseDocumentFilter />
                        {tileView && <SortingDropdown viewId={viewId} />}
                        <ViewDropdown viewId={viewId} />
                    </PageActions>
                </PageActions>
                <TemplatesResultsView templates={templates} viewId={viewId} emptyMessage="No results." isLoading={isLoading} />
            </Page>
        </React.Fragment>
    );
}

const TemplateFilterPage = (props: { pageTitle: string, publicationFilter: PublicationFilter }) => {
    const searchState = useSelector((state: ApplicationState) => state.search);
    const viewId = `filter_${props.publicationFilter.orgaEntityId}`;
    const { viewSetting, isInitialized } = useViewSettings(viewId);
    const dispatch = useDispatch();

    const search = useCallback(async (newViewSetting?: UserViewSetting) => {
        if (isInitialized) {
            await dispatch(searchActions.searchTemplates(newViewSetting ?? viewSetting));
        }
    }, [dispatch, viewSetting, isInitialized]);

    useEffect(() => {
        dispatch(searchActions.setPublicationFilter(props.publicationFilter));
    }, [dispatch, props.publicationFilter]);

    useEffect(() => {
        search();
    }, [search, searchState.languageFilter, searchState.baseDocumentFilter, searchState.refetchTrigger])

    const templates = searchState.results;

    return (
        <TemplateFilterPageRender
            pageTitle={props.pageTitle}
            templates={templates}
            viewId={viewId}
            searchCallback={search}
            isLoading={!isInitialized || searchState.isLoading} />
    );
}


export const TemplateFilter = () => {

    const { filter } = useParams<{filter: string }>();
    const userInfo = useRecoilValue(userInfoAtom);
    const { quickFilters } = useQuickFilters();

    const quickFilter = quickFilters.find(e => e.id === filter) ?? userInfo.orgaEntities.find(e => e.id === filter);
    const publicationFilter = { orgaEntityId: quickFilter?.id ?? '', hierarchy: false }
    const pageTitle = quickFilter ? `Templates Published to ${quickFilter.name}` : 'N/A';

    return (
        <React.Fragment>
            {(publicationFilter.hierarchy || publicationFilter.orgaEntityId) && <TemplateFilterPage pageTitle={pageTitle} publicationFilter={publicationFilter} />}
        </React.Fragment>
    );
};
