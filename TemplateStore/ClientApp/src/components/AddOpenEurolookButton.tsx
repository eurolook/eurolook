import React from "react";
import { toast } from "react-toastify";
import { ButtonProps, UncontrolledTooltip } from "reactstrap";
import { useRecoilState } from "recoil";
import styled from "styled-components";
import { saveAsUserTemplate } from "../api";
import { TemplateSummary } from "../Models";
import { StyledButton } from "../theme";
import { userInfoAtom } from "./sidebar/UserInfo";


const ActionButton = styled(StyledButton).attrs(props => ({
    className: "btn"
}))
    `
    min-width: 120px;
`;

export function AddOpenEurolookButton(props: { template: TemplateSummary } & ButtonProps) {

    const { template, ...other } = props;
    const host = `${window.location.host}`;

    const [userInfo, setUserInfo] = useRecoilState(userInfoAtom);
    const isUserTemplate = userInfo.userTemplates.has(template.id);

    async function addToEurolook() {
        try {
            await saveAsUserTemplate(template.id);
            const userTemplates = userInfo.userTemplates;
            userTemplates.add(template.id);
            const newUserInfo = { ...userInfo, userTemplates: userTemplates }
            setUserInfo(newUserInfo);
            toast.success(<div>Successfully added template to your user profile.<br /><br />It will become visible in your Eurolook Create Document dialog.</div>);
        } catch (e: any) {
            console.error(e.message);
            toast.error("Failed to add template to user profile");
        }
    }

    const openInEurolookId = `openInEurolook${template.id}`;
    const addToEurolookId = `addToEurolook${template.id}`;

    const showAddToEurolook = !isUserTemplate && template.isPublic;

    const openInEurolookText = template.isPublic
        ? "Open in Eurolook"
        : "Test in Eurolook";
    const openInEurolookTooltip = template.isPublic
        ? "Create a Eurolook document based on this custom template"
        : "Test your custom template in Eurolook";

    const openInEurolookButtonColor = template.isPublic
        ? "primary"
        : "highlight"

    return (
        <>
            {!showAddToEurolook &&
                <>
                    <ActionButton color={openInEurolookButtonColor} href={`el10://templatedownload/${template.id}/${host}/0`} id={openInEurolookId} {...other}>
                        {openInEurolookText}
                    </ActionButton>
                    <UncontrolledTooltip placement="top" target={openInEurolookId}>
                        {openInEurolookTooltip}
                    </UncontrolledTooltip>
                </>
            }
            {showAddToEurolook && 
                <>
                    <ActionButton onClick={addToEurolook} id={addToEurolookId} {...other}>
                        Add to Eurolook
                    </ActionButton>
                    <UncontrolledTooltip placement="top" target={addToEurolookId}>
                        Add this custom template to your Eurolook profile
                    </UncontrolledTooltip>
                </>
            }
        </>
    );
}
