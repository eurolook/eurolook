import * as React from 'react';
import styled from "styled-components";
import { LargeSpinner } from '../theme';

const StyledSpinner = styled(LargeSpinner)`
    margin-top: 40px;
`;

export function WaitForData({ dataAvailable, children }: { dataAvailable: boolean, children: React.ReactNode }) {

    return !dataAvailable
        ? <StyledSpinner />
        : <>{children}</>
}
