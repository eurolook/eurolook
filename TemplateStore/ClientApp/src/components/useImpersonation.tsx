import { useDispatch } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import { actionCreators as generalActions } from 'store/General';
import { useDisplayPreferencesFetch } from '../DisplayPreferences';
import { RoutingPaths } from '../Routing';


export function useImpersonation() {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const location = useLocation();
    const { fetchDisplayPreferences } = useDisplayPreferencesFetch();

    async function impersonate(login: string) {
        await dispatch(generalActions.setImpersonatedUser(login));
        await fetchDisplayPreferences();   
        if (location.pathname !== RoutingPaths.storefront) {
            navigate(RoutingPaths.storefront);
        }
    };

    async function resetImpersonation() {
        await dispatch(generalActions.setImpersonatedUser(''));
        await fetchDisplayPreferences();
        if (location.pathname !== RoutingPaths.storefront) {
            navigate(RoutingPaths.storefront, { replace: true });
        }
    };

    return { impersonate, resetImpersonation };
}
