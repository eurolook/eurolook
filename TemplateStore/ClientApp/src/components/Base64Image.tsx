import * as React from 'react';

export type Base64ImageProps = { base64String?: string, alt: string, className?: string }

export const Base64Image = React.memo((props: Base64ImageProps) => {

    const imgSrcPrefix = "data:image/png;base64,";
    const { base64String, alt, ...other } = props;
    const src = base64String?.startsWith("data:") ? base64String : `${imgSrcPrefix}${base64String}`;

    return (
        <React.Fragment>
            {base64String && <img {...other} alt={alt} src={src}/> }
            {!base64String && <div></div>}
        </React.Fragment>);
});
