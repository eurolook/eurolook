import * as React from 'react';

import { Box } from 'theme';
import styled from 'styled-components/macro';

import './ActionDisplayRow.scss'

const FlexDiv = styled.div`
    display: flex;
    min-width: 0;
    overflow: hidden;
`;

const LeftBox = styled(Box)`
    flex-grow: 1;
    border-right: none;
    min-width: 0;
    overflow: hidden;
`;

export const ActionDisplayRow = (props: { children: React.ReactNode[] }) => {
    return (
        <FlexDiv {...props}>
            <LeftBox>
                {props.children[0]}
            </LeftBox>
            {props.children[1]}
        </FlexDiv>);
};
