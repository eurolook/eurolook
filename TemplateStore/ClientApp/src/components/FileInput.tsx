import * as React from 'react';
import { InputProps, Label } from 'reactstrap';
import { StyledInput } from 'theme';
import styled from 'styled-components/macro';

const StyledLabel = styled(Label)`
    &:active {
        box-shadow: 0 0 0 0.25rem rgb(130 138 145 / 50%)
    }
    transition: box-shadow 0.25s ease-in-out;
`;

export const FileInput = (props: InputProps & {children: React.ReactElement}) => {
    const { type, onClick, onChange, children, ...other } = props;
    return (
        <StyledLabel>
            <StyledInput type="file" onClick={(e: Event) => ((e.currentTarget as HTMLInputElement).value = '')} onChange={onChange} {...other} />
            {children}
        </StyledLabel>
    );
};
