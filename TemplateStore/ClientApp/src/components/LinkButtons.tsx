import * as React from 'react';
import { ButtonProps } from 'reactstrap';
import { Link, LinkProps } from 'react-router-dom';
import { TransparentButton, StyledButton } from 'theme';

export type ILinkButtonProps = ButtonProps & LinkProps;

export const LinkButton = (props: ILinkButtonProps) => {

    const { to, target, ...other } = props;

    const button = <TransparentButton {...other}>{props.children}</TransparentButton>;

    return (
        <React.Fragment>
            {!props.disabled && 
                <Link to={to} target={target}>
                    {button}
                </Link>}

            {props.disabled && button}
        </React.Fragment>);
};


export type IDownloadButtonProps = { href: string, children?: React.ReactNode } & ButtonProps;

export const DownloadButton = (props: IDownloadButtonProps) => {

    const { href, color, ...other } = props;

    const button = (color !== "transparent") ? <StyledButton {...other} color={color}>{props.children}</StyledButton>
        : <TransparentButton {...other} color={color}>{props.children}</TransparentButton>;

    return (
        <React.Fragment>
            {!props.disabled && 
                <a href={href} download>
                    {button}
                </a>}

            {props.disabled && button}
        </React.Fragment>);
};
