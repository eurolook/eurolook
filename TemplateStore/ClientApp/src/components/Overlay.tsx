import React from "react"
import { ButtonProps } from "reactstrap"
import styled from "styled-components"
import { FlexDiv, StyledButton } from "../theme"

const OverlayBackground = styled(StyledButton)`  
    position: absolute;
    z-index: 2;
    top: 0;
    opacity: 0;
    //color: #FFF;
    width: 100%;
    height: 100%;
    transition: .5s ease;
    background-color: ${props => props.theme.secondaryBorderColor};
    color: ${props => props.theme.primaryColor};
    display: flex;
    align-items: center;
    justify-content: center;

   :hover {
    opacity: 0.95;
    color: ${props => props.theme.primaryColor};
  }
}
`

export const OverlayContainerDiv = styled.div`
    position: relative;
    width:fit-content;
`



export function OverlayContent(props: { children: React.ReactNode | React.ReactNode[] } & ButtonProps) {

    const { children, ...other } = props;

    return <OverlayBackground {...other}>
        <FlexDiv>
            {children}
        </FlexDiv>
    </OverlayBackground>
}

