import * as React from 'react';
import { UncontrolledTooltip } from 'reactstrap';
import { HTMLAttributes, useEffect, useRef, useState } from 'react';

export function DivWithTooltipOnOverflow(props: { id: string; children: React.ReactNode; } & HTMLAttributes<HTMLDivElement>) {

    const { children, id, ...other } = props;
    const [overflowActive, setOverflowActive] = useState<boolean>(false);
    const overflowingText = useRef<HTMLDivElement | null>(null);

    function checkOverflow(textContainer: HTMLDivElement | null) {
        if (textContainer) {
            return (
                textContainer.offsetHeight < textContainer.scrollHeight || textContainer.offsetWidth < textContainer.scrollWidth
            );
        }
        return false;
    };

    useEffect(() => {
        if (checkOverflow(overflowingText.current)) {
            setOverflowActive(true);
            return;
        }

        setOverflowActive(false);
    }, [overflowActive]);


    return (
        <>
            <div {...other} id={id} ref={overflowingText}>
                {children}
            </div>
            {overflowActive &&
                <UncontrolledTooltip placement="top" target={id}>
                    {children}
                </UncontrolledTooltip>}
        </>
    );
}
