import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom';
import { actionCreators as generalActions } from 'store/General';
import { RoutingPaths } from '../Routing';

export const IntegratedMode = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    useEffect(() => {
            const asyncInit = async () => {
                await dispatch(generalActions.setIntegratedMode());                
                navigate(RoutingPaths.storefront, {replace: true});
            }

            asyncInit();
        },
        [dispatch, navigate]);
    
    return (<React.Fragment></React.Fragment>);
};

