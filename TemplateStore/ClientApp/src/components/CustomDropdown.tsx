import * as React from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem, DropdownProps} from 'reactstrap';
import { ChevronDownIcon } from '@heroicons/react/24/outline';


import styled from 'styled-components';
import { ButtonStyle, IconContainer, SpacerDiv } from 'theme';

export type Variant = "primary" | "secondary" | "info";

export interface ICustomDropdownItem {
    id?: string,
    key: string,
    display: string|JSX.Element,
    isVisible?: boolean,
    onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void,
    isDivider?: boolean,
}

export type CustomDropdownProps = DropdownProps & { children: ICustomDropdownItem[], dropdownTitle: string|JSX.Element }

const StyledDropdownToggle = styled(DropdownToggle)`${ButtonStyle}
    display: flex;
    align-items: center;
    padding-right: 0px;
`;

const StyledDropdownMenu = styled(DropdownMenu)`
    border-radius: 0;
    min-width: 100%;
    margin-left: -5px;
    max-height: 500px;
    overflow-y: auto;
`;

const StyledIconContainer = styled(IconContainer)`
    flex-shrink: 0;
`

export const CustomDropdown = (props: CustomDropdownProps) => {
    const [dropdownOpen, setDropdownOpen] = React.useState(false);

    const toggle = () => setDropdownOpen(prevState => !prevState);

    const { children, dropdownTitle, disabled, autoFocus, ...other} = props;

    return (
        <Dropdown isOpen={dropdownOpen} toggle={toggle} {...other}>
            <StyledDropdownToggle disabled={disabled} autoFocus={autoFocus}>
                {dropdownTitle}
                <SpacerDiv />
                <StyledIconContainer iconHeight={16} addRightSpace={4}><ChevronDownIcon /></StyledIconContainer>
            </StyledDropdownToggle>
            <StyledDropdownMenu end>
                {children.filter(i => i.isVisible !== false).map(c =>
                    <DropdownItem onClick={c.onClick} key={c.key} divider={c.isDivider}>
                        {c.display}
                    </DropdownItem>
                )}
            </StyledDropdownMenu>
        </Dropdown>
    );
}
