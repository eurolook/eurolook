import React, { useCallback, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { TemplateTileGrid } from 'components/templatesView/TemplateTileGrid';
import { ApplicationState } from 'store';
import { actionCreators as searchActions } from 'store/Search';
import { ModalTriggerButton } from 'components/modals/ModalTriggerButton';
import { NewTemplateModal } from 'components/template/NewTemplateModal';
import { PageTitle, SectionTitle, Page, Header, PageActions, PageAlert } from 'components/page/Page';
import { Alert } from 'reactstrap';
import styled from 'styled-components';
import { unloadedTemplateDetails, UserViewSetting } from '../Models';
import { ButtonInAlertCss } from 'theme';
import { WaitForData } from './WaitForData';
import { SearchBar } from './search/SearchBar';
import { LanguageFilter } from './search/filters/LanguageFilter';
import { BaseDocumentFilter } from './search/filters/BaseDocumentFilter';
import { SortingDropdown, ViewDropdown } from './templatesView/ViewDropdowns';
import { useViewSettings } from '../DisplayPreferences';
import { TemplateListView } from './templatesView/TemplateListView';
import { ManagePublicationsButton } from './template/publication/ManagePublicationsButton';

const StyledAlert = styled(Alert)`
    margin-top: -10px;
    margin-bottom: 10px;
    max-width: ${(props) => props.theme.pageWidthXl}px;
    line-height: 20px;
`

const DisabledPublishTemplateButton = styled(ManagePublicationsButton)`
    &&& {
        &&& {
            ${ ButtonInAlertCss }
            color: ${props => props.theme.primaryColor};
            background-color: transparent;

            svg {
                path {
                    fill: ${props => props.theme.primaryColor};
                }
            }
        }
    }
`

export function MyTemplates() {
    const searchState = useSelector((state: ApplicationState) => state.search);
    const dispatch = useDispatch();
    const viewId = "myTemplates";
    const { viewSetting, isInitialized } = useViewSettings(viewId);
    const tileView = viewSetting.tileView;

    const fetchMyTemplates = useCallback(async (newViewSettings?: UserViewSetting) => {
        if (isInitialized) {
            dispatch(searchActions.fetchMyTemplates(newViewSettings ?? viewSetting));
        }
    }, [dispatch, viewSetting, isInitialized]);

    useEffect(() => {
        fetchMyTemplates();
    }, [fetchMyTemplates, searchState.baseDocumentFilter, searchState.languageFilter, viewSetting, searchState.refetchTrigger]);

    const templatesToShow = searchState.results.slice(0, 50);
    const unpublishedTemplates = templatesToShow.filter(t => t.publications.length < 1);
    const publishedTemplates = templatesToShow.filter(t => t.publications.length > 0) ?? [];
    const emptyMessage = "No templates found.";

    return (
        <React.Fragment>
            <Page>
                <Header $tileView={tileView}>
                    <PageTitle>My Templates</PageTitle>
                    <div>
                        <ModalTriggerButton
                            color="primary"
                            modal={{ component: NewTemplateModal }}>
                            <React.Fragment>New Template</React.Fragment>
                        </ModalTriggerButton>
                    </div>
                </Header>

                <PageActions $tileView={tileView}>
                    <SearchBar small={true} searchCallback={fetchMyTemplates} tileView={tileView} />
                    <PageActions $tileView={tileView}>
                        <LanguageFilter />
                        <BaseDocumentFilter />
                        {tileView && <SortingDropdown viewId={viewId} />}
                        <ViewDropdown viewId={viewId} />
                    </PageActions>
                </PageActions>

                <WaitForData dataAvailable={isInitialized && !searchState.isLoading}>
                    {searchState.results.length > 50 &&
                        <PageAlert color="warning" $tileView={true}>{`First 50 results are shown. Use a filter or refine your search to limit the results.`}</PageAlert>}
                    <Header $tileView={tileView}>
                        <SectionTitle>Unpublished Templates</SectionTitle>
                    </Header>
                    {unpublishedTemplates.length > 0 && <StyledAlert color="warning">Unpublished templates are only visible to you. Use<DisabledPublishTemplateButton disabled template={unloadedTemplateDetails} />in the details view to make it available to others.</StyledAlert>}
                    {!tileView && <TemplateListView templates={unpublishedTemplates} emptyMessage={emptyMessage} viewId={viewId} />}
                    {tileView && <TemplateTileGrid templates={unpublishedTemplates} emptyMessage={emptyMessage} />}

                    <Header $tileView={tileView}>
                        <SectionTitle>Published Templates</SectionTitle>
                    </Header>                    
                    {!tileView && <TemplateListView templates={publishedTemplates} emptyMessage={emptyMessage} viewId={viewId} />}
                    {tileView && <TemplateTileGrid templates={publishedTemplates} emptyMessage={emptyMessage} />}
                </WaitForData>
            </Page>
        </React.Fragment>
    );
};
