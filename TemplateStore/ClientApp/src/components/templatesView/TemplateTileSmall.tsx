import * as React from 'react';
import { useNavigate } from 'react-router-dom'
import { TemplateSummary } from 'Models'
import { Base64Image } from 'components/Base64Image';
import { RoutingPaths } from 'Routing';
import { StyledBadge, TilePreview } from 'theme';
import { PreviewImageOrPlaceholder } from 'components/template/TemplatePreview';

import './TemplateTile.scss'
import { UserTemplateBadge } from './UserTemplateBadge';
import { useAsync } from 'react-async-hook';
import { getTemplateImages } from '../../api';
import { TemplateTileNameDiv, TileTemplateLabelsContainer } from './TemplateTile';

const TemplateTileSmall = React.memo((props: TemplateSummary) => {

    const { id, name, thumbnailBase64, baseDocumentModelName } = props;

    const navigate = useNavigate();

    const previewImages = useAsync(getTemplateImages, [id, true]);
   
    return (
        <button className="template-tile small" onClick={() => navigate(RoutingPaths.getTemplateDetailsPage(id))}>
            <div className="tile-preview-container-small">
                {thumbnailBase64 && <TilePreview><Base64Image base64String={thumbnailBase64} alt="Template thumbnail" /></TilePreview>}
                {!thumbnailBase64 && <TilePreview><PreviewImageOrPlaceholder base64String={previewImages.result?.previewPage1Base64} alt="Preview of page 1" /></TilePreview>}
            </div>
            <TemplateTileNameDiv id={`templateName-${id}`}>
                {name}
            </TemplateTileNameDiv>
            <TileTemplateLabelsContainer>
                <StyledBadge>{baseDocumentModelName}</StyledBadge>
                <UserTemplateBadge templateId={id} />
            </TileTemplateLabelsContainer>
        </button>);
});

export { TemplateTileSmall };

