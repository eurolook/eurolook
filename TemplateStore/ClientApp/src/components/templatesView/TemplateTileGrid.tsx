import * as React from 'react';
import { TemplateTile }  from './TemplateTile';
import { TemplateTileSmall } from './TemplateTileSmall';
import { TemplateSummary } from 'Models';

export const TemplateTileGrid = (props: { templates: TemplateSummary[], emptyMessage: string, smallTiles?: boolean }) => {
    const { templates, emptyMessage, smallTiles } = props;
    const tiles = smallTiles
        ? templates.map(t => <TemplateTileSmall {...t} key={t.id} />)
        : templates.map(t => <TemplateTile {...t} key={t.id} />);

    const result = templates.length > 0 ? tiles : emptyMessage;

    return (
        <div className="tile-grid">
            {result}
        </div >);
};

