import * as React from 'react';
import { UncontrolledTooltip } from 'reactstrap';
import { useRecoilValue } from 'recoil';
import styled from 'styled-components';
import { SpacerDiv, StyledBadge } from '../../theme';
import { userInfoAtom } from '../sidebar/UserInfo';

const StyledStarBadge = styled(StyledBadge)`
    font-size: 14px;
    padding: 1px 2px 1px 2px;
    flex-shrink:0;
`

export function UserTemplateBadge(props: { templateId: string; }) {
    const { templateId } = props;
    const userInfo = useRecoilValue(userInfoAtom);
    const isUserTemplate = userInfo.userTemplates.has(templateId);
    const idForTooltip = `star-${templateId}`;

    return isUserTemplate
        ?
        <>
            <SpacerDiv />
            <StyledStarBadge id={idForTooltip}>★</StyledStarBadge>
            <UncontrolledTooltip placement="top" target={idForTooltip}>
                Contained in your Eurolook profile
            </UncontrolledTooltip>
        </>
        :
        <></>;
}
