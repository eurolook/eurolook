import * as React from 'react';
import { OrderByPropertyEnum, TemplateSummary } from 'Models';

import styled from 'styled-components/macro';
import { IconContainer, StyledLink } from '../../theme';
import { TemplateDetailsFormatter } from '../../TemplateDetailsFormatter';
import { ArrowSmallDownIcon, ArrowSmallUpIcon, ArrowsUpDownIcon } from '@heroicons/react/24/outline';

import { RoutingPaths } from '../../Routing';
import { AddOpenEurolookButton } from '../AddOpenEurolookButton';
import { useViewSettings } from '../../DisplayPreferences';

const TableDiv = styled.div`
    overflow-x: auto;
    margin-bottom: 20px;
`

const StyledTable = styled.table`
    width: 100%;
    min-width: 800px;
    max-width: 1310px;
    padding: 4px;
    table-layout: fixed;
`;

const HeaderRow = styled.tr`
    border-bottom: 2px solid ${(props) => props.theme.sidebarColor};
    color: ${(props) => props.theme.sidebarColor};

    th {
        props.theme.secondaryBorderColor
    }
    th:first-child {
        padding-left: 10px;
    }
`;

const Row = styled.tr`
    height: 32px;
    background: #f9f9f9;
    td:first-child {
        padding-left: 10px;
    }
`

const SpacerRow = styled.tr`
    height: 2px;
`

const ActionColumn = styled.td`
    width: 120px;
    svg {
        margin-right: 4px;
        path {
            fill: ${(props) => props.theme.primaryColor};
        }
    }
`

const StyledArrowsUpDownIcon = styled(ArrowsUpDownIcon)`
    stroke: ${(props) => props.theme.secondaryBorderColor };
`

const StyledTh = styled.th`
    span {
        cursor: pointer;
        line-height: 20px;
    }

    width: ${(props: {width: string}) => props.width };
`;

function TemplateListRow(props: { template: TemplateSummary }) {

    const { template } = props;
    const templateDetailsFormatter = new TemplateDetailsFormatter(template);

    return (
        <Row>
            <td><StyledLink to={RoutingPaths.getTemplateDetailsPage(template.id)}>{template.name}</StyledLink></td>
            <td>{templateDetailsFormatter.getLanguagesString()}</td>
            <td>{template.baseDocumentModelName}</td>
            <td>{templateDetailsFormatter.getMainOwner()?.displayName}</td>
            <td>{templateDetailsFormatter.getLastUpdateDateOnlyString()}</td>
            <ActionColumn>
                <AddOpenEurolookButton template={template} />
            </ActionColumn>
        </Row>
    );
}

export const TemplateListView = (props: { templates: TemplateSummary[], emptyMessage: string, viewId: string}) => {
    const { templates, emptyMessage, viewId } = props;

    const { viewSetting, addOrUpdateViewSetting } = useViewSettings(viewId);
    const orderAsc = viewSetting.orderAsc;

    function setOrderPropertyAndToggleAsc(orderByProperty: OrderByPropertyEnum) {
        const newViewSetting = { ...viewSetting, orderByProperty: orderByProperty, orderAsc: !orderAsc };
        addOrUpdateViewSetting(newViewSetting);
    }

    function SortableHeader(props: { orderBy: OrderByPropertyEnum, header: string, width: string }) {

        const sortedIndicator = orderAsc ? <ArrowSmallUpIcon /> : <ArrowSmallDownIcon />;
        const sortableIndicator = <StyledArrowsUpDownIcon />
        const isSorted = viewSetting.orderByProperty === props.orderBy;

        const HeaderAppendix = () => isSorted ? sortedIndicator : sortableIndicator;
        const Header = () => <>{props.header}<HeaderAppendix /></>;

        return (
            <StyledTh width={props.width}><IconContainer iconHeight={20} highlightOnHover onClick={() => setOrderPropertyAndToggleAsc(props.orderBy)}><Header /></IconContainer></StyledTh>
        );
    }

    const table = <TableDiv>
        <StyledTable>
            <tbody>
                <HeaderRow>
                    <SortableHeader width="40%" orderBy={OrderByPropertyEnum.templateName} header="Template Name" />
                    <StyledTh width="15%">Languages</StyledTh>
                    <SortableHeader width="27%" orderBy={OrderByPropertyEnum.eurolookTemplate} header="Eurolook Template" />
                    <StyledTh width="20%">Owner</StyledTh>
                    <SortableHeader width="110px" orderBy={OrderByPropertyEnum.lastModified} header="Last Update" />
                    <StyledTh width="120px"></StyledTh>
                </HeaderRow>
                <SpacerRow />
                {
                    templates.map(t =>
                        <React.Fragment key={t.id}>
                            <TemplateListRow template={t} />
                            <SpacerRow />
                        </React.Fragment>)
                }
            </tbody>
        </StyledTable>
    </TableDiv>

    return templates.length > 0 ? table : <div>{emptyMessage}</div>;
};

