import * as React from 'react';
import { useNavigate } from 'react-router-dom'
import { RoutingPaths } from 'Routing';

import { Base64Image } from 'components/Base64Image';
import { PreviewImageOrPlaceholder } from 'components/template/TemplatePreview';
import {TemplateSummary} from 'Models'
import { StyledBadge, TilePreview } from 'theme';
import './TemplateTile.scss'
import { DivWithTooltipOnOverflow } from '../DivWithTooltipOnOverflow';
import { UserTemplateBadge } from './UserTemplateBadge';
import { useAsync } from 'react-async-hook';
import { getTemplateImages } from '../../api';
import styled from 'styled-components';

export const TemplateTileNameDiv = styled(DivWithTooltipOnOverflow)`
    font-size: 14px;
    color: ${props => props.theme.sidebarColor};
    text-align: left;
    padding: 0 4px 0 4px;
    margin-top: 5px;
    margin-bottom: 5px;
    height: 46px;
    width: 100%;
    overflow: hidden;
    word-wrap: break-word;
`;

export const TileTemplateLabelsContainer = styled.div`
    margin-left: 4px;
    margin-right: 4px;
    height: 18px;
    display: flex;
    width: 100%;
`;

const TilePreviewContainer = styled.div`
    height: 133px;
    display: flex;
    justify-content: space-between;
    width: 195px;
`;

const TemplateTile = React.memo((props: TemplateSummary) => {
    const navigate = useNavigate();
    const { id, name } = props;

    const languagesCount = props.languages.length;
    const maxDistinctLanguageDisplayCount = 4;

    const previewImages = useAsync(getTemplateImages, [id, true]);

    return (
        <button className="template-tile" onClick={() => navigate(RoutingPaths.getTemplateDetailsPage(id))}>
            <TilePreviewContainer>
                {!props.thumbnailBase64 && <TilePreview><PreviewImageOrPlaceholder base64String={previewImages.result?.previewPage1Base64} alt="Preview of page 1" /></TilePreview>}
                {!props.thumbnailBase64 && previewImages.result?.previewPage1Base64 && <div>&nbsp;&nbsp;</div>}
                {!props.thumbnailBase64 && previewImages.result?.previewPage1Base64 && <TilePreview><Base64Image base64String={previewImages.result.previewPage2Base64} alt="Preview of page 2" /></TilePreview>}
                {props.thumbnailBase64 && <TilePreview><Base64Image base64String={props.thumbnailBase64} alt="Template thumbnail" /></TilePreview>}                
            </TilePreviewContainer>
            <TemplateTileNameDiv id={`templateName-${id}`}>
                {name}
            </TemplateTileNameDiv>
            <TileTemplateLabelsContainer>
                <StyledBadge>{props.baseDocumentModelName}</StyledBadge>
                {languagesCount <= maxDistinctLanguageDisplayCount && props.languages.map(l => <StyledBadge key={l}>{l}</StyledBadge>)}
                {languagesCount > maxDistinctLanguageDisplayCount && <StyledBadge>{languagesCount} Languages</StyledBadge>}
                <UserTemplateBadge templateId={id} />
            </TileTemplateLabelsContainer>
        </button>);
});


export { TemplateTile };
