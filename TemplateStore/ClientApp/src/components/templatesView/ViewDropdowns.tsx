import React from "react";
import { CustomDropdown } from "../CustomDropdown";
import { ArrowSmallUpIcon, ArrowSmallDownIcon, Bars3Icon, Squares2X2Icon } from '@heroicons/react/24/outline';
import { OrderByPropertyEnum, PaginationInfo } from "../../Models";
import { useViewSettings } from "../../DisplayPreferences";
import { IconContainer, NoOverflowSpan } from "../../theme";

export function ViewDropdown(props: { viewId: string}) {
    const { viewId } = props;
    const { viewSetting, addOrUpdateViewSetting } = useViewSettings(viewId);

    const dropdownItems = [
        {
            key: "listView",
            display: <><IconContainer iconHeight={16} addRightSpace><Bars3Icon />List View</IconContainer></>,
            onClick: () => {
                if (!viewSetting.tileView) {
                    return;
                }
                addOrUpdateViewSetting({ ...viewSetting, tileView: false });
            },
        },
        {
            key: "tileView",
            display: <><IconContainer iconHeight={16} addRightSpace><Squares2X2Icon />Tile View</IconContainer></>,
            onClick: () => {
                if (viewSetting.tileView) {
                    return;
                }
                addOrUpdateViewSetting({ ...viewSetting, tileView: true });
            },
        },
    ];

    const title = viewSetting.tileView ? dropdownItems[1].display : dropdownItems[0].display;

    return (
        <CustomDropdown dropdownTitle={title}>{dropdownItems}</CustomDropdown>
    );
};

export function SortingDropdown(props: { viewId: string }) {

    const { viewId } = props;
    const { viewSetting, addOrUpdateViewSetting } = useViewSettings(viewId);
    
    function setPaginationInfo(paginationInfo: PaginationInfo) {
        if (viewSetting.orderByProperty === paginationInfo.orderByProperty && viewSetting.orderAsc === paginationInfo.orderAsc) {
            return;
        }
        
        const newViewSetting = { ...viewSetting, ...paginationInfo };
        addOrUpdateViewSetting(newViewSetting);
    }

    const dropdownItems = [
        {
            key: OrderByPropertyEnum.templateName + "_asc",
            display: <><IconContainer iconHeight={16} addRightSpace><ArrowSmallUpIcon /><NoOverflowSpan>Template Name</NoOverflowSpan></IconContainer></>,
            onClick: () => { setPaginationInfo({ orderByProperty: OrderByPropertyEnum.templateName, orderAsc: true }) },
        },
        {
            key: OrderByPropertyEnum.templateName + "_desc",
            display: <><IconContainer iconHeight={16} addRightSpace><ArrowSmallDownIcon /><NoOverflowSpan>Template Name</NoOverflowSpan></IconContainer></>,
            onClick: () => { setPaginationInfo({ orderByProperty: OrderByPropertyEnum.templateName, orderAsc: false }) },
        },
        {
            key: OrderByPropertyEnum.eurolookTemplate + "_asc",
            display: <><IconContainer iconHeight={16} addRightSpace><ArrowSmallUpIcon /><NoOverflowSpan>Eurolook Template</NoOverflowSpan></IconContainer></>,
            onClick: () => { setPaginationInfo({ orderByProperty: OrderByPropertyEnum.eurolookTemplate, orderAsc: true }) },
        },
        {
            key: OrderByPropertyEnum.eurolookTemplate + "_desc",
            display: <><IconContainer iconHeight={16} addRightSpace><ArrowSmallDownIcon /><NoOverflowSpan>Eurolook Template</NoOverflowSpan></IconContainer></>,
            onClick: () => { setPaginationInfo({ orderByProperty: OrderByPropertyEnum.eurolookTemplate, orderAsc: false }) },            
        },
        {
            key: OrderByPropertyEnum.lastModified + "_asc",
            display: <><IconContainer iconHeight={16} addRightSpace><ArrowSmallUpIcon /><NoOverflowSpan>Last Update</NoOverflowSpan></IconContainer></>,
            onClick: () => { setPaginationInfo({ orderByProperty: OrderByPropertyEnum.lastModified, orderAsc: true }) },
        },
        {
            key: OrderByPropertyEnum.lastModified + "_desc",
            display: <><IconContainer iconHeight={16} addRightSpace><ArrowSmallDownIcon /><NoOverflowSpan>Last Update</NoOverflowSpan></IconContainer></>,
            onClick: () => { setPaginationInfo({ orderByProperty: OrderByPropertyEnum.lastModified, orderAsc: false }) },
        },
    ];

    const title = (dropdownItems.find(d => d.key === viewSetting.orderByProperty + (viewSetting.orderAsc ? "_asc" : "_desc")) ?? dropdownItems[0]).display;

    return (
        <CustomDropdown dropdownTitle={title}>{dropdownItems}</CustomDropdown>
    );
};
