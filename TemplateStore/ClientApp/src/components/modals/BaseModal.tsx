import * as React from 'react';
import { IModalProps } from './ModalTriggerButton';
import { Modal, ModalBody } from 'reactstrap';
import { StyledButton, ButtonGroup } from 'theme';
import styled from "styled-components";

export interface BaseModalProps extends IModalProps {
    children: React.ReactNode,
    confirmButtonLabel?: string,
    confirmAction: () => Promise<void>,
    confirmActionDisabled?: boolean,
    hideConfirmButton?: boolean,
    hideCancelButton?: boolean,
};

const StyledModalBody = styled(ModalBody)`
    padding: 30px 40px;  
`;

const ModalButtonGroup = styled(ButtonGroup)`
  margin-top: 20px;
`;

export const ModalTitle = styled.div`
  font-family: Segoe UI Light;
  font-size: 18px;
  margin-bottom: 20px;
`;

export const ModalParagraph = styled.div`
  margin-bottom: 14px;
`;

export const BaseModal = (props: BaseModalProps) => {
    const { closeModal, beforeClose, isOpen, confirmAction, confirmButtonLabel, confirmActionDisabled, hideConfirmButton, hideCancelButton, children, ...other } = props;

    const action = async () => {
        await confirmAction();
        beforeClose?.();
        closeModal();
    }

    const cancel = () => {
        beforeClose?.();
        closeModal();
    }

    return (
        <React.Fragment>
            <Modal {...other} isOpen={isOpen} centered autoFocus={false} backdrop={'static'}>
                <StyledModalBody>
                    {children}

                    <ModalButtonGroup>
                        {!hideConfirmButton &&
                            <StyledButton color="primary" onClick={action} disabled={confirmActionDisabled}>
                                {confirmButtonLabel ?? 'OK'}
                            </StyledButton>}
                        {!hideCancelButton &&
                            <StyledButton onClick={cancel}>
                                Cancel
                            </StyledButton>}
                    </ModalButtonGroup>
                </StyledModalBody>
            </Modal>
        </React.Fragment>
    );
}
