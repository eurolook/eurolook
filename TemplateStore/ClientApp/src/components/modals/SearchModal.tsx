import * as React from 'react';
import { IIdentifiable } from 'Models';
import { BaseModal, ModalTitle, ModalParagraph } from 'components/modals/BaseModal';
import { IModalProps } from 'components/modals/ModalTriggerButton'
import { IconContainer, StyledInput, TransparentButton } from 'theme';
import { Alert } from 'reactstrap';
import { useAsync } from 'react-async-hook';
import styled, { css } from 'styled-components/macro';
import { MagnifyingGlassCircleIcon } from '@heroicons/react/24/outline';

const SearchResultsModalParagraph = styled(ModalParagraph)`
  height: 500px;
  border: 1px solid ${(props) => props.theme.secondaryBorderColor};

  button {
    display: flex;
    justify-content: space-between;
    width: 100%;
  }
`;

const StyledBaseModal = styled(BaseModal)`
    min-width: 600px;
`;

const SearchResultButton = styled(TransparentButton)`
    &&& {
            ${props => props.$selected && css`
                font-weight: bold;
                background-color: ${props => props.theme.secondaryColor};
            `}

            ${props => props.disabled && css`
                background-color: white;
                color: ${props => props.theme.secondaryBorderColor};
            `}

        box-shadow: none;
        div {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            max-width: 80%;
        }
    }
`;

const WrapperDiv = styled.div`
    position: relative;
`;

const OverlayIconContainer = styled(IconContainer)`
    position: absolute;
    z-index: 2;
    right: 4px;
    top: 0;

    svg {
        stroke: ${props => props.theme.secondaryBorderColor};
    }
`;

const SearchInput = styled(StyledInput)`
    &:focus,&:not(:disabled):not(.disabled):active {
        ~span > svg {
            stroke: ${(props) => props.theme.primaryBorderColor};
        }
    }

    &:hover {
        ~span > svg {
            stroke: ${(props) => props.theme.primaryBorderColor};
        }
    }

    padding-right: 28px;
`;

export interface ISearchModalProps<T> extends IModalProps {
    searchFunction: (searchQuery: string) => Promise<T[]>,
    confirmAction: (selectedSearchResult: T|null) => Promise<void>,
    searchResultDisplay: (result: T) => React.ReactFragment,
    isSearchResultDisabled: (result: T) => boolean,
    title: string,
    placeholder: string,
    confirmButtonLabel: string,
};

export function SearchModal<T extends IIdentifiable>(props: ISearchModalProps<T>) {
    const { title, placeholder, searchFunction, searchResultDisplay, confirmButtonLabel, confirmAction, isSearchResultDisabled, closeModal, ...other } = props;

    const [searchQuery, setSearchQuery] = React.useState('');
    const [selectedSearchResult, setSelectedSearchResult] = React.useState<T | null>(null);
    const [lastClick, setLastClick] = React.useState<number | null>(null);

    async function search(): Promise<T[]> {
        if (searchQuery?.length > 1) {
            return await searchFunction(searchQuery);
        }
        return [];
    }

    const asyncSearch = useAsync(search, [searchQuery]);

    const searchQueryChanged = async (event: React.ChangeEvent<HTMLInputElement>) => {
        const query = event.target.value;
        setSearchQuery(query ?? '');
        setSelectedSearchResult(null);
    }

    const action = async () => {
        await confirmAction(selectedSearchResult);
        setSelectedSearchResult(null);
        closeModal();
    }

    const selectSearchResult = (searchResult: T) => {
        const isDoubleClick = lastClick && lastClick + 250 > Date.now() && searchResult.id === selectedSearchResult?.id;
        if (isDoubleClick) {
            action();
        } else {
            setSelectedSearchResult(searchResult);
            setLastClick(Date.now());
        }
    }

    return (
        <StyledBaseModal confirmAction={action} confirmActionDisabled={!selectedSearchResult} confirmButtonLabel={confirmButtonLabel} closeModal={closeModal} {...other}>
            <React.Fragment>
                <ModalTitle>{title}</ModalTitle>
                <ModalParagraph>
                    <WrapperDiv>
                        <SearchInput placeholder={placeholder} value={searchQuery} onChange={searchQueryChanged} autoFocus></SearchInput>
                        <OverlayIconContainer><MagnifyingGlassCircleIcon /></OverlayIconContainer>
                    </WrapperDiv>
                </ModalParagraph>
                <SearchResultsModalParagraph>
                    { asyncSearch.result &&
                        asyncSearch.result.map(sr => {

                            return <SearchResultButton
                                    key={sr.id}
                                    $selected={(sr.id === selectedSearchResult?.id)}
                                    onClick={() => selectSearchResult(sr)}
                                    disabled={isSearchResultDisabled(sr)}>
                                    {searchResultDisplay(sr)}
                                </SearchResultButton>
                        })
                    }
                    {
                        asyncSearch.error && <Alert color="danger">An error occurred while searching.</Alert>
                    }
                </SearchResultsModalParagraph>
            </React.Fragment>
        </StyledBaseModal>
    );
}
