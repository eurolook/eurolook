import * as React from 'react';
import { ButtonProps } from 'reactstrap';
import { StyledButton } from 'theme';
import styled from 'styled-components/macro';
import { TransparentButtonStyle, NavButtonStyle } from 'theme';

import './Modals.scss'

export interface IModalProps {
    toggle: () => void,
    beforeClose?: () => void,
    closeModal: () => void,
    isOpen: boolean,
}

export type ModalTriggerButtonProps = {
    children: React.ReactNode,
    modal: {
        component: (props: IModalProps & any) => JSX.Element,
        props?: any,
        beforeOpen?: () => void,
        beforeClose?: () => void,
    },
} & ButtonProps

export const ModalTriggerButton = (props: ModalTriggerButtonProps) => {

    const { modal, children, ...other } = props;

    const [isOpen, setIsOpen] = React.useState(false);
    const toggle = () => {
        setIsOpen(!isOpen);
    };

    const openModal = () => {
        modal.beforeOpen?.();
        setIsOpen(true);
    }

    const closeModal = () => {
        modal.beforeClose?.();
        setIsOpen(false);
    }

    const Modal = modal.component;
    const modalProps = modal.props;

    return (
        <React.Fragment>
            <StyledButton {...other} onClick={openModal}>{children}</StyledButton>
            <Modal {...modalProps} toggle={toggle} isOpen={isOpen} closeModal={closeModal}></Modal>
        </React.Fragment>
    );
};

export const TransparentModalTriggerButton = styled(ModalTriggerButton)`${TransparentButtonStyle}`;
export const NavModalTriggerButton = styled(ModalTriggerButton)`${NavButtonStyle}`;
