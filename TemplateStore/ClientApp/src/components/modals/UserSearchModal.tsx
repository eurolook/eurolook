import * as React from 'react';
import * as api from 'api';
import { TemplateOwner, User } from 'Models';
import { IModalProps } from 'components/modals/ModalTriggerButton'
import { SearchModal } from './SearchModal';

export interface IUserSearchModalProps extends IModalProps {
    confirmAction: (selectedUser: User|null) => Promise<void>,
    isSearchResultDisabled: (user: User) => boolean,
    title: string,
    confirmButtonLabel: string,
};

export function UserSearchModal(props: IUserSearchModalProps) {
    const searchResultDisplay = (sr: TemplateOwner) => <>
        <div>{`${sr.displayName}`}</div>
        <div>{`[${sr.orgaName}]`}</div>
    </>;

    return <SearchModal<TemplateOwner>
        {...props}
        searchFunction={api.searchUsers}
        searchResultDisplay={searchResultDisplay}
        placeholder={'Search for name or email'} />
};