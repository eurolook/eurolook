import * as React from 'react';
import * as api from 'api';
import { OrgaEntity } from 'Models';
import { IModalProps } from 'components/modals/ModalTriggerButton';
import { SearchModal } from './SearchModal';

export interface IOrgaEntitySearchModalProps extends IModalProps {
    title: string;
    confirmAction: (selectedUser: OrgaEntity | null) => Promise<void>;
    isSearchResultDisabled: (searchResult: OrgaEntity) => boolean;
};

export function OrgaEntitySearchModal(props: IOrgaEntitySearchModalProps) {
    const {title, confirmAction, isSearchResultDisabled } = props;

    const searchResultDisplay = (sr: OrgaEntity) => <>
        <div>{`${sr.name}`}</div>
    </>;

    return <SearchModal<OrgaEntity>
        {...props}
        confirmAction={confirmAction}
        searchFunction={api.searchOrgaEntities}
        searchResultDisplay={searchResultDisplay}
        placeholder={'Search for the acronym of an orga entity'}
        isSearchResultDisabled={isSearchResultDisabled}
        title={title}
        confirmButtonLabel="Add" />;
}
