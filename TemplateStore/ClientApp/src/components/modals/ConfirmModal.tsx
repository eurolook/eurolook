import * as React from 'react';
import { IModalProps } from './ModalTriggerButton';
import { BaseModal, ModalTitle, ModalParagraph } from 'components/modals/BaseModal';
import './Modals.scss';

export interface ConfirmModalProps extends IModalProps {
    confirmAction: () => Promise<void>,
    title: string,
    message: string,
};

export const ConfirmModal = (props: ConfirmModalProps) => {

    const { confirmAction, title, message, closeModal, ...other } = props;

    return (
        <BaseModal
            confirmAction={confirmAction}
            closeModal={closeModal}
            confirmButtonLabel={"Confirm"}
            {...other}>
                <ModalTitle>{title}</ModalTitle>
                <ModalParagraph>{message}</ModalParagraph>
        </BaseModal>);
}
