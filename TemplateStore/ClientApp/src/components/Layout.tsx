import React from 'react';
import { SideBar } from 'components/sidebar/SideBar';

import './Layout.scss'

export function Layout(props: { children?: React.ReactNode }) {
    return (
        <React.Fragment>
            <SideBar/>
            <div className="content">
                {props.children}
            </div>
        </React.Fragment>
    );
}
