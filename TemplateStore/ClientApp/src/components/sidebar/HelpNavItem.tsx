import * as React from 'react';
import { NavItem } from 'reactstrap';
import * as api from 'api';
import { QuestionMarkCircleIcon } from '@heroicons/react/24/outline';
import { IconContainer } from '../../theme';



export const HelpNavItem = () => {
    const [helpBaseUrl, setHelpBaseUrl] = React.useState("");

    React.useEffect(() => {
            const fetchData = async () => {
                const data = await api.getHelpBaseUrl();
                setHelpBaseUrl(data);
            }

            fetchData();
        },
        []);

    return (
        <NavItem>
            <a href={`${helpBaseUrl}#cshid=TemplateStore`} target="_blank" rel="noopener noreferrer" className="nav-link">
                <IconContainer addRightSpace><QuestionMarkCircleIcon />Get Help</IconContainer>
            </a>
        </NavItem>);
}
