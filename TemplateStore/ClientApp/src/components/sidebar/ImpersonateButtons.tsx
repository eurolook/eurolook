import * as React from 'react';
import { NavModalTriggerButton } from 'components/modals/ModalTriggerButton';
import { UserSearchModal } from 'components/modals/UserSearchModal';
import { User } from 'Models';
import { UsersIcon, XMarkIcon } from '@heroicons/react/24/outline';
import { IconContainer, NavButton } from 'theme';
import { useImpersonation } from '../useImpersonation';

export const ImpersonateButton = (props: {hidden: boolean}) => {
    const { impersonate } = useImpersonation();

    async function impersonateUser(selectedUser: User) {
        if (selectedUser) {
            await impersonate(selectedUser.login);
        }
    }

    const modal = {
        component: UserSearchModal,
        props: {
            confirmAction: impersonateUser,
            isSearchResultDisabled: () => false,
            title: "Impersonate User",
            confirmButtonLabel: "Select",
        }
    }

    return (
        <NavModalTriggerButton className={props.hidden ? 'hidden' : ''} modal={modal}>
            <IconContainer addRightSpace><UsersIcon />Impersonate</IconContainer>
        </NavModalTriggerButton>
    );
}

export const ResetImpersonationButton = () => {
    const { resetImpersonation } = useImpersonation();

    return (
        <NavButton onClick={resetImpersonation}>
            <IconContainer addRightSpace><XMarkIcon />Reset Impersonation</IconContainer>
        </NavButton>
    );
}

