import * as React from 'react';
import * as api from 'api';

export const ApplicationInfo = () => {

    const [applicationInfo, setApplicationInfo] = React.useState({
        version: 'N/A',
        buildTime: 'N/A',
        commitHash: 'N/A',
        environmentName: 'N/A'
    });

    React.useEffect(() => {
        const fetchData = async () => {
            const data = await api.getApplicationInfo();
                setApplicationInfo(data);
            }

            fetchData();
        },
        []);
    return (
        <div>
            <div>Environment: {applicationInfo.environmentName}</div>
            <div>{`Version: ${applicationInfo.version} [${applicationInfo.commitHash}]`}</div>
            <div>{`Build Time: ${applicationInfo.buildTime}`}</div>
        </div>
    );
}
