import * as React from 'react';
import { useAsync } from 'react-async-hook';
import { useSelector } from 'react-redux';
import { atom, useRecoilState } from 'recoil';
import { UserIcon, UserCircleIcon, AcademicCapIcon } from '@heroicons/react/24/outline';
import { ApplicationState } from 'store';
import { UserDetails } from '../../Models';
import { useImpersonation } from '../useImpersonation';
import { getUserInfo } from 'api';
import { IconContainer } from '../../theme';
import styled from 'styled-components';

const UserNameSpan = styled.span`
    white-space: break-spaces;
    margin-right:4px;
`

const StyledIconContainer = styled(IconContainer)`
    align-items: flex-start;
`;

const userInfoUnloadedState: UserDetails = {
    displayName: '',
    id: '',
    login: '',
    email: '',
    isAdmin: false,
    orgaName: '',
    orgaEntities: [],
    userTemplates: new Set<string>(),
    isImpersonated: false,
    authenticatedUser: {
        displayName: '',
        id: '',
        login: '',
        email: '',
        isAdmin: false,
        orgaName: ''
    }    
};

export const userInfoAtom = atom({
    key: "userInfoAtom",
    default: userInfoUnloadedState
});

export function UserInfo() {
    const generalState = useSelector((state: ApplicationState) => state.general);
    const [userInfo, setUserInfo] = useRecoilState(userInfoAtom);
    const impersonatedLogin = new URLSearchParams(window.location.search).get('impersonate') ?? '';
    const { impersonate } = useImpersonation();

    const asyncFetchUserInfo = useAsync(getUserInfo, [generalState.impersonatedUser]);

    React.useEffect(() => {
        if (impersonatedLogin) {
            impersonate(impersonatedLogin)
        }
    }, [impersonate, impersonatedLogin]);

    React.useEffect(() => {
        if (asyncFetchUserInfo.result) {
            const userInfo = { ...asyncFetchUserInfo.result, userTemplates: new Set(asyncFetchUserInfo.result.userTemplates) };
            setUserInfo(userInfo);
        }
    }, [asyncFetchUserInfo.result, setUserInfo]);

    return (
        <StyledIconContainer addRightSpace>
            {!userInfo.isImpersonated && !userInfo.isAdmin && <UserIcon />}
            {!userInfo.isImpersonated && userInfo.isAdmin && <UserCircleIcon />}
            {userInfo.isImpersonated && <AcademicCapIcon />}
            <UserNameSpan>{userInfo.displayName || "Myself"}</UserNameSpan>
        </StyledIconContainer>
    );
}
