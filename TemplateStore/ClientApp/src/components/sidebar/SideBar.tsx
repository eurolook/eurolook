import React from 'react';
import { NavItem, NavLink, Nav, } from 'reactstrap';
import { NavLink as ReactRouterNavLink } from 'react-router-dom';
import { UserInfo, userInfoAtom } from './UserInfo';
import { NavModalTriggerButton } from 'components/modals/ModalTriggerButton';
import { NewTemplateModal } from 'components/template/NewTemplateModal';
import { ImpersonateButton, ResetImpersonationButton } from './ImpersonateButtons';

import { BuildingOffice2Icon, Cog6ToothIcon, MagnifyingGlassIcon, BuildingStorefrontIcon, Square3Stack3DIcon, DocumentPlusIcon } from '@heroicons/react/24/outline';

import { ApplicationInfo } from './ApplicationInfo';
import { HelpNavItem } from './HelpNavItem';

import { useDispatch } from 'react-redux';
import { actionCreators as searchActions } from 'store/Search';
import { RoutingPaths } from 'Routing';

import './SideBar.scss';
import styled from 'styled-components';
import { useRecoilValue } from 'recoil';
import { useQuickFilters } from '../../DisplayPreferences';
import { IconContainer, SpacerDiv } from '../../theme';

const StyledNav = styled(Nav)`
    min-height: 100%;
`

const GapDiv = styled.div`
    height: 8px;
`;

export const SidebarNavItemLink = (props: {id?: string, isHidden?: boolean, to: string, exact?: boolean, children: React.ReactNode}) => {
    const { id, isHidden, to, exact, children } = props;
    const dispatch = useDispatch();

    const resetSearch = () => {
        dispatch(searchActions.resetSearch());
        document.getElementById('searchBox')?.focus();
    }

    return (
            <React.Fragment>
                {!isHidden &&
                <NavItem id={id}>
                <NavLink tag={ReactRouterNavLink} to={to} exact={exact} onClick={resetSearch}>{children}</NavLink>
                </NavItem>}
            </React.Fragment>
        );
}

export const SideBar = () => {
    const userInfo = useRecoilValue(userInfoAtom);
    const { quickFilters } = useQuickFilters();    

    return (
        <div className="sidebar">
            <StyledNav vertical>
                <NavItem className="sidebar-header">
                    <span>Template Store</span>
                </NavItem>

                <SidebarNavItemLink to={RoutingPaths.storefront}><IconContainer addRightSpace><BuildingStorefrontIcon />Home</IconContainer></SidebarNavItemLink>
                <SidebarNavItemLink to={RoutingPaths.search}>
                    <IconContainer addRightSpace><MagnifyingGlassIcon />Search</IconContainer>
                </SidebarNavItemLink>

                {
                    userInfo.orgaEntities.map(q =>
                        <SidebarNavItemLink key={q.id} to={RoutingPaths.getTemplateFilterPage(q.id)}>
                            <IconContainer addRightSpace><BuildingOffice2Icon />{`${q.name}`}</IconContainer>
                        </SidebarNavItemLink>)
                }
                <GapDiv />
                {
                    quickFilters.map(q =>
                        <SidebarNavItemLink key={q.id} to={RoutingPaths.getTemplateFilterPage(q.id)}>
                            <IconContainer addRightSpace><BuildingOffice2Icon />{`${q.name}`}</IconContainer>
                        </SidebarNavItemLink>)
                }

                <SidebarNavItemLink to={RoutingPaths.quickFilters}>
                    <IconContainer addRightSpace><Cog6ToothIcon />{quickFilters.length < 1 ? "Add" : "Manage"} Custom Filters</IconContainer>
                </SidebarNavItemLink>
                <NavItem id="UserInfo" className={userInfo.isImpersonated ? "impersonated" : ''}>
                    <UserInfo/>
                </NavItem>
                {userInfo.authenticatedUser.isAdmin &&
                    <React.Fragment>
                        <ImpersonateButton hidden={userInfo.isImpersonated} />
                        {userInfo.isImpersonated && <ResetImpersonationButton />}
                    </React.Fragment>
                }

                <SidebarNavItemLink id="MyTemplates" to={RoutingPaths.myTemplates}>
                    <IconContainer addRightSpace><Square3Stack3DIcon />My Templates</IconContainer>
                </SidebarNavItemLink>
                <NavModalTriggerButton modal={{ component: NewTemplateModal }}>
                    <IconContainer addRightSpace><DocumentPlusIcon />New Template</IconContainer>
                </NavModalTriggerButton>
                <SpacerDiv />
                <div className="sidebar-footer">
                    <HelpNavItem/>
                    {userInfo.isAdmin && <div>Administrator Mode</div> }
                    <ApplicationInfo/>
                </div>
            </StyledNav>
        </div>
    );
};
