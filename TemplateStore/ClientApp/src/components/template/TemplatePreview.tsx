import React from 'react';
import styled from 'styled-components/macro';
import { Base64Image, Base64ImageProps } from 'components/Base64Image';
import { TemplateSummary } from 'Models';
import { IconContainer, TilePreview } from 'theme';
import { PageRight } from 'components/page/Page';
import { useAsync } from 'react-async-hook';
import { createPreviewImages, getTemplateImages } from 'api';
import { ArrowPathIcon, CheckCircleIcon, ExclamationCircleIcon, XCircleIcon } from '@heroicons/react/24/outline';
import { OverlayContainerDiv, OverlayContent } from '../Overlay';
import { toast } from 'react-toastify';
import { ApiError } from '../../HttpHelper';

const PreviewImageContainer = styled(TilePreview)`
    width: 360px;
    border: 1px solid ${(props) => props.theme.secondaryColor};
    margin-bottom: 20px;

    &:first-child {
      height: 508px;
    }

    &:not(:first-child) {
      height: 254px;
      border-bottom: 0;
    }
`;

const PreviewNotAvailablePlaceholder = () => {
    return (
        <React.Fragment>
            <div>Preview</div>
            <div>not yet</div>
            <div>available</div>
        </React.Fragment>
    );
}

export const PreviewImageOrPlaceholder = (props: Base64ImageProps) => {
    const { base64String } = props;

    return (
        <React.Fragment>
            {base64String && <Base64Image {...props} />}
            {!base64String && <PreviewNotAvailablePlaceholder />}
        </React.Fragment>);
}

const StatusTooManyRequests = 429;

export const TemplatePreview = (props: { template: TemplateSummary, canRecreatePreview: boolean }) => {

    const { template, canRecreatePreview } = props;

    const previewImages = useAsync(getTemplateImages, [template.id]);

    async function recreatePreviewImages() {
        try {
            await createPreviewImages(template.id, template.languages[0]);
            toast.success(<IconContainer addRightSpace><CheckCircleIcon />Preview generation has been initiated</IconContainer>);
        }
        catch (e: unknown) {
            if (e instanceof ApiError && e.response.status === StatusTooManyRequests) {
                toast.warning(<IconContainer addRightSpace><ExclamationCircleIcon />Preview generation still in progress</IconContainer>);
                return;
            }

            toast.error(<IconContainer addRightSpace><XCircleIcon />Failed to initiate preview generation</IconContainer>);
        }
    }

    return (
        <PageRight>
            <OverlayContainerDiv>
                <PreviewImageContainer><PreviewImageOrPlaceholder base64String={previewImages.result?.previewPage1Base64} alt="Preview of page 1" /></PreviewImageContainer>
                {canRecreatePreview && <OverlayContent onClick={recreatePreviewImages}>
                    <IconContainer addRightSpace><ArrowPathIcon />Recreate Preview</IconContainer>
                </OverlayContent>}
            </OverlayContainerDiv>
            <PreviewImageContainer><Base64Image base64String={previewImages.result?.previewPage2Base64} alt="Preview of page 2" /></PreviewImageContainer>
        </PageRight>
    );
}
