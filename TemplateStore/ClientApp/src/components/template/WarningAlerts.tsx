import React from 'react';
import { Alert } from 'reactstrap';
import styled from 'styled-components/macro'
import { TemplateDetails } from '../../Models';
import { ButtonInAlertCss } from 'theme';
import { ManagePublicationsButton } from './publication/ManagePublicationsButton';
import { ManageTemplateFilesButton } from './files/ManageTemplateFilesButton';

const StyledAlert = styled(Alert)`
    margin-bottom: 12px;
    line-height: 20px;
`;

const StyledManageTemplateFilesButton = styled(ManageTemplateFilesButton)`${ButtonInAlertCss}
`;

const StyledManagePublicationsButton = styled(ManagePublicationsButton)`${ButtonInAlertCss}
`;

export const NoFilesAttachedWarning = (props: {templateId: string}) => {

    const { templateId } = props;

    return (
        <StyledAlert color="warning">
            The template does not have any template files attached yet.<br />Use <StyledManageTemplateFilesButton templateId={templateId} /> to proceed.
        </StyledAlert>
    )
}

export const TemplateNotPublishedWarning = (props: { template: TemplateDetails}) => {

    const { template } = props;

    return (
        <StyledAlert color="warning">
            The template is currently not published. Use<StyledManagePublicationsButton template={template} />to make it available to others.
        </StyledAlert>
    )
}
