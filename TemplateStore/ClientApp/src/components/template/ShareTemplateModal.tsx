import React from "react";
import { Label, UncontrolledTooltip } from "reactstrap";
import styled from "styled-components";
import { FlexDiv, IconContainer, SpacerDiv, StyledButton } from "../../theme";
import { BaseModal, BaseModalProps, ModalTitle } from "../modals/BaseModal";
import { TransparentModalTriggerButton } from "../modals/ModalTriggerButton";
import { ShareIcon, EnvelopeOpenIcon, ArrowDownTrayIcon } from '@heroicons/react/24/outline';
import { DownloadButton } from "../LinkButtons";
import download from "downloadjs";
import { TemplateSummary } from "../../Models";
import { getTemplateXmlExport } from "../../api";
import { toast } from "react-toastify";

interface IShareTemplateModalProps {
    mailtoSubject: string,
    mailtoBody: string
    template: TemplateSummary
}

const StyledLabel = styled(Label)`
    align-self: center;
    margin-bottom: 0;
`

const SmallMarginFlexDiv = styled(FlexDiv)`
    margin-bottom: 8px;
`;

const LargeMarginFlexDiv = styled(FlexDiv)`
    margin-bottom: 30px;
`;

const ShareTemplateModal = (props: IShareTemplateModalProps & BaseModalProps) => {

    const { mailtoSubject, mailtoBody, closeModal, template, ...other } = props;

    async function downloadXmlExport() {
        const blob = await getTemplateXmlExport(template.id);
        await download(blob, `${template.name}.xml`, "application/xml");
        toast.success(<IconContainer addRightSpace><ArrowDownTrayIcon />Template exported successfully</IconContainer>);
        closeModal();
    }

    async function MailToLinkClick() {
        toast.success(<IconContainer addRightSpace><EnvelopeOpenIcon />Opening email...</IconContainer>);
        closeModal();
    }

    return (
        <BaseModal hideConfirmButton {...{ closeModal }} {...other}>
            <ModalTitle>Share or export template</ModalTitle>

            <SmallMarginFlexDiv>
                <StyledLabel>Share template link via email</StyledLabel>
                <SpacerDiv />
                <DownloadButton color="primary" onClick={MailToLinkClick} href={`mailto:?subject=${mailtoSubject}&body=${mailtoBody}`}>Share</DownloadButton>
            </SmallMarginFlexDiv>
            <LargeMarginFlexDiv>
                <StyledLabel>Export template details as XML file</StyledLabel>
                <SpacerDiv />
                <StyledButton color="primary" onClick={downloadXmlExport}>
                    Export
                </StyledButton>
            </LargeMarginFlexDiv>
        </BaseModal>
    );
}

export function ShareTemplateModalButton(props: IShareTemplateModalProps) { 

    const modal =
    {
        component: ShareTemplateModal,
        props: {
            confirmAction: async () => { },
            ...props
        }
    }

    return <>
        <TransparentModalTriggerButton modal={modal} id="shareTemplate">
            <IconContainer><ShareIcon />Share</IconContainer>
        </TransparentModalTriggerButton>
        <UncontrolledTooltip placement="top" target="shareTemplate">Share or Export Template</UncontrolledTooltip>
    </>;
}
