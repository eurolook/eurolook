import * as React from 'react';
import { useDispatch } from 'react-redux';
import { ButtonProps } from 'reactstrap';
import { ConfirmModal } from 'components/modals/ConfirmModal';
import { ModalTriggerButton } from 'components/modals/ModalTriggerButton';
import * as api from 'api';
import { actionCreators as templateDetailsActions } from 'store/TemplateDetails';
import { Language } from 'Models';
import { toast } from 'react-toastify';
import { IconContainer } from '../../../theme';
import { TrashIcon } from '@heroicons/react/24/outline';

export const DeleteTemplateFileButton = (props: { templateId: string, language: Language } & ButtonProps ) => {

    const { templateId, language, ...other } = props;

    const dispatch = useDispatch();

    const deleteTemplateFile = async () => {
        try {
            await api.deleteTemplateFile(templateId, language.name);
            toast.success(<span>The template file for <b>{language.name}</b> has been deleted.</span>);
        } catch (e) {
            console.error(e);
            toast.error(`An error occured while deleting the template file.`);
        }

        dispatch(templateDetailsActions.refreshTemplateDetails(templateId));
    };

    const modal =
    {
        component: ConfirmModal,
        props: {
            title: "Delete Template File",
            message: `Delete the ${language.displayName} template file?`,
            confirmAction: deleteTemplateFile,
        }
    }

    return (
        <ModalTriggerButton {...other} modal={modal}>
            <IconContainer><TrashIcon />Delete</IconContainer>
        </ModalTriggerButton>
    );
}
