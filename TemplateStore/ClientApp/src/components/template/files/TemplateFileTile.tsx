import React from 'react';
import { ReactComponent as TileNoFileSvgComponent } from 'resources/TileNoFile.svg';
import { ReactComponent as TileFileSvgComponent } from 'resources/TileFile.svg';
import { ReactComponent as TileFileHoverComponent } from 'resources/TileFileHover.svg';
import { ArrowDownTrayIcon, ArrowPathRoundedSquareIcon, PlusIcon } from '@heroicons/react/24/outline';

import { TemplateDetails, Language } from 'Models';
import { IconContainer, TransparentButton, TransparentButtonStyle } from 'theme';
import styled, { css } from 'styled-components/macro';
import { renderToString } from 'react-dom/server';
import { DeleteTemplateFileButton } from './DeleteTemplateFileButton';
import { AddReplaceTemplateFileButton } from './AddReplaceTemplateFileButton';
import * as api from 'api';
import download from 'downloadjs';
import { UncontrolledTooltip } from 'reactstrap';

const TileFileSvgContent = encodeURIComponent(renderToString(<TileFileSvgComponent />));
const TileNoFileSvgContent = encodeURIComponent(renderToString(<TileNoFileSvgComponent />));
const TileFileHoverSvgContent = encodeURIComponent(renderToString(<TileFileHoverComponent />));
const dataImagePrefix = 'data:image/svg+xml;charset=UTF-8,';

const LanguageWithFileDiv = styled.div`
    height: 112px;
    width: 96px;  
    margin-right: 12px;
    margin-bottom: 12px;
    background-image: url('${dataImagePrefix}${TileFileSvgContent}');
`;

const LanguageWithoutFileDiv = styled(LanguageWithFileDiv)`
    background-image: url('${dataImagePrefix}${TileNoFileSvgContent}');
    &:hover {
        background-image: url('${dataImagePrefix}${TileFileHoverSvgContent}');
    }
`;

const TileUpperPart = styled.div`
    height: 56px;
    display: flex;
    &:hover {
        background-image: url('${dataImagePrefix}${TileFileHoverSvgContent}');
        button {
            display: block;
        }
    }
`;
const TileLowerPart = styled.div`
    padding-left: 8px;
    height: 56px;
    display:flex;
    flex-direction: column-reverse;
`;


const LanguageCode = styled.div`
    padding-top: 4px;
    padding-left: 8px;
    width: 43px;
    font-size: 18px;
    font-family: Segoe UI;
    font-weight: 300
    color: #000;
`;

const DownloadTemplateFileButton = styled(TransparentButton)`
    position: relative;
    width: 46px;
    height: 54px;
    left: 0px;
    top: 0px;
    display: none;
    &:hover {
        background-color: transparent;
    }
    svg {
        margin: 0;
    }
`;

const TemplateFileActionButtonStyle = css`
    ${TransparentButtonStyle}
    margin-top:0px;
    text-align:left;
    height: 30px;
    &:hover {
        background-color: transparent;
    }
    svg {
        width: 18px;
    }
     padding: 0;
`;


const StyledDeleteTemplateFileButton = styled(DeleteTemplateFileButton)`${TemplateFileActionButtonStyle}`;
const StyledAddReplaceTemplateFileButton = styled(AddReplaceTemplateFileButton).attrs((props:any) => ({ id: props.id }))`${TemplateFileActionButtonStyle}`;    

export const TemplateFileTile = (props: {language: Language, template: TemplateDetails, hasFile: boolean}) => {

    const { language, template, hasFile } = props;

    const downloadTemplateFile = async () => {
        const blob = await api.getTemplateFile(props.template.id, language.name);
        download(blob, `${template.name}_${language.name}.docx`, "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    }

    const isDeleteDisabled = template.languages.length < 2;

    const downloadTooltipTarget = `downloadTemplateFile-${language.name}`;
    const deleteTooltipTarget = `deleteTemplateFile-${language.name}`;
    const addTooltipTarget = `addTemplateFile-${language.name}`;
    const replaceTooltipTarget = `replaceTemplateFile-${language.name}`;

    return (
        <React.Fragment>
            {hasFile &&
                <LanguageWithFileDiv>
                    <TileUpperPart>
                        <LanguageCode>{language.name}</LanguageCode>
                        <DownloadTemplateFileButton id={downloadTooltipTarget} onClick={downloadTemplateFile}><IconContainer><ArrowDownTrayIcon /></IconContainer></DownloadTemplateFileButton>
                        <UncontrolledTooltip placement="top" target={downloadTooltipTarget}>Download {language.displayName} template file.</UncontrolledTooltip>
                    </TileUpperPart>
                    <TileLowerPart>
                        <StyledDeleteTemplateFileButton id={deleteTooltipTarget} language={language} templateId={template.id} disabled={isDeleteDisabled} />
                        <UncontrolledTooltip placement="top" target={deleteTooltipTarget}>Delete {language.displayName} template file.</UncontrolledTooltip>
                        <StyledAddReplaceTemplateFileButton id={addTooltipTarget} template={template} language={language}><IconContainer><ArrowPathRoundedSquareIcon />Replace</IconContainer></StyledAddReplaceTemplateFileButton>
                        <UncontrolledTooltip placement="top" target={addTooltipTarget}>Replace {language.displayName} template file</UncontrolledTooltip>
                    </TileLowerPart>
                </LanguageWithFileDiv>}
            {!hasFile &&
                <LanguageWithoutFileDiv>
                    <TileUpperPart>
                        <LanguageCode>{language.name}</LanguageCode>
                    </TileUpperPart>
                    <TileLowerPart>
                        <StyledAddReplaceTemplateFileButton id={replaceTooltipTarget} template={template} language={language}><IconContainer><PlusIcon />Add</IconContainer></StyledAddReplaceTemplateFileButton>
                        <UncontrolledTooltip placement="top" target={replaceTooltipTarget}>Add new {language.displayName} template file</UncontrolledTooltip>
                    </TileLowerPart>
                </LanguageWithoutFileDiv>}
        </React.Fragment>
    );
}


