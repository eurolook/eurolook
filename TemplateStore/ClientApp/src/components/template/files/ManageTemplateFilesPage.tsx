import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { actionCreators as templateDetailsActions } from 'store/TemplateDetails';
import { TemplateDetails, unloadedTemplateDetails, Language } from 'Models';
import { StyledLabel, Box, StyledButton, ButtonGroup } from 'theme';
import { Page, Header, PageTitle } from 'components/page/Page';
import { useParams } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import { ApplicationState } from 'store';
import styled from 'styled-components/macro';
import { RoutingPaths } from 'Routing';
import { TemplateFileTile } from './TemplateFileTile';
import { Alert } from 'reactstrap';

const SubPage = styled.div`
    max-width: 756px;
    padding-right: 12px;
`;

const TemplateFileGrid = styled.div`
    display: flex;
    flex-flow: wrap;
    margin-right: -12px;
`;

const StyledButtonGroup = styled(ButtonGroup)`
  margin-top: 20px;  
`;

export function ManageTemplateFilesPage() {

    const dispatch = useDispatch();
    const params = useParams<{ id: string }>();
    const navigate = useNavigate();

    const templateDetailsState = useSelector((state: ApplicationState) => state.templateDetails);
    const template: TemplateDetails = templateDetailsState.templates.find(t => t.id === params.id) ?? unloadedTemplateDetails;
    const searchState = useSelector((state: ApplicationState) => state.search);

    const hasFilesAttached = template.languages.length > 0;

    useEffect(() => {
            if (params.id) {
                dispatch(templateDetailsActions.requestTemplateDetails(params.id));
            }
        },
        [dispatch, params.id]);

    return (
        <Page>
            <Header $tileView={false}>
                <PageTitle>Manage Template Files</PageTitle>
            </Header>
            {searchState.languages.length > 0 && template.id !== '' &&
                <SubPage>
                    <StyledLabel>Template:</StyledLabel>
                    <Box>{template.name}</Box>
                    <StyledLabel>Files:</StyledLabel>
                    <TemplateFileGrid>
                        {searchState.languages.filter((l, i) => i < 3).map((l: Language) =>
                            <TemplateFileTile key={l.id} language={l} template={template} hasFile={template.languages.some(tl => tl === l.name)} />)}
                    </TemplateFileGrid>
                    <TemplateFileGrid>
                        {searchState.languages.filter((l, i) => i > 2).map((l: Language) =>
                            <TemplateFileTile key={l.id} language={l} template={template} hasFile={template.languages.some(tl => tl === l.name)} />)}
                    </TemplateFileGrid>

                    {!hasFilesAttached &&
                        <Alert color="warning">
                            Add one or more template files to your custom template.
                        </Alert>}

                    <StyledButtonGroup leftAligned>
                        <StyledButton disabled={!hasFilesAttached} onClick={() => navigate(RoutingPaths.getTemplateDetailsPage(template.id))} color="primary">OK</StyledButton>
                    </StyledButtonGroup>

                </SubPage>}
        </Page>
    );
};
