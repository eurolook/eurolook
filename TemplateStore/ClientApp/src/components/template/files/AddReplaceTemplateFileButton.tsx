import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Alert } from 'reactstrap';
import { ActionDisplayRow } from 'components/ActionDisplayRow';
import { actionCreators as templateDetailsActions } from 'store/TemplateDetails';
import { BaseModal, ModalTitle, ModalParagraph } from 'components/modals/BaseModal';
import { ModalTriggerButton, IModalProps } from 'components/modals/ModalTriggerButton';
import { FileInput } from 'components/FileInput';
import { TemplateDetails, Language } from 'Models';
import { StyledButton, PropertiesBox, LabelWithoutTopMargin } from 'theme';
import { DocxAnalyzer, EurolookCustomProperties } from 'DocxAnalyzer';
import { TemplateProperty } from 'components/template/TemplateProperty';
import filesize from 'filesize';
import { toast } from 'react-toastify';
import * as api from 'api';
import styled from 'styled-components';

export interface IAddReplaceTemplateFileModalProps extends IModalProps {
    template: TemplateDetails,
    language: Language,
};

const StyledBaseModal = styled(BaseModal)`
    min-width: 550px;
`;


const AddReplaceTemplateFileModal = (props: IAddReplaceTemplateFileModalProps) => {
    const { template, language, ...other } = props;

    const dispatch = useDispatch();
    const isAddAction = template.languages.find(l => l === language.name) == null;
    const [selectedFile, setSelectedFile] = useState<File | undefined>();
    const initialDetails = { languageName: 'N/A', documentTypeName: 'N/A', fileSize: 'N/A', };
    const [details, setDetails] = useState(initialDetails);
    const [error, setError] = useState<React.ReactNode | undefined>(undefined);

    const resetState = () => {
        setSelectedFile(undefined);
        setDetails(initialDetails);
        setError(undefined);
    };

    const onFileChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
        const fileList = event?.target?.files;
        if (fileList && fileList[0]) {
            resetState();
            const file: File = fileList[0];
            try {
                setSelectedFile(file);

                if (file.size > 20 * 1024 * 1024) {
                    setError(`The file exceeds the maximum size for a template (20 MB).`);
                    return;
                }

                const docxAnalyzer = new DocxAnalyzer();
                const properties: EurolookCustomProperties = await docxAnalyzer.validateDocxFile(file);
                setDetails({ fileSize: filesize(file.size), languageName: properties.languageName, documentTypeName: properties.documentModelName });

                if (template?.baseDocumentModelId != null && template?.baseDocumentModelId !== properties.documentModelId) {
                    setError(`The file is not based on the same Eurolook template as the initial upload (${template.baseDocumentModelName})`);
                    return;
                }
                if (language.name !== properties.languageName) {
                    setError(
                        <React.Fragment>The detected language (<b>{properties.languageName}</b>) does not match the selected language (<b>{language.name}</b>).</React.Fragment>);
                }
            } catch (ex: any) {
                setError(ex.message);
            }
        }
    }

    const uploadTemplateFile = async () => 
    {
        try {
            if (selectedFile) {
                await api.uploadTemplateFile(selectedFile, template.id);
                toast.success(<span>The template file for <b>{language.name}</b> has been successfully {isAddAction ? 'added' : 'replaced'}.</span>);
            }
        } catch (e) {
            console.error(e);
            toast.error(`An error occured while deleting the template file.`);
        }

        dispatch(templateDetailsActions.refreshTemplateDetails(template.id));
    }

    const isInvalid: boolean = !selectedFile || error !== undefined;

    return (
        <StyledBaseModal
            confirmAction={uploadTemplateFile}
            confirmActionDisabled={isInvalid}
            confirmButtonLabel={"Confirm"}
            beforeClose={resetState}
            {...other}>
            <React.Fragment>
                <ModalTitle>{`${isAddAction ? "Add" : "Replace"} ${language.displayName} Template File`}</ModalTitle>
                <ModalParagraph>
                    <LabelWithoutTopMargin>File:</LabelWithoutTopMargin>
                    <ActionDisplayRow>
                        <span>{selectedFile?.name ?? ''}</span>
                        <FileInput onChange={onFileChange}>
                            <StyledButton as="div">Browse...</StyledButton>
                        </FileInput>
                    </ActionDisplayRow>
                </ModalParagraph>
                <ModalParagraph>
                    <LabelWithoutTopMargin>Detected Properties:</LabelWithoutTopMargin>
                    <PropertiesBox>
                        <TemplateProperty label="File Size">{details.fileSize}</TemplateProperty>
                        <TemplateProperty label="Language">{details.languageName}</TemplateProperty>
                        <TemplateProperty label="Eurolook Template">{details.documentTypeName}</TemplateProperty>
                    </PropertiesBox>
                </ModalParagraph>
                {error &&
                    <Alert color="danger">
                        {error}
                    </Alert>}
            </React.Fragment>
        </StyledBaseModal>
    );
};


export const AddReplaceTemplateFileButton = (props: { template: TemplateDetails, language: Language, children: React.ReactNode }) => {

    const { template, language, children, ...other } = props;

    const modal = {
        component: AddReplaceTemplateFileModal,
        props: {
            template: template,
            language: language
        }
    }

    return (
        <ModalTriggerButton modal={modal} {...other}>
            {children}
        </ModalTriggerButton>);
}
