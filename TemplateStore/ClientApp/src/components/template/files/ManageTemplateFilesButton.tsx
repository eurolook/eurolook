import React from 'react';
import { LinkButton } from 'components/LinkButtons';
import { DocumentDuplicateIcon } from '@heroicons/react/24/outline';
import { RoutingPaths } from 'Routing';
import { IconContainer } from 'theme';

export function ManageTemplateFilesButton(props: { templateId: string; className?: string; }) {
    const { templateId, ...other } = props;
    return (
        <LinkButton to={RoutingPaths.getTemplateFilesPage(templateId)} {...other}>
            <IconContainer><DocumentDuplicateIcon />Manage Template Files</IconContainer>
        </LinkButton>
    );
}
