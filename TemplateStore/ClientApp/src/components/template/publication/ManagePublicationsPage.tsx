import React, { useEffect, useState } from 'react';
import { ModalTriggerButton } from 'components/modals/ModalTriggerButton';
import { PageTitle, Page, Header, SubPageContainer, PageLeft, PageAlert} from 'components/page/Page';
import { INamedIdentifiable, OrgaEntity, TemplateDetails, unloadedTemplateDetails } from 'Models';
import { useRecoilValue } from 'recoil';
import { Box, ButtonGroup, IconContainer, ItemsContainerAction, Rect, SpacerDiv, StyledButton, StyledLabel } from '../../../theme';
import styled from 'styled-components';
import { BuildingOffice2Icon, CheckCircleIcon, ExclamationCircleIcon, PlusIcon } from '@heroicons/react/24/outline';
import { useNavigate, useParams } from 'react-router-dom';
import { userInfoAtom } from '../../sidebar/UserInfo';
import { OrgaEntitySearchModal } from '../../modals/OrgaEntitySearchModal';
import { useDispatch, useSelector } from 'react-redux';
import { ApplicationState } from '../../../store';
import { actionCreators as templateDetailsActions } from 'store/TemplateDetails';
import { updatePublications } from 'api';
import { toast } from 'react-toastify';
import { useQuickFilters } from '../../../DisplayPreferences';
import { WaitForData } from '../../WaitForData';
import { CheckboxLine } from "components/CheckboxLine";
import { useLinks } from '../../useLinks';

const StyledButtonGroup = styled(ButtonGroup)`
  margin-top: 20px;
  margin-bottom: 20px;
`;

const StyledItemContainerAction = styled(ItemsContainerAction)`
  margin-top: 10px;
`;

const StyledPageAlert = styled(PageAlert)`
    display: flex;
    justify-content: space-between;
`;

function PublishedStatusAlert(props: { isPublished: boolean }) {

    const { isPublished } = props;
    const alertMessage = isPublished ? "Published" : "Not published";
    const alertColor = isPublished ? "info" : "warning";

    return <StyledPageAlert color={alertColor} $tileView={false}>
        <div>
            <IconContainer addRightSpace>{isPublished ? <CheckCircleIcon /> : <ExclamationCircleIcon />}{alertMessage}</IconContainer>
        </div>
    </StyledPageAlert>
}

export function ManagePublicationsPage() {
    const userInfo = useRecoilValue(userInfoAtom);
    const { quickFilters, isInitialized } = useQuickFilters();

    const navigate = useNavigate();
    const dispatch = useDispatch();
    const params = useParams<{ id: string }>();
    const templateDetailsState = useSelector((state: ApplicationState) => state.templateDetails);
    const template: TemplateDetails = templateDetailsState.templates.find(t => t.id === params.id) ?? unloadedTemplateDetails;
    const { getHttpLinkToTemplateDetailsPage } = useLinks();

    const [publications, setPublications] = useState<INamedIdentifiable[]>([]);
    
    const [selectabelOrgaEntities, setSelectabelOrgaEntities] = useState<INamedIdentifiable[]>([]);

    const dataAvailable = userInfo != null && isInitialized && template.id !== unloadedTemplateDetails.id;

    useEffect(() => {
        if (params.id) {
            dispatch(templateDetailsActions.requestTemplateDetails(params.id));
        }
    }, [dispatch, params.id]);

    useEffect(() => {
        if (dataAvailable) {
            setPublications(template.publications);

            let initiallySelectableOrgaEntities: INamedIdentifiable[] = userInfo.orgaEntities;
            initiallySelectableOrgaEntities = initiallySelectableOrgaEntities.concat(quickFilters);
            
            const additionalPublications = template.publications.filter(p => !initiallySelectableOrgaEntities.some(oe => oe.id === p.id));
            initiallySelectableOrgaEntities = initiallySelectableOrgaEntities.concat(additionalPublications);

            setSelectabelOrgaEntities(initiallySelectableOrgaEntities);
        }
    }, [template, userInfo, quickFilters, dataAvailable]);

    function addNewSelectablePublication(oe: INamedIdentifiable) {
        setSelectabelOrgaEntities([...selectabelOrgaEntities, oe]);
        setPublications([...publications, oe]);
    }

    function changePublication(oe: INamedIdentifiable) {

        if (isOrgaEntityInTargetGroups(oe)) {
            setPublications([...publications.filter(p => p.id !== oe.id)]);
        } else {
            setPublications([...publications, oe]);
        }
    }

    async function savePublications() {
        try {
            await updatePublications(template.id, publications.map(o => o.id));
        } catch {
            toast.error("Saving target groups failed.");
        };
    }    

    const rootOrgaEntityId = userInfo.orgaEntities.find(oe => oe.logicalLevel === 0)?.id;

    const isOrgaEntityInTargetGroups = (oe: INamedIdentifiable) => publications.some(p => p.id === oe.id);
    const isPublished = publications.length > 0;
    const isPublishedToCommission = publications.some(oe => oe.id === rootOrgaEntityId);
    const wasPublishedToCommission = template.publications.some(oe => oe.id === rootOrgaEntityId);
    const isPublishToCommissionValid = userInfo.isAdmin || wasPublishedToCommission || !isPublishedToCommission;
    const maxNumberExceeded = (publications.length > 10);
    const isValid = !maxNumberExceeded && isPublishToCommissionValid

    const encodedTemplateName = encodeURIComponent(template.name);
    const eurolookFunctionalMailbox = "";
    const mailtoSubject = `Publishing of Template: ${encodedTemplateName}`;
    const mailtoBody = `Please make the following template available for the entire Commission: ${getHttpLinkToTemplateDetailsPage(template.id)}%0D%0A%0D%0A`
        
    const MailtoLink = () => <a href={`mailto:${eurolookFunctionalMailbox}?subject=${mailtoSubject}&body=${mailtoBody}`}>Eurolook Team</a>

    return (
        <Page>
            <SubPageContainer>
                <WaitForData dataAvailable={dataAvailable}>
                    <PageLeft>
                        <Header $tileView={false}>
                            <PageTitle>Availability</PageTitle>
                        </Header>
                        <PublishedStatusAlert isPublished={isPublished} />
                        <StyledLabel>Template:</StyledLabel>
                        <Box>{template.name}</Box>
                        <StyledLabel>Select Target Group(s):</StyledLabel>
                        <Rect>
                            {selectabelOrgaEntities.map(oe => <React.Fragment key={oe.id}>
                                    <CheckboxLine toggle={() => changePublication(oe)} checked={isOrgaEntityInTargetGroups(oe)}>
                                        <IconContainer addRightSpace><BuildingOffice2Icon />{`${oe.name}`}</IconContainer>
                                    </CheckboxLine>
                                    {oe.id === rootOrgaEntityId && !isPublishToCommissionValid && <PageAlert color="danger" $tileView={false}>For target group Commission, contact the&nbsp;<MailtoLink /></PageAlert> }
                                </React.Fragment>
                            )}
                            <SpacerDiv />
                            <StyledItemContainerAction>
                                <ModalTriggerButton
                                    color="secondary"
                                    modal={
                                        {
                                            component: OrgaEntitySearchModal,
                                            props: {
                                                title: "Add Target Group",
                                                confirmAction: addNewSelectablePublication,
                                                isSearchResultDisabled: (o: OrgaEntity) => [...selectabelOrgaEntities].some(i => i.id === o.id) || (o.id === rootOrgaEntityId),
                                            }
                                        }}>
                                    <IconContainer addRightSpace><PlusIcon />Add Target Group</IconContainer>
                                </ModalTriggerButton>
                            </StyledItemContainerAction>
                        </Rect>
                        {maxNumberExceeded && <PageAlert color="danger" $tileView={false}>{`The maximum number of target groups (10) exceeded`}</PageAlert>}
                        <StyledButtonGroup>
                            <StyledButton color="primary" onClick={async () => { await savePublications(); navigate(-1); }} disabled={!isValid}>Save</StyledButton>
                            <StyledButton color="secondary" onClick={() => navigate(-1)}>Cancel</StyledButton>
                        </StyledButtonGroup>
                    </PageLeft>
                </WaitForData>
            </SubPageContainer>
        </Page>
    );
};
