import React from 'react';
import { LinkButton } from 'components/LinkButtons';
import { TemplateDetails } from 'Models';
import { ArrowUpOnSquareIcon } from '@heroicons/react/24/outline';
import { RoutingPaths } from 'Routing';
import { IconContainer } from 'theme';




export function ManagePublicationsButton(props: { template: TemplateDetails; className?: string; disabled?: boolean; }) {
    const { template, disabled, ...other } = props;

    const hasFilesAttached = template.languages.length > 0;

    return (
        <LinkButton to={RoutingPaths.getTemplatePublicationsPage(template.id)} {...other} disabled={disabled || !hasFilesAttached}>
            {!template.isPublic && <IconContainer><ArrowUpOnSquareIcon />Publish</IconContainer>}
            {template.isPublic && <IconContainer><ArrowUpOnSquareIcon />Change Availability</IconContainer>}
        </LinkButton>
    );
}
