import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Alert } from 'reactstrap';
import { ActionDisplayRow } from 'components/ActionDisplayRow';
import { actionCreators as templateDetailsActions } from 'store/TemplateDetails';
import { BaseModal, ModalTitle, ModalParagraph } from 'components/modals/BaseModal';
import { ModalTriggerButton, IModalProps } from 'components/modals/ModalTriggerButton';
import { FileInput } from 'components/FileInput';
import { TemplateDetails } from 'Models';
import { Cropper } from "react-cropper";
import { IconContainer, StyledButton } from 'theme';
import { ArrowUpTrayIcon } from '@heroicons/react/24/outline';

import "cropperjs/dist/cropper.css";

export interface IUploadThumbnailModalProps extends IModalProps {
    template: TemplateDetails,
};

const UploadThumbnailModal = (props: IUploadThumbnailModalProps) => {
    const { template, ...other } = props;

    const dispatch = useDispatch();
  
    const [selectedFile, setSelectedFile] = useState<File | undefined>();
    const [image, setImage] = useState('');
    const [error, setError] = useState('');
    const [cropper, setCropper] = useState<any>();

    const validateFile = async (file: File) => {
        try {
            if (file) {
                const validExtensions = ['.jpg', '.jpeg', '.png', '.svg', '.bmp', '.gif'];

                if (!validExtensions.some(e => file.name.toLowerCase().endsWith(e))) {
                    throw Error(`Invalid file extension; valid extensions are: ${validExtensions.join(', ')}`);
                }

                if (file.size > 10 * 1024 * 1024) {
                    throw Error("Image file too large.");
                }
            }
        } catch (ex: any) {
            setError(`${ex.message}`);
            return false;
        }

        return true;
    }

    const readAndValidateImage = async (file: File) => {
        setError('');
        if (!await validateFile(file)) {
            return '';
        }

        const data: any = await new Promise((resolve) => {
            const reader = new FileReader();
            reader.onloadend = () => resolve(reader.result as any);
            reader.readAsDataURL(file);

        });

        const img = new Image();
        img.onerror = () => {
            setError('Invalid image.');
            return '';
        };
        img.src = data;

        return data;
    }

    const uploadThumbnail = async () => {
        if (!selectedFile) {
            return;
        }

        cropper.getCroppedCanvas().toBlob((blob: Blob) => {

            if (blob.size > 2 * 1024 * 1024) {
                setError("Cropped image exceeds max file size (2 MB).");
                return;
            }

            const newDraft = { ...props.template.draft };
            newDraft.thumbnailBlob = blob;
            dispatch(templateDetailsActions.editTemplateDetails(template.id, newDraft));
        });
    };

    const onFileChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
        const fileList = event?.target?.files;
        const file = fileList ? fileList[0] : null;
        if (file) {
            setSelectedFile(file);
            const data = await readAndValidateImage(file);
            setImage(data);
        }
    }

    const isInvalid: boolean = !selectedFile || error !== '';

    return (
        <BaseModal confirmAction={uploadThumbnail} confirmActionDisabled={isInvalid} {...other}>
            <React.Fragment>
                <ModalTitle>Upload a Thumbnail</ModalTitle>
                <ModalParagraph>
                    <span>File:</span>
                    <ActionDisplayRow>
                        <span>{selectedFile?.name ?? ''}</span>
                        <FileInput onChange={onFileChange}>
                            <StyledButton as="div">Browse...</StyledButton>
                        </FileInput>
                    </ActionDisplayRow>
                </ModalParagraph>
                <ModalParagraph>
                    <Cropper
                        style={{ height: 400, width: "100%" }}
                        zoomTo={2}
                        initialAspectRatio={1}
                        src={image}
                        viewMode={1}
                        guides={true}
                        minCropBoxHeight={200}
                        minCropBoxWidth={150}
                        aspectRatio={1.3}
                        background={false}
                        responsive={true}
                        dragMode="move"
                        cropBoxResizable={false}
                        autoCropArea={1}
                        onInitialized={(instance) => {
                            setCropper(instance);
                        }}
                        cropstart={() => {
                            setError("");
                        }}
                        zoom={() => {
                            setError("");
                        }}
                    />
                </ModalParagraph>
                {error &&
                    <Alert color="danger">
                        {`Error: ${error}`}
                    </Alert>}
            </React.Fragment>
        </BaseModal>
    );
};


export const UploadThumbnailButton = (props: { template: TemplateDetails }) => {
    const modal = {
        component: UploadThumbnailModal,
        props: {
            template: props.template
        }
    }

    return (
        <ModalTriggerButton modal={modal}>
            <IconContainer addRightSpace={4} iconHeight={20}><ArrowUpTrayIcon />Upload Thumbnail</IconContainer>
        </ModalTriggerButton>);
}
