import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { TemplateDetails } from 'Models';
import { Base64Image } from 'components/Base64Image';
import { UploadThumbnailButton } from './UploadThumbnailButton';
import { actionCreators as templateDetailsActions } from 'store/TemplateDetails';
import { IconContainer, ItemsContainerAction, TilePreview } from 'theme';
import { TrashIcon } from '@heroicons/react/24/outline';
import styled from 'styled-components';
import { OverlayContainerDiv, OverlayContent } from '../../Overlay';

const StyledOverlayContainerDiv = styled(OverlayContainerDiv)`
    height: 90px;
`;

export const ThumbnailView = (props: { template: TemplateDetails }) => {

    const dispatch = useDispatch();
    const [thumbnailBase64, setThumbnailBase64] = useState<string | null>(null);

    const { template } = props;

    useEffect(() => {
            setThumbnailBase64(template.thumbnailBase64);
        },
        [dispatch, template.thumbnailBase64]);

    if (template.draft.thumbnailBlob) {
        const reader = new FileReader();
        reader.onload = () => {
            setThumbnailBase64(reader.result as any);
        };
        reader.readAsDataURL(template.draft.thumbnailBlob);
    }

    const removeThumbnail = () => {
        const newDraft = { ...template.draft, thumbnailBlob: null, deleteThumbnail: true };
        dispatch(templateDetailsActions.editTemplateDetails(template.id, newDraft));
        setThumbnailBase64(null);
    }

    return (
        <React.Fragment>
            <div className="edit-items-container">
                {thumbnailBase64 && <StyledOverlayContainerDiv>
                        <TilePreview><Base64Image base64String={thumbnailBase64} alt="Template thumbnail" /></TilePreview>
                        <OverlayContent onClick={removeThumbnail}>
                            <IconContainer><TrashIcon /></IconContainer>
                        </OverlayContent>
                </StyledOverlayContainerDiv>}
            </div>
            <ItemsContainerAction>
                <UploadThumbnailButton template={template}/>
            </ItemsContainerAction>
        </React.Fragment>);
}
