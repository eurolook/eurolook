import React, { useEffect, useState } from 'react';
import { ModalTriggerButton } from 'components/modals/ModalTriggerButton';
import { PageTitle, Page, Header, SubPageContainer, PageLeft, PageAlert} from 'components/page/Page';
import { TemplateDetails, TemplateOwner, unloadedTemplateDetails } from 'Models';
import { Box, ButtonGroup, IconContainer, ItemsContainerAction, Rect, SpacerDiv, StyledButton, StyledLabel } from '../../../theme';
import styled from 'styled-components';
import { PlusIcon, TrashIcon, UserIcon } from '@heroicons/react/24/outline';
import { useNavigate, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { ApplicationState } from '../../../store';
import { actionCreators as templateDetailsActions } from 'store/TemplateDetails';
import { toast } from 'react-toastify';
import { WaitForData } from '../../WaitForData';
import { UserSearchModal } from '../../modals/UserSearchModal';
import { Badge } from 'reactstrap';
import { useRecoilValue } from 'recoil';
import { userInfoAtom } from '../../sidebar/UserInfo';
import { updateOwners } from '../../../api';
import { DivWithTooltipOnOverflow } from '../../DivWithTooltipOnOverflow';

const StyledButtonGroup = styled(ButtonGroup)`
  margin-top: 20px;
  margin-bottom: 20px;
`;

const StyledItemContainerAction = styled(ItemsContainerAction)`
  margin-top: 10px;
`;

const StyledBox = styled(Box)`
  margin-bottom: 4px;
  display flex;
  padding-right: 4px;
`;

const MainOwnerBadge = styled(Badge)`
  color: white;
  background-color: ${props => props.theme.primaryColor} !important;
  margin-right: 8px;
`;

const ClickableBadge = styled(Badge)`
  cursor: pointer;
  color: #00819D !important;
  background-color: #CFE7ED !important;
  margin-right: 8px;  
    &:hover, &:not(:disabled):not(.disabled):active {
        background-color: ${props => props.theme.secondaryColorAccent} !important;
    };
`;

export const StyledDivWithTooltivOnOverflow = styled(DivWithTooltipOnOverflow)`
    width:286px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
`;

export function ManageOwnersPage() {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const params = useParams<{ id: string }>();
    const userInfo = useRecoilValue(userInfoAtom);
    const templateDetailsState = useSelector((state: ApplicationState) => state.templateDetails);
    const template: TemplateDetails = templateDetailsState.templates.find(t => t.id === params.id) ?? unloadedTemplateDetails;
 
    const [owners, setOwners] = useState<TemplateOwner[]>([]);
    const dataAvailable = template.id !== unloadedTemplateDetails.id;

    useEffect(() => {
        if (params.id) {
            dispatch(templateDetailsActions.requestTemplateDetails(params.id));
        }
    }, [dispatch, params.id]);

    useEffect(() => {
        if (dataAvailable) {
            setOwners(template.owners);
        }
    }, [template, dataAvailable]);

    function addOwnerAction(owner: TemplateOwner) {
        setOwnersInternal([...owners, owner]);
    }

    function removeOwner(owner: TemplateOwner) {
        setOwnersInternal([...owners.filter(o => o.id !== owner.id)]);
    }

    function setMainOwner(owner: TemplateOwner) {
        const newOwners = owners.map(o => ({ ...o, isMain: o.id === owner.id }));
        setOwners([...newOwners]);
    }

    function setOwnersInternal(owners: TemplateOwner[]) {        
        if (owners.length === 1) {
            const newOwners = [({ ...owners[0], isMain: true })];
            setOwners(newOwners);
        } else {
            setOwners(owners);
        }
    }

    const isAlreadyOwner = (owner: TemplateOwner) => owners.some(o => o.id === owner.id);

    async function saveOwners() {
        try {
            await updateOwners(template.id, owners);
        } catch {
            toast.error("Saving owners failed.");
        };
    }

    const removedOneselfFromOwners = template.owners.find(o => o.id === userInfo.id) && !owners.find(o => o.id === userInfo.id);
    const maxNumberExceeded = (owners.length > 10);
    const isValid = !maxNumberExceeded && owners.filter(o => o.isMain).length === 1;

    return (
        <Page>
            <SubPageContainer>
                <WaitForData dataAvailable={dataAvailable}>
                    <PageLeft>
                        <Header $tileView={false}>
                            <PageTitle>Manage Owners</PageTitle>
                        </Header>
                        <StyledLabel>Template:</StyledLabel>
                        <Box>{template.name}</Box>
                        <StyledLabel>Owners:</StyledLabel>
                        <Rect>
                            {owners.map(o =>
                                <React.Fragment key={o.id}>
                                    <StyledBox>
                                        <IconContainer addRightSpace><UserIcon />
                                            <div>
                                                <StyledDivWithTooltivOnOverflow id={`ownername-div-${o.id}`}>{o.displayName}</StyledDivWithTooltivOnOverflow>
                                            </div>
                                        </IconContainer>
                                        <SpacerDiv />
                                        {o.isMain && <div>
                                            <MainOwnerBadge>Main Owner</MainOwnerBadge>
                                        </div>}
                                        {!o.isMain && <div>
                                            <ClickableBadge onClick={() => setMainOwner(o)}>Set as Main Owner</ClickableBadge>
                                        </div>}
                                        <div>
                                            <IconContainer onClick={() => removeOwner(o)} highlightOnHover><TrashIcon /></IconContainer>
                                        </div>
                                    </StyledBox>
                                </React.Fragment>
                            )}
                            <SpacerDiv />
                            <StyledItemContainerAction>
                                <ModalTriggerButton
                                    color="secondary"
                                    modal={
                                        {
                                            component: UserSearchModal,
                                            props: {
                                                title: "Add Template Owner",
                                                confirmAction: addOwnerAction,
                                                isSearchResultDisabled: isAlreadyOwner,
                                                confirmButtonLabel: "Add",
                                            }
                                        }}>
                                    <IconContainer addRightSpace><PlusIcon />Add Owner</IconContainer>
                                </ModalTriggerButton>
                            </StyledItemContainerAction>
                        </Rect>

                        {maxNumberExceeded &&
                            <PageAlert color="danger" $tileView={false}>{`The maximum number of owners (10) exceeded`}</PageAlert>}

                        {!owners.some(o => o.isMain) &&
                            <PageAlert color="danger" $tileView={false}>{`The custom template requires a main owner`}</PageAlert>}

                        {removedOneselfFromOwners && !userInfo.isAdmin &&
                            <PageAlert color="warning">{`Be advised: you will no longer be able to edit the custom template.`}</PageAlert>}

                        <StyledButtonGroup>
                            <StyledButton color="primary" onClick={async () => { await saveOwners(); navigate(-1); }} disabled={!isValid}>Save</StyledButton>
                            <StyledButton color="secondary" onClick={() => navigate(-1)}>Cancel</StyledButton>
                        </StyledButtonGroup>
                    </PageLeft>
                </WaitForData>
            </SubPageContainer>
        </Page>
    );
};
