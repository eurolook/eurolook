import React from 'react';
import { LinkButton } from 'components/LinkButtons';
import { TemplateDetails } from 'Models';
import { UsersIcon } from '@heroicons/react/24/outline';
import { RoutingPaths } from 'Routing';
import { IconContainer } from 'theme';

export function ManageOwnersButton(props: { template: TemplateDetails; className?: string; disabled?: boolean; }) {
    const { template, ...other } = props;

    return (
        <LinkButton to={RoutingPaths.getTemplateOwnersPage(template.id)} {...other}>
            <IconContainer><UsersIcon />Manage Owners</IconContainer>
        </LinkButton>
    );
}
