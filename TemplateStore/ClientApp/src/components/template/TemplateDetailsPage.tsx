import React from 'react';
import { TemplateProperty } from './TemplateProperty';
import { LinkButton } from 'components/LinkButtons';
import { TemplateDetails, unloadedTemplateDetails } from 'Models';
import { TemplateDetailsFormatter } from 'TemplateDetailsFormatter';
import { useParams } from "react-router-dom";
import { DeleteTemplateButton } from './DeleteTemplateButton';
import { ApplicationState } from 'store';

import { useSelector, useDispatch } from 'react-redux';
import { actionCreators as templateDetailsActions } from 'store/TemplateDetails';

import { PencilIcon } from '@heroicons/react/24/outline';

import { RoutingPaths } from 'Routing';
import { Page, Header, PageLeft, SubPageContainer, PageTitle } from 'components/page/Page';
import { ButtonGroup, IconContainer, PropertiesBox } from 'theme';
import styled from 'styled-components/macro';
import { TemplatePreview } from './TemplatePreview';
import { useAsync } from 'react-async-hook';
import { NoFilesAttachedWarning, TemplateNotPublishedWarning } from './WarningAlerts';

import './TemplateDetailsPage.scss';
import { CopyLinkModalButton } from './CopyLinkModal';
import { userInfoAtom } from '../sidebar/UserInfo';
import { useRecoilValue } from 'recoil';
import { AddOpenEurolookButton } from '../AddOpenEurolookButton';
import { WaitForData } from '../WaitForData';
import { ShareTemplateModalButton } from './ShareTemplateModal';
import { useLinks } from '../useLinks';
import { ManagePublicationsButton } from './publication/ManagePublicationsButton';
import { ManageOwnersButton } from './owners/ManageOwnersButton';
import { ManageTemplateFilesButton } from './files/ManageTemplateFilesButton';


const StyledAddOpenInEurolookButton = styled(AddOpenEurolookButton)`
    margin-top:20px;
`;

const StyledPropertiesBox = styled(PropertiesBox)`
  margin-top: 20px;
  margin-bottom: 20px;

`;

export const eurolookLinkButtonTitle = "Add to Eurolook";

export function TemplateDetailsPage() {

    const params = useParams<{ id: string }>();
    const dispatch = useDispatch();
    const templateDetailsState = useSelector((state: ApplicationState) => state.templateDetails);
    const userInfo = useRecoilValue(userInfoAtom);
    const template: TemplateDetails = templateDetailsState.templates.find(t => t.id === params.id) ?? unloadedTemplateDetails;
    const { getEl10LinkToTemplate, getHttpLinkToTemplateDetailsPage } = useLinks();

    const requestTemplate = async (id: string) => await dispatch(templateDetailsActions.requestTemplateDetails(id));
    const asyncRequestTemplate = useAsync(requestTemplate, [params.id!]);

    const templateDetailsFormatter = new TemplateDetailsFormatter(template);
    const mainOwner = templateDetailsFormatter.getMainOwner();

    const elLink = getEl10LinkToTemplate(template.id);
    const httpLink = getHttpLinkToTemplateDetailsPage(template.id);

    const encodedTemplateName = encodeURIComponent(template.name);
    const mailtoSubject = `Template: ${encodedTemplateName}`;
    const mailtoBody =
        `The template "${encodedTemplateName}" is now available in the Template Store: ${httpLink}%0D%0A%0D%0A` +
        `Use the ${eurolookLinkButtonTitle} button in the Template Store and the template will become available in Eurolook for further use.%0D%0A%0D%0A` +
        `Find out more about the Template Store in the Online Help:%0D%0A` +
        `https://europa.eu/!RhKmTY`;

    const hasFilesAttached = template.languages.length > 0;
    const isPublishable = !template.isPublic && hasFilesAttached;
    const canBeEdited = !!template?.id && (template.isEditable || userInfo.isAdmin);

    return (
        <Page>
            <WaitForData dataAvailable={!asyncRequestTemplate.loading}>
                {asyncRequestTemplate.result && <SubPageContainer>
                    <PageLeft>
                        <Header $tileView={false}>
                            <PageTitle>{template.name}</PageTitle>
                        </Header>
                        <div className="template-actions">
                            <div className="template-owner">{`${mainOwner?.displayName} - ${mainOwner?.orgaName}`}</div>
                            <div>
                                {/*<LinkButton to={`#`} disabled={true}><FavoritesIcon/>Add to Favourites</LinkButton>*/}
                                <CopyLinkModalButton {...{ elLink, httpLink }} />
                                <ShareTemplateModalButton {...{ template, mailtoSubject, mailtoBody }} />
                            </div>
                        </div>
                        <div className="template-description">
                            <span>{template.description}</span>
                        </div>
                        <StyledPropertiesBox>
                            <TemplateProperty label="Eurolook Template">{template.baseDocumentModelName}</TemplateProperty>
                            <TemplateProperty label="Languages">{templateDetailsFormatter.getLanguagesString()}</TemplateProperty>
                            <TemplateProperty label="Availability">{templateDetailsFormatter.getPublicationSummary()}</TemplateProperty>
                            <TemplateProperty label="Main Owner"><a href={`mailto:${mainOwner?.email}?subject=${mailtoSubject}`}>{mainOwner?.displayName}</a></TemplateProperty>
                            <TemplateProperty label="Users"><div>{template.userCount}</div></TemplateProperty>
                            <TemplateProperty label="Last Updated"><div>{templateDetailsFormatter.getLastUpdateString()}</div></TemplateProperty>
                        </StyledPropertiesBox>

                        {!hasFilesAttached && <NoFilesAttachedWarning templateId={template.id} />}
                        {hasFilesAttached && isPublishable && <TemplateNotPublishedWarning template={template} />}

                        <ButtonGroup leftAligned>
                            {canBeEdited &&
                                <React.Fragment>
                                    <ManageTemplateFilesButton templateId={template.id} />
                                    <ManagePublicationsButton template={template} />
                                    <ManageOwnersButton template={template} />
                                    <LinkButton to={RoutingPaths.getTemplateEditingPage(template.id)}><IconContainer><PencilIcon />Edit Properties</IconContainer></LinkButton>
                                    <DeleteTemplateButton templateId={template.id} templateName={template.name} />
                                </React.Fragment>}
                        </ButtonGroup>

                        <StyledAddOpenInEurolookButton
                            template={template}
                            disabled={!hasFilesAttached}
                            className="btn">
                        </StyledAddOpenInEurolookButton>

                    </PageLeft>
                    <TemplatePreview template={template} canRecreatePreview={hasFilesAttached && canBeEdited} />
                </SubPageContainer>}
            </WaitForData>
            {asyncRequestTemplate.error &&
                <Header $tileView={false}>
                    <p className="page-title">Loading template failed.</p>
                </Header>
            }
        </Page>
    );
};
