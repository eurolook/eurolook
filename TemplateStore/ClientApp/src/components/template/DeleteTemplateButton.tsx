import * as React from 'react';
import * as api from 'api';

import { TrashIcon } from '@heroicons/react/24/outline';

import { useNavigate } from "react-router-dom";
import { BaseModal, ModalTitle, ModalParagraph, BaseModalProps } from 'components/modals/BaseModal';
import { TransparentModalTriggerButton } from 'components/modals/ModalTriggerButton';
import { Alert, UncontrolledTooltip } from 'reactstrap';
import { toast } from 'react-toastify';
import { RoutingPaths } from '../../Routing';
import { IconContainer } from '../../theme';

const DeleteTemplateModal = (props: BaseModalProps) => {
    return (
        <BaseModal {...props}>
            <React.Fragment>
                <ModalTitle>{'Delete template'}</ModalTitle>
                <ModalParagraph>
                    Do you really want to delete this template?
                </ModalParagraph>
                <Alert color="warning">
                    <ModalParagraph>
                        The template and all its files will be removed from the Template Store. The template will no longer be available in Eurolook. This process cannot be undone.
                    </ModalParagraph>
                    <ModalParagraph>
                        If you simply want to remove or update a template file, use <b>Manage Template Files</b> instead.
                    </ModalParagraph>
                </Alert>
            </React.Fragment>
        </BaseModal>);
};

export const DeleteTemplateButton = (props: { templateId: string, templateName: string }) => {

    const navigate = useNavigate();

    const deleteTemplate = async () => {
        try {
            await api.deleteTemplate(props.templateId);
            navigate(RoutingPaths.myTemplates);
        } catch (e: any) {
            toast.error(e.message);
        }
    };

    const modal =
    {
        component: DeleteTemplateModal,
        props: {
            confirmAction: deleteTemplate,
        }
    }

    return <>
        <TransparentModalTriggerButton modal={modal} id="deleteTemplate">
            <IconContainer><TrashIcon />Delete</IconContainer>
        </TransparentModalTriggerButton>
        <UncontrolledTooltip placement="top" target="deleteTemplate">Delete template</UncontrolledTooltip>
    </>
}
