import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Label, Alert } from 'reactstrap';
import * as api from 'api';
import { IModalProps } from 'components/modals/ModalTriggerButton';
import { RoutingPaths } from 'Routing';
import { toast } from 'react-toastify';
import { NewTemplate } from 'Models';
import { StyledInput } from 'theme';
import { actionCreators as searchActions } from 'store/Search';
import { useNavigate } from 'react-router-dom';
import { BaseModal, ModalTitle, ModalParagraph } from 'components/modals/BaseModal';

const initialState = { name: '', description: '', tags: '' };

export const NewTemplateModal = (props: IModalProps) => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    
    const [newTemplate, setNewTemplate] = React.useState<NewTemplate>(initialState);
    const [isNameFocused, setNameFocused] = React.useState(true);
    const onFocusName = () => setNameFocused(true);
    const onFocusSomething = () => setNameFocused(false);

    useEffect(() => {
        setNewTemplate(initialState);
    }, [props.isOpen]);


    const createNewTemplate = async () => {
        let templateId;

        try {
            templateId = await api.createTemplate(newTemplate);
        } catch (e: any) {
            toast.error(`Failed to create custom template: ${e.message}`);
            return;
        }

        // reset search to refetch templates on MyTemplates page
        dispatch(searchActions.resetSearch());
        navigate(RoutingPaths.getTemplateFilesPage(templateId));
        
        props.closeModal();
    };

    const handleNameChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
        setNewTemplate({ ...newTemplate, name: event.target.value });
    }

    const handleDescriptionChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
        setNewTemplate({ ...newTemplate, description: event.target.value });
        }

    const handleTagsChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
        setNewTemplate({ ...newTemplate, tags: event.target.value });
    }

    const isInvalid: boolean = !newTemplate.name;

    return (
        <BaseModal confirmAction={createNewTemplate} confirmActionDisabled={isInvalid} {...props}>
            <ModalTitle>Create New Custom Template</ModalTitle>

            <ModalParagraph>
                <Label for="templateName">Name:</Label>
                <StyledInput
                    id="templateName"
                    value={newTemplate.name}
                    placeholder="Enter a name for the custom template"
                    onChange={handleNameChange}
                    onFocus={onFocusName}
                    autoFocus>
                </StyledInput>
                {!newTemplate.name && !isNameFocused && <Alert color="danger">{`Template name is required.`}</Alert>}
            </ModalParagraph>

            <ModalParagraph>
                <Label for="templateDescription">Description:</Label>
                <StyledInput
                    id="templateDescription"
                    type="textarea"
                    value={newTemplate.description}
                    placeholder="Enter a description for the custom template"
                    onChange={handleDescriptionChange}
                    onFocus={onFocusSomething}>
                </StyledInput>
            </ModalParagraph>

            <ModalParagraph>
                <Label for="templateTags">Search Keywords:</Label>
                <StyledInput
                    id="templateTags"
                    placeholder="Enter search keywords / tags for the custom template"
                    value={newTemplate.tags}
                    onChange={handleTagsChange}
                    onFocus={onFocusSomething}>
                </StyledInput>
             </ModalParagraph>
        </BaseModal>
    );
};
