import React, { useEffect } from 'react';
import { TemplateProperty } from './TemplateProperty';
import { TemplateDetails, unloadedTemplateDetails } from 'Models';
import { TemplateDetailsFormatter } from 'TemplateDetailsFormatter';
import { ThumbnailView } from './thumbnail/ThumbnailView';
import { Alert } from 'reactstrap';
import { useParams, useNavigate } from "react-router-dom";
import { TemplatePreview } from './TemplatePreview';
import { StyledInput, StyledButton, ButtonGroup, StyledLabel, LabelWithoutTopMargin, PropertiesBox, IconContainer } from 'theme';

import { ApplicationState } from 'store';

import { useSelector, useDispatch } from 'react-redux';
import { actionCreators as templateDetailsActions } from 'store/TemplateDetails';
import { Page, Header, PageTitle, PageLeft, SubPageContainer } from 'components/page/Page';
import { NoFilesAttachedWarning } from './WarningAlerts';

import { RoutingPaths } from 'Routing';
import './TemplateDetailsPage.scss';
import styled from 'styled-components/macro';
import { userInfoAtom } from '../sidebar/UserInfo';
import { useRecoilValue } from 'recoil';
import { WaitForData } from '../WaitForData';
import { BuildingStorefrontIcon } from '@heroicons/react/24/outline';
import { CheckboxLine } from "components/CheckboxLine";

const StyledPropertiesBox = styled(PropertiesBox)`
  margin-top: 20px;
  margin-bottom: 20px;
`;

const StyledIconContainer = styled(IconContainer)`
    color: ${props => props.theme.primaryColor}
`

export function TemplateEditingPage() {
  
    const navigate = useNavigate();    
    const params = useParams<{ id: string }>();
    const dispatch = useDispatch();
    const templateDetailsState = useSelector((state: ApplicationState) => state.templateDetails);
    const userInfo = useRecoilValue(userInfoAtom);
    const template: TemplateDetails = templateDetailsState.templates.find(t => t.id === params.id) ?? unloadedTemplateDetails;

    const templateDetailsFormatter = new TemplateDetailsFormatter(template);

    useEffect(() => {
        if (params.id) {
            dispatch(templateDetailsActions.requestTemplateDetails(params.id));
        }
    }, [dispatch, params.id, template.thumbnailBase64]);

    const handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newDraft = { ...template.draft, name: event.target.value };
        dispatch(templateDetailsActions.editTemplateDetails(params.id!, newDraft));
    }

    const handleDescriptionChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newDraft = { ...template.draft, description: event.target.value };
        dispatch(templateDetailsActions.editTemplateDetails(params.id!, newDraft));
    }

    const handleTagsChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newDraft = { ...template.draft, tags: event.target.value };
        dispatch(templateDetailsActions.editTemplateDetails(params.id!, newDraft));
    }

    const handleIsSignerFixedChanged = () => {
        const newDraft = { ...template.draft, isSignerFixed: !template.draft.isSignerFixed };
        dispatch(templateDetailsActions.editTemplateDetails(params.id!, newDraft));
    }

    const handleIsFeaturedChanged = () => {
        const newDraft = { ...template.draft, isFeatured: !template.draft.isFeatured };
        dispatch(templateDetailsActions.editTemplateDetails(params.id!, newDraft));
    }

    const saveTemplate = async () => {

        const updatedTemplate = {
            ...template,
            name: template.draft.name,
            description: template.draft.description,
            tags: template.draft.tags,
        };

        dispatch(templateDetailsActions.saveTemplateDetails(updatedTemplate));
        navigate(RoutingPaths.getTemplateDetailsPage(template.id));
    };

    const cancelEdit = () => {
        navigate(RoutingPaths.getTemplateDetailsPage(template.id));
    }

    const isInvalid = !template.draft.name;
    const hasFilesAttached = template.languages.length > 0;

    const fixedSignerLabel = "User cannot change signatory upon document creation";

    return (
        <Page>
            <WaitForData dataAvailable={!templateDetailsState.isLoading && !!template.id}>
                <SubPageContainer>
                    <PageLeft>
                        <Header $tileView={false}>
                            <PageTitle>{template.draft.name ? template.draft.name : '<Template Name>'}</PageTitle>
                        </Header>

                        <LabelWithoutTopMargin for="templateName">Name:</LabelWithoutTopMargin>
                        <StyledInput id="templateName" value={template.draft.name ?? ''} onChange={handleNameChange}></StyledInput>
                        {!template.draft.name && <Alert color="danger">{`Template name is required.`}</Alert>}

                        <StyledLabel for="templateDescription">Description:</StyledLabel>
                        <StyledInput
                            id="templateDescription"
                            type="textarea"
                            value={template.draft.description ?? ''}
                            onChange={handleDescriptionChange}
                            placeholder="Enter a description for the custom template"
                            autoFocus>
                        </StyledInput>

                        <StyledLabel for="templateTags">Search Keywords:</StyledLabel>
                        <StyledInput
                            id="templateTags"
                            value={template.draft.tags ?? ''}
                            placeholder="Enter search keywords for the custom template" onChange={handleTagsChange}>
                        </StyledInput>

                        {
                            template.hasSigner &&
                            <>
                                <StyledLabel>Document Creation Options:</StyledLabel>
                                <CheckboxLine toggle={handleIsSignerFixedChanged} checked={template.draft.isSignerFixed}>
                                    {fixedSignerLabel}
                                </CheckboxLine>
                            </>
                        }

                        {
                            userInfo.isAdmin &&
                            <>
                                <StyledLabel>Featured Status:</StyledLabel>
                                <CheckboxLine toggle={handleIsFeaturedChanged} checked={template.draft.isFeatured}>
                                    Show custom template in the featured section of the<StyledIconContainer iconHeight={20} addRightSpace={4}><BuildingStorefrontIcon />Home </StyledIconContainer>page
                                </CheckboxLine>
                            </>
                        }

                        <StyledLabel>Thumbnail:</StyledLabel>
                        <ThumbnailView template={template} />

                        <StyledPropertiesBox>
                            <TemplateProperty label="Eurolook Template">{template.baseDocumentModelName}</TemplateProperty>
                            <TemplateProperty label="Users"><div>{template.userCount}</div></TemplateProperty>
                            <TemplateProperty label="Last Updated"><div>{templateDetailsFormatter.getLastUpdateString()}</div></TemplateProperty>
                        </StyledPropertiesBox>

                        {!hasFilesAttached && <NoFilesAttachedWarning templateId={template.id} />}

                        <ButtonGroup>
                            <StyledButton color="primary" onClick={saveTemplate} disabled={isInvalid}>Save</StyledButton>
                            <StyledButton color="secondary" onClick={cancelEdit}>Cancel</StyledButton>
                        </ButtonGroup>
                    </PageLeft>
                    <TemplatePreview template={template} canRecreatePreview={hasFilesAttached && (template.isEditable || userInfo.isAdmin)} />
                </SubPageContainer>
            </WaitForData>
        </Page>
    );
};
