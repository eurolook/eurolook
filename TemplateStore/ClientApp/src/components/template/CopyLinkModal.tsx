import React, { useRef } from "react";
import { Label, UncontrolledTooltip } from "reactstrap";
import styled from "styled-components";
import { FlexDiv, IconContainer, StyledButton, StyledInput } from "../../theme";
import { BaseModal, BaseModalProps, ModalParagraph, ModalTitle } from "../modals/BaseModal";
import { TransparentModalTriggerButton } from "../modals/ModalTriggerButton";
import { toast } from 'react-toastify';
import { ClipboardIcon, CheckCircleIcon } from '@heroicons/react/24/outline';

interface ICopyLinkModalProps {
    elLink: string,
    httpLink: string
}

const StyledBaseModal = styled(BaseModal)`
    min-width: 600px;
`;

const ModalParagraphWithMargin = styled(ModalParagraph)`
    margin-bottom: 30px;
`;

const CopyLinkModal = (props: ICopyLinkModalProps & BaseModalProps) => {

    const { elLink, httpLink, closeModal, ...other } = props;

    const elLinkInputRef = useRef<HTMLInputElement>(null);
    const httpLinkInputRef = useRef(null);


    //Copying the link to the clipboard via parent.navigator.clipboard.writeText(...) does not work in cefsharp at this point
    //async function copyToClipboardAndClodeModal(text: string) {        
    //    await parent.navigator.clipboard.writeText(text);
    //    toast.success(<><ClipboardCheckIcon />Link copied to clipboard</>);
    //    closeModal();
    //};

    async function copyInputValueToClipboardAndClodeModal(inputRef: HTMLInputElement | null) {
        if (inputRef) {            
            inputRef.focus();
            inputRef.select();
            document.execCommand('copy')
            toast.success(<IconContainer addRightSpace><CheckCircleIcon />Link copied to clipboard</IconContainer>);
            closeModal();
        }
    };

    return (
        <StyledBaseModal hideConfirmButton {...{ closeModal }} {...other}>
            <ModalTitle>Copy link to clipboard</ModalTitle>

            <ModalParagraph>
                <Label>Link to open the template in Eurolook:</Label>
                <FlexDiv>
                    <StyledInput
                        value={elLink}
                        onChange={() => { }}
                        innerRef={elLinkInputRef}>
                    </StyledInput>                   
                    {<StyledButton color="primary" onClick={() => copyInputValueToClipboardAndClodeModal(elLinkInputRef?.current)}>
                        Copy Link
                    </StyledButton>}
                </FlexDiv>
            </ModalParagraph>

            <ModalParagraphWithMargin>
                <Label>Link to the template in the Template Store:</Label>
                <FlexDiv>
                    <StyledInput
                        value={httpLink}
                        onChange={() => { }}
                        innerRef={httpLinkInputRef}>
                    </StyledInput>
                    <StyledButton color="primary" onClick={() => copyInputValueToClipboardAndClodeModal(httpLinkInputRef?.current)}>
                        Copy Link
                    </StyledButton>
                </FlexDiv>
            </ModalParagraphWithMargin>
        </StyledBaseModal>
    );
}

export function CopyLinkModalButton(props: ICopyLinkModalProps) {
    const modal =
    {
        component: CopyLinkModal,
        props: {
            confirmAction: async () => { },
            elLink: props.elLink,
            httpLink: props.httpLink,
        }
    }

    return <>
        <TransparentModalTriggerButton modal={modal} id="copyLink">
            <IconContainer><ClipboardIcon />Copy Link</IconContainer>
        </TransparentModalTriggerButton>
        <UncontrolledTooltip placement="top" target="copyLink">Copy link to template into clipboard</UncontrolledTooltip>
    </>;
}
