import React from 'react';
import styled from 'styled-components/macro';
import { DivWithTooltipOnOverflow } from '../DivWithTooltipOnOverflow';


const TemplatePropertyDiv = styled.div`
    margin-top: 10px;
    margin-bottom: 10px;
    flex-basis: 130px;
    flex-grow: 1;

    label {
        font-family: "Segoe UI Semibold"
    }
`;

const StyledDivWithTooltipOnOverflow = styled(DivWithTooltipOnOverflow)`
    max-width:148px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
`;

export function TemplateProperty(props: {label: string, children: React.ReactNode }) {

    const { label, children } = props;
    const id = label.replace(/[^a-zA-Z0-9]/g, '-').toLowerCase() + "-property";

    return (
        <TemplatePropertyDiv>
            <label>{label}</label>
            <StyledDivWithTooltipOnOverflow id={id}>{children}</StyledDivWithTooltipOnOverflow>
        </TemplatePropertyDiv>
    );
}
