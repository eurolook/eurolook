import { Alert } from 'reactstrap';
import styled from "styled-components";
import { MediaQueries } from 'theme';
import { DivWithTooltipOnOverflow } from '../DivWithTooltipOnOverflow';


export const Page = styled.div`
    margin-left: ${props => props.theme.pageMargin}px;
    margin-right: 4px;
`;

export const SubPageContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
`;

export const PageLeft = styled.div`
    max-width: 594px;
    margin-right: 40px;
    flex-basis: 482px;
    min-width: 220px;
`;

export const PageRight = styled.div`
    margin-top: 20px;
    width: 360px;
`;

export const PageTitle = styled(DivWithTooltipOnOverflow).attrs(props => ({
    id: "pageTitle"
}))`
  font-family: Segoe UI Semibold;
  font-size: 24px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  min-width: 200px;
  margin: 10px 0 6px 0;
  padding: 0 4px 4px 0;
`;

export const SectionTitle = styled(PageTitle)`
  font-size: 20px;
`;

export const Header = styled.div`
    display: flex;
    justify-content: space-between;
    flex-flow: wrap;
    margin: 20px 0 9px 0;
    align-items: center;
    max-width: ${(props) => props.theme.pageWidthXl}px;

    ${(props: { $tileView: boolean }) => props.$tileView && MediaQueries }
`

export const PageActions = styled.div`

    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    column-gap: 10px;

    & > div {
    margin-bottom: 8px;
    }

    .dropdown {
        width: 154px;

        button {
            width: 100%;
            height: 30px;
        }
    }

    max-width: ${(props) => props.theme.pageWidthXl}px;

    ${(props: { $tileView: boolean }) => props.$tileView && MediaQueries }
`

export const PageAlert = styled(Alert)`
    margin-bottom: 10px;
    max-width: ${(props) => props.theme.pageWidthXl}px;

    ${(props: { $tileView: boolean }) => props.$tileView && MediaQueries}
`;
