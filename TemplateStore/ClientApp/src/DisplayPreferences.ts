import { atom, useRecoilState, useRecoilValue, useSetRecoilState } from "recoil";
import { getDisplayPreferences, putDisplayPreferences } from "./api";
import { userInfoAtom } from "./components/sidebar/UserInfo";
import { DisplayPreferences, KeyedViewSetting, OrderByPropertyEnum, QuickFilter, UserViewSetting } from "./Models";

const defaultUserViewSetting: UserViewSetting = {
    orderAsc: true,
    orderByProperty: OrderByPropertyEnum.templateName,
    tileView: true
};

export const displayPreferencesAtom = atom<DisplayPreferences>({
    key: "displayPreferencesAtom",
    default: {
        isInitialized: false,
        viewSettings: [],
        quickFilters: []
    }
});


function saveToServer(displayPreferences: DisplayPreferences, isImpersonated: boolean) {  
    if (isImpersonated) {
        console.log("display preferences won't be saved to the DB while impersonated.");
    } else {
        putDisplayPreferences(displayPreferences);
    }
}

export function useViewSettings(viewId: string) {
    const userInfo = useRecoilValue(userInfoAtom);
    const [displayPreferences, setDisplayPreferences] = useRecoilState(displayPreferencesAtom);
    const { viewSettings, isInitialized } = displayPreferences;

    const viewSetting = viewSettings.find(vs => vs.viewId === viewId)?.viewSetting ?? defaultUserViewSetting;

    function addOrUpdateViewSetting(viewSetting: UserViewSetting) {
        const newViewSettings = [...viewSettings.filter(vs => vs.viewId !== viewId), { viewId, viewSetting }];
        const newDisplayPreferences = { ...displayPreferences, viewSettings: newViewSettings };
        setDisplayPreferences(newDisplayPreferences);
        saveToServer(newDisplayPreferences, userInfo.isImpersonated);
    };

    return { viewSetting, isInitialized, addOrUpdateViewSetting };
};


export function useQuickFilters() {
    const userInfo = useRecoilValue(userInfoAtom);
    const [displayPreferences, setDisplayPreferences] = useRecoilState(displayPreferencesAtom);
    const { quickFilters, isInitialized } = displayPreferences;

    function setQuickFilters(newQuickFilters: QuickFilter[]) {

        // cleanup unused viewSettings
        const usedFilterIds = [...userInfo.orgaEntities, ...newQuickFilters].map(item => item.id);
        const filterPrefix = 'filter_';
        const isViewSettingUsed = (vs: KeyedViewSetting) => !vs.viewId.startsWith(filterPrefix) || usedFilterIds.includes(vs.viewId.substring(filterPrefix.length));
        const newViewSettings = displayPreferences.viewSettings.filter(vs => isViewSettingUsed(vs));

        const newDisplayPreferences = { ...displayPreferences, viewSettings: newViewSettings, quickFilters: newQuickFilters };
        setDisplayPreferences(newDisplayPreferences);
        saveToServer(newDisplayPreferences, userInfo.isImpersonated);
    };

    return { isInitialized, quickFilters, setQuickFilters };
};

export function useDisplayPreferencesFetch() {

    const setDisplayPreferences = useSetRecoilState(displayPreferencesAtom);    

    async function fetchDisplayPreferences() {

        const data = await getDisplayPreferences();
        const validatedViewSettings = data && Array.isArray(data?.viewSettings) ? data.viewSettings : [];
        const validatedQuickFilters = data && Array.isArray(data?.quickFilters) ? data.quickFilters : [];

        setDisplayPreferences(
            {
                isInitialized: true,
                viewSettings: validatedViewSettings,
                quickFilters: validatedQuickFilters
            });
    }

    return { fetchDisplayPreferences };
}
