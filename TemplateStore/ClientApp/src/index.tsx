import 'bootstrap/dist/css/bootstrap.css';

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
//import { ConnectedRouter } from 'connected-react-router';

//import { createBrowserHistory } from 'history';
import configureStore from './store/configureStore';

import { ThemeProvider } from 'styled-components';
import { Theme } from 'theme';

import { App } from './App';
//import registerServiceWorker from './registerServiceWorker';
import { unregister } from './registerServiceWorker';

// Create browser history to use in the Redux store
//const history = createBrowserHistory({ basename: process.env.REACT_APP_BASE_PATH });

console.log("REACT_APP_BASE_PATH: " + process.env.REACT_APP_BASE_PATH);
const basename = process.env.REACT_APP_BASE_PATH ?? '/';

// Get the application-wide store instance, prepopulating with state from the server where available.
const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <ThemeProvider theme={Theme}>
            <App basename={basename} />
        </ThemeProvider>
    </Provider>,
    document.getElementById('root'));

unregister();
