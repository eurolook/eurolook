using Microsoft.AspNetCore.Http;

namespace TemplateStore.ExtensionMethods
{
    public static class HttpRequestExtensions
    {
        public static bool IsImpersonated(this HttpRequest request)
        {
            return request.Query["impersonate"].Count > 0;
        }

        public static string GetLoginOfAuthorizedIdentity(this HttpRequest request)
        {
            return request.HttpContext.User.Identity.Name;
        }
    }
}
