﻿namespace TemplateStore.DisplayPreference
{
    public class KeyedViewSetting
    {
        public string ViewId { get; set; }
        public UserViewSetting ViewSetting { get; set; }
    }
}
