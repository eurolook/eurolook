﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Serilog;
using TemplateStore.ExtensionMethods;
using TemplateStore.Users;

namespace TemplateStore.DisplayPreference
{
    [Route("api/[controller]")]
    [ApiController]
    public class DisplayPreferencesController : ControllerBase
    {
        private readonly IEurolookUserService userService;

        public DisplayPreferencesController(IEurolookUserService userService)
        {
            this.userService = userService;
        }

        [HttpGet]
        public async Task<ActionResult<DisplayPreferences>> GetDisplayPreferences()
        {
            var user = await userService.GetEurolookUser(Request);
            if (user == null)
            {
                return Unauthorized();
            }

            var result = new DisplayPreferences();
            try
            {
                if (user.Settings.TemplateStoreViewSettings != null)
                {
                    result = JsonConvert.DeserializeObject<DisplayPreferences>(user.Settings.TemplateStoreViewSettings);
                    result.ViewSettings = result.ViewSettings.Where(x => x?.ViewId != null).ToArray();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Stored display preferences are not valid");
            }

            return Ok(result);
        }

        [HttpPut]
        public async Task<ActionResult> SetDisplayPreferences([FromBody] DisplayPreferences displayPreferences)
        {
            if (Request.IsImpersonated())
            {
                return BadRequest("Cannot store display preferences while impersonated.");
            }

            displayPreferences.QuickFilters = displayPreferences.QuickFilters.DistinctBy(q => q.Id).Take(10).ToArray();

            await userService.SaveUserDisplayPreferences(Request, JsonConvert.SerializeObject(displayPreferences));

            return Ok();
        }
    }
}
