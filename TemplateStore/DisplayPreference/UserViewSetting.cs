﻿namespace TemplateStore.DisplayPreference
{
    public class UserViewSetting
    {
        public OrderByPropertyEnum OrderByProperty { get; set; }
        public bool OrderAsc { get; set; }
        public bool TileView { get; set; }
    }
}
