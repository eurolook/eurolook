﻿namespace TemplateStore.DisplayPreference
{
    // Has to be kept in sync with respective enum in client side code
    public enum OrderByPropertyEnum
    {
        TemplateName,
        EurolookTemplate,
        LastModified
    }
}
