﻿using System;

namespace TemplateStore.DisplayPreference
{
    public class DisplayPreferences
    {
        public QuickFilters[] QuickFilters { get; set; } = Array.Empty<QuickFilters>();
        public KeyedViewSetting[] ViewSettings { get; set; } = Array.Empty<KeyedViewSetting>();
    }
}
