﻿namespace TemplateStore
{
    public static class CacheSettings
    {
        public const int OneHourDuration = 60 * 60;
        public const int OneDayDuration = OneHourDuration * 24;
        public const int OneWeekDuration = OneDayDuration * 7;
        public const int OneMonthDuration = OneDayDuration * 30;
    }
}
