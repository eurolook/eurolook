﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.Models.TemplateStore;
using LinqKit;

namespace TemplateStore.Templates
{
    public class TemplateSearchQuery
    {
        public Guid? LanguageId { get; set; }

        public string Query { get; set; }

        public Guid? BaseDocumentId { get; set; }

        public List<Guid> OrgaEntityIds { get; set; } = new List<Guid>();

        public bool IncludeUnpublished { get; set; }

        public bool IncludeOnlyFeatured { get; set; }

        public int PageSize { get; set; }

        public ExpressionStarter<Template> GetPredicate()
        {
            var predicate = PredicateBuilder.New<Template>();
            if (!string.IsNullOrWhiteSpace(Query))
            {
                predicate = predicate.And(x => x.Name.Contains(Query) || x.Description.Contains(Query) || x.Tags.Contains(Query));
            }

            if (BaseDocumentId != null)
            {
                predicate = predicate.And(x => x.BaseDocumentModelId == BaseDocumentId.Value);
            }

            if (LanguageId != null)
            {
                predicate = predicate.And(x => x.TemplateFiles.Any(tf => tf.LanguageId == LanguageId));
            }

            if (OrgaEntityIds.Any())
            {
                predicate = predicate.And(x => x.Publications.Any(p => OrgaEntityIds.Contains(p.OrgaEntityId)));
            }

            if (!IncludeUnpublished)
            {
                predicate = predicate.And(x => x.Publications.Any());
            }

            if (IncludeOnlyFeatured)
            {
                predicate = predicate.And(x => x.FeaturedUntil > DateTime.UtcNow);
            }

            return predicate;
        }
    }
}
