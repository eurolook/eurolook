using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.Models.TemplateStore;

namespace TemplateStore.Templates
{
    public class StorefrontSection
    {
        public StorefrontSection(string name, IEnumerable<Template> templates, string templateFilter = null)
        {
            Name = name;
            Templates = templates.Select(t => new TemplateDataModel(t));
            TemplateFilter = templateFilter;
        }

        public string Name { get; }

        public IEnumerable<TemplateDataModel> Templates { get; }

        public string TemplateFilter { get; }
    }

    public class StorefrontDataModel
    {
        public List<StorefrontSection> Sections { get; set; } = new List<StorefrontSection>();
    }
}
