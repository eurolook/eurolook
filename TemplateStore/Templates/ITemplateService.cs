﻿using Eurolook.Data.Models.TemplateStore;

namespace TemplateStore.Templates
{
    public interface ITemplateService
    {
        void PrepareForDownload(UserTemplate userTemplate);
    }
}
