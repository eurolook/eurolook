﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using TemplateStore.Preview;
using TemplateStore.Users;
using TemplateStore.DisplayPreference;
using Eurolook.Data.TemplateStore;
using Eurolook.Common;
using TemplateStore.Auth;
using Microsoft.AspNetCore.Server.IISIntegration;

namespace TemplateStore.Templates
{
    [Route("api/[controller]")]
    [ApiController]
    public class TemplatesController : ControllerBase, ICanLog
    {
        private readonly ITemplateDatabase _database;
        private readonly IOrgaEntitiesDatabase _orgaEntitiesDatabase;
        private readonly IEurolookUserService _userService;
        private readonly IAuthorizationService _authorizationService;
        private readonly ITemplateAnalyzer _templateAnalyzer;
        private readonly IPreviewService _previewService;
        private readonly ISharedTemplateService _sharedTemplateService;

        public TemplatesController(
            ITemplateDatabase database,
            IOrgaEntitiesDatabase orgaEntitiesDatabase,
            IEurolookUserService userService,
            IAuthorizationService authorizationService,
            ITemplateAnalyzer templateAnalyzer,
            IPreviewService previewService,
            ISharedTemplateService sharedTemplateService)
        {
            _database = database;
            _orgaEntitiesDatabase = orgaEntitiesDatabase;
            _userService = userService;
            _authorizationService = authorizationService;
            _templateAnalyzer = templateAnalyzer;
            _previewService = previewService;
            _sharedTemplateService = sharedTemplateService;
        }

        [HttpGet]
        public async Task<TemplateDataModel[]> Get(
            [FromQuery(Name = "q")]string query,
            [FromQuery(Name = "l")]Guid? languageId,
            [FromQuery(Name = "b")]Guid? baseDocumentId,
            [FromQuery(Name = "o")]Guid? orgaEntityId,
            [FromQuery(Name = "h")]bool hierarchy,
            [FromQuery(Name = "asc")]bool orderAsc,
            [FromQuery(Name = "orderBy")] OrderByPropertyEnum orderBy)
        {
            try
            {
                var searchQuery = new TemplateSearchQuery
                {
                    Query = query,
                    LanguageId = languageId,
                    BaseDocumentId = baseDocumentId,
                };

                if (orgaEntityId.HasValue)
                {
                    searchQuery.OrgaEntityIds.Add(orgaEntityId.Value);
                }
                
                var paginationInfo = new PaginationInfo
                {
                    OrderProperty = orderBy.ToString(),
                    OrderAscending = orderAsc,
                    PageSize = 51
                };

                var templates = hierarchy
                    ? await GetTemplatesForHierarchy(searchQuery, paginationInfo)
                    : await _database.FindTemplates(searchQuery, paginationInfo);

                return templates.Select(x => new TemplateDataModel(x)).ToArray();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }

        [HttpGet("Languages")]
        [ResponseCache(Duration = CacheSettings.OneWeekDuration, Location = ResponseCacheLocation.Any, NoStore = false)]
        public async Task<LanguageModel[]> GetLanguageFilters()
        {
            try
            {
                var languages = await _database.GetAllLanguages();
                return languages.OrderBy(x => x, new EnFrDeAlphabeticalLanguagesComparer()).Select(
                    x => new LanguageModel
                    {
                        Id = x.Id,
                        Name = x.Name,
                        DisplayName = x.DisplayName
                    }).ToArray();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }

        [HttpGet("BaseDocumentFilters")]
        [ResponseCache(Duration = CacheSettings.OneWeekDuration, Location = ResponseCacheLocation.Any, NoStore = false)]
        public async Task<BaseDocumentFilter[]> GetBaseDocumentFilters()
        {
            try
            {
                var documentModels = await _database.GetAllBaseDocuments();
                return documentModels.Select(
                    x => new BaseDocumentFilter
                    {
                        Id = x.Id,
                        Name = x.DisplayName
                    }).ToArray();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }

        [HttpGet("storefront")]
        public async Task<IActionResult> GetStorefrontTemplates()
        {
            try
            {
                var user = await _userService.GetEurolookUser(Request);
                if (user == null)
                {
                    return Unauthorized();
                }

                var entities = await _userService.GetEntitiesForUser(user);
                var entityIds = entities.Select(e => e.Id).ToList();

                var searchQuery = new TemplateSearchQuery { IncludeOnlyFeatured = true };
                searchQuery.OrgaEntityIds.AddRange(entityIds);

                var paginationInfo = new PaginationInfo
                {
                    OrderAscending = false,
                    OrderProperty = nameof(Updatable.ServerModificationTimeUtc),
                    PageSize = 6
                };

                var featuredTemplates = await _database.FindTemplates(searchQuery, paginationInfo);

                paginationInfo.PageSize = 8;
                var popularTemplates = await _database.GetPopularTemplates(entityIds, paginationInfo);
                var recentlyUsedTemplates = await _database.GetRecentlyUsedTemplates(user, paginationInfo);
                //var hierarchyTemplatesDictionary = await GetTemplatesDictionaryForHierarchy(paginationInfo);

                var result = new StorefrontDataModel();
                result.Sections.Add(new StorefrontSection("Featured Templates", featuredTemplates));
                result.Sections.Add(new StorefrontSection("Popular Templates", popularTemplates));
                result.Sections.Add(new StorefrontSection("Recently Used Templates", recentlyUsedTemplates));
                //var sections = hierarchyTemplatesDictionary
                //               .Where(kvp => kvp.Value.Any())
                //               .Select(kvp => new StorefrontSection($"{new OrgaEntityDataModel(kvp.Key).Name} Templates", kvp.Value, kvp.Key.Id.ToString()));
                //result.Sections.AddRange(sections);

                return Ok(result);
            }

            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }

        [HttpGet("my")]
        public async Task<TemplateDataModel[]> My(
            [FromQuery(Name = "q")] string query,
            [FromQuery(Name = "l")] Guid? languageId,
            [FromQuery(Name = "b")] Guid? baseDocumentId,
            [FromQuery(Name = "asc")] bool orderAsc,
            [FromQuery(Name = "orderBy")] OrderByPropertyEnum orderBy)
        {
            try
            {
                var user = await _userService.GetEurolookUser(Request);

                var searchQuery = new TemplateSearchQuery
                {
                    Query = query,
                    LanguageId = languageId,
                    BaseDocumentId = baseDocumentId,
                    IncludeUnpublished = true
                };

                var paginationInfo = new PaginationInfo
                {
                    OrderProperty = orderBy.ToString(),
                    OrderAscending = orderAsc,
                    PageSize = 51
                };

                var templates = await _database.GetMyTemplates(user, searchQuery, paginationInfo);
                return templates.Select(x => new TemplateDataModel(x)).ToArray();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            try
            {
                var template = await _database.GetTemplateDetails(id);

                var hasSigner = await _database.DocumentModelHasSignerRole(template.BaseDocumentModelId);

                var authResult = await _authorizationService.AuthorizeAsync(User, template, TemplateOperations.Read);
                if (authResult.Succeeded)
                {
                    var user = await _userService.GetEurolookUser(Request);
                    int userProfileCount = await _database.GetUserProfileCount(id);
                    return Ok(new TemplateDetailsModel(template, user, userProfileCount, hasSigner));
                }

                return Forbid();
            }
            catch (ArgumentException aex)
            {
                Log.Error(aex.ToString());
                return BadRequest(aex.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }

        // FIXME: Remove method when client has been updated (moved to data sync service)
        [Authorize(IISDefaults.AuthenticationScheme)]
        [HttpGet("{templateId}/owners")]
        public async Task<IActionResult> GetOwners(Guid templateId)
        {
            try
            {
                var template = await _database.GetTemplateDetails(templateId);

                var authResult = await _authorizationService.AuthorizeAsync(User, template, TemplateOperations.Read);
                if (authResult.Succeeded)
                {
                    return Ok(template.Owners.Select(o => new OwnerDataModel(o)));
                }

                return Forbid();
            }
            catch (ArgumentException aex)
            {
                Log.Error(aex.ToString());
                return BadRequest(aex.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }

        [HttpGet("{templateId}/images")]
        public async Task<IActionResult> GetImages(Guid templateId, [FromQuery] bool small)
        {
            try
            {
                var template = await _database.GetTemplate(templateId);

                var authResult = await _authorizationService.AuthorizeAsync(User, template, TemplateOperations.Read);
                if (authResult.Succeeded)
                {
                    return Ok(new TemplatePreviewImagesDataModel(template, small));
                }

                return Forbid();
            }
            catch (ArgumentException aex)
            {
                Log.Error(aex.ToString());
                return BadRequest(aex.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }

        [HttpPost("{templateId}/images/{language}")]
        public async Task<IActionResult> CreatePreviewImages(Guid templateId, string language)
        {
            try
            {
                var template = await _database.GetTemplateWithIncludes(templateId);

                var authResult = await _authorizationService.AuthorizeAsync(User, template, TemplateOperations.Update);
                if (!authResult.Succeeded)
                {
                    return Forbid();
                }

                if((DateTime.UtcNow - template.PreviewInitiated) < TimeSpan.FromMinutes(5))
                {
                    return StatusCode(StatusCodes.Status429TooManyRequests);
                }

                var templateFile = template.TemplateFiles.FirstOrDefault(tf => tf.Language.Name.EqualsIgnoreCase(language))
                    ?? throw new ArgumentException($"File for language {language} not found.");
                _previewService.ConvertTemplateFileToPdf(templateId, templateFile.Bytes, templateFile.Language.Name);

                return Ok();
            }
            catch (ArgumentException aex)
            {
                Log.Error(aex.ToString());
                return BadRequest(aex.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }

        // FIXME: Remove method when client has been updated (moved to data sync service)
        [Authorize(IISDefaults.AuthenticationScheme)]
        [HttpGet("download/{id}")]
        public async Task<IActionResult> GetTemplateDownload(Guid id)
        {
            try
            {
                var template = await _database.GetTemplateWithIncludes(id);

                template.BaseDocumentModel = null;
                if (template.Owners != null)
                {
                    foreach (var owner in template.Owners)
                    {
                        owner.Template = null;
                        owner.User = null;
                    }
                }

                if (template.Publications != null)
                {
                    foreach (var publication in template.Publications)
                    {
                        publication.Template = null;
                        publication.OrgaEntity = null;
                    }
                }

                if (template.TemplateFiles != null)
                {
                    foreach (var file in template.TemplateFiles)
                    {
                        file.Template = null;
                        file.Language = null;
                    }
                }

                return Ok(template);

            }
            catch (ArgumentException aex)
            {
                Log.Error(aex.ToString());
                return BadRequest(aex.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }

        [HttpGet("{templateId}/export")]
        public async Task<IActionResult> ExportTemplate(Guid templateId)
        {
            try
            {
                var template = await _database.GetTemplate(templateId);

                var authResult = await _authorizationService.AuthorizeAsync(
                    User,
                    template,
                    TemplateOperations.Read);
                if (!authResult.Succeeded)
                {
                    return Forbid();
                }

                using var exportFile = new TemporaryFile();

                if (!await _sharedTemplateService.ExportTemplate(templateId, exportFile.FullName))
                {
                    Log.Error("Export of template failed");
                    return BadRequest("Export of template failed");
                }

                var file = System.IO.File.OpenRead(exportFile.FullName);
                {
                    string contentType = "text/xml";
                    return File(file, contentType);
                }
            }
            catch (ArgumentException aex)
            {
                Log.Error(aex.ToString());
                return BadRequest(aex.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] TemplateDataModel model)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(model.Name))
                {
                    return BadRequest($"Parameter {nameof(model.Name)} cannot be empty.");
                }

                var authResult = await _authorizationService.AuthorizeAsync(User, new Template(), TemplateOperations.Create);
                if (!authResult.Succeeded)
                {
                    return Forbid();
                }

                var user = await _userService.GetEurolookUser(Request);
                var template = await _database.CreateTemplate(model.Name, model.Description, model.Tags, user);
                return Ok(template.Id);
            }
            catch (ArgumentException aex)
            {
                Log.Error(aex.ToString());
                return BadRequest(aex.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }

        [HttpPost("{templateId}/files")]
        public async Task<IActionResult> UploadTemplateFile(Guid templateId, IFormFile file)
        {
            try
            {
                var templateAnalyzerResult = await ValidateFile(file);
                var template = await _database.GetTemplateWithIncludes(templateId);

                var authResult = await _authorizationService.AuthorizeAsync(User, template, TemplateOperations.Update);
                if (authResult.Succeeded)
                {
                    if (template.BaseDocumentModelId != null && template.BaseDocumentModelId != templateAnalyzerResult.BaseDocumentId)
                    {
                        throw new ArgumentException($"Given file is no {template.BaseDocumentModel.Name}.");
                    }

                    var bytes = await GetFileBytes(file);
                    await _database.AddOrUpdateTemplateFile(
                    templateId,
                        // ReSharper disable once PossibleInvalidOperationException
                        templateAnalyzerResult.BaseDocumentId.Value,
                        templateAnalyzerResult.Language,
                        bytes);

                    if (template.PreviewPage1 == null ||
                        templateAnalyzerResult.Language.EqualsIgnoreCase("EN") ||
                        templateAnalyzerResult.Language.EqualsIgnoreCase(template.PreviewLanguage))
                    {
                        _previewService.ConvertTemplateFileToPdf(templateId, bytes, templateAnalyzerResult.Language);
                    }

                    return Ok();
                }

                return Forbid();
            }
            catch (ArgumentException aex)
            {
                Log.Error(aex.ToString());
                return BadRequest(aex.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }

        [HttpPut("{templateId}/thumbnail")]
        public async Task<IActionResult> UploadThumbnail(Guid templateId, IFormFile file)
        {
            try
            {
                var template = await _database.GetTemplateWithIncludes(templateId);

                var authResult = await _authorizationService.AuthorizeAsync(User, template, TemplateOperations.Update);
                if (authResult.Succeeded)
                {
                    if (file.ContentType != "image/png")
                    {
                        return BadRequest("No valid image file.");
                    }

                    if (file.Length > 2 * 1024 * 1024)
                    {
                        return BadRequest("Max file size (2 MB) exceeded.");
                    }

                    var bytes = await GetFileBytes(file);

                    await _database.UpdateThumbnail(templateId, bytes);
                    return Ok();
                }

                return Forbid();
            }
            catch (ArgumentException aex)
            {
                Log.Error(aex.ToString());
                return BadRequest(aex.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }

        [HttpDelete("{templateId}/thumbnail")]
        public async Task<IActionResult> DeleteThumbnail(Guid templateId)
        {
            try
            {
                var template = await _database.GetTemplateWithIncludes(templateId);

                var authResult = await _authorizationService.AuthorizeAsync(User, template, TemplateOperations.Delete);
                if (authResult.Succeeded)
                {
                    await _database.DeleteThumbnail(templateId);
                    return Ok();
                }

                return Forbid();
            }
            catch (ArgumentException aex)
            {
                Log.Error(aex.ToString());
                return BadRequest(aex.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }

        private async Task<TemplateAnalyzerResult> ValidateFile(IFormFile file)
        {
            var templateAnalyzerResult = await _templateAnalyzer.Process(file);
            if (!templateAnalyzerResult.IsEurolookDocument)
            {
                throw new ArgumentException("Given file is no Eurolook document.");
            }

            if (!templateAnalyzerResult.BaseDocumentId.HasValue)
            {
                throw new ArgumentException("Given file is no valid Eurolook document.");
            }

            if(!await _database.DocumentModelExists(templateAnalyzerResult.BaseDocumentId.Value))
            {
                throw new ArgumentException("Given file is no known Eurolook document.");
            }

            if (!templateAnalyzerResult.HasAuthorRoles)
            {
                throw new ArgumentException("The file contains legacy author XML information without author roles. Please recreate the document with a current Eurolook version.");
            }

            return templateAnalyzerResult;
        }

        private static async Task<byte[]> GetFileBytes(IFormFile file)
        {
            try
            {
                await using var memoryStream = new MemoryStream();
                await file.CopyToAsync(memoryStream);
                var bytes = memoryStream.ToArray();
                return bytes;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }

        [HttpGet("{templateId}/files/{languageName}")]
        public async Task<IActionResult> DownloadTemplateFile(Guid templateId, string languageName)
        {
            try
            {
                var templateFile = await _database.GetTemplateFile(templateId, languageName);

                var authResult = await _authorizationService.AuthorizeAsync(
                    User,
                    templateFile.Template,
                    TemplateOperations.Read);
                if (authResult.Succeeded)
                {
                    var content = new MemoryStream(templateFile.Bytes);
                    var contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                    var fileName = $"{templateFile.Template.Name}_{templateFile.Language.Name}.docx";
                    return File(content, contentType, fileName);
                }

                return Forbid();
            }
            catch (ArgumentException aex)
            {
                Log.Error(aex.ToString());
                return BadRequest(aex.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }

        [HttpDelete("{templateId}/files/{languageName}")]
        public async Task<IActionResult> DeleteTemplateFile(Guid templateId, string languageName)
        {
            try
            {
                var template = await _database.GetTemplateWithIncludes(templateId);

                var authResult = await _authorizationService.AuthorizeAsync(User, template, TemplateOperations.Update);
                if (authResult.Succeeded)
                {
                    await _database.DeleteTemplateFile(templateId, languageName);
                    return Ok();
                }

                return Forbid();
            }
            catch (ArgumentException aex)
            {
                Log.Error(aex.ToString());
                return BadRequest(aex.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }


        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] TemplateDetailsModel model)
        {
            try
            {
                var template = await _database.GetTemplateWithIncludes(id);

                var authResult = await _authorizationService.AuthorizeAsync(User, template, TemplateOperations.Update);
                if (authResult.Succeeded)
                {
                    await _database.UpdateTemplate(template, model);
                    return Ok();
                }

                return Forbid();
            }
            catch (ArgumentException aex)
            {
                Log.Error(aex.ToString());
                return BadRequest(aex.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            try
            {
                var template = await _database.GetTemplateWithIncludes(id);

                var authResult = await _authorizationService.AuthorizeAsync(User, template, TemplateOperations.Delete);
                if (authResult.Succeeded)
                {
                    await _database.DeleteTemplate(id);
                    return Ok();
                }

                return Forbid();
            }
            catch (ArgumentException aex)
            {
                Log.Error(aex.ToString());
                return BadRequest(aex.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }

        [HttpPut("{templateId}/Publications")]
        public async Task<IActionResult> UpdatePublications(Guid templateId, [FromBody] Guid[] orgaIds)
        {
            try
            {
                var template = await _database.GetTemplateWithIncludes(templateId);

                var authResult = await _authorizationService.AuthorizeAsync(User, template, TemplateOperations.Update);
                if (!authResult.Succeeded || !await IsPublishingAllowed(template, orgaIds))
                {
                    return Forbid();
                }

                await _database.UpdatePublications(template.Id, orgaIds);
                return Ok();
            }
            catch (ArgumentException aex)
            {
                Log.Error(aex.ToString());
                return BadRequest(aex.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }

        [HttpPut("{templateId}/owners")]
        public async Task<IActionResult> UpdateOwners(Guid templateId, [FromBody] OwnerDataModel[] owners)
        {
            try
            {
                var template = await _database.GetTemplateWithIncludes(templateId);

                var authResult = await _authorizationService.AuthorizeAsync(User, template, TemplateOperations.Update);
                if (!authResult.Succeeded)
                {
                    return Forbid();
                }

                if (owners.Count(o => o.IsMain) != 1)
                {
                    throw new ArgumentException("Exactly one main owner is required");
                }

                if (owners.Length > 10)
                {
                    throw new ArgumentException("Maximum number of owners exceeded");
                }

                await _database.SetOwners(template.Id, owners);
                return Ok();
            }
            catch (ArgumentException aex)
            {
                Log.Error(aex.ToString());
                return BadRequest(aex.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }

        private async Task<bool> IsPublishingAllowed(Template template, Guid[] orgaEntitiyIds)
        {
            if (!template.TemplateFiles.Any() && orgaEntitiyIds.Length > 0)
            {
                //Publishing a template that has no template files attached is not allowed.
                return false;
            }
            
            var orgaEntities = await _orgaEntitiesDatabase.GetOrgaEntitiesIncludingDeleted(orgaEntitiyIds);

            if (orgaEntities.Any(e => e.Deleted))
            {
                return false;
            }

            var rootOrgaEntity = orgaEntities.FirstOrDefault(e => e.LogicalLevel == 0);

            if (rootOrgaEntity != null)
            {
                // publishing to root level is restricted
                bool userIsAdmin = User.HasClaim(AccessClaim.Type, AccessClaim.Admin);
                bool templateAlreadyPublishedToRootLevel = template.Publications.Any(p => p.OrgaEntityId == rootOrgaEntity.Id);
                return userIsAdmin || templateAlreadyPublishedToRootLevel;
            }

            return true;
        }

        private async Task<Dictionary<OrgaEntity, IEnumerable<Template>>> GetTemplatesDictionaryForHierarchy(PaginationInfo paginationInfo)
        {
            var user = await _userService.GetEurolookUser(Request);
            var entities = await _userService.GetEntitiesForUser(user);

            var result = new Dictionary<OrgaEntity, IEnumerable<Template>>();

            foreach (var entity in entities)
            {
                var searchQuery = new TemplateSearchQuery();
                searchQuery.OrgaEntityIds.Add(entity.Id);
                var templates = await _database.FindTemplates(searchQuery, paginationInfo);
                result.Add(entity, templates);
            }

            return result;
        }

        private async Task<IEnumerable<Template>> GetTemplatesForHierarchy(
            TemplateSearchQuery searchQuery,
            PaginationInfo paginationInfo)
        {
            var user = await _userService.GetEurolookUser(Request);
            var entities = await _userService.GetEntitiesForUser(user);

            searchQuery.OrgaEntityIds.AddRange(entities.Select(e => e.Id));
         
            return await _database.FindTemplates(searchQuery, paginationInfo);
        }

        private PaginationInfo GetAlphabeticallyOrderedPaginationInfo(uint pageSize)
        {
            return new PaginationInfo
            {
                OrderProperty = nameof(Template.Name),
                OrderAscending = true,
                PageSize = pageSize
            };
        }
    }
}
