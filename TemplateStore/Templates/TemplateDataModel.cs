﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Data;
using Eurolook.Data.Models.TemplateStore;
using JetBrains.Annotations;
using TemplateStore.Users;

namespace TemplateStore.Templates
{

    public class TemplateDataModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid? BaseDocumentModelId { get; set; }
        public string BaseDocumentModelName { get; set; }
        public string Description { get; set; }
        public string Tags { get; set; }
        public bool IsPublic { get; set; }
        public string ThumbnailBase64 { get; set; }
        public string[] Languages { get; set; }
        public OwnerDataModel[] Owners { get; set; }
        public OrgaEntityDataModel[] Publications { get; set; }
        public DateTime LastUpdate { get; set; }
        public bool HasSigner { get; set; }
        public bool IsSignerFixed { get; set; }
        public bool IsFeatured { get; set; }

        [UsedImplicitly]
        public TemplateDataModel()
        {
        }

        public TemplateDataModel(Template model)
        {
            Id = model.Id;
            Name = model.Name;
            Description = model.Description;
            BaseDocumentModelId = model.BaseDocumentModelId;
            Tags = model.Tags;
            IsPublic = model.IsPublic;
            IsSignerFixed = model.IsSignerFixed;
            IsFeatured = model.FeaturedUntil > DateTime.UtcNow;
            LastUpdate = model.ServerModificationTimeUtc;

            ThumbnailBase64 = ToBase64String(model.Thumbnail);

            BaseDocumentModelName = model.BaseDocumentModel?.DisplayName;
            Languages = model.TemplateFiles?.Where(x => x.Language != null)
                             .Select(x => x.Language)
                             .OrderBy(x => x, new EnFrDeAlphabeticalLanguagesComparer())
                             .Select(x => x.Name)
                             .ToArray();

            Owners = model.Owners?.Select(o => new OwnerDataModel(o)).OrderBy(o => !o.IsMain).ThenBy(o => o.DisplayName).ToArray();
            Publications = model.Publications?.Select(x => new OrgaEntityDataModel(x.OrgaEntity)).OrderBy(o => o.LogicalLevel != 0).ThenBy(o => o.Name).ToArray();

            if (Owners != null && Owners.Length > 0 && !Owners.Any(o => o.IsMain))
            {
                Owners[0].IsMain = true;
            }
        }

        private string ToBase64String(byte[] byteArray)
        {
            return byteArray != null ? Convert.ToBase64String(byteArray) : null;
        }
    }
}
