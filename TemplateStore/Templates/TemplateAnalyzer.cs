using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;
using Eurolook.Common;
using Eurolook.Data.Xml;
using Eurolook.DocumentProcessing;
using Eurolook.DocumentProcessing.Extensions;
using Microsoft.AspNetCore.Http;

namespace TemplateStore.Templates
{
    public interface ITemplateAnalyzer
    {
        Task<TemplateAnalyzerResult> Process(IFormFile file);
    }

    public class TemplateAnalyzerResult
    {
        public string Language { get; set; }

        public Guid? BaseDocumentId { get; set; }

        public bool IsEurolookDocument { get; set; }

        public bool HasAuthorRoles { get; set; }
    }

    public class TemplateAnalyzer : ITemplateAnalyzer
    {
        private readonly IAuthorManager _authorManager;

        public TemplateAnalyzer(IAuthorManager authorManager)
        {
            _authorManager = authorManager;
        }

        public async Task<TemplateAnalyzerResult> Process(IFormFile file)
        {
            try
            {
                using var temporaryFile = new TemporaryFile(".docx");
                await using (var fileStream = new FileStream(temporaryFile.FullName, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }

                using var wordDocument = WordprocessingDocument.Open(temporaryFile.FullName, false);
                var eurolookPropertiesPart = wordDocument.MainDocumentPart.CustomXmlParts.FirstOrDefault(
                    p => EurolookPropertiesCustomXml.RootName == p.GetXDocument()?.Root?.Name);

                var result = new TemplateAnalyzerResult();
                if (eurolookPropertiesPart != null)
                {
                    result.IsEurolookDocument = true;
                    var eurolookProperties = new EurolookPropertiesCustomXml(eurolookPropertiesPart.GetXDocument());
                    result.Language = eurolookProperties.CreationLanguage;
                    result.BaseDocumentId = eurolookProperties.DocumentModelId;
                }

                var authorCustomXmlDocs = _authorManager.GetAuthorCustomXmlDocsFromPackage(wordDocument);
                result.HasAuthorRoles =_authorManager.HasAuthorRoles(authorCustomXmlDocs);

                return result;
            }
            catch (InvalidDataException)
            {
                throw new ArgumentException("Given file is no .docx file.");
            }
            catch (OpenXmlPackageException)
            {
                throw new ArgumentException("Given file is no .docx file.");
            }
        }
    }
}
