using System;

namespace TemplateStore.Templates
{
    public class BaseDocumentFilter
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
