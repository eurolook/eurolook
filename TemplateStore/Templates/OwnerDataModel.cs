using Eurolook.Data.Models.TemplateStore;
using JetBrains.Annotations;
using TemplateStore.Users;

namespace TemplateStore.Templates
{
    public class OwnerDataModel : UserDataModel
    {
        public bool IsMain { get; set; }

        [UsedImplicitly]
        public OwnerDataModel()
        {
            // for deserialization
        }

        public OwnerDataModel(TemplateOwner owner)
        :base(owner.User)
        {
            IsMain = owner.IsMain;
        }
    }
}
