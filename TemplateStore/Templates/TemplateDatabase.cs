﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.Constants;
using Eurolook.Data.Extensions;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using TemplateStore.DisplayPreference;

namespace TemplateStore.Templates
{
    public class TemplateDatabase : ITemplateDatabase
    {
        public Func<EurolookContext> GetContext { get; }

        public TemplateDatabase(Func<EurolookContext> eurolookContextFunc)
        {
            GetContext = eurolookContextFunc;
        }

        public async Task<Template[]> FindTemplates(TemplateSearchQuery searchQuery, PaginationInfo paginationInfo)
        {
            var predicate = searchQuery.GetPredicate();
            await using var context = GetContext();
            return await GetTemplates(context, predicate, paginationInfo);
        }

        public async Task<Template[]> GetMyTemplates(User user, TemplateSearchQuery searchQuery, PaginationInfo paginationInfo)
        {
            if (user == null)
            {
                return Array.Empty<Template>();
            }

            await using var context = GetContext();

            var templateIds = await context.TemplateOwners
                                           .Where(x => x.UserId == user.Id)
                                           .Select(x => x.TemplateId)
                                           .ToArrayAsync();

            var predicate = searchQuery.GetPredicate();
            predicate.And(x => templateIds.Contains(x.Id));

            return await GetTemplates(context, predicate, paginationInfo);
        }

        private async Task<Template[]> GetTemplates(EurolookContext context, ExpressionStarter<Template> predicate, PaginationInfo paginationInfo)
        {
            var templatesQuery = context.Templates
                                        .Include(t => t.BaseDocumentModel)
                                        .Include(t => t.TemplateFiles).ThenInclude(tf => tf.Language)
                                        .Include(t => t.Publications).ThenInclude(p => p.OrgaEntity)
                                        .Include(x => x.Owners).ThenInclude(y => y.User).ThenInclude(z => z.Self).ThenInclude(a => a.OrgaEntity)
                                        .Where(predicate);

            Template[] result;
            if (paginationInfo.OrderProperty == OrderByPropertyEnum.LastModified.ToString())
            {
                result = await templatesQuery.OrderBy(t => t.ServerModificationTimeUtc, paginationInfo.OrderAscending)
                                             .Paginate(paginationInfo)
                                             .ToArrayAsync();
            }
            else if (paginationInfo.OrderProperty == OrderByPropertyEnum.EurolookTemplate.ToString())
            {
                result = await templatesQuery.OrderBy(t => t.BaseDocumentModel.DisplayName, paginationInfo.OrderAscending)
                                             .Paginate(paginationInfo)
                                             .ToArrayAsync();
            }
            else
            {
                result = await templatesQuery.OrderBy(t => t.Name, paginationInfo.OrderAscending)
                                             .Paginate(paginationInfo)
                                             .ToArrayAsync();
            }

            return result;
        }

        public async Task<Template[]> GetPopularTemplates(IEnumerable<Guid> orgaEntityIds, PaginationInfo paginationInfo)
        {
            await using var context = GetContext();
            var popularTemplateIdsCount =
                await context.UserTemplates
                             .Include(x => x.Template)
                             .ThenInclude(t => t.Publications)
                             .Where(x => x.Template.Publications.Any(p => orgaEntityIds.Contains(p.OrgaEntityId)))
                             .GroupBy(x => x.Template.Id)
                             .Select(
                                 grp => new
                                 {
                                     grp.Key,
                                     Count = grp.Count()
                                 })
                             .OrderByDescending(x => x.Count)
                             .Paginate(paginationInfo)
                             .ToDictionaryAsync(grp => grp.Key, grp => grp.Count);

            var popularTemplateIds = popularTemplateIdsCount.Select(x => x.Key).ToArray();
            var predicate = PredicateBuilder.New<Template>(x => popularTemplateIds.Contains(x.Id));
            var popularTemplates = await GetTemplates(context, predicate, paginationInfo);
            return popularTemplates.OrderByDescending(t => popularTemplateIdsCount[t.Id])
                                   .ThenByDescending(t => t.ServerModificationTimeUtc)
                                   .ToArray();
        }

        public async Task<Template[]> GetRecentlyUsedTemplates(User user, PaginationInfo paginationInfo)
        {
            await using var context = GetContext();
            return await context.UserTemplates
                                .Where(ut => ut.UserId == user.Id)
                                .Include(ut => ut.Template).ThenInclude(t => t.BaseDocumentModel)
                                .Include(ut => ut.Template).ThenInclude(t => t.TemplateFiles).ThenInclude(y => y.Language)
                                .OrderByAndPaginate(paginationInfo)
                                .Select(ut => ut.Template)
                                .ToArrayAsync();
        }


        public async Task<Template> GetTemplate(Guid templateId)
        {
            await using var context = GetContext();
            var template = await context.Templates
                                        .Include(x => x.Owners) // needed for authorization
                                        .Where(x => x.Id == templateId)
                                        .FirstOrDefaultAsync();
            if (template == null)
            {
                throw new ArgumentException($"Template with ID '{templateId}' does not exist.", nameof(templateId));
            }

            return template;
        }

        public async Task<Template> GetTemplateWithIncludes(Guid templateId)
        {
            await using var context = GetContext();
            return await context.Templates
                                .Include(x => x.BaseDocumentModel)
                                .Include(x => x.TemplateFiles).ThenInclude(y => y.Language)
                                .Include(x => x.Publications)
                                .Include(x => x.Owners)
                                .Where(x => x.Id == templateId)
                                .FirstOrDefaultAsync()
                                ?? throw new ArgumentException($"Template with ID '{templateId}' does not exist.", nameof(templateId));
        }

        public async Task<Template> GetTemplateDetails(Guid templateId)
        {
            await using var context = GetContext();
            return await context.Templates
                                .Include(x => x.BaseDocumentModel)
                                .Include(x => x.Owners).ThenInclude(y => y.User).ThenInclude(z => z.Self).ThenInclude(a => a.OrgaEntity)
                                .Include(x => x.Publications).ThenInclude(y => y.OrgaEntity)
                                .Include(x => x.TemplateFiles).ThenInclude(y => y.Language)
                                .Where(x => x.Id == templateId)
                                .FirstOrDefaultAsync()
                                ?? throw new ArgumentException($"Template with ID '{templateId}' does not exist.", nameof(templateId));
        }

        public async Task<int> GetUserProfileCount(Guid templateId)
        {
            await using var context = GetContext();
            return await context.UserTemplates.Where(ut => ut.TemplateId == templateId).CountAsync();
        }

        public async Task<Template> CreateTemplate(string name, string description, string tags, User user)
        {
            if (user == null)
            {
                throw new ArgumentException("Invalid User");
            }

            await using var context = GetContext();

            var template = new Template
            {
                Name = name,
                Description = description,
                Tags = tags
            };
            template.Init();
            await context.Templates.AddAsync(template);

            var owner = new TemplateOwner
            {
                TemplateId = template.Id,
                UserId = user.Id,
                IsMain = true,
            };
            owner.Init();
            await context.TemplateOwners.AddAsync(owner);

            await context.SaveChangesAsync();
            return template;
        }

        public async Task<Template> UpdateTemplate(Template template, TemplateDetailsModel model)
        {
            await using var context = GetContext();

            if (template == null)
            {
                throw new ArgumentException("Template does not exist.");
            }

            context.Attach(template);
            template.Name = model.Name;
            template.Description = model.Description;
            template.Tags = model.Tags;
            template.SetModification(ModificationType.ServerModification);
            template.IsSignerFixed = model.IsSignerFixed;
            template.FeaturedUntil = model.IsFeatured ? DateTime.MaxValue : (DateTime?)null;
            await context.SaveChangesAsync();

            return template;
        }

        public async Task<TemplateFile> GetTemplateFile(Guid templateId, string languageName)
        {
            await using var context = GetContext();
            var language = await context.Languages.FirstOrDefaultAsync(x => x.Name == languageName);
            if (language == null)
            {
                throw new ArgumentException($"Language '{languageName}' does not exist.", nameof(languageName));
            }

            var template = await context.TemplateFiles
                                        .Where(x => x.TemplateId == templateId && x.LanguageId == language.Id)
                                        .Include(x => x.Template).ThenInclude(t => t.Owners)
                                        .Include(x => x.Language)
                                        .FirstOrDefaultAsync();
            if (template == null)
            {
                throw new ArgumentException($"Template with ID '{templateId}' does not exist.", nameof(templateId));
            }

            return template;
        }

        public async Task AddOrUpdateTemplateFile(Guid templateId, Guid baseDocumentModelId, string languageName, byte[] templateBytes)
        {
            await using var context = GetContext();
            var template = await context.Templates.FirstOrDefaultAsync(x => x.Id == templateId);
            await AddOrUpdateTemplateFile(context, template, languageName, baseDocumentModelId, templateBytes);
            template.SetModification(ModificationType.ServerModification);
            await context.SaveChangesAsync();
        }
            
        private static async Task AddOrUpdateTemplateFile(
            EurolookContext context,
            Template template,
            string languageName,
            Guid baseDocumentModelId,
            byte[] templateBytes
            )
        {
            var language = await context.Languages.FirstAsync(x => x.Name == languageName);
            if (await context.TemplateFiles.IgnoreQueryFilters().AnyAsync(x => x.TemplateId == template.Id) == false)
            {
                // It's the first upload to the Template, set BaseDocumentModelId
                template.BaseDocumentModelId = baseDocumentModelId;
            }
            else if (template.BaseDocumentModelId != baseDocumentModelId)
            {
                throw new ArgumentException(
                    $"Cannot update template file for {template.Id}. BaseDocumentModelId doesn't match.");
            }

            // create or update the template file
            var templateFile = await context.TemplateFiles
                                            .IgnoreQueryFilters()
                                            .FirstOrDefaultAsync(
                                                x => x.LanguageId == language.Id && x.TemplateId == template.Id);
            if (templateFile != null)
            {
                templateFile.SetDeletedFlag(false);
            }
            else
            {
                templateFile = new TemplateFile
                {
                    LanguageId = language.Id,
                    TemplateId = template.Id
                };
                templateFile.Init();
                await context.TemplateFiles.AddAsync(templateFile);
            }

            templateFile.Bytes = templateBytes;
        }

        public async Task DeleteTemplateFile(Guid templateId, string languageName)
        {
            await using var context = GetContext();

            var language = await context.Languages.FirstAsync(x => x.Name == languageName);

            if (!await context.TemplateFiles.AnyAsync(tf => tf.TemplateId == templateId && tf.LanguageId != language.Id))
            {
                throw new Exception($"Cannot delete last template file from template with id {templateId}");
            }

            var templateFile = await context
                                     .TemplateFiles
                                     .FirstOrDefaultAsync(x => x.TemplateId == templateId && x.LanguageId == language.Id);
            if (templateFile != null)
            {
                templateFile.SetDeletedFlag();
                var template = await context.Templates.FirstOrDefaultAsync(x => x.Id == templateId);
                template.SetModification(ModificationType.ServerModification);
                await context.SaveChangesAsync();
            }
        }

        public async Task DeleteTemplate(Guid templateId)
        {
            await using var context = GetContext();
            var template = await context.Templates.FirstOrDefaultAsync(x => x.Id == templateId);
            if (template == null)
            {
                throw new ArgumentException($"Template with ID '{templateId}' does not exist.", nameof(templateId));
            }

            foreach (var templateOwner in await context.TemplateOwners.Where(x => x.TemplateId == templateId).ToArrayAsync())
            {
                templateOwner.SetDeletedFlag();
            }

            foreach (var templatePublication in await context.TemplatePublications.Where(x => x.TemplateId == templateId).ToArrayAsync())
            {
                templatePublication.SetDeletedFlag();
            }

            foreach (var templateFile in await context.TemplateFiles.Where(x => x.TemplateId == templateId).ToArrayAsync())
            {
                templateFile.SetDeletedFlag();
            }

            foreach (var userTemplate in await context.UserTemplates.Where(x => x.TemplateId == templateId).ToArrayAsync())
            {
                userTemplate.SetDeletedFlag();
            }

            template.SetDeletedFlag();
            await context.SaveChangesAsync();
        }

        public async Task UpdatePublications(Guid templateId, Guid[] orgaEntityIds)
        {
            await using var context = GetContext();

            var publications = await context.TemplatePublications
                .IgnoreQueryFilters()
                .Where(x => x.TemplateId == templateId)
                .ToListAsync();

            var publicationsToDelete = publications.Where(p => !p.Deleted && !orgaEntityIds.Contains(p.OrgaEntityId));
            var publicationsToRevive = publications.Where(p => p.Deleted && orgaEntityIds.Contains(p.OrgaEntityId));
            var orgaEntityIdsToAdd = orgaEntityIds.Where(o => !publications.Any(p => o == p.OrgaEntityId));

            foreach (var publication in publicationsToDelete)
            {
                publication.SetDeletedFlag(true);
            }

            foreach (var publication in publicationsToRevive)
            {
                publication.SetDeletedFlag(false);
            }

            foreach (var orgaEntityId in orgaEntityIdsToAdd)
            {
                var publication = new TemplatePublication
                {
                    TemplateId = templateId,
                    OrgaEntityId = orgaEntityId
                };
                publication.Init();
                await context.TemplatePublications.AddAsync(publication);
            }

            var template = await context.Templates.FirstAsync(x => x.Id == templateId);
            template.IsPublic = orgaEntityIds.Any();
            template.SetModification(ModificationType.ServerModification);

            await context.SaveChangesAsync();
        }

        public async Task SetPreviewInitiated(Guid templateId)
        {
            await using var context = GetContext();
            var template = await context.Templates.FirstAsync(x => x.Id == templateId);
            template.PreviewInitiated = DateTime.UtcNow;
            template.SetModification(ModificationType.ServerModification);
            await context.SaveChangesAsync();
        }

        public async Task UpdateLargePreviewImages(Guid templateId, byte[] image1, byte[] image2, string language)
        {
            await using var context = GetContext();
            var template = await context.Templates.FirstAsync(x => x.Id == templateId);
            template.PreviewPage1 = image1;
            template.PreviewPage2 = image2;
            template.PreviewLanguage = language;
            template.PreviewModifiedTime = DateTime.UtcNow;
            template.SetModification(ModificationType.ServerModification);
            await context.SaveChangesAsync();
        }

        public async Task UpdateSmallPreviewImages(Guid templateId, byte[] image1, byte[] image2, string language)
        {
            await using var context = GetContext();
            var template = await context.Templates.FirstAsync(x => x.Id == templateId);
            template.SmallPreviewPage1 = image1;
            template.SmallPreviewPage2 = image2;
            template.PreviewLanguage = language;
            template.SmallPreviewModifiedTime = DateTime.UtcNow;
            template.SetModification(ModificationType.ServerModification);
            await context.SaveChangesAsync();
        }

        public async Task UpdateThumbnail(Guid templateId, byte[] thumbnail)
        {
            await using var context = GetContext();
            var template = await context.Templates.FirstAsync(x => x.Id == templateId);
            template.Thumbnail = thumbnail;
            template.SetModification(ModificationType.ServerModification);
            await context.SaveChangesAsync();
        }

        public async Task DeleteThumbnail(Guid templateId)
        {
            await using var context = GetContext();
            var template = await context.Templates.FirstAsync(x => x.Id == templateId);
            template.Thumbnail = null;
            template.SetModification(ModificationType.ServerModification);
            await context.SaveChangesAsync();
        }

        public async Task<Language[]> GetAllLanguages()
        {
            await using var context = GetContext();
            return await context.Languages.ToArrayAsync();
        }

        public async Task<DocumentModel[]> GetAllBaseDocuments()
        {
            await using var context = GetContext();
            return await context.DocumentModels.Where(dm => dm.IsHidden != true).ToArrayAsync();
        }

        public async Task<bool> DocumentModelExists(Guid documentModelId)
        {
            await using var context = GetContext();
            return context.DocumentModels.Any(dm => dm.Id == documentModelId);
        }

        public async Task<bool> DocumentModelHasSignerRole(Guid? documentModelId)
        {
            await using var context = GetContext();

            if (documentModelId == null)
            {
                return false;
            }

            var authorRolesIds = context.DocumentModelAuthorRoles
                                        .Where(x => x.DocumentModelId == documentModelId)
                                        .Select(x => x.AuthorRoleId);
            var authorRoles = context.AuthorRoles.Where(ar => authorRolesIds.Contains(ar.Id));

            return authorRoles.Any(ar => ar.Id == CommonDataConstants.SignatoryAuthorRoleId);
        }

        public async Task SetOwners(Guid templateId, OwnerDataModel[] templateOwners)
        {
            await using var context = GetContext();
            var owners = await context.TemplateOwners
                .IgnoreQueryFilters()
                .Where(x => x.TemplateId == templateId)
                .ToArrayAsync();

            foreach (var owner in owners)
            {
                owner.IsMain = false;
                owner.SetDeletedFlag();
            }

            foreach (var owner in templateOwners)
            {
                var existingOwner = owners.FirstOrDefault(o => o.UserId == owner.Id);

                if (existingOwner != null)
                {
                    existingOwner.SetDeletedFlag(false);
                    existingOwner.IsMain = owner.IsMain;
                }
                else
                {
                    var newOwner = new TemplateOwner
                    {
                        TemplateId = templateId,
                        UserId = owner.Id,
                        IsMain = owner.IsMain
                    };
                    newOwner.Init();
                    await context.TemplateOwners.AddAsync(newOwner);
                }
            }

            await context.SaveChangesAsync();
        }
    }
}
