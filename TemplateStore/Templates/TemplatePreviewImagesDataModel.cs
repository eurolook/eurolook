﻿using System;
using Eurolook.Data.Models.TemplateStore;
using JetBrains.Annotations;

namespace TemplateStore.Templates
{
    public class TemplatePreviewImagesDataModel
    {
        public string PreviewPage1Base64  { get; set; }
        public string PreviewPage2Base64  { get; set; }

        [UsedImplicitly]
        public TemplatePreviewImagesDataModel()
        {
        }

        public TemplatePreviewImagesDataModel(Template model, bool small)
        {

            PreviewPage1Base64 = ToBase64String(small ? model.SmallPreviewPage1 : model.PreviewPage1);
            PreviewPage2Base64 = ToBase64String(small ? model.SmallPreviewPage2 : model.PreviewPage2);
        }

        //TODO move to another class and reuse in TemplateDataModel
        private string ToBase64String(byte[] byteArray)
        {
            return byteArray != null ? Convert.ToBase64String(byteArray) : null;
        }
    }
}
