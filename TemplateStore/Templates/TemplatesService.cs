﻿using Eurolook.Data.Models.TemplateStore;

namespace TemplateStore.Templates
{
    // FIXME: Remove once clients have been installed
    public class TemplateService : ITemplateService
    {
        public void PrepareForDownload(UserTemplate userTemplate)
        {
            var template = userTemplate.Template;

            template.BaseDocumentModel = null;
            template.Owners = null;
            template.PreviewPage1 = null;
            template.PreviewPage2 = null;
            template.Thumbnail = null;

            if (template.Publications != null)
            {
                foreach (var publication in template.Publications)
                {
                    publication.Template = null;
                    publication.OrgaEntity = null;
                }
            }

            if (template.TemplateFiles != null)
            {
                foreach (var file in template.TemplateFiles)
                {
                    file.Template = null;
                    file.Language = null;
                    file.Bytes = null;
                }
            }
        }
    }
}
