﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;
using JetBrains.Annotations;
using TemplateStore.Users;

namespace TemplateStore.Templates
{
    public class TemplateDetailsModel : TemplateDataModel
    {
        public bool IsEditable { get; set; }
        public int UserCount { get; set; }

        [UsedImplicitly]
        public TemplateDetailsModel()
        {
        }

        public TemplateDetailsModel(Template model, User user, int userProfileCount, bool hasSigner)
        : base(model)
        {
            IsEditable = IsEditableBy(model, user.Login);
            UserCount = userProfileCount;
            HasSigner = hasSigner;
        }

        private bool IsEditableBy(Template model, string login)
        {
            if (model?.Owners == null)
            {
                return false;
            }

            return model.Owners.Any(x => string.Equals(x.User.Login, login, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}   
