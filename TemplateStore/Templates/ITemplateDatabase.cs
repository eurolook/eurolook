﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;

namespace TemplateStore.Templates
{
    public interface ITemplateDatabase
    {
        public Task<Template[]> FindTemplates(TemplateSearchQuery searchQuery, PaginationInfo paginationInfo);
        public Task<Template[]> GetMyTemplates(User user, TemplateSearchQuery searchQuery, PaginationInfo paginationInfo);

        Task<Template[]> GetPopularTemplates(IEnumerable<Guid> orgaEntityIds, PaginationInfo paginationInfo);

        Task<Template[]> GetRecentlyUsedTemplates(User user, PaginationInfo paginationInfo);

        public Task<Template> GetTemplateWithIncludes(Guid templateId);

        public Task<Template> GetTemplate(Guid templateId);

        public Task<Template> GetTemplateDetails(Guid templateId);

        public Task<int> GetUserProfileCount(Guid templateId);

        public Task<Template> CreateTemplate(string name, string description, string tags, User user);
        public Task<Template> UpdateTemplate(Template template, TemplateDetailsModel model);
        public Task DeleteTemplate(Guid templateId);

        public Task<TemplateFile> GetTemplateFile(Guid templateId, string languageName);
        public Task AddOrUpdateTemplateFile(Guid templateId, Guid baseDocumentModelId, string languageName, byte[] templateBytes);
        public Task DeleteTemplateFile(Guid templateId, string languageName);
        public Task UpdateLargePreviewImages(Guid templateId, byte[] image1, byte[] image2, string language);

        public Task UpdateSmallPreviewImages(Guid templateId, byte[] image1, byte[] image2, string language);

        public Task UpdateThumbnail(Guid templateId, byte[] thumbnail);

        public Task DeleteThumbnail(Guid templateId);

        public Task UpdatePublications(Guid templateId, Guid[] orgaEntityIds);

        public Task<Language[]> GetAllLanguages();
        public Task<DocumentModel[]> GetAllBaseDocuments();

        Task<bool> DocumentModelExists(Guid documentModelId);

        Task<bool> DocumentModelHasSignerRole(Guid? documentModelId);
        Task SetPreviewInitiated(Guid templateId);
        Task SetOwners(Guid templateId, OwnerDataModel[] templateOwners);
    }
}
