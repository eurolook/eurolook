﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.Models;

namespace TemplateStore.Users
{
    public class UserInfoDataModel : UserDataModel
    {
        public bool IsImpersonated { get; }

        public UserDataModel AuthenticatedUser { get; }

        public OrgaEntityDataModel[] OrgaEntities { get; }

        public Guid[] UserTemplates { get; }

        public UserInfoDataModel(User user, User authenticatedUser, List<OrgaEntity> entities, bool authenticatedUserIsAdmin)
            : base(user)
        {
            OrgaEntities = entities.Select(e => new OrgaEntityDataModel(e)).ToArray();
            IsImpersonated = user.Id != authenticatedUser.Id;
            UserTemplates = user.Templates.Select(t => t.TemplateId).ToArray();

            AuthenticatedUser = new UserDataModel(authenticatedUser, authenticatedUserIsAdmin);

            if (!IsImpersonated && authenticatedUserIsAdmin)
            {
                IsAdmin = true;
            }
        }
    }
}
