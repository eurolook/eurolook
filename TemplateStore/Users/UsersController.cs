﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Data.Models.TemplateStore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.IISIntegration;
using Serilog;
using TemplateStore.Auth;
using TemplateStore.Templates;

namespace TemplateStore.Users
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IEurolookUserService _userService;
        private readonly IUserRoleService _userRoleService;
        private readonly IAuthorizationService _authorizationService;
        private readonly ITemplateDatabase _templateDatabase;
        private readonly ITemplateService _templateService;

        public UsersController(
            IEurolookUserService userService,
            IUserRoleService userRoleService,
            IAuthorizationService authorizationService,
            ITemplateDatabase templateDatabase,
            ITemplateService templateService)
        {
            _userService = userService;
            _userRoleService = userRoleService;
            _authorizationService = authorizationService;
            _templateDatabase = templateDatabase;
            _templateService = templateService;
        }

        [HttpGet("my")]
        public async Task<UserInfoDataModel> My()
        {
            var authenticatedUser = await _userService.GetEurolookUser(Request, false);
            var user = await _userService.GetEurolookUser(Request);
            if (user == null)
            {
                return null;
            }

            bool authenticatedUserIsAdmin = _userRoleService.IsAdmin(User);

            var entities = await _userService.GetEntitiesForUser(user);
            return new UserInfoDataModel(user, authenticatedUser, entities, authenticatedUserIsAdmin);
        }

        [HttpGet("search")]
        public async Task<IEnumerable<UserDataModel>> SearchUsers(string q, uint page = 0, uint pageSize = 20)
        {
            var users = await _userService.GetUsers(q, page, pageSize);
            return users.Select(u => new UserDataModel(u)).OrderBy(o => o.DisplayName);
        }

        // FIXME: Remove authorize attributes once client has been updated ( -> only use default scheme)
        [Authorize(IISDefaults.AuthenticationScheme)]
        [Authorize]
        [HttpPut("templates/{templateId}")]
        public async Task<ActionResult> SaveAsUserTemplate(Guid templateId)
        {
            var user = await _userService.GetEurolookUser(Request);
            if (user == null)
            {
                return Forbid();
            }

            var template = await _templateDatabase.GetTemplateWithIncludes(templateId);

            var authResult = await _authorizationService.AuthorizeAsync(User, template, TemplateOperations.Read);
            if (!authResult.Succeeded)
            {
                return Forbid();
            }

            try
            {
                await _userService.SaveAsUserTemplateAsync(templateId, user.Id);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return BadRequest();
            }

            return Ok();
        }

        // FIXME: Remove method when client has been updated (moved to data sync service)
        [Authorize(IISDefaults.AuthenticationScheme)]
        [HttpGet("templates")]
        public async Task<ActionResult<UserTemplate[]>> GetUserTemplates(uint modifiedSinceHours)
        {
            var user = await _userService.GetEurolookUser(Request);
            if (user == null)
            {
                return Forbid();
            }

            try
            {
                var userTemplates = await _userService.GetUserTemplateAsync(user.Id);
                if (modifiedSinceHours > 0)
                {
                    var now = DateTime.UtcNow;
                    userTemplates = userTemplates.Where(x => (now - x.ServerModificationTimeUtc).TotalHours < modifiedSinceHours).ToArray();
                }

                foreach (var userTemplate in userTemplates)
                {
                    _templateService.PrepareForDownload(userTemplate);
                }

                return userTemplates;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return BadRequest();
            }
        }
    }
}
