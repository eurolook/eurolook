﻿using System;
using Eurolook.Data.Constants;
using Eurolook.Data.Models;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace TemplateStore.Users
{   
    public class UserDataModel
    {
        public Guid Id { get; set; }

        public string Login { get; }

        public string DisplayName { get; }

        public string Email { get; }

        [BindNever]
        public bool IsAdmin { get; protected set; }

        public string OrgaName { get; }

        public UserDataModel()
        {
        }

        public UserDataModel(User user)
        {
            Id = user.Id;
            Login = user.Login;
            DisplayName = GetDisplayName(user);
            Email = user.Self.Email;
            IsAdmin = user.IsMemberOfUserGroup(CommonDataConstants.AdministratorsUserGroupId);
            OrgaName = user.Self.OrgaEntity?.Name;
        }

        public UserDataModel(User user, bool isAdmin)
            : this(user)
        {
            IsAdmin = isAdmin;
        }

        private string GetDisplayName(User user)
        {
            var author = user?.Self;
            return author?.FullNameWithLastNameFirst ?? user?.Login;
        }
    }
}
