﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Serilog;
using TemplateStore.Auth;
using TemplateStore.ExtensionMethods;
using TemplateStore.Templates;

namespace TemplateStore.Users
{
    public class EurolookUserService : IEurolookUserService
    {
        private readonly IUserRepository _usersRepository;
        private readonly ITemplateDatabase _templateDatabase;
        private readonly ISharedTemplateRepository _sharedTemplateRepository;
        private readonly IAuthorizationService _authorizationService;
        private readonly IUserRoleService _userRoleService;

        public EurolookUserService(
            IUserRepository usersRepository,
            ITemplateDatabase templateDatabase,
            ISharedTemplateRepository sharedTemplateRepository,
            IAuthorizationService authorizationService,
            IUserRoleService userRoleService)
        {
            _usersRepository = usersRepository;
            _templateDatabase = templateDatabase;
            _sharedTemplateRepository = sharedTemplateRepository;
            _authorizationService = authorizationService;
            _userRoleService = userRoleService;
        }

        public async Task<User> GetEurolookUser(HttpRequest request, bool useImpersonatedUser = true)
        {
            string login;
            if (useImpersonatedUser)
            {
                login = GetLoginOfImpersonatedIdentity(request) ?? request.GetLoginOfAuthorizedIdentity();
            }
            else
            {
                login = request.GetLoginOfAuthorizedIdentity();
            }

            return await _usersRepository.GetUser(login);
        }

        public async Task<List<OrgaEntity>> GetEntitiesForUser(User user)
        {
            return await _usersRepository.GetEntitiesForAuthor(user.Self);
        }

        public async Task<List<User>> GetUsers(string q, uint page = 0, uint pageSize = 20)
        {
            var paginationInfo = new PaginationInfo
            {
                From = page,
                PageSize = pageSize
            };

            return await _usersRepository.GetUsers(q, paginationInfo);
        }

        public async Task SaveUserDisplayPreferences(HttpRequest request, string displayPreferences)
        {
            string login = request.GetLoginOfAuthorizedIdentity();
            await _usersRepository.SaveUserDisplayPreferences(login, displayPreferences);
        }

        public async Task SaveAsUserTemplateAsync(Guid templateId, Guid userId)
        {                      
            await _sharedTemplateRepository.SaveAsUserTemplateAsync(templateId, userId);
        }

        public async Task<UserTemplate[]> GetUserTemplateAsync(Guid userId)
        {
            return await _sharedTemplateRepository.GetUserTemplatesAsync(userId);
        }

        private string GetLoginOfImpersonatedIdentity(HttpRequest request)
        {
            if (request.IsImpersonated() && _userRoleService.IsAdmin(request.HttpContext.User))
            {
                Log.Debug("Impersonation");
                var stringValues = request.Query["impersonate"];
                return stringValues.ToArray()[0];
            }

            return null;
        }
    }
}
