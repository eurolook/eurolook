﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;
using Microsoft.AspNetCore.Http;

namespace TemplateStore.Users
{
    public interface IEurolookUserService
    {
        Task<User> GetEurolookUser(HttpRequest request, bool useImpersonatedUser = true);

        Task<List<OrgaEntity>> GetEntitiesForUser(User user);

        Task<List<User>> GetUsers(string q, uint page = 0, uint pageSize = 20);

        Task SaveUserDisplayPreferences(HttpRequest request, string viewSettings);

        Task SaveAsUserTemplateAsync(Guid templateId, Guid userid);

        Task<UserTemplate[]> GetUserTemplateAsync(Guid userId);
    }
}
