﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.Models;

namespace TemplateStore.Users
{
    public interface IUserRepository
    {
        public Task<User> GetUser(string login);

        public Task<User> GetUserByEmail(string email);

        Task<UserGroup> GetUserGroupByName(string name);

        public Task<List<OrgaEntity>> GetEntitiesForAuthor(Author author);

        Task<List<User>> GetUsers(string q, PaginationInfo paginationInfo);

        Task SaveUserDisplayPreferences(string login, string displayPreferences);
    }
}
