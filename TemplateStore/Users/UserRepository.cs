﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Data;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace TemplateStore.Users
{
    public class UserRepository: AuthorSearchRepository, IUserRepository
    {
        public UserRepository(Func<EurolookContext> eurolookContextFunc)
            :base(eurolookContextFunc)
        { }

        public async Task<User> GetUser(string login)
        {
            await using var context = GetContext();
            return await context.Users
                                .AsNoTracking()
                                .Where(x => x.Login == login)
                                .Include(x => x.Self).ThenInclude(u => u.JobAssignments)
                                .Include(x => x.Settings)
                                .Include(x => x.Templates)
                                .FirstOrDefaultAsync();
        }

        public async Task<User> GetUserByEmail(string email)
        {
            await using var context = GetContext();
            var authorId = await context.Authors
                                .AsNoTracking()
                                .Where(a => a.Email == email)
                                .Select(a => a.Id)
                                .FirstOrDefaultAsync();

            return await context.Users
                                .AsNoTracking()
                                .Where(x => x.SelfId == authorId)
                                .FirstOrDefaultAsync();
        }

        public async Task<UserGroup> GetUserGroupByName(string name)
        {
            using var context = GetContext();
            return await context.UserGroups
                                .AsNoTracking()
                                .FirstOrDefaultAsync(ug => ug.Name == name);
        }

        public async Task<List<OrgaEntity>> GetEntitiesForAuthor(Author author)
        {
            var result = new List<OrgaEntity>();

            result.AddRange(await GetHierarchyForOrgaEntity(author.OrgaEntityId));

            foreach (var jobAssignment in author.JobAssignments)
            {
                var orgaEntities = await GetHierarchyForOrgaEntity(jobAssignment.OrgaEntityId);
                result.AddRange(orgaEntities.Where(o => !result.Select(x => x.Name).Contains(o.Name)));
            }

            return result;
        }

        public async Task<List<User>> GetUsers(string q, PaginationInfo paginationInfo)
        {
            if (string.IsNullOrWhiteSpace(q))
            {
                return new List<User>();
            }

            var queries = q.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            var authors = SearchAuthors(queries, paginationInfo, true);

            var users = new List<User>();
            await using var context = GetContext();
            foreach (var author in authors)
            {
                var user = await context.Users.FirstOrDefaultAsync(u => u.SelfId == author.Id);
                if (user == null)
                {
                    continue;
                }

                user.Self = author;
                users.Add(user);
            }

            return users;
        }

        public async Task SaveUserDisplayPreferences(string login, string displayPreferences)
        {
            await using var context = GetContext();
            var user = await context.Users
                                .Where(x => x.Login == login)
                                .Include(x => x.Settings)
                                .FirstAsync();

            user.Settings.TemplateStoreViewSettings = displayPreferences;
            user.Settings.SetModification(ModificationType.ServerModification);
            await context.SaveChangesAsync();
        }

        private async Task<IEnumerable<OrgaEntity>> GetHierarchyForOrgaEntity(Guid? orgaEntityId)
        {
            var result = new List<OrgaEntity>();
            if (!orgaEntityId.HasValue)
            {
                return result;
            }

            await using var context = GetContext();

            var entity = await context.OrgaEntities.FirstOrDefaultAsync(e => e.Id == orgaEntityId);
            while (entity != null)
            {
                result.RemoveAll(o => o.Name == entity.Name);
                result.Insert(0, entity);
                if (entity.LogicalLevel == 0)
                {
                    break;
                }

                entity = await context.OrgaEntities.FirstOrDefaultAsync(e => e.Id == entity.SuperEntityId);
            }

            return result;
        }
    }
}
