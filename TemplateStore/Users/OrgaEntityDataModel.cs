using System;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace TemplateStore.Users
{
    public class OrgaEntityDataModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public int LogicalLevel { get; set; }

        [UsedImplicitly]
        public OrgaEntityDataModel()
        {
            // for deserialization
        }

        public OrgaEntityDataModel(OrgaEntity orgaEntity)
        {
            Id = orgaEntity.Id;
            Name = orgaEntity.LogicalLevel == 0 ? "Commission" : orgaEntity.Name;
            LogicalLevel = orgaEntity.LogicalLevel;
        }
    }
}
