﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Eurolook.Web.Common;

namespace TemplateStore.Metadata
{
    [Route("api/[controller]")]
    [ApiController]
    public class MetadataController : ControllerBase
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfiguration _configuration;

        public MetadataController(IWebHostEnvironment env, IConfiguration configuration)
        {
            _env = env;
            _configuration = configuration;
        }
        
        [HttpGet("[action]")]
        public ActionResult ApplicationInfo()
        {
            return new JsonResult(new ApplicationInfo(_env.EnvironmentName));
        }

        [HttpGet("[action]")]
        [ResponseCache(Duration = CacheSettings.OneWeekDuration, Location = ResponseCacheLocation.Any, NoStore = false)]
        public ActionResult HelpBaseUrl()
        {
            return new JsonResult(_configuration.GetValue<string>("OnlineHelpUrl"));
        }
    }
}
