﻿using JetBrains.Annotations;

namespace Eurolook.ActiveDirectoryLink
{
    public interface IActiveDirectoryConfigurationProvider
    {
        string LdapServer { get; }

        [CanBeNull]
        string[] LdapSearchBases { get; }

        string LdapAuthenticationType { get; }

        int ActiveDirectoryPageSize { get; }

        string ActiveDirectorySearchFilter { get; }

        string RetiredUsersSearchFilter { get; }

        string AgencyNames { get; }

        string DefaultDomain { get; }

        string ServerDatabaseConnection { get; }
    }
}
