﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;
using System.Linq;
using Eurolook.ActiveDirectoryLink.Extensions;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using JetBrains.Annotations;

namespace Eurolook.ActiveDirectoryLink
{
    [UsedImplicitly]
    public class ActiveDirectorySearcher : IActiveDirectorySearcher, ICanLog
    {
        private readonly IActiveDirectoryMapping _activeDirectoryMapping;
        private readonly IActiveDirectoryConfigurationProvider _activeDirectoryConfigurationProvider;
        private readonly Dictionary<string, string> _domainNameCache;

        public ActiveDirectorySearcher(
            IActiveDirectoryMapping activeDirectoryMapping,
            IActiveDirectoryConfigurationProvider activeDirectoryConfigurationProvider)
        {
            _activeDirectoryMapping = activeDirectoryMapping;
            _activeDirectoryConfigurationProvider = activeDirectoryConfigurationProvider;
            SearchResults = new Dictionary<string, ActiveDirectoryUser>(StringComparer.InvariantCultureIgnoreCase);
            _domainNameCache = new Dictionary<string, string>();
            VerboseLogging = true;
        }

        [NotNull]
        public Dictionary<string, ActiveDirectoryUser> SearchResults { get; }

        public bool VerboseLogging { get; set; }

        public IEnumerable<string> RetrieveUsers(IEnumerable<string> userNames)
        {
            var sw = Stopwatch.StartNew();

            SearchResults.Clear();
            _domainNameCache.Clear();

            foreach (string userName in userNames)
            {
                var activeDirectoryUser = RetrieveUser(userName, null);
                if (activeDirectoryUser != null)
                {
                    SearchResults[activeDirectoryUser.FullLoginName] = activeDirectoryUser;
                }
            }

            this.LogTrace($"Retrieved {SearchResults.Count} user entries in {sw.ElapsedMilliseconds}ms.");
            return GetUserNames();
        }

        public IEnumerable<string> RetrieveSingleUser(string userName, string email = null)
        {
            var sw = Stopwatch.StartNew();

            SearchResults.Clear();
            _domainNameCache.Clear();

            var activeDirectoryUser = RetrieveUser(userName, email);
            if (activeDirectoryUser != null)
            {
                SearchResults[activeDirectoryUser.FullLoginName] = activeDirectoryUser;
                if (VerboseLogging)
                {
                    this.LogTrace($"Retrieved single user entry in {sw.ElapsedMilliseconds}ms.");
                }
            }
            else
            {
                if (VerboseLogging)
                {
                    this.LogDebug($"Did not find user {userName} in directory.");
                }
            }

            return GetUserNames();
        }

        public IEnumerable<string> RetrieveAllUsers()
        {
            try
            {
                string searchFilter =
                    string.IsNullOrWhiteSpace(_activeDirectoryConfigurationProvider.ActiveDirectorySearchFilter)
                        ? "(objectClass=user)"
                        : _activeDirectoryConfigurationProvider.ActiveDirectorySearchFilter;

                this.LogInfo($"Retrieving all users from Active Directory using filter {searchFilter} ...");

                var sw = Stopwatch.StartNew();
                SearchResults.Clear();
                _domainNameCache.Clear();

                var searchResults = !string.IsNullOrEmpty(_activeDirectoryConfigurationProvider.LdapServer)
                    ? SearchUsersInLdap(_activeDirectoryConfigurationProvider.LdapServer, searchFilter)
                    : SearchUsersInGlobalCatalog(searchFilter);
                foreach (var activeDirectoryUser in searchResults)
                {
                    if (string.IsNullOrWhiteSpace(activeDirectoryUser?.Login))
                    {
                        this.LogWarn("Skipped a result because it doesn't have a login. Probably it's no user object.");
                        continue;
                    }

                    SearchResults[activeDirectoryUser.FullLoginName] = activeDirectoryUser;
                }

                this.LogTrace($"Retrieved {SearchResults.Count} user entries in {sw.ElapsedMilliseconds}ms.");
                return GetUserNames();
            }
            catch (Exception ex)
            {
                this.LogError("Retrieving users from Active Directory failed.", ex);
                return Enumerable.Empty<string>();
            }
        }

        public IEnumerable<string> RetrieveRetiredUsers()
        {
            if (string.IsNullOrWhiteSpace(_activeDirectoryConfigurationProvider.RetiredUsersSearchFilter))
            {
                throw new ArgumentException(
                    $"{nameof(_activeDirectoryConfigurationProvider.RetiredUsersSearchFilter)} cannot be empty.");
            }

            try
            {
                this.LogInfo(
                    $"Retrieving retired users from Active Directory using filter {_activeDirectoryConfigurationProvider.RetiredUsersSearchFilter} ...");

                var sw = Stopwatch.StartNew();
                SearchResults.Clear();
                _domainNameCache.Clear();

                var searchResults = !string.IsNullOrEmpty(_activeDirectoryConfigurationProvider.LdapServer)
                    ? SearchUsersInLdap(
                        _activeDirectoryConfigurationProvider.LdapServer,
                        _activeDirectoryConfigurationProvider.RetiredUsersSearchFilter)
                    : SearchUsersInGlobalCatalog(_activeDirectoryConfigurationProvider.RetiredUsersSearchFilter);
                foreach (var activeDirectoryUser in searchResults)
                {
                    if (string.IsNullOrWhiteSpace(activeDirectoryUser?.Login))
                    {
                        this.LogWarn("Skipped a result because it doesn't have a login. Probably it's no user object.");
                        continue;
                    }

                    SearchResults[activeDirectoryUser.FullLoginName] = activeDirectoryUser;
                }

                this.LogTrace($"Retrieved {SearchResults.Count} user entries in {sw.ElapsedMilliseconds}ms.");
                return GetUserNames();
            }
            catch (Exception ex)
            {
                this.LogError("Retrieving retired users from Active Directory failed.", ex);
                return Enumerable.Empty<string>();
            }
        }

        public ActiveDirectoryUser GetFirstResult()
        {
            return SearchResults.Values.FirstOrDefault();
        }

        public ActiveDirectoryUser GetUser(string userName)
        {
            if (!SearchResults.ContainsKey(userName))
            {
                RetrieveSingleUser(userName);
            }

            if (SearchResults.ContainsKey(userName))
            {
                return SearchResults[userName];
            }

            return null;
        }

        private static string ToSamAccountName(string userName)
        {
            if (userName.Contains("\\"))
            {
                return userName.Substring(userName.LastIndexOf('\\') + 1).ToLowerInvariant();
            }

            return userName.ToLowerInvariant();
        }

        private ActiveDirectoryUser RetrieveUser(string userName, string email)
        {
            try
            {
                string filter = "(objectClass=*)";
                if (userName != null)
                {
                    string login = ToSamAccountName(userName);
                    filter = $"(sAMAccountName={login})";
                    if (email != null)
                    {
                        filter = $"(&{filter}(mail={email}))";
                    }
                }
                else if (email != null)
                {
                    filter = $"(mail={email})";
                }

                if (VerboseLogging)
                {
                    this.LogInfo($"Retrieving user {userName} from Active Directory using filter {filter} ...");
                }

                var users = !string.IsNullOrWhiteSpace(_activeDirectoryConfigurationProvider.LdapServer)
                    ? SearchUsersInLdap(_activeDirectoryConfigurationProvider.LdapServer, filter)
                    : SearchUsersInGlobalCatalog(filter);
                return users.Any() ? users.First() : null;
            }
            catch (Exception ex)
            {
                this.LogError($"Retrieving user {userName} from Active Directory failed.\n{ex}");
                return null;
            }
        }

        private void ConfigureDirectorySearcher(DirectorySearcher directorySearcher)
        {
            directorySearcher.PageSize = _activeDirectoryConfigurationProvider.ActiveDirectoryPageSize;
            foreach (string propertyToLoad in _activeDirectoryMapping.PropertiesToLoad())
            {
                directorySearcher.PropertiesToLoad.Add(propertyToLoad);
            }
        }

        private List<ActiveDirectoryUser> ParseSearchResults(IEnumerable<SearchResult> searchResults)
        {
            var result = new List<ActiveDirectoryUser>();
            foreach (var s in searchResults)
            {
                var entry = s.GetDirectoryEntry();
                var userEntry = _activeDirectoryMapping.CreateActiveDirectoryUser(entry);
                var dcPath = GetDcPath(entry);
                var domain = GetDomainName(dcPath);
                if (domain == null && !string.IsNullOrEmpty(_activeDirectoryConfigurationProvider.DefaultDomain))
                {
                    userEntry.Domain = _activeDirectoryConfigurationProvider.DefaultDomain;
                    if (VerboseLogging)
                    {
                        this.LogWarn($"Cannot find domain name of '{entry.Path}'. Assuming '{userEntry.Domain}'.");
                    }
                }
                else
                {
                    userEntry.Domain = domain;
                }

                result.Add(userEntry);
            }

            return result;
        }

        private string GetDcPath(DirectoryEntry entry)
        {
            return entry.Path.Substring(entry.Path.IndexOf("dc=", StringComparison.InvariantCultureIgnoreCase));
        }

        private string GetDomainName(string dcPath)
        {
            if (!_domainNameCache.ContainsKey(dcPath))
            {
                var rootDse = new DirectoryEntry(@"LDAP://RootDSE");
                var partitions = new DirectoryEntry(
                    @"LDAP://cn=Partitions," + rootDse.Properties["configurationNamingContext"].Value);
                var directorySearcher = new DirectorySearcher(partitions)
                {
                    Filter = $"(&(objectCategory=crossRef)(ncName={dcPath}))",
                    SearchScope = SearchScope.Subtree,
                };

                const string netBiosProperty = "nETBIOSName";
                directorySearcher.PropertiesToLoad.Add(netBiosProperty);
                var results = directorySearcher.FindAll();
                if (results.Count > 0)
                {
                    _domainNameCache[dcPath] = results[0].Properties[netBiosProperty][0].ToString();
                }
            }

            if (_domainNameCache.ContainsKey(dcPath))
            {
                return _domainNameCache[dcPath];
            }

            return null;
        }

        private List<ActiveDirectoryUser> SearchUsersInGlobalCatalog(string filter)
        {
            var result = new List<ActiveDirectoryUser>();
            using (var currentForest = Forest.GetCurrentForest())
            {
                using (var globalCatalog = currentForest.FindGlobalCatalog())
                {
                    if (VerboseLogging)
                    {
                        this.LogTrace($"Searching in '{globalCatalog.Name}' using filter '{filter}'");
                    }

                    using (var searcher = globalCatalog.GetDirectorySearcher())
                    {
                        ConfigureDirectorySearcher(searcher);
                        searcher.Filter = filter;
                        result.AddRange(ParseSearchResults(searcher.SafeFindAll()).ToList());
                    }
                }
            }

            return result;
        }

        private List<ActiveDirectoryUser> SearchUsersInLdap(string ldapServer, string filter)
        {
            var result = new List<ActiveDirectoryUser>();

            if (_activeDirectoryConfigurationProvider.LdapSearchBases.IsNullOrEmpty())
            {
                result.AddRange(SearchLdap(ldapServer, filter));
            }
            else
            {
                // ReSharper disable once PossibleNullReferenceException
                foreach (var searchBase in _activeDirectoryConfigurationProvider.LdapSearchBases)
                {
                    result.AddRange(SearchLdap($"{ldapServer}/{searchBase}", filter));
                }
            }

            return result;
        }

        private IEnumerable<ActiveDirectoryUser> SearchLdap(string ldapPath, string filter)
        {
            var directory = new DirectoryEntry(ldapPath);
            if (VerboseLogging)
            {
                this.LogTrace($"Searching in '{directory.Path}' using filter '{filter}'");
            }

            if (_activeDirectoryConfigurationProvider.LdapAuthenticationType == nameof(AuthenticationTypes.Anonymous))
            {
#pragma warning disable S4433 // LDAP connections should be authenticated
                directory.AuthenticationType = AuthenticationTypes.Anonymous;
#pragma warning restore S4433 // LDAP connections should be authenticated
            }

            using var searcher = new DirectorySearcher(directory);
            ConfigureDirectorySearcher(searcher);
            searcher.Filter = filter;
            return ParseSearchResults(searcher.SafeFindAll());
        }

        private IEnumerable<string> GetUserNames()
        {
            return SearchResults.Select(entry => entry.Value.FullLoginName);
        }
    }
}
