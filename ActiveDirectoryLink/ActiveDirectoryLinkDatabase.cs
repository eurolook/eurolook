﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Eurolook.Common;
using Eurolook.Common.Log;
using Eurolook.Data;
using Eurolook.Data.Constants;
using Eurolook.Data.Models;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace Eurolook.ActiveDirectoryLink
{
    [UsedImplicitly]
    public class ActiveDirectoryLinkDatabase : EurolookServerDatabase, IEurolookAuthorRepository, IActiveDirectoryLinkDatabase
    {
        public ActiveDirectoryLinkDatabase(IActiveDirectoryConfigurationProvider activeDirectoryConfigurationProvider)
            : base(activeDirectoryConfigurationProvider.ServerDatabaseConnection)
        {
        }

        public new Author GetAuthorByUserName(string userName, bool includeDeleted = true)
        {
            using (var context = GetContext())
            {
                var query = context.Users.Where(u => u.Login == userName);
                if (!includeDeleted)
                {
                    query = query.Where(u => !u.Deleted);
                }

                var user = query.FirstOrDefault();
                if (user != null)
                {
                    return context.Authors.Include(a => a.Workplaces).FirstOrDefault(a => a.Id == user.SelfId);
                }
            }

            return null;
        }

        public List<string> GetAllUsers()
        {
            var sw = Stopwatch.StartNew();
            this.LogInfo("Retrieving user names from EL10 database ...");
            List<string> result;
            using (var context = GetContext())
            {
                result = context.Users.Select(u => u.Login).ToList();
            }

            this.LogTrace($"Retrieved {result.Count} user names in {sw.ElapsedMilliseconds}ms.");
            return result;
        }

        public List<string> GetInactiveUsers()
        {
            var sw = Stopwatch.StartNew();
            this.LogInfo("Retrieving inactive users from EL10 database ...");
            List<string> result;
            using (var context = GetContext())
            {
                result = context.Users
                                .Include(u => u.Settings.DeviceSettings)
                                .Where(
                                    u => u.Settings.DeviceSettings.All(
                                        x => x.LastLogin == null || x.LastLogin.Value < DateTime.UtcNow.AddYears(-1)))
                                .Select(u => u.Login).ToList();
            }

            this.LogTrace($"Retrieved {result.Count} inactive user names in {sw.ElapsedMilliseconds}ms.");
            return result;
        }

        public Workplace GetWorkplace(Author author, string activeDirectoryCity)
        {
            using (var context = GetContext())
            {
                return context.Workplaces.Include(wp => wp.Address)
                              .FirstOrDefault(
                                  wp => (wp.AuthorId == author.Id)
                                        && (wp.Address.ActiveDirectoryReference == activeDirectoryCity));
            }
        }

        public Workplace GetPrimaryWorkplace(Author author)
        {
            using (var context = GetContext())
            {
                return context.Workplaces.Include(wp => wp.Address)
                              .FirstOrDefault(wp => wp.Id == author.MainWorkplaceId);
            }
        }

        public Workplace UpdateWorkplace(Workplace workplace, ModificationType modificationType)
        {
            using (var context = GetContext())
            {
                var result = Update(context.Workplaces, workplace, modificationType);
                context.SaveChanges();
                return result;
            }
        }

        public bool UpdateAdGroupsOfUser(string login, string adGroups)
        {
            using var context = GetContext();
            var user = context.Users.FirstOrDefault(u => u.Login == login);
            if (user.AdGroups == adGroups)
            {
                return false;
            }

            user.AdGroups = adGroups;
            context.SaveChanges();
            return true;
        }

        public IEnumerable<OrgaEntity> GetOrgaEntitiesByName(string name)
        {
            using (var context = GetContext())
            {
                return context.OrgaEntities.Where(x => !x.Deleted && (x.Name == name)).ToList();
            }
        }

        public OrgaEntity[] GetAllOrgaEntities()
        {
            using (var context = GetContext())
            {
                return context.OrgaEntities.Where(x => !x.Deleted).ToArray();
            }
        }

        public PredefinedFunction GetPredefinedFunction(string adReference)
        {
            if (string.IsNullOrWhiteSpace(adReference))
            {
                return null;
            }

            using (var context = GetContext())
            {
                return context.PredefinedFunctions.FirstOrDefault(x => !x.Deleted && (x.AdReference == adReference));
            }
        }

        public PredefinedFunction CreateOrRevivePredefinedFunction(
            string adReference,
            Dictionary<string, string> functionsByLanguageName)
        {
            using (var context = GetContext())
            {
                var function = context.PredefinedFunctions.Include(pdf => pdf.FunctionText)
                                      .ThenInclude(x => x.Translations)
                                      .FirstOrDefault(x => x.AdReference == adReference);

                if (function != null)
                {
                    if (function.Deleted)
                    {
                        function.SetDeletedFlag(false);
                        function.FunctionText.SetDeletedFlag(false);
                        foreach (var translation in function.FunctionText.Translations)
                        {
                            translation.SetDeletedFlag(false);
                        }

                        context.SaveChanges();
                    }

                    return function;
                }

                function = new PredefinedFunction
                {
                    Name = adReference,
                    AdReference = adReference,
                    ShowInSignature = true,
                    FunctionText = new Text
                    {
                        Alias = $"Function_{Regex.Replace(adReference, @"\s+", "-")}_{new ShortGuid(Guid.NewGuid())}",
                        Translations = new HashSet<Translation>(),
                    },
                };

                function.FunctionText.Init();
                function.Init();

                foreach (var kvp in functionsByLanguageName)
                {
                    var language = context.Languages.AsNoTracking().FirstOrDefault(l => l.Name == kvp.Key);
                    if ((language == null) || string.IsNullOrWhiteSpace(kvp.Value))
                    {
                        continue;
                    }

                    var translation = new Translation
                    {
                        Value = kvp.Value,
                        Text = function.FunctionText,
                        LanguageId = language.Id,
                    };
                    translation.Init();
                    function.FunctionText.Translations.Add(translation);
                }

                var result = context.PredefinedFunctions.Add(function);
                context.SaveChanges();
                return result.Entity;
            }
        }

        public Address GetAddress(string reference)
        {
            using (var context = GetContext())
            {
                return context.Addresses.FirstOrDefault(x => x.ActiveDirectoryReference == reference && !x.Deleted);
            }
        }

        public void ResetWorkplaceAddress(Workplace workplace, Address notAddress)
        {
            using (var context = GetContext())
            {
                var dbWorkplace = context.Workplaces.FirstOrDefault(x => x.Id == workplace.Id);
                if (dbWorkplace != null)
                {
                    var resetAddress = context.Addresses.FirstOrDefault(
                                           x => x.Id == CommonDataConstants.NoAddressId && x.Id != dbWorkplace.AddressId)
                                       ?? context.Addresses.FirstOrDefault(x => x.Id != dbWorkplace.AddressId);
                    if (resetAddress != null)
                    {
                        dbWorkplace.AddressId = resetAddress.Id;
                        dbWorkplace.PhoneExtension = null;
                        dbWorkplace.Office = null;
                        dbWorkplace.FaxExtension = null;
                        dbWorkplace.SetModification(ModificationType.ServerModification);
                        context.SaveChanges();
                    }
                }
            }
        }
    }
}
