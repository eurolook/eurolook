using System.Collections.Generic;
using System.DirectoryServices;

namespace Eurolook.ActiveDirectoryLink
{
    public interface IActiveDirectoryMapping
    {
        IEnumerable<string> PropertiesToLoad();

        ActiveDirectoryUser CreateActiveDirectoryUser(DirectoryEntry entry);
    }
}
