﻿using System;
using Eurolook.Data.Models;

namespace Eurolook.ActiveDirectoryLink
{
    public class ActiveDirectoryUser : IAuthor
    {
        public string Domain { get; set; }

        public string Login { get; set; }

        public string LatinFirstName { get; set; }

        public string LatinLastName { get; set; }

        public string Initials { get; set; }

        public string Gender { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string PhoneExtension { get; set; }

        public string Fax { get; set; }

        public string FaxExtension { get; set; }

        public string Office { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string Company { get; set; }

        public string Title { get; set; }

        public string Department { get; set; }

        public string WebAddress { get; set; }

        public string FullLoginName => $"{Domain}\\{Login}";

        public DateTime? Deleted { get; set; }

        public string PersonalTitle { get; set; }

        public string FunctionEn { get; set; }

        public string FunctionFr { get; set; }

        public string OrgaEntityNameEn { get; set; }

        public string OrgaEntityNameFr { get; set; }

        public string Description { get; set; }

        public string MemberOf { get; set; }
    }
}
