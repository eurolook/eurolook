using System.Collections.Generic;
using System.DirectoryServices;

namespace Eurolook.ActiveDirectoryLink.Extensions
{
    public static class DirectorySearcherExtensions
    {
        /// <summary>
        /// A "safe" version of the <see cref="DirectorySearcher.FindAll" /> method that prevents
        /// memory leaks by making sure the collection returned by <see cref="DirectorySearcher.FindAll" />
        /// is properly disposed.
        /// </summary>
        /// <param name="searcher">The <see cref="DirectorySearcher" /> used to query Active Directory.</param>
        /// <returns>An <see cref="IEnumerable{T}" /> enumerating the found search results.</returns>
        public static IEnumerable<SearchResult> SafeFindAll(this DirectorySearcher searcher)
        {
            using (var results = searcher.FindAll())
            {
                foreach (SearchResult result in results)
                {
                    yield return result;
                }
            }
        }
    }
}
