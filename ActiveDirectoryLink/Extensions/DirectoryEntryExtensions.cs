using System.DirectoryServices;
using JetBrains.Annotations;

namespace Eurolook.ActiveDirectoryLink.Extensions
{
    public static class DirectoryEntryExtensions
    {
        [CanBeNull]
        public static string GetFirstPropertyValue(this DirectoryEntry entry, string propertyName)
        {
            var propertyValueCollection = entry.Properties[propertyName];
            return propertyValueCollection.Count > 0 ? propertyValueCollection[0].ToString() : null;
        }
    }
}
