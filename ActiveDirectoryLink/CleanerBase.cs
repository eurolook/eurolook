using System;
using Eurolook.Common.Log;

namespace Eurolook.ActiveDirectoryLink
{
    public abstract class CleanerBase : ICanLog, ICleaner
    {
        protected CleanerBase(
            IActiveDirectorySearcher activeDirectorySearcher,
            IEurolookAuthorRepository eurolookAuthorRepository)
        {
            ActiveDirectorySearcher = activeDirectorySearcher;
            EurolookAuthorRepository = eurolookAuthorRepository;
        }

        protected IActiveDirectorySearcher ActiveDirectorySearcher { get; }

        protected IEurolookAuthorRepository EurolookAuthorRepository { get; }

        public bool CleanUser(string userName, bool useCache = false)
        {
            try
            {
                if (!useCache)
                {
                    ActiveDirectorySearcher.SearchResults.Clear();
                }

                var activeDirectoryUser = ActiveDirectorySearcher.GetUser(userName);
                if (activeDirectoryUser == null)
                {
                    this.LogWarn($"User {userName} not found in Active Directory.");
                    return false;
                }

                return CleanUser(activeDirectoryUser);
            }
            catch (Exception ex)
            {
                this.LogErrorFormat("Failed cleaning user {0}. {1}", userName, ex);
                return false;
            }
        }

        public abstract bool DeleteUserPermanently(string userName);

        public abstract bool CleanUser(ActiveDirectoryUser activeDirectoryUser);
    }
}
