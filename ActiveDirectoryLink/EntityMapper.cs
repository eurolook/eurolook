﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using Eurolook.Common;
using Eurolook.Common.Log;
using Eurolook.Data.Models;

namespace Eurolook.ActiveDirectoryLink
{
    public class EntityMapper<TActiveDirectoryUser, TEntity> : ICanLog
        where TEntity : Synchronizable
    {
        private readonly TActiveDirectoryUser _activeDirectoryUser;
        private readonly TEntity _entity;
        private readonly Author _author;

        public EntityMapper(TActiveDirectoryUser activeDirectoryUser, TEntity entity, Author author)
        {
            _activeDirectoryUser = activeDirectoryUser;
            _entity = entity;
            _author = author;
        }

        public bool IsUpdated { get; set; }

        public void InitFromActiveDirectory(
            Expression<Func<TActiveDirectoryUser, string>> propertyExpression,
            Expression<Func<TEntity, string>> entityPropertyExpression = null)
        {
            var linkedProperty = new MappedProperty(propertyExpression, entityPropertyExpression);

            string activeDirectoryValue = linkedProperty.GetActiveDirectoryValue(_activeDirectoryUser);
            if (string.IsNullOrWhiteSpace(activeDirectoryValue))
            {
                // not mapped
                return;
            }

            linkedProperty.UpdateLinkedProperty(_activeDirectoryUser, _entity, _author);
            IsUpdated = true;
        }

        public void UpdateFromActiveDirectory(
            Expression<Func<TActiveDirectoryUser, string>> propertyExpression,
            Expression<Func<TEntity, string>> entityPropertyExpression = null)
        {
            // TODO check again
            var linkedProperty = new MappedProperty(propertyExpression, entityPropertyExpression);

            string activeDirectoryValue = linkedProperty.GetActiveDirectoryValue(_activeDirectoryUser);
            if (string.IsNullOrWhiteSpace(activeDirectoryValue))
            {
                // not mapped
                return;
            }

            if (linkedProperty.IsUnchangedInActiveDirectory(_activeDirectoryUser, _entity, _author))
            {
                // no change
                return;
            }

            // field has changed, update silently
            linkedProperty.UpdateLinkedProperty(_activeDirectoryUser, _entity, _author);
            IsUpdated = true;
        }

        public class MappedProperty : ICanLog
        {
            private readonly Func<TActiveDirectoryUser, string> _activeDirectoryPropertyGet;
            private readonly Func<TEntity, string> _entityPropertyGet;
            private readonly Action<TEntity, string> _entityPropertySet;

            private readonly Func<Author, string> _authorPropertyAdGet;
            private readonly Action<Author, string> _authorPropertyAdSet;

            public MappedProperty(
                Expression<Func<TActiveDirectoryUser, string>> propertyExpression,
                Expression<Func<TEntity, string>> entityPropertyExpression = null)
            {
                if (propertyExpression == null)
                {
                    throw new ArgumentNullException(nameof(propertyExpression));
                }

                PropertyNameActiveDirectory = GetPropertyName(propertyExpression);
                PropertyNameEntity = entityPropertyExpression != null
                    ? GetPropertyName(entityPropertyExpression)
                    : PropertyNameActiveDirectory;
                string propertyNameEntityAd = "Ad" + PropertyNameEntity;
                string propertyNameEntityAdPending = "Ad" + PropertyNameEntity + "Pending";

                _activeDirectoryPropertyGet =
                    GetPropertyGetter<TActiveDirectoryUser, string>(PropertyNameActiveDirectory);

                _entityPropertyGet = GetPropertyGetter<TEntity, string>(PropertyNameEntity);
                _entityPropertySet = GetPropertySetter<TEntity, string>(PropertyNameEntity);

                _authorPropertyAdGet = GetPropertyGetter<Author, string>(propertyNameEntityAd);
                _authorPropertyAdSet = GetPropertySetter<Author, string>(propertyNameEntityAd);
            }

            public string PropertyNameEntity { get; }

            public string PropertyNameActiveDirectory { get; }

            public bool IsLinkedToActiveDirectory(TEntity entity, Author author)
            {
                string entityValue = GetEntityValue(entity);
                string entityAdValue = GetEntityAdValue(author ?? entity as Author);
                return TrimmedStringComparer.InvariantCultureIgnoreCase.Equals(entityValue, entityAdValue);
            }

            public bool IsUnchangedInActiveDirectory(
                TActiveDirectoryUser activeDirectoryUser,
                TEntity entity,
                Author author)
            {
                string activeDirectoryValue = GetActiveDirectoryValue(activeDirectoryUser);
                string entityAdValue = GetEntityAdValue(author ?? entity as Author);
                return TrimmedStringComparer.InvariantCultureIgnoreCase.Equals(
                    activeDirectoryValue,
                    entityAdValue);
            }

            public string GetActiveDirectoryValue(TActiveDirectoryUser activeDirectoryUser)
            {
                return _activeDirectoryPropertyGet(activeDirectoryUser);
            }

            public void UpdateLinkedProperty(TActiveDirectoryUser activeDirectoryUser, TEntity entity, Author author)
            {
                string activeDirectoryValue = GetActiveDirectoryValue(activeDirectoryUser);
                SetEntityValue(entity, activeDirectoryValue);
                SetEntityAdValue(author ?? entity as Author, activeDirectoryValue);

                this.LogDebug($"Updated {PropertyNameActiveDirectory} => {PropertyNameEntity}");
            }

            private static Func<TObject, TProperty> GetPropertyGetter<TObject, TProperty>(string propertyName)
            {
                var paramExpression = Expression.Parameter(typeof(TObject), "value");
                var propertyGetterExpression = Expression.Property(paramExpression, propertyName);

                var result = Expression.Lambda<Func<TObject, TProperty>>(propertyGetterExpression, paramExpression)
                                       .Compile();
                return result;
            }

            private static Action<TObject, TProperty> GetPropertySetter<TObject, TProperty>(string propertyName)
            {
                var paramExpression = Expression.Parameter(typeof(TObject));
                var paramExpression2 = Expression.Parameter(typeof(TProperty), propertyName);
                var propertyGetterExpression = Expression.Property(paramExpression, propertyName);

                var result = Expression.Lambda<Action<TObject, TProperty>>(
                    Expression.Assign(propertyGetterExpression, paramExpression2),
                    paramExpression,
                    paramExpression2).Compile();

                return result;
            }

            private static string GetPropertyName<TObject, TProperty>(
                Expression<Func<TObject, TProperty>> propertyExpression)
            {
                var body = propertyExpression.Body as MemberExpression;
                if (body == null)
                {
                    throw new ArgumentException("Expression is not a MemberExpression", nameof(propertyExpression));
                }

                var member = body.Member as PropertyInfo;
                if (member == null)
                {
                    throw new ArgumentException("Argument is not a property", nameof(propertyExpression));
                }

                return member.Name;
            }

            private string GetEntityValue(TEntity entity)
            {
                return _entityPropertyGet(entity);
            }

            private void SetEntityValue(TEntity entity, string value)
            {
                _entityPropertySet(entity, value);
            }

            private string GetEntityAdValue(Author author)
            {
                return _authorPropertyAdGet(author);
            }

            private void SetEntityAdValue(Author author, string value)
            {
                _authorPropertyAdSet(author, value);
            }
        }
    }
}
