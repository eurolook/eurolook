using System.Collections.Generic;

namespace Eurolook.ActiveDirectoryLink
{
    public interface IActiveDirectorySearcher
    {
        Dictionary<string, ActiveDirectoryUser> SearchResults { get; }

        bool VerboseLogging { get; set; }

        ActiveDirectoryUser GetUser(string userName);

        IEnumerable<string> RetrieveSingleUser(string userName, string email = null);

        IEnumerable<string> RetrieveUsers(IEnumerable<string> userNames);

        IEnumerable<string> RetrieveAllUsers();

        IEnumerable<string> RetrieveRetiredUsers();

        ActiveDirectoryUser GetFirstResult();
    }
}
