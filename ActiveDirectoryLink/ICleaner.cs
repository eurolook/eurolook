namespace Eurolook.ActiveDirectoryLink
{
    public interface ICleaner
    {
        bool CleanUser(string userName, bool useCache = false);

        bool DeleteUserPermanently(string userName);
    }
}
