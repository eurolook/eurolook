using Eurolook.Data.Database;

namespace Eurolook.ActiveDirectoryLink.TemplateOwnership
{
    public class TemplateOwnershipConfiguration : ISystemConfigurationSetting
    {
        public string Key => "TemplateOwnership";

        public string FallbackUser { get; set; }

        public string FromAddress { get; set; }

        public string SmtpServer { get; set; }

        public string TemplateStoreUrl { get; set; }

        public string MailBody { get; set; }
    }
}
