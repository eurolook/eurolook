﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data;
using Eurolook.Data.Database;
using Eurolook.Data.Models.TemplateStore;

namespace Eurolook.ActiveDirectoryLink.TemplateOwnership
{
    public class TemplateOwnershipHandler : EurolookServerDatabase, ITemplateOwnershipHandler
    {
        public TemplateOwnershipHandler(IActiveDirectoryConfigurationProvider activeDirectoryConfigurationProvider)
            : base(activeDirectoryConfigurationProvider.ServerDatabaseConnection)
        {
        }

        public void RemoveUserAsTemplateOwner(Guid userId)
        {
            using var context = GetContext();
            RemoveUserAsTemplateOwner(context, userId);
        }

        public void RemoveUserAsTemplateOwner(string userLogin)
        {
            using var context = GetContext();
            var userId = GetUserIdByLogin(context, userLogin);
            RemoveUserAsTemplateOwner(context, userId);
        }

        private static string PopulateBody(
            string newName,
            string oldName,
            IEnumerable<(Guid Id, string Name)> templatesToNotify,
            TemplateOwnershipConfiguration config)
        {
            string templateStoreUrl = config.TemplateStoreUrl;
            string body = config.MailBody;

            var changes = new StringBuilder();
            foreach ((var id, string name) in templatesToNotify)
            {
                changes.AppendLine(
                    "<li><a href=\"" + templateStoreUrl + "/Templates/" + id + "\">" + name + "</a></li>");
            }

            body = body.Replace("{newOwner}", newName);
            body = body.Replace("{oldOwner}", oldName);
            body = body.Replace("{templates}", changes.ToString());
            body = body.Replace("{templatestoreurl}", templateStoreUrl + "MyTemplates");
            return body;
        }

        private static Guid? FindDirectSuperiorsIdForUser(EurolookContext context, Guid userId)
        {
            var superior = context.UserSuperiors
                                  .Where(s => s.UserId == userId && !s.Deleted && s.AuthorId.HasValue)
                                  .OrderByDescending(s => s.OrderIndex)
                                  .FirstOrDefault();
            if (superior != null)
            {
                return context.Users.FirstOrDefault(u => u.SelfId == superior.AuthorId && !u.Deleted)?.Id;
            }

            return null;
        }

        private static string GetTemplateName(EurolookContext context, Guid templateId)
        {
            return context.Templates
                          .Where(t => t.Id == templateId)
                          .Select(t => t.Name)
                          .FirstOrDefault();
        }

        private static Guid GetUserIdByLogin(EurolookContext context, string login)
        {
            return context.Users.Where(u => u.Login == login).Select(u => u.Id).FirstOrDefault();
        }

        private static bool IsValidConfig(TemplateOwnershipConfiguration config)
        {
            return config.FallbackUser != null
                   && config.FromAddress != null
                   && config.MailBody != null
                   && config.SmtpServer != null
                   && config.TemplateStoreUrl != null;
        }

        private void RemoveUserAsTemplateOwner(EurolookContext context, Guid userId)
        {
            var templateOwnership = context.TemplateOwners.Where(to => to.UserId == userId && to.IsMain && !to.Deleted)
                                           .ToList();
            if (templateOwnership.IsNullOrEmpty())
            {
                // user is not main owner of any templates.
                return;
            }

            var user = context.Users.FirstOrDefault(u => u.Id == userId);
            var config = GetSystemConfiguration(context);
            if (!IsValidConfig(config))
            {
                throw new Exception(
                    $"Could not handle template ownership for user {user?.Login} - system configuration is invalid.");
            }

            var modificationDate = DateTime.UtcNow;
            var fallbackOwnerId = GetUserIdByLogin(context, config.FallbackUser);
            var superiorId = FindDirectSuperiorsIdForUser(context, userId);
            var newOwnerId = superiorId ?? fallbackOwnerId;
            if (newOwnerId == Guid.Empty || newOwnerId == userId)
            {
                throw new Exception(
                    $"Could not handle template ownership for user {user?.Login} - new owner is invalid.");
            }

            var templatesToNotify = new List<(Guid, string)>();

            foreach (var templateOwner in templateOwnership)
            {
                var otherOwner = context.TemplateOwners
                                        .FirstOrDefault(
                                            to => to.TemplateId == templateOwner.TemplateId && to.UserId != userId
                                                && !to.Deleted);
                if (otherOwner != null)
                {
                    otherOwner.IsMain = true;
                }
                else
                {
                    var existingOwner = context.TemplateOwners.FirstOrDefault(
                        to => to.TemplateId == templateOwner.TemplateId && to.UserId == newOwnerId);

                    if (existingOwner != null)
                    {
                        existingOwner.SetDeletedFlag(false);
                        existingOwner.IsMain = true;
                    }
                    else
                    {
                        var newOwner = new TemplateOwner
                        {
                            TemplateId = templateOwner.TemplateId,
                            UserId = newOwnerId,
                            IsMain = true,
                        };
                        newOwner.Init();
                        context.TemplateOwners.Add(newOwner);
                    }

                    templatesToNotify.Add(
                        (templateOwner.TemplateId, GetTemplateName(context, templateOwner.TemplateId)));
                }

                templateOwner.SetDeletedFlag(modificationDate);
            }

            context.SaveChanges();

            SendNotificationMail(templatesToNotify, newOwnerId, userId, config);
        }

        private void SendNotificationMail(
            IReadOnlyCollection<(Guid Id, string Name)> templatesToNotify,
            Guid newOwner,
            Guid oldOwner,
            TemplateOwnershipConfiguration config)
        {
            if (templatesToNotify.IsNullOrEmpty())
            {
                return;
            }

            var oldUser = GetAuthorByUserId(oldOwner, false);
            var newUser = GetAuthorByUserId(newOwner, false);

            try
            {
                var mailMessage = new MailMessage
                {
                    From = new MailAddress(config.FromAddress),
                };
                mailMessage.To.Add(newUser.Email);

                string replyTo = config.FromAddress;
                if (!string.IsNullOrWhiteSpace(replyTo))
                {
                    mailMessage.ReplyToList.Add(new MailAddress(replyTo));
                }

                mailMessage.Subject = "EUROLOOK 10 – Ownership of Templates";
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = PopulateBody(
                    newUser.LatinFullName,
                    oldUser.LatinFullName,
                    templatesToNotify,
                    config);

                var smtpClient = new SmtpClient(config.SmtpServer);
                smtpClient.Send(mailMessage);
                this.LogInfo($"Sent template ownership notification to {newUser.Email} ({newUser.LatinFullName}).");
            }
            catch (Exception ex)
            {
                this.LogError(
                    $"Failed to send template ownership notification to {newUser.Email} ({newUser.LatinFullName}). \n{ex}");
            }
        }

        private TemplateOwnershipConfiguration GetSystemConfiguration(EurolookContext context)
        {
            return SystemConfigurationRepository.GetConfiguration<TemplateOwnershipConfiguration>(context, this);
        }
    }
}
