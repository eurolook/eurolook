using System;

namespace Eurolook.ActiveDirectoryLink.TemplateOwnership
{
    public interface ITemplateOwnershipHandler
    {
        public void RemoveUserAsTemplateOwner(Guid userId);

        public void RemoveUserAsTemplateOwner(string userLogin);
    }
}
