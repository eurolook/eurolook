﻿namespace Eurolook.ActiveDirectoryLink
{
    public interface IActiveDirectoryLinker
    {
        void PrepareLinking();

        bool CreateAndLinkUser(string userName, out bool hasBeenUpdated, bool useCache = false, bool forceCreate = false);

        bool CreateAndLinkUser(string userName, bool useCache = false, bool forceCreate = false);

        void InitAuthorProperties(string userName);

        /// <summary>
        /// Sometimes usernames need to be migrate (i.e. when the domain name changed).
        /// </summary>
        /// <param name="login">The login of the user</param>
        /// <returns>The new login of the user or null if no migration is needed.</returns>
        string GetMigratedUserName(string login);
    }
}
