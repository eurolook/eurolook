﻿using System;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.ActiveDirectoryLink
{
    public abstract class ActiveDirectoryLinkerBase : ICanLog, IActiveDirectoryLinker
    {
        protected ActiveDirectoryLinkerBase(
            IActiveDirectorySearcher activeDirectorySearcher,
            IEurolookAuthorRepository eurolookAuthorRepository,
            IActiveDirectoryLinkDatabase activeDirectoryLinkDatabase)
        {
            ActiveDirectorySearcher = activeDirectorySearcher;
            EurolookAuthorRepository = eurolookAuthorRepository;
            ActiveDirectoryLinkDatabase = activeDirectoryLinkDatabase;
        }

        public IActiveDirectorySearcher ActiveDirectorySearcher { get; }

        public IActiveDirectoryLinkDatabase ActiveDirectoryLinkDatabase { get; }

        protected IEurolookAuthorRepository EurolookAuthorRepository { get; }

        public abstract bool OnBeforeCreateAndLink(OnBeforeCreateLinkArgs args);

        /// <summary>
        /// Creates or revives the user profile if it does not exist.
        /// A newly created profile is initialized using the InitAuthorProperties method.
        /// Then the user profile is linked using UpdateUserFromActiveDirectory method.
        /// </summary>
        /// <param name="userName">The user's login name.</param>
        /// <param name="useCache">
        /// When set to true SearchResults is not cleared, the user record might be loaded from cache (if
        /// cached).
        /// </param>
        /// <param name="forceCreate">Create the user even if it is not in Active Directory.</param>
        /// <returns>True when the operation succeeded. This method doesn't throw an exception.</returns>
        public bool CreateAndLinkUser(string userName, bool useCache = false, bool forceCreate = false)
        {
            return CreateAndLinkUser(userName, out _, useCache, forceCreate);
        }

        /// <summary>
        /// Creates or revives the user profile if it does not exist.
        /// A newly created profile is initialized using the InitAuthorProperties method.
        /// Then the user profile is linked using UpdateUserFromActiveDirectory method.
        /// </summary>
        /// <param name="userName">The user's login name.</param>
        /// <param name="hasBeenUpdated">Set to true when a user has been updated by the function.</param>
        /// <param name="useCache">
        /// When set to true SearchResults is not cleared, the user record might be loaded from cache (if
        /// cached).
        /// </param>
        /// <param name="forceCreate">Create the user even if it is not in Active Directory.</param>
        /// <returns>True when the operation succeeded. This method doesn't throw an exception.</returns>
        public bool CreateAndLinkUser(string userName, out bool hasBeenUpdated, bool useCache = false, bool forceCreate = false)
        {
            hasBeenUpdated = false;

            try
            {
                if (!useCache)
                {
                    ActiveDirectorySearcher.SearchResults.Clear();
                }

                var activeDirectoryUser = ActiveDirectorySearcher.GetUser(userName);
                if (activeDirectoryUser == null && !forceCreate)
                {
                    this.LogWarn($"User {userName} not found in Active Directory.");
                    return false;
                }

                var args = new OnBeforeCreateLinkArgs
                {
                    UserName = userName,
                    ActiveDirectoryUser = activeDirectoryUser,
                };
                if (OnBeforeCreateAndLink(args))
                {
                    userName = args.UserName;
                }
                else
                {
                    return false;
                }

                if (ActiveDirectoryLinkDatabase.CreateOrReviveUserProfile(userName))
                {
                    InitAuthorProperties(userName);
                }

                var eurolookAuthor = EurolookAuthorRepository.GetAuthorByUserName(userName);
                if (eurolookAuthor == null)
                {
                    this.LogWarn($"Author for user {userName} not found in EL10 database.");
                    return false;
                }

                if (activeDirectoryUser != null)
                {
                    this.LogInfo($"Checking for updated data for '{userName}'");
                    hasBeenUpdated = UpdateUserFromActiveDirectory(activeDirectoryUser, eurolookAuthor);
                    this.LogInfo(
                        hasBeenUpdated ? $"Successfully linked '{userName}'" : $"No update necessary for '{userName}'");
                }

                return true;
            }
            catch (Exception ex)
            {
                this.LogError($"Failed linking user {userName}.\n{ex}");
                return false;
            }
        }

        public void InitAuthorProperties(string userName)
        {
            try
            {
                var activeDirectoryUser = ActiveDirectorySearcher.GetUser(userName);
                if (activeDirectoryUser == null)
                {
                    return;
                }

                var author = EurolookAuthorRepository.GetAuthorByUserName(userName);
                InitUserFromActiveDirectory(activeDirectoryUser, author);
            }
            catch (Exception ex)
            {
                this.LogError($"Failed initialising user {userName}. {ex}");
            }
        }

        public abstract void PrepareLinking();

        public abstract void InitUserFromActiveDirectory(
            [NotNull] ActiveDirectoryUser activeDirectoryUser,
            [NotNull] Author author);

        public abstract bool UpdateUserFromActiveDirectory(
            [NotNull] ActiveDirectoryUser activeDirectoryUser,
            [NotNull] Author author);

        public abstract string GetMigratedUserName(string login);

        public class OnBeforeCreateLinkArgs
        {
            public string UserName { get; set; }

            public ActiveDirectoryUser ActiveDirectoryUser { get; set; }
        }
    }
}
