﻿using System.Threading.Tasks;

namespace Eurolook.ActiveDirectoryLink
{
    public interface IActiveDirectoryLinkDatabase
    {
        bool CreateOrReviveUserProfile(string login, string email = null);

        bool UpdateAdGroupsOfUser(string login, string adGroups);
    }
}
