﻿using Autofac;
using Eurolook.ActiveDirectoryLink.TemplateOwnership;

namespace Eurolook.ActiveDirectoryLink.AutofacModules
{
    public class ActiveDirectoryLinkModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ActiveDirectoryLinkDatabase>().AsImplementedInterfaces();
            builder.RegisterType<ActiveDirectorySearcher>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<TemplateOwnershipHandler>().AsImplementedInterfaces();
        }
    }
}
