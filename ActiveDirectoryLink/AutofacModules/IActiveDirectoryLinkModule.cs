using Autofac.Core;

namespace Eurolook.ActiveDirectoryLink.AutofacModules
{
    public interface IActiveDirectoryLinkModule : IModule
    {
    }
}
