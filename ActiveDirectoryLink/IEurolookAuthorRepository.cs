﻿using System.Collections.Generic;
using Eurolook.Data.Models;
using JetBrains.Annotations;

namespace Eurolook.ActiveDirectoryLink
{
    public interface IEurolookAuthorRepository
    {
        Author GetAuthorByUserName(string userName, bool includeDeleted = true);

        Author UpdateAuthor(Author author, ModificationType modificationType);

        List<string> GetAllUsers();

        List<string> GetInactiveUsers();

        [CanBeNull]
        Workplace GetWorkplace(Author author, string activeDirectoryCity);

        Workplace GetPrimaryWorkplace(Author author);

        Workplace UpdateWorkplace(Workplace workplace, ModificationType modificationType);

        IEnumerable<OrgaEntity> GetOrgaEntitiesByName(string name);

        OrgaEntity[] GetAllOrgaEntities();

        PredefinedFunction GetPredefinedFunction(string adReference);

        PredefinedFunction CreateOrRevivePredefinedFunction(
            string adReference,
            Dictionary<string, string> functionsByLanguageName);

        bool DeleteUserProfilePermanently(string userName);

        bool DeleteUserProfile(string userName);

        Address GetAddress(string city);

        void ResetWorkplaceAddress(Workplace workplace, Address notAddress);

        string FindUserName(string fullLoginName);

        bool TryMigrateUserName(string userName, string newUserName);
    }
}
