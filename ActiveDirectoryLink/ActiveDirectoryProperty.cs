﻿namespace Eurolook.ActiveDirectoryLink
{
    /// <summary>
    /// A static class defining constants for the AD property names used by ActiveDirectoryLink.
    /// </summary>
    public static class ActiveDirectoryProperty
    {
        public const string SamAccountName = "sAMAccountName";
        public const string DisplayName = "displayName";

        /// <summary>
        /// Contains the surname / lastname of a user.
        /// </summary>
        public const string Sn = "sn";

        public const string GivenName = "givenName";
        public const string Department = "department";
        public const string Title = "title";
        public const string Comment = "comment";
        public const string Mail = "mail";
        public const string MailNickname = "mailNickname";
        public const string PersonalTitle = "personalTitle";
        public const string PhysicalDeliveryOfficeName = "physicalDeliveryOfficeName";
        public const string TelephoneNumber = "telephoneNumber";
        public const string FacsimileTelephoneNumber = "facsimileTelephoneNumber";
        public const string Description = "description";
        public const string AccountExpires = "accountExpires";

        /// <summary>
        /// May contain the location / city of a user.
        /// </summary>
        public const string L = "l";

        /// <summary>
        /// May contain the country of a user.
        /// </summary>
        public const string Co = "co";

        public const string Company = "company";

        public const string Ou = "ou";

        public const string MemberOf = "memberOf";

        /// <summary>
        /// May be used to contain additional information such as the name of the role / function in EN.
        /// </summary>
        public const string ExtensionAttribute7 = "extensionAttribute7";

        /// <summary>
        /// May be used to contain additional information such as the name of role / function in FR.
        /// </summary>
        public const string ExtensionAttribute8 = "extensionAttribute8";

        /// <summary>
        /// May be used to contain additional information such as the name of the Orga Entity in EN.
        /// </summary>
        public const string ExtensionAttribute9 = "extensionAttribute9";

        /// <summary>
        /// May be used to contain additional information such as the name of the Orga Entity in FR.
        /// </summary>
        public const string ExtensionAttribute10 = "extensionAttribute10";

        /// <summary>
        /// May be used to contain additional information such as the phone number extension.
        /// </summary>
        public const string ExtensionAttribute11 = "extensionAttribute11";

        /// <summary>
        /// May be used to contain additional information such as fax number extension.
        /// </summary>
        public const string ExtensionAttribute12 = "extensionAttribute12";
    }
}
