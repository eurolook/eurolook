# Eurolook

Eurolook is an add-on to Microsoft Word. It supports users in drafting standard Word documents that observe the institution’s visual identity. It is part of the European Commission’s corporate document style management system.

Eurolook was designed and developed by the European Commission. Documents created using Eurolook contain the appropriate format and layout for each template (e.g. a Note with a Commission logo) and are prefilled with the author’s details (i.e. their name, initials, directorate, unit, etc.) along with other text elements (e.g. table of contents, subject, references etc.). It automates the creation of many templates.

Eurolook supports all the EU languages used in the Commission.

Eurolook uses document models, organisational chart information, author information and other data. To ensure that you benefit from the most recent data at all times, Eurolook updates it regularly from a central repository.

## Getting started
Read the developer guide in our wiki: https://code.europa.eu/eurolook/eurolook/-/wikis/Developer-Guide

<!--
## Installation
To be filled.

## Usage
To be filled.

## Support
To be filled.

## Roadmap
To be filled.

## Contributing
To be filled.

## Authors and acknowledgment
To be filled.

## License
MIT -->