namespace Eurolook.Data
{
    public class PaginationInfo : IPaginationInfo
    {
        public uint From { get; set; }

        public uint PageSize { get; set; }

        public bool OrderAscending { get; set; }

        public string OrderProperty { get; set; }
    }
}
