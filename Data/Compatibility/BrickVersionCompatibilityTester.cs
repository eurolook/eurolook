﻿using System;
using Eurolook.Data.Models;

namespace Eurolook.Data.Compatibility
{
    public class BrickVersionCompatibilityTester : IBrickVersionCompatibilityTester
    {
        private readonly IConfigurationInfo _configurationInfo;

        public BrickVersionCompatibilityTester(IConfigurationInfo configurationInfo)
        {
            _configurationInfo = configurationInfo;
        }

        public bool IsBrickCompatibleWithAddinVersion(Brick brick)
        {
            if (!string.IsNullOrWhiteSpace(brick.MinVersion) && Version.TryParse(brick.MinVersion, out var minVersion)
                                                             && minVersion > _configurationInfo.ClientVersion)
            {
                // Add-in is older than the allowed MinVersion
                return false;
            }

            if (!string.IsNullOrWhiteSpace(brick.MaxVersion) && Version.TryParse(brick.MaxVersion, out var maxVersion)
                                                             && _configurationInfo.ClientVersion > maxVersion)
            {
                // Add-in is newer than the allowed MaxVersion
                return false;
            }

            return true;
        }
    }
}
