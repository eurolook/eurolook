﻿using System;
using Eurolook.Data.Models;

namespace Eurolook.Data.Compatibility
{
   public class DocumentModelVersionCompatibilityTester : IDocumentModelVersionCompatibilityTester
    {
        private readonly IConfigurationInfo _configurationInfo;

        public DocumentModelVersionCompatibilityTester(IConfigurationInfo configurationInfo)
        {
            _configurationInfo = configurationInfo;
        }

        public bool IsDocumentModelCompatibleWithAddinVersion(DocumentModel documentModel)
        {
            if (!string.IsNullOrWhiteSpace(documentModel.MinVersion) && Version.TryParse(documentModel.MinVersion, out var minVersion)
                                                                     && minVersion > _configurationInfo.ClientVersion)
            {
                // Add-in is older than the allowed MinVersion
                return false;
            }

            if (!string.IsNullOrWhiteSpace(documentModel.MaxVersion) && Version.TryParse(documentModel.MaxVersion, out var maxVersion)
                                                                     && _configurationInfo.ClientVersion > maxVersion)
            {
                // Add-in is newer than the allowed MaxVersion
                return false;
            }

            return true;
        }
    }
}
