﻿using Eurolook.Data.Models;

namespace Eurolook.Data.Compatibility
{
    public interface IDocumentModelVersionCompatibilityTester
    {
        bool IsDocumentModelCompatibleWithAddinVersion(DocumentModel documentModel);
    }
}
