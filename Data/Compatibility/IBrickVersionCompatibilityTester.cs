﻿using Eurolook.Data.Models;

namespace Eurolook.Data.Compatibility
{
    public interface IBrickVersionCompatibilityTester
    {
        bool IsBrickCompatibleWithAddinVersion(Brick brick);
    }
}
