﻿using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;

namespace Eurolook.Data.TemplateStore
{
    public class TemplateOwnerInfo
    {
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public bool IsMain { get; set; }

        public TemplateOwnerInfo()
        {
            // used implicitely
        }

        public TemplateOwnerInfo(TemplateOwner owner)
        {
            DisplayName = GetDisplayName(owner.User);
            Email = owner.User.Self.Email;
            IsMain = owner.IsMain;
        }

        private string GetDisplayName(User user)
        {
            var author = user?.Self;
            return author?.FullNameWithLastNameFirst ?? user?.Login;
        }
    }
}
