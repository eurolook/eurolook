﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using Eurolook.Data.Models.TemplateStore;
using JetBrains.Annotations;

namespace Eurolook.Data.TemplateStore
{
    public class TemplatePackage
    {
        [UsedImplicitly]
        public TemplatePackage()
        {
        }

        public TemplatePackage(Template template, IEnumerable<TemplateFile> templateFiles)
        {
            TemplateName = template.Name;
            Created = DateTime.UtcNow;
            var mainOwner = template.Owners.FirstOrDefault(x => x.IsMain) ?? template.Owners.FirstOrDefault();
            MainAuthor = mainOwner?.User?.Self?.LatinFullName ?? "";
            Template = template;
            TemplateFiles.AddRange(templateFiles);
        }

        public string TemplateName { get; set; }

        public DateTime Created { get; set; }

        public string MainAuthor { get; set; }

        public Template Template { get; set; }

        public List<TemplateFile> TemplateFiles { get; set; } = new();

        public string GetTemplateCreator()
        {
            return !string.IsNullOrEmpty(MainAuthor) ? MainAuthor : Template.Tags;
        }

        public static TemplatePackage FromFile(string fileName)
        {
            using var fileStream = File.OpenText(fileName);
            var serializer = new XmlSerializer(typeof(TemplatePackage));
            return (TemplatePackage)serializer.Deserialize(fileStream);
        }

        public void SaveAs(string fileName)
        {
            using var fileStream = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write);
            var writer = new XmlSerializer(typeof(TemplatePackage));
            writer.Serialize(XmlWriter.Create(fileStream, new XmlWriterSettings { Indent = true }), this);
        }
    }
}
