﻿using System;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.Data.Database;

namespace Eurolook.Data.TemplateStore
{
    public class SharedTemplateService : ISharedTemplateService, ICanLog
    {
        private readonly ISharedTemplateRepository _sharedTemplateRepository;

        public SharedTemplateService(ISharedTemplateRepository sharedTemplateRepository)
        {
            _sharedTemplateRepository = sharedTemplateRepository;
        }

        public async Task<bool> ExportTemplate(Guid templateId, string fileName)
        {
            try
            {
                var template = await _sharedTemplateRepository.GetTemplate(templateId);
                var templateFiles = await _sharedTemplateRepository.GetTemplateFilesForTemplate(templateId);
                var package = new TemplatePackage(template, templateFiles);
                package.SaveAs(fileName);
            }
            catch (Exception ex)
            {
                this.LogError($"Failed to save template package at {fileName}.", ex);
                return false;
            }

            return true;
        }
    }
}
