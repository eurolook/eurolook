﻿using System;
using System.Threading.Tasks;

namespace Eurolook.Data.TemplateStore
{
    public interface ISharedTemplateService
    {
        Task<bool> ExportTemplate(Guid templateId, string fileName);
    }
}
