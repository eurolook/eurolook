﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Eurolook.Common;
using Eurolook.Common.Log;
using Eurolook.Data.Constants;
using Eurolook.Data.Extensions;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace Eurolook.Data
{
    public abstract class EurolookDatabase<T> : ICanLog
        where T : EurolookContext
    {
        public static bool ExistsLocally<TEntity>(EurolookContext context, TEntity entity)
            where TEntity : Identifiable
        {
            return context.Set<TEntity>().Local.Any(e => e.Id == entity.Id);
        }

        public static void AddOrUpdateUserSuperior(UserSuperior userSuperior, T context)
        {
            // check the Superiors.Local set because the author could be added by other userAuthor relations before
            if (userSuperior.Author != null && !ExistsLocally(context, userSuperior.Author))
            {
                context.Authors.AddOrUpdate(ShallowClone.Clone(userSuperior.Author));

                foreach (var jobFunction in userSuperior.Author.Functions)
                {
                    context.JobFunctions.AddOrUpdate(ShallowClone.Clone(jobFunction));
                }

                foreach (var workplace in userSuperior.Author.Workplaces)
                {
                    context.Workplaces.AddOrUpdate(ShallowClone.Clone(workplace));
                }

                foreach (var jobAssignment in userSuperior.Author.JobAssignments)
                {
                    context.JobAssignments.AddOrUpdate(ShallowClone.Clone(jobAssignment));
                    foreach (var jobFunction in jobAssignment.Functions)
                    {
                        context.JobFunctions.AddOrUpdate(ShallowClone.Clone(jobFunction));
                    }
                }
            }

            context.UserSuperiors.AddOrUpdate(ShallowClone.Clone(userSuperior));
        }

        public bool UserExists(string login)
        {
            try
            {
                using (var context = GetContext())
                {
                    return context.Users.AsNoTracking().Any(x => x.Login == login);
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public User AddUserProfile(User user)
        {
            if (user == null)
            {
                return null;
            }

            using (var context = GetContext())
            {
                AddUserTemplatesDistinct(context, user.Templates);

                if (user.Settings != null)
                {
                    context.UserSettings.AddOrUpdate(ShallowClone.Clone(user.Settings));
                    foreach (var docSettings in user.Settings.DocumentModelSettings)
                    {
                        if (docSettings.Template != null &&
                            context.Templates.Local.All(x => x.Id != docSettings.Template.Id))
                        {
                            context.Templates.AddOrUpdate(ShallowClone.Clone(docSettings.Template));
                        }

                        context.DocumentModelSettings.AddOrUpdate(ShallowClone.Clone(docSettings));
                    }

                    foreach (var deviceSettings in user.Settings.DeviceSettings)
                    {
                        context.DeviceSettings.AddOrUpdate(deviceSettings);
                    }

                    foreach (var brickSettings in user.Settings.BrickSettings)
                    {
                        context.BrickSettings.AddOrUpdate(brickSettings);
                    }
                }

                if (user.Bricks != null)
                {
                    foreach (var userBrick in user.Bricks)
                    {
                        if (userBrick?.Brick != null)
                        {
                            context.Bricks.AddOrUpdate(userBrick.Brick);
                            context.UserBricks.AddOrUpdate(userBrick);
                        }
                    }
                }

                if (user.Self != null)
                {
                    context.Authors.AddOrUpdate(ShallowClone.Clone(user.Self));
                    if (user.Self.Functions != null)
                    {
                        foreach (var jobFunction in user.Self.Functions)
                        {
                            context.JobFunctions.AddOrUpdate(ShallowClone.Clone(jobFunction));
                        }
                    }

                    if (user.Self.JobAssignments != null)
                    {
                        foreach (var jobAssignment in user.Self.JobAssignments)
                        {
                            context.JobAssignments.AddOrUpdate(ShallowClone.Clone(jobAssignment));
                            foreach (var jobFunction in jobAssignment.Functions)
                            {
                                context.JobFunctions.AddOrUpdate(ShallowClone.Clone(jobFunction));
                            }
                        }
                    }

                    if (user.Self.Workplaces != null)
                    {
                        foreach (var workplace in user.Self.Workplaces)
                        {
                            context.Workplaces.AddOrUpdate(ShallowClone.Clone(workplace));
                        }
                    }

                    if (user.Authors != null)
                    {
                        foreach (var userAuthor in user.Authors)
                        {
                            AddOrUpdateUserAuthor(userAuthor, context);
                        }
                    }

                    if (user.Superiors != null)
                    {
                        foreach (var userSuperior in user.Superiors)
                        {
                            AddOrUpdateUserSuperior(userSuperior, context);
                        }
                    }
                }

                context.Users.AddOrUpdate(ShallowClone.Clone(user));
                context.SaveChanges();
            }

            return user;
        }

        private static void AddUserTemplatesDistinct(T context, IEnumerable<UserTemplate> userTemplates)
        {
            var distinctUserTemplates = new List<UserTemplate>();
            var distinctTemplates = new List<Template>();
            var distinctTemplateFiles = new List<TemplateFile>();
            var distinctTemplatePublications = new List<TemplatePublication>();

            foreach (var userTemplate in userTemplates)
            {
                if (userTemplate != null && !distinctUserTemplates.Any(x => x.Id == userTemplate.Id))
                {
                    distinctUserTemplates.Add(ShallowClone.Clone(userTemplate));
                }

                var template = userTemplate?.Template;
                if (template == null)
                {
                    continue;
                }

                if (!distinctTemplates.Any(x => x.Id == template.Id))
                {
                    distinctTemplates.Add(ShallowClone.Clone(template));
                }

                foreach (var templateFile in template.TemplateFiles)
                {
                    if (templateFile != null && !distinctTemplateFiles.Any(x => x.Id == templateFile.Id))
                    {
                        distinctTemplateFiles.Add(ShallowClone.Clone(templateFile));
                    }
                }

                foreach (var templatePublication in template.Publications)
                {
                    if (templatePublication != null && !distinctTemplatePublications.Any(x => x.Id == templatePublication.Id))
                    {
                        distinctTemplatePublications.Add(ShallowClone.Clone(templatePublication));
                    }
                }
            }

            context.Templates.AddRange(distinctTemplates);
            context.TemplatePublications.AddRange(distinctTemplatePublications);
            context.TemplateFiles.AddRange(distinctTemplateFiles);
            context.UserTemplates.AddRange(distinctUserTemplates);
        }

        public Author UpdateAuthor(Author author, ModificationType modificationType)
        {
            using (var context = GetContext())
            {
                var result = Update(context.Authors, author, modificationType);
                context.SaveChanges();
                return result;
            }
        }

        public Author GetAuthorByUserName(string userName, bool includeDeleted = true)
        {
            using (var context = GetContext())
            {
                var query = context.Users.Where(u => u.Login == userName);
                if (!includeDeleted)
                {
                    query = query.Where(u => !u.Deleted);
                }

                var user = query.FirstOrDefault();
                if (user != null)
                {
                    return context.Authors.FirstOrDefault(a => a.Id == user.SelfId);
                }
            }

            return null;
        }

        public Author GetAuthorByUserId(Guid userId, bool includeDeleted = true)
        {
            using (var context = GetContext())
            {
                var query = context.Users.Where(u => u.Id == userId);
                if (!includeDeleted)
                {
                    query = query.Where(u => !u.Deleted);
                }

                var user = query.FirstOrDefault();
                if (user != null)
                {
                    return context.Authors.FirstOrDefault(a => a.Id == user.SelfId);
                }
            }

            return null;
        }

        public UserSettings UpdateUserSettings(UserSettings settings, ModificationType modificationType)
        {
            var context = GetContext();
            var result = Update(context.UserSettings, settings, modificationType);
            context.SaveChanges();
            return result;
        }

        public List<Address> GetAddresses(bool includeSpecialAddressNoAddress = false)
        {
            using (var context = GetContext())
            {
                return GetAddressesQueryable(context, includeSpecialAddressNoAddress).ToList();
            }
        }

        public async Task<List<Address>> GetAddressesAsync(bool includeSpecialAddressNoAddress = false)
        {
            using (var context = GetContext())
            {
                return await GetAddressesQueryable(context, includeSpecialAddressNoAddress).ToListAsync();
            }
        }

        public Address GetAddress(Guid? id)
        {
            if (id == null)
            {
                return null;
            }

            using (var context = GetContext())
            {
                return context.Addresses.AsNoTracking().FirstOrDefault(x => x.Id == id);
            }
        }

        public OrgaEntity GetOrgaEntity(Guid id)
        {
            using (var context = GetContext())
            {
                return GetOrgaEntity(context, id);
            }
        }

        public OrgaEntity GetOrgaEntity(EurolookContext context, Guid id)
        {
            return context.OrgaEntities.FirstOrDefault(x => x.Id == id);
        }

        public BrickGroup GetBrickGroup(Guid id)
        {
            var context = GetContext();
            return context.BrickGroups.FirstOrDefault(x => x.Id == id && !x.Deleted);
        }

        [CanBeNull]
        public BrickGroup GetBrickGroupForEdit(Guid id)
        {
            using (var context = GetContext())
            {
                var brickGroup = context.BrickGroups.Include(bg => bg.Bricks).FirstOrDefault(bg => bg.Id == id);
                brickGroup?.Bricks.RemoveAll(b => b.Deleted);

                return brickGroup;
            }
        }

        public List<Brick> GetAllBricks()
        {
            using (var context = GetContext())
            {
                return context.Bricks.AsNoTracking().OrderBy(b => b.Name).ToList();
            }
        }

        public async Task<List<Brick>> GetAllBricksAsync()
        {
            using (var context = GetContext())
            {
                return await context.Bricks.AsNoTracking().ToListAsync();
            }
        }

        public List<BrickCategory> GetAllBrickCategories()
        {
            using (var context = GetContext())
            {
                return context.BrickCategories.AsNoTracking().OrderBy(b => b.UiPositionIndex).ToList();
            }
        }

        public List<UserGroup> GetAllUserGroups()
        {
            using (var context = GetContext())
            {
                return context.UserGroups.AsNoTracking()
                              .Where(ug => !ug.Deleted)
                              .OrderBy(ug => ug.UiPositionIndex).ToList();
            }
        }

        public Brick GetBrick(Guid id)
        {
            using (var context = GetContext())
            {
                return context.Bricks.AsNoTracking().FirstOrDefault(x => x.Id == id);
            }
        }

        public async Task<List<Brick>> GetBricksOfGroup(Guid groupId)
        {
            using (var context = GetContext())
            {
                return await context.Bricks.AsNoTracking().Where(b => b.GroupId == groupId && !b.Deleted).ToListAsync();
            }
        }

        public List<CommandBrick> GetCommandBricksOfGroup(Guid groupId)
        {
            using (var context = GetContext())
            {
                return context.Bricks
                              .AsNoTracking()
                              .Where(b => b.GroupId == groupId && !b.Deleted)
                              .OfType<CommandBrick>()
                              .ToList();
            }
        }

        public int GetCommandBricksOfGroupCount(Guid groupId)
        {
            using (var context = GetContext())
            {
                return context.Bricks
                              .AsNoTracking()
                              .Where(b => b.GroupId == groupId && !b.Deleted)
                              .OfType<CommandBrick>()
                              .Count();
            }
        }

        public DocumentStructure GetDocumentStructure(Guid id)
        {
            using (var context = GetContext())
            {
                return context.DocumentStructure.AsNoTracking().FirstOrDefault(x => x.Id == id);
            }
        }

        public List<Text> GetAllTexts()
        {
            var context = GetContext();
            return context.Texts.Where(t => !t.Deleted).OrderBy(t => t.Alias).ToList();
        }

        public async Task<List<Text>> GetAllTextsAsync()
        {
            using (var context = GetContext())
            {
                return await context.Texts.AsNoTracking().ToListAsync();
            }
        }

        public List<Resource> GetAllResources()
        {
            var context = GetContext();
            return context.Resources.Where(r => !r.Deleted).OrderBy(x => x.Alias).ToList();
        }

        public async Task<List<Resource>> GetAllResourcesAsync()
        {
            using (var context = GetContext())
            {
                return await context.Resources.Where(r => !r.Deleted).OrderBy(x => x.Alias).ToListAsync();
            }
        }

        public void LoadTexts<TT>(TT entity, Language lang, Language fallback, bool deleted = true)
            where TT : Identifiable
        {
            if (entity == null)
            {
                return;
            }

            using (var context = GetContext())
            {
                LoadTexts(context, lang.Id, entity, fallback, deleted);
            }
        }

        protected static async Task UpdateDiscriminator(EurolookContext context, IEnumerable<Brick> bricks)
        {
            foreach (var brick in bricks)
            {
                string discriminator = brick.GetType().Name;
                string id = brick.Id.ToString();
                await context.Database.ExecuteSqlRawAsync(
                    $"UPDATE Bricks SET Discriminator = '{discriminator}' WHERE Id = '{id}'");
            }
        }

        protected static void UpdateEntity<TT>(TT source, TT dest)
            where TT : Identifiable
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source), "Cannot update from a null object");
            }

            if (dest == null)
            {
                throw new ArgumentNullException(nameof(dest), "Cannot update a null object");
            }

            if (source.Id != dest.Id)
            {
                throw new Exception("Cannot update an Identifiable object with another Id");
            }

            CopyProperties<TT>.Copy(source, dest);
        }

        protected abstract T GetContext();

        protected void SetLocalDataDirectory(bool useRoamingDataDirectory)
        {
            var eurolookProfileDirectory = Directories.GetApplicationDataDirectory(useRoamingDataDirectory);
            if (!eurolookProfileDirectory.Exists)
            {
                eurolookProfileDirectory.Create();
            }

            AppDomain.CurrentDomain.SetData("DataDirectory", eurolookProfileDirectory.FullName);
        }

        /// <summary>
        /// Insert or updates all eurolook data in the correct order (to avoid foreign key constraint violations).
        /// </summary>
        protected void AddOrUpdateUpdatePlan(EurolookContext context, UpdatePlan updatePlan)
        {
            AddOrUpdateBasicEurolookData(context, updatePlan);
            AddOrUpdate(context.Notifications, updatePlan.Notifications.ToArray());
        }

        /// <summary>
        /// Insert or updates all eurolook data in the correct order (to avoid foreign key constraint violations).
        /// </summary>
        protected void AddOrUpdateBasicEurolookData(EurolookContext context, BasicEurolookDataSet dataSet)
        {
            AddOrUpdate(context.Languages, dataSet.Languages.ToArray());
            AddOrUpdate(context.Resources, dataSet.Resources.ToArray());
            AddOrUpdate(context.LocalisedResources, dataSet.LocalisedResources.ToArray());
            AddOrUpdate(context.Texts, dataSet.Texts.ToArray());
            AddOrUpdate(context.Translations, dataSet.Translations.ToArray());
            AddOrUpdate(context.Addresses, dataSet.Addresses.ToArray());
            AddOrUpdate(context.OrgaEntities, dataSet.OrgaEntities.ToArray());
            AddOrUpdate(context.BrickCategories, dataSet.BrickCategories.ToArray());
            AddOrUpdate(context.BrickGroups, dataSet.BrickGroups.ToArray());
            AddOrUpdate(context.UserGroups, RetainAdGroupsOfUserGroup(context, dataSet.UserGroups).ToArray());
            AddOrUpdate(context.Bricks, dataSet.CommandBricks.Cast<Brick>().ToArray());
            AddOrUpdate(context.Bricks, dataSet.DynamicBricks.Cast<Brick>().ToArray());
            AddOrUpdate(context.Bricks, dataSet.ContentBricks.Cast<Brick>().ToArray());
            AddOrUpdate(context.DocumentCategories, dataSet.DocumentCategories.ToArray());
            AddOrUpdate(context.DocumentModels, dataSet.DocumentModels.ToArray());
            AddOrUpdate(context.DocumentStructure, dataSet.DocumentStructures.ToArray());
            AddOrUpdate(context.DocumentModelLanguages, dataSet.DocumentModelLanguages.ToArray());
            AddOrUpdate(context.BrickTexts, dataSet.BrickTexts.ToArray());
            AddOrUpdate(context.BrickResources, dataSet.BrickResources.ToArray());
            AddOrUpdate(context.CharacterMappings, dataSet.CharacterMappings.ToArray());
            AddOrUpdate(context.PersonNames, dataSet.PersonNames.ToArray());
            AddOrUpdate(context.StyleShortcuts, dataSet.StyleShortcuts.ToArray());
            AddOrUpdate(context.ColorSchemes, dataSet.ColorSchemes.ToArray());
            AddOrUpdate(context.TermStoreImports, dataSet.TermStoreImports.ToArray());
            AddOrUpdate(context.MetadataCategories, dataSet.MetadataCategories.ToArray());
            AddOrUpdate(context.MetadataDefinitions, dataSet.MetadataDefinitions.ToArray());
            AddOrUpdate(context.DocumentModelMetadataDefinitions, dataSet.DocumentModelMetadataDefinitions.ToArray());
            AddOrUpdate(context.AuthorRoles, dataSet.AuthorRoles.ToArray());
            AddOrUpdate(context.DocumentModelAuthorRoles, dataSet.DocumentModelAuthorRoles.ToArray());
            AddOrUpdate(context.PredefinedFunctions, dataSet.PredefinedFunctions.ToArray());
            AddOrUpdate(context.SystemConfigurations, dataSet.SystemConfigurations.ToArray());
            AddOrUpdate(context.SystemTexts, dataSet.SystemTexts.ToArray());
            AddOrUpdate(context.SystemResources, dataSet.SystemResources.ToArray());
            AddOrUpdate(context.Notifications, dataSet.Notifications.ToArray());
        }

        protected void AddOrUpdate<TEntity>(DbSet<TEntity> dbSet, TEntity[] entities)
            where TEntity : Identifiable
        {
            if (entities.Any())
            {
                // NOTE: We check whether the table is empty or not because simply adding to an empty table
                // is much faster than calling AddOrUpdate
                if (dbSet.Any())
                {
                    foreach (var entity in entities)
                    {
                        dbSet.AddOrUpdate(entity);
                    }
                }
                else
                {
                    dbSet.AddRange(entities);
                }

                this.LogDebugFormat("Updated {0}: {1} items", typeof(TEntity), entities.Length);
            }
        }

        /// <summary>
        /// Updates an existing entity in the database;
        /// child entities are ignored, only id properties of foreign keys are used.
        /// </summary>
        /// <typeparam name="TT">The type of the entity being updated.</typeparam>
        protected TT Update<TT>(DbSet<TT> table, TT entity, ModificationType modificationType)
            where TT : Synchronizable
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity), "Cannot update with a null object");
            }

            this.LogTrace($"Update<{typeof(T).Name}>(entity.Id={entity.Id}, mod={modificationType})");
            var dbEntity = SelectFromLocalOrDb(table, entity.Id);
            UpdateEntity(entity, dbEntity);
            dbEntity.SetModification(modificationType);
            return dbEntity;
        }

        /// <summary>
        /// Updates an existing entity in the database;
        /// child entities are ignored, only id properties of foreign keys are used.
        /// </summary>
        /// <typeparam name="TT">The type of the entity being updated.</typeparam>
        /// <returns>The entity that has been added to the context.</returns>
        protected TT Add<TT>(DbSet<TT> table, TT entity, ModificationType modificationType)
            where TT : Synchronizable
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity), "Cannot add a null object");
            }

            this.LogDebugFormat("Add<{0}>(entity.Id={1}, mod={2})", typeof(T).Name, entity.Id, modificationType);

            var entityEntry = table.Add(entity);
            entityEntry.Entity.SetModification(modificationType);
            return entityEntry.Entity;
        }

        protected void LoadTexts<TT>(EurolookContext context, Language lang, TT entity, Language fallback)
            where TT : Identifiable
        {
            LoadTexts(context, lang.Id, entity, fallback);
        }

        /// <summary>
        /// Loads all localized texts of a PredefinedFunction.
        /// </summary>
        /// <param name="predefinedFunction">The <see cref="PredefinedFunction" /> of an author.</param>
        /// <param name="language">The id of the language in which the texts are loaded.</param>
        /// <param name="fallback">
        /// The language to be used in case there is no localization available
        /// for the language requested by <paramref name="language" />.
        /// </param>
        /// <param name="context">The database context.</param>
        /// <param name="deleted">
        /// Optional parameter to indicate whether the calling method is the
        /// recursive construction of the OrgaChart. (from OrgaChartDatabase class).
        /// </param>
        protected void LoadPredefinedFunctionForAuthor(
            PredefinedFunction predefinedFunction,
            Language language,
            Language fallback,
            EurolookContext context,
            bool deleted = true)
        {
            if (predefinedFunction == null)
            {
                return;
            }

            var properties = typeof(PredefinedFunction).GetProperties();
            var transProperties = properties.Where(p => p.PropertyType == typeof(Translation)).ToArray();
            var textIds = new List<Guid?>();
            var transProps = new List<PropertyInfo>();
            var translations = new List<Translation>();
            foreach (var prop in properties.Where(p => p.PropertyType == typeof(Text)).ToList())
            {
                textIds.Add(FindTextId(predefinedFunction, prop, properties));
                transProps.Add(FindTranslationProperty(transProperties, prop));
            }

            int k = transProps.FindIndex(t => t.Name == "Function");
            var maleFunctionFallback = !deleted
                ? context.Translations.AsNoTracking().FirstOrDefault(
                    t => t.LanguageId == fallback.Id && t.TextId == textIds[k] && !t.Deleted)
                : context.Translations.AsNoTracking()
                         .FirstOrDefault(t => t.LanguageId == fallback.Id && t.TextId == textIds[k]);

            foreach (var textId in textIds)
            {
                // do not show deleted translations
                translations.Add(
                    !deleted
                        ? context.Translations.AsNoTracking().FirstOrDefault(
                            t => t.LanguageId == language.Id && t.TextId == textId && !t.Deleted)
                        : context.Translations.AsNoTracking()
                                 .FirstOrDefault(t => t.LanguageId == language.Id && t.TextId == textId));
            }

            for (var i = 0; i < textIds.Count; i++)
            {
                if (textIds[i] == null)
                {
                    continue;
                }

                if (transProps[i] == null)
                {
                    continue;
                }

                if (translations[i] != null)
                {
                    transProps[i].SetValue(predefinedFunction, translations[i], null);
                }
                else
                {
                    switch (transProps[i].Name)
                    {
                        // male function text => male function text fallback language
                        case "Function":

                            if (fallback != null)
                            {
                                transProps[i].SetValue(predefinedFunction, maleFunctionFallback, null);
                            }

                            break;

                        // female function text => male function text => male function text fallback language
                        // male header text => male function => male fallback language header text
                        case "FunctionFemale":
                        case "FunctionHeader":

                            if (translations[k] != null)
                            {
                                transProps[i].SetValue(predefinedFunction, translations[k], null);
                            }
                            else if (fallback != null)
                            {
                                transProps[i].SetValue(predefinedFunction, maleFunctionFallback, null);
                            }

                            break;

                        // female header text => male header text => female function text => male function text => male function text fallback language
                        case "FunctionHeaderFemale":

                            int j = transProps.FindIndex(t => t.Name == "FunctionHeader");
                            if (translations[j] != null)
                            {
                                transProps[i].SetValue(predefinedFunction, translations[j], null);
                            }
                            else
                            {
                                int m = transProps.FindIndex(t => t.Name == "FunctionFemale");
                                if (translations[m] != null)
                                {
                                    transProps[i].SetValue(predefinedFunction, translations[m], null);
                                }
                                else
                                {
                                    transProps[i].SetValue(
                                        predefinedFunction,
                                        translations[k] ?? maleFunctionFallback,
                                        null);
                                }
                            }

                            break;
                    }
                }
            }
        }

        protected void LoadTexts<TT>(
            EurolookContext context,
            Guid languageId,
            TT entity,
            Language fallback,
            bool deleted = true)
            where TT : Identifiable
        {
            var properties = entity.GetType().GetProperties();
            var transProperties = properties.Where(p => p.PropertyType == typeof(Translation)).ToArray();
            foreach (var textProp in properties.Where(p => p.PropertyType == typeof(Text)).ToList())
            {
                // get the Text Id ...
                var textId = FindTextId(entity, textProp, properties);
                if (textId == null)
                {
                    continue;
                }

                // find the Translation property referencing the Text property
                var transProp = FindTranslationProperty(transProperties, textProp);
                if (transProp == null)
                {
                    continue;
                }

                // do not show deleted translations
                var translation = !deleted
                    ? context.Translations.AsNoTracking().FirstOrDefault(
                        t => t.LanguageId == languageId && t.TextId == textId && !t.Deleted)
                    : context.Translations.AsNoTracking()
                             .FirstOrDefault(t => t.LanguageId == languageId && t.TextId == textId);

                if (translation?.Value != null)
                {
                    transProp.SetValue(entity, translation, null);
                }
                else if (translation?.Value == null && fallback != null)
                {
                    translation = context.Translations.AsNoTracking()
                                         .FirstOrDefault(t => t.LanguageId == fallback.Id && t.TextId == textId);
                    if (translation != null)
                    {
                        transProp.SetValue(entity, translation, null);
                    }
                }
            }
        }

        protected TT SelectFromLocalOrDb<TT>(DbSet<TT> table, Guid id)
            where TT : Identifiable
        {
            return table.Local.FirstOrDefault(e => e.Id == id) ?? table.FirstOrDefault(e => e.Id == id);
        }

        private static void AddOrUpdateUserAuthor(UserAuthor userAuthor, T context)
        {
            // check the Authors.Local set because the author could be added by other userAuthor relations before
            if (userAuthor.Author != null && !ExistsLocally(context, userAuthor.Author))
            {
                context.Authors.AddOrUpdate(ShallowClone.Clone(userAuthor.Author));
                foreach (var jobFunction in userAuthor.Author.Functions)
                {
                    context.JobFunctions.AddOrUpdate(ShallowClone.Clone(jobFunction));
                }

                foreach (var workplace in userAuthor.Author.Workplaces)
                {
                    context.Workplaces.AddOrUpdate(ShallowClone.Clone(workplace));
                }

                foreach (var jobAssignment in userAuthor.Author.JobAssignments)
                {
                    context.JobAssignments.AddOrUpdate(ShallowClone.Clone(jobAssignment));
                    foreach (var jobFunction in jobAssignment.Functions)
                    {
                        context.JobFunctions.AddOrUpdate(ShallowClone.Clone(jobFunction));
                    }
                }
            }

            context.UserAuthors.AddOrUpdate(ShallowClone.Clone(userAuthor));
        }

        private static Guid? FindTextId<TT>(TT entity, PropertyInfo textProp, PropertyInfo[] properties)
            where TT : Identifiable
        {
            if (textProp.GetValue(entity, null) is Text text)
            {
                // directly from the Text property ...
                return text.Id;
            }

            // or from a ForeignKey property referencing the Text property
            var foreignKeyProperty = FindForeignKeyProperty(properties, textProp);
            if (foreignKeyProperty.PropertyType == typeof(Guid))
            {
                return (Guid)foreignKeyProperty.GetValue(entity, null);
            }

            if (foreignKeyProperty.PropertyType == typeof(Guid?))
            {
                return (Guid?)foreignKeyProperty.GetValue(entity, null);
            }

            return null;
        }

        private static PropertyInfo FindForeignKeyProperty(IEnumerable<PropertyInfo> properties, PropertyInfo reference)
        {
            return properties.FirstOrDefault(
                prop => prop.GetCustomAttributes<ForeignKeyAttribute>(false)
                            .Any(fkAttr => fkAttr?.Name == reference.Name));
        }

        private static PropertyInfo FindTranslationProperty(
            IEnumerable<PropertyInfo> properties,
            PropertyInfo reference)
        {
            return properties.FirstOrDefault(
                prop => prop.GetCustomAttributes<TranslationForAttribute>(false)
                            .Any(transAttr => transAttr?.Target == reference.Name));
        }

        private IQueryable<Address> GetAddressesQueryable(EurolookContext context, bool includeSpecialAddressNoAddress)
        {
            return context.Addresses
                          .AsNoTracking()
                          .Where(
                              a => !a.Deleted &&
                                   (includeSpecialAddressNoAddress || a.Id != CommonDataConstants.NoAddressId))
                          .OrderBy(a => a.Name);
        }

        private static List<UserGroup> RetainAdGroupsOfUserGroup(EurolookContext context, List<UserGroup> userGroups)
        {
            // Set AD groups to values on server to prevent an update
            var existingUserGroups = context.UserGroups.AsNoTracking();
            userGroups.ForEach(g => g.AdGroups = existingUserGroups.Where(ug => ug.Id == g.Id).FirstOrDefault()?.AdGroups);
            return userGroups;
        }
    }
}
