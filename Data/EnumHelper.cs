using System;
using System.Collections.Generic;
using Eurolook.Data.Models;

namespace Eurolook.Data
{
    public static class EnumHelper
    {
        public static bool IsPositionMatch(PositionType reference, PositionType compare)
        {
            // Simple Mapping
            if ((reference == PositionType.Begin && compare == PositionType.Begin)
                || (reference == PositionType.Cursor && compare == PositionType.Cursor)
                || (reference == PositionType.End && compare == PositionType.End))
            {
                return true;
            }

            // Header/Footer Mapping
            bool isRefHeader = IsStory(reference, StoryType.Header);
            bool isRefFooter = IsStory(reference, StoryType.Footer);
            bool isCompHeader = IsStory(compare, StoryType.Header);
            bool isCompFooter = IsStory(compare, StoryType.Footer);

            if (isRefHeader && isRefFooter)
            {
                return (isCompHeader || isCompFooter) && IsOccurrenceMatch(reference, compare);
            }

            if (isRefHeader)
            {
                return isCompHeader && IsOccurrenceMatch(reference, compare);
            }

            if (isRefFooter)
            {
                return isCompFooter && IsOccurrenceMatch(reference, compare);
            }

            return false;
        }

        /// <summary>
        /// Checks if the given PositionTypes have corresponding Occurence bits.<br />
        /// "Corresponding" means that they have exactly the same Occurrence bit or one of them has Occurrence.All.
        /// </summary>
        public static bool IsOccurrenceMatch(PositionType pos1, PositionType pos2)
        {
            if (IsOccurrence(pos1, Occurrence.First) && IsOccurrence(pos2, Occurrence.First))
            {
                return true;
            }

            if (IsOccurrence(pos1, Occurrence.Even) && IsOccurrence(pos2, Occurrence.Even))
            {
                return true;
            }

            if (IsOccurrence(pos1, Occurrence.Odd) && IsOccurrence(pos2, Occurrence.Odd))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Demask the StoryType bit from the given PositionType and compares it with the given StoryType.
        /// </summary>
        public static bool IsStory(PositionType pos, StoryType story)
        {
            return ((uint)pos & (uint)story) == (uint)story;
        }

        /// <summary>
        /// Demask the Occurrence bit from the given PositionType and compares it with the given Occurrence.
        /// </summary>
        public static bool IsOccurrence(PositionType pos, Occurrence occ)
        {
            return ((uint)pos & (uint)occ) == (uint)occ;
        }

        public static List<PositionType> GetPositionTypes(StoryType story)
        {
            var result = new List<PositionType>();
            foreach (PositionType val in Enum.GetValues(typeof(PositionType)))
            {
                if (IsStory(val, story))
                {
                    result.Add(val);
                }
            }

            return result;
        }

        public static PositionType GetPosition(StoryType storyType, bool firstPage, bool oddPages, bool evenPages)
        {
            int value = (int)storyType;
            if (storyType == StoryType.Header || storyType == StoryType.Footer)
            {
                if (firstPage)
                {
                    value |= (int)Occurrence.First;
                }

                if (oddPages)
                {
                    value |= (int)Occurrence.Odd;
                }

                if (evenPages)
                {
                    value |= (int)Occurrence.Even;
                }
            }

            var result = (PositionType)value;
            return result;
        }
    }
}
