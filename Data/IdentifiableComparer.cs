﻿using System.Collections.Generic;
using Eurolook.Data.Models;

namespace Eurolook.Data
{
    public class IdentifiableComparer<T> : IEqualityComparer<T>
        where T : Identifiable
    {
        public bool Equals(T x, T y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
            {
                return false;
            }

            return x.Id == y.Id;
        }

        public int GetHashCode(T obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}
