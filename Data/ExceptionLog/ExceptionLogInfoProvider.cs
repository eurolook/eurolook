using System;
using Eurolook.Data.Models;

namespace Eurolook.Data.ExceptionLog
{
    public class ExceptionLogInfoProvider : IExceptionLogInfoProvider
    {
        public DeviceSettings DeviceSettings { get; set; }

        public User User { get; set; }

        public Version ClientVersion { get; set; }

        public Version OfficeVersion { get; set; }
    }
}
