using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Eurolook.Data.ExceptionLog
{
    public class ExceptionLogEntry
    {
        public ExceptionLogEntry()
        {
            CreatedUtc = DateTime.UtcNow;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime CreatedUtc { get; set; }

        [ForeignKey("ExceptionBucket")]
        public byte[] ExceptionBuckedId { get; set; }

        public ExceptionBucket ExceptionBucket { get; set; }

        public Guid OriginatorId { get; set; }

        [StringLength(1024)]
        public string Message { get; set; }

        [StringLength(32)]
        public string ClientVersion { get; set; }

        [StringLength(32)]
        public string OfficeVersion { get; set; }
    }
}
