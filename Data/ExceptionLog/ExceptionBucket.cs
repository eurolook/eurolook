using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Eurolook.Data.ExceptionLog
{
    public class ExceptionBucket
    {
        [Key]
        [Column(TypeName = "binary(20)")]
        [MaxLength(20)]
        public byte[] Id { get; set; }

        [StringLength(256)]
        public string LoggerName { get; set; }

        [StringLength(1024)]
        public string LastMessage { get; set; }

        [StringLength(512)]
        public string ExceptionType { get; set; }

        public string StackTrace { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime CreatedUtc { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime ModifiedUtc { get; set; }

        public bool IsIgnored { get; set; }
    }
}
