using NLog;
using NLog.Config;

namespace Eurolook.Data.ExceptionLog
{
    public static class NLogExceptionLogger
    {
        public static void Register(IExceptionLogInfoProvider exceptionLogInfoProvider)
        {
            var target = new EurolookDatabaseTarget(exceptionLogInfoProvider);

            var loggingRule = new LoggingRule("*", LogLevel.Error, target);

            var configuration = LogManager.Configuration;
            configuration.AddTarget("eurolookDatabase", target);
            configuration.LoggingRules.Add(loggingRule);

            // Assigning the configuration to the logger
            LogManager.Configuration = configuration;
        }
    }
}
