using System;
using Eurolook.Data.Models;

namespace Eurolook.Data.ExceptionLog
{
    public interface IExceptionLogInfoProvider
    {
        DeviceSettings DeviceSettings { get; }

        User User { get; }

        Version ClientVersion { get; }

        Version OfficeVersion { get; }
    }
}
