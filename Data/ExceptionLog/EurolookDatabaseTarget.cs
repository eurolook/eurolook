﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Eurolook.Common.Extensions;
using Microsoft.EntityFrameworkCore;
using NLog;
using NLog.Common;
using NLog.Targets;

namespace Eurolook.Data.ExceptionLog
{
    [Target("EurolookDatabaseTarget")]
    internal sealed class EurolookDatabaseTarget : Target
    {
        private readonly IExceptionLogInfoProvider _exceptionLogInfoProvider;

        public EurolookDatabaseTarget(IExceptionLogInfoProvider exceptionLogInfoProvider)
        {
            _exceptionLogInfoProvider = exceptionLogInfoProvider;
        }

        private Guid OriginatorId
        {
            get { return _exceptionLogInfoProvider?.User?.Id ?? Guid.Empty; }
        }

        private string ClientVersion
        {
            get { return _exceptionLogInfoProvider?.ClientVersion?.ToString() ?? "<unknown client version>"; }
        }

        private string OfficeVersion
        {
            get { return _exceptionLogInfoProvider?.OfficeVersion?.ToString() ?? "<unknown Office version>"; }
        }

        protected override async void Write(AsyncLogEventInfo logEvent)
        {
            if (logEvent.LogEvent == null || logEvent.LogEvent.Level < LogLevel.Error)
            {
                return;
            }

            try
            {
                await WriteToDatabaseAsync(logEvent.LogEvent).ConfigureAwait(false);
                logEvent.Continuation(null);
            }
            catch (Exception ex)
            {
                logEvent.Continuation(ex);
            }
        }

        private static byte[] ComputeBuckedId(string loggerName, string stackTrace)
        {
            string key = $"{loggerName}\t{stackTrace}";

            var buckedId = CreateHash(key);
            return buckedId;
        }
#pragma warning disable S2070 // Use of outdated SHA1
        private static byte[] CreateHash(string input)
        {
            // we still allow SHA1 here, it's still good for our purpose
            // changing to another algorithm requires adjusting the database schema
            using (var sha1 = new SHA1Managed())
            {
                return sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
            }
        }
#pragma warning restore S2070

        private async Task WriteToDatabaseAsync(LogEventInfo logEvent)
        {
            using (var context = new EurolookClientContext(ignoreDeletedFlags: false))
            {
                string loggerName = logEvent.LoggerName ?? "<unknown logger>";
                string exceptionType = logEvent.Exception != null
                    ? logEvent.Exception.GetType().FullName
                    : "<unknown exception>";
                string exceptionToString = logEvent.Exception != null
                    ? logEvent.Exception.ToString().TruncateLongString(4000)
                    : "<no exception info>";
                string message = logEvent.FormattedMessage != null
                    ? logEvent.FormattedMessage.TruncateLongString(1024)
                    : "<no message>";
                string stackTrace = logEvent.Exception?.StackTrace != null
                    ? logEvent.Exception.StackTrace.TruncateLongString(4000)
                    : message.TruncateLongString(50);

                var timeStamp = DateTime.UtcNow;

                var buckedId = ComputeBuckedId(loggerName, stackTrace);

                var exceptionBucket =
                    await context.ExceptionBuckets.FirstOrDefaultAsync(bucket => bucket.Id == buckedId);
                if (exceptionBucket != null)
                {
                    // update existing error bucket
                    // updating the modified date allows us to query all recent buckets
                    exceptionBucket.LastMessage = message;
                    exceptionBucket.ModifiedUtc = timeStamp;
                }
                else
                {
                    exceptionBucket = new ExceptionBucket
                    {
                        Id = buckedId,
                        LoggerName = loggerName.TruncateLongString(256),
                        LastMessage = message,
                        ExceptionType = exceptionType,
                        StackTrace = exceptionToString,
                        CreatedUtc = timeStamp,
                        ModifiedUtc = timeStamp,
                    };
                    context.ExceptionBuckets.Add(exceptionBucket);
                }

                context.ExceptionLogEntries.Add(
                    new ExceptionLogEntry
                    {
                        ExceptionBuckedId = exceptionBucket.Id,
                        Message = message,
                        OriginatorId = OriginatorId,
                        ClientVersion = ClientVersion,
                        OfficeVersion = OfficeVersion,
                        CreatedUtc = timeStamp,
                    });

                // Only keep the last x days in the local database. This is to prevent the local database from growing when
                // there is no successful sync with the Eurolook server
                var threeDaysAgo = DateTime.UtcNow.AddDays(-3);
                var oldBuckets = context.ExceptionBuckets.Where(b => b.CreatedUtc < threeDaysAgo);
                context.ExceptionBuckets.RemoveRange(oldBuckets);

                await context.SaveChangesAsync();
            }
        }
    }
}
