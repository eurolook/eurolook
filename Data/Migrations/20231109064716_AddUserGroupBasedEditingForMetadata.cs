﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Eurolook.Data.Migrations
{
    public partial class AddUserGroupBasedEditingForMetadata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EditableByUserGroups",
                table: "MetadataDefinitions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EditableByUserGroups",
                table: "DocumentModelMetadataDefinitions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EditableByUserGroups",
                table: "MetadataDefinitions");

            migrationBuilder.DropColumn(
                name: "EditableByUserGroups",
                table: "DocumentModelMetadataDefinitions");
        }
    }
}
