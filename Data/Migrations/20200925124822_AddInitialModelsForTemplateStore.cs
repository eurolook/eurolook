using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Eurolook.Data.Migrations
{
    public partial class AddInitialModelsForTemplateStore : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Templates",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    ClientModification = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Tags = table.Column<string>(nullable: true),
                    BaseDocumentModelId = table.Column<Guid>(nullable: true),
                    PreviewPage1 = table.Column<byte[]>(nullable: true),
                    PreviewPage2 = table.Column<byte[]>(nullable: true),
                    IsPublic = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Templates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Templates_DocumentModels_BaseDocumentModelId",
                        column: x => x.BaseDocumentModelId,
                        principalTable: "DocumentModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TemplateFiles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    ClientModification = table.Column<bool>(nullable: false),
                    TemplateId = table.Column<Guid>(nullable: false),
                    LanguageId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TemplateFiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TemplateFiles_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TemplateFiles_Templates_TemplateId",
                        column: x => x.TemplateId,
                        principalTable: "Templates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TemplateOwners",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    TemplateId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TemplateOwners", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TemplateOwners_Templates_TemplateId",
                        column: x => x.TemplateId,
                        principalTable: "Templates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TemplateOwners_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TemplatePublications",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    TemplateId = table.Column<Guid>(nullable: false),
                    OrgaEntityId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TemplatePublications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TemplatePublications_OrgaEntities_OrgaEntityId",
                        column: x => x.OrgaEntityId,
                        principalTable: "OrgaEntities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TemplatePublications_Templates_TemplateId",
                        column: x => x.TemplateId,
                        principalTable: "Templates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TemplateFiles_LanguageId",
                table: "TemplateFiles",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_TemplateLanguageUniqueness",
                table: "TemplateFiles",
                columns: new[] { "TemplateId", "LanguageId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TemplateOwners_UserId",
                table: "TemplateOwners",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_TemplateUserUniqueness",
                table: "TemplateOwners",
                columns: new[] { "TemplateId", "UserId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TemplatePublications_OrgaEntityId",
                table: "TemplatePublications",
                column: "OrgaEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_TemplateOrgaEntityUniqueness",
                table: "TemplatePublications",
                columns: new[] { "TemplateId", "OrgaEntityId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Templates_BaseDocumentModelId",
                table: "Templates",
                column: "BaseDocumentModelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TemplateFiles");

            migrationBuilder.DropTable(
                name: "TemplateOwners");

            migrationBuilder.DropTable(
                name: "TemplatePublications");

            migrationBuilder.DropTable(
                name: "Templates");
        }
    }
}
