using Microsoft.EntityFrameworkCore.Migrations;

namespace Eurolook.Data.Migrations
{
    public partial class AddFileNameDateToResources : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FileName",
                table: "Resources",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FileNameDate",
                table: "LocalisedResources",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FileName",
                table: "Resources");

            migrationBuilder.DropColumn(
                name: "FileNameDate",
                table: "LocalisedResources");
        }
    }
}
