﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Eurolook.Data.Migrations
{
    public partial class AddVectorIconColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "VectorIcon",
                table: "Bricks",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "VectorIcon",
                table: "BrickGroups",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "VectorIcon",
                table: "Bricks");

            migrationBuilder.DropColumn(
                name: "VectorIcon",
                table: "BrickGroups");
        }
    }
}
