using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Eurolook.Data.Migrations
{
    public partial class AddResourceDataFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MimeType",
                table: "Resources",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RawData",
                table: "Resources",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MimeType",
                table: "Resources");

            migrationBuilder.DropColumn(
                name: "RawData",
                table: "Resources");
        }
    }
}
