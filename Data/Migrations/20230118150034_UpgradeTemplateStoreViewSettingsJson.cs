﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Eurolook.Data.Migrations
{
    public partial class UpgradeTemplateStoreViewSettingsJson : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            string prefix = "{\"ViewSettings\":";
            string suffix = ",\"QuickFilters\": []}";
            string startsWithBracketPattern = "[[]%";
            migrationBuilder.Sql($"Update UserSettings SET TemplateStoreViewSettings = '{prefix}' + TemplateStoreViewSettings + '{suffix}' WHERE (TemplateStoreViewSettings like '{startsWithBracketPattern}') ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
