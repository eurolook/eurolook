﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Eurolook.Data.Migrations
{
    public partial class AddTableForSystemText : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SystemTexts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(type: "DateTime2", nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    TextId = table.Column<Guid>(nullable: true),
                    Comment = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SystemTexts", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SystemTexts");
        }
    }
}
