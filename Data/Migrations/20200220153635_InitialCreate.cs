﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Eurolook.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AuthorEditRequests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Token = table.Column<string>(nullable: true),
                    CreatedUtc = table.Column<DateTime>(nullable: false),
                    ExpiredUtc = table.Column<DateTime>(nullable: true),
                    OriginatorEmail = table.Column<string>(nullable: true),
                    OriginatorName = table.Column<string>(nullable: true),
                    AddresseeEmail = table.Column<string>(nullable: true),
                    AddresseeName = table.Column<string>(nullable: true),
                    IsApproved = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthorEditRequests", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AuthorRoles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 200, nullable: true),
                    DisplayName = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    UiPositionIndex = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthorRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BrickCategories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    UiPositionIndex = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BrickCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BrickGroups",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    ClientModification = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    DisplayName = table.Column<string>(nullable: true),
                    Icon = table.Column<byte[]>(nullable: true),
                    UiPositionIndex = table.Column<int>(nullable: false),
                    Acronym = table.Column<string>(nullable: true),
                    ColorLuminance = table.Column<int>(nullable: false),
                    IsMultipleChoice = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BrickGroups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CharacterMappings",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    Original = table.Column<string>(nullable: true),
                    Mapped = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterMappings", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ColorSchemes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    PrimaryColor = table.Column<string>(nullable: true),
                    SecondaryColor = table.Column<string>(nullable: true),
                    TileColor1 = table.Column<string>(nullable: true),
                    TileColor2 = table.Column<string>(nullable: true),
                    TileColor3 = table.Column<string>(nullable: true),
                    TileColor1Luminances = table.Column<string>(nullable: true),
                    TileColor2Luminances = table.Column<string>(nullable: true),
                    TileColor3Luminances = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ColorSchemes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DocumentCategories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ExceptionBuckets",
                columns: table => new
                {
                    Id = table.Column<byte[]>(type: "binary(20)", maxLength: 20, nullable: false),
                    LoggerName = table.Column<string>(maxLength: 256, nullable: true),
                    LastMessage = table.Column<string>(maxLength: 1024, nullable: true),
                    ExceptionType = table.Column<string>(maxLength: 512, nullable: true),
                    StackTrace = table.Column<string>(nullable: true),
                    CreatedUtc = table.Column<DateTime>(nullable: false),
                    ModifiedUtc = table.Column<DateTime>(nullable: false),
                    IsIgnored = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExceptionBuckets", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Languages",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Locale = table.Column<string>(nullable: true),
                    DateFormatShort = table.Column<string>(nullable: true),
                    DateFormatLong = table.Column<string>(nullable: true),
                    ArticleText = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Languages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MetadataCategories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 200, nullable: true),
                    Description = table.Column<string>(nullable: true),
                    DisplayName = table.Column<string>(nullable: true),
                    UiPositionIndex = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MetadataCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Notifications",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Content = table.Column<string>(nullable: true),
                    Link = table.Column<string>(nullable: true),
                    ThumbnailBytes = table.Column<byte[]>(nullable: true),
                    IsHidden = table.Column<bool>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false),
                    LinkText = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notifications", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PersonNames",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    PoliticalGroup = table.Column<string>(nullable: true),
                    Category = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonNames", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Resources",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    Alias = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Resources", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StyleShortcuts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    Shortcut = table.Column<string>(nullable: true),
                    StyleName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StyleShortcuts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TermStoreImports",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    TermStoreXmlFile = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TermStoreImports", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Texts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    Alias = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Texts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserGroups",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    Description = table.Column<string>(nullable: true),
                    UiPositionIndex = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserGroups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Bricks",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    ClientModification = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    DisplayName = table.Column<string>(nullable: true),
                    Icon = table.Column<byte[]>(nullable: true),
                    UiPositionIndex = table.Column<int>(nullable: false),
                    Acronym = table.Column<string>(nullable: true),
                    ColorLuminance = table.Column<int>(nullable: false),
                    CategoryId = table.Column<Guid>(nullable: false),
                    GroupId = table.Column<Guid>(nullable: true),
                    HotKey = table.Column<string>(nullable: true),
                    IsHidden = table.Column<bool>(nullable: false),
                    VisibleToUserGroups = table.Column<string>(nullable: false),
                    IsMultiInstance = table.Column<bool>(nullable: false),
                    HasCustomUi = table.Column<bool>(nullable: false),
                    MinVersion = table.Column<string>(nullable: true),
                    MaxVersion = table.Column<string>(nullable: true),
                    Discriminator = table.Column<string>(nullable: false),
                    BuildingBlockBytes = table.Column<byte[]>(nullable: true),
                    PreviewImageBytes = table.Column<byte[]>(nullable: true),
                    CommandClass = table.Column<string>(nullable: true),
                    Argument1 = table.Column<string>(nullable: true),
                    Configuration = table.Column<string>(nullable: true),
                    Content = table.Column<string>(nullable: true),
                    ContentBrickActionsClassName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bricks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Bricks_BrickCategories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "BrickCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Bricks_BrickGroups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "BrickGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserSettings",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    ClientModification = table.Column<bool>(nullable: false),
                    LastAuthorId = table.Column<Guid>(nullable: true),
                    LastDocumentModelId = table.Column<Guid>(nullable: true),
                    LastDocumentLanguageId = table.Column<Guid>(nullable: true),
                    LastJobAssignmentId = table.Column<Guid>(nullable: true),
                    DataSyncIntervalMinutes = table.Column<int>(nullable: false),
                    DataSyncDelayMinutes = table.Column<int>(nullable: false),
                    ColorSchemeId = table.Column<Guid>(nullable: true),
                    IsCepEnabled = table.Column<bool>(nullable: false),
                    IsInsertNameInline = table.Column<bool>(nullable: false),
                    IsStyleBoxDisabled = table.Column<bool>(nullable: false),
                    ContactOptions = table.Column<int>(nullable: false),
                    FavoriteLanguageIds = table.Column<string>(nullable: true),
                    IsFavoriteLanguageListEnabled = table.Column<bool>(nullable: false),
                    LastMarkingsSettings = table.Column<string>(nullable: true),
                    HideMarkingsDialog = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserSettings_ColorSchemes_ColorSchemeId",
                        column: x => x.ColorSchemeId,
                        principalTable: "ColorSchemes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ExceptionLogEntries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedUtc = table.Column<DateTime>(nullable: false),
                    ExceptionBuckedId = table.Column<byte[]>(nullable: true),
                    OriginatorId = table.Column<Guid>(nullable: false),
                    Message = table.Column<string>(maxLength: 1024, nullable: true),
                    ClientVersion = table.Column<string>(maxLength: 32, nullable: true),
                    OfficeVersion = table.Column<string>(maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExceptionLogEntries", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExceptionLogEntries_ExceptionBuckets_ExceptionBuckedId",
                        column: x => x.ExceptionBuckedId,
                        principalTable: "ExceptionBuckets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MetadataDefinitions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 200, nullable: true),
                    DisplayName = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    MinLength = table.Column<int>(nullable: true),
                    MaxLength = table.Column<int>(nullable: true),
                    Position = table.Column<int>(nullable: false),
                    IsReadOnly = table.Column<bool>(nullable: false),
                    IsHidden = table.Column<bool>(nullable: false),
                    IsMandatory = table.Column<bool>(nullable: false),
                    MetadataType = table.Column<int>(nullable: false),
                    TermSetId = table.Column<Guid>(nullable: true),
                    MetadataCategoryId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MetadataDefinitions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MetadataDefinitions_MetadataCategories_MetadataCategoryId",
                        column: x => x.MetadataCategoryId,
                        principalTable: "MetadataCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LocalisedResources",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    ResourceId = table.Column<Guid>(nullable: false),
                    LanguageId = table.Column<Guid>(nullable: false),
                    RawData = table.Column<byte[]>(nullable: true),
                    MimeType = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocalisedResources", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LocalisedResources_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LocalisedResources_Resources_ResourceId",
                        column: x => x.ResourceId,
                        principalTable: "Resources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Addresses",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    PhoneNumberPrefix = table.Column<string>(nullable: true),
                    LocationTextId = table.Column<Guid>(nullable: true),
                    FooterTextId = table.Column<Guid>(nullable: true),
                    IsDefault = table.Column<bool>(nullable: true),
                    ActiveDirectoryReference = table.Column<string>(nullable: true),
                    NameTextId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Addresses_Texts_FooterTextId",
                        column: x => x.FooterTextId,
                        principalTable: "Texts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Addresses_Texts_LocationTextId",
                        column: x => x.LocationTextId,
                        principalTable: "Texts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Addresses_Texts_NameTextId",
                        column: x => x.NameTextId,
                        principalTable: "Texts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PredefinedFunctions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    FunctionTextId = table.Column<Guid>(nullable: true),
                    FunctionFemaleTextId = table.Column<Guid>(nullable: true),
                    ShowInSignature = table.Column<bool>(nullable: false),
                    ShowInHeader = table.Column<bool>(nullable: false),
                    AdReference = table.Column<string>(nullable: true),
                    FunctionHeaderTextId = table.Column<Guid>(nullable: true),
                    FunctionHeaderFemaleTextId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PredefinedFunctions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PredefinedFunctions_Texts_FunctionFemaleTextId",
                        column: x => x.FunctionFemaleTextId,
                        principalTable: "Texts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PredefinedFunctions_Texts_FunctionHeaderFemaleTextId",
                        column: x => x.FunctionHeaderFemaleTextId,
                        principalTable: "Texts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PredefinedFunctions_Texts_FunctionHeaderTextId",
                        column: x => x.FunctionHeaderTextId,
                        principalTable: "Texts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PredefinedFunctions_Texts_FunctionTextId",
                        column: x => x.FunctionTextId,
                        principalTable: "Texts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Translations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    TextId = table.Column<Guid>(nullable: false),
                    LanguageId = table.Column<Guid>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Translations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Translations_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Translations_Texts_TextId",
                        column: x => x.TextId,
                        principalTable: "Texts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BrickResources",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    BrickId = table.Column<Guid>(nullable: false),
                    ResourceId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BrickResources", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BrickResources_Bricks_BrickId",
                        column: x => x.BrickId,
                        principalTable: "Bricks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BrickResources_Resources_ResourceId",
                        column: x => x.ResourceId,
                        principalTable: "Resources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BrickTexts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    BrickId = table.Column<Guid>(nullable: false),
                    TextId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BrickTexts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BrickTexts_Bricks_BrickId",
                        column: x => x.BrickId,
                        principalTable: "Bricks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BrickTexts_Texts_TextId",
                        column: x => x.TextId,
                        principalTable: "Texts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BrickSettings",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    ClientModification = table.Column<bool>(nullable: false),
                    UserSettingsId = table.Column<Guid>(nullable: false),
                    BrickId = table.Column<Guid>(nullable: true),
                    SettingsJson = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BrickSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BrickSettings_Bricks_BrickId",
                        column: x => x.BrickId,
                        principalTable: "Bricks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BrickSettings_UserSettings_UserSettingsId",
                        column: x => x.UserSettingsId,
                        principalTable: "UserSettings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DeviceSettings",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    ClientModification = table.Column<bool>(nullable: false),
                    UserSettingsId = table.Column<Guid>(nullable: false),
                    DeviceName = table.Column<string>(nullable: true),
                    OperatingSystem = table.Column<string>(nullable: true),
                    IsTouchScreen = table.Column<bool>(nullable: false),
                    HasBattery = table.Column<bool>(nullable: false),
                    ScreenWidth = table.Column<int>(nullable: false),
                    ScreenHeight = table.Column<int>(nullable: false),
                    ScreenDpi = table.Column<double>(nullable: false),
                    PhysicallyInstalledSystemMemoryKb = table.Column<long>(nullable: false),
                    ProcessorName = table.Column<string>(nullable: true),
                    DotNetRuntimeVersion = table.Column<string>(nullable: true),
                    DotNetFullFrameworkReleaseVersion = table.Column<int>(nullable: true),
                    AddinVersion = table.Column<string>(nullable: true),
                    OfficeVersion = table.Column<string>(nullable: true),
                    InstallRoot = table.Column<string>(nullable: true),
                    IsPerUserInstall = table.Column<bool>(nullable: false),
                    IsPerMachineInstall = table.Column<bool>(nullable: false),
                    AddInLoadTimesMilliseconds = table.Column<string>(nullable: true),
                    MaxAddInLoadTimeBucketSeconds = table.Column<float>(nullable: false),
                    Owner = table.Column<string>(nullable: true),
                    IsInitialised = table.Column<bool>(nullable: false),
                    LastLogin = table.Column<DateTime>(nullable: true),
                    TaskpaneExpansionState = table.Column<string>(nullable: true),
                    IsTaskpaneVisible = table.Column<bool>(nullable: true),
                    TaskpaneWidth = table.Column<int>(nullable: true),
                    IsRemoteWipeRequested = table.Column<bool>(nullable: false),
                    QuickStartGuideShown = table.Column<string>(nullable: true),
                    ActivityTrack = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeviceSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DeviceSettings_UserSettings_UserSettingsId",
                        column: x => x.UserSettingsId,
                        principalTable: "UserSettings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OrgaEntities",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    SuperEntityId = table.Column<Guid>(nullable: true),
                    PrimaryAddressId = table.Column<Guid>(nullable: true),
                    SecondaryAddressId = table.Column<Guid>(nullable: true),
                    LogicalLevel = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    HeaderTextId = table.Column<Guid>(nullable: true),
                    WebAddress = table.Column<string>(nullable: true),
                    OrderIndex = table.Column<int>(nullable: false),
                    ShowInHeader = table.Column<bool>(nullable: false),
                    Function = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    RepositoryReference = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrgaEntities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrgaEntities_Texts_HeaderTextId",
                        column: x => x.HeaderTextId,
                        principalTable: "Texts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrgaEntities_Addresses_PrimaryAddressId",
                        column: x => x.PrimaryAddressId,
                        principalTable: "Addresses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrgaEntities_Addresses_SecondaryAddressId",
                        column: x => x.SecondaryAddressId,
                        principalTable: "Addresses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrgaEntities_OrgaEntities_SuperEntityId",
                        column: x => x.SuperEntityId,
                        principalTable: "OrgaEntities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ActivityTracks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateUtc = table.Column<DateTime>(nullable: false),
                    OrgaEntityId = table.Column<Guid>(nullable: false),
                    SessionCount = table.Column<int>(nullable: false),
                    DeviceCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityTracks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ActivityTracks_OrgaEntities_OrgaEntityId",
                        column: x => x.OrgaEntityId,
                        principalTable: "OrgaEntities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Authors",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    ClientModification = table.Column<bool>(nullable: false),
                    BulgarianFirstName = table.Column<string>(nullable: true),
                    BulgarianLastName = table.Column<string>(nullable: true),
                    GreekFirstName = table.Column<string>(nullable: true),
                    GreekLastName = table.Column<string>(nullable: true),
                    LatinFirstName = table.Column<string>(nullable: true),
                    LatinLastName = table.Column<string>(nullable: true),
                    Service = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Initials = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    SharePointAccountId = table.Column<int>(nullable: true),
                    SharePointLoginName = table.Column<string>(nullable: true),
                    WebAddress = table.Column<string>(nullable: true),
                    OrgaEntityId = table.Column<Guid>(nullable: true),
                    PredefinedFunctionId = table.Column<Guid>(nullable: true),
                    MainWorkplaceId = table.Column<Guid>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    PendingADChange = table.Column<bool>(nullable: false),
                    AD_LatinFirstName = table.Column<string>(nullable: true),
                    AD_LatinFirstName_Pending = table.Column<string>(nullable: true),
                    AD_LatinLastName = table.Column<string>(nullable: true),
                    AD_LatinLastName_Pending = table.Column<string>(nullable: true),
                    AD_Initials = table.Column<string>(nullable: true),
                    AD_Initials_Pending = table.Column<string>(nullable: true),
                    AD_Service = table.Column<string>(nullable: true),
                    AD_Service_Pending = table.Column<string>(nullable: true),
                    AD_Description = table.Column<string>(nullable: true),
                    AD_Description_Pending = table.Column<string>(nullable: true),
                    AD_Email = table.Column<string>(nullable: true),
                    AD_Email_Pending = table.Column<string>(nullable: true),
                    AD_WebAddress = table.Column<string>(nullable: true),
                    AD_WebAddress_Pending = table.Column<string>(nullable: true),
                    AD_Gender = table.Column<string>(nullable: true),
                    AD_Gender_Pending = table.Column<string>(nullable: true),
                    AD_Office = table.Column<string>(nullable: true),
                    AD_Office_Pending = table.Column<string>(nullable: true),
                    AD_PhoneExtension = table.Column<string>(nullable: true),
                    AD_PhoneExtension_Pending = table.Column<string>(nullable: true),
                    AD_FaxExtension = table.Column<string>(nullable: true),
                    AD_FaxExtension_Pending = table.Column<string>(nullable: true),
                    AD_Address = table.Column<string>(nullable: true),
                    AD_Address_Pending = table.Column<string>(nullable: true),
                    AD_PredefinedFunction = table.Column<Guid>(nullable: true),
                    AD_PredefinedFunctionId_Pending = table.Column<Guid>(nullable: true),
                    AD_OrgaEntityId = table.Column<Guid>(nullable: true),
                    AD_OrgaEntityId_Pending = table.Column<Guid>(nullable: true),
                    LastActiveDirectorySyncTimeUtc = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Authors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Authors_OrgaEntities_OrgaEntityId",
                        column: x => x.OrgaEntityId,
                        principalTable: "OrgaEntities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Authors_PredefinedFunctions_PredefinedFunctionId",
                        column: x => x.PredefinedFunctionId,
                        principalTable: "PredefinedFunctions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DocumentModels",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    DisplayName = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Keywords = table.Column<string>(nullable: true),
                    OwnerId = table.Column<Guid>(nullable: true),
                    PreviewImage = table.Column<byte[]>(nullable: true),
                    PreviewImageFileName = table.Column<string>(nullable: true),
                    PreviewImageWithSampleText = table.Column<byte[]>(nullable: true),
                    PreviewImageWithSampleTextFileName = table.Column<string>(nullable: true),
                    Template = table.Column<byte[]>(nullable: true),
                    TemplateFileName = table.Column<string>(nullable: true),
                    EL4DocType = table.Column<string>(nullable: true),
                    DocumentCategoryId = table.Column<Guid>(nullable: true),
                    IsClassicDocument = table.Column<bool>(nullable: true),
                    IsHidden = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentModels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DocumentModels_DocumentCategories_DocumentCategoryId",
                        column: x => x.DocumentCategoryId,
                        principalTable: "DocumentCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DocumentModels_OrgaEntities_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "OrgaEntities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "JobAssignments",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    ClientModification = table.Column<bool>(nullable: false),
                    OrgaEntityId = table.Column<Guid>(nullable: true),
                    Service = table.Column<string>(nullable: true),
                    WebAddress = table.Column<string>(nullable: true),
                    AuthorId = table.Column<Guid>(nullable: false),
                    PredefinedFunctionId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobAssignments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JobAssignments_Authors_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Authors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_JobAssignments_OrgaEntities_OrgaEntityId",
                        column: x => x.OrgaEntityId,
                        principalTable: "OrgaEntities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_JobAssignments_PredefinedFunctions_PredefinedFunctionId",
                        column: x => x.PredefinedFunctionId,
                        principalTable: "PredefinedFunctions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    ClientModification = table.Column<bool>(nullable: false),
                    Login = table.Column<string>(maxLength: 200, nullable: true),
                    SelfId = table.Column<Guid>(nullable: false),
                    SettingsId = table.Column<Guid>(nullable: false),
                    IsElevated = table.Column<bool>(nullable: false),
                    UserGroupId = table.Column<Guid>(nullable: false),
                    UserGroupIds = table.Column<string>(nullable: true),
                    CreatedTimeUtc = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Authors_SelfId",
                        column: x => x.SelfId,
                        principalTable: "Authors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_UserSettings_SettingsId",
                        column: x => x.SettingsId,
                        principalTable: "UserSettings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Workplaces",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    ClientModification = table.Column<bool>(nullable: false),
                    AuthorId = table.Column<Guid>(nullable: false),
                    AddressId = table.Column<Guid>(nullable: false),
                    PhoneExtension = table.Column<string>(nullable: true),
                    FaxExtension = table.Column<string>(nullable: true),
                    Office = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Workplaces", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Workplaces_Addresses_AddressId",
                        column: x => x.AddressId,
                        principalTable: "Addresses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Workplaces_Authors_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Authors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ActionLogs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Action = table.Column<string>(nullable: true),
                    OriginatorId = table.Column<Guid>(nullable: true),
                    DocumentModelId = table.Column<Guid>(nullable: true),
                    Info = table.Column<string>(nullable: true),
                    CreatedUtc = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActionLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ActionLogs_DocumentModels_DocumentModelId",
                        column: x => x.DocumentModelId,
                        principalTable: "DocumentModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DocumentModelAuthorRoles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    DocumentModelId = table.Column<Guid>(nullable: false),
                    AuthorRoleId = table.Column<Guid>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentModelAuthorRoles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DocumentModelAuthorRoles_AuthorRoles_AuthorRoleId",
                        column: x => x.AuthorRoleId,
                        principalTable: "AuthorRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DocumentModelAuthorRoles_DocumentModels_DocumentModelId",
                        column: x => x.DocumentModelId,
                        principalTable: "DocumentModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DocumentModelLanguages",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    DocumentModelId = table.Column<Guid>(nullable: false),
                    DocumentLanguageId = table.Column<Guid>(nullable: false),
                    Template = table.Column<byte[]>(nullable: true),
                    TemplateFileName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentModelLanguages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DocumentModelLanguages_Languages_DocumentLanguageId",
                        column: x => x.DocumentLanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DocumentModelLanguages_DocumentModels_DocumentModelId",
                        column: x => x.DocumentModelId,
                        principalTable: "DocumentModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DocumentModelMetadataDefinitions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    DocumentModelId = table.Column<Guid>(nullable: false),
                    MetadataDefinitionId = table.Column<Guid>(nullable: false),
                    DefaultValue = table.Column<string>(nullable: true),
                    IsReadOnlyOverride = table.Column<bool>(nullable: true),
                    MinLengthOverride = table.Column<int>(nullable: true),
                    MaxLengthOverride = table.Column<int>(nullable: true),
                    IsHiddenOverride = table.Column<bool>(nullable: true),
                    IsMandatoryOverride = table.Column<bool>(nullable: true),
                    CalculationCommandClassName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentModelMetadataDefinitions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DocumentModelMetadataDefinitions_DocumentModels_DocumentModelId",
                        column: x => x.DocumentModelId,
                        principalTable: "DocumentModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DocumentModelMetadataDefinitions_MetadataDefinitions_MetadataDefinitionId",
                        column: x => x.MetadataDefinitionId,
                        principalTable: "MetadataDefinitions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DocumentModelSettings",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    ClientModification = table.Column<bool>(nullable: false),
                    UserSettingsId = table.Column<Guid>(nullable: false),
                    DocumentModelId = table.Column<Guid>(nullable: false),
                    InsertRichBody = table.Column<bool>(nullable: false),
                    UserBrickChoices = table.Column<string>(nullable: true),
                    LastUsedAuthorRoles = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentModelSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DocumentModelSettings_DocumentModels_DocumentModelId",
                        column: x => x.DocumentModelId,
                        principalTable: "DocumentModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DocumentModelSettings_UserSettings_UserSettingsId",
                        column: x => x.UserSettingsId,
                        principalTable: "UserSettings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DocumentStructure",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    DocumentModelId = table.Column<Guid>(nullable: false),
                    BrickId = table.Column<Guid>(nullable: false),
                    Position = table.Column<int>(nullable: false),
                    VerticalPositionIndex = table.Column<int>(nullable: false),
                    SectionId = table.Column<int>(nullable: false),
                    AutoBrickCreationType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentStructure", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DocumentStructure_Bricks_BrickId",
                        column: x => x.BrickId,
                        principalTable: "Bricks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DocumentStructure_DocumentModels_DocumentModelId",
                        column: x => x.DocumentModelId,
                        principalTable: "DocumentModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "JobFunctions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    ClientModification = table.Column<bool>(nullable: false),
                    JobAssignmentId = table.Column<Guid>(nullable: true),
                    AuthorId = table.Column<Guid>(nullable: true),
                    LanguageId = table.Column<Guid>(nullable: false),
                    Function = table.Column<string>(nullable: true),
                    AD_Function = table.Column<string>(nullable: true),
                    AD_Function_Pending = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobFunctions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JobFunctions_Authors_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Authors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_JobFunctions_JobAssignments_JobAssignmentId",
                        column: x => x.JobAssignmentId,
                        principalTable: "JobAssignments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_JobFunctions_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserAuthors",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    ClientModification = table.Column<bool>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    AuthorId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserAuthors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserAuthors_Authors_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Authors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserAuthors_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserBricks",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    ClientModification = table.Column<bool>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    BrickId = table.Column<Guid>(nullable: false),
                    DocumentModelId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserBricks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserBricks_Bricks_BrickId",
                        column: x => x.BrickId,
                        principalTable: "Bricks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserBricks_DocumentModels_DocumentModelId",
                        column: x => x.DocumentModelId,
                        principalTable: "DocumentModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserBricks_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserFeedbacks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<Guid>(nullable: false),
                    Submitted = table.Column<DateTime>(nullable: false),
                    Version = table.Column<string>(nullable: true),
                    Rating = table.Column<byte>(nullable: false),
                    Message = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserFeedbacks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserFeedbacks_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "AuthorRoles",
                columns: new[] { "Id", "Deleted", "Description", "DisplayName", "Name", "ServerModificationTimeUtc", "UiPositionIndex" },
                values: new object[] { new Guid("a4fbaff4-b07c-48b4-a21e-e7b9eedf3796"), false, null, null, "Creator", new DateTime(2020, 2, 20, 15, 36, 34, 640, DateTimeKind.Utc).AddTicks(735), 0 });

            migrationBuilder.InsertData(
                table: "BrickCategories",
                columns: new[] { "Id", "Deleted", "Description", "Name", "ServerModificationTimeUtc", "UiPositionIndex" },
                values: new object[] { new Guid("3a32df8c-4813-412e-a351-5c67157c5d89"), false, null, "Tools", new DateTime(2020, 2, 20, 15, 36, 34, 634, DateTimeKind.Utc).AddTicks(898), 401 });

            migrationBuilder.InsertData(
                table: "DocumentModels",
                columns: new[] { "Id", "Deleted", "Description", "DisplayName", "DocumentCategoryId", "EL4DocType", "IsClassicDocument", "IsHidden", "Keywords", "Name", "OwnerId", "PreviewImage", "PreviewImageFileName", "PreviewImageWithSampleText", "PreviewImageWithSampleTextFileName", "ServerModificationTimeUtc", "Template", "TemplateFileName" },
                values: new object[] { new Guid("c7ca1146-07f3-41b1-bcb2-d5213d29a28d"), false, "Document model used for documents that are not recognized as Eurolook documents", "Standard Word Document", null, null, false, true, null, "Standard Word Document", null, null, null, null, null, new DateTime(2020, 2, 20, 15, 36, 34, 638, DateTimeKind.Utc).AddTicks(792), null, null });

            migrationBuilder.InsertData(
                table: "OrgaEntities",
                columns: new[] { "Id", "Deleted", "Function", "Gender", "HeaderTextId", "LogicalLevel", "Name", "OrderIndex", "PrimaryAddressId", "RepositoryReference", "SecondaryAddressId", "ServerModificationTimeUtc", "ShowInHeader", "SuperEntityId", "WebAddress" },
                values: new object[] { new Guid("5655049f-e175-4ff8-acf5-93833d66b82d"), false, null, null, null, 0, null, 0, null, null, null, new DateTime(2020, 2, 20, 15, 36, 34, 638, DateTimeKind.Utc).AddTicks(792), false, null, null });

            migrationBuilder.InsertData(
                table: "UserGroups",
                columns: new[] { "Id", "Deleted", "Description", "Name", "ServerModificationTimeUtc", "UiPositionIndex" },
                values: new object[,]
                {
                    { new Guid("c590a59f-5231-46bd-b4c3-2eaa804f1a7a"), false, "The default user group. Everyone is member of this group.", "Users", new DateTime(2020, 2, 20, 15, 36, 34, 641, DateTimeKind.Utc).AddTicks(711), 10 },
                    { new Guid("345406cc-dfa8-4f0f-a9ca-984b64977e71"), false, "Members can switch between environments.", "Contributors", new DateTime(2020, 2, 20, 15, 36, 34, 641, DateTimeKind.Utc).AddTicks(711), 20 },
                    { new Guid("dabaee97-c3f6-4a8c-87a0-20d528481819"), false, "Members can switch between environments and see all bricks that are not explicitly hidden.", "Administrators", new DateTime(2020, 2, 20, 15, 36, 34, 641, DateTimeKind.Utc).AddTicks(711), 50 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActionLogs_DocumentModelId",
                table: "ActionLogs",
                column: "DocumentModelId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityTracks_OrgaEntityId",
                table: "ActivityTracks",
                column: "OrgaEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_FooterTextId",
                table: "Addresses",
                column: "FooterTextId");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_LocationTextId",
                table: "Addresses",
                column: "LocationTextId");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_Name",
                table: "Addresses",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_NameTextId",
                table: "Addresses",
                column: "NameTextId");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorRoles_Name",
                table: "AuthorRoles",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Authors_OrgaEntityId",
                table: "Authors",
                column: "OrgaEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_Authors_PredefinedFunctionId",
                table: "Authors",
                column: "PredefinedFunctionId");

            migrationBuilder.CreateIndex(
                name: "IX_BrickResources_ResourceId",
                table: "BrickResources",
                column: "ResourceId");

            migrationBuilder.CreateIndex(
                name: "IX_BrickResourceUniqueness",
                table: "BrickResources",
                columns: new[] { "BrickId", "ResourceId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Bricks_CategoryId",
                table: "Bricks",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Bricks_GroupId",
                table: "Bricks",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_BrickSettings_BrickId",
                table: "BrickSettings",
                column: "BrickId");

            migrationBuilder.CreateIndex(
                name: "IX_BrickSettings_UserSettingsId",
                table: "BrickSettings",
                column: "UserSettingsId");

            migrationBuilder.CreateIndex(
                name: "IX_BrickTexts_TextId",
                table: "BrickTexts",
                column: "TextId");

            migrationBuilder.CreateIndex(
                name: "IX_BrickTextUniqueness",
                table: "BrickTexts",
                columns: new[] { "BrickId", "TextId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DeviceSettings_UserSettingsId",
                table: "DeviceSettings",
                column: "UserSettingsId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentModelAuthorRoles_AuthorRoleId",
                table: "DocumentModelAuthorRoles",
                column: "AuthorRoleId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentModelAuthorRoles_DocumentModelId",
                table: "DocumentModelAuthorRoles",
                column: "DocumentModelId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentModelLanguages_DocumentLanguageId",
                table: "DocumentModelLanguages",
                column: "DocumentLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentModelDocumentLanguageUniqueness",
                table: "DocumentModelLanguages",
                columns: new[] { "DocumentModelId", "DocumentLanguageId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DocumentModelMetadataDefinitions_DocumentModelId",
                table: "DocumentModelMetadataDefinitions",
                column: "DocumentModelId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentModelMetadataDefinitions_MetadataDefinitionId",
                table: "DocumentModelMetadataDefinitions",
                column: "MetadataDefinitionId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentModels_DocumentCategoryId",
                table: "DocumentModels",
                column: "DocumentCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentModels_OwnerId",
                table: "DocumentModels",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentModelSettings_DocumentModelId",
                table: "DocumentModelSettings",
                column: "DocumentModelId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentModelSettings_UserSettingsId",
                table: "DocumentModelSettings",
                column: "UserSettingsId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentStructure_BrickId",
                table: "DocumentStructure",
                column: "BrickId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentModelBrickPositionSectionUniqueness",
                table: "DocumentStructure",
                columns: new[] { "DocumentModelId", "BrickId", "Position", "SectionId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ExceptionLogEntries_ExceptionBuckedId",
                table: "ExceptionLogEntries",
                column: "ExceptionBuckedId");

            migrationBuilder.CreateIndex(
                name: "IX_JobAssignments_AuthorId",
                table: "JobAssignments",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_JobAssignments_OrgaEntityId",
                table: "JobAssignments",
                column: "OrgaEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_JobAssignments_PredefinedFunctionId",
                table: "JobAssignments",
                column: "PredefinedFunctionId");

            migrationBuilder.CreateIndex(
                name: "IX_JobFunctions_AuthorId",
                table: "JobFunctions",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_JobFunctions_JobAssignmentId",
                table: "JobFunctions",
                column: "JobAssignmentId");

            migrationBuilder.CreateIndex(
                name: "IX_JobFunctions_LanguageId",
                table: "JobFunctions",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_LocalisedResources_LanguageId",
                table: "LocalisedResources",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_ResourceLanguageUniqueness",
                table: "LocalisedResources",
                columns: new[] { "ResourceId", "LanguageId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MetadataCategories_Name",
                table: "MetadataCategories",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_MetadataDefinitions_MetadataCategoryId",
                table: "MetadataDefinitions",
                column: "MetadataCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_MetadataDefinitions_Name",
                table: "MetadataDefinitions",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_OrgaEntities_HeaderTextId",
                table: "OrgaEntities",
                column: "HeaderTextId");

            migrationBuilder.CreateIndex(
                name: "IX_OrgaEntities_PrimaryAddressId",
                table: "OrgaEntities",
                column: "PrimaryAddressId");

            migrationBuilder.CreateIndex(
                name: "IX_OrgaEntities_SecondaryAddressId",
                table: "OrgaEntities",
                column: "SecondaryAddressId");

            migrationBuilder.CreateIndex(
                name: "IX_OrgaEntities_SuperEntityId",
                table: "OrgaEntities",
                column: "SuperEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_PredefinedFunctions_FunctionFemaleTextId",
                table: "PredefinedFunctions",
                column: "FunctionFemaleTextId");

            migrationBuilder.CreateIndex(
                name: "IX_PredefinedFunctions_FunctionHeaderFemaleTextId",
                table: "PredefinedFunctions",
                column: "FunctionHeaderFemaleTextId");

            migrationBuilder.CreateIndex(
                name: "IX_PredefinedFunctions_FunctionHeaderTextId",
                table: "PredefinedFunctions",
                column: "FunctionHeaderTextId");

            migrationBuilder.CreateIndex(
                name: "IX_PredefinedFunctions_FunctionTextId",
                table: "PredefinedFunctions",
                column: "FunctionTextId");

            migrationBuilder.CreateIndex(
                name: "IX_Texts_Alias",
                table: "Texts",
                column: "Alias",
                unique: true,
                filter: "[Alias] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Translations_LanguageId",
                table: "Translations",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_TextLanguageUniqueness",
                table: "Translations",
                columns: new[] { "TextId", "LanguageId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserAuthors_AuthorId",
                table: "UserAuthors",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_UserAuthors_UserId",
                table: "UserAuthors",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserBricks_BrickId",
                table: "UserBricks",
                column: "BrickId");

            migrationBuilder.CreateIndex(
                name: "IX_UserBricks_DocumentModelId",
                table: "UserBricks",
                column: "DocumentModelId");

            migrationBuilder.CreateIndex(
                name: "IX_UserBricks_UserId",
                table: "UserBricks",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserFeedbacks_UserId",
                table: "UserFeedbacks",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserGroups_Name",
                table: "UserGroups",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Users_Login",
                table: "Users",
                column: "Login",
                unique: true,
                filter: "[Login] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Users_SelfId",
                table: "Users",
                column: "SelfId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_SettingsId",
                table: "Users",
                column: "SettingsId");

            migrationBuilder.CreateIndex(
                name: "IX_UserSettings_ColorSchemeId",
                table: "UserSettings",
                column: "ColorSchemeId");

            migrationBuilder.CreateIndex(
                name: "IX_Workplaces_AddressId",
                table: "Workplaces",
                column: "AddressId");

            migrationBuilder.CreateIndex(
                name: "IX_Workplaces_AuthorId",
                table: "Workplaces",
                column: "AuthorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActionLogs");

            migrationBuilder.DropTable(
                name: "ActivityTracks");

            migrationBuilder.DropTable(
                name: "AuthorEditRequests");

            migrationBuilder.DropTable(
                name: "BrickResources");

            migrationBuilder.DropTable(
                name: "BrickSettings");

            migrationBuilder.DropTable(
                name: "BrickTexts");

            migrationBuilder.DropTable(
                name: "CharacterMappings");

            migrationBuilder.DropTable(
                name: "DeviceSettings");

            migrationBuilder.DropTable(
                name: "DocumentModelAuthorRoles");

            migrationBuilder.DropTable(
                name: "DocumentModelLanguages");

            migrationBuilder.DropTable(
                name: "DocumentModelMetadataDefinitions");

            migrationBuilder.DropTable(
                name: "DocumentModelSettings");

            migrationBuilder.DropTable(
                name: "DocumentStructure");

            migrationBuilder.DropTable(
                name: "ExceptionLogEntries");

            migrationBuilder.DropTable(
                name: "JobFunctions");

            migrationBuilder.DropTable(
                name: "LocalisedResources");

            migrationBuilder.DropTable(
                name: "Notifications");

            migrationBuilder.DropTable(
                name: "PersonNames");

            migrationBuilder.DropTable(
                name: "StyleShortcuts");

            migrationBuilder.DropTable(
                name: "TermStoreImports");

            migrationBuilder.DropTable(
                name: "Translations");

            migrationBuilder.DropTable(
                name: "UserAuthors");

            migrationBuilder.DropTable(
                name: "UserBricks");

            migrationBuilder.DropTable(
                name: "UserFeedbacks");

            migrationBuilder.DropTable(
                name: "UserGroups");

            migrationBuilder.DropTable(
                name: "Workplaces");

            migrationBuilder.DropTable(
                name: "AuthorRoles");

            migrationBuilder.DropTable(
                name: "MetadataDefinitions");

            migrationBuilder.DropTable(
                name: "ExceptionBuckets");

            migrationBuilder.DropTable(
                name: "JobAssignments");

            migrationBuilder.DropTable(
                name: "Resources");

            migrationBuilder.DropTable(
                name: "Languages");

            migrationBuilder.DropTable(
                name: "Bricks");

            migrationBuilder.DropTable(
                name: "DocumentModels");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "MetadataCategories");

            migrationBuilder.DropTable(
                name: "BrickCategories");

            migrationBuilder.DropTable(
                name: "BrickGroups");

            migrationBuilder.DropTable(
                name: "DocumentCategories");

            migrationBuilder.DropTable(
                name: "Authors");

            migrationBuilder.DropTable(
                name: "UserSettings");

            migrationBuilder.DropTable(
                name: "OrgaEntities");

            migrationBuilder.DropTable(
                name: "PredefinedFunctions");

            migrationBuilder.DropTable(
                name: "ColorSchemes");

            migrationBuilder.DropTable(
                name: "Addresses");

            migrationBuilder.DropTable(
                name: "Texts");
        }
    }
}
