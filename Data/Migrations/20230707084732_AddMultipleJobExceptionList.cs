﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Eurolook.Data.Migrations
{
    public partial class AddMultipleJobExceptionList : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IgnoreInJobLink",
                table: "ComRefExceptions",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IgnoreInJobLink",
                table: "ComRefExceptions");
        }
    }
}
