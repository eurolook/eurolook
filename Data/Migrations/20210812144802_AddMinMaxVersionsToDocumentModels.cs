using Microsoft.EntityFrameworkCore.Migrations;

namespace Eurolook.Data.Migrations
{
    public partial class AddMinMaxVersionsToDocumentModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MaxVersion",
                table: "DocumentModels",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MinVersion",
                table: "DocumentModels",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaxVersion",
                table: "DocumentModels");

            migrationBuilder.DropColumn(
                name: "MinVersion",
                table: "DocumentModels");
        }
    }
}
