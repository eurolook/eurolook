using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Eurolook.Data.Migrations
{
    public partial class AddPerformanceLogs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PerformanceLogs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Event = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: true),
                    Version = table.Column<string>(nullable: true),
                    DeviceSettingsId = table.Column<Guid>(nullable: true),
                    DeviceName = table.Column<string>(nullable: true),
                    Total = table.Column<TimeSpan>(nullable: false),
                    Min = table.Column<TimeSpan>(nullable: false),
                    Max = table.Column<TimeSpan>(nullable: false),
                    Average = table.Column<TimeSpan>(nullable: false),
                    Median = table.Column<TimeSpan>(nullable: false),
                    Count = table.Column<int>(nullable: false),
                    Track = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PerformanceLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PerformanceLogs_DeviceSettings_DeviceSettingsId",
                        column: x => x.DeviceSettingsId,
                        principalTable: "DeviceSettings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PerformanceLogs_DeviceSettingsId",
                table: "PerformanceLogs",
                column: "DeviceSettingsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PerformanceLogs");
        }
    }
}
