using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Eurolook.Data.Migrations
{
    public partial class AddTablesForComRefSettings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ComRefExceptions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AcronymPattern = table.Column<string>(nullable: false),
                    IsIgnored = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComRefExceptions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ComRefHeaderTextPatterns",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Format = table.Column<string>(nullable: false),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComRefHeaderTextPatterns", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ComRefDgSettings",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DgName = table.Column<string>(nullable: false),
                    Institution = table.Column<string>(nullable: false),
                    Contacts = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    DirectoratePatternId = table.Column<Guid>(nullable: false),
                    UnitPatternId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComRefDgSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ComRefDgSettings_ComRefHeaderTextPatterns_DirectoratePatternId",
                        column: x => x.DirectoratePatternId,
                        principalTable: "ComRefHeaderTextPatterns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ComRefDgSettings_ComRefHeaderTextPatterns_UnitPatternId",
                        column: x => x.UnitPatternId,
                        principalTable: "ComRefHeaderTextPatterns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ComRefDgSettings_DgName",
                table: "ComRefDgSettings",
                column: "DgName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ComRefDgSettings_DirectoratePatternId",
                table: "ComRefDgSettings",
                column: "DirectoratePatternId");

            migrationBuilder.CreateIndex(
                name: "IX_ComRefDgSettings_UnitPatternId",
                table: "ComRefDgSettings",
                column: "UnitPatternId");

            migrationBuilder.CreateIndex(
                name: "IX_ComRefHeaderTextPatterns_Name",
                table: "ComRefHeaderTextPatterns",
                column: "Name",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ComRefDgSettings");

            migrationBuilder.DropTable(
                name: "ComRefExceptions");

            migrationBuilder.DropTable(
                name: "ComRefHeaderTextPatterns");
        }
    }
}
