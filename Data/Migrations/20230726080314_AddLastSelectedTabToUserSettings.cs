﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Eurolook.Data.Migrations
{
    public partial class AddLastSelectedTabToUserSettings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LastSelectedTab",
                table: "UserSettings",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastSelectedTab",
                table: "UserSettings");
        }
    }
}
