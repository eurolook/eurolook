using Microsoft.EntityFrameworkCore.Migrations;

namespace Eurolook.Data.Migrations
{
    public partial class AddAdGroupsToUserGroups : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OrgaEntities",
                table: "UserGroups");

            migrationBuilder.AddColumn<string>(
                name: "AdGroups",
                table: "UserGroups",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdGroups",
                table: "UserGroups");

            migrationBuilder.AddColumn<string>(
                name: "OrgaEntities",
                table: "UserGroups",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
