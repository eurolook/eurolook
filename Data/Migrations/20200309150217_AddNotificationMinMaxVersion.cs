using Microsoft.EntityFrameworkCore.Migrations;

namespace Eurolook.Data.Migrations
{
    public partial class AddNotificationMinMaxVersion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MaxVersion",
                table: "Notifications",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MinVersion",
                table: "Notifications",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsHighlighted",
                table: "Notifications",
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsHighlighted",
                table: "Notifications");

            migrationBuilder.DropColumn(
                name: "MaxVersion",
                table: "Notifications");

            migrationBuilder.DropColumn(
                name: "MinVersion",
                table: "Notifications");
        }
    }
}
