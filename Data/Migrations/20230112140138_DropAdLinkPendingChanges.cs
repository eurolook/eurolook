﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Eurolook.Data.Migrations
{
    public partial class DropAdLinkPendingChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AD_Address_Pending",
                table: "Authors");

            migrationBuilder.DropColumn(
                name: "AD_Description_Pending",
                table: "Authors");

            migrationBuilder.DropColumn(
                name: "AD_Email_Pending",
                table: "Authors");

            migrationBuilder.DropColumn(
                name: "AD_FaxExtension_Pending",
                table: "Authors");

            migrationBuilder.DropColumn(
                name: "AD_Gender_Pending",
                table: "Authors");

            migrationBuilder.DropColumn(
                name: "AD_Initials_Pending",
                table: "Authors");

            migrationBuilder.DropColumn(
                name: "AD_LatinFirstName_Pending",
                table: "Authors");

            migrationBuilder.DropColumn(
                name: "AD_LatinLastName_Pending",
                table: "Authors");

            migrationBuilder.DropColumn(
                name: "AD_Office_Pending",
                table: "Authors");

            migrationBuilder.DropColumn(
                name: "AD_OrgaEntityId_Pending",
                table: "Authors");

            migrationBuilder.DropColumn(
                name: "AD_PhoneExtension_Pending",
                table: "Authors");

            migrationBuilder.DropColumn(
                name: "AD_PredefinedFunctionId_Pending",
                table: "Authors");

            migrationBuilder.DropColumn(
                name: "AD_Service_Pending",
                table: "Authors");

            migrationBuilder.DropColumn(
                name: "AD_WebAddress_Pending",
                table: "Authors");

            migrationBuilder.DropColumn(
                name: "PendingADChange",
                table: "Authors");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AD_Address_Pending",
                table: "Authors",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AD_Description_Pending",
                table: "Authors",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AD_Email_Pending",
                table: "Authors",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AD_FaxExtension_Pending",
                table: "Authors",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AD_Gender_Pending",
                table: "Authors",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AD_Initials_Pending",
                table: "Authors",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AD_LatinFirstName_Pending",
                table: "Authors",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AD_LatinLastName_Pending",
                table: "Authors",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AD_Office_Pending",
                table: "Authors",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "AD_OrgaEntityId_Pending",
                table: "Authors",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AD_PhoneExtension_Pending",
                table: "Authors",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "AD_PredefinedFunctionId_Pending",
                table: "Authors",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AD_Service_Pending",
                table: "Authors",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AD_WebAddress_Pending",
                table: "Authors",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "PendingADChange",
                table: "Authors",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
