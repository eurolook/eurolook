﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Eurolook.Data.Migrations
{
    public partial class AddExternalDatabaseValuesToJobAssignment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "AD_OrgaEntityId",
                table: "JobAssignments",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "AD_PredefinedFunction",
                table: "JobAssignments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AD_Service",
                table: "JobAssignments",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ExternalId",
                table: "JobAssignments",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AD_OrgaEntityId",
                table: "JobAssignments");

            migrationBuilder.DropColumn(
                name: "AD_PredefinedFunction",
                table: "JobAssignments");

            migrationBuilder.DropColumn(
                name: "AD_Service",
                table: "JobAssignments");

            migrationBuilder.DropColumn(
                name: "ExternalId",
                table: "JobAssignments");
        }
    }
}
