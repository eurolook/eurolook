using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Eurolook.Data.Migrations
{
    public partial class AddUserTemplates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UserTemplates",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ServerModificationTimeUtc = table.Column<DateTime>(type: "DateTime2", nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    ClientModification = table.Column<bool>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    TemplateId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTemplates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserTemplates_Templates_TemplateId",
                        column: x => x.TemplateId,
                        principalTable: "Templates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserTemplates_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserTemplates_UserId",
                table: "UserTemplates",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserTemplates_TemplateId",
                table: "UserTemplates",
                column: "TemplateId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserTemplates");
        }
    }
}
