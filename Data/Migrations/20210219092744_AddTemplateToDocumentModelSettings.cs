using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Eurolook.Data.Migrations
{
    public partial class AddTemplateToDocumentModelSettings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "TemplateId",
                table: "DocumentModelSettings",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_DocumentModelSettings_TemplateId",
                table: "DocumentModelSettings",
                column: "TemplateId");

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentModelSettings_Templates_TemplateId",
                table: "DocumentModelSettings",
                column: "TemplateId",
                principalTable: "Templates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DocumentModelSettings_Templates_TemplateId",
                table: "DocumentModelSettings");

            migrationBuilder.DropIndex(
                name: "IX_DocumentModelSettings_TemplateId",
                table: "DocumentModelSettings");

            migrationBuilder.DropColumn(
                name: "TemplateId",
                table: "DocumentModelSettings");
        }
    }
}
