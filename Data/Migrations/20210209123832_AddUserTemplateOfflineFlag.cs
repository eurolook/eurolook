﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Eurolook.Data.Migrations
{
    public partial class AddUserTemplateOfflineFlag : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsOfflineAvailable",
                table: "UserTemplates",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsOfflineAvailable",
                table: "UserTemplates");
        }
    }
}
