using Microsoft.EntityFrameworkCore.Migrations;

namespace Eurolook.Data.Migrations
{
    public partial class AddFunctionalMailbox : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FunctionalMailbox",
                table: "JobAssignments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FunctionalMailbox",
                table: "Authors",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FunctionalMailbox",
                table: "JobAssignments");

            migrationBuilder.DropColumn(
                name: "FunctionalMailbox",
                table: "Authors");
        }
    }
}
