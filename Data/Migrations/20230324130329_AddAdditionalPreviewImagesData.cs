﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Eurolook.Data.Migrations
{
    public partial class AddAdditionalPreviewImagesData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PreviewLanguage",
                table: "Templates",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "PreviewModifiedTime",
                table: "Templates",
                type: "DateTime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "SmallPreviewModifiedTime",
                table: "Templates",
                type: "DateTime2",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "SmallPreviewPage1",
                table: "Templates",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "SmallPreviewPage2",
                table: "Templates",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PreviewLanguage",
                table: "Templates");

            migrationBuilder.DropColumn(
                name: "PreviewModifiedTime",
                table: "Templates");

            migrationBuilder.DropColumn(
                name: "SmallPreviewModifiedTime",
                table: "Templates");

            migrationBuilder.DropColumn(
                name: "SmallPreviewPage1",
                table: "Templates");

            migrationBuilder.DropColumn(
                name: "SmallPreviewPage2",
                table: "Templates");
        }
    }
}
