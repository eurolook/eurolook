using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.Data.Xml.Metadata
{
    public abstract class UserMetadataPropertyXmlReaderWriter
    {
        protected IEnumerable<XElement> CreateUserInfoElements(IEnumerable<UserInfo> userInfos)
        {
            foreach (var userInfo in userInfos)
            {
                yield return new XElement(
                    EurolookPropertiesCustomXml.ElementUserInfo,
                    new XElement(EurolookPropertiesCustomXml.ElementUserInfoDisplayName, userInfo.DisplayName),
                    new XElement(EurolookPropertiesCustomXml.ElementUserInfoAccountId, userInfo.AccountId),
                    new XElement(EurolookPropertiesCustomXml.ElementUserInfoAccountType, userInfo.AccountType));
            }
        }

        protected IEnumerable<UserInfo> TryReadUserInfos(XElement element)
        {
            try
            {
                return ReadUserInfos(element);
            }
            catch (Exception)
            {
                return Enumerable.Empty<UserInfo>();
            }
        }

        private IEnumerable<UserInfo> ReadUserInfos(XElement element)
        {
            var userInfoElements = element.Descendants(EurolookPropertiesCustomXml.ElementUserInfo);
            var userInfos = userInfoElements.Select(
                ui =>
                {
                    string displayName = (string)ui.Element(EurolookPropertiesCustomXml.ElementUserInfoDisplayName);
                    var accountId = int.TryParse(
                        (string)ui.Element(EurolookPropertiesCustomXml.ElementUserInfoAccountId),
                        out int tempVal)
                        ? tempVal
                        : (int?)null;

                    return new UserInfo(accountId, displayName);
                });

            return userInfos;
        }
    }
}
