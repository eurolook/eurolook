using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.Data.Xml.Metadata
{
    public class SingleBoundedTermMetadataPropertyXmlReaderWriter :
        BoundedTermsMetadataPropertyXmlReaderWriter,
        IMetadataPropertyXmlReaderWriter
    {
        public MetadataProperty ReadPropertyContent(XElement element)
        {
            return new MetadataProperty(element.Name.LocalName, TryReadTermBindings(element).FirstOrDefault());
        }

        public XElement WritePropertyContent(MetadataProperty property)
        {
            var termBinding = property.GetTermBinding();
            var termBindings = new List<TermBinding>();
            if (termBinding != null)
            {
                termBindings.Add(termBinding);
            }

            var termsElement = CreateTermsElement(termBindings);

            return new XElement(
                property.Name,
                new XAttribute(
                    EurolookPropertiesCustomXml.MetadataSerializationTypeAttribute,
                    MetadataSerializationType.SingleBoundedTerm),
                termsElement);
        }

        public bool CanHandle(MetadataSerializationType metadataSerializationType)
        {
            return metadataSerializationType == MetadataSerializationType.SingleBoundedTerm;
        }
    }
}
