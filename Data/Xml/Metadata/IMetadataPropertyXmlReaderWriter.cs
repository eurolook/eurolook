﻿using System.Xml.Linq;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.Data.Xml.Metadata
{
    public interface IMetadataPropertyXmlReaderWriter
    {
        MetadataProperty ReadPropertyContent(XElement element);

        XElement WritePropertyContent(MetadataProperty property);

        bool CanHandle(MetadataSerializationType metadataSerializationType);
    }
}
