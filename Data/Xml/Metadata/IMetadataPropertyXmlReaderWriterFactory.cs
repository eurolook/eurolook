using System.Xml.Linq;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.Data.Xml.Metadata
{
    public interface IMetadataPropertyXmlReaderWriterFactory
    {
        IMetadataPropertyXmlReaderWriter GetMetadataPropertyReaderWriter(
            MetadataSerializationType metadataSerializationType);

        IMetadataPropertyXmlReaderWriter GetMetadataPropertyReaderWriter(XElement xElement);
    }
}
