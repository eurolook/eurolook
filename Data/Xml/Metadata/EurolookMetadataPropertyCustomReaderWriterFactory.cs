using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.Data.Xml.Metadata
{
    public class EurolookMetadataPropertyCustomReaderWriterFactory : IMetadataPropertyXmlReaderWriterFactory
    {
        private readonly IEnumerable<IMetadataPropertyXmlReaderWriter> _candidates;

        public EurolookMetadataPropertyCustomReaderWriterFactory(
            IEnumerable<IMetadataPropertyXmlReaderWriter> candidates)
        {
            _candidates = candidates;
        }

        public IMetadataPropertyXmlReaderWriter GetMetadataPropertyReaderWriter(
            MetadataSerializationType metadataSerializationType)
        {
            return _candidates.Single(c => c.CanHandle(metadataSerializationType));
        }

        public IMetadataPropertyXmlReaderWriter GetMetadataPropertyReaderWriter(XElement xElement)
        {
            string serializationTypeAttribute =
                (string)xElement.Attribute(EurolookPropertiesCustomXml.MetadataSerializationTypeAttribute);
            var serializationType = (MetadataSerializationType)Enum.Parse(
                typeof(MetadataSerializationType),
                serializationTypeAttribute);
            return GetMetadataPropertyReaderWriter(serializationType);
        }
    }
}
