﻿using System.Xml.Linq;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.Data.Xml.Metadata
{
    public class SimpleMetadataPropertyXmlReaderWriter : IMetadataPropertyXmlReaderWriter
    {
        public MetadataProperty ReadPropertyContent(XElement element)
        {
            return new MetadataProperty(element.Name.LocalName, element.Value);
        }

        public XElement WritePropertyContent(MetadataProperty property)
        {
            var result = new XElement(
                property.Name,
                new XAttribute(
                    EurolookPropertiesCustomXml.MetadataSerializationTypeAttribute,
                    MetadataSerializationType.SimpleValue));
            if (property.GetSimpleValue() != null)
            {
                result.Add(property.GetSimpleValue());
            }

            return result;
        }

        public bool CanHandle(MetadataSerializationType metadataSerializationType)
        {
            return metadataSerializationType == MetadataSerializationType.SimpleValue;
        }
    }
}
