using System.Xml.Linq;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.Data.Xml.Metadata
{
    public class MultipleUsersMetadataPropertyXmlReaderWriter : UserMetadataPropertyXmlReaderWriter,
                                                                IMetadataPropertyXmlReaderWriter
    {
        public MetadataProperty ReadPropertyContent(XElement element)
        {
            return new MetadataProperty(element.Name.LocalName, TryReadUserInfos(element));
        }

        public XElement WritePropertyContent(MetadataProperty property)
        {
            var userInfos = property.GetUserInfos();
            var userInfoElements = CreateUserInfoElements(userInfos);

            return new XElement(
                property.Name,
                new XAttribute(
                    EurolookPropertiesCustomXml.MetadataSerializationTypeAttribute,
                    MetadataSerializationType.MultipleUsers),
                userInfoElements);
        }

        public bool CanHandle(MetadataSerializationType metadataSerializationType)
        {
            return metadataSerializationType == MetadataSerializationType.MultipleUsers;
        }
    }
}
