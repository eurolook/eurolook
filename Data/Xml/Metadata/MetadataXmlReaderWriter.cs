﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Eurolook.Data.Models.Metadata;
using JetBrains.Annotations;

namespace Eurolook.Data.Xml.Metadata
{
    public class MetadataXmlReaderWriter : IMetadataXmlReaderWriter
    {
        private readonly IMetadataPropertyXmlReaderWriterFactory _xmlReaderWriterFactory;

        public MetadataXmlReaderWriter(IMetadataPropertyXmlReaderWriterFactory metadataPropertyXmlReaderWriterFactory)
        {
            _xmlReaderWriterFactory = metadataPropertyXmlReaderWriterFactory;
        }

        public void WriteMetadata(XElement rootElement, Dictionary<string, MetadataProperty> metadataDictionary)
        {
            var metadataXmlElements = metadataDictionary
                .Select(
                    kvp =>
                    {
                        var serializationType = kvp.Value.MetadataSerializationType;
                        var writer = _xmlReaderWriterFactory.GetMetadataPropertyReaderWriter(serializationType);
                        return writer.WritePropertyContent(kvp.Value);
                    });

            rootElement.Add(metadataXmlElements);
        }

        [NotNull]
        public Dictionary<string, MetadataProperty> ReadMetadata(XElement rootElement)
        {
            return rootElement?.Elements()
                              .Select(TryReadMetadataProperty)
                              .ToDictionary(e => e.Name, e => e) ?? new Dictionary<string, MetadataProperty>();
        }

        private MetadataProperty TryReadMetadataProperty(XElement element)
        {
            try
            {
                var reader = _xmlReaderWriterFactory.GetMetadataPropertyReaderWriter(element);
                return reader.ReadPropertyContent(element);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
