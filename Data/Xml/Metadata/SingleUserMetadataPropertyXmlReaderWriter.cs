using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.Data.Xml.Metadata
{
    public class SingleUserMetadataPropertyXmlReaderWriter : UserMetadataPropertyXmlReaderWriter,
                                                             IMetadataPropertyXmlReaderWriter
    {
        public MetadataProperty ReadPropertyContent(XElement element)
        {
            return new MetadataProperty(element.Name.LocalName, TryReadUserInfos(element).FirstOrDefault());
        }

        public XElement WritePropertyContent(MetadataProperty property)
        {
            var userInfo = property.GetUserInfo();
            var userInfos = new List<UserInfo>();
            if (userInfo != null)
            {
                userInfos.Add(userInfo);
            }

            var userInfoElement = CreateUserInfoElements(userInfos);

            return new XElement(
                property.Name,
                new XAttribute(
                    EurolookPropertiesCustomXml.MetadataSerializationTypeAttribute,
                    MetadataSerializationType.SingleUser),
                userInfoElement);
        }

        public bool CanHandle(MetadataSerializationType metadataSerializationType)
        {
            return metadataSerializationType == MetadataSerializationType.SingleUser;
        }
    }
}
