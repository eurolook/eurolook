using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.Data.Xml.Metadata
{
    public abstract class BoundedTermsMetadataPropertyXmlReaderWriter
    {
        protected XElement CreateTermsElement(IEnumerable<TermBinding> termBindings)
        {
            var termsElement = new XElement(EurolookPropertiesCustomXml.ElementTerms);
            foreach (var termBinding in termBindings)
            {
                var termInfoElement = new XElement(EurolookPropertiesCustomXml.ElementTermInfo);
                var termNameElement = new XElement(EurolookPropertiesCustomXml.ElementTermName, termBinding.TermName);
                var termIdElement = new XElement(EurolookPropertiesCustomXml.ElementTermId, termBinding.TermId);

                termInfoElement.Add(termNameElement, termIdElement);
                termsElement.Add(termInfoElement);
            }

            return termsElement;
        }

        protected IEnumerable<TermBinding> TryReadTermBindings(XElement element)
        {
            try
            {
                return ReadTermBindings(element);
            }
            catch (Exception)
            {
                return new List<TermBinding>();
            }
        }

        private IEnumerable<TermBinding> ReadTermBindings(XElement element)
        {
            var termInfoElements = element.Descendants(EurolookPropertiesCustomXml.ElementTermInfo);
            var termBindings = termInfoElements.Select(
                ti =>
                {
                    string termName = ti.Element(EurolookPropertiesCustomXml.ElementTermName).Value;
                    var termId = Guid.Parse(ti.Element(EurolookPropertiesCustomXml.ElementTermId).Value);
                    return new TermBinding(termName, termId);
                });
            return termBindings;
        }
    }
}
