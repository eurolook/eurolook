﻿using System.Linq;
using System.Xml.Linq;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.Data.Xml.Metadata
{
    public class MultipleBoundedTermsMetadataPropertyXmlReaderWriter :
        BoundedTermsMetadataPropertyXmlReaderWriter,
        IMetadataPropertyXmlReaderWriter
    {
        public MetadataProperty ReadPropertyContent(XElement element)
        {
            return new MetadataProperty(element.Name.LocalName, TryReadTermBindings(element));
        }

        public XElement WritePropertyContent(MetadataProperty property)
        {
            var termBindings = property.GetTermBindings().ToList();
            var termsElement = CreateTermsElement(termBindings);

            return new XElement(
                property.Name,
                new XAttribute(
                    EurolookPropertiesCustomXml.MetadataSerializationTypeAttribute,
                    MetadataSerializationType.MultipleBoundedTerms),
                termsElement);
        }

        public bool CanHandle(MetadataSerializationType metadataSerializationType)
        {
            return metadataSerializationType == MetadataSerializationType.MultipleBoundedTerms;
        }
    }
}
