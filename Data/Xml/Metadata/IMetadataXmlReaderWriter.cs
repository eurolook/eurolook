using System.Collections.Generic;
using System.Xml.Linq;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.Data.Xml.Metadata
{
    public interface IMetadataXmlReaderWriter
    {
        void WriteMetadata(XElement rootElement, Dictionary<string, MetadataProperty> metadataDictionary);

        Dictionary<string, MetadataProperty> ReadMetadata(XElement rootElement);
    }
}
