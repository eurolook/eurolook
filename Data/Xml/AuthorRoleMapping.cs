using Eurolook.Data.Models;
using Eurolook.Data.Models.AuthorRoles;

namespace Eurolook.DocumentProcessing.DocumentCreation
{
    public class AuthorRoleMapping
    {
        public AuthorRoleMapping(Author author, JobAssignment jobAssignment, AuthorRole authorRole)
        {
            Author = author;
            JobAssignment = jobAssignment;
            AuthorRole = authorRole;
        }

        public Author Author { get; set; }

        public JobAssignment JobAssignment { get; set; }

        public AuthorRole AuthorRole { get; set; }
    }
}
