namespace Eurolook.Data.Xml
{
    public class AuthorCustomXmlStoreInfo
    {
        public string StoreId { get; set; }

        public AuthorCustomXml AuthorCustomXml { get; set; }
    }
}
