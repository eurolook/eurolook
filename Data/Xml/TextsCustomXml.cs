using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Eurolook.Data.Models;

namespace Eurolook.Data.Xml
{
    public class TextsCustomXml : XDocument
    {
        public static readonly string RootName = "Texts";
        public static readonly Guid DatastoreItemId = new Guid("{4EF90DE6-88B6-4264-9629-4D8DFDFE87D2}");

        public TextsCustomXml(XDocument xdoc)
            : base(xdoc)
        {
        }

        public TextsCustomXml(IEnumerable<Translation> translations, Language language)
        {
            Add(new XElement(RootName));
            foreach (var t in translations)
            {
                if (t?.Text != null)
                {
                    Root?.Add(new XElement(t.Text.Alias, t.Value));
                }
            }

            // also store the language-specific date formats
            Root?.Add(new XElement("DateFormatShort", language.DateFormatShort));
            Root?.Add(new XElement("DateFormatLong", language.DateFormatLong));
        }
    }
}
