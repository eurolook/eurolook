using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Eurolook.Data.Xml
{
    public class CombinedPropertiesXml : XDocument
    {
        public CombinedPropertiesXml(
            List<AuthorCustomXml> authorCustomXmlList,
            IReadOnlyEurolookProperties eurolookProperties)
        {
            // merge custom XML files
            Add(new XElement("Custom"));

            if (Root == null)
            {
                throw new InvalidOperationException("Could not properly create XDocument.");
            }

            var mainAuthor = authorCustomXmlList.FirstOrDefault();
            if (mainAuthor?.Root != null)
            {
                Root.Add(mainAuthor.Root);
            }

            bool HasAuthorRole(AuthorCustomXml a) => a?.Root != null && (a.AuthorRoleName != null);

            foreach (var authorCustomXml in authorCustomXmlList.Where(HasAuthorRole))
            {
                authorCustomXml.Root.Name = authorCustomXml.AuthorRoleName;
                Root.Add(authorCustomXml.Root);
                authorCustomXml.Root.Name = AuthorCustomXml.RootName;
            }

            bool HasOriginalAuthorRole(AuthorCustomXml a) => a?.Root != null && (a.OriginalAuthorRoleName != null);

            foreach (var authorCustomXml in authorCustomXmlList.Where(HasOriginalAuthorRole))
            {
                if (Root.Elements().All(e => e.Name.LocalName != authorCustomXml.OriginalAuthorRoleName))
                {
                    authorCustomXml.Root.Name = authorCustomXml.OriginalAuthorRoleName;
                    Root.Add(authorCustomXml.Root);
                    authorCustomXml.Root.Name = AuthorCustomXml.RootName;
                }
            }

            if (eurolookProperties != null)
            {
                Root.Add(eurolookProperties.ToXDocument().Root);
            }
        }
    }
}
