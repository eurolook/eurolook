﻿using System;
using System.Xml.Linq;

namespace Eurolook.Data.Xml
{
    public interface IReadOnlyEurolookProperties
    {
        string ProductCustomizationId { get; }

        string CreationVersion { get; }

        DateTime? CreationDate { get; }

        string CreationLanguage { get; }

        string EditVersion { get; }

        DateTime? EditDate { get; }

        Guid? DocumentModelId { get; }

        string DocumentModelName { get; }

        Guid? CustomTemplateId { get; }

        string CustomTemplateName { get; }

        DateTime? DocumentDate { get; }

        EurolookCompatibilityMode CompatibilityMode { get; }

        XDocument ToXDocument();
    }
}
