using Eurolook.Data.Models;
using Eurolook.Data.Models.AuthorRoles;

namespace Eurolook.Data.Xml
{
    public class AuthorCustomXmlCreationInfo
    {
        public AuthorCustomXmlCreationInfo(Author author, IJobAssignment jobAssignment, Language language)
        {
            Author = author;
            JobAssignment = jobAssignment;
            Language = language;
        }

        public Author Author { get; }

        public IJobAssignment JobAssignment { get; set; }

        public Language Language { get; }

        public bool IsMainAuthor { get; set; }

        public AuthorRole AuthorRole { get; set; }

        public AuthorRole OriginalAuthorRole { get; set; }

        public string ContentControlTitle { get; set; }
    }
}
