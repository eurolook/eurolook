﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using Eurolook.Common.Log;
using Eurolook.Data.Models.Metadata;
using Eurolook.Data.Xml.Metadata;

namespace Eurolook.Data.Xml
{
    public class EurolookPropertiesCustomXml : IEurolookProperties, ICanLog
    {
        private const string ElementProductCustomizationId = "ProductCustomizationId";
        private const string ElementVersion = "Version";
        private const string ElementDate = "Date";
        private const string ElementLanguage = "Language";
        private const string ElementCreationNote = "Note";
        private const string ElementId = "Id";
        private const string ElementName = "Name";
        private const string ElementDocumentDate = "DocumentDate";
        private const string ElementCompatibilityMode = "CompatibilityMode";
        private const string ElementDocumentVersion = "DocumentVersion";
        private const string ElementAddress = "Address";
        private const string ElementDocumentMetadata = "DocumentMetadata";

        // Format must be 's': '2016-02-16T12:30:05'; otherwise Word cannot bind date controls
        private const string DateFormat = "s";
        public static readonly string RootName = "EurolookProperties";
        public static readonly string SharePointRootName = "properties";

        public static readonly string ElementTermId = "TermId";
        public static readonly string ElementTermName = "TermName";
        public static readonly string ElementTerms = "Terms";
        public static readonly string ElementTermInfo = "TermInfo";

        public static readonly string ElementUserInfo = "UserInfo";
        public static readonly string ElementUserInfoDisplayName = "DisplayName";
        public static readonly string ElementUserInfoAccountId = "AccountId";
        public static readonly string ElementUserInfoAccountType = "AccountType";

        public static readonly string MetadataSerializationTypeAttribute = "MetadataSerializationType";

        public static readonly string SharePointRootNamespaceUri =
            "http://schemas.microsoft.com/office/2006/metadata/properties";

        public static readonly Guid DatastoreItemId = new Guid("{D3EA5527-7367-4268-9D83-5125C98D0ED2}");

        private readonly IMetadataXmlReaderWriter _metadataXmlReaderWriter;

        public EurolookPropertiesCustomXml()
        {
            var metadataReaderWriterFactory = new EurolookMetadataPropertyCustomReaderWriterFactory(
                new List<IMetadataPropertyXmlReaderWriter>
                {
                    new SimpleMetadataPropertyXmlReaderWriter(),
                    new SingleBoundedTermMetadataPropertyXmlReaderWriter(),
                    new MultipleBoundedTermsMetadataPropertyXmlReaderWriter(),
                    new SingleUserMetadataPropertyXmlReaderWriter(),
                    new MultipleUsersMetadataPropertyXmlReaderWriter(),
                });

            _metadataXmlReaderWriter = new MetadataXmlReaderWriter(metadataReaderWriterFactory);
        }

        public EurolookPropertiesCustomXml(string xml)
            : this()
        {
            if (xml != null)
            {
                Parse(xml);
            }
        }

        public EurolookPropertiesCustomXml(XDocument xDocument)
            : this()
        {
            if (xDocument != null)
            {
                Parse(xDocument);
            }
        }

        public string ProductCustomizationId { get; set; }

        public string CreationVersion { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreationLanguage { get; set; }

        public string CreationNote { get; set; }

        public string EditVersion { get; set; }

        public DateTime? EditDate { get; set; }

        public Guid? DocumentModelId { get; set; }

        public string DocumentModelName { get; set; }

        public Guid? CustomTemplateId { get; set; }

        public string CustomTemplateName { get; set; }

        public DateTime? DocumentDate { get; set; }

        public EurolookCompatibilityMode CompatibilityMode { get; set; }

        public string DocumentVersion { get; set; }

        public Dictionary<string, MetadataProperty> DocumentMetadata { get; set; }
            = new Dictionary<string, MetadataProperty>();

        public XDocument ToXDocument()
        {
            var result = new XDocument(new XElement(RootName));
            result.Root?.Add(
                CreateXElement(ElementProductCustomizationId, ProductCustomizationId),
                new XElement(
                    "Created",
                    CreateXElement(ElementVersion, CreationVersion),
                    CreateXElement(ElementDate, GetFormattedDate(CreationDate)),
                    CreateXElement(ElementLanguage, CreationLanguage),
                    CreateXElement(ElementCreationNote, CreationNote)),
                new XElement(
                    "Edited",
                    CreateXElement(ElementVersion, EditVersion),
                    CreateXElement(ElementDate, GetFormattedDate(EditDate))),
                new XElement(
                    "DocumentModel",
                    CreateXElement(ElementId, DocumentModelId),
                    CreateXElement(ElementName, DocumentModelName)),
                new XElement(
                    "CustomTemplate",
                    CreateXElement(ElementId, CustomTemplateId),
                    CreateXElement(ElementName, CustomTemplateName)),
                CreateXElement(ElementDocumentDate, GetFormattedDate(DocumentDate)),
                CreateXElement(ElementDocumentVersion, DocumentVersion),
                CreateXElement(ElementCompatibilityMode, CompatibilityMode.ToString()));

            WriteMetadata(result);

            return result;
        }

        private static DateTime? TryGetDate(XElement element)
        {
            string value = (string)element;
            if (!string.IsNullOrWhiteSpace(value))
            {
                if (DateTime.TryParseExact(
                    value,
                    DateFormat,
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.RoundtripKind,
                    out var result))
                {
                    return result;
                }

                if (DateTime.TryParseExact(
                    value,
                    "o",
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.RoundtripKind,
                    out result))
                {
                    // backwards compatibility, we changed the format from o to s
                    return result;
                }
            }

            return null;
        }

        private static string GetFormattedDate(DateTime? value)
        {
            return value?.ToString(DateFormat);
        }

        private static T TryGetEnum<T>(XElement element)
            where T : struct
        {
            string value = (string)element;
            if (!string.IsNullOrEmpty(value) && Enum.TryParse(value, true, out T result))
            {
                return result;
            }

            return default;
        }

        private static Guid? TryGetGuid(XElement element)
        {
            string value = (string)element;
            if (!string.IsNullOrEmpty(value) && Guid.TryParse(value, out var result))
            {
                return result;
            }

            return null;
        }

        private static XElement CreateXElement(string name, object content)
        {
            return content == null ? new XElement(name) : new XElement(name, content);
        }

        private void Parse(string xml)
        {
            try
            {
                var xdoc = XDocument.Parse(xml);
                Parse(xdoc);
            }
            catch (Exception ex)
            {
                LogManager.GetLogger().Debug(ex.Message, ex);
            }
        }

        private void Parse(XDocument xdoc)
        {
            try
            {
                if (xdoc.Root == null)
                {
                    return;
                }

                ProductCustomizationId = (string)xdoc.Root.Element(ElementProductCustomizationId);

                var createdNode = xdoc.Root.Element("Created");
                if (createdNode != null)
                {
                    CreationVersion = (string)createdNode.Element(ElementVersion);
                    CreationDate = TryGetDate(createdNode.Element(ElementDate));
                    CreationLanguage = (string)createdNode.Element(ElementLanguage);
                    CreationNote = (string)createdNode.Element(ElementCreationNote);
                }

                var editNode = xdoc.Root.Element("Edited");
                if (editNode != null)
                {
                    EditVersion = (string)editNode.Element(ElementVersion);
                    EditDate = TryGetDate(editNode.Element(ElementDate));
                }

                var documentModelNode = xdoc.Root.Element("DocumentModel");
                if (documentModelNode != null)
                {
                    DocumentModelId = TryGetGuid(documentModelNode.Element(ElementId));
                    DocumentModelName = (string)documentModelNode.Element(ElementName);
                }

                var customTemplateNode = xdoc.Root.Element("CustomTemplate");
                if (customTemplateNode != null)
                {
                    CustomTemplateId = TryGetGuid(customTemplateNode.Element(ElementId));
                    CustomTemplateName = (string)customTemplateNode.Element(ElementName);
                }

                DocumentDate = TryGetDate(xdoc.Root.Element(ElementDocumentDate));
                DocumentVersion = (string)xdoc.Root.Element(ElementDocumentVersion);

                var compatibilityMode =
                    TryGetEnum<EurolookCompatibilityMode>(xdoc.Root.Element(ElementCompatibilityMode));
                if (compatibilityMode == EurolookCompatibilityMode.Unknown)
                {
                    compatibilityMode = EurolookCompatibilityMode.Eurolook10;
                }

                CompatibilityMode = compatibilityMode;
                DocumentMetadata = _metadataXmlReaderWriter.ReadMetadata(xdoc.Root.Element(ElementDocumentMetadata));
            }
            catch (Exception ex)
            {
                LogManager.GetLogger().Debug(ex.Message, ex);
            }
        }

        private void WriteMetadata(XDocument xDoc)
        {
            if (DocumentMetadata == null || !DocumentMetadata.Any())
            {
                return;
            }

            var metadataRootElement = new XElement(ElementDocumentMetadata);
            _metadataXmlReaderWriter.WriteMetadata(metadataRootElement, DocumentMetadata);
            xDoc.Root?.Add(metadataRootElement);
        }
    }
}
