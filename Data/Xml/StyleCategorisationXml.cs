﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Eurolook.Common.Extensions;

namespace Eurolook.Data.Xml
{
    public class StyleCategorisationXml : XDocument
    {
        public static readonly string RootName = "StyleCategorisation";
        public static readonly string StyleElementName = "Style";
        public static readonly string StyleListName = "Styles";
        public static readonly string CategoryName = "Category";
        public static readonly string CategoryListName = "Categories";
        public static readonly string StyleNameElementName = "Name";

        // categories with special behaviour.
        public static readonly string StyleCategoryNameFavorites = "Favourites";
        public static readonly string StyleCategoryNameDocument = "Document All template styles";
        public static readonly string StyleCategoryNameUserDefined = "User-defined styles";
        public static readonly string StyleCategoryNameCustom = "Custom styles";
        public static readonly string StyleCategoryNameBuiltIn = "Built-in styles";

        // standard categories
        public static readonly string StyleCategoryNameHeading = "Headings and titles";
        public static readonly string StyleCategoryNameText = "Text";
        public static readonly string StyleCategoryNameBullets = "Bulleted lists";
        public static readonly string StyleCategoryNameNumbered = "Numbered lists";
        public static readonly string StyleCategoryNameImage = "Figures and images";
        public static readonly string StyleCategoryNameTable = "Tables and table text";
        public static readonly string StyleCategoryNameBoxes = "Boxes and box text";
        public static readonly string StyleCategoryNameSidenotes = "Footnotes and Endnotes";
        public static readonly string StyleCategoryNameToc = "Table of contents";

        // old category names for upgrade
        private static readonly string _oldCategoryNameFavorites = "Favorites";
        private static readonly string _oldCategoryNameDocument = "Document";
        private static readonly string _oldCategoryNameUserDefined = "User-Defined";
        private static readonly string _oldCategoryNameBuiltIn = "Built-In";
        private static readonly string _oldCategoryNameHeading = "Heading";
        private static readonly string _oldCategoryNameText = "Text";
        private static readonly string _oldCategoryNameBullets = "Bulleted Lists";
        private static readonly string _oldCategoryNameNumbered = "Numbered Lists";
        private static readonly string _oldCategoryNameImage = "Figures and Images";
        private static readonly string _oldCategoryNameTable = "Tables";
        private static readonly string _oldCategoryNameBox = "Boxes";
        private static readonly string _oldCategoryNameSidenotes = "Footnotes and Endnotes";
        private static readonly string _oldCategoryNameToc = "Toc";

        private static readonly IReadOnlyCollection<(string, string)> _updateCategoryNamesMapping = new List<(string, string)>
        {
            (_oldCategoryNameFavorites, StyleCategoryNameFavorites),
            (_oldCategoryNameDocument, StyleCategoryNameDocument),
            (_oldCategoryNameUserDefined, StyleCategoryNameUserDefined),
            (_oldCategoryNameBuiltIn, StyleCategoryNameBuiltIn),
            (_oldCategoryNameHeading, StyleCategoryNameHeading),
            (_oldCategoryNameText, StyleCategoryNameText),
            (_oldCategoryNameBullets, StyleCategoryNameBullets),
            (_oldCategoryNameNumbered, StyleCategoryNameNumbered),
            (_oldCategoryNameImage, StyleCategoryNameImage),
            (_oldCategoryNameTable, StyleCategoryNameTable),
            (_oldCategoryNameBox, StyleCategoryNameBoxes),
            (_oldCategoryNameSidenotes, StyleCategoryNameSidenotes),
            (_oldCategoryNameToc, StyleCategoryNameToc),
        };

        public static readonly IReadOnlyCollection<string> StyleCategoryNames = new List<string>
        {
            StyleCategoryNameDocument,
            StyleCategoryNameFavorites,
            StyleCategoryNameHeading,
            StyleCategoryNameText,
            StyleCategoryNameBullets,
            StyleCategoryNameNumbered,
            StyleCategoryNameImage,
            StyleCategoryNameTable,
            StyleCategoryNameBoxes,
            StyleCategoryNameSidenotes,
            StyleCategoryNameToc,
            StyleCategoryNameUserDefined,
            StyleCategoryNameCustom,
            StyleCategoryNameBuiltIn,
        };

        public StyleCategorisationXml(XDocument xdoc)
            : base(xdoc)
        {
        }

        public IEnumerable<string> GetStyles()
        {
            return Root.Descendants(StyleElementName).Elements(StyleNameElementName).Select(n => n.Value);
        }

        public IEnumerable<string> GetCategories(string stylename)
        {
            return Root.Descendants(StyleElementName).Where(n => n.Element(StyleNameElementName).Value == stylename)
                       .Descendants(CategoryName).Select(n => n.Value);
        }

        public void UpdateXml(string styleName, IEnumerable<string> categories)
        {
            if (GetStyles().Contains(styleName))
            {
                Root.Descendants(StyleElementName).Where(n => n.Element(StyleNameElementName).Value == styleName)
                    .Remove();
            }

            var styleList = Root.Element(StyleListName);
            if (styleList == null)
            {
                styleList = new XElement(StyleListName);
                Root.Add(styleList);
            }

            var newStyle = new XElement(StyleElementName, new XElement(StyleNameElementName, styleName));
            styleList.Add(newStyle);
            var categoryList = new XElement(CategoryListName);
            foreach (var category in categories)
            {
                categoryList.Add(new XElement(CategoryName, category));
            }

            newStyle.Add(categoryList);
        }

        public void UpdateCategoryNames()
        {
            foreach (var (oldName, newName) in _updateCategoryNamesMapping)
            {
                Root.Descendants(CategoryName)
                    .Where(c => c.Value.Equals(oldName, StringComparison.OrdinalIgnoreCase))
                    .ForEach(n => n.SetValue(newName));
            }
        }

        public void RemoveStyle(string styleName)
        {
            if (GetStyles().Contains(styleName))
            {
                Root.Descendants(StyleElementName).Where(n => n.Element(StyleNameElementName).Value == styleName)
                    .Remove();
            }
        }

        private void AddStyleCategory(XElement styleList, string categoryName, IEnumerable<string> styles)
        {
            foreach (var styleName in styles)
            {
                var styleElement = GetXElement(styleName, new[] { categoryName, "Document" });
                styleList.Add(styleElement);
            }
        }

        private XElement GetXElement(string styleName, string[] categories)
        {
            try
            {
                var newStyle = new XElement(StyleElementName, new XElement(StyleNameElementName, styleName));
                var categoryList = new XElement(CategoryListName);
                foreach (var category in categories)
                {
                    categoryList.Add(new XElement(CategoryName, category));
                }

                newStyle.Add(categoryList);
                return newStyle;
            }
            catch (Exception)
            {
                return new XElement(StyleElementName);
            }
        }
    }
}
