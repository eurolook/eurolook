﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Eurolook.Data.Models;
using Eurolook.Data.Models.AuthorRoles;

namespace Eurolook.Data.Xml
{
    public class AuthorCustomXml : XDocument
    {
        public static readonly string RootName = "Author";
        public static readonly string RoleAttr = "Role";
        public static readonly string AuthorRoleNameAttribute = "AuthorRoleName";
        public static readonly string AuthorRoleIdAttribute = "AuthorRoleId";
        public static readonly string OriginalAuthorRoleIdAttribute = "OriginalAuthorRoleId";
        public static readonly string OriginalAuthorRoleNameAttribute = "OriginalAuthorRoleName";
        public static readonly string ContentControlTitleAttribute = "ContentControlTitle";

        public AuthorCustomXml(XDocument xDoc)
            : base(xDoc)
        {
        }

        public AuthorCustomXml(AuthorCustomXmlCreationInfo authorCustomXmlCreationInfo)
        {
            var root = new XElement(RootName);

            if (authorCustomXmlCreationInfo.IsMainAuthor)
            {
                root.Add(new XAttribute(RoleAttr, "Creator"));
            }

            AddAuthorRole(root, authorCustomXmlCreationInfo.AuthorRole);
            AddOriginalAuthorRole(root, authorCustomXmlCreationInfo.OriginalAuthorRole);
            AddContentControlTitle(root, authorCustomXmlCreationInfo.ContentControlTitle);
            Add(root);

            var author = authorCustomXmlCreationInfo.Author;
            var jobAssignment = authorCustomXmlCreationInfo.JobAssignment;
            var language = authorCustomXmlCreationInfo.Language;
            // ReSharper disable once PossibleNullReferenceException
            Root.Add(new XElement("Id", author.Id));
            Root.Add(GetAuthorNames(author, language));
            Root.Add(new XElement("Initials", author.Initials));
            Root.Add(new XElement("Gender", author.Gender));
            Root.Add(new XElement("Email", author.Email));

            // Job Assignment
            Root.Add(new XElement("Service", jobAssignment.Service));
            Root.Add(GetFunctionElement(author, jobAssignment, language));
            Root.Add(new XElement("WebAddress", jobAssignment.WebAddress));
            Root.Add(new XElement("FunctionalMailbox", jobAssignment.FunctionalMailbox));
            Root.Add(new XElement("InheritedWebAddress", jobAssignment.InheritedWebAddress));
            AttachOrgaEntities(Root, jobAssignment);
            Root.Add(new XElement("Hierarchy", GetHierarchy(jobAssignment)));
            Root.Add(new XElement("Addresses", GetAddresses(jobAssignment, author.Workplaces)));

            var assignment = jobAssignment as JobAssignment;
            Root.Add(new XElement("JobAssignmentId", assignment?.Id));

            // Workplaces
            var mainWp = author.Workplaces.FirstOrDefault(wp => wp.Id == author.MainWorkplaceId);
            if (mainWp != null)
            {
                Root.Add(GetWorkplaceElement("MainWorkplace", mainWp, author));
            }
            else
            {
                Root.Add(new XElement("MainWorkplace"));
            }

            Root.Add(GetWorkplaces(author));
        }

        public bool HasCreatorAttribute => Root?.Attribute(RoleAttr)?.Value == "Creator";

        public Guid AuthorId => Guid.Parse(Root.Element("Id").Value);

        public string Email => Root?.Element("Email")?.Value;

        public string DocumentScriptFullName =>
            Root?.Element("Names")?.Element("DocumentScript")?.Element("FullName")?.Value;

        public Guid? JobAssignmentId
        {
            get
            {
                var jobAssignmentIdElement = Root?.Element("JobAssignmentId");
                if (jobAssignmentIdElement == null)
                {
                    return null;
                }

                if (Guid.TryParse(jobAssignmentIdElement.Value, out var result))
                {
                    return result;
                }

                return null;
            }
        }

        public string AuthorRoleName => Root?.Attribute(AuthorRoleNameAttribute)?.Value;

        public Guid? AuthorRoleId
        {
            get
            {
                if (Guid.TryParse(Root?.Attribute(AuthorRoleIdAttribute)?.Value, out var result))
                {
                    return result;
                }

                return null;
            }
        }

        public Guid? OriginalAuthorRoleId
        {
            get
            {
                if (Guid.TryParse(Root?.Attribute(OriginalAuthorRoleIdAttribute)?.Value, out var result))
                {
                    return result;
                }

                return null;
            }
        }

        public string OriginalAuthorRoleName => Root?.Attribute(OriginalAuthorRoleNameAttribute)?.Value;

        public string ContentControlTitle => Root?.Attribute(ContentControlTitleAttribute)?.Value;

        public string OrgaEntity1Name => Root?.Element("OrgaEntity1")?.Element("Name")?.Value;

        public string OrgaEntity1Id => Root?.Element("OrgaEntity1")?.Element("Id")?.Value;

        public string Service => Root?.Element("Service")?.Value;

        public string FunctionText => Root?.Element("Function")?.Value;

        public string AuthorLatinFirstName => LatinNamesElement?.Element("FirstName")?.Value;

        public string AuthorLatinLastName => LatinNamesElement?.Element("LastName")?.Value;

        private XElement LatinNamesElement => Root?.Element("Names")?.Element("Latin");

        private static XElement GetFunctionElement(Author author, IJobAssignment jobAssignment, Language lang)
        {
            string functionText = null;
            string functionHeaderText = null;
            var showInSignature = true;
            var showInHeader = false;
            string adCode = null;

            if (jobAssignment.PredefinedFunction != null)
            {
                adCode = jobAssignment.PredefinedFunction.AdReference;
                showInSignature = jobAssignment.PredefinedFunction.ShowInSignature;
                showInHeader = jobAssignment.PredefinedFunction.ShowInHeader;

                if (author.IsFemale)
                {
                    if (jobAssignment.PredefinedFunction.FunctionFemale != null)
                    {
                        functionText = jobAssignment.PredefinedFunction.FunctionFemale.Value;
                    }

                    if (jobAssignment.PredefinedFunction.FunctionHeaderFemale != null)
                    {
                        functionHeaderText = jobAssignment.PredefinedFunction.FunctionHeaderFemale.Value;
                    }
                }

                if ((functionText == null) && (jobAssignment.PredefinedFunction.Function != null))
                {
                    functionText = jobAssignment.PredefinedFunction.Function.Value;
                }

                if ((functionHeaderText == null) && (jobAssignment.PredefinedFunction.FunctionHeader != null))
                {
                    functionHeaderText = jobAssignment.PredefinedFunction.FunctionHeader.Value;
                }
            }
            else
            {
                var authorFunction = jobAssignment.Functions.FirstOrDefault(af => af.LanguageId == lang.Id);
                if (string.IsNullOrEmpty(authorFunction?.Function))
                {
                    authorFunction =
                        jobAssignment.Functions.FirstOrDefault(
                            af => (af.Language != null) && (af.Language.Name == "EN"));
                }

                functionText = authorFunction != null ? authorFunction.Function : "";
            }

            return new XElement(
                "Function",
                functionText,
                new XAttribute("ADCode", adCode ?? ""),
                new XAttribute("ShowInSignature", showInSignature),
                new XAttribute("ShowInHeader", showInHeader),
                new XAttribute("HeaderText", functionHeaderText ?? ""));
        }

        private static XElement GetWorkplaces(Author author)
        {
            return new XElement(
                "Workplaces",
                author.Workplaces.Select(w => GetWorkplaceElement("Workplace", w, author)));
        }

        private static XElement GetWorkplaceElement(string name, Workplace w, Author author)
        {
            return new XElement(
                name,
                new XAttribute("IsMain", author.MainWorkplaceId == w.Id),
                new XElement("AddressId", w.AddressId),
                new XElement(
                    "Fax",
                    !string.IsNullOrWhiteSpace(w.FaxExtension) ? w.Address.PhoneNumberPrefix + w.FaxExtension : null),
                new XElement(
                    "Phone",
                    !string.IsNullOrWhiteSpace(w.PhoneExtension)
                        ? w.Address.PhoneNumberPrefix + w.PhoneExtension
                        : null),
                new XElement("Office", w.Office));
        }

        /// <summary>
        /// create language properties for latin, greek and cyrillic script.
        /// the documentScript element contains the name of the current language and additional a fullname element
        /// </summary>
        /// <param name="author">author data to be used</param>
        /// <param name="lang">language to be used</param>
        /// <returns>XElement containing the XML</returns>
        private static XElement GetAuthorNames(Author author, Language lang)
        {
            // element names
            const string documentScript = "DocumentScript";
            const string firstName = "FirstName";
            const string lastName = "LastName";
            const string fullName = "FullName";

            XElement documentScriptElement;
            if (lang.Name.Equals("BG", StringComparison.InvariantCultureIgnoreCase)
                && !string.IsNullOrEmpty(author.BulgarianFullNameNonBreaking))
            {
                documentScriptElement = new XElement(
                    documentScript,
                    new XElement(firstName, author.BulgarianFirstName),
                    new XElement(lastName, author.BulgarianLastNameNonBreaking),
                    new XElement(fullName, author.BulgarianFullNameNonBreaking));
            }
            else if (lang.Name.Equals("EL", StringComparison.InvariantCultureIgnoreCase)
                     && !string.IsNullOrEmpty(author.GreekFullNameNonBreaking))
            {
                documentScriptElement = new XElement(
                    documentScript,
                    new XElement(firstName, author.GreekFirstName),
                    new XElement(lastName, author.GreekLastNameNonBreaking),
                    new XElement(fullName, author.GreekFullNameNonBreaking));
            }
            else
            {
                documentScriptElement = new XElement(
                    documentScript,
                    new XElement(firstName, author.LatinFirstName),
                    new XElement(lastName, author.LatinLastNameNonBreaking),
                    new XElement(fullName, author.LatinFullNameNonBreaking));
            }

            return new XElement(
                "Names",
                new XElement(
                    "Latin",
                    new XElement(firstName, author.LatinFirstName),
                    new XElement(lastName, author.LatinLastNameNonBreaking)),
                new XElement(
                    "Greek",
                    new XElement(firstName, author.GreekFirstName),
                    new XElement(lastName, author.GreekLastNameNonBreaking)),
                new XElement(
                    "Cyrillic",
                    new XElement(firstName, author.BulgarianFirstName),
                    new XElement(lastName, author.BulgarianLastNameNonBreaking)),
                documentScriptElement);
        }

        private static OrgaEntity GetOrgaEntityOfLevel(int level, IJobAssignment jobAssignment)
        {
            var entity = jobAssignment.OrgaEntity;
            while (entity != null)
            {
                if (entity.LogicalLevel == level)
                {
                    return entity;
                }

                entity = entity.SuperEntity;
            }

            return null;
        }

        private static object[] GetOrgaEntityChildren(OrgaEntity orgaEntity)
        {
            if (orgaEntity == null)
            {
                return new object[0];
            }

            return new object[]
            {
                new XElement("Id", orgaEntity.Id),
                new XElement("LogicalLevel", orgaEntity.LogicalLevel),
                new XElement("Name", orgaEntity.Name),
                new XElement("HeadLine1", GetHeadLine1(orgaEntity)),
                new XElement("HeadLine2", GetHeadLine2(orgaEntity)),
                new XElement("PrimaryAddressId", orgaEntity.PrimaryAddressId),
                new XElement("SecondaryAddressId", orgaEntity.SecondaryAddressId),
                new XElement("WebAddress", orgaEntity.WebAddress),
                new XElement("InheritedWebAddress", orgaEntity.InheritedWebAddress),
                new XElement("ShowInHeader", orgaEntity.ShowInHeader)
            };
        }

        private static string GetHeadLine1(OrgaEntity entity)
        {
            if (entity.Header?.Value != null)
            {
                var header = entity.Header.Value;
                if ((header != null) && header.Contains("<>"))
                {
                    return header.Split(new[] { "<>" }, StringSplitOptions.RemoveEmptyEntries)[0];
                }

                return header;
            }

            return "";
        }

        private static string GetHeadLine2(OrgaEntity entity)
        {
            if (entity.Header?.Value != null)
            {
                var header = entity.Header.Value;
                if (header.Contains("<>"))
                {
                    return header.Split(new[] { "<>" }, StringSplitOptions.RemoveEmptyEntries)[1];
                }

                return "";
            }

            return "";
        }

        private static object GetAddresses(IJobAssignment jobAssignment, IEnumerable<Workplace> workplaces)
        {
            var addresses = new Dictionary<Guid, Address>();

            // add OrgaEntity adresses
            var orgaEntity = jobAssignment.OrgaEntity;
            while (orgaEntity != null)
            {
                if ((orgaEntity.PrimaryAddressId != null) && (orgaEntity.PrimaryAddress != null))
                {
                    addresses[orgaEntity.PrimaryAddressId.Value] = orgaEntity.PrimaryAddress;
                }

                if ((orgaEntity.SecondaryAddressId != null) && (orgaEntity.SecondaryAddress != null))
                {
                    addresses[orgaEntity.SecondaryAddressId.Value] = orgaEntity.SecondaryAddress;
                }

                orgaEntity = orgaEntity.SuperEntity;
            }

            // add Workplace addresses
            foreach (var wp in workplaces)
            {
                if ((wp.Address != null) && !addresses.ContainsKey(wp.AddressId))
                {
                    addresses[wp.AddressId] = wp.Address;
                }
            }

            return
                addresses.Values.Select(
                    a =>
                        new XElement(
                            "Address",
                            new XElement("Id", a.Id),
                            new XElement("Name", a.Name),
                            new XElement("PhoneNumberPrefix", a.PhoneNumberPrefix),
                            new XElement("TranslatedName", a.TranslatedName),
                            new XElement("Location", a.Location),
                            new XElement("Footer", a.Footer))).ToList();
        }

        private void AddAuthorRole(XElement root, AuthorRole authorRole)
        {
            // cannot use existing role attribute due to backwards compatibility issues
            if (authorRole == null)
            {
                return;
            }

            root.Add(new XAttribute(AuthorRoleNameAttribute, authorRole.Name));
            root.Add(new XAttribute(AuthorRoleIdAttribute, authorRole.Id));
        }

        private void AddOriginalAuthorRole(XElement root, AuthorRole originalAuthorRole)
        {
            if (originalAuthorRole == null)
            {
                return;
            }

            root.Add(new XAttribute(OriginalAuthorRoleNameAttribute, originalAuthorRole.Name));
            root.Add(new XAttribute(OriginalAuthorRoleIdAttribute, originalAuthorRole.Id));
        }

        private void AddContentControlTitle(XElement root, string contentControlTitle)
        {
            if (contentControlTitle == null)
            {
                return;
            }

            contentControlTitle = new string(contentControlTitle.ToCharArray().Where(XmlConvert.IsXmlChar).ToArray());
            root.Add(new XAttribute(ContentControlTitleAttribute, contentControlTitle));
        }

        private void AttachOrgaEntities(XElement parent, IJobAssignment jobAssignment)
        {
            if (jobAssignment.OrgaEntity != null)
            {
                for (var i = 1; i <= 3; i++) ////author.OrgaEntity.LogicalLevel; i++)
                {
                    var orgaEntity = GetOrgaEntityOfLevel(i, jobAssignment);
                    parent.Add(new XElement($"OrgaEntity{i}", GetOrgaEntityChildren(orgaEntity)));
                }
            }
        }

        private IList<XElement> GetHierarchy(IJobAssignment jobAssignment)
        {
            var result = new List<XElement>();
            AddHierarchyElement(result, jobAssignment.OrgaEntity);
            result.Reverse();
            return result;
        }

        private void AddHierarchyElement(IList<XElement> list, OrgaEntity entity)
        {
            if ((entity != null) && (list != null) && (entity.LogicalLevel > 0))
            {
                list.Add(new XElement("OrgaEntity", GetOrgaEntityChildren(entity)));
                AddHierarchyElement(list, entity.SuperEntity);
            }
        }
    }
}
