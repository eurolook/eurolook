﻿using System;
using System.Collections.Generic;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.Data.Xml
{
    public interface IEurolookProperties : IReadOnlyEurolookProperties
    {
        new string ProductCustomizationId { get; set; }

        new string CreationVersion { get; set; }

        new DateTime? CreationDate { get; set; }

        new string CreationLanguage { get; set; }

        string CreationNote { get; set; }

        new string EditVersion { get; set; }

        new DateTime? EditDate { get; set; }

        new Guid? DocumentModelId { get; set; }

        new string DocumentModelName { get; set; }

        new Guid? CustomTemplateId { get; set; }

        new string CustomTemplateName { get; set; }

        new DateTime? DocumentDate { get; set; }

        new EurolookCompatibilityMode CompatibilityMode { get; set; }

        string DocumentVersion { get; set; }

        Dictionary<string, MetadataProperty> DocumentMetadata { get; set; }
    }
}
