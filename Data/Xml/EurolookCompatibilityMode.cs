namespace Eurolook.Data.Xml
{
    public enum EurolookCompatibilityMode
    {
        Unknown = 0,
        Eurolook4X,
        Eurolook4XCompatibility,
        Eurolook46,
        Eurolook46Compatibility,
        Eurolook10,
    }
}
