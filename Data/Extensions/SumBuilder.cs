using System;
using System.Linq.Expressions;

namespace Eurolook.Data.Extensions
{
    public static class SumBuilder
    {
        public static Expression<Func<T, int>> Zero<T>()
        {
            return f => 0;
        }

        public static Expression<Func<T, int>> Add<T>(
            this Expression<Func<T, int>> expr1,
            Expression<Func<T, int>> expr2)
        {
            var invocationExpression = Expression.Invoke(expr2, expr1.Parameters);
            return Expression.Lambda<Func<T, int>>(Expression.Add(expr1.Body, invocationExpression), expr1.Parameters);
        }
    }
}
