using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Eurolook.Common.Log;
using Eurolook.Data.Models;

namespace Eurolook.Data.Extensions
{
    public static class QueryExtensions
    {
        public static IQueryable<T> Paginate<T>(this IQueryable<T> query, IPaginationInfo paginationInfo)
        {
            return query.Skip((int)paginationInfo.From)
                        .Take((int)paginationInfo.PageSize);
        }

        public static IQueryable<T> OrderByAndPaginate<T>(this IQueryable<T> query, IPaginationInfo paginationInfo)
            where T : Identifiable
        {
            return query.OrderBy(paginationInfo)
                        .Paginate(paginationInfo);
        }

        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> query, IPaginationInfo paginationInfo)
            where T : Identifiable
        {
            try
            {
                var typeParams = new[] { Expression.Parameter(typeof(T), "") };
                var propertyInfo = typeof(T).GetProperty(
                    paginationInfo.OrderProperty,
                    BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

                string ordering = paginationInfo.OrderAscending ? "OrderBy" : "OrderByDescending";

                return (IOrderedQueryable<T>)query.Provider.CreateQuery(
                    Expression.Call(
                        typeof(Queryable),
                        ordering,
                        new[] { typeof(T), propertyInfo.PropertyType },
                        query.Expression,
                        Expression.Lambda(
                            Expression.Property(typeParams[0], propertyInfo),
                            typeParams)));
            }
            catch (Exception ex)
            {
                LogManager.GetLogger().Error("Failed to execute OrderBy.", ex);
                return query.OrderBy(x => x.Id);
            }
        }

        public static IOrderedQueryable<TSource> OrderBy<TSource, TKey>(this IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, bool ascending)
        {
            return ascending ? source.OrderBy(keySelector) : source.OrderByDescending(keySelector);
        }
    }
}
