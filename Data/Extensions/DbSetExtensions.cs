﻿using System.Linq;
using Eurolook.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Eurolook.Data.Extensions
{
    public static class DbSetExtensions
    {
        public static void AddOrUpdate<TEntity>(this DbSet<TEntity> dbSet, TEntity entity)
            where TEntity : Identifiable
        {
            if (entity == null)
            {
                return;
            }

            if (dbSet.IgnoreQueryFilters().Any(x => x.Id == entity.Id))
            {
                dbSet.Update(entity);
            }
            else
            {
                dbSet.Add(entity);
            }
        }
    }
}
