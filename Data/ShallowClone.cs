using Eurolook.Common;
using Eurolook.Data.Models;

namespace Eurolook.Data
{
    public static class ShallowClone
    {
        public static TT Clone<TT>(TT entity)
            where TT : Identifiable, new()
        {
            var clone = new TT();
            CopyProperties<TT>.Copy(entity, clone);
            return clone;
        }
    }
}
