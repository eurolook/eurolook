﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using Eurolook.Common.Log;
using Eurolook.Data.Extensions;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;
using LinqKit;
using Microsoft.EntityFrameworkCore;

namespace Eurolook.Data
{
    public abstract class EurolookServerDatabase : EurolookDatabase<EurolookServerContext>
    {
        private readonly string _connectionString;

        protected EurolookServerDatabase(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void Migrate()
        {
            using (var context = GetContext())
            {
                context.Migrate();
            }
        }

        /// <summary>
        /// Creates or revives a user profile.
        /// </summary>
        /// <param name="login">The user's Windows login.</param>
        /// <param name="email">The user's email address.</param>
        /// <returns>Returns true when the profile was created or revived, false when it already exists.</returns>
        public bool CreateOrReviveUserProfile(string login, string email = null)
        {
            using (var context = GetContext())
            {
                // first check if the profile already exists
                var existingUser = context.Users.FirstOrDefault(u => u.Login.ToLower() == login.ToLower());
                if (existingUser != null)
                {
                    // and revive it if it was deleted
                    if (existingUser.Deleted)
                    {
                        ReviveUserProfile(context, existingUser.Id);
                        return true;
                    }

                    return false;
                }

                // otherwise create a new one
                var settings = new UserSettings();
                settings.Init();
                settings.DataSyncDelayMinutes = 0;
                settings.DataSyncIntervalMinutes = 60;

                var author = new Author();
                author.Init();
                author.Email = email;

                var user = new User();
                user.Init();
                user.Login = login;
                user.Self = author;
                user.Settings = settings;

                var addresses = context.Addresses.Where(a => a.IsDefault.HasValue && a.IsDefault.Value).ToList();
                if (addresses.Count == 0)
                {
                    addresses.Add(context.Addresses.First());
                }

                foreach (var address in addresses)
                {
                    var workplace = new Workplace();
                    workplace.Init();
                    workplace.Address = address;
                    workplace.Author = author;
                    author.Workplaces.Add(workplace);
                }

                var firstWorkplace = author.Workplaces.FirstOrDefault();
                if (firstWorkplace != null)
                {
                    author.MainWorkplaceId = firstWorkplace.Id;
                }

                context.Users.Add(user);
                context.SaveChanges();
                return true;
            }
        }

        public bool DeleteUserProfile(string userName)
        {
            using (var context = GetContext())
            {
                var userId = context.Users.Where(u => u.Login == userName).Select(u => u.Id).FirstOrDefault();
                if (userId != Guid.Empty)
                {
                    DeleteUserProfile(context, userId);
                    return true;
                }

                return false;
            }
        }

        public void DeleteUserProfile(Guid id)
        {
            using (var context = GetContext())
            {
                DeleteUserProfile(context, id);
            }
        }

        public bool DeleteUserProfilePermanently(string userName)
        {
            using (var context = GetContext())
            {
                var userId = context.Users.Where(u => u.Login == userName).Select(u => u.Id).FirstOrDefault();
                if (userId != Guid.Empty)
                {
                    DeleteUserProfilePermanently(context, userId);
                    return true;
                }

                return false;
            }
        }

        public void DeleteUserProfilePermanently(Guid id)
        {
            using (var context = GetContext())
            {
                DeleteUserProfilePermanently(context, id);
            }
        }

        public void ReviveUserProfile(Guid id)
        {
            using (var context = GetContext())
            {
                ReviveUserProfile(context, id);
            }
        }

        public void ReviveUserProfile(EurolookServerContext context, Guid id)
        {
            var user = context.Users.First(u => u.Id == id);
            // 1. AuthorFunctions
            foreach (var af in context.JobFunctions.Where(a => a.AuthorId == user.SelfId))
            {
                af.SetDeletedFlag(false);
            }

            // 2. Workplaces
            foreach (var wp in context.Workplaces.Where(a => a.AuthorId == user.SelfId))
            {
                wp.SetDeletedFlag(false);
            }

            // 3. UserAuthors
            // Me referencing other authors
            foreach (var ua in context.UserAuthors.Where(u => u.UserId == user.Id))
            {
                ua.SetDeletedFlag(false);
            }

            // other Users referencing me
            foreach (var ua in context.UserAuthors.Where(u => u.AuthorId == user.SelfId))
            {
                ua.SetDeletedFlag(false);
            }

            // 4. DeviceSettings
            foreach (var ds in context.DeviceSettings.Where(d => d.UserSettingsId == user.SettingsId))
            {
                ds.SetDeletedFlag(false);
            }

            // 5. DocumentModelSettings
            foreach (var dms in context.DocumentModelSettings.Where(d => d.UserSettingsId == user.SettingsId))
            {
                dms.SetDeletedFlag(false);
            }

            // 6. UserFeedbacks
            context.UserFeedbacks.RemoveRange(context.UserFeedbacks.Where(u => u.UserId == user.Id));
            // 7. User
            user.SetDeletedFlag(false);
            // 8. Authors
            var author = context.Authors.First(a => a.Id == user.SelfId);
            author.SetDeletedFlag(false);
            // 9. UserSettings
            var settings = context.UserSettings.First(u => u.Id == user.SettingsId);
            settings.SetDeletedFlag(false);
            context.SaveChanges();
        }

        public List<Translation> GetTranslations(Guid textId)
        {
            using (var context = GetContext())
            {
                return context.Translations.Where(x => x.TextId == textId).ToList();
            }
        }

        public Translation GetTranslation(Guid textId, Guid langId)
        {
            using (var context = GetContext())
            {
                return context.Translations.FirstOrDefault(x => x.TextId == textId && x.LanguageId == langId);
            }
        }

        public string FindUserName(string fullLoginName)
        {
            string login = fullLoginName;
            if (fullLoginName.Contains("\\"))
            {
                login = fullLoginName.Substring(fullLoginName.LastIndexOf("\\", StringComparison.Ordinal) + 1);
            }

            using (var context = GetContext())
            {
                string suffix = $"\\{login}";
                var user = context.Users.FirstOrDefault(u => u.Login.EndsWith(suffix));
                return user?.Login;
            }
        }

        public bool TryMigrateUserName(string userName, string newUserName)
        {
            using (var context = GetContext())
            {
                var user = context.Users.FirstOrDefault(u => u.Login.ToLower() == userName.ToLower());
                if (user != null)
                {
                    user.Login = newUserName;
                    user.SetModification(ModificationType.ServerModification);
                    context.SaveChanges();
                    LogManager.GetLogger().Info($"Migrated user name '{userName}' to '{newUserName}'.");
                    return true;
                }

                return false;
            }
        }

        protected void DeleteUserProfile(EurolookServerContext context, Guid id)
        {
            var modificationDate = DateTime.UtcNow;
            var user = context.Users.First(u => u.Id == id);

            // JobFunctions
            foreach (var jf in context.JobFunctions.Where(j => j.AuthorId == user.SelfId))
            {
                jf.SetDeletedFlag(modificationDate);
            }

            // Workplaces
            foreach (var wp in context.Workplaces.Where(a => a.AuthorId == user.SelfId))
            {
                wp.SetDeletedFlag(modificationDate);
            }

            // Job Assignments
            foreach (var ja in context.JobAssignments.Where(j => j.AuthorId == user.SelfId).ToArray())
            {
                ja.SetDeletedFlag(modificationDate);
                foreach (var jaf in context.JobFunctions.Where(j => j.JobAssignmentId == ja.Id).ToArray())
                {
                    jaf.SetDeletedFlag(modificationDate);
                }
            }

            // Superiors
            foreach (var userSuperior in context.UserSuperiors.Where(u => u.UserId == user.Id))
            {
                userSuperior.SetDeletedFlag(modificationDate);
            }

            // UserAuthors
            // Me referencing other authors
            foreach (var ua in context.UserAuthors.Where(u => u.UserId == user.Id))
            {
                ua.SetDeletedFlag(modificationDate);
            }

            // other Users referencing me
            foreach (var ua in context.UserAuthors.Where(u => u.AuthorId == user.SelfId))
            {
                ua.SetDeletedFlag(modificationDate);
            }

            // DeviceSettings
            foreach (var ds in context.DeviceSettings.Where(d => d.UserSettingsId == user.SettingsId))
            {
                ds.SetDeletedFlag(modificationDate);
            }

            // DocumentModelSettings
            foreach (var dms in context.DocumentModelSettings.Where(d => d.UserSettingsId == user.SettingsId))
            {
                dms.SetDeletedFlag(modificationDate);
            }

            // UserTemplates
            foreach (var userTemplate in context.UserTemplates.Where(d => d.UserId == user.Id))
            {
                userTemplate.SetDeletedFlag(modificationDate);
            }

            // TemplateOwnership
            foreach (var templateOwner in context.TemplateOwners.Where(o => o.UserId == user.Id))
            {
                templateOwner.SetDeletedFlag(modificationDate);
            }

            // UserFeedbacks
            context.UserFeedbacks.RemoveRange(context.UserFeedbacks.Where(u => u.UserId == user.Id).ToArray());
            // User
            user.SetDeletedFlag(modificationDate);
            // Authors
            var author = context.Authors.First(a => a.Id == user.SelfId);
            author.SetDeletedFlag(modificationDate);
            // UserSettings
            var settings = context.UserSettings.First(u => u.Id == user.SettingsId);
            settings.SetDeletedFlag(modificationDate);

            context.SaveChanges();
        }

        protected void DeleteUserProfilePermanently(EurolookServerContext context, Guid id)
        {
            var user = context.Users.First(u => u.Id == id);

            // "nullify" references
            foreach (var userAuthor in context.UserAuthors.Where(ua => ua.AuthorId == user.SelfId).ToArray())
            {
                userAuthor.Author = null;
                userAuthor.SetDeletedFlag();
            }

            foreach (var userSuperior in context.UserSuperiors.Where(us => us.AuthorId == user.SelfId).ToArray())
            {
                userSuperior.Author = null;
                userSuperior.SetDeletedFlag();
            }

            var devices = context.DeviceSettings.Where(d => d.UserSettingsId == user.SettingsId).ToArray();
            var deviceIds = devices.Select(x => x.Id).ToArray();
            foreach (var performanceLog in context.PerformanceLogs
                                                  .Where(
                                                      x => x.DeviceSettingsId != null
                                                           && deviceIds.Contains(x.DeviceSettingsId.Value)))
            {
                performanceLog.DeviceSettingsId = null;
            }

            // delete profile
            context.Workplaces.RemoveRange(context.Workplaces.Where(a => a.AuthorId == user.SelfId));
            foreach (var jobAssignment in context.JobAssignments.Where(j => j.AuthorId == user.SelfId).ToArray())
            {
                var job = jobAssignment; // copy local
                context.JobFunctions.RemoveRange(context.JobFunctions.Where(j => j.JobAssignmentId == job.Id));
                context.JobAssignments.Remove(job);
            }

            context.UserBricks.RemoveRange(context.UserBricks.Where(x => x.UserId == user.Id));
            context.JobFunctions.RemoveRange(context.JobFunctions.Where(j => j.AuthorId == user.SelfId));
            context.UserAuthors.RemoveRange(context.UserAuthors.Where(u => u.UserId == user.Id));
            context.UserSuperiors.RemoveRange(context.UserSuperiors.Where(x => x.UserId == user.Id));
            context.DeviceSettings.RemoveRange(devices);
            context.DocumentModelSettings.RemoveRange(
                context.DocumentModelSettings.Where(d => d.UserSettingsId == user.SettingsId));
            context.UserTemplates.RemoveRange(context.UserTemplates.Where(u => u.UserId == user.Id));
            context.BrickSettings.RemoveRange(context.BrickSettings.Where(d => d.UserSettingsId == user.SettingsId));
            context.UserFeedbacks.RemoveRange(context.UserFeedbacks.Where(u => u.UserId == user.Id));
            context.Users.Remove(user);
            context.Authors.Remove(context.Authors.First(a => a.Id == user.SelfId));
            context.UserSettings.Remove(context.UserSettings.First(u => u.Id == user.SettingsId));
            context.TemplateOwners.RemoveRange(context.TemplateOwners.Where(t => t.UserId == user.Id));
            context.SaveChanges();
        }

        protected override EurolookServerContext GetContext()
        {
            return new EurolookServerContext(_connectionString);
        }
    }
}
