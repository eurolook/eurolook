﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Eurolook.Common.Log;
using Eurolook.Data.ExceptionLog;
using Eurolook.Data.Models;
using Eurolook.Data.Models.AuthorRoles;
using Eurolook.Data.Models.ComRef;
using Eurolook.Data.Models.Metadata;
using Eurolook.Data.Models.SharePointTermStore;
using Eurolook.Data.Models.TemplateStore;
using Microsoft.EntityFrameworkCore;

namespace Eurolook.Data
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Setters are used implicit.")]
    public abstract class EurolookContext : DbContext, ICanLog
    {
        public DbSet<Address> Addresses { get; set; }

        public DbSet<Author> Authors { get; set; }

        public DbSet<BrickCategory> BrickCategories { get; set; }

        public DbSet<BrickGroup> BrickGroups { get; set; }

        public DbSet<Brick> Bricks { get; set; }

        public DbSet<Language> Languages { get; set; }

        public DbSet<DocumentModelLanguage> DocumentModelLanguages { get; set; }

        public DbSet<DocumentModel> DocumentModels { get; set; }

        public DbSet<DocumentStructure> DocumentStructure { get; set; }

        public DbSet<OrgaEntity> OrgaEntities { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<UserGroup> UserGroups { get; set; }

        public DbSet<UserAuthor> UserAuthors { get; set; }

        public DbSet<UserSuperior> UserSuperiors { get; set; }

        public DbSet<UserTemplate> UserTemplates { get; set; }

        public DbSet<UserSettings> UserSettings { get; set; }

        public DbSet<Text> Texts { get; set; }

        public DbSet<Translation> Translations { get; set; }

        public DbSet<BrickText> BrickTexts { get; set; }

        public DbSet<Resource> Resources { get; set; }

        public DbSet<LocalisedResource> LocalisedResources { get; set; }

        public DbSet<BrickResource> BrickResources { get; set; }

        public DbSet<Workplace> Workplaces { get; set; }

        public DbSet<PersonName> PersonNames { get; set; }

        public DbSet<CharacterMapping> CharacterMappings { get; set; }

        public DbSet<StyleShortcut> StyleShortcuts { get; set; }

        public DbSet<DocumentModelSettings> DocumentModelSettings { get; set; }

        public DbSet<ActionLog> ActionLogs { get; set; }

        public DbSet<ExceptionBucket> ExceptionBuckets { get; set; }

        public DbSet<ExceptionLogEntry> ExceptionLogEntries { get; set; }

        public DbSet<ColorScheme> ColorSchemes { get; set; }

        public DbSet<DeviceSettings> DeviceSettings { get; set; }

        public DbSet<ActivityTrack> ActivityTracks { get; set; }

        public DbSet<JobAssignment> JobAssignments { get; set; }

        public DbSet<JobFunction> JobFunctions { get; set; }

        public DbSet<DocumentCategory> DocumentCategories { get; set; }

        public DbSet<TermStoreImport> TermStoreImports { get; set; }

        public DbSet<MetadataCategory> MetadataCategories { get; set; }

        public DbSet<MetadataDefinition> MetadataDefinitions { get; set; }

        public DbSet<DocumentModelMetadataDefinition> DocumentModelMetadataDefinitions { get; set; }

        public DbSet<AuthorRole> AuthorRoles { get; set; }

        public DbSet<DocumentModelAuthorRole> DocumentModelAuthorRoles { get; set; }

        public DbSet<PredefinedFunction> PredefinedFunctions { get; set; }

        public DbSet<BrickSettings> BrickSettings { get; set; }

        public DbSet<Notification> Notifications { get; set; }

        public DbSet<UserBrick> UserBricks { get; set; }

        public DbSet<PerformanceLog> PerformanceLogs { get; set; }

        public DbSet<Template> Templates { get; set; }

        public DbSet<TemplateFile> TemplateFiles { get; set; }

        public DbSet<TemplateOwner> TemplateOwners { get; set; }

        public DbSet<TemplatePublication> TemplatePublications { get; set; }

        public DbSet<SystemConfiguration> SystemConfigurations { get; set; }

        public DbSet<ComRefHeaderTextPattern> ComRefHeaderTextPatterns { get; set; }

        public DbSet<ComRefDgSetting> ComRefDgSettings { get; set; }

        public DbSet<ComRefException> ComRefExceptions { get; set; }

        public DbSet<SystemText> SystemTexts { get; set; }

        public DbSet<SystemResource> SystemResources { get; set; }

        public override int SaveChanges()
        {
            var entities = from e in ChangeTracker.Entries()
                           where e.State == EntityState.Added
                                 || e.State == EntityState.Modified
                           select e.Entity;
            foreach (var entity in entities)
            {
                var validationContext = new ValidationContext(entity);
                Validator.ValidateObject(entity, validationContext);
            }

            return base.SaveChanges();
        }

        public abstract bool IsServerContext { get; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // disable cascade delete
            foreach (var fk in modelBuilder.Model.GetEntityTypes()
                                           .SelectMany(t => t.GetForeignKeys())
                                           .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade))
            {
                fk.DeleteBehavior = DeleteBehavior.Restrict;
            }

            // Table-per-hierarchy
            modelBuilder.Entity<Brick>().ToTable("Bricks")
                        .HasDiscriminator<string>("Discriminator")
                        .HasValue<ContentBrick>(nameof(ContentBrick))
                        .HasValue<DynamicBrick>(nameof(DynamicBrick))
                        .HasValue<CommandBrick>(nameof(CommandBrick));

            // set indices
            modelBuilder.Entity<Address>().HasIndex(x => x.Name).IsUnique();
            modelBuilder.Entity<AuthorRole>().HasIndex(x => x.Name).IsUnique();
            modelBuilder.Entity<User>().HasIndex(x => x.Login).IsUnique();
            modelBuilder.Entity<UserGroup>().HasIndex(x => x.Name).IsUnique();
            modelBuilder.Entity<Text>().HasIndex(x => x.Alias).IsUnique();
            modelBuilder.Entity<MetadataCategory>().HasIndex(x => x.Name).IsUnique();
            modelBuilder.Entity<MetadataDefinition>().HasIndex(x => x.Name).IsUnique();
            modelBuilder.Entity<BrickText>()
                        .HasIndex(
                            x => new
                            {
                                x.BrickId,
                                x.TextId,
                            })
                        .HasName("IX_BrickTextUniqueness").IsUnique();
            modelBuilder.Entity<DocumentModelLanguage>()
                        .HasIndex(
                            x => new
                            {
                                x.DocumentModelId,
                                x.DocumentLanguageId,
                            })
                        .HasName("IX_DocumentModelDocumentLanguageUniqueness").IsUnique();
            modelBuilder.Entity<BrickResource>()
                        .HasIndex(
                            x => new
                            {
                                x.BrickId,
                                x.ResourceId,
                            })
                        .HasName("IX_BrickResourceUniqueness").IsUnique();
            modelBuilder.Entity<LocalisedResource>()
                        .HasIndex(
                            x => new
                            {
                                x.ResourceId,
                                x.LanguageId,
                            })
                        .HasName("IX_ResourceLanguageUniqueness").IsUnique();
            modelBuilder.Entity<Translation>()
                        .HasIndex(
                            x => new
                            {
                                x.TextId,
                                x.LanguageId,
                            })
                        .HasName("IX_TextLanguageUniqueness").IsUnique();
            modelBuilder.Entity<DocumentStructure>()
                        .HasIndex(
                            x => new
                            {
                                x.DocumentModelId,
                                x.BrickId,
                                x.RawPosition,
                                x.SectionId,
                            })
                        .HasName("IX_DocumentModelBrickPositionSectionUniqueness").IsUnique();

            modelBuilder.Entity<TemplateFile>()
                        .HasIndex(
                            x => new
                            {
                                x.TemplateId,
                                x.LanguageId,
                            })
                        .HasName("IX_TemplateLanguageUniqueness")
                        .IsUnique();

            modelBuilder.Entity<TemplateOwner>()
                        .HasIndex(
                            x => new
                            {
                                x.TemplateId,
                                x.UserId,
                            })
                        .HasName("IX_TemplateUserUniqueness")
                        .IsUnique();

            modelBuilder.Entity<TemplatePublication>()
                        .HasIndex(
                            x => new
                            {
                                x.TemplateId,
                                x.OrgaEntityId,
                            })
                        .HasName("IX_TemplateOrgaEntityUniqueness")
                        .IsUnique();

            modelBuilder.Entity<SystemConfiguration>()
                        .HasIndex(
                            x => new
                            {
                                x.Key,
                            })
                        .HasName("IX_SystemConfigurationKeyUniqueness")
                        .IsUnique();

            modelBuilder.Entity<PerformanceLog>()
                        .HasIndex(x => new
                        {
                            x.Event,
                            x.Date,
                            x.DeviceSettingsId,
                            x.Version,
                        })
                        .HasName("IX_PerformanceLogEntries");

            modelBuilder.Entity<ComRefHeaderTextPattern>().HasIndex(x => x.Name).IsUnique();
            modelBuilder.Entity<ComRefDgSetting>().HasIndex(x => x.DgName).IsUnique();

            base.OnModelCreating(modelBuilder);
        }
    }
}
