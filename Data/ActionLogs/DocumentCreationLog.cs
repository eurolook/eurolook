﻿namespace Eurolook.Data.ActionLogs
{
    public class DocumentCreationLog
    {
        public static readonly string ActionId = "CreateDocument";

        public string Language { get; set; }

        public bool InsertedRichBody { get; set; }
    }
}
