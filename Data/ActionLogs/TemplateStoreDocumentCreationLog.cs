﻿using System;

namespace Eurolook.Data.ActionLogs
{
    public class TemplateStoreDocumentCreationLog
    {
        public static readonly string ActionId = "CreateTemplateStoreDocument";

        public Guid TemplateId { get; set; }

        public string Language { get; set; }
    }
}
