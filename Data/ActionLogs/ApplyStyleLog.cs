﻿namespace Eurolook.Data.ActionLogs
{
    public class ApplyStyleLog
    {
        public static readonly string ActionId = "ApplyStyle";

        public string Method { get; set; }

        public string AppliedStyle { get; set; }
    }
}
