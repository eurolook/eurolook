﻿namespace Eurolook.Data.ActionLogs
{
    public class DocumentConversionLog
    {
        public static readonly string ActionId = "ConvertDocument";

        public string Language { get; set; }
    }
}
