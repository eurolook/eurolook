﻿using System;

namespace Eurolook.Data.ActionLogs
{
    public class ToolCommandExecutionLog : InsertionInfo
    {
        public static readonly string ActionId = "ExecuteToolCommand";
    }
}
