﻿using System;

namespace Eurolook.Data.ActionLogs
{
    public class InsertionInfo
    {
        public Guid BrickId { get; set; }

        public string BrickName { get; set; }

        public string Method { get; set; }
    }
}
