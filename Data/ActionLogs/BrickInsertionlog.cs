﻿using System;

namespace Eurolook.Data.ActionLogs
{
    public class BrickInsertionLog : InsertionInfo
    {
        public static readonly string ActionId = "InsertBrick";
    }
}
