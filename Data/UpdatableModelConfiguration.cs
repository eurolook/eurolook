﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Eurolook.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Eurolook.Data
{
    public static class UpdatableModelConfiguration
    {
        public static void ConfigureIgnoreDeletedFlags(ModelBuilder modelBuilder)
        {
            var entityTypes = GetEntityTypesDerivedFromUpdatable(modelBuilder);
            foreach (var entityType in entityTypes)
            {
                ApplyIgnoreDeletedFlagsQueryFilterForEntity(entityType, modelBuilder);
            }
        }

        private static IEnumerable<IMutableEntityType> GetEntityTypesDerivedFromUpdatable(ModelBuilder modelBuilder)
        {
            return modelBuilder.Model.GetEntityTypes().Where(et =>
            et.ClrType != null
            && et.BaseType == null
            && et.ClrType.IsSubclassOf(typeof(Updatable)));
        }

        private static void ApplyIgnoreDeletedFlagsQueryFilterForEntity(IMutableEntityType entityType, ModelBuilder modelBuilder)
        {
            var configureMethod = typeof(UpdatableModelConfiguration).GetTypeInfo().DeclaredMethods.Single(m => m.Name == nameof(ApplyIgnoreDeletedFlagsQueryFilter));
            var args = new object[] { modelBuilder };
            configureMethod.MakeGenericMethod(entityType.ClrType).Invoke(null, args);
        }

        private static void ApplyIgnoreDeletedFlagsQueryFilter<TEntity>(ModelBuilder modelBuilder)
         where TEntity : Updatable
        {
            var entity = modelBuilder.Entity<TEntity>();
            entity.HasQueryFilter(e => !e.Deleted);
        }
    }
}
