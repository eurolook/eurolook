﻿using System;

namespace Eurolook.Data
{
    /// <summary>
    /// Denotes that a property is a Translation for a Text property.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class TranslationForAttribute : Attribute
    {
        public TranslationForAttribute(string target)
        {
            Target = target;
        }

        public string Target { get; set; }
    }
}
