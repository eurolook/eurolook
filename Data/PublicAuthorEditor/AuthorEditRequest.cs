using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Eurolook.Data.PublicAuthorEditor
{
    public class AuthorEditRequest
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Token { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime CreatedUtc { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime? ExpiredUtc { get; set; }

        public string OriginatorEmail { get; set; }

        public string OriginatorName { get; set; }

        public string AddresseeEmail { get; set; }

        public string AddresseeName { get; set; }

        public bool IsApproved { get; set; }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        public int ExpirationDays => 3;

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        public string ExpiredDate => ExpiredUtc?.ToLocalTime().ToString(CultureInfo.CurrentCulture) ?? "";

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        public bool IsExpired => ExpiredUtc != null && ExpiredUtc < DateTime.UtcNow;
    }
}
