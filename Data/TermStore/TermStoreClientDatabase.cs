﻿namespace Eurolook.Data.TermStore
{
    public class TermStoreClientDatabase : AbstractTermStoreDatabase<EurolookClientContext>
    {
        protected override EurolookClientContext GetContext()
        {
            return new EurolookClientContext(ignoreDeletedFlags: true);
        }
    }
}
