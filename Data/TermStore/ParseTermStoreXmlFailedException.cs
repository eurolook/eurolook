﻿using System;
using System.Runtime.Serialization;

namespace Eurolook.Data.TermStore
{
    [Serializable]
    public class ParseTermStoreXmlFailedException : Exception
    {
        public ParseTermStoreXmlFailedException()
        {
        }

        public ParseTermStoreXmlFailedException(string message)
            : base(message)
        {
        }

        public ParseTermStoreXmlFailedException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected ParseTermStoreXmlFailedException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}
