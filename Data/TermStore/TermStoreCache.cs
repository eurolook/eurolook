using System;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Linq;
using Eurolook.Data.Models.SharePointTermStore;

namespace Eurolook.Data.TermStore
{
    public class TermStoreCache : ITermStoreCache
    {
        private readonly ITermStoreDatabase _termStoreDatabase;
        private readonly ITermStoreXmlParser _termStoreXmlParser;

        private Models.SharePointTermStore.TermStore _termStore;
        private DateTime _termStoreModificationTime;

        public TermStoreCache(
            ITermStoreDatabase termStoreDatabase,
            ITermStoreXmlParser termStoreXmlParser)
        {
            _termStoreDatabase = termStoreDatabase;
            _termStoreXmlParser = termStoreXmlParser;
        }

        public async Task<Models.SharePointTermStore.TermStore> GetTermStoreAsync()
        {
            var termStoreModificationTime = await _termStoreDatabase.GetTermStoreModificationTimeUtc();

            if ((termStoreModificationTime == default) || (termStoreModificationTime == _termStoreModificationTime))
            {
                return _termStore;
            }

            var termStoreImport = await _termStoreDatabase.GetTermStoreImport();
            UpdateTermStoreCache(termStoreImport);

            return _termStore;
        }

        private void UpdateTermStoreCache(TermStoreImport termStoreImport)
        {
            using (var stream = new MemoryStream(termStoreImport.TermStoreXmlFile, false))
            {
                var xDocument = XDocument.Load(stream);
                _termStore = _termStoreXmlParser.Parse(xDocument);
                _termStoreModificationTime = termStoreImport.ServerModificationTimeUtc;
            }
        }
    }
}
