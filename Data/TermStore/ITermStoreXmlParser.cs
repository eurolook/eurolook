﻿using System.Xml.Linq;

namespace Eurolook.Data.TermStore
{
    public interface ITermStoreXmlParser
    {
        Models.SharePointTermStore.TermStore Parse(XDocument document);
    }
}
