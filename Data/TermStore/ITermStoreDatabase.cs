using System;
using System.IO;
using System.Threading.Tasks;
using Eurolook.Data.Models.SharePointTermStore;

namespace Eurolook.Data.TermStore
{
    public interface ITermStoreDatabase
    {
        Task UpdateTermStore(Stream stream);

        Task<TermStoreImport> GetTermStoreImport();

        Task<DateTime> GetTermStoreModificationTimeUtc();
    }
}
