using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Eurolook.Common.Extensions;
using Eurolook.Data.Models.SharePointTermStore;

namespace Eurolook.Data.TermStore
{
    public class TermStoreXmlParser : ITermStoreXmlParser
    {
        public Models.SharePointTermStore.TermStore Parse(XDocument document)
        {
            try
            {
                return new Models.SharePointTermStore.TermStore(GetTermSets(document));
            }
            catch (Exception ex)
            {
                throw new ParseTermStoreXmlFailedException("Failed to parse TermStore XML.", ex);
            }
        }

        private HashSet<TermSet> GetTermSets(XDocument document)
        {
            return document.Descendants("Group")
                           .Where(g => g.Attribute("Name")?.Value == "ECA")
                           .Descendants("TermSet")
                           .Select(
                               e => new TermSet
                               {
                                   CreatedDateUtc = GetDateTimeAttribute(e, "CreatedDate"),
                                   LastModifiedDateUtc = GetDateTimeAttribute(e, "LastModifiedDate"),
                                   Description = e.Attribute("Description")?.Value,
                                   IsAvailableForTagging = GetBooleanAttribute(e, "IsAvailableForTagging"),
                                   IsOpenForTermCreation = GetBooleanAttribute(e, "IsOpenForTermCreation"),
                                   SharePointId = GetGuidAttribute(e, "Id"),
                                   Name = e.Attribute("Name")?.Value,
                                   Terms = GetTerms(e),
                               })
                           .ToHashSet();
        }

        private HashSet<Term> GetTerms(XElement element)
        {
            var customSortOrder = element.Element("CustomSortOrder")?.Value.Split(':')
                                         .Distinct(StringComparer.InvariantCultureIgnoreCase)
                                         .Select(
                                             (guid, index) => new
                                             {
                                                 Id = Guid.Parse(guid),
                                                 Index = index,
                                             })
                                         .ToDictionary(kvp => kvp.Id, kvp => kvp.Index);

            return element.Elements("Term")
                          .Select(
                              e => new Term
                              {
                                  LastModifiedDateUtc = GetDateTimeAttribute(e, "LastModifiedDate"),
                                  IsAvailableForTagging = GetBooleanAttribute(e, "IsAvailableForTagging"),
                                  CreatedDateUtc = GetDateTimeAttribute(e, "CreatedDate"),
                                  IsDeprecated = GetBooleanAttribute(e, "IsDeprecated"),
                                  IsReused = GetBooleanAttribute(e, "IsReused"),
                                  Name = e.Attribute("Name")?.Value,
                                  ParentTermId = GetNullableGuidAttribute(e, "ParentTermId"),
                                  SourceTermId = GetNullableGuidAttribute(e, "SourceTermId"),
                                  SharePointId = GetGuidAttribute(e, "Id"),
                                  Labels = GetTermLabels(e),
                                  CustomSortIndex = GetSortIndex(e, customSortOrder),
                                  IsExcluded = GetBooleanAttribute(GetCustomProperty(e, "EUROLOOK_exclude"), "Value"),
                                  DefaultAcronym = GetCustomProperty(e, "EUROLOOK_Default_Acronym")?.Attribute("Value")
                                      ?.Value,
                              })
                          .ToHashSet();
        }

        private HashSet<TermLabel> GetTermLabels(XElement element)
        {
            return element.Elements("Label")
                          .Select(
                              e => new TermLabel
                              {
                                  IsDefaultForLanguage = GetBooleanAttribute(e, "IsDefaultForLanguage"),
                                  LanguageId = XmlConvert.ToInt32(e.Attribute("Language").Value),
                                  Value = e.Value,
                              })
                          .ToHashSet();
        }

        private bool GetBooleanAttribute(XElement element, string attributeName)
        {
            string attributeValue = element?.Attribute(attributeName)?.Value;
            return (attributeValue?.EqualsIgnoreCase("true") == true)
                   || (attributeValue?.EqualsIgnoreCase("yes") == true);
        }

        private DateTime GetDateTimeAttribute(XElement element, string attributeName)
        {
            return DateTime.ParseExact(
                element.Attribute(attributeName).Value,
                "MM/dd/yyyy HH:mm:ss",
                CultureInfo.InvariantCulture);
        }

        private Guid? GetNullableGuidAttribute(XElement element, string attributeName)
        {
            string value = element.Attribute(attributeName)?.Value;
            return value == null ? null as Guid? : XmlConvert.ToGuid(value);
        }

        private Guid GetGuidAttribute(XElement element, string attributeName)
        {
            return XmlConvert.ToGuid(element.Attribute(attributeName).Value);
        }

        private int GetSortIndex(XElement element, IReadOnlyDictionary<Guid, int> customSortOrder)
        {
            if (customSortOrder != null)
            {
                var id = GetGuidAttribute(element, "Id");
                if (customSortOrder.ContainsKey(id))
                {
                    return customSortOrder[id];
                }
            }

            return 0;
        }

        private XElement GetCustomProperty(XElement element, string name)
        {
            return element
                   .Elements("CustomProperty")
                   .FirstOrDefault(cp => name.EqualsIgnoreCase(cp.Attribute("Name")?.Value));
        }
    }
}
