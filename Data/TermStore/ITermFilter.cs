using System.Collections.Generic;
using Eurolook.Data.Models.SharePointTermStore;

namespace Eurolook.Data.TermStore
{
    public interface ITermFilter
    {
        IEnumerable<Term> GetFilteredTerms(TermSet termSet);
    }
}
