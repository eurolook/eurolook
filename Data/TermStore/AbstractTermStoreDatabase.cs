﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Data.Models.SharePointTermStore;
using Microsoft.EntityFrameworkCore;

namespace Eurolook.Data.TermStore
{
    public abstract class AbstractTermStoreDatabase<T> : EurolookDatabase<T>, ITermStoreDatabase
        where T : EurolookContext
    {
        public async Task UpdateTermStore(Stream stream)
        {
            using (var context = GetContext())
            {
                var termStoreImport = await context.TermStoreImports.FirstOrDefaultAsync();

                if (termStoreImport == null)
                {
                    await AddNewTermStoreImport(stream, context);
                }
                else
                {
                    await UpdateTermStoreImport(stream, termStoreImport);
                }

                await context.SaveChangesAsync();
            }
        }

        public async Task<DateTime> GetTermStoreModificationTimeUtc()
        {
            using var context = GetContext();
            return await context.TermStoreImports
                                .AsNoTracking()
                                .Select(ts => ts.ServerModificationTimeUtc)
                                .FirstOrDefaultAsync();
        }

        public async Task<TermStoreImport> GetTermStoreImport()
        {
            using var context = GetContext();
            return await context.TermStoreImports
                                .AsNoTracking()
                                .FirstOrDefaultAsync();
        }

        private async Task AddNewTermStoreImport(Stream stream, EurolookContext context)
        {
            var newTermStoreImport = new TermStoreImport();
            newTermStoreImport.Init();
            await CopyStreamToTermStoreImport(stream, newTermStoreImport);
            context.TermStoreImports.Add(newTermStoreImport);
        }

        private async Task UpdateTermStoreImport(Stream stream, TermStoreImport termStoreImport)
        {
            await CopyStreamToTermStoreImport(stream, termStoreImport);
            termStoreImport.UpdateServerModificationTime();
        }

        private async Task CopyStreamToTermStoreImport(Stream stream, TermStoreImport termStoreImport)
        {
            using (var ms = new MemoryStream())
            {
                await stream.CopyToAsync(ms);
                termStoreImport.TermStoreXmlFile = ms.ToArray();
            }
        }
    }
}
