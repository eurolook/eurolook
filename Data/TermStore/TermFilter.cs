using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.Models.SharePointTermStore;

namespace Eurolook.Data.TermStore
{
    public class TermFilter : ITermFilter
    {
        public IEnumerable<Term> GetFilteredTerms(TermSet termSet)
        {
            var terms = termSet.Terms;
            Func<Term, bool> hasExcludedParent = t => GetParentTerms(terms, t)
                                                     .Any(parentTerm => parentTerm.IsExcluded);

            return terms
                   .Where(t => !t.IsDeprecated)
                   .Where(t => !hasExcludedParent(t) && !t.IsExcluded);
        }

        private IEnumerable<Term> GetParentTerms(HashSet<Term> terms, Term term)
        {
            if (term.ParentTermId == null)
            {
                yield break;
            }

            var parentTerm = terms.Single(t => t.SharePointId == term.ParentTermId);
            GetParentTerms(terms, parentTerm);
            yield return parentTerm;
        }
    }
}
