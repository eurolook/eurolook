﻿using System.Threading.Tasks;

namespace Eurolook.Data.TermStore
{
    public interface ITermStoreCache
    {
        Task<Models.SharePointTermStore.TermStore> GetTermStoreAsync();
    }
}
