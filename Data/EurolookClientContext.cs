﻿using System;
using System.Linq;
using Eurolook.Common;
using Eurolook.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Eurolook.Data
{
    public sealed class EurolookClientContext : EurolookContext
    {
        private static readonly ILoggerFactory DebugLoggerFactory
            = LoggerFactory.Create(builder => builder.AddDebug());

        private readonly string _connectionString;
        private readonly bool _ignoreDeletedFlags;

        public EurolookClientContext(string connectionString, bool ignoreDeletedFlags)
        {
            _connectionString = connectionString;
            _ignoreDeletedFlags = ignoreDeletedFlags;
        }

        public EurolookClientContext(bool ignoreDeletedFlags)
            : this("Data Source=|DataDirectory|Eurolook.db", ignoreDeletedFlags)
        {
        }

        public DbSet<DatabaseState> DatabaseState { get; set; }

        public override bool IsServerContext => false;

        public bool EnsureCreated()
        {
            using (new SingleGlobalInstance("EurolookCreateClientDatabase", 10000))
            {
                return Database.EnsureCreated();
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(_connectionString);

#if DEBUG
            optionsBuilder
                .UseLoggerFactory(DebugLoggerFactory)
                .EnableSensitiveDataLogging();
#endif
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // SQLite specific model building
            // make string and guid columns case insensitive
            foreach (var pb in modelBuilder.Model.GetEntityTypes()
                                           .SelectMany(t => t.GetProperties())
                                           .Where(
                                               p => p.ClrType == typeof(Guid) || p.ClrType == typeof(Guid?)
                                                                              || p.ClrType == typeof(string))
                                           .Select(
                                               p => modelBuilder.Entity(p.DeclaringEntityType.ClrType)
                                                                .Property(p.Name)))
            {
                pb.HasColumnType("TEXT COLLATE NOCASE");
            }

            // seeding client data
            modelBuilder.Entity<DatabaseState>().HasData(
                new DatabaseState
                {
                    Id = -1,
                    ProductCustomizationId = ConfigurationInfo.Instance.ProductCustomizationId,
                    ClientVersion = ConfigurationInfo.Instance.ClientVersion,
                });

            if (_ignoreDeletedFlags)
            {
                UpdatableModelConfiguration.ConfigureIgnoreDeletedFlags(modelBuilder);
            }
        }
    }
}
