﻿using System;
using Eurolook.Common;

namespace Eurolook.Data
{
    public class ConfigurationInfo : IConfigurationInfo
    {
        private ConfigurationInfo(string productCustomizationId)
        {
            ProductCustomizationId = productCustomizationId;
        }

        public static ConfigurationInfo Instance { get; private set; }

        public string ProductCustomizationId { get; }

        public Version ClientVersion => typeof(DotNetVersionInfo).Assembly.GetName().Version;

        public static void Initialize(string productCustomizationId)
        {
            Instance = new ConfigurationInfo(productCustomizationId);
        }
    }
}
