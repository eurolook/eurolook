﻿using System;

namespace Eurolook.Data
{
    public interface IConfigurationInfo
    {
        /// <summary>
        /// Gets an id value identifying for which customer Eurolook has been customized.
        /// </summary>
        string ProductCustomizationId { get; }

        /// <summary>
        /// Gets the version of the Eurolook client.
        /// </summary>
        Version ClientVersion { get; }
    }
}
