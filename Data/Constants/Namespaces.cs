using System.Xml.Linq;

namespace Eurolook.Data.Constants
{
    public static class Namespaces
    {
        public static readonly XNamespace SharedContentTypeNs = "Microsoft.SharePoint.Taxonomy.ContentTypeSync";
        public static readonly XNamespace SharePointEventsNs = "http://schemas.microsoft.com/sharepoint/events";
        public static readonly XNamespace ContentTypePropertiesNs = "http://schemas.microsoft.com/office/2006/metadata/properties";
        public static readonly XNamespace ContentTypeSchemaNs = "http://schemas.microsoft.com/office/2006/metadata/contentType";
        public static readonly XNamespace FormTemplatesNs = "http://schemas.microsoft.com/sharepoint/v3/contenttype/forms";
        public static readonly XNamespace CustomXsnNs = "http://schemas.microsoft.com/office/2006/metadata/customXsn";
        public static readonly XNamespace PartnerControlsNs = "http://schemas.microsoft.com/office/infopath/2007/PartnerControls";
    }
}
