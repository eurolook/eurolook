﻿using System;

namespace Eurolook.Data.Constants
{
    public static class CommonDataConstants
    {
        public static readonly Guid InsertAtFixedPositionBrickCategoryId = new Guid("718e29e0-4588-41b4-aa64-bcf77b7fdbdf");
        public static readonly Guid ToolsBrickCategoryId = new Guid("3A32DF8C-4813-412E-A351-5C67157C5D89");
        public static readonly Guid StandardWordDocDocumentModel = new Guid("C7CA1146-07F3-41B1-BCB2-D5213D29A28D");
        public static readonly Guid CreatorAuthorRole = new Guid("A4FBAFF4-B07C-48B4-A21E-E7B9EEDF3796");
        public static readonly Guid SignatoryAuthorRoleId = new Guid("dd422d74-d41f-4095-8cb8-8304a90a6b0c");
        public static readonly Guid ChangeBrickAuthorActionId = new Guid("c4a6fb22-4f7d-428a-ab63-e5b59cfebaba");
        public static readonly Guid CreateCustomBrickId = new Guid("1b94c2c0-45af-4af6-a395-ba4cb4ec4752");
        public static readonly Guid DefaultUserGroupId = new Guid("C590A59F-5231-46BD-B4C3-2EAA804F1A7A");
        public static readonly Guid AdministratorsUserGroupId = new Guid("DABAEE97-C3F6-4A8C-87A0-20D528481819");
        public static readonly Guid ContributorsUserGroupId = new Guid("345406CC-DFA8-4F0F-A9CA-984B64977E71");
        public static readonly Guid CustomBricksCategoryId = new Guid("cb2811d9-0c13-419d-999e-efd6ea653a0a");

        public static readonly Guid EnglishLanguageId = new Guid("CD269368-BBBE-4898-806F-E261443968DB");

        public static readonly Guid NoAddressId = new Guid("9477b21f-3bff-4071-bc5a-78636fecdbbf");

        public static readonly Guid EL4DocumentModelId = new Guid("7b1f02d8-4b91-4f4e-81fc-235dda31e4ef");
        public static readonly Guid NoteId = new Guid("0b054141-88b1-4efb-8c91-2905cb0bed6c");
        public static readonly Guid BodyGroupId = new Guid("87c9bc0b-3e24-4c1c-bd10-c9a49edd9b10");
        public static readonly Guid FooterGroupId = new Guid("2a90fc71-c830-4baa-8dc3-f32382b7f890");
        public static readonly Guid ClosingGroupId = new Guid("db316230-803e-492e-a8a2-6646f64bcc60");
    }
}
