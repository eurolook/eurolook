using System;
using System.Reflection;
using Microsoft.EntityFrameworkCore;

namespace Eurolook.Data
{
    /// <summary>
    /// provides access to the pluralizer of the entity framework
    /// </summary>
    /// <remarks>
    /// copied from http://dotnetspeak.com/2011/09/pluralization-in-entity-framework
    /// </remarks>
    public static class Pluralizer
    {
        private static object _pluralizer;
        private static MethodInfo _pluralizationMethod;

        public static string Pluralize(string word)
        {
            CreatePluralizer();
            return (string)_pluralizationMethod.Invoke(_pluralizer, new object[] { word });
        }

        public static void CreatePluralizer()
        {
            if (_pluralizer != null)
            {
                return;
            }

            var assembly = typeof(DbContext).Assembly;

            var type = assembly.GetType("System.Data.Entity.Infrastructure.Pluralization.EnglishPluralizationService");
            _pluralizer = Activator.CreateInstance(type, true);
            _pluralizationMethod = _pluralizer.GetType().GetMethod("Pluralize");
        }
    }
}
