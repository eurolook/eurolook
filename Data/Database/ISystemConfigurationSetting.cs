namespace Eurolook.Data.Database
{
    public interface ISystemConfigurationSetting
    {
        string Key { get; }
    }
}
