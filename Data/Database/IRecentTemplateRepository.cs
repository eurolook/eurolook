﻿using System;
using System.Threading.Tasks;
using Eurolook.Data.Models.Wizard;

namespace Eurolook.Data.Database
{
    public interface IRecentTemplateRepository
    {
        Task<RecentTemplate[]> GetRecentTemplatesAsync(Guid id);

        Task SaveAsRecentTemplateAsync(Guid userId, Guid templateId, bool isDocumentModel);

        void DeleteCustomTemplate(Guid userId, Guid templateId);
    }
}
