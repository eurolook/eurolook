﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data.Models;

namespace Eurolook.Data.Database
{
    public interface ILanguageRepository
    {
        Language GetLanguage(Guid id);

        Language GetLanguage(string name);

        List<Language> GetAllLanguages();

        Task<List<Language>> GetAllLanguagesAsync();

        Language GetEnglishLanguage();

        Language GetEnglishLanguage(EurolookContext context);

        DocumentModelLanguage GetDocumentModelLanguage(Guid documentModelId, Guid languageId);

        List<Language> GetLanguagesForLocalisedResource(string alias);
    }
}
