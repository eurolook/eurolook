﻿using System.Collections.Generic;
using Eurolook.Data.Models;

namespace Eurolook.Data.Database
{
    public interface IOrgaEntityRepository
    {
        List<string> DeleteOrgaEntity(OrgaEntity entity);
    }
}
