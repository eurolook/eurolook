﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Data.Models;
using Eurolook.Data.Models.Wizard;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Eurolook.Data.Database
{
    public class RecentTemplateRepository : IRecentTemplateRepository
    {
        private readonly Func<EurolookContext> _eurolookContextFunc;

        public RecentTemplateRepository(Func<EurolookContext> eurolookContextFunc)
        {
            _eurolookContextFunc = eurolookContextFunc;
        }

        public async Task<RecentTemplate[]> GetRecentTemplatesAsync(Guid id)
        {
            await using var context = _eurolookContextFunc();
            var user = await context.Users
                                .Where(x => x.Id == id)
                                .Include(x => x.Settings)
                                .FirstAsync();

            var result = Array.Empty<RecentTemplate>();

            if (user.Settings.RecentlyUsedTemplates != null)
            {
                result = JsonConvert.DeserializeObject<RecentTemplate[]>(user.Settings.RecentlyUsedTemplates);
            }

            return result;
        }

        public async Task SaveAsRecentTemplateAsync(Guid userId, Guid templateId, bool isDocumentModel)
        {
            var existingRecentTemplates = await GetRecentTemplatesAsync(userId);
            var recentTemplates = CreateRecentTemplatesJSON(existingRecentTemplates, templateId, isDocumentModel);

            await SaveRecentTemplatesAsync(userId, recentTemplates);
        }

        private string CreateRecentTemplatesJSON(RecentTemplate[] existingRecentTemplates, Guid templateId, bool isDocumentModel)
        {
            var recentTemplate = new RecentTemplate
            {
                TemplateId = templateId,
                IsDocumentModel = isDocumentModel,
            };

            var recentTemplates = existingRecentTemplates.Where(x => x.TemplateId != templateId)
                                                         .Prepend(recentTemplate)
                                                         .Take(10)
                                                         .ToArray();

            return JsonConvert.SerializeObject(recentTemplates);
        }

        private async Task SaveRecentTemplatesAsync(Guid userId, string recentlyUsedTemplates)
        {
            await using var context = _eurolookContextFunc();
            var user = await context.Users
                                .Where(x => x.Id == userId)
                                .Include(x => x.Settings)
                                .FirstAsync();

            user.Settings.RecentlyUsedTemplates = recentlyUsedTemplates;
            user.Settings.SetModification(ModificationType.ServerModification);
            await context.SaveChangesAsync();
        }

        public async void DeleteCustomTemplate(Guid userId, Guid templateId)
        {
            var recentTemplates = await GetRecentTemplatesAsync(userId);
            recentTemplates = recentTemplates.Where(t => t.TemplateId != templateId).ToArray();

            await SaveRecentTemplatesAsync(userId, JsonConvert.SerializeObject(recentTemplates));
        }
    }
}
