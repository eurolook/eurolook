﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Eurolook.Data.Database
{
    public class UserGroupRepository : IUserGroupRepository, ICanLog
    {
        private readonly Func<EurolookContext> _eurolookContextFunc;

        public UserGroupRepository(Func<EurolookContext> eurolookContextFunc)
        {
            _eurolookContextFunc = eurolookContextFunc;
        }

        public bool IsMemberOfUserGroupOrMappedAdGroup(User user, Guid userGroupId)
        {
            if (BelongsToUserGroup(user, userGroupId))
            {
                return true;
            }

            IPrincipal principal = GetCurrentPrincipalOrNull();
            var adGroupsInUserGroup = GetAdGroupsByUserGroup(userGroupId);
            return BelongsToAdGroupsOfUserGroup(principal, adGroupsInUserGroup);
        }

        public async Task<bool> IsMemberOfUserGroupOrMappedAdGroupAsync(User user, Guid userGroupId)
        {
            if (BelongsToUserGroup(user, userGroupId))
            {
                return true;
            }

            IPrincipal principal = GetCurrentPrincipalOrNull();
            var adGroupsInUserGroup = await GetAdGroupsByUserGroupAsync(userGroupId);
            return BelongsToAdGroupsOfUserGroup(principal, adGroupsInUserGroup);
        }

        public async Task<bool> IsMemberOfUserGroupOrMappedAdGroupAsync(User user, Guid userGroupId, IPrincipal principal)
        {
            if (BelongsToUserGroup(user, userGroupId))
            {
                return true;
            }

            var adGroupsInUserGroup = await GetAdGroupsByUserGroupAsync(userGroupId);
            return BelongsToAdGroupsOfUserGroup(principal, adGroupsInUserGroup);
        }

        private IPrincipal GetCurrentPrincipalOrNull()
        {
            try
            {
                WindowsIdentity windowsIdentity = WindowsIdentity.GetCurrent();
                return new WindowsPrincipal(windowsIdentity);
            }
            catch (Exception ex)
            {
                this.LogError($"Getting current WindowsPrincipal failed", ex);
                return null;
            }
        }

        private bool BelongsToUserGroup(User user, Guid userGroupId)
        {
            return user != null && user.IsMemberOfUserGroup(userGroupId);
        }

        private bool BelongsToAdGroupsOfUserGroup(IPrincipal principal, List<string> adGroupsInUserGroup)
        {
            try
            {
                if (principal == null)
                {
                    this.LogWarn($"Principal == null -> ad groups cannot be determined");
                    return false;
                }

                return adGroupsInUserGroup.Any(g => principal.IsInRole(g));
            }
            catch (Exception ex)
            {
                this.LogError($"{nameof(BelongsToAdGroupsOfUserGroup)} failed", ex);
            }

            return false;
        }

        protected EurolookContext GetContext()
        {
            return _eurolookContextFunc();
        }

        private List<string> GetAdGroupsByUserGroup(Guid userGroupId)
        {
            using (var context = GetContext())
            {
                var group = context.UserGroups.AsNoTracking().FirstOrDefault(g => g.Id == userGroupId);
                return ConvertToAdGroups(group);
            }
        }

        private async Task<List<string>> GetAdGroupsByUserGroupAsync(Guid userGroupId)
        {
            using (var context = GetContext())
            {
                var group = await context.UserGroups.AsNoTracking().FirstOrDefaultAsync(g => g.Id == userGroupId);
                return ConvertToAdGroups(group);
            }
        }

        private List<string> ConvertToAdGroups(UserGroup group)
        {
            if (group == null || string.IsNullOrEmpty(group.AdGroups))
            {
                return new List<string>();
            }

            return group.AdGroups.Split(new[] { "\n", "\r", "\r\n" }, StringSplitOptions.None).ToList();
        }
    }
}
