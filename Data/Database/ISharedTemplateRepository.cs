﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data.Models.TemplateStore;

namespace Eurolook.Data.Database
{
    public interface ISharedTemplateRepository
    {
        Task<Template> GetTemplate(Guid templateId);
        Task<List<TemplateFile>> GetTemplateFilesForTemplate(Guid templateId);
        Task<UserTemplate[]> GetUserTemplatesAsync(Guid userId);
        Task SaveAsUserTemplateAsync(Guid templateId, Guid userId);
        Task DeleteUserTemplate(Guid templateId, Guid userId);
        Task<Template> GetTemplateWithIncludes(Guid templateId);
    }
}
