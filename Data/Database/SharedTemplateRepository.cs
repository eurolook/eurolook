﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;
using Microsoft.EntityFrameworkCore;

namespace Eurolook.Data.Database
{
    public class SharedTemplateRepository : ISharedTemplateRepository
    {
        private readonly Func<EurolookContext> _eurolookContextFunc;

        public SharedTemplateRepository(Func<EurolookContext> eurolookContextFunc)
        {
            _eurolookContextFunc = eurolookContextFunc;
        }

        public async Task<UserTemplate[]> GetUserTemplatesAsync(Guid userId)
        {
            using var context = _eurolookContextFunc();

            var userTemplates = await context.UserTemplates
                                             .Where(x => x.UserId == userId && !x.Deleted)
                                             .Include(x => x.Template).ThenInclude(t => t.BaseDocumentModel).ThenInclude(dm => dm.AuthorRoles).ThenInclude(ar => ar.AuthorRole)
                                             .Include(x => x.Template).ThenInclude(t => t.TemplateFiles).ThenInclude(f => f.Language)
                                             .Include(x => x.Template).ThenInclude(t => t.Publications).ThenInclude(f => f.OrgaEntity)
                                             .Where(x => !x.Template.Deleted && x.Template.IsPublic)
                                             .ToArrayAsync();

            var result = userTemplates.Where(t => t.Template.IsPublic).ToArray();
            foreach (var userTemplate in result)
            {
                userTemplate.Template.BaseDocumentModel.AuthorRoles.RemoveWhere(ar => ar.Deleted);
            }

            return result;
        }

        public async Task SaveAsUserTemplateAsync(Guid templateId, Guid userId)
        {
            using var context = _eurolookContextFunc();
            var modificationType = context.IsServerContext ? ModificationType.ServerModification : ModificationType.ClientModification;

            var existingUserTemplate
                = await context.UserTemplates
                        .IgnoreQueryFilters()
                        .Where(x => x.UserId == userId && x.TemplateId == templateId)
                        .Include(x => x.Template)
                        .FirstOrDefaultAsync();

            if (existingUserTemplate != null)
            {
                existingUserTemplate.SetDeletedFlag(modificationType, false);
            }
            else
            {
                var userTemplate = new UserTemplate();
                userTemplate.Init();
                userTemplate.TemplateId = templateId;
                userTemplate.UserId = userId;
                userTemplate.SetModification(modificationType);
                await context.UserTemplates.AddAsync(userTemplate);
            }

            await context.SaveChangesAsync();
        }

        public async Task DeleteUserTemplate(Guid templateId, Guid userId)
        {
            using var context = _eurolookContextFunc();
            var modificationType = context.IsServerContext ? ModificationType.ServerModification : ModificationType.ClientModification;

            var existingUserTemplates = await context.UserTemplates
                .Where(x => x.UserId == userId && x.TemplateId == templateId)
                .ToListAsync();

            foreach (var userTemplate in existingUserTemplates)
            {
                userTemplate.IsOfflineAvailable = false;
                userTemplate.SetDeletedFlag(modificationType);
            }

            context.SaveChanges();
        }

        public async Task<Template> GetTemplate(Guid templateId)
        {
            using var context = _eurolookContextFunc();
            return await context.Templates
                .Include(t => t.Owners)
                .ThenInclude(o => o.User)
                .ThenInclude(u => u.Self)
                .FirstOrDefaultAsync(t => t.Id == templateId && !t.Deleted);
        }

        public async Task<Template> GetTemplateWithIncludes(Guid templateId)
        {
            await using var context = _eurolookContextFunc();
            return await context.Templates
                                .Include(x => x.BaseDocumentModel)
                                .Include(x => x.TemplateFiles).ThenInclude(y => y.Language)
                                .Include(x => x.Publications)
                                .Include(x => x.Owners)
                                .Where(x => x.Id == templateId)
                                .FirstOrDefaultAsync()
                                ?? throw new ArgumentException($"Template with ID '{templateId}' does not exist.", nameof(templateId));
        }

        public async Task<List<TemplateFile>> GetTemplateFilesForTemplate(Guid templateId)
        {
            using var context = _eurolookContextFunc();
            return await context.TemplateFiles.Where(f => f.TemplateId == templateId && !f.Deleted).ToListAsync();
        }
    }
}
