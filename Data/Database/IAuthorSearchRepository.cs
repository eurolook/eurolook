﻿using System.Collections.Generic;
using Eurolook.Data.Models;

namespace Eurolook.Data.Database
{
    public interface IAuthorSearchRepository
    {
        IEnumerable<Author> SearchAuthors(string[] searchTerms, PaginationInfo paginationInfo, bool includeOnlyOrgaEntity = false);
    }
}
