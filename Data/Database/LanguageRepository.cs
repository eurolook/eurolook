using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Data.Models;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace Eurolook.Data.Database
{
    public class LanguageRepository : ILanguageRepository
    {
        private readonly Func<EurolookContext> _eurolookContextFunc;

        public LanguageRepository(Func<EurolookContext> eurolookContextFunc)
        {
            _eurolookContextFunc = eurolookContextFunc;
        }

        [NotNull]
        public List<Language> GetAllLanguages()
        {
            using (var context = GetContext())
            {
                return context.Languages.AsNoTracking().Where(l => !l.Deleted).OrderBy(dl => dl.Name).ToList();
            }
        }

        [NotNull]
        public async Task<List<Language>> GetAllLanguagesAsync()
        {
            using (var context = GetContext())
            {
                return await context.Languages
                                    .AsNoTracking()
                                    .Where(l => !l.Deleted)
                                    .OrderBy(dl => dl.Name)
                                    .ToListAsync();
            }
        }

        public DocumentModelLanguage GetDocumentModelLanguage(Guid documentModelId, Guid languageId)
        {
            using (var context = GetContext())
            {
                return context.DocumentModelLanguages
                              .FirstOrDefault(
                                  x => x.DocumentLanguageId == languageId && x.DocumentModelId == documentModelId);
            }
        }

        public Language GetEnglishLanguage()
        {
            return GetLanguage("EN");
        }

        public Language GetLanguage(string name)
        {
            using (var context = GetContext())
            {
                return GetLanguage(context, name);
            }
        }

        public Language GetLanguage(Guid id)
        {
            using (var context = GetContext())
            {
                return GetLanguage(context, id);
            }
        }

        public Language GetEnglishLanguage(EurolookContext context)
        {
            return GetLanguage(context, "EN");
        }

        [CanBeNull]
        public List<Language> GetLanguagesForLocalisedResource(string alias)
        {
            using (var context = GetContext())
            {
                return context.LocalisedResources.AsNoTracking().Where(r => r.Resource.Alias == alias)
                              .Select(r => r.Language).ToList();
            }
        }

        protected EurolookContext GetContext()
        {
            return _eurolookContextFunc();
        }

        private Language GetLanguage(EurolookContext context, string name)
        {
            return context.Languages.AsNoTracking().FirstOrDefault(l => l.Name == name);
        }

        private Language GetLanguage(EurolookContext context, Guid id)
        {
            return context.Languages.AsNoTracking().FirstOrDefault(x => x.Id == id);
        }
    }
}
