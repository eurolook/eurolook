﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.Models;

namespace Eurolook.Data.Database
{
    public class OrgaEntityRepository : IOrgaEntityRepository
    {
        private readonly Func<EurolookContext> _eurolookContextFunc;

        public OrgaEntityRepository(Func<EurolookContext> eurolookContextFunc)
        {
            _eurolookContextFunc = eurolookContextFunc;
        }

        public List<string> DeleteOrgaEntity(OrgaEntity entity)
        {
            var detachedAuthors = new List<string>();
            using (var context = GetContext())
            {
                context.OrgaEntities.Attach(entity);
                entity.SetDeletedFlag();

                var headerText = context.Texts.FirstOrDefault(t => t.Id == entity.HeaderTextId);
                headerText?.SetDeletedFlag();

                foreach (var headerTranslation in context.Translations.Where(t => t.TextId == entity.HeaderTextId))
                {
                    headerTranslation.SetDeletedFlag();
                }

                context.SaveChanges();
            }

            return detachedAuthors;
        }

        protected EurolookContext GetContext()
        {
            return _eurolookContextFunc();
        }
    }
}
