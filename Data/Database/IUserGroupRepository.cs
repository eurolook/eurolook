﻿using System;
using System.Security.Principal;
using System.Threading.Tasks;
using Eurolook.Data.Models;

namespace Eurolook.Data.Database
{
    public interface IUserGroupRepository
    {
        bool IsMemberOfUserGroupOrMappedAdGroup(User user, Guid userGroupId);

        Task<bool> IsMemberOfUserGroupOrMappedAdGroupAsync(User user, Guid userGroupId, IPrincipal principal);

        Task<bool> IsMemberOfUserGroupOrMappedAdGroupAsync(User user, Guid userGroupId);
    }
}
