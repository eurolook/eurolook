﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.Data.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Eurolook.Data.Database
{
    public class SystemConfigurationRepository : ISystemConfigurationRepository, ICanLog
    {
        private readonly Func<EurolookContext> _eurolookContextFunc;

        public SystemConfigurationRepository(Func<EurolookContext> eurolookContextFunc)
        {
            _eurolookContextFunc = eurolookContextFunc;
        }

        public async Task<T> GetConfigurationAsync<T>()
            where T : ISystemConfigurationSetting, new()
        {
            await using var context = _eurolookContextFunc();
            var instance = new T();
            var configuration = await LoadConfigurationAsync(context, instance.Key);
            return GetConfiguration(instance, configuration, this);
        }

        public T GetConfiguration<T>(ICanLog logger)
            where T : ISystemConfigurationSetting, new()
        {
            using var context = _eurolookContextFunc();
            var instance = new T();
            var configuration = LoadConfiguration(context, instance.Key);
            return GetConfiguration(instance, configuration, logger);
        }

        public static T GetConfiguration<T>(EurolookContext context, ICanLog logger)
            where T : ISystemConfigurationSetting, new()
        {
            var instance = new T();
            var configuration = LoadConfiguration(context, instance.Key);
            return GetConfiguration(instance, configuration, logger);
        }

        private static T GetConfiguration<T>(T instance, SystemConfiguration configuration, ICanLog logger)
            where T : ISystemConfigurationSetting, new()
        {
            if (configuration == null)
            {
                logger.LogInfo(
                    $"The configuration \"{instance.Key}\" was not found in the database. Using default settings.");
            }

            try
            {
                return configuration != null
                    ? JsonConvert.DeserializeObject<T>(configuration.Value)
                    : instance;
            }
            catch (Exception ex)
            {
                logger.LogError($"Failed to load system configuration \"{instance.Key}\".", ex);
                return instance;
            }
        }

        private static async Task<SystemConfiguration> LoadConfigurationAsync(EurolookContext context, string key)
        {
            return await context.SystemConfigurations.FirstOrDefaultAsync(c => !c.Deleted && c.Key == key);
        }

        private static SystemConfiguration LoadConfiguration(EurolookContext context, string key)
        {
            return context.SystemConfigurations.FirstOrDefault(c => !c.Deleted && c.Key == key);
        }
    }
}
