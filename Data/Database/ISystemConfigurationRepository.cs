﻿using System.Threading.Tasks;
using Eurolook.Common.Log;

namespace Eurolook.Data.Database
{
    public interface ISystemConfigurationRepository
    {
        Task<T> GetConfigurationAsync<T>()
            where T : ISystemConfigurationSetting, new();

        T GetConfiguration<T>(ICanLog logger)
            where T : ISystemConfigurationSetting, new();
    }
}
