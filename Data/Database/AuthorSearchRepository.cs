﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Eurolook.Common.Log;
using Eurolook.Data.Extensions;
using Eurolook.Data.Models;
using LinqKit;
using Microsoft.EntityFrameworkCore;

namespace Eurolook.Data.Database
{
    public class AuthorSearchRepository : IAuthorSearchRepository, ICanLog
    {
        private readonly Func<EurolookContext> _eurolookContextFunc;

        public AuthorSearchRepository(Func<EurolookContext> eurolookContextFunc)
        {
            _eurolookContextFunc = eurolookContextFunc;
        }

        public EurolookContext GetContext()
        {
            return _eurolookContextFunc();
        }

        // *********************************************************************
        //                                                        CLIENT SEARCH
        // *********************************************************************
        public IEnumerable<Author> SearchAuthors(
            string[] searchTerms,
            PaginationInfo paginationInfo,
            bool includeOnlyOrgaEntity = false)
        {
            using var context = GetContext();
            var predicate = PredicateBuilder.New<Author>(true);
            var score = SumBuilder.Zero<Author>();

            try
            {
                foreach (string term in searchTerms)
                {
                    // TODO: ignore diacritics
                    string normalizedTerm = term.ToLowerInvariant();
                    Expression<Func<Author, bool>> searchExpression;
                    Expression<Func<Author, int>> orderByNameExpression;
                    Expression<Func<Author, int>> orderByAcronymExpression;

                    if (Guid.TryParse(term, out var authorId))
                    {
                        searchExpression = a => a.Id == authorId;
                        orderByNameExpression = a => 1000;
                        orderByAcronymExpression = a => 0;
                    }
                    else
                    {
                        if (!includeOnlyOrgaEntity)
                        {
                            searchExpression = a => !a.Deleted && a.LatinLastName != "" && a.LatinLastName != null &&
                                                    (a.LatinFirstName.Contains(normalizedTerm)
                                                     || a.LatinLastName.Contains(normalizedTerm)
                                                     || a.Email.Contains(normalizedTerm)
                                                     || a.Service.Contains(normalizedTerm)
                                                     || a.JobAssignments.Any(
                                                         j => j.Service.Contains(normalizedTerm)));

                            orderByAcronymExpression = a => a.Service.StartsWith(normalizedTerm)
                                                           ? 5
                                                           : a.JobAssignments.Any(
                                                               j => j.Service.StartsWith(normalizedTerm))
                                                               ? 2
                                                               : 0;
                        }
                        else
                        {
                            searchExpression = a => !a.Deleted && a.LatinLastName != "" && a.LatinLastName != null &&
                                                    (a.LatinFirstName.Contains(normalizedTerm)
                                                     || a.LatinLastName.Contains(normalizedTerm)
                                                     || a.Email.Contains(normalizedTerm)
                                                     || a.Service.Contains(normalizedTerm));

                            orderByAcronymExpression = a => a.Service.StartsWith(normalizedTerm) ? 5 : 0;
                        }

                        // NOTE: uncomment the lines below to order authors so that valid authors come before
                        //   invalid authors, prefix matches come before infix matches
                        orderByNameExpression = a => a.LatinLastName.StartsWith(normalizedTerm)
                                                    ? 3
                                                    : a.LatinFirstName.StartsWith(normalizedTerm)
                                                        ? 2
                                                        : a.Email.StartsWith(normalizedTerm)
                                                            ? 1
                                                            : 0;

                        ////a.LatinLastName.StartsWith(normalizedTerm)
                        ////&& a.OrgaEntityId != null
                        ////&& a.LatinFirstName != null && a.LatinFirstName != ""
                        ////&& a.Initials != null && a.Initials != ""
                        ////&& a.Service != null && a.Service != ""
                        ////? 10
                        ////: a.OrgaEntityId != null
                        ////    && a.LatinFirstName != null && a.LatinFirstName != ""
                        ////    && a.Initials != null && a.Initials != ""
                        ////    && a.Service != null && a.Service != ""
                        ////? 5
                        ////: a.LatinLastName.StartsWith(normalizedTerm)
                        ////? 1
                        ////: 0;
                    }

                    score = score.Add(orderByNameExpression);
                    score = score.Add(orderByAcronymExpression);
                    predicate = predicate.And(searchExpression);
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                return Enumerable.Empty<Author>();
            }

            var result = includeOnlyOrgaEntity
                ? context.Authors
                         .AsNoTracking()
                         .Include(a => a.OrgaEntity)
                         .AsExpandable().Where(predicate).OrderByDescending(score)
                         .ThenBy(a => a.LatinLastName)
                : context.Authors
                         .AsNoTracking()
                         .Include(a => a.OrgaEntity)
                         .Include(a => a.Functions).ThenInclude(f => f.Language)
                         .Include(a => a.PredefinedFunction)
                         .Include(a => a.Workplaces).ThenInclude(wp => wp.Address)
                         .Include(a => a.JobAssignments).ThenInclude(j => j.OrgaEntity)
                         .Include(a => a.JobAssignments).ThenInclude(j => j.Functions)
                         .Include(a => a.JobAssignments).ThenInclude(j => j.PredefinedFunction)
                         .AsExpandable().Where(predicate).OrderByDescending(score)
                         .ThenBy(a => a.LatinLastName);

            return result.Paginate(paginationInfo).ToList();
        }
    }
}
