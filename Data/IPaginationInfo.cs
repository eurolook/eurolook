namespace Eurolook.Data
{
    public interface IPaginationInfo
    {
        uint From { get; }

        uint PageSize { get; }

        bool OrderAscending { get; }

        string OrderProperty { get; }
    }
}
