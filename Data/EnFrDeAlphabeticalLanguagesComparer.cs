using System.Collections.Generic;
using Eurolook.Data.Models;

namespace Eurolook.Data
{
    /// <summary>
    /// Language order: EN, FR, DE, then alphabetical.
    /// </summary>
    public class EnFrDeAlphabeticalLanguagesComparer : IComparer<Language>
    {
        public int Compare(Language x, Language y)
        {
            if (x == null && y == null)
            {
                return 0;
            }

            if (x == null)
            {
                return -1;
            }

            if (y == null)
            {
                return 1;
            }

            var mainLanguages = new List<string>
            {
                "DE",
                "FR",
                "EN",
            };
            var result = mainLanguages.IndexOf(y.Name).CompareTo(mainLanguages.IndexOf(x.Name));
            if (result == 0)
            {
                result = string.CompareOrdinal(x.Name, y.Name);
            }

            return result;
        }
    }
}
