using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Eurolook.Common;

namespace Eurolook.Data.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Property accessors are used by Entity Framework")]
    public class UpdateRequest
    {
        private const string IsoUtcFormat = "yyyy-MM-ddTHH:mm:ssZ";

        // ReSharper disable once InconsistentNaming
        public string ISOMinimumServerModificationTimeUTC { get; set; }

        public string Login { get; set; }

        public string DeviceName { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public bool IsInitializationRequest => MinimumServerModificationTimeUTC == DateTime.MinValue;

        [IgnoreDataMember]
        [XmlIgnore]
        // Do not serialize, serialize ISOMinimumServerModificationTimeUTC instead
        // ReSharper disable once InconsistentNaming
        public DateTime MinimumServerModificationTimeUTC
        {
            get
            {
                var dateTime = DateTime.ParseExact(
                    ISOMinimumServerModificationTimeUTC,
                    IsoUtcFormat,
                    CultureInfo.InvariantCulture);
                return TimeUtils.GetUtcDateTime(dateTime);
            }

            set
            {
                var utcDateTime = TimeUtils.GetUtcDateTime(value);
                ISOMinimumServerModificationTimeUTC = utcDateTime.ToString(IsoUtcFormat, CultureInfo.InvariantCulture);
            }
        }
    }
}
