using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Eurolook.Common.Extensions;
using Eurolook.Data.Constants;

namespace Eurolook.Data.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Property accessors are used by Entity Framework")]
    public abstract class Brick : DisplayItem
    {
        protected Brick()
        {
            VisibleToUserGroups = CommonDataConstants.DefaultUserGroupId.ToString();
        }

        [ForeignKey("Category")]
        public Guid CategoryId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public BrickCategory Category { get; set; }

        [ForeignKey("Group")]
        public Guid? GroupId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public BrickGroup Group { get; set; }

        /// <summary>
        /// Gets or sets the keyboard shortcut that is displayed in the user interface.
        /// (The actual short cut is not set automatically but must be configured in the <code>HotKeys</code> class).
        /// </summary>
        public string HotKey { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the brick will be shown in the Eurolook taskpane.
        /// </summary>
        public bool IsHidden { get; set; }

        [Required(AllowEmptyStrings = true)]
        public string VisibleToUserGroups { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this brick can be inserted once or multiple times per document.
        /// </summary>
        public bool IsMultiInstance { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public HashSet<BrickText> Texts { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public HashSet<DocumentStructure> DocumentModelReferences { get; set; }

        public bool HasCustomUi { get; set; }

        public string MinVersion { get; set; }

        public string MaxVersion { get; set; }

        public bool IsVisibleToUserGroup(string userGroupId)
        {
            return VisibleToUserGroups.ContainsInvariantIgnoreCase(userGroupId);
        }

        public bool IsVisibleToUserGroup(Guid userGroupId)
        {
            return IsVisibleToUserGroup(userGroupId.ToString());
        }
    }
}
