using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Eurolook.Common;

namespace Eurolook.Data.Models
{
    public class PredefinedFunction : Updatable
    {
        public string Name { get; set; }

        [ForeignKey(nameof(FunctionText))]
        public Guid? FunctionTextId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Text FunctionText { get; set; }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        [TranslationFor(nameof(FunctionText))]
        public Translation Function { get; set; }

        [ForeignKey(nameof(FunctionFemaleText))]
        public Guid? FunctionFemaleTextId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Text FunctionFemaleText { get; set; }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        [TranslationFor(nameof(FunctionFemaleText))]
        public Translation FunctionFemale { get; set; }

        public bool ShowInSignature { get; set; }

        public bool ShowInHeader { get; set; }

        public string AdReference { get; set; }

        public bool IsSuperior { get; set; }

        [ForeignKey(nameof(FunctionHeaderText))]
        public Guid? FunctionHeaderTextId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Text FunctionHeaderText { get; set; }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        [TranslationFor(nameof(FunctionHeaderText))]
        public Translation FunctionHeader { get; set; }

        [ForeignKey(nameof(FunctionHeaderFemaleText))]
        public Guid? FunctionHeaderFemaleTextId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Text FunctionHeaderFemaleText { get; set; }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        [TranslationFor(nameof(FunctionHeaderFemaleText))]
        public Translation FunctionHeaderFemale { get; set; }
    }
}
