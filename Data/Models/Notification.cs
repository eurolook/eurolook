﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Eurolook.Common;

namespace Eurolook.Data.Models
{
    public enum NotificationPriority
    {
        Normal = 0,
        Low = 1,
        High = 2,
    }

    public class Notification : Updatable
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }

        public string Content { get; set; }

        [CopyPropertyInclude]
        [MaxLength]
        public byte[] ThumbnailBytes { get; set; }

        public bool IsHidden { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime FromDate { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime ToDate { get; set; }

        public string MinVersion { get; set; }

        public string MaxVersion { get; set; }

        public bool IsHighlighted { get; set; }

        public NotificationPriority Priority { get; set; }
    }
}
