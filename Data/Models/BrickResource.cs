using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Eurolook.Data.Models
{
    public class BrickResource : Updatable
    {
        [ForeignKey("Brick")]
        public Guid BrickId { get; set; }

        [ForeignKey("Resource")]
        public Guid ResourceId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Resource Resource { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Brick Brick { get; set; }
    }
}
