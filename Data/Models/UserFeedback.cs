using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace Eurolook.Data.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Property accessors are used by Entity Framework")]
    public class UserFeedback
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("User")]
        public Guid UserId { get; set; }

        public User User { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime Submitted { get; set; }

        public string Version { get; set; }

        public byte Rating { get; set; }

        public string Message { get; set; }

        public string GetSmilie()
        {
            switch (Rating)
            {
                case 1: return ":)";
                case 2: return ":|";
                case 3: return ":(";
                default: return "?";
            }
        }
    }
}
