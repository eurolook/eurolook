﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Eurolook.Common.Log;

namespace Eurolook.Data.Models
{
    public abstract class Identifiable : ICanLog
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        /// <summary>
        /// generates a new guid.
        /// </summary>
        public virtual void Init()
        {
            Id = Guid.NewGuid();
        }
    }
}
