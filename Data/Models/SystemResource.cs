﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Eurolook.Data.Models
{
    public class SystemResource : Updatable
    {
        public Guid? ResourceId { get; set; }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        public Resource Resource { get; set; }
    }
}
