﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Eurolook.Data.Models
{
    public class JobAssignment : Synchronizable, IJobAssignment
    {
        public JobAssignment()
        {
            Functions = new HashSet<JobFunction>();
        }

        [ForeignKey("OrgaEntity")]
        public Guid? OrgaEntityId { get; set; }

        [Column("AD_OrgaEntityId")]
        public Guid? AdOrgaEntityId { get; set; }

        public OrgaEntity OrgaEntity { get; set; }

        public string Service { get; set; }

        [Column("AD_Service")]
        public string AdService { get; set; }

        public string WebAddress { get; set; }

        public string FunctionalMailbox { get; set; }

        [ForeignKey("Author")]
        public Guid AuthorId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Author Author { get; set; }

        public HashSet<JobFunction> Functions { get; set; }

        public string InheritedWebAddress => GetInheritedWebAddress();

        [ForeignKey("PredefinedFunction")]
        public Guid? PredefinedFunctionId { get; set; }

        [Column("AD_PredefinedFunction")]
        public Guid? AdPredefinedFunctionId { get; set; }

        public PredefinedFunction PredefinedFunction { get; set; }

        [Column("ExternalId")]
        public long? ExternalId { get; set; }

        public object Clone()
        {
            var result = new JobAssignment
            {
                Id = Id,
                ExternalId = ExternalId,
                ServerModificationTimeUtc = ServerModificationTimeUtc,
                ClientModification = ClientModification,
                AuthorId = AuthorId,
                Author = Author,
                OrgaEntityId = OrgaEntityId,
                AdOrgaEntityId = AdOrgaEntityId,
                OrgaEntity = OrgaEntity,
                PredefinedFunctionId = PredefinedFunctionId,
                AdPredefinedFunctionId = AdPredefinedFunctionId,
                PredefinedFunction = PredefinedFunction,
                Service = Service,
                AdService = AdService,
                WebAddress = WebAddress,
                FunctionalMailbox = FunctionalMailbox,
            };
            foreach (var jobFunction in Functions)
            {
                result.Functions.Add((JobFunction)jobFunction.Clone());
            }

            return result;
        }

        public bool IsIncomplete()
        {
            return HasNoOrga();
        }

        public bool HasNoOrga()
        {
            return OrgaEntityId == null;
        }

        private string GetInheritedWebAddress()
        {
            return !string.IsNullOrEmpty(WebAddress) ? WebAddress : OrgaEntity?.InheritedWebAddress;
        }
    }
}
