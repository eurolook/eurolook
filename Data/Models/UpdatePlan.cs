using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Eurolook.Common;

namespace Eurolook.Data.Models
{
    public class UpdatePlan : BasicEurolookDataSet
    {
        private const string IsoUtcFormat = "yyyy-MM-ddTHH:mm:ssZ";

        [IgnoreDataMember]
        [XmlIgnore]
        // Do not serialize, serialize ISOServerTimestampUTC instead
        // ReSharper disable once InconsistentNaming
        public DateTime ServerTimestampUTC
        {
            get
            {
                var dateTime = DateTime.ParseExact(ISOServerTimestampUTC, IsoUtcFormat, CultureInfo.InvariantCulture);
                return TimeUtils.GetUtcDateTime(dateTime);
            }

            set
            {
                var utcDateTime = TimeUtils.GetUtcDateTime(value);
                ISOServerTimestampUTC = utcDateTime.ToString(IsoUtcFormat, CultureInfo.InvariantCulture);
            }
        }

        public List<Notification> Notifications { get; set; }

        // ReSharper disable once InconsistentNaming
        public string ISOServerTimestampUTC { get; set; }

        public bool IsRemoteWipeRequested { get; set; }
    }
}
