﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Eurolook.Data.Models
{
    public class SystemText : Updatable
    {
        public Guid? TextId { get; set; }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        public Text Text { get; set; }
    }
}
