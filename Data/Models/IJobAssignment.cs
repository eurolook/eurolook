using System;
using System.Collections.Generic;

namespace Eurolook.Data.Models
{
    public interface IJobAssignment : ICloneable
    {
        Guid Id { get; set; }

        Guid? OrgaEntityId { get; set; }

        OrgaEntity OrgaEntity { get; set; }

        string Service { get; set; }

        string WebAddress { get; set; }

        string FunctionalMailbox { get; set; }

        HashSet<JobFunction> Functions { get; set; }

        string InheritedWebAddress { get; }

        Guid? PredefinedFunctionId { get; set; }

        PredefinedFunction PredefinedFunction { get; set; }
    }
}
