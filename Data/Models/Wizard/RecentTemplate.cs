﻿using System;

namespace Eurolook.Data.Models.Wizard
{
    public class RecentTemplate
    {
        public Guid TemplateId { get; set; }

        public bool IsDocumentModel { get; set; }
    }
}
