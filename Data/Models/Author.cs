﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Eurolook.Data.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is used by Entity Framework")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Property accessors are used by Entity Framework")]
    public class Author : Synchronizable, IJobAssignment, IAuthor
    {
        private const char NonBreakingSpace = '\u00A0';
        private const char NonBreakingHyphen = '\u2011';

        public Author()
        {
            Users = new HashSet<UserAuthor>();
            Functions = new HashSet<JobFunction>();
            Workplaces = new HashSet<Workplace>();
            JobAssignments = new HashSet<JobAssignment>();
        }

        public string BulgarianFirstName { get; set; }

        public string BulgarianLastName { get; set; }

        public string BulgarianFullName =>
            string.IsNullOrEmpty(BulgarianFirstName) && string.IsNullOrEmpty(BulgarianLastName)
                ? ""
                : (BulgarianFirstName + " " + BulgarianLastName).Trim();

        [IgnoreDataMember]
        [XmlIgnore]
        [NotMapped]
        public string BulgarianLastNameNonBreaking
            => BulgarianLastName?.Replace(' ', NonBreakingSpace).Replace('-', NonBreakingHyphen) ?? "";

        [IgnoreDataMember]
        [XmlIgnore]
        [NotMapped]
        public string BulgarianFullNameNonBreaking
            => string.IsNullOrEmpty(BulgarianFirstName) && string.IsNullOrEmpty(BulgarianLastNameNonBreaking)
                ? ""
                : (BulgarianFirstName + " " + BulgarianLastNameNonBreaking).Trim();

        public string BulgarianFullNameWithLastNameFirst
        {
            get
            {
                if (string.IsNullOrEmpty(BulgarianLastName) || string.IsNullOrEmpty(BulgarianFirstName))
                {
                    return $"{BulgarianLastName} {BulgarianFirstName}".Trim();
                }

                return $"{BulgarianLastName}, {BulgarianFirstName}".Trim();
            }
        }

        public string GreekFirstName { get; set; }

        public string GreekLastName { get; set; }

        public string GreekFullName => string.IsNullOrEmpty(GreekFirstName) && string.IsNullOrEmpty(GreekLastName)
            ? ""
            : (GreekFirstName + " " + GreekLastName).Trim();

        [IgnoreDataMember]
        [XmlIgnore]
        [NotMapped]
        public string GreekLastNameNonBreaking =>
            GreekLastName?.Replace(' ', NonBreakingSpace).Replace('-', NonBreakingHyphen) ?? "";

        [IgnoreDataMember]
        [XmlIgnore]
        [NotMapped]
        public string GreekFullNameNonBreaking =>
            string.IsNullOrEmpty(GreekFirstName) && string.IsNullOrEmpty(GreekLastNameNonBreaking)
                ? ""
                : (GreekFirstName + " " + GreekLastNameNonBreaking).Trim();

        public string GreekFullNameWithLastNameFirst
        {
            get
            {
                if (string.IsNullOrEmpty(GreekLastName) || string.IsNullOrEmpty(GreekFirstName))
                {
                    return $"{GreekLastName} {GreekFirstName}".Trim();
                }

                return $"{GreekLastName}, {GreekFirstName}".Trim();
            }
        }

        public string LatinFirstName { get; set; }

        public string LatinLastName { get; set; }

        public string LatinFullName => string.IsNullOrEmpty(LatinFirstName) && string.IsNullOrEmpty(LatinLastName)
            ? ""
            : (LatinFirstName + " " + LatinLastName).Trim();

        [IgnoreDataMember]
        [XmlIgnore]
        [NotMapped]
        public string LatinLastNameNonBreaking =>
            LatinLastName?.Replace(' ', NonBreakingSpace).Replace('-', NonBreakingHyphen) ?? "";

        [IgnoreDataMember]
        [XmlIgnore]
        [NotMapped]
        public string LatinFullNameNonBreaking =>
            string.IsNullOrEmpty(LatinFirstName) && string.IsNullOrEmpty(LatinLastNameNonBreaking)
                ? ""
                : (LatinFirstName + " " + LatinLastNameNonBreaking).Trim();

        public string FullNameWithLastNameFirst
        {
            get
            {
                if (string.IsNullOrEmpty(LatinLastName) || string.IsNullOrEmpty(LatinFirstName))
                {
                    return $"{LatinLastName} {LatinFirstName}".Trim();
                }

                return $"{LatinLastName}, {LatinFirstName}".Trim();
            }
        }

        public string FullNameWithLastNameLast => $"{LatinFirstName} {LatinLastName}".Trim();

        public string Service { get; set; }

        public string Description { get; set; }

        public string Initials { get; set; }

        public string Email { get; set; }

        [Obsolete("Do not use this as it only works for a single SharePoint site")]
        public int? SharePointAccountId { get; set; }

        [Obsolete("Do not use this as it only works for a single SharePoint site")]
        public string SharePointLoginName { get; set; }

        public string WebAddress { get; set; }

        public string FunctionalMailbox { get; set; }

        [ForeignKey("OrgaEntity")]
        public Guid? OrgaEntityId { get; set; }

        public OrgaEntity OrgaEntity { get; set; }

        [NotMapped]
        public string OrgaEntityName { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public HashSet<UserAuthor> Users { get; set; }

        [ForeignKey("PredefinedFunction")]
        public Guid? PredefinedFunctionId { get; set; }

        public PredefinedFunction PredefinedFunction { get; set; }

        // [IgnoreDataMember]: DataSyncService needs to deliver the Functions of an author
        public HashSet<JobFunction> Functions { get; set; }

        // [IgnoreDataMember]: DataSyncService needs to deliver the Workplaces of an author
        public HashSet<Workplace> Workplaces { get; set; }

        public HashSet<JobAssignment> JobAssignments { get; set; }

        public Guid? MainWorkplaceId { get; set; }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        public string InheritedWebAddress
        {
            get { return GetInheritedWebAddress(); }
        }

        public string Gender { get; set; }

        public bool IsFemale
        {
            get { return string.Equals(Gender, "f", StringComparison.InvariantCultureIgnoreCase); }
        }

        public bool IsMale
        {
            get { return string.Equals(Gender, "m", StringComparison.InvariantCultureIgnoreCase); }
        }

        // Don't use this reference.
        // Entity Framework could not handle two references to the same table (Workplaces and MainWorkplace)
        ////public Workplace MainWorkplace { get; set; }

        // Active Directory Fields
        [Column("AD_LatinFirstName")]
        public string AdLatinFirstName { get; set; }

        [Column("AD_LatinLastName")]
        public string AdLatinLastName { get; set; }

        [Column("AD_Initials")]
        public string AdInitials { get; set; }

        [Column("AD_Service")]
        public string AdService { get; set; }

        [Column("AD_Description")]
        public string AdDescription { get; set; }

        [Column("AD_Email")]
        public string AdEmail { get; set; }

        [Column("AD_WebAddress")]
        public string AdWebAddress { get; set; }

        [Column("AD_Gender")]
        public string AdGender { get; set; }

        [Column("AD_Office")]
        public string AdOffice { get; set; }

        [Column("AD_PhoneExtension")]
        public string AdPhoneExtension { get; set; }

        [Column("AD_FaxExtension")]
        public string AdFaxExtension { get; set; }

        [Column("AD_Address")]
        public string AdAddress { get; set; }

        [Column("AD_PredefinedFunction")]
        public Guid? AdPredefinedFunctionId { get; set; }

        [Column("AD_OrgaEntityId")]
        public Guid? AdOrgaEntityId { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime? LastActiveDirectorySyncTimeUtc { get; set; }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(FullNameWithLastNameLast))
            {
                return Id.ToString();
            }

            return FullNameWithLastNameLast;
        }

        public bool IsIncomplete(bool checkJobDetails = true)
        {
            bool authorIsIncomplete = HasNoFirstName() || HasNoLastName() || HasNoInitials();
            bool jobIsIncomplete = false;
            if (checkJobDetails)
            {
                jobIsIncomplete = HasNoOrga();
            }

            return authorIsIncomplete || jobIsIncomplete;
        }

        public bool HasNoInitials()
        {
            return string.IsNullOrWhiteSpace(Initials);
        }

        public bool HasNoFirstName()
        {
            return string.IsNullOrWhiteSpace(LatinFirstName);
        }

        public bool HasNoLastName()
        {
            return string.IsNullOrWhiteSpace(LatinLastName);
        }

        public bool HasNoOrga()
        {
            return OrgaEntityId == null;
        }

        public bool HasNoService()
        {
            return string.IsNullOrWhiteSpace(Service);
        }

        public object Clone()
        {
            var author = new Author
            {
                Id = Id,
                ServerModificationTimeUtc = ServerModificationTimeUtc,
                ClientModification = ClientModification,
                AdEmail = AdEmail,
                AdOffice = AdOffice,
                AdPhoneExtension = AdPhoneExtension,
                AdService = AdService,
                AdAddress = AdAddress,
                AdLatinFirstName = AdLatinFirstName,
                AdLatinLastName = AdLatinLastName,
                BulgarianFirstName = BulgarianFirstName,
                BulgarianLastName = BulgarianLastName,
                Email = Email,
                Gender = Gender,
                GreekFirstName = GreekFirstName,
                GreekLastName = GreekLastName,
                Initials = Initials,
                LatinFirstName = LatinFirstName,
                LatinLastName = LatinLastName,
                MainWorkplaceId = MainWorkplaceId,
                OrgaEntityId = OrgaEntityId,
                OrgaEntity = OrgaEntity,
                PredefinedFunctionId = PredefinedFunctionId,
                PredefinedFunction = PredefinedFunction,
                Service = Service,
                WebAddress = WebAddress,
                FunctionalMailbox = FunctionalMailbox,
                AdPredefinedFunctionId = AdPredefinedFunctionId,
            };
            foreach (var func in Functions)
            {
                author.Functions.Add((JobFunction)func.Clone());
            }

            foreach (var jobAssignment in JobAssignments)
            {
                author.JobAssignments.Add((JobAssignment)jobAssignment.Clone());
            }

            foreach (var workplace in Workplaces)
            {
                author.Workplaces.Add((Workplace)workplace.Clone());
            }

            return author;
        }

        private string GetInheritedWebAddress()
        {
            return !string.IsNullOrEmpty(WebAddress) ? WebAddress : OrgaEntity?.InheritedWebAddress;
        }
    }
}
