﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Eurolook.Data.Models
{
    public class PerformanceLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Event { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime? Date { get; set; }

        public string Version { get; set; }

        [ForeignKey("DeviceSettings")]
        public Guid? DeviceSettingsId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public DeviceSettings DeviceSettings { get; set; }

        public string DeviceName { get; set; }

        public TimeSpan Total { get; set; }

        public TimeSpan Min { get; set; }

        public TimeSpan Max { get; set; }

        public TimeSpan Average { get; set; }

        public TimeSpan Median { get; set; }

        public int Count { get; set; }

        public string Track { get; set; }
    }

    public enum PerformanceLogEvent
    {
        AddInLoaded,
        CreationDialogLoaded,
        DocumentCreated,
        DataSyncFinished,
        ServerGetUpdates,
        ServerGetInitialization,
        ServerGetUserProfile,
    }
}
