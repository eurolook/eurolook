using System.ComponentModel.DataAnnotations;

namespace Eurolook.Data.Models
{
    public class CommandBrick : Brick
    {
        public string CommandClass { get; set; }

        public string Argument1 { get; set; }

        [MaxLength]
        public string Configuration { get; set; }
    }
}
