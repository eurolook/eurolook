﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Eurolook.Common;

namespace Eurolook.Data.Models
{
    public abstract class DisplayItem : Synchronizable
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string DisplayName { get; set; }

        [CopyPropertyInclude]
        [MaxLength]
        public byte[] Icon { get; set; }

        [CopyPropertyInclude]
        [MaxLength]
        public byte[] VectorIcon { get; set; }

        public int UiPositionIndex { get; set; }

        public string Acronym { get; set; }

        [Column("ColorLuminance")]
        public int ColorLuminanceValue { get; set; }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public Luminance ColorLuminance
        {
            get { return (Luminance)ColorLuminanceValue; }
            set { ColorLuminanceValue = (int)value; }
        }
    }
}
