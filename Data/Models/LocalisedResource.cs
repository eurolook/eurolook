using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Eurolook.Common;

namespace Eurolook.Data.Models
{
    public class LocalisedResource : Updatable
    {
        [ForeignKey("Resource")]
        public Guid ResourceId { get; set; }

        [ForeignKey("Language")]
        public Guid LanguageId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Resource Resource { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Language Language { get; set; }

        [CopyPropertyInclude]
        [MaxLength]
        public byte[] RawData { get; set; }

        public string MimeType { get; set; }

        public string FileNameDate { get; set; }

        public string GetFileName()
        {
            return $"{Resource.Alias}_{Language.Name} {(string.IsNullOrEmpty(FileNameDate) ? "" : FileNameDate)}{GetFileExtension()}";
        }

        public string GetFileExtension()
        {
            return Resource.GetFileExtension(MimeType);
        }
    }
}
