﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Eurolook.Common;
using Eurolook.Common.Log;

namespace Eurolook.Data.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Member is used by Entity Framework")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Property accessors are used by Entity Framework")]
    public class ColorScheme : Updatable
    {
        public string Name { get; set; }

        [Column("PrimaryColor")]
        public string PrimaryColorValue { get; set; }

        [Column("SecondaryColor")]
        public string SecondaryColorValue { get; set; }

        [Column("TileColor1")]
        public string TileColor1Value { get; set; }

        [Column("TileColor2")]
        public string TileColor2Value { get; set; }

        [Column("TileColor3")]
        public string TileColor3Value { get; set; }

        [Column("TileColor1Luminances")]
        public string TileColor1LuminancesValue { get; set; }

        [Column("TileColor2Luminances")]
        public string TileColor2LuminancesValue { get; set; }

        [Column("TileColor3Luminances")]
        public string TileColor3LuminancesValue { get; set; }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public Color PrimaryColor
        {
            get { return ColorTranslator.FromHtml(PrimaryColorValue); }
        }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public Color SecondaryColor
        {
            get { return ColorTranslator.FromHtml(SecondaryColorValue); }
        }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public Color TileColor1
        {
            get { return ColorTranslator.FromHtml(TileColor1Value); }
        }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public Color TileColor2
        {
            get { return ColorTranslator.FromHtml(TileColor2Value); }
        }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public Color TileColor3
        {
            get { return ColorTranslator.FromHtml(TileColor3Value); }
        }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public double[] TileColor1Luminances
        {
            get { return ParseLuminanceValue(TileColor1LuminancesValue); }
        }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public double[] TileColor2Luminances
        {
            get { return ParseLuminanceValue(TileColor2LuminancesValue); }
        }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public double[] TileColor3Luminances
        {
            get { return ParseLuminanceValue(TileColor3LuminancesValue); }
        }

        private double[] ParseLuminanceValue(string value)
        {
            try
            {
                var array = value.Split(',').Select(s => s.Trim()).ToArray();
                return array.Select(s => double.Parse(s, CultureInfo.InvariantCulture)).ToArray();
            }
            catch (Exception)
            {
                this.LogWarnFormat("Cannot parse luminance value: {0}", value);
            }

            return new double[0];
        }
    }
}
