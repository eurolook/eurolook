namespace Eurolook.Data.Models
{
    public interface IAuthor
    {
        string LatinFirstName { get; set; }

        string LatinLastName { get; set; }

        string Initials { get; set; }

        string Email { get; set; }

        string Gender { get; set; }
    }
}
