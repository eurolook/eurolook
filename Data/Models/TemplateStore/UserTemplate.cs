using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Eurolook.Data.Models.TemplateStore
{
    public class UserTemplate : Synchronizable
    {
        [ForeignKey("User")]
        public Guid UserId { get; set; }

        [ForeignKey("Template")]
        public Guid TemplateId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public User User { get; set; }

        public Template Template { get; set; }

        public bool IsOfflineAvailable { get; set; }
    }
}
