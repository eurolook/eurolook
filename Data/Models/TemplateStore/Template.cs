﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Eurolook.Common;

namespace Eurolook.Data.Models.TemplateStore
{
    public class Template : Synchronizable, ICloneable
    {
        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public string Tags { get; set; }

        [ForeignKey("BaseDocumentModel")]
        public Guid? BaseDocumentModelId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public DocumentModel BaseDocumentModel { get; set; }

        public HashSet<TemplateOwner> Owners { get; set; }

        [CopyPropertyInclude]
        [MaxLength]
        public byte[] PreviewPage1 { get; set; }

        [CopyPropertyInclude]
        [MaxLength]
        public byte[] PreviewPage2 { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime? PreviewModifiedTime { get; set; }

        [CopyPropertyInclude]
        [MaxLength]
        public byte[] SmallPreviewPage1 { get; set; }

        [CopyPropertyInclude]
        [MaxLength]
        public byte[] SmallPreviewPage2 { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime? SmallPreviewModifiedTime { get; set; }

        public string PreviewLanguage { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime? PreviewInitiated { get; set; }

        [CopyPropertyInclude]
        [MaxLength]
        public byte[] Thumbnail { get; set; }

        public bool IsPublic { get; set; }

        public bool IsSignerFixed { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime? FeaturedUntil { get; set; }

        public HashSet<TemplatePublication> Publications { get; set; }

        public HashSet<TemplateFile> TemplateFiles { get; set; }

        public object Clone()
        {
            var template = new Template
            {
                Id = Id,
                ServerModificationTimeUtc = ServerModificationTimeUtc,
                ClientModification = ClientModification,
                Name = Name,
                Description = Description,
                Tags = Tags,
                BaseDocumentModelId = BaseDocumentModelId,
                BaseDocumentModel = BaseDocumentModel,
                PreviewPage1 = PreviewPage1,
                PreviewPage2 = PreviewPage2,
                PreviewModifiedTime = PreviewModifiedTime,
                SmallPreviewPage1 = SmallPreviewPage1,
                SmallPreviewPage2 = SmallPreviewPage2,
                SmallPreviewModifiedTime = SmallPreviewModifiedTime,
                PreviewLanguage = PreviewLanguage,
                PreviewInitiated = PreviewInitiated,
                Thumbnail = Thumbnail,
                IsPublic = IsPublic,
                IsSignerFixed = IsSignerFixed,
                FeaturedUntil = FeaturedUntil,
            };

            foreach (var owner in Owners)
            {
                template.Owners.Add((TemplateOwner)owner.Clone());
            }

            foreach (var publication in Publications)
            {
                template.Publications.Add((TemplatePublication)publication.Clone());
            }

            foreach (var file in TemplateFiles)
            {
                template.TemplateFiles.Add((TemplateFile)file.Clone());
            }

            return template;
        }
    }
}
