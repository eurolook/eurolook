﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Eurolook.Data.Models.TemplateStore
{
    public class TemplateOwner : Updatable, ICloneable
    {
        [ForeignKey("Template")]
        public Guid TemplateId { get; set; }

        [ForeignKey("User")]
        public Guid UserId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Template Template { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public User User { get; set; }

        public bool IsMain { get; set; }

        public object Clone()
        {
            var owner = new TemplateOwner()
            {
                Id = Id,
                ServerModificationTimeUtc = ServerModificationTimeUtc,
                TemplateId = TemplateId,
                UserId = UserId,
                Template = Template,
                User = User,
                IsMain = IsMain,
            };

            return owner;
        }
    }
}
