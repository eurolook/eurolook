﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Eurolook.Data.Models.TemplateStore
{
    public class TemplatePublication : Updatable, ICloneable
    {
        [ForeignKey("Template")]
        public Guid TemplateId { get; set; }

        [ForeignKey("OrgaEntity")]
        public Guid OrgaEntityId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Template Template { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public OrgaEntity OrgaEntity { get; set; }

        public object Clone()
        {
            var publication = new TemplatePublication()
            {
                Id = Id,
                ServerModificationTimeUtc = ServerModificationTimeUtc,
                TemplateId = TemplateId,
                OrgaEntityId = OrgaEntityId,
                Template = Template,
                OrgaEntity = OrgaEntity,
            };

            return publication;
        }
    }
}
