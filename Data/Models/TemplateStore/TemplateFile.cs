﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Eurolook.Common;

namespace Eurolook.Data.Models.TemplateStore
{
    public class TemplateFile : Synchronizable, ICloneable
    {
        [ForeignKey("Template")]
        public Guid TemplateId { get; set; }

        [ForeignKey("Language")]
        public Guid LanguageId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Template Template { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Language Language { get; set; }

        [CopyPropertyInclude]
        [MaxLength]
        public byte[] Bytes { get; set; }

        public object Clone()
        {
            var file = new TemplateFile()
            {
                Id = Id,
                ServerModificationTimeUtc = ServerModificationTimeUtc,
                ClientModification = ClientModification,
                TemplateId = TemplateId,
                LanguageId = LanguageId,
                Template = Template,
                Language = Language,
                Bytes = Bytes,
            };

            return file;
        }
    }
}
