namespace Eurolook.Data.Models
{
    public enum InsertionBehavior
    {
        SelectExisting = 0,
        ReplaceExisting,
        AlwaysInsert,
    }
}
