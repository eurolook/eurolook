﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Eurolook.Data.Models
{
    public class DocumentCategory : Updatable
    {
        public string Name { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public HashSet<DocumentModel> DocumentModels { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
