using System;
using System.ComponentModel.DataAnnotations.Schema;
using Eurolook.Common;

namespace Eurolook.Data.Models
{
    public abstract class Updatable : Identifiable
    {
        private DateTime _serverModificationTimeUtc;

        [Column(TypeName = "DateTime2")]
        public DateTime ServerModificationTimeUtc
        {
            get { return _serverModificationTimeUtc; }
            set { _serverModificationTimeUtc = TimeUtils.GetUtcDateTime(value); }
        }

        public bool Deleted { get; set; }

        /// <summary>
        /// Rounds the Milliseconds part of the DateTime to one digit.
        /// E.g.: 12:45,923 -> 12:45,9.
        /// </summary>
        public static DateTime RoundDateTime(DateTime newDateTime)
        {
            var roundedDateTime = new DateTime(
                newDateTime.Ticks - (newDateTime.Ticks % TimeSpan.TicksPerSecond),
                newDateTime.Kind);
            roundedDateTime = roundedDateTime.AddMilliseconds(Math.Round(newDateTime.Millisecond / 100.0, 0) * 100);
            return roundedDateTime;
        }

        /// <summary>
        /// initializes some properties like <see cref="ServerModificationTimeUtc" /> and <see cref="Deleted" />
        /// </summary>
        public override void Init()
        {
            base.Init();
            Deleted = false;
            UpdateServerModificationTime();
        }

        public void SetDeletedFlag(bool isDeleted = true)
        {
            Deleted = isDeleted;
            UpdateServerModificationTime();
        }

        public void SetDeletedFlag(DateTime modificationDate, bool isDeleted = true)
        {
            Deleted = isDeleted;
            UpdateServerModificationTime(modificationDate);
        }

        public void UpdateServerModificationTime(DateTime? dateTime = null)
        {
            var newDateTime = dateTime ?? DateTime.UtcNow;

            // Round the DateTime to one digit because SQL Server and SQLite don't have the same precision.
            ServerModificationTimeUtc = RoundDateTime(newDateTime);
        }
    }
}
