namespace Eurolook.Data.Models
{
    /// <summary>
    /// An enum that defines whether a document structure / brick is inserted during document creation.
    /// </summary>
    public enum AutoBrickCreationType
    {
        /// <summary>
        /// Do not insert the brick when the document is created.
        /// </summary>
        None = 0,

        /// <summary>
        /// Always insert the brick when the document is created.
        /// </summary>
        Always = 1,

        /// <summary>
        /// Insert the brick when a document is created without rich body content.
        /// </summary>
        SimpleBodyContent = 2,

        /// <summary>
        /// Insert the brick when a document with rich / sample document is created.
        /// </summary>
        RichBodyContent = 3,
    }
}
