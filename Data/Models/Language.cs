using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Eurolook.Data.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Property accessors are used by Entity Framework")]
    public class Language : Updatable
    {
        private CultureInfo _cultureInfo;

        /// <summary>
        /// Initializes a new instance of the <see cref="Language" /> class.
        /// </summary>
        public Language()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Language" /> class.
        /// </summary>
        /// <param name="name">The name of the culture, e.g. 'EN' or 'de-DE'.</param>
        public Language(string name)
        {
            Name = name;
        }

        [IgnoreDataMember]
        [XmlIgnore]
        public CultureInfo CultureInfo => _cultureInfo ??= new CultureInfo(Name);

        /// <summary>
        /// Gets or sets a two letter name of the culture, e.g. 'EN' or 'DE'.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the name of the culture as used in the OpenXml format.
        /// </summary>
        public string Locale { get; set; }

        /// <summary>
        /// Gets the full name of the language for display, e.g. "German".
        /// </summary>
        public string DisplayName => CultureInfo?.EnglishName ?? Name;

        /// <summary>
        /// Gets or sets the date format to be used for short and long dates, respectively.
        /// </summary>
        public string DateFormatShort { get; set; }

        public string DateFormatLong { get; set; }

        /// <summary>
        /// Gets or sets the Text for Article style.
        /// </summary>
        public string ArticleText { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public HashSet<DocumentModelLanguage> DocumentModels { get; set; }

        public override string ToString()
        {
            return DisplayName;
        }
    }
}
