﻿namespace Eurolook.Data.Models
{
    public class StyleShortcut : Updatable
    {
        public string Shortcut { get; set; }

        public string StyleName { get; set; }
    }
}
