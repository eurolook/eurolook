﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using Eurolook.Common;

namespace Eurolook.Data.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Property accessors are used by Entity Framework")]
    public class DataPackage : BasicEurolookDataSet
    {
        public DataPackage()
        {
            // Default constructor for serialization
            Id = Guid.NewGuid();
            Created = DateTime.UtcNow;
            Authors = new List<Author>();
            JobAssignments = new List<JobAssignment>();
        }

        [XmlAttribute("xml:space")]
        public string SpacePreserve { get; set; } = "preserve";

        public Guid Id { get; set; }

        public string Description { get; set; }

        public DateTime Created { get; set; }

        public string Report { get; set; }

        public List<Author> Authors { get; }

        public List<JobAssignment> JobAssignments { get; }

        // DataPackage should hide OrgaEntities
        [XmlIgnore]
        public override List<OrgaEntity> OrgaEntities { get; set; }

        public static DataPackage FromFile(string fileName, bool onlyHeaders = false)
        {
            DataPackage result;
            using (var fileStream = File.OpenText(fileName))
            {
                if (onlyHeaders)
                {
                    var xpath = new XPathDocument(fileStream);
                    var navigator = xpath.CreateNavigator();
                    var id = navigator.SelectSingleNode("/DataPackage/Id");
                    var description = navigator.SelectSingleNode("/DataPackage/Description");
                    var created = navigator.SelectSingleNode("/DataPackage/Created");
                    var report = navigator.SelectSingleNode("/DataPackage/Report");
                    result = new DataPackage
                    {
                        Id = id != null ? Guid.Parse(id.Value) : Guid.Empty,
                        Description = description != null ? description.Value : "",
                        Created = created != null ? DateTime.Parse(created.Value) : DateTime.MinValue,
                        Report = report != null ? report.Value : "",
                    };
                }
                else
                {
                    var serializer = new XmlSerializer(typeof(DataPackage));
                    result = (DataPackage)serializer.Deserialize(fileStream);
                }
            }

            return result;
        }

        public static bool EntityEquals(object a, object b)
        {
            var type = a.GetType();
            if (type != b.GetType())
            {
                return false;
            }

            foreach (var property in type.GetProperties())
            {
                if (SkipProperty(property))
                {
                    continue;
                }

                var aVal = property.GetValue(a);
                var bVal = property.GetValue(b);
                if (aVal != null)
                {
                    if (aVal.GetType() == typeof(byte[]))
                    {
                        var aBytes = (byte[])aVal;
                        var bBytes = (byte[])bVal;
                        if (bBytes == null)
                        {
                            return false;
                        }

                        if (!aBytes.SequenceEqual(bBytes))
                        {
                            return false;
                        }
                    }
                    else if (!aVal.Equals(bVal))
                    {
                        return false;
                    }
                }
                else if (bVal != null)
                {
                    return false;
                }
            }

            return true;
        }

        public static bool SkipProperty(PropertyInfo property)
        {
            if (!property.CanRead || !property.CanWrite)
            {
                return true;
            }

            if (property.Name.Equals("ServerModificationTimeUtc", StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }

            if (property.GetCustomAttribute<NotMappedAttribute>(true) != null)
            {
                return true;
            }

            if (property.GetCustomAttribute<XmlIgnoreAttribute>(true) != null)
            {
                return true;
            }

            if (property.GetCustomAttribute<IgnoreDataMemberAttribute>(true) != null)
            {
                return true;
            }

            if (property.GetCustomAttribute<CopyPropertyIgnoreAttribute>(true) != null)
            {
                return true;
            }

            return false;
        }

        public DataPackage Diff(DataPackage other)
        {
            var result = new DataPackage
            {
                Description = $"Diff of {other.Id}",
            };
            result.Authors.AddRange(DiffEntities(other.Authors, Authors));
            result.Addresses.AddRange(DiffEntities(other.Addresses, Addresses));
            result.BrickCategories.AddRange(DiffEntities(other.BrickCategories, BrickCategories));
            result.UserGroups.AddRange(DiffEntities(other.UserGroups, UserGroups));
            result.BrickGroups.AddRange(DiffEntities(other.BrickGroups, BrickGroups));
            result.BrickResources.AddRange(DiffEntities(other.BrickResources, BrickResources));
            result.BrickTexts.AddRange(DiffEntities(other.BrickTexts, BrickTexts));
            result.CharacterMappings.AddRange(DiffEntities(other.CharacterMappings, CharacterMappings));
            result.CommandBricks.AddRange(DiffEntities(other.CommandBricks, CommandBricks));
            result.ContentBricks.AddRange(DiffEntities(other.ContentBricks, ContentBricks));
            result.ColorSchemes.AddRange(DiffEntities(other.ColorSchemes, ColorSchemes));
            result.DocumentCategories.AddRange(DiffEntities(other.DocumentCategories, DocumentCategories));
            result.DocumentModels.AddRange(DiffEntities(other.DocumentModels, DocumentModels));
            result.DocumentModelLanguages.AddRange(DiffEntities(other.DocumentModelLanguages, DocumentModelLanguages));
            result.DocumentStructures.AddRange(DiffEntities(other.DocumentStructures, DocumentStructures));
            result.DynamicBricks.AddRange(DiffEntities(other.DynamicBricks, DynamicBricks));
            result.JobAssignments.AddRange(DiffEntities(other.JobAssignments, JobAssignments));
            result.Languages.AddRange(DiffEntities(other.Languages, Languages));
            result.LocalisedResources.AddRange(DiffEntities(other.LocalisedResources, LocalisedResources));
            result.PersonNames.AddRange(DiffEntities(other.PersonNames, PersonNames));
            result.Resources.AddRange(DiffEntities(other.Resources, Resources));
            result.StyleShortcuts.AddRange(DiffEntities(other.StyleShortcuts, StyleShortcuts));
            result.Texts.AddRange(DiffEntities(other.Texts, Texts));
            result.Translations.AddRange(DiffEntities(other.Translations, Translations));
            result.OrgaEntities.AddRange(DiffEntities(other.OrgaEntities, OrgaEntities));
            result.MetadataCategories.AddRange(DiffEntities(other.MetadataCategories, MetadataCategories));
            result.MetadataDefinitions.AddRange(DiffEntities(other.MetadataDefinitions, MetadataDefinitions));
            result.DocumentModelMetadataDefinitions.AddRange(DiffEntities(other.DocumentModelMetadataDefinitions, DocumentModelMetadataDefinitions));
            result.DocumentModelAuthorRoles.AddRange(DiffEntities(other.DocumentModelAuthorRoles, DocumentModelAuthorRoles));
            result.AuthorRoles.AddRange(DiffEntities(other.AuthorRoles, AuthorRoles));
            result.TermStoreImports.AddRange(DiffEntities(other.TermStoreImports, TermStoreImports));
            result.PredefinedFunctions.AddRange(DiffEntities(other.PredefinedFunctions, PredefinedFunctions));
            result.SystemTexts.AddRange(DiffEntities(other.SystemTexts, SystemTexts));
            result.SystemResources.AddRange(DiffEntities(other.SystemResources, SystemResources));
            result.SystemConfigurations.AddRange(DiffEntities(other.SystemConfigurations, SystemConfigurations));
            result.Notifications.AddRange(DiffEntities(other.Notifications, Notifications));
            return result;
        }

        public DataPackage DiffOrgaUploadPackage(DataPackage other)
        {
            var result = new DataPackage
            {
                Description = $"Diff of {other.Id}",
            };
            result.Addresses.AddRange(DiffEntities(other.Addresses, Addresses));
            result.Languages.AddRange(DiffEntities(other.Languages, Languages));
            result.Texts.AddRange(DiffEntities(other.Texts, Texts));
            result.Translations.AddRange(DiffEntities(other.Translations, Translations));
            result.OrgaEntities.AddRange(DiffEntities(other.OrgaEntities, OrgaEntities));
            return result;
        }

        public void SaveTo(Stream stream)
        {
            var writer = new XmlSerializer(typeof(DataPackage));
            writer.Serialize(
                XmlWriter.Create(
                    stream,
                    new XmlWriterSettings { Indent = true }),
                this);
        }

        public void SaveAs(string fileName)
        {
            using (var fileStream = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write))
            {
                SaveTo(fileStream);
            }
        }

        private static List<T> DiffEntities<T>(IEnumerable<T> otherEntities, List<T> diffBase)
            where T : Updatable
        {
            var result = new List<T>();
            foreach (var otherEntity in otherEntities)
            {
                if (otherEntity != null)
                {
                    T baseEntity = null;
                    if (diffBase.Any())
                    {
                        baseEntity = diffBase.FirstOrDefault(x => x.Id == otherEntity.Id);
                    }

                    if (baseEntity != null)
                    {
                        // its an updated entity
                        if (!EntityEquals(baseEntity, otherEntity))
                        {
                            result.Add(otherEntity);
                        }
                    }
                    else if (!otherEntity.Deleted)
                    {
                        // it is an additional
                        result.Add(otherEntity);
                    }
                }
            }

            return result;
        }
    }
}
