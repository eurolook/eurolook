using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Eurolook.Data.Models
{
    public class ActionLog
    {
        public ActionLog()
        {
            CreatedUtc = DateTime.UtcNow;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Action { get; set; }

        public Guid? OriginatorId { get; set; }

        [ForeignKey("DocumentModel")]
        public Guid? DocumentModelId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public DocumentModel DocumentModel { get; set; }

        public string Info { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime CreatedUtc { get; set; }
    }
}
