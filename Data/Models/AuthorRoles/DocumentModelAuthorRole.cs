using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Eurolook.Data.Models.AuthorRoles
{
    public class DocumentModelAuthorRole : Updatable
    {
        [ForeignKey("DocumentModel")]
        public Guid DocumentModelId { get; set; }

        [ForeignKey("AuthorRole")]
        public Guid AuthorRoleId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public DocumentModel DocumentModel { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public AuthorRole AuthorRole { get; set; }

        public string Description { get; set; }
    }
}
