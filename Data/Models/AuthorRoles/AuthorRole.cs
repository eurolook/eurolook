using System.ComponentModel.DataAnnotations;
using Eurolook.Common;

namespace Eurolook.Data.Models.AuthorRoles
{
    public class AuthorRole : Updatable
    {
        private string _name;

        [MaxLength(200)]
        public string Name
        {
            get => _name;
            set => _name = Tools.GetCleanXmlName(value);
        }

        public string DisplayName { get; set; }

        public string Description { get; set; }

        public int UiPositionIndex { get; set; }
    }
}
