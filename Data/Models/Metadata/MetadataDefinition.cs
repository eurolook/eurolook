﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Eurolook.Common;
using Eurolook.Common.Extensions;

namespace Eurolook.Data.Models.Metadata
{
    public class MetadataDefinition : Updatable
    {
        private string _name;

        [MaxLength(200)]
        public string Name
        {
            get => _name;
            set => _name = Tools.GetCleanXmlName(value);
        }

        public string DisplayName { get; set; }

        public string Description { get; set; }

        public int? MinLength { get; set; }

        public int? MaxLength { get; set; }

        public int Position { get; set; }

        public bool IsReadOnly { get; set; }

        public string EditableByUserGroups { get; set; }

        public bool IsHidden { get; set; }

        public bool IsMandatory { get; set; }

        public MetadataType MetadataType { get; set; }

        public Guid? TermSetId { get; set; }

        [ForeignKey("MetadataCategory")]
        public Guid? MetadataCategoryId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public MetadataCategory MetadataCategory { get; set; }

        public bool IsBoundToTermSet()
        {
            return (MetadataType == MetadataType.SingleBoundedTerm ||
                    MetadataType == MetadataType.MultipleBoundedTerms)
                   && TermSetId.HasValue;
        }

        public bool IsEditableByUserGroup(Guid id)
        {
            return EditableByUserGroups?.ContainsInvariantIgnoreCase(id.ToString()) == true;
        }
    }
}
