using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Eurolook.Data.Models.Metadata
{
    public interface IMetadataPropertyFactory
    {
        MetadataProperty Create(MetadataDefinition metadataDefinition, string value);

        MetadataProperty Create(MetadataDefinition metadataDefinition, Guid termId);

        MetadataProperty Create(MetadataDefinition metadataDefinition, IEnumerable<Guid> termIds);

        MetadataProperty Create(MetadataDefinition metadataDefinition, UserInfo userInfo);

        MetadataProperty Create(MetadataDefinition metadataDefinition, IEnumerable<UserInfo> userInfos);

        Task InitAsync(int languageCode);
    }
}
