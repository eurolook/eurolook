using System;
using System.Runtime.Serialization;

namespace Eurolook.Data.Models.Metadata
{
    [Serializable]
    public class InvalidMetadataTypeException : Exception
    {
        public InvalidMetadataTypeException()
        {
        }

        public InvalidMetadataTypeException(string message)
            : base(message)
        {
        }

        public InvalidMetadataTypeException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected InvalidMetadataTypeException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}
