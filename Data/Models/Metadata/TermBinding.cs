using System;

namespace Eurolook.Data.Models.Metadata
{
    public class TermBinding
    {
        public TermBinding(string termName, Guid termId)
        {
            TermName = termName;
            TermId = termId;
        }

        public string TermName { get; }

        public Guid TermId { get; }
    }
}
