using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Data.TermStore;
using JetBrains.Annotations;

namespace Eurolook.Data.Models.Metadata
{
    public class MetadataPropertyFactory : IMetadataPropertyFactory
    {
        private readonly ITermStoreCache _termStoreCache;
        private SharePointTermStore.TermStore _termStore;
        private int _languageCode;

        public MetadataPropertyFactory(ITermStoreCache termStoreCache)
        {
            _termStoreCache = termStoreCache;
        }

        public async Task InitAsync(int languageCode)
        {
            _termStore = await _termStoreCache.GetTermStoreAsync();
            _languageCode = languageCode;
        }

        public MetadataProperty Create(MetadataDefinition metadataDefinition, IEnumerable<Guid> termIds)
        {
            switch (metadataDefinition.MetadataType)
            {
                case MetadataType.MultipleBoundedTerms:
                    var termBindings = termIds.Select(t => GetTermBinding(metadataDefinition, t))
                                              .Where(t => t != null)
                                              .ToList();

                    return new MetadataProperty(metadataDefinition.Name, termBindings);
                default:
                    throw new InvalidOperationException();
            }
        }

        public MetadataProperty Create(MetadataDefinition metadataDefinition, Guid termId)
        {
            switch (metadataDefinition.MetadataType)
            {
                case MetadataType.SingleBoundedTerm:
                    return new MetadataProperty(metadataDefinition.Name, GetTermBinding(metadataDefinition, termId));
                case MetadataType.MultipleBoundedTerms:
                    var termBindings = new List<TermBinding>();
                    var termBinding = GetTermBinding(metadataDefinition, termId);
                    if (termBinding != null)
                    {
                        termBindings.Add(termBinding);
                    }

                    return new MetadataProperty(metadataDefinition.Name, termBindings);
                default:
                    throw new InvalidOperationException();
            }
        }

        public MetadataProperty Create(MetadataDefinition metadataDefinition, UserInfo userInfo)
        {
            switch (metadataDefinition.MetadataType)
            {
                case MetadataType.SingleUser:
                    return new MetadataProperty(metadataDefinition.Name, userInfo);

                default:
                    throw new InvalidOperationException();
            }
        }

        public MetadataProperty Create(MetadataDefinition metadataDefinition, IEnumerable<UserInfo> userInfos)
        {
            switch (metadataDefinition.MetadataType)
            {
                case MetadataType.MultipleUsers:
                    return new MetadataProperty(metadataDefinition.Name, userInfos);

                default:
                    throw new InvalidOperationException();
            }
        }

        public MetadataProperty Create(MetadataDefinition metadataDefinition, string value)
        {
            switch (metadataDefinition.MetadataType)
            {
                case MetadataType.SingleBoundedTerm:
                    return new MetadataProperty(metadataDefinition.Name, GetTermBinding(metadataDefinition, value));
                case MetadataType.MultipleBoundedTerms:
                    var termBindings = new List<TermBinding>();
                    var termBinding = GetTermBinding(metadataDefinition, value);
                    if (termBinding != null)
                    {
                        termBindings.Add(termBinding);
                    }

                    return new MetadataProperty(metadataDefinition.Name, termBindings);

                case MetadataType.SingleUser:
                case MetadataType.MultipleUsers:
                    return new MetadataProperty(metadataDefinition.Name, (UserInfo)null);

                default:
                    return new MetadataProperty(metadataDefinition.Name, value);
            }
        }

        [CanBeNull]
        private TermBinding GetTermBinding(MetadataDefinition metadataDefinition, string termIdString)
        {
            if (!Guid.TryParse(termIdString, out var termId))
            {
                return null;
            }

            return GetTermBinding(metadataDefinition, termId);
        }

        [CanBeNull]
        private TermBinding GetTermBinding(MetadataDefinition metadataDefinition, Guid termId)
        {
            if (metadataDefinition.TermSetId == null)
            {
                return null;
            }

            var defaultTermLabel = _termStore.GetDefaultTermLabel(termId, _languageCode)
                                   ?? _termStore.GetDefaultTermLabel(termId); // EN as fallback

            return new TermBinding(defaultTermLabel?.Value, termId);
        }
    }
}
