namespace Eurolook.Data.Models.Metadata
{
    public enum MetadataType
    {
        Text = 0,
        LongText = 1,
        Numeric = 2,
        Bool = 3,
        Date = 4,
        Time = 5,
        DateTime = 6,
        SingleBoundedTerm = 7,
        MultipleBoundedTerms = 8,
        SingleUser = 9,
        MultipleUsers = 10,
    }
}
