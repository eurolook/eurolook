using System;
using System.Collections.Generic;
using System.Linq;

namespace Eurolook.Data.Models.Metadata
{
    public class MetadataProperty
    {
        private readonly string _value;
        private readonly IEnumerable<TermBinding> _termBindings;
        private readonly IEnumerable<UserInfo> _userInfos;

        private readonly Func<string> _toString;

        public MetadataProperty(string name, string value)
            : this(MetadataSerializationType.SimpleValue, name)
        {
            _value = value;
            _toString = GetSimpleValue;
        }

        public MetadataProperty(string name, TermBinding termBinding)
            : this(MetadataSerializationType.SingleBoundedTerm, name)
        {
            _termBindings = new List<TermBinding> { termBinding };
            _toString = () => GetTermBinding()?.TermId.ToString();
        }

        public MetadataProperty(string name, IEnumerable<TermBinding> termBindings)
            : this(MetadataSerializationType.MultipleBoundedTerms, name)
        {
            _termBindings = termBindings;
            _toString = () => string.Join(";", GetTermBindings().Select(tb => tb.TermId));
        }

        public MetadataProperty(string name, UserInfo userInfo)
            : this(MetadataSerializationType.SingleUser, name)
        {
            _userInfos = new List<UserInfo> { userInfo };
            _toString = () => GetUserInfo()?.DisplayName;
        }

        public MetadataProperty(string name, IEnumerable<UserInfo> userInfos)
            : this(MetadataSerializationType.MultipleUsers, name)
        {
            _userInfos = userInfos;
            _toString = () => string.Join(";", GetUserInfos().Select(u => u.DisplayName));
        }

        private MetadataProperty(MetadataSerializationType metadataSerializationType, string name)
        {
            Name = name;
            MetadataSerializationType = metadataSerializationType;
        }

        public MetadataSerializationType MetadataSerializationType { get; }

        public string Name { get; }

        public string GetSimpleValue()
        {
            if (MetadataSerializationType != MetadataSerializationType.SimpleValue)
            {
                throw new InvalidMetadataTypeException();
            }

            return _value;
        }

        public TermBinding GetTermBinding()
        {
            if (MetadataSerializationType != MetadataSerializationType.SingleBoundedTerm)
            {
                throw new InvalidMetadataTypeException();
            }

            return _termBindings.Single();
        }

        public IEnumerable<TermBinding> GetTermBindings()
        {
            if (MetadataSerializationType != MetadataSerializationType.MultipleBoundedTerms)
            {
                throw new InvalidMetadataTypeException();
            }

            return _termBindings;
        }

        public UserInfo GetUserInfo()
        {
            if (MetadataSerializationType != MetadataSerializationType.SingleUser)
            {
                throw new InvalidOperationException();
            }

            return _userInfos.Single();
        }

        public IEnumerable<UserInfo> GetUserInfos()
        {
            if (MetadataSerializationType != MetadataSerializationType.MultipleUsers)
            {
                throw new InvalidOperationException();
            }

            return _userInfos;
        }

        public override string ToString()
        {
            return _toString();
        }
    }
}
