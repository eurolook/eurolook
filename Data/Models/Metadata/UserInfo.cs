﻿namespace Eurolook.Data.Models.Metadata
{
    /// <summary>
    /// A class holding the information of a SharePoint content-type property of type User.
    /// </summary>
    /// <remarks>
    /// A sample properties XML snippet looks as follows:
    ///
    ///         &lt;Metadata_Property_Name xmlns="0642e2f2-8d4b-4433-b1b5-b9ddd33ff8f0">
    ///             &lt;UserInfo>
    ///                 &lt;DisplayName>&lt;/DisplayName>
    ///                 &lt;AccountId xsi:nil="true">&lt;/AccountId>
    ///                 &lt;AccountType/>
    ///             &lt;/UserInfo>
    ///         &lt;/Metadata_Property_Name>
    /// </remarks>
    public class UserInfo
    {
        public UserInfo()
        {
        }

        public UserInfo(int? accountId, string displayName, string accountType = null)
        {
            AccountId = accountId;
            DisplayName = displayName;
            AccountType = accountType;
        }

        public int? AccountId { get; }

        public string DisplayName { get; }

        public string AccountType { get; }
    }
}
