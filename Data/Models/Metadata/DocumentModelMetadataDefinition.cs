﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Eurolook.Data.Models.Metadata
{
    public class DocumentModelMetadataDefinition : Updatable
    {
        [ForeignKey("DocumentModel")]
        public Guid DocumentModelId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public DocumentModel DocumentModel { get; set; }

        [ForeignKey("MetadataDefinition")]
        public Guid MetadataDefinitionId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public MetadataDefinition MetadataDefinition { get; set; }

        public string DefaultValue { get; set; }

        public bool? IsReadOnlyOverride { get; set; }

        public int? MinLengthOverride { get; set; }

        public int? MaxLengthOverride { get; set; }

        public bool? IsHiddenOverride { get; set; }

        public bool? IsMandatoryOverride { get; set; }

        public string CalculationCommandClassName { get; set; }

        public string EditableByUserGroups { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public bool IsReadOnly => IsReadOnlyOverride ?? MetadataDefinition.IsReadOnly;

        [IgnoreDataMember]
        [XmlIgnore]
        public int? MinLength => MinLengthOverride ?? MetadataDefinition.MinLength;

        [IgnoreDataMember]
        [XmlIgnore]
        public int? MaxLength => MaxLengthOverride ?? MetadataDefinition.MaxLength;

        [IgnoreDataMember]
        [XmlIgnore]
        public bool IsHidden => IsHiddenOverride ?? MetadataDefinition.IsHidden;

        [IgnoreDataMember]
        [XmlIgnore]
        public bool IsMandatory => IsMandatoryOverride ?? MetadataDefinition.IsMandatory;
    }
}
