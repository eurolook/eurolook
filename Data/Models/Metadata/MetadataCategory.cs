using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Eurolook.Common;

namespace Eurolook.Data.Models.Metadata
{
    public class MetadataCategory : Updatable
    {
        [MaxLength(200)]
        public string Name { get; set; }

        public string Description { get; set; }

        public string DisplayName { get; set; }

        public int UiPositionIndex { get; set; }

        [IgnoreDataMember]
        [CopyPropertyIgnore]
        [XmlIgnore]
        public HashSet<MetadataDefinition> MetadataDefinitions { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
