namespace Eurolook.Data.Models.Metadata
{
    public enum MetadataSerializationType
    {
        SimpleValue,
        SingleBoundedTerm,
        MultipleBoundedTerms,
        SingleUser,
        MultipleUsers,
    }
}
