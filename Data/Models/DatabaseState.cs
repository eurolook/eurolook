using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Eurolook.Common;
using JetBrains.Annotations;

namespace Eurolook.Data.Models
{
    public class DatabaseState
    {
        private DateTime? _lastUpdateUtc;
        private DateTime? _lastSuccessfulRunDateUtc;
        private DateTime? _lastRunDateUtc;

        [UsedImplicitly]
        public int Id { get; set; }

        public string ProductCustomizationId { get; set; }

        [Column("ClientVersion")]
        [UsedImplicitly]
        public string RawClientVersion { get; set; }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public Version ClientVersion
        {
            get { return Version.Parse(RawClientVersion); }
            set { RawClientVersion = value.ToString(); }
        }

        public DateTime? LastUpdateUtc
        {
            get { return _lastUpdateUtc; }
            set
            {
                if (value.HasValue)
                {
                    _lastUpdateUtc = TimeUtils.GetUtcDateTime(value.Value);
                }
            }
        }

        public DateTime? LastSuccessfulRunDateUtc
        {
            get { return _lastSuccessfulRunDateUtc; }
            set
            {
                if (value != null)
                {
                    _lastSuccessfulRunDateUtc = TimeUtils.GetUtcDateTime(value.Value);
                }
                else
                {
                    _lastSuccessfulRunDateUtc = null;
                }
            }
        }

        public DateTime? LastRunDateUtc
        {
            get { return _lastRunDateUtc; }
            set
            {
                if (value != null)
                {
                    _lastRunDateUtc = TimeUtils.GetUtcDateTime(value.Value);
                }
                else
                {
                    _lastRunDateUtc = null;
                }
            }
        }
    }
}
