using System.ComponentModel.DataAnnotations;
using Eurolook.Common;

namespace Eurolook.Data.Models
{
    public class BuildingBlockBrick : Brick
    {
        [CopyPropertyInclude]
        [MaxLength]
        public byte[] BuildingBlockBytes { get; set; }

        [CopyPropertyInclude]
        [MaxLength]
        public byte[] PreviewImageBytes { get; set; }
    }
}
