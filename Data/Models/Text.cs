using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Eurolook.Data.Models
{
    public class Text : Updatable
    {
        [MaxLength(200)]
        public string Alias { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public HashSet<Translation> Translations { get; set; }
    }
}
