using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace Eurolook.Data.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Property accessors are used by Entity Framework")]
    public class UserGroup : Updatable
    {
        [MaxLength(100)]
        public string Name { get; set; }

        public string Description { get; set; }

        public int UiPositionIndex { get; set; }

        public string AdGroups { get; set; }
    }
}
