using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Eurolook.Common;

namespace Eurolook.Data.Models
{
    public class OrgaEntity : Updatable
    {
        [ForeignKey("SuperEntity")]
        public Guid? SuperEntityId { get; set; }

        public OrgaEntity SuperEntity { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        // do not serialize -> circular reference
        public List<OrgaEntity> SubEntities { get; set; }

        [ForeignKey("PrimaryAddress")]
        public Guid? PrimaryAddressId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Address PrimaryAddress { get; set; }

        [ForeignKey("SecondaryAddress")]
        public Guid? SecondaryAddressId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Address SecondaryAddress { get; set; }

        public int LogicalLevel { get; set; }

        public string Name { get; set; }

        [ForeignKey("HeaderText")]
        public Guid? HeaderTextId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Text HeaderText { get; set; }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        [TranslationFor("HeaderText")]
        public Translation Header { get; set; }

        public string WebAddress { get; set; }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        public string InheritedWebAddress
        {
            get { return GetInhertitedWebAddress(this); }
        }

        public int OrderIndex { get; set; }

        [NotMapped]
        public bool IsNew { get; set; }

        [NotMapped]
        public bool WasDeleted { get; set; }

        public bool ShowInHeader { get; set; }

        public string Function { get; set; }

        public string Gender { get; set; }

        public string RepositoryReference { get; set; }

        public override string ToString()
        {
            return Header != null ? Header.ToString() : Id.ToString();
        }

        public void SetSuperEntity(OrgaEntity superEntity)
        {
            SuperEntity = superEntity;
            SuperEntityId = superEntity != null ? (Guid?)superEntity.Id : null;
        }

        private string GetInhertitedWebAddress(OrgaEntity entity)
        {
            if (!string.IsNullOrEmpty(entity.WebAddress))
            {
                return entity.WebAddress;
            }

            if (entity.SuperEntity != null)
            {
                return GetInhertitedWebAddress(entity.SuperEntity);
            }

            return null;
        }
    }
}
