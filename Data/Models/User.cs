﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using Eurolook.Common;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data.Constants;
using Eurolook.Data.Database;
using Eurolook.Data.Models.TemplateStore;

namespace Eurolook.Data.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Property accessors are used by Entity Framework")]
    public class User : Synchronizable
    {
        private DateTime _createdTimeUtc;

        public User()
        {
            Authors = new HashSet<UserAuthor>();
            Superiors = new HashSet<UserSuperior>();
            UserGroupIds = CommonDataConstants.DefaultUserGroupId.ToString();
            Templates = new HashSet<UserTemplate>();
        }

        [MaxLength(200)]
        public string Login { get; set; }

        [ForeignKey("Self")]
        public Guid SelfId { get; set; }

        public Author Self { get; set; }

        [ForeignKey("Settings")]
        public Guid SettingsId { get; set; }

        public UserSettings Settings { get; set; }

        public HashSet<UserAuthor> Authors { get; set; }

        public HashSet<UserSuperior> Superiors { get; set; }

        public HashSet<UserBrick> Bricks { get; set; }

        public HashSet<UserTemplate> Templates { get; set; }

        [Obsolete("Obsolete starting from version 1912")]
        public bool IsElevated { get; set; }

        [Obsolete("Obsolete starting from version 2003")]
        public Guid UserGroupId { get; set; }

        public string UserGroupIds { get; set; }

        public string AdGroups { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime CreatedTimeUtc
        {
            get { return _createdTimeUtc; }
            set { _createdTimeUtc = TimeUtils.GetUtcDateTime(value); }
        }

        public override void Init()
        {
            base.Init();
            CreatedTimeUtc = DateTime.UtcNow;
        }

        public Author GetAuthor(Guid id)
        {
            if (Authors == null)
            {
                this.LogWarn("property Authors is not set, GetAuthor will return null");
                return null;
            }

            var authorRef = Authors.SingleOrDefault(a => a.AuthorId == id);
            if (authorRef == null)
            {
                this.LogWarn("authorRef not found. GetAuthor will return null");
                return null;
            }

            if (authorRef.Author == null)
            {
                this.LogWarn("property Author is not set for UserAuthor, GetAuthor will return null");
            }

            return authorRef.Author;
        }

        public bool IsMemberOfUserGroup(Guid userGroupId)
        {
            return UserGroupIds.ContainsInvariantIgnoreCase(userGroupId.ToString());
        }
    }
}
