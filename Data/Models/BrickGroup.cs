﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Eurolook.Common;

namespace Eurolook.Data.Models
{
    public class BrickGroup : DisplayItem
    {
        public bool IsMultipleChoice { get; set; }

        [IgnoreDataMember]
        [CopyPropertyIgnore]
        [XmlIgnore]
        public List<Brick> Bricks { get; set; }

        public override bool Equals(object obj)
        {
            if (obj is BrickGroup)
            {
                var other = obj as BrickGroup;
                return other.Id == Id;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
