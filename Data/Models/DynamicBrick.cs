using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.Xsl;
using Eurolook.Common;
using Eurolook.Common.Log;

namespace Eurolook.Data.Models
{
    public class DynamicBrick : ContentBrick, ICanLog
    {
        private XslCompiledTransform _transform;
        private XDocument _content;
        private XDocument _combinedPropertiesXml;

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public override XDocument Content => _content;

        public override void Initialize(XDocument combinedPropertiesXml)
        {
            _content = null;
            _transform = null;
            _combinedPropertiesXml = combinedPropertiesXml;
            ApplyXslTransformation();
        }

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times", Justification = "Reviewed")]
        private void ApplyXslTransformation()
        {
            try
            {
                if (_transform == null)
                {
                    _transform = new XslCompiledTransform();
                    using (var stringReader = new StringReader(RawContent))
                    {
                        using (var xslt = XmlReader.Create(stringReader))
                        {
                            _transform.Load(xslt);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                _content = XDocument.Parse("<html><p><span>Could not load dynamic brick.</span></p></html>");
                _transform = null;
                return;
            }

            try
            {
                var content = new XDocument();
                using (var writer = content.CreateWriter())
                {
                    _transform.Transform(_combinedPropertiesXml.CreateReader(), writer);
                }

                _content = content;
            }
            catch (Exception ex)
            {
                this.LogError(ex);
                _content = XDocument.Parse("<html><p><span>Could not process dynamic brick.</span></p></html>");
            }
        }
    }
}
