using System;

namespace Eurolook.Data.Models
{
    [Flags]
    public enum StoryType
    {
        Begin = 0x1,
        Body = 0x2,
        End = 0x4,
        Header = 0x10,
        Footer = 0x20,
        None = 0x1000,
    }

    [Flags]
    public enum Occurrence
    {
        First = 0x40,
        Odd = 0x80,
        Even = 0x100,
        All = First | Odd | Even,
    }

    public enum PositionType
    {
        Cursor = 0x0,
        Begin = StoryType.Begin,
        Body = StoryType.Body,
        End = StoryType.End,

        HeaderNone = StoryType.Header,
        HeaderFirst = StoryType.Header | Occurrence.First, // 0x50 = 80
        HeaderOdd = StoryType.Header | Occurrence.Odd, // 0x90 = 144
        HeaderEven = StoryType.Header | Occurrence.Even, // 0x110 = 272
        HeaderFirstOdd = StoryType.Header | Occurrence.First | Occurrence.Odd, // 0xD0 = 208
        HeaderFirstEven = StoryType.Header | Occurrence.First | Occurrence.Even, // 0x150 = 336
        HeaderOddEven = StoryType.Header | Occurrence.Odd | Occurrence.Even, // 0x190 = 400
        HeaderAll = StoryType.Header | Occurrence.All, // 0x1D0 = 464

        FooterNone = StoryType.Footer,
        FooterFirst = StoryType.Footer | Occurrence.First, // 0x60 = 96
        FooterOdd = StoryType.Footer | Occurrence.Odd, // 0xA0 = 160
        FooterEven = StoryType.Footer | Occurrence.Even, // 0x120 = 288
        FooterFirstOdd = StoryType.Footer | Occurrence.First | Occurrence.Odd, // 0xE0 = 224
        FooterFirstEven = StoryType.Footer | Occurrence.First | Occurrence.Even, // 0x160 = 352
        FooterOddEven = StoryType.Footer | Occurrence.Odd | Occurrence.Even, // 0x1A0 = 416
        FooterAll = StoryType.Footer | Occurrence.All, // 1E0 = 480
    }

    public enum ModificationType
    {
        ClientModification,
        ServerModification,
        Synchronization,
    }

    public enum Luminance
    {
        Medium = 0,
        Bright = 1,
        Dark = 2,
        Highlighted = 3,
    }
}
