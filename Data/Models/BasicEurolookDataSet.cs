﻿using System;
using System.Collections.Generic;
using Eurolook.Data.Models.AuthorRoles;
using Eurolook.Data.Models.Metadata;
using Eurolook.Data.Models.SharePointTermStore;

namespace Eurolook.Data.Models
{
    public class BasicEurolookDataSet
    {
        public BasicEurolookDataSet()
        {
            DocumentCategories = new List<DocumentCategory>();
            DocumentModels = new List<DocumentModel>();
            DocumentStructures = new List<DocumentStructure>();
            BrickGroups = new List<BrickGroup>();
            BrickCategories = new List<BrickCategory>();
            CommandBricks = new List<CommandBrick>();
            ContentBricks = new List<ContentBrick>();
            DynamicBricks = new List<DynamicBrick>();
            Texts = new List<Text>();
            Languages = new List<Language>();
            Translations = new List<Translation>();
            BrickTexts = new List<BrickText>();
            Resources = new List<Resource>();
            LocalisedResources = new List<LocalisedResource>();
            BrickResources = new List<BrickResource>();
            DocumentModelLanguages = new List<DocumentModelLanguage>();
            Addresses = new List<Address>();
            OrgaEntities = new List<OrgaEntity>();
            CharacterMappings = new List<CharacterMapping>();
            PersonNames = new List<PersonName>();
            StyleShortcuts = new List<StyleShortcut>();
            ColorSchemes = new List<ColorScheme>();
            TermStoreImports = new List<TermStoreImport>();
            MetadataCategories = new List<MetadataCategory>();
            MetadataDefinitions = new List<MetadataDefinition>();
            DocumentModelMetadataDefinitions = new List<DocumentModelMetadataDefinition>();
            DocumentModelAuthorRoles = new List<DocumentModelAuthorRole>();
            AuthorRoles = new List<AuthorRole>();
            PredefinedFunctions = new List<PredefinedFunction>();
            SystemTexts = new List<SystemText>();
            SystemResources = new List<SystemResource>();
            UserGroups = new List<UserGroup>();
            SystemConfigurations = new List<SystemConfiguration>();
            Notifications = new List<Notification>();
        }

        public List<SystemText> SystemTexts { get; set; }

        public List<SystemResource> SystemResources { get; set; }

        public List<PredefinedFunction> PredefinedFunctions { get; set; }

        public List<DocumentCategory> DocumentCategories { get; set; }

        public List<DocumentModel> DocumentModels { get; set; }

        public List<DocumentStructure> DocumentStructures { get; set; }

        public List<BrickGroup> BrickGroups { get; set; }

        public List<BrickCategory> BrickCategories { get; set; }

        public List<CommandBrick> CommandBricks { get; set; }

        public List<ContentBrick> ContentBricks { get; set; }

        public List<DynamicBrick> DynamicBricks { get; set; }

        public List<Text> Texts { get; set; }

        public List<Language> Languages { get; set; }

        public List<DocumentModelLanguage> DocumentModelLanguages { get; set; }

        public List<Translation> Translations { get; set; }

        public List<BrickText> BrickTexts { get; set; }

        public List<Resource> Resources { get; set; }

        public List<LocalisedResource> LocalisedResources { get; set; }

        public List<BrickResource> BrickResources { get; set; }

        public List<Address> Addresses { get; set; }

        public virtual List<OrgaEntity> OrgaEntities { get; set; }

        public List<CharacterMapping> CharacterMappings { get; set; }

        public List<PersonName> PersonNames { get; set; }

        public List<StyleShortcut> StyleShortcuts { get; set; }

        public List<ColorScheme> ColorSchemes { get; set; }

        public List<TermStoreImport> TermStoreImports { get; set; }

        public List<MetadataCategory> MetadataCategories { get; set; }

        public List<MetadataDefinition> MetadataDefinitions { get; set; }

        public List<DocumentModelMetadataDefinition> DocumentModelMetadataDefinitions { get; set; }

        public List<AuthorRole> AuthorRoles { get; set; }

        public List<DocumentModelAuthorRole> DocumentModelAuthorRoles { get; set; }

        public List<UserGroup> UserGroups { get; set; }

        public List<SystemConfiguration> SystemConfigurations { get; set; }

        public List<Notification> Notifications { get; set; }

        public void SetModificationTime(DateTime dateTime)
        {
            var dataSets = new IEnumerable<Updatable>[]
            {
                DocumentCategories,
                DocumentModels,
                DocumentStructures,
                BrickGroups,
                BrickCategories,
                UserGroups,
                CommandBricks,
                ContentBricks,
                DynamicBricks,
                Texts,
                Languages,
                Translations,
                BrickTexts,
                Resources,
                LocalisedResources,
                BrickResources,
                DocumentModelLanguages,
                Addresses,
                OrgaEntities,
                CharacterMappings,
                PersonNames,
                StyleShortcuts,
                ColorSchemes,
                TermStoreImports,
                MetadataCategories,
                MetadataDefinitions,
                DocumentModelMetadataDefinitions,
                AuthorRoles,
                DocumentModelAuthorRoles,
                PredefinedFunctions,
                SystemTexts,
                SystemResources,
                SystemConfigurations,
                Notifications,
            };

            foreach (var dataSet in dataSets)
            {
                foreach (var entity in dataSet)
                {
                    entity.ServerModificationTimeUtc = dateTime;
                }
            }
        }
    }
}
