﻿using System.Diagnostics.CodeAnalysis;

namespace Eurolook.Data.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Property accessors are used by Entity Framework")]
    public class CharacterMapping : Updatable
    {
        public string Original { get; set; }

        public string Mapped { get; set; }
    }
}
