using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Eurolook.Common;

namespace Eurolook.Data.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Property accessors are used by Entity Framework")]
    public class DocumentModelLanguage : Updatable
    {
        [ForeignKey("DocumentModel")]
        public Guid DocumentModelId { get; set; }

        [ForeignKey("DocumentLanguage")]
        public Guid DocumentLanguageId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public DocumentModel DocumentModel { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Language DocumentLanguage { get; set; }

        [CopyPropertyInclude]
        [MaxLength]
        public byte[] Template { get; set; }

        public string TemplateFileName { get; set; }
    }
}
