﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Eurolook.Common;

namespace Eurolook.Data.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Property accessors are used by Entity Framework")]
    public class PersonName : Updatable
    {
        public const string MepCategoryName = "Members of the European Parliament";
        public const string CommissionCategoryName = "Commissioners";

        private string _displayName;
        private string _insertName;

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Country { get; set; }

        public string PoliticalGroup { get; set; }

        public string Category { get; set; }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public string SearchName { get; set; }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public string DisplayName
        {
            get
            {
                if (string.IsNullOrEmpty(_displayName))
                {
                    switch (Category)
                    {
                        case MepCategoryName:
                            _displayName = LastName + ", " + FirstName + " (" + PoliticalGroup + ", " + Country + ")";
                            break;
                        case CommissionCategoryName:
                            _displayName = LastName + ", " + FirstName;
                            break;
                        default:
                            _displayName = string.IsNullOrEmpty(FirstName) ? LastName : LastName + ", " + FirstName;
                            break;
                    }
                }

                return _displayName;
            }
        }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public string InsertName
        {
            get
            {
                if (string.IsNullOrEmpty(_insertName))
                {
                    switch (Category)
                    {
                        case MepCategoryName:
                            _insertName = FirstName + " " + LastName + " (" + PoliticalGroup + ", " + Country + ")";
                            break;
                        default:
                            _insertName = FirstName + " " + LastName;
                            break;
                    }
                }

                return _insertName;
            }
        }
    }
}
