﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Eurolook.Data.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Property accessors are used by Entity Framework")]
    public class UserSettings : Synchronizable
    {
        public UserSettings()
        {
            DocumentModelSettings = new List<DocumentModelSettings>();
            DeviceSettings = new List<DeviceSettings>();
            BrickSettings = new List<BrickSettings>();
        }

        public Guid? LastAuthorId { get; set; }

        public Guid? LastDocumentModelId { get; set; }

        public Guid? LastTemplateId { get; set; }

        public Guid? LastDocumentLanguageId { get; set; }

        public Guid? LastJobAssignmentId { get; set; }

        public List<DocumentModelSettings> DocumentModelSettings { get; set; }

        public List<DeviceSettings> DeviceSettings { get; set; }

        public List<BrickSettings> BrickSettings { get; set; }

        public int DataSyncIntervalMinutes { get; set; }

        public int DataSyncDelayMinutes { get; set; }

        [ForeignKey("ColorScheme")]
        public Guid? ColorSchemeId { get; set; }

        public ColorScheme ColorScheme { get; set; }

        public bool IsCepEnabled { get; set; }

        public bool IsInsertNameInline { get; set; }

        public bool IsStyleBoxDisabled { get; set; }

        public int ContactOptions { get; set; }

        [Column("FavoriteLanguageIds")]
        public string FavoriteLanguageIdsRaw { get; set; }

        public bool IsFavoriteLanguageListEnabled { get; set; }

        public string LastMarkingsSettings { get; set; }

        public bool HideMarkingsDialog { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime? WelcomeWindowShown { get; set; }

        public string TemplateStoreViewSettings { get; set; }

        public string RecentlyUsedTemplates { get; set; }

        public int LastSelectedTab { get; set; }

        public List<Guid> GetFavoriteLanguageIds()
        {
            return !string.IsNullOrWhiteSpace(FavoriteLanguageIdsRaw)
                ? FavoriteLanguageIdsRaw.Split(',').Select(Guid.Parse).ToList()
                : new List<Guid>();
        }

        public DocumentModelSettings GetSettingsForTemplate(Guid templateId)
        {
            return DocumentModelSettings?.FirstOrDefault(x => x.TemplateId == templateId);
        }

        public DocumentModelSettings GetSettingsForDocumentModel(Guid documentModelId)
        {
            return DocumentModelSettings?.FirstOrDefault(x => x.DocumentModelId == documentModelId && x.TemplateId == null);
        }

        public void UpdateFavoriteLanguageIdsRaw(IEnumerable<Guid> languageIds)
        {
            FavoriteLanguageIdsRaw = string.Join(",", languageIds);
        }
    }
}
