using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Eurolook.Data.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Property accessors are used by Entity Framework")]
    public class BrickText : Updatable
    {
        [ForeignKey("Brick")]
        public Guid BrickId { get; set; }

        [ForeignKey("Text")]
        public Guid TextId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Brick Brick { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Text Text { get; set; }
    }
}
