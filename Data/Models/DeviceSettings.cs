using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;
using Eurolook.Common;

namespace Eurolook.Data.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "This is an EF model class.")]
    public class DeviceSettings : Synchronizable
    {
        private const char KeyValuePairSeparator = ';';
        private const char KeyValueSeparator = ':';
        private const char ActivityValueDelimiter = ';';
        private const char ActivityMonthDelimiter = '-';

        [ForeignKey("UserSettings")]
        public Guid UserSettingsId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public UserSettings UserSettings { get; set; }

        // Device Infos
        public string DeviceName { get; set; }

        public string OperatingSystem { get; set; }

        public bool IsTouchScreen { get; set; }

        public bool HasBattery { get; set; }

        public int ScreenWidth { get; set; }

        public int ScreenHeight { get; set; }

        public double ScreenDpi { get; set; }

        public long PhysicallyInstalledSystemMemoryKb { get; set; }

        public string ProcessorName { get; set; }

        public string DotNetRuntimeVersion { get; set; }

        public int? DotNetFullFrameworkReleaseVersion { get; set; }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public string DotNetFrameworkVersion
        {
            get
            {
                var dotNetVersionInfo = new DotNetVersionInfo(DotNetRuntimeVersion, DotNetFullFrameworkReleaseVersion);
                return dotNetVersionInfo.FrameworkVersion;
            }
        }

        public string AddinVersion { get; set; }

        public string OfficeVersion { get; set; }

        public string InstallRoot { get; set; }

        public bool IsPerUserInstall { get; set; }

        public bool IsPerMachineInstall { get; set; }

        public string AddInLoadTimesMilliseconds { get; set; }

        public float MaxAddInLoadTimeBucketSeconds { get; set; }

        public string Owner { get; set; }

        public bool IsInitialised { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime? LastLogin { get; set; }

        // Device settings
        public string TaskpaneExpansionState { get; set; }

        public bool? IsTaskpaneVisible { get; set; }

        public int? TaskpaneWidth { get; set; }

        public bool IsRemoteWipeRequested { get; set; }

        public string QuickStartGuideShown { get; set; }

        public string ActivityTrack { get; set; }

        public Dictionary<Guid, bool> DeserializeExpansionState()
        {
            var result = new Dictionary<Guid, bool>();
            if (TaskpaneExpansionState == null)
            {
                return result;
            }

            var segments = TaskpaneExpansionState.Split(
                new[] { KeyValuePairSeparator },
                StringSplitOptions.RemoveEmptyEntries);
            foreach (string segment in segments)
            {
                var keyValuePair = segment.Split(new[] { KeyValueSeparator }, StringSplitOptions.RemoveEmptyEntries);
                if (keyValuePair.Length != 2)
                {
                    continue;
                }

                Guid.TryParse(keyValuePair[0], out var key);
                if (key != Guid.Empty)
                {
                    if (bool.TryParse(keyValuePair[1], out bool value))
                    {
                        result.Add(key, value);
                    }
                    else
                    {
                        result.Add(key, true);
                    }
                }
            }

            return result;
        }

        public void SerializeExpansionState(Dictionary<Guid, bool> state)
        {
            if (state == null)
            {
                TaskpaneExpansionState = null;
                return;
            }

            var result = new StringBuilder();
            foreach (var key in state.Keys)
            {
                result.Append(key);
                result.Append(KeyValueSeparator);
                result.Append(state[key]);
                result.Append(KeyValuePairSeparator);
            }

            TaskpaneExpansionState = result.ToString();
        }

        public void SerializeActivityTrack(List<DateTime> months)
        {
            var result = new StringBuilder();
            foreach (var month in months)
            {
                result.Append($"{month.Year}{ActivityMonthDelimiter}{month.Month:D2}{ActivityValueDelimiter}");
            }

            ActivityTrack = result.ToString();
        }

        public List<DateTime> DeserializeActivityTrack()
        {
            var result = new List<DateTime>();
            if (string.IsNullOrEmpty(ActivityTrack))
            {
                return result;
            }

            foreach (string val in ActivityTrack.Split(ActivityValueDelimiter))
            {
                string dateValue = val.Trim();
                if (dateValue.Length != 7 || !dateValue.Contains(ActivityMonthDelimiter))
                {
                    continue;
                }

                var date = dateValue.Split(ActivityMonthDelimiter);
                if (date.Length != 2)
                {
                    continue;
                }

                if (!int.TryParse(date[0].Trim(), out int year))
                {
                    continue;
                }

                if (!int.TryParse(date[1].Trim(), out int month))
                {
                    continue;
                }

                result.Add(new DateTime(year, month, 1));
            }

            return result;
        }
    }
}
