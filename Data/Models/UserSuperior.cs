using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Eurolook.Data.Models
{
    public class UserSuperior : Updatable
    {
        [ForeignKey("User")]
        public Guid UserId { get; set; }

        [ForeignKey("Author")]
        public Guid? AuthorId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public User User { get; set; }

        public Author Author { get; set; }

        public int OrderIndex { get; set; }
    }
}
