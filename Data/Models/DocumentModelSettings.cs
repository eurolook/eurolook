using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Eurolook.Data.Models.TemplateStore;

namespace Eurolook.Data.Models
{
    public class DocumentModelSettings : Synchronizable
    {
        [ForeignKey("UserSettings")]
        public Guid UserSettingsId { get; set; }

        [ForeignKey("DocumentModel")]
        public Guid DocumentModelId { get; set; }

        [ForeignKey("Template")]
        public Guid? TemplateId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public UserSettings UserSettings { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public DocumentModel DocumentModel { get; set; }

        public Template Template { get; set; }

        public bool InsertRichBody { get; set; }

        public string UserBrickChoices { get; set; }

        public string LastUsedAuthorRoles { get; set; }

        public bool IsHidden { get; set; }
    }
}
