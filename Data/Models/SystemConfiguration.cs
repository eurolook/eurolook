﻿namespace Eurolook.Data.Models
{
    public class SystemConfiguration : Updatable
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}
