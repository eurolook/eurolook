namespace Eurolook.Data.Models
{
    public abstract class Synchronizable : Updatable
    {
        public bool ClientModification { get; set; }

        public override void Init()
        {
            base.Init();
            ClientModification = false;
        }

        public void SetModification(ModificationType modification)
        {
            switch (modification)
            {
                case ModificationType.ClientModification:
                    ClientModification = true;
                    break;
                case ModificationType.ServerModification:
                    ClientModification = false;
                    UpdateServerModificationTime();
                    break;
                case ModificationType.Synchronization:
                    ClientModification = false;
                    break;
            }
        }

        public void SetDeletedFlag(ModificationType modification, bool isDeleted = true)
        {
            Deleted = isDeleted;
            SetModification(modification);
        }
    }
}
