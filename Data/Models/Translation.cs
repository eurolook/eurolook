using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Eurolook.Data.Models
{
    public class Translation : Updatable
    {
        [ForeignKey("Text")]
        public Guid TextId { get; set; }

        [ForeignKey("Language")]
        public Guid LanguageId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Text Text { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Language Language { get; set; }

        public string Value { get; set; }

        public override string ToString()
        {
            return Value;
        }
    }
}
