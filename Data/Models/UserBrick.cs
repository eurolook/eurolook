using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Eurolook.Data.Models
{
    public class UserBrick : Synchronizable
    {
        [ForeignKey("User")]
        public Guid UserId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public User User { get; set; }

        [ForeignKey("Brick")]
        public Guid BrickId { get; set; }

        public BuildingBlockBrick Brick { get; set; }

        [ForeignKey("DocumentModel")]
        public Guid DocumentModelId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public DocumentModel DocumentModel { get; set; }
    }
}
