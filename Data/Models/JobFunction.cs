using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Eurolook.Data.Models
{
    public class JobFunction : Synchronizable, ICloneable
    {
        [ForeignKey("JobAssignment")]
        public Guid? JobAssignmentId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public JobAssignment JobAssignment { get; set; }

        [ForeignKey("Author")]
        public Guid? AuthorId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Author Author { get; set; }

        [ForeignKey("Language")]
        public Guid LanguageId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Language Language { get; set; }

        public string Function { get; set; }

        [Column("AD_Function")]
        public string AdFunction { get; set; }

        [Column("AD_Function_Pending")]
        public string AdFunctionPending { get; set; }

        public override string ToString()
        {
            return Function;
        }

        public object Clone()
        {
            return new JobFunction
            {
                Id = Id,
                ServerModificationTimeUtc = ServerModificationTimeUtc,
                ClientModification = ClientModification,
                AuthorId = AuthorId,
                Author = Author,
                LanguageId = LanguageId,
                Language = Language,
                Function = Function,
            };
        }
    }
}
