﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.Xml.Serialization;
using Eurolook.Common;

namespace Eurolook.Data.Models
{
    [Table("DocumentStructure")]
    [DebuggerDisplay("{DebuggerDisplay,nq}")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Property accessors are used by Entity Framework")]
    public class DocumentStructure : Updatable
    {
        [ForeignKey("DocumentModel")]
        public Guid DocumentModelId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public DocumentModel DocumentModel { get; set; }

        [ForeignKey("Brick")]
        public Guid BrickId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Brick Brick { get; set; }

        [Column("Position")]
        public int RawPosition { get; set; }

        [Column("VerticalPositionIndex")]
        public int VerticalPositionIndex { get; set; }

        public int SectionId { get; set; }

        public AutoBrickCreationType AutoBrickCreationType { get; set; }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public PositionType Position
        {
            get { return (PositionType)RawPosition; }
            set { RawPosition = (int)value; }
        }

        /// <summary>
        /// Gets a value indicating whether this structure contains content that is to be inserted
        /// into a first page header/footer.
        /// </summary>
        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public bool RequiresDifferentFirstPageHeaderFooter =>
            Position == PositionType.HeaderFirst || Position == PositionType.FooterFirst;

        /// <summary>
        /// Gets a value indicating whether the structure contains content that is to be inserted
        /// into a specific odd or even header/footer.
        /// </summary>
        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public bool RequiresOddAndEvenPagesHeaderFooter =>
            Position == PositionType.HeaderEven || Position == PositionType.FooterEven;

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public InsertionBehavior InsertionBehavior
        {
            get
            {
                if (Brick is { IsMultiInstance: true })
                {
                    return InsertionBehavior.AlwaysInsert;
                }

                if (Brick?.Group is { IsMultipleChoice: false })
                {
                    return InsertionBehavior.ReplaceExisting;
                }

                return InsertionBehavior.SelectExisting;
            }
        }

        // ReSharper disable once UnusedMember.Local
#pragma warning disable S1144 // Unused private types or members should be removed
        private string DebuggerDisplay
        {
            get { return $"{typeof(DocumentStructure)}: {(Brick != null ? Brick.Name : "")}"; }
        }
#pragma warning restore S1144 // Unused private types or members should be removed

        /// <summary>
        /// returns <see cref="Brick" /> as a <see cref="ContentBrick" />,
        /// throws a nice exception if the operation cannot be performed
        /// </summary>
        public ContentBrick InitializeContentBrick(XDocument combinedPropertiesXml)
        {
            if (Brick == null)
            {
                throw new InvalidOperationException("brick reference is null");
            }

            var cb = Brick as ContentBrick;
            if (cb == null)
            {
                throw new InvalidOperationException("brick reference had unexpected type " + Brick.GetType());
            }

            cb.Initialize(combinedPropertiesXml);
            return cb;
        }
    }
}
