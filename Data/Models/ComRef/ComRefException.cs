﻿using System.ComponentModel.DataAnnotations;

namespace Eurolook.Data.Models.ComRef
{
    public class ComRefException : Identifiable
    {
        [Required]
        public string AcronymPattern { get; set; }

        public bool IsIgnored { get; set; }

        public bool IgnoreInJobLink { get; set; }
    }
}
