using System.ComponentModel.DataAnnotations;

namespace Eurolook.Data.Models.ComRef
{
    public class ComRefHeaderTextPattern : Identifiable
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Format { get; set; }

        public ComRefHeaderTextPatternType Type { get; set; }
    }
}
