﻿namespace Eurolook.Data.Models.ComRef
{
    public enum ComRefHeaderTextPatternType
    {
        Directorate,
        Unit,
    }
}
