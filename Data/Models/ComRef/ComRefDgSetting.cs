using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Eurolook.Data.Models.ComRef
{
    public class ComRefDgSetting : Identifiable
    {
        [Required]
        public string DgName { get; set; }

        [Required]
        public string Institution { get; set; }

        // Multiple email addresses have to be separated with a comma
        public string Contacts { get; set; }

        public bool IsActive { get; set; }

        [ForeignKey(nameof(DirectoratePattern))]
        public Guid? DirectoratePatternId { get; set; }

        public ComRefHeaderTextPattern DirectoratePattern { get; set; }

        [ForeignKey(nameof(UnitPattern))]
        public Guid? UnitPatternId { get; set; }

        public ComRefHeaderTextPattern UnitPattern { get; set; }
    }
}
