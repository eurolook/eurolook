using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Eurolook.Data.Models
{
    public class ActivityTrack
    {
        private DateTime _date;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime DateUtc
        {
            get { return _date; }
            set { _date = value.Date; }
        }

        [ForeignKey("OrgaEntity")]
        public Guid OrgaEntityId { get; set; }

        public OrgaEntity OrgaEntity { get; set; }

        public int SessionCount { get; set; }

        public int DeviceCount { get; set; }
    }
}
