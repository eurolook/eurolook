using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.XPath;
using Eurolook.Common;

namespace Eurolook.Data.Models
{
    public class ContentBrick : Brick
    {
        private XDocument _content;

        [Column("Content")]
        [MaxLength]
        public string RawContent { get; set; }

        public string ContentBrickActionsClassName { get; set; }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public bool HasAuthorBinding
        {
            get
            {
                if (Content == null)
                {
                    return false;
                }

                var bindings =
                    Content.XPathSelectElements(
                        @"//node()[@data-type = 'binding' and starts-with(@data-prop, '/Author/')]");
                return bindings.Any();
            }
        }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public IEnumerable<string> RequiredStyles
        {
            get
            {
                if (Content == null)
                {
                    return Enumerable.Empty<string>();
                }

                var classAttibuteValues =
                    ((IEnumerable)Content.XPathEvaluate("//@class")).Cast<XAttribute>()
                                                                    .Select(a => (string)a)
                                                                    .Distinct();
                return classAttibuteValues;
            }
        }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public IEnumerable<string> RequiredLocalisedResources
        {
            get
            {
                if (Content == null)
                {
                    return Enumerable.Empty<string>();
                }

                var resourceAliases =
                    ((IEnumerable)Content.XPathEvaluate("//img/@src")).Cast<XAttribute>()
                                                                      .Select(a => (string)a)
                                                                      .Where(src => !string.IsNullOrEmpty(src));
                return resourceAliases;
            }
        }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public bool HasContent
        {
            get { return Content?.Root?.Elements().FirstOrDefault() != null; }
        }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        public virtual XDocument Content
        {
            get { return _content; }
        }

        public virtual void Initialize(XDocument combinedPropertiesXml)
        {
            if (RawContent != null)
            {
                _content = XDocument.Parse(RawContent);
            }
        }
    }
}
