using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Eurolook.Data.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Property accessors are used by Entity Framework")]
    public class Workplace : Synchronizable, ICloneable
    {
        [ForeignKey("Author")]
        public Guid AuthorId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        // should not be serialized to prevent circular references (Author has reference to Workplace)
        public Author Author { get; set; }

        [ForeignKey("Address")]
        public Guid AddressId { get; set; }

        // [IgnoreDataMember] needs to be serialized for the author search (circular reference is not possible)
        public Address Address { get; set; }

        public string PhoneExtension { get; set; }

        public string FaxExtension { get; set; }

        public string Office { get; set; }

        public object Clone()
        {
            return new Workplace
            {
                Id = Id,
                ServerModificationTimeUtc = ServerModificationTimeUtc,
                ClientModification = ClientModification,
                AuthorId = AuthorId,
                Author = Author,
                AddressId = AddressId,
                Address = Address,
                PhoneExtension = PhoneExtension,
                FaxExtension = FaxExtension,
                Office = Office,
            };
        }
    }
}
