using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Eurolook.Common;

namespace Eurolook.Data.Models
{
    public class Resource : Updatable
    {
        public string Alias { get; set; }

        public string FileName { get; set; }

        [CopyPropertyInclude]
        [MaxLength]
        public byte[] RawData { get; set; }

        public string MimeType { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public HashSet<LocalisedResource> Localisations { get; set; }

        public string GetFileName()
        {
            return $"{(string.IsNullOrEmpty(FileName) ? Alias : FileName)}{GetFileExtension(MimeType)}";
        }

        public static string GetFileExtension(string type)
        {
            switch (type)
            {
                case "image/png":
                    return ".png";
                case "image/jpeg":
                    return ".jpg";
                case "image/emf":
                case "image/x-emf":
                    return ".emf";
                case "text/xml":
                    return ".xml";
                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                    return ".xlsx";
                default:
                    return "";
            }
        }
    }
}
