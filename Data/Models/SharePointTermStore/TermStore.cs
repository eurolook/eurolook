using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.TermStore;
using JetBrains.Annotations;

namespace Eurolook.Data.Models.SharePointTermStore
{
    public class TermStore
    {
        public const int DefaultLanguageCode = 1033;

        public TermStore(HashSet<TermSet> termSets)
        {
            TermSets = termSets;
        }

        public HashSet<TermSet> TermSets { get; }

        public HashSet<Term> GetTermsFlat(Guid sharepointTermSetId, ITermFilter termFilter = null)
        {
            var termSet = TermSets.FirstOrDefault(t => t.SharePointId == sharepointTermSetId);
            return termFilter == null ? termSet?.Terms : termFilter.GetFilteredTerms(termSet).ToHashSet();
        }

        public Term GetTerm(Guid sharePointTermId)
        {
            return TermSets
                   .SelectMany(ts => ts.Terms)
                   .FirstOrDefault(t => t.SharePointId == sharePointTermId);
        }

        [CanBeNull]
        public TermLabel GetDefaultTermLabel(Guid sharePointTermId, int languageCode = DefaultLanguageCode)
        {
            return GetTerm(sharePointTermId)?.Labels
                                            .FirstOrDefault(
                                                l => l.LanguageId == languageCode && l.IsDefaultForLanguage);
        }
    }
}
