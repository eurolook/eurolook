using System;
using System.Collections.Generic;

namespace Eurolook.Data.Models.SharePointTermStore
{
    public class Term
    {
        public Guid SharePointId { get; set; }

        public string Name { get; set; }

        public DateTime CreatedDateUtc { get; set; }

        public DateTime LastModifiedDateUtc { get; set; }

        public bool IsAvailableForTagging { get; set; }

        public bool IsDeprecated { get; set; }

        public bool IsReused { get; set; }

        public Guid? ParentTermId { get; set; }

        public Guid? SourceTermId { get; set; }

        public HashSet<TermLabel> Labels { get; set; }

        public int CustomSortIndex { get; set; }

        public bool IsExcluded { get; set; }

        public string DefaultAcronym { get; set; }
    }
}
