﻿using System.ComponentModel.DataAnnotations;

namespace Eurolook.Data.Models.SharePointTermStore
{
    public class TermStoreImport : Updatable
    {
        [MaxLength]
        public byte[] TermStoreXmlFile { get; set; }
    }
}
