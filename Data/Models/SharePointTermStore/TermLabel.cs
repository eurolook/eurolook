﻿namespace Eurolook.Data.Models.SharePointTermStore
{
    public class TermLabel
    {
        public bool IsDefaultForLanguage { get; set; }

        public int LanguageId { get; set; }

        public string Value { get; set; }
    }
}
