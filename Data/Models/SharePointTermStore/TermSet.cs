﻿using System;
using System.Collections.Generic;

namespace Eurolook.Data.Models.SharePointTermStore
{
    public class TermSet
    {
        public Guid SharePointId { get; set; }

        public string Name { get; set; }

        public DateTime CreatedDateUtc { get; set; }

        public DateTime LastModifiedDateUtc { get; set; }

        public string Description { get; set; }

        public bool IsAvailableForTagging { get; set; }

        public bool IsOpenForTermCreation { get; set; }

        public HashSet<Term> Terms { get; set; }
    }
}
