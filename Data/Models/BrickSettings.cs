using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Eurolook.Data.Models
{
    public class BrickSettings : Synchronizable
    {
        [ForeignKey("UserSettings")]
        public Guid UserSettingsId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public UserSettings UserSettings { get; set; }

        [ForeignKey("Brick")]
        public Guid? BrickId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Brick Brick { get; set; }

        public string SettingsJson { get; set; }
    }
}
