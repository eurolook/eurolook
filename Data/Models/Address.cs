using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Eurolook.Common;

namespace Eurolook.Data.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Property accessors are used by Entity Framework")]
    public class Address : Updatable
    {
        [MaxLength(100)]
        public string Name { get; set; }

        public string PhoneNumberPrefix { get; set; }

        [ForeignKey("LocationText")]
        public Guid? LocationTextId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Text LocationText { get; set; }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        [TranslationFor("LocationText")]
        public Translation Location { get; set; }

        [ForeignKey("FooterText")]
        public Guid? FooterTextId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Text FooterText { get; set; }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        [TranslationFor("FooterText")]
        public Translation Footer { get; set; }

        public bool? IsDefault { get; set; }

        public string ActiveDirectoryReference { get; set; }

        [ForeignKey("NameText")]
        public Guid? NameTextId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public Text NameText { get; set; }

        [NotMapped]
        [IgnoreDataMember]
        [XmlIgnore]
        [CopyPropertyIgnore]
        [TranslationFor("NameText")]
        public Translation TranslatedName { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
