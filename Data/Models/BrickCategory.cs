﻿using System.Diagnostics.CodeAnalysis;

namespace Eurolook.Data.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Property accessors are used by Entity Framework")]
    public class BrickCategory : Updatable
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int UiPositionIndex { get; set; }
    }
}
