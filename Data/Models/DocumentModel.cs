﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Eurolook.Common;
using Eurolook.Data.Models.AuthorRoles;
using Eurolook.Data.Models.Metadata;

namespace Eurolook.Data.Models
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Property accessors are used by Entity Framework")]
    public class DocumentModel : Updatable
    {
        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string Description { get; set; }

        public string Keywords { get; set; }

        [ForeignKey("Owner")]
        public Guid? OwnerId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public OrgaEntity Owner { get; set; }

        [CopyPropertyInclude]
        [MaxLength]
        public byte[] PreviewImage { get; set; }

        public string PreviewImageFileName { get; set; }

        [CopyPropertyInclude]
        [MaxLength]
        public byte[] PreviewImageWithSampleText { get; set; }

        public string PreviewImageWithSampleTextFileName { get; set; }

        [CopyPropertyInclude]
        [MaxLength]
        public byte[] Template { get; set; }

        public string TemplateFileName { get; set; }

        public string EL4DocType { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public HashSet<DocumentStructure> BrickReferences { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public HashSet<DocumentModelLanguage> DocumentLanguages { get; set; }

        [ForeignKey("DocumentCategory")]
        public Guid? DocumentCategoryId { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public DocumentCategory DocumentCategory { get; set; }

        public int UiPositionIndex { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public HashSet<DocumentModelMetadataDefinition> DocumentModelMetadataDefinitions { get; set; }

        public bool? IsClassicDocument { get; set; }

        public bool? IsHidden { get; set; }

        public string MinVersion { get; set; }

        public string MaxVersion { get; set; }

        public string CreateDocumentWizardPages { get; set; }

        public bool? HideBrickChooser { get; set; }

        [IgnoreDataMember]
        [XmlIgnore]
        public HashSet<DocumentModelAuthorRole> AuthorRoles { get; set; }

        public override string ToString()
        {
            return DisplayName;
        }
    }
}
