namespace Eurolook.Data.Models
{
    public class DeviceInfo
    {
        public string DeviceName { get; set; }

        public string OsName { get; set; }

        public bool IsTouchScreen { get; set; }

        public bool HasBattery { get; set; }

        public int ScreenWidth { get; set; }

        public int ScreenHeight { get; set; }

        public double ScreenDpi { get; set; }

        public long PhysicallyInstalledSystemMemoryKb { get; set; }

        public string ProcessorName { get; set; }

        public string DeviceOwner { get; set; }

        public string DotNetRuntimeVersion { get; set; }

        public int? DotNetFullFrameworkReleaseVersion { get; set; }

        public bool IsPerUserInstall { get; set; }

        public bool IsPerMachineInstall { get; set; }
    }
}
