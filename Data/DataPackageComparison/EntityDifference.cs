﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Eurolook.Data.Models;

namespace Eurolook.Data.DataPackageComparison
{
    public class EntityDifference
    {
        public EntityDifference(Updatable entity, bool isNew = false)
        {
            Entity = entity;
            var type = entity.GetType();
            Name = (type.GetProperty("Name") ?? type.GetProperty("Alias") ?? type.GetProperty("Key"))?.GetValue(entity)?.ToString();
            TypeName = type.Name;
            Id = entity.Id;
            IsNew = isNew;
            IsDeleted = entity.Deleted;
        }

        [XmlIgnore]
        [IgnoreDataMember]
        public Updatable Entity { get; set; }

        public string Name { get; set; }

        public string TypeName { get; set; }

        public Guid Id { get; set; }

        public bool IsNew { get; set; }

        public bool IsDeleted { get; set; }

        public List<PropertyDifference> PropertyDifferences { get; set; } = new List<PropertyDifference>();
    }
}
