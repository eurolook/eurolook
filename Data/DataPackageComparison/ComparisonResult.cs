using System.Collections.Generic;

namespace Eurolook.Data.DataPackageComparison
{
    public class ComparisonResult
    {
        public List<EntityDifference> Changed { get; } = new List<EntityDifference>();

        public List<EntityDifference> Added { get; } = new List<EntityDifference>();
    }
}
