namespace Eurolook.Data.DataPackageComparison
{
    public class PropertyDifference
    {
        public string PropertyName { get; set; }

        public string ValueA { get; set; }

        public string ValueB { get; set; }
    }
}
