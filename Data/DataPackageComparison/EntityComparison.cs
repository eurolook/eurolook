﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eurolook.Data.Models;

namespace Eurolook.Data.DataPackageComparison
{
    public class EntityComparison<T>
        where T : Updatable
    {
        private readonly IEnumerable<T> _newEntities;
        private readonly List<T> _baseEntities;

        public EntityComparison(IEnumerable<T> newEntities, List<T> baseEntities)
        {
            _newEntities = newEntities;
            _baseEntities = baseEntities;
        }

        public IEnumerable<EntityDifference> CompareEntities()
        {
            foreach (var otherEntity in _newEntities)
            {
                if (otherEntity != null)
                {
                    T baseEntity = null;
                    if (_baseEntities.Any())
                    {
                        baseEntity = _baseEntities.FirstOrDefault(x => x.Id == otherEntity.Id);
                    }

                    if (baseEntity != null)
                    {
                        // its an updated entity
                        if (!DataPackage.EntityEquals(baseEntity, otherEntity))
                        {
                            yield return CalculateDifference(baseEntity, otherEntity);
                        }
                    }
                    else
                    {
                        // it is an additional
                        yield return new EntityDifference(otherEntity, true);
                    }
                }
            }
        }

        private EntityDifference CalculateDifference(Updatable a, Updatable b)
        {
            var type = a.GetType();
            if (type != b.GetType())
            {
                throw new ArgumentException("Arguments have to be of same type");
            }

            var result = new EntityDifference(b);

            foreach (var property in type.GetProperties())
            {
                if (DataPackage.SkipProperty(property))
                {
                    continue;
                }

                var aVal = property.GetValue(a);
                var bVal = property.GetValue(b);
                if (aVal != null)
                {
                    if (aVal.GetType() == typeof(byte[]))
                    {
                        var aBytes = (byte[])aVal;
                        var bBytes = (byte[])bVal;
                        if (bBytes == null || !aBytes.SequenceEqual(bBytes))
                        {
                            result.PropertyDifferences.Add(
                                new PropertyDifference
                                {
                                    PropertyName = property.Name,
                                    ValueA = "Binary Value",
                                    ValueB = "Different Binary Value",
                                });
                        }
                    }
                    else if (!aVal.Equals(bVal))
                    {
                        result.PropertyDifferences.Add(
                            new PropertyDifference
                            {
                                PropertyName = property.Name,
                                ValueA = aVal.ToString(),
                                ValueB = bVal?.ToString() ?? "<null>",
                            });
                    }
                }
                else if (bVal != null)
                {
                    result.PropertyDifferences.Add(
                        new PropertyDifference
                        {
                            PropertyName = property.Name,
                            ValueA = "<null>",
                            ValueB = bVal.ToString(),
                        });
                }
            }

            return result;
        }
    }
}
