﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eurolook.Common;
using Eurolook.Common.Extensions;
using Eurolook.Data.Models;

namespace Eurolook.Data.DataPackageComparison
{
    public class DataPackageComparer
    {
        private readonly List<Func<IEnumerable<EntityDifference>>> _compareFuncs =
            new List<Func<IEnumerable<EntityDifference>>>();

        private readonly Dictionary<Guid, Tuple<string, string>> _guidNameDictionary =
            new Dictionary<Guid, Tuple<string, string>>();

        public ComparisonResult Compare(DataPackage basePackage, DataPackage newPackage)
        {
            PrepareComparison(newPackage.Authors, basePackage.Authors);
            PrepareComparison(newPackage.Addresses, basePackage.Addresses);
            PrepareComparison(newPackage.BrickCategories, basePackage.BrickCategories);
            PrepareComparison(newPackage.UserGroups, basePackage.UserGroups);
            PrepareComparison(newPackage.BrickGroups, basePackage.BrickGroups);
            PrepareComparison(newPackage.BrickResources, basePackage.BrickResources);
            PrepareComparison(newPackage.BrickTexts, basePackage.BrickTexts);
            PrepareComparison(newPackage.CharacterMappings, basePackage.CharacterMappings);
            PrepareComparison(newPackage.CommandBricks, basePackage.CommandBricks);
            PrepareComparison(newPackage.ContentBricks, basePackage.ContentBricks);
            PrepareComparison(newPackage.ColorSchemes, basePackage.ColorSchemes);
            PrepareComparison(newPackage.DocumentCategories, basePackage.DocumentCategories);
            PrepareComparison(newPackage.DocumentModels, basePackage.DocumentModels);
            PrepareComparison(newPackage.DocumentModelLanguages, basePackage.DocumentModelLanguages);
            PrepareComparison(newPackage.DocumentStructures, basePackage.DocumentStructures);
            PrepareComparison(newPackage.DynamicBricks, basePackage.DynamicBricks);
            PrepareComparison(newPackage.JobAssignments, basePackage.JobAssignments);
            PrepareComparison(newPackage.Languages, basePackage.Languages);
            PrepareComparison(newPackage.LocalisedResources, basePackage.LocalisedResources);
            PrepareComparison(newPackage.PersonNames, basePackage.PersonNames);
            PrepareComparison(newPackage.Resources, basePackage.Resources);
            PrepareComparison(newPackage.StyleShortcuts, basePackage.StyleShortcuts);
            PrepareComparison(newPackage.Texts, basePackage.Texts);
            PrepareComparison(newPackage.Translations, basePackage.Translations);
            PrepareComparison(newPackage.OrgaEntities, basePackage.OrgaEntities);
            PrepareComparison(newPackage.MetadataCategories, basePackage.MetadataCategories);
            PrepareComparison(newPackage.MetadataDefinitions, basePackage.MetadataDefinitions);
            PrepareComparison(newPackage.DocumentModelMetadataDefinitions, basePackage.DocumentModelMetadataDefinitions);
            PrepareComparison(newPackage.DocumentModelAuthorRoles, basePackage.DocumentModelAuthorRoles);
            PrepareComparison(newPackage.AuthorRoles, basePackage.AuthorRoles);
            PrepareComparison(newPackage.TermStoreImports, basePackage.TermStoreImports);
            PrepareComparison(newPackage.PredefinedFunctions, basePackage.PredefinedFunctions);
            PrepareComparison(newPackage.SystemConfigurations, basePackage.SystemConfigurations);
            PrepareComparison(newPackage.Notifications, basePackage.Notifications);

            return Compare();
        }

        private void PrepareComparison<T>(List<T> newEntities, List<T> baseEntities)
            where T : Updatable
        {
            var comparison = new EntityComparison<T>(newEntities, baseEntities);
            _compareFuncs.Add(comparison.CompareEntities);

            foreach (var entity in baseEntities.Concat(newEntities))
            {
                FillGuidNameDictionary(entity);
            }
        }

        private ComparisonResult Compare()
        {
            var result = new ComparisonResult();

            var entityDifferences = _compareFuncs.SelectMany(f => f());

            foreach (var entityDifference in entityDifferences)
            {
                if (entityDifference.Name.IsNullOrEmpty())
                {
                    CalculateNameBasedOnGuidProperties(entityDifference);
                }

                var xmlContentProperty =
                    entityDifference.PropertyDifferences.FirstOrDefault(pd => pd.PropertyName == "RawContent");
                if (xmlContentProperty != null)
                {
                    xmlContentProperty.ValueA = XmlFormatter.PrettyPrint(xmlContentProperty.ValueA);
                    xmlContentProperty.ValueB = XmlFormatter.PrettyPrint(xmlContentProperty.ValueB);
                }

                if (entityDifference.IsNew)
                {
                    result.Added.Add(entityDifference);
                }
                else
                {
                    result.Changed.Add(entityDifference);
                }
            }

            return result;
        }

        private void FillGuidNameDictionary(Identifiable entity)
        {
            var id = entity.Id;
            if (_guidNameDictionary.ContainsKey(id))
            {
                return;
            }

            var type = entity.GetType();
            string name = (string)(type.GetProperty("Name") ?? type.GetProperty("Alias") ?? type.GetProperty("Key"))?.GetValue(entity) ?? "";
            if (!name.IsNullOrEmpty())
            {
                _guidNameDictionary.Add(entity.Id, Tuple.Create(type.Name, name));
            }
        }

        private void CalculateNameBasedOnGuidProperties(EntityDifference entityDifference)
        {
            var entity = entityDifference.Entity;
            var nameBuilder = new StringBuilder();

            var guidProperties = entity.GetType().GetProperties().Where(pi => pi.PropertyType == typeof(Guid));

            foreach (var guidProperty in guidProperties)
            {
                var guid = (Guid)guidProperty.GetValue(entity);
                if (guid == entity.Id || !_guidNameDictionary.ContainsKey(guid))
                {
                    continue;
                }

                string prefix = nameBuilder.Length == 0 ? "Referencing " : " and ";

                nameBuilder.Append($"{prefix}{_guidNameDictionary[guid].Item1}: '{_guidNameDictionary[guid].Item2}'");
            }

            entityDifference.Name = nameBuilder.ToString();
        }
    }
}
