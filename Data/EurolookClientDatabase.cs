﻿namespace Eurolook.Data
{
    public class EurolookClientDatabase : EurolookDatabase<EurolookClientContext>
    {
        public bool EnsureCreated()
        {
            using (var context = GetContext())
            {
                return context.EnsureCreated();
            }
        }

        public bool EnsureDeleted()
        {
            using (var context = GetContext())
            {
                return context.Database.EnsureDeleted();
            }
        }

        protected override EurolookClientContext GetContext()
        {
            return new EurolookClientContext(ignoreDeletedFlags: true);
        }
    }
}
