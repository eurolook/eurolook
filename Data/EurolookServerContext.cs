﻿using System.Linq;
using System.Reflection;
using Eurolook.Data.Models;
using Eurolook.Data.PublicAuthorEditor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Eurolook.Data
{
    public class EurolookServerContext : EurolookContext
    {
        private static readonly ILoggerFactory DebugLoggerFactory = LoggerFactory.Create(
            builder =>
            {
                builder.AddDebug();
            });

        private readonly string _nameOrConnectionString;
        private readonly bool _ignoreDeletedFlags;

        public EurolookServerContext()
            : this("FAKE_CONNECTION_STRING_FOR_ADDING_MIGRATIONS")
        {
        }

        public EurolookServerContext(string nameOrConnectionString, bool ignoreDeletedFlags = false)
        {
            _nameOrConnectionString = nameOrConnectionString;
            _ignoreDeletedFlags = ignoreDeletedFlags;
        }

        // ReSharper disable once IdentifierTypo
        public DbSet<UserFeedback> UserFeedbacks { get; set; }

        public DbSet<AuthorEditRequest> AuthorEditRequests { get; set; }

        public override bool IsServerContext => true;

        public void Migrate()
        {
            Database.Migrate();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseSqlServer(_nameOrConnectionString, x => x.MigrationsAssembly("Eurolook.Data"));

#if DEBUG
            optionsBuilder
                .UseLoggerFactory(DebugLoggerFactory)
                .EnableSensitiveDataLogging();
#endif
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            if (_ignoreDeletedFlags)
            {
                UpdatableModelConfiguration.ConfigureIgnoreDeletedFlags(modelBuilder);
            }
        }
    }
}
