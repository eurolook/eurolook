using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Eurolook.Common
{
    public class AssemblyBuildInfo
    {
        public AssemblyBuildInfo(Assembly assembly)
        {
            if (assembly?.Location == null)
            {
                return;
            }

            var fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
            Version = fileVersionInfo.FileVersion;

            var fileInfo = new FileInfo(assembly.Location);
            BuildTime = fileInfo.LastWriteTime;

            var informationalVersionAttribute = assembly.GetCustomAttributes()
                                                        .OfType<AssemblyInformationalVersionAttribute>()
                                                        .FirstOrDefault();
            if (informationalVersionAttribute != null)
            {
                var match = Regex.Match(
                    informationalVersionAttribute.InformationalVersion,
                    @"Sha\.(?<hash>[0-9a-fA-F]{7})");
                CommitHash = match.Groups["hash"]?.Value ?? "";
            }
        }

        public string Version { get; }

        public DateTime BuildTime { get; }

        public string CommitHash { get; }
    }
}
