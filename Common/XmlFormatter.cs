using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace Eurolook.Common
{
    public static class XmlFormatter
    {
        public static readonly XmlWriterSettings PrettyPrintSettings = new XmlWriterSettings
        {
            Indent = true,
            NewLineChars = Environment.NewLine,
        };

        public static string PrettyPrint(string xml)
        {
            return PrintXml(xml, PrettyPrintSettings);
        }

        public static string Linearize(string xml)
        {
            var settings = new XmlWriterSettings
            {
                Indent = false,
                NewLineChars = "",
            };

            return PrintXml(xml, settings);
        }

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times", Justification = "Reviewed")]
        private static string PrintXml(string xml, XmlWriterSettings settings)
        {
            if (string.IsNullOrWhiteSpace(xml))
            {
                return "";
            }

            using (var sw = new StringWriter())
            {
                using (var xw = XmlWriter.Create(sw, settings))
                {
                    XDocument.Parse(xml).WriteTo(xw);
                }

                return sw.ToString();
            }
        }
    }
}
