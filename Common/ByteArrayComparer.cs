﻿using System.Collections.Generic;
using System.Linq;

namespace Eurolook.Common
{
    public class ByteArrayComparer : IEqualityComparer<byte[]>
    {
        public bool Equals(byte[] x, byte[] y)
        {
            if (x == null || y == null)
            {
                return x == y;
            }

            return x.SequenceEqual(y);
        }

        public int GetHashCode(byte[] obj)
        {
            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            if (obj == null)
            {
                return 0;
            }

            // ReSharper restore HeuristicUnreachableCode
            return obj.Sum(b => b);
        }
    }
}
