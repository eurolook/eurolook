﻿using System;
using System.Threading;

namespace Eurolook.Common
{
    public static class Retry
    {
        public static void OnException<TException>(Action action, int numRetries = 3, int retryTimeout = 100)
            where TException : Exception
        {
            OnException(action, ex => ex is TException, numRetries, retryTimeout);
        }

        public static void OnException<TReturn, TException>(
            Func<TReturn> func,
            int numRetries = 3,
            int retryTimeout = 100)
            where TException : Exception
        {
            OnException(func, ex => ex is TException, numRetries, retryTimeout);
        }

        public static void OnException(
            Action action,
            Predicate<Exception> exceptionPredicate,
            int numRetries = 3,
            int retryTimeout = 100)
        {
            if (action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }

            if (exceptionPredicate == null)
            {
                throw new ArgumentNullException(nameof(exceptionPredicate));
            }

            OnException<object>(
                () =>
                {
                    action();
                    return null;
                },
                exceptionPredicate,
                numRetries,
                retryTimeout);
        }

        public static TReturn OnException<TReturn>(
            Func<TReturn> func,
            Predicate<Exception> exceptionPredicate,
            int numRetries = 3,
            int retryTimeout = 100)
        {
            if (func == null)
            {
                throw new ArgumentNullException(nameof(func));
            }

            if (exceptionPredicate == null)
            {
                throw new ArgumentNullException(nameof(exceptionPredicate));
            }

            while (true)
            {
                try
                {
                    return func();
                }
                catch (Exception ex)
                {
                    if (--numRetries <= 0 || !exceptionPredicate(ex))
                    {
                        throw;
                    }

                    if (retryTimeout > 0)
                    {
                        Thread.Sleep(retryTimeout);
                    }
                }
            }
        }
    }
}
