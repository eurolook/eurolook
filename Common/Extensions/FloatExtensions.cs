﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Eurolook.Common.Extensions
{
    /// <summary>
    /// A class containing extension methods for the <see cref="float" /> type.
    /// </summary>
    public static class FloatExtensions
    {
        /// <summary>
        /// Compares two float values for equality and accounts for float precision issues.
        /// </summary>
        /// <param name="x">The first value to compare.</param>
        /// <param name="y">The second value to compare.</param>
        /// <returns>A boolean value indicating whether the input arguments can be considered equal.</returns>
        public static bool AlmostEquals(this float x, float y)
        {
            float epsilon = Math.Max(Math.Abs(x), Math.Abs(y)) * 1E-7F;
            return AlmostEquals(x, y, epsilon);
        }

        public static bool AlmostEquals(this float? x, float? y)
        {
            if (x == null && y == null)
            {
                return true;
            }

            if (x == null || y == null)
            {
                return false;
            }

            return AlmostEquals(x.Value, y.Value);
        }

        /// <summary>
        /// Compares two float values for equality and accounts for float precision issues.
        /// </summary>
        /// <param name="x">The first value to compare.</param>
        /// <param name="y">The second value to compare.</param>
        /// <param name="epsilon">A value specifying how close the two operands have to be to be considered equal.</param>
        /// <returns>A boolean value indicating whether the input arguments can be considered equal.</returns>
        [SuppressMessage("ReSharper", "CompareOfFloatsByEqualityOperator", Justification = "Reviewed")]
        [SuppressMessage("Sonar", "S1244", Justification = "Reviewed")]
        public static bool AlmostEquals(this float x, float y, float epsilon)
        {
            if (x == y)
            {
                // shortcut
                return true;
            }

            return Math.Abs(x - y) < epsilon;
        }
    }
}
