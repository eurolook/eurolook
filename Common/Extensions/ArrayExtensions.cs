namespace Eurolook.Common.Extensions
{
    public static class ArrayExtensions
    {
        public static void Swap<T>(this T[] source, int index1, int index2)
        {
            var temp = source[index1];
            source[index1] = source[index2];
            source[index2] = temp;
        }

        public static T GetCircular<T>(this T[] source, int index)
        {
            index %= source.Length;
            return source[index];
        }
    }
}
