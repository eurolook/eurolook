﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Autofac;
using Autofac.Core;

namespace Eurolook.Common.Extensions
{
    public static class ContainerBuilderExtensions
    {
        private const string EurolookPlugInAssembliesKey = "EurolookPlugInAssemblies";

        /// <summary>
        /// Makes the plugin assemblies known to the <see cref="ContainerBuilder"/> so that they can be accessed
        /// from within modules.
        /// </summary>
        /// <param name="builder">The <see cref="ContainerBuilder"/> used by Autofac.</param>
        /// <param name="assemblies">The plugin assemblies retrieved from the <see cref="PluginLoader"/>.</param>
        public static void SetEurolookPluginAssemblies(
            this ContainerBuilder builder,
            IReadOnlyCollection<Assembly> assemblies)
        {
            builder.Properties[EurolookPlugInAssembliesKey] = assemblies;
        }

        /// <summary>
        /// Retrieves the plugin assemblies available inside Autofac <see cref="IModule"/>.
        /// </summary>
        /// <param name="builder">The <see cref="ContainerBuilder"/> used by Autofac.</param>
        public static IEnumerable<Assembly> GetEurolookPluginAssemblies(this ContainerBuilder builder)
        {
            return builder.Properties.ContainsKey(EurolookPlugInAssembliesKey)
                   && builder.Properties[EurolookPlugInAssembliesKey] is IEnumerable<Assembly> assemblies
                ? assemblies
                : Array.Empty<Assembly>();
        }
    }
}
