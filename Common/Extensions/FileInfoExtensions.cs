using System.Globalization;
using System.IO;

namespace Eurolook.Common.Extensions
{
    public static class FileInfoExtensions
    {
        public static bool HasZipHeader(this FileInfo fileInfo)
        {
            using (var br = new BinaryReader(File.Open(fileInfo.FullName, FileMode.Open, FileAccess.Read)))
            {
                return br.ReadUInt16() == 0x4B50; // PK header
            }
        }

        public static bool HasCompoundDocumentHeader(this FileInfo fileInfo)
        {
            using (var br = new BinaryReader(File.Open(fileInfo.FullName, FileMode.Open, FileAccess.Read)))
            {
                return br.ReadUInt16() == 0xCFD0; // ÐÏ header
            }
        }

        public static string HumanReadableFileSize(this FileInfo fileInfo)
        {
            long length = fileInfo.Length;
            string unit;
            double result;

            if (length >= 0x10000000000)
            {
                unit = "TB";
                result = length >> 30;
            }
            else if (length >= 0x40000000)
            {
                unit = "GB";
                result = length >> 20;
            }
            else if (length >= 0x100000)
            {
                unit = "MB";
                result = length >> 10;
            }
            else if (length >= 0x400)
            {
                unit = "KB";
                result = length;
            }
            else
            {
                return length.ToString("0") + "B";
            }

            result /= 1024;
            return result.ToString("0.##", CultureInfo.InvariantCulture) + unit;
        }
    }
}
