﻿using Microsoft.Extensions.Configuration;

namespace Eurolook.Common.Extensions
{
    public static class ConfigurationBuilderExtensions
    {
        public static IConfigurationBuilder AddDefaultConfigurationProviders(this IConfigurationBuilder builder, string env)
        {
            builder
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env}.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env}.secrets.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env}.local.json", optional: true, reloadOnChange: true);

            return builder;
        }
    }
}
