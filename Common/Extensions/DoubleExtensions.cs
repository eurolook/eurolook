﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Eurolook.Common.Extensions
{
    /// <summary>
    /// A class containing extension methods for the <see cref="double" /> type.
    /// </summary>
    public static class DoubleExtensions
    {
        /// <summary>
        /// Compares two double values for equality and accounts for double precision issues.
        /// </summary>
        /// <param name="x">The first value to compare.</param>
        /// <param name="y">The second value to compare.</param>
        /// <returns>A boolean value indicating whether the input arguments can be considered equal.</returns>
        public static bool AlmostEquals(this double x, double y)
        {
            double epsilon = Math.Max(Math.Abs(x), Math.Abs(y)) * 1E-15;
            return AlmostEquals(x, y, epsilon);
        }

        /// <summary>
        /// Compares two double values for equality and accounts for double precision issues.
        /// </summary>
        /// <param name="x">The first value to compare.</param>
        /// <param name="y">The second value to compare.</param>
        /// <param name="epsilon">A value specifying how close the two operands have to be to be considered equal.</param>
        /// <returns>A boolean value indicating whether the input arguments can be considered equal.</returns>
        [SuppressMessage("ReSharper", "CompareOfFloatsByEqualityOperator", Justification = "Reviewed")]
        [SuppressMessage("Sonar", "S1244", Justification = "Reviewed")]
        public static bool AlmostEquals(this double x, double y, double epsilon)
        {
            if (x == y)
            {
                // shortcut
                return true;
            }

            return Math.Abs(x - y) < epsilon;
        }
    }
}
