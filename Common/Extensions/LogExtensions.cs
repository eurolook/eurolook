using System.IO;
using System.Reflection;
using System.Security.Principal;
using Eurolook.Common.Log;

namespace Eurolook.Common.Extensions
{
    public static class LogExtensions
    {
        public static void LogStartupMessage(this ILog logger, Assembly assembly, string userName = null)
        {
            var assemblyName = assembly.GetName();

            userName ??= WindowsIdentity.GetCurrent().Name;

            var dotNetVersionInfo = new DotNetVersionInfo();
            var windowsVersionInfo = new WindowsVersionInfo();
            var fileInfo = new FileInfo(assembly.Location);

            logger.Info($"##### Starting up {assemblyName.FullName}");
            logger.Info($"- Build: ${fileInfo.LastWriteTime}");
            logger.Info(
                "- Configuration: "
                + $"{AssemblyAttributeReader.GetFirstAssemblyAttributeString<AssemblyConfigurationAttribute>(assembly)}");
            logger.Info(
                "- Informational Version: "
                + $"{AssemblyAttributeReader.GetFirstAssemblyAttributeString<AssemblyInformationalVersionAttribute>(assembly)}");
            logger.Info($"- .NET Runtime Version: {dotNetVersionInfo.RuntimeVersion}");
            logger.Info($"- .NET Framework Version: {dotNetVersionInfo.FrameworkVersion}");
            logger.Info($"- OS Version: {windowsVersionInfo}");

            if (!string.IsNullOrEmpty(userName))
            {
                logger.Info($"- User: {userName}");
            }
        }
    }
}
