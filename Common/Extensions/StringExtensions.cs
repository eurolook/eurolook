﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Eurolook.Common.Extensions
{
    public static class StringExtensions
    {
        private static readonly Regex ConsecutiveControlCharsRegex = new Regex(@"\p{C}+");

        private static readonly Regex WhitespaceRegex = new Regex(@"\s");

        public static string TruncateLongString(this string str, int maxLength)
        {
            return str.Substring(0, Math.Min(str.Length, maxLength));
        }

        public static bool Contains(this string str, string otherStr, StringComparison comparison)
        {
            if (str == null || otherStr == null)
            {
                return false;
            }

            return str.IndexOf(otherStr, comparison) >= 0;
        }

        public static bool ContainsInvariantIgnoreCase(this string str, string otherStr)
        {
            return Contains(str, otherStr, StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool EqualsIgnoreCase(this string firstString, string secondString)
        {
            return string.Equals(firstString, secondString, StringComparison.InvariantCultureIgnoreCase);
        }

        public static string RemoveControlCharacters(this string value)
        {
            return ReplaceAllConsecutiveControlCharacters(value, string.Empty);
        }

        public static string ReplaceAllConsecutiveControlCharacters(this string value, string replacement)
        {
            return value != null ? ConsecutiveControlCharsRegex.Replace(value, replacement) : null;
        }

        public static string ReplaceAllWhitespace(this string value, string replacement)
        {
            return value != null ? WhitespaceRegex.Replace(value, replacement) : null;
        }

        public static string GetSafeFilename(this string filename)
        {
            return string.Join("_", filename.Split(Path.GetInvalidFileNameChars()));
        }

        public static string[] Split(this string value, string splitString, StringSplitOptions options)
        {
            return value.Split(new[] { splitString }, options);
        }

        public static bool Like(this string toSearch, string toFind)
        {
            // TODO: this should probably be replaced by Regex.Escape?
            var normalizedToFind = new Regex(@"\.|\$|\^|\{|\[|\(|\||\)|\*|\+|\?|\\")
                                   .Replace(toFind, ch => @"\" + ch)
                                   .Replace('_', '.')
                                   .Replace("%", ".*");

            return new Regex(@"\A" + normalizedToFind + @"\z", RegexOptions.Singleline | RegexOptions.IgnoreCase)
                .IsMatch(toSearch);
        }

        /// <summary>
        /// Levenshtein Distance algorithm with transposition. <br />
        /// </summary>
        /// <param name="input">The first string to be compared.</param>
        /// <param name="comparedTo">The second string to be compared.</param>
        /// <param name="caseSensitive">A flag to indicate whether the comparison shall be case-sensitive.</param>
        /// <returns>The Levenshtein distance between two string values.</returns>
        public static int LevenshteinDistance(this string input, string comparedTo, bool caseSensitive = false)
        {
            if (string.IsNullOrWhiteSpace(input) || string.IsNullOrWhiteSpace(comparedTo))
            {
                return -1;
            }

            if (!caseSensitive)
            {
                input = input.ToUpperInvariant();
                comparedTo = comparedTo.ToUpperInvariant();
            }

            int inputLen = input.Length;
            int comparedToLen = comparedTo.Length;

            int[,] matrix = new int[inputLen, comparedToLen];

            // initialize
            for (int i = 0; i < inputLen; i++)
            {
                matrix[i, 0] = i;
            }

            for (int i = 0; i < comparedToLen; i++)
            {
                matrix[0, i] = i;
            }

            // analyze
            for (int i = 1; i < inputLen; i++)
            {
                char si = input[i - 1];
                for (int j = 1; j < comparedToLen; j++)
                {
                    char tj = comparedTo[j - 1];
                    int cost = si == tj ? 0 : 1;

                    int above = matrix[i - 1, j];
                    int left = matrix[i, j - 1];
                    int diagonal = matrix[i - 1, j - 1];

                    int MinOfThree(int a, int b, int c)
                    {
                        return Math.Min(a, Math.Min(b, c));
                    }

                    int cell = MinOfThree(above + 1, left + 1, diagonal + cost);

                    // transposition
                    if (i > 1 && j > 1)
                    {
                        int trans = matrix[i - 2, j - 2] + 1;
                        if (input[i - 2] != comparedTo[j - 1])
                        {
                            trans++;
                        }

                        if (input[i - 1] != comparedTo[j - 2])
                        {
                            trans++;
                        }

                        if (cell > trans)
                        {
                            cell = trans;
                        }
                    }

                    matrix[i, j] = cell;
                }
            }

            return matrix[inputLen - 1, comparedToLen - 1];
        }

        public static string RemoveDiacritics(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return text;
            }

            text = text.Normalize(NormalizationForm.FormD);
            var chars = text.Where(c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                            .ToArray();
            return new string(chars).Normalize(NormalizationForm.FormC);
        }

        public static bool ToBool(this string value)
        {
            if (bool.TryParse(value, out bool result))
            {
                return result;
            }

            return false;
        }

        public static string EnsureEndsWith(this string value, string appendix)
        {
            return value.EndsWith(appendix) ? value : value + appendix;
        }
    }
}
