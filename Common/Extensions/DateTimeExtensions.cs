using System;
using System.Globalization;

namespace Eurolook.Common.Extensions
{
    public static class DateTimeExtensions
    {
        public enum DateGroupType
        {
            Day,
            Week,
            Month,
        }

        public static DateTime ToDateType(this DateTime date, DateGroupType dateType)
        {
            var result = new DateTime(date.Ticks / 10000 * 10000); // remove the fractals
            result = result.AddHours(-1 * result.Hour);
            result = result.AddMinutes(-1 * result.Minute);
            result = result.AddSeconds(-1 * result.Second);
            result = result.AddMilliseconds(-1 * result.Millisecond);
            if (dateType == DateGroupType.Week)
            {
                result = result.StartOfWeek(DayOfWeek.Monday);
            }
            else if (dateType == DateGroupType.Month)
            {
                result = result.AddDays(-1 * result.Day + 1);
            }

            return result;
        }

        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = (7 + (dt.DayOfWeek - startOfWeek)) % 7;
            return dt.AddDays(-1 * diff).Date;
        }

        public static string ToUIString(this DateTime dt)
        {
            return dt.ToLocalTime().ToString("g", CultureInfo.CurrentCulture);
        }
    }
}
