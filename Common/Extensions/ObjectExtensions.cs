using System.Collections.Generic;

namespace Eurolook.Common.Extensions
{
    public static class ObjectExtensions
    {
        public static List<T> CreateList<T>(this T item)
        {
            return new List<T> { item };
        }
    }
}
