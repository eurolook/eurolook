﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Eurolook.Common.Extensions
{
    public static class EnumerableExtensions
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> source)
        {
            return source == null || !source.Any();
        }

        public static void ForEach<T>(this IEnumerable<T> list, Action<T> action)
        {
            if (list == null || action == null)
            {
                return;
            }

            foreach (T obj in list)
            {
                action(obj);
            }
        }

        public static IEnumerable<T> Descendants<T>(this IEnumerable<T> source, Func<T, IEnumerable<T>> descendBy)
        {
            if (source == null)
            {
                yield break;
            }

            foreach (var value in source)
            {
                yield return value;

                if (descendBy(value).IsNullOrEmpty())
                {
                    continue;
                }

                foreach (var child in descendBy(value).Descendants(descendBy))
                {
                    yield return child;
                }
            }
        }

        public static async Task<IEnumerable<T>> Where<T>(this IEnumerable<T> source, Func<T, Task<bool>> predicate)
        {
            var results = await Task.WhenAll(source.Select(async x => (x, await predicate(x))));
            return results.Where(x => x.Item2).Select(x => x.Item1);
        }

        public static async Task<int> Count<T>(this IEnumerable<T> source, Func<T, Task<bool>> predicate)
        {
            var results = await Task.WhenAll(source.Select(async x => await predicate(x)));
            return results.Count(b => b);
        }

        public static bool IsEqualForAll<T1, T2>(this IEnumerable<T1> source, Func<T1, T2> selector)
        {
            var first = source.FirstOrDefault();
            return source.All(x => selector(x).Equals(selector(first)));
        }

        public static async Task ParallelForEachAsync<T>(
            this IEnumerable<T> source,
            Func<T, Task> asyncAction,
            int maxDegreeOfParallelism)
        {
            var throttler = new SemaphoreSlim(maxDegreeOfParallelism);
            var tasks = source.Select(
                async item =>
                {
                    await throttler.WaitAsync();
                    try
                    {
                        await asyncAction(item).ConfigureAwait(false);
                    }
                    finally
                    {
                        throttler.Release();
                    }
                });
            await Task.WhenAll(tasks);
        }
    }
}
