using System;
using System.IO;
using System.Text;

namespace Eurolook.Common.Extensions
{
    public static class ByteArrayExtensions
    {
        private const string Base62CodingSpace = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        public static string ToHexString(this byte[] buffer)
        {
            if (buffer == null)
            {
                return null;
            }

            var formatted = new StringBuilder(2 * buffer.Length);
            foreach (byte b in buffer)
            {
                formatted.AppendFormat("{0:x2}", b);
            }

            return formatted.ToString();
        }

        /// <summary>
        /// Convert a byte array into a Base62 representation.
        /// </summary>
        /// <param name="original">The byte array.</param>
        /// <returns>A Base62 string representation.</returns>
        public static string ToBase62(this byte[] original)
        {
            var sb = new StringBuilder();
            var stream = new BitStream(original); // Set up the BitStream
            var read = new byte[1]; // Only read 6-bit at a time
            while (true)
            {
                read[0] = 0;

                // Try to read 6 bits
                int length = stream.Read(read, 0, 6);

                // Not reaching the end
                if (length == 6)
                {
                    if (read[0] >> 3 == 0x1f)
                    {
                        // First 5-bit is 11111
                        sb.Append(Base62CodingSpace[61]);
                        stream.Seek(-1, SeekOrigin.Current); // Leave the 6th bit to next group
                    }
                    else if (read[0] >> 3 == 0x1e)
                    {
                        // First 5-bit is 11110
                        sb.Append(Base62CodingSpace[60]);
                        stream.Seek(-1, SeekOrigin.Current);
                    }
                    else
                    {
                        // Encode 6-bit
                        sb.Append(Base62CodingSpace[read[0] >> 2]);
                    }
                }
                else
                {
                    // Padding 0s to make the last bits to 6 bit
                    sb.Append(Base62CodingSpace[read[0] >> (8 - length)]);
                    break;
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Convert a Base62 string to a byte array.
        /// </summary>
        /// <param name="base62">The Base62 string to be converted.</param>
        /// <returns>The byte array.</returns>
        public static byte[] FromBase62(this string base62)
        {
            // Character count
            int count = 0;

            // Set up the BitStream
            var stream = new BitStream(base62.Length * 6 / 8);

            foreach (char c in base62)
            {
                // Look up coding table
                int index = Base62CodingSpace.IndexOf(c);

                // If end is reached
                if (count == base62.Length - 1)
                {
                    // Check if the ending is good
                    int mod = (int)(stream.Position % 8);
                    stream.Write(new[] { (byte)(index << mod) }, 0, 8 - mod);
                }
                else
                {
                    // If 60 or 61 then only write 5 bits to the stream, otherwise 6 bits.
                    if (index == 60)
                    {
                        stream.Write(new byte[] { 0xf0 }, 0, 5);
                    }
                    else if (index == 61)
                    {
                        stream.Write(new byte[] { 0xf8 }, 0, 5);
                    }
                    else
                    {
                        stream.Write(new[] { (byte)index }, 2, 6);
                    }
                }

                count++;
            }

            // Dump out the bytes
            var result = new byte[stream.Position / 8];
            stream.Seek(0, SeekOrigin.Begin);
            int bytesRead = stream.Read(result, 0, result.Length * 8);
            if (bytesRead != result.Length * 8)
            {
                throw new InvalidOperationException("Incorrect number of bytes read.");
            }

            return result;
        }

        /// <summary>
        /// Utility that read and write bits in byte array.
        /// </summary>
        private class BitStream : Stream
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="BitStream" /> class with an initial capacity.
            /// </summary>
            /// <param name="capacity">Capacity of the stream.</param>
            public BitStream(int capacity)
            {
                Source = new byte[capacity];
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="BitStream" /> class with a source byte array.
            /// </summary>
            /// <param name="source">A byte array to initialize the stream from.</param>
            public BitStream(byte[] source)
            {
                Source = source;
            }

            public override bool CanRead
            {
                get { return true; }
            }

            public override bool CanSeek
            {
                get { return true; }
            }

            public override bool CanWrite
            {
                get { return true; }
            }

            /// <summary>
            /// Gets the bit length of the stream.
            /// </summary>
            public override long Length
            {
                get { return Source.Length * 8; }
            }

            /// <summary>
            /// Gets or sets the bit position of the stream.
            /// </summary>
            public override long Position { get; set; }

            private byte[] Source { get; }

            public override void Flush()
            {
                throw new NotImplementedException();
            }

            /// <summary>
            /// Read the stream to the buffer.
            /// </summary>
            /// <param name="buffer">The buffer.</param>
            /// <param name="offset">The offset bit start position of the stream.</param>
            /// <param name="count">The number of bits to read.</param>
            /// <returns>The number of bits read.</returns>
            public override int Read(byte[] buffer, int offset, int count)
            {
                // Temporary position cursor
                long tempPos = Position;
                tempPos += offset;

                // Buffer byte position and in-byte position
                int readPosCount = 0;
                int readPosMod = 0;

                // Stream byte position and in-byte position
                long posCount = tempPos >> 3;
                int posMod = (int)(tempPos - ((tempPos >> 3) << 3));

                while (tempPos < Position + offset + count && tempPos < Length)
                {
                    // Copy the bit from the stream to buffer
                    if ((Source[posCount] & (0x1 << (7 - posMod))) != 0)
                    {
                        buffer[readPosCount] = (byte)(buffer[readPosCount] | (0x1 << (7 - readPosMod)));
                    }
                    else
                    {
                        buffer[readPosCount] = (byte)(buffer[readPosCount] & (0xffffffff - (0x1 << (7 - readPosMod))));
                    }

                    // Increment position cursors
                    tempPos++;
                    if (posMod == 7)
                    {
                        posMod = 0;
                        posCount++;
                    }
                    else
                    {
                        posMod++;
                    }

                    if (readPosMod == 7)
                    {
                        readPosMod = 0;
                        readPosCount++;
                    }
                    else
                    {
                        readPosMod++;
                    }
                }

                int bits = (int)(tempPos - Position - offset);
                Position = tempPos;
                return bits;
            }

            /// <summary>
            /// Set up the stream position.
            /// </summary>
            /// <param name="offset">The offset to seek.</param>
            /// <param name="origin">The position origin.</param>
            /// <returns>The position after setup.</returns>
            public override long Seek(long offset, SeekOrigin origin)
            {
                switch (origin)
                {
                    case SeekOrigin.Begin:
                        Position = offset;
                        break;

                    case SeekOrigin.Current:
                        Position += offset;
                        break;

                    case SeekOrigin.End:
                        Position = Length + offset;
                        break;
                }

                return Position;
            }

            public override void SetLength(long value)
            {
                throw new NotImplementedException();
            }

            /// <summary>
            /// Write from buffer to the stream.
            /// </summary>
            /// <param name="buffer">The source buffer from which is written to the stream.</param>
            /// <param name="offset">The offset start bit position of buffer.</param>
            /// <param name="count">The number of bits.</param>
            public override void Write(byte[] buffer, int offset, int count)
            {
                // Temporary position cursor
                long tempPos = Position;

                // Buffer byte position and in-byte position
                int readPosCount = offset >> 3;
                int readPosMod = offset - ((offset >> 3) << 3);

                // Stream byte position and in-byte position
                long posCount = tempPos >> 3;
                int posMod = (int)(tempPos - ((tempPos >> 3) << 3));

                while (tempPos < Position + count && tempPos < Length)
                {
                    // Copy the bit from buffer to the stream
                    if ((buffer[readPosCount] & (0x1 << (7 - readPosMod))) != 0)
                    {
                        Source[posCount] = (byte)(Source[posCount] | (0x1 << (7 - posMod)));
                    }
                    else
                    {
                        Source[posCount] = (byte)(Source[posCount] & (0xffffffff - (0x1 << (7 - posMod))));
                    }

                    // Increment position cursors
                    tempPos++;
                    if (posMod == 7)
                    {
                        posMod = 0;
                        posCount++;
                    }
                    else
                    {
                        posMod++;
                    }

                    if (readPosMod == 7)
                    {
                        readPosMod = 0;
                        readPosCount++;
                    }
                    else
                    {
                        readPosMod++;
                    }
                }

                Position = tempPos;
            }
        }
    }
}
