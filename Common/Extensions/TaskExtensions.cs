using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;

namespace Eurolook.Common.Extensions
{
    public static class TaskExtensions
    {
#pragma warning disable RECS0165 // Asynchronous methods should return a Task instead of void
        [SuppressMessage("Sonar", "S3168:async methods should not return void", Justification = "Method acts an event handler")]
        public static async void FireAndForgetSafeAsync(this Task task, Action<Exception> errorHandler)
        {
            try
            {
                await task;
            }
            catch (Exception ex)
            {
                errorHandler?.Invoke(ex);
            }
        }
#pragma warning restore RECS0165 // Asynchronous methods should return a Task instead of void
    }
}
