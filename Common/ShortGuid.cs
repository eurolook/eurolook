using System;
using Eurolook.Common.Extensions;

namespace Eurolook.Common
{
    /// <summary>
    /// Represents a globally unique identifier (GUID) with a shorter Base62 string representation.
    /// </summary>
    public struct ShortGuid
    {
        /// <summary>
        /// A read-only instance of the ShortGuid class whose value
        /// is guaranteed to be all zeroes.
        /// </summary>
        public static readonly ShortGuid Empty = new ShortGuid(Guid.Empty);

        private readonly Guid _guid;
        private readonly string _value;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShortGuid" /> struct from a base62-encoded string.
        /// </summary>
        /// <param name="value">
        /// The encoded guid as a base62 string.
        /// </param>
        public ShortGuid(string value)
        {
            if (!TryDecode(value, out var guid))
            {
                throw new ArgumentException("value is not a valid base62-encoded guid");
            }

            _guid = guid;
            _value = value;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShortGuid" /> struct from a Guid value.
        /// </summary>
        /// <param name="guid">The Guid to encode.</param>
        public ShortGuid(Guid guid)
        {
            _value = Encode(guid);
            _guid = guid;
        }

        private ShortGuid(string value, Guid guid)
        {
            _value = value;
            _guid = guid;
        }

        /// <summary>
        /// Implicitly converts the ShortGuid to it's string equivalent.
        /// </summary>
        /// <param name="shortGuid">The <see cref="ShortGuid" /> to be implicitly converted to string.</param>
        /// <returns>A string representation of the <see cref="ShortGuid" />.</returns>
        public static implicit operator string(ShortGuid shortGuid)
        {
            return shortGuid._value;
        }

        /// <summary>
        /// Implicitly converts the ShortGuid to it's Guid equivalent.
        /// </summary>
        /// <param name="shortGuid">The <see cref="ShortGuid" /> to be implicitly converted to a <see cref="Guid" />.</param>
        /// <returns>A <see cref="Guid" /> representation of the <see cref="ShortGuid" />.</returns>
        public static implicit operator Guid(ShortGuid shortGuid)
        {
            return shortGuid._guid;
        }

        /// <summary>
        /// Implicitly converts the string to a ShortGuid.
        /// </summary>
        /// <param name="shortGuid">A string to be converted to a <see cref="ShortGuid" />.</param>
        /// <returns>A <see cref="ShortGuid" /> matching the string value passed in.</returns>
        public static implicit operator ShortGuid(string shortGuid)
        {
            return new ShortGuid(shortGuid);
        }

        /// <summary>
        /// Implicitly converts the Guid to a ShortGuid.
        /// </summary>
        /// <param name="guid">A <see cref="Guid" /> to be converted to a <see cref="ShortGuid" />.</param>
        /// <returns>A <see cref="ShortGuid" /> matching the <see cref="Guid" /> value passed in.</returns>
        public static implicit operator ShortGuid(Guid guid)
        {
            return new ShortGuid(guid);
        }

        /// <summary>
        /// Determines if both ShortGuid values have the same underlying
        /// Guid value.
        /// </summary>
        /// <param name="x">The first operand of the equality operator.</param>
        /// <param name="y">The second operand of the equality operator.</param>
        /// <returns>A boolean flag indicating whether to <see cref="ShortGuid" /> values are equal.</returns>
        public static bool operator ==(ShortGuid x, ShortGuid y)
        {
            return x._guid == y._guid;
        }

        /// <summary>
        /// Determines if both ShortGuid values do not have the
        /// same underlying Guid value.
        /// </summary>
        /// <param name="x">The first operand of the unequal operator.</param>
        /// <param name="y">The second operand of the unequal operator.</param>
        /// <returns>A boolean flag indicating whether to <see cref="ShortGuid" /> values are unequal.</returns>
        public static bool operator !=(ShortGuid x, ShortGuid y)
        {
            return !(x == y);
        }

        /// <summary>
        /// Tries to parse a string value into a ShortGuid value.
        /// </summary>
        /// <param name="value">A string containing the string representation in the format used by ShortGuid.</param>
        /// <param name="shortGuid">If the input value could be parsed, this argument contains the parsed value.</param>
        /// <returns>True if the string was parsed successfully, false otherwise.</returns>
        public static bool TryParse(string value, out ShortGuid shortGuid)
        {
            shortGuid = Empty;
            if (!TryDecode(value, out var guid))
            {
                return false;
            }

            shortGuid = new ShortGuid(value, guid);
            return true;
        }

        /// <summary>
        /// Returns the base-62 encoded guid as a string.
        /// </summary>
        /// <returns>A base-62 encoded string representation of the Guid value.</returns>
        public override string ToString()
        {
            return _value;
        }

        /// <summary>
        /// Returns a value indicating whether this instance and a
        /// specified Object represent the same type and value.
        /// </summary>
        /// <param name="obj">The object to compare.</param>
        /// <returns>A flag indicating whether this instance and the passed in object represent the same type and value.</returns>
        public override bool Equals(object obj)
        {
            if (obj is ShortGuid shortGuid)
            {
                return _guid.Equals(shortGuid._guid);
            }

            if (obj is Guid guid)
            {
                return _guid.Equals(guid);
            }

            if (obj is string value)
            {
                return _guid.Equals(new ShortGuid(value)._guid);
            }

            return false;
        }

        /// <summary>
        /// Returns the HashCode for underlying Guid.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            return _guid.GetHashCode();
        }

        /// <summary>
        /// Encodes the given Guid as a base62 string.
        /// </summary>
        /// <param name="guid">The Guid to encode.</param>
        /// <returns>A base-62 encoded string representation of the Guid value.</returns>
        private static string Encode(Guid guid)
        {
            return guid.ToByteArray().ToBase62();
        }

        /// <summary>
        /// Decodes the given base62 string.
        /// </summary>
        /// <param name="value">The base62 encoded string of a Guid.</param>
        /// <param name="guid">The decoded Guid value.</param>
        /// <returns>True if the value could successfully be parsed into a Guid.</returns>
        private static bool TryDecode(string value, out Guid guid)
        {
            var buffer = value.FromBase62();
            if (buffer.Length != 16)
            {
                guid = Guid.Empty;
                return false;
            }

            guid = new Guid(buffer);
            return true;
        }
    }
}
