﻿namespace Eurolook.Common.SecretsProvider
{
    public interface ISecretsProvider
    {
        string RetrieveDataSyncServicePassword(string input);
    }
}
