﻿using Autofac.Core;

namespace Eurolook.Common
{
    public interface IPluginModule : IModule
    {
    }
}
