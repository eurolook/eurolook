﻿using System;
using System.Collections.Generic;

namespace Eurolook.Common
{
    public class TrimmedStringComparer : IEqualityComparer<string>
    {
        public static readonly TrimmedStringComparer InvariantCultureIgnoreCase =
            new(StringComparison.InvariantCultureIgnoreCase);

        private readonly StringComparison _stringComparison;

        private TrimmedStringComparer(StringComparison stringComparison)
        {
            _stringComparison = stringComparison;
        }

        public bool Equals(string x, string y)
        {
            bool isXNullOrWhiteSpace = string.IsNullOrWhiteSpace(x);
            bool isYNullOrWhiteSpace = string.IsNullOrWhiteSpace(y);

            if (isXNullOrWhiteSpace && isYNullOrWhiteSpace)
            {
                return true;
            }

            if (isXNullOrWhiteSpace || isYNullOrWhiteSpace)
            {
                return false;
            }

            string trimX = x.Trim();
            string trimY = y.Trim();
            return trimX.Equals(trimY, _stringComparison);
        }

        public int GetHashCode(string obj)
        {
            return obj.GetHashCode();
        }
    }
}
