using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;

namespace Eurolook.Common
{
    /// <summary>
    /// Implements a wrapper around the native JobObject API.
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Names refer to native types.")]
    public sealed class ManagedJobObject : IDisposable
    {
        private bool _isDisposed;
        private IntPtr _jobObjectHandle;

        public ManagedJobObject()
        {
            _jobObjectHandle = CreateJobObject(IntPtr.Zero, null);

            var info = new JOBOBJECT_BASIC_LIMIT_INFORMATION
            {
                LimitFlags = JobObjectLimit.KillOnJobClose,
            };

            var extendedInfo = new JOBOBJECT_EXTENDED_LIMIT_INFORMATION
            {
                BasicLimitInformation = info,
            };

            int length = Marshal.SizeOf(typeof(JOBOBJECT_EXTENDED_LIMIT_INFORMATION));
            var extendedInfoPtr = Marshal.AllocHGlobal(length);
            Marshal.StructureToPtr(extendedInfo, extendedInfoPtr, false);

            if (!SetInformationJobObject(
                    _jobObjectHandle,
                    JobObjectInfoType.ExtendedLimitInformation,
                    extendedInfoPtr,
                    (uint)length))
            {
                throw new Win32Exception();
            }
        }

        ~ManagedJobObject()
        {
            Dispose(false);
        }

        /// <summary>
        /// An enumeration specifying the type of information when setting job limits by calling the
        /// <c>SetInformationJobObject</c>
        /// function.
        /// </summary>
        [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "This type is defined by an external API.")]
        private enum JobObjectInfoType
        {
            /// <summary>
            /// The lpJobObjectInfo parameter is a pointer to a JOBOBJECT_BASIC_LIMIT_INFORMATION structure.
            /// </summary>
            BasicLimitInformation = 2,

            /// <summary>
            /// The lpJobObjectInfo parameter is a pointer to a JOBOBJECT_BASIC_UI_RESTRICTIONS structure.
            /// </summary>
            BasicUIRestrictions = 4,

            /// <summary>
            /// This flag is not supported. Applications must set security limitations individually for each process.
            /// </summary>
            SecurityLimitInformation = 5,

            /// <summary>
            /// The lpJobObjectInfo parameter is a pointer to a JOBOBJECT_END_OF_JOB_TIME_INFORMATION structure.
            /// </summary>
            EndOfJobTimeInformation = 6,

            /// <summary>
            /// The lpJobObjectInfo parameter is a pointer to a JOBOBJECT_ASSOCIATE_COMPLETION_PORT structure.
            /// </summary>
            AssociateCompletionPortInformation = 7,

            /// <summary>
            /// The lpJobObjectInfo parameter is a pointer to a JOBOBJECT_EXTENDED_LIMIT_INFORMATION structure.
            /// </summary>
            ExtendedLimitInformation = 9,

            /// <summary>
            /// The lpJobObjectInfo parameter is a pointer to a USHORT value that specifies the list of processor groups to
            /// assign
            /// the job to. The cbJobObjectInfoLength parameter is set to the size of the group data. Divide this value by
            /// sizeof(USHORT) to determine the number of groups.
            /// </summary>
            /// <remarks>Supported on Windows 7 / Windows Server 2012 and above only</remarks>
            GroupInformation = 11,

            /// <summary>
            /// The lpJobObjectInfo parameter is a pointer to a JOBOBJECT_NOTIFICATION_LIMIT_INFORMATION structure.
            /// </summary>
            /// <remarks>Supported on Windows 8 / Windows Server 2012 and above only</remarks>
            NotificationLimitInformation = 12,

            /// <summary>
            /// The <c>lpJobObjectInfo</c> parameter is a pointer to a buffer that contains an array of <c>GROUP_AFFINITY</c>
            /// structures that specify the affinity of the job for the processor groups to which the job is currently
            /// assigned.
            /// The
            /// cbJobObjectInfoLength parameter is set to the size of the group affinity data. Divide this value by
            /// sizeof(GROUP_AFFINITY) to determine the number of groups.
            /// </summary>
            /// <remarks>Supported on Windows 8 / Windows Server 2012 and above only</remarks>
            GroupInformationEx = 14,

            /// <summary>
            /// The lpJobObjectInfo parameter is a pointer to a JOBOBJECT_CPU_RATE_CONTROL_INFORMATION structure.
            /// </summary>
            /// <remarks>Supported on Windows 8 / Windows Server 2012 and above only</remarks>
            CpuRateControlInformation = 15,
        }

        /// <summary>
        /// An enumeration to specify the limits that are in effect on a JobObject.
        /// </summary>
        [Flags]
        [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "This type is defined by an external API.")]
        private enum JobObjectLimit : uint
        {
            /// <summary>
            /// Causes all processes associated with the job to use the same minimum and maximum working set sizes. The
            /// MinimumWorkingSetSize and MaximumWorkingSetSize members contain additional information.
            /// <para />
            /// If the job is nested, the effective working set size is the smallest working set size in the job chain.
            /// </summary>
            WorkingSet = 0x00000001,

            /// <summary>
            /// Establishes a user-mode execution time limit for each currently active process and for all future processes
            /// associated with the job. The PerProcessUserTimeLimit member contains additional information.
            /// </summary>
            ProcessTime = 0x00000002,

            /// <summary>
            /// Establishes a user-mode execution time limit for the job. The PerJobUserTimeLimit member contains additional
            /// information. This flag cannot be used with JOB_OBJECT_LIMIT_PRESERVE_JOB_TIME.
            /// </summary>
            JobTime = 0x00000004,

            /// <summary>
            /// Establishes a maximum number of simultaneously active processes associated with the job. The
            /// <c>ActiveProcessLimit</c> member contains additional information.
            /// </summary>
            ActiveProcess = 0x00000008,

            /// <summary>
            /// Causes all processes associated with the job to use the same processor affinity. The <c>Affinity</c> member
            /// contains additional information.
            /// <para />
            /// If the job is nested, the specified processor affinity must be a subset of the effective affinity of the parent
            /// job. If the specified affinity a superset of the affinity of the parent job, it is ignored and the affinity of
            /// the
            /// parent job is used.
            /// </summary>
            Affinity = 0x00000010,

            /// <summary>
            /// Causes all processes associated with the job to use the same priority class. For more information, see
            /// Scheduling
            /// Priorities. The PriorityClass member contains additional information.
            /// <para />
            /// If the job is nested, the effective priority class is the lowest priority class in the job chain.
            /// </summary>
            PriorityClass = 0x00000020,

            /// <summary>
            /// Preserves any job time limits you previously set. As long as this flag is set, you can establish a per-job time
            /// limit once, then alter other limits in subsequent calls. This flag cannot be used with
            /// JOB_OBJECT_LIMIT_JOB_TIME.
            /// </summary>
            PreserveJobTime = 0x00000040,

            /// <summary>
            /// Causes all processes in the job to use the same scheduling class. The SchedulingClass member contains
            /// additional
            /// information.
            /// <para />
            /// If the job is nested, the effective scheduling class is the lowest scheduling class in the job chain.
            /// </summary>
            SchedulingClass = 0x00000080,

            /// <summary>
            /// Causes all processes associated with the job to limit their committed memory. When a process attempts to commit
            /// memory that would exceed the per-process limit, it fails. If the job object is associated with a completion
            /// port, a
            /// JOB_OBJECT_MSG_PROCESS_MEMORY_LIMIT message is sent to the completion port.
            /// <para />
            /// If the job is nested, the effective memory limit is the most restrictive memory limit in the job chain.
            /// <para />
            /// This limit requires use of a JOBOBJECT_EXTENDED_LIMIT_INFORMATION structure. Its BasicLimitInformation member
            /// is a
            /// JOBOBJECT_BASIC_LIMIT_INFORMATION structure.
            /// </summary>
            ProcessMemory = 0x00000100,

            /// <summary>
            /// Causes all processes associated with the job to limit the job-wide sum of their committed memory. When a
            /// process
            /// attempts to commit memory that would exceed the job-wide limit, it fails. If the job object is associated with
            /// a
            /// completion port, a JOB_OBJECT_MSG_JOB_MEMORY_LIMIT message is sent to the completion port.
            /// <para />
            /// This limit requires use of a JOBOBJECT_EXTENDED_LIMIT_INFORMATION structure. Its BasicLimitInformation member
            /// is a
            /// JOBOBJECT_BASIC_LIMIT_INFORMATION structure.
            /// <para />
            /// To register for notification when this limit is exceeded while allowing processes to continue to commit memory,
            /// use
            /// the SetInformationJobObject function with the JobObjectNotificationLimitInformation information class.
            /// </summary>
            JobMemory = 0x00000200,

            /// <summary>
            /// Forces a call to the SetErrorMode function with the <c>SEM_NOGPFAULTERRORBOX</c> flag for each process
            /// associated
            /// with the job.
            /// <para />
            /// If an exception occurs and the system calls the <c>UnhandledExceptionFilter</c> function, the debugger will be
            /// given a
            /// chance to act. If there is no debugger, the functions returns <c>EXCEPTION_EXECUTE_HANDLER</c>. Normally, this
            /// will
            /// cause
            /// termination of the process with the exception code as the exit status.
            /// <para />
            /// This limit requires use of a <c>JOBOBJECT_EXTENDED_LIMIT_INFORMATION</c> structure. Its
            /// <c>BasicLimitInformation</c> member is a
            /// <c>JOBOBJECT_BASIC_LIMIT_INFORMATION</c> structure.
            /// </summary>
            DieOnUnhandledException = 0x00000400,

            /// <summary>
            /// If any process associated with the job creates a child process using the <c>CREATE_BREAKAWAY_FROM_JOB</c> flag
            /// while this
            /// limit is in effect, the child process is not associated with the job.
            /// <para />
            /// This limit requires use of a <c>JOBOBJECT_EXTENDED_LIMIT_INFORMATION</c> structure. Its
            /// <c>BasicLimitInformation</c> member is a <c>JOBOBJECT_BASIC_LIMIT_INFORMATION</c> structure.
            /// </summary>
            BreakawayOk = 0x00000800,

            /// <summary>
            /// Allows any process associated with the job to create child processes that are not associated with the job.
            /// <para />
            /// If the job is nested and its immediate job object allows breakaway, the child process breaks away from the
            /// immediate job object and from each job in the parent job chain, moving up the hierarchy until it reaches a job
            /// that
            /// does not permit breakaway. If the immediate job object does not allow breakaway, the child process does not
            /// break
            /// away even if jobs in its parent job chain allow it.
            /// <para />
            /// This limit requires use of a JOBOBJECT_EXTENDED_LIMIT_INFORMATION structure. Its BasicLimitInformation member
            /// is a
            /// JOBOBJECT_BASIC_LIMIT_INFORMATION structure.
            /// </summary>
            SilentBreakawayOk = 0x00001000,

            /// <summary>
            /// Causes all processes associated with the job to terminate when the last handle to the job is closed.
            /// <para />
            /// This limit requires use of a JOBOBJECT_EXTENDED_LIMIT_INFORMATION structure. Its BasicLimitInformation member
            /// is a
            /// JOBOBJECT_BASIC_LIMIT_INFORMATION structure.
            /// </summary>
            KillOnJobClose = 0x00002000,

            /// <summary>
            /// Allows processes to use a subset of the processor affinity for all processes associated with the job. This
            /// value
            /// must be combined with JOB_OBJECT_LIMIT_AFFINITY.
            /// </summary>
            SubsetAffinity = 0x00004000,
        }

        /// <summary>
        /// Adds a process to the current job object.
        /// </summary>
        /// <param name="processHandle">A handle to the process to be added.</param>
        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global", Justification = "Reviewed")]
        public void AddProcess(IntPtr processHandle)
        {
            bool result = AssignProcessToJobObject(_jobObjectHandle, processHandle);
            if (!result)
            {
                throw new Exception($"Unable to set information.  Error: {Marshal.GetLastWin32Error()}");
            }
        }

        /// <summary>
        /// Adds a process to the current job object.
        /// </summary>
        /// <param name="processId">The id of the process to be added.</param>
        public void AddProcess(int processId)
        {
            AddProcess(Process.GetProcessById(processId).Handle);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Creates or opens a job object.
        /// </summary>
        /// <param name="lpJobAttributes">
        /// A pointer to a <c>SECURITY_ATTRIBUTES</c> structure that specifies the security descriptor for the job object
        /// and
        /// determines whether child processes can inherit the returned handle. If <c>lpJobAttributes</c> is <c>NULL</c>,
        /// the
        /// job object gets
        /// a default security descriptor and the handle cannot be inherited. The ACLs in the default security descriptor
        /// for a
        /// job object come from the primary or impersonation token of the creator.
        /// </param>
        /// <param name="lpName">
        /// The name of the job. The name is limited to MAX_PATH characters. Name comparison is case-sensitive.
        /// If lpName is NULL, the job is created without a name.
        /// If lpName matches the name of an existing event, semaphore, mutex, waitable timer, or file-mapping object, the
        /// function fails and the GetLastError function returns ERROR_INVALID_HANDLE. This occurs because these objects
        /// share
        /// the same namespace.
        /// The object can be created in a private namespace. For more information, see Object Namespaces.
        /// Terminal Services:  The name can have a "Global\" or "Local\" prefix to explicitly create the object in the
        /// global
        /// or session namespace. The remainder of the name can contain any character except the backslash character (\).
        /// For
        /// more information, see Kernel Object Namespaces.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is a handle to the job object. The handle has the
        /// JOB_OBJECT_ALL_ACCESS
        /// access right. If the object existed before the function call, the function returns a handle to the existing job
        /// object and GetLastError returns ERROR_ALREADY_EXISTS.
        /// If the function fails, the return value is NULL. To get extended error information, call GetLastError.
        /// </returns>
        [SuppressMessage("Microsoft.Design", "CA1060:MovePInvokesToNativeMethodsClass", Justification = "Keep everything in this class")]
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
        private static extern IntPtr CreateJobObject(IntPtr lpJobAttributes, string lpName);

        /// <summary>
        /// Sets limits for a job object.
        /// </summary>
        /// <param name="hJob">
        /// A handle to the job whose limits are being set. The <c>CreateJobObject</c> or <c>OpenJobObject</c>
        /// function returns this handle. The handle must have the <c>JOB_OBJECT_SET_ATTRIBUTES</c> access right.
        /// </param>
        /// <param name="infoType">
        /// The information class for the limits to be set. This can be any of the value defined by
        /// <c>JobObjectInfoType</c>.
        /// </param>
        /// <param name="lpJobObjectInfo">
        /// The limits or job state to be set for the job. The format of this data depends on the
        /// value of teh parameter <c>infoType</c>.
        /// </param>
        /// <param name="cbJobObjectInfoLength">The size of the job information being set, in bytes.</param>
        /// <returns>
        /// If the function succeeds, the return value is true.
        /// If the function fails, the return value is false. To get extended error information, call <c>GetLastError</c>.
        /// </returns>
        [SuppressMessage("Microsoft.Design", "CA1060:MovePInvokesToNativeMethodsClass", Justification = "Keep everything in this class")]
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool SetInformationJobObject(
            IntPtr hJob,
            JobObjectInfoType infoType,
            IntPtr lpJobObjectInfo,
            uint cbJobObjectInfoLength);

        /// <summary>
        /// Assigns a process to an existing job object.
        /// </summary>
        /// <param name="hJob">
        /// A handle to the job object to which the process will be associated. The <c>CreateJobObject</c> or
        /// <c>OpenJobObject</c> function returns this handle. The handle must have the <c>JOB_OBJECT_ASSIGN_PROCESS</c>
        /// access
        /// right.
        /// </param>
        /// <param name="hProcess">
        /// A handle to the process to associate with the job object. The handle must have the <c>PROCESS_SET_QUOTA</c> and
        /// <c>PROCESS_TERMINATE</c> access rights. For more information, see
        /// <a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ms684880(v=vs.85).aspx">
        /// Process Security and Access Rights
        /// </a>
        /// .
        /// If the process is already associated with a job, the job specified by hJob must be empty or it must be in the
        /// hierarchy of nested jobs to which the process already belongs, and it cannot have UI limits set
        /// (<c>SetInformationJobObject</c> with <c>JobObjectBasicUIRestrictions</c>).
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is true.
        /// If the function fails, the return value is false. To get extended error information, call <c>GetLastError</c>.
        /// </returns>
        [SuppressMessage("Microsoft.Design", "CA1060:MovePInvokesToNativeMethodsClass", Justification = "Keep everything in this class")]
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool AssignProcessToJobObject(IntPtr hJob, IntPtr hProcess);

        /// <summary>
        /// Closes an open object handle.
        /// </summary>
        /// <param name="hObject">A valid handle to an open object.</param>
        /// <returns>
        /// If the function succeeds, the return value is true.
        /// If the function fails, the return value is false. To get extended error information, call <c>GetLastError</c>.
        /// </returns>
        [SuppressMessage("Microsoft.Design", "CA1060:MovePInvokesToNativeMethodsClass", Justification = "Keep everything in this class")]
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool CloseHandle(IntPtr hObject);

        [SuppressMessage("ReSharper", "UnusedParameter.Local", Justification = "This method signature follows the IDisposable pattern.")]
        private void Dispose(bool disposing)
        {
            if (_isDisposed)
            {
                return;
            }

            CloseHandle(_jobObjectHandle);
            _jobObjectHandle = IntPtr.Zero;

            _isDisposed = true;
        }

        [StructLayout(LayoutKind.Sequential)]
        [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "This type is defined by an external API.")]
        [SuppressMessage("ReSharper", "MemberCanBePrivate.Local", Justification = "This type is defined by an external API.")]
        [SuppressMessage("ReSharper", "FieldCanBeMadeReadOnly.Local", Justification = "This type is defined by an external API.")]
        [SuppressMessage("Sonar", "S1144:Unused private types or members should be removed", Justification = "Types are defined by an external API")]
        [SuppressMessage("Sonar", "S101:Types should be named in camel case", Justification = "Types are defined by an external API")]
        private struct IO_COUNTERS
        {
            public ulong ReadOperationCount;
            public ulong WriteOperationCount;
            public ulong OtherOperationCount;
            public ulong ReadTransferCount;
            public ulong WriteTransferCount;
            public ulong OtherTransferCount;
        }

        /// <summary>
        /// Contains basic limit information for a job object.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "This type is defined by an external API.")]
        [SuppressMessage("ReSharper", "MemberCanBePrivate.Local", Justification = "This type is defined by an external API.")]
        [SuppressMessage("ReSharper", "FieldCanBeMadeReadOnly.Local", Justification = "This type is defined by an external API.")]
        [SuppressMessage("Sonar", "S1144:Unused private types or members should be removed", Justification = "Types are defined by an external API")]
        [SuppressMessage("Sonar", "S101:Types should be named in camel case", Justification = "Types are defined by an external API")]
        private struct JOBOBJECT_BASIC_LIMIT_INFORMATION
        {
            /// <summary>
            ///     <para>
            ///     If <c>LimitFlags</c> specifies <c>LimitProcessTime</c>, this member is the per-process user-mode execution
            ///     time
            ///     limit, in 100-nanosecond ticks. Otherwise, this member is ignored.
            ///     </para>
            ///     <para>
            ///     The system periodically checks to determine whether each process associated with the job has accumulated
            ///     more
            ///     user-mode time than the set limit. If it has, the process is terminated.
            ///     </para>
            ///     <para>
            ///     If the job is nested, the effective limit is the most restrictive limit in the job chain.
            ///     </para>
            /// </summary>
            public long PerProcessUserTimeLimit;

            /// <summary>
            ///     <para>
            ///     If <c>LimitFlags</c> specifies <c>LJOB_OBJECT_LIMIT_JOB_TIME</c>, this member is the per-job user-mode
            ///     execution time limit,
            ///     in 100-nanosecond ticks. Otherwise, this member is ignored.
            ///     </para>
            ///     <para>
            ///     The system adds the current time of the processes associated with the job to this limit. For example, if
            ///     you
            ///     set this limit to 1 minute, and the job has a process that has accumulated 5 minutes of user-mode time, the
            ///     limit actually enforced is 6 minutes.
            ///     </para>
            ///     <para>
            ///     The system periodically checks to determine whether the sum of the user-mode execution time for all
            ///     processes
            ///     is greater than this end-of-job limit. If it is, the action specified in the <c>LEndOfJobTimeAction</c>
            ///     member
            ///     of the
            ///     <c>LJOBOBJECT_END_OF_JOB_TIME_INFORMATION</c> structure is carried out. By default, all processes are
            ///     terminated and
            ///     the status code is set to <c>LERROR_NOT_ENOUGH_QUOTA</c>.
            ///     </para>
            ///     <para>
            ///     To register for notification when this limit is exceeded without terminating processes, use the
            ///     <c>LSetInformationJobObject</c> function with the <c>LJobObjectNotificationLimitInformation</c> information
            ///     class.
            ///     </para>
            /// </summary>
            public long PerJobUserTimeLimit;

            /// <summary>
            /// The limit flags that are in effect. This member is a bitfield that determines whether other structure members
            /// are
            /// used.
            /// </summary>
            public JobObjectLimit LimitFlags;

            /// <summary>
            /// If <c>LimitFlags</c> specifies <c>JOB_OBJECT_LIMIT_WORKINGSET</c>, this member is the minimum working set size
            /// for
            /// each
            /// process associated with the job. Otherwise, this member is ignored.
            /// <para />
            /// If <c>MaximumWorkingSetSize</c> is nonzero, <c>MinimumWorkingSetSize</c> cannot be zero.
            /// </summary>
            public UIntPtr MinimumWorkingSetSize;

            /// <summary>
            /// If <c>LimitFlags</c> specifies <c>JOB_OBJECT_LIMIT_WORKINGSET</c>, this member is the maximum working set size
            /// for
            /// each process associated with the job. Otherwise, this member is ignored.
            /// <para />
            /// If <c>MinimumWorkingSetSize</c> is nonzero, <c>MaximumWorkingSetSize</c> cannot be zero.
            /// </summary>
            public UIntPtr MaximumWorkingSetSize;

            /// <summary>
            /// If <c>LimitFlags</c> specifies <c>JOB_OBJECT_LIMIT_ACTIVE_PROCESS</c>, this member is the active process limit
            /// for
            /// the job. Otherwise, this member is ignored.
            /// <para />
            /// If you try to associate a process with a job, and this causes the active process count to exceed this limit,
            /// the
            /// process is terminated and the association fails.
            /// </summary>
            public uint ActiveProcessLimit;

            /// <summary>
            /// If <c>LimitFlags</c> specifies <c>JOB_OBJECT_LIMIT_AFFINITY</c>, this member is the processor affinity for all
            /// processes associated with the job. Otherwise, this member is ignored.
            /// <para />
            /// The affinity must be a subset of the system affinity mask obtained by calling the <c>GetProcessAffinityMask</c>
            /// function. The affinity of each thread is set to this value, but threads are free to subsequently set their
            /// affinity, as long as it is a subset of the specified affinity mask. Processes cannot set their own affinity
            /// mask.
            /// </summary>
            public UIntPtr Affinity;

            /// <summary>
            /// If <c>LimitFlags</c> specifies <c>JOB_OBJECT_LIMIT_PRIORITY_CLASS</c>, this member is the priority class for
            /// all
            /// processes associated with the job. Otherwise, this member is ignored.
            /// <para />
            /// Processes and threads cannot modify their priority class. The calling process must enable the
            /// <c>SE_INC_BASE_PRIORITY_NAME</c> privilege.
            /// </summary>
            public uint PriorityClass;

            /// <summary>
            /// If <c>LimitFlags</c> specifies <c>JOB_OBJECT_LIMIT_SCHEDULING_CLASS</c>, this member is the scheduling class
            /// for
            /// all processes associated with the job. Otherwise, this member is ignored.
            /// <para />
            /// The valid values are 0 to 9. Use 0 for the least favorable scheduling class relative to other threads, and 9
            /// for
            /// the most favorable scheduling class relative to other threads. By default, this value is 5. To use a scheduling
            /// class greater than 5, the calling process must enable the <c>SE_INC_BASE_PRIORITY_NAME</c> privilege.
            /// </summary>
            public uint SchedulingClass;
        }

#pragma warning disable SA1214 // Readonly fields must appear before non-readonly fields

        /// <summary>
        /// Contains basic and extended limit information for a job object.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "This type is defined by an external API.")]
        [SuppressMessage("ReSharper", "MemberCanBePrivate.Local", Justification = "This type is defined by an external API.")]
        [SuppressMessage("Sonar", "S1144:Unused private types or members should be removed", Justification = "Types are defined by an external API")]
        [SuppressMessage("Sonar", "S101:Types should be named in camel case", Justification = "Types are defined by an external API")]
        private struct JOBOBJECT_EXTENDED_LIMIT_INFORMATION
        {
            /// <summary>
            /// A <c>JOBOBJECT_BASIC_LIMIT_INFORMATION</c> structure that contains basic limit information.
            /// </summary>
            public JOBOBJECT_BASIC_LIMIT_INFORMATION BasicLimitInformation;

            /// <summary>
            /// Reserved.
            /// </summary>
            public readonly IO_COUNTERS IoInfo;

            /// <summary>
            /// If the LimitFlags member of the <c>JOBOBJECT_BASIC_LIMIT_INFORMATION</c> structure specifies the
            /// <c>JOB_OBJECT_LIMIT_PROCESS_MEMORY</c> value, this member specifies the limit for the virtual memory that can
            /// be
            /// committed
            /// by a process. Otherwise, this member is ignored.
            /// </summary>
            public readonly UIntPtr ProcessMemoryLimit;

            /// <summary>
            /// If the LimitFlags member of the <c>JOBOBJECT_BASIC_LIMIT_INFORMATION</c> structure specifies the
            /// <c>JOB_OBJECT_LIMIT_JOB_MEMORY</c> value, this member specifies the limit for the virtual memory that can be
            /// committed for the job. Otherwise, this member is ignored.
            /// </summary>
            public readonly UIntPtr JobMemoryLimit;

            /// <summary>
            /// The peak memory used by any process ever associated with the job.
            /// </summary>
            public readonly UIntPtr PeakProcessMemoryUsed;

            /// <summary>
            /// The peak memory usage of all processes currently associated with the job.
            /// </summary>
            public readonly UIntPtr PeakJobMemoryUsed;
        }

#pragma warning restore SA1214 // Readonly fields must appear before non-readonly fields
    }
}
