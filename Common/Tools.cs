﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using JetBrains.Annotations;

namespace Eurolook.Common
{
    public static class Tools
    {
        public static string ObjectOrNullToString(object o, string whenNull = "<null>", string whenEmpty = "<empty>")
        {
            if (o == null)
            {
                return whenNull;
            }

            string s = o as string;
            if (s == null)
            {
                return o.ToString();
            }

            if (s == "")
            {
                return whenEmpty;
            }

            return s;
        }

        public static string TypeOrNullToString(object o, string whenNull = "<null>")
        {
            return o?.GetType().ToString() ?? whenNull;
        }

        public static string CollectionCount<T>(IEnumerable<T> collectionOrNull, string whenNull = "<null>")
        {
            string type = typeof(T).Name;
            string count = collectionOrNull?.Count().ToString(CultureInfo.InvariantCulture) ?? whenNull;
            return $"IEnumerable<{type}>.Count()={count}";
        }

        public static string GetBase64Image(byte[] bytes)
        {
            if (bytes != null)
            {
                return $"data:image/png;base64,{Convert.ToBase64String(bytes)}";
            }

            return "";
        }

        public static async Task WaitForFileAccessAsync(
            string path,
            FileAccess access,
            Func<FileStream, Task> handler,
            int maxRetries = 20,
            int maxTimeout = 60000)
        {
            int retryCount = 0;
            while (retryCount < maxRetries)
            {
                try
                {
                    using (var file = File.Open(path, FileMode.OpenOrCreate, access))
                    {
                        await handler(file);
                        break;
                    }
                }
                catch (IOException)
                {
                    retryCount = WaitForFileAccess(path, maxRetries, maxTimeout, retryCount);
                }
            }
        }

        public static void WaitForFileAccess(
            string path,
            FileAccess access,
            Action<FileStream> handler,
            int maxRetries = 20,
            int maxTimeout = 60000)
        {
            int retryCount = 0;
            while (retryCount < maxRetries)
            {
                try
                {
                    using (var file = File.Open(path, FileMode.OpenOrCreate, access))
                    {
                        handler(file);
                        break;
                    }
                }
                catch (IOException)
                {
                    retryCount = WaitForFileAccess(path, maxRetries, maxTimeout, retryCount);
                }
            }
        }

        public static string GetCleanXmlName(string name)
        {
            return new string(name.ToCharArray().Where(IsValidXmlNameCharacter).ToArray());
        }

        private static int WaitForFileAccess(
            string path,
            int maxRetries,
            int maxTimeout,
            int retryCount)
        {
            string filePath = Path.GetDirectoryName(path);
            if (filePath != null)
            {
                var autoResetEvent = new AutoResetEvent(false);

                var fileSystemWatcher = new FileSystemWatcher(filePath)
                {
                    EnableRaisingEvents = true,
                };
                fileSystemWatcher.Changed += (o, e) =>
                                             {
                                                 if (Path.GetFullPath(e.FullPath) == Path.GetFullPath(path))
                                                 {
                                                     autoResetEvent.Set();
                                                 }
                                             };

                // waiting here blocks the while loop -> less CPU usage
                autoResetEvent.WaitOne(maxTimeout / maxRetries);
                retryCount++;
            }

            return retryCount;
        }

        private static bool IsValidXmlNameCharacter(char c)
        {
            // 0-9
            if ((c >= 48) && (c <= 57))
            {
                return true;
            }

            // a-z
            if ((c >= 97) && (c <= 122))
            {
                return true;
            }

            // A-Z
            if ((c >= 65) && (c <= 90))
            {
                return true;
            }

            // - _ .
            if ((c == '-') || (c == '_') || (c == '.'))
            {
                return true;
            }

            return false;
        }
    }
}
