﻿using System.Reflection;
using NLog;

namespace Eurolook.Common.Log
{
    internal class NLogLogProvider : ILogProvider
    {
        public ILog Create(string typeName, string logFileName)
        {
            if (!string.IsNullOrEmpty(logFileName))
            {
                GlobalDiagnosticsContext.Set("DataFile", logFileName);
            }
            else
            {
                var assembly = Assembly.GetEntryAssembly();
                string assemblyName = assembly != null ? assembly.GetName().Name : "Eurolook";
                GlobalDiagnosticsContext.Set("DataFile", assemblyName);
            }

            return new NLogLog(NLog.LogManager.GetLogger(typeName));
        }
    }
}
