namespace Eurolook.Common.Log
{
    /// <summary>
    /// All classes that implement this empty interface will have Logging capability
    /// see CanLogExtensions.cs for more details.
    /// </summary>
    public interface ICanLog
    {
    }
}
