﻿using System;

namespace Eurolook.Common.Log
{
    internal sealed class NoLog : ILog
    {
        public static readonly ILog Instance;

        static NoLog()
        {
            Instance = new NoLog();
        }

        private NoLog()
        {
        }

        public bool IsTraceEnabled
        {
            get { return false; }
        }

        public bool IsDebugEnabled
        {
            get { return false; }
        }

        public bool IsErrorEnabled
        {
            get { return false; }
        }

        public bool IsFatalEnabled
        {
            get { return false; }
        }

        public bool IsInfoEnabled
        {
            get { return false; }
        }

        public bool IsWarnEnabled
        {
            get { return false; }
        }

        public void Trace(string message)
        {
            // don't log as this is a non-logging default logger
        }

        public void Trace(string message, Exception exception)
        {
            // don't log as this is a non-logging default logger
        }

        public void Trace(string format, params object[] parameters)
        {
            // don't log as this is a non-logging default logger
        }

        public void Debug(string message)
        {
            // don't log as this is a non-logging default logger
        }

        public void Debug(string message, Exception exception)
        {
            // don't log as this is a non-logging default logger
        }

        public void Debug(string format, params object[] parameters)
        {
            // don't log as this is a non-logging default logger
        }

        public void Info(string message)
        {
            // don't log as this is a non-logging default logger
        }

        public void Info(string message, Exception exception)
        {
            // don't log as this is a non-logging default logger
        }

        public void Info(string format, params object[] parameters)
        {
            // don't log as this is a non-logging default logger
        }

        public void Warn(string message)
        {
            // don't log as this is a non-logging default logger
        }

        public void Warn(string message, Exception exception)
        {
            // don't log as this is a non-logging default logger
        }

        public void Warn(string format, params object[] parameters)
        {
            // don't log as this is a non-logging default logger
        }

        public void Error(string message)
        {
            // don't log as this is a non-logging default logger
        }

        public void Error(string message, Exception exception)
        {
            // don't log as this is a non-logging default logger
        }

        public void Error(string format, params object[] parameters)
        {
            // don't log as this is a non-logging default logger
        }

        public void Fatal(string message)
        {
            // don't log as this is a non-logging default logger
        }

        public void Fatal(string message, Exception exception)
        {
            // don't log as this is a non-logging default logger
        }

        public void Fatal(string format, params object[] parameters)
        {
            // don't log as this is a non-logging default logger
        }
    }
}
