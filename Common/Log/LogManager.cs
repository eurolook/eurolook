using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Eurolook.Common.Log
{
    public static class LogManager
    {
        private static readonly object Lock;
        private static readonly Dictionary<string, ILog> Loggers;

        static LogManager()
        {
            Lock = new object();
            Loggers = new Dictionary<string, ILog>();
            LogEnabled = true;
            LogProviderFunc = () => new NLogLogProvider();
        }

        public static bool LogEnabled { get; set; }

        public static string LogFileName { get; set; }

        public static Version AssemblyVersion { get; set; }

        public static Func<ILogProvider> LogProviderFunc { get; set; }

        /// <summary>
        /// Gets a logger for the specified type.
        /// </summary>
        /// <param name="type">
        /// The type for which to retrieve a logger;
        /// if null the type is derived from the first stack frame.
        /// </param>
        /// <returns>Returns an instance of a logger implementing the <see cref="ILog" /> interface.</returns>
        public static ILog GetLogger(Type type = null)
        {
            string typeString;
            if (type != null)
            {
                typeString = type.ToString();
            }
            else
            {
                // get type of caller from the stack trace
                try
                {
                    var callingMethod = new StackTrace().GetFrame(1).GetMethod();
                    typeString = callingMethod?.DeclaringType?.ToString() ?? "unknown.type";
                }
                catch
                {
                    typeString = "unknown.type";
                }
            }

            return GetLogger(typeString);
        }

        public static ILog GetLogger(string typeName)
        {
            if (!Loggers.TryGetValue(typeName, out var retval))
            {
                lock (Lock)
                {
                    if (!Loggers.TryGetValue(typeName, out retval))
                    {
                        retval = CreateLogger(typeName);
                        if (retval == null)
                        {
                            throw new InvalidOperationException($"Unable to create logger for typename='{typeName}'");
                        }

                        Loggers.Add(typeName, retval);
                    }
                }
            }

            return retval;
        }

        private static ILog CreateLogger(string typeName)
        {
            var logProvider = CreateLogProvider();
            return logProvider?.Create(typeName, LogFileName);
        }

        private static ILogProvider CreateLogProvider()
        {
            if (LogEnabled)
            {
                return LogProviderFunc();
            }

            return new NoLogProvider();
        }
    }
}
