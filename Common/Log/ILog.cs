﻿using System;

namespace Eurolook.Common.Log
{
    public interface ILog
    {
        /// <summary>
        /// Gets a value indicating whether logging is enabled for the <c>Trace</c> level.
        /// </summary>
        /// <returns>
        /// A value of <see langword="true" /> if logging is enabled for the <c>Trace</c> level, otherwise it
        /// returns <see langword="false" />.
        /// </returns>
        bool IsTraceEnabled { get; }

        /// <summary>
        /// Gets a value indicating whether logging is enabled for the <c>Debug</c> level.
        /// </summary>
        /// <returns>
        /// A value of <see langword="true" /> if logging is enabled for the <c>Debug</c> level, otherwise it
        /// returns <see langword="false" />.
        /// </returns>
        bool IsDebugEnabled { get; }

        /// <summary>
        /// Gets a value indicating whether logging is enabled for the <c>Info</c> level.
        /// </summary>
        /// <returns>
        /// A value of <see langword="true" /> if logging is enabled for the <c>Info</c> level, otherwise it
        /// returns <see langword="false" />.
        /// </returns>
        bool IsInfoEnabled { get; }

        /// <summary>
        /// Gets a value indicating whether logging is enabled for the <c>Warn</c> level.
        /// </summary>
        /// <returns>
        /// A value of <see langword="true" /> if logging is enabled for the <c>Warn</c> level, otherwise it
        /// returns <see langword="false" />.
        /// </returns>
        bool IsWarnEnabled { get; }

        /// <summary>
        /// Gets a value indicating whether logging is enabled for the <c>Error</c> level.
        /// </summary>
        /// <returns>
        /// A value of <see langword="true" /> if logging is enabled for the <c>Error</c> level, otherwise it
        /// returns <see langword="false" />.
        /// </returns>
        bool IsErrorEnabled { get; }

        /// <summary>
        /// Gets a value indicating whether logging is enabled for the <c>Fatal</c> level.
        /// </summary>
        /// <returns>
        /// A value of <see langword="true" /> if logging is enabled for the <c>Fatal</c> level, otherwise it
        /// returns <see langword="false" />.
        /// </returns>
        bool IsFatalEnabled { get; }

        void Trace(string message);

        void Trace(string message, Exception exception);

        void Trace(string format, params object[] parameters);

        void Debug(string message);

        void Debug(string message, Exception exception);

        void Debug(string format, params object[] parameters);

        void Info(string message);

        void Info(string message, Exception exception);

        void Info(string format, params object[] parameters);

        void Warn(string message);

        void Warn(string message, Exception exception);

        void Warn(string format, params object[] parameters);

        void Error(string message);

        void Error(string message, Exception exception);

        void Error(string format, params object[] parameters);

        void Fatal(string message);

        void Fatal(string message, Exception exception);

        void Fatal(string format, params object[] parameters);
    }
}
