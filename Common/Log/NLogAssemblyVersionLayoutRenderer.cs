using System.Reflection;
using System.Text;
using NLog;
using NLog.Config;
using NLog.LayoutRenderers;

namespace Eurolook.Common.Log
{
    [LayoutRenderer("assemblyversion")]
    [ThreadAgnostic]
    public class NLogAssemblyVersionLayoutRenderer : LayoutRenderer
    {
        private string _assemblyVersion;

        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            builder.Append(GetAssemblyVersion());
        }

        private string GetAssemblyVersion()
        {
            if (!string.IsNullOrEmpty(_assemblyVersion))
            {
                return _assemblyVersion;
            }

            if (LogManager.AssemblyVersion != null)
            {
                _assemblyVersion = LogManager.AssemblyVersion.ToString();
            }
            else
            {
                var assembly = Assembly.GetEntryAssembly() ?? Assembly.GetExecutingAssembly();
                _assemblyVersion = assembly.GetName().Version.ToString();
            }

            return _assemblyVersion;
        }
    }
}
