﻿namespace Eurolook.Common.Log
{
    internal class NoLogProvider : ILogProvider
    {
        public ILog Create(string typeName, string logFileName)
        {
            return NoLog.Instance;
        }
    }
}
