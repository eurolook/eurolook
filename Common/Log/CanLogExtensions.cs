using System;
using System.Diagnostics;

namespace Eurolook.Common.Log
{
    public static class CanLogExtensions
    {
        /// <summary>
        /// Gets a value indicating whether logging is enabled for the <c>Trace</c> level.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <returns>
        /// A value of <see langword="true" /> if logging is enabled for the <c>Trace</c> level, otherwise it
        /// returns <see langword="false" />.
        /// </returns>
        [DebuggerStepThrough]
        public static bool LogIsTraceEnabled(this ICanLog canLog)
        {
            return GetLog(canLog).IsTraceEnabled;
        }

        /// <summary>
        /// Gets a value indicating whether logging is enabled for the <c>Debug</c> level.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <returns>
        /// A value of <see langword="true" /> if logging is enabled for the <c>Debug</c> level, otherwise it
        /// returns <see langword="false" />.
        /// </returns>
        [DebuggerStepThrough]
        public static bool LogIsDebugEnabled(this ICanLog canLog)
        {
            return GetLog(canLog).IsDebugEnabled;
        }

        /// <summary>
        /// Gets a value indicating whether logging is enabled for the <c>Info</c> level.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <returns>
        /// A value of <see langword="true" /> if logging is enabled for the <c>Info</c> level, otherwise it
        /// returns <see langword="false" />.
        /// </returns>
        [DebuggerStepThrough]
        public static bool LogIsInfoEnabled(this ICanLog canLog)
        {
            return GetLog(canLog).IsInfoEnabled;
        }

        /// <summary>
        /// Gets a value indicating whether logging is enabled for the <c>Warn</c> level.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <returns>
        /// A value of <see langword="true" /> if logging is enabled for the <c>Warn</c> level, otherwise it
        /// returns <see langword="false" />.
        /// </returns>
        [DebuggerStepThrough]
        public static bool LogIsWarnEnabled(this ICanLog canLog)
        {
            return GetLog(canLog).IsWarnEnabled;
        }

        /// <summary>
        /// Gets a value indicating whether logging is enabled for the <c>Error</c> level.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <returns>
        /// A value of <see langword="true" /> if logging is enabled for the <c>Error</c> level, otherwise it
        /// returns <see langword="false" />.
        /// </returns>
        [DebuggerStepThrough]
        public static bool LogIsErrorEnabled(this ICanLog canLog)
        {
            return GetLog(canLog).IsErrorEnabled;
        }

        /// <summary>
        /// Gets a value indicating whether logging is enabled for the <c>Fatal</c> level.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <returns>
        /// A value of <see langword="true" /> if logging is enabled for the <c>Fatal</c> level, otherwise it
        /// returns <see langword="false" />.
        /// </returns>
        [DebuggerStepThrough]
        public static bool LogIsFatalEnabled(this ICanLog canLog)
        {
            return GetLog(canLog).IsFatalEnabled;
        }

        /// <summary>
        /// Logs trace messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="message">The log message.</param>
        [DebuggerStepThrough]
        public static void LogTrace(this ICanLog canLog, string message)
        {
            GetLog(canLog).Trace(message);
        }

        /// <summary>
        /// Logs trace messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="object">The @object.</param>
        [DebuggerStepThrough]
        public static void LogTrace(this ICanLog canLog, object @object)
        {
            GetLog(canLog).Trace("{0}", @object);
        }

        /// <summary>
        /// Logs trace messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="exception">The exception.</param>
        [DebuggerStepThrough]
        public static void LogTrace(this ICanLog canLog, Exception exception)
        {
            GetLog(canLog).Trace(exception.Message, exception);
        }

        /// <summary>
        /// Logs trace messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="message">The log message.</param>
        /// <param name="exception">The exception.</param>
        [DebuggerStepThrough]
        public static void LogTrace(this ICanLog canLog, string message, Exception exception)
        {
            GetLog(canLog).Trace(message, exception);
        }

        /// <summary>
        /// Logs trace messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="format">The format.</param>
        /// <param name="parameters">The parameters.</param>
        [DebuggerStepThrough]
        public static void LogTraceFormat(this ICanLog canLog, string format, params object[] parameters)
        {
            GetLog(canLog).Trace(format, parameters);
        }

        /// <summary>
        /// Logs debug messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="message">The log message.</param>
        [DebuggerStepThrough]
        public static void LogDebug(this ICanLog canLog, string message)
        {
            GetLog(canLog).Debug(message);
        }

        /// <summary>
        /// Logs debug messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="object">The @object.</param>
        [DebuggerStepThrough]
        public static void LogDebug(this ICanLog canLog, object @object)
        {
            GetLog(canLog).Debug("{0}", @object);
        }

        /// <summary>
        /// Logs debug messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="exception">The exception.</param>
        [DebuggerStepThrough]
        public static void LogDebug(this ICanLog canLog, Exception exception)
        {
            GetLog(canLog).Debug(exception.Message, exception);
        }

        /// <summary>
        /// Logs debug messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="message">The log message.</param>
        /// <param name="exception">The exception.</param>
        [DebuggerStepThrough]
        public static void LogDebug(this ICanLog canLog, string message, Exception exception)
        {
            GetLog(canLog).Debug(message, exception);
        }

        /// <summary>
        /// Logs debug messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="format">The format.</param>
        /// <param name="parameters">The parameters.</param>
        [DebuggerStepThrough]
        public static void LogDebugFormat(this ICanLog canLog, string format, params object[] parameters)
        {
            GetLog(canLog).Debug(format, parameters);
        }

        /// <summary>
        /// Logs info messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="message">The log message.</param>
        [DebuggerStepThrough]
        public static void LogInfo(this ICanLog canLog, string message)
        {
            GetLog(canLog).Info(message);
        }

        /// <summary>
        /// Logs info messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="object">The @object.</param>
        [DebuggerStepThrough]
        public static void LogInfo(this ICanLog canLog, object @object)
        {
            GetLog(canLog).Info("{0}", @object);
        }

        /// <summary>
        /// Logs info messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="exception">The exception.</param>
        [DebuggerStepThrough]
        public static void LogInfo(this ICanLog canLog, Exception exception)
        {
            GetLog(canLog).Info(exception.Message, exception);
        }

        /// <summary>
        /// Logs info messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="message">The log message.</param>
        /// <param name="exception">The exception.</param>
        [DebuggerStepThrough]
        public static void LogInfo(this ICanLog canLog, string message, Exception exception)
        {
            GetLog(canLog).Info(message, exception);
        }

        /// <summary>
        /// Logs info messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="format">The format.</param>
        /// <param name="parameters">The parameters.</param>
        [DebuggerStepThrough]
        public static void LogInfoFormat(this ICanLog canLog, string format, params object[] parameters)
        {
            GetLog(canLog).Info(format, parameters);
        }

        /// <summary>
        /// Logs warn messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="message">The log message.</param>
        [DebuggerStepThrough]
        public static void LogWarn(this ICanLog canLog, string message)
        {
            GetLog(canLog).Warn(message);
        }

        /// <summary>
        /// Logs warn messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="object">The @object.</param>
        [DebuggerStepThrough]
        public static void LogWarn(this ICanLog canLog, object @object)
        {
            GetLog(canLog).Warn("{0}", @object);
        }

        /// <summary>
        /// Logs warn messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="exception">The exception.</param>
        [DebuggerStepThrough]
        public static void LogWarn(this ICanLog canLog, Exception exception)
        {
            GetLog(canLog).Warn(exception.Message, exception);
        }

        /// <summary>
        /// Logs warn messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="message">The log message.</param>
        /// <param name="exception">The exception.</param>
        [DebuggerStepThrough]
        public static void LogWarn(this ICanLog canLog, string message, Exception exception)
        {
            GetLog(canLog).Warn(message, exception);
        }

        /// <summary>
        /// Logs warn messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="format">The format.</param>
        /// <param name="parameters">The parameters.</param>
        [DebuggerStepThrough]
        public static void LogWarnFormat(this ICanLog canLog, string format, params object[] parameters)
        {
            GetLog(canLog).Warn(format, parameters);
        }

        /// <summary>
        /// Logs error messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="message">The log message.</param>
        [DebuggerStepThrough]
        public static void LogError(this ICanLog canLog, string message)
        {
            GetLog(canLog).Error(message);
        }

        /// <summary>
        /// Logs error messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="object">The @object.</param>
        [DebuggerStepThrough]
        public static void LogError(this ICanLog canLog, object @object)
        {
            GetLog(canLog).Error("{0}", @object);
        }

        /// <summary>
        /// Logs error messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="exception">The exception.</param>
        [DebuggerStepThrough]
        public static void LogError(this ICanLog canLog, Exception exception)
        {
            GetLog(canLog).Error(exception.Message, exception);
        }

        /// <summary>
        /// Logs error messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="message">The log message.</param>
        /// <param name="exception">The exception.</param>
        [DebuggerStepThrough]
        public static void LogError(this ICanLog canLog, string message, Exception exception)
        {
            GetLog(canLog).Error(message, exception);
        }

        /// <summary>
        /// Logs error messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="format">The format.</param>
        /// <param name="parameters">The parameters.</param>
        [DebuggerStepThrough]
        public static void LogErrorFormat(this ICanLog canLog, string format, params object[] parameters)
        {
            GetLog(canLog).Error(format, parameters);
        }

        /// <summary>
        /// Logs fatal messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="message">The log message.</param>
        [DebuggerStepThrough]
        public static void LogFatal(this ICanLog canLog, string message)
        {
            GetLog(canLog).Fatal(message);
        }

        /// <summary>
        /// Logs fatal messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="object">The @object.</param>
        [DebuggerStepThrough]
        public static void LogFatal(this ICanLog canLog, object @object)
        {
            GetLog(canLog).Fatal("{0}", @object);
        }

        /// <summary>
        /// Logs fatal messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="exception">The exception.</param>
        [DebuggerStepThrough]
        public static void LogFatal(this ICanLog canLog, Exception exception)
        {
            GetLog(canLog).Fatal(exception.Message, exception);
        }

        /// <summary>
        /// Logs fatal messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="message">The log message.</param>
        /// <param name="exception">The exception.</param>
        [DebuggerStepThrough]
        public static void LogFatal(this ICanLog canLog, string message, Exception exception)
        {
            GetLog(canLog).Fatal(message, exception);
        }

        /// <summary>
        /// Logs fatal messages.
        /// </summary>
        /// <param name="canLog">An object implementing the <see cref="ICanLog" /> interface.</param>
        /// <param name="format">The format.</param>
        /// <param name="parameters">The parameters.</param>
        [DebuggerStepThrough]
        public static void LogFatalFormat(this ICanLog canLog, string format, params object[] parameters)
        {
            GetLog(canLog).Fatal(format, parameters);
        }

        public static Exception GetInnerException(this Exception exception)
        {
            if (exception != null && exception.Message.Contains("inner exception") && exception.InnerException != null)
            {
                return GetInnerException(exception.InnerException);
            }

            return exception;
        }

        private static ILog GetLog(ICanLog canLog)
        {
            return canLog != null ? LogManager.GetLogger(canLog.GetType()) : NoLog.Instance;
        }
    }
}
