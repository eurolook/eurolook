﻿namespace Eurolook.Common.Log
{
    public interface ILogProvider
    {
        ILog Create(string typeName, string logFileName);
    }
}
