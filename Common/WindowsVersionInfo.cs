using Microsoft.Win32;

namespace Eurolook.Common
{
    public class WindowsVersionInfo
    {
        private static readonly string RegistryKeyName = @"SOFTWARE\Microsoft\Windows NT\CurrentVersion";

        /// <summary>
        /// Gets the unified build revision as the XXX in Windows 10 OSBuild 18362.XXX.
        /// </summary>
        public string UnifiedBuildRevision => ReadVersionValueFromRegistry("UBR");

        /// <summary>
        /// Gets the unified build number as the XXXXX in Windows 10 OSBuild XXXXX.239.
        /// </summary>
        public string BuildNumber => ReadVersionValueFromRegistry("CurrentBuild");

        /// <summary>
        /// Gets the release ID such as 1903 in Windows 10 1903.
        /// </summary>
        public string ReleaseId => ReadVersionValueFromRegistry("ReleaseId");

        /// <summary>
        /// Gets the product name such as Windows 10 Enterprise.
        /// </summary>
        public string ProductName => ReadVersionValueFromRegistry("ProductName", "Windows");

        /// <summary>
        /// Gets the OS build as shown in Settings > About.
        /// </summary>
        public string OsBuild => string.Join(".", BuildNumber, UnifiedBuildRevision);

        public override string ToString()
        {
            return $"{ProductName} {ReleaseId} Build {OsBuild}";
        }

        private string ReadVersionValueFromRegistry(string valueName, string defaultValue = "<na>")
        {
            try
            {
                using (var registryKey = Registry.LocalMachine.OpenSubKey(RegistryKeyName))
                {
                    return registryKey?.GetValue(valueName)?.ToString();
                }
            }
            catch
            {
                return defaultValue;
            }
        }
    }
}
