using System;

namespace Eurolook.Common
{
    /// <summary>
    /// Denotes that a property should be copied by <see cref="CopyProperties{T}.Copy" />
    /// although it doesn't meet the standard requirements.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class CopyPropertyIncludeAttribute : Attribute
    {
    }
}
