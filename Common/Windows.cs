using System.Security.Principal;

namespace Eurolook.Common
{
    public static class Windows
    {
        public static string Login
        {
            get { return WindowsIdentity.GetCurrent().Name; }
        }
    }
}
