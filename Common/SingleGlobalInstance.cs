using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading;
using Eurolook.Common.Log;

namespace Eurolook.Common
{
    public sealed class SingleGlobalInstance : IDisposable, ICanLog
    {
        private readonly Mutex _mutex;
        private readonly bool _hasHandle;

        public SingleGlobalInstance(int timeoutMs)
            : this("", timeoutMs)
        {
        }

        public SingleGlobalInstance(string nameSuffix, int timeoutMs)
        {
            string appGuid =
                ((GuidAttribute)Assembly.GetExecutingAssembly()
                                        .GetCustomAttributes(typeof(GuidAttribute), false)
                                        .GetValue(0)).Value;

            string mutexId = $"Global\\{{{appGuid}}}-{nameSuffix}";
            _mutex = new Mutex(false, mutexId);

            var allowEveryoneRule = new MutexAccessRule(
                new SecurityIdentifier(WellKnownSidType.WorldSid, null),
                MutexRights.FullControl,
                AccessControlType.Allow);
            var securitySettings = new MutexSecurity();
            securitySettings.AddAccessRule(allowEveryoneRule);
            _mutex.SetAccessControl(securitySettings);

            try
            {
                _hasHandle = _mutex.WaitOne(timeoutMs < 0 ? Timeout.Infinite : timeoutMs, false);

                if (!_hasHandle)
                {
                    throw new TimeoutException("Timeout: Did not acquire mutex while waiting for exclusive access.");
                }
            }
            catch (AbandonedMutexException ex)
            {
                this.LogError("Mutex was abandoned by another thread.", ex);

                // The current thread now owns the abandoned mutex, therefore is must be released.
                _hasHandle = true;
            }
        }

        public void Dispose()
        {
            if (_mutex == null)
            {
                return;
            }

            if (_hasHandle)
            {
                _mutex.ReleaseMutex();
            }

            _mutex.Close();
        }
    }
}
