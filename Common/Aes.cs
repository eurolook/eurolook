﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Eurolook.Common
{
    public static class Aes
    {
        public static string Encrypt256(string iv, string plainText, byte[] key)
        {
            var aes = System.Security.Cryptography.Aes.Create();
            aes.BlockSize = 128;
            aes.KeySize = 256;
            aes.IV = Encoding.UTF8.GetBytes(iv);
            aes.Key = key;
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            var src = Encoding.Unicode.GetBytes(plainText);

            using var encrypt = aes.CreateEncryptor();
            var dest = encrypt.TransformFinalBlock(src, 0, src.Length);
            return Convert.ToBase64String(dest);
        }

        public static string Decrypt256(string input, byte[] key)
        {
            var index = input.IndexOf(':');

            // TODO: Change after migration to .NET 6
            //var iv = input[..index];
            //var cipherText = input[++index..];
            var iv = input.Substring(0, index);
            var cipherText = input.Substring(index + 1);

            var aes = System.Security.Cryptography.Aes.Create();
            aes.BlockSize = 128;
            aes.KeySize = 256;
            aes.IV = Encoding.UTF8.GetBytes(iv);
            aes.Key = key;
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            var src = Convert.FromBase64String(cipherText);

            using var decrypt = aes.CreateDecryptor();
            var dest = decrypt.TransformFinalBlock(src, 0, src.Length);
            return Encoding.Unicode.GetString(dest);
        }

        public static byte[] Sha256(string text)
        {
            return SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(text));
        }
    }
}
