using System;
using System.Collections.Generic;
using System.Linq;

namespace Eurolook.Common
{
    public class ClassNameResolver<T> : IClassNameResolver<T>
    {
        private readonly Lazy<IEnumerable<T>> _candidates;

        public ClassNameResolver(Lazy<IEnumerable<T>> candidates)
        {
            _candidates = candidates;
        }

        public T Resolve(string className)
        {
            return _candidates.Value.FirstOrDefault(cc => cc.GetType().Name == className);
        }
    }
}
