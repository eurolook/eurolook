using System;
using System.Linq;
using System.Reflection;

namespace Eurolook.Common
{
    /// <summary>
    /// A class to read attributes from an assembly.
    /// </summary>
    public static class AssemblyAttributeReader
    {
        /// <summary>
        /// Gets the first attribute of the specified type or null if none is found.
        /// </summary>
        /// <typeparam name="T">The type of attribute to search for.</typeparam>
        /// <param name="assembly">the assembly to use, if it is null the assembly of this method is used.</param>
        /// <returns>Returns the first attribute of the specified type or null if none is found.</returns>
        public static T GetFirstAssemblyAttribute<T>(Assembly assembly)
            where T : Attribute
        {
            if (assembly == null)
            {
                assembly = typeof(AssemblyAttributeReader).Assembly;
            }

            var attribute = assembly.GetCustomAttributes(typeof(T), false).OfType<T>().FirstOrDefault();
            return attribute;
        }

        /// <summary>
        /// Gets the value of the first attribute of the specified type,
        /// a default string is returned if no attribute is found.
        /// </summary>
        /// <typeparam name="T">The type of attribute to search for.</typeparam>
        /// <param name="assembly">the assembly to use, if it is null the assembly of this method is used.</param>
        /// <returns>
        /// Returns the value of the first attribute of teh specified type, or a default value if the attribute is
        /// not found.
        /// </returns>
        public static string GetFirstAssemblyAttributeString<T>(Assembly assembly = null)
            where T : Attribute
        {
            var attribute = GetFirstAssemblyAttribute<T>(assembly);
            if (attribute == null)
            {
                return "no information found";
            }

            if (attribute is AssemblyCompanyAttribute)
            {
                return (attribute as AssemblyCompanyAttribute).Company;
            }

            if (attribute is AssemblyConfigurationAttribute)
            {
                return (attribute as AssemblyConfigurationAttribute).Configuration;
            }

            if (attribute is AssemblyCopyrightAttribute)
            {
                return (attribute as AssemblyCopyrightAttribute).Copyright;
            }

            if (attribute is AssemblyDescriptionAttribute)
            {
                return (attribute as AssemblyDescriptionAttribute).Description;
            }

            if (attribute is AssemblyProductAttribute)
            {
                return (attribute as AssemblyProductAttribute).Product;
            }

            if (attribute is AssemblyTitleAttribute)
            {
                return (attribute as AssemblyTitleAttribute).Title;
            }

            if (attribute is AssemblyInformationalVersionAttribute)
            {
                return (attribute as AssemblyInformationalVersionAttribute).InformationalVersion;
            }

            return "type not recognized: " + typeof(T);
        }
    }
}
