using System;
using System.Runtime.Serialization;

namespace Eurolook.Common
{
    [Serializable]
    public class EurolookConfigurationException : Exception
    {
        public EurolookConfigurationException()
        {
        }

        public EurolookConfigurationException(string message)
            : base(message)
        {
        }

        public EurolookConfigurationException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected EurolookConfigurationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
