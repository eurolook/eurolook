﻿using System;

namespace Eurolook.Common
{
    public static class TimeUtils
    {
        // The time is stored without timezone information in SQL Server CE.
        // SQL Server CE doesn't support timezones (DATETIME2).
        // The time stored is always UTC. Ensure that the DateTime object is set correctly.
        public static DateTime GetUtcDateTime(DateTime value)
        {
            switch (value.Kind)
            {
                case DateTimeKind.Unspecified:
                    return DateTime.SpecifyKind(value, DateTimeKind.Utc);
                case DateTimeKind.Local:
                    return value.ToUniversalTime();
                default:
                    return value;
            }
        }
    }
}
