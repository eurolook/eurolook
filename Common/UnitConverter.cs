﻿namespace Eurolook.Common
{
    public static class UnitConverter
    {
        // TODO: this seems duplicated by Eurolook.WordAddIn.Utils.UnitConverter
        public static float CmToPoints(double value)
        {
            return (float)(value * 72 / 2.54);
        }
    }
}
