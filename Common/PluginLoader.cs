﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Core;
using Eurolook.Common.Log;
#if NET6_0_OR_GREATER
using System.Runtime.Loader;
#endif

namespace Eurolook.Common;

#if NET6_0_OR_GREATER
public class PluginLoadContext : AssemblyLoadContext
{
    private readonly AssemblyDependencyResolver _resolver;

    public PluginLoadContext(string pluginPath)
    {
        _resolver = new AssemblyDependencyResolver(pluginPath);

        Resolving += (context, name) =>
                             {
                                 string assemblyPath = _resolver.ResolveAssemblyToPath(name);
                                 return assemblyPath != null ? context.LoadFromAssemblyPath(assemblyPath) : null;
                             };
    }

    protected override Assembly Load(AssemblyName assemblyName)
    {
        string assemblyPath = _resolver.ResolveAssemblyToPath(assemblyName);
        if (assemblyPath != null)
        {
            return LoadFromAssemblyPath(assemblyPath);
        }

        return null;
    }

    protected override IntPtr LoadUnmanagedDll(string unmanagedDllName)
    {
        string libraryPath = _resolver.ResolveUnmanagedDllToPath(unmanagedDllName);
        if (libraryPath != null)
        {
            return LoadUnmanagedDllFromPath(libraryPath);
        }

        return IntPtr.Zero;
    }
}
#endif

public class PluginLoader : ICanLog
{
    private readonly List<Assembly> _pluginAssemblies = new();

    public PluginLoader(string pluginFolder, IEnumerable<string> pluginFiles)
    {
        PluginFolder = pluginFolder;

        if (pluginFiles != null)
        {
            foreach (var plugin in pluginFiles)
            {
                PluginFileNames.Add(plugin);
            }
        }

        LoadPluginsFromPattern();
    }

    public List<string> PluginFileNames { get; } = new();

    public string PluginFolder { get; }

    public IReadOnlyCollection<Assembly> PluginAssemblies => _pluginAssemblies;

    public void RegisterSingleMandatoryPluginModule<T>(ContainerBuilder builder)
        where T : IModule
    {
        var modules = GetPluginModules<T>();
        if (!modules.Any())
        {
            throw new ArgumentException(
                $"No plugins of type {typeof(T).Name} found in '{PluginFolder}' (files '{string.Join(", ", PluginFileNames)}').");
        }

        if (modules.Count > 1)
        {
            throw new ArgumentException(
                $"More than one plugin of type {typeof(T).Name} found in '{PluginFolder}' (files '{string.Join(", ", PluginFileNames)}').");
        }

        builder.RegisterModule(modules.First());
    }

    public void RegisterPluginModules(ContainerBuilder builder)
    {
        foreach (var pluginAssembly in PluginAssemblies)
        {
            builder.RegisterAssemblyModules<IModule>(pluginAssembly);
        }
    }

    private void LoadPluginsFromPattern()
    {
        if (!Directory.Exists(PluginFolder))
        {
            return;
        }

        foreach (var dllFile in PluginFileNames.Select(f => Path.Combine(PluginFolder, f)))
        {
            if (!File.Exists(dllFile))
            {
                // Probably the plugins are not correctly configured in the appsettings
                this.LogError($"Requested plugin not found: '{dllFile}'");
                if (Debugger.IsAttached)
                {
                    Debugger.Break();
                }

                continue;
            }

            try
            {
                this.LogInfo($"Found plugin '{dllFile}'");
#if NET6_0_OR_GREATER
                var assembly = Assembly.Load(Path.GetFileNameWithoutExtension(dllFile));
#else
                var assembly = Assembly.LoadFile(dllFile);
#endif
                _pluginAssemblies.Add(assembly);
                this.LogInfo($"Loaded plugin assembly '{assembly}'");
            }
            catch (Exception e)
            {
                this.LogError($"Failed to load plugin '{dllFile}'", e);
            }
        }
    }

    private IReadOnlyCollection<T> GetPluginModules<T>()
        where T : IModule
    {
        var result = new List<T>();
        var searchType = typeof(T);
        if (!searchType.IsInterface)
        {
            throw new ArgumentException("Given type T is not an interface.");
        }

        foreach (var assembly in PluginAssemblies)
        {
            LogManager.GetLogger().Info($"Searching for plugin modules of type '{searchType.Name}' in '{assembly}'");

            foreach (var type in assembly.GetTypes().Where(p => searchType.IsAssignableFrom(p)))
            {
                try
                {
                    var pluginModule = (T)Activator.CreateInstance(type);
                    result.Add(pluginModule);
                    LogManager.GetLogger().Info($"Found plugin module {pluginModule.GetType()}");
                }
                catch (Exception)
                {
                    this.LogError($"Failed to create plugin module '{searchType}'");
                }
            }
        }

        return result;
    }
}
