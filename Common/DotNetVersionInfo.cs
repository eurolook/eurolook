using System;
using Microsoft.Win32;

namespace Eurolook.Common
{
    /// <summary>
    /// A class providing information on the installed .NET Framework version and runtime.
    /// </summary>
    /// <remarks>
    /// Source code is based on document available at
    /// https://docs.microsoft.com/en-us/dotnet/framework/migration-guide/how-to-determine-which-versions-are-installed.
    /// </remarks>
    public class DotNetVersionInfo
    {
        public DotNetVersionInfo()
        {
            RuntimeVersion = Environment.Version.ToString();
            FullFrameworkReleaseVersion = GetDotNetFullFrameworkReleaseVersion();
        }

        public DotNetVersionInfo(string dotNetRuntimeVersion, int? dotNetFrameworkFullReleaseVersion)
        {
            RuntimeVersion = dotNetRuntimeVersion;
            FullFrameworkReleaseVersion = dotNetFrameworkFullReleaseVersion;
        }

        public string RuntimeVersion { get; }

        public int? FullFrameworkReleaseVersion { get; }

        public string FrameworkVersion
        {
            get
            {
                return FullFrameworkReleaseVersion.HasValue
                    ? CheckFor45PlusVersion(FullFrameworkReleaseVersion.Value)
                    : "4.5 or later is not detected.";
            }
        }

        private static int? GetDotNetFullFrameworkReleaseVersion()
        {
            const string subKey = @"SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\";

            using (var ndpKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32)
                                           .OpenSubKey(subKey))
            {
                if (ndpKey?.GetValue("Release") != null)
                {
                    return (int)ndpKey.GetValue("Release");
                }

                return null;
            }
        }

        /// <summary>
        /// See https://docs.microsoft.com/en-us/dotnet/framework/migration-guide/minimum-release-dword
        /// and https://docs.microsoft.com/en-us/dotnet/framework/migration-guide/versions-and-dependencies.
        /// </summary>
        /// <param name="releaseKey">The value of the Release as found in the Registry.</param>
        /// <returns>A textual description of the currently installed .NET Framework version.</returns>
        /// <remarks>Checking the version using >= will enable forward compatibility.</remarks>
        private static string CheckFor45PlusVersion(int releaseKey)
        {
            if (releaseKey >= 528040)
            {
                return "4.8";
            }

            if (releaseKey >= 461808)
            {
                return "4.7.2";
            }

            if (releaseKey >= 461308)
            {
                return "4.7.1";
            }

            if (releaseKey >= 460798)
            {
                return "4.7";
            }

            if (releaseKey >= 394802)
            {
                return "4.6.2";
            }

            if (releaseKey >= 394254)
            {
                return "4.6.1";
            }

            if (releaseKey >= 393295)
            {
                return "4.6";
            }

            if (releaseKey >= 379893)
            {
                return "4.5.2";
            }

            if (releaseKey >= 378675)
            {
                return "4.5.1";
            }

            if (releaseKey >= 378389)
            {
                return "4.5";
            }

            // This code should never execute. A non-null release key should mean
            // that 4.5 or later is installed.
            return "No 4.5 or later version detected";
        }
    }
}
