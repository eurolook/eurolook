using System;
using System.Threading;
using System.Threading.Tasks;

namespace Eurolook.Common
{
    public class SemaphoreLocker
    {
        private readonly SemaphoreSlim _semaphore = new SemaphoreSlim(1, 1);

        public async Task LockAsync(Func<Task> func)
        {
            await _semaphore.WaitAsync();
            try
            {
                await func();
            }
            finally
            {
                _semaphore.Release();
            }
        }
    }
}
