using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Eurolook.Common.Extensions;

namespace Eurolook.Common.AsyncCommands
{
    public class AsyncCommand : IAsyncCommand
    {
        private bool _isExecuting;

        public AsyncCommand(
            Func<Task> execute,
            Func<bool> canExecute = null,
            Action<Exception> errorHandler = null)
        {
            ExecuteFunc = execute;
            CanExecuteFunc = canExecute;
            ErrorHandler = errorHandler;
        }

        public AsyncCommand()
        {
        }

        public event EventHandler CanExecuteChanged;

        protected Func<Task> ExecuteFunc { get; set; }

        protected Func<bool> CanExecuteFunc { get; set; }

        protected Action<Exception> ErrorHandler { get; set; }

        public bool CanExecute()
        {
            return !_isExecuting && (CanExecuteFunc?.Invoke() ?? true);
        }

        public async Task ExecuteAsync()
        {
            if (CanExecute())
            {
                try
                {
                    _isExecuting = true;
                    await ExecuteFunc();
                }
                finally
                {
                    _isExecuting = false;
                }
            }

            RaiseCanExecuteChanged();
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute();
        }

        void ICommand.Execute(object parameter)
        {
            ExecuteAsync().FireAndForgetSafeAsync(ErrorHandler);
        }
    }

    public class AsyncCommand<T> : IAsyncCommand<T>
    {
        private readonly Func<T, Task> _execute;
        private readonly Func<T, bool> _canExecute;
        private readonly Action<Exception> _errorHandler;
        private bool _isExecuting;

        public AsyncCommand(
            Func<T, Task> execute,
            Func<T, bool> canExecute = null,
            Action<Exception> errorHandler = null)
        {
            _execute = execute;
            _canExecute = canExecute;
            _errorHandler = errorHandler;
        }

        public AsyncCommand()
        {
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(T parameter)
        {
            return !_isExecuting && (_canExecute?.Invoke(parameter) ?? true);
        }

        public async Task ExecuteAsync(T parameter)
        {
            if (CanExecute(parameter))
            {
                try
                {
                    _isExecuting = true;
                    await _execute(parameter);
                }
                finally
                {
                    _isExecuting = false;
                }
            }

            RaiseCanExecuteChanged();
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute((T)parameter);
        }

        void ICommand.Execute(object parameter)
        {
            ExecuteAsync((T)parameter).FireAndForgetSafeAsync(_errorHandler);
        }
    }
}
