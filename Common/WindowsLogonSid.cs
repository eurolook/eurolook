using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace Eurolook.Common
{
    public class WindowsLogonSid
    {
        private const int UoiUserSid = 4;

        [DllImport("user32.dll")]
        public static extern bool GetUserObjectInformation(
            IntPtr hObj,
            int nIndex,
            [MarshalAs(UnmanagedType.LPArray)] byte[] pvInfo,
            int nLength,
            out uint lpnLengthNeeded);

        [DllImport("kernel32.dll")]
        public static extern int GetCurrentThreadId();

        [DllImport("advapi32", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool ConvertSidToStringSid(
            [MarshalAs(UnmanagedType.LPArray)] byte[] pSid,
            out IntPtr ptrSid);

        public static string GetLogonSid()
        {
            string sidString = "";
            var desktopId = GetThreadDesktop(GetCurrentThreadId());
            var buf = new byte[100];
            GetUserObjectInformation(desktopId, UoiUserSid, buf, 100, out uint _);
            if (!ConvertSidToStringSid(buf, out var ptrSid))
            {
                throw new Win32Exception();
            }

            try
            {
                sidString = Marshal.PtrToStringAuto(ptrSid);
            }
            catch
            {
                // ignored
            }

            return sidString;
        }

        [DllImport("user32.dll")]
        private static extern IntPtr GetThreadDesktop(int dwThreadId);
    }
}
