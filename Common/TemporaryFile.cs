﻿using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using Eurolook.Common.Log;

namespace Eurolook.Common
{
    /// <summary>
    /// Use this class to automatically delete temporary files.
    /// </summary>
    /// <code>
    /// using (var tempFile = new TemporaryFile(".docx"))
    /// {
    ///     factory.CreateDocument(tempFile.FullName, author, type, lang);
    /// }
    /// </code>
    [Localizable(false)]
    public sealed class TemporaryFile : IDisposable, ICanLog
    {
        public TemporaryFile(string extension = ".tmp")
        {
            string fileName = Guid.NewGuid().ToString().Replace("-", "") + extension;
            FullName = Path.Combine(Path.GetTempPath(), fileName);
        }

        public TemporaryFile(string fileName, string extension)
        {
            // sometimes it is important to keep a specific file name (e.g. Outlook uses the file name on disk when opening/saving an attachment)
            // -> create a unique sub folder which is deleted together with the temporary file (on dispose).
            string subfolderName = Guid.NewGuid().ToString().Replace("-", "");
            GeneratedFolder = Path.Combine(Path.GetTempPath(), subfolderName);
            Directory.CreateDirectory(GeneratedFolder);
            FullName = Path.Combine(GeneratedFolder, fileName.EndsWith(extension) ? fileName : $"{fileName}{extension}");
        }

        ~TemporaryFile()
        {
            Dispose(false);
        }

        public string FullName { get; private set; }

        public string GeneratedFolder { get; private set; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        [SuppressMessage("ReSharper", "UnusedParameter.Local", Justification = "This method signature is according to the IDisposable pattern.")]
        private void Dispose(bool disposing)
        {
            if (!string.IsNullOrEmpty(FullName) && File.Exists(FullName))
            {
                try
                {
                    // Remove possible read-only flag
                    File.SetAttributes(FullName, FileAttributes.Normal);
                    File.Delete(FullName);
                    if (GeneratedFolder != null && Directory.Exists(GeneratedFolder))
                    {
                        Directory.Delete(GeneratedFolder);
                    }
                }
                catch (IOException ex)
                {
                    // Just log the exception if we can't delete the temp file.
                    this.LogWarn("Unable to delete temporary file.", ex);
                }

                FullName = null;
                GeneratedFolder = null;
            }
        }
    }
}
