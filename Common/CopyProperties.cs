using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Eurolook.Common
{
    public static class CopyProperties<T>
    {
        private static readonly Action<T, T> CopyAction;

#pragma warning disable S3963 // "static" fields should be initialized inline
        static CopyProperties()
        {
            var type = typeof(T);
            var sourceParameter = Expression.Parameter(type, "source");
            var destParameter = Expression.Parameter(type, "dest");
            var copyExpressions = GetPropertiesToCopy(type).Select(
                p => Expression.Assign(
                    Expression.Property(destParameter, p.Name),
                    Expression.Property(sourceParameter, p.Name)));
            CopyAction = Expression.Lambda<Action<T, T>>(
                Expression.Block(copyExpressions),
                sourceParameter,
                destParameter).Compile();
        }
#pragma warning restore S3963 // "static" fields should be initialized inline

        /// <summary>
        /// Copies properties from <paramref name="source" /> to <paramref name="dest" />.
        /// <para>
        /// Only properties matching the following criteria are copied:
        /// the property is a public instance property,
        /// has getter and setter,
        /// the type is a value type or string.
        /// </para>
        /// <para>
        /// standard behaviour can be the overridden with <see cref="CopyPropertyIgnoreAttribute" /> and
        /// <see cref="CopyPropertyIncludeAttribute" />.
        /// </para>
        /// </summary>
        /// <param name="source">The source entity.</param>
        /// <param name="dest">The target entity.</param>
        public static void Copy(T source, T dest)
        {
            CopyAction(source, dest);
        }

        private static bool PropertyShouldBeCopied(PropertyInfo prop)
        {
            if (!prop.CanRead || !prop.CanWrite)
            {
                return false;
            }

            if (prop.GetCustomAttributes(typeof(CopyPropertyIncludeAttribute), false).Any())
            {
                return true;
            }

            return (prop.PropertyType.IsValueType || prop.PropertyType == typeof(string))
                   && !prop.GetCustomAttributes(typeof(CopyPropertyIgnoreAttribute), false).Any();
        }

        private static IEnumerable<PropertyInfo> GetPropertiesToCopy(Type type)
        {
            return type.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(PropertyShouldBeCopied);
        }
    }
}
