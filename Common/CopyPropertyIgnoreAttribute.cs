using System;

namespace Eurolook.Common
{
    /// <summary>
    /// Denotes that a property should not be copied by <see cref="CopyProperties{T}.Copy" />.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class CopyPropertyIgnoreAttribute : Attribute
    {
    }
}
