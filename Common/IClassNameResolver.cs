namespace Eurolook.Common
{
    public interface IClassNameResolver<T>
    {
        T Resolve(string className);
    }
}
