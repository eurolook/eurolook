using System;
using System.IO;
using System.Reflection;

namespace Eurolook.Common
{
    public static class Directories
    {
        public static DirectoryInfo GetApplicationDataDirectory(bool useRoamingDataDirectory)
        {
            var specialFolder = useRoamingDataDirectory
                ? Environment.SpecialFolder.ApplicationData
                : Environment.SpecialFolder.LocalApplicationData;

            string appDataDir = Environment.GetFolderPath(specialFolder);
            return new DirectoryInfo(Path.Combine(appDataDir, "DIaLOGIKa", "Eurolook"));
        }

        public static DirectoryInfo GetProgramDirectory()
        {
            var dir = Directory.GetParent(Assembly.GetAssembly(typeof(Directories)).Location);
            return new DirectoryInfo(dir.FullName);
        }
    }
}
