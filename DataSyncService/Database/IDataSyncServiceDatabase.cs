﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eurolook.Data.ExceptionLog;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;
using Eurolook.DataSyncService.Services;

namespace Eurolook.DataSyncService.Database
{
    public interface IDataSyncServiceDatabase
    {
        Task<User> GetUserProfileForSyncAsync(string login);

        Task<User> GetUser(string login);

        Task<Template> GetTemplate(Guid id);

        UserSettings UpdateUserSettings(UserSettings settings);

        Author UpdateAuthor(Author author);

        void AddActionLogs(ActionLog[] actionLogs);

        Task WriteServerPerformanceLogAsync(
            PerformanceLogEvent logEvent,
            TimeSpan time,
            string deviceName,
            ISettingsService settingsService);

        Task AddClientPerformanceLogs(
            Dictionary<DateTime, List<Data.Models.PerformanceLog>> performanceLogs,
            PerformanceLogEvent logEvent,
            ISettingsService settingsService);

        void AddOrUpdateExceptionBuckets(ExceptionBucket[] exceptionBuckets);

        void AddExceptionLogEntries(ExceptionLogEntry[] exceptionLogEntries);

        DeviceSettings AddOrUpdateDeviceSettings(DeviceSettings deviceSettings, ModificationType modificationType);

        BrickSettings AddOrUpdateBrickSettings(BrickSettings brickSettings, ModificationType modificationType);

        JobFunction AddOrUpdateJobFunction(JobFunction jobFunction, ModificationType modificationType);

        DocumentModelSettings AddOrUpdateDocumentModelSettings(DocumentModelSettings docSettings, ModificationType modificationType);

        Workplace AddOrUpdateWorkplace(Workplace workplace, ModificationType modificationType);

        JobAssignment AddOrUpdateJobAssignment(JobAssignment jobAssignment, ModificationType modificationType);

        UserAuthor AddOrUpdateUserAuthor(UserAuthor userAuthor, ModificationType modificationType);

        UserTemplate AddOrUpdateUserTemplate(UserTemplate userTemplate, ModificationType modificationType);

        UserBrick AddOrUpdateUserBrick(UserBrick userBrick, ModificationType modificationType);

        BuildingBlockBrick AddOrUpdateBuildingBlockBrick(BuildingBlockBrick buildingBlockBrick, ModificationType modificationType);

        Task<UpdatePlan> GetUpdatesAsync(UpdateRequest request);

        void ConfirmRemoteWipe(string login, string deviceName);

        void AddActivityTracks(ActivityTrack[] activityTracks);

        Task ApplyDeviceInfo(string login, DeviceInfo deviceInfo, string clientVersion);

        bool UserExists(string login);
    }
}
