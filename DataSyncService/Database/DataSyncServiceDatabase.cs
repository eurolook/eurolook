﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data;
using Eurolook.Data.ExceptionLog;
using Eurolook.Data.Models;
using Eurolook.Data.Models.AuthorRoles;
using Eurolook.Data.Models.Metadata;
using Eurolook.Data.Models.SharePointTermStore;
using Eurolook.Data.Models.TemplateStore;
using Eurolook.DataSyncService.Services;
using LinqKit;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Eurolook.DataSyncService.Database
{
    public class DataSyncServiceDatabase : EurolookServerDatabase, IDataSyncServiceDatabase
    {
        public DataSyncServiceDatabase(IConfiguration configuration)
            : base(configuration.GetConnectionString("ServerDatabaseConnection"))
        {
        }

        // *********************************************************************
        //                                            USER PROFILE SYNC METHODS
        // *********************************************************************
        public async Task<User> GetUserProfileForSyncAsync(string login)
        {
            using (var context = GetContext())
            {
                var user = await context.Users
                                        .Include(x => x.Settings)
                                        .Include(x => x.Self)
                                        .FirstOrDefaultAsync(x => x.Login == login && !x.Deleted);

                if (user == null)
                {
                    return null;
                }

                // Load further reference (for performance reasons in separate queries)
                user.Settings.DocumentModelSettings = await context.DocumentModelSettings
                                                                   .Include(x => x.Template)
                                                                   .Where(x => x.UserSettingsId == user.SettingsId)
                                                                   .ToListAsync();
                user.Settings.DeviceSettings = await context.DeviceSettings.Where(x => x.UserSettingsId == user.SettingsId).ToListAsync();
                user.Settings.BrickSettings = await context.BrickSettings.Where(x => x.UserSettingsId == user.SettingsId).ToListAsync();

                user.Authors = new HashSet<UserAuthor>(
                    await context.UserAuthors.Where(x => x.UserId == user.Id).ToArrayAsync());
                user.Superiors = new HashSet<UserSuperior>(
                    await context.UserSuperiors.Where(x => x.UserId == user.Id).ToArrayAsync());
                user.Templates = new HashSet<UserTemplate>(
                    await context.UserTemplates.Where(x => x.UserId == user.Id).ToArrayAsync());
                user.Bricks = new HashSet<UserBrick>(
                    await context.UserBricks.Where(x => x.UserId == user.Id).ToArrayAsync());
                foreach (var userBrick in user.Bricks)
                {
                    userBrick.Brick = await context.Bricks.OfType<BuildingBlockBrick>().FirstOrDefaultAsync(x => x.Id == userBrick.BrickId);
                }

                LoadAuthorData(user.Self, context);

                // Load the favourites and superiors via separate queries.
                foreach (var userAuthor in user.Authors)
                {
                    //if (!userAuthor.Deleted)
                    //{
                        userAuthor.Author = context.Authors.FirstOrDefault(x => x.Id == userAuthor.AuthorId);
                        if (userAuthor.Author != null)
                        {
                            LoadAuthorData(userAuthor.Author, context);
                        }
                    //}
                    //else
                    //{
                    //    userAuthor.AuthorId = null;
                    //    userAuthor.Author = null;
                    //}
                }

                foreach (var userSuperior in user.Superiors)
                {
                    //if (!userSuperior.Deleted)
                    //{
                        userSuperior.Author = context.Authors.FirstOrDefault(x => x.Id == userSuperior.AuthorId);
                        if (userSuperior.Author != null)
                        {
                            LoadAuthorData(userSuperior.Author, context);
                        }
                    //}
                    //else
                    //{
                    //    userSuperior.AuthorId = null;
                    //    userSuperior.Author = null;
                    //}
                }

                foreach (var userTemplate in user.Templates)
                {
                    await LoadTemplateData(userTemplate, context);
                }

                return user;
            }
        }

        public async Task<User> GetUser(string login)
        {
            using var context = GetContext();
            return await context.Users.FirstOrDefaultAsync(x => x.Login == login && !x.Deleted);
        }

        private async Task LoadTemplateData(UserTemplate userTemplate, EurolookServerContext context)
        {
            userTemplate.Template = await GetTemplate(context, userTemplate.TemplateId);

            // remove unnecessary template bytes to reduce package size
            var files = userTemplate.Template?.TemplateFiles;
            if (files != null && (userTemplate.Deleted || !userTemplate.IsOfflineAvailable))
            {
                foreach (var templateFile in files)
                {
                    templateFile.Bytes = null;
                }
            }
        }

        public async Task<Template> GetTemplate(Guid id)
        {
            using (var context = GetContext())
            {
                return await GetTemplate(context, id);
            }
        }

        private static async Task<Template> GetTemplate(EurolookServerContext context, Guid id)
        {
            // Don't send TemplateOwner, it references a UserId that is not present in the client db.
            return await context.Templates
                                .Include(x => x.Publications)
                                .Include(x => x.TemplateFiles)
                                .FirstOrDefaultAsync(x => x.Id == id);
        }

        private static void LoadAuthorData(Author author, EurolookServerContext context)
        {
            author.Functions = context.JobFunctions.Where(x => x.AuthorId == author.Id).ToHashSet();
            author.Workplaces = context.Workplaces.Where(x => x.AuthorId == author.Id).ToHashSet();
            author.JobAssignments = context.JobAssignments
                                           .Where(x => x.AuthorId == author.Id)
                                           .Include(x => x.Functions).ToHashSet();
        }

        public UserSettings UpdateUserSettings(UserSettings settings)
        {
            return UpdateUserSettings(settings, ModificationType.ServerModification);
        }

        public Author UpdateAuthor(Author author)
        {
            return UpdateAuthor(author, ModificationType.ServerModification);
        }

        public void AddActionLogs(ActionLog[] actionLogs)
        {
            using (var context = GetContext())
            {
                foreach (var al in actionLogs)
                {
                    context.Database.ExecuteSqlRaw(
                        "INSERT INTO ActionLogs(Action, OriginatorId, DocumentModelId, Info, CreatedUtc) "
                        + "VALUES (@Action, @OriginatorId, @DocumentModelId, @Info, @CreatedUtc);",
                        new SqlParameter("Action", al.Action ?? ""),
                        new SqlParameter("OriginatorId", al.OriginatorId),
                        new SqlParameter("DocumentModelId", al.DocumentModelId),
                        new SqlParameter("Info", al.Info),
                        new SqlParameter("CreatedUtc", al.CreatedUtc));
                }
            }
        }

        public async Task WriteServerPerformanceLogAsync(
            PerformanceLogEvent logEvent,
            TimeSpan time,
            string deviceName,
            ISettingsService settingsService)
        {
            var today = DateTime.Now.ToDateType(DateTimeExtensions.DateGroupType.Day);
            var version = GetVersion();
            using (var context = GetContext())
            {
                var serverLog = await context.PerformanceLogs.FirstOrDefaultAsync(
                                                  x => x.Event == logEvent.ToString() &&
                                                       x.Date == today &&
                                                       x.DeviceName == deviceName &&
                                                       x.Version == version);
                if (serverLog == null)
                {
                    serverLog = new Data.Models.PerformanceLog
                    {
                        Event = logEvent.ToString(),
                        Date = today,
                        DeviceName = deviceName,
                        Version = version,
                    };
                    await context.PerformanceLogs.AddAsync(serverLog);
                }

                SetPerformanceLogData(serverLog, time, settingsService);
                await context.SaveChangesAsync();
            }
        }

        private string GetVersion()
        {
            var assembly = GetType().Assembly;
            var fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
            return fileVersionInfo.FileVersion;
        }

        public async Task AddClientPerformanceLogs(
            Dictionary<DateTime, List<Data.Models.PerformanceLog>> performanceLogs,
            PerformanceLogEvent logEvent,
            ISettingsService settingsService)
        {
            if (!performanceLogs.Any())
            {
                return;
            }

            using (var context = GetContext())
            {
                foreach (var date in performanceLogs.Keys)
                {
                    foreach (var log in performanceLogs[date])
                    {
                        var serverLog = await context.PerformanceLogs.FirstOrDefaultAsync(
                            x => x.Event == logEvent.ToString() &&
                                 x.Date == date &&
                                 x.DeviceSettingsId == log.DeviceSettingsId &&
                                 x.Version == log.Version);
                        if (serverLog == null)
                        {
                            serverLog = new Data.Models.PerformanceLog
                            {
                                Event = logEvent.ToString(),
                                Date = date,
                                DeviceSettingsId = log.DeviceSettingsId,
                                Version = log.Version,
                            };
                            await context.PerformanceLogs.AddAsync(serverLog);
                        }

                        serverLog.DeviceName = log.DeviceName;
                        SetPerformanceLogData(serverLog, log.Total, settingsService);
                        await context.SaveChangesAsync();
                    }
                }
            }
        }

        private void SetPerformanceLogData(Data.Models.PerformanceLog serverLog, TimeSpan time, ISettingsService settingsService)
        {
            var maxTime = new TimeSpan(23, 59, 59);
            if (time > maxTime)
            {
                this.LogWarn($"Given time exceeds max value ({time}), using {maxTime} instead ({serverLog.DeviceName}, Log ID {serverLog.Id}).");
                time = maxTime;
            }

            if (serverLog.Min.Ticks == 0 || time < serverLog.Min)
            {
                serverLog.Min = time;
            }

            if (time > serverLog.Max)
            {
                serverLog.Max = time;
            }

            var newTotal = serverLog.Total + time;
            if (newTotal <= maxTime)
            {
                serverLog.Total = newTotal;
                serverLog.Count += 1;
                serverLog.Average = new TimeSpan(serverLog.Total.Ticks / serverLog.Count);
            }
            else
            {
                this.LogWarn($"Average time not updated, Total time exceeds max value ({serverLog.DeviceName}, Log ID {serverLog.Id}).");
            }

            if (settingsService.IsPerformanceTrackEnabled)
            {
                var track = ParseTrack(serverLog.Track, ';');
                if (track.Count >= settingsService.PerformanceTrackSize)
                {
                    track.RemoveAt(0);
                }

                track.Add(time);
                serverLog.Track = SaveTrack(track, ';');
                serverLog.Median = GetMedian(track);
            }
        }

        public void AddOrUpdateExceptionBuckets(ExceptionBucket[] exceptionBuckets)
        {
            using (var context = GetContext())
            {
                foreach (var exceptionBucket in exceptionBuckets)
                {
                    var existingBucket = context.ExceptionBuckets.FirstOrDefault(b => b.Id == exceptionBucket.Id);
                    if (existingBucket != null)
                    {
                        // update existing error bucket which allows us to query all recent buckets easily
                        if (existingBucket.ModifiedUtc < exceptionBucket.ModifiedUtc)
                        {
                            existingBucket.ModifiedUtc = exceptionBucket.ModifiedUtc;
                        }
                    }
                    else
                    {
                        context.ExceptionBuckets.Add(exceptionBucket);
                    }
                }

                context.SaveChanges();
            }
        }

        public void AddExceptionLogEntries(ExceptionLogEntry[] exceptionLogEntries)
        {
            using (var context = GetContext())
            {
                foreach (var ex in exceptionLogEntries)
                {
                    context.Database.ExecuteSqlRaw(
                        "INSERT INTO ExceptionLogEntries(CreatedUtc, ExceptionBuckedId, OriginatorId, Message, ClientVersion, OfficeVersion) "
                        + "VALUES (@createdUtc, @exceptionBuckedId, @originatorId, @message, @clientVersion, @officeVersion);",
                        new SqlParameter("createdUtc", ex.CreatedUtc),
                        new SqlParameter("exceptionBuckedId", ex.ExceptionBuckedId),
                        new SqlParameter("originatorId", ex.OriginatorId),
                        new SqlParameter("message", ex.Message ?? ""),
                        new SqlParameter("clientVersion", ex.ClientVersion ?? ""),
                        new SqlParameter("officeVersion", ex.OfficeVersion ?? ""));
                }
            }
        }

        public DeviceSettings AddOrUpdateDeviceSettings(DeviceSettings deviceSettings, ModificationType modificationType)
        {
            var context = GetContext();
            var result = AddOrUpdateDeviceSettings(context, deviceSettings, modificationType);
            context.SaveChanges();
            return result;
        }

        private DeviceSettings AddOrUpdateDeviceSettings(EurolookContext context, DeviceSettings deviceSettings, ModificationType modificationType)
        {
            var result = context.DeviceSettings.Count(ds => ds.Id == deviceSettings.Id) > 0
                ? Update(context.DeviceSettings, deviceSettings, modificationType)
                : Add(context.DeviceSettings, deviceSettings, modificationType);
            return result;
        }

        public BrickSettings AddOrUpdateBrickSettings(BrickSettings brickSettings, ModificationType modificationType)
        {
            using (var context = GetContext())
            {
                var result = context.BrickSettings.Count(ds => ds.Id == brickSettings.Id) > 0
                    ? Update(context.BrickSettings, brickSettings, modificationType)
                    : Add(context.BrickSettings, brickSettings, modificationType);
                context.SaveChanges();
                return result;
            }
        }

        public JobFunction AddOrUpdateJobFunction(JobFunction jobFunction, ModificationType modificationType)
        {
            using (var context = GetContext())
            {
                var result = AddOrUpdateJobFunction(context, jobFunction, modificationType);
                context.SaveChanges();
                return result;
            }
        }

        public DocumentModelSettings AddOrUpdateDocumentModelSettings(DocumentModelSettings docSettings, ModificationType modificationType)
        {
            var context = GetContext();
            var result = context.DocumentModelSettings.Count(dms => dms.Id == docSettings.Id) > 0
                ? Update(context.DocumentModelSettings, docSettings, modificationType)
                : Add(context.DocumentModelSettings, docSettings, modificationType);

            context.SaveChanges();

            return result;
        }

        public Workplace AddOrUpdateWorkplace(Workplace workplace, ModificationType modificationType)
        {
            using (var context = GetContext())
            {
                var result = context.Workplaces.Any(wp => wp.Id == workplace.Id)
                                ? Update(context.Workplaces, workplace, modificationType)
                                : Add(context.Workplaces, workplace, modificationType);
                context.SaveChanges();
                return result;
            }
        }

        public JobAssignment AddOrUpdateJobAssignment(JobAssignment jobAssignment, ModificationType modificationType)
        {
            using (var context = GetContext())
            {
                var result = context.JobAssignments.Count(j => j.Id == jobAssignment.Id) > 0
                ? Update(context.JobAssignments, jobAssignment, modificationType)
                : Add(context.JobAssignments, jobAssignment, modificationType);
                context.SaveChanges();
                return result;
            }
        }

        public UserAuthor AddOrUpdateUserAuthor(UserAuthor userAuthor, ModificationType modificationType)
        {
            using (var context = GetContext())
            {
                var result = context.UserAuthors.Any(ua => ua.Id == userAuthor.Id)
                    ? Update(context.UserAuthors, userAuthor, modificationType)
                    : Add(context.UserAuthors, userAuthor, modificationType);
                context.SaveChanges();
                return result;
            }
        }

        public UserTemplate AddOrUpdateUserTemplate(UserTemplate userTemplate, ModificationType modificationType)
        {
            using (var context = GetContext())
            {
                var result = context.UserTemplates.Any(a => a.Id == userTemplate.Id)
                    ? Update(context.UserTemplates, userTemplate, modificationType)
                    : Add(context.UserTemplates, userTemplate, modificationType);
                context.SaveChanges();
                return result;
            }
        }

        public UserBrick AddOrUpdateUserBrick(UserBrick userBrick, ModificationType modificationType)
        {
            using (var context = GetContext())
            {
                var userBrickClone = ShallowClone.Clone(userBrick);
                var result = context.UserBricks.Any(x => x.Id == userBrickClone.Id)
                    ? Update(context.UserBricks, userBrickClone, modificationType)
                    : Add(context.UserBricks, userBrickClone, modificationType);
                context.SaveChanges();
                return result;
            }
        }

        public BuildingBlockBrick AddOrUpdateBuildingBlockBrick(BuildingBlockBrick buildingBlockBrick, ModificationType modificationType)
        {
            using (var context = GetContext())
            {
                var result = context.Bricks.OfType<BuildingBlockBrick>().Any(x => x.Id == buildingBlockBrick.Id)
                    ? UpdateBuildingBlockBrick(context, buildingBlockBrick, modificationType)
                    : AddBuildingBlockBrick(context, buildingBlockBrick, modificationType);
                context.SaveChanges();
                return result;
            }
        }

        private BuildingBlockBrick AddBuildingBlockBrick(EurolookServerContext context, BuildingBlockBrick buildingBlockBrick, ModificationType modificationType)
        {
            if (buildingBlockBrick == null)
            {
                throw new ArgumentNullException(nameof(buildingBlockBrick), "Cannot add a null object");
            }

            this.LogDebug($"Add<{nameof(BuildingBlockBrick)}>(entity.Id={buildingBlockBrick.Id}, mod={modificationType})");
            var entityEntry = context.Bricks.Add(buildingBlockBrick);
            entityEntry.Entity.SetModification(modificationType);
            return entityEntry.Entity as BuildingBlockBrick;
        }

        private BuildingBlockBrick UpdateBuildingBlockBrick(EurolookContext context, BuildingBlockBrick buildingBlockBrick, ModificationType modificationType)
        {
            if (buildingBlockBrick == null)
            {
                throw new ArgumentNullException(nameof(buildingBlockBrick), "Cannot update with a null object");
            }

            this.LogDebug($"Update<{nameof(BuildingBlockBrick)}>(entity.Id={buildingBlockBrick.Id}, mod={modificationType})");
            var dbEntity = SelectFromLocalOrDb(context.Bricks, buildingBlockBrick.Id) as BuildingBlockBrick;
            if (dbEntity != null)
            {
                UpdateEntity(buildingBlockBrick, dbEntity);
                dbEntity.SetModification(modificationType);
            }

            return dbEntity;
        }

        public JobFunction AddOrUpdateJobFunction(EurolookContext context, JobFunction jobFunction, ModificationType modificationType)
        {
            var result = context.JobFunctions.Count(af => af.Id == jobFunction.Id) > 0
                ? Update(context.JobFunctions, jobFunction, modificationType)
                : Add(context.JobFunctions, jobFunction, modificationType);
            return result;
        }

        public async Task<UpdatePlan> GetUpdatesAsync(UpdateRequest request)
        {
            using (var context = GetContext())
            {
                // Is remote wipe requested?
                if (!string.IsNullOrWhiteSpace(request.Login))
                {
                    var user = await context.Users.FirstOrDefaultAsync(u => u.Login == request.Login);
                    if (user != null)
                    {
                        var deviceSettings = await context.DeviceSettings.FirstOrDefaultAsync(ds => ds.UserSettingsId == user.SettingsId && ds.DeviceName == request.DeviceName);
                        if (deviceSettings != null && deviceSettings.IsRemoteWipeRequested)
                        {
                            return new UpdatePlan
                            {
                                IsRemoteWipeRequested = true,
                            };
                        }
                    }
                }

                if (request.IsInitializationRequest)
                {
                    return await UpdatePlanCache.GetSingleInstance()
                                                .GetUpdatePlanForInitializationRequestAsync(
                                                    () => GetUpdatePlanFromDatabaseAsync(context, request));
                }

                return await GetUpdatePlanFromDatabaseAsync(context, request);
            }
        }

        private async Task<UpdatePlan> GetUpdatePlanFromDatabaseAsync(EurolookServerContext context, UpdateRequest request)
        {
            // Get updates
            var cleanedOrgaEntities = await context.OrgaEntities.Where(GetUpdatesPredicate<OrgaEntity>(request)).ToListAsync();
            foreach (var oe in cleanedOrgaEntities)
            {
                oe.SuperEntity = null;
                oe.SubEntities = null;
            }

            return new UpdatePlan
            {
                ServerTimestampUTC = DateTime.UtcNow,
                Texts = await context.Texts.Where(GetUpdatesPredicate<Text>(request)).ToListAsync(),
                OrgaEntities = cleanedOrgaEntities,
                Translations = await context.Translations.Where(GetUpdatesPredicate<Translation>(request, true)).ToListAsync(),
                Addresses = await context.Addresses.Where(GetUpdatesPredicate<Address>(request)).ToListAsync(),
                BrickGroups = await context.BrickGroups.Where(GetUpdatesPredicate<BrickGroup>(request)).ToListAsync(),
                BrickCategories = await context.BrickCategories.Where(GetUpdatesPredicate<BrickCategory>(request)).ToListAsync(),
                UserGroups = await context.UserGroups.Where(GetUpdatesPredicate<UserGroup>(request)).ToListAsync(),
                ContentBricks = await context.Bricks.OfType<ContentBrick>().Where(GetUpdatesPredicate<ContentBrick>(request).And(x => !(x is DynamicBrick))).ToListAsync(),
                DynamicBricks = await context.Bricks.OfType<DynamicBrick>().Where(GetUpdatesPredicate<DynamicBrick>(request)).ToListAsync(),
                CommandBricks = await context.Bricks.OfType<CommandBrick>().Where(GetUpdatesPredicate<CommandBrick>(request)).ToListAsync(),
                Languages = await context.Languages.Where(GetUpdatesPredicate<Language>(request)).ToListAsync(),
                DocumentCategories = await context.DocumentCategories.Where(GetUpdatesPredicate<DocumentCategory>(request)).ToListAsync(),
                DocumentModels = await context.DocumentModels.Where(GetUpdatesPredicate<DocumentModel>(request)).ToListAsync(),
                DocumentModelLanguages = await context.DocumentModelLanguages.Where(GetUpdatesPredicate<DocumentModelLanguage>(request)).ToListAsync(),
                BrickTexts = await context.BrickTexts.Where(GetUpdatesPredicate<BrickText>(request)).ToListAsync(),
                DocumentStructures = await context.DocumentStructure.Where(GetUpdatesPredicate<DocumentStructure>(request)).ToListAsync(),
                LocalisedResources = await context.LocalisedResources.Where(GetUpdatesPredicate<LocalisedResource>(request)).ToListAsync(),
                Resources = await context.Resources.Where(GetUpdatesPredicate<Resource>(request)).ToListAsync(),
                BrickResources = await context.BrickResources.Where(GetUpdatesPredicate<BrickResource>(request)).ToListAsync(),
                CharacterMappings = await context.CharacterMappings.Where(GetUpdatesPredicate<CharacterMapping>(request)).ToListAsync(),
                PersonNames = await context.PersonNames.Where(GetUpdatesPredicate<PersonName>(request)).ToListAsync(),
                StyleShortcuts = await context.StyleShortcuts.Where(GetUpdatesPredicate<StyleShortcut>(request)).ToListAsync(),
                ColorSchemes = await context.ColorSchemes.Where(GetUpdatesPredicate<ColorScheme>(request)).ToListAsync(),
                TermStoreImports = await context.TermStoreImports.Where(GetUpdatesPredicate<TermStoreImport>(request)).ToListAsync(),
                MetadataCategories = await context.MetadataCategories.Where(GetUpdatesPredicate<MetadataCategory>(request)).ToListAsync(),
                MetadataDefinitions = await context.MetadataDefinitions.Where(GetUpdatesPredicate<MetadataDefinition>(request)).ToListAsync(),
                DocumentModelMetadataDefinitions = await context.DocumentModelMetadataDefinitions.Where(GetUpdatesPredicate<DocumentModelMetadataDefinition>(request)).ToListAsync(),
                AuthorRoles = await context.AuthorRoles.Where(GetUpdatesPredicate<AuthorRole>(request)).ToListAsync(),
                DocumentModelAuthorRoles = await context.DocumentModelAuthorRoles.Where(GetUpdatesPredicate<DocumentModelAuthorRole>(request)).ToListAsync(),
                PredefinedFunctions = await context.PredefinedFunctions.Where(GetUpdatesPredicate<PredefinedFunction>(request)).ToListAsync(),
                Notifications = await context.Notifications.Where(GetUpdatesPredicate<Notification>(request, true)).ToListAsync(),
                SystemConfigurations = await context.SystemConfigurations.Where(GetUpdatesPredicate<SystemConfiguration>(request, true)).ToListAsync(),
            };
        }

        private static Expression<Func<T, bool>> GetUpdatesPredicate<T>(UpdateRequest request, bool ignoreDeletedOnInit = false)
            where T : Updatable
        {
            // get records that are newer than the given date
            Expression<Func<T, bool>> predicate =
                x => x.ServerModificationTimeUtc > request.MinimumServerModificationTimeUTC;

            // if it's an initialization, so skip the deleted records
            if (request.IsInitializationRequest && ignoreDeletedOnInit)
            {
                predicate = predicate.And(x => x.Deleted == false);
            }

            return predicate;
        }

        public void ConfirmRemoteWipe(string login, string deviceName)
        {
            using (var context = GetContext())
            {
                var user = context.Users.First(u => u.Login == login);
                if (user != null)
                {
                    var deviceSettings = context.DeviceSettings.FirstOrDefault(ds => ds.UserSettingsId == user.SettingsId && ds.DeviceName == deviceName);
                    if (deviceSettings != null)
                    {
                        deviceSettings.IsRemoteWipeRequested = false;
                        context.SaveChanges();
                    }
                }
            }
        }

        public void AddActivityTracks(ActivityTrack[] activityTracks)
        {
            using (var context = GetContext())
            {
                foreach (var track in activityTracks)
                {
                    var dbTrack =
                        context.ActivityTracks.FirstOrDefault(
                            u => u.DateUtc == track.DateUtc && u.OrgaEntityId == track.OrgaEntityId);
                    if (dbTrack != null)
                    {
                        dbTrack.SessionCount += track.SessionCount;

                        // Every track is submitted only once a day by each device.
                        // DataSync submits the data for the past days. After submitting, data is deleted in the local database.
                        // Therefore we can simply increase the device count without remembering the device id.
                        // (Remembering the device id would increase the number of records stored in the database dramatically)
                        dbTrack.DeviceCount += 1;
                    }
                    else
                    {
                        var newTrack = new ActivityTrack
                        {
                            OrgaEntityId = track.OrgaEntityId,
                            DateUtc = track.DateUtc,
                            DeviceCount = 1,
                            SessionCount = track.SessionCount,
                        };
                        context.ActivityTracks.Add(newTrack);
                    }
                }

                context.SaveChanges();
            }
        }

        public async Task ApplyDeviceInfo(string login, DeviceInfo deviceInfo, string clientVersion)
        {
            using (var context = GetContext())
            {
                var user = context.Users.Include(u => u.Settings).ThenInclude(x => x.DeviceSettings).First(u => u.Login == login);
                var deviceSettings = user.Settings.DeviceSettings.FirstOrDefault(d => d.DeviceName == deviceInfo.DeviceName);

                if (deviceSettings == null)
                {
                    deviceSettings = new DeviceSettings();
                    deviceSettings.Init();
                    deviceSettings.ClientModification = true;
                    deviceSettings.UserSettingsId = user.SettingsId;
                    deviceSettings.DeviceName = deviceInfo.DeviceName;

                    context.DeviceSettings.Add(deviceSettings);
                }

                deviceSettings.Owner = deviceInfo.DeviceOwner;
                deviceSettings.AddinVersion = clientVersion;
                deviceSettings.OperatingSystem = deviceInfo.OsName;
                deviceSettings.IsTouchScreen = deviceInfo.IsTouchScreen;
                deviceSettings.HasBattery = deviceInfo.HasBattery;
                deviceSettings.ScreenWidth = deviceInfo.ScreenWidth;
                deviceSettings.ScreenHeight = deviceInfo.ScreenHeight;
                deviceSettings.ScreenDpi = deviceInfo.ScreenDpi;
                deviceSettings.PhysicallyInstalledSystemMemoryKb = deviceInfo.PhysicallyInstalledSystemMemoryKb;
                deviceSettings.ProcessorName = deviceInfo.ProcessorName;
                deviceSettings.IsInitialised = false;
                deviceSettings.IsPerUserInstall = deviceInfo.IsPerUserInstall;
                deviceSettings.IsPerMachineInstall = deviceInfo.IsPerMachineInstall;

                // NOTE: MiniMode should not overwrite LastLogin
                await context.SaveChangesAsync();
            }
        }

        private static string SaveTrack(IEnumerable<TimeSpan> track, char separator)
        {
            var result = new StringBuilder();
            foreach (var time in track)
            {
                result.Append($"{time:g}{separator}");
            }

            return result.ToString();
        }

        private static List<TimeSpan> ParseTrack(string track, char separator)
        {
            if (string.IsNullOrEmpty(track))
            {
                return new List<TimeSpan>();
            }

            var result = new List<TimeSpan>();
            foreach (var part in track.Split(new[] { separator }, StringSplitOptions.RemoveEmptyEntries))
            {
                if (TimeSpan.TryParse(part, out var time))
                {
                    result.Add(time);
                }
            }

            return result;
        }

        public static TimeSpan GetMedian(IEnumerable<TimeSpan> track)
        {
            var times = track.ToArray();
            if (times.IsNullOrEmpty())
            {
                return TimeSpan.MinValue;
            }

            Array.Sort(times);
            int count = times.Length;
            if (count % 2 == 0)
            {
                // count is even, average two middle elements
                var a = times[count / 2 - 1];
                var b = times[count / 2];
                var c = (a.Ticks + b.Ticks) / 2;
                return new TimeSpan(c);
            }

            // count is odd, return the middle element
            return times[count / 2];
        }
    }
}
