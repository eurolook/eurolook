﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace Eurolook.DataSyncService
{
    public static class Extensions
    {
        public static StatusCodeResult CreateInternalServerError(
            this ControllerBase controller,
            Exception ex,
            string message = null)
        {
            var request = controller.Request;

            string error = string.IsNullOrEmpty(message) ? ex.ToString() : $"{message}\n{ex}";
            Log.Error(error);

            return new StatusCodeResult(StatusCodes.Status500InternalServerError);
        }

        public static string GetClientIp(this HttpRequest request)
        {
            var remoteIpAddress = request.HttpContext.Connection.RemoteIpAddress;
            return remoteIpAddress?.ToString();
        }

        public static string GetRequestIdPrefix(this HttpRequest request)
        {
            // TODO: Add correlationID middleware
            return $"#{request.HttpContext.TraceIdentifier}#";
        }
    }
}
