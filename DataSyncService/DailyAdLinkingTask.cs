﻿using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Eurolook.ActiveDirectoryLink;
using Eurolook.Web.Common.ScheduledTask;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace Eurolook.DataSyncService
{
    public class DailyAdLinkingTask : AbstractScheduledTask
    {
        private readonly IConfigurationSection _configurationSection;
        private readonly ILifetimeScope _lifetimeScope;

        public DailyAdLinkingTask(IConfiguration configuration, ILifetimeScope lifetimeScope)
            : base(nameof(DailyAdLinkingTask))
        {
            _configurationSection = configuration.GetSection(ScheduledTaskName);
            _lifetimeScope = lifetimeScope;
        }

        protected override void SetConfigurationValues()
        {
            Hour = _configurationSection.GetValue<int>("Hour");
            Minute = _configurationSection.GetValue<int>("Minute");
            IsActive = _configurationSection.GetValue("IsActive", false);
        }

        protected override async Task RunTask()
        {
            var userNames = await RetrieveAllUserNames();
            await LinkUsers(userNames);
        }

        private async Task<string[]> RetrieveAllUserNames()
        {
            await using var scope = _lifetimeScope.BeginLifetimeScope();
            var activeDirectorySearcher = scope.Resolve<IActiveDirectorySearcher>();
            return activeDirectorySearcher.RetrieveAllUsers().ToArray();
        }

        private async Task LinkUsers(string[] userNames)
        {
            await using var scope = _lifetimeScope.BeginLifetimeScope();
            var activeDirectoryLinker = scope.Resolve<IActiveDirectoryLinker>();
            activeDirectoryLinker.PrepareLinking();

            int updatedUsers = userNames.Count(userName =>
            {
                activeDirectoryLinker.CreateAndLinkUser(userName, out var hasBeenUpdated, true);
                return hasBeenUpdated;
            });

            Log.Information($"{ScheduledTaskName} updated {updatedUsers} out of {userNames.Length} user(s).");
        }
    }
}
