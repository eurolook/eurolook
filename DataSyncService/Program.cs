﻿using System;
using System.IO;
using System.Net;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Eurolook.ActiveDirectoryLink.AutofacModules;
using Eurolook.Common;
using Eurolook.Common.Extensions;
using Eurolook.Data;
using Eurolook.Data.Database;
using Eurolook.DataSyncService;
using Eurolook.DataSyncService.Database;
using Eurolook.DataSyncService.PerformanceLog;
using Eurolook.DataSyncService.Services;
using Eurolook.Web.Common;
using Eurolook.Web.Common.ScheduledTask;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Serilog;

try
{
    var builder = WebApplication.CreateBuilder(args);

    builder.AddDefaultConfigurationProviders();
    builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory());
    builder.Host.UseSerilog();

    builder.AddMigrationCommandLineOption();

    builder.Services.AddControllers()
           // Newtonsoft.Json is added for compatibility reasons
           // The recommended approach is to use System.Text.Json for serialization
           // Visit the following link for more guidance about moving away from Newtonsoft.Json to System.Text.Json
           // https://docs.microsoft.com/dotnet/standard/serialization/system-text-json-migrate-from-newtonsoft-how-to
           .AddNewtonsoftJson(
               options =>
               {
                   options.UseMemberCasing();
               });

    builder.Services.AddAuthentication(
        x =>
        {
            x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        }).AddJwtBearer(
        o =>
        {
            var key = WebEncoders.Base64UrlDecode(builder.Configuration["JWT:Key"]);
            o.SaveToken = true;
            o.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                ValidIssuer = builder.Configuration["JWT:Issuer"],
                ValidAudience = builder.Configuration["JWT:Audience"],
                IssuerSigningKey = new SymmetricSecurityKey(key),
            };
        });

    // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen(
               options =>
               {
                   options.SwaggerDoc(
                       "v1",
                       new OpenApiInfo
                       {
                           Title = "Eurolook DataSync API",
                           Version = "v2",
                       });
                   options.AddSecurityDefinition(
                       "Bearer",
                       new OpenApiSecurityScheme()
                       {
                           Name = "Authorization",
                           Type = SecuritySchemeType.ApiKey,
                           Scheme = "Bearer",
                           BearerFormat = "JWT",
                           In = ParameterLocation.Header,
                           Description =
                               "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 1safsfsdfdfd\"",
                       });
                   options.AddSecurityRequirement(new OpenApiSecurityRequirement
                   {
                       {
                           new OpenApiSecurityScheme
                           {
                               Reference = new OpenApiReference
                               {
                                   Type = ReferenceType.SecurityScheme,
                                   Id = "Bearer",
                               },
                           },
                           Array.Empty<string>()
                       },
                   });
               })
           .AddSwaggerGenNewtonsoftSupport();

    string connectionString = builder.Configuration.GetConnectionString("ServerDatabaseConnection");

    builder.Services.AddHealthChecks().AddSqlServer(connectionString);
    builder.Services.AddHealthChecks().AddSystemInfo(builder);

    builder.Host.ConfigureContainer<ContainerBuilder>(
        containerBuilder =>
        {
            containerBuilder.RegisterType<PerformanceLogService>().AsImplementedInterfaces();
            containerBuilder.RegisterModule<ActiveDirectoryLinkModule>();
            containerBuilder.RegisterType<DataSyncServiceDatabase>().AsImplementedInterfaces();
            containerBuilder.RegisterType<SharedTemplateRepository>().AsImplementedInterfaces();
            containerBuilder.Register(_ => new EurolookServerContext(connectionString))
                            .As<EurolookContext>()
                            .As<EurolookServerContext>()
                            .ExternallyOwned();

            var pluginLoader = new PluginLoader(AppContext.BaseDirectory, builder.Configuration.GetSection("Plugins").Get<string[]>());
            pluginLoader.RegisterSingleMandatoryPluginModule<IActiveDirectoryLinkModule>(containerBuilder);

            containerBuilder.RegisterType<SettingsService>().AsImplementedInterfaces();
            containerBuilder.RegisterType<AuthorSearchRepository>().AsImplementedInterfaces();

            if (builder.Configuration["FeatureManagement:DailyAdLinkingTask"].ToBool())
            {
                containerBuilder.RegisterType<DailyAdLinkingTask>().AsImplementedInterfaces().SingleInstance();
            }

            if (builder.Configuration["FeatureManagement:DailyAdCleanupTask"].ToBool())
            {
                containerBuilder.RegisterType<DailyAdCleanupTask>().AsImplementedInterfaces().SingleInstance();
            }

            if (builder.Configuration["FeatureManagement:KeepAliveTask"].ToBool())
            {
                containerBuilder.RegisterType<KeepAliveTask>().AsImplementedInterfaces().SingleInstance();
            }
        });

    var app = builder.Build();
    app.UseHttpsRedirection();

    // Configure the HTTP request pipeline.
    if (app.Environment.IsLocalDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
        // app.UseDeveloperExceptionPage();
    }

    app.CreateAndSetupLogger(typeof(Program).Assembly);

    app.UseRouting();
    app.UseAuthentication();
    app.UseAuthorization();

    app.UseStaticFiles();
    app.UseStaticFiles(new StaticFileOptions
    {
        FileProvider = new PhysicalFileProvider(Path.Combine(app.Environment.ContentRootPath, "Help")),
        RequestPath = "/Help",
        OnPrepareResponse = ctx =>
        {
            var headers = ctx.Context.Response.Headers;
            headers.CacheControl = "no-cache, no-store, must-revalidate";
            headers.Pragma = "no-cache";
            headers.Expires = "0";
        }
    });

    // add a redirect for non-existing help files
    app.Use(
        async (context, next) =>
        {
            await next();

            if (context.Response.StatusCode == (int)HttpStatusCode.NotFound
                && context.Request.Path.StartsWithSegments("/Help"))
            {
                context.Response.Redirect("/Help/Content/Home.htm");
            }
        });

    app.UseStaticFiles(new StaticFileOptions
    {
        FileProvider = new PhysicalFileProvider(Path.Combine(app.Environment.ContentRootPath, "QuickStartGuide")),
        RequestPath = "/QuickStartGuide",
    });

    app.MapControllers();

    app.UseEndpoints(endpoints =>
    {
        endpoints.MapHealthChecksWithAnonymousAccess("/health");
    });

    app.StartRegisteredScheduledTasks();
    app.Run();
}
catch (Exception ex)
{
    Log.Fatal(ex, "Host terminated unexpectedly");
}
finally
{
    Log.Information("Shutdown complete");
    Log.CloseAndFlush();
}
