﻿using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Eurolook.ActiveDirectoryLink;
using Eurolook.Web.Common.ScheduledTask;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace Eurolook.DataSyncService
{
    public class DailyAdCleanupTask : AbstractScheduledTask
    {
        private readonly IConfigurationSection _configurationSection;
        private readonly ILifetimeScope _lifetimeScope;

        public DailyAdCleanupTask(IConfiguration configuration, ILifetimeScope lifetimeScope)
            : base(nameof(DailyAdCleanupTask))
        {
            _configurationSection = configuration.GetSection(ScheduledTaskName);
            _lifetimeScope = lifetimeScope;
        }

        protected override void SetConfigurationValues()
        {
            Hour = _configurationSection.GetValue<int>("Hour");
            Minute = _configurationSection.GetValue<int>("Minute");
            IsActive = _configurationSection.GetValue("IsActive", false);
        }

        protected override async Task RunTask()
        {
            var userNames = await RetrieveRetiredUserNames();
            await CleanRetiredUsers(userNames);
        }

        private async Task<string[]> RetrieveRetiredUserNames()
        {
            await using var scope = _lifetimeScope.BeginLifetimeScope();
            var activeDirectorySearcher = scope.Resolve<IActiveDirectorySearcher>();
            return activeDirectorySearcher.RetrieveRetiredUsers().ToArray();
        }

        private async Task CleanRetiredUsers(string[] userNames)
        {
            await using var scope = _lifetimeScope.BeginLifetimeScope();
            var cleaner = scope.Resolve<ICleaner>();
            foreach (string userName in userNames)
            {
                cleaner.CleanUser(userName, true);
            }

            Log.Information($"{ScheduledTaskName} cleaned {userNames.Length} user(s).");
        }
    }
}
