﻿using System;
using System.Collections.Generic;
using Eurolook.Common.Log;
using Eurolook.Data;
using Eurolook.Data.Database;
using Eurolook.Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.DataSyncService.Controllers.v2
{
    [Authorize]
    [Route("api/v2/search/[action]")]
    [ApiController]
    public class SearchController : ControllerBase, ICanLog
    {
        private readonly IAuthorSearchRepository _authorSearchRepository;

        public SearchController(IAuthorSearchRepository authorSearchRepository)
        {
            _authorSearchRepository = authorSearchRepository;
        }

        [HttpGet]
        [ActionName("authors")]
        public ActionResult<IEnumerable<Author>> SearchAuthors(
            [FromQuery] string q,
            [FromQuery] uint page = 0,
            [FromQuery] uint pageSize = 20)
        {
            string query = q ?? "";

            try
            {
                var queries = query.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                var paginationInfo = new PaginationInfo
                {
                    From = page * pageSize,
                    PageSize = pageSize,
                };

                var result = _authorSearchRepository.SearchAuthors(queries, paginationInfo);
                return Ok(result);
            }
            catch (Exception ex)
            {
                this.LogError("SearchController.SearchAuthors failed.", ex);
                return this.CreateInternalServerError(ex);
            }
        }
    }
}
