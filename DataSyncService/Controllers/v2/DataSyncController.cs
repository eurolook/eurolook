﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eurolook.ActiveDirectoryLink;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Data.ExceptionLog;
using Eurolook.Data.Models;
using Eurolook.Data.Models.TemplateStore;
using Eurolook.DataSyncService.Database;
using Eurolook.DataSyncService.PerformanceLog;
using Eurolook.DataSyncService.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Eurolook.DataSyncService.Controllers.v2;

[Authorize]
[Route("api/v2/[controller]/[action]")]
[ApiController]
[DisableRequestSizeLimit]
[SuppressMessage("csharpsquid", "S5693:Make sure the content length limit is safe here.", Justification = "API endpoint is internal only.")]
public class DataSyncController : ControllerBase, ICanLog
{
    private readonly IActiveDirectoryLinker _activeDirectoryLinker;
    private readonly ISettingsService _settingsService;
    private readonly IPerformanceLogService _performanceLogService;
    private readonly IDataSyncServiceDatabase _dataSyncServiceDatabase;

    public DataSyncController(
        IActiveDirectoryLinker activeDirectoryLinker,
        ISettingsService settingsService,
        IPerformanceLogService performanceLogService,
        IDataSyncServiceDatabase dataSyncServiceDatabase)
    {
        _activeDirectoryLinker = activeDirectoryLinker;
        _settingsService = settingsService;
        _performanceLogService = performanceLogService;
        _dataSyncServiceDatabase = dataSyncServiceDatabase;
    }

    [HttpPost]
    [ActionName("updates")]
    public async Task<ActionResult<UpdatePlan>> GetUpdates(
        [FromBody] UpdateRequest request,
        [FromQuery] string clientVersion = null)
    {
        try
        {
            var sw = new Stopwatch();
            sw.Start();
            if (request == null)
            {
                // WebApi fails to read a chunked request automatically.
                // -> try to read the body manually
                using (var reader = new StreamReader(Request.Body, Encoding.UTF8, true, 1024, true))
                {
                    string content = await reader.ReadToEndAsync();
                    if (!string.IsNullOrWhiteSpace(content))
                    {
                        request = JsonConvert.DeserializeObject<UpdateRequest>(content);
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(request?.ISOMinimumServerModificationTimeUTC))
            {
                return null;
            }

            this.LogTrace(
                $"{Request.GetRequestIdPrefix()} Update request with MinimumServerModificationTimeUTC: {request.ISOMinimumServerModificationTimeUTC}");
            var result = await _dataSyncServiceDatabase.GetUpdatesAsync(request);
            sw.Stop();

            if (_settingsService.IsPerformanceLoggingEnabled)
            {
                if (request.IsInitializationRequest)
                {
                    await _performanceLogService.LogGetInitialization(sw.ElapsedMilliseconds);
                }
                else
                {
                    await _performanceLogService.LogGetUpdates(sw.ElapsedMilliseconds);
                }
            }

            return Ok(result);
        }
        catch (Exception ex)
        {
            return this.CreateInternalServerError(ex, $"{nameof(GetUpdates)} failed.");
        }
    }

    //[Microsoft.AspNetCore.Mvc.HttpPost]
    //[Microsoft.AspNetCore.Mvc.ActionName("error")]
    //public Task<UpdatePlan> GetError([Microsoft.AspNetCore.Mvc.FromBody] UpdateRequest request, [FromQuery] string clientVersion = null)
    //{
    //    var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
    //    throw new HttpResponseException(response);
    //}

    [HttpGet]
    [ActionName("userprofile")]
    public async Task<ActionResult<User>> GetUserProfile([FromQuery] string login, [FromQuery] string clientVersion = null)
    {
        try
        {
            var sw = new Stopwatch();
            sw.Start();
            var userProfile = await _dataSyncServiceDatabase.GetUserProfileForSyncAsync(login);
            if (userProfile == null)
            {
                var migratedUserName = _activeDirectoryLinker.GetMigratedUserName(login);
                if (migratedUserName != null)
                {
                    userProfile = await _dataSyncServiceDatabase.GetUserProfileForSyncAsync(migratedUserName);
                }
            }

            sw.Stop();
            if (_settingsService.IsPerformanceLoggingEnabled)
            {
                await _performanceLogService.LogGetUserProfile(sw.ElapsedMilliseconds);
            }

            return Ok(userProfile);
        }
        catch (Exception ex)
        {
            return this.CreateInternalServerError(ex, $"{nameof(GetUserProfile)} failed.");
        }
    }

    [HttpGet("{id:guid}")]
    [ActionName("template")]
    public async Task<ActionResult<Template>> GetTemplate(Guid id, [FromQuery] string clientVersion = null)
    {
        try
        {
            return await _dataSyncServiceDatabase.GetTemplate(id);
        }
        catch (Exception ex)
        {
            return this.CreateInternalServerError(ex, $"{nameof(GetTemplate)} failed.");
        }
    }

    [HttpPost]
    [ActionName("user")]
    public async Task<ActionResult<User>> CreateUser([FromBody] User user, [FromQuery] string clientVersion = null)
    {
        if (user == null)
        {
            return this.CreateInternalServerError(new Exception("Parameter 'user' is required."));
        }

        // CreateAndLinkUser calls CreateOrReviveUserProfile
        if (_activeDirectoryLinker.CreateAndLinkUser(user.Login))
        {
            try
            {
                return await _dataSyncServiceDatabase.GetUserProfileForSyncAsync(user.Login);
            }
            catch (Exception ex)
            {
                return this.CreateInternalServerError(ex, $"{nameof(CreateUser)} failed for user {user.Login}");
            }
        }

        return this.CreateInternalServerError(new Exception($"ADLink failed to create or link the user {user.Login}."));
    }

    [HttpPut("{id:guid}")]
    [ActionName("author")]
    public ActionResult<Author> UpdateAuthor(
        Guid id,
        [FromBody] Author author,
        [FromQuery] string clientVersion = null,
        [FromQuery] string originator = null)
    {
        try
        {
            var updatedAuthor = _dataSyncServiceDatabase.UpdateAuthor(author);
            if (originator != null)
            {
                this.LogInfo($"'{originator}' updated author '{updatedAuthor.LatinFullName}' ({updatedAuthor.Id}).");
            }

            return Ok(updatedAuthor);
        }
        catch (Exception ex)
        {
            return this.CreateInternalServerError(ex, $"{nameof(UpdateAuthor)} failed.");
        }
    }

    [HttpPut("{id:guid}")]
    [ActionName("authorfunction")]
    public ActionResult<JobFunction> AddOrUpdateAuthorFunction(
        Guid id,
        [FromBody] JobFunction jobFunction,
        [FromQuery] string clientVersion = null)
    {
        return AddOrUpdateJobFunction(id, jobFunction, clientVersion);
    }

    [HttpPut("{id:guid}")]
    [ActionName("jobfunction")]
    public ActionResult<JobFunction> AddOrUpdateJobFunction(
        Guid id,
        [FromBody] JobFunction authorFunction,
        [FromQuery] string clientVersion = null)
    {
        try
        {
            return _dataSyncServiceDatabase.AddOrUpdateJobFunction(authorFunction, ModificationType.ServerModification);
        }
        catch (Exception ex)
        {
            return this.CreateInternalServerError(ex, $"{nameof(AddOrUpdateJobFunction)} failed.");
        }
    }

    [HttpPut("{id:guid}")]
    [ActionName("workplace")]
    public ActionResult<Workplace> AddOrUpdateWorkplace(
        Guid id,
        [FromBody] Workplace workplace,
        [FromQuery] string clientVersion = null)
    {
        try
        {
            return _dataSyncServiceDatabase.AddOrUpdateWorkplace(workplace, ModificationType.ServerModification);
        }
        catch (Exception ex)
        {
            return this.CreateInternalServerError(ex, $"{nameof(AddOrUpdateWorkplace)} failed.");
        }
    }

    [HttpPut("{id:guid}")]
    [ActionName("jobassignment")]
    public ActionResult<JobAssignment> UpdateJobAssignment(
        Guid id,
        [FromBody] JobAssignment jobAssignment,
        [FromQuery] string clientVersion = null)
    {
        try
        {
            return _dataSyncServiceDatabase.AddOrUpdateJobAssignment(jobAssignment, ModificationType.ServerModification);
        }
        catch (Exception ex)
        {
            return this.CreateInternalServerError(ex, $"{nameof(UpdateJobAssignment)} failed.");
        }
    }

    [HttpPut("{id:guid}")]
    [ActionName("userbrick")]
    public ActionResult<UserBrick> UpdateUserBrick(
        Guid id,
        [FromBody] UserBrick userBrick,
        [FromQuery] string clientVersion = null)
    {
        try
        {
            return _dataSyncServiceDatabase.AddOrUpdateUserBrick(userBrick, ModificationType.ServerModification);
        }
        catch (Exception ex)
        {
            return this.CreateInternalServerError(ex, $"{nameof(UpdateUserBrick)} failed.");
        }
    }

    [HttpPut("{id:guid}")]
    [ActionName("buildingblockbrick")]
    public ActionResult<BuildingBlockBrick> UpdateBuildingBlockBrick(
        Guid id,
        [FromBody] BuildingBlockBrick buildingBlockBrick,
        [FromQuery] string clientVersion = null)
    {
        try
        {
            return _dataSyncServiceDatabase.AddOrUpdateBuildingBlockBrick(buildingBlockBrick, ModificationType.ServerModification);
        }
        catch (Exception ex)
        {
            return this.CreateInternalServerError(ex, $"{nameof(UpdateBuildingBlockBrick)} failed.");
        }
    }

    [HttpPut("{id:guid}")]
    [ActionName("userauthor")]
    public ActionResult<UserAuthor> AddOrUpdateUserAuthor(
        Guid id,
        [FromBody] UserAuthor userAuthor,
        [FromQuery] string clientVersion = null)
    {
        try
        {
            userAuthor.Author = null;
            return _dataSyncServiceDatabase.AddOrUpdateUserAuthor(userAuthor, ModificationType.ServerModification);
        }
        catch (Exception ex)
        {
            return this.CreateInternalServerError(ex, $"{nameof(AddOrUpdateUserAuthor)} failed.");
        }
    }

    [HttpPut("{id:guid}")]
    [ActionName("documentmodelsettings")]
    public ActionResult<DocumentModelSettings> AddOrUpdateDocumentModelSettings(
        Guid id,
        [FromBody] DocumentModelSettings docSettings,
        [FromQuery] string clientVersion = null)
    {
        try
        {
            return _dataSyncServiceDatabase.AddOrUpdateDocumentModelSettings(docSettings, ModificationType.ServerModification);
        }
        catch (Exception ex)
        {
            return this.CreateInternalServerError(ex, $"{nameof(AddOrUpdateDocumentModelSettings)} failed.");
        }
    }

    [HttpPut("{id:guid}")]
    [ActionName("devicesettings")]
    public ActionResult<DeviceSettings> AddOrUpdateDeviceSettings(
        Guid id,
        [FromBody] DeviceSettings deviceSettings,
        [FromQuery] string clientVersion = null)
    {
        try
        {
            return _dataSyncServiceDatabase.AddOrUpdateDeviceSettings(deviceSettings, ModificationType.ServerModification);
        }
        catch (Exception ex)
        {
            return this.CreateInternalServerError(ex, $"{nameof(AddOrUpdateDeviceSettings)} failed.");
        }
    }

    [HttpPut("{id:guid}")]
    [ActionName("bricksettings")]
    public ActionResult<BrickSettings> AddOrUpdateBrickSettings(
        Guid id,
        [FromBody] BrickSettings brickSettings,
        [FromQuery] string clientVersion = null)
    {
        try
        {
            return _dataSyncServiceDatabase.AddOrUpdateBrickSettings(brickSettings, ModificationType.ServerModification);
        }
        catch (Exception ex)
        {
            return this.CreateInternalServerError(ex, $"{nameof(AddOrUpdateBrickSettings)} failed.");
        }
    }

    [HttpPut("{id:guid}")]
    [ActionName("usertemplate")]
    public ActionResult<UserTemplate> AddOrUpdateUserTemplate(
        Guid id,
        [FromBody] UserTemplate userTemplate,
        [FromQuery] string clientVersion = null)
    {
        try
        {
            return _dataSyncServiceDatabase.AddOrUpdateUserTemplate(userTemplate, ModificationType.ServerModification);
        }
        catch (Exception ex)
        {
            return this.CreateInternalServerError(ex, $"{nameof(AddOrUpdateUserTemplate)} failed.");
        }
    }

    [HttpPut("{id:guid}")]
    [ActionName("usersettings")]
    public ActionResult<UserSettings> UpdateUserSettings(
        Guid id,
        [FromBody] UserSettings settings,
        [FromQuery] string clientVersion = null)
    {
        try
        {
            return _dataSyncServiceDatabase.UpdateUserSettings(settings);
        }
        catch (Exception ex)
        {
            return this.CreateInternalServerError(ex, $"{nameof(UpdateUserSettings)} failed.");
        }
    }

    [HttpPost]
    [ActionName("actionlogs")]
    public ActionResult<bool> UploadActionLogs([FromBody] ActionLog[] actionLogs, [FromQuery] string clientVersion = null)
    {
        try
        {
            _dataSyncServiceDatabase.AddActionLogs(actionLogs);
            return true;
        }
        catch (Exception ex)
        {
            return this.CreateInternalServerError(ex, $"{nameof(UploadActionLogs)} failed.");
        }
    }

    [HttpPost]
    [ActionName("performancelogs")]
    public async Task<ActionResult<bool>> UploadPerformanceLogs(
        [FromBody] Data.Models.PerformanceLog[] performanceLogs,
        [FromQuery] string clientVersion = null)
    {
        try
        {
            if (!_settingsService.IsPerformanceLoggingEnabled)
            {
                return true;
            }

            var addinLoadedLogs =
                performanceLogs.Where(x => x.Event == nameof(PerformanceLogEvent.AddInLoaded)).ToArray();
            var groupedAddinLoadedLogs = GetGroupedByDate(addinLoadedLogs, DateTimeExtensions.DateGroupType.Week);
            await _dataSyncServiceDatabase.AddClientPerformanceLogs(
                groupedAddinLoadedLogs,
                PerformanceLogEvent.AddInLoaded,
                _settingsService);

            var creationDialogLoadedLogs =
                performanceLogs.Where(x => x.Event == nameof(PerformanceLogEvent.CreationDialogLoaded)).ToArray();
            var groupedCreationDialogLoadedLogs = GetGroupedByDate(
                creationDialogLoadedLogs,
                DateTimeExtensions.DateGroupType.Week);
            await _dataSyncServiceDatabase.AddClientPerformanceLogs(
                groupedCreationDialogLoadedLogs,
                PerformanceLogEvent.CreationDialogLoaded,
                _settingsService);

            var documentCreatedLogs =
                performanceLogs.Where(x => x.Event == nameof(PerformanceLogEvent.DocumentCreated)).ToArray();
            var groupedDocumentCreatedLogs = GetGroupedByDate(
                documentCreatedLogs,
                DateTimeExtensions.DateGroupType.Week);
            await _dataSyncServiceDatabase.AddClientPerformanceLogs(
                groupedDocumentCreatedLogs,
                PerformanceLogEvent.DocumentCreated,
                _settingsService);

            var dataSyncFinishedLogs = performanceLogs
                                       .Where(x => x.Event == nameof(PerformanceLogEvent.DataSyncFinished))
                                       .ToArray();
            var groupedDataSyncFinishedLogs = GetGroupedByDate(
                dataSyncFinishedLogs,
                DateTimeExtensions.DateGroupType.Week);
            await _dataSyncServiceDatabase.AddClientPerformanceLogs(
                groupedDataSyncFinishedLogs,
                PerformanceLogEvent.DataSyncFinished,
                _settingsService);

            return true;
        }
        catch (Exception ex)
        {
            var deviceSettingsIds = performanceLogs.Select(x => x.DeviceSettingsId).Distinct().ToList();
            return this.CreateInternalServerError(
                ex,
                $"{nameof(UploadPerformanceLogs)} failed.\nClient version: {clientVersion}; device settings: {string.Join(", ", deviceSettingsIds)}.");
        }
    }

    [HttpPost]
    [ActionName("exceptionbuckets")]
    public ActionResult<bool> UploadExceptionLogs(
        [FromBody] ExceptionBucket[] exceptionBuckets,
        [FromQuery] string clientVersion = null)
    {
        try
        {
            _dataSyncServiceDatabase.AddOrUpdateExceptionBuckets(exceptionBuckets);
            return true;
        }
        catch (Exception ex)
        {
            return this.CreateInternalServerError(ex, $"{nameof(UploadExceptionLogs)} failed.");
        }
    }

    [HttpPost]
    [ActionName("exceptionlogentries")]
    public ActionResult<bool> UploadExceptionLogEntries(
        [FromBody] ExceptionLogEntry[] exceptionLogEntries,
        [FromQuery] string clientVersion = null)
    {
        try
        {
            _dataSyncServiceDatabase.AddExceptionLogEntries(exceptionLogEntries);
            return true;
        }
        catch (Exception ex)
        {
            return this.CreateInternalServerError(ex, $"{nameof(UploadExceptionLogEntries)} failed.");
        }
    }

    [HttpPost]
    [ActionName("activitytracks")]
    public ActionResult<bool> UploadActivityTracks(
        [FromBody] ActivityTrack[] activityTracks,
        [FromQuery] string clientVersion = null)
    {
        try
        {
            _dataSyncServiceDatabase.AddActivityTracks(activityTracks);
            return true;
        }
        catch (Exception ex)
        {
            return this.CreateInternalServerError(ex, $"{nameof(UploadActivityTracks)} failed.");
        }
    }

    [HttpPost]
    [ActionName("confirmremotewipe")]
    public ActionResult<bool> ConfirmRemoteWipe(
        [FromBody] string login,
        [FromQuery] string deviceName,
        [FromQuery] string clientVersion = null)
    {
        try
        {
            _dataSyncServiceDatabase.ConfirmRemoteWipe(login, deviceName);
            return true;
        }
        catch (Exception ex)
        {
            return this.CreateInternalServerError(ex, $"{nameof(ConfirmRemoteWipe)} failed.");
        }
    }

    [HttpPost]
    [ActionName("deviceinfo")]
    public async Task<ActionResult<bool>> UploadDeviceInfo(
        [FromBody] DeviceInfo deviceInfo,
        [FromQuery] string userName = null,
        [FromQuery] string clientVersion = null)
    {
        try
        {
            bool userExists = _dataSyncServiceDatabase.UserExists(userName);

            if (!userExists)
            {
                this.LogDebug($"User {userName} doesn't exist.");
                userExists = _activeDirectoryLinker.CreateAndLinkUser(userName);
            }

            if (!userExists)
            {
                return false;
            }

            this.LogDebug("Updating device infos...");
            await RetryOnDeadlockAsync(_dataSyncServiceDatabase.ApplyDeviceInfo(userName, deviceInfo, clientVersion), 3);
            return
                true; // always return true, even if RetryOnDeadlockAsync failed (in order to wipe the Exception from the client log)
        }
        catch (Exception ex)
        {
            return this.CreateInternalServerError(ex, $"{nameof(UploadDeviceInfo)} failed.");
        }
    }

    protected async Task<bool> RetryOnDeadlockAsync(Task task, int retries)
    {
        try
        {
            await task;
            return true;
        }
        catch (Exception ex)
        {
            if (ex is SqlException || ex is Microsoft.Data.SqlClient.SqlException)
            {
                if (ex.Message.Contains("was deadlocked") ||
                    ex.InnerException?.Message.Contains("was deadlocked") == true)
                {
                    if (retries > 0)
                    {
                        return await RetryOnDeadlockAsync(task, retries - 1);
                    }

                    this.LogWarn(
                        "Failed to execute a task that was the victim of a deadlock. No more retries left...");
                    return false;
                }

                throw;
            }

            throw;
        }
    }

    private Dictionary<DateTime, List<Data.Models.PerformanceLog>> GetGroupedByDate(
        Data.Models.PerformanceLog[] performanceLogs,
        DateTimeExtensions.DateGroupType dateType)
    {
        var result = new Dictionary<DateTime, List<Data.Models.PerformanceLog>>();
        foreach (var log in performanceLogs)
        {
            if (log.Date == null)
            {
                continue;
            }

            var groupDate = log.Date.Value.ToDateType(dateType);
            if (!result.ContainsKey(groupDate))
            {
                result.Add(groupDate, new List<Data.Models.PerformanceLog>());
            }

            var list = result[groupDate];
            list.Add(log);
        }

        return result;
    }
}
