﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Eurolook.Common;
using Eurolook.Common.Log;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Eurolook.DataSyncService.Controllers.v2;

[ApiController]
[Route("~/oauth2/[action]")]
public class OAuth2Controller : ControllerBase, ICanLog
{
    private const int TokenLifetimeInSeconds = 3600;

    private readonly IConfiguration _configuration;

    public OAuth2Controller(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    [HttpPost]
    [ActionName("token")]
    public ActionResult GetToken(
        [FromHeader] string audience,
        [FromForm] string username,
        [FromForm] string password,
        [FromForm] string grant_type)
    {
        // Check if code is correct and if client credentials are correct.
        if (grant_type != "password")
        {
            return StatusCode(
                403,
                new
                {
                    error = "invalid_grant",
                    error_description = "Unsupported response type",
                });
        }

        if (username != _configuration["OAuth:OAuthUser"]
            || password != _configuration["OAuth:OAuthPassword"])
        {
            return StatusCode(
                403,
                new
                {
                    error = "invalid_grant",
                    error_description = "The user name or password is incorrect",
                });
        }

        var key = WebEncoders.Base64UrlDecode(_configuration["JWT:Key"]);
        var symmetricSecurityKey = new SymmetricSecurityKey(key);
        var tokenHandler = new JwtSecurityTokenHandler();
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[] { new Claim(ClaimTypes.Name, username) }),
            Expires = DateTime.UtcNow.AddSeconds(TokenLifetimeInSeconds),
            Issuer = _configuration["JWT:Issuer"],
            Audience = audience,
            SigningCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256Signature),
        };
        var token = tokenHandler.CreateToken(tokenDescriptor);
        var access_token = tokenHandler.WriteToken(token);

        // Returns the 'access_token' and the type in lower case
        return Ok(
            new
            {
                access_token,
                token_type = "bearer",
                expires_in = TokenLifetimeInSeconds,
            });
    }
}
