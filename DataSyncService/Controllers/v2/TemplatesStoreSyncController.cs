﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Eurolook.Common.Log;
using Eurolook.Data.Database;
using Eurolook.Data.Models.TemplateStore;
using Eurolook.Data.TemplateStore;
using Eurolook.DataSyncService.Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace Eurolook.DataSyncService.Controllers.v2
{
    [Authorize]
    [Route("api/v2/TemplateStoreSync/")]
    [ApiController]
    public class TemplatesStoreSyncController : ControllerBase, ICanLog
    {
        private readonly IDataSyncServiceDatabase _dataSyncServiceDatabase;
        private readonly ISharedTemplateRepository _sharedTemplateRepository;

        public TemplatesStoreSyncController(
            IDataSyncServiceDatabase dataSyncServiceDatabase,
            ISharedTemplateRepository sharedTemplateRepository)
        {
            _dataSyncServiceDatabase = dataSyncServiceDatabase;
            _sharedTemplateRepository = sharedTemplateRepository;
        }

        [HttpGet("usertemplates")]
        public async Task<ActionResult<UserTemplate[]>> GetUserTemplates([FromQuery] string login, [FromQuery] uint modifiedSinceHours)
        {
            var user = await _dataSyncServiceDatabase.GetUser(login);
            if (user == null)
            {
                return Forbid();
            }

            try
            {
                var userTemplates = await _sharedTemplateRepository.GetUserTemplatesAsync(user.Id);

                if (modifiedSinceHours > 0)
                {
                    var now = DateTime.UtcNow;
                    userTemplates = userTemplates.Where(x => (now - x.ServerModificationTimeUtc).TotalHours < modifiedSinceHours).ToArray();
                }

                foreach (var userTemplate in userTemplates)
                {
                    PrepareForDownload(userTemplate);
                }

                return userTemplates;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return BadRequest();
            }
        }

        [HttpPut("usertemplates/{templateId}")]
        public async Task<ActionResult> SaveAsUserTemplate(Guid templateId, [FromQuery] string login)
        {
            var user = await _dataSyncServiceDatabase.GetUser(login);
            if (user == null)
            {
                return Forbid();
            }

            try
            {
                await _sharedTemplateRepository.SaveAsUserTemplateAsync(templateId, user.Id);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return BadRequest();
            }

            return Ok();
        }

        [HttpDelete("usertemplates/{templateId}")]
        public async Task<ActionResult> DeleteFromUserTemplate(Guid templateId, [FromQuery] string login)
        {
            var user = await _dataSyncServiceDatabase.GetUser(login);
            if (user == null)
            {
                return Forbid();
            }

            try
            {
                await _sharedTemplateRepository.DeleteUserTemplate(templateId, user.Id);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return BadRequest();
            }

            return Ok();
        }

        [HttpGet("templates/{templateId}/owners")]
        public async Task<ActionResult<TemplateOwnerInfo[]>> GetOwners(Guid templateId)
        {
            try
            {
                var template = await _sharedTemplateRepository.GetTemplate(templateId);
                return Ok(template.Owners.Select(o => new TemplateOwnerInfo(o)).ToArray());
            }
            catch (ArgumentException aex)
            {
                Log.Error(aex.ToString());
                return BadRequest(aex.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }

        [HttpGet("templates/{templateId}/download")]
        public async Task<IActionResult> GetTemplateDownload(Guid templateId, [FromQuery] string login)
        {
            try
            {
                var user = await _dataSyncServiceDatabase.GetUser(login);
                if (user == null)
                {
                    return Forbid();
                }

                var template = await _sharedTemplateRepository.GetTemplateWithIncludes(templateId);

                template.BaseDocumentModel = null;
                if (template.Owners != null)
                {
                    foreach (var owner in template.Owners)
                    {
                        owner.Template = null;
                        owner.User = null;
                    }
                }

                if (template.Publications != null)
                {
                    foreach (var publication in template.Publications)
                    {
                        publication.Template = null;
                        publication.OrgaEntity = null;
                    }
                }

                if (template.TemplateFiles != null)
                {
                    foreach (var file in template.TemplateFiles)
                    {
                        file.Template = null;
                        file.Language = null;
                    }
                }

                return Ok(template);

            }
            catch (ArgumentException aex)
            {
                Log.Error(aex.ToString());
                return BadRequest(aex.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }

        private void PrepareForDownload(UserTemplate userTemplate)
        {
            var template = userTemplate.Template;

            template.BaseDocumentModel = null;
            template.Owners = null;
            template.PreviewPage1 = null;
            template.PreviewPage2 = null;
            template.Thumbnail = null;

            if (template.Publications != null)
            {
                foreach (var publication in template.Publications)
                {
                    publication.Template = null;
                    publication.OrgaEntity = null;
                }
            }

            if (template.TemplateFiles != null)
            {
                foreach (var file in template.TemplateFiles)
                {
                    file.Template = null;
                    file.Language = null;
                    file.Bytes = null;
                }
            }
        }
    }
}
