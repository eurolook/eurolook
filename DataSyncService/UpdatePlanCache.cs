using System;
using System.Threading.Tasks;
using Eurolook.Common;
using Eurolook.Common.Log;
using Eurolook.Data.Models;

namespace Eurolook.DataSyncService
{
    public class UpdatePlanCache : ICanLog
    {
        private const int CachedMinutes = 60;
        private static readonly SemaphoreLocker Locker = new SemaphoreLocker();
        private static readonly UpdatePlanCache SingleInstance = new UpdatePlanCache();
        private UpdatePlan _cachedInitializationUpdatePlan;

        private UpdatePlanCache()
        {
        }

        public static UpdatePlanCache GetSingleInstance()
        {
            return SingleInstance;
        }

        public async Task<UpdatePlan> GetUpdatePlanForInitializationRequestAsync(
            Func<Task<UpdatePlan>> updateCacheFuncAsync)
        {
            await Locker.LockAsync(() => UpdateCacheIfRequiredAsync(updateCacheFuncAsync));

            return _cachedInitializationUpdatePlan;
        }

        private async Task UpdateCacheIfRequiredAsync(Func<Task<UpdatePlan>> updateCacheFuncAsync)
        {
            if (_cachedInitializationUpdatePlan == null ||
                _cachedInitializationUpdatePlan.ServerTimestampUTC.AddMinutes(CachedMinutes) < DateTime.UtcNow)
            {
                this.LogTrace("Updating cache for initialization request.");
                _cachedInitializationUpdatePlan = await updateCacheFuncAsync();
            }
            else
            {
                var timestamp = _cachedInitializationUpdatePlan.ISOServerTimestampUTC;
                this.LogTrace($"Cache update for initialization request not required. (ServerTimestamp: {timestamp})");
            }
        }
    }
}
