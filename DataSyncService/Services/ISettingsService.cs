﻿namespace Eurolook.DataSyncService.Services
{
    public interface ISettingsService
    {
        bool IsPerformanceLoggingEnabled { get; }

        bool IsPerformanceTrackEnabled { get; }

        int PerformanceTrackSize { get; }
    }
}
