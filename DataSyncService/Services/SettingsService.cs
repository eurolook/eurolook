﻿using System;
using System.Configuration;
using System.Linq;
using Eurolook.ActiveDirectoryLink;
using Microsoft.Extensions.Configuration;

namespace Eurolook.DataSyncService.Services
{
    public class SettingsService : ISettingsService, IActiveDirectoryConfigurationProvider
    {
        private readonly IConfiguration _configuration;

        public SettingsService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public bool IsPerformanceLoggingEnabled
        {
            get
            {
                bool.TryParse(_configuration["IsPerformanceLoggingEnabled"], out bool result);
                return result;
            }
        }

        public bool IsPerformanceTrackEnabled
        {
            get
            {
                bool.TryParse(_configuration["IsPerformanceTrackEnabled"], out bool result);
                return result;
            }
        }

        public int PerformanceTrackSize
        {
            get
            {
                int.TryParse(_configuration["PerformanceTrackSize"], out int result);
                return result;
            }
        }

        public string LdapServer => _configuration["LDAPServer"];

        public string[] LdapSearchBases => _configuration.GetSection("LDAPSearchBases").Get<string[]>();

        public string LdapAuthenticationType => _configuration["LDAPAuthenticationType"];

        public int ActiveDirectoryPageSize => int.Parse(_configuration["ActiveDirectoryPageSize"]);

        public string ActiveDirectorySearchFilter => _configuration["ActiveDirectorySearchFilter"];

        public string RetiredUsersSearchFilter => _configuration["RetiredUsersSearchFilter"];

        public string AgencyNames => _configuration["AgencyNames"];

        public string DefaultDomain => _configuration["DefaultDomain"];

        public string ServerDatabaseConnection => _configuration.GetConnectionString("ServerDatabaseConnection");
    }
}
