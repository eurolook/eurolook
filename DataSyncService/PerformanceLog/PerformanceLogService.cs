﻿using System;
using System.Threading.Tasks;
using Eurolook.Data.Models;
using Eurolook.DataSyncService.Database;
using Eurolook.DataSyncService.Services;

namespace Eurolook.DataSyncService.PerformanceLog
{
    public class PerformanceLogService : IPerformanceLogService
    {
        private readonly ISettingsService _settingsService;
        private readonly IDataSyncServiceDatabase _dataSyncServiceDatabase;

        public PerformanceLogService(ISettingsService settingsService, IDataSyncServiceDatabase dataSyncServiceDatabase)
        {
            _settingsService = settingsService;
            _dataSyncServiceDatabase = dataSyncServiceDatabase;
        }

        public async Task LogGetUpdates(long milliseconds)
        {
            await _dataSyncServiceDatabase.WriteServerPerformanceLogAsync(
                PerformanceLogEvent.ServerGetUpdates,
                new TimeSpan(TimeSpan.TicksPerMillisecond * milliseconds),
                GetServerName(),
                _settingsService);
        }

        public async Task LogGetInitialization(long milliseconds)
        {
            await _dataSyncServiceDatabase.WriteServerPerformanceLogAsync(
                PerformanceLogEvent.ServerGetInitialization,
                new TimeSpan(TimeSpan.TicksPerMillisecond * milliseconds),
                GetServerName(),
                _settingsService);
        }

        public async Task LogGetUserProfile(long milliseconds)
        {
            await _dataSyncServiceDatabase.WriteServerPerformanceLogAsync(
                PerformanceLogEvent.ServerGetUserProfile,
                new TimeSpan(TimeSpan.TicksPerMillisecond * milliseconds),
                GetServerName(),
                _settingsService);
        }

        private string GetServerName()
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            if (string.IsNullOrWhiteSpace(env))
            {
                env = "DEBUG";
            }

            return $"{Environment.MachineName} ({env})";
        }
    }
}
