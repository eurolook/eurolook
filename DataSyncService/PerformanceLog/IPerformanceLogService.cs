using System.Threading.Tasks;

namespace Eurolook.DataSyncService.PerformanceLog
{
    public interface IPerformanceLogService
    {
        Task LogGetUpdates(long milliseconds);

        Task LogGetInitialization(long milliseconds);

        Task LogGetUserProfile(long milliseconds);
    }
}
