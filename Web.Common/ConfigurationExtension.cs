﻿using System;
using Microsoft.Extensions.Configuration;

namespace Eurolook.Web.Common;

public static class ConfigurationExtension
{
    public static string GetMandatory(this IConfiguration configuration, string value)
    {
        return configuration[value] ?? throw new Exception($"Mandatory configuration value {value} not found");
    }
}
