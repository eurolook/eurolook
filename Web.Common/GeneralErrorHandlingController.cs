﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Eurolook.Web.Common;

[ApiController]
public class GeneralErrorHandlingController : ControllerBase
{
    [AllowAnonymous]
    [Route("/Account/AccessDenied")]
    public IActionResult AccessDenied()
    {
        return Problem("Access denied", null, 401);
    }

    [AllowAnonymous]
    [Route("/error")]
    public IActionResult HandleError() => Problem();
}
