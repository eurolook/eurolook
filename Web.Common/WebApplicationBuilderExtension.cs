﻿using System;
using System.Diagnostics;
using Eurolook.Common.Extensions;
using Eurolook.Data;
using Eurolook.Web.Common.EuLogin;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Server.IISIntegration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Eurolook.Web.Common
{
    public static class WebApplicationBuilderExtension
    {
        public static WebApplicationBuilder AddDefaultConfigurationProviders(this WebApplicationBuilder builder)
        {
            string env = builder.Environment.EnvironmentName;
            builder.Configuration.AddDefaultConfigurationProviders(env);
            return builder;
        }

        //public static void AddEuLoginAuthentication(this WebApplicationBuilder builder, bool useEuLogin, string loginPath = "/Account/Login")
        //{
        //    if (!useEuLogin)
        //    {
        //        // Use Windows Authentication if EU Login is not enabled
        //        builder.Services.AddAuthentication(IISDefaults.AuthenticationScheme);
        //        return;
        //    }

        //    builder.AddEuLoginAuthentication(loginPath);
        //}

        // apply migrations:
        // 1) by using launchsettings.json:
        // "commandLineArgs": "startup=migrate"
        //
        // 2) by using command line:
        // set ASPNETCORE_ENVIRONMENT=<Environment>
        // <Executable>.exe startup=migrate
        public static void AddMigrationCommandLineOption(this WebApplicationBuilder builder)
        {
            if (builder.Configuration.GetValue<string>("startup") == "migrate")
            {
                var connectionString = builder.Configuration.GetConnectionString("ServerDatabaseConnection");
                using var eurolookServerContext = new EurolookServerContext(connectionString);

                Debug.WriteLine($"Applying migrations: {connectionString}");
                Console.WriteLine($"Applying migrations: {connectionString}");

                eurolookServerContext.Migrate();

                Debug.WriteLine("Done.");
                Console.WriteLine("Done.");

                Environment.Exit(0);
            }
        }
    }
}
