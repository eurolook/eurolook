﻿using System;
using System.Reflection;
using Eurolook.Common;
using Microsoft.AspNetCore.Builder;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using Serilog.Sinks.Email;

namespace Eurolook.Web.Common
{
    public class ApplicationInfo
    {
        public string Version { get; }

        public string BuildTime { get; }

        public string CommitHash { get; }

        public string EnvironmentName { get; }

        public ApplicationInfo(string environmentName)
        {
            var assemblyBuildInfo = new AssemblyBuildInfo(Assembly.GetEntryAssembly());
            Version = assemblyBuildInfo.Version;
            BuildTime = assemblyBuildInfo.BuildTime.ToString();
            CommitHash = assemblyBuildInfo.CommitHash;
            EnvironmentName = environmentName;
        }
    }
}
