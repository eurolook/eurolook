﻿using Microsoft.AspNetCore.Hosting;

namespace Eurolook.Web.Common
{
    public static class WebHostingEnvironmentExtensions
    {
        public static bool IsLocalDevelopment(this IWebHostEnvironment env)
        {
            return env.EnvironmentName.EndsWith("Development");
        }
    }
}
