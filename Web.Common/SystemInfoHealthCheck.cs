﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Eurolook.Web.Common
{
    public static class SystemInfoHealthCheck
    {
        public static IHealthChecksBuilder AddSystemInfo(this IHealthChecksBuilder builder, WebApplicationBuilder appBuilder)
        {
            var dataInfo = new Dictionary<string, object>
            {
                { "EL_ENV", appBuilder.Configuration["EL_ENV"] },
                { "Environment Name", appBuilder.Environment.EnvironmentName},
            };

            return builder.AddCheck("System Information", () => HealthCheckResult.Healthy("System Information", dataInfo));
        }
    }
}
