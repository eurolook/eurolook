﻿namespace Eurolook.Web.Common.EuLogin
{
    public class EuLoginOptions
    {
        public string ServerUrlBase { get; set; }

        public string ValidationPathSuffix { get; set; } = "serviceValidate";

        public string RedirectUri { get; set; } = "/";

        public string[] LoginIdentifierPrefixes { get; set; }

        public bool LogValidationResponse { get; set; }
    }
}
