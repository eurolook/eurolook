﻿using System;
using System.Linq;
using System.Net.Http;
using System.Security.Authentication;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using Eurolook.Common.Extensions;
using GSS.Authentication.CAS.AspNetCore;
using GSS.Authentication.CAS.Security;
using GSS.Authentication.CAS.Validation;
using Microsoft.Extensions.Primitives;
using Serilog;

namespace Eurolook.Web.Common.EuLogin
{
    public class EuLoginCasServiceTicketValidator : IServiceTicketValidator
    {
        private static readonly XNamespace _namespace = "https://ecas.ec.europa.eu/cas/schemas";
        private static readonly XName _failureElementName = _namespace + "authenticationFailure";
        private static readonly XName _successElementName = _namespace + "authenticationSuccess";
        private static readonly XName _userElementName = _namespace + "user";

        private readonly CasAuthenticationOptions _casAuthenticationOptions;
        private readonly EuLoginOptions _euLoginOptions;

        public EuLoginCasServiceTicketValidator(CasAuthenticationOptions casAuthenticationOptions, EuLoginOptions euLoginOptions)
        {
            _casAuthenticationOptions = casAuthenticationOptions;
            _euLoginOptions = euLoginOptions;
        }

        public async Task<ICasPrincipal> ValidateAsync(string ticket, string service, CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrEmpty(ticket))
            {
                throw new ArgumentException($"{nameof(ticket)} argument null or empty");
            }

            if (string.IsNullOrEmpty(service))
            {
                throw new ArgumentException($"{nameof(service)} argument null or empty");
            }

            var baseUri = new Uri(_casAuthenticationOptions.CasServerUrlBase.EnsureEndsWith("/"));
            var validationUri = new Uri(baseUri, $"{_euLoginOptions.ValidationPathSuffix}?ticket={Uri.EscapeDataString(ticket)}&service={Uri.EscapeDataString(service)}&userDetails=true");

            using var httpClient = new HttpClient();
            var response = await httpClient.GetAsync(validationUri, cancellationToken).ConfigureAwait(false);
            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException($"Request failed: {validationUri}");
            }

            var responseBody = await response.Content.ReadAsStringAsync(cancellationToken).ConfigureAwait(false);
            return BuildPrincipal(responseBody);
        }

        private ICasPrincipal BuildPrincipal(string responseBody)
        {
            var xelementRoot = XElement.Parse(responseBody);

            if (_euLoginOptions.LogValidationResponse)
            {
                Log.Debug($"Response Body: {responseBody}");
            }

            var authFailureElement = xelementRoot.Element(_failureElementName);
            if (authFailureElement != null)
            {
                throw new AuthenticationException(authFailureElement.Value);
            }

            var authSuccessElement = xelementRoot.Element(_successElementName)
                ?? throw new AuthenticationException($"{_successElementName} element not present in response");

            var user = authSuccessElement.Element(_userElementName)?.Value;
            if (string.IsNullOrEmpty(user))
            {
                throw new AuthenticationException($"{_userElementName} element not present in response");
            }

            var attributes = authSuccessElement.Elements()
                .Where(d => !d.HasElements)
                .ToDictionary(d => d.Name.LocalName, d => new StringValues(d.Value));

            var assertion = new Assertion(user, attributes);
            return new CasPrincipal(assertion, _casAuthenticationOptions.AuthenticationType);
        }
    }
}
