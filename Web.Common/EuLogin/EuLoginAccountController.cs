﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.FeatureManagement.Mvc;

namespace Eurolook.Web.Common.EuLogin;

[ApiController]
[FeatureGate("EuLogin")]
public class EuLoginAccountController : ControllerBase
{
    private readonly EuLoginOptions _euLoginOptions;

    public EuLoginAccountController(IOptionsSnapshot<EuLoginOptions> euLoginOptions)
    {
        _euLoginOptions = euLoginOptions.Value;
    }

    [AllowAnonymous]
    [HttpGet("/account/login")]
    public IActionResult Login()
    {
        return Challenge(new AuthenticationProperties { RedirectUri = _euLoginOptions.RedirectUri }, EuLoginAuthentication.AuthenticationScheme);
    }
}
