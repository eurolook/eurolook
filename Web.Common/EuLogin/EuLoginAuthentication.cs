﻿using System.Security.Claims;
using System.Threading.Tasks;
using GSS.Authentication.CAS.AspNetCore;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Eurolook.Web.Common.EuLogin
{
    public static class EuLoginAuthentication
    {
        public static readonly string AuthenticationScheme = "CAS";

        public static void AddEuLoginAuthentication(this WebApplicationBuilder builder, string loginPath = "/Account/Login")
        {
            var euLogionOptionsSection = builder.Configuration.GetSection(nameof(EuLoginOptions));
            var euLoginOptions = euLogionOptionsSection.Get<EuLoginOptions>();
            builder.Services.AddOptions<EuLoginOptions>().Bind(euLogionOptionsSection);
            builder.Services.AddScoped<CustomCookieAuthenticationEvents>();
            builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            .AddCookie(options =>
            {
                // Default login path in asp net core : /Account/Login
                options.LoginPath = loginPath;
                options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
                options.EventsType = typeof(CustomCookieAuthenticationEvents);
            })
            .AddCAS(options =>
            {
                options.CasServerUrlBase = euLoginOptions.ServerUrlBase;
                options.ServiceTicketValidator = new EuLoginCasServiceTicketValidator(options, euLoginOptions);
                options.Events.OnCreatingTicket = context =>
                {
                    if (context.Identity == null)
                    {
                        return Task.CompletedTask;
                    }

                    var assertion = context.Assertion;
                    context.Identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, assertion.PrincipalName));

                    if (assertion.Attributes.TryGetValue("email", out var email) && !string.IsNullOrWhiteSpace(email))
                    {
                        context.Identity.AddClaim(new Claim(ClaimTypes.Email, email));
                    }

                    return Task.CompletedTask;
                };
            });
        }
    }
}
