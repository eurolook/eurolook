﻿using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Newtonsoft.Json;

namespace Eurolook.Web.Common
{
    public static class EndpointRouteBuilderExtensions
    {
        public static IEndpointConventionBuilder MapHealthChecksWithAnonymousAccess(this IEndpointRouteBuilder endpoints, string pattern)
        {
            var healthCheckOptions = new HealthCheckOptions
            {
                ResponseWriter = async (c, r) =>
                {
                    c.Response.ContentType = "application/json";

                    var result = JsonConvert.SerializeObject(new
                    {
                        status = r.Status.ToString(),
                        entries = r.Entries.Select(e => new
                        {
                            key = e.Key,
                            status = e.Value.Status.ToString(),
                            data = e.Value.Data.Select(d => new
                            {
                                key = d.Key,
                                value = d.Value,
                            }),
                        }),
                    });

                    await c.Response.WriteAsync(result);
                },
            };

            return endpoints.MapHealthChecks(pattern, healthCheckOptions).WithMetadata(new AllowAnonymousAttribute());
        }
    }
}
