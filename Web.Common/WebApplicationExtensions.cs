﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using Eurolook.Common;
using Eurolook.Common.Extensions;
using Eurolook.Common.Log;
using Eurolook.Web.Common.ScheduledTask;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Debugging;
using Serilog.Events;
using Serilog.Sinks.Email;

namespace Eurolook.Web.Common
{
    public static class WebApplicationExtensions
    {
        [SuppressMessage("csharpsquid", "S4792:Make sure that this logger's configuration is safe.", Justification = "Reviewed")]
        public static void CreateAndSetupLogger(this WebApplication app, Assembly assembly)
        {
            var configuration = new LoggerConfiguration().ReadFrom.Configuration(app.Configuration);

            var notificationSettings = app.Configuration.GetSection("NotificationSettings");
            var emailConnectionInfo = new EmailConnectionInfo
            {
                FromEmail = notificationSettings["FromAddress"],
                ToEmail = notificationSettings["AlertToAddress"],
                MailServer = notificationSettings["SmtpServer"],
                EmailSubject = $"[Alert] {app.Environment.ApplicationName} [{app.Environment.EnvironmentName}] [{Environment.MachineName}]",
            };

            var isAlertingEnabled =
                !string.IsNullOrWhiteSpace(emailConnectionInfo.FromEmail) &&
                !string.IsNullOrWhiteSpace(emailConnectionInfo.ToEmail) &&
                !string.IsNullOrWhiteSpace(emailConnectionInfo.MailServer);

            if (isAlertingEnabled)
            {
                configuration = configuration.WriteTo.Email(
                    emailConnectionInfo,
                    restrictedToMinimumLevel: LogEventLevel.Error,
                    period: TimeSpan.FromDays(1),
                    batchPostingLimit: 100);
            }

            SelfLog.Enable(Console.WriteLine);
            app.UseSerilogRequestLogging();

            Log.Logger = configuration.CreateLogger();

            Log.Information($"ApplicationName: {app.Environment.ApplicationName}");
            Log.Information($"EnvironmentName: {app.Environment.EnvironmentName}");
            Log.Information($"ContentRootPath: {app.Environment.ContentRootPath}");
            Log.Information($"WebRootPath: {app.Environment.WebRootPath}");
            Log.Information($"MachineName: {Environment.MachineName}");
            Log.Information($"Configuration: {AssemblyAttributeReader.GetFirstAssemblyAttributeString<AssemblyConfigurationAttribute>(assembly)}");
            Log.Information($"Informational Version: {AssemblyAttributeReader.GetFirstAssemblyAttributeString<AssemblyInformationalVersionAttribute>(assembly)}");

            if (app.Configuration["FeatureManagement:HttpHeaderLogging"].ToBool())
            {
                Log.Information($"HttpHeaderLogging enabled.");
                app.UseMiddleware<HttpHeaderLogging>();
            }

            if (isAlertingEnabled)
            {
                Log.Information($"Alerting enabled. Recipients: {emailConnectionInfo.ToEmail}");
            }
            else
            {
                Log.Information("Alerting is disabled.");
            }

            // Support logging via ILog interface which is used in referenced libraries. Can be removed once Serilog is used everywhere
            LogManager.LogProviderFunc = () => new SerilogILogProvider();
        }

        public static void StartRegisteredScheduledTasks(this WebApplication app)
        {
            foreach (var scheduledTask in app.Services.GetServices<IScheduledTask>())
            {
                scheduledTask.Setup();
                scheduledTask.Start();
            }
        }
    }
}
