﻿using Eurolook.Common.Log;

namespace Eurolook.Web.Common
{
    public class SerilogILogProvider : ILogProvider
    {
        public ILog Create(string typeName, string logFileName)
        {
            return new SerilogILogAdapter();
        }
    }
}
