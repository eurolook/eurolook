﻿using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Serilog;

namespace Eurolook.Web.Common
{
    public class HttpHeaderLogging
    {
        private readonly RequestDelegate _next;

        public HttpHeaderLogging(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine($"HTTP {context.Request.Method.ToUpperInvariant()} {context.Request.Path} - Headers:");
            foreach (var header in context.Request.Headers)
            {
                // keep spaces for log output formatting
                builder.AppendLine($"                                     {header.Key}: {header.Value}");
            }

            Log.Debug(builder.ToString());
            await _next(context);
        }
    }
}
