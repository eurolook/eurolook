﻿using System;
using Eurolook.Common.Log;
using Serilog;
using Serilog.Events;

namespace Eurolook.Web.Common
{
    public class SerilogILogAdapter : ILog
    {
        public bool IsTraceEnabled { get; } = Log.IsEnabled(LogEventLevel.Verbose);

        public bool IsDebugEnabled { get; } = Log.IsEnabled(LogEventLevel.Debug);

        public bool IsInfoEnabled { get; } = Log.IsEnabled(LogEventLevel.Information);

        public bool IsWarnEnabled { get; } = Log.IsEnabled(LogEventLevel.Warning);

        public bool IsErrorEnabled { get; } = Log.IsEnabled(LogEventLevel.Error);

        public bool IsFatalEnabled { get; } = Log.IsEnabled(LogEventLevel.Fatal);

        public void Trace(string message)
        {
            Log.Write(LogEventLevel.Verbose, message);
        }

        public void Trace(string message, Exception exception)
        {
            Log.Write(LogEventLevel.Verbose, message, exception);
        }

        public void Trace(string format, params object[] parameters)
        {
        }

        public void Debug(string message)
        {
            Log.Debug(message);
        }

        public void Debug(string message, Exception exception)
        {
            Log.Debug(message, exception);
        }

        public void Debug(string format, params object[] parameters)
        {
        }

        public void Info(string message)
        {
            Log.Information(message);
        }

        public void Info(string message, Exception exception)
        {
            Log.Information(message, exception);
        }

        public void Info(string format, params object[] parameters)
        {
        }

        public void Warn(string message)
        {
            Log.Warning(message);
        }

        public void Warn(string message, Exception exception)
        {
            Log.Warning(message, exception);
        }

        public void Warn(string format, params object[] parameters)
        {
        }

        public void Error(string message)
        {
            Log.Error(message);
        }

        public void Error(string message, Exception exception)
        {
            Log.Error(message, exception);
        }

        public void Error(string format, params object[] parameters)
        {
        }

        public void Fatal(string message)
        {
            Log.Fatal(message);
        }

        public void Fatal(string message, Exception exception)
        {
            Log.Fatal(message, exception);
        }

        public void Fatal(string format, params object[] parameters)
        {
        }
    }
}
