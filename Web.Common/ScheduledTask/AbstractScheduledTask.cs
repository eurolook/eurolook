﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Serilog;

namespace Eurolook.Web.Common.ScheduledTask
{
    public abstract class AbstractScheduledTask : IScheduledTask
    {
        private Timer _timer;
        private Timer _configurationChangeTimer;
        private bool _disposedValue;

        protected string ScheduledTaskName { get; }

        protected AbstractScheduledTask(string name)
        {
            ScheduledTaskName = name;
        }

        protected int? IntervalMinutes { get; set; }

        protected int Minute { get; set; }

        protected int Hour { get; set; }

        protected bool IsActive { get; set; }

        protected DayOfWeek? DayOfWeek { get; set; }

        private bool IsWeekly => DayOfWeek != null;

        private bool IsDaily => DayOfWeek == null;

        public void Setup()
        {
            SetConfigurationValues();
            StartConfigurationChangeListener();

            if (!IsActive)
            {
                return;
            }

            _timer = new Timer(Execute, this, Timeout.Infinite, Timeout.Infinite);
        }

        public void Start()
        {
            if (!IsActive)
            {
                Log.Information($"Scheduled task {ScheduledTaskName} is deactivated.");
                return;
            }

            try
            {
                StartTimer();
            }
            catch
            {
                Task.Delay(5000);
                try
                {
                    StartTimer();
                }
                catch (Exception ex)
                {
                    Log.Error(ex, $"Failed to start {ScheduledTaskName}.");
                }
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        protected abstract void SetConfigurationValues();

        protected abstract Task RunTask();

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    Stop();
                    _timer?.Dispose();
                    _configurationChangeTimer?.Dispose();
                }

                _disposedValue = true;
            }
        }

        private void StartConfigurationChangeListener()
        {
            int configChangeIntervalSeconds = 5;
            Log.Information($"Listening for configuration changes every {configChangeIntervalSeconds} seconds.");
            _configurationChangeTimer = new Timer(DetectConfigurationChange, null, TimeSpan.FromSeconds(configChangeIntervalSeconds), TimeSpan.FromSeconds(configChangeIntervalSeconds));
        }

        private void DetectConfigurationChange(object _)
        {
            var minute = Minute;
            var hour = Hour;
            var isActive = IsActive;
            var dayOfWeek = DayOfWeek;

            SetConfigurationValues();

            if (minute != Minute || hour != Hour || isActive != IsActive || dayOfWeek != DayOfWeek)
            {
                Log.Information($"{ScheduledTaskName}: Configuration change detected.");
                Start();
            }
        }

        private void StartTimer()
        {
            if (IntervalMinutes.HasValue)
            {
                _timer?.Change(TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(IntervalMinutes.Value));
                Log.Information($"Setup {ScheduledTaskName} to run every {IntervalMinutes} minutes");
            }
            else
            {
                var targetDate = GetTargetDate();
                var dueTime = targetDate - DateTime.Now;
                var dailyInterval = 1000 * 60 * 60 * 24;
                _timer?.Change((int)dueTime.TotalMilliseconds, IsWeekly ? dailyInterval * 7 : dailyInterval);
                var logSuffix = IsWeekly ? $"{DayOfWeek}" : "day";
                Log.Information($"Setup {ScheduledTaskName} to run at {Hour}:{Minute}, every {logSuffix}.");
            }
        }

        private void Stop()
        {
            _configurationChangeTimer?.Change(Timeout.Infinite, Timeout.Infinite);
            _timer?.Change(Timeout.Infinite, Timeout.Infinite);
            if (!IsActive)
            {
                return;
            }

            Log.Information($"Stopped {ScheduledTaskName}.");
        }

        private DateTime GetTargetDate()
        {
            int daysUntil;
            var now = DateTime.Now;
            if (IsDaily || now.DayOfWeek == DayOfWeek)
            {
                // today is 'the' day and it's already too late
                if (now.Hour > Hour || (now.Hour == Hour && now.Minute > Minute))
                {
                    daysUntil = IsDaily ? 1 : 7;
                }
                else
                {
                    daysUntil = 0;
                }
            }
            else
            {
                // the day is next week / tomorrow
                daysUntil = IsWeekly ? ((int)DayOfWeek - (int)now.DayOfWeek + 7) % 7 : 1;
            }

            var targetDate = DateTime.Today.AddDays(daysUntil);
            targetDate = targetDate.AddHours(Hour);
            targetDate = targetDate.AddMinutes(Minute);
            return targetDate;
        }

        private async void Execute(object state)
        {
            try
            {
                Log.Information($"Starting {ScheduledTaskName}.");
                await RunTask();
                Log.Information($"Successfully executed {ScheduledTaskName}.");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Execute failed");
            }
        }
    }
}
