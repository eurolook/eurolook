﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Serilog;

namespace Eurolook.Web.Common.ScheduledTask
{
    public class KeepAliveTask : AbstractScheduledTask
    {
        private readonly IServer _server;

        public KeepAliveTask(IServer server)
            : base(nameof(KeepAliveTask))
        {
            _server = server;
        }

        protected override void SetConfigurationValues()
        {
            IntervalMinutes = 10;
            IsActive = true;
        }

        protected override async Task RunTask()
        {
            var baseAddress = GetBaseAddress();
            Log.Information($"Sending keep alive request to {baseAddress}health");

            using var client = new HttpClient();
            client.BaseAddress = new Uri(baseAddress);
            await client.SendAsync(new HttpRequestMessage(HttpMethod.Get, "health"));
        }

        private string GetBaseAddress()
        {
            var addresses = _server?.Features.Get<IServerAddressesFeature>()?.Addresses;
            var baseAddress = addresses?.FirstOrDefault() ?? throw new Exception("Failed to determine base address");
            return baseAddress.Replace("*", System.Net.Dns.GetHostEntry("localhost").HostName);
        }
    }
}
