﻿using System;

namespace Eurolook.Web.Common.ScheduledTask
{
    public interface IScheduledTask: IDisposable
    {
        void Setup();

        void Start();
    }
}
