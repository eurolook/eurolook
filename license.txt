EUROLOOK

MIT License

Copyright (c) 2024 European Commission

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

========================================================================================

This product uses software developed by third parties

Copies of the MIT, Apache 2.0, BSD-3-Clause licences and of the MICROSOFT SOFTWARE LICENSE TERMS under which the third party software used is licenced are provided at the end of this document.


.NET Packages (NuGet)
----------------------------

AspNetCore.HealthChecks.SqlServer
	Version 6.0.2
	Apache-2.0
	No copyright info available

Autofac
	Version 8.0.0
	MIT
	Copyright © 2015 Autofac Contributors

Autofac.Extensions.DependencyInjection
	Version 9.0.0
	MIT
	Copyright © 2015 Autofac Contributors

AutoMapper
	Version 12.0.1
	MIT
	Copyright (c) 2010 Jimmy Bogard

AvalonEdit
	Version 6.3.0.90
	MIT
	2000-2023 AlphaSierraPapa for the SharpDevelop Team

CommandLineParser
	Version 2.9.1
	License.md
	Copyright (c) 2005 - 2020 Giacomo Stelluti Scala & Contributors

DocumentFormat.OpenXml
	Version 2.20.0
	MIT
	© Microsoft Corporation. All rights reserved.

EFCore.BulkExtensions
	Version 3.6.6
	MIT
	Copyright (c) 2017-present: borisdj (Boris Djurdjevic)

GitVersion.MsBuild
	Version 5.12.0
	MIT
	Copyright GitTools 2023.

GSS.Authentication.CAS.AspNetCore
	Version 5.4.0
	MIT
	No copyright info available

jQuery
	Version 3.7.1
	MIT
	Copyright 2023 OpenJS Foundation

jQuery.UI.Combined
	Version 1.13.2
	MIT
	Copyright 2023 OpenJS Foundation

Knockout-Sortable
	Version 1.2.0
	MIT
	Copyright (c) 2015 Ryan Niemeyer

knockoutjs
	Version 3.4.1
	MIT
	Copyright (c) 2010 Steven Sanderson, the Knockout.js team, and other contributors

LinqKit.Microsoft.EntityFrameworkCore
	Version 3.2.4
	MIT
	Copyright (c) 2007-2019 Joseph Albahari, Tomas Petricek, Scott Smith

MaterialDesignThemes
	Version 4.8.0
	MIT
	Copyright (c) James Willock,  Mulholland Software and Contributors

Microsoft.AspNet.WebApi.Client
	Version 5.2.9
	NET_Library_EULA_ENU.txt
	© Microsoft Corporation. All rights reserved.

Microsoft.AspNetCore.Authentication.JwtBearer
	Version 6.0.5
	MIT
	© Microsoft Corporation. All rights reserved.

Microsoft.AspNetCore.Mvc.NewtonsoftJson
	Version 6.0.5
	MIT
	© Microsoft Corporation. All rights reserved.

Microsoft.AspNetCore.SpaServices.Extensions
	Version 6.0.10
	MIT
	© Microsoft Corporation. All rights reserved.

Microsoft.Build.Utilities.Core
	Version 16.8.0
	MIT
	© Microsoft Corporation. All rights reserved.

Microsoft.EntityFrameworkCore
	Version 3.1.32
	Apache-2.0
	© Microsoft Corporation. All rights reserved.

Microsoft.EntityFrameworkCore.Design
	Version 3.1.32
	Apache-2.0
	© Microsoft Corporation. All rights reserved.

Microsoft.EntityFrameworkCore.Relational
	Version 3.1.32
	Apache-2.0
	© Microsoft Corporation. All rights reserved.

Microsoft.EntityFrameworkCore.Sqlite
	Version 3.1.32
	Apache-2.0
	© Microsoft Corporation. All rights reserved.

Microsoft.EntityFrameworkCore.SqlServer
	Version 3.1.32
	Apache-2.0
	© Microsoft Corporation. All rights reserved.

Microsoft.EntityFrameworkCore.Tools
	Version 3.1.32
	Apache-2.0
	© Microsoft Corporation. All rights reserved.

Microsoft.Extensions.Configuration.EnvironmentVariables
	Version 6.0.1
	MIT
	© Microsoft Corporation. All rights reserved.

Microsoft.Extensions.Configuration.Json
	Version 6.0.0
	MIT
	© Microsoft Corporation. All rights reserved.

Microsoft.Extensions.Logging.Configuration
	Version 3.1.32
	Apache-2.0
	© Microsoft Corporation. All rights reserved.

Microsoft.Extensions.Logging.Console
	Version 3.1.24
	Apache-2.0
	© Microsoft Corporation. All rights reserved.

Microsoft.Extensions.Logging.Debug
	Version 3.1.24
	Apache-2.0
	© Microsoft Corporation. All rights reserved.

Microsoft.FeatureManagement.AspNetCore
	Version 3.2.0
	https://licenses.nuget.org/MIT
	© Microsoft Corporation. All rights reserved.

Microsoft.Win32.Registry
	Version 5.0.0
	MIT
	© Microsoft Corporation. All rights reserved.

Microsoft.Windows.Compatibility
	Version 6.0.0
	MIT
	© Microsoft Corporation. All rights reserved.

Modernizr
	Version 2.8.3
	MIT
	Copyright (c) 2021 The Modernizr Team

MvvmLightLibs
	Version 5.4.1.1
	MIT
	Copyright 2009-2018 Laurent Bugnion (GalaSoft)

Newtonsoft.Json
	Version 13.0.3
	MIT
	Copyright © James Newton-King 2008

NLog
	Version 5.1.3
	BSD-3-Clause
	Copyright (c) 2004-2023 NLog Project - https://nlog-project.org/

NLog.Schema
	Version 5.1.3
	BSD-3-Clause
	Copyright (c) 2004-2021 Jaroslaw Kowalski <jaak@jkowalski.net>, Kim Christensen, Julian Verdurmen

OpenIddict.AspNetCore
	Version 3.1.1
	Apache-2.0
	© Kévin Chalet. All rights reserved.

Serilog
	Version 2.10.0
	Apache-2.0
	Copyright (c) 2013-2020 Serilog Contributors

Serilog.AspNetCore
	Version 5.0.0
	Apache-2.0
	Copyright (c) 2013-2020 Serilog Contributors

Serilog.Enrichers.Environment
	Version 2.2.0
	Apache-2.0
	Copyright (c) 2013-2020 Serilog Contributors

Serilog.Enrichers.Thread
	Version 3.1.0
	Apache-2.0
	Copyright (c) 2013-2020 Serilog Contributors

Serilog.Settings.Configuration
	Version 3.3.0
	Apache-2.0
	Copyright (c) 2013-2020 Serilog Contributors

Serilog.Sinks.Console
	Version 4.0.1
	Apache-2.0
	Copyright (c) 2013-2020 Serilog Contributors

Serilog.Sinks.Email
	Version 2.4.0
	Apache-2.0
	Copyright (c) 2013-2020 Serilog Contributors

Serilog.Sinks.File
	Version 5.0.0
	Apache-2.0
	Copyright (c) 2013-2020 Serilog Contributors

SharpVectors.Wpf
	Version 1.8.1
	BSD-3-Clause
	Copyright (c) 2010 - 2022 Elinam LLC

SlowCheetah
	Version 2.5.48
	Apache-2.0
	Copyright 2012-2013 Sayed Ibrahim Hashimi

SQLitePCLRaw.bundle_e_sqlite3
	Version 2.1.4
	Apache-2.0
	Copyright 2014-2023 SourceGear, LLC

StyleCop.Analyzers
	Version 1.2.0-beta.556
	MIT
	Copyright 2015 Tunnel Vision Laboratories, LLC

Swashbuckle.AspNetCore
	Version 6.3.1
	MIT
	Copyright (c) 2016 Richard Morris

Swashbuckle.AspNetCore.Newtonsoft
	Version 6.3.1
	MIT
	Copyright (c) 2016 Richard Morris

JavaScript Packages (Npm)
----------------------------

@dnd-kit/core
	Version 6.0.7
	MIT
	Copyright (c) 2021, Claudéric Demers

@dnd-kit/sortable
	Version 7.0.2
	MIT
	Copyright (c) 2021, Claudéric Demers

@heroicons/react
	Version 2.0.13
	MIT
	Copyright (c) 2020 Refactoring UI Inc.

@monaco-editor/react
	Version 4.4.1
	MIT
	Copyright (c) 2018 Suren Atoyan

@popperjs/core
	Version 2.11.5
	MIT
	Copyright (c) 2019 Federico Zivolo

bootstrap
	Version 5.2.0
	MIT
	Copyright (c) 2011-2023 The Bootstrap Authors

chart.js
	Version 4.3.0
	MIT
	Copyright (c) 2014-2022 Chart.js Contributors

clean-webpack-plugin
	Version 3.0.0
	MIT
	Copyright (c) 2015 John Agan

copy-webpack-plugin
	Version 11.0.0
	MIT
	Copyright JS Foundation and other contributors

cross-env
	Version 7.0.3
	MIT
	Copyright (c) 2017 Kent C. Dodds

css-loader
	Version 6.8.1
	MIT
	Copyright JS Foundation and other contributors

downloadjs
	Version 1.4.7
	MIT
	Copyright (c) 2016 dandavis

file-loader
	Version 6.2.0
	MIT
	Copyright JS Foundation and other contributors

filesize
	Version 9.0.11
	BSD-3-Clause
	Copyright (c) 2023, Jason Mulligan

jquery
	Version 2.2.4
	MIT
	Copyright OpenJS Foundation and other contributors

jquery-datetimepicker
	Version 2.5.20
	MIT
	Copyright (c) 2013 http://xdsoft.net

jqueryui
	Version 1.11.1
	MIT
	No copyright info available

jszip
	Version 3.10.0
	(MIT OR GPL-3.0-or-later)
	Copyright (c) 2009-2016 Stuart Knightley, David Duponchel, Franz Buchinger, António Afonso

knockout
	Version 3.4.1
	MIT
	Copyright (c) 2010 Steven Sanderson, the Knockout.js team, and other contributors

monaco-editor
	Version 0.32.1
	MIT
	Copyright (c) 2016 - present Microsoft Corporation

nan
	Version 2.14.2
	MIT
	Copyright (c) 2018 NAN contributors

polished
	Version 4.2.2
	MIT
	Copyright (c) 2016 - 2021 Brian Hough and Maximilian Stoiber

react
	Version 17.0.2
	MIT
	Copyright (c) Meta Platforms, Inc. and affiliates.

react-async-hook
	Version 4.0.0
	MIT
	No copyright info available

react-cropper
	Version 2.1.8
	MIT
	Copyright (c) 2015 Kuanghui Fong

react-dom
	Version 17.0.2
	MIT
	Copyright (c) Meta Platforms, Inc. and affiliates.

react-redux
	Version 7.2.4
	MIT
	Copyright (c) 2015-present Dan Abramov

react-router-dom
	Version 6.3.0
	MIT
	Copyright (c) React Training LLC 2015-2019 Copyright (c) Remix Software Inc. 2020-2021 Copyright (c) Shopify Inc. 2022-2023

react-toastify
	Version 7.0.3
	MIT
	Copyright (c) 2023 Fadi Khadra

react-tooltip
	Version 4.2.19
	MIT
	Copyright (c) 2022 ReactTooltip Team Github

reactstrap
	Version 9.1.2
	MIT
	Copyright (c) 2016-Present Eddy Hernandez, Chris Burrell, Evan Sharp

recoil
	Version 0.7.4
	MIT
	Copyright (c) Facebook, Inc. and its affiliates.

redux
	Version 4.0.4
	MIT
	Copyright (c) 2015-present Dan Abramov

redux-logger
	Version 3.0.6
	MIT
	Copyright (c) 2016 Eugene Rodionov

redux-thunk
	Version 2.3.0
	MIT
	Copyright (c) 2015-present Dan Abramov

sass
	Version 1.54.0
	MIT
	Copyright (c) 2016, Google Inc.

style-loader
	Version 3.3.3
	MIT
	Copyright JS Foundation and other contributors

styled-components
	Version 5.3.5
	MIT
	Copyright (c) 2016-present Glen Maddern and Maximilian Stoiber

ts-loader
	Version 8.0.18
	MIT
	Copyright (c) 2015-present TypeStrong

========================================================================================

MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

========================================================================================

BSD 3-Clause License

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================================

Apache License, Version 2.0

https://www.apache.org/licenses/

TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION

1. Definitions.

"License" shall mean the terms and conditions for use, reproduction, and distribution as defined by Sections 1 through 9 of this document.

"Licensor" shall mean the copyright owner or entity authorized by the copyright owner that is granting the License.

"Legal Entity" shall mean the union of the acting entity and all other entities that control, are controlled by, or are under common control with that entity. For the purposes of this definition, "control" means (i) the power, direct or indirect, to cause the direction or management of such entity, whether by contract or otherwise, or (ii) ownership of fifty percent (50%) or more of the outstanding shares, or (iii) beneficial ownership of such entity.

"You" (or "Your") shall mean an individual or Legal Entity exercising permissions granted by this License.

"Source" form shall mean the preferred form for making modifications, including but not limited to software source code, documentation source, and configuration files.

"Object" form shall mean any form resulting from mechanical transformation or translation of a Source form, including but not limited to compiled object code, generated documentation, and conversions to other media types.

"Work" shall mean the work of authorship, whether in Source or Object form, made available under the License, as indicated by a copyright notice that is included in or attached to the work (an example is provided in the Appendix below).

"Derivative Works" shall mean any work, whether in Source or Object form, that is based on (or derived from) the Work and for which the editorial revisions, annotations, elaborations, or other modifications represent, as a whole, an original work of authorship. For the purposes of this License, Derivative Works shall not include works that remain separable from, or merely link (or bind by name) to the interfaces of, the Work and Derivative Works thereof.

"Contribution" shall mean any work of authorship, including the original version of the Work and any modifications or additions to that Work or Derivative Works thereof, that is intentionally submitted to Licensor for inclusion in the Work by the copyright owner or by an individual or Legal Entity authorized to submit on behalf of the copyright owner. For the purposes of this definition, "submitted" means any form of electronic, verbal, or written communication sent to the Licensor or its representatives, including but not limited to communication on electronic mailing lists, source code control systems, and issue tracking systems that are managed by, or on behalf of, the Licensor for the purpose of discussing and improving the Work, but excluding communication that is conspicuously marked or otherwise designated in writing by the copyright owner as "Not a Contribution."

"Contributor" shall mean Licensor and any individual or Legal Entity on behalf of whom a Contribution has been received by Licensor and subsequently incorporated within the Work.

2. Grant of Copyright License. Subject to the terms and conditions of this License, each Contributor hereby grants to You a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable copyright license to reproduce, prepare Derivative Works of, publicly display, publicly perform, sublicense, and distribute the Work and such Derivative Works in Source or Object form.

3. Grant of Patent License. Subject to the terms and conditions of this License, each Contributor hereby grants to You a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable (except as stated in this section) patent license to make, have made, use, offer to sell, sell, import, and otherwise transfer the Work, where such license applies only to those patent claims licensable by such Contributor that are necessarily infringed by their Contribution(s) alone or by combination of their Contribution(s) with the Work to which such Contribution(s) was submitted. If You institute patent litigation against any entity (including a cross-claim or counterclaim in a lawsuit) alleging that the Work or a Contribution incorporated within the Work constitutes direct or contributory patent infringement, then any patent licenses granted to You under this License for that Work shall terminate as of the date such litigation is filed.

4. Redistribution. You may reproduce and distribute copies of the Work or Derivative Works thereof in any medium, with or without modifications, and in Source or Object form, provided that You meet the following conditions:

    You must give any other recipients of the Work or Derivative Works a copy of this License; and
    You must cause any modified files to carry prominent notices stating that You changed the files; and
    You must retain, in the Source form of any Derivative Works that You distribute, all copyright, patent, trademark, and attribution notices from the Source form of the Work, excluding those notices that do not pertain to any part of the Derivative Works; and
    If the Work includes a "NOTICE" text file as part of its distribution, then any Derivative Works that You distribute must include a readable copy of the attribution notices contained within such NOTICE file, excluding those notices that do not pertain to any part of the Derivative Works, in at least one of the following places: within a NOTICE text file distributed as part of the Derivative Works; within the Source form or documentation, if provided along with the Derivative Works; or, within a display generated by the Derivative Works, if and wherever such third-party notices normally appear. The contents of the NOTICE file are for informational purposes only and do not modify the License. You may add Your own attribution notices within Derivative Works that You distribute, alongside or as an addendum to the NOTICE text from the Work, provided that such additional attribution notices cannot be construed as modifying the License.

You may add Your own copyright statement to Your modifications and may provide additional or different license terms and conditions for use, reproduction, or distribution of Your modifications, or for any such Derivative Works as a whole, provided Your use, reproduction, and distribution of the Work otherwise complies with the conditions stated in this License.

5. Submission of Contributions. Unless You explicitly state otherwise, any Contribution intentionally submitted for inclusion in the Work by You to the Licensor shall be under the terms and conditions of this License, without any additional terms or conditions. Notwithstanding the above, nothing herein shall supersede or modify the terms of any separate license agreement you may have executed with Licensor regarding such Contributions.

6. Trademarks. This License does not grant permission to use the trade names, trademarks, service marks, or product names of the Licensor, except as required for reasonable and customary use in describing the origin of the Work and reproducing the content of the NOTICE file.

7. Disclaimer of Warranty. Unless required by applicable law or agreed to in writing, Licensor provides the Work (and each Contributor provides its Contributions) on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied, including, without limitation, any warranties or conditions of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A PARTICULAR PURPOSE. You are solely responsible for determining the appropriateness of using or redistributing the Work and assume any risks associated with Your exercise of permissions under this License.

8. Limitation of Liability. In no event and under no legal theory, whether in tort (including negligence), contract, or otherwise, unless required by applicable law (such as deliberate and grossly negligent acts) or agreed to in writing, shall any Contributor be liable to You for damages, including any direct, indirect, special, incidental, or consequential damages of any character arising as a result of this License or out of the use or inability to use the Work (including but not limited to damages for loss of goodwill, work stoppage, computer failure or malfunction, or any and all other commercial damages or losses), even if such Contributor has been advised of the possibility of such damages.

9. Accepting Warranty or Additional Liability. While redistributing the Work or Derivative Works thereof, You may choose to offer, and charge a fee for, acceptance of support, warranty, indemnity, or other liability obligations and/or rights consistent with this License. However, in accepting such obligations, You may act only on Your own behalf and on Your sole responsibility, not on behalf of any other Contributor, and only if You agree to indemnify, defend, and hold each Contributor harmless for any liability incurred by, or claims asserted against, such Contributor by reason of your accepting any such warranty or additional liability.

END OF TERMS AND CONDITIONS

========================================================================================

NET_Library_EULA_ENU.txt
These license terms are an agreement between you and Microsoft Corporation (or based on where you live, one of its affiliates). They apply to the software named above. The terms also apply to any Microsoft services or updates for the software, except to the extent those have different terms.

If you comply with these license terms, you have the rights below.
1.    INSTALLATION AND USE RIGHTS.

You may install and use any number of copies of the software to develop and test your applications. 
2.    THIRD PARTY COMPONENTS. The software may include third party components with separate legal notices or governed by other agreements, as may be described in the ThirdPartyNotices file(s) accompanying the software.
3.    ADDITIONAL LICENSING REQUIREMENTS AND/OR USE RIGHTS.
a.     DISTRIBUTABLE CODE.  The software is comprised of Distributable Code. “Distributable Code” is code that you are permitted to distribute in applications you develop if you comply with the terms below.
i.      Right to Use and Distribute.

·        You may copy and distribute the object code form of the software.

·        Third Party Distribution. You may permit distributors of your applications to copy and distribute the Distributable Code as part of those applications.
ii.     Distribution Requirements. For any Distributable Code you distribute, you must

·        use the Distributable Code in your applications and not as a standalone distribution;

·        require distributors and external end users to agree to terms that protect it at least as much as this agreement; and

·        indemnify, defend, and hold harmless Microsoft from any claims, including attorneys’ fees, related to the distribution or use of your applications, except to the extent that any claim is based solely on the unmodified Distributable Code.
iii.   Distribution Restrictions. You may not

·        use Microsoft’s trademarks in your applications’ names or in a way that suggests your applications come from or are endorsed by Microsoft; or

·        modify or distribute the source code of any Distributable Code so that any part of it becomes subject to an Excluded License. An “Excluded License” is one that requires, as a condition of use, modification or distribution of code, that (i) it be disclosed or distributed in source code form; or (ii) others have the right to modify it.
4.    DATA.
a.     Data Collection. The software may collect information about you and your use of the software, and send that to Microsoft. Microsoft may use this information to provide services and improve our products and services.  You may opt-out of many of these scenarios, but not all, as described in the software documentation.  There are also some features in the software that may enable you and Microsoft to collect data from users of your applications. If you use these features, you must comply with applicable law, including providing appropriate notices to users of your applications together with Microsoft’s privacy statement. Our privacy statement is located at https://go.microsoft.com/fwlink/?LinkID=824704. You can learn more about data collection and its use from the software documentation and our privacy statement. Your use of the software operates as your consent to these practices.
b.    Processing of Personal Data. To the extent Microsoft is a processor or subprocessor of personal data in connection with the software, Microsoft makes the commitments in the European Union General Data Protection Regulation Terms of the Online Services Terms to all customers effective May 25, 2018, at https://docs.microsoft.com/en-us/legal/gdpr.
5.    Scope of License. The software is licensed, not sold. This agreement only gives you some rights to use the software. Microsoft reserves all other rights. Unless applicable law gives you more rights despite this limitation, you may use the software only as expressly permitted in this agreement. In doing so, you must comply with any technical limitations in the software that only allow you to use it in certain ways. You may not

·        work around any technical limitations in the software;

·        reverse engineer, decompile or disassemble the software, or otherwise attempt to derive the source code for the software, except and to the extent required by third party licensing terms governing use of certain open source components that may be included in the software;

·        remove, minimize, block or modify any notices of Microsoft or its suppliers in the software;

·        use the software in any way that is against the law; or

·        share, publish, rent or lease the software, provide the software as a stand-alone offering for others to use, or transfer the software or this agreement to any third party.
6.    Export Restrictions. You must comply with all domestic and international export laws and regulations that apply to the software, which include restrictions on destinations, end users, and end use. For further information on export restrictions, visit www.microsoft.com/exporting.  
7.    SUPPORT SERVICES. Because this software is “as is,” we may not provide support services for it.
8.    Entire Agreement. This agreement, and the terms for supplements, updates, Internet-based services and support services that you use, are the entire agreement for the software and support services.
9.    Applicable Law.  If you acquired the software in the United States, Washington law applies to interpretation of and claims for breach of this agreement, and the laws of the state where you live apply to all other claims. If you acquired the software in any other country, its laws apply.
10. CONSUMER RIGHTS; REGIONAL VARIATIONS. This agreement describes certain legal rights. You may have other rights, including consumer rights, under the laws of your state or country. Separate and apart from your relationship with Microsoft, you may also have rights with respect to the party from which you acquired the software. This agreement does not change those other rights if the laws of your state or country do not permit it to do so. For example, if you acquired the software in one of the below regions, or mandatory country law applies, then the following provisions apply to you:
a)    Australia. You have statutory guarantees under the Australian Consumer Law and nothing in this agreement is intended to affect those rights.
b)    Canada. If you acquired this software in Canada, you may stop receiving updates by turning off the automatic update feature, disconnecting your device from the Internet (if and when you re-connect to the Internet, however, the software will resume checking for and installing updates), or uninstalling the software. The product documentation, if any, may also specify how to turn off updates for your specific device or software.
c)    Germany and Austria.

(i)        Warranty. The software will perform substantially as described in any Microsoft materials that accompany it. However, Microsoft gives no contractual guarantee in relation to the software.

(ii)       Limitation of Liability. In case of intentional conduct, gross negligence, claims based on the Product Liability Act, as well as in case of death or personal or physical injury, Microsoft is liable according to the statutory law.
Subject to the foregoing clause (ii), Microsoft will only be liable for slight negligence if Microsoft is in breach of such material contractual obligations, the fulfillment of which facilitate the due performance of this agreement, the breach of which would endanger the purpose of this agreement and the compliance with which a party may constantly trust in (so-called "cardinal obligations"). In other cases of slight negligence, Microsoft will not be liable for slight negligence
11. Disclaimer of Warranty. THE SOFTWARE IS LICENSED “AS-IS.” YOU BEAR THE RISK OF USING IT. MICROSOFT GIVES NO EXPRESS WARRANTIES, GUARANTEES OR CONDITIONS. TO THE EXTENT PERMITTED UNDER YOUR LOCAL LAWS, MICROSOFT EXCLUDES THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
12. Limitation on and Exclusion of Remedies and Damages. YOU CAN RECOVER FROM MICROSOFT AND ITS SUPPLIERS ONLY DIRECT DAMAGES UP TO U.S. $5.00. YOU CANNOT RECOVER ANY OTHER DAMAGES, INCLUDING CONSEQUENTIAL, LOST PROFITS, SPECIAL, INDIRECT OR INCIDENTAL DAMAGES.

This limitation applies to (a) anything related to the software, services, content (including code) on third party Internet sites, or third party applications; and (b) claims for breach of contract, breach of warranty, guarantee or condition, strict liability, negligence, or other tort to the extent permitted by applicable law.

It also applies even if Microsoft knew or should have known about the possibility of the damages. The above limitation or exclusion may not apply to you because your state or country may not allow the exclusion or limitation of incidental, consequential or other damages.

